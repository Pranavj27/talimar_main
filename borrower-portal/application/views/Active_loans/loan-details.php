<style type="text/css">
    .addspace{
        margin-bottom: 10px;
    }

    label{
        font-weight: 600;
    }

    h4.addbg{
        font-weight: 600;
        padding: 12px 1px;
        background: #e2e2e2;
        font-size: 16px;
    }
</style>
<div class="page-container">
                <!-- BEGIN SIDEBAR -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content" style="padding-left: 30px;padding-right: 30px;">
                <!-- BEGIN PAGE HEADER-->
                <div class="row">
                	<div class="col-md-10">
		                <div class="page-bar" style="border-bottom: 0px !important;padding-top: 10px;padding-bottom: 10px;">
		                    <h4><b>Loan Details</b></h4>
		                </div>
		            </div>
                    <!-- <div class="col-md-2">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Request Payoff</button>
                    </div> -->
		        </div>

                <div class="row">
                    <div class="col-md-12">
                        <?php if($this->session->flashdata('error') !=''){ ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <?php echo $this->session->flashdata('error');?> 
                            </div>
                        <?php } ?>
                        <?php if($this->session->flashdata('success') !=''){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <?php echo $this->session->flashdata('success');?> 
                            </div>
                        <?php } ?>
                    </div>
                </div>


                <div class="row addspace">
                    <div class="col-md-12">
                        <label>Property Address:</label>
                        <span><?php echo isset($Alldetails) ? $Alldetails[0]->property_address.''.$Alldetails[0]->unit.', '.$Alldetails[0]->city.', '.$Alldetails[0]->state.' '.$Alldetails[0]->zip : '';?></span>
                    </div>
                </div>

                <div class="row addspace">
                    <div class="col-md-4">
                        <label>Loan Amount:</label>
                        <span><?php echo isset($Alldetails) ? '$'.number_format($Alldetails[0]->loan_amount) : '';?></span>
                    </div>
                    <div class="col-md-4">
                        <label>Outstanding Balance:</label>
                        <span><?php echo isset($Alldetails) ? '$'.number_format($Alldetails[0]->current_balance) : '';?></span>
                    </div>
                    <div class="col-md-4">
                        <label></label>
                        <span></span>
                    </div>
                </div>

                <div class="row addspace">
                    <div class="col-md-4">
                        <label>Payment:</label>
                        <span></span>
                    </div>
                    <div class="col-md-4">
                        <label>Payment Due Date:</label>
                        <span><?php echo isset($Alldetails) ? date('m-d-Y', strtotime($Alldetails[0]->first_payment_date)) : '';?></span>
                    </div>
                    <div class="col-md-4">
                        <label></label>
                        <span></span>
                    </div>
                </div>

                <div class="row addspace">
                    <div class="col-md-4">
                        <label>Close Date:</label>
                        <span><?php echo isset($Alldetails) ? date('m-d-Y', strtotime($Alldetails[0]->loan_funding_date)) : '';?></span>
                    </div>
                    <div class="col-md-4">
                        <label>Maturity Date:</label>
                        <span><?php echo isset($Alldetails) ? date('m-d-Y', strtotime($Alldetails[0]->maturity_date)) : '';?></span>
                    </div>
                    <div class="col-md-4">
                        <label></label>
                        <span></span>
                    </div>
                </div>

                <div class="row addspace">
                    <div class="col-md-12">
                        <h4 class="addbg">Construction Funds Account</h4>
                    </div>
                </div>

                

               

            </div>

    </div>

</div>