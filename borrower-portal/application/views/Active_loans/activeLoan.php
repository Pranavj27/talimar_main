<?php
$loan_status_option = $this->config->item('loan_status_option');
$position_option = $this->config->item('position_option');
$loan_payment_status = $this->config->item('loan_payment_status');

?>
<style type="text/css">
	.mb-4{
		margin-bottom: 20px;
	}
</style>
<div class="page-container">
                <!-- BEGIN SIDEBAR -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content" style="padding-left: 30px;padding-right: 30px;">
                <!-- BEGIN PAGE HEADER-->
                <div class="row">
                	<div class="col-md-12">
		                <div class="page-bar" style="border-bottom: 0px !important;padding-top: 10px;padding-bottom: 10px;">
		                    <h4><b>Active Loans</b></h4>
		                </div>
		            </div>
		        </div>
                <div class="row mb-4">
                	<form method="post" action="<?php echo base_url();?>activeLoan">
	                	<div class="col-md-3">
	                		<label>Account Name:</label>
	                		<select class="form-control" name="account_name">
	                			<option value="">Select All</option>
	                			<?php 
		                			if(isset($displayallaccount) && is_array($displayallaccount)){
		                				foreach($displayallaccount as $row){ ?>
		                					<option value="<?php echo $row->b_id;?>" <?php if($account_name == $row->b_id){echo 'selected';}?>><?php echo $row->b_name;?></option>
	                			<?php } } ?>
	                		</select>
	                	</div>
	                	<div class="col-md-1">
	                		<button type="submit" name="submit" class="btn btn-primary btn-sm" style="margin-top: 25px;">Filter</button>
	                	</div>
	                </form>

                </div>
                <div class="row">
                    <div class="col-lg-12 table-responsive">

                    	<table class="tableexampleddd table table-bordered">
					        <thead class="thead_dark_blue">
					            <tr>
					                <th>Location</th>
					                <th>Position</th>
					                <th>Loan Amount</th>
					                <th>Outstanding Balance</th>
					                <th>Interest Rate</th>
					                <th>Monthly Payment</th>
					                <th>Loan Status</th>
					                <th>Payment Status</th>
					            </tr>
					        </thead>
					        <tbody>

					        <?php 
					        	$count = 0;
					        	$totalamout = 0;
					        	
					        	if(isset($activeAccountinfo) && is_array($activeAccountinfo)){ 
					        			foreach($activeAccountinfo as $row){ 

					        		$count++;
					        		$totalamout += $row['loan_amount'];
					        ?>
						        	<tr>
						               <td><a href="<?php echo base_url();?>loan-details/<?php echo $row['talimar_loan'];?>"><?php echo $row['location'];?></a></td>
						               <td><?php echo $position_option[$row['position']];?></td>
						               <td>$<?php echo number_format($row['loan_amount']);?></td>
						               <td>$<?php echo number_format($row['current_balance']);?></td>
						               <td><?php echo number_format($row['intrest_rate'],2);?>%</td>
						               <td>$<?php echo number_format($row['monthlypayment'],2);?></td>
						               <td><?php echo $loan_status_option[$row['loan_status']];?></td>
						               <td><?php echo $loan_payment_status[$row['loan_payment_status']];?></td>
						            </tr>

					        <?php } ?>

					            <tr>
					                <th>Total: <?php echo $count;?></th>
					                <th></th>
					                <th>$<?php echo number_format($totalamout);?></th>
					                <th></th>
					                <th></th>
					                <th></th>
					                <th></th>
					                <th></th>
					                
					            </tr>

					        <?php }else{ ?>
					        	<tr>
					                <td colspan="9">No data found</td>
					            </tr>
					        <?php } ?>
					        </tbody>
					        
					    </table>
                        
                    </div>
                </div>
                <div class="clearfix"></div> 
                   
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
   
    <!-- END QUICK SIDEBAR -->
</div>