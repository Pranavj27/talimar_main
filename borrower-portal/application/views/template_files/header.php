<?php
$loginid = $this->session->userdata('login_bid');
$checkIMG = $this->User_model->query("SELECT `imgurl` FROM `borrower_contact_type` WHERE `contact_id`= '".$loginid."' ");
$checkIMG = $checkIMG->result();
$ImgLink  = $checkIMG[0]->imgurl;
if($ImgLink != ''){
    $userimg = $ImgLink;
}else{
    $userimg = base_url().'borrowerAssets/img/avatar1.jpg';
}

?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Borrower Portal</title>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>borrowerAssets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>borrowerAssets/css/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>borrowerAssets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>borrowerAssets/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url();?>borrowerAssets/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url();?>borrowerAssets/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url();?>borrowerAssets/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>borrowerAssets/css/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url();?>borrowerAssets/css/custom.min.css" rel="stylesheet" type="text/css" />
        
        <!-- END THEME LAYOUT STYLES -->

        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"> -->
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        

        <style>
        .page-header.navbar .page-logo .logo-default {
            margin: 9px 0 0;
            width: 132px;
        }

        *{
            font-family: 'Open Sans', sans-serif !important;
        }

        .page-header-top{
            padding-left: 20px;
            padding-right: 20px;

        }

        .top-menunenu li a{
            color: #fff !important;
        }

        .page-header.navbar {
            background-color: #103a60 !important;
        }

        .top-menunenu, .top-menu{
            padding-left: 25px;
            padding-right: 25px;

        }
        a{
            text-decoration: none !important;
        }

        thead.thead_dark_blue th {
            background: #ddd !important;
            color: #000 !important;
            text-align: center !important;
        }
        .page-header.navbar .top-menu .navbar-nav>li.dropdown-user .dropdown-menu>li>a {
            font-size: 13px !important;
            font-weight: 600 !important;
        }
        .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
            border: 0px !important;
        }

        h4.deed_heading {
            background-color: #ececec;
            padding: 12px 10px;
            font-weight: 600;
        }

        .alert-success{
            background-color: #46a03b; 
            color: #fff;
        }

        .page-footer{
            background-color: #103a60;
        }

        .page-footer .page-footer-inner {
            color: #ffffff;
        }
        
        </style>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>borrowerAssets/img/favicon.png">
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
                <div class="page-header-top">
                    
                    <div class="page-logo">
                        <a href="<?php echo base_url();?>dashboard">
                            <img src="<?php echo base_url();?>borrowerAssets/img/logo-big.png" alt="logo" class="logo-default" style="width: 180px; margin: 20px 12px;">
                        </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    
                </div>
                <div class="page-header navbar navbar-fixed-top">
                    <!-- BEGIN HEADER INNER -->
                    
                    <div class="page-header-inner ">
                        <!-- BEGIN LOGO -->
                        <div class="top-menunenu">
                           
                            <li class="nav-item">
                                <a href="<?php echo base_url();?>dashboard" class="nav-link nav-toggle">
                                    <span class="title">Dashboard</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url();?>activeLoan" class="nav-link nav-toggle">
                                    <span class="title">Active Loans</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url();?>pof-request" class="nav-link nav-toggle">
                                    <span class="title">POF Request</span>
                                </a>
                            </li>
                        </div>
                        <!-- END RESPONSIVE MENU TOGGLER -->
                        <!-- BEGIN TOP NAVIGATION MENU -->
                        <div class="top-menu">
                            <ul class="nav navbar-nav pull-right"> 
                        
                               
                                <li class="dropdown dropdown-user">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <img alt="User profile" class="img-circle" src="<?php echo $userimg;?>">
                                        <span class="username username-hide-on-mobile"><b><?php echo $this->session->userdata('login_bname'); ?></b></span>
                                        
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-default">
                                        <li>
                                            <a href="<?php echo base_url();?>profile"> My Profile</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url();?>home/changePassword">Change Password</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url();?>signout"> Log Out</a>
                                        </li>
                                    </ul>
                                </li>
                               
                            </ul>
                        </div>
                        <!-- END TOP NAVIGATION MENU -->
                    </div>
                    
                    <!-- END HEADER INNER -->
                </div>
            <!-- END HEADER -->
           
          
           
        