    <div class="page-footer">
        <div class="page-footer-inner pull-right"> 
          TaliMar Financial | Borrower Portal © <?php echo date('Y');?>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
       
    <div class="quick-nav-overlay"></div>
        <!-- END QUICK NAV -->
        
    <!-- BEGIN CORE PLUGINS -->
    <script src="<?php echo base_url();?>borrowerAssets/js/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>borrowerAssets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>borrowerAssets/js/js.cookie.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>borrowerAssets/js/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>borrowerAssets/js/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>borrowerAssets/js/bootstrap-switch.min.js" type="text/javascript"></script>

    <!-- END CORE PLUGINS -->
    
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>

    <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    
    <script type="text/javascript">
      
      $(document).ready(function() {
          $('.tableexample').DataTable({

              "searching": false,
              "bLengthChange": false,
          });
      });

      $( function() {
        $( ".datepicker" ).datepicker();
      });

      function replace_dollar(n)
      {
        var a = n.replace('$', '');
        var b = a.replace(',', '');
        var b = b.replace(',', '');
        var b = b.replace(',', '');
        var b = b.replace(',', '');
        var c = b.replace('%', '');
        return c;
      }

      $(document).ready(function() {
        $(".number_only").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
      });

     
      $(".amount_format").change(function(){
        var a = this.value;
        var a = replace_dollar(a);
        if(a == '')
        {
          a = 0;
        }
        a = parseFloat(a);
        this.value = '$'+a.toLocaleString();
      });
    </script>
</body>


</html>