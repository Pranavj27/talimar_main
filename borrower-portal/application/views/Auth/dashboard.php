<style type="text/css">
	.mb-4{
		margin-bottom: 20px;
	}
</style>
<div class="page-container">
                <!-- BEGIN SIDEBAR -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content" style="padding-left: 30px;padding-right: 30px;">
                <!-- BEGIN PAGE HEADER-->
                <div class="row">
                	<div class="col-md-12">
		                <div class="page-bar" style="border-bottom: 0px !important;padding-top: 10px;padding-bottom: 10px;">
		                    <h4><b>Dashboard</b></h4>
		                </div>
		            </div>
		        </div>
                <div class="row mb-4">
                	<form method="post" action="<?php echo base_url();?>dashboard">
                	<div class="col-md-3">
                		<label>Account Name:</label>
                		<select class="form-control" name="account_name">
                			<option value="">Select All</option>
                			<?php 
	                			if(isset($displayallaccount) && is_array($displayallaccount)){
	                				foreach($displayallaccount as $row){ ?>
	                					<option value="<?php echo $row->b_id;?>" <?php if($account_name == $row->b_id){echo 'selected';}?>><?php echo $row->b_name;?></option>
                			<?php } } ?>
                		</select>
                	</div>
                	<div class="col-md-1">
                		<button type="submit" name="submit" class="btn btn-primary btn-sm" style="margin-top: 25px;">Filter</button>
                	</div>

                </div>
                <div class="row">
                    <div class="col-lg-12 table-responsive">

                    	<table class="tableexampleddd table table-bordered">
					        <thead class="thead_dark_blue">
					            <tr>
					                <th>Account Name</th>
					                <th>Account #</th>
					                <th># of Active Loans</th>
					                <th>Active Balance</th>
					                
					            </tr>
					        </thead>
					        <tbody>
					            
					        	<?php 
					        	$count = 0;
					        	$totalcount = 0;
					        	$totalAmount = 0;
					        	if(isset($contactAccountsArray) && is_array($contactAccountsArray)){ 
					        			foreach($contactAccountsArray as $row){ 

					        			$count++;
					        			$totalcount += $row['totalcount'];
					        			$totalAmount += $row['totalAmount'];

					        	?>
							            <tr>
							                <td><a href="<?php echo base_url('activeLoan');?>/<?php echo $row['b_id'];?>"><?php echo $row['b_name'];?></a></td>
							                <td><?php echo $row['ira_account'];?></td>
							                <td><?php echo $row['totalcount'];?></td>
							                <td>$<?php echo number_format($row['totalAmount']);?></td>					
							                
							            </tr>
					        <?php } ?>

								        <tr>
							        		<th>Total: <?php echo $count;?></th>
							        		<th></th>
							        		<th><?php echo $totalcount;?></th>
							        		<th>$<?php echo number_format($totalAmount);?></th>
							        		
							        	</tr>

					    	<?php }else{ ?>
							        	<tr>
							                <td colspan="4">No data found!</td>
							            </tr>
					        <?php } ?>
					            
					        </tbody>
					       
					    </table>
                        
                    </div>
                </div>
                <div class="clearfix"></div> 
                   
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
   
    <!-- END QUICK SIDEBAR -->
</div>
