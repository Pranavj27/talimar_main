<!DOCTYPE html>

<html lang="en">
   
    <head>
        <meta charset="utf-8" />
        <title>TaliMar Financial | Borrower Portal</title>
        
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>borrowerAssets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>borrowerAssets/css/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>borrowerAssets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>borrowerAssets/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url();?>borrowerAssets/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>borrowerAssets/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url();?>borrowerAssets/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url();?>borrowerAssets/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?php echo base_url();?>borrowerAssets/css/login-5.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>borrowerAssets/img/favicon.png">
        <style> 
            * {
                font-family: 'Open Sans', sans-serif !important;
            }
        </style>
    </head>
    <!-- END HEAD -->
<style>
.user-login-5 .login-container>.login-content>h1 {
    font-size: 1.8em;
    font-weight: 600;
    color: #4e5a64;
    border-bottom: solid 1px #d3d0d0;
    padding-bottom: 16px;
    color: #123657 !important;
}


a.forget-password {
    margin-bottom: 14px;
    display: inline-block;
}
label {
    width: 100%;
    float: left;
    color: rgb(4,13,20);
    padding: 9px 0px;
    font-size: 13px;
    font-weight: 600;
}

form.login-form .col-xs-12 input {
    width: 100% !important;
    float: left;
    background: rgb(245 248 250);
    border: 1px solid #eceeef !important;
    font-size: 14px !important;
    padding: 10px 7px !important;
    border-radius: 2px !important;
    height: initial;
}

 .user-login-5 .login-container>.login-content>.login-form {
    margin-top: 25px;
}
button.btn.btn-primary {
    color: #fff;
    background-color: #3379b5;
    border-color: #3379b5;
    border: 0;
    padding: 11px 37px;
    border-radius: 4px !important;
    cursor: pointer;
    margin-top: 20px;
    float: left;
}


@media screen and (min-width: 1024px) {

.user-login-5 .login-container>.login-content {
    margin-top: 23% !important;
}
constructed stylesheet
.user-login-5 .login-container .login-copyright, .user-login-5 .login-container .login-social, .user-login-5 .login-container>.login-content {
    padding: 0 80px 0 40px;
}

}
</style>
    <body class=" login">
        <!-- BEGIN : LOGIN PAGE 5-1 -->
        <div class="user-login-5">
            <div class="row bs-reset">
                <div class="col-md-6 bs-reset mt-login-5-bsfix">
                    <div class="login-bg">
                        <img class="login-logo" src="<?php echo base_url();?>borrowerAssets/img/logo.png" /> 
                    </div>
                </div>
                <div class="col-md-6 login-container bs-reset mt-login-5-bsfix">
                    <div class="login-content">
                        <h1 style="margin-bottom: 10px;">Enter Your New Password Below</h1>
                        
                        <form action="<?php echo base_url();?>new_password" class="login-form" method="post">

                            <input type="hidden" name="contact_id" value="<?php echo $contact_id;?>">

                            <?php if ($this->session->flashdata('error') != '') {?>
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <span><b><?php echo $this->session->flashdata('error'); ?></b></span>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('success') != '') {?>
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <span><b><?php echo $this->session->flashdata('success'); ?></b></span>
                                </div>
                            <?php } ?>

                            <div class="row">
                                <div class="col-xs-12">
								<label>New Password</label>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Password" name="password" required minlength="8"> 
                                </div>
                                <div class="col-xs-12">
								<label>Confirm Password</label>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Confirm Password" name="cpassword" required > 
                                </div>
                            </div>
                            <div class="row">
                                
                                <div class="col-sm-12 text-left">
                                    <button class="btn btn-primary" type="submit" name="submit">Change Password</button>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                    <div class="login-footer">
                        <div class="row bs-reset">
                            <div class="col-xs-2 bs-reset">
                               <!---icons-->
                            </div>
                            <div class="col-xs-10 bs-reset">
                                <div class="login-copyright text-right">
                                    <p>Copyright &copy; Talimar Financial | Borrower Portal <?php echo date('Y');?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END : LOGIN PAGE 5-1 -->
        
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url();?>borrowerAssets/js/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>borrowerAssets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>borrowerAssets/js/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>borrowerAssets/js/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>borrowerAssets/js/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>borrowerAssets/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url();?>borrowerAssets/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>borrowerAssets/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>borrowerAssets/js/select2.full.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>borrowerAssets/js/jquery.backstretch.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url();?>borrowerAssets/js/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url();?>borrowerAssets/js/login-5.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
        <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
        </script>
    <!-- Google Code for Universal Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-37564768-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- End -->
</body>
</html>