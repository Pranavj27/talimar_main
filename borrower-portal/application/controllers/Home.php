<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct() {

		parent::__construct();
		$this->load->model('User_model');
		$this->load->library('email');
		$this->load->library('Aws3','aws3');
		
	}


	public function index()
	{
		$this->signin();
	}


	public function signin()
	{
		if(isset($_POST['submit'])){

			$username 	= $this->input->post('username');
			$password 	= $this->input->post('password');

			if($username && $password !=''){

				$where['borrower_username'] = $username;
				$where['borrower_password'] = base64_encode($password);

				$fetchborrower = $this->User_model->select_where('borrower_contact_type',$where);
				if($fetchborrower->num_rows() > 0){

					$fetchborrower = $fetchborrower->result();
					if($fetchborrower[0]->allow_access_borrower == '1'){

						$getcontactinfo = $this->fetchContactname($fetchborrower[0]->contact_id);
						$contactFullname = $getcontactinfo[0]->contact_firstname.' '.$getcontactinfo[0]->contact_lastname;

						$data['login_bid'] = $this->session->set_userdata('login_bid', $fetchborrower[0]->contact_id);
						$this->session->set_userdata('login_bname' , $contactFullname);
						$this->session->set_userdata('login_username' , $fetchborrower[0]->borrower_username);
						
						redirect(base_url()."dashboard");

					}else{

						$this->session->set_flashdata('error', 'No access allowed yet.');
						$this->load->view('Auth/signin');
					}
				}else{

					$this->session->set_flashdata('error', 'Username or Password is incorrect.');
					$this->load->view('Auth/signin');
				}
			}else{

				$this->session->set_flashdata('error', 'All fields are required.');
				$this->load->view('Auth/signin');
			}
		}else{

			$this->load->view('Auth/signin');
		}
	}


	public function signup()
	{
		if(isset($_POST['submit'])){

			echo '<pre>';
			print_r($_POST);
			echo '</pre>';
		}else{

			$this->load->view('Auth/signup');
		}
	}


	public function forgotpassword()
	{
		if(isset($_POST['submit'])){

			$email_address = $this->input->post('email');
			$fetch_email = $this->User_model->query("Select * from contact where contact_email = '".$email_address."'");
			if($fetch_email->num_rows() > 0){

				$fetch_email = $fetch_email->result();
				$contact_id  = $fetch_email[0]->contact_id;
				$name 		 = $fetch_email[0]->contact_firstname;

				$borrowerQal = $this->User_model->query("Select * from borrower_contact_type where contact_id = '".$contact_id."'");
				$borrowerData = $borrowerQal->result();
				$borrower_username = $borrowerData[0]->borrower_username;

				$resetLINK = base_url().'forgot-password-secure/'.$this->encodeURL($contact_id);

				$Message = '<div style="background-color: #eeeeef; padding: 50px 0; ">
						<div style="max-width:640px; margin:0 auto; ">
							<div style="color: #fff; text-align: center; background-color:#337ab7; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;">
								<h1>Forgot Password</h1>
							</div>
							<div style="padding: 20px; background-color: rgb(255, 255, 255); color:#555;">
								<p style="font-size: 14px;">Hello '.$name.',
									<br>
									Username : '.$borrower_username.'
									<br>You may reset your Username and Password by following the link below. Should you have any questions, please reach out to Investor Relations.
									<p style="font-size: 14px;">
										<br><a href="'.$resetLINK.'" target="_blank" style="display: inline-block; padding: 10px 12px; font-family: Source Sans Pro, Helvetica, Arial, sans-serif; font-size: 14px; color: #ffffff; text-decoration: none; border-radius: 0px;background-color: #337ab7;">Reset Your Password</a>
										<br>
									</p>
									<p style="font-size: 14px;">Thanks -
										<br>TaliMar Financial</p>
							</div>
						</div>
					</div>';
					$this->email->initialize(SMTP_SENDGRID);
					$this->email->from('noreplay@talimarfinancial.com', 'TaliMar Financial');
					$this->email->to($email_address);
					$this->email->subject('Username / Password Reset');
					$this->email->message($Message);
					$this->email->set_mailtype('html'); 
					$this->email->send(); 

					$this->session->set_flashdata('success','A reset password link has been sent to your email address.');
					redirect(base_url());
			}else{

				$this->session->set_flashdata('error','Plese enter a valid E-mail address.');
				redirect(base_url());
			}

		}else{

			$this->load->view('Auth/signin');
		}
	}

	public function forgotpasswordsecure(){
		$urisegment = $this->uri->segment(2);
		$decodelink = $this->decodeURL($urisegment);

		$fetch_id = $this->User_model->query("Select * from contact where contact_id = '".$decodelink."'");
		if($fetch_id->num_rows() > 0){
			$fetch_id = $fetch_id->result();
			$data['contact_id'] = $fetch_id[0]->contact_id;
			$this->load->view('Auth/newpassword', $data);
		}else{
			redirect(base_url().'404');
		}
	}

	public function newpassword(){

		if(isset($_POST['submit'])){

			$password = $this->input->post('password');
			$cpassword = $this->input->post('cpassword');
			$contact_id = $this->input->post('contact_id');
			$data['contact_id'] = $contact_id;

			if(($password == $cpassword) && $password != ''){

				$getupdate = $this->User_model->query("Update borrower_contact_type set borrower_password = '".base64_encode($password)."' where contact_id = '".$contact_id."'");
				$this->session->set_flashdata('success','Password updated successfully.');
				redirect(base_url());

			}else{

				$this->session->set_flashdata('error','Both password are not match.');
				$this->load->view('Auth/newpassword', $data);
			}

		}else{
			redirect(base_url().'404');
		}
	}

	public function dashboard(){

		if($this->session->userdata('login_bid') == ''){
			redirect(base_url());
		}

		$borrowerID = $this->input->post('account_name');
		$data['account_name'] = $borrowerID;
		if($borrowerID !=''){
			$contactAccounts = $this->both_Borrower_and_contactdata($this->session->userdata('login_bid'),$borrowerID);
		}else{
			$contactAccounts = $this->Borrower_associate_with_contact($this->session->userdata('login_bid'));
		}

		$data['contactAccounts'] = $contactAccounts;
		if($contactAccounts != ''){
			
			foreach ($contactAccounts as $value) {
				
				$fetchActiveloan = $this->User_model->query("SELECT COUNT(*) as totalcount, SUM(l.loan_amount) as totalAmount FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.loan_status = '2' AND l.borrower = '".$value->b_id."'");
				if($fetchActiveloan->num_rows() > 0){

					$fetchActiveloan = $fetchActiveloan->result();
					$totalcount 	= $fetchActiveloan[0]->totalcount;
					$totalAmount 	= $fetchActiveloan[0]->totalAmount;
				}else{
					$totalcount 	= 0;
					$totalAmount 	= 0;
				}

				$contactAccountsArray[] = array(
													"b_name" => $value->b_name,
													"borrower_type" => $value->borrower_type,
													"ira_account" => $value->ira_account,
													"fci_acc_text" => $value->fci_acc_text,
													"b_id" => $value->b_id,
													"b_tax_id" => $value->b_tax_id,
													"totalcount" => $totalcount,
													"totalAmount" => $totalAmount,
												);
			}

			$data['contactAccountsArray'] = $contactAccountsArray;
		}else{
			$data['contactAccountsArray'] = '';
		}
		
		$displayallaccount = $this->Borrower_associate_with_contact($this->session->userdata('login_bid'));
		$data['displayallaccount'] = $displayallaccount;
		
		$data['borrowerinfo'] = 'abc';
		$data['content'] = $this->load->view('Auth/dashboard', $data, true);
		$this->load->view('template_files/template', $data);
	}


	public function signout(){

		$this->session->sess_destroy();
		redirect(base_url());
	}

	public function myprofile(){

		$fetchContactData = $this->fetchContactname($this->session->userdata('login_bid'));

		$data['contactinfo'] = $fetchContactData;
		$data['content'] = $this->load->view('Auth/profile', $data, true);
		$this->load->view('template_files/template', $data);
	}


	public function uploadImage(){

		if(isset($_POST['upload'])){

			$imgFile = $_FILES['file']['name'];

	        if($imgFile !=''){

    	        $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';

                $this->load->library('upload', $config);
                if( ! $this->upload->do_upload('file'))
                {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect(base_url().'profile');

                }
                else
                {
                    $imgdata = $this->upload->data();
                    $profile_img = base_url().'uploads/'.$imgdata['file_name'];

                    $where['contact_id'] = $this->session->userdata('login_bid');
                    $bdata['imgurl'] = $profile_img;

    	            $this->User_model->updatedata('borrower_contact_type',$where,$bdata);
    	            $this->session->set_flashdata('success', 'Profile updated successfully');
    	            redirect(base_url().'profile');
                }
            }
		}
	}

	public function changePassword()
	{
		if($this->session->userdata('login_bid') == ''){
			redirect(base_url());
		}

		$borrowerID = $this->input->post('account_name');

		$data['account_name'] = $borrowerID;
		if($borrowerID !=''){
			$contactAccounts = $this->both_Borrower_and_contactdata($this->session->userdata('login_bid'),$borrowerID);
		}else{
			$contactAccounts = $this->Borrower_associate_with_contact($this->session->userdata('login_bid'));
		}

		$displayallaccount = $this->Borrower_associate_with_contact($this->session->userdata('login_bid'));
		$data['displayallaccount'] = $displayallaccount;
		
		$data['borrowerinfo'] = 'abc';
		$data['content'] = $this->load->view('Auth/changePassword', $data, true);
		$this->load->view('template_files/template', $data);
	}

	public function changePasswordUpdate()
 	{
 		$this->load->library('form_validation');

		$this->form_validation->set_rules('oldPassword', 'old password', 'required');
		$this->form_validation->set_rules('newPassword', 'new password', 'required|min_length[6]');
		$this->form_validation->set_rules('confirmPassword', 'confirmPassword Confirmation', 'required|matches[newPassword]');

        if ($this->form_validation->run() == FALSE)
        {
            echo validation_errors();
        }
        else
        {

            $param = array(
				'borrower_password'=>base64_encode($this->input->post('newPassword'))
            );

            $where = array(
				'contact_id'=>$this->session->userdata('login_bid'),
				'borrower_password'=>base64_encode($this->input->post('oldPassword'))
            );

           
            $user = $this->User_model->updatedata1('borrower_contact_type', $where, $param);

            if($user)
            {
            	$array = array(
            		"status" => 1,
            		"message" => '<div class="alert alert-success alert-dismissible">
						  <a href="#" class="close" data-dismiss="alert" aria-label="close" style="text-indent: unset;">&times;</a>
						  <strong>Success!</strong> Password change successfully.
						</div>'  
            	);
            }
            else
            {
            	$array = array(
            		"status" => $user,
            		"message" => '<div class="alert alert-danger alert-dismissible">
						  <a href="#" class="close" data-dismiss="alert" aria-label="close" style="text-indent: unset;">&times;</a>
						  <strong>Danger!</strong> Invalid old password.
						</div>'  
            	);
            } 

            echo json_encode($array);           
        }    
 	}


}

?>
