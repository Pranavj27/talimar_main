<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Active_loan extends MY_Controller {

	public function __construct() {

		parent::__construct();
		$this->load->model('User_model');
		$this->load->library('Aws3','aws3');

		if($this->session->userdata('login_bid') == ''){
			redirect(base_url());
		}
		
	}


	public function activeLoan()
	{

		$borrowerID = $this->input->post('account_name') ? $this->input->post('account_name') : $this->uri->segment(2);
		$data['account_name'] = $borrowerID;
		if($borrowerID !=''){
			$contactAccounts = $this->both_Borrower_and_contactdata($this->session->userdata('login_bid'),$borrowerID);
		}else{
			$contactAccounts = $this->Borrower_associate_with_contact($this->session->userdata('login_bid'));
		}

		if($contactAccounts != ''){
			
			foreach ($contactAccounts as $value) {
				
				$fetchActiveloan = $this->User_model->query("SELECT * FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan JOIN loan_property as lp on l.talimar_loan = lp.talimar_loan WHERE ls.loan_status = '2' AND l.borrower = '".$value->b_id."'");
				if($fetchActiveloan->num_rows() > 0){

					$fetchActiveloan = $fetchActiveloan->result();

						foreach ($fetchActiveloan as $row) {
				
					
						$monthlypayment = $this->get_monthly_payment_for_loan($row->talimar_loan);
				

						$activeAccountinfo[] = array(
													"b_name" => $value->b_name,
													"talimar_loan" => $row->talimar_loan,
													"borrower_type" => $value->borrower_type,
													"fci_acc_text" => $value->fci_acc_text,
													"b_id" => $value->b_id,
													"b_tax_id" => $value->b_tax_id,
													"location" => $row->property_address.'; '.$row->city.', '.$row->state.' '.$row->zip,
													"position" => $row->position,
													"loan_amount" => $row->loan_amount,
													"current_balance" => $row->current_balance,
													"intrest_rate" => $row->intrest_rate,
													"loan_status" => $row->loan_status,
													"loan_payment_status" => $row->loan_payment_status,
													"monthlypayment" => $monthlypayment,

													);

						
					}

					$data['activeAccountinfo'] = $activeAccountinfo;
				}
			}

			

		}else{
			$data['activeAccountinfo'] = '';
		}


		$displayallaccount = $this->Borrower_associate_with_contact($this->session->userdata('login_bid'));
		$data['displayallaccount'] = $displayallaccount;
		
		$data['activeLoan'] = 'abc';
		$data['content'] = $this->load->view('Active_loans/activeLoan', $data, true);
		$this->load->view('template_files/template', $data);
	}


	public function loanDetails(){

		$uri2 = $this->uri->segment(2);

		$fetchdetails = $this->User_model->query("SELECT * FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan JOIN loan_property as lp ON l.talimar_loan = lp.talimar_loan WHERE l.talimar_loan = '".$uri2."'");
		if($fetchdetails->num_rows() > 0){
			$fetchdetails = $fetchdetails->result();
			$data['Alldetails'] = $fetchdetails;
		}

		$data['loanDetails'] = 'abc';
		$data['content'] = $this->load->view('Active_loans/loan-details', $data, true);
		$this->load->view('template_files/template', $data);
	}

}
?>