<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class POF extends MY_Controller {

	public function __construct() {

		parent::__construct();
		$this->load->model('User_model');
		$this->load->library('email');
		$this->load->library('Aws3','aws3');
		if($this->session->userdata('login_bid') == ''){
			redirect(base_url());
		}
	}


	public function index()
	{
		
	}

	public function AllborrowerAcc(){

		$borrower_data = $this->User_model->query("SELECT * FROM borrower_data");
		$borrower_data = $borrower_data->result();
		foreach ($borrower_data as $value) {
			
			$borrower_data[$value->id] = $value->b_name;
		}

		return $borrower_data;
	}

	public function pof_request()
	{

		$displayallaccount = $this->Borrower_associate_with_contact($this->session->userdata('login_bid'));
		$data['displayallaccount'] = $displayallaccount;

		$data['ActivePropertyname'] = $this->ActivePropertyname();
		$AllborrowerAcc 			= $this->AllborrowerAcc();

		if(isset($_POST['send'])){

			$emailto = 'info@talimarfinancial.com';
			//$emailto = 'pankaj.wartiz@gmail.com';
			$Message = '<div style="background-color: #eeeeef; padding: 50px 0; "><div style="max-width:640px; margin:0 auto; "><div style="color: #fff; text-align: center; background-color:#337ab7; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;"><h1>POF Request</h1></div><div style="padding: 20px; background-color: rgb(255, 255, 255); color:#555;"><p style="font-size: 14px;">New POF request send by the '.$this->session->userdata('login_bname').'. Please check the below details:
				<p style="font-size: 14px;">
					<b>Borrower Account:</b> '.$AllborrowerAcc[$this->input->post('b_account')].'<br>
					<b>Street Address:</b> '.$this->input->post('address').'<br>
					<b>Unit #:</b> '.$this->input->post('unit').'<br>
					<b>City:</b> '.$this->input->post('city').'<br>
					<b>State:</b> '.$this->input->post('state').'<br>
					<b>Zip:</b> '.$this->input->post('zip').'<br>
					<b>Purchase Price:</b> '.$this->input->post('pur_price').'<br>
					<b>Rehab Cost:</b> '.$this->input->post('reb_cost').'<br>
					<b>Completion Value:</b> '.$this->input->post('com_value').'<br>
				</p>
				<p style="font-size: 14px;">Thanks - <br>TaliMar Financial</p></div></div></div>';
				$this->email->initialize(SMTP_SENDGRID);
				$this->email->from('noreplay@talimarfinancial.com', 'TaliMar Financial');
				$this->email->to($emailto);
				$this->email->subject('POF Request by '.$this->session->userdata('login_bname'));
				$this->email->message($Message);
				$this->email->set_mailtype('html'); 
				$this->email->send(); 

				$this->session->set_flashdata('success','Request Submitted');
				redirect(base_url().'pof-request');
		}
		
		$data['pof_request'] = 'abc';
		$data['content'] = $this->load->view('pof/send-pof', $data, true);
		$this->load->view('template_files/template', $data);
	}

	public function ActivePropertyname(){

		$fetchProperty = $this->User_model->query("SELECT lp.property_address, lp.id FROM loan_property as lp JOIN loan_servicing as ls ON ls.talimar_loan = lp.talimar_loan WHERE ls.loan_status = '2'");

		$fetchProperty = $fetchProperty->result();
		foreach ($fetchProperty as $value) {
			
			$Allactivelist[$value->id] = $value->property_address;
		}

		return $Allactivelist;
	}


	public function send_req(){

		if(isset($_POST['submit'])){

			$ActivePropertyname = $this->ActivePropertyname();

			$emailto = 'payoff@talimarfinancial.com';
			//$emailto = 'pankaj.wartiz@gmail.com';
			$Message = '<div style="background-color: #eeeeef; padding: 50px 0; "><div style="max-width:640px; margin:0 auto; "><div style="color: #fff; text-align: center; background-color:#337ab7; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;"><h1>Payoff Request</h1></div><div style="padding: 20px; background-color: rgb(255, 255, 255); color:#555;"><p style="font-size: 14px;">The Borrower on '.$ActivePropertyname[$this->input->post('property')].' has requested a payoff demand. The expected payoff date is '.$this->input->post('exp_date').'.
				<p style="font-size: 14px;"></p>
				<p style="font-size: 14px;">Thanks - <br>TaliMar Financial</p></div></div></div>';
				$this->email->initialize(SMTP_SENDGRID);
				$this->email->from('noreplay@talimarfinancial.com', 'TaliMar Financial');
				$this->email->to($emailto);
				$this->email->subject('Payoff Request – '.$ActivePropertyname[$this->input->post('property')]);
				$this->email->message($Message);
				$this->email->set_mailtype('html'); 
				$this->email->send(); 

				$this->session->set_flashdata('success','Request Submitted');
				redirect(base_url().'pof-request');
		}

	}

}
?>