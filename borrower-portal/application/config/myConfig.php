<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

$config['debit_account_type'] = array(

	'' => 'Select One',
	1 => 'Checking',
	2 => 'Savings',
);

$config['cash_flow_option'] = array(

	1 => 'No',
	2 => 'Yes',
);

$config['fci_doc_options'] = array(
	'' => 'Select One',
	1 => 'Promissory Note',
	2 => 'Deed of Trust',
	3 => 'Assignment',
	4 => 'Personal Guaranty',
	5 => 'Est Closing Statement',
	6 => 'Final Closing Statement',
	7 => 'Construction Agreement',
	8 => 'Other',
);

$config['setup_package_option'] = array(
	1 => 'Not Sent',
	2 => 'Sent',
	3 => 'Completed',
);

$config['contact_record_limit'] = array(
	'0, 2000' => '0-2000',
	'2000, 5000' => '2000-5000',
	'5000, 10000' => '5000-10000',
	'10000, 15000' => '10000-15000',
	
);

$config['record_limit_option'] = array(
	10  => '10',
	50  => '50',
	100  => '100',
	500  => '500',
	1000  => '1000',
	1500  => '1500',
	2000  => '2000',
	2500  => '2500',
);

$config['Ass_status_paidto'] = array(

	'' => 'Select One',
	1  => 'Title',
	2  => 'TaliMar',
	3  => 'Other',
);

$config['lender_app_status'] = array(

	'' => 'Select One',
	1  => 'Under Review',
	2  => 'Requested',
	3  => 'Approved',
);

$config['lender_app_request'] = array(

	'' => 'Select One',
	1  => 'Extension',
	2  => 'Notice of Default',
	3  => 'Foreclosure Sale',
	4  => 'Other',
);

$config['Ass_Recording_Status'] = array(

	1  => 'No',
	2  => 'Submitted',
	3  => 'Recorded',
	4  => 'Received',
);

$config['Broker_Authorization_option'] = array(

	1  => 'Loan Payoff',
	2  => 'Loan Reinstatement',
	3  => 'Change Fees / Terms',
	4  => 'Start Foreclosure',
);

$config['Astatus_paidto'] = array(

	'' => 'Select One',
	1  => 'Title',
	2  => 'TaliMar',
	3  => 'Other',
);

$config['loan_cross_collateralized'] = array(

	1 => 'No',
	2 => 'Yes',
);

$config['link_to_lender'] = array(

	1 => 'No',
	2 => 'Yes',
);

$config['account_status_option'] = array(

	'' => 'Select One',
	1 => 'Processing',
	2 => 'Active',
	3 => 'Frozen',
	4 => 'Closed',
);

$config['lender_portal_access'] = array(

	0 => 'No',
	1 => 'Yes',
);

$config['filter_loan_status'] = array(

	'' => 'Select All',
	1 => 'Pipeline',
	2 => 'Active',
	3 => 'Paid Off',
);

$config['property_newval_options'] = array(

	'' => 'Select One',
	1 => 'Full Appraisal',
	2 => 'DT Appraisal',
	3 => '3rd Party BPO',
	4 => 'TaliMar BPO',
	5 => 'Not Applicable',
);

$config['contact_marketing_data'] = array(

	1 => 'Wholesale Deals',
	2 => 'Referral Partner',
	3 => 'Direct Prospect',
	4 => 'Sphere Partner',

);

$config['loan_source_option'] = array(

	1 => 'Direct',
	2 => 'Referral',
	3 => 'Brokered',
);

$config['marketing_before_after_option'] = array(

	1 => 'Not Completed',
	2 => 'Submitted',
	3 => 'Available',
	4 => 'Posted',

);

$config['ext_PaymentType'] = array(

	'' => 'Select One',
	1 => 'Deferred',
	2 => 'Paid Current',

);

$config['loan_reserve_status_option'] = array(

	6 => 'Not Submitted',
	//1 => 'Requested',
	//2 => 'Approved',
	3 => 'Submitted',
	4 => 'Released',
	5 => 'Closed',

);

$config['before_after_option_new'] = array(
	'' => 'Select One',
	5 => 'Not Started',
	1 => 'Images Submitted',
	2 => 'Video Received',
	3 => 'Video Posted',
	4 => 'Social Media Complete',

);

$config['project_status'] = array(

	2 => 'Processing',
	3 => 'Active',
	1 => 'Completed',

);

$config['selct_contact_task'] = array(

	0 => 'Select One',
	1 => 'Tickler Call',
	2 => 'Schedule Meet',
	3 => 'Schedule Call',
	4 => 'FU on Loan',
	5 => 'Other',

);

$config['loan_reserve_draw_status'] = array(

	0 => 'Select One',
	1 => 'Requested',
	2 => 'Approved',
	3 => 'Submitted',
	4 => 'Released',

);

$config['serviceing_condition_option'] = array(
	0 => '',
	2 => 'Current',
	//3 => 'Current – 10+ Days Late',
	//4 =>'Current (20 Days Late)',
	//5 =>'Current (25 Days Late)',
	1 => 'Default',
);

$config['loan_payment_status'] = array(
	''=> '',
	1 => 'Current',
	2 => '10+ Days Late',
	3 => ' 30+ Days Late',
	
);

$config['cashflow_edit_options'] = array(
	0 => 'Select One',
	1 => 'Apt / Condo,',
	2 => 'SFR Detached',
	3 => 'Retail',
	4 => 'Commercial',
	5 => 'Hotal',
	
);


$config['term_sheet'] = array(

	0 => 'Not Submitted',
	1 => 'Submitted',
	//2 => 'Signed',

);

$config['closing_term_sheet'] = array(

	2 => 'No',
	1 => 'Yes',

);
$config['propp_type'] = array(
	'' => 'Select one',
	8 => 'Condominium',
	1 => 'Single Family Home',
	2 => '2-4 Units',
	3 => 'Multi Family',
	4 => 'Land',
	5 => 'Commercial',
	6 => 'Retail',
	7 => 'Office',

);

$config['template_yes_no_option'] = array(
	2 => 'Not Complete',
	1 => 'Complete',

	//'' => 'N/A',

);

// $config['marketing_before_after_option']  = array(

// 											1 => 'No',
// 											2 => 'Yes',
// 											3 => 'Completed',

// 										);

$config['phone_types'] = array(
	'' => 'Select one',
	1 => 'Office',
	2 => 'Mobile',
	3 => 'Home',
	4 => 'Fax',

);

$config['email_types_option'] = array(

	'' => '',
	1 => 'Work',
	2 => 'Home',
	3 => 'Other',

);

$config['com_type'] = array(

	1 => 'Call',
	2 => 'E-Mail',
	3 => 'Letter',
	5 => 'Meeting',
	6 => 'Site Visit',
	4 => 'Other',

);

$config['market_trust_deed_new_option'] = array(

	1 => 'No',
	2 => 'Available',
	3 => 'Coming Soon',
	4 => 'Fully Subscribed',

);

$config['assi_security_type'] = array(
	0 => 'Select One',
	1 => 'Deed of Trust',
	2 => 'Assignment',
	3 => 'Other',
);

$config['assi_disclouser_status'] = array(
	0 => 'Not Ready',
	1 => 'Available',
	2 => 'Submitted',
	3 => 'Signed',
);

$config['assi_servicer_status'] = array(

	0 => 'Not Submitted',
	1 => 'Submitted',
	2 => 'Confirmed',

);

$config['assi_fund_received'] = array(

	0 => 'Pending',
	1 => 'Requested',
	2 => 'Sent',
	3 => 'Received',

);

$config['assi_file_status'] = array(

	0 => 'Open',
	1 => 'Closed',

);

//..................report config start.....................//

$config['assii_security_type'] = array(
	0 => 'Select All',
	1 => 'Deed of Trust',
	2 => 'Assignment',
	3 => 'Other',
);

$config['assii_disclouser_status'] = array(
	'' => 'Select All',
	//0 => 'Not Ready',
	//1 => 'Ready',
	2 => 'Submitted',
	3 => 'Signed',
);

$config['new_assii_disclouser_status'] = array(
	'' => 'Not Ready',
	1 => 'Ready',
	2 => 'Submitted',
	3 => 'Signed',
);

// $config['assii_servicer_status']  = array(
// 											''=> 'Select All',
// 											0 => 'Not Submitted',
// 											1 => 'Submitted',
// 											//2 => 'Confirmed',

// 										);

// $config['assii_fund_received']  = array(
// 											''=> 'Select All',
// 											1 => 'Yes',
// 											0 => 'No',

// 										);

// $config['assii_file_status']  = array(
// 											''=> 'Select All',
// 											0 => 'Open',
// 											1 => 'Closed',

// 									);
//..................report config end .....................//
$config['loan_source'] = array(
	'' => 'Select one',
	1 => 'Direct',
	2 => 'Broker',
	3 => 'Referral',

);

$config['yes_no_option_verified'] = array(

	1 => 'Yes',
	2 => 'No',

);

$config['pay_off_processing_one'] = array(

	1 => 'No',
	2 => 'Yes',
);

$config['primary_contact_option'] = array(

	1 => 'Yes',
	2 => 'No',
);

$config['extension_status'] = array(
	'' => 'Select one',
	1 => 'Processing',
	2 => 'Complete',

);

$config['fractional'] = array(
	'' => 'Select one',
	1 => 'Fraction',
	3 => 'Unknown',
	2 => 'Whole',

);

$config['date_option'] = array(

	1 => 'Today',
	2 => 'Week',
	3 => 'Month',
	4 => 'Quarter',
	5 => 'Year to Date',

);

$config['funded_loans'] = array(

	1 => 'At Least 1',
	2 => '1 – 5',
	3 => '5+',
);

$config['list_month'] = array(

	'' => 'Select one',
	1 => 'January',
	2 => 'February',
	3 => 'March',
	4 => 'April',
	5 => 'May',
	6 => 'June',
	7 => 'July',
	8 => 'August',
	9 => 'September',
	10 => 'October',
	11 => 'November',
	12 => 'December',
);

$config['list_year'] = array(

	'' => 'Select one',
	2015 => '2015',
	2016 => '2016',
	2017 => '2017',
	2018 => '2018',
	2019 => '2019',
	2020 => '2020',

);

$config['lender_property_type'] = array(

	'1' => 'Single Family',
	'2' => 'Multi-Family (2+ units)',
	'3' => 'Commercial',
	'4' => 'Land',
	'5' => 'Retail',

);

$config['lender_lend_value'] = array(

	'1' => 'Yes',
	'2' => 'No',

);

$config['payoff_previous_days'] = array(

	'' => 'Select One',
	'7' => '7 Days',
	'15' => '15 Days',
	'30' => '30 Days',
	'60' => '60 Days',
	'90' => '90 Days',
	'180' => '180 Days',
);

$config['valuation_com'] = array(
	'' => 'Select One',
	1 => 'TaliMar Financial',
	2 => 'Other',

);

$config['loan_tye'] = array(
	1 => 'Bridge Loan',
	2 => 'Construction (New)',
	3 => 'Construction (Existing)',

);
$config['ltv_option'] = array(
	1 => '0% - 50%',
	2 => '51% - 65%',
	3 => '66% - 70%',
	4 => '71% - 75%',
	5 => '76%+',

);

$config['position_optionn'] = array(

	1 => '1st',
	2 => '2nd',
	3 => '3rd+',

);

$config['yeild_percent'] = array(

	1 => '7% - 7.99%',
	2 => '8% - 8.99%',
	4 => '9% - 9.99%',
	6 => '10%+',

);

$config['borrower_option'] = array(
	1 => 'one',
	2 => 'two',
	3 => 'three',
	4 => 'four',

);


$config['project_role_option'] = array(
	'' => 'Select One',
	1 => 'Primary',
	2 => 'Secondary',
	

);

$config['garge_option'] = array(
	0 => 'Zero',
	1 => 'one',
	2 => 'two',
	3 => 'three',
	4 => 'four',
	5 => 'five',

);

$config['property_typee'] = array('' => 'Select One',
	1 => 'Single Family Home',
	5 => 'Condominium',
	6 => 'Duplex',
	7 => 'Triplex',
	12 => '4-Plex',
	2 => 'Multi-Family',
	3 => 'Commercial',
	4 => 'Land',
	8 => 'Manufactured Home',
	9 => 'Retail',
	10 => 'Mixed-Use',
	11 => 'Agriculture',

);

$config['loan_typee'] = array('' => 'Select One',
	1 => 'Fix and Flip Loan',
	2 => 'Construction Loan',
	// 3 => 'Commercial Building',
	4 => 'Bridge Loan',

);

$config['new_servicer_option'] = array(

	15 => 'TaliMar Financial',
	14 => 'FCI Lender Services',
	16 => 'La Mesa Fund Control',
	19 => 'Del Toro Loan Servicing',
	20 => 'Other',

);

$config['newsub_servicer_option'] = array(

	14 => 'FCI Lender Services',
	15 => 'TaliMar Financial',
	16 => 'La Mesa Fund Control',
	19 => 'Del Toro Loan Servicing',
	20 => 'Other',

);

$config['payment_remind'] = array(

	2 => 'No',
	1 => 'Yes',

);

$config['loan_doc_type'] = array(

	1 => 'Promissory Note',
	2 => 'Deed of Trust',
	3 => 'Final Closing Statement',
	4 => 'Assignment 1',
	5 => 'Preliminary Title Report',
	6 => 'Property Report',
	0 => 'Add New',
);

$config['yes_no_not_applicable_option'] = array(
	'' => 'Select One',
	3 => 'No',
	1 => 'Yes',
	2 => 'Not Applicable',
);

$config['yes_no_option'] = array(
	'' => 'Select One',
	0 => 'No',
	1 => 'Yes',
);

$config['portal_access_option'] = array(
	
	1 => 'Lender Portal',
	2 => 'Borrower Portal',
	3 => 'Broker Portal',
);

$config['l_s_c'] = array(
	0 => 'No',
	1 => 'Yes',
);
$config['yes_no_option_67'] = array(
	//'' => 'Select One',
	0 => 'No',
	1 => 'Yes',
);

$config['yes_no_option1'] = array(
	'' => 'Select One',
	1 => 'No',
	2 => 'Yes',
);

$config['yes_no_option2'] = array(
	'' => 'Select One',
	1 => 'Yes',
	0 => 'No',
	2 => 'Not Required',
);

$config['loan_type_option'] = array(
	'' => 'Select One',
	1 => 'Bridge',
	2 => 'Fix and Flip',
	4 => 'Fix and Hold',
	3 => 'New Construction',

);

$config['valuation_based_upon'] = array(

	'' => 'Select One',
	1 => 'Current Value',
	2 => 'Completion Value',
	3 => 'Purchase Price',
);

$config['transaction_type_option'] = array(
	'' => 'Select One',
	1 => 'Purchase',
	2 => 'Refinance',
	3 => 'Cash Out Refinance',

);

$config['payment_due_option'] = array(

	'' => 'Select One',
	1 => '1st',
	2 => '2nd',
	3 => '3rd',
	4 => '4th',
	5 => '5th',
	6 => '6th',
	7 => '7th',
	8 => '8th',
	9 => '9th',
	10 => '10th',
	11 => '11th',
	12 => '12th',
	13 => '13th',
	14 => '14th',
	15 => '15th',
	16 => '16th',
	17 => '17th',
	18 => '18th',
	19 => '19th',
	20 => '20th',
	21 => '21st',
	22 => '22nd',
	23 => '23rd',
	24 => '24th',
	25 => '25th',
	26 => '26th',
	27 => '27th',
	28 => '28th',
	29 => '29th',
	30 => '30th',
	31 => '31st',

);

$config['position_option'] = array(
	'' => 'Select All',
	1 => '1st',
	2 => '2nd',
	3 => '3rd',
	4 => '4th',

);

$config['ecum_position_option'] = array(

	'' => 'Select One',
	1 => '1st',
	2 => '2nd',
	3 => '3rd',
	4 => '4th',
	5 => 'N/A',

);

$config['marital_status'] = array(
	'' => 'Select One',
	1 => 'Single',
	2 => 'Married',
	3 => 'Divorced',
	5 => 'Widowed',
	4 => 'Other',

);

$config['loan_payment_type_option'] = array(
	1 => 'Interest Only',
	2 => 'Amortization',
	3 => 'Partial Amortization',
);

$config['loan_payment_schedule_option'] = array(
	'30' => 'Monthly',
	'7' => 'Weekly',
	'90' => 'Quaterly',
	'365' => 'Yearly',
	'paid' => 'Paid off',
);

$config['loan_intrest_type_option'] = array(

	1 => 'Fixed',
	2 => 'Adjustable',
	3 => '3  Yr Fixed',
	4 => '5  Yr Fixed',
	5 => '7  Yr Fixed',
	6 => '10 Yr Fixed',
);

$config['contact_marketing_status_option'] = array(

	0 => 'Select One',
	1 => 'Requested',
	2 => 'Completed',
);

$config['contact_purpose_menu'] = array(

	'' => 'Select One',
	1 => 'Loan Program',
	2 => 'TD Information',
	3 => 'POF',
	4 => 'General',
	//5 => 'TD Program',
	6 => 'Term Sheet',
	7 => 'Swag',
	8 => 'Loan Input Form',
	9 => 'Soft Quote',
	10 => 'Loan Request',
	11 => 'Tickler Message',
	12 => 'Networking',
	13 => 'PB Application',
	14 => 'PB Approved',
);

$config['STATE_USA'] = array(
	"none" => "",
	"AL" => "Alabama",
	"AK" => "Alaska",
	"AZ" => "Arizona",
	"AR" => "Arkansas",
	"CA" => "California",
	"CO" => "Colorado",
	"CT" => "Connecticut",
	"DE" => "Delaware",
	"FL" => "Florida",
	"GA" => "Georgia",
	"HI" => "Hawaii",
	"ID" => "Idaho",
	"IL" => "Illinois",
	"IN" => "Indiana",
	"IA" => "Iowa",
	"KS" => "Kansas",
	"KY" => "Kentucky",
	"LA" => "Louisiana",
	"ME" => "Maine",
	"MD" => "Maryland",
	"MA" => "Massachusetts",
	"MI" => "Michigan",
	"MN" => "Minnesota",
	"MS" => "Mississippi",
	"MO" => "Missouri",
	"MT" => "Montana",
	"NE" => "Nebraska",
	"NV" => "Nevada",
	"NH" => "New Hampshire",
	"NJ" => "New Jersey",
	"MS" => "New Mexico",
	"NM" => "New York",
	"NC" => "North Carolina",
	"ND" => "North Dakota",
	"OH" => "Ohio",
	"OK" => "Oklahoma",
	"OR" => "Oregon",
	"PA" => "Pennsylvania",
	"RI" => "Rhode Island",
	"SC" => "South Carolina",
	"SD" => "South Dakota",
	"TN" => "Tennessee",
	"TX" => "Texas",
	"UT" => "Utah",
	"VT" => "Vermont",
	"VA" => "Virginia",
	"WA" => "Washington",
	"WV" => "West Virginia",
	"WI" => "Wisconsin",
	"WY" => "Wyoming ",

);

// -----        -----  Loan Data property modal -----   ------    ----

$config['property_modal_property_type'] = array(
	0 => 'Select One',
	1 => 'Residential (1-4 units)',
	2 => 'Residential (4+ units)',
	3 => 'Commercial',
	4 => 'Industrial',
	5 => 'Land',
	6 => 'Retail',
	7 => 'Church',
	8 => 'Gas Station',
	9 => 'Mobile Home',
);

$config['property_modal_property_condition'] = array(
	0 => 'Select One',
	1 => 'Poor',
	2 => 'Moderate',
	3 => 'Good',
	4 => 'Excellent',
	5 => 'New',
	6 => 'Not Applicable',

);

$config['property_modal_valuation_type'] = array(
	1 => 'Appraiser',
	2 => 'Broker Price Opinion',

);

$config['valuation_type'] = array(
	0 => 'Select One',
	1 => 'Appraisal',
	2 => 'Broker Price Opinion',
	3 => 'None',
	4 => 'Internal Valuation',

);
$config['relationship_type'] = array(
	0 => 'Select One',
	1 => 'Employee',
	2 => 'Agent',
	3 => 'Independent Contractor',

);

$config['all_countary'] = array(

	"AF" => 'Afghanistan',
	"AX" => 'Åland Islands',
	"AL" => 'Albania',
	"DZ" => 'Algeria',
	"AS" => 'American Samoa',
	"AD" => 'Andorra',
	"AO" => 'Angola',
	"AI" => 'Anguilla',
	"AQ" => 'Antarctica',
	"AG" => 'Antigua and Barbuda',
	"AR" => 'Argentina',
	"AM" => 'Armenia',
	"AW" => 'Aruba',
	"AU" => 'Australia',
	"AT" => 'Austria',
	"AZ" => 'Azerbaijan',
	"BS" => 'Bahamas',
	"BH" => 'Bahrain',
	"BD" => 'Bangladesh',
	"BB" => 'Barbados',
	"BY" => 'Belarus',
	"BE" => 'Belgium',
	"BZ" => 'Belize',
	"BJ" => 'Benin',
	"BM" => 'Bermuda',
	"BT" => 'Bhutan',
	"BO" => 'Bolivia, Plurinational State of',
	"BQ" => 'Bonaire, Sint Eustatius and Saba',
	"BA" => 'Bosnia and Herzegovina',
	"BW" => 'Botswana',
	"BV" => 'Bouvet Island',
	"BR" => 'Brazil',
	"IO" => 'British Indian Ocean Territory',
	"BN" => 'Brunei Darussalam',
	"BG" => 'Bulgaria',
	"BF" => 'Burkina Faso',
	"BI" => 'Burundi',
	"KH" => 'Cambodia',
	"CM" => 'Cameroon',
	"CA" => 'Canada',
	"CV" => 'Cape Verde',
	"KY" => 'Cayman Islands',
	"CF" => 'Central African Republic',
	"TD" => 'Chad',
	"CL" => 'Chile',
	"CN" => 'China',
	"CX" => 'Christmas Island',
	"CC" => 'Cocos (Keeling) Islands',
	"CO" => 'Colombia',
	"KM" => 'Comoros',
	"CG" => 'Congo',
	"CD" => 'Congo, the Democratic Republic of the',
	"CK" => 'Cook Islands',
	"CR" => 'Costa Rica',
	"CI" => 'Côte dIvoire',
	"HR" => 'Croatia',
	"CU" => 'Cuba',
	"CW" => 'Curaçao',
	"CY" => 'Cyprus',
	"CZ" => 'Czech Republic',
	"DK" => 'Denmark',
	"DJ" => 'Djibouti',
	"DM" => 'Dominica',
	"DO" => 'Dominican Republic',
	"EC" => 'Ecuador',
	"EG" => 'Egypt',
	"SV" => 'El Salvador',
	"GQ" => 'Equatorial Guinea',
	"ER" => 'Eritrea',
	"EE" => 'Estonia',
	"ET" => 'Ethiopia',
	"FK" => 'Falkland Islands (Malvinas)',
	"FO" => 'Faroe Islands',
	"FJ" => 'Fiji',
	"FI" => 'Finland',
	"FR" => 'France',
	"GF" => 'French Guiana',
	"PF" => 'French Polynesia',
	"TF" => 'French Southern Territories',
	"GA" => 'Gabon',
	"GM" => 'Gambia',
	"GE" => 'Georgia',
	"DE" => 'Germany',
	"GH" => 'Ghana',
	"GI" => 'Gibraltar',
	"GR" => 'Greece',
	"GL" => 'Greenland',
	"GD" => 'Grenada',
	"GP" => 'Guadeloupe',
	"GU" => 'Guam',
	"GT" => 'Guatemala',
	"GG" => 'Guernsey',
	"GN" => 'Guinea',
	"GW" => 'Guinea-Bissau',
	"GY" => 'Guyana',
	"HT" => 'Haiti',
	"HM" => 'Heard Island and McDonald Islands',
	"VA" => 'Holy See (Vatican City State)',
	"HN" => 'Honduras',
	"HK" => 'Hong Kong',
	"HU" => 'Hungary',
	"IS" => 'Iceland',
	"IN" => 'India',
	"ID" => 'Indonesia',
	"IR" => 'Iran, Islamic Republic of',
	"IQ" => 'Iraq',
	"IE" => 'Ireland',
	"IM" => 'Isle of Man',
	"IL" => 'Israel',
	"IT" => 'Italy',
	"JM" => 'Jamaica',
	"JP" => 'Japan',
	"JE" => 'Jersey',
	"JO" => 'Jordan',
	"KZ" => 'Kazakhstan',
	"KE" => 'Kenya',
	"KI" => 'Kiribati',
	"KP" => 'Korea, Democratic Peoples Republic of',
	"KR" => 'Korea, Republic of',
	"KW" => 'Kuwait',
	"KG" => 'Kyrgyzstan',
	"LA" => 'Lao Peoples Democratic Republic',
	"LV" => 'Latvia',
	"LB" => 'Lebanon',
	"LS" => 'Lesotho',
	"LR" => 'Liberia',
	"LY" => 'Libya',
	"LI" => 'Liechtenstein',
	"LT" => 'Lithuania',
	"LU" => 'Luxembourg',
	"MO" => 'Macao',
	"MK" => 'Macedonia, the former Yugoslav Republic of',
	"MG" => 'Madagascar',
	"MW" => 'Malawi',
	"MY" => 'Malaysia',
	"MV" => 'Maldives',
	"ML" => 'Mali',
	"MT" => 'Malta',
	"MH" => 'Marshall Islands',
	"MQ" => 'Martinique',
	"MR" => 'Mauritania',
	"MU" => 'Mauritius',
	"YT" => 'Mayotte',
	"MX" => 'Mexico',
	"FM" => 'Micronesia, Federated States of',
	"MD" => 'Moldova, Republic of',
	"MC" => 'Monaco',
	"MN" => 'Mongolia',
	"ME" => 'Montenegro',
	"MS" => 'Montserrat',
	"MA" => 'Morocco',
	"MZ" => 'Mozambique',
	"MM" => 'Myanmar',
	"NA" => 'Namibia',
	"NR" => 'Nauru',
	"NP" => 'Nepal',
	"NL" => 'Netherlands',
	"NC" => 'New Caledonia',
	"NZ" => 'New Zealand',
	"NI" => 'Nicaragua',
	"NE" => 'Niger',
	"NG" => 'Nigeria',
	"NU" => 'Niue',
	"NF" => 'Norfolk Island',
	"MP" => 'Northern Mariana Islands',
	"NO" => 'Norway',
	"OM" => 'Oman',
	"PK" => 'Pakistan',
	"PW" => 'Palau',
	"PS" => 'Palestinian Territory, Occupied',
	"PA" => 'Panama',
	"PG" => 'Papua New Guinea',
	"PY" => 'Paraguay',
	"PE" => 'Peru',
	"PH" => 'Philippines',
	"PN" => 'Pitcairn',
	"PL" => 'Poland',
	"PT" => 'Portugal',
	"PR" => 'Puerto Rico',
	"QA" => 'Qatar',
	"RE" => 'Réunion',
	"RO" => 'Romania',
	"RU" => 'Russian Federation',
	"RW" => 'Rwanda',
	"BL" => 'Saint Barthélemy',
	"SH" => 'Saint Helena, Ascension and Tristan da Cunha',
	"KN" => 'Saint Kitts and Nevis',
	"LC" => 'Saint Lucia',
	"MF" => 'Saint Martin (French part)',
	"PM" => 'Saint Pierre and Miquelon',
	"VC" => 'Saint Vincent and the Grenadines',
	"WS" => 'Samoa',
	"SM" => 'San Marino',
	"ST" => 'Sao Tome and Principe',
	"SA" => 'Saudi Arabia',
	"SN" => 'Senegal',
	"RS" => 'Serbia',
	"SC" => 'Seychelles',
	"SL" => 'Sierra Leone',
	"SG" => 'Singapore',
	"SX" => 'Sint Maarten (Dutch part)',
	"SK" => 'Slovakia',
	"SI" => 'Slovenia',
	"SB" => 'Solomon Islands',
	"SO" => 'Somalia',
	"ZA" => 'South Africa',
	"GS" => 'South Georgia and the South Sandwich Islands',
	"SS" => 'South Sudan',
	"ES" => 'Spain',
	"LK" => 'Sri Lanka',
	"SD" => 'Sudan',
	"SR" => 'Suriname',
	"SJ" => 'Svalbard and Jan Mayen',
	"SZ" => 'Swaziland',
	"SE" => 'Sweden',
	"CH" => 'Switzerland',
	"SY" => 'Syrian Arab Republic',
	"TW" => 'Taiwan, Province of China',
	"TJ" => 'Tajikistan',
	"TZ" => 'Tanzania, United Republic of',
	"TH" => 'Thailand',
	"TL" => 'Timor-Leste',
	"TG" => 'Togo',
	"TK" => 'Tokelau',
	"TO" => 'Tonga',
	"TT" => 'Trinidad and Tobago',
	"TN" => 'Tunisia',
	"TR" => 'Turkey',
	"TM" => 'Turkmenistan',
	"TC" => 'Turks and Caicos Islands',
	"TV" => 'Tuvalu',
	"UG" => 'Uganda',
	"UA" => 'Ukraine',
	"AE" => 'United Arab Emirates',
	"GB" => 'United Kingdom',
	"US" => 'United States',
	"UM" => 'United States Minor Outlying Islands',
	"UY" => 'Uruguay',
	"UZ" => 'Uzbekistan',
	"VU" => 'Vanuatu',
	"VE" => 'Venezuela, Bolivarian Republic of',
	"VN" => 'Viet Nam',
	"VG" => 'Virgin Islands, British',
	"VI" => 'Virgin Islands, U.S.',
	"WF" => 'Wallis and Futuna',
	"EH" => 'Western Sahara',
	"YE" => 'Yemen',
	"ZM" => 'Zambia',
	"ZW" => 'Zimbabwe',

	// Naveda counties
	'Austin' => 'Austin',
	'Battle Mountain' => 'Battle Mountain',
	'Boulder City' => 'Boulder City',
	'Carlin' => 'Carlin',
	'Carson City' => 'Carson City',
	'Crescent Valley' => 'Crescent Valley',
	'Elko' => 'Elko',
	'Ely' => 'Ely',
	'Fallon' => 'Fallon',
	'Fernley' => 'Fernley',
	'Fish Lake Valley' => 'Fish Lake Valley',
	'Gardnerville' => 'Gardnerville',
	'Genoa' => 'Genoa',
	'Goldfield' => 'Goldfield',
	'Henderson' => 'Henderson',
	'Incline Village' => 'Incline Village',
	'Kingston' => 'Kingston',
	'Lake Tahoe - South Shore' => 'Lake Tahoe - South Shore',
	'Las Vegas' => 'Las Vegas',
	'Las Vegas, North' => 'Las Vegas, North',
	'Lovelock' => 'Lovelock',
	'Mesquite' => 'Mesquite',
	'Minden' => 'Minden',
	'Pahrump' => 'Pahrump',
	'Reno' => 'Reno',
	'Silver Peak' => 'Silver Peak',
	'Silver Springs' => 'Silver Springs',
	'Sparks' => 'Sparks',
	'Tonopah' => 'Tonopah',
	'Virginia City' => 'Virginia City',
	'Wells' => 'Wells',
	'West Wendover' => 'West Wendover',
	'Winnemucca' => 'Winnemucca',
	'Yerington' => 'Yerington',
);

$config['naveda_counties_options'] = array(
	'Austin' => 'Austin',
	'Battle Mountain' => 'Battle Mountain',
	'Boulder City' => 'Boulder City',
	'Carlin' => 'Carlin',
	'Carson City' => 'Carson City',
	'Crescent Valley' => 'Crescent Valley',
	'Elko' => 'Elko',
	'Ely' => 'Ely',
	'Fallon' => 'Fallon',
	'Fernley' => 'Fernley',
	'Fish Lake Valley' => 'Fish Lake Valley',
	'Gardnerville' => 'Gardnerville',
	'Genoa' => 'Genoa',
	'Goldfield' => 'Goldfield',
	'Henderson' => 'Henderson',
	'Incline Village' => 'Incline Village',
	'Kingston' => 'Kingston',
	'Lake Tahoe - South Shore' => 'Lake Tahoe - South Shore',
	'Las Vegas' => 'Las Vegas',
	'Las Vegas, North' => 'Las Vegas, North',
	'Lovelock' => 'Lovelock',
	'Mesquite' => 'Mesquite',
	'Minden' => 'Minden',
	'Pahrump' => 'Pahrump',
	'Reno' => 'Reno',
	'Silver Peak' => 'Silver Peak',
	'Silver Springs' => 'Silver Springs',
	'Sparks' => 'Sparks',
	'Tonopah' => 'Tonopah',
	'Virginia City' => 'Virginia City',
	'Wells' => 'Wells',
	'West Wendover' => 'West Wendover',
	'Winnemucca' => 'Winnemucca',
	'Yerington' => 'Yerington',
);

//  ------     -----   Modal Popup Title   ---     -----      -------

$config['title_lender_policy_type_option'] = array(
	1 => 'ALTA Policy',
	2 => 'CLTA policy',
);

$config['junior_senior_option'] = array(
	1 => 'Junior',
	2 => 'Senior',
);

$config['loan_purpose_option'] = array(
	1 => 'Consumer',
	2 => 'Business',
);

$config['loan_status_option'] = array(
	0 => 'Select One',
	//6 => 'Term Sheet',
	1 => 'Pipeline',
	2 => 'Active',
	3 => 'Paid off',
	4 => 'Cancelled',
	5 => 'Brokered',
	7 => 'Real Estate Owned',
);

$config['closing_status_option'] = array(
	0 => 'Select One',
	//7 => 'POF Letter',
	11 => 'Review',
	1 => 'Term Sheet',
	2 => 'Underwriting',
	9 => 'Ready for Docs',
	10 => 'Loan Docs Drafted/Saved',
	3 => 'Documents Released',
	4 => 'Documents Signed',
	//5 => 'Funds Released',
	8 => 'Closing Approved',
	6 => 'Loan Closed',

);

$config['boarding_status_option'] = array(
	'' => '',
	0 => 'Not Submitted',
	1 => 'Loan Submitted',
	2 => 'Loan Boarded',
	//3 => 'File Closed'
);

$config['serviceing_reason_option'] = array(
	0 => '',
	1 => 'Late Payment',
	2 => 'Maturity',
	3 => 'Other',
);

// $config['serviceing_condition_option']				 	= array(
// 													0 => '',
// 													1 => 'Default',
// 													2 => 'Current'
// 													);

$config['notes_type'] = array(
	0 => 'Select One',
	1 => 'Email',
	2 => 'Phone',
	3 => 'Letter',
	4 => 'Meeting',
	5 => 'Other',
	//6 => 'POF Letter',
	//7 => 'Executive Summary',
	//8 => 'Loan Request',
	//9 => 'Networking Event',
	//10 => 'Loan Input Form',
);

$config['contact_type'] = array(

	1 => 'Borrower',
	2 => 'Trust Deed Investor',
);

$config['contact_specialty'] = array(

	25 => '1031 Exchange',
	26 => 'Accountant',
	1 => 'Appraiser',
	2 => 'Attorney',
	3 => 'Agent / Broker',
	4 => 'Architect / Engineer',
	5 => 'Banker',
	27 => 'Capital Provider',
	6 => 'Contractor',
	24 => 'CPA',
	23 => 'Environmental Consultant',
	7 => 'Escrow / Title',
	21 => 'Gap Lender',
	8 => 'Hard Money Lender',
	9 => 'Insurance Agent',
	10 => 'IRA Specialist',
	11 => 'Financial Planner',
	12 => 'Loan Servicer',
	13 => 'Mortgage Broker',
	22 => 'Probate',
	14 => 'Property Inspector',
	15 => 'Property Manager',
	20 => 'Property Owner',
	18 => 'Real Estate Investor',
	28 => 'Scotsman',
	19 => 'Transaction Coordinator',
	16 => 'Trust Deed Investor',
	17 => 'Wholesaler',

);

$config['contact_specialty_trust'] = array(
	16 => 'Trust Deed Investor',

);

$config['contact_number_option'] = array(
	'' => '',
	1 => 'Mobile',
	2 => 'Home',
	3 => 'Business',
	4 => 'Fax',

);

$config['contact_email_blast'] = array(

	//10 =>'No Advertisement',
	//11 =>'No Advertisement',
	1 => 'Email – Wholesale Deal',
	2 => 'Email – Trust Deed Investor',
	8 => 'Trust Deed Investor',
	3 => 'Email – Hard Money',
	6 => 'Mail – Borrower',
	7 => 'Mail – Trust Deed Investor',
	9 => 'Text – Trust Deed Investor',
	4 => 'Mail Opt Out',
	5 => 'E-Mail Opt Out',

);

$config['contact_permission'] = array(

	2 => 'Everyone',
	1 => 'Admin Only',
	//3 => 'Only Selected user',
	//4 => 'Borrower Dept',
	//5 => 'Lender Dept',
	6 => 'Loan Servicing',
	7 => 'Brock VandenBerg',
	8 => 'Sarah Hosseini',

);

$config['serviceing_agent_option'] = array(
	0 => '',
	1 => 'TaliMar Financial',
	2 => 'FCI Lender Services',
	3 => 'Del Toro Loan Servicing',
);

$config['serviceing_sub_agent_option'] = array(

	14 => 'FCI Lender Services',
	15 => 'TaliMar Financial',
	19 => 'Del Toro Loan Servicing',

);

$config['serviceing_who_pay_servicing_option'] = array(
	'' => 'Select One',
	1 => 'Broker',
	2 => 'Lender',
);

$config['company_type_option'] = array(
	0 => '',
	1 => 'Corporation',
	2 => 'Limited Liability Company',
	3 => 'Limited Partnership',
	4 => 'Other',
	5 => 'Personal',
	6 => 'Self Directed IRA',
	7 => 'Trust',
);

/*$config['investor_type_option']					= array(
0 => '',
1 => 'Individual / Couple',
2 => 'Company / Trust',
3 => 'Self Directed IRA'
); */

$config['investor_type_option'] = array(
	0 => 'Select One',
	1 => 'Individual(s)',
	2 => 'Limited Liability Company',
	9 => 'C Corp',
	6 => 'S Corp',
	7 => 'Limited Partnership',
	3 => 'Self Directed IRA',
	4 => 'Trust',
	5 => '401k',
	8 => 'Sole Proprietorship',

);

$config['encumbrances_loan_type'] = array(
	0 => '',
	1 => 'Conventional',
	2 => 'Hard Money',
);
$config['dead_reason_option'] = array(
	0 => '',
	1 => 'Loan Cancelled',
	2 => 'Lost to Competitor',
);

$config['usa_city_county'] = array(
	0 => "",
	1 => "Alameda",
	2 => "Alpine",
	3 => "Amador",
	4 => "Butte",
	5 => "Calaveras",
	6 => "Contra Costa",
	7 => "Del Norte",
	8 => "El Dorado",
	9 => "Fresno",
	10 => "Glenn",
	11 => "Humboldt",
	12 => "Imperial",
	13 => "Inyo",
	14 => "Kern",
	15 => "King",
	16 => "Lake",
	17 => "Lassen",
	18 => "Los Angeles",
	19 => "Madera",
	20 => "Marin",
	21 => "Mariposa",
	22 => "Mandocino",
	23 => "Merced",
	24 => "Modoc",
	25 => "Mono",
	26 => "Monterey",
	27 => "Napa",
	28 => "Nevada",
	29 => "Orange",
	30 => "Placer",
	31 => "Plumas",
	32 => "Riverside",
	33 => "Sacramento",
	34 => "San Benito",
	35 => "San Bernardino",
	36 => "San Diego",
	37 => "San Francisco",
	38 => "San Joaquin",
	39 => "San Luis Obispo",
	40 => "San Mateo",
	41 => "Santa Barbara",
	42 => "Santa Clara",
	43 => "Santa Cruz",
	44 => "Shasta",
	45 => "Sierra",
	46 => "Siskiyou",
	47 => "Solano",
	48 => "Sonoma",
	49 => "Stanislaus",
	50 => "Sutter",
	51 => "Tehama",
	52 => "Trinity",
	53 => "Tulare",
	54 => "Tuolumne",
	55 => "Ventura",
	56 => "Yolo",
	57 => "Yuba",
	58 => "Maui",
);

$config['borrower_type_option'] = array(
	0 => '',
	7 => 'Corporation',
	2 => 'Limited Liability Company',
	3 => 'Limited Partnership',
	4 => 'Other',
	5 => 'Trust',
	6 => 'Self Directed IRA',
	1 => 'Individual(s)',
);

$config['broker_capacity_option'] = array(
	0 => '',
	2 => 'New Loan (RE851A)',
	1 => 'Existing Loan (RE851B)',
);

$config['entity_type_option'] = array(
	'' => 'Select One',
	1 => 'LLC',
	//2 => 'Inc.',
	2 => 'Corp',
	3 => 'Trust',
	4 => 'IRA',
	5 => '401k',
	6 => 'Other',
	7 => 'Defined Benefit Plan',
	8 => 'Sole Proprietorship',

);

$config['account_type_option'] = array(
	0 => '',
	1 => 'Checking',
	2 => 'Savings',
);

$config['search_by_option'] = array(
	'' => 'Select One',
	3 => 'Property Address',
	1 => 'TaliMar #',
	2 => 'Servicer Loan #',
	// 4 => 'Contact Name',
	// 5 => 'Borrower Name'
);

$config['responsibility'] = array(
	'' => 'Select One',
	4 => 'Sent',
	1 => 'Approved',
	2 => 'Declined',
	5 => 'Waitlist',
	3 => 'No Response',
	6 => 'Other',
);

$config['loan_affiliated_parties'] = array(
	1 => 'Affiliated Parties 1',
	//2 => 'Affiliated Parties 2',
	//3 => 'Affiliated Parties 3',
);

$config['affiliated_option'] = array(
	'' => 'Select One',
	1 => 'Buyer Agent',
	2 => 'Seller Agent',
	3 => 'Contractor',
	4 => 'Mortgage Broker',
	5 => 'Referral',
	6 => 'Other',
	7 => 'Wholesaler',
	8 => 'Transaction Coordinator',
	9 => 'Appraiser',
	10 => 'Attorney',

);

$config['ach_account_type_option'] = array(
	0 => '',
	1 => 'Business',
	2 => 'Personal',
);

$config['payment_option_distribution'] = array(
	0 => '',
	1 => 'ACH',
	2 => 'Mail',
);

$config['fee_disbursement_list'] = array(
	0 => 'Late Fee',
	1 => 'Extension Fee',
	2 => 'Minimum Interest',
	3 => 'Default Rate',
);

$config['paidoff_status_option'] = array(
	0 => '',
	1 => 'Good Standing',
	2 => 'Default',
	3 => 'Trustee Sale',
	4 => 'REO Sale',
);

$config['escrow_account_option'] = array(
	0 => '',
	1 => 'La Mesa Fund Control',
	2 => 'Del Toro Loan Servicing',
	3 => 'FCI Lender Services',
	4 => 'Other',
);

$config['calc_day_option'] = array(
	0 => '',
	1 => '360 days',
	2 => '365 days',
);
$config['construction_status_option'] = array(
	0 => '',
	1 => 'Active',
	2 => 'Complete',
);
$config['min_bal_type_option'] = array(
	0 => '',
	1 => 'Original Loan Balance',
	2 => 'Unpaid Balance',
);
$config['will_option'] = array(
	0 => 'Select One',
	1 => 'May',
	2 => 'Will',
	3 => 'Will Not',
);

$config['vendor_type'] = array(

	1 => 'Loan Servicer',
	2 => 'Fund Control',
	3 => 'Escrow Company',
	4 => 'Title Company',
	5 => 'IRA Custodian',
);

$config['closing_statement_item_loads'] = array(

	'800' => 'Items Payable in Connection with Loan',
	'801' => 'Lender Origination Fee',
	'803' => 'Appraisal Fee',
	'804' => 'Credit Report',
	'805' => 'Lender’s Inspection Fee',
	'808' => 'Broker Fee',
	'809' => 'Tax Service Fee',
	'810' => 'Processing Fee',
	'811' => 'Underwriting Fee',
	'812' => 'Wire Transfer Fee',
	'813' => 'Fund Control Setup Fee',
	'814' => 'Fund Control Draw Fee',
	'815' => 'Loan Boarding Fee',

	'900' => 'Items Required by Lender to be Paid in Advance',
	'901' => 'load_content',
	'902' => 'Mortgage Insurance Premiums',
	'903' => 'Hazard Insurance Premiums',
	'904' => 'County Property Taxes',
	'905' => 'VA Funding Fee',

	'1000' => 'Reserves Deposited with Lender',
	'1001' => 'load_content',
	'1002' => 'load_content',
	'1004' => 'load_content',
	'1005' => 'Construction Reserve Holdback',
	'1006' => 'Payment Reserve Holdback',

	'1100' => 'Title Charges',
	'1101' => 'Settlement or Closing/Escrow Fee',
	'1105' => 'Document Preparation Fee',
	'1106' => 'Notary Fee',
	'1108' => 'Title Insurance',
	'1108' => 'Title Insurance',
	'1109' => 'Endorsements',
	'1110' => 'Closing Cost Cushion',

	'1200' => 'Government Recording and Transfer Charges',
	'1201' => 'Recording Fees',
	'1202' => 'City/County Tax/Stamps',
	//'1203'	=>	'County',

	'1300' => 'Additional Settlement Charges',
	'1302' => 'Pest Inspection',

);

$config['input_data_closing_statement'] = array(
	'901_input1' => '',
	'901_input2' => '',
	'1001_input1' => '',
	'1001_input2' => '',
	'1002_input1' => '',
	'1002_input2' => '',
	'1004_input1' => '',
	'1004_input2' => '',
	'mortage_broker_com' => '',
	'additional_compensation_checkbox' => '',
	'additional_compensation_amount' => '',
	'proposed_loan_amount' => '',
	'fee_cost_expence' => '',
	'payment_of_other_obligation' => '',
	'credit_life_insurance' => '',
	'sub_total_all_deduction' => '',
	'estimate_cash_close_amount' => '',
);

$config['loan_property_insurance_type'] = array(
	1 => 'General Property Insurance',
	2 => 'Builder Risk Insurance',
	3 => 'Flood Insurance',
);
$config['hoa_good_stand_option'] = array(
	'' => 'Select One',
	0 => 'No',
	1 => 'Yes',
	2 => 'Unknown',
);

// $config['underwriting_option']	 = array(
// 													0=>'Select One',
// 													1=>'Purchase Price',
// 													4=>'Assessed Value',
// 													2=>'BPO - Current Market Value',
// 													3=>'BPO - After Repair Value',
// 													5=>'Appraisal - Completion Value',
// 													6=>'Appraisal - Current Market Value',
// 													7=>'Paid Off Value',
// 										);

$config['underwriting_option'] = array(
	0 => 'Select One',
	1 => 'Estimate',
	2 => 'Confirmed',

);

$config['current_value_option'] = array(
	0 => 'Select One',
	1 => 'Purchase Price',
	2 => 'Appraised Value',
	3 => 'Assessed Value',
	4 => 'Broker Price Opinion',
);

$config['occupancy_value_option'] = array(
	0 => 'Select One',
	1 => 'Owner Occupied',
	2 => 'Non Owner Occupied',

);

$config['zoning_value_option'] = array(
	0 => 'Select One',
	1 => 'Single Family (1 Unit)',
	2 => 'Single Family (2-4 Unit)',
	3 => 'Multi Family (4+ Units)',
	4 => 'Commercial',
	5 => 'Industrial',
	6 => 'Retail',
	7 => 'Mixed Use',
	8 => 'Land',
);

$config['property_modal_re851d_data'] = array(
	0 => 'Select One',
	1 => 'SFR-OO',
	2 => 'SFR-NOO',
	3 => 'Income Producing',
	4 => 'Land-SFR',
	5 => 'Land-Commercial',
	6 => 'Land-Other',
	7 => 'Other',

);

$config['loan_purpose_option_type'] = array(

	0 => 'Select One',
	1 => 'Business',
	2 => 'Personal',
);

$config['relationships_option'] = array(

	0 => 'Select One',
	1 => 'Partner',
	2 => 'Spouse',
	3 => 'Referral',
	4 => 'Other',
	5 => 'Work Colleague',
);

$config['contact_status_option'] = array(

	0 => 'Select One',
	3 => 'Active',
	1 => 'Complete',
	2 => 'Cancelled',

);

$config['extension_yes_no'] = array(

	'' => 'Select One',
	1 => 'Yes',
	0 => 'No',

);

$config['extension_agreement'] = array(

	'' => 'Select One',
	1 => 'Sent to Borrower',
	2 => 'Signed by Borrower',

);

$config['extension_loan_servicer'] = array(

	'' => 'Select One',
	1 => 'Submitted to Servicer',
	2 => 'Processed by Servicer',

);

$config['extension_fee'] = array(

	'' => 'Select One',
	1 => 'Received by Servicer',
	2 => 'Processed by Servicer',

);

$config['yes_no_option3'] = array(
	'' => 'Select One',
	1 => 'Yes',
	2 => 'No',

);
$config['no_yes_option'] = array(
	2 => 'No',
	1 => 'Yes',

);

$config['who_pay_setup_fees'] = array(
	'' => 'Select One',
	1 => 'Broker',
	2 => 'Lender(s)',
);

$config['re885_complete_option'] = array(

	2 => 'No',
	1 => 'Yes',
	3 => 'Submitted',

);

$config['nno_yes_option'] = array(
	1 => 'Yes',
	2 => 'No',
	

);

$config['recording_no_yes_option'] = array(

	0 => 'No',
	1 => 'Yes',

);

$config['underwriting_items_option'] = array(

	//'1' => 'Borrower Data',
	'2' => 'Completed Loan Application',
	'3' => 'Copy of Driver’s License',
	'4' => 'Credit Check',
	'5' => 'Account Statement evidencing cash equity requirement',
	'12' => 'Personal – YTD Profit & Loss Statement',
	'13' => 'Personal – Prior YE Profit & Loss Statement',
	'14' => 'Personal – YTD Balance Sheet',
	'15' => 'Personal – Prior YE Balance Sheet',
	'16' => 'Company – YTD Profit & Loss Statement',
	'17' => 'Company – Prior YE Profit & Loss Statement',
	'18' => 'Company – YTD Balance Sheet',
	'19' => 'Company – Prior YE Balance Sheet',
	'6' => 'Prior Year Tax Return',
	'11' => 'Schedule of Assets and Liabilities',
	'20' => 'Real Estate Schedule',
	'7' => 'LLC Documents (Articles of Organization and Operating Agreement)',
	'8' => 'Corporate Documents (Articles of Incorporation and By Laws)',
	'9' => 'Trust Documents',
	'10' => 'OFAC Search',

	//'101' => 'Transaction Document',
	'102' => 'Purchase Contact',
	'103' => 'Term Sheet Signed',
	'104' => 'Escrow Contact Information',
	'105' => 'Preliminary Title Report',
	'110' => 'Escrow  / Title Closing Statement Estimate',
	//'106' => 'Detailed Project Budget',
	//'107' => 'Project Scope of Work',
	//'108' => 'Construction Reserve Deposit Account Form',
	'109' => 'Mortgage Loan Disclosure Statement (RE885)',
	'111' => 'Title Wire Instructions',
	'112' => 'Buyer Funds Deposited',
	'113' => 'Payoff Demand(s)',

	//'201' => 'Property Information',
	'202' => 'Property Images',
	'203' => 'Property Insurance',
	'204' => 'Year to Date Property P&L Statement',
	'205' => 'Prior Year Property Year End P&L Statement',
	'206' => 'Signed Lease Agreement(s)',
	'207' => 'Bank Statement showing tenant deposits',
	//'208' => 'Appraisal / Broker Price Opinion (if BPO, completed by TaliMar Financial)',
	'208' => 'Internal Valuation',
	'213' => 'Broker Price Opinion completed by 3rd Party',
	'214' => 'Appraisal completed by 3rd Party',
	'209' => 'Property Report',
	'210' => 'Copy of Prior Month Mortgage Statement',
	'211' => 'Copy of Promissory Note',
	'212' => 'Property Inspection',

);

$config['yes_no_option4'] = array(
	'' => 'Select One',
	1 => 'Yes',
	2 => 'No',
	3 => 'N/A',

);

$config['referral_source'] = array(

	4 => 'Bigger Pockets',
	20 => 'Broker',
	14 => 'County Recorder',
	18 => 'Direct',
	13 => 'E-Mail blast',
	2 => 'Facebook',
	10 => 'Google Adwords',
	11 => 'Google Search',
	3 => 'Linkedin',
	12 => 'Loan Closing',
	17 => 'Mail Blast',
	8 => 'Networking Event',
	9 => 'Other',
	15 => 'Private Lender Link',
	19 => 'Purchased',
	1 => 'Referral',
	16 => 'Website',
	// 5 => 'SDCIA',
	// 6 => 'NSDREI',
	// 7 => 'SDIC',

);

$config['lender_ownership_type'] = array(

	1 => 'Yes',
	2 => 'No',
	//1 => 'Whole Interest',
	//2 => 'Fractional Interest',
	//3 => 'Both',

);

$config['loan_servicing_checklist'] = array(

	'101' => 'Enter Loan, Contact, Borrower, Property Information in Database',
	'102' => 'Update Closing Statement figures',
	'103' => 'Submit Term Sheet',
	//'127' => 'Update Internal Contacts',
	'127' => 'Input Internal Contacts',
	//'131' => 'Loan Closing Posted',
	'104' => 'Submit Loan Input Form to Borrower(s)',
	'105' => 'Submit Loan Application, RE885 MLDS, and Other Disclosures to Borrower',
	'140' => 'Send Borrower Due Diligence Checklist',
	'139' => 'Obtain / Save Completed Loan Application, RE885, and other Disclosures',

	'106' => 'Submit Introduction e-mail to Escrow with Lender Closing Statement',
	//'128' => 'Construction Draw Account Info Inputted',
	'128' => 'Enter Borrower Construction Draw Account Information',
	//'147' => 'Loan Servicing Introduction call with Borrower',
	'117' => 'Confirm Borrower Auto Debit Account Information',
	'144' => 'Enter Title Wire Instructions into Database',
	'129' => 'Confirm Lender Schedule with Title',
	'131' => 'Underwriting Approved to Close',
	'132' => 'Investor Relations Approved to Close',
	'130' => 'Final Approval to Close',

	//'107' => 'Confirm Borrower and Contact Information',
	//'134' => 'If Entity, confirm correct spelling of Name, Signee(s), Title(s) and State',
	//'134' => 'Confirm Entity Name, Contact Name(s), Title(s), Tax ID, Address of Borrower Contact(s)',

	'108' => 'Enter Escrow & Title Information into Database',
	'145' => 'Enter Mortgage Broker, Listing / Buyer Agent, Etc. into Affiliated Parties',
	//'143' => 'Submit Introduction E-Mail to Escrow / Title',

	'109' => 'Review Loan Terms, Contact / Borrower Information, Property Information, and Closing Fee figures',
	'110' => 'Submit Loan Documents to Escrow',
	//'111' => 'Disburse Lender Disclosures and Wire Information',
	'111' => 'Distribute Lender Disclosures',
	//'112' => 'Obtain signed Loan Documents',
	'112' => 'Obtain, Review, and Upload Signed Loan Documents',
	'115' => 'Confirm Receipt of Borrower Funds',
	'116' => 'Distribute Wire Instructions',

	'125' => 'Complete Due Diligence Checklist',
	//'113' => 'Approve Estimated Closing Statement',
	//'114' => 'Lenders Notified of Loan Boarding',
	//'115' => 'Submit Request for Notice to Escrow',
	//'124' => 'Signed Lender Disclosures Received',
	'124' => 'Obtain / Save Signed Lender Disclosures',
	//'133' => 'Review Signed Loan Documents',
	//'126' => 'Confirm Receipt of Wire(s) with Escrow/Title',
	'126' => 'Confirm Receipt of Lender Funds',

	'135' => 'Underwriting Complete',
	'136' => 'Investor Relations Complete',
	'137' => 'Loan Closing Complete',
	'138' => 'Loan Servicing complete',
	'146' => 'Initial Review Complete',
	'141' => 'Post Trust Deed to Website',
	'142' => 'Market Trust Deed Offering',


	//'115' => 'RE885 MLDS Signed',
	//'116' => 'Term Sheet Signed',
	//'117' => 'Complete Borrower Wire Form for Loan Service Disbursements',
	//'118' => 'Submit Term Sheet',
	//'119' => 'Submit Loan Application',
	// '120' => 'Submit RE885 MLDS to Borrower(s)',
	// '121' => 'Submit Loan Application to Borrower(s)',
	// '122' => 'Submit Term Sheet to Borrower',
	// '123' => 'Submit Subordination Agreement',

	//'1001' => 'Confirm Receipt of Wire(s) with Escrow/Title',

	'1002' => 'Confirm Recording with Escrow',
	'1003' => 'Confirm Closing with Investor(s)',
	'1025' => 'Request Google Review',
	//'1023' => 'Loan Boarding Fees Paid',
	'1024' => 'Lender File(s) Closed',
	'1004' => 'Input Recording Information for Deed of Trust',
	'1019' => 'Loan Servicing Introduction call with Borrower',
	'1013' => 'Send Welcome E-Mail to Borrower',
	'1005' => 'Update Loan to Active',
	'1006' => 'Update Loan File Notes with Closing Irregularities',
	'1007' => 'Move Online Loan Folder to Active Folder',
	'1042' => 'Confirm Servicer Received Funds',
	'1008' => 'Submit Loan to Servicer',
	'1041' => 'Submit Incoming Wire Notification to Servicer',
	'1029' => 'Input Loan Servicing # on Loan Terms Page',
	//'1020' => 'Confirm Closing Funds Received / Processed by Servicer',

	'1009' => 'Notify Lenders when Loan has been Boarded',
	'1010' => 'Release Construction Draw to Borrower',
	'1035' => 'Confirm Loan has been added to Loan Servicer website',
	'1011' => 'Input Property Insurance into Database',
	//'1018' => 'Loan Documents Uploaded to Database',

	'1012' => 'File Original Loan Documents in Hard Loan Folder ',
	'1014' => 'Lender Fees Received',
	'1015' => 'Complete Loan Servicer Review Checklist',
	//'1016' => 'Confirm Reserves were Applied Correctly by Servicer',
	'1016' => 'Confirm Servicer Applied Closing Funds Correctly',
	'1026' => 'Loan Final Boarded with Servicer',
	//'1031' => 'Move Hard Folder to Active Loan Files',
	//'1017' => 'Loan Closing Posted',
	'1017' => 'Loan Closing Posted and URL Saved',
	'1027' => 'Social Media Blast Complete',
	'1028' => 'TaliMar Financial Sign Posted',
	'1039' => 'Loan Documents Reviewed / Approved',
	'1030' => 'Loan Closer Approved Closing',
	'1032' => 'Advertising Images Created',
	'1038' => 'Loan Description Completed',
	'1033' => 'Loan Advertising Complete',
	'1034' => 'Confirm Escrow / Title has Submitted Funds to Servicer',

	'1036' => 'Notify Loan Servicing of Closing',
	'1040' => 'Send Escrow Post-Closing Instructions Email',
	'1037' => 'Post Loan Closing Complete',

	// '1017' => 'Obtain / File original loan documents in hard Loan folder',
	// '1018' => 'Send Welcome E-Mail to Borrower',
	// '1019' => 'Move Hard Loan Folder to Active Loan File',
	// '1020' => 'Fees / Costs received by TaliMar Financial',
	// '1021' => 'Confirm Lender Name / Account # / Interest correct with Servicer',
	// '1022' => 'Confirm Reserves and/or Interest Payments received by Servicer',

);

$config['lender_investment_experience'] = array(

	1 => 'No investment Experience',
	2 => 'Mutual Funds',
	3 => 'Stocks / Shares',
	4 => 'Real Estate',
	5 => 'Annuities',
	6 => 'Notes',
	7 => 'Other',
	8 => 'Bonds',
	9 => 'Options',

);


$config['property_Valuation_Status'] = array(

	1 => 'Not Ordered',
	2 => 'Ordered',
	3 => 'Complete',

);

$config['lender_financial_situation'] = array(

	1 => 'Under $50,000',
	2 => '$50,001 to $100,000',
	3 => '$100,001 to 200,000',
	4 => '$200,001 to $300,000',
	5 => '$300,001 to $500,000',
	6 => '$500,001 to $750,000',
	7 => '$750,001 to $1,000,000',
	8 => '$1,000,001 to $5,000,000',
	9 => '$5,000,001 to $10,000,000',
	10 => 'Over $10,000,000',

);

$config['lender_liquidity_needs'] = array(

	1 => 'Primary Need is liquidity/cash',
	2 => 'Need some liquidity for possible quick access to cash',
	3 => 'No liquidity needed; have other sources of cash',

);

$config['boarding_brocker_disbrusement'] = array(

	1 => 'Not Confirmed',
	2 => 'Confirmed',

);

$config['reason_for_default'] = array(

	'' => 'Select One',
	1 => 'Late Payment',
	2 => 'Maturity',
	3 => 'Property Taxes',

);

// $config['foreclosuer_status']			= array(

// 												''	=> 'Select One',
// 												1	=> 'Borrower Notification',
// 												4	=> 'NOD Processing',
// 												2	=> 'NOD Recorded',
// 												3	=> 'NOS Recorded',
// 												5	=> 'Cancelled',

// 											);

$config['foreclosuer_status'] = array(

	'' => 'Select One',
	1 => 'Active',
	2 => 'Complete',
	5 => 'Cancelled',

);

$config['foreclosuer_step'] = array(

	'' => 'Select One',
	5 => 'NOD Pending',
	4 => 'NOD Requested',
	2 => 'NOD Recorded',
	3 => 'NOS Recorded',
	6 => 'Sale Completed',

);

$config['foreclosuer_processor'] = array(

	'' => 'Select One',
	1 => 'FCI Lender Services',
	2 => 'Del Toro Loan Servicing',

);

$config['estimale_complete_option'] = array(

	'' => 'Select One',
	1 => 'Estimate',
	2 => 'Completed',
);

$config['loan_note_option'] = array(
	'' => 'Select One',
	1 => 'Borrower',
	2 => 'Lender',
	3 => 'Internal Note',

);

$config['lender_contact_role'] = array(
	'' => 'Select One',
	1 => 'Signer',
	2 => 'Alternative',
	3 => 'Contact',

);

$config['yes_how_many_times'] = array(
	'' => 'Select One',
	11 => 'N/A',
	1 => '1',
	2 => '2',
	3 => '3',
	4 => '4',
	5 => '5',
	6 => '6',
	7 => '7',
	8 => '8',
	9 => '9',
	10 => '10',
);

$config['yes_no_option4'] = array(
	'' => 'Select One',
	1 => 'Yes',
	2 => 'No',
	3 => 'N/A',

);

$config['borrower_employment_status'] = array(
	'' => 'Select One',
	1 => 'Employed',
	2 => 'Retired',
	3 => 'Self-Employed',
	4 => 'Unemployed ',

);

$config['us_yes_no_option'] = array(
	'' => 'Select One',
	1 => 'Yes',
	2 => 'No',
);

$config['search_by_optionn'] = array(

	1 => 'Name',
	2 => 'Email',
	3 => 'Company Name',
);

$config['yes_no_with_not_applicable'] = array(
	'' => 'Select One',
	1 => 'No',
	2 => 'Yes',
	//3  => 'Not Applicable',
);

$config['not_applicable_option'] = array(
	1  => 'Not Applicable',
	2  => 'Yes',
	3  => 'No',
);

?>