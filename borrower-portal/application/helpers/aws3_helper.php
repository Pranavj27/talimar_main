<?php
ini_set('max_execution_time', 108000);
// If access is requested from anywhere other than index.php then exit
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function getAwsUrl($fileName){
    try {
        $CI = &get_instance();
        $CI->load->library('Aws3');
        // $CI->load->helper('aws3_helper.php');
        //Creating a presigned URL
        $info = pathinfo($fileName);
        $newfilename = explode('____',$info['filename']);
        if(isset($newfilename[1])){
            $newfilename = urldecode($newfilename[1]);
            // $newfilename = removeSpecialChar($newfilename);
            $newfilename = $newfilename.".".$info['extension'];
        }else{
            $newfilename = urldecode($info['filename']);
            // $newfilename = removeSpecialChar($newfilename);
            $newfilename = $newfilename.".".$info['extension'];
        }
        $cmd = $CI->aws3->getS3Object()->getCommand('GetObject', [
            'Bucket' => $CI->config->item('aws_bucketname'),
            'Key' => $fileName,
            'ResponseContentDisposition' => "inline; filename=$newfilename"
        ]);
        $request = $CI->aws3->getS3Object()->createPresignedRequest($cmd, '+10080 minutes');
        return (string)$request->getUri();
    }catch (Exception $r){
        return '';
    }
}

function aws_s3_document_url($documentPath){    
    if (strpos($documentPath, 'public_html/') !== false) {
        $arrFile = explode('public_html/', $documentPath);
        return S3_VIEW_URL.$arrFile[1];
    }
    return S3_VIEW_URL.$documentPath;
}
