<?php

// If access is requested from anywhere other than index.php then exit
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


//............................................................................Closing Status – Loan Docs Drafted / Saved......................................................................//


	if ( ! function_exists('auto_email_notification'))
	{
	    function auto_email_notification($closing_status,$talimar,$name_array,$prop_full_address,$e_closing_date,$loan_amount,$unit,$city,$state,$zip,$name_array_2)
	    {
	       $CI =& get_instance();

	  		$CI->load->library('email');

        if($closing_status == '10')
        	{  
        		
        		  	$message	=''; 
		         	$to 		= $name_array_2;
		         	//$email 		= 'anjul.wartiz@gmail.com';
		    	    $cc    		= $name_array;
		    	   //	$email_to   ='pankaj.wartiz@gmail.com';
					$subject	= "Loan Documents Drafted – ".$prop_full_address.""; 
					$header		= "From: mail@database.talimarfinancial.com"; 
					
					
					$message .='<p>The loan documents for the loan <a href="'.base_url().'">'.$talimar.'</a> secured on '.$prop_full_address.' '.$unit.'; '.$city.', '.$state.' '.$zip.' in the amount of $'.number_format($loan_amount,2).' have been drafted and saved. The loan is scheduled to close on '.$e_closing_date.'. Please complete a final review of the loan documents and submit to escrow.</p>';
					
					$message .='<p>Regards –</p>';
			
					$message .='<p>Database</p>';
				
					//$message .='TaliMar Financial<br>';
					//$message .='Phone: (858) 201-3253<br>';
					//$message .='16880 West Bernardo Ct., #140<br>';
					////$message .='San Diego, CA 92127<br>';
					//$message .='www.talimarfinancial.com';


						$CI->email
						->from('mail@database.talimarfinancial.com', $subject)
						->to($to)
						->cc($cc)
						//->bcc($email_to)
						->subject($subject)
						->message($message)
						->set_mailtype('html');
						 $CI->email->send();

		

	
			}
	    }   
	}
		

//............................................................................Closing Status – Closing Approved......................................................................//

         if ( ! function_exists('auto_email_closing_approved'))
	{
	    function auto_email_closing_approved($closing_status,$talimar,$name_array,$prop_full_address,$name_array_2)
	    {
	       $CI =& get_instance();

	  		$CI->load->library('email');

        if($closing_status == '8')
        	{  
        		
        		  	$message	=''; 
		         	$to 		= $name_array_2;
		         	$cc 		= $name_array;
		    	   // $email_t    ='anjul.wartiz@gmail.com';
		    	   	//$email_to   ='pankaj.wartiz@gmail.com';
					$subject	= "Closing Approved – ".$prop_full_address.""; 
					$header		= "From: mail@database.talimarfinancial.com"; 
					
					$message .='<p>Loan <a href="'.base_url().'">'.$talimar.'</a> as been approved to close. Please notify escrow to initiate closing.</p>';
					
					$message .='<p>Regards –</p>';
			
					$message .='<p>Database</p>';
		
					$CI->email
					->from('mail@database.talimarfinancial.com', $subject)
					->to($to)
					->cc($cc)
					//->bcc($email_to)
					->subject($subject)
					->message($message)
					->set_mailtype('html');
					 $CI->email->send();

		

	
			}
	    }   
	}



	//............................................................................Closing Status – Document Released......................................................................//

     if ( ! function_exists('auto_email_document_released'))
	{
	    function auto_email_document_released($closing_status,$prop_full_address,$under_writter)
	    {
	       $CI =& get_instance();

	  		$CI->load->library('email');

        if($closing_status == '3')
        	{  
        		
        		  	$message	=''; 
		         	$to 		= $under_writter;
		         	//$to 		= 'chuffor@talimarfinancial.com';
		         	//$cc 		= $name_array;
		    	   // $email_t    ='anjul.wartiz@gmail.com';
		    	   	//$email_to   ='pankaj.wartiz@gmail.com';
					$subject	= "Status Update - ".$prop_full_address.""; 
					$header		= "From: mail@database.talimarfinancial.com"; 
				
					$message .='<p>The loan documents for '.$prop_full_address.' have been submitted. Please notify the Borrower to schedule a signing.</p>';
					
					$CI->email
					->from('mail@database.talimarfinancial.com', $subject)
					->to($to)
					//->cc($cc)
					//->bcc($email_to)
					->subject($subject)
					->message($message)
					->set_mailtype('html');
					 $CI->email->send();

		

	
			}
	    }   
	}





	//........................Loan Paid Off.....................//

     if ( ! function_exists('auto_email_loan_payoff'))
	{
	    function auto_email_loan_payoff($loan_status,$talimar_loan,$full_address,$half_address,$ls_ir_c,$loan)
	    {
	       $CI =& get_instance();

	  		$CI->load->library('email');

        if($loan_status == '3')
        	{  
        		
        		  	$message	=''; 
		         	$to 		= $ls_ir_c;
		         	//$to 		= 'chuffor@talimarfinancial.com';
		         	//$cc 		="";
		         //	$bcc 		="";
		    	   // $email_t    ='anjul.wartiz@gmail.com';
		    	   	//$email_to   ='pankaj.wartiz@gmail.com';
					$subject	= "Loan Paid Off - ".$half_address.""; 
					$header		= "From: mail@database.talimarfinancial.com"; 
				
					$message .='<p>Loan <a href="'.base_url('load_data/'.$loan).'">'.$talimar_loan.'</a> secured on '.$full_address.' has been paid off. Please process the payoff and notify the Lenders.</p>';
					
					$message .='<p>Regards –</p>';
			
					$message .='<p>Database</p>';
					
					$CI->email
					->from('mail@database.talimarfinancial.com', $subject)
					->to($to)
					//->cc()
					//->bcc()
					->subject($subject)
					->message($message)
					->set_mailtype('html');
					 $CI->email->send();

		

	
			}
	    }   
	}
		
?>