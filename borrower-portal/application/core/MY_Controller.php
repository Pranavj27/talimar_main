<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	/**
		Main Controller file
		Common function here 
	**/
	
	public function fetchContactname($contactid)
	{	
		
		$fetch_contact = $this->User_model->query("SELECT * FROM contact WHERE contact_id = '".$contactid."'");
		$fetch_contact = $fetch_contact->result();

		return $fetch_contact;
		
	}


	public function Borrower_associate_with_contact($contactid){

		$fetchborrower = $this->User_model->query("SELECT bd.b_name, bd.borrower_type, bd.ira_account, bd.fci_acc_text, bd.id as b_id, bd.b_tax_id FROM borrower_contact as bc JOIN borrower_data as bd ON bc.borrower_id = bd.id WHERE bc.contact_id = '".$contactid."' AND bc.borrower_portal_access = '1'");
		if($fetchborrower->num_rows() > 0){
			$fetchborrower = $fetchborrower->result();
		}else{
			$fetchborrower = '';
		}

		return $fetchborrower;
	}

	public function both_Borrower_and_contactdata($contactid,$borowerid){

		$fetchborrower = $this->User_model->query("SELECT bd.b_name, bd.borrower_type, bd.ira_account, bd.fci_acc_text, bd.id as b_id, bd.b_tax_id FROM borrower_contact as bc JOIN borrower_data as bd ON bc.borrower_id = bd.id WHERE bc.contact_id = '".$contactid."' AND bc.borrower_id = '".$borowerid."' AND bc.borrower_portal_access = '1'");
		if($fetchborrower->num_rows() > 0){
			$fetchborrower = $fetchborrower->result();
		}else{
			$fetchborrower = '';
		}

		return $fetchborrower;
	}

	public function get_monthly_payment_for_loan($talimar_loan) {
		
		$fetchmdata = $this->User_model->query("SELECT * FROM `impound_account` WHERE talimar_loan = '".$talimar_loan."'");
		$fetch_impound_account = $fetchmdata->result();
		
				
		$fetchloandata = $this->User_model->query("SELECT intrest_rate,loan_amount FROM `loan` WHERE talimar_loan = '".$talimar_loan."'");
		$fetch_loan_result = $fetchloandata->result();
		
		$aa = 0;
		foreach ($fetch_impound_account as $key => $row) {
			$impound_amount = $row->amount;
			$readonly = '';

			if ($row->items == 'Mortgage Payment') {

				$aa += ($fetch_loan_result[0]->intrest_rate / 100) * $fetch_loan_result[0]->loan_amount / 12;
			 // $aa += ($outstanding_balance * ($fetch_loan_result[0]->intrest_rate)/100)/12;
			}
			if ($row->items == 'Property / Hazard Insurance Payment') {
				if ($row->impounded == 1) {
					$aa += isset($loan_property_result) ? ($loan_property_insurance_result->annual_insurance_payment / 12) : 0;
				}
			}

			if ($row->items == 'County Property Taxes') {
				if ($row->impounded == 1) {
					$aa += isset($loan_property_result) ? ($loan_property_result[0]->annual_property_taxes / 12) : 0;
				}
			}
			if ($row->items == 'Flood Insurance') {
				if ($row->impounded == 1) {
					$aa += isset($row->amount) ? ($row->amount) : 0;
				}
			}

			if ($row->items == 'Mortgage Insurance') {
				if ($row->impounded == 1) {
					$aa += isset($row->amount) ? ($row->amount) : 0;
				}
			}
		}
		
		return $aa;
	}

	public function encodeURL($string){
		$encode = base64_encode(urlencode($string*12345678)/12345);
		return $encode;
	}

	public function decodeURL($string){
		$checkID = base64_decode(urldecode($string));
		$decodedID = number_format(($checkID*12345)/12345678);
		return $decodedID;
	}
}
?>
