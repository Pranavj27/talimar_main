<?php
Defined('BASEPATH') OR exit('No direct script access allowed');

Class Personal_details extends CI_Controller
{
	
	public function __construct(){
		
		parent:: __construct();
		
		$this->load->model('User_model');
		$this->load->library('email');
		$this->load->library('TCPDF');
		$this->load->helper('mailhtml_helper.php');

		if($this->session->userdata('p_user_id') != ''){
			
		}
		else
		{
			redirect(base_url());
		}
	}

	
	public function update_notification(){

		$notify2 = $this->input->post('notify2');
		$notify7 = $this->input->post('notify7');
		$notify9 = $this->input->post('notify9');

		$useriD  = $this->session->userdata('p_user_id');
		$notification_setting 	= $this->config->item('notification_setting');

		if($notify2 == '' && $notify7 == '' && $notify9 == ''){

			echo 1;

		}else{

			$createArray = array($notify2,$notify7,$notify9);
			foreach ($notification_setting as $key => $value) {

				$fetchcontactTag = $this->User_model->query("SELECT * from contact_tags where contact_id = '".$useriD."' AND contact_email_blast = '".$key."'");
				if($fetchcontactTag->num_rows() > 0){

					if(in_array($key,$createArray))
					{
						
					}
					else
					{
						$this->User_model->delete('contact_tags',array('contact_id'=>$useriD,'contact_email_blast'=>$key));
					}

				}else{

					if(in_array($key,$createArray))
					{
						$this->User_model->insertdata('contact_tags',array('contact_id'=>$useriD,'contact_email_blast'=>$key));
					}
				}
				
			}

			echo 2;
			
		}
	}


	public function Updateusernameinfo(){

		$contact_id = $this->session->userdata('p_user_id');
		$username = $this->input->post('fieldval');

		$data1['username'] 		= $username;
		$where1['contact_id'] 	= $contact_id;
		$this->User_model->updatedata('lender_contact_type',$where1,$data1);
		echo '1';
	}


	public function UpdatePassword(){

		/*echo '<pre>';
		print_r($_POST);
		echo '</pre>';*/

		$oldpassword = $this->input->post('old_password');
		$newpassword = $this->input->post('new_password');
		$repassword = $this->input->post('re_password');
		$contact_id = $this->session->userdata('p_user_id');

		if($oldpassword && $newpassword && $repassword != ''){

			$where['password'] = $oldpassword;
			$where['contact_id'] = $contact_id;

			$checkPassword = $this->User_model->select_where('lender_contact_type',$where);
			if($checkPassword->num_rows() > 0){

				if($newpassword == $repassword){

					$data1['password'] = $newpassword;
					$data1['c_password'] = $newpassword;
					$where1['contact_id'] = $contact_id;
					$UpdatePassword = $this->User_model->updatedata('lender_contact_type',$where1,$data1);
					$this->session->set_flashdata('success','Password updated.');

					$this->session->unset_userdata('p_user_id');		
					$this->session->unset_userdata('p_user_fname');			
		
					//$this->session->sess_destroy(); // logout
					redirect(base_url());

				}else{
					$this->session->set_flashdata('error','New password and confirm password not match.');
					redirect(base_url().'view_details');
				}
			}else{
				$this->session->set_flashdata('error','Old password not match with your account.');
				redirect(base_url().'view_details');
			}
		}else{

			$this->session->set_flashdata('error','All fields required.');
			redirect(base_url().'view_details');
		}

	}


	public function view_details(){
		
		$personal_data = $this->User_model->query("Select * from contact where contact_id = '".$this->session->userdata('p_user_id')."'");
		$personal_data = $personal_data->result();
		$data['personal_view_details']	= $personal_data;


		$contact_tags = $this->User_model->query("Select * from contact_tags where contact_id = '".$this->session->userdata('p_user_id')."'");
		$contact_tags = $contact_tags->result();
		$data['contact_tags_details']	= $contact_tags;


		$loginuser_detail = $this->fetch_login_user();
		$data['loginuser_detail'] = $loginuser_detail;
		
		$data['content'] = $this->load->view('Personal_detail/view_info',$data,true);
		$this->load->view('template_files/template',$data);

	
	}
	
	
	public function personal_setting(){
		if($this->uri->segment(1)=='personal_setting'){

		redirect(base_url().'view_details','refresh');
		
		}
		$personal_data = $this->User_model->query("Select * from contact where contact_id = '".$this->session->userdata('p_user_id')."'");
		$personal_data = $personal_data->result();
		$data['personal_data_details']	= $personal_data;


		$loginuser_detail = $this->fetch_login_user();
		$data['loginuser_detail'] = $loginuser_detail;
		
		$data['content'] = $this->load->view('Personal_detail/personal_info',$data,true);
		$this->load->view('template_files/template',$data);

	
	}

	public function fetch_login_user(){

		$contcat_id = $this->session->userdata('p_user_id');
		$fetch_user = $this->User_model->query("SELECT username,password FROM lender_contact_type WHERE contact_id = '".$contcat_id."'");
		$fetch_user = $fetch_user->result();

		return $fetch_user;
	}
	
	//.........edit personal setting start ..............//
	
	public function edit_profile($idd){

		
		if(isset($_POST['submit'])){

			
			$data['contact_firstname'] 	        = $this->input->post('fname');
			$data['contact_lastname'] 			= $this->input->post('lname');
	        $data['contact_email'] 				= $this->input->post('email');
			$data['contact_phone'] 			    = $this->input->post('p_contact');
			$data['alter_phone'] 				= $this->input->post('secondary_contact');
			$data['primary_option'] 			= $this->input->post('primary_type');
			$data['alter_option'] 				= $this->input->post('alter_type');
			$data['contact_fax'] 				= $this->input->post('fax');
			$data['contact_unit'] 				= $this->input->post('unit');
			$data['contact_city'] 				= $this->input->post('City');
			$data['contact_state'] 				= $this->input->post('State');
			$data['contact_zip'] 				= $this->input->post('Zip');
			$data['contact_personal_notes'] 	= $this->input->post('Contact');
			$data['contact_mailing_address'] 	= $this->input->post('Mailing_add');
			$data['contact_mailing_unit'] 		= $this->input->post('Mailing_unit');
			$data['contact_mailing_city']		= $this->input->post('Mailing_city');
			$data['contact_mailing_state']		= $this->input->post('Mailing_state');
			$data['contact_mailing_zip']		= $this->input->post('Mailing_zip');
			$data['contact_address']			= $this->input->post('street');
			$data['same_mailing_addresss']		= $this->input->post('same_mailing_addresss');
			$data['pemail_type']				= $this->input->post('email_type');
			$data['contact_suffix']				= $this->input->post('legal_lirst');
   
			$this->User_model->updatedata('contact',array('contact_id'=>$idd),$data);

			if($this->input->post('username') || $this->input->post('password')){

				$updata['username'] = $this->input->post('username');
				$updata['password'] = $this->input->post('password');
				$updata['c_password'] = $this->input->post('password');


				$this->User_model->updatedata('lender_contact_type',array('contact_id'=>$idd),$updata);

			}

			$this->session->set_flashdata('success','Account Updated Successfully!');

				$message	=''; 
		         	//$email 		=  $contact_email_onee;		         	
	            $email 			='invest@talimarfinancial.com';		    	  
				$subject	= "Personal Setting"; 
				$header		= "From: mail@talimarfinancial.com"; 
				
				$message .='<p>'.$this->session->userdata('p_user_fname').' '.$this->session->userdata('p_user_lname').' has modified the settings on their account.</p>';

			$maildata = array();
			$maildata['from_email'] = 'mail@talimarfinancial.com';
			$maildata['from_name'] = $subject;
			$maildata['to'] = $email;
			$maildata['name'] = $this->session->userdata('p_user_fname');
			$maildata['subject'] = $subject;
			$maildata['content_body'] = $message;
			$maildata['button_url'] = '';
			$maildata['button_text'] = '';
			send_mail_template($maildata);
			
			redirect(base_url().'personal_setting');
		
		
		}
	}	
	
	
}
	
	?>