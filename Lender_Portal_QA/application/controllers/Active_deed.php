<?php
Defined('BASEPATH') OR exit('No direct script access allowed');

Class Active_deed extends CI_Controller{
	
	public function __construct(){
		
		parent:: __construct();
		
		$this->load->model('User_model');
		$this->load->library('email');
		$this->load->library('TCPDF');
		
		 if($this->session->userdata('p_user_id') != ''){
			
		}
		else
		{
			redirect(base_url());
		}
	}
	
	public function active_trust_deeds(){
		
		error_reporting(0);

		
		  $trust_deed = $this->input->post('trust_deed');
		  $status_option = $this->input->post('status_option');
		  $account_option = $this->input->post('account_option');

		 if($account_option != 0){

		 	$sql = "SELECT * FROM lender_contact JOIN investor ON lender_contact.lender_id = investor.id WHERE lender_contact.contact_id = '".$this->session->userdata('p_user_id')."' AND investor.id = '".$account_option."'";

		 }else{

			$sql = "SELECT * FROM lender_contact JOIN investor ON lender_contact.lender_id = investor.id WHERE lender_contact.contact_id = '".$this->session->userdata('p_user_id')."'";
		 }

		 if($trust_deed != ''){

		 	if($trust_deed == '2'){
		 		$trust_deed_filter = 'AND loan_assigment.nfiles_status = 0';
		 	}elseif($trust_deed == '1'){
		 		$trust_deed_filter = 'AND loan_assigment.nfiles_status = 1';
		 	}else{
		 		$trust_deed_filter = '';
		 	}
		 }else{
		 	$trust_deed_filter = '';
		 }

		$fetch_investors 	= $this->User_model->query($sql);
		$fetch_investors 	= $fetch_investors->result();
		// echo '<pre>';
		// print_r($fetch_investors);
		// echo '</pre>';
		foreach($fetch_investors as $investor_data)
		{
			
			$lender_name_new					= $investor_data->id;
			$investor_id['lender_name'] 		= $investor_data->id;
			$investor_name  					= $investor_data->name;
			$data['header_lender_name'] 		= $investor_name ;
			$talimar_lender 					= $investor_data->talimar_lender;
			if($this->uri->segment(2)){
				$cond3=" AND loan_assigment.lender_name='".$this->uri->segment(2)."'";	
			}else{

			$cond3="";	
			}

			if($status_option != 0){

				$sql_assign = "SELECT loan_assigment.nfiles_status,loan_assigment.nservicer_status,loan_assigment.talimar_loan, loan_assigment.investment, loan_assigment.payment, loan_assigment.invester_yield_percent ,loan_assigment.percent_loan ,loan_servicing.loan_status,loan_servicing.loan_payment_status FROM loan_assigment JOIN loan_servicing ON loan_servicing.talimar_loan = loan_assigment.talimar_loan WHERE loan_assigment.lender_name = '".$lender_name_new."' ".$loan_status_condition." AND loan_servicing.loan_status = '".$status_option."' ".$trust_deed_filter." ";

			}else{

				if($trust_deed_filter != ''){
					
					$sql_assign = "SELECT loan_assigment.nfiles_status,loan_assigment.nservicer_status,loan_assigment.talimar_loan, loan_assigment.investment, loan_assigment.payment, loan_assigment.invester_yield_percent ,loan_assigment.percent_loan ,loan_servicing.loan_status,loan_servicing.loan_payment_status FROM loan_assigment JOIN loan_servicing ON loan_servicing.talimar_loan = loan_assigment.talimar_loan WHERE loan_assigment.lender_name = '".$lender_name_new."' ".$trust_deed_filter." AND loan_servicing.loan_status IN('1','2') ";


				}else{
				
				 $sql_assign = "SELECT loan_assigment.nfiles_status,loan_assigment.nservicer_status,loan_assigment.talimar_loan, loan_assigment.investment, loan_assigment.payment, loan_assigment.invester_yield_percent ,loan_assigment.percent_loan ,loan_servicing.loan_status,loan_servicing.loan_payment_status FROM loan_assigment JOIN loan_servicing ON loan_servicing.talimar_loan = loan_assigment.talimar_loan WHERE loan_assigment.lender_name = '".$lender_name_new."' ".$loan_status_condition." ".$cond3." AND loan_servicing.loan_status IN('1','2') ";
				}

			}
				
				
			$fetch_assignment_data 				= $this->User_model->query($sql_assign);
				
			$fetch_assignment_data 				= $fetch_assignment_data->result();
				
			foreach($fetch_assignment_data as $assigment_data)
			{
					
				$talimar['talimar_loan'] 	= $assigment_data->talimar_loan;
				$talimar_loan 				= $assigment_data->talimar_loan;
				$nfiles_status 				= $assigment_data->nfiles_status;
				$loan_payment_status 		= $assigment_data->loan_payment_status;
				$servicer_status 			= $assigment_data->nservicer_status;
					

					
				$fetch_loan_data 	= $this->User_model->select_where('loan', $talimar);
				$fetch_loan_data 	= $fetch_loan_data->result();
					
				$loan_amount  		= $fetch_loan_data[0]->loan_amount;
				$loan_id  			= $fetch_loan_data[0]->id;
				$fci  				= $fetch_loan_data[0]->fci;
				$term  				= $fetch_loan_data[0]->term_month;
				$position  			= $fetch_loan_data[0]->position;
				$payment_amount  	= $fetch_loan_data[0]->payment_amount;
					
				
					
				$fetch_loan_servicing 	= $this->User_model->select_where('loan_servicing',$talimar);
				$fetch_loan_servicing 	= $fetch_loan_servicing->result();
				$loan_status 			= $fetch_loan_servicing[0]->loan_status;
				$servicing_agent 	    = $fetch_loan_servicing[0]->sub_servicing_agent;
				$condition 				= $fetch_loan_servicing[0]->condition;
				$payment_status 		= $fetch_loan_servicing[0]->loan_payment_status;
	
				$fetch_property_data 	= $this->User_model->select_where('loan_property',$talimar);
				$fetch_property_data 	= $fetch_property_data->result();
				$property_address 		= $fetch_property_data[0]->property_address;
				$state 					= $fetch_property_data[0]->state;
				$unit 					= $fetch_property_data[0]->unit;
				$city 					= $fetch_property_data[0]->city;
				$zip 					= $fetch_property_data[0]->zip;
					
					
				$sql = "SELECT SUM(investment) as total_balance FROM loan_assigment WHERE  talimar_loan ='".$talimar_loan."' ";
				
				$investment_total = $this->User_model->query($sql);
					
				if($investment_total->num_rows() > 0)
				{
					$investment_total = $investment_total->result();
					$investment_total = $investment_total[0]->total_balance;
					
				
				}
				else
				{
					$investment_total = 0;
				
				}
					
				$available = $loan_amount - $investment_total;
			if($this->uri->segment(2)){	
				if($loan_status == '2')
				{
					$loan_schedule_investor[]  = array(
													'investor_name' 	=> $investor_name,
													'talimar_lender' 	=> $talimar_lender,
													'talimar_loan' 		=> $assigment_data->talimar_loan,
													'loan_id' 			=> $loan_id,
													'fci' 				=> $fci,
													'servicer_status' 	=> $servicer_status,
													'servicing_agent'   => $servicing_agent,
													'investment' 		=> $assigment_data->investment,
													'percent_loan' 		=> $assigment_data->percent_loan,
													'lender_payment' 	=> $assigment_data->payment,
													'percent_yield' 	=> $assigment_data->invester_yield_percent,
													'percent_yield_dollar' => ($loan_amount/100) * $assigment_data->invester_yield_percent,
													'loan_amount'		=> $loan_amount,
													'term'				=> $term,
													'payment_amount'	=> $payment_amount,
													'available'			=> $available,
													'position'			=> $position,
													'loan_status'		=> $loan_status,
													'loan_funding_date'	=> $fetch_loan_data[0]->loan_funding_date,
													'property_address'	=> $property_address,
													'unit'				=> $unit,
													'city'				=> $city,
													'state'				=> $state,
													'zip'				=> $zip,
													'loan_condition'	=> $condition,
													'nfiles_status'		=> $nfiles_status,
													'payment_status'	=> $loan_payment_status,

												); 
				}
			}
		    else{
					
					$loan_schedule_investor[]  = array(
													'investor_name' 	=> $investor_name,
													'talimar_lender' 	=> $talimar_lender,
													'talimar_loan' 		=> $assigment_data->talimar_loan,
													'loan_id' 			=> $loan_id,
													'fci' 				=> $fci,
													'servicer_status' 	=> $servicer_status,
													'servicing_agent'   => $servicing_agent,
													'investment' 		=> $assigment_data->investment,
													'percent_loan' 		=> $assigment_data->percent_loan,
													'lender_payment' 	=> $assigment_data->payment,
													'percent_yield' 	=> $assigment_data->invester_yield_percent,
													'percent_yield_dollar' => ($loan_amount/100) * $assigment_data->invester_yield_percent,
													'loan_amount'		=> $loan_amount,
													'term'				=> $term,
													'payment_amount'	=> $payment_amount,
													'available'			=> $available,
													'position'			=> $position,
													'loan_status'		=> $loan_status,
													'loan_funding_date'	=> $fetch_loan_data[0]->loan_funding_date,
													'property_address'	=> $property_address,
													'unit'				=> $unit,
													'city'				=> $city,
													'state'				=> $state,
													'zip'				=> $zip,
													'loan_condition'	=> $condition,
													'nfiles_status'		=> $nfiles_status,
													'payment_status'	=> $loan_payment_status,
												); 
				}
					
			}
		}
		
		$data['trust_deed_val'] =  $trust_deed;
		$data['status_option_val'] =  $status_option;
		$data['account_option_val'] =  $account_option;

		$data['active_trust_deed_schedule']	 = $loan_schedule_investor;

		$all_investor = $this->fetch_only_investor_name();
		$data['all_investor_name'] = $all_investor;
		
		
		
		$data['content'] = $this->load->view('Active_trust_deed/active_deeds',$data,true);
		$this->load->view('template_files/template',$data);
	}


	public function fetch_only_investor_name(){

		$query = "SELECT * FROM lender_contact join investor on lender_contact.lender_id = investor.id  where lender_contact.contact_id = '".$this->session->userdata('p_user_id')."' AND lender_contact.link_to_lender = '2'";
		
		$fetch_account_info = $this->User_model->query($query);

		if($fetch_account_info->num_rows() > 0){

			$fetch_account_info = $fetch_account_info->result();
		}else{

			$fetch_account_info = '';
		}

		return $fetch_account_info;
	}


	public function active_trust_pdf(){
		
	    error_reporting(0);

	    $trustdeed_value = $this->input->post('trustdeed_value');
	    $account_value = $this->input->post('account_value');

		$loan_type 			= $this->config->item('loan_type_option');
		$position_option 	= $this->config->item('position_option');
	    $loan_status_option 		= $this->config->item('loan_status_option');
		$servicing_agent_option 		= $this->config->item('servicing_agent_option');

		if($account_value != 0){

		 	$sql = "SELECT * FROM lender_contact JOIN investor ON lender_contact.lender_id = investor.id WHERE lender_contact.contact_id = '".$this->session->userdata('p_user_id')."' AND investor.id = '".$account_value."'";

		 }else{

			$sql = "SELECT * FROM lender_contact JOIN investor ON lender_contact.lender_id = investor.id WHERE lender_contact.contact_id = '".$this->session->userdata('p_user_id')."'";
		 }

		$fetch_investors 	= $this->User_model->query($sql);
		$fetch_investors 	= $fetch_investors->result();
		
		foreach($fetch_investors as $investor_data)
		{
			
			$lender_name_new					= $investor_data->id;
			$investor_id['lender_name'] 		= $investor_data->id;
			$investor_name  					= $investor_data->name;
			$data['header_lender_name'] 		= $investor_name ;
			$talimar_lender 					= $investor_data->talimar_lender;	
			

			if($trustdeed_value != 0){

				$sql_assign = "SELECT loan_assigment.nfiles_status,loan_assigment.talimar_loan, loan_assigment.investment, loan_assigment.payment, loan_assigment.invester_yield_percent ,loan_assigment.percent_loan ,loan_servicing.loan_status FROM loan_assigment JOIN loan_servicing ON loan_servicing.talimar_loan = loan_assigment.talimar_loan WHERE loan_assigment.lender_name = '".$lender_name_new."' ".$loan_status_condition." AND loan_servicing.loan_status= '".$trustdeed_value."'";

			}else{	
				
				$sql_assign = "SELECT loan_assigment.nfiles_status,loan_assigment.talimar_loan, loan_assigment.investment, loan_assigment.payment, loan_assigment.invester_yield_percent ,loan_assigment.percent_loan ,loan_servicing.loan_status FROM loan_assigment JOIN loan_servicing ON loan_servicing.talimar_loan = loan_assigment.talimar_loan WHERE loan_assigment.lender_name = '".$lender_name_new."' ".$loan_status_condition." ";

			}
				
				
			$fetch_assignment_data 				= $this->User_model->query($sql_assign);
				
			$fetch_assignment_data 				= $fetch_assignment_data->result();
				
			foreach($fetch_assignment_data as $assigment_data)
			{
					
				$talimar['talimar_loan'] 	= $assigment_data->talimar_loan;
				$talimar_loan 				= $assigment_data->talimar_loan;
				$nfiles_status 				= $assigment_data->nfiles_status;
					

					
				$fetch_loan_data 	= $this->User_model->select_where('loan', $talimar);
				$fetch_loan_data 	= $fetch_loan_data->result();
					
				$loan_amount  		= $fetch_loan_data[0]->loan_amount;
				$loan_id  			= $fetch_loan_data[0]->id;
				$fci  				= $fetch_loan_data[0]->fci;
				$term  				= $fetch_loan_data[0]->term_month;
				$position  			= $fetch_loan_data[0]->position;
				$payment_amount  	= $fetch_loan_data[0]->payment_amount;
					
				
					
				$fetch_loan_servicing 	= $this->User_model->select_where('loan_servicing',$talimar);
				$fetch_loan_servicing 	= $fetch_loan_servicing->result();
				$loan_status 			= $fetch_loan_servicing[0]->loan_status;
				$servicing_agent 	    = $fetch_loan_servicing[0]->sub_servicing_agent;
				$condition 				= $fetch_loan_servicing[0]->condition;
	
				$fetch_property_data 	= $this->User_model->select_where('loan_property',$talimar);
				$fetch_property_data 	= $fetch_property_data->result();
				$property_address 		= $fetch_property_data[0]->property_address;
				$state 					= $fetch_property_data[0]->state;
				$unit 					= $fetch_property_data[0]->unit;
				$city 					= $fetch_property_data[0]->city;
				$zip 					= $fetch_property_data[0]->zip;
					
					
				$sql = "SELECT SUM(investment) as total_balance FROM loan_assigment WHERE  talimar_loan ='".$talimar_loan."' ";
				
				$investment_total = $this->User_model->query($sql);
					
				if($investment_total->num_rows() > 0)
				{
					$investment_total = $investment_total->result();
					$investment_total = $investment_total[0]->total_balance;
					
				
				}
				else
				{
					$investment_total = 0;
				
				}
					
				$available = $loan_amount - $investment_total;
			

			if($this->input->post('uri')!=''){	
				if($loan_status == '2')
				{
	
					
					$active_trust_data[]  = array(
													'investor_name' 	=> $investor_name,
													'talimar_loan' 		=> $assigment_data->talimar_loan,
													'loan_id' 			=> $loan_id,
													'fci' 				=> $fci,
													'servicing_agent'   => $servicing_agent,
													'investment' 		=> $assigment_data->investment,
													'percent_loan' 		=> $assigment_data->percent_loan,
													'lender_payment' 	=> $assigment_data->payment,
													'percent_yield' 	=> $assigment_data->invester_yield_percent,
													'percent_yield_dollar' => ($loan_amount/100) * $assigment_data->invester_yield_percent,
													'loan_amount'		=> $loan_amount,
													'term'				=> $term,
													'payment_amount'	=> $payment_amount,
													'available'			=> $available,
													'position'			=> $position,
													'loan_status'		=> $loan_status,
													'loan_funding_date'	=> $fetch_loan_data[0]->loan_funding_date,
													'property_address'	=> $property_address,
													'city'				=> $city,
													'state'				=> $state,
													'zip'				=> $zip,
													'loan_condition'	=> $condition,
													'nfiles_status'		=> $nfiles_status,
													'talimar_lender'	=> $talimar_lender,
												); 
				}

			}else{

						$active_trust_data[]  = array(
													'investor_name' 	=> $investor_name,
													'talimar_loan' 		=> $assigment_data->talimar_loan,
													'loan_id' 			=> $loan_id,
													'fci' 				=> $fci,
													'servicing_agent'   => $servicing_agent,
													'investment' 		=> $assigment_data->investment,
													'percent_loan' 		=> $assigment_data->percent_loan,
													'lender_payment' 	=> $assigment_data->payment,
													'percent_yield' 	=> $assigment_data->invester_yield_percent,
													'percent_yield_dollar' => ($loan_amount/100) * $assigment_data->invester_yield_percent,
													'loan_amount'		=> $loan_amount,
													'term'				=> $term,
													'payment_amount'	=> $payment_amount,
													'available'			=> $available,
													'position'			=> $position,
													'loan_status'		=> $loan_status,
													'loan_funding_date'	=> $fetch_loan_data[0]->loan_funding_date,
													'property_address'	=> $property_address,
													'city'				=> $city,
													'state'				=> $state,
													'zip'				=> $zip,
													'loan_condition'	=> $condition,
													'nfiles_status'		=> $nfiles_status,
													'talimar_lender'	=> $talimar_lender,
												); 


			}
					
			}
		}	
		
		
		                $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);

						// set document information
						$pdf->SetCreator(PDF_CREATOR);
						// set default header data
						 $pdf->SetHeaderData("include/icons/logo-light.png", 30, '', '');

						// set header and footer fonts
						$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
						$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

						// set default monospaced font
						$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

						// set margins
						$pdf->SetMargins(2, 20, 2);
						// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
						$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
						$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

						// set auto page breaks
						$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

						// set image scale factor
						$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
						$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
						$pdf->SetFont('times', '', 7);

						// add a page
						$pdf->AddPage('L', 'A4');
						// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
						
						$html ='<h1 style="color:#003468;font-size:18px;">Active Trust Deeds:</h1>'; 
						$html .= '<style>
						.table {
							width: 100%;
							max-width: 100%;
							margin-bottom: 20px;
							border-collapse:collapse;
						
						}
						.table-bordered {
						border: 1px solid #ddd;
						}
						table {
							border-spacing: 0;
							border-collapse: collapse;
						
						}
						.table td{
							height : 28px;
							font-size:13px;
						text-align:left;
						
						}
						tr.table_header th{
							height:25px;
							font-size:12px;
							font-family: "Times New Roman", Georgia, Serif;
							text-align:center;
							background-color:#bfcfe3;
					
						}
						
					tr.table_bottom th{
							height:25px;
							font-size:12px;
							font-family: "Times New Roman", Georgia, Serif;
							text-align:left;
							background-color:#bfcfe3;
						
						
						}
					
					
						tr.odd td{
							background-color:#ededed;
						
						}
						</style>';
						
						$html .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';
						
						$html .= '<tr class="table_header">';
						$html .= '<th style="width:11%;text-decoration:underline;"><strong>Loan #</strong></th>';
						$html .= '<th style="width:10%;text-decoration:underline;"><strong>Address</strong></th>';
						$html .= '<th style="width:8%;text-decoration:underline;"><strong>Position</strong></th>';
						$html .= '<th style="width:8%;text-decoration:underline;"><strong>Loan Amount</strong></th>'; 
						$html .= '<th style="width:8%;text-decoration:underline;"><strong>$ Ownership</strong></th>';
						
						$html .= '<th style="width:8%;text-decoration:underline;"><strong>% Ownership</strong></th>'; 
						$html .= '<th style="width:8%;text-decoration:underline;"><strong>Lender Yield</strong></th>'; 
						$html .= '<th style="width:7%;text-decoration:underline;"><strong>Payment</strong></th>';
						$html .= '<th style="width:8%;text-decoration:underline;"><strong>City</strong></th>';
						$html .= '<th style="width:5%;text-decoration:underline;"><strong>State</strong></th>';
						$html .= '<th style="width:5%;text-decoration:underline;"><strong>Zip</strong></th>';
					//	$html .= '<th style="width:14%;text-decoration:underline;"><strong>Servicer</strong></th>';
						$html .= '<th style="width:7%;text-decoration:underline;"><strong>Loan Status</strong></th>';
						$html .= '<th style="width:7%;text-decoration:underline;"><strong>TD Status</strong></th>';
					
						$html .= '</tr>';
						
					
						$num = 0;
					    $loan_amount = 0;
						$total_payment = 0;
					
						
						if(isset($active_trust_data))
						{
							
							foreach($active_trust_data as $key => $row)
							{
								$num++;
								$loan_amount += $row['loan_amount'];
								$total_payment += $row['lender_payment'];	
								$number = $key;
									if($number % 2 == 0) {
										$class_ad = "even";
										}
									
										else
										{
											$class_ad = "odd";
										}

										 if($row['nfiles_status']=='1'){
                                               // $class='black_color';
                                                $text='Complete';
                                              }else{

                                                $text='Processing';
                                                //$class='red_color';
                                              }
  							$percent_ownership= ($row['investment']/$row['loan_amount'])*100;
								
								$html .= '<tr class="'.$class_ad.'">';
								$html .= '<td>'.$row['talimar_loan'].'</td>';
								$html .= '<td >'.$row['city'].', '.$row['state'].'</td>';
								$html .= '<td>'.$position_option[$row['position']].'</td>';
								$html .= '<td>'.'$'.number_format($row['loan_amount']).'</td>';
								$html .= '<td>'.'$'.number_format($row['investment']).'</td>';
								
								$html .= '<td>'.number_format($percent_ownership).'%'.'</td>';
								$html .= '<td>'.number_format($row['percent_yield'],2).'%'.'</td>';
				                $html .= '<td>'.'$'.number_format($row['lender_payment'],2).'</td>';
				                $html .= '<td>'.$row['city'].'</td>';
				                $html .= '<td>'.$row['state'].'</td>';
				                $html .= '<td>'.$row['zip'].'</td>';
						      //  $html .= '<td>'.$servicing_agent_option[$row['servicing_agent']].'</td>';
						       $html .= '<td>'.$loan_status_option[$row['loan_status']].'</td>';
						       $html .= '<td>'.$text.'</td>';
								$html .= '</tr>';
								
							}
						}
						
								$html .='<tfoot>
                                    <tr class="table_bottom">
                                        <th style="text-align:left"><strong>Total:'.$num.'</strong></th>
                                        <th></th>
                                        <th></th>
                                    
                                        <th><strong>'.'$'.number_format($loan_amount,2).'</strong></th>
                                        <th></th>
                                        <th></th>
                                          <th></th>
                                        <th><strong>'.'$'.number_format($total_payment,2).'</strong></th>
                                       
									    <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>';
                                        
                                    $html .='</tr>';
									$html .='<tr class="table_bottom">
                                        <th style="text-align:left"><strong>Average:</strong></th>
                                        <th></th>
                                        <th></th>
                                     
                                        <th><strong>'.'$'.number_format($loan_amount/$num,2).'</strong></th>
                                        <th></th>
                                           <th></th>
                                        <th></th>
                                        <th><strong>'.'$'.number_format($total_payment/$num,2).'</strong></th>
										<th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        
                                    </tr>
                                </tfoot>';
						
						$html .= '</table>';
						$pdf->WriteHTML($html);
						$pdf->Output("Active_trust_deeds.pdf","D");
		
		
	}

}

?>