<?php
Defined('BASEPATH') OR exit('No direct script access allowed');

Class Home extends CI_Controller{
	
	public function __construct(){
		
		parent:: __construct();
		
		$this->load->helper('url');
		$this->load->model('User_model');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
	}
	
	public function index(){

		//$this->load->view('login');
		$this->load->view('auth/login');

		//echo date('h:i:s a');
		
	}	


	/*public function testSignin(){		
		
		//$this->load->view('New_login/signin');			
		$this->load->view('auth/login');			
	}

	public function testforgotpassword(){		
		
		//$this->load->view('New_login/forgotpassword');			
		$this->load->view('auth/forgot');			
	}
	public function testSignup(){		
		
		//$this->load->view('New_login/signup');			
		$this->load->view('auth/register');			
	}*/

	//03-02-2021
	public function user_name_checker(){

		$username 		= $this->input->post('username');
		$portal_user_sql = "Select * from lender_contact_type where username = '".$username."'";
		$portal_user = $this->User_model->query($portal_user_sql);
		$portal_user = $portal_user->result();

		if(count($portal_user) > 0){
			echo "exist";
		}else{
			echo "not-exist";
		}
		die();
	}

	public function verified(){
		if($this->uri->segment(3)){
			$lender_user_id = $this->uri->segment(3);
			$portal_user_sql = "Select * from lender_contact_type where lender_contact_id = '".$lender_user_id."' AND status = 'active'";
			$portal_user = $this->User_model->query($portal_user_sql);
			if($portal_user->num_rows() > 0){
				redirect(base_url());
			}else{
				$portal_user = $this->User_model->query("UPDATE lender_contact_type SET status = 'active' WHERE lender_contact_id = '".$lender_user_id."'");
				$portal_userSql = "Select * from lender_contact_type where lender_contact_id = '".$lender_user_id."'";
				$portalUser = $this->User_model->query($portal_userSql);
				$portalUserData = $portalUser->row();
				$username = $portalUserData->username;
				$password = $portalUserData->password;
				$this->goto_dashboard_page($username,$password);
				redirect(base_url());
				//$this->load->view('auth/verified');
			}
		}else{
			redirect(base_url());
		}
	}

	public function send_user_email_verification($name, $email, $lid){		
		//$From: TaliMar Financial [invest@talimarfinancial.com]
		$main_content = 'Please verify youLender Account by following the link below. Should you have any questions, please reach out to Investor Relations.';
				
		$data = array();
		$data['from_email'] = 'invest@talimarfinancial.com';
		$data['from_name'] = 'TaliMar Financial';
		$data['to'] = $email;
		$data['name'] = $name;
		$data['subject'] = 'Verify new user account';
		$data['content_body'] = $main_content;
		$data['button_url'] = base_url().'Home/verified/'.$lid.'';
		$data['button_text'] = 'Verify Your Account';
		send_mail_template($data);

	    return true;
	}
	//03-02-2021


	
	public function register(){


		if(isset($_POST['submit'])){
		
			$fname 		= $this->input->post('fname');
			$lname 		= $this->input->post('lname');
			$phone 		= $this->input->post('phone');
			$email 		= $this->input->post('email');
			$username 	= $this->input->post('username');
			$password 	= $this->input->post('password');


			//03-02-2021
			$lender_contact_id = $this->input->post('lender_contact_id');
			if($lender_contact_id){
				$ref_base = $this->input->post('ref_base');
				$portal_user_sql = "Select * from lender_contact_type where lender_contact_id = '".$lender_contact_id."'";
				$portal_user = $this->User_model->query($portal_user_sql);
				if($portal_user->num_rows() > 0){
					$arrayQt = array();					
					$arrayQt['username'] 		= $username;
					$arrayQt['password'] 		= $password;
					$arrayQt['status']          = 'active';

					$where['lender_contact_id'] = $lender_contact_id;

					$this->User_model->updatedata('lender_contact_type',$where,$arrayQt);
					$this->goto_dashboard_page($username,$password);
					redirect(base_url());
				}else{
					$this->session->set_flashdata('error', 'Username address already registered.');
					redirect(base_url().'register/'.$ref_base);
				}
			}
			//03-02-2021
			if($fname && $email && $username && $password != ''){

				$dataconntact['contact_firstname']		=	$fname;
				$dataconntact['contact_lastname']		=	$lname;
				$dataconntact['contact_email']			=	$email;
				$dataconntact['contact_phone']			=	$phone;
				$dataconntact['user_id']			=	0;
				
				$select_User = $this->User_model->query("SELECT * FROM contact WHERE contact_email = '".$email."'");

				if($select_User->num_rows() > 0){
					
					$this->session->set_flashdata('error', 'E-mail address already registered.');
					redirect(base_url().'register');
					
				}else
				{
					
					$lender = $this->User_model->insertdataNew('contact', $dataconntact);

					if($lender)
					{

							$insert_id = $this->db->insert_id();



							$datalcon['contact_id'] 	= $insert_id;
							$datalcon['username'] 		= $username;
							$datalcon['password'] 		= $password;
							$datalcon['allow_access'] 	= '1';
							$datalcon['status'] 	    = 'inactive';

							$this->User_model->insertdata('lender_contact_type', $datalcon);
							$lid_id = $this->db->insert_id();

							$this->User_model->insertdata('contact_tags',array('contact_id'=>$insert_id,'contact_specialty'=>'16'));
							$this->User_model->insertdata('contact_tags',array('contact_id'=>$insert_id,'contact_email_blast'=>'2'));

							$this->send_user_email_verification($fname.' '.$lname, $email, $lid_id);

							//insert to user_added cobntact table...
							$data_user['date']  		= date('Y-m-d');
							$data_user['type'] 			= 'Contact';
							$data_user['status'] 		= 'Insert';
							$data_user['contact_id']    = $insert_id;
							$data_user['t_user_id']		= '11';
							$data_user['msg']			= 'Registered through Lender Portal on';

							$this->User_model->insertdata('user_added_contact',$data_user);

							//Send email to New account user...
							//$this->Send_new_account_email_to_user($fname,$email);

			                //Send email to admin/invester

			                
			                $main_content = '';
			                $main_content .= '<p style="font-size: 14px;">A new user has registered through the Lender Portal.';
							$main_content .= '<br>';
							$main_content .= '<p style="font-size: 14px;"><b>First Name:</b> '.$fname.'';
							$main_content .= '<br><b>Last Name:</b> '.$lname.'';
							$main_content .= '<br><b>Phone Number:</b> '.$phone.'';
							$main_content .= '<br><b> User Name:</b> '.$username.'';
							$main_content .= '<br><b>E-Mail:</b> '.$email.'';
							$main_content .= '<br><b>Specialty:</b> Trust Deed Lender';
							$main_content .= '<br><b>Media Contacts:</b> E-Mail Trust Investor, Mail – Trust Deed Investor, and Text Message – Trust Deed Investor';
							$main_content .= '<br><b>Portal:</b> Lender Portal';
							$main_content .= '<br>';
							$main_content .= '</p>';

			                $data = array();
							$data['from_email'] = 'invest@talimarfinancial.com';
							$data['from_name'] = 'TaliMar Financial';
							$data['to'] = 'invest@talimarfinancial.com';
							$data['name'] = '';
							$data['subject'] = 'New Lender Registration';
							$data['content_body'] = $main_content;
							$data['button_url'] = '';
							$data['button_text'] = '';
							send_mail_template($data);

							$this->session->set_flashdata('success', 'Registration successful, and send verification mail in your mail id');
							//redirect(base_url().'register');
							$this->load->view('auth/verified');
							//$this->goto_dashboard_page($username,$password);
							//redirect(base_url());
					}
					else{
						

							$this->session->set_flashdata('error', 'Something went wrong.');
							redirect(base_url().'register');
					}
				}
			}else{

				$this->session->set_flashdata('error', 'Please fill all required fields.');
				redirect(base_url().'register');
			}
		}else{
			$data = array();
			$data['lender_contact_id'] = '';
			$data['contact_email']     = '';
			$data['contact_firstname'] = '';
			$data['contact_lastname']  = '';
			$data['contact_phone']     = '';
			$data['ref_base']     	   = '';
			
			if($this->uri->segment(3)){
                $av_account_data 		   = $this->uri->segment(3);                        
				$dataAv 		   		   = explode('@@@@', base64_decode($av_account_data));
				$data['lender_contact_id'] = $dataAv[0];
				$data['contact_email']     = $dataAv[1];
				$data['contact_firstname'] = $dataAv[2];
				$data['contact_lastname']  = $dataAv[3];
				$data['contact_phone']     = $dataAv[4];
				$data['ref_base']          = $av_account_data;

				
				$portal_userSql = "Select * from lender_contact_type where lender_contact_id = '".$dataAv[0]."'";
				$portalUser = $this->User_model->query($portal_userSql);
				$portalUserData = $portalUser->row();
				if($portalUserData){
					$status = $portalUserData->status;
					if($status == 'active'){
						$this->session->set_flashdata('error', 'Already used this URL');
						redirect(base_url());
					}
				}
			}

			$this->load->view('auth/register', $data);
		}
		
	}

	public function goto_dashboard_page($user,$pass){

		$email 		= $user;
		$password 	= $pass;

		$portal_user_sql = "Select * from lender_contact_type where username = '".$email."' AND password ='".$password."'";
		$portal_user = $this->User_model->query($portal_user_sql);
		$portal_user = $portal_user->result();
				
		foreach($portal_user as $row){
							
			$user_id	 	= $row->contact_id;
			$user_name 		= $row->username;

			$fetch_contact = $this->User_model->query("SELECT contact_firstname, contact_lastname FROM contact WHERE contact_id = '".$user_id."'");
			$fetch_contact = $fetch_contact->result();
			$fname = $fetch_contact[0]->contact_firstname;
			$lname = $fetch_contact[0]->contact_lastname;
							
		}
						
		$login['p_user_id']	= $user_id;
		$login['status']	= 'Logged In';
		$login['date']		= date('Y-m-d');
		$login['time']		= date('h:i:s a');
		$login['timeout']	= '';
		
		$this->User_model->insertdata('portal_login_details', $login);
		$data['p_user_id'] = $this->session->set_userdata('p_user_id', $user_id);
		$this->session->set_userdata('p_user_fname' , $fname);
		$this->session->set_userdata('p_user_lname' , $lname);
		$this->session->set_userdata('user_name' , $user_name);
		
		redirect(base_url()."dashboard");
	}
	
	public function Send_new_account_email_to_user($fname,$emails){

			$reset_link = base_url();
			

            $main_content = '<p style="font-size: 14px;">';
			$main_content .= '<br>';
			$main_content .= '<br>Thank you for signing up for the TaliMar Financial Lender Portal. An account representative will be contacting you shortly to discuss your new account. You may also contact an account representative at (858) 613-0111 x3.';
			$main_content .= '<br>';
			$main_content .= '</p>';

            $data = array();
			$data['from_email'] = 'invest@talimarfinancial.com';
			$data['from_name'] = 'TaliMar Financial';
			$data['to'] = $emails;
			$data['name'] = $fname;
			$data['subject'] = 'New Lender Registration';
			$data['content_body'] = $main_content;
			$data['button_url'] = $reset_link;
			$data['button_text'] = 'Login Here';
			send_mail_template($data);

            return;

	}

	public function login(){
		
		if(isset($_POST['submit'])){
			
			$email 		= $this->input->post('email');
			$password 	= $this->input->post('password');

			//$portal_user_sql = "Select * from lender_contact_type where username = '".$email."' AND password ='".$password."' AND lender_id !=''";

			//echo $stremail = getRequestString($email); echo '<br>';
			$portal_user_sql = "Select * from lender_contact_type where username = '".$email."' AND (password ='".$password."' OR temp_password ='".$password."') AND status = 'active'";
			
			//echo $portal_user_sql; die('stop working');
			
			$portal_user = $this->User_model->query($portal_user_sql);
			if($portal_user->num_rows() > 0){
				
				$portal_user = $portal_user->result();
				
				if($portal_user[0]->allow_access == '1'){

					/*$fetch_any_lender = $this->User_model->query("SELECT * FROM `lender_contact` WHERE `contact_id`= '".$portal_user[0]->contact_id."'");
					if($fetch_any_lender->num_rows() > 0){ */
					
						foreach($portal_user as $row){
							
							$user_id	 	= $row->contact_id;
							$user_name 		= $row->username;


							$fetch_contact = $this->User_model->query("SELECT contact_firstname, contact_lastname FROM contact WHERE contact_id = '".$user_id."'");
							$fetch_contact = $fetch_contact->result();
							$fname = $fetch_contact[0]->contact_firstname;
							$lname = $fetch_contact[0]->contact_lastname;
											
						}
						
						$login['p_user_id']	= $user_id;
						$login['status']	= 'Logged In';
						$login['date']		= date('Y-m-d');
						$login['time']		= date('h:i:s a');
						$login['timeout']	= '';
						
						$this->User_model->insertdata('portal_login_details', $login);


						$CheckL = $this->User_model->query("Select * from lender_contact_type where username = '".$email."' AND temp_password ='".$password."' AND status = 'active'");
						if($CheckL->num_rows() > 0){
							$CheckData  = $CheckL->row();
							$contact_id = $CheckData->lender_contact_id;
							$Username   = $email;
							$reset_link = base_url().'Home/createPassword/'.$contact_id.'/'.$Username.'/'.$this->generate_pwd(15);
							
							redirect($reset_link);
						}

						$data['p_user_id'] = $this->session->set_userdata('p_user_id', $user_id);
						$this->session->set_userdata('p_user_fname' , $fname);
						$this->session->set_userdata('p_user_lname' , $lname);
						$this->session->set_userdata('user_name' , $user_name);
						

						redirect(base_url()."dashboard");

					/* }else{

						$this->session->set_flashdata('error', 'Thank you for your interest in this trust deed. It does not appear you have an active account with TaliMar Financial. Please contact an account representative at (858) 613-0111 x4 to activate your account.');
						redirect(base_url()."error_page");
					} */
					
				}else{
					
					$this->session->set_flashdata('error', 'No access allowed yet.');
					//$this->load->view('login');
					$this->load->view('auth/login');
				}
				
			}else{
				
				
				$this->session->set_flashdata('error', 'Unable to log in with provided credentials.');
				//$this->load->view('login');
				$this->load->view('auth/login');
			}
			
		}else{
		
			//$this->load->view('login');
			$this->load->view('auth/login');
		}
		
	}

	
	
	public function error_page(){

		$this->load->view('error404');
	}

	

	public function addImage(){

		$filename = $_FILES['file']['name'];
		if($filename != ''){

			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			if($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPEG'){

				$path = 'profile_image/'.$filename; //FCPATH
				//echo '<br>';
				//if(move_uploaded_file($_FILES["file"]["tmp_name"],$path)){
				$attachmentName=$this->aws3->uploadFile($path,$_FILES["file"]["tmp_name"]);
				if($attachmentName){

					$datas['imgurl'] = aws_s3_document_url('profile_image/'.$filename);
					$where['contact_id'] = $this->session->userdata('p_user_id');

					$this->User_model->updatedata('lender_contact_type',$where,$datas);

					$this->session->set_flashdata('success', 'Image uploaded successfully.');
					redirect(base_url().'view_details');
				}else{

					$this->session->set_flashdata('error', 'Error! image not uploaded.');
					redirect(base_url().'view_details');
				}

			}else{

				$this->session->set_flashdata('error', 'Upload only png, jpg & jpeg files.');
				redirect(base_url().'view_details');
			}

		}else{

			$this->session->set_flashdata('error', 'Please choose a file for upload.');
			redirect(base_url().'view_details');
		}
	}


	public function dashboard(){

		if($this->session->userdata('p_user_id') == ''){
			redirect(base_url());
		}
		
		error_reporting(0);
		$query = "SELECT * FROM lender_contact join investor on lender_contact.lender_id = investor.id  where lender_contact.contact_id = '".$this->session->userdata('p_user_id')."' AND lender_contact.link_to_lender = '2'";
		
		$fetch_account_info = $this->User_model->query($query);
		$fetch_account_info = $fetch_account_info->result();
	
	
		foreach($fetch_account_info as $row)
		{
			
			$lender_id=$row->id;
			$account_name=$row->name;
			$account_no=$row->account_number;
			$talimar_lender=$row->talimar_lender;
			$account_status=$row->account_status;
			$fci_acct=$row->fci_acct;

				
			$sql_assignn = "SELECT * FROM loan_assigment as la JOIN loan_servicing as ls ON la.talimar_loan = ls.talimar_loan WHERE la.lender_name ='".$lender_id."' AND ls.loan_status = '2'";
				
			$fetch_assignment_data 			= $this->User_model->query($sql_assignn);	
			$fetch_assignment_data 			= $fetch_assignment_data->result();
			
			$count = 0;
			$active_investment = 0;
			$avg_yield = 0;
			$lender_payment = 0;
	        foreach($fetch_assignment_data as $row){
				
				$talimar_loan['talimar_loan']=$row->talimar_loan;
				// echo'<pre>';
				// print_r($row);
				// echo'</pre>';
				 
				 $count++;
				 $active_investment += $row->investment;
				 $avg_yield =$avg_yield + $row->invester_yield_percent;
				 $lender_payment += $row->payment;

				
				// $fetch_impound_account = $this->User_model->select_where_asc('impound_account',$talimar_loan);
				// if($fetch_impound_account->num_rows() > 0)
				// {
				// 	$fetch_impound_account = $fetch_impound_account->result();
				// $impound_amount[] = $fetch_impound_account[0]->amount;
				// $impound_amount[] =0.00;
				// }	
					
				// $aa = 0;
		
				// foreach($impound_amount as $row){
				// $aa+=$row;

				// }			 
		
			}	

			$dashboard_data[]  =    array(	
			                                'account_name' 	=> $account_name,
											'account_no' 	=> $account_no,
											'talimar_lender'=> $talimar_lender,
											'id' 			=> $lender_id,
											'active' 		=> 'Active',
											'monthly' 	    => $aa,
											'total_count' 	=> $count,
											'active_bal' 	=> $active_investment,
											'avg_yieldd' 	=> $avg_yield,
											'lender_payment' => $lender_payment,
											'account_status' => $account_status,
											'fci_acct' 		=> $fci_acct,
										  ); 
		}
		
		if($dashboard_data){
		
			$data['dashboard_data'] = $dashboard_data;
		
		}else{
			
			$data['dashboard_data'] = '1';
		}
		
		$data['content'] = $this->load->view('dashboard',$data,true);
		$this->load->view('template_files/template',$data);
				
	}
	
	public function logout(){
		
		$add_login['p_user_id'] = $this->session->userdata('p_user_id');
		$add_login['status'] 	= 'Logged Out';
		$add_login['date'] 		= date('Y-m-d');
		$add_login['time'] 		= '';
		$add_login['timeout']	= date('h:i:s a');
				
		$this->User_model->insertdata('portal_login_details',$add_login);
		
		$this->session->unset_userdata('p_user_id');		
		$this->session->unset_userdata('p_user_fname');			
		
		redirect(base_url());
		
	}
	
	public function view_profile(){
		
		if(isset($_POST['submit'])){
			
			$where['id']				= $this->input->post('id');
			
			$data['fname'] 				= $this->input->post('fname');
			$data['lname'] 				= $this->input->post('lname');
			$data['phone'] 				= $this->input->post('phone');
			$data['email'] 				= $this->input->post('email');
			$data['street_address'] 	= $this->input->post('street_address');
			$data['unit'] 				= $this->input->post('unit');
			$data['city']			 	= $this->input->post('city');
			$data['state']			 	= $this->input->post('state');
			$data['zip']			 	= $this->input->post('zip');
			//$data['profile_name']		= $this->input->post('profile_name');
			
			$this->User_model->updatedata('portal_users',$where,$data);
			$this->session->set_flashdata('success','Profile Updated Successfully!');
			redirect(base_url()."view_profile");
			
			
		}else{
			
			$select_user = $this->User_model->query("Select * from portal_users where id= '".$this->session->userdata('p_user_id')."'");
			if($select_user->num_rows() > 0){
				$select_user = $select_user->result();
					
				$data['portal_user'] = $select_user;
				$data['content'] = $this->load->view('view_profile',$data,true);
				$this->load->view('template_files/template',$data);
			}
		}
	}
	
	public function view_personal_information(){
		
	$select_user = $this->User_model->query("Select * from contact  where contact_id= '".$this->session->userdata('p_user_id')."'");
			if($select_user->num_rows() > 0){
				$select_user = $select_user->result();
					
				$data['fetch_contact_data'] = $select_user;
				$data['contact_id'] = $select_user[0]->contact_id;
				$data['content'] = $this->load->view('personal_info',$data,true);
				$this->load->view('template_files/template',$data);
	
			}
	}

	public function generate_pwd($length) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return substr(str_shuffle($chars), 0, $length);
    }

	public function resetPassword(){

		if(isset($_POST['email'])){

			$email_address = $this->input->post('emailreset');
			$fetch_email = $this->User_model->query("Select * from contact where contact_email = '".$email_address."'");

				if($fetch_email->num_rows() > 0){
					
					$fetch_email = $fetch_email->result();

					$contact_id  = $fetch_email[0]->contact_id;
					//$name 		 = $fetch_email[0]->contact_firstname.' '.$fetch_email[0]->contact_lastname;
					$name 		 = $fetch_email[0]->contact_firstname;

					$getusername = $this->User_model->query("Select username from lender_contact_type where contact_id = '".$contact_id."'");
					$getusername = $getusername->result();
					$Username = $getusername[0]->username;
					
					$Passwordcode   = $this->generate_pwd(10);

					//$Updated_password = $this->User_model->query("Update lender_contact_type set password = '".$Passwordcode."', c_password = '".$Passwordcode."' where contact_id = '".$contact_id ."'");

					$reset_link = base_url().'Home/createPassword/'.$contact_id.'/'.$Username.'/'.$this->generate_pwd(15);
					

	                $main_content  = '';	                
					$main_content .= '<p style="font-size: 14px;">Username : '.$Username.'</p>';
					$main_content .= '<p style="font-size: 14px;">You may reset your Password by following the link below. Should you have any questions, please reach out to Investor Relations.</p>';
					$main_content .= '<br>';					

	                $data = array();
					$data['from_email'] = 'invest@talimarfinancial.com';
					$data['from_name'] = 'TaliMar Financial';
					$data['to'] = $email_address;
					$data['name'] = $name;
					$data['subject'] = 'Password Reset';
					$data['content_body'] = $main_content;
					$data['button_url'] = $reset_link;
					$data['button_text'] = 'Create Password';
					send_mail_template($data);

					$this->session->set_flashdata('success','You will receive an email with a link to reset your password.');
					redirect(base_url().'resetPassword');
				}
				else
				{
					$this->session->set_flashdata('error','Plese enter a valid E-mail address.');
					redirect(base_url().'resetPassword');
				}
		}else{

			$this->load->view('auth/forgot');
		}
	
	}

	public function createPassword(){

		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		$uri5 = $this->uri->segment(5);

		if($uri3 && $uri4 && $uri5 != ''){

			$data['id'] = $uri3;
			$data['username'] = $uri4;
			$data['uri5'] = $uri5;
			$this->load->view('auth/create_password',$data);

		}else{

			echo "<h2 style='text-align:center;color:red;'>Error on the Create Password link.</h2>"; 
		}
	}


	public function CreatePasswordData(){


		$id 			= $this->input->post('id');
		$username 		= $this->input->post('username');
		$password 		= $this->input->post('password');
		$c_password 	= $this->input->post('c_password');
		$randomcode		= $this->input->post('uri5');
		//$randomcode   	= $this->generate_pwd(15);

		if($password == $c_password){

			$where['contact_id'] = $id;
			$where['username'] 	 = $username;

			$getcorrect = $this->User_model->select_where('lender_contact_type',$where);
			if($getcorrect->num_rows() > 0){

				$getupdate = $this->User_model->query("Update lender_contact_type set password = '".$password."', c_password = '".$c_password."', temp_password = '' where contact_id = '".$id."' AND username = '".$username."'");

				$this->session->set_flashdata('success','Password updated.');
				redirect(base_url());

			}else{

				$this->session->set_flashdata('error','Something is wrong. password not updated.');
				redirect(base_url().'Home/createPassword/'.$id.'/'.$username.'/'.$randomcode);
			}

		}else{

			//die('ddd');

			$this->session->set_flashdata('error','Password not match.');
			redirect(base_url().'Home/createPassword/'.$id.'/'.$username.'/'.$randomcode);
		}

	}




	public function forget_password(){
		
		$email 		= $this->input->post('email');
		$password 	= $this->input->post('password');
		$c_password = $this->input->post('c_password');
		
		$fetch_email = $this->User_model->query("Select * from contact where contact_email = '".$email."'");

			if($fetch_email->num_rows() > 0){
				
				$fetch_email = $fetch_email->result();

				$contact_id  = $fetch_email[0]->contact_id;
				$name 		 = $fetch_email[0]->contact_firstname.' '.$fetch_email[0]->contact_lastname;
				
				if($password == $c_password)
				{

					$Updated_password = $this->User_model->query("Update lender_contact_type set password = '".$password."', c_password = '".$c_password."' where contact_id = '".$contact_id ."'");
					
					$subject	= "Talimar Financial - Forget Password"; 
					$header		= "From: mail@talimarfinancial.com"; 
					$message	= '<h2><center></center></h2><br>
					Dear <h3>'.$name.',</h3><br>
					Your password successfully changed!'; 
					
					$data = array();
					$data['from_email'] = 'mail@talimarfinancial.com';
					$data['from_name'] = $subject;
					$data['to'] = $email;
					$data['name'] = $name;
					$data['subject'] = $subject;
					$data['content_body'] = $message;
					$data['button_url'] = '';
					$data['button_text'] = '';
					send_mail_template($data);


					$this->session->set_flashdata('success','Password updated!');
					redirect(base_url());
					
				}else
				{
					$this->session->set_flashdata('error','Password Not Match!');
					redirect(base_url());
				}
			}
			else
			{
				$this->session->set_flashdata('error','Plese Enter a Valid E-mail!');
				redirect(base_url());
			}
		
	}


	public function addNewAccount(){
		

		$data['newaccount'] = '';
		$data['content'] = $this->load->view('new-account',$data,true);
		$this->load->view('template_files/template',$data);

	}
	
	
	
}

?>