<?php
$entity_type_option 				= $this->config->item('entity_type_option');
$investor_type_option 			= $this->config->item('investor_type_option');
$STATE_USA 						= $this->config->item('STATE_USA');
$yes_no_option3 				= $this->config->item('yes_no_option3');
$user_access_type 				= $this->config->item('user_access_type');
$dishbrushment_option 			= $this->config->item('dishbrushment_option');

?>

<style>
h4.deed_heading{
	background-color:#ececec;
	padding:12px;
}	
h4.a {
    margin-left: 15px;
}
label.ach_l{
    margin-left: 10px;
}
</style> <form method="post" action="<?php echo base_url();?>add_account_modal">
		<div class="container">
	<!--------------------MESSEGE SHOW-------------------------->
				<?php if($this->session->flashdata('error')!=''){  ?>
				 <div id='error' class="alert alert-danger"><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error');?></div><?php } ?>
				  <?php if($this->session->flashdata('success')!=''){  ?>
				 <div id='success'><i class='fa fa-thumbs-o-up'></i><?php echo $this->session->flashdata('success');?></div><?php } ?>
				 
			<!-------------------END OF MESSEGE-------------------------->
		 
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<h3 Style="font-weight:400;">Add Account<span Style="font-weight:300;"></span></h3>					
				</div>
				 <form method="post" action="<?php echo base_url();?>add_account_modal">
				<div class="col-md-6 col-sm-6 col-xs-12">
					
					<button type="submit"  name="submit" class="btn btn-primary pull-right"  style="margin-right: 5px;">Save</button> 
					
					<button type="reset" class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModalfour" style="margin-right: 5px;">Close</button>
					
					<a href="<?php echo base_url();?>add_edit_account"><button  type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModalfour" style="margin-right: 5px;">New</button></a>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-12">
					<?php if($this->session->flashdata('success')!=''){ ?>
					<div class="alert alert-success alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="notika-icon notika-close"></i></span></button> <?php echo $this->session->flashdata('success');?>
					</div>
					<?php } ?>
					
					<?php if($this->session->flashdata('msg')!=''){ ?>		 
						<div class="alert alert-success alert-dismissible" role="alert">					
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('msg');?>	 
						</div>	
					<?php } ?>
				</div>
			</div>
			
			<div class="row">&nbsp;</div>
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="form-group">
                        <div class="form-single nk-int-st widget-form">
                        	<select type="text" class="form-control" name = "investor_type" id="investor_type"  />
								<?php
								foreach($investor_type_option as $key => $row)
								{
									?>
									<option value="<?= $key;?>" <?php if(isset($fetch_investor_data['investor_type'])){ if($fetch_investor_data['investor_type'] == $key){ echo 'selected'; }} ?> ><?= $row;?></option>
									<?php
								}
								?>
								</select>
						</div>
					</div>
				</div>
					<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="form-group">
                        <div class="form-single nk-int-st widget-form">
                          <input type="text" class="form-control" name = "talimar_lender"  placeholder="TaliMar Lender # "value =""/>
						</div>
					</div>
				</div>
					
				
			</div>
			
			
			
		<div class="trust">	
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
                        <div class="form-single nk-int-st widget-form">
							<input type="text" id="investor_name" class="form-control" placeholder="Lender Name" name="investor_name" value ="">
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
                        <div class="form-single nk-int-st widget-form">
							<input type="text" name="tax_id" class="form-control" placeholder="Lender Tax ID#">
						</div>
					</div>
				</div>
			</div>
			
		
			
			</div>
		
	
		
			<div class="row borrower_data_row" id="investor_type_based">
				<div class="col-md-3">
					<div class="form-group">
                        <div class="form-single nk-int-st widget-form">
							 	<input type="text" class="form-control " name = "entity_name" placeholder="Entity Name" value ="" >
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
                        <div class="form-single nk-int-st widget-form">
							 	<select type="text" class="form-control" name ="entity_type" >
							<?php
							foreach($entity_type_option as $key => $row)
							{
								?>
								
								<option value="<?php echo $key; ?>" <?php if(isset($fetch_investor_data['entity_type'])){ if($fetch_investor_data['entity_type'] == $key){ echo 'selected'; } } ?> ><?php echo $row; ?></option>
								<?php
							}
							?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
                        <div class="form-single nk-int-st widget-form">
						<select type="text" class="form-control" name = "entity_res_state" >
							<?php
							
							foreach($STATE_USA as $key => $row)
							{
								?>
								<option value="<?= $key;?>" <?php if(isset($fetch_investor_data['entity_res_state'])){ if($fetch_investor_data['entity_res_state'] == $key){ echo 'selected'; }} ?>><?= $row;?></option>
								<?php
							}
							?>
							</select>
						</div>
					</div>
				</div>
			</div>
			
		
		
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
                        <div class="form-single nk-int-st widget-form">
							<input type="text" class="form-control" name="state_entity_no" placeholder="State Entity Number" value ="" >
						</div>
					</div>
				</div>
				
			</div>
		
		
			<div class="row borrower_data_row" id="investor_type_based">
				<div class="col-md-3">
					<div class="form-group">
                        <div class="form-single nk-int-st widget-form">
							<input type="text" class="form-control " id="directed_IRA_NAME" placeholder="Self Directed IRA Name" name = "directed_IRA_NAME" value ="<?= isset($fetch_investor_data['directed_IRA_NAME']) ? $fetch_investor_data['directed_IRA_NAME'] : '';?>">
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
                        <div class="form-single nk-int-st widget-form">
							<input type="text" class="form-control " id="custodian" placeholder="Custodian" name = "custodian" value ="<?= isset($fetch_investor_data['custodian']) ? $fetch_investor_data['custodian'] : '';?>">
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
                        <div class="form-single nk-int-st widget-form">
						<input type="text" class="form-control " placeholder="Account #" id="self_dir_account" name ="self_dir_account" value ="<?= isset($fetch_investor_data['self_dir_account']) ? $fetch_investor_data['self_dir_account'] : '';?>">
						</div>
					</div>
				</div>
			</div>
		
		
	  
		
		
					<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<div class="form-single nk-int-st widget-form">
								<input type="text" name="address" class="form-control" placeholder="Street Address">
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<div class="form-single nk-int-st widget-form">
								<input type="text" name="unit" class="form-control" placeholder="Unit #">
							</div>
						</div>
					</div>
				</div>
			
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<div class="form-single nk-int-st widget-form">
							<input type="text" name="city" class="form-control" placeholder="City">
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<div class="form-single nk-int-st widget-form">
								 <select class="form-control" name="state">
							<?php foreach($STATE_USA as $key => $row){ ?>
								<option value="<?php echo $key;?>"><?php echo $row;?></option>
							<?php } ?>
							</select>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<div class="form-single nk-int-st widget-form">
							<input type="text" name="zip" class="form-control" placeholder="Zip">
							</div>
						</div>
					</div>
				</div>
			
				<div class="row" id="lender_vestingg">
				<div class="col-md-9">
					<div class="form-group">
                        <div class="form-single nk-int-st widget-form">
							<input type="text" name="vesting" class="form-control" placeholder="Vesting">
						</div>
					</div>
				</div>
			</div>
				<div class="row borrower_data_row border-top">
						<!--<h2>Primary Contact Information</h2>-->
						<h4 class="a">Loan Servicing Information:</h4>
					</div><br>
			<div class="row">
                <div class="col-md-4">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<label>Active Account w/ FCI Lender Services</label>
																		
							<select class="form-control" name="fci_acct_service" id="fci_type" onchange="hide_fci_lender(this.value);">
								<option  value = "">Select One</option>
								<option <?php if(isset($fetch_investor_data['fci_acct_service'])){if($fetch_investor_data['fci_acct_service'] == '1'){echo 'selected';}}?> value = "1">No</option>
								<option <?php if(isset($fetch_investor_data['fci_acct_service'])){if($fetch_investor_data['fci_acct_service'] == '2'){echo 'selected';}}?> value = "2">Yes</option>
										
							</select>	
							</div>
							</div>
							</div>
			   <div class="col-md-4">
							
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">	
							<label>FCI Account #</label>								
					<input type="text" class="form-control contact_title" placeholder="Fci Account #"  id="fci_acct" name ="fci_acct" value ="<?php echo isset($fetch_investor_data['fci_acct']) ? $fetch_investor_data['fci_acct'] : '';?>" >
					
					
					</div>
					</div>
					</div>
					</div>
						<div class="row">
                <div class="col-md-4">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
								<label>Active Account w/ Del Toro Loan Services</label>
																		
						<select class="form-control" name="del_acct_service" id="del_toro_type" onchange="hide_del_toro_lender(this.value);">
								<option  value = "">Select One</option>
								<option <?php if(isset($fetch_investor_data['del_acct_service'])){if($fetch_investor_data['del_acct_service'] == '1'){echo 'selected';}}?> value = "1">No</option>
								<option <?php if(isset($fetch_investor_data['del_acct_service'])){if($fetch_investor_data['del_acct_service'] == '2'){echo 'selected';}}
								else{ echo 'selected';} ?> value = "2">Yes</option>
							</select>	
							</div>
							</div>
							</div>
			   <div class="col-md-4">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">	
							<label>Del Toro Account #</label>								
						
					<input type="text" class="form-control contact_title" id="del_toro_acct" placeholder="Del Toro Account #" name = "del_toro_acct">
					</div>
					</div>
					</div>
					</div>
					
					
			<div class="row borrower_data_row border-top">
							<h4 class="a">Lender Disbursement Information:</h4>
						</div><br>

			<div class="row" class="sign_box">
                <div class="col-md-3">
					<div class="form-group">
			
				 <label class="loan_data_lable" for="textinput">Disbursement Type:</label>		
				<select class="form-control" name="ach_and_check_drop" id="ach_and_check" onchange="show_hide_selectbox(this.value);">
				
						<option value ="1">ACH</option>
						<option value ="2">Check</option>
							
				</select>	
						
					</div>
				</div>
		
			</div>	
						
		<!-- 	<div class="row" class="sign_box">
                <div class="col-md-6">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
								<input type="checkbox" class="" id="ach_disbrusement" onclick="ach_disbrusement_value(this);" ><label class="ach_l">ACH Disbursement</label>
							<input type="hidden" name="ach_disbrusement" id="ach_disbrusement_val" value="">
						</div>
					</div>
				</div>
			
				
			</div> -->
	
		<div id="bank_details" style="display:none;">
			
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<div class="form-single nk-int-st widget-form">
								<input type="text" name="bank_name" class="form-control" placeholder="Bank Name">
							</div>
						</div>
					</div>
					
					<div class="col-md-3">
						<div class="form-group">
							<div class="form-single nk-int-st widget-form">
								<input type="text" name="account_number" class="form-control" placeholder="Account Number">
							</div>
						</div>
					</div>
				
					<div class="col-md-3">
						<div class="form-group">
							<div class="form-single nk-int-st widget-form">
															
									<input type="text" class="form-control" name ="routing_number" placeholder="Routing Number" value ="">							
							</div>
						</div>
					</div>
				
			</div>
				</div>
			
			
			
			
		<!-- 	
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							
						<input type="checkbox" class="" id="check_disbrusement" onclick="check_disbrusement_value(this);"><label class="ach_l">Check Disbursement</label>
							<input type="hidden" name="check_disbrusement" id="check_disbrusement_val" value="">
						</div>
					</div>
				</div> -->


				<div id="check_hide_div" style="display:none;">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<div class="form-single nk-int-st widget-form">
								<input type="text" name="mail_address" class="form-control" placeholder="Mailing Street Address">
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<div class="form-single nk-int-st widget-form">
								<input type="text" name="mail_unit" class="form-control" placeholder="Unit #">
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<div class="form-single nk-int-st widget-form">
								<input type="text" name="mail_city" class="form-control" placeholder="City">
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<div class="form-single nk-int-st widget-form">
								<select class="form-control" name="mail_state">
								<?php foreach($STATE_USA as $key => $row){ ?>
									<option value="<?php echo $key;?>"><?php echo $row;?></option>
								<?php } ?>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<div class="form-single nk-int-st widget-form">
								<input type="text" name="mail_zip" class="form-control" placeholder="Zip">
							</div>
						</div>
					</div>
				</div>
			
			
			
		</div>
		</div>
		
		</div>
		</form>
<script>

//...........................................Add user start.........................................................//


// function add_more_user(that){
		
	 // $('#main_add').append('<div class="row"><div class="col-md-3"><div class="form-group"><div class="form-single nk-int-st widget-form"><input type="text" name="first_name[]" class="form-control" placeholder="First Name"></div></div></div><div class="col-md-3"><div class="form-group"><div class="form-single nk-int-st widget-form"><input type="text" name="last_name[]" class="form-control" placeholder="Last Name"></div></div></div><div class="col-md-3"><div class="form-group"><div class="form-single nk-int-st widget-form"><input type="email" name="email[]" class="form-control" placeholder="E-mail"></div></div></div><div class="col-md-3"><div class="form-group"><div class="form-single nk-int-st widget-form"><select class="form-control" name="access_type[]"><?php foreach($user_access_type as $key => $row){ ?><option value="<?php echo $key;?>"><?php echo $row;?></option><?php } ?></select></div></div></div></div>');	

	
	
 // }

//...........................................Add user end........................................................//

//...........................................conditions for hide and show start..............................//


// function add_account_conditions(tha){

 // var value1=$(tha).val();		
 	

  // if(value1=='1'){
	// $('.trust').css('display','none'); 
	// $('.llc_corp').css('display','none'); 
	// $('#trust_part').css('display','none');
	// $('#lender_vestingg').css('display','block');
	// requ_sign();
	// change_dishbrushment_option();
	  // $('#main_block_hide').css('display','block');
	  // $('#main_buy_direction').css('display','none');
	  // $('#Contact_TaliMar').css('display','none');
	  // $('#selff').css('display','none');
  // }
 
 // else if(value1=='3'){
	
	// $('.llc_corp').css('display','none');  
	// $('.trust').css('display','block');
	// $('#lender_vestingg').css('display','none');
	// $('#title').css('display','none');
	// $('#trust_part').css('display','block');
	// requ_sign();
	// change_dishbrushment_option();
	  // $('#main_block_hide').css('display','block');
	  // $('#main_buy_direction').css('display','none');
	  // $('#Contact_TaliMar').css('display','none');
	  // $('#selff').css('display','none');
	  
      // }
 
  // else if(value1=='2'){
	// $('.llc_corp').css('display','block'); 
	// $('.trust').css('display','none');  
	// $('#trust_part').css('display','none');
	// requ_sign();
	// change_dishbrushment_option();
	 // $('#main_block_hide').css('display','block');
	// $('#lender_vestingg').css('display','block');
	// $('#main_buy_direction').css('display','none');
	 // $('#Contact_TaliMar').css('display','none');
	 // $('#selff').css('display','none');
      // }
   
   // else if(value1=='4'){

	 // $('#main_block_hide').css('display','none');  
	 // $('#selff').css('display','block');
	 
   // selff();

	  
      // }
  
  // else {
	// $('.check').css('display','block');
	// $('#trust_part').css('display','block');
	// $('#bank_details').css('display','block');
	// $('#required_sign').css('display','block'); 
	// $('.trust').css('display','block'); 
	// $('.llc_corp').css('display','block'); 
	// $('#title').css('display','block'); 
	// $('#selff').css('display','block'); 
	// $('#main_buy_direction').css('display','block');
	// $('#Contact_TaliMar').css('display','block'); 
	  // }
 
 // }

 // function change_dishbrushment_option(th){

  // var value2=$(th).val();		
 	
  // if(value2=='1'){
	  
  // $('#bank_details').css('display','block');

   // }else{
    // $('#bank_details').css('display','none');   
	  
    // }
 
  // if(value2=='2'){
	// $('.check').css('display','block');
 
	// }else{
	// $('.check').css('display','none'); 
	// }
 
 // }
 
  // function requ_sign(t){

 // var value3=$(t).val();		
 	
  // if(value3=='1'){
	  
// $('#required_sign').css('display','block');

  // }else{
	// $('#required_sign').css('display','none');   
	  
  // }
 
 function show_hide_selectbox(vall){

  var value2=vall;
 
  if(value2=='1'){
	  
  $('#bank_details').css('display','block');
  $('#check_hide_div').css('display','none'); 
   }
 
  else if(value2=='2'){

	$('#check_hide_div').css('display','block');
 	$('#bank_details').css('display','none'); 
	}
 
 }

 
 // }
 
 // function selff(se){
	
 // var Buy_Direction=$(se).val();		
//alert(Buy_Direction);
  // if(Buy_Direction=='1'){
	  
// $('#main_buy_direction').css('display','block');

  // } else{
	// $('#main_buy_direction').css('display','none');   

	  
  // }
  
   // if(Buy_Direction=='2'){
	  
// $('#Contact_TaliMar').css('display','block');

  // }
  // else{

	// $('#Contact_TaliMar').css('display','none');   
	  
  // }
	
 // }
 
 
 
//...........................................conditions for hide and show end..............................//

function ach_disbrusement_value(that){
	
	if($(that).is(':checked')){
		$('input#ach_disbrusement_val').val('1');
		$('input#check_disbrusement').attr('disabled',true);
		$('input#check_disbrusement').val('0');
	}else{
		$('input#check_disbrusement').attr('disabled',false);
		$('input#ach_disbrusement_val').val('0');
	}
}

function check_disbrusement_value(that){
	
	if($(that).is(':checked')){
		$('input#check_disbrusement_val').val('1');
		$('input#ach_disbrusement').attr('disabled',true);
		$('input#ach_disbrusement').val('0');
	}else{
		$('input#ach_disbrusement').attr('disabled',false);
		$('input#check_disbrusement_val').val('0');
	}
}

</script>