<?php
$investor_type_option 			= $this->config->item('investor_type_option');
$STATE_USA 						= $this->config->item('STATE_USA');
$yes_no_option3 				= $this->config->item('yes_no_option3');


// echo'<pre>';
// print_r($fetch_account);
// echo'</pre>';

?>		
<style type="text/css">
	h4.deed_heading {
	    background-color: #ececec;
	    padding: 12px;
	}
</style>
<div class="content-wrapper">
<section class="content">
<div class="data-table-area">
        <div class="container">
            <div class="row">
			
			<?php if($this->session->flashdata('success')!=''){ ?>
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="notika-icon notika-close"></i></span></button> <?php echo $this->session->flashdata('success');?>
                </div>
			<?php } ?>
	
			<?php if($this->session->flashdata('msg')!=''){ ?>		 
				<div class="alert alert-success alert-dismissible" role="alert">					
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('msg');?>	 
				</div>	
			<?php } ?>
			<div class="col-xs-12">
				<!--<h4 class="deed_heading">>Account Settings</h4> -->
					<h3>Account Settings</h3>
                
            </div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                
					
					<table id="data-table-basicss" class="table table-striped table-responsive">
					
						<thead style="background-color: #f2f2f2;">
							<tr>
								<th>Account Name</th>
								<th>Lender Type</th>
								<th>Lender ID</th>
								<th>FCI Account #</th>
								<th>Address</th>
								<th>ACH</th>
								<th>View</th>
							   
							</tr>
						</thead>
						<tbody>
						<?php foreach($fetch_account as $row){ 

							?>

							<tr>
								<td><?php echo $row->name;?></td>
								<td><?php echo $investor_type_option[$row->investor_type];?></td>
								<td><?php echo $row->talimar_lender;?></td>
								<td><?php echo $row->fci_acct;?></td>
								<td><?php echo $row->address.' '.$row->unit.'; '.$row->city.', '.$row->state.' '.$row->zip;?></td>
								<td>
								<?php 
								if($row->ach_disbrusement==1){
								
								echo "Yes";
								
								}
								else{
								
								echo "No";
								 }
								?>
								</td>
							
								
								<td>
									<a href="<?php echo base_url().'view_edit_account/'.$row->id;?>" title="View" class="btn btn-primary btn-xs">View</a>
								</td>
									
							</tr>
						<?php } ?>
					
						</tbody>
						
						
						
					</table>
                
            </div>
            </div>
        </div>
    </div>
	</section>
	</div>
	
<!-- add account modal in button click----------------------------------------------------------------------------->

 <div class="modal fade" id="myModalthree" role="dialog">
 <form method="post" action="<?php echo base_url();?>add_account_modal">
    <div class="modal-dialog modal-large">
     <div class="modal-content">
			<div class="modal-header">
				<!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
			</div>
     <div class="modal-body">
	<div class="container">
            <div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<h3 Style="font-weight:400;">Add Account:</h3>					
				</div>
			</div>
			<div class="row">&nbsp;</div>
			
			<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
                        <div class="form-single nk-int-st widget-form">
                            <select class="form-control" name="lender_type">
							<?php foreach($investor_type_option as $key => $row){ ?>
								<option value="<?php echo $key;?>"><?php echo $row;?></option>
							<?php } ?>
							</select>
						</div>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
			</div>
			<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input type="text" name="lender_name" class="form-control" placeholder="Lender Name">
						</div>
					</div>
				</div>
				
			 <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<!--<input type="text" name="m_street_address" class="form-control" placeholder="Mailing Street Address">-->
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				</div>
			</div>
			<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input type="text" name="lender_texid" class="form-control" placeholder="Lender Tex ID #">
						</div>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
			</div>
			<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input type="text" name="lender_vesting" class="form-control" placeholder="Lender Vesting">
						</div>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				</div>
				
			</div>
			<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input type="text" name="m_street_address" class="form-control" placeholder="Mailing Street Address">
						</div>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input type="text" name="m_unit" class="form-control" placeholder="Unit #">
						</div>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
			</div>
			<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input type="text" name="m_city" class="form-control" placeholder="City">
						</div>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
                            <select class="form-control" name="m_state">
							<?php foreach($STATE_USA as $key => $row){ ?>
								<option value="<?php echo $key;?>"><?php echo $row;?></option>
							<?php } ?>
							</select>
						</div>
					</div>
				</div>
				
				<!--<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input type="text" name="m_zip" class="form-control" placeholder="Zip">
						</div>
					</div>
				</div>-->
				
			</div>
			
			<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input type="text" name="costodian_name" class="form-control" placeholder="Name of Custodian">
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input type="text" name="account_number" class="form-control" placeholder="Account Number">
						</div>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				</div>
			</div>
			
			<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<label style="color: #a59e9e;font-weight:500;margin-top:8px;">Do you prefer to have the funds deposited electronically?</label>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
                            <select class="form-control" name="deposited_electronically">
							<?php foreach($yes_no_option3 as $key => $row){ ?>
								<option value="<?php echo $key;?>"><?php echo $row;?></option>
							<?php } ?>
							</select>
						</div>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				</div>
			</div>
			<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input type="text" name="bank_name" class="form-control" placeholder="Bank Name">
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				</div>
			</div>
			<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input type="text" name="account_name" class="form-control" placeholder="Account Name">
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				</div>
			</div>
			<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input type="text" name="account_number1" class="form-control" placeholder="Account Number">
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				</div>
			</div>
			<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<label style="color: #a59e9e;font-weight:500;margin-top:8px;">Will another person be required to sign?</label>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
                            <select class="form-control" name="sign_required">
							<?php foreach($yes_no_option3 as $key => $row){ ?>
								<option value="<?php echo $key;?>"><?php echo $row;?></option>
							<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				</div>
			</div>
			<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input type="text" name="first_name" class="form-control" placeholder="First Name">
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input type="text" name="middle_name" class="form-control" placeholder="Middle Name">
						</div>
					</div>
				</div>
				<br>
				<!--<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input type="text" name="last_name" class="form-control" placeholder="Last Name">
						</div>
					</div>
				</div>-->
				
			</div>
			
			<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input type="text" name="phone" data-mask="(999) 999-9999"  class="form-control" placeholder="Primary Phone">
						</div>
					</div>
				</div>
				
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input type="text" name="email" class="form-control" placeholder="E-mail Address">
						</div>
					</div>
				</div>
			</div>
		
		</div>
	</div>
	 <div class="modal-footer">
		<button type="submit" class="btn nk-indigo" name="submit" value="submit">Submit</button>
        <button type="button" class="btn nk-indigo" data-dismiss="modal">Close</button>
    </div>
	</div>
	</div>
	</form>
	</div>
	
	
	
<!--- edit and update  portal acacount modal --------------------------------------------------------------------->
	
	<div class="modal" id="edit_portal_account">
	 <form method="post" action="<?php echo base_url();?>update_portal_account">
	 
         <div class="modal-dialog modals-default">
             <div class="modal-content">
				<div class="modal-header">
					<!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
				</div>
     <div class="modal-body">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<h3 Style="font-weight:400;">Edit Account:</h3>					
				</div>
			</div>
		
			<div class="row">&nbsp;</div>
			<div class="row">
            </div>
			<div class="row">
			
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<label style="color: black;font-weight:600;margin-top:8px;">Account Name:</label>
						</div>
					</div>
				</div>
				<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<input type="hidden" name="account_id" id="account_id" value="<?php //echo $row->id;?>">
                        <div class="form-single nk-int-st widget-form">
							<input id="account_name" type="text" name="account_name" class="form-control" placeholder="Account Name">
							
						</div>
					</div>
				</div>
				
			</div>
			
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<label style="color: black;font-weight:600;margin-top:8px;">Account Number:</label>
						</div>
					</div>
				</div>
                <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input id="account_number1" type="text" name="account_number1" class="form-control" placeholder="Account Number" value="">
						</div>
					</div>
				</div>
				
				
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<label style="color: black;font-weight:600;margin-top:8px;">Primary Contact:</label>
						</div>
					</div>
				</div>
                <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input id="phone" type="text" name="phone" data-mask="(999) 999-9999"  class="form-control" placeholder="Primary Phone"value="">
							
						</div>
					</div>
				</div>
				
			</div>

			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<label style="color: black;font-weight:600;margin-top:8px;">Address:</label>
						</div>
					</div>
				</div>
                <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<input id="m_street_address" type="text" name="m_street_address" class="form-control" placeholder="Mailing Street Address">
						</div>
					</div>
				</div>
				
			</div>
			
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
							<label style="color: black;font-weight:600;margin-top:8px;">Auto Deposite:</label>
						</div>
					</div>
				</div>
                <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="form-single nk-int-st widget-form">
                            <select class="form-control" name="deposited_electronically" id="deposited_electronically">
							<?php foreach($yes_no_option3 as $key => $row){ ?>
								<option value="<?php echo $key;?>"><?php echo $row;?></option>
							<?php } ?>
							</select>
						</div>
					</div>
				</div>
				
			</div>
	</div>
	<div class="modal-footer">
        <button type="submit" class="btn nk-indigo" name="update" value="update">Update</button>
        <button type="button" class="btn nk-indigo" data-dismiss="modal">Close</button>
    </div>
	</div>
	</div>
	</form>
	</div>
	<!-- edit and update account---------------------------------------------------------------------------->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	    <script src="http://localhost/Lender_Portal/assets/js/vendor/jquery-1.12.4.min.js"></script>

	<script>
	
	function row_edit(ddd){

		var id = ddd.id;

		//alert(id);
		$('div#edit_portal_account select option').attr('selected',false);
		$.ajax({

			type	: 'post',
			url		: '<?php echo base_url()."Account_setting/fetch_account_id";?>',

			data	: {'id':id},

			success	: function(response){

					var a = JSON.parse(response);
				    
					$('div#edit_portal_account input#account_id').val(a.id);  
					
					$('div#edit_portal_account input#account_name').val(a.account_name);  

					$('div#edit_portal_account input#account_number1').val(a.account_number1);  

					$('div#edit_portal_account input#phone').val(a.phone);  

					$('div#edit_portal_account input#m_street_address').val(a.m_street_address);
					$('div#edit_portal_account input#m_city').val(a.m_city);
					$('div#edit_portal_account input#m_unit').val(a.m_unit);
					
					$('div#edit_portal_account select#deposited_electronically option[value="'+a.deposited_electronically+'"]').attr('selected',true);

				}

			});
}
	</script>

	