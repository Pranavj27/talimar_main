<?php
$loan_type 			= $this->config->item('loan_type_option');
$position_option 	= $this->config->item('position_option');
$property_type 		= $this->config->item('property_modal_property_type');
$loan_status_option 		= $this->config->item('loan_status_option');
$servicing_agent_option         = $this->config->item('servicing_agent_option');
$trustdeed_filter_option 		= $this->config->item('trustdeed_filter_option');
$loan_payment_status        = $this->config->item('loan_payment_status');


 //echo '<pre>';
 //print_r($active_trust_deed_schedule);
 //echo '</pre>';


?>
<style>
a.btn.btn-primary.primary-icon-notika.btn-reco-mg.btn-button-mg.waves-effect {
    border-radius: 50%;
}
h4.deed_heading {
    background-color: #ececec;
    padding: 12px;
}

.table.table-striped>tbody>tr>td, .table.table-striped>tfoot>tr>th, .table.table-striped>thead>tr>th {

    padding: 10px !important;
}
</style>
	<div class="data-table-area">
        <div class="container">
		
		
			<div class="row">
				
                <div class="col-xs-12">
                    
                    <!--<h4 class="deed_heading">Trust Deed Portfolio</h4> -->
                    <h3>Trust Deed Portfolio</h3> 
                </div>
				<!--<div class="col-md-6 col-sm-6 col-xs-12">
					<form method="post" action="<?php echo base_url();?>active_trust_pdf">
					    <button type="submit" class="btn btn-primary pull-right">Print Schedule</button>
                        <input type="hidden" name="trustdeed_value" value="<?php echo $status_option_val ? $status_option_val : '';?>">
                        <input type="hidden" name="account_value" value="<?php echo $account_option_val ? $account_option_val : '';?>">
                        <input type="hidden" name="uri" value="<?php echo $this->uri->segment(2) ? $this->uri->segment(2) : '';?>">
                    </form>
				</div>-->
			</div>
            <div class="row">&nbsp;</div>
            <div class="row">
                <form id="trustdeed_form" method="post" action="<?php echo base_url();?>active_trust_deeds">

                    <div class="col-md-3" Style="margin-left:20px;">
                        <label> Account Name:</label>
                        <select name="account_option" class="form-control" onchange="account_value(this.value);">
                            <option value="0">Select All</option>
                            <?php foreach($all_investor_name as $key => $rows){?>
                                <option value="<?php echo $rows->id;?>" <?php if($account_option_val == $rows->id){echo 'selected'; }?>><?php echo $rows->name;?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <!--<div class="col-md-2" Style="margin-left:20px;">
                        <label>Trust Deed Status:</label>
                        <select name="trust_deed" class="form-control" onchange="status_value(this.value);">
                            <?php foreach($trustdeed_filter_option as $key => $row){?>
                                <option value="<?php echo $key;?>" <?php if($trust_deed_val == $key){echo 'Selected';}?>><?php echo $row;?></option>
                            <?php } ?>
                        </select>
                    </div>-->
                   <div class="col-md-2" Style="margin-left:20px;">
                        <label>Loan Status:</label>
                        <select name="status_option" class="form-control" onchange="status_value(this.value);">
                            <?php
                            unset($loan_status_option[1]);
                            unset($loan_status_option[4]);
                            unset($loan_status_option[5]);
                             foreach($loan_status_option as $key => $row){?>
                                <option value="<?php echo $key;?>" <?php if($status_option_val == $key){echo 'Selected';}?>><?php echo $row;?></option>
                            <?php } ?>
                        </select>
                    </div>

                    
                </form>
            </div>
            <div class="row">&nbsp;<br></div>

            <!------------------- PENDING SECTION ------------------------>
           
            <div class="row">
                <div class="col-md-12">
                    
                            <table id="trustdeed_table222222" class="table table-striped table-border">
                                <thead style="background-color: #f2f2f2;">
                                    <tr>
                                        <th>Location</th>
                                        <th>Position</th>
                                        <th>Loan Amount</th>
                                        <th>$ Ownership</th>                                  
                                        <th>% Ownership</th>
                                        <th>Lender Rate</th>
                                        <th>Payment</th>
                                        <th>Loan Status</th>
                                       <!-- <th>Trust Deed<br>Status</th>-->
                                        <th>Payment Status</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php
								$count = 0;
								$loan_amount = 0;
								$total_payment = 0;
                                $Ownership = 0;
                                $perOwnership = 0;
                                $percent_yield = 0;

								foreach($active_trust_deed_schedule as $row){
                                    $c_loan_amount = 0;
                                    $c_investment = 0;
                                    $c_lender_payment = 0;
                                    $c_percent_yield = 0;
                                    if($row['loan_amount']){
                                        $c_loan_amount = $row['loan_amount'];
                                    }                                    
                                    if($row['lender_payment']){
                                        $c_lender_payment = $row['lender_payment'];
                                    }
                                    if($row['investment']){
                                        $c_investment = $row['investment'];
                                    }
                                    if($row['percent_yield']){
                                        $c_percent_yield = $row['percent_yield'];
                                    }

									$count++;
									$loan_amount += $c_loan_amount;
									$total_payment += $c_lender_payment;
                                    $Ownership += $c_investment;

                                        if($row['loan_status'] == '3'){

                                            $text = 'Paid Off';
                                            $gotolink = '#'; 

                                        }else{

                                            if($row['nfiles_status'] == '1'){
                                               // $class='black_color';
                                                $text='Active';
                                            }else{

                                                $text='Pending';
                                                //$class='red_color';
                                            }

                                            $gotolink = base_url().'trust_deed_details/'.$row['talimar_loan']; 
                                        }

                                        if($row['payment_status'] == '1'){

                                            $Latetext = 'Current';
                                        }elseif($row['payment_status'] == '2' || $row['payment_status'] == '3'){
                                            $Latetext = 'Late';
                                        }else{
                                            $Latetext = '';
                                        }

                                        if($row['unit'] != ''){

                                            $fulladdress = $row['property_address'].' '.$row['unit'].'; '.$row['city'].', '.$row['state'].' '.$row['zip'];
                                        }else{
                                            $fulladdress = $row['property_address'].'; '.$row['city'].', '.$row['state'].' '.$row['zip'];
                                        }

                                        $percent_ownership = 0;
                                        if($c_loan_amount > 0){
                                            $percent_ownership = ($c_investment/$c_loan_amount)*100;
                                        }
                                        

                                        $perOwnership +=  $percent_ownership;
                                        $percent_yield +=  $c_percent_yield;

								?>
									<tr>
									    <td style="width:30%"><a href="<?php echo $gotolink;?>"><?php echo $fulladdress;?></a></td>
										<!--<td style="width:7%"><?php //echo $row['talimar_loan'];?></td>
										<td style="width:10%"><?php// echo $row['city'];?></td>
										<td style="width:10%"><?php// echo $row['state'];?></td>-->
										<td style="width:7%"><?php echo $position_option[$row['position']];?></td>
                                        <td style="width:9%">$<?php echo number_format($c_loan_amount);?></td>
										<td style="width:10%">$<?php echo number_format($c_investment);?></td>
										
										<td style="width:9%"><?php echo number_format($percent_ownership,2);?>%</td>
                                        <td style="width:6%"><?php echo number_format($c_percent_yield,2);?>%</td>
										<td style="width:10%">$<?php echo number_format($c_lender_payment,2);?></td>
						
										<!--<td style="width:10%"><?php //echo $row['zip'];?></td>-->
										<!--<td style="width:14%"><?php //echo $servicing_agent_option[$row['servicing_agent']];?></td>-->
										<td style="width:9%"><?php echo $loan_status_option[$row['loan_status']];?></td>
                                        <!--<td style="width:10%"><?php //echo $text;?></td>-->
                                        <td style="width:8%"><?php echo $Latetext;?></td>
                                        <!--<td>
                                        	<a href="<?php //echo base_url();?>trust_deed_details/<?php //echo $row['talimar_loan'];?>" class="btn btn-primary btn-xs">View</a>
                                        </td>-->
									</tr>
								<?php }  ?>
								</tbody>
								
                                <tfoot style="background-color: #f2f2f2;">
                                    <tr>
                                        <th>Total: <?php echo $count;?></th>
                                        
                                        <th></th>
                                        <th>$<?php echo number_format($loan_amount);?></th>
                                        <th>$<?php echo number_format($Ownership);?></th>
                                        <th></th>
                                        <th></th>
                                        <th>$<?php echo number_format($total_payment,2);?></th>
                                        <th></th>
                                        <!--<th></th>-->
                                        <th></th>
                                        
                                    </tr>
									<tr>
                                        <th>Average:</th>                                        
                                        <th></th>
                                        <th>$<?php 
                                            if($count > 0){
                                                echo number_format($loan_amount/$count);
                                            }else{
                                                echo number_format(0);
                                            }                                            
                                            ?>
                                        </th>
                                        <th>$<?php 
                                            if($count > 0){
                                                echo number_format($Ownership/$count);
                                            }else{
                                                echo number_format(0);
                                            }                                            
                                            ?>                                                
                                        </th>
                                        <th>
                                            <?php 
                                            if($loan_amount > 0){
                                                echo number_format($Ownership/$loan_amount*100,2);
                                            }else{
                                                echo number_format(0);
                                            }                                            
                                            ?>% *
                                        </th>
                                        <th>
                                            <?php 
                                            if($Ownership > 0){
                                                echo number_format(($total_payment*12)/$Ownership*100,2);
                                            }else{
                                                echo number_format(0);
                                            }                                            
                                            ?>% **                                            
                                        </th>
                                        <th>$<?php 
                                            if($count > 0){
                                                echo number_format($total_payment/$count,2);
                                            }else{
                                                echo number_format(0);
                                            }                                            
                                            ?>
                                            </th>
                                        <th></th>
                                        <!--<th></th>-->
                                        <th></th>
                                        
                                    </tr>
                                </tfoot>
                            </table>
                    
                </div>
            </div>

            

            <div class="row"><br></div>
            <div class="row">
                <div class="col-md-12">
                    
                    <span class="">* Represents weighted average of Total $ Ownership / Total Loan Amount. </span>
                    <br>
                    <span class="">** Represents weighted average calculated by total monthly payments / total active balance.</span>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        
        function status_value(that){

            $('form#trustdeed_form').submit();
        }

        function account_value(that){

            $('form#trustdeed_form').submit();
        }

    </script>