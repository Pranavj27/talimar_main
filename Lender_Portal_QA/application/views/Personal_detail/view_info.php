<?php
error_reporting(0);
$phone_type_option 		= $this->config->item('phone_type_option');
$notification_setting 	= $this->config->item('notification_setting');

?>
<style type="text/css">
h4.deed_heading {
    background-color: #ececec;
    padding: 12px;
}

.search-engine-int{
	padding: 10px 0px 5px 12px !important;
}

.btn-primary {
    color: #fff;
    background-color: #3379b5 !important;
    border-color: #3379b5 !important;
}
.modal-dialog.modals-default .modal-content, .modal-dialog.modal-sm .modal-content, .modal-dialog.modal-large .modal-content {
    border-radius: 0px;
    padding: 30px 24px;
}

.addmar {
    margin-top: 8px;
}

.modal-dialog.modal-sm {
    width: 400px;
    margin-top: 110px;
}
</style>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<h3 Style="font-weight:400;margin-left: 12px;">View Details: </h3>					
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<!-- <a href="<?php echo base_url();?>personal_setting" class="btn btn-primary pull-right" style="margin-right:12px;">Edit</a> -->

			<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#chekmodallnoti" style="margin-right: 5px;">Back</button>
			
		</div>
		
	</div>

						
	<div class="modal animated bounce in" id="chekmodallnoti" role="dialog">
		<div class="modal-dialog modals-default modal-sm">
			<div class="modal-content">
				
				<div class="modal-body">
					<div class="row">
						<div class="col-md-8">
							<label>Do you want to save the information?</label>
						</div>
						<div class="col-md-4">
							<button type="button" onclick="go_back(this);" class="btn btn-primary waves-effect btn-sm">No</button>
							<button type="submit" onclick="SaveNotificatioVAl(this);" class="btn btn-primary waves-effect btn-sm">Yes</button>
						</div>
						
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<!------------------ Check save or not ---------------------------->
				
	<div class="row">&nbsp;</div>
	<div class="row">
		<div class="col-xs-12">
		<?php if($this->session->flashdata('success')!=''){ ?>
			<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="notika-icon notika-close"></i></span></button> <?php echo $this->session->flashdata('success');?>
            </div>
		<?php } ?>
		
		<?php if($this->session->flashdata('error')!=''){ ?>
			<div class="alert alert-danger alert-dismissible alert-mg-b-0" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="notika-icon notika-close"></i></span></button> <?php echo $this->session->flashdata('error');?>
            </div>
		<?php } ?>
		</div>
	</div>
		<div class="row">&nbsp;</div>
		<div class="row">
			<div class="col-xs-12">
				<h4 class="deed_heading">Notification Settings:</h4> 
			</div>
		</div>

		<?php
		/*echo '<pre>';
		print_r($contact_tags_details);
		echo '</pre>';*/
			foreach ($contact_tags_details as $value) {
				if($value->contact_email_blast !=''){
					$Tagarray[$value->contact_email_blast] = $value->contact_email_blast;			
				}
			}
		?>

		<div class="row">
			<?php foreach($notification_setting as $key => $row){ 
						if(in_array($key, $Tagarray)){
							$checked = 'checked="checked"';
						}else{
							$checked = '';
						} ?>
	            <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<input type="checkbox" class="" name="notify_by_<?php echo $key;?>" value="<?php echo $key;?>" required="required" <?php echo $checked;?> > <?php echo $row;?>
					</div>
				</div>
			<?php } ?>

				<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<button type="button" class="btn btn-primary btn-sm" onclick="SaveNotificatioVAl(this);">Save</button>
					</div>
				</div>
		</div>
		<div class="row">
            <div class="col-lg-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<small>* Text notifications will be sent to the phone number listed in the Contact Information section. Please note, only mobile phones can be used when selecting text message notifications.</small>
				</div>
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">
			<div class="col-xs-12">
				<h4 class="deed_heading">Security Settings:</h4> 
			</div>
		</div>
		<div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>User Name:</label> <span><?php echo $this->session->userdata('user_name');?></span>
				</div>
			</div>

			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>Password:</label> <span>**********</span>
				</div>
			</div>

			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#PasswordModal">Change Password</button>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#usernameModal">Change Username</button>
				</div>
			</div>
						
		</div>

		<!--<div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ImageModal">Add Image</button>
				</div>
			</div>
		</div>-->

		<div class="row">
			<div class="col-xs-12">
				<h4 class="deed_heading">Contact Information:</h4> 
			</div>
		</div>

		<div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>Name:</label> <span><?php echo $personal_view_details[0]->contact_firstname.' '.$personal_view_details[0]->contact_lastname; ?></span>
				</div>
			</div>

			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>Legal Name:</label> <span><?php echo $personal_view_details[0]->contact_suffix;?></span>
				</div>
			</div>
						
		</div>

		<!--<div class="row">

			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>Company:</label> <span><?php echo $personal_view_details[0]->company_name_1;?></span>
				</div>
			</div>
		</div>-->

		<div class="row">

			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>Phone:</label> <span><?php echo $personal_view_details[0]->contact_phone;?> (<?php echo $phone_type_option[$personal_view_details[0]->primary_option];?>)</span>
				</div>
			</div>
			
			
			<!--<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>Phone Type:</label> <span><?php echo $personal_view_details[0]->primary_option ? $phone_type_option[$personal_view_details[0]->primary_option] : '';?></span>
				</div>
			</div>-->
							
		</div>

		<!--<div class="row">

			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>Other Phone #:</label> <span><?php echo $personal_view_details[0]->alter_phone;?></span>
				</div>
			</div>
			
			
			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>Phone Type:</label> <span><?php echo $personal_view_details[0]->alter_option ? $phone_type_option[$personal_view_details[0]->alter_option] : '';?></span>
				</div>
			</div>
							
		</div>-->

		<div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>E-Mail:</label> <span><?php echo $personal_view_details[0]->contact_email; ?></span>
				</div>
			</div>
			<!--<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>Fax:</label> <span><?php echo $personal_view_details[0]->contact_fax;?></span>
				</div>
			</div>-->
			
			
		</div>

		<!--<div class="row">
            <div class="col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>Home Address:</label> <span><?php echo $personal_view_details[0]->contact_address. $personal_view_details[0]->contact_unit.'; '.$personal_view_details[0]->contact_city.', '.$personal_view_details[0]->contact_state.' '.$personal_view_details[0]->contact_zip; ?></span>
				</div>
			</div>
		</div>-->

		<div class="row">
            <div class="col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>Mailing Address:</label> <span><?php echo $personal_view_details[0]->contact_mailing_address.' '.$personal_view_details[0]->contact_mailing_unit.'; '.$personal_view_details[0]->contact_mailing_city.', '.$personal_view_details[0]->contact_mailing_state.' '.$personal_view_details[0]->contact_mailing_zip; ?></span>
				</div>
			</div>
			<!--<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>Unit #:</label> <span><?php echo $personal_view_details[0]->contact_unit; ?></span>
				</div>
			</div>--> 
			
			<!-- <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>Title:</label> <span><?php echo $personal_view_details[0]->contact_title; ?></span>
				</div>
			</div> -->
							
		</div>
		<!--<div class="row">
			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>City:</label> <span><?php echo $personal_view_details[0]->contact_city; ?></span>
				</div>
			</div> 
			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>State:</label> <span><?php echo $personal_view_details[0]->contact_state; ?></span>
				</div>
			</div> 
			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>Zip:</label> <span><?php echo $personal_view_details[0]->contact_zip; ?></span>
				</div>
			</div> 

	</div>-->

	<!-- 	<div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>About:</label> <span><?php echo $personal_view_details[0]->contact_about; ?></span>
				</div>
			</div>
		</div> -->
<!-- 		<div class="row">
 <?php $dob=$personal_view_details[0]->contact_dob ? date('m-d-Y',strtotime($personal_view_details[0]->contact_dob)):'';?>
			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>DOB:</label> <span><?php echo $dob;?></span>
				</div>
			</div>
			
			
							
		</div> -->

		<div class="row">
			<div class="col-xs-12">
				<h4 class="deed_heading">Image:</h4> 
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">

				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ImageModal" >Add Image</button>

			</div> 
		</div>

		<div class="row"><br></div>

</div>	

<!------------------------------------------------->

<div class="modal fade" id="ImageModal" role="dialog">
    <div class="modal-dialog modal-sm modals-default">
        <div class="modal-content">
        	<form method="post" action="<?php echo base_url();?>Home/addImage" enctype="multipart/form-data">
	            <div class="modal-header">
	               <!-- <button type="button" class="close" data-dismiss="modal">&times;</button>-->
	            </div>
	            <div class="modal-body">
	                <div class="row">
			        	<div class="col-sm-12">
			        		<label>Upload image:</label>
			        		<input type="file" name="file" class="form-control">
			        	</div>
			        </div>
			        <h2><br></h2>
	            </div>
	            <div class="modal-footer" style="margin-right: 15px;">
	                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	                <button type="submit " class="btn btn-primary">Save</button>
	            </div>
	        </form>
        </div>
    </div>
</div>

<!---------------------------------------------->

<div class="modal fade" id="usernameModal" role="dialog">
    <div class="modal-dialog modal-sm modals-default">
        <div class="modal-content">
        	
	            <div class="modal-header">
	               
	            </div>
	            <div class="modal-body">
	                <div class="row">
			        	<div class="col-sm-12">
			        		<label>Enter New Username:</label>
			        		<input type="text" name="username" id="newUserval" class="form-control">
			        	</div>
			        </div>
			        <h2><br></h2>

	            </div>
	            <div class="modal-footer" style="margin-right: 15px;">
	                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	                <button type="button" onclick="changeUsername(this);" class="btn btn-primary">Save</button>
	            </div>
	        
        </div>
    </div>
</div>

<!--------------------------------------------------------->

<div class="modal fade" id="PasswordModal" role="dialog">
    <div class="modal-dialog modal-sm modals-default">
        <div class="modal-content">
        	<form method="post" action="<?php echo base_url();?>Personal_details/UpdatePassword">
	            <div class="modal-header">
	               <!-- <button type="button" class="close" data-dismiss="modal">&times;</button>-->
	               <h4></h4>
	            </div>
	            <div class="modal-body">
	                <div class="row">
			        	<div class="col-sm-12">
			        		<label>Old Password:</label>
			        		<input type="password" name="old_password" class="form-control" required="required">
			        	</div>
			        </div>
			        
			        <div class="row addmar">
			        	<div class="col-sm-12">
			        		<label>New Password:</label>
			        		<input type="password" name="new_password" class="form-control" minlength="8">
			        	</div>
			        </div>

			        <div class="row addmar">
			        	<div class="col-sm-12">
			        		<label>Confirm Password:</label>
			        		<input type="password" name="re_password" class="form-control">
			        	</div>
			        </div>
			        <h2><br></h2>
	            </div>
	            <div class="modal-footer" style="margin-right: 15px;">
	                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	                <button type="submit " class="btn btn-primary">Save</button>
	            </div>
	        </form>
        </div>
    </div>
</div>
<!------------------------------------------------->

<script type="text/javascript">
	function go_back(){
		window.history.back();
		//window.location.href='<?php echo base_url()."view_details";?>';
	}


	function changeUsername(that){

		var fieldval = $.trim($('#usernameModal input#newUserval').val());
		
		if(fieldval !=''){

			var lengthcount = fieldval.length;

			if(lengthcount > 5){

				$.ajax({

					type : 'POST',
					url  : '<?php echo base_url()."Personal_details/Updateusernameinfo";?>',
					data : {'fieldval':fieldval},
					success : function(response){

						$('#usernameModal div.row').append('<div class="col-sm-12"><p class="text-center text-success"><b>Username updated successfully</b></p></div>');
						window.location.reload();
					}
				});

			}else{
				alert('Please enter more then 5 letters in your username');
				return false;
			}
		}else{

			alert('Field is required');
			return false;
		}
	}

	function SaveNotificatioVAl(that){

		//default val = 2,7,9

		var notify2 = $("input[name='notify_by_2']:checked").val();
		var notify7 = $("input[name='notify_by_7']:checked").val();
		var notify9 = $("input[name='notify_by_9']:checked").val();

		$.ajax({

				type : 'POST',
				url  : '<?php echo base_url()."Personal_details/update_notification";?>',
				data : {'notify2':notify2, 'notify7':notify7, 'notify9':notify9},
				success : function(response){

					if(response == '1'){

						alert('Please select an notification option.');
						window.location.reload();

					}else{

						alert('Notification updated successfully.');
						window.location.reload();
					}
				}
		})

			
	}
</script>