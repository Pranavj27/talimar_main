<?php

$phone_type_option = $this->config->item('phone_type_option');
$email_types_option		 	= $this->config->item('email_types_option');
?>
<style>
.search-engine-int.sm-res-mg-t-30.tb-res-mg-t-30.tb-res-mg-t-0 {
    padding: 10px !important;
}
a:hover {
    color: #337ab7;
    text-decoration: none;
}
</style>
	<div class="container">
		<div class="row">
			<!--------------------MESSEGE SHOW-------------------------->
				<?php if($this->session->flashdata('error')!=''){  ?>
				 <div class="error alert alert-danger"><i class='fa fa-thumbs-o-down'></i> <?php echo $this->session->flashdata('error');?></div><?php } ?>
				  <?php if($this->session->flashdata('success')!=''){  ?>
				 <div class="success alert alert-success"><i class='fa fa-thumbs-o-up'></i> <?php echo $this->session->flashdata('success');?></div><?php } ?>
				 
			<!-------------------END OF MESSEGE-------------------------->
		
				<div class="col-md-6 col-sm-6 col-xs-12">
					<h3 Style="font-weight:540;margin-left:9px;">Personal Settings</h3>					
				</div>
			<?php 

				$id=$this->session->userdata('p_user_id');
			?>				
				
			<form method="post" action="<?php echo base_url('Personal_details/edit_profile/'.$id);?>">			
				<div class="col-md-6 col-sm-6 col-xs-12">
					
					<button type="submit"  name="submit" class="btn btn-primary pull-right"  style="margin-right: 5px;">Save</button>
					<!-- <a href="<?php echo base_url();?>view_details" class="btn btn-primary pull-right" style="margin-right:12px;">Close</a>  -->
					<button type="button" class="btn btn-primary pull-right waves-effect" data-toggle="modal" data-target="#chekmodall" style="margin-right: 5px;">Close</button> 
				</div>

						<!----------------- Check save or not ----------------------------->
				<div class="modal animated bounce in" id="chekmodall" role="dialog">
					<div class="modal-dialog modals-default modal-sm">
						<div class="modal-content">
							
							<div class="modal-body">
								<div class="row">
									<div class="col-md-8">
										<label>Do you want to save the information?</label>
									</div>
									<div class="col-md-4">
										<button type="button" onclick="go_back(this);" class="btn nk-default waves-effect btn-sm">No</button>&nbsp;&nbsp;
										<button type="submit" name="submit" class="btn btn-primary waves-effect btn-sm">Yes</button>
									</div>
									
								</div>
								
							</div>
						</div>
					</div>
				</div>
				<!------------------ Check save or not ---------------------------->
				
				
		</div>
	
		<div class="row"><br></div>

			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
					 <label class="loan_data_lable" for="textinput">First Name:</label>
                       
							<input type="text"  class="form-control" placeholder="First Name" name="fname" value ="<?php echo $personal_data_details[0]->contact_firstname;?>">
					
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
					 <label class="loan_data_lable" for="textinput">Last Name:</label>
                       
							<input type="text" name="lname" class="form-control" value="<?php echo $personal_data_details[0]->contact_lastname;?>" placeholder="Last Name">
						
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
					 <label class="loan_data_lable" for="textinput">Legal First Name:</label>
                       
							<input type="text" name="legal_lirst" class="form-control" value="<?php echo $personal_data_details[0]->contact_suffix;?>" placeholder="Legal First Name">
						
					</div>
				</div>
			</div>
			

			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
					 <label class="loan_data_lable" for="textinput">User Name:</label>
						<input type="text" class="form-control" name="username" value ="<?php echo $loginuser_detail[0]->username;?>" required>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
					 <label class="loan_data_lable" for="textinput">Password:</label>                      
						<input type="password" class="form-control" name="password" value="<?php echo $loginuser_detail[0]->password;?>" required>						
					</div>
				</div>
			</div>
			
						
			<div class="row">

				<div class="col-md-3">
					<div class="form-group">
					 <label class="loan_data_lable" for="textinput">Primary Contact #:</label>
							
							<input type="text" name="p_contact"  value="<?php echo $personal_data_details[0]->contact_phone;?>" class="form-control" placeholder="Primary Contact">
							
						</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
					 <label class="loan_data_lable" for="textinput">Phone Type:</label>
							
						<select class="form-control" name="primary_type">
							<?php foreach($phone_type_option as $key => $row){ ?>
								<option value="<?php echo $key;?>" <?php if($personal_data_details[0]->primary_option == $key){echo 'selected';}?>><?php echo $row;?></option>
							<?php } ?>
						</select>
						
						</div>
				</div>
			</div>
			
			
			<div class="row">

				
				<div class="col-md-3">
					<div class="form-group">
					 <label class="loan_data_lable" for="textinput">Other Phone #:</label>
							
							<input type="text" name="secondary_contact"  value="<?php echo $personal_data_details[0]->contact_secondary_phone;?>" class="form-control" placeholder="Secondary Contact">
						
						</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
					 <label class="loan_data_lable" for="textinput">Phone Type:</label>
							
						<select class="form-control" name="alter_type">
							<?php foreach($phone_type_option as $key => $row){ ?>
								<option value="<?php echo $key;?>" <?php if($personal_data_details[0]->alter_option == $key){echo 'selected';}?>><?php echo $row;?></option>
							<?php } ?>
						</select>
						
						</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
					 <label class="loan_data_lable" for="textinput">E-Mail:</label>
                        
						<input type="text" class="form-control contact_title" name = "email" placeholder="Email" value ="<?php echo $personal_data_details[0]->contact_email;?>" >
					
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
					 <label class="loan_data_lable" for="textinput">E-Mail Type:</label>
                        
	<input type="text" class="form-control contact_title" name = "email_type" placeholder="Email Type" value ="<?php echo $email_types_option[$personal_data_details[0]->pemail_type]?>" >
					
					</div>
				</div>
			</div>

		
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						 <label class="loan_data_lable" for="textinput">Fax:</label>
                      
							<input type="text" class="form-control" name="fax" placeholder="Fax" value ="<?php echo $personal_data_details[0]->contact_fax;?>" >
						
					</div>
				</div>
				
			</div>
		
		
			<div class="row borrower_data_row">
				<div class="col-md-6">
					<div class="form-group">
					 <label class="loan_data_lable" for="textinput">Street Address:</label>
                      
							<input type="text" class="form-control " id="directed_IRA_NAME" placeholder="Street" name = "street" value ="<?php echo $personal_data_details[0]->contact_address;?>">
						
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						 <label class="loan_data_lable" for="textinput">Unit:</label>
                      
							<input type="text" class="form-control " id="custodian" placeholder="Unit" name = "unit" value ="<?php echo $personal_data_details[0]->contact_unit;?>">
						
					</div>
				</div>
				
			</div>
		
		
			<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							 <label class="loan_data_lable" for="textinput">City:</label>
	                     
							<input type="text" class="form-control " placeholder="City" name ="City" value ="<?php echo $personal_data_details[0]->contact_city;?>">
							
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
						 <label class="loan_data_lable" for="textinput">State:</label>
							
								<input type="text" name="State" class="form-control" value="<?php echo $personal_data_details[0]->contact_state;?>" placeholder="State">
							
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
						 <label class="loan_data_lable" for="textinput">Zip:</label>
							
								<input type="text" name="Zip" value="<?php echo $personal_data_details[0]->contact_zip;?>" class="form-control" placeholder="Zip">
						
						</div>
					</div>
				</div>
			

				<div class="row">

			        <div class="col-md-12">
						<div class="form-group">
						     <input type = "checkbox" <?php if($personal_data_details[0]->same_mailing_addresss == '1'){echo 'checked';} ?> id = "samee_mailing_chkbx" onclick = "copy_addresss(this)" /> <label>Check, If same as Above</label>
							<input type = "hidden" id = "same_mailing_addresss" name = "same_mailing_addresss"  value =  "<?php  echo $personal_data_details[0]->same_mailing_addresss; ?>"/> </label>
						
						</div>
					</div>

				</div>
				<div class="row">
					<!--<div class="col-md-3">
						<div class="form-group">
						 <label class="loan_data_lable" for="textinput">Contact Personal Notes:</label>
						
							<input type="text" name="Contact" value="<?php// echo $personal_data_details[0]->contact_personal_notes;?>" class="form-control" placeholder="Contact Personal Notes">
							
						</div>
					</div>-->
					<div class="col-md-6">
						<div class="form-group">
						 <label class="loan_data_lable" for="textinput">Mailing Address: </label> 
						
							<input type="text" name="Mailing_add"  value="<?php echo $personal_data_details[0]->contact_mailing_address;?>" class="form-control" placeholder="Mailing Addresss">
							
						</div>
					</div>

					

					<div class="col-md-3">
						<div class="form-group">
						 <label class="loan_data_lable" for="textinput">Mailing Unit:</label>
							
							<input type="text" name="Mailing_unit"  value="<?php echo $personal_data_details[0]->contact_mailing_unit;?>" class="form-control" placeholder="Mailing Unit">
							
						</div>
					</div>
				</div>


				
				
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							 <label class="loan_data_lable" for="textinput">Mailing City:</label>
							
							<input type="text" name="Mailing_city" value="<?php echo $personal_data_details[0]->contact_mailing_city;?>" class="form-control" placeholder="Mailing City">
							
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							 <label class="loan_data_lable" for="textinput">Mailing State:</label>
							
							<input type="text" name="Mailing_state"  value="<?php echo $personal_data_details[0]->contact_mailing_state;?>" class="form-control" placeholder="Mailing State">
							
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
					
						 <label class="loan_data_lable" for="textinput">Mailing zip:</label>
						
							<input type="text" name="Mailing_zip"  value="<?php echo $personal_data_details[0]->contact_mailing_zip;?>" class="form-control" placeholder="Mailing zip">
							
						</div>
					</div>
					
				</div>
		</form>
		<!--<div class="row">
			<div class="col-md-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>Facebook: </label> <a target="_blank" href="<?php //echo $personal_data_details[0]->fb_profile;?>"><span><?php //echo $personal_data_details[0]->fb_profile;?></span></a>
				</div>
			</div>
		</div>
		<div class="row">	
			<div class="col-md-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>Linkedin: </label> <a target="_blank" href="<?php //echo $personal_data_details[0]->lkdin_profile;?>"><span><?php// echo $personal_data_details[0]->lkdin_profile;?></span></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<label>Bigger Pockets: </label> <a target="_blank" href="<?php //echo $personal_data_details[0]->bigger_pocket_profile;?>"><span><?php //echo $personal_data_details[0]->bigger_pocket_profile;?></span></a>
				</div>
			</div>
		</div>-->
	</div>

	<script>
		
	function go_back(){

		window.location.href='<?php echo base_url()."view_details";?>';
	}


	function copy_addresss(tha){

	if($(tha).is(':checked')) {
		
		var street_address 	= $('#directed_IRA_NAME').val();
		var unit 			= $('#custodian').val();
		var city 			= $('input[name="City"]').val();
		var state 			= $('input[name="State"]').val();
		var zip 			= $('input[name="Zip"]').val();
		
		 $('input[name="Mailing_add"]').val(street_address);
		 $('input[name="Mailing_unit"]').val(unit);
	     $('input[name="Mailing_city"]').val(city);
		 $('input[name="Mailing_state"]').val(state);
		 $('input[name="Mailing_zip"]').val(zip);
		 $('#same_mailing_addresss').val('1');
		
	}else{
		
		
		 $('input[name="Mailing_add"]').val('');
		 $('input[name="Mailing_unit"]').val('');
	     $('input[name="Mailing_city"]').val('');
		 $('input[name="Mailing_state"]').val('');
		 $('input[name="Mailing_zip"]').val('');
		 $('#same_mailing_addresss').val('0');
	}
}




	</script>