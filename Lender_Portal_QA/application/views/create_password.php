<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Password | Lender Portal</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/img/favicon1.png">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <!-- font awesome CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <!--<link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">-->
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/normalize.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/wave/waves.min.css">
    <!-- Notika icon CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/notika-custom-icon.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<style>
h2{
	color:#123657 !important;
}
a{
    cursor: pointer;
}
</style>

<body>
	
    <div class="login-content">
	
        <!-- Login -->
        <div class="nk-block toggled" id="l-create">
		<div class="input-group mg-t-15">
			<img src="<?php echo base_url();?>assets/img/logo/logo-big.png" alt="TaliMar Financial">
		</div>
		
		<div class="input-group mg-t-15">&nbsp;</div>
            <div class="nk-form">
			<h2>Lender Portal</h2><br>
			<?php if($this->session->flashdata('success')!=''){ ?>
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="notika-icon notika-close"></i></span></button> <?php echo $this->session->flashdata('success');?>
                </div>
			<?php } ?>
			
			<?php if($this->session->flashdata('error')!=''){ ?>
				<div class="alert alert-danger alert-dismissible alert-mg-b-0" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="notika-icon notika-close"></i></span></button> <?php echo $this->session->flashdata('error');?>
                </div>
			<?php } ?>
			
            <?php //echo $id.$username;?>
			<form method="post" action="<?php echo base_url();?>Home/CreatePasswordData">
                <input type="hidden" name="id" value="<?php echo $id;?>">
                <input type="hidden" name="uri5" value="<?php echo $uri5;?>">
                <div class="form-group">
                    <label class="pull-left">Username</label>
                    <div class="">
                        <input type="text" name="username" class="form-control" placeholder="Username" value="<?php echo $username;?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                     <label class="pull-left">New Password</label>
                    <div class="">
                        <input type="password" name="password" class="form-control" placeholder="New Password" minlength="8" required>
                    </div>
                </div>
                <div class="form-group">
                     <label class="pull-left">Confirm Password</label>
                    <div class="">
                        <input type="password" name="c_password" class="form-control" placeholder="Confirm Password" required>
                    </div>
                </div>
                <div class="form-group"></div>
				<div class="form-example-int mg-t-15">
					
                    <!--<a data-ma-action="nk-login-switch" data-ma-block="#l-reset-password" class="text-primary pull-left">Forgot Password</a><br>
                    <a data-ma-action="nk-login-switch" data-ma-block="#l-register" class="text-primary pull-left">Not Registered? Create an Account</a>-->

					<button class="btn btn-primary notika-btn-success waves-effect pull-right" type="submit" name="submit">Change Password</button>
					
                </div>
                <div class="form-example-int mg-t-15">&nbsp;</div>
			</form>
               
            </div>
			
        </div>
	
    </div>
    <!-- Login Register area End-->
    <!-- jquery
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/main.js"></script>

    

</body>

</html>