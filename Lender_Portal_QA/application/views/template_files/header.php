<?php
$contact_id = $this->session->userdata('p_user_id');
$fetchimg = $this->User_model->query("SELECT imgurl FROM `lender_contact_type` WHERE `contact_id`= '".$contact_id."' ");
$fetchimg = $fetchimg->result();
$imgurl = $fetchimg[0]->imgurl;
if($imgurl != ''){

    $Imgpath = $imgurl;
}else{
    $Imgpath = base_url().'assets/img/post/avatar0.jpg';
}
?>
<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>TaliMar Financial | Lender Portal</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/img/favicon1.png">
    <!-- Google Fonts
		============================================ -->
    <!--<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">-->

	<link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">

    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.transitions.css">
    <!-- meanmenu CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/meanmenu/meanmenu.min.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/normalize.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- jvectormap CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jvectormap/jquery-jvectormap-2.0.3.css">
    <!-- notika icon CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/notika-custom-icon.css">
    <!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/wave/waves.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/vendor/modernizr-2.8.3.min.js"></script>
	
	<!-- bootstrap select CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-select/bootstrap-select.css">
    <!-- datapicker CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/datapicker/datepicker3.css">
	<!-- main CSS Chosen
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/chosen/chosen.css">
	 <!-- Data Table JS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.dataTables.min.css">
	
	<!-- dialog CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/dialog/sweetalert2.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/dialog/dialog.css">
</head>
<style>
* {
	font-family: 'Open Sans', sans-serif !important;
}
a#username_bk:hover, a#username_bk:focus, a#username_bk:active{
	background-color: #fff0 !important;
}
.header-top-menu ul.nav.navbar-nav.notika-top-nav li a {
    color: #ffffff !important;
    font-size: 14px !important;
    /*font-weight: 800;
    letter-spacing: 1px;*/
}
.header-top-menu ul.nav.navbar-nav.notika-top-nav li a#username_bk {
    
    background: #103a60;
}
.main-menu-area.mg-tb-40 {
    margin-top: 0;
}
.header-top-menu {
    float: right;
    margin-top: -46px;
}
.table-striped>tbody>tr:nth-of-type(odd) {
    background-color: #fff;
}

.table.table-striped>thead>tr>th {
    color: #000;
}

.table.table-striped>tfoot>tr>th {
    color: #000;
    
}

.table.table-striped thead{

    background-color: #ddd !important;
}

.table.table-striped tfoot{

    background-color: #ddd !important;
}
.nav.navbar-nav.notika-top-nav li a{
    font-size: 15px !important;
}

.navbar-nav>li>a {
    color: #333 !important;
    padding: 10px 0px 8px 30px !important;
}

.hd-message-info a:hover {
    background: #e9eaea !important;
}

.nav>li>a:focus, .nav>li>a:hover {
    text-decoration: none;
    background-color: #2b4a65 !important;
}


</style>

<body>
   
    <!-- Start Header Top Area -->
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo-area">
                        <a href="<?php echo base_url();?>dashboard"><img src="<?php echo base_url();?>assets/img/logo/logo-big.png" alt="TaliMar Financial" style="max-width:45% !important;"/></a>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    
                </div>
            </div>
        </div>
    </div>
    <!-- End Header Top Area -->
    <!-- Mobile Menu start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul class="mobile-menu-nav">
                                <li>
									<a href="<?php echo base_url();?>dashboard">Dashboard</a>                             
                                </li>
                                <li>
									<a href="<?php echo base_url();?>active_trust_deeds">Trust Deed Portfolio</a>
                                    
                                </li>
                                <li>
									<a href="<?php echo base_url();?>trust_deed">Available Trust Deeds</a>
                                    
                                </li>
                                <li>
									<a href="<?php echo base_url();?>accounts">Account Settings</a>
                                    
                                </li>
                               
                                
                            </ul>
                        </nav>
						<div class="header-top-menu">
                        <ul class="nav navbar-nav notika-top-nav">

                            <?php
                              $loggedinUser  = $this->session->userdata('p_user_fname').' '.$this->session->userdata('p_user_lname');
                              if($loggedinUser != ''){
                                $loggedinUserVAl = $loggedinUser;
                              }else{
                                $loggedinUserVAl = 'Trust Deed';
                              }
                            ?>
                           
                            <li class="nav-item"><a id="username_bk" href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle" style="color:#333;">
                                <img src="<?php echo $Imgpath; ?>" style="border-radius: 50%;width: 46px;height: 45px;">&nbsp; <?php echo $loggedinUserVAl;?> </a> 
                                <div role="menu" class="dropdown-menu message-dd chat-dd animated zoomIn" style="left: 0px !important;padding: 6px;width: 100%; ">
                                    <div class="hd-message-info">
                                           
                                       <a href="<?php echo base_url();?>view_details">
                                            <div class="hd-message-sn">
												
                                                <div class="hd-mg-ctn">
                                                    <h3><i class="notika-icon notika-support"></i> &nbsp; My Profile</h3>                                                 
                                                </div>
                                            </div>
                                        </a>
										<a href="<?php echo base_url()?>logout">
                                            <div class="hd-message-sn">                                   
                                                <div class="hd-mg-ctn">
                                                    <h3><i class="notika-icon notika-next"></i> &nbsp; Log Out</h3>                                                 
                                                </div>
                                            </div>
                                        </a>
                                        
                                    </div>
                                    
                                </div>
                            </li>
                        </ul>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                        
                        <li>
							<a href="<?php echo base_url();?>dashboard">Dashboard</a>
                        </li>
                        <li>
							<a href="<?php echo base_url();?>active_trust_deeds">Trust Deed Portfolio</a>
                        </li>
                        <li>
							<a href="<?php echo base_url();?>trust_deed">Available Trust Deeds</a>							
						</li>
						<li>
							<a href="<?php echo base_url();?>accounts">Account Settings</a>							
						</li>
						<!--<li>
							<a href="<?php echo base_url();?>view_details">Personal Settings</a>														
						</li>-->

                    </ul>
					<div class="header-top-menu">
                        <ul class="nav navbar-nav notika-top-nav">

                            <?php
                              $loggedinUser  = $this->session->userdata('p_user_fname').' '.$this->session->userdata('p_user_lname');
                              if($loggedinUser != ''){
                                $loggedinUserVAl = $loggedinUser;
                              }else{
                                $loggedinUserVAl = 'Trust Deed';
                              }
                            ?>
                           
                            <li class="nav-item"><a id="username_bk" href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle" style="color:#333;">
                                <img src="<?php echo $Imgpath; ?>" style="border-radius: 50%;width: 29px;height: 29px;">&nbsp; <?php echo $loggedinUserVAl;?> </a> 
                                <div role="menu" class="dropdown-menu message-dd chat-dd animated zoomIn" style="left: 0px !important;padding: 6px;width: 100%; ">
                                    <div class="hd-message-info">
                                           
                                       <a href="<?php echo base_url();?>view_details">
                                            <div class="hd-message-sn">
												
                                                <div class="hd-mg-ctn">
                                                    <h3><i class="notika-icon notika-support"></i> &nbsp; My Profile</h3>                                                 
                                                </div>
                                            </div>
                                        </a>
										<a href="<?php echo base_url()?>logout">
                                            <div class="hd-message-sn">                                   
                                                <div class="hd-mg-ctn">
                                                    <h3><i class="notika-icon notika-next"></i> &nbsp; Log Out</h3>                                                 
                                                </div>
                                            </div>
                                        </a>
                                        
                                    </div>
                                    
                                </div>
                            </li>
                        </ul>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- Main Menu area End-->