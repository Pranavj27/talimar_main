<?php
/*echo '<pre>';
print_r($trust_deed_schedule);
echo '</pre>';
*/
$property_image_Erro = base_url() . 'assets/images/no_image.png';
$urival = $this->uri->segment(1);
if($urival == 'trust_deed'){
    $selectoption2 = 'selected';
}elseif($urival == 'trust_deeds'){
    $selectoption1 = 'selected';
}else{
    $selectoption1 = '';
    $selectoption2 = '';
}

$trust_deed_option      = $this->config->item('trust_deed_option');
$position_option        = $this->config->item('position_option');
?>
<style type="text/css">
    
div.blog-inner-list {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
</style>
<div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <!--<h4 class="deed_heading">Trust Deed Portfolio</h4> -->
                        <h3 style="margin: 7px;">Available Trust Deeds</h3>
                    </div>
                    <div class="col-xs-6">
                        <div class="col-xs-3 pull-right">

                            <select class="form-control" onchange="viewpagetype(this.value);">
                                <option value="1" <?php echo $selectoption1;?>>Table</option>
                                <option value="2" <?php echo $selectoption2;?>>Block</option>
                            </select>
                            <!--<a href="<?php echo base_url();?>avaliable_trust_pdf" class="btn btn-primary pull-right">Print Schedule</a>-->
                        </div>
                        <label class="pull-right">View As:</label>
                    </div>
                </div>
                <!--<div class="col-xs-6">
                     
                    <a href="<?php echo base_url();?>avaliable_trust_pdf" class="btn btn-primary pull-right">Print Schedule</a>
                </div>-->
            </div>
            <div class="row">
                <div class="col-md-12">

                <?php 

                $account_status = '';
                $p_contact_id = $this->session->userdata('p_user_id'); 

                $sql = "SELECT * FROM lender_contact_type WHERE contact_id ='".$p_contact_id."'";
                $fetch_contact = $this->User_model->query($sql);
                $fetch_contact = $fetch_contact->row();     
                $account_status = $fetch_contact->lender_td_details;
                

                if(isset($trust_deed_schedule) && is_array($trust_deed_schedule)) {
                        foreach($trust_deed_schedule as $row){ 

                            $ltv = ($row['loan_amount'] + $row['sel_ecum'])/$row['underwriting'] * 100;

                            if($row['trust_deed'] != 3){
                                                                            
                                if($row['servicing_lender_rate'] == 'NaN' || $row['servicing_lender_rate'] == '' || $row['servicing_lender_rate'] == '0.00'){

                                    if($row['invester_yield_percent'] >0){

                                    $lender_ratessss = $row['invester_yield_percent'].'%';
                                }}else{

                                   $lender_ratessss = $row['servicing_lender_rate'].'%';
                                }
                            }else{
                                $lender_ratessss = 'TBD*';
                            }

                            $addclass = '';
                            if($row['trust_deed'] == '2'){
                                $addclass = '';
                                $viewlink = base_url().'trust_deed_details/'.$row['talimar_loan'];
                            }else{
                                $addclass = 'disabled = "disabled"';
                                $viewlink = '#';
                                //$addclass = '';
                            }

                            if($account_status){
                                if($account_status == 'no'){
                                    $addclass = '';
                                    $viewlink = base_url().'error_page';
                                    if($row['trust_deed'] != '2'){
                                        $addclass = 'disabled = "disabled"';
                                        $viewlink = '#';
                                    }
                                }
                            }                   

                ?>

                        <div class="col-md-3">
                            <div class="blog-inner-list notika-shadow mg-t-30 tb-res-ds-n dk-res-ds">
                                <div class="blog-img">
                                    <?php

                                        if($row['get_proprty_img'] !=''){

                                            $propertyinglink = aws_s3_document_url($row['get_proprty_img']);
                                        }else{
                                            $propertyinglink = base_url().'assets/images/default_property.jpg';
                                        }
                                    ?>
                                    <img src="<?php echo $propertyinglink;?>" alt="Property Image" style="width: 100%;height: 30vh;">
                                </div>
                                <div class="blog-ctn">
                                    <div class="blog-hd-sw">
                                       <h2><?php echo $row['city'].', '.$row['state'];?></h2>
                                        <!--<a class="bg-au" href="#">By Malinda on 9th June 2018</a>-->
                                    </div>

                                    <ul class="tab-ctn-list">
                                        <li><b>Available Balance:</b> $<?php echo number_format($row['available']);?></li>
                                        <li><b>Loan Amount:</b> $<?php echo number_format($row['loan_amount']);?></li>
                                        <li><b>Min Investment:</b> $<?php echo number_format($row['lender_minimum']);?></li>
                                        <li><b>Position:</b> <?php echo $position_option[$row['position']];?></li>
                                        <li><b>LTV:</b> <?php echo number_format($ltv,2);?>%</li>
                                        <li><b>Lender Rate:</b> <?php echo $lender_ratessss;?></li>
                                        <li><b>Status:</b> <?php echo $trust_deed_option[$row['trust_deed']];?></li>
                                    </ul>
                                    <p></p>
                                    <a class="btn btn-primary" href="<?php echo $viewlink;?>" <?php echo $addclass;?>>Learn More</a>
                                </div>
                            </div>
                        </div>
                    <?php } }else{ ?>

                            <div class="col-md-4">
                                <h5>No tust deed available!</h5>
                            </div>

                    <?php } ?>

                </div>
            </div>
            
            <div class="row"><br></div>
            <div class="row">
                <div class="col-md-12">
                <div class="col-md-12">
                    <span class="">* TBD = To Be Determined.</span>
                </div>
                </div>
            </div>
        </div>
</div>
<script type="text/javascript">
    
    function viewpagetype(val){

        if(val == 1){
            window.location.href = '<?php echo base_url();?>trust_deeds';
        }else{
            window.location.href = '<?php echo base_url();?>trust_deed';
        }
    }
</script>