<?php
if($this->session->userdata('p_user_id') != ''){
    redirect(base_url().'dashboard');
}else{
    //redirect(base_url().'login');
}
?>
<!DOCTYPE html>
 
<html lang="en-US" dir="ltr">
  <head>
    <title>TaliMar Financial Lender Portal</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>/assets/img/favicon1.png">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover" /> 
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
   
<style> 
* {
    font-family: 'Open Sans', sans-serif !important;
}
  .cont {
    float: left;
    width: 100%;
}

.col-6 {
    float: left;
    width: 50%;
}
body.theme-open-up {
    padding: 0;
    margin: 0;
}
.css-1upilqn {
    margin-bottom: 12px;
}
.input-lg input[type], .form-lg input[type] {
    height: 48px;
    padding: 15px 20px;
}
.bg-image {
    height: 100vh;
    overflow: hidden;
}
h2 {
    color: #123657 !important;
}
.form-outer {
    width: 80% !important;
    padding: 25vh 10vh 10vh 48px;
}

.form-outer label {
    width: 80%;
    float: left;
    color: rgb(4,13,20);
    padding: 3px 0px;
    font-size: 13px; 
    font-weight: 700; 
}
.form-outer input[type="text"], input[type="password"]  {
    width: 80%;
    float: left;
    background: rgb(245 248 250);
    border: 1px solid #3379b5 !important;
    font-size: 13px; 
    padding:8px 7px;
    border-radius: 4px;
    margin-bottom: 6px;
}
.form-outer input[type="text"]:focus, input[type="password"]:focus {
    width: 80%;
    float: left;
    background: #ffffff;
    font-size: 13px; 
    padding:8px 7px;
    border-radius: 4px;
    outline: none;
}
button._1OsoaRGpMCXh9KT8s7wtwm._2GHn41jUsfSSC9HmVWT-eg {
    color: #fff;
    background-color: #3379b5;
    border-color: #3379b5;
    border: 0;
    padding: 11px 37px;
    border-radius: 4px;
    cursor: pointer;
}
.form-div p a { 
    color: #3379b5;
    font-size: 13px;
    text-decoration: none;
    padding: 3px 0px 0px 0px;
    float: left;
    width: 100%;
}

p.error {
    color: red;
    font-weight: 600;
    font-size: 13px;
}

p.successs {
    color: green;
    font-weight: 600;
    font-size: 13px;
}


.form-outer h2 {
    border-bottom: solid 1px #d3d0d0;
    padding-bottom: 16px;
}

.form-div label {
    margin-top: 13px;
}

p.space_b a {
    margin-top: 9px;
}

.bg-image img {
    height: 100vh;
}


@media (max-width: 767px) {

.col-6 {
    float: left;
    width: 100%;
}

.bg-image {
    height: initial !important;
}
.form-outer {
    width: 88%;
    padding: 30px;
}
.bg-image img {
    height: inherit;
}
}
</style>


  </head>
  <body class="theme-open-up">
    <div class="cont">
		<div class="row">
			<div class="col-6">
				<div class="bg-image">
					<img src="<?php echo base_url()?>assets/images/TrustDeed.jpg" width="100%">
				</div> 
			</div>
			<div class="col-6">
				<div class="form-outer">
					<h2>Welcome to TaliMar Financial</h2>
                    <form method="POST" action="<?php echo base_url();?>login">
    					<div class="form-div">
                        <?php if($this->session->flashdata('success')!=''){ ?>
                            <p class="successs"><?php echo $this->session->flashdata('success');?></p>
                        <?php } ?>
                        <?php if($this->session->flashdata('error')!=''){ ?>
                            <p class="error"><?php echo $this->session->flashdata('error');?></p>
                        <?php } ?>

    						<label>Username:</label>
    						<input type="text" name="email" placeholder="Enter Your Username" required="required" autocomplete="off">
    						<label>Password:</label>
    						<input type="password" name="password" placeholder="Enter Your Password" autocomplete="off">
    						<p class="space_b">
                                <a href="<?php echo base_url()?>resetPassword">Forgot your username or password?</a>
                                <a href="<?php echo base_url()?>register">Not Registered? Create new account</a>
                                <a><br></a>
                            </p>

    						<button type="submit" name="submit" class="_1OsoaRGpMCXh9KT8s7wtwm _2GHn41jUsfSSC9HmVWT-eg">Sign In</button>
    				    </div>
                    </form>
			</div>
			
		</div>
	</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">

        $('input[name="email"]').focus();

    </script>
	</body>
</html>
