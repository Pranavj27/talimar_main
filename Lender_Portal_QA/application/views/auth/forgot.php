
<!DOCTYPE html>
 
<html lang="en-US" dir="ltr">
  <head>
    <title>TaliMar Financial Lender Portal</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>/assets/img/favicon1.png">

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover" /> 
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
   
<style>
* {
    font-family: 'Open Sans', sans-serif !important;
}
   .cont {
    float: left;
    width: 100%;
}
.bg-image.log {
    text-align: center;
    min-height: auto;
    padding: 78px 0px 60px 0px;
}

.col-6 {
    float: left;
    width: 50%;
}
body.theme-open-up {
    padding: 0;
    margin: 0;
}
.css-1upilqn {
    margin-bottom: 12px;
}
.input-lg input[type], .form-lg input[type] {
    height: 48px;
    padding: 15px 20px;
}
.bg-image {
    min-height: 100vh;
    float: left;
    width: 100%;
}
h2 {
    color: #123657 !important;
}
 
.form-outer {
    width: 80% !important;
    padding: 25vh 10vh 10vh 48px;
}
.form-outer { 
    text-align: center;
}
input[type="email"] {
    width: 100%;
    float: left;
    background: rgb(245 248 250);
    border: 1px solid #3379b5;
    font-size: 13px; 
    padding:8px 7px;
    border-radius: 4px;
    margin: 33px 4px;
    outline: none;
}

.form-outer label {
        width: 100%;
        float: left;
        color: rgb(4,13,20);
        padding: 9px 0px;
        font-size: 13px;  
}
.form-outer-look input[type="email"] {
    width: 45%;
    float: left;
    background: rgb(245 248 250);
    border: 1px solid #eceeef;
    font-size: 13px; 
    padding:8px 7px;
    border-radius: 4px;
    margin: 0px 1%;
}

.form-outer-look input[type="email"]:focus {
      width: 45%;
    float: left;
    background: #ffffff;
    border: 1px solid #3379b5 !important;
    font-size: 13px; 
    padding:8px 7px;
    border-radius: 4px;
    outline: none;
}
 
.rest-button {
    color: #fff;
    background-color: #3379b5;
    border-color: #3379b5;
    border: 0;
    padding: 10px 37px;
    border-radius: 4px;
    width: 46%;
    margin: 0px 2%;
    font-size: 15px;
    cursor: pointer;
}
.form-div p a { 
    color: #3379b5;
    font-size: 14px;
    text-decoration: none;
    padding: 14px 0px 57px 0px;
    float: left;
    width: 100%;
}
.form-outer-look {
    margin: 12px auto;
    width: 738px;
}
p.tec {
    text-align: center;
    font-style: italic;
    color: #c5c4c4;
    padding: 19px 0px 0px 0px;
}

.css-1jlyxxn {
    border-top-width: 1px;
    border-top-style: solid;
    border-top-color: rgb(180, 189, 194);
    margin: 100px auto 18px;
    width: 1000px;
}
.css-1xb8cnv {
    padding-left: 15px;
    padding-right: 15px;
}
.css-x4k1b4 {
    margin-top: 1em;
    margin-bottom: 1em;
    font-size: 12.75px;
    color: rgb(180, 189, 194);
	font-weight: 300;
    line-height: 1.2;
    letter-spacing: 0.25px;
}

p.error {
    color: red;
    font-weight: 600;
    font-size: 13px;
}

p.successs {
    color: green;
    font-weight: 600;
    font-size: 13px;
}




.form-outer h2 {
    border-bottom: solid 1px #d3d0d0;
    padding-bottom: 16px;
}

.form-div label {
    margin-top: 13px;
}

p.space_b a {
    margin-top: 9px;
}

.bg-image img {
    min-height: 100vh;
}


@media (max-width: 767px) {

.col-6 {
    float: left;
    width: 100%;
}

.bg-image {
    height: initial !important;
}
.form-outer {
    width: 88%;
    padding: 30px !important;
}
.bg-image img {
    height: inherit;
}
.bg-image {
    min-height: inherit !important; 
}

.bg-image img {
  min-height: inherit !important;  
}


.rest-button {
    width: 80% !important;
}



input[type="email"] {
    width: 92%;
}
 
}

 
</style>



  </head>
  <body class="theme-open-up">
    <div class="cont">
		<div class="row">
			<div class="col-6">
			<div class="bg-image">
					<img src="<?php echo base_url()?>assets/images/TrustDeed.jpg" width="100%">
				</div> 
				 
			</div>
			<div class="col-6">
			
			
				<div class="form-outer"> 

			<img src="<?php echo base_url()?>assets/images/logo-big.png" width="300px">
                    <form method="POST" action="<?php echo base_url();?>resetPassword">
    					<div class="form-div">

                        <?php if($this->session->flashdata('success')!=''){ ?>
                            <p class="successs"><?php echo $this->session->flashdata('success');?></p>
                        <?php } ?>
                        <?php if($this->session->flashdata('error')!=''){ ?>
                            <p class="error"><?php echo $this->session->flashdata('error');?></p>
                        <?php } ?>
    						 
    						<input type="email" name="emailreset" required="required" placeholder="Email address" autocomplete="off">
    						<button type="submit" name="email" class="rest-button">Send Reset Instructions</button>
    						<p class="tec">You will receive an email with a link to reset your password.</p>
                            <!--<p class="tec"><a href="<?php echo base_url()?>">Back to Sign In</a></p>-->
    				    </div>
                    </form>
			</div>
			
		</div>
		
	</div>
	</body>
</html>
