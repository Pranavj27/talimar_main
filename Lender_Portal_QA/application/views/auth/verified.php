
<!DOCTYPE html>
 
<html lang="en-US" dir="ltr">
  <head>
    <title>TaliMar Financial Lender Portal</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>/assets/img/favicon1.png">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover" /> 
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
    
<style> 
* {
    font-family: 'Open Sans', sans-serif !important;
}
  .cont {
    float: left;
    width: 100%;
}

.col-6 {
    float: left;
    width: 50%;
}
body.theme-open-up {
    padding: 0;
    margin: 0;
}
.css-1upilqn {
    margin-bottom: 12px;
}
.input-lg input[type], .form-lg input[type] {
    height: 48px;
    padding: 15px 20px;
}
.bg-image {
    height: 100vh;
    float: left;
    width: 100%;
    overflow: hidden;
}
h2 {
    color: #123657 !important;
}
.form-outer {
    width: 60% !important;
    padding: 30px 10vh 3vh 48px;
}

.form-outer label {
        width: 100%;
        float: left;
        color: rgb(4,13,20);
        padding: 2px 0px;
        font-size: 13px;
        font-weight: 700;  
}
.form-outer input[type="text"], input[type="email"], input[type="password"] {
    width: 100%;
    float: left;
    background: rgb(245 248 250);
    border: 1px solid #3379b5 !important;
    font-size: 13px; 
    padding:8px 7px;
    border-radius: 4px;
}
.form-outer input[type="text"]:focus, input[type="email"]:focus, input[type="password"]:focus {
    width: 100%;
    float: left;
    background: #ffffff;
    border: 1px solid #3379b5 !important;
    font-size: 13px; 
    padding:8px 7px;
    border-radius: 4px;
    outline: none;
}
button._1OsoaRGpMCXh9KT8s7wtwm._2GHn41jUsfSSC9HmVWT-eg {
    color: #fff;
    background-color: #3379b5;
    border-color: #3379b5;
    border: 0;
    padding: 8px 37px;
    border-radius: 4px;
    cursor: pointer;
}
.form-div p a { 
    color: #3379b5;
    font-size: 14px;
    text-decoration: none;
    padding: 14px 0px 0px 0px;
    float: left;
    width: 100%;
}

a{
    cursor: pointer;
    font-size: 13px;
    text-decoration: none;
}

p.error {
    color: red;
    font-weight: 600;
    font-size: 13px;
}

p.successs {
    color: green;
    font-weight: 600;
    font-size: 13px;
}

.btn {
    border-width: 0;
    padding: 8px 14px;
    font-size: 13px;
    outline: none !important;
    background-image: none !important;
    filter: none;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
    text-shadow: none;
    border-radius: 4px;
}

.btn-primary {
    color: #fff;
    background-color: #428bca;
    border-color: #357ebd;
}




.form-outer h2 {
    border-bottom: solid 1px #d3d0d0;
    padding-bottom: 16px;
}

.form-div label {
    margin-top: 13px;
}

p.space_b a {
    margin-top: 9px;
}

.bg-image img {
    height: 100vh;
}


@media (max-width: 767px) {

.col-6 {
    float: left;
    width: 100%;
}

.bg-image {
    height: initial !important;
}
.form-outer {
    width: 88%;
    padding: 30px;
}
.bg-image img {
    height: inherit;
}
}

/*03-02-2021*/
.username_errors img {
    width: 24px;
}
.username_errors {
    position: inherit;
    float: right;
    margin-top: -30px;
}
.Scriptcontent_Bx {
    position: relative;
    width: 100%;
    display: inline-block;
    margin: 20px 0 30px;
    text-align: center;
    border-radius: 8px;
}
.upper-side {
    padding: 2em;
    background-color: #103a60;
    display: block;
    color: #fff;
    border-top-right-radius: 8px;
    border-top-left-radius: 8px;
}
.lower-side {
    padding: 2em 2em 5em;
    background: #fafafa;
    display: block;
    border-bottom-right-radius: 8px;
    border-bottom-left-radius: 8px;
}
.status_title {
    font-weight: lighter;
    text-transform: uppercase;
    letter-spacing: 2px;
    font-size: 1em;
    margin-top: 15px;
    margin-bottom: 0;
    font-weight: 600;
}
.message_para {
    margin-top: -.5em;
    color: #757575;
    letter-spacing: 1px;
}
.contBtn {
    position: relative;
    top: 1.5em;
    text-decoration: none;
    background: #103a60;
    color: #fff;
    margin: auto;
    padding: .6em 3em;
    -webkit-box-shadow: 0 15px 30px rgba(50,50,50,.21);
    -moz-box-shadow: 0 15px 30px rgba(50,50,50,.21);
    box-shadow: 0 15px 30px rgba(50,50,50,.21);
    border-radius: 25px;
    -webkit-transition: all .4s ease;
    -moz-transition: all .4s ease;
    -o-transition: all .4s ease;
    transition: all .4s ease;
    font-size: 17px;
}
.contBtn:hover {
    -webkit-box-shadow: 0 15px 30px rgba(50,50,50,.41);
    -moz-box-shadow: 0 15px 30px rgba(50,50,50,.41);
    box-shadow: 0 15px 30px rgba(50,50,50,.41);
    -webkit-transition: all .4s ease;
    -moz-transition: all .4s ease;
    -o-transition: all .4s ease;
    transition: all .4s ease;
}
/*03-02-2021*/
</style>


  </head>
  <body class="theme-open-up">
    <div class="cont">
		<div class="row">
			<div class="col-6">
				<div class="bg-image">
					<img src="<?php echo base_url()?>assets/images/TrustDeed.jpg" width="100%">
				</div> 
			</div>
			<div class="col-6">
				<div class="form-outer">
					<h2>Welcome to TaliMar Financial</h2>
                 

<div class="Scriptcontent_Bx">
<div class="upper-side">



<svg xmlns="http://www.w3.org/2000/svg" fill="none" height="64" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" width="64"><path d="M22 11.07V12a10 10 0 1 1-5.93-9.14"/><polyline points="23 3 12 14 9 11"/></svg>
<h3 class="status_title">
Registration Successful
</h3>
</div>
<div class="lower-side">
<p class="message_para">
Congratulations, your account has been successfully created.
</p>
<!-- <a href="<?php //echo base_url(); ?>" class="contBtn">Sign In</a> -->
</div>
</div>
                    
			</div>
			
		</div>
	</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script>
    <script type="text/javascript">
        
        $(document).ready(function(){
          /***phone number format***/
            $(".phone-format").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                  return false;
                }
                var curchr = this.value.length;
                var curval = $(this).val();
                if (curchr == 3 && curval.indexOf("(") <= -1) {
                  $(this).val("(" + curval + ")" + "-");
                } else if (curchr == 4 && curval.indexOf("(") > -1) {
                  $(this).val(curval + ")-");
                } else if (curchr == 5 && curval.indexOf(")") > -1) {
                  $(this).val(curval + "-");
                } else if (curchr == 9) {
                  $(this).val(curval + "-");
                  $(this).attr('maxlength', '14');
                }
            });

        });
    </script>
	</body>
</html>
