<?php

	$account_status_option         		= $this->config->item('account_status_option');
?>
<style type="text/css">
	h4.deed_heading {
	    background-color: #ececec;
	    padding: 12px;
	}
	.hd-message-sn {
    display: flex;
    padding: 6px 15px;
}
.table.table-striped>tbody>tr>td, .table.table-striped>tbody>tr>th, .table.table-striped>tfoot>tr>td, .table.table-striped>tfoot>tr>th, .table.table-striped>thead>tr>td, .table.table-striped>thead>tr>th {
    
    padding: 8px;
}
.table.table-striped>thead>tr>th {
    min-width: 104px;
    font-size: 13px !important;
}
tfoot tr th {
	border: 1px solid #e7ecf1;
}
table#data-table-basicss td {
    border: 1px solid #e7ecf1;
}
body .table.table-striped tfoot {
    background-color: #fff !important;
}
</style>
	<div class="notika-status-area">

        <div class="container">

            <div class="row">

				<div class="col-xs-12">
					<!--<h4 class="deed_heading">Dashboard</h4> -->
					<h4>Dashboard</h4> 
				</div>
				<div class="row"><br>&nbsp;<br></div>

                <div class="col-md-12">
                    
                           <table id="data-table-basicss" class="table table-striped">
                                <thead style="background-color: #f2f2f2;">
                                    <tr>
                                        <th style="width:21%">Account Name</th>
                                        <th style="width:10%">Lender ID</th>
                                        <th style="width:10%">FCI Account #</th>
                                        <th style="width:5%">Account Status</th>
                                        <th style="width:10%"># of Loans</th>
                                        <th style="width:15%">Active Balance</th>
                                        <th style="width:10%">Monthly Income</th>
                                        <th style="width:10%">Average Yield</th>
                                        <th style="width:5%">View Portfolio</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
								<?php
								$count = 0;
								$total_avrg_yieldd = 0;
								$total_active_balance = 0;
								$total_monthly = 0;
								$totaldeed_count = 0;
							
					            
								foreach($dashboard_data as $row){

								$count++;
								$total_active_balance +=$row['active_bal'];
								//$total_avrg_yield +=($row['avg_yieldd']);
								$total_monthly +=$row['lender_payment'];
								$totaldeed_count +=$row['total_count'];

								if($row['avg_yieldd'] / $row['total_count'] > 0 ){ 

									$total_avrg_yieldd += $row['avg_yieldd'] / $row['total_count'];
								}else{
									//$total_avrg_yieldd += 0;
								}
								?>
									<tr>
										<td style="width:18%"><a href="<?php echo base_url().'active_trust_deeds/'.$row['id'];?>"><?php echo $row['account_name'];?></a></td>
										<td style="width:10%"><?php echo $row['talimar_lender'];?></td>
										<td style="width:10%"><?php echo $row['fci_acct'];?></td>
										<td style="width:10%"><?php echo $account_status_option[$row['account_status']];?></td>
										<td style="width:5%"><?php echo $row['total_count'];?></td>
										<td style="width:14%">$<?php echo number_format($row['active_bal'],2);?></td>
										<td style="width:5%">$<?php echo number_format($row['lender_payment'],2);?></td>
									
										<td style="width:10%"><?php if(($row['avg_yieldd']/$row['total_count'])>0){ echo number_format(($row['avg_yieldd']/$row['total_count']),2).'%';}else{echo "0.00%";}?></td>
										
											<td style="width:10%"><a href="<?php echo base_url().'active_trust_deeds/'.$row['id'];?>" class="btn btn-primary pull-right waves-effect btn-xs">view</a></td>
									</tr>
									
								<?php  } ?>
								 	
								     <!-- <tr>
								     	<td colspan="9">
								     		<a href="<?php echo base_url();?>Home/addNewAccount" class="btn btn-primary pull-left waves-effect">Add Account</a>
								     	</td>
								     </tr> -->
								</tbody>
								
                                <tfoot style="background-color: #f2f2f2;">
								   <tr> 
									  	<th>Total: <?php echo $count;?></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th><?php echo $totaldeed_count;?></th>
                                        <th>$<?php echo number_format($total_active_balance,2);?></th>
                                        <th>$<?php echo number_format($total_monthly,2);?></th>

                                        <?php 

	                                        $checkaverage = number_format(($total_monthly*12)/$total_active_balance*100,2);
	                                        if($checkaverage == 'NaN' || $checkaverage == 'nan'){
	                                        	$checkaverageVAl = '0.00%';
	                                        }else{
	                                        	$checkaverageVAl = $checkaverage.'%';
	                                        }

                                        ?>
                                        <th>
                                        	<?php 
                                        	if($count > 0){
                                        		echo number_format($total_avrg_yieldd/$count,2);
                                        	}else{
                                        		echo number_format(0);
                                        	}
                                        	?>% 
                                        	<!-- * --></th>
                                        <th></th>
                                        
                                    </tr>
								
                                </tfoot>
                            </table>
                    	

                </div>
                <!--<div class="col-md-12">
                	<a href="<?php echo base_url();?>add_edit_account" class="btn btn-primary pull-right waves-effect">[+] Add Account</a> 
                </div>-->

            </div>

            <div class="row">
            	<div class="col-md-12">
            		<span class="">* Represents weighted average calculated by total monthly payments / total active balance.</span>
            	</div>
            </div>

        </div>

    </div>
    <script type="text/javascript">
    	
    $(document).ready(function(){

		$('#data-table-basic').DataTable({
			"searching":false,
			// "pageLength":25,
			"bLengthChange":false,

		});	
	});	


	
    </script>

    <!-- End Status area-->

   

  