<?php
function send_mail_template($data = array()){
	ob_start();

	$from_email 	= '';
	$from_name 		= '';
	$to 			= '';
	$name 			= '';
	$subject 		= '';
	$content_body 	= '';
	$button_url 	= '';
	$button_text 	= '';

	if($data['from_email']){
		$from_email = $data['from_email'];
	}
	if($data['from_name']){
		$from_name = $data['from_name'];
	}
	if($data['to']){
		$to = $data['to'];
	}
	if($data['name']){
		$name = $data['name'];
	}
	if($data['subject']){
		$subject = $data['subject'];
	}
	if($data['content_body']){
		$content_body = $data['content_body'];
	}
	if($data['button_url']){
		$button_url = $data['button_url'];
		$button_text = $data['button_text'];
	}

	?>
	<!doctype html>
		<html>

		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta name="format-detection" content="telephone=no" />
			<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;" />
			<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
			<title>Welcome To Talimar Financial</title>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">
			<style>
				@import url("https://fonts.googleapis.com/css?family=Poppins:400,500,100%,700&display=swap");
				.cst-btn {
				background-color: #4eb8f6;
				/* font-family: '."'".'Poppins'."'". */
				}
				.cst-btn:hover {
				background-color: #108ed8;
				}
				@media only screen and (max-width:600px) {
				table[class=box] {
				width: 100%
				}
				td[class=head-title] {
				font-size: 24px !important;
				padding-bottom: 10px !important;
				line-height: 32px !important;
				}
				table[class=mb_Super] {
				width: 100%;
				}
				div[class=mb_View] {
				width: 96% !important;
				padding-left: 2%;
				padding-right: 2%; 
				}
				}
				@media only screen and (max-width:480px) {
				td[class=head-title] {
				font-size: 18px !important;
				}
				a[class=cst-btn] {	
				padding-left: 12px !important;
				padding-right: 12px !important;
				font-size: 13px !important;
				}
				img[class=logo_img] {
				width: 95px !important;
				}
				}
				@media only screen and (max-width:400px) {
				td[class=title1] {
				font-size: 18px !important;
				}
				td[class=footer-items] {
				font-size: 11px !important;
				}
				td[class=ic-text] {
				font-size: 16px !important;
				}
				td[class=link-item] {
				font-size: 18px !important;
				}

				td[class=up_Number] {
				text-align: right;
				}
				td[class=redu_Space] {
				padding-left: 15px;
				padding-right: 15px;
				}
				}
			</style>
		</head>

		<body style="margin:0px; padding:0px; font-family: 'Poppins',sans-serif!important; color:#333;" bgcolor="#f4f4f4">
			<table align="center" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td>
						<div class="mb_View" style="background-color: #f4f4f4 !important; max-width:600px; margin: 0 auto;">&nbsp;
							<br>&nbsp;
							<br>
							<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td>
										<table width="100%" cellpadding="0" cellspacing="0">
											<tr>
												<td class="logo_block" bgcolor="" style="padding-top:5px; padding-bottom:26px; text-align: center;">	<a href="#m_-1778674465433749821_m_-1945850375391101209_m_-6818727870886611857_" target="_blank" target="_blank"> 
				<img src="<?php echo base_url(); ?>assets/images/logo-big.png" width="136px" class="logo_img"> 
				</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" cellpadding="0" cellspacing="0" align="center">
											<tr>
												<td style="padding-left:20px; padding-right:20px; padding-top:37px; padding-bottom:40px; border-radius: 5px 5px 0 0;" bgcolor="#FFFFFF">
													<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
														<tbody>
															<tr>
																<td>
																	<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
																		<tbody>
																			<tr>
																				<td class="head-title" style="font-family: 'Poppins',sans-serif !important; font-size:20px; padding-bottom:20px; color:#000; line-height:30px; font-weight:500; text-align:center;"></td>
																			</tr>
																			<tr>
																				<td style="padding-top:1px; padding-bottom:7px; font-family: 'Poppins',sans-serif !important; color:#233047; font-size:15px; line-height:24px; font-weight: 500">
																					<?php if($name){ ?>
																					Hi <?php echo $name; ?>,
																					<?php } ?>
																				</td>

																			</tr>
																			<tr>
																				<td style="font-family: 'Poppins',sans-serif !important; font-size:14px; padding-top:10px;  color:#141414; line-height:24px; padding-bottom:30px;" class="para1">
																				<?php echo $content_body; ?>
																				</td>
																			</tr>
																			<tr>
																				<td style="padding-bottom: 20px;">
																					<?php if($button_url){ ?>
																					<table style="background-color: #093164; border: 1px solid #093164; border-radius: 4px;" cellspacing="0" cellpadding="0" align="center">
																						<tbody>
																							<tr>
																								<td style="padding-left: 15px; padding-right: 15px; text-align: center;" valign="middle" height="50" width="164">
																									<?php if($button_url){ ?>
																									<a href="<?php echo $button_url; ?>" target="_blank"  style="font-family: 'Poppins',sans-serif !important; border-radius: 4px; font-size: 14px; line-height: 14px; color: #fff; text-decoration: none;">
																				                        <span><?php echo $button_text; ?>
																				                        </span>
																				                      </a>
																				                  	<?php } ?>
																								</td>
																							</tr>
																						</tbody>
																					</table>
																					<?php } ?>
																				</td>
																			</tr>
																			<tr>
																				<td style="padding-top:0px; padding-bottom:0px; font-family: 'Poppins',sans-serif !important; color:#141414; font-size:14px; line-height:24px;">Regards,</td>
																			</tr>
																			<tr>
																				<td style="padding-top:0px; padding-bottom:0px; font-family: 'Poppins',sans-serif !important; color:#141414; font-size:14px; line-height:24px;">Investor Relations</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td bgcolor="#efeeed" style="padding-left:11px; padding-right:10px; padding-top:15px; padding-bottom:15px; border-radius: 0 0 5px 5px;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="text-align:center; font-size:14px; line-height:14px; font-family: 'Poppins',sans-serif !important; color:#141414; line-height:23px;">Copyright © 2020 | TaliMar Financial Inc.</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</table>
						</div>
				</tr>
				</td>
			</table>
		</body>

		</html>
	<?php

	$content = ob_get_contents();

	$CI =& get_instance();
	$CI->load->library('email');

	$CI->email->initialize(SMTP_SENDGRID);
    $CI->email->from($from_email, $from_name);
    $CI->email->to($to);        
    $CI->email->subject($subject);
    $CI->email->message($content);
    $CI->email->set_mailtype('html'); 
    $CI->email->send();

	ob_end_clean();
	return $content;
}