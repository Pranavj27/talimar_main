<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//    in loan data tab

$config['trustdeed_filter_option']	= array(
                                        '' => 'Select All',
                                        2 => 'Boarding',
                                        1 => 'Active',
                                        
                                     );

$config['loan_payment_status'] = array(
	
	1 => 'Current',
	2 => '10+ Days Late',
	3 => '30+ Days Late',
	
);

$config['notification_setting'] = array(
	
	2 => 'E-Mail',
	7 => 'Mail',
	9 => 'Text *',
	
);

$config['payment_gurranty_option'] = array(
	
	1 => 'Yes',
	3 => 'No',
	2 => 'Not Applicable',
	
);

$config['account_status_option'] = array(

	'' => 'Select One',
	1 => 'Processing',
	2 => 'Active',
	3 => 'Frozen',
	4 => 'Closed',
);

$config['borrower_employment_status'] = array(
	'' => 'Select One',
	1 => 'Employed',
	2 => 'Retired',
	3 => 'Self-Employed',
	4 => 'Unemployed ',

);


$config['borrower_option']	= array(
                                        1 => 'one',
                                        2 => 'two',
                                        3 => 'three',
                                        4 => 'four'
                                       
                                       );

$config['trust_deed_option']	= array(
                                        1 => 'No',
                                        2 => 'Available',
                                        3 => 'Coming Soon',
                                        4 => 'Fully Subscribed'
                                       
                                       );

$config['phone_type_option']	= array(
                                        '' => '',
                                        1 => 'Mobile',
                                        2 => 'Home',
                                        3 => 'Business',
                                        4 => 'Fax'
                                       
                                       );
									   
$config['garge_option']	= array(
                                        0 => 'Zero',
                                        1 => 'one',
                                        2 => 'two',
                                        3 => 'three',
                                        4 => 'four',
                                        5 => 'five'
                                       
                                       );
									   
									   
$config['loan_doc_type'] = array(		
										
										1 => 'Promissory Note',
										2 => 'Deed of Trust',
										3 => 'Final Closing Statement',
										4 => 'Assignment 1',
										5 => 'Preliminary Title Report',
										6 => 'Property Report',
										0 => 'Add New'
										);

$config['yes_no_not_applicable_option'] = array(		
										'' => 'Select One',
										3 => 'No',
										1 => 'Yes',
										2 => 'Not Applicable'
										);

$config['usa_city_county'] = array(
	0 => "",
	1 => "Alameda",
	2 => "Alpine",
	3 => "Amador",
	4 => "Butte",
	5 => "Calaveras",
	6 => "Contra Costa",
	7 => "Del Norte",
	8 => "El Dorado",
	9 => "Fresno",
	10 => "Glenn",
	11 => "Humboldt",
	12 => "Imperial",
	13 => "Inyo",
	14 => "Kern",
	15 => "King",
	16 => "Lake",
	17 => "Lassen",
	18 => "Los Angeles",
	19 => "Madera",
	20 => "Marin",
	21 => "Mariposa",
	22 => "Mandocino",
	23 => "Merced",
	24 => "Modoc",
	25 => "Mono",
	26 => "Monterey",
	27 => "Napa",
	28 => "Nevada",
	29 => "Orange",
	30 => "Placer",
	31 => "Plumas",
	32 => "Riverside",
	33 => "Sacramento",
	34 => "San Benito",
	35 => "San Bernardino",
	36 => "San Diego",
	37 => "San Francisco",
	38 => "San Joaquin",
	39 => "San Luis Obispo",
	40 => "San Mateo",
	41 => "Santa Barbara",
	42 => "Santa Clara",
	43 => "Santa Cruz",
	44 => "Shasta",
	45 => "Sierra",
	46 => "Siskiyou",
	47 => "Solano",
	48 => "Sonoma",
	49 => "Stanislaus",
	50 => "Sutter",
	51 => "Tehama",
	52 => "Trinity",
	53 => "Tulare",
	54 => "Tuolumne",
	55 => "Ventura",
	56 => "Yolo",
	57 => "Yuba",
	58 => "Maui",
);	


$config['yes_no_option'] = array(		
										'' => 'Select One',
										0 => 'No',
										1 => 'Yes'
										);

$config['yes_no_option1'] = array(		
										'' => 'Select One',
										1 => 'No',
										2 => 'Yes'
										);
										
$config['yes_no_option2'] = array(		
										'' => 'Select One',
										1 => 'Yes',
										0 => 'No',
										2 => 'Not Required'
										);
										
$config['loan_type_option'] = array(
										'' => 'Select One',
										1 	=> 'Bridge',
										2 	=> 'Construction (Existing)',
										3 	=> 'Construction (New)'
										
										);

$config['loan_type_optionadmin'] = array(
							'' => '',
							1 => 'Bridge',
							2 => 'Fix and Flip',
							4 => 'Fix and Hold',
							3 => 'New Construction',
							5 => 'Other',
							6 => 'Conventional',
							7 => 'Construction',
							8 => 'Line of Credit',
							9 => 'Auto',
							10 => 'Business Perpose Loan',
							11 => 'Cash Advance',
							12 => 'Fannie Mae',
							13 => 'Fha',
							14 => 'Freddie Mac',
							15 => 'Hecm',
							16 => 'Hud',
							17 => 'Lease',
							18 => 'Personal',
							19 => 'Purchase Contract',
							20 => 'Unsecured',
							21 => 'VA',
							22 => 'Securitized Loan',
							23 => 'Draw Loan',
						);


$config['marketing_loan_type_option'] = array(
										'' => 'Select One',
										1 	=> 'Fix and Flip Loan',
										2 	=> 'Construction Loan',
										4 	=> 'Bridge Loan'
										
										);
										
$config['transaction_type_option'] = array(
										'' => 'Select One',
										1 	=> 'Purchase Loan',
										2 	=> 'Refinance Loan',
										3 	=> 'Cash Out Refinance'
										
									);
										
										
$config['position_option'] = array(
										'' => 'Select One',
										1 => '1st',
										2 => '2nd',
										3 => '3rd',
										4 => '4th',
										
										);

$config['marital_status'] = array(
										'' => 'Select One',
										1 => 'Single',
										2 => 'Married',
										3 => 'Divorced',
										5 => 'Widowed',
										4 => 'Other',
										
										);
										
$config['loan_payment_type_option'] = array(
										1 => 'Interest Only',
										2 => 'Amortization',
										3 => 'Partial Amortization'
										);
										
$config['loan_payment_schedule_option'] = array(
										'30' => 'Monthly',
										'7' => 'Weekly',
										'90' => 'Quaterly',
										'365' => 'Yearly',
										'paid' => 'Paid off'
										);	
										
$config['loan_intrest_type_option'] = array(
										1 => 'Fixed',
										2 => 'Adjustable'
										);
									


										
$config['STATE_USA'] 				= array(
										""=> "Select One",
										"AL" => "Alabama",
										"AK" => "Alaska",
										"AZ" => "Arizona",
										"AR" => "Arkansas",
										"CA" => "California",
										"CO" => "Colorado",
										"CT" => "Connecticut",
										"DE" => "Delaware",
										"FL" => "Florida",
										"GA" => "Georgia",
										"HI" => "Hawaii",
										"ID" => "Idaho",
										"IL" => "Illinois",
										"IN" => "Indiana",
										"IA" => "Iowa",
										"KS" => "Kansas",
										"KY" => "Kentucky",
										"LA" => "Louisiana",
										"ME" => "Maine",
										"MD" => "Maryland",
										"MA" => "Massachusetts",
										"MI" => "Michigan",	
										"MN" => "Minnesota",
										"MS" => "Mississippi",		
										"MO" => "Missouri",
										"MT" => "Montana",
										"NE" => "Nebraska",
										"NV" => "Nevada",
										"NH" => "New Hampshire",
										"NJ" => "New Jersey",
										"MS" => "New Mexico",
										"NM" => "New York",
										"NC" => "North Carolina",
										"ND" => "North Dakota",
										"OH" => "Ohio",
										"OK" => "Oklahoma",
										"OR" => "Oregon",
										"PA" => "Pennsylvania",
										"RI" => "Rhode Island",
										"SC" => "South Carolina",
										"SD" => "South Dakota",
										"TN" => "Tennessee",
										"TX" => "Texas",
										"UT" => "Utah",
										"VT" => "Vermont",
										"VA" => "Virginia",
										"WA" => "Washington",
										"WV" => "West Virginia",
										"WI" => "Wisconsin",
										"WY" => "Wyoming "
										
										);
										
	// -----        -----  Loan Data property modal -----   ------    ----
	
										
$config['property_modal_property_type'] = array(
										0 => 'Select One',
										1 => 'Residential (1-4 units)',
										2 => 'Residential (4+ units)',
										3 => 'Commercial',
										4 => 'Industrial',
										5 => 'Land',
										6 => 'Retail',
										7 => 'Church',
										8 => 'Gas Station',
										9 => 'Mobile Home'
										);

$config['marketing_property_type'] = array(
										0 => 'Select One',
										1 => 'Single Family Home',
										5 => 'Condominium',
										6 => 'Duplex',
										7 => 'Triplex',
										12 => '4-Plex',
										2 => 'Multi-Family',
										3 => 'Commercial',
										4 => 'Land',
										8 => 'Manufactured Home',
										9 => 'Retail',
										10 => 'Mixed-Use',
										11 => 'Agriculture',
									);	

$config['property_modal_property_condition'] = array(
										0 => 'Select One',
										1 => 'Poor',
										2 => 'Moderate',
										3 => 'Good',
										4 => 'Excellent',
										5 => 'New'
										);
										
$config['property_modal_valuation_type'] = array(
										1 => 'Appraiser',
										2 => 'Broker Price Opinion'
										
										);	
										
	
$config['valuation_type'] = array(
										0 => 'Select One',
										1 => 'Appraisal',
										2 => 'Broker Price Opinion',
										3 => 'None',
										4 => 'Internal Valuation',
										
										);	
$config['relationship_type'] = array(
										0 => 'Select One',
										1 => 'Employee',
										2 => 'Agent',
										3 => 'Independent Contractor'
										
										);	
										

	
$config['title_lender_policy_type_option'] 		= array(
													1 => 'ALTA Policy',
													2 => 'CLTA policy'
													);
													
$config['junior_senior_option'] 		= array(
													1 => 'Junior',
													2 => 'Senior'
													);	
													
$config['loan_purpose_option'] 		= array(
													1 => 'Consumer',
													2 => 'Business'
													);
													
$config['loan_status_option']				 	= array(
													0 => 'Select All',
													1 => 'Pipeline',
													2 => 'Active',
													3 => 'Paid Off',
													4 => 'Dead',
													5 => 'Brokered'
														);	
														
$config['closing_status_option']				= array(
													0 => 'Select One',
													6 => 'Loan Closed',
													5 => 'Funds Released',
													4 => 'Documents Signed',
													3 => 'Documents Released',
													2 => 'Underwriting',
													1 => 'Term Sheet',	
													7 => 'POF Letter',
													
												);

$config['boarding_status_option']				= array(
													'' => '',
													0 => 'Not Submitted',
													1 => 'Loan Submitted',
													2 => 'Loan Boarded',
													//3 => 'File Closed'
												);	
														
$config['serviceing_reason_option']				 	= array(
													0 => '',
													1 => 'Late Payment',
													2 => 'Maturity',
													3 => 'Other'
													);	
													
$config['serviceing_condition_option']				 	= array(
													0 => '',
													1 => 'Default',
													2 => 'Current'
													);	
													
$config['notes_type']				 	= array(
													0 => 'Select One',
													1 => 'Email',
													2 => 'Phone',
													3 => 'Letter',
													4 => 'Meeting',
													5 => 'Other'
												);	
$config['contact_type']				 	= array(
													
													1 => 'Borrower',
													2 => 'Trust Deed Investor'
												);	
$config['contact_specialty']				 	= array(
													
													1 => 'Appraiser',
													2 => 'Attorney',
													3 => 'Agent / Broker',
													4 => 'Architect / Engineer',
													5 => 'Banker',
													6 => 'Contractor',
													7 => 'Escrow / Title',
													8 => 'Hard Money Lender',
													9 => 'Insurance Agent',
													10 => 'IRA Specialist',
													11 => 'Financial Planner',
													12 => 'Loan Servicer',
													13 => 'Mortgage Broker',
													22 => 'Probate',
													14 => 'Property Inspector',
													15 => 'Property Manager',
													20 => 'Property Owner',
													18 => 'Real Estate Investor ',
													19 => 'Transaction Coordinator',
													16 => 'Trust Deed Investor',
													17 => 'Wholesaler',
													21 => 'Gap Lender',
													
												);
												
$config['contact_email_blast']				 	= array(
													
													1 => 'Email – Wholesale Deal',
													2 => 'Email – Trust Deed',
													3 => 'Email – Hard Money',
													6 => 'Mail – Borrower',
													7 => 'Mail – Trust Deed Investor',
													4 => 'Opt Out - Mail',
													5 => 'Opt Out – E-Mail'
												);	
												
$config['contact_permission']				 	= array(
													
													2 => 'Everyone',
													1 => 'Admin Only',
													//3 => 'Only Selected user',
													//4 => 'Borrower Dept',
													//5 => 'Lender Dept',
													6 => 'Loan Servicing',
													7 => 'Brock VandenBerg',
													8 => 'Sarah Hosseini'
													
								);													
													
$config['serviceing_agent_option']				 	= array(
													0 => '',
													1 => 'TaliMar Financial',
													2 => 'FCI Lender Services',
													3 => 'Del Toro Loan Servicing',
													);	
													
$config['serviceing_sub_agent_option']				 	= array(
													
													14 => 'FCI Lender Services',
													15 => 'TaliMar Financial',
													19 => 'Del Toro Loan Servicing',
													
													);	
													
$config['serviceing_who_pay_servicing_option']	= array(
													'' => 'Select One',
													1 => 'Broker',
													2 => 'Lender'
													);													
$config['company_type_option']				 	= array(
													0 => '',
													1 => 'Corporation',
													2 => 'Limited Liability Company',
													3 => 'Limited Partnership',
													4 => 'Other',
													5 => 'Personal',
													6 => 'Self Directed IRA',
													7 => 'Trust'
													);
													


$config['investor_type_option'] = array(
	0 => 'Select One',
	1 => 'Individual(s)',
	2 => 'Limited Liability Company',
	9 => 'C Corp',
	6 => 'S Corp',
	7 => 'Limited Partnership',
	3 => 'Self Directed IRA',
	4 => 'Trust',
	5 => '401k',
	8 => 'Sole Proprietorship',

);	



// $config['investor_type_option']					= array(
// 														0 => 'Select One',
// 														1 => 'Personal',
// 														2 => 'Limited Liability Company',
// 														6 => 'Corp',
// 														7 => 'Limited Partnership',
// 														3 => 'Self Directed IRA custodian',
// 														4 => 'Trust',
// 														5 => '401k'

// 														);		

														
$config['encumbrances_loan_type']					= array(
														0 => '',
														1 => 'Conventional',
														2 => 'Hard Money'
														);													
$config['dead_reason_option']					= array(
														0 => '',
														1 => 'Loan Cancelled',
														2 => 'Lost to Competitor'
														);
														
$config['usa_city_county']						= array(
														0 => "",
														1 => "Alameda",
														2 => "Alpine",
														3 => "Amador",
														4 => "Butte",
														5 => "Calaveras",
														6 => "Contra Costa",
														7 => "Del Norte",
														8 => "El Dorado",
														9 => "Fresno",
														10 => "Glenn",
														11 => "Humboldt",
														12 => "Imperial",
														13 => "Inyo",
														14 => "Kern",
														15 => "King",
														16 => "Lake",
														17 => "Lassen",
														18 => "Los Angeles",
														19 => "Madera",
														20 => "Marin",
														21 => "Mariposa",
														22 => "Mandocino",
														23 => "Merced",
														24 => "Modoc",
														25 => "Mono",
														26 => "Monterey",
														27 => "Napa",
														28 => "Nevada",
														29 => "Orange",
														30 => "Placer",
														31 => "Plumas",
														32 => "Riverside",
														33 => "Sacramento",
														34 => "San Benito",
														35 => "San Bernardino",
														36 => "San Diego",
														37 => "San Francisco",
														38 => "San Joaquin",
														39 => "San Luis Obispo",
														40 => "San Mateo",
														41 => "Santa Barbara",
														42 => "Santa Clara",
														43 => "Santa Cruz",
														44 => "Shasta",
														45 => "Sierra",
														46 => "Siskiyou",
														47 => "Solano",
														48 => "Sonoma",
														49 => "Stanislaus",
														50 => "Sutter",
														51 => "Tehama",
														52 => "Trinity",
														53 => "Tulare",
														54 => "Tuolumne",
														55 => "Ventura",
														56 => "Yolo",
														57 => "Yuba"
														);
														
$config['borrower_type_option']				= array(
													0 => '',
													7=>'Corporation',
													2=>'Limited Liability Company',
													3=>'Limited Partnership',
													4=>'Other',
													5=>'Trust',
													6=>'Self Directed IRA',
													1 => 'Individual(s)'
													);			

$config['broker_capacity_option']				= array(
													0 => '',
													2 => 'New Loan (RE851A)',
													1 => 'Existing Loan (RE851B)'
													);	
													
$config['entity_type_option']				= array(
													
													1 => 'LLC',
													//2 => 'Inc.',
													2 => 'Corp',
													3 => 'Trust',
													4 => 'IRA',
													5 => '401k',
													6 => 'Other',
													7 => 'Defined Benefit Plan',
													
													);
													
$config['account_type_option']				= array(
													0 => '',
													1 => 'Checking',
													2 => 'Savings'
													);
													
$config['search_by_option']				= array(
													''=> 'Select One',
													3 => 'Property Address',
													1 => 'TaliMar #',
													2 => 'Servicer Loan #',
													// 4 => 'Contact Name',
													// 5 => 'Borrower Name'
													);				


$config['responsibility']				= array(
													'' => 'Select One',
													1 => 'Yes',
													2 => 'No'
												);				


													
$config['ach_account_type_option']				= array(
													0 => '',
													1 => 'Business',
													2 => 'Personal'
													);	
													
$config['payment_option_distribution']				= array(
													0 => '',
													1 => 'ACH',
													2 => 'Mail'
													);	
													
$config['fee_disbursement_list']				= array(
													0 => 'Late Fee',
													1 => 'Extension Fee',
													2 => 'Minimum Interest',
													3 => 'Default Rate',
													);	
													
$config['paidoff_status_option']				= array(
													0 => '',
													1 => 'Good Standing',
													2 => 'Default',
													3 => 'Trustee Sale',
													);	
													
$config['escrow_account_option']				= array(
													0 => '',
													1 => 'La Mesa Fund Control',
													2 => 'Del Toro Loan Servicing',
													3 => 'FCI Lender Services',
													4 => 'Other',
													);	
													
$config['calc_day_option']							= array(
													0 => '',
													1 => '360 days',
													2 => '365 days'
													);													
$config['construction_status_option']				= array(
													0 => '',
													1 => 'Active',
													2 => 'Complete'
													);													
$config['min_bal_type_option']				= array(
													0 => '',
													1 => 'Original Loan Balance',
													2 => 'Unpaid Balance'
													);													
$config['will_option']						= array(
													0 => 'Select One',
													1 => 'May',
													2 => 'Will',
													3 => 'Will Not',
													);
													
$config['vendor_type']						= array(
													
													1 => 'Loan Servicer',
													2 => 'Fund Control',
													3 => 'Escrow Company',
													4 => 'Title Company',
													5 => 'IRA Custodian',
												);
													
$config['closing_statement_item_loads']		= array(

													'800'	=>	'Items Payable in Connection with Loan',			
													'801'	=>	'Lender’s Loan Origination Fee',
													'803'	=>	'Appraisal Fee',
													'804'	=>	'Credit Report',
													'805'	=>	'Lender’s Inspection Fee',
													'808'	=>	'Mortgage Broker Commission/Fee',
													'809'	=>	'Tax Service Fee',
													'810'	=>	'Processing Fee',
													'811'	=>	'Underwriting Fee',
													'812'	=>	'Wire Transfer Fee',
													'813'	=>	'Renovation Reserve Setup Fee',
													'814'	=>	'Renovation Draw Fee',

													'900'	=>	'Items Required by Lender to be Paid in Advance',
													'901'	=>	'load_content',
													'902'	=>	'Mortgage Insurance Premiums',
													'903'	=>	'Hazard Insurance Premiums',
													'904'	=>	'County Property Taxes',
													'905'	=>	'VA Funding Fee',

													'1000'	=>	'Reserves Deposited with Lender',
													'1001'	=>	'load_content',
													'1002'	=>	'load_content',
													'1004'	=>	'load_content',
													'1005'	=>	'Renovation Reserve',
													'1006'	=>	'Interest Reserve',

													'1100'	=>	'Title Charges',
													'1101'	=>	'Settlement or Closing/Escrow Fee',
													'1105'	=>	'Document Preparation Fee',
													'1106'	=>	'Notary Fee',
													'1108'	=>	'Title Insurance',
													'1108'	=>	'Title Insurance',
													'1109'	=>	'Endorsements',
													'1110'	=>	'Closing Cost Cushion',

													'1200'	=>	'Government Recording and Transfer Charges',
													'1201'	=>	'Recording Fees',
													'1202'	=>	'City/County Tax/Stamps',
													//'1203'	=>	'County',

													'1300'	=>	'Additional Settlement Charges',
													'1302'	=>	'Pest Inspection'

												);
												
$config['input_data_closing_statement']		=	array(
													'901_input1' => '',
													'901_input2' => '',
													'1001_input1' => '',
													'1001_input2' => '',
													'1002_input1' => '',
													'1002_input2' => '',
													'1004_input1' => '',
													'1004_input2' => '',
													'mortage_broker_com' 				=> '',
													'additional_compensation_checkbox' 	=> '',
													'additional_compensation_amount' 	=> '',
													'proposed_loan_amount' 				=> '',
													'fee_cost_expence' 					=> '',
													'payment_of_other_obligation' 		=> '',
													'credit_life_insurance' 			=> '',
													'sub_total_all_deduction' 			=> '',
													'estimate_cash_close_amount' 		=> '',
												);
													
													
$config['loan_property_insurance_type']	 = array(
													1 => 'Property Insurance',
													2 => 'General Liability Insurance',
													3 => 'Flood Insurance',
												);	
													
$config['loan_affiliated_parties']	 = array(
													1 => 'Affiliated Parties 1',
													2 => 'Affiliated Parties 2',
													3 => 'Affiliated Parties 3',
													);	
													
$config['affiliated_option']			= array (
													'' => 'Select One',
													1 => 'Buyer Agent',
													2 => 'Seller Agent',
													3 => 'Contractor',
													4 => 'Mortgage Broker',
													5 => 'Referral',
													6 => 'Other'

													);	
													
$config['hoa_good_stand_option']	 = array(
													''=> 'Select One',
													0 => 'No',
													1 => 'Yes',
													2 => 'Unknown',
													);
													
$config['underwriting_option']	 = array(
													0=>'Select One',
													1=>'Purchase Price',
													4=>'Assessed Value',
													2=>'BPO - Current Market Value',
													3=>'BPO - After Repair Value',
													5=>'Appraisal - Completion Value',
													6=>'Appraisal - Current Market Value',
												);
													
$config['current_value_option']	 = array(
													0=>'Select One',
													1=>'Purchase Price',
													2=>'Appraised Value',
													3=>'Assessed Value',
													4=>'Broker Price Opinion',
												);	

													
$config['occupancy_value_option']	= array(
										0=>'Select One',
										1=>'Owner Occupied',
										2=>'Non Owner Occupied',

									);

$config['zoning_value_option']	= array(
										0 => 'Select One',
										1=>'Single Family(1 Unit)',
										2=>'Single Family(2-4 Unit)',
										3=>'Multi Family (4+ Units)',
										4=>'Commercial',
										5=>'Industrial',
										6=>'Retail',
										7=>'Mixed Use',
										8=>'Land',
									);

									
$config['property_modal_re851d_data'] = array(
											0=>'Select One',
											1=>'SFR-OO',
											2=>'SFR-NOO',
											3=>'Income Producing',
											4=>'Land-SFR',
											5=>'Land-Commercial',
											6=>'Land-Other',
											7=>'Other',
											
										);
				
$config['loan_purpose_option_type'] = array(
											
											0=>'Select One',
											1=>'Business',
											2=>'Personal',
										);
										
$config['relationships_option'] = array(
											
											0=>'Select One',
											1=>'Partner',
											2=>'Spouse',
											3=>'Referral',
											4=>'Other',
										);

$config['contact_status_option'] = array(
											
											0=>'Select One',
											3=>'Active',
											1=>'Complete',
											2=>'Cancelled',
											
										);

$config['extension_yes_no']	= array(
								
									'' => 'Select One',
									1 => 'Yes',
									0 => 'No',

								);
								
$config['extension_agreement']	= array(
								
									'' => 'Select One',
									1 => 'Sent to Borrower',
									2 => 'Signed by Borrower',

								);
								
$config['extension_loan_servicer']	= array(
								
									'' => 'Select One',
									1 => 'Submitted to Servicer',
									2 => 'Processed by Servicer',

								);
								
$config['extension_fee'] = array(
								
									'' => 'Select One',
									1 => 'Received by Servicer',
									2 => 'Processed by Servicer',

								);
								
$config['yes_no_option3'] = array(		
										'' => 'Select One',
										1 => 'Yes',
										2 => 'No',
										
										);
								
$config['underwriting_items_option'] = array(
								
									//'1' => 'Borrower Data',
									'2' => 'Completed Loan Application',
									'3' => 'Copy of Driver’s License',
									'4' => 'Credit Check (completed by TaliMar Financial)',
									'5' => 'Account Statement evidencing cash equity requirement',
									'6' => 'Prior Year Tax Return',
									'7' => 'LLC Documents (Operating Agreement, Articles of Organization)',
									'8' => 'Corp Documents (Corp Bylaws, Articles of Incorporation)',
									'9' => 'Trust (Trust Agreement)',
									
									//'101' => 'Transaction Document',
									'102' => 'Purchase Contact',
									'103' => 'Term Sheet Signed',
									'104' => 'Escrow Contact Information',
									'105' => 'Preliminary Title Report (completed by TaliMar Financial)',
									'106' => 'Detailed Project Budget',
									'107' => 'Project Scope of Work',
									'108' => 'Loan Reserve Deposit Account Form ',
									'109' => 'RE885 MLDS signed',
									'110' => 'Estimated Closing Statement (completed by TaliMar Financial)',
									'111' => 'Title Wire Instructions (completed by TaliMar Financial)',
									
									//'201' => 'Property Information',
									'202' => 'Property Images',
									'203' => 'Property Insurance (TaliMar to provide requirements)',
									'204' => 'YTD Cash Flow Statement',
									'205' => 'YE Cash Flow Statement',
									'206' => 'Signed Lease Agreements',
									'207' => 'Bank Statement showing tenant deposits',
									'208' => 'Appraisal / Broker Price Opinion',
									'209' => 'Property Report (completed by TaliMar Financial)',
									'210' => 'Most Recent Mortgage Statement',
									'211' => 'Promissory Note',
									
								);
			
$config['referral_source'] = array(
									
									
									4 	=> 	'Bigger Pockets',
									14 	=> 	'County Recorder',
									13 	=> 	'E-Mail blast',
									2 	=> 	'Facebook',
									10 	=> 	'Google Adwords',
									11 	=> 	'Google Search',
									3 	=> 	'Linkedin',
									12 	=> 	'Loan Closing',
									8 	=> 	'Networking Event',
									9 	=> 	'Other',
									15 	=> 	'Private Lender Link',
									1 	=> 	'Referral',
									16	=> 	'Website',
									// 5 => 'SDCIA',
									// 6 => 'NSDREI',
									// 7 => 'SDIC',
									

								);
								
								
								
$config['loan_servicing_checklist'] = array(
										
										'101' => 'Submit Introduction e-mail to Escrow',
										'102' => 'Enter Escrow Wire Instructions into Database',
										'103' => 'Submit Loan Documents to Escrow',
										'104' => 'Approve / Save Estimated Closing Statement',
										'105' => 'Confirm Borrower has Deposited funds',
										'106' => 'Review / Complete Underwriting Checklist',
										'107' => 'Obtain / Save Electronic Signed Copies of Loan Documents',
										'108' => 'Submit Wire Instructions to Lenders',
										'109' => 'Wire Submitted to Escrow / Title',
										// '110' => 'Lender(s)',
										// '111' => 'TaliMar Financial',
										// '112' => 'Other',
										'113' => 'Complete Fraud Prevention',
										'114' => 'Submit Lender Wire Schedule to Escrow',
										//'115' => 'RE885 MLDS Signed',
										//'116' => 'Term Sheet Signed',
										//'117' => 'Complete Borrower Wire Form for Loan Service Disbursements',
										'118' => 'Submit Term Sheet',
										'119' => 'Submit Loan Application',
										'120' => 'Submit RE885 MLDS to Borrower(s)',
										'121' => 'Submit Loan Application to Borrower(s)',
										'122' => 'Submit Term Sheet to Borrower',
										'123' => 'Submit Subordination Agreement',
										
										
										
										'1001' => 'Notify Escrow/Title of Incoming Wire',
										'1002' => 'Confirm Wire Receive by Recipient',
										'1003' => 'Confirm Closing w/ Investor(s)',
										'1004' => 'Update Loan File notes for any irregularities at closing',
										'1005' => 'Move Online Loan Folder to Active Folder',
										//'1006' => 'Loan Database',
										'1007' => 'Change Loan to Funded',
										'1008' => 'Enter Trust Deed recording information',
										//'1009' => 'Loan Boarding',
										'1010' => 'Submit Loan Package to Servicer',
										//'1011' => 'Submit Interest Reserve / Odds Day Interest',
										//'1012' => 'Submit Loan Reserves',
										'1013' => 'Update Loan with Servicer Loan #',
										'1014' => 'Notify Lenders once Loan has been Boarded',
										'1015' => 'Release Construction Draw to Borrower',
										'1016' => 'Input Property Insurance data into Database',
										'1017' => 'Obtain / File original loan documents in hard Loan folder',
										'1018' => 'Send Welcome E-Mail to Borrower',
										//'1019' => 'Move Hard Loan Folder to Active Loan File',
										'1020' => 'Fees / Costs received by TaliMar Financial',
										'1021' => 'Confirm Lender Name / Account # / Interest correct with Servicer',
										'1022' => 'Confirm Reserves and/or Interest Payments received by Servicer',

									);	

$config['lender_investment_experience'] = array(
											
											1 => 'No investment Experience',
											2 => 'Mutual Funds',
											3 => 'Stocks / Shares',
											4 => 'Real Estate',
											5 => 'Annuities',
											6 => 'Notes',
											7 => 'Other',
											8 => 'Bonds',
											9 => 'Options',
											
										);	

$config['lender_financial_situation']	 = array(

											1 => 'Under $50,000',
											2 => '$50,001 to $100,000',
											3 => '$100,001 to 200,000',
											4 => '$200,001 to $300,000',
											5 => '$300,001 to $500,000',
											6 => '$500,001 to $750,000',
											7 => '$750,001 to $1,000,000',
											8 => '$1,000,001 to $5,000,000',
											9 => '$5,000,001 to $10,000,000',
											10 => 'Over $10,000,000',

											);
											
$config['lender_liquidity_needs']	 = array(

											1 => 'Primary Need is liquidity/cash',
											2 => 'Need some liquidity for possible quick access to cash',
											3 => 'No liquidity needed; have other sources of cash',

										);
										
										
$config['boarding_brocker_disbrusement'] = array(

											
											1 => 'Not Confirmed',
											2 => 'Confirmed',
											
										);
										
$config['reason_for_default']			= array(

												''	=> 'Select One',
												1	=> 'Late Payment',
												2	=> 'Maturity',
												3	=> 'Property Taxes',
												
											);

$config['foreclosuer_status']			= array(

												''	=> 'Select One',
												1	=> 'Borrower notification',
												2	=> 'NOD Recorded',
												3	=> 'NOS Recorded',
												
											);	
$config['foreclosuer_processor']			= array(

												''	=> 'Select One',
												1	=> 'FCI Lender Services',
												2	=> 'Del Toro Loan Servicing',
												
											);	

$config['estimale_complete_option']		= array(
				
												''	=> 'Select One',
												1	=> 'Estimate',
												2	=> 'Completed',
											);

$config['loan_note_option']				= array(
													'' => 'Select One',
													1 => 'Borrower',
													2 => 'Lender',
												
												);
												
												
$config['servicing_agent_option']				= array(
													'' => 'Select One',
													14 => 'FCI Lender Services',
													15 => 'TaliMar Financial Inc.',
													16 => 'La Mesa Fund Control',
													19 => 'Del Toro Loan Servicing',
												
												);
												
$config['credit_score']				= array(												
													'' => 'Select One',	
													1  => '800+',
													2  => '750-799',
													3  => '700-749',
													4  => '650-699',
													5  => '600-649',
													6  => '550-599',
													7  => '500-549',
													
												);	

$config['user_access_type']				= array(
													'' => 'Access Type',	
													1  => 'View',
													2  => 'View and Edit',
													3  => 'Full Access(View, Edit, Subscribe)',

											);	

$config['dishbrushment_option']				= array(
													'' => 'Select One',	
													1  => 'ACH',
													2  => 'Check',

											);	


$config['valuation_based_upon'] = array(

	'' => 'Select One',
	1 => 'Current Value',
	2 => 'Completion Value',
	3 => 'Purchase Price',
);
?>