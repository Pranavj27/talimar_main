<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;



$route['user_name_checker']		= 'Home/user_name_checker';
$route['verified/:id']			= 'Home/verified';

$route['register']				= 'Home/register';
$route['login']					= 'Home/login';
$route['dashboard']				= 'Home/dashboard';
$route['error_page']			= 'Home/error_page';
$route['logout']				= 'Home/logout';
$route['view_profile']			= 'Home/view_profile';
$route['forget_password']		= 'Home/forget_password';
$route['resetPassword']			= 'Home/resetPassword';
$route['resetUsername']			= 'Home/resetUsername'; //05-24-2021


$route['testSignin']			= 'Home/testSignin';
$route['testSignup']			= 'Home/testSignup';
$route['testforgotpassword']	= 'Home/testforgotpassword';





$route['accounts']						= 'Account_setting/accounts';
$route['view_edit_account/(:any)']		= 'Account_setting/view_edit_account/$1';
$route['add_account_modal']				= 'Account_setting/add_account_modal';
$route['fetch']							= 'Account_setting/fetch';
$route['fetch_account_id']				= 'Account_setting/fetch_account_id';
$route['update_portal_account']			= 'Account_setting/update_portal_account';
$route['add_edit_account']				= 'Account_setting/add_edit_account';






$route['trust_deeds']						= 'Available_deed/trust_deeds';
$route['trust_deed']						= 'Available_deed/trust_deeds_new';
$route['active_trust_deeds/(:any)']			= 'Active_deed/active_trust_deeds/$1';


$route['confirm_order']						= 'Available_deed/confirm_order';
$route['trust_deed_details/(:any)']			= 'Available_deed/trust_deed_details/$1';
$route['avaliable_trust_pdf']	  	        = 'Available_deed/avaliable_trust_pdf';

$route['active_trust_deeds']				= 'Active_deed/active_trust_deeds';
$route['active_trust_pdf']					= 'Active_deed/active_trust_pdf';


$route['personal_setting']					= 'Personal_details/personal_setting';
$route['view_details']						= 'Personal_details/view_details';
$route['view_details/(:any)']				= 'Personal_details/view_details/$1';
$route['update_user_profile']				= 'Personal_details/Updatelenderprofile';
$route['update_username']					= 'Personal_details/Updatelenderusername';
$route['update_password']					= 'Personal_details/Updatelenderpassword';
$route['profile_image_remove']				= 'Personal_details/profile_image_remove';
$route['profile_image_upload']				= 'Personal_details/profile_image_upload';

/***************Lender section Start *************/

$route['add_lender_user']	= 'LenderUsers/add_form/';
$route['add_investor_data']='LenderUsers/add_investor_data/';
$route['add_lender_user/(:any)'] = 'LenderUsers/add_form/$1';
$route['add_lender_user/(:any)/(:any)']= 'LenderUsers/add_form/$1/$2';
$route['delete_lender_user/(:any)'] ='LenderUsers/delete_investor/$1';
$route['view_lender_user/(:any)'] = 'LenderUsers/view_form/$1';
$route['view_lender_user/(:any)/(:any)']= 'LenderUsers/view_form/$1/$2';
$route['LenderUsers/(:any)']= 'LenderUsers/$1';
$route['LenderUsers']= 'LenderUsers/lender_users_list';

/***************Lender section End *************/

















