<?php
Defined('BASEPATH') OR exit('No direct script access allowed');

Class Account_setting extends CI_Controller{
	
	public function __construct(){
		
		parent:: __construct();
		
		$this->load->model('User_model');
		$this->load->library('email');
		$this->load->library('Aws3','aws3');
		$this->load->helper('mailhtml_helper.php');
		
		
		if($this->session->userdata('p_user_id') != ''){
			
		}
		else
		{
			 redirect(base_url());
		}
	}
	
	public function accounts(){
		
		$data['ex']		= '1';
		
		//$fetch_account = $this->User_model->query("Select * from portal_account");
		
		$sql = "SELECT * FROM lender_contact JOIN investor ON lender_contact.lender_id = investor.id join contact on contact.contact_id=lender_contact.contact_id  WHERE lender_contact.contact_id = '".$this->session->userdata('p_user_id')."' AND lender_contact.link_to_lender = '2'";
		
		$fetch_account = $this->User_model->query($sql);
		$fetch_account = $fetch_account->result();
		$data['fetch_account'] = $fetch_account;
		
		$data['page'] = 'settings';
		$data['content'] = $this->load->view('Accounts_setting/add_accounts',$data,true);
		$this->load->view('template_files/template',$data);
	}

	public function checkPassworddata(){

		$pass = $this->input->post('pass');
		$userid = $this->session->userdata('p_user_id');

		$where['password'] = $pass;
		$where['contact_id'] = $userid;

		$fetchpass = $this->User_model->select_where('lender_contact_type',$where);
		if($fetchpass->num_rows() > 0){

			$res['msg'] = 'Password match with your account.';
			$res['status'] = '2';

			echo json_encode($res);

		}else{

			$res['msg'] = 'Password not match with your account.';
			$res['status'] = '1';

			echo json_encode($res);
		}
	}

	public function add_edit_account(){
		
		$data['ex']		= '1';
		$data['page'] = 'settings';
		$data['content'] = $this->load->view('Accounts_setting/add_edit_account',$data,true);
		$this->load->view('template_files/template',$data);
	}
		
	public function add_account_modal(){

	if($this->input->post('investor_name')==''){
		
			$this->session->set_flashdata('error', 'Lender Name Required!');
			redirect(base_url().'add_edit_account');	
	}else{
	
		//$data['talimar_lender']    			= $this->input->post('talimar_lender');
		$data['fci_acct_service']    		= $this->input->post('fci_acct_service');
		$data['del_acct_service']    		= $this->input->post('del_acct_service');
		$data['fci_acct']    				= $this->input->post('fci_acct');
		$data['del_toro_acct']    			= $this->input->post('del_toro_acct');
		$data['fci_lender']    				= $this->input->post('fci_lender');
		$data['del_toro_no']    			= $this->input->post('del_toro_no');
		$data['name']    					= $this->input->post('investor_name');
		$data['investor_type']    			= $this->input->post('investor_type');
		$data['active_fci_account']    		= $this->input->post('active_fci_account');
		$data['entity_name']    			= $this->input->post('entity_name');
		$data['entity_type']    			= $this->input->post('entity_type');
		$data['entity_res_state']    		= $this->input->post('entity_res_state');
		$data['state_entity_no']    		= $this->input->post('state_entity_no');
		// $data['register_state']    		= $this->input->post('register_state');
		$data['address']    				= $this->input->post('address');
	
		$data['unit']    					= $this->input->post('unit');
		$data['custodian']    				= $this->input->post('custodian');
		$data['self_dir_account']    		= $this->input->post('self_dir_account');
		$data['directed_IRA_NAME']    		= $this->input->post('directed_IRA_NAME');
		$data['tax_id']    					= $this->input->post('tax_id');
	
		$data['city']    						= $this->input->post('city');
		$data['state']    						= $this->input->post('state');
		$data['zip']    						= $this->input->post('zip');
		$data['vesting']    					= $this->input->post('vesting');
		// $data['company_name']    				= $this->input->post('company_name');
		// $data['company_type']    				= $this->input->post('company_type');

		if($this->input->post('ach_and_check_drop')== 1){
		
		 $ach_disbrusement =1;
		}
		else{

		$check_disbrusement=1;
		
		
		}
			// $data['ach_disbrusement']    			= $this->input->post('ach_disbrusement');
			// $data['check_disbrusement']    			= $this->input->post('check_disbrusement');
			
		$data['ach_disbrusement']  				= $ach_disbrusement ? $ach_disbrusement :'';
		$data['check_disbrusement'] 			= $check_disbrusement ? $check_disbrusement:'';
		$data['bank_name']    					= $this->input->post('bank_name');
		$data['account_number']    				= $this->input->post('account_number');
		$data['routing_number']    				= $this->input->post('routing_number');
		
		
		$data['mail_address']    				= $this->input->post('mail_address');
		$data['mail_city']    					= $this->input->post('mail_city');
		$data['mail_state']    					= $this->input->post('mail_state');
		$data['mail_zip']    					= $this->input->post('mail_zip');
		$data['mail_unit']    					= $this->input->post('mail_unit');
		
		$data['t_user_id']    				= $this->session->userdata('p_user_id');
		$data['created_date']    			= date('m-d-Y');
		
		
		
					$this->User_model->insertdata('investor',$data);
					$insert_id=$this->db->insert_id();
					$dataa['lender_id']=$insert_id;
					$dataa['contact_id']=$this->session->userdata('p_user_id');
					$dataa['t_user_id']=$this->session->userdata('p_user_id');
					
					
					$this->User_model->insertdata('lender_contact',$dataa);
							
					
			$this->session->set_flashdata('success', 'Accounts added sucessfully!');
			$email 			='invest@talimarfinancial.com';
		    
		         		  
			$subject	= "TaliMar Financial - Account Setting"; 
			$header		= "From: mail@talimarfinancial.com"; 
			
			$message	= ''; 			
			$message   .= '<p>'.$this->session->userdata('p_user_fname').' '.$this->session->userdata('p_user_lname').' has modified the settings on their account.</p>';
			
			
			$data = array();
			$data['from_email'] = 'mail@talimarfinancial.com';
			$data['from_name'] = $subject;
			$data['to'] = $email;
			$data['name'] = $this->session->userdata('p_user_fname');
			$data['subject'] = $subject;
			$data['content_body'] = $message;
			$data['button_url'] = '';
			$data['button_text'] = '';
			send_mail_template($data);

					redirect(base_url().'accounts');
		
		
		}	
	}
	
	    public function view_edit_account() 
		{
			
		$id=$this->uri->segment(2);	
		
		$fetch_account = $this->User_model->query("Select * from investor where id='".$id."'");
		$fetch_account = $fetch_account->result();
		$data['fetch_data_account'] = $fetch_account;

		//foreach ($fetch_account as $value) {
			
			$fetchsign = $this->User_model->query("SELECT required_sign FROM lender_contact Where lender_id = '".$id."' AND contact_id = '".$this->session->userdata('p_user_id')."'");
			if($fetchsign->num_rows() > 0){
				$fetchsign = $fetchsign->result();

				$signAuth = $fetchsign[0]->required_sign;
				if($signAuth == '1'){

					$signAuthVal = 'Yes';
				}else{
					$signAuthVal = 'No';
				}
			}else{
				$signAuthVal = 'No';
			}
		//}

		//Select all contact that are related to this lender...
		$fetchaccesstocontact = $this->User_model->query("SELECT * FROM lender_contact Where lender_id = '".$id."' AND link_to_lender = '2'");
		if($fetchaccesstocontact->num_rows() > 0){

			$fetchaccesstocontact = $fetchaccesstocontact->result();
			foreach ($fetchaccesstocontact as $value) {
				
				$fetchaccesstocontactarray[] = $value;
			}
		}else{
				$fetchaccesstocontactarray = '';
		}


		$data['all_contact'] = $this->all_contact();
		$data['fetchaccesstocontact'] = $fetchaccesstocontactarray;

		$data['signAuthVal'] = $signAuthVal;
		$data['page'] = 'settings';
		$data['content'] = $this->load->view('Accounts_setting/edit_account',$data,true);
		$this->load->view('template_files/template',$data);
	
		
		}

		public function all_contact(){

			$fetcontact = $this->User_model->query("SELECT contact_id,contact_firstname, contact_lastname FROM contact");
			$fetcontact = $fetcontact->result();
			foreach ($fetcontact as $key => $value) {
				
				$allname[$value->contact_id] = $value->contact_firstname.' '.$value->contact_lastname;
			}

			return $allname;
		}
	
	   
		public function fetch_account_id()
		{
			$id = $this->input->post('id');
			// echo $id;
			$get_id = $this->User_model->query("select * from portal_account where id = '".$id."'");
			$fetch 	= $get_id->result();
			
			foreach($fetch as $row)
			{
				$data['id'] 							= $row->id;
				$data['account_name'] 					= $row->account_name;
				$data['account_number1'] 				= $row->account_number1;
				$data['phone'] 							= $row->phone;
				$data['m_street_address'] 				= $row->m_street_address;
				$data['m_city']                         = $row->m_city;
				$data['deposited_electronically']       = $row->deposited_electronically;
			}
			
			echo json_encode($data);	
		}
		

		public function edit_update_account($idd) 
		{
		
			error_reporting(0);
			 $idd=$this->uri->segment(3);
			
			//$data['talimar_lender']    			= $this->input->post('talimar_lender');
			$data['fci_acct_service']    		= $this->input->post('fci_acct_service');
			$data['del_acct_service']    		= $this->input->post('del_acct_service');
			$data['fci_acct']    				= $this->input->post('fci_acct');
			$data['del_toro_acct']    			= $this->input->post('del_toro_acct');
			$data['fci_lender']    				= $this->input->post('fci_lender');
			$data['del_toro_no']    			= $this->input->post('del_toro_no');
			$data['name']    					= $this->input->post('investor_name');
			$data['investor_type']    			= $this->input->post('investor_type');
			$data['active_fci_account']    		= $this->input->post('active_fci_account');
			$data['entity_name']    			= $this->input->post('entity_name');
			$data['entity_type']    			= $this->input->post('entity_type');
			$data['entity_res_state']    		= $this->input->post('entity_res_state');
			$data['state_entity_no']    		= $this->input->post('state_entity_no');
			// $data['register_state']    		= $this->input->post('register_state');
			$data['address']    				= $this->input->post('address');
		
			$data['unit']    					= $this->input->post('unit');
			$data['custodian']    				= $this->input->post('custodian');
			$data['self_dir_account']    		= $this->input->post('self_dir_account');
			$data['directed_IRA_NAME']    		= $this->input->post('directed_IRA_NAME');
			$data['tax_id']    					= $this->input->post('tax_id');
		
			$data['city']    						= $this->input->post('city');
			$data['state']    						= $this->input->post('state');
			$data['zip']    						= $this->input->post('zip');
			$data['vesting']    					= $this->input->post('vesting');
			// $data['company_name']    				= $this->input->post('company_name');
			// $data['company_type']    				= $this->input->post('company_type');
			

		if($this->input->post('ach_and_check_drop')== 1){
		
		 $ach_disbrusement =1;
		}
		else{

		$check_disbrusement=1;
		
		
		}
			// $data['ach_disbrusement']    			= $this->input->post('ach_disbrusement');
			// $data['check_disbrusement']    			= $this->input->post('check_disbrusement');
			
			$data['ach_disbrusement']  				= $ach_disbrusement ? $ach_disbrusement :'';
			$data['check_disbrusement'] 			= $check_disbrusement ? $check_disbrusement:'';
			$data['bank_name']    					= $this->input->post('bank_name');
			$data['account_number']    				= $this->input->post('account_number');
			$data['routing_number']    				= $this->input->post('routing_number');
			
			
			$data['mail_address']    				= $this->input->post('mail_address');
			$data['mail_city']    					= $this->input->post('mail_city');
			$data['mail_state']    					= $this->input->post('mail_state');
			$data['mail_zip']    					= $this->input->post('mail_zip');
			$data['mail_unit']    					= $this->input->post('mail_unit');
			
			$data['t_user_id']    				= $this->session->userdata('p_user_id');
			$data['created_date']    			= date('m-d-Y');
			
				// print_r($data);
				// die();
			$this->User_model->updatedata('investor',array('id'=>$idd),$data);
			$this->session->set_flashdata('success','Account Updated Successfully!');


			//send mail to Talimar financial...

		    $email 			='invest@talimarfinancial.com';
		    
		         		  
			$subject	= "TaliMar Financial - Account Setting"; 
			$header		= "From: mail@talimarfinancial.com"; 
			
			$message	= '';			
			$message   .= '<p>'.$this->session->userdata('p_user_fname').' '.$this->session->userdata('p_user_lname').' has modified the settings on their account.</p>';
			
			
			$data = array();
			$data['from_email'] = 'mail@talimarfinancial.com';
			$data['from_name'] = $subject;
			$data['to'] = $email;
			$data['name'] = $this->session->userdata('p_user_fname');
			$data['subject'] = $subject;
			$data['content_body'] = $message;
			$data['button_url'] = '';
			$data['button_text'] = '';
			send_mail_template($data);

			redirect(base_url().'accounts');
	
		}	
	
}


?>