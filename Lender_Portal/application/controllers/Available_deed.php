<?php
Defined('BASEPATH') OR exit('No direct script access allowed');

Class Available_deed extends CI_Controller{
	
	public function __construct(){
		
		parent:: __construct();
		
		$this->load->model('User_model');
		$this->load->library('email');
		$this->load->library('TCPDF');
		$this->load->library('Aws3','aws3');	
	}
	
	
	
	public function trust_deeds(){

		if($this->session->userdata('p_user_id') != ''){
			
		}
		else
		{
			redirect(base_url());
		}
		
		$join_sql = "SELECT *,l.id as loan_id FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.new_market_trust_deed IN('2','3','4') AND  ls.loan_status IN('1','2') ORDER BY ls.new_market_trust_deed ASC";
		
		
		$fetch_loan = $this->User_model->query($join_sql);
		if($fetch_loan->num_rows() > 0){
			$fetch_loan = $fetch_loan->result();
			
			foreach($fetch_loan as $row)
			{
				error_reporting(0);
				$where['talimar_loan'] 	= $row->talimar_loan;
				$loan_id 				= $row->loan_id;
				$servicing_lender_rate 				= $row->servicing_lender_rate;
				$fetch_property = $this->User_model->query('Select *,SUM(underwriting_value) as underwriting_value from loan_property where talimar_loan="'.$row->talimar_loan.'"');
				$fetch_property = $fetch_property->result();

				
				$sum_assigment = "SELECT *,SUM(payment) as total_payment,SUM(investment) as total_investment FROM loan_assigment WHERE talimar_loan = '".$row->talimar_loan."'";
				
				$assigment_sum1 				= $this->User_model->query($sum_assigment);
				$assigment_sum1 				= $assigment_sum1->result();
				
				
				$assigment_investment_amount 	= $assigment_sum1[0]->total_investment;
				$assigment_total_payment 		= $assigment_sum1[0]->total_payment;
				$invester_yield_percent 		= $assigment_sum1[0]->invester_yield_percent;
				
				$where2['id'] = $row->loan_borrower;
				
				$fetch_borrower 	= $this->User_model->select_where('borrower_data',$where2);
				$fetch_borrower1 	= $fetch_borrower->result();
				if($fetch_borrower->num_rows() > 0)
				{
					$b_name 		= $fetch_borrower1[0]->b_name;
				}
				else
				{
					$b_name 		= '';
				}
				
				$sel_ecum = $this->User_model->query("SELECT SUM(`current_balance`) as total_balance FROM `encumbrances` WHERE `talimar_loan` = '".$row->talimar_loan."' AND property_home_id !='' AND will_it_remain = '0'");
				$sel_ecum = $sel_ecum->result();
				
				
				$total_current_balance = $sel_ecum[0]->total_balance;
				
				$trust_deed_schedule[] = array(
												'talimar_loan' 		=> $row->talimar_loan,
												'loan_id' 			=> $loan_id,
												'servicing_fee' 	=> $row->servicing_fee,
												'loan_amount'  		=> $row->loan_amount,
												'term_month'  		=> $row->term_month,
												'closing_status'  	=> $row->closing_status,
												'lender_fee'		=> $row->lender_fee,
												'lender_minimum'	=> $row->lender_minimum,
												'intrest_rate'		=> $row->intrest_rate,
												'closing_date'		=> $row->loan_funding_date,
												'position'			=> $row->position,
												'loan_type'			=> $row->loan_type,
												'trust_deed'		=> $row->new_market_trust_deed,
												'borrower'			=> $b_name,
												'property_address' 	=> $fetch_property[0]->property_address,
												'city' 				=> $fetch_property[0]->city,
												'state' 			=> $fetch_property[0]->state,
												'zip' 				=> $fetch_property[0]->zip,
												'property_type' 	=> $fetch_property[0]->property_type,
												'underwriting' 		=> $fetch_property[0]->underwriting_value,
												'sel_ecum' 			=> $total_current_balance,
												'available' 		=> $row->loan_amount - $assigment_investment_amount,
												'payment' 		    => $assigment_total_payment,
												'invester_yield_percent' => $invester_yield_percent,
												'servicing_lender_rate' => $servicing_lender_rate,
											);
				
			}
		
			$data['trust_deed_schedule']		= $trust_deed_schedule;


		}else{
			$data['trust_deed_schedule'] 		= '';
		}
		$data['page'] = 'availableTrustDeeds';
		$data['content'] = $this->load->view('Available_trust_deed/available_deeds',$data,true);
		$this->load->view('template_files/template',$data);
	}

	public function trust_deeds_new(){

		$join_sql = "SELECT *,l.id as loan_id FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.new_market_trust_deed IN('2','3','4') AND  ls.loan_status IN('1','2') ORDER BY ls.new_market_trust_deed ASC";
		
		$fetch_loan = $this->User_model->query($join_sql);
		if($fetch_loan->num_rows() > 0){
			$fetch_loan = $fetch_loan->result();
			
			foreach($fetch_loan as $row)
			{

				error_reporting(0);
				$where['talimar_loan'] 	= $row->talimar_loan;
				$loan_id 				= $row->loan_id;
				$servicing_lender_rate 				= $row->servicing_lender_rate;
				$fetch_property = $this->User_model->query('Select *,SUM(underwriting_value) as underwriting_value from loan_property where talimar_loan="'.$row->talimar_loan.'"');
				$fetch_property = $fetch_property->result();
				$property_home_id = $fetch_property[0]->property_home_id;

				
				$sum_assigment = "SELECT *,SUM(payment) as total_payment,SUM(investment) as total_investment FROM loan_assigment WHERE talimar_loan = '".$row->talimar_loan."'";
				
				$assigment_sum1 				= $this->User_model->query($sum_assigment);
				$assigment_sum1 				= $assigment_sum1->result();
				
				
				$assigment_investment_amount 	= $assigment_sum1[0]->total_investment;
				$assigment_total_payment 		= $assigment_sum1[0]->total_payment;
				$invester_yield_percent 		= $assigment_sum1[0]->invester_yield_percent;
				
				$where2['id'] = $row->loan_borrower;
				
				$fetch_borrower 	= $this->User_model->select_where('borrower_data',$where2);
				$fetch_borrower1 	= $fetch_borrower->result();
				if($fetch_borrower->num_rows() > 0)
				{
					$b_name 		= $fetch_borrower1[0]->b_name;
				}
				else
				{
					$b_name 		= '';
				}
				
				$sel_ecum = $this->User_model->query("SELECT SUM(`current_balance`) as total_balance FROM `encumbrances` WHERE `talimar_loan` = '".$row->talimar_loan."' AND property_home_id !='' AND will_it_remain = '0'");
				$sel_ecum = $sel_ecum->result();
				
				
				$total_current_balance = $sel_ecum[0]->total_balance;

				$get_proprty_img = $this->get_proprty_img($row->talimar_loan,$property_home_id);
				
				$trust_deed_schedule[] = array(
												'talimar_loan' 		=> $row->talimar_loan,
												'loan_id' 			=> $loan_id,
												'property_home_id' 	=> $property_home_id,
												'servicing_fee' 	=> $row->servicing_fee,
												'loan_amount'  		=> $row->loan_amount,
												'term_month'  		=> $row->term_month,
												'closing_status'  	=> $row->closing_status,
												'lender_fee'		=> $row->lender_fee,
												'lender_minimum'	=> $row->lender_minimum,
												'intrest_rate'		=> $row->intrest_rate,
												'closing_date'		=> $row->loan_funding_date,
												'position'			=> $row->position,
												'loan_type'			=> $row->loan_type,
												'trust_deed'		=> $row->new_market_trust_deed,
												'borrower'			=> $b_name,
												'property_address' 	=> $fetch_property[0]->property_address,
												'city' 				=> $fetch_property[0]->city,
												'state' 			=> $fetch_property[0]->state,
												'zip' 				=> $fetch_property[0]->zip,
												'property_type' 	=> $fetch_property[0]->property_type,
												'underwriting' 		=> $fetch_property[0]->underwriting_value,
												'sel_ecum' 			=> $total_current_balance,
												'available' 		=> $row->loan_amount - $assigment_investment_amount,
												'payment' 		    => $assigment_total_payment,
												'invester_yield_percent' => $invester_yield_percent,
												'servicing_lender_rate' => $servicing_lender_rate,
												'get_proprty_img' => $get_proprty_img,
											);
				
			}
		
			$data['trust_deed_schedule']		= $trust_deed_schedule;

		}else{
			$data['trust_deed_schedule'] 		= '';
		}
		$data['page'] = 'availableTrustDeeds';
		$data['content'] = $this->load->view('Available_trust_deed/available_deed_card',$data,true);
		$this->load->view('template_files/template',$data);

	}

	public function get_proprty_img($talimar_loan,$property_home_id){

		$getUnderImgonly = $this->User_model->query("SELECT * FROM `property_folders` WHERE `talimar_loan`='".$talimar_loan."' And title = 'Underwriting' AND `property_home_id`='".$property_home_id."'");
        if($getUnderImgonly->num_rows() > 0){

            $getUnderImgonly = $getUnderImgonly->result();
            foreach ($getUnderImgonly as $key => $value) {
                
                $folderidarray[] = $value->id;
            }

            $folderidarrayVAl = implode("','", $folderidarray);

            $property_img_sql = "SELECT * FROM `loan_property_images` WHERE `folder_id` IN('".$folderidarrayVAl."')";
            $property_img = $this->User_model->query($property_img_sql);
            $property_img = $property_img->result();

            $count = 0;
            foreach($property_img as $rowss){

                $count++;
                if($count == 1){
                
                    if($rowss->image_name != 'blank.jpg'){

                        $img_name = 'https://database.talimarfinancial.com/all_property_images/'.$rowss->talimar_loan.'/'.$rowss->property_home_id.'/'.$rowss->folder_id.'/'.$rowss->image_name;
                        
                    }
                }   
            }

        }else{
            $img_name = '';
        }

        return $img_name;
	}

	public function contact_associate_with_lender(){

		$fetch_any_lender = $this->User_model->query("SELECT * FROM `lender_contact` WHERE `contact_id`= '".$this->session->userdata('p_user_id')."'");
		if($fetch_any_lender->num_rows() > 0){

			$result = 'Yes';
		}else{
			$result = 'No';
		}

		return $result;
	}

	public function error_page(){

		$this->load->view('error404');
	}
	
	
	public function trust_deed_details(){
		error_reporting(0);

		if($this->session->userdata('p_user_id') != ''){

			$contact_associate_with_lender = $this->contact_associate_with_lender();

			if($contact_associate_with_lender == 'No'){

				//view trust deed data...
				$select_propertyforview = $this->User_model->query("Select property_address from loan_property where talimar_loan = '".$this->uri->segment(2)."'");
				$select_propertyforview = $select_propertyforview->result();
				$property_address_valview = $select_propertyforview[0]->property_address;
				$this->View_trust_deed_by_user($this->session->userdata('p_user_id'),$property_address_valview,$this->uri->segment(2));

				$this->session->set_flashdata('error', 'Thank you for your interest in this trust deed. It does not appear you have an active account with TaliMar Financial. Please contact a Lender Account Representative at (858) 613-0111 x3 to setup your account.');
				redirect(base_url()."error_page");
			}
		}
		else
		{
			
		}

		
		$talimar_loan = $this->uri->segment(2);
		$data['talimar_loan'] = $talimar_loan;
		//fetch_loan_data...
		$select_loan = $this->User_model->query("Select * from loan where talimar_loan = '".$talimar_loan."'");
		$select_loan = $select_loan->result();
		$borrower 	 = $select_loan[0]->borrower;

		$fee_disbursement_data = $this->User_model->query("SELECT * FROM fee_disbursement WHERE talimar_loan = '" . $talimar_loan. "' ORDER by id ");

		if ($fee_disbursement_data->num_rows() > 0) {
			$fee_disbursement_data = $fee_disbursement_data->result();
			$data['fee_disbursement_data'] = $fee_disbursement_data;
		}
		
		//fetch_loan_draws_data...
		$select_loan_draws = $this->User_model->query("SELECT * FROM `loan_reserve_draws` where talimar_loan = '".$talimar_loan."'");
		$select_loan_draws = $select_loan_draws->result();
		$data['select_loan_draws'] = $select_loan_draws;

		//fetch_loan_servicing_data...
		$select_loan_servicing = $this->User_model->query("Select * from loan_servicing where talimar_loan = '".$talimar_loan."'");
		$select_loan_servicing = $select_loan_servicing->result();

		//loan_source_and_uses...
		$select_loan_source_and_uses = $this->User_model->query("SELECT * FROM `loan_source_and_uses` WHERE `talimar_loan` = '".$talimar_loan."'");
		$select_loan_source_and_uses = $select_loan_source_and_uses->result();
		$data['select_loan_source_and_uses'] = $select_loan_source_and_uses;
		
		
		$select_property_ho = $this->User_model->query("Select * from property_home where talimar_loan = '".$talimar_loan."' AND primary_property <> 1");
		$select_property_ho = $select_property_ho->result();
		//fetch_loan_property_data...

		$select_property = $this->User_model->query("Select * from loan_property where talimar_loan = '".$talimar_loan."'");
		$select_property = $select_property->result();
		$property_home_id = $select_property[0]->property_home_id;
		$property_address_val = $select_property[0]->property_address;
		$valuation_vendor = $select_property[0]->valuation_vendor;
		$valuation_based = $select_property[0]->valuation_based;
		
		//fetch_borrower_data...
		$borrower_data = $this->User_model->query("Select * from borrower_data where id = '".$borrower."'");
		$borrower_data = $borrower_data->result();

		//fetch_borrower_data...
		$capital_diligence_document = $this->User_model->query("Select * from capital_diligence_document where talimar_no = '".$talimar_loan."'");
		$capital_diligence_document = $capital_diligence_document->result();

		//fetch_borrower_prior_project...

		//fetch_borrower_prior_project...
		$borro_dprior = $this->User_model->query("SELECT * FROM borrower_prior_project WHERE borrower_id = '".$borrower."'");
		if($borro_dprior->num_rows() > 0){
			$borro_dprior = $borro_dprior->result();
			$data['prior_project'] = $borro_dprior;
		}else{
			$data['prior_project'] = '';
		}
		
		
		//fetch_borrower_contact_data...
		$borrower_contact = $this->User_model->query("select * from borrower_contact where borrower_id = '".$borrower."' AND contact_responsibility = '1'");
		if($borrower_contact->num_rows() > 0){
		$borrower_contact = $borrower_contact->result();
		foreach($borrower_contact as $row){
			
			$contact_id = $row->contact_id;
			//fetch contact details...
			$select_contact = $this->User_model->query("Select * from contact where contact_id = '".$contact_id."'");
			$select_contact = $select_contact->result();
			
			//fetch_borrower_contact type data...
			$borrower_contact_type = $this->User_model->query("select * from borrower_contact_type where contact_id = '".$contact_id."'");
			$borrower_contact_type = $borrower_contact_type->result();
			/*echo '<pre>';
			print_r($borrower_contact_type);
			echo '</pre>';*/

			//contact associate with how many borrower...
			$howmanyborrower = $this->User_model->query("Select borrower_id from borrower_contact where contact_id = '".$contact_id."'");
			$activeData = array();
			$PaidoffData= array();
			if($howmanyborrower->num_rows() > 0){
				$howmanyborrower = $howmanyborrower->result();
				foreach ($howmanyborrower as $value) {

					
					
					//get Active data loan...
					$select_loansdd = "Select Count(*) as total_count, SUM(l.loan_amount) as total_amount from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where l.borrower = '".$value->borrower_id."' AND ls.loan_status = '2'";
					$fetch_loansdd = $this->User_model->query($select_loansdd);
					$fetch_loansdd = $fetch_loansdd->result();

					$total_count = $fetch_loansdd[0]->total_count;
					$total_amount = $fetch_loansdd[0]->total_amount;

					if($total_count > 0){

						$activeData[] = array(
												"count" => $total_count,
												"amount" => $total_amount,
									);
					}

					//get Paid Off data loan...
					$select_loansddd = "Select Count(*) as total_counts, SUM(l.loan_amount) as total_amounts from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where l.borrower = '".$value->borrower_id."' AND ls.loan_status = '3'";
					$fetch_loansddd = $this->User_model->query($select_loansddd);
					$fetch_loansddd = $fetch_loansddd->result();

					$total_counts = $fetch_loansddd[0]->total_counts;
					$total_amounts = $fetch_loansddd[0]->total_amounts;

					if($total_counts > 0){

						$PaidoffData[] = array(
												"counts" => $total_counts,
												"amounts" => $total_amounts,
									);
					}
				}
			}

			

		    $contact_fetails[] = array("contact_name"		=> $select_contact[0]->contact_firstname.' '.$select_contact[0]->contact_middlename.' '.$select_contact[0]->contact_lastname,
										"contact_id"		=> $select_contact[0]->contact_id,
										"contact_fname"		=> $select_contact[0]->contact_firstname,
										"contact_lname"		=> $select_contact[0]->contact_lastname,
										"contact_about"		=> $select_contact[0]->contact_about,
										"credit_score"		=> $borrower_contact_type[0]->borrower_credit_score,
										"employment_status"	=> $borrower_contact_type[0]->employment_status,
										"job_title"			=> $borrower_contact_type[0]->job_title,
										"current_employer"	=> $borrower_contact_type[0]->current_employer,
										"annual_income"		=> $borrower_contact_type[0]->prvious_yr_income,
										"year"				=> $borrower_contact_type[0]->prvious_income_year,
										"year_at_job"		=> $borrower_contact_type[0]->year_at_job,
										"bankruptcy_year"	=> $borrower_contact_type[0]->bankruptcy_year,
										"bank_year_text"	=> $borrower_contact_type[0]->bankruptcy_7year_text,
										"us_citizen"		=> $borrower_contact_type[0]->borrower_us_citizen,
										"us_citizen_desc"	=> $borrower_contact_type[0]->borrower_us_citizen_desc,
										"litigation"		=> $borrower_contact_type[0]->borrower_litigation,
										"litigation_desc"	=> $borrower_contact_type[0]->borrower_litigation_desc,
										"felony"			=> $borrower_contact_type[0]->borrower_felony,
										"felony_desc"		=> $borrower_contact_type[0]->borrower_felony_desc,
										"credit_score_desc"	=> $borrower_contact_type[0]->borrower_credit_score_desc,
										"activeData"		=> $activeData,
										"PaidoffData"		=> $PaidoffData
										
										);
			// }
			}

			$data['select_contact_details']					= $contact_fetails;
		}
		


		$getUnderImgonly = $this->User_model->query("SELECT * FROM `property_folders` WHERE `talimar_loan`='".$talimar_loan."' And title = 'Underwriting' AND `property_home_id`='".$property_home_id."'");

		if($getUnderImgonly->num_rows() > 0){

			$getUnderImgonly = $getUnderImgonly->result();
			foreach ($getUnderImgonly as $key => $value) {
				
				$folderidarray[] = $value->id;
			}

			//print_r($folderidarray);

			$folderidarrayVAl = implode("','", $folderidarray);
			//echo $folderidarrayVAl;

			$property_img_sql = "SELECT * FROM `loan_property_images` WHERE `folder_id` IN('".$folderidarrayVAl."')";
			$property_img = $this->User_model->query($property_img_sql);
			$property_img = $property_img->result();

		}else{
			$property_img = '';
		}

		
		//fetch sum of investment...
		$assignment_sql = "Select SUM(investment) as total_investment, SUM(payment) as total_payment, invester_yield_percent from loan_assigment where talimar_loan = '".$talimar_loan."'";
		$assignment = $this->User_model->query($assignment_sql);
		$assignment = $assignment->result();
		$data['assignment_investment'] = $assignment[0]->total_investment;
		$data['assignment_payment']	   = $assignment[0]->total_payment;
		$data['invester_yield_percent']	  = $assignment[0]->invester_yield_percent;


		//fetch status...
		$assignment_sqlstatus = "Select lender_name, ndisclouser_status from loan_assigment where talimar_loan = '".$talimar_loan."'";
		$assignment_sqlstatus = $this->User_model->query($assignment_sqlstatus);
		$assignmentStatus = $assignment_sqlstatus->result();
		foreach ($assignmentStatus as  $value) {
			
			$assignmentStatusarray[$value->lender_name] = $value->ndisclouser_status;
		}
		$data['assignmentStatus'] = $assignmentStatusarray;
		
		//fetch ecumnbrance data...
		$select_ecum = $this->User_model->query("SELECT *,SUM(`current_balance`) as total_current_val FROM `encumbrances` WHERE `talimar_loan` = '".$talimar_loan."' AND will_it_remain = '0'");
		$select_ecum = $select_ecum->result();

		$totalEcumTotal = 0;
		foreach ($select_ecum as $value) {
			$totalEcumTotal += $value->total_current_val;
		}
		
		$data['current_ecum'] = $totalEcumTotal;
		
		
		//fetch full ecumnbrance details...
		$select_ecumbrance = $this->User_model->query("SELECT * FROM `encumbrances` WHERE `talimar_loan` = '".$talimar_loan."' AND `property_home_id` = '".$property_home_id."'");
		$select_ecumbrance = $select_ecumbrance->result();
		
		//fetch full ecumnbrance data details...
		$select_ecumbrance_data = $this->User_model->query("SELECT * FROM `encumbrances_data` WHERE `talimar_loan` = '".$talimar_loan."'");
		$select_ecumbrance_data = $select_ecumbrance_data->result();
		
		//fetch_property home data...
		
		$property_home = $this->User_model->query("Select COUNT(*) as total_count from property_home where talimar_loan = '".$talimar_loan."' AND property_address != ''");
		$property_home = $property_home->result();
		
		$data['count_property'] = $property_home[0]->total_count;
		
		//fetch monthly_income data...
		$monthly_income = $this->User_model->query("SELECT * FROM `monthly_income` WHERE `talimar_loan`='".$talimar_loan."' AND `property_home_id`='".$property_home_id."'");	
		$monthly_income = $monthly_income->result();
		
		//fetch monthly_expense data...
		$monthly_expense = $this->User_model->query("SELECT * FROM `monthly_expense` WHERE `talimar_loan`='".$talimar_loan."' AND `property_home_id`='".$property_home_id."'");	
		$monthly_expense = $monthly_expense->result();
		
		
		//fetch login user accounts...
		$sql = "SELECT * FROM lender_contact JOIN investor ON lender_contact.lender_id = investor.id WHERE lender_contact.contact_id = '".$this->session->userdata('p_user_id')."' AND lender_contact.link_to_lender = '2'";
		
		$fetch_account = $this->User_model->query($sql);
		$fetch_account = $fetch_account->result();
		$data['fetch_account'] = $fetch_account;
	
		$fetch_additional_loan = $this->User_model->query("select loan_purpose from servicing_purpose where talimar_loan='".$talimar_loan."'");
		if($fetch_additional_loan->num_rows() > 0)
		{
			$fetch_additional_loan = $fetch_additional_loan->result();
			$fetch_additional_loan_result = $fetch_additional_loan;
			$fetch_additional_loan_result[0]->loan_purpose;
			$data['loan_purpose']=$fetch_additional_loan_result[0]->loan_purpose;

				
		}
			
		//Lender_associate_with_loginUser
		$allLenders = $this->Lender_associate_with_loginUser();
		$data['allLenders'] = $allLenders;

		$implodeArray = implode("','",$allLenders);
		$fetchDocusign = $this->User_model->query("SELECT * FROM `docusign_link` WHERE `lender_id` IN('".$implodeArray."')");
		if($fetchDocusign->num_rows() > 0){
			$fetchDocusign = $fetchDocusign->result();
		}else{
			$fetchDocusign = '';
		}

		$data['fetchDocusign'] = $fetchDocusign;

		$data['valuation_vendor'] = $valuation_vendor;
		$data['valuation_based'] = $valuation_based;
		$data['trust_deed_loan_details']				= $select_loan;
		$data['trust_deed_loan_servicing_details']		= $select_loan_servicing;
		$data['select_property']						= $select_property;
		$data['borrower_data_details']					= $borrower_data;
		$data['capital_diligence_document']				= $capital_diligence_document;
		
		$data['select_property_img']					= $property_img;
		$data['select_portal_account']					= $fetch_account;
		$data['select_ecumbrance']						= $select_ecumbrance;
		$data['select_ecumbrance_data']					= $select_ecumbrance_data;
		$data['select_monthly_income']					= $monthly_income;
		$data['select_monthly_expense']					= $monthly_expense;
		$data['multi_properties']						= $select_property_ho[0]->multi_properties;

		/*03-10-2021*/		
		$talimar_loan_id = array();
		$talimar_loan_id['talimar_loan'] = $this->uri->segment(2);
		$fetch_escrow_data = $this->User_model->select_where('loan_escrow', $talimar_loan_id);
		if ($fetch_escrow_data->num_rows() > 0) {
			$fetch_escrow_data = $fetch_escrow_data->result();

			foreach ($fetch_escrow_data as $row) {

				$q = "select * from contact where contact_firstname ='" . $row->first_name . "' AND contact_lastname = '" . $row->last_name . "'";
				$fetch_contact = $this->User_model->query($q);
				$fetch_contact = $fetch_contact->result();

				$c_idd = $fetch_contact[0]->contact_id;

				$sql = "select contact_id,contact_firstname,contact_middlename,contact_lastname from contact where contact_id = '" . $row->first_name . "'";
				$fetch_contact = $this->User_model->query($sql);
				$fetch_contact = $fetch_contact->result();

				$contact = $fetch_contact ? $fetch_contact[0]->contact_firstname . ' ' . $fetch_contact[0]->contact_middlename . ' ' . $fetch_contact[0]->contact_lastname : '0';
				$contact1 = $fetch_contact[0]->contact_firstname;
				$c_id = $fetch_contact[0]->contact_id;

				
				$escrow_contact_name = array(
					"id" => $row->id,
					"talimar_loan" => $row->talimar_loan,
					"first_name" => $row->first_name,
					"last_name" => $row->last_name,
					"company" => $row->company,
					"escrow" => $row->escrow,
					"phone" => $row->phone,
					"address" => $row->address,
					"address_b" => $row->address_b,
					"city" => $row->city,
					"state" => $row->state,
					"zip" => $row->zip,
					"email" => $row->email,
					"first_payment" => $row->first_payment_close,
					"net_fund" => $row->net_fund,
					"tax_service" => $row->tax_service,
					"tax_service" => $row->tax_service,
					"record_assigment_close" => $row->record_assigment_close,
					"expiration" => $row->expiration,
					"instruction" => $row->instruction,
					"t_user_id" => $row->t_user_id,
					"full_name" => $contact,
					"full_fname" => $contact1,
					"c_id" => $c_id,
					"c_idd" => $c_idd,
				);
			}
			
			$data['escrow_contact_name'] = $escrow_contact_name;
		}

		$fetch_title_data = $this->User_model->select_where('loan_title', $talimar_loan_id);
		if ($fetch_title_data->num_rows() > 0) {
			$fetch_title_data = $fetch_title_data->row();
			$data['fetch_title_data'] = $fetch_title_data;

			$sql = "select contact_id,contact_firstname,contact_middlename,contact_lastname from contact where contact_id = '" . $fetch_title_data->first_name . "'";
			$fetch_title_contact = $this->User_model->query($sql);
			$fetch_title_contact = $fetch_title_contact->row();

			$t_contact = $fetch_title_contact ? $fetch_title_contact->contact_firstname . ' ' . $fetch_title_contact->contact_middlename . ' ' . $fetch_title_contact->contact_lastname : '0';
		}

		$data['full_nameCompny'] = $t_contact;

		/*03-10-2021*/

		//View by lender user...
		if($this->session->userdata('p_user_id') != ''){
			$this->View_trust_deed_by_user($this->session->userdata('p_user_id'),$property_address_val,$talimar_loan);
		}


		$diligence_document = $this->User_model->query("SELECT * FROM capital_diligence_document WHERE talimar_no = '".$talimar_loan."' ORDER BY id ASC");
		$diligence_document = $diligence_document->result();
									
		$data['diligence_document'] = $diligence_document;

		$appraisal_document    = $this->User_model->query("SELECT ad.* FROM property_appraisal_document AS ad INNER JOIN loan_property AS lp ON ad.property_id=lp.property_home_id WHERE ad.talimar_loan = '$talimar_loan' AND lp.valuation_type IN (1,2)");
        $appraisal_document    = $appraisal_document->result();
        $data['appraisal_document'] = $appraisal_document;

        $query_payemnt_grantor = $this->User_model->query("SELECT contact.contact_firstname, contact.contact_lastname,borrower_contact.borrower_credit_score AS credit_score, gurrantor.* FROM payment_gurrantor AS gurrantor INNER JOIN borrower_contact_type AS borrower_contact ON gurrantor.contact_name=borrower_contact.contact_id  INNER JOIN contact ON borrower_contact.contact_id=contact.contact_id WHERE gurrantor.talimar_loan = '$talimar_loan'");
			if ($query_payemnt_grantor->num_rows() > 0) {
				$fetch_payment_gurrantor = $query_payemnt_grantor->result();
			} else {
				$fetch_payment_gurrantor = '';
			}

		$data['fetch_payment_gurrantor'] = $fetch_payment_gurrantor;
		
		$data['page'] = 'trustDeedPortfolio';
		$data['content'] = $this->load->view('Available_trust_deed/trust_deed_details',$data,true);
		$this->load->view('template_files/template',$data);
	}


	public function View_trust_deed_by_user($loginUser,$property_address,$talimar_loan){

		$fetchviewby = $this->User_model->query("SELECT * FROM `trust_deed_view` WHERE p_user_id = '".$loginUser."' AND talimar_loan = '".$talimar_loan."'");
		if($fetchviewby->num_rows() > 0){

			$fetchviewby = $fetchviewby->result();

			$countview = $fetchviewby[0]->view_count;
			$addcountview = $countview + 1;

			$updateforview = $this->User_model->query("UPDATE `trust_deed_view` SET `view_count`= '".$addcountview."', `date`='".date('Y-m-d')."', `time`='".date('h:i:s a')."' WHERE p_user_id = '".$loginUser."' AND talimar_loan = '".$talimar_loan."'");

		}else{

			$insertforview = $this->User_model->query("INSERT INTO `trust_deed_view`(`p_user_id`, `talimar_loan`, `prop_address`, `view_count`, `date`, `time`) VALUES ('".$loginUser."', '".$talimar_loan."', '".$property_address."', '1', '".date('Y-m-d')."', '".date('h:i:s a')."')");
		}

		return;
	}

	public function Lender_associate_with_loginUser(){

		$loginUser = $this->session->userdata('p_user_id');
		$fetchlenders = $this->User_model->query("SELECT lender_id FROM lender_contact Where contact_id = '".$loginUser."'");
		$fetchlenders = $fetchlenders->result();
		foreach ($fetchlenders as $value) {
			$allArray[] = $value->lender_id;
		}

		return $allArray;

	} 

	public function fetch_lender_contact_info($lender_id){

		$fetch_contact_id = $this->User_model->query("SELECT contact_id FROM `lender_contact` WHERE `lender_id`='".$lender_id."'");
		if($fetch_contact_id->num_rows() > 0){

			$fetch_contact_id = $fetch_contact_id->result();
			$contact_id = $fetch_contact_id[0]->contact_id;

			$fetch_contact_info = $this->User_model->query("SELECT contact_firstname, contact_middlename, contact_lastname, contact_phone, contact_email FROM contact WHERE contact_id='".$contact_id."'");
			$fetch_contact_info = $fetch_contact_info->result();
			$name = $fetch_contact_info[0]->contact_firstname.' '.$fetch_contact_info[0]->contact_middlename.' '.$fetch_contact_info[0]->contact_lastname;
			$phone = $fetch_contact_info[0]->contact_phone;
			$email = $fetch_contact_info[0]->contact_email;

			$all_array = array(
									"cname" => $name,
									"cphone" => $phone,
									"cemail" => $email,
								);
		}else{

			$all_array = array(
									"cname" => '',
									"cphone" => '',
									"cemail" => '',
								);
		}

		return $all_array;
	}

	public function full_property_address($talimar_loan){

		$fetch_property = $this->User_model->query("SELECT property_address, unit, city, state, zip from loan_property where talimar_loan = '".$talimar_loan."'");
		if($fetch_property->num_rows() > 0){
			$fetch_property = $fetch_property->result();

			$full_address = '';
			if($fetch_property[0]->property_address){
				$full_address .= $fetch_property[0]->property_address.'';
			}
			if($fetch_property[0]->unit){
				$full_address .= ' '.$fetch_property[0]->unit.'; ';	
			}else{
				$full_address .= '; ';
			}
			if($fetch_property[0]->city){
				$full_address .= $fetch_property[0]->city.', ';	
			}
			if($fetch_property[0]->state){
				$full_address .= $fetch_property[0]->state.' ';
			}
			if($fetch_property[0]->zip){
				$full_address .= $fetch_property[0]->zip.' ';
			}
			//$full_address = $fetch_property[0]->property_address.' '.$fetch_property[0]->unit.'; '.$fetch_property[0]->city.', '.$fetch_property[0]->state.' '.$fetch_property[0]->zip;

			$array_address = array("full_address"=>$full_address,);
		}else{
			$array_address = array("full_address"=>'',);
		}

		return $array_address;
	}

	public function send_email_to_users(){

		$investor_relation = $this->User_model->query("SELECT user_id FROM `user_settings` WHERE `investor_relation`='1'");
		if($investor_relation->num_rows() > 0){
			$investor_relation = $investor_relation->result();
		}

		$admin_user = $this->User_model->query("SELECT id as user_id FROM `user` WHERE `role`='2'");
		if($admin_user->num_rows() > 0){
			$admin_user = $admin_user->result();
		}

		$merge_user_id = array_merge($investor_relation,$admin_user);
		foreach($merge_user_id as $value) {
		
			$user_id = $value->user_id;
			$fetch_email_id = $this->User_model->query("SELECT email_address FROM `user` WHERE `id`='".$user_id."'");
			$fetch_email    = $fetch_email_id->result();

			foreach($fetch_email as $val){
				$all_email_array[] = array("email"=>$val->email_address);

				
			}
			
		}
		
		return $all_email_array;
	}

	public function array_multi_unique($multiArray){

	  $uniqueArray = array();

	  foreach($multiArray as $subArray){

	    if(!in_array($subArray, $uniqueArray)){
	      $uniqueArray[] = $subArray;
	    }
	  }
	  return $uniqueArray;
	}
	
	
	public function questionData(){

		if($this->session->userdata('p_user_id') != ''){
			
		}
		else
		{
			redirect(base_url());
		}

		$username 	= $this->input->post('username');
		$loannumber = $this->input->post('loannumber');
		$question 	= $this->input->post('question');
		$talimar_loan 	= $this->input->post('talimar_loan');
		if($username && $loannumber && $question !=''){
			/*****************************Auto Notification - Property Insurance Expiring Start****************************/
			$where['talimar_loan'] 				= $talimar_loan;
			$maturityNotificationQuery 			= $this->User_model->select_where('loan', $where);
			$maturityNotificationQueryResult 	= $maturityNotificationQuery->result();
			if(!empty($maturityNotificationQueryResult)){
				foreach($maturityNotificationQueryResult as $key => $maturityNotificationRow){				
						$talimarLoanId				= $maturityNotificationRow->id;
						$talimarLoan				= $maturityNotificationRow->talimar_loan;
						$borrowerId 				= $maturityNotificationRow->borrower;
						$BorrowerFirstName		= "";
						$BorrowerLastName       = "";
						$BorrowerFirstEmail		= "";
						$BorrowerPhone          = "";
						$property_address 		= "";
						$unit 					= "";
						$city 					= "";
						$state 					= "";
						$zip 					= "";
						$propertyAddressQuery 	= $this->User_model->query("SELECT property_address, unit, city, state, zip FROM loan_property WHERE talimar_loan = '".$talimarLoan."'");
						$propertyAddressQueryResult = $propertyAddressQuery->result();					
						if(!empty($propertyAddressQueryResult)){
							$property_address   = $propertyAddressQueryResult[0]->property_address;
							$unit 				= $propertyAddressQueryResult[0]->unit;
							$city 				= $propertyAddressQueryResult[0]->city;
							$state 				= $propertyAddressQueryResult[0]->state;
							$zip 				= $propertyAddressQueryResult[0]->zip;
						}
						$contactId=$this->session->userdata('p_user_id');
						$BorrowerContactSql 	= $this->User_model->query("SELECT c.contact_firstname,c.contact_lastname,c.contact_email,c.contact_phone FROM  contact as c  WHERE c.contact_id = '".$contactId."' ");					
						$BorrowerContactResult 	= $BorrowerContactSql->result();
						if(!empty($BorrowerContactResult)){
							$BorrowerFirstName	=$BorrowerContactResult[0]->contact_firstname;
							$BorrowerLastName	=$BorrowerContactResult[0]->contact_lastname;
							$BorrowerFirstEmail	=$BorrowerContactResult[0]->contact_email;
							$BorrowerPhone	    =$BorrowerContactResult[0]->contact_phone;
						}
						$contentMail  = '<p>'.$BorrowerFirstName.' '.$BorrowerLastName.' submitted a question.</p>';
						$contentMail .='<p></p>';
				        $contentMail .= '<b>User Name:</b> '.$BorrowerFirstName.' '.$BorrowerLastName.'. <br>';
				        $contentMail .= '<b>User Phone:</b> '.$BorrowerPhone.'. <br>';
				        $contentMail .= '<b>User E-Mail:</b><a href="mailto:"'.$BorrowerFirstEmail.'"> '.$BorrowerFirstEmail.'</a>. <br>';
				        $contentMail .= '<b>Property Address:</b> '.$property_address.' '.$unit.'; '.$city.', '.$state.' '.$zip.'. <br>';
				        $contentMail .= '<b>Loan Number:</b> '.$talimarLoan.'. <br>';
				        $contentMail .= '<b>Question:</b> '.$question.'. <br>';
				        $contentMail .='<p></p>';
				        $regards = '
								Loan Servicing<br>
								TaliMar Financial';
						$dataMail = array();
				        $dataMail['from_email']  	= 'servicing@talimarfinancial.com';
				        $dataMail['from_name']   	= 'TaliMar Financial';
				        $dataMail['to']      		= 'amarjain@bitcot.com';
				       // $dataMail['to']      		= 'invest@talimarfinancial.com';
				        $dataMail['cc']      		= '';
				        $dataMail['bcc']      		= '';
				        $dataMail['name']      		= '';
				        $dataMail['button_url']  	= '';
						$dataMail['button_text']    = '';
						$dataMail['attach_file']    = '';
				        $dataMail['subject']     	= "Lender Question– ".$property_address." ".$unit;
				        $dataMail['content_body']  	= $contentMail;
				        $dataMail['regards']   = $regards;
				        send_mail_template($dataMail);
				}
			}
			/**************************Auto Notification - Property Insurance Expiring END*******************************/
		

			/*$mailto		= 'invest@talimarfinancial.com';
			
			$subject	= "Question from ".$username;  
			$message  ='';
	    	$message .= '<p style="font-size: 14px;">'.$username.' submitted a question on the Lender Portal regarding '.$loannumber.'
										<br>
										<p style="font-size: 14px;"><b>User Name:</b> '.$username.'
											<br><b>Loan Number:</b> '.$talimar_loan.'
											<br><b>Property Address:</b> '.$loannumber.'
											<br><b>Question:</b> '.$question.'
											<br>
										</p>';

	    		
				$maildata = array();
				$maildata['from_email'] = 'invest@talimarfinancial.com';
				$maildata['from_name'] = $subject;
				$maildata['to'] = $mailto;
				$maildata['name'] = $username;
				$maildata['subject'] = 'Trust Deed Question';
				$maildata['content_body'] = $message;
				$maildata['button_url'] = '';
				$maildata['button_text'] = '';
				send_mail_template($maildata);*/

				$this->session->set_flashdata('success','We have received your question. Your account representative will respond shortly.');
				redirect(base_url().'trust_deed_details/'.$talimar_loan);
		}else{

			$this->session->set_flashdata('error','Something is wrong. please fill all fields!');
			redirect(base_url().'trust_deed_details/'.$talimar_loan);
		}
	}

	public function confirm_order(){

		if($this->session->userdata('p_user_id') != ''){
			
		}
		else
		{
			redirect(base_url());
		}

		if(isset($_POST['submit'])){

			$data['talimar_loan'] 			= $this->input->post('talimar_loan');
			$data['lender_name'] 			= $this->input->post('lender_id');
			$data['investment'] 			= $this->input->post('investment_amount');
			$data['investmenttotal'] 		= $this->input->post('investment_amount');
			$data['payment'] 				= $this->input->post('payment');
			$data['paymenttotal'] 			= $this->input->post('payment');
			$data['percent_loan'] 			= str_replace("%", "", $this->input->post('percent_loan'));
			//$data['invester_yield_percent'] = str_replace("%", "", $this->input->post('invester_yield_percent'));
			$data['invester_yield_percent'] = str_replace("%", "", $this->input->post('lender_ratenew'));
			
			$lender_ratenew 				= $this->input->post('lender_ratenew');

			$this->User_model->insertdata('loan_assigment',$data);
			$this->session->set_flashdata('success', 'We have received your commitment of $'.number_format($this->input->post('investment_amount')).'. A TaliMar Financial representative will contact you shortly to review your commitment. Should you have any immediate questions, please contact us at (858) 613-0111 ext. 3.');

			//Track subscription data...
			$this->track_subscrption_info($this->input->post('talimar_loan'),$this->input->post('lender_id'),$this->input->post('investment_amount'));

			//send email to admin...
			$this->send_confirm_email($this->input->post('talimar_loan'),$this->input->post('lender_id'),$this->input->post('investment_amount'));

			//send email to user...

			$this->send_confirm_email_to_user($this->input->post('talimar_loan'),$this->input->post('lender_id'),$this->input->post('investment_amount'),$lender_ratenew);

			
			redirect(base_url().'trust_deed_details/'.$this->input->post('talimar_loan'));
		}
	}

	public function track_subscrption_info($talimar_loan,$lender_id,$investment_amount){

		$get_lender_name = $this->fetch_lender_name($lender_id);
		$get_loan_info = $this->fetch_loan_info($talimar_loan);
		$fetch_street_address = $this->fetch_street_address($talimar_loan);

		$data['loan_id'] 			= $get_loan_info[0]->loan_id;
		$data['contactID'] 			= $this->session->userdata('p_user_id');
		$data['talimar_loan'] 		= $talimar_loan;
		$data['lender_id'] 			= $lender_id;
		$data['street_address'] 	= $fetch_street_address;
		$data['lender_name'] 		= $get_lender_name;
		$data['investment'] 		= $investment_amount;
		$data['datetime'] 			= date('Y-m-d h:i:s a');

		$this->User_model->insertdata('track_subscriptions',$data);

		return;
	}


	public function send_confirm_email($talimar_loan,$lender_id,$investment_amount){

		$fetch_full_address = $this->full_property_address($talimar_loan);
		$fetch_lender_contacts = $this->fetch_lender_contact_info($lender_id);
		$get_lender_name = $this->fetch_lender_name($lender_id);

		$cname = $fetch_lender_contacts['cname'] ? $fetch_lender_contacts['cname'] : '';
		$full_address = $fetch_full_address['full_address'];

		$mailto		= 'invest@talimarfinancial.com';		
		$subject	= "Talimar Financial - Trust Deed Order"; 
		 
		$message  ='';

    	$message .= '<p style="font-size: 14px;">'.$cname.' has subscribed '.$get_lender_name.' to the trust deed secured on '.$full_address.'.
				<br>
				<p style="font-size: 14px;"><b>Contact Name:</b> '.$cname.'
					<br><b>Account Name:</b> '.$get_lender_name.'
					<br><b>TaliMar Loan #:</b> '.$talimar_loan.'
					<br><b>Property Address:</b> '.$full_address.'
					<br><b>Investment Amount:</b> $'.number_format($investment_amount).'
					<br>
				</p>';

    		
			$maildata = array();
			$maildata['from_email'] = 'invest@talimarfinancial.com';
			$maildata['from_name'] = $subject;
			$maildata['to'] = $mailto;
			$maildata['name'] = $cname;
			$maildata['subject'] = 'Trust Deed Confirmation';
			$maildata['content_body'] = $message;
			$maildata['button_url'] = '';
			$maildata['button_text'] = '';
			send_mail_template($maildata);

			return;
	}

	public function fetch_loan_info($talimar_loan){

		$getloan = $this->User_model->query("SELECT l.id as loan_id, l.loan_amount, ls.loan_status, l.loan_funding_date FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE l.talimar_loan = '".$talimar_loan."'");
		$getloan = $getloan->result();

		return $getloan;
	}

	public function fetch_street_address($talimar_loan){

		$fetchaddress = $this->User_model->query("SELECT property_address FROM loan_property where talimar_loan = '".$talimar_loan."'");
		$fetchaddress = $fetchaddress->result();
		$streetAdd = $fetchaddress[0]->property_address;

		return $streetAdd;
	}

	public function send_confirm_email_to_user($talimar_loan,$lender_id,$investment_amount,$lender_rate){

		$fetch_full_address = $this->full_property_address($talimar_loan);
		$full_address = $fetch_full_address['full_address'];
		$get_lender_name = $this->fetch_lender_name($lender_id);
		$get_loan_info = $this->fetch_loan_info($talimar_loan);
		$fetch_street_address = $this->fetch_street_address($talimar_loan);

		if($get_loan_info[0]->loan_status == '2'){
			$extimatedata = 'Active Loan';
		}else{
			$extimatedata = date('m-d-Y', strtotime($get_loan_info[0]->loan_funding_date));
		}

		$fetchlendercon = $this->User_model->query("SELECT contact_id FROM lender_contact WHERE lender_id = '".$lender_id."'");
		if($fetchlendercon->num_rows() > 0){

			$fetchlendercon = $fetchlendercon->result();
			foreach ($fetchlendercon as $value) {

				$fetchcontact = $this->User_model->query("SELECT contact_id, contact_firstname, contact_email FROM contact WHERE contact_id = '".$value->contact_id."'");
				$fetchcontact = $fetchcontact->result();
				
			
				$mailto		= $fetchcontact[0]->contact_email;
				
				$subject	= "Confirmation – ".$fetch_street_address; 
				 
				$message  ='';
		    	$message .= '<p style="font-size: 14px;">
											We have received your subscription request in the amount of $'.number_format($investment_amount).' for '.$full_address.'. A Lender Account Representative will contact you shortly to confirm your interest and discuss the next step in the funding process.</p>										
										<p style="font-size: 14px;">	<b>Account: </b>'.$get_lender_name.'
											<br>	<b>Property: </b>'.$full_address.'
											<br>	<b>Investment: </b>$'.number_format($investment_amount).'
											<br>	<b>Loan Amount: </b>$'.number_format($get_loan_info[0]->loan_amount).'
											<br>	<b>% Interest: </b>'.number_format($investment_amount/$get_loan_info[0]->loan_amount*100,2).'%
											<br>	<b>Lender Rate: </b>'.number_format($lender_rate,2).'%
											<br>	<b>Payment Amount: </b>$'.number_format($investment_amount * ($lender_rate/12)/100,2).' per month
											<br>	<b>Estimated Closing Date: </b>'.$extimatedata.'
											<br>
										</p>';

					$maildata = array();
					$maildata['from_email'] = 'invest@talimarfinancial.com';
					$maildata['from_name'] = $subject;
					$maildata['to'] = $mailto;
					$maildata['name'] = $fetchcontact[0]->contact_firstname;
					$maildata['subject'] = 'Trust Deed Confirmation';
					$maildata['content_body'] = $message;
					$maildata['button_url'] = '';
					$maildata['button_text'] = '';
					send_mail_template($maildata);

			}
		}	
		return;
	}

	public function fetch_lender_name($lender_id){

		$get_lender_name = $this->User_model->query("SELECT name FROM `investor` WHERE id= '".$lender_id."'");
		if($get_lender_name->num_rows() > 0){

			$get_lender_name = $get_lender_name->result();
			$lender_name = $get_lender_name[0]->name;
		}else{
			$lender_name = '';
		}

		return $lender_name;
	}

	public function confirm_order_old(){
		error_reporting(0);
		
		$fetch_full_address = $this->full_property_address($this->input->post('talimar_loan'),$this->input->post('property_address'));
		$fetch_lender_contacts = $this->fetch_lender_contact_info($this->input->post('lender_id'));

		$cname = $fetch_lender_contacts['cname'] ? $fetch_lender_contacts['cname'] : '';
		$cphone = $fetch_lender_contacts['cphone'] ? $fetch_lender_contacts['cphone'] : '';
		$cemail = $fetch_lender_contacts['cemail'] ? $fetch_lender_contacts['cemail'] : '';

		$full_address = $fetch_full_address['full_address'] ? $fetch_full_address['full_address'] : $this->input->post('property_address');

		$all_email_address = $this->send_email_to_users();
		$unique_emails = $this->array_multi_unique($all_email_address);
		
		$l_amount=str_replace(",","",$this->input->post('loan_amount'));
		 	$new_l_amount=str_replace(",","",$l_amount);
		
			$talimar_loan = $this->input->post('talimar_loan');


			$fetch['talimar_loan']		= $talimar_loan;
			$fetch_loan  				= $this->User_model->select_where('loan',$fetch);
			if($fetch_loan->num_rows() > 0){

				$fetch_loan_result  		= $fetch_loan->result();
				
			  }

			$loan_assigment_data 	= $this->User_model->select_where_asc('loan_assigment',$fetch,'position');
			if($loan_assigment_data->num_rows() > 0){
			
			$loan_assigment_data					= $loan_assigment_data->result();
				
				}
			
			$loan_servicing_data 	= $this->User_model->select_where('loan_servicing',$fetch);
			
			if($loan_servicing_data->num_rows() > 0){
				
			$loan_servicing_data					= $loan_servicing_data->result();
			
				
		  	}


			 if($new_l_amount > 0)
			 {
		 
	  			$percent_loan =($this->input->post('investment_amount') / $new_l_amount)*100;
		
			 }else{

				 $percent_loan = 0;

			 	 }

			$intrest_rate =$fetch_loan_result[0]->intrest_rate;
			
			if($loan_servicing_data->servicing_lender_rate == 'NaN' || $loan_servicing_data->servicing_lender_rate == '' ||  $loan_servicing_data->servicing_lender_rate == '0.00'){
						
						$lender_ratessss = ($loan_assigment_data[0]->invester_yield_percent) ? $loan_assigment_data[0]->invester_yield_percent :'0.00';
											
					}else{

						$lender_ratessss = ($loan_servicing_data->servicing_lender_rate)? $loan_servicing_data->servicing_lender_rate :'0.00';
					}

			$servicing_fee =$lender_ratessss;

		    if($servicing_fee == '0'){

		 		$nv= $intrest_rate - $servicing_fee;

		    }else{

				$nv=$servicing_fee;
		    }
		   
   		 	$payment = (($nv/100) * $this->input->post('investment_amount')) / 12;
	
	

		$data['percent_loan']=$percent_loan;
		$data['invester_yield_percent']=$nv;
		$data['payment']	=$payment;
		$data['talimar_loan']=$talimar_loan;
		$data['investment']=$this->input->post('investment_amount');
		$data['lender_name']=$this->input->post('lender_id');
		$data['t_user_id']=$this->session->userdata('p_user_id');
		
		
		$insert=$this->User_model->insertdata('loan_assigment',$data);
		$this->session->set_flashdata('success','Trust Deed Added Successfully!');

		
	
		
		
		$subject	= "Talimar Financial - Trust Deed Order"; 
		$header		= "From: mail@database.talimarfinancial.com"; 

		$message ='';
		$message .='<p style="font-size: 14px;">Hi,
						<br>You have received the following trust deed order.</p>
					<p style="font-size: 14px;"><strong>Contact Name: '.$cname.' <br> Contact Phone: '.$cphone.' <br> Contact Email: '.$cemail.' <br> TaliMar Loan #: '.$this->input->post('talimar_loan').' <br> Property Address: '.$full_address.'<br> Loan Amount: '.$this->input->post('loan_amount').' <br> Investment Amount: $'.number_format($this->input->post('investment_amount')).'</strong>
					</p>';

		foreach($unique_emails as $value) {
		 	

				$maildata = array();
				$maildata['from_email'] = 'mail@talimarfinancial.com';
				$maildata['from_name'] = $subject;
				$maildata['to'] = $unique_emails;
				$maildata['name'] = $cname;
				$maildata['subject'] = $subject;
				$maildata['content_body'] = $message;
				$maildata['button_url'] = '';
				$maildata['button_text'] = '';
				send_mail_template($maildata);

		}

		$single_email ='invest@talimarfinancial.com';

		$maildata = array();
		$maildata['from_email'] = 'mail@talimarfinancial.com';
		$maildata['from_name'] = $subject;
		$maildata['to'] = $single_email;
		$maildata['name'] = $cname;
		$maildata['subject'] = $subject;
		$maildata['content_body'] = $message;
		$maildata['button_url'] = '';
		$maildata['button_text'] = '';
		send_mail_template($maildata);

		$this->session->set_flashdata('success', 'We have received your commitment of $'.number_format($this->input->post('investment_amount')).'. A TaliMar Financial representative will contact you shortly to review your commitment. Should you have any immediate questions, please contact us at (858) 613-0111.');

		redirect(base_url().'trust_deed_details/'.$talimar_loan);
		
	}
	
	public function amount_format($n)
	{
		$n = str_replace(',','',$n);
		$a = trim($n , '$');
		$b = trim($a , ',');
		$c = trim($b , '%');
		return $c;
	}
	
	
	
	public function avaliable_trust_pdf()	
	{
		error_reporting(0);
		
		$position_option 	= $this->config->item('position_option');
		$property_type 		= $this->config->item('property_modal_property_type');
		$trust_deed_option	= $this->config->item('trust_deed_option');
            

		$join_sql = "SELECT *,l.id as loan_id FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.new_market_trust_deed IN('2','3','4') AND ls.loan_status IN('1','2') ORDER BY ls.loan_status DESC";
		
		$fetch_loan = $this->User_model->query($join_sql);
		$fetch_loan = $fetch_loan->result();
		
		foreach($fetch_loan as $row)
		{
					
	        $where['talimar_loan'] 	= $row->talimar_loan;
			$loan_id 				= $row->loan_id;
			$servicing_lender_rate  = $row->servicing_lender_rate;
			$fetch_property = $this->User_model->query('Select *,SUM(underwriting_value) as underwriting_value from loan_property where talimar_loan="'.$row->talimar_loan.'"');
				$fetch_property = $fetch_property->result();
			
			
			$sum_assigment = "SELECT *,SUM(payment) as total_payment,SUM(investment) as total_investment FROM loan_assigment WHERE talimar_loan = '".$row->talimar_loan."'";
			
			$assigment_sum1 				= $this->User_model->query($sum_assigment);
			$assigment_sum1 				= $assigment_sum1->result();
			
			
			$assigment_investment_amount 	= $assigment_sum1[0]->total_investment;
			$assigment_total_payment 		= $assigment_sum1[0]->total_payment;
			$invester_yield_percent 		= $assigment_sum1[0]->invester_yield_percent;
			
			$where2['id'] = $row->loan_borrower;
			
			$fetch_borrower 	= $this->User_model->select_where('borrower_data',$where2);
			$fetch_borrower1 	= $fetch_borrower->result();
			if($fetch_borrower->num_rows() > 0)
			{
				$b_name 		= $fetch_borrower1[0]->b_name;
			}
			else
			{
				$b_name 		= '';
			}
			
			$sel_ecum = $this->User_model->query("SELECT SUM(`current_balance`) as total_balance FROM `encumbrances` WHERE `talimar_loan` = '".$row->talimar_loan."' AND property_home_id !='' AND will_it_remain = '0'");
			$sel_ecum = $sel_ecum->result();
			
			
			$total_current_balance = $sel_ecum[0]->total_balance;
			
			$avaliable_trust_data[] = array(
											'talimar_loan' 		=> $row->talimar_loan,
											'loan_amount'  		=> $row->loan_amount,
											'lender_minimum'	=> $row->lender_minimum,
											'intrest_rate'		=> $row->intrest_rate,
											'closing_date'		=> $row->loan_funding_date,
											'closing_status'  	=> $row->closing_status,
											'position'			=> $row->position,
											'trust_deed'		=> $row->new_market_trust_deed,
											'property_address' 	=> $fetch_property[0]->property_address,
											'city' 				=> $fetch_property[0]->city,
											'state' 			=> $fetch_property[0]->state,
											'zip' 				=> $fetch_property[0]->zip,
											'property_type' 	=> $fetch_property[0]->property_type,
											'underwriting' 		=> $fetch_property[0]->underwriting_value,
											'sel_ecum' 			=> $total_current_balance,
											'available' 		=> $row->loan_amount - $assigment_investment_amount,
											'payment' 		    => $assigment_total_payment,
											'servicing_lender_rate' => $servicing_lender_rate,
											'invester_yield_percent' => $invester_yield_percent,
										);
			
		 } 
		
		             		
		              $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);

						// set document information
						$pdf->SetCreator(PDF_CREATOR);
						// set default header data
						 $pdf->SetHeaderData("include/icons/logo-light.png", 30, '', '');

						// set header and footer fonts
						$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
						$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

						// set default monospaced font
						$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

						// set margins
						$pdf->SetMargins(2, 20, 2);
						// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
						$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
						$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

						// set auto page breaks
						$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

						// set image scale factor
						$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
						$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
						$pdf->SetFont('times', '', 7);

						// add a page
						$pdf->AddPage('L', 'A4');
						// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
						
						$html ='<h1 style="color:#003468;font-size:18px;">Available Trust Deeds:</h1>'; 
						$html .= '<style>
						.table {
							width: 100%;
							max-width: 100%;
							margin-bottom: 20px;
							border-collapse:collapse;
						
						}
						.table-bordered {
						border: 1px solid #ddd;
						}
						table {
							border-spacing: 0;
							border-collapse: collapse;
						
						}
						.table td{
							height : 28px;
							font-size:13px;
						
							text-align:center;	
						}
						tr.table_header th{
							height:25px;
							font-size:12px;
							font-family: "Times New Roman", Georgia, Serif;
							text-align:center;
							background-color:#bfcfe3;
					
						}
						
					tr.table_bottom th{
							height:25px;
							font-size:12px;
							font-family: "Times New Roman", Georgia, Serif;
							text-align:center;
						
							background-color:#bfcfe3;
						
						
						}
					
					
						tr.odd td{
							background-color:#ededed;
						
						}
						</style>';
						
						$html .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';
						
						$html .= '<tr class="table_header">';

						$html .= '<th style="width:10%;text-decoration:underline;"><strong>Loan #</strong></th>';
						$html .= '<th style="width:10%;text-decoration:underline;"><strong>Location</strong></th>';
						$html .= '<th style="width:10%;text-decoration:underline;"><strong>Position</strong></th>';
						$html .= '<th style="width:10%;text-decoration:underline;"><strong>lender<br>Rate</strong></th>';
						$html .= '<th style="width:10%;text-decoration:underline;"><strong>Loan Amount</strong></th>'; 						
						$html .= '<th style="width:10%;text-decoration:underline;"><strong>Avaliable</strong></th>'; 
						$html .= '<th style="width:10%;text-decoration:underline;"><strong>Minimum</strong></th>';
						$html .= '<th style="width:10%;text-decoration:underline;"><strong>LTV</strong></th>';
						$html .= '<th style="width:10%;text-decoration:underline;"><strong>Closing Date</strong></th>';
						$html .= '<th style="width:10%;text-decoration:underline;"><strong>Loan Status</strong></th>';
						
						$html .= '</tr>';
						
					
						$num 				= 0;
						$minimum 		    = 0;
						$available 	        = 0;

						if(isset($avaliable_trust_data))
						{
							foreach($avaliable_trust_data as $key => $row)
							{
								$num++;
								$minimum =$minimum+$row['lender_minimum'];
								$available=$available+$row['available']; 	
								$number = $key;
									if($number % 2 == 0) {
										$class_ad = "even";
										}
									
										else
										{
											$class_ad = "odd";
										}
										  
                                    if($row['servicing_lender_rate'] == 'NaN' || $row['servicing_lender_rate'] == '' || $row['servicing_lender_rate'] == '0.00'){

                                        if($row['invester_yield_percent'] >0){

                                        $lender_ratessss = $row['invester_yield_percent'];
                                    }}else{

                                       $lender_ratessss = $row['servicing_lender_rate'];
                                    }


                                         $current_date=date('m-d-Y');
                                         $close_date=date('m-d-Y', strtotime($row['closing_date']));

                                          //if($close_date < $current_date){
                                          if($row['closing_status'] == '6'){
                                              
                                             $c="Active"; 
                                          }else{
                                              
                                              $c=$close_date;
                                          }
 
								
								$ltv = ($row['loan_amount'] + $row['sel_ecum'])/$row['underwriting'] * 100;
								
								$html .= '<tr class="'.$class_ad.'">';
								
								$html .= '<td>'.$row['talimar_loan'].'</td>';
								$html .= '<td>'.$row['city'].', '.$row['state'].'</td>';
								$html .= '<td>'.$position_option[$row['position']].'</td>';
								$html .= '<td>'.$lender_ratessss.'%'.'</td>';
								$html .= '<td>'.'$'.number_format($row['loan_amount']).'</td>';
								$html .= '<td>'.number_format($row['available']).'</td>';
								$html .= '<td>'.number_format($row['lender_minimum']).'</td>';
								$html .= '<td>'.number_format($ltv,2).'%'.'</td>';
								$html .= '<td>'.$c.'</td>';
								$html .= '<td>'.$trust_deed_option[$row['trust_deed']].'</td>';							
								$html .= '</tr>';
								
							}
						}
						
								$html .='<tfoot>
                                    <tr class="table_bottom">
                                        <th style="text-align:left"><strong>Total: '.$num.'</strong></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th><strong>'.'$'.number_format($available).'</strong></th>
                                        <th><strong>'.'$'.number_format($minimum).'</strong></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>';
                                        
                                    $html .='</tr>';
									$html .='<tr class="table_bottom">
                                        <th style="text-align:left"><strong>Average:</strong></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                     	<th><strong>'.'$'.number_format($available/$num).'</strong></th>
                                        <th><strong>'.'$'.number_format($minimum/$num).'</strong></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        
                                    </tr>
                                </tfoot>';
						
						$html .= '</table>';
						$pdf->WriteHTML($html);
						$pdf->Output("Avaliable_trust_deeds.pdf","D");
		
	}

    /*
	    Description : This function use for get mortgage loan -> Under Current Assets in Fund, only show loans with TaliMar Income Fund I LLC and TaliMar Income Fund REIT as Lenders 
	    Author      : Bitcot
	    Created     : 20-04-2021
	    Modified    : 06-05-2021
	    Modified    : 31-08-2021
	*/
	public function mortgageFund()
	{
		if($this->session->userdata('p_user_id') == '' || $this->session->userdata('mortgageActivate') != 'yes'){
			redirect(base_url());
		}
		else
		{
			$data = array();

			$investerData = $this->User_model->query('SELECT SUM(lender_mortgage.amount) as amountTotal, investor.name FROM investor LEFT JOIN lender_mortgage ON investor.id = lender_mortgage.lenderId WHERE lender_mortgage.lenderId = "'.$this->session->userdata("lender_id").'" AND lender_mortgage.fundStatus = "Received" AND investor.mortgageActivate = "yes"');
			
			$investerData = $investerData->row();
			$data['investerData'] = $investerData;

			$fetch_sql = $this->User_model->query("SELECT lp.property_type,loan.talimar_loan as talimar_loan, lp.city as city, lp.state as state, loan_servicing.servicing_lender_rate, loan_servicing.condition, loan.id as loan_id, loan.fci as fci, loan.payment_amount as payment_amount, SUM(loan.loan_amount) as loan_amount, loan.loan_amount as loan_amount_ltv, SUM(loan.current_balance) as current_balance, loan.current_balance as current_balance_t, loan.intrest_rate as intrest_rate , loan.lender_fee as lender_fee, loan.position as position,loan.loan_type as loan_type,  loan.term_month as term, loan.loan_funding_date as funding_date,  loan_servicing.loan_status as loan_status, loan_servicing.condition as loan_condition, loan_servicing.payoff_date as payoff_date, loan_servicing.servicing_fee as servicing_fee, SUM(loan_assigment.investment) as investment, loan_assigment.invester_yield_percent, loan_servicing.servicing_lender_rate as invester_yield_percent, SUM(loan_assigment.paymenttotal) as paymenttotal, '' as loanToValue FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan JOIN loan_property as lp ON lp.talimar_loan = loan_servicing.talimar_loan JOIN loan_assigment ON loan.talimar_loan = loan_assigment.talimar_loan WHERE (loan_status = '2' AND loan_assigment.lender_name = '417') OR (loan_status = '2' AND loan_assigment.lender_name = '69')  GROUP BY loan.talimar_loan ORDER BY loan_servicing.loan_status,loan.loan_funding_date ASC ");
			$loanData = $fetch_sql->result();
			$LoanDetails = array();
			if($loanData)
			{
				foreach ($loanData as $key => $val) {
					$fetch_property_home = $this->User_model->select_where_asc('property_home', array('talimar_loan'=>$val->talimar_loan), 'primary_property');

					if ($fetch_property_home->num_rows() > 0) {
						$property_home = $fetch_property_home->result();

						$total_uw_value = 0;
						$total_sl_value = 0;
						$sum_secondary_uw = 0;
						$sum_secondary_sl = 0;
						$total_of_senior_lien = 0;
						$primary_uw_value = 0;
						$primary_senior_liens_value = 0;


						foreach ($property_home as $key => $row) {

							$sql = "SELECT COUNT(*) as count_encum, SUM(current_balance) as sum_current_balance, existing_priority_to_talimar, original_balance FROM encumbrances WHERE property_home_id = '" . $row->id . "' AND talimar_loan = '" . $val->talimar_loan . "' AND will_it_remain = 0 ";
							$fetch_ecum_right = $this->User_model->query($sql);
							$fetch_ecum_right = $fetch_ecum_right->row();

							if($fetch_ecum_right){
							
								$total_of_senior_lien = $total_of_senior_lien + $fetch_ecum_right->sum_current_balance;
							}

							if ($row->primary_property == 1) {
								$primary_uw_value = $row->future_value;

								$primary_senior_liens_value = $row->senior_liens ? $row->senior_liens : 0;

							}

							if ($row->primary_property == 2) {
								
								$sum_secondary_uw += $row->future_value;

								$sum_secondary_sl += $row->senior_liens;
							}
						}

						$total_uw_value = $primary_uw_value + $sum_secondary_uw;

						$total_sl_value = $primary_senior_liens_value + $sum_secondary_sl;


						$total_cv_value = 0;
						$total_cv_sl_value = 0;
						$sum_secondary_cv = 0;
						$sum_secondary_cv_sl = 0;
						$total_of_senior_lien = 0;
						$primary_cv_value = 0;
						$primary_senior_liens_value = 0;

						foreach ($property_home as $key => $row) {

							$sql = "SELECT COUNT(*) as count_encum, SUM(current_balance) as sum_current_balance, existing_priority_to_talimar, original_balance FROM encumbrances WHERE property_home_id = '" . $row->id . "' AND talimar_loan = '" . $val->talimar_loan . "' AND will_it_remain = 0 ";
							$fetch_ecum_right = $this->User_model->query($sql);
							$fetch_ecum_right = $fetch_ecum_right->row();

							if($fetch_ecum_right)
							{
								if($fetch_ecum_right->existing_priority_to_talimar == '1'){
									$total_of_senior_lien = $total_of_senior_lien + $fetch_ecum_right->sum_current_balance;
								}
							}

							if ($row->primary_property == 1) {
								$primary_cv_value = $row->current_value;
								$primary_senior_liens_value = $row->senior_liens ? $row->senior_liens : 0;
							}

							if ($row->primary_property == 2) {


								$sum_secondary_cv += $row->current_value;

								$sum_secondary_cv_sl += $row->senior_liens;
							}
						}

					

						$total_cv_value = (double)$primary_cv_value + (double)$sum_secondary_cv;

						$total_cv_sl_value = (double)$primary_senior_liens_value + (double)$sum_secondary_cv_sl;	


						$totalV = (double)$val->loan_amount_ltv + $total_of_senior_lien;


						$val->loanToValue = number_format($totalV > 0 ?$totalV / (double)$total_uw_value * 100 : 0.00, 2);	
					}

					$LoanDetails[] = $val;

				}
			}
	
			$data['LoanDetails'] = $LoanDetails;				
			

			$data['page'] = 'mortgageFund';

			$data['content'] = $this->load->view('Available_trust_deed/mortgageFund',$data,true);
			$this->load->view('template_files/template',$data);
		 
		}
	}	
	
	
	public function export_avaliable_trust_deed(){
		
			$loan_type 						= $this->config->item('loan_type_option');
			$position_option 				= $this->config->item('position_option');
			$property_type 					= $this->config->item('property_modal_property_type');
			$yes_no_option 					= $this->config->item('yes_no_option');
			$yes_no_option3 				= $this->config->item('yes_no_option3');
			$yes_no_option4 				= $this->config->item('yes_no_option4');
			$valuation_type 				= $this->config->item('valuation_type');
			$condition 						= $this->config->item('property_modal_property_condition');
			$STATE_USA 						= $this->config->item('STATE_USA');
			$borrower_type_option 			= $this->config->item('borrower_type_option');
			$yes_how_many_times 			= $this->config->item('yes_how_many_times');

			error_reporting(0);
			header("Cache-Control: ");// leave blank to avoid IE errors
			header("Pragma: ");// leave blank to avoid IE errors
			header("Content-type: application/octet-stream");
			// header("Content-type: application/vnd.ms-word");

			header("content-disposition: attachment;filename=Trust_deed_schedule.doc");
	        echo '<html xmlns:v="urn:schemas-microsoft-com:vml"
				xmlns:o="urn:schemas-microsoft-com:office:office"
				xmlns:w="urn:schemas-microsoft-com:office:word"
				xmlns="http://www.w3.org/TR/REC-html40">

				<head>
				<meta http-equiv=Content-Type content="text/html; charset=utf-8">
				<meta name=ProgId content=Word.Document>
				<meta name=Generator content="Microsoft Word 9">
				<meta name=Originator content="Microsoft Word 9">
				<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->
				<title>title</title>
				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				body{
					font-size:10pt;
				}
				</style>
				</head>
				<body>';

		echo '<style>

		table.table{
			width:100%;
			padding : 3px 0px;
		}
		table.table th{
			text-align : left;
			font-size : 10pt;
			
		}
		table.table td{
			font-size:10pt;
			font-size : 10pt;
		
		}
		table.two_contact_signer td{
			margin : 3px;
			font-size:10pt;
		}
		h1{
			text-align:center;
		}
		h2{
			text-align:center;
		}
		p{
			font-size:10pt;
			word-spacing : 10px;
			padding : 2px 0px;
		}
		table{
			font-size:10pt;
		}
</style>';
		
        
        $talimar_loan = $this->uri->segment(3);
		
		
		$select_loan = $this->User_model->query("Select * from loan where talimar_loan = '".$talimar_loan."'");
		$select_loan = $select_loan->result();
		$borrower 	 = $select_loan[0]->borrower;
		
	
		$select_loan_servicing = $this->User_model->query("Select * from loan_servicing where talimar_loan = '".$talimar_loan."'");
		$select_loan_servicing = $select_loan_servicing->result();
		
	
		$select_property = $this->User_model->query("Select * from loan_property where talimar_loan = '".$talimar_loan."'");
		$select_property = $select_property->result();
		$property_home_id = $select_property[0]->property_home_id;
		
		
		$borrower_data = $this->User_model->query("Select * from borrower_data where id = '".$borrower."'");
		$borrower_data = $borrower_data->result();
		
		$borrower_contact = $this->User_model->query("select * from borrower_contact where borrower_id = '".$borrower."'");
		$borrower_contact = $borrower_contact->result();
		foreach($borrower_contact as $row){
			
			$contact_id = $row->contact_id;
			
			$select_contact = $this->User_model->query("Select * from contact where contact_id = '".$contact_id."'");
			$select_contact = $select_contact->result();
			
			
			$borrower_contact_type = $this->User_model->query("select * from borrower_contact_type where contact_id = '".$contact_id."'");
			$borrower_contact_type = $borrower_contact_type->result();
			
			if($row->contact_primary == '1'){
				$contact_fetails[] = array(
										"contact_name"		=> $select_contact[0]->contact_firstname.' '.$select_contact[0]->contact_middlename.' '.$select_contact[0]->contact_lastname,
										"contact_id"		=> $select_contact[0]->contact_id,
										"contact_firstname"=> $select_contact[0]->contact_firstname,
										"contact_lastname"=> $select_contact[0]->contact_lastname,
										"contact_about"		=> $select_contact[0]->contact_about,
										"credit_score"		=> $borrower_contact_type[0]->borrower_credit_score,
										
										);
			}
		}
		
	
		$select_loans = "Select *,Count(*) as total_count,SUM(l.loan_amount) as total_amount from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where l.borrower = '".$borrower."' AND ls.loan_status = '2'";
		
		$fetch_loans = $this->User_model->query($select_loans);
		$fetch_loans = $fetch_loans->result();
		$data['loan_count']	 =	$fetch_loans[0]->total_count;
		$data['loan_amount'] =	$fetch_loans[0]->total_amount;
		
		
	
		$select_loans1 = "Select *,Count(*) as total_count,SUM(l.loan_amount) as total_amount from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where l.borrower = '".$borrower."' AND ls.loan_status = '3'";
		$fetch_loans1 = $this->User_model->query($select_loans1);
		$fetch_loans1 = $fetch_loans1->result();
		$data['loan_count1']	 =	$fetch_loans1[0]->total_count;
		$data['loan_amount1'] =	$fetch_loans1[0]->total_amount;
		
		$property_img_sql = "SELECT * FROM `loan_property_images` WHERE talimar_loan = '".$talimar_loan."' AND property_home_id = '".$property_home_id."'";
		
		$property_img = $this->User_model->query($property_img_sql);
		$property_img = $property_img->result();
		
		
		$assignment_sql = "Select SUM(investment) as total_investment, SUM(payment) as total_payment from loan_assigment where talimar_loan = '".$talimar_loan."'";
		$assignment = $this->User_model->query($assignment_sql);
		$assignment = $assignment->result();
		
	
		$select_ecum = $this->User_model->query("SELECT *,SUM(`current_balance`) as total_current_val FROM `encumbrances` WHERE `talimar_loan` = '".$talimar_loan."' AND `property_home_id` = '".$property_home_id."' AND will_it_remain = '0'");
		$select_ecum = $select_ecum->result();
		$current_total_val = $select_ecum[0]->total_current_val;
		$data['current_ecum'] = $current_total_val ? $current_total_val : '0';
		
		
		
		$select_ecumbrance = $this->User_model->query("SELECT * FROM `encumbrances` WHERE `talimar_loan` = '".$talimar_loan."' AND `property_home_id` = '".$property_home_id."'");
		$select_ecumbrance = $select_ecumbrance->result();
		
	
		$select_ecumbrance_data = $this->User_model->query("SELECT * FROM `encumbrances_data` WHERE `talimar_loan` = '".$talimar_loan."'");
		$select_ecumbrance_data = $select_ecumbrance_data->result();
		
		
		
		$property_home = $this->User_model->query("Select COUNT(*) as total_count from property_home where talimar_loan = '".$talimar_loan."' AND property_address != ''");
		$property_home = $property_home->result();
		
		$data['count_property'] = $property_home[0]->total_count;
		
		
		$monthly_income = $this->User_model->query("SELECT * FROM `monthly_income` WHERE `talimar_loan`='".$talimar_loan."' AND `property_home_id`='".$property_home_id."'");	
		$monthly_income = $monthly_income->result();
		
		
		$monthly_expense = $this->User_model->query("SELECT * FROM `monthly_expense` WHERE `talimar_loan`='".$talimar_loan."' AND `property_home_id`='".$property_home_id."'");	
		$monthly_expense = $monthly_expense->result();
		

			
		    $html .= '';
		    $html .= '<div style="width:100%;">';
			$html .= '<table><tr><td><p>&nbsp;<br></p></td></tr></table>';
			$html .= '<table style="font-size:12pt;width:100%;color:white;background:#003468;font-family:Calibri, sans-serif;">';
			$html .= '<tr>';
			$html .= '<td style="font-size:12pt;">LOAN INFORMATION</td>';
			$html .= '</tr>';
			$html .= '</table>';
			$html .= '<style>
						#loan_summery_table tr{
							
							height:30px;
						}
			
			</style>';
			
			$html .= '<table class="table"  id="loan_summery_table" style="width:80%;vertical-align:top;font-size:12pt;padding-top:5px;">';
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Loan Amount:</th>';
			$html .= '<td style="width:40%;">$'.number_format($select_loan[0]->loan_amount).'</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Minimum Investment:</th>';
			$html .= '<td style="width:40%;">$'.number_format($select_loan_servicing[0]->lender_minimum).'</td>';
			$html .= '</tr>';
			
	
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Loan Term:</th>';
			$html .= '<td style="width:40%;">'.number_format($select_loan[0]->term_month).'</td>';
			$html .= '</tr>';
			
		
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Minimum Payments:</th>';
			$html .= '<td style="width:40%;">'.$yes_no_option[$select_loan[0]->min_payment].'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Cross Collateralized:</th>';
	
			$html .= '<td style="width:40%;"></td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Extension Option:</th>';
			$html .= '<td style="width:40%;">'.$yes_no_option[$select_loan[0]->extention_option].'</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<th  style="width:60%;">Closing Date:</th>';
			$html .= '<td  style="width:40%;">'.$closing_date = $select_loan[0]->loan_funding_date ? date('m-d-Y', strtotime($select_loan[0]->loan_funding_date)) : ''.'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th  style="width:60%;">Available:</th>';
			$html .= '<td  style="width:40%;">'.$available = '$'.number_format($select_loan[0]->loan_amount - $assignment[0]->total_investment).'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Lender Rate:</th>';
			
			$servicing_fee =$select_loan_servicing[0]->servicing_fee ? $select_loan_servicing[0]->servicing_fee : '1.00%';
			$html .= '<td  style="width:40%;">'.$lender_rate=$select_loan[0]->intrest_rate - $servicing_fee.'%'.'</td>';
			$html .= '</tr>';
		
			$html .= '<tr>';
			$html .= '<th  style="width:60%;">Position:</th>';
			$html .= '<td style="width:40%;">'.$position_option[$select_loan[0]->position].'</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<th  style="width:60%;"># of Minimum Payments:</th>';
			$html .= '<td  style="width:40%;">'.$select_loan[0]->minium_intrest.'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th  style="width:60%;"># of Properties:</th>';
			$html .= '<td  style="width:40%;">'.$property_home[0]->total_count.'</td>';
			$html .= '</tr>';
		   
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Extension Term:</th>';
		    $html .= '<td style="width:40%;">'.$select_loan[0]->extention_month.'</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<th  style="width:60%;">Maturity Date:</th>';
			$html .= '<td  style="width:40%;">'.$maturity_date = $select_loan[0]->maturity_date ? date('m-d-Y', strtotime($select_loan[0]->maturity_date)) : ''.'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Loan to Value:</th>';
			
			$html .= '<td  style="width:40%;">'.number_format(((($select_loan[0]->loan_amount + $current_ecum)/$select_property[0]->underwriting_value)*100),2).'%'.'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th  style="width:60%;">Lender Payment:</th>';
			$html .= '<td  style="width:40%;">$'.number_format($assignment[0]->total_payment).'</td>';
			$html .= '</tr>';
			
			$loan_type_optionadmin = $this->config->item('loan_type_optionadmin');
			$html .= '<tr>';
			$html .= '<th  style="width:60%;">Loan Type:</th>';
			//$html .= '<td style="width:40%;">'.$loan_type[$select_loan[0]->loan_type].'</td>';
			$html .= '<td style="width:40%;">'.$loan_type_optionadmin[$select_loan[0]->loan_type].'</td>';
			$html .= '</tr>';
			$html .= '</table>';
			$html .= '</div>';
			
		
			$html .= '<table><tr><td></td></tr></table>';
			$html .= '<br style="page-break-before:always">';
		
		
			$html .= '<div style="width:100%;">';
			$html .= '<table><tr><td><p>&nbsp;<br></p></td></tr></table>';
			$html .= '<table style="font-size:12pt;width:100%;color:white;background:#003468;font-family:Calibri, sans-serif;">';
			$html .= '<tr>';
			$html .= '<td style="font-size:12pt;">PROPERTY ADDRESS</td>';
			$html .= '</tr>';
			$html .= '</table>';
			$html .= '<style>
						#property_table tr{
							
							height:30px;
						}
			
			</style>';
			
			$html .= '<table class="table"  id="property_table" style="width:80%;vertical-align:top;font-size:12pt;padding-top:5px;">';
			
			$property_address = $select_property[0]->property_address .''.$select_property[0]->unit.'; '.$select_property[0]->city.', '.$select_property[0]->state.' '.$select_property[0]->zip;
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;vertical-align:top;">Property Address:</th>';
			$html .= '<td style="width:40%;">'.$property_address.'</td>';
			$html .= '</tr>';
	
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Property Type:</th>';
			$html .= '<td style="width:40%;">'.$property_type[$select_property[0]->property_type].'</td>';
			$html .= '</tr>';
			
		
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Square Feet:</th>';
			$square=$select_property[0]->square_feet ? number_format($select_property[0]->square_feet): '';
			$html .= '<td style="width:40%;">'.$square.'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Property Value:</th>';
	
			$html .= '<td style="width:40%;">'.'$'.number_format($select_property[0]->underwriting_value).'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Condition:</th>';
			$html .= '<td style="width:40%;">'.$condition[$select_property[0]->conditions].'</td>';
			$html .= '</tr>';

			
			$html .= '<tr>';
			$html .= '<th style="width:60%;"># of Units:</th>';
			$html .= '<td style="width:40%;">'.$select_property[0]->of_unit.'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;"># of Beds:</th>';
			
			
			$html .= '<td style="width:40%;">'.$select_property[0]->bedrooms.'</td>';
			$html .= '</tr>';
		
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Valuation Type:</th>';
			$html .= '<td style="width:40%;">'.$valuation_type[$select_property[0]->valuation_type].'</td>';
			$html .= '</tr>';
			
		   
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Net Cashflow:</th>';
		    $html .= '<td style="width:40%;"></td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<th style="width:60%;"># of Baths:</th>';
			$html .= '<td style="width:40%;">'.$select_property[0]->bathrooms.'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">$ per SF:</th>';
			$html .= '<td style="width:40%;"></td>';
	
			
			$html .= '</table>';
			$html .= '</div>';	
	
	
			
			$html .= '<table><tr><td></td></tr></table>';
			$html .= '<br style="page-break-before:always">';
			
		
			$html .= '<div style="width:100%;">';
			$html .= '<table><tr><td><p>&nbsp;<br></p></td></tr></table>';
			$html .= '<table style="font-size:12pt;width:100%;color:white;background:#003468;font-family:Calibri, sans-serif;">';
			$html .= '<tr>';
			$html .= '<td style="font-size:12pt;">BORROWER INFORMATION</td>';
			$html .= '</tr>';
			$html .= '</table>';
			$html .= '<style>
						#borrower_table tr{
							
							height:30px;
						}
			
			</style>';
			
			$html .= '<table class="table"  id="borrower_table" style="width:80%;vertical-align:top;font-size:12pt;padding-top:5px;">';
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Borrower Name:</th>';
			
			
			
			// $stringExp = explode(' ', $borrower_data[0]->b_name);
			// $shortCode = '';
			// foreach($stringExp as $row){
				
				// $shortCode .= substr($row, 0, 1);
				
				
				// $strlength = strlen($row) - 1;
			
				// for($i = 1; $i <= $strlength; $i++){

					// $shortCode .= '*';

				// }
			// }
			
			$html .= '<td style="width:40%;">'.$borrower_data[0]->b_name.'</td>';
			
			$html .= '</tr>';
			
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Contact Name:</th>';
	
			// $contactExp = explode(' ', $contact_fetails[0]['contact_name']);
			// $contactCode ='';
			// foreach($contactExp as $row){
				
				// $contactCode .= substr($row, 0, 1);
				
				// $strlength = strlen($row) - 1;
			
				// for($i = 1; $i <= $strlength; $i++){

					// $contactCode .='*';

				// }
			// }
			
			$html .= '<td style="width:40%;">'.$contact_fetails[0]['contact_name'].'</td>';
			$html .= '</tr>';
			
	
			$html .= '<tr>';
			$html .= '<th style="width:60%;vertical-align:top;">About Contact:</th>';
			$html .= '<td style="width:40%;">'.$contact_fetails[0]['contact_about'].'</td>';
			$html .= '</tr>';
			
		
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Borrower History:</th>';
			
			$html .= '<td style="width:40%;"></td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Borrower Type:</th>';
	
			$html .= '<td style="width:40%;">'.$borrower_type_option[$borrower_data[0]->borrower_type].'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Credit Score:</th>';
			$html .= '<td style="width:40%;">'.$contact_fetails[0]['credit_score'].'</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<th style="width:60%;"># of Current Loans: </th>';
			$html .= '<td style="width:40%;">'.$fetch_loans[0]->total_count.'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;"># of Paid Off Loans: </th>';
			$html .= '<td style="width:40%;">'.$fetch_loans1[0]->total_count.'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">$ of Current Loans:</th>';
			
			
			$html .= '<td style="width:40%;">'.'$'.number_format($fetch_loans[0]->total_amount).'</td>';
			$html .= '</tr>';
		
			$html .= '<tr>';
			$html .= '<th style="width:60%;">$ of Paid Off Loans:</th>';
			$html .= '<td style="width:40%;">'.'$'.number_format($fetch_loans1[0]->total_amount).'</td>';
			$html .= '</tr>';
		    $html .= '</table>';
			$html .= '</div>';	
		
			
			$html .= '<table><tr><td></td></tr></table>';
			$html .= '<br style="page-break-before:always">';
				
			
	
			$html .= '<div style="width:100%;">';
			$html .= '<table><tr><td><p>&nbsp;<br></p></td></tr></table>';
			$html .= '<table style="font-size:12pt;width:100%;color:white;background:#003468;font-family:Calibri, sans-serif;">';
			$html .= '<tr>';
			$html .= '<td style="font-size:12pt;">OTHER ENCUMBRANCES</td>';
			$html .= '</tr>';
			$html .= '</table>';
			$html .= '<style>
						#Encumbrances_table tr{
							
							height:30px;
						}
			
			</style>';
			
			$html .= '<table class="table"  id="Encumbrances_table" style="width:80%;vertical-align:top;font-size:12pt;padding-top:5px;">';
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Original Balance:</th>';
			
			$html .= '<td style="width:40%;">'.'$'.number_format($select_ecumbrance[0]->original_balance).'</td>';
			$html .= '</tr>';
		
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Rate:</th>';
			$html .= '<td style="width:40%;">'.number_format($select_ecumbrance[0]->intrest_rate,2).'%'.'</td>';
			$html .= '</tr>';
			
	
			$html .= '<tr>';
			
			$html .= '<th style="width:60%;">Maturity Date:</th>';
			$maturity_date = $select_ecumbrance[0]->maturity_date ? date('m-d-Y', strtotime($select_ecumbrance[0]->maturity_date)) : '';
			
			$html .= '<td style="width:40%;">'.$maturity_date.'</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<th style="width:60%;">LTV:</th>';
			
			
						
			$html .= '<td style="width:40%;">'.number_format(((($select_loan[0]->loan_amount + $current_ecum)/$select_property[0]->underwriting_value)*100),2).'%'.'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Current Balance:</th>';
			
			$html .= '<td style="width:40%;">'.'$'.number_format($select_ecumbrance[0]->current_balance).'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Monthly Payment:</th>';
			
			$html .= '<td style="width:40%;">'.'$'.number_format($select_ecumbrance[0]->monthly_payment,2).'</td>';
			$html .= '</tr>';
			
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Is this an Existing Loan?:</th>';
	
			$html .= '<td style="width:40%;">'.$yes_no_option3[$select_ecumbrance[0]->existing_lien].'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">What is the current Position:</th>';
			$html .= '<td style="width:40%;">'.$position_option[$select_ecumbrance[0]->current_position].'</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Will the loan be paid off at close?</th>';
			$html .= '<td style="width:40%;">'.$yes_no_option3[$select_ecumbrance[0]->will_it_remain].'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Has the loan been delinquent greater than 60 days in the last 12 months?</th>';
			$html .= '<td style="width:40%;">'.$yes_no_option3[$select_ecumbrance_data[0]->payment_lates].'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th  style="width:60%;">How many times?</th>';
			$html .= '<td style="width:40%;">'.$yes_how_many_times[$select_ecumbrance_data[0]->yes_how_many].'</td>';
			$html .= '</tr>';
		
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Will any payments remain outstanding?</th>';
			$html .= '<td style="width:40%;">'.$yes_no_option4[$select_ecumbrance_data[0]->payment_unpaid].'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Will the loan proceeds repay the delinquent payments?</th>';
			$html .= '<td style="width:40%;">'.$yes_no_option4[$select_ecumbrance_data[0]->yes_proceeds_subject_loan].'</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<th style="width:60%;">If not, what is the source of payments to bring the loan current?</th>';
			$html .= '<td style="width:40%;">'.$select_ecumbrance_data[0]->no_source_of_fund.'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">What will be the position of the loan after close? </th>';
			$html .= '<td style="width:40%;">'.$position_option[$select_ecumbrance[0]->proposed_priority].'</td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Will the loan have priority over the proposed loan?</th>';
			$html .= '<td style="width:40%;">'.$yes_no_option3[$select_ecumbrance[0]->existing_priority_to_talimar].'</td>';
			$html .= '</tr>';
	
		    $html .= '</table>';
			$html .= '</div>';	
		
	
			$html .= '<table><tr><td></td></tr></table>';
			$html .= '<br style="page-break-before:always">';
		
			$html .= '<div style="width:100%;">';
			$html .= '<table><tr><td><p>&nbsp;<br></p></td></tr></table>';
			$html .= '<table style="font-size:12pt;width:100%;color:white;background:#003468;font-family:Calibri, sans-serif;">';
			$html .= '<tr>';
			$html .= '<td style="font-size:12pt;">CASHFLOW STATEMENT:</td>';
			$html .= '</tr>';
			$html .= '</table>';
			$html .= '<style>
						#cashflow_table tr{
							
							height:30px;
						}
			
			</style>';
			
			$html .= '<table class="table" style="width:100%;vertical-align:top;font-size:12pt;padding-top:5px;">';
			
			$html .='<div class="row">
			
					<div class="col-md-6">
				
						<h4>Income:</h4>
						
						<table class="table table-bordered">
							<thead>
								<tr>
									<th style="margin-left:15px;">Income (Monthly)</th>
									<th>Amount</th>
									
								</tr>
							</thead>
							<tbody>';
						
							$total_amount_income = 0;
							foreach($monthly_income as $row){

								$total_amount_income += $row->amount;
						
								$html .='<tr>
									<td style="margin-left:15px;">'.$row->items.'</td>
									<td>$'. number_format($row->amount).'</td>
									
								</tr>';
						 } 
						$html .='</tbody>
							<tfoot>
								<tr>
									<th style="margin-left:15px;">Total Income</th>
									<th>$'.number_format($total_amount_income).'</th>
									
								</tr>
							</tfoot>
						</table>
					</div>
					<div class="col-md-6">
				
						<h4>Expenses:</h4>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th style="margin-left:15px;">Expense (Monthly)</th>
									<th>Amount</th>
									
								</tr>
							</thead>
							<tbody>';
							
							$total_amount_expense = 0;
							foreach($monthly_expense as $roww){
								$total_amount_expense += $roww->amount;
							
							
								$html .='<tr>
									<td style="margin-left:15px;">'.$roww->items.'</td>
									<td>$'. number_format($roww->amount).'</td>
									
								</tr>';
							 } 
							$html .='</tbody>
							<tfoot>
								<tr>
									<th style="margin-left:15px;">Total Expense</th>
									<th>$'.number_format($total_amount_expense).'</th>
									
								</tr>
								
							</tfoot>
							
						</table>
						
						<h5 style="margin-left:15px;">Net Operating Income:$'.number_format($total_amount_income - $total_amount_expense).'</h5>
						
					</div>
			
			</div>';
			
		
		    $html .= '</table>';
			$html .= '</div>';	
			
	
			$html .= '<table><tr><td></td></tr></table>';
			$html .= '<br style="page-break-before:always">';
		
		
			$html .= '<div style="width:100%;">';
			$html .= '<table><tr><td><p>&nbsp;<br></p></td></tr></table>';
			$html .= '<table style="font-size:12pt;width:100%;color:white;background:#003468;font-family:Calibri, sans-serif;">';
			$html .= '<tr>';
			$html .= '<td style="font-size:12pt;">SERVICING INFORMATION / OTHER DISCLOSURES:</td>';
			$html .= '</tr>';
			$html .= '</table>';
			$html .= '<style>
						#servicing_table tr{
							
							height:30px;
						}
			
			</style>';
			
			$html .= '<table class="table"  id="servicing_table" style="width:80%;vertical-align:top;font-size:12pt;padding-top:5px;">';
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Loan Servicer:</th>';
			if($select_loan_servicing[0]->servicing_agent == '14'){
			  $servicer_name = 'FCI Lender Services';
			}elseif($select_loan_servicing[0]->servicing_agent == '15'){
				$servicer_name = 'TaliMar Financial Inc.';
			}elseif($select_loan_servicing[0]->servicing_agent == '16'){
				$servicer_name = 'La Mesa Fund Control';
			}elseif($select_loan_servicing[0]->servicing_agent == '19'){
				$servicer_name = 'Del Toro Loan Servicing';
			}else{
				$servicer_name = '';
			}
			
			
			$html .= '<td style="width:40%;">'.$servicer_name.'</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Actual Note Rate:</th>';
			$html .= '<td style="width:40%;">'.number_format($select_loan[0]->intrest_rate,2).'%</td>';
			$html .= '</tr>';
			
			$servicing_fee = $select_loan_servicing[0]->servicing_fee ? number_format($select_loan_servicing[0]->servicing_fee,2) : '1.00';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Servicing Fee: </th>';
			$html .= '<td style="width:40%;">'.$servicing_fee.'%</td>';
			$html .= '</tr>';
			
			$lender_note_rate = $select_loan[0]->intrest_rate - $servicing_fee;
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Lender Note Rate:</th>';
			$html .= '<td style="width:40%;">'.number_format($lender_note_rate,2).'%</td>';
			$html .= '</tr>';
	
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Fee Disbursements:</th>';
			$html .= '<td style="width:40%;"></td>';
			$html .= '</tr>';
			
		
			$html .= '<tr>';
			$html .= '<th style="margin-left:15px; width:60%;">Late Fee:</th>';
			
			$html .= '<td style="width:40%;"></td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="margin-left:15px; width:60%;">Extension Fee:</th>';
	
			$html .= '<td style="width:40%;"></td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="margin-left:15px; width:60%;">Default Rate:</th>';
			$html .= '<td style="width:40%;"></td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Link] of payment: </th>';
			$html .= '<td style="width:40%;"></td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">[Link]% of Loan Amount:</th>';
			
			
			$html .= '<td style="width:40%;"></td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">[Link]:</th>';
			
			
			$html .= '<td style="width:40%;"></td>';
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<th style="width:60%;">Lender Receives:</th>';
			$html .= '<td style="width:40%;"></td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<th style="width:60%;">	Lender Receives:</th>';
			$html .= '<td style="width:40%;"></td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<th style="width:60%;">	Lender Receives:</th>';
			$html .= '<td style="width:40%;"></td>';
			$html .= '</tr>';
			
		    $html .= '</table>';
			$html .= '</div>';	
	
		
			$html .= '<table><tr><td></td></tr></table>';
			$html .= '<br style="page-break-before:always">';
	
		
			$html .= '<div style="width:100%;">';
			$html .= '<table><tr><td><p>&nbsp;<br></p></td></tr></table>';
			$html .= '<table style="font-size:12pt;width:100%;color:white;background:#003468;font-family:Calibri, sans-serif;">';
			$html .= '<tr>';
			$html .= '<td style="font-size:12pt;">PROPERTY IMAGES:</td>';
			$html .= '</tr>';
			$html .= '</table>';
			$html .= '<style>
						#proper_table tr{
							
							height:30px;
						}
			
			</style>';
			
			$html .= '<table class="table" id="proper_table" style="width:100%;vertical-align:top;font-size:12pt;padding-top:5px;">';
			
			
				foreach($property_img as $row){
					if($row->image_name != 'blank.jpg'){
				
						$img_name = 'https://database.talimarfinancial.com/all_property_images/'.$row->talimar_loan.'/'.$row->property_home_id.'/'.$row->folder_id.'/'.$row->image_name;
					
				
						 // $html .='<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
							// <div class="img-preview img-preview-custom">
								// <img src="'.$img_name.'" width="220" height="150">
							// </div>
							// <h4 style="text-align:left;">'.$row->description.'</h4>
						// </div>'; 
						
						$html .='<tr>';
								$html .='<td style="width:50%;">
											<img src="'.$img_name.'" width="220" height="150"><br>
											<h4 style="text-align:left;">'.$row->description.'</h4>
										</td>';
						$html .='</tr>';		
				
					} 
				}  
			
			
		    $html .= '</table>';
			$html .= '</div>';	
	

			echo $html;
			
			
	}
	
	
	
	
	

	
}

?>