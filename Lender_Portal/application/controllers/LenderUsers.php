<?php
//Defined('BASEPATH') OR exit('No direct script access allowed');
Class LenderUsers extends CI_Controller{
	
	public function __construct(){
		parent:: __construct();
		$this->load->helper('url');
		$this->load->model('User_model');
		$this->load->library('email');
		$this->load->library('Aws3','aws3');
		$this->load->helper('mailhtml_helper.php');
		$this->load->library("pagination");
	}
	public function lender_users_list(){
		$user_id       			= $this->session->userdata('p_user_id');
		$fetch_contact 			= $this->User_model->query("SELECT id,name,account_status FROM investor where account_status='1' and t_user_id='$user_id' order by id desc ");
		$fetch_contact 			= $fetch_contact->result();
		$data['lender_Data'] 	= $fetch_contact;
		$data['page'] 			= 'Lender List';
		$data['content'] 		= $this->load->view('list_lender_users',$data,true);
		$this->load->view('template_files/template',$data);
	}
	public function view_form(){
		$investor_id 				=$this->uri->segment(3);
		$data['investor_idd'] 		= $investor_id;
		$data['contact_idd'] 		= $this->input->post('search_contact_id') ? $this->input->post('search_contact_id') : $this->input->post('contact_idd');
		$data['search_contact_id'] 	= $this->input->post('search_contact_id') ? $this->input->post('search_contact_id') : $this->input->post('contact_idd');
		$fetch_contact 				= $this->User_model->query("SELECT * FROM contact ORDER by contact_firstname");
		$fetch_contact 				= $fetch_contact->result();
		$data['fetch_all_contact'] 	= $fetch_contact;
		if($investor_id)
		{
			$fetch_id_data['id'] 	= $investor_id;
			$fetch_investor_data 	= $this->User_model->select_where('investor',$fetch_id_data);
			$fetch_investor_data 	= $fetch_investor_data->result();
			$data['fetch_investor_data']['id']  				 = $fetch_investor_data[0]->id;
			$data['fetch_investor_data']['fci_acct_service']  	 = $fetch_investor_data[0]->fci_acct_service;
			$data['fetch_investor_data']['fci_acct']  		     = $fetch_investor_data[0]->fci_acct;
			$data['fetch_investor_data']['investor_type']  		 = $fetch_investor_data[0]->investor_type;
			$data['fetch_investor_data']['ldate_created']  		 = $fetch_investor_data[0]->ldate_created;
			$data['fetch_investor_data']['account_status']  	 = $fetch_investor_data[0]->account_status;
			$data['fetch_investor_data']['entity_name']  		 = $fetch_investor_data[0]->entity_name;
			$data['fetch_investor_data']['entity_res_state']  	 = $fetch_investor_data[0]->entity_res_state;
			$data['fetch_investor_data']['state_entity_no']  	 = $fetch_investor_data[0]->state_entity_no;
			$data['fetch_investor_data']['date_of_trust']        = $fetch_investor_data[0]->date_of_trust;
			$data['fetch_investor_data']['name']  				 = $fetch_investor_data[0]->name;
			$data['fetch_investor_data']['address']  			 = $fetch_investor_data[0]->address;
			$data['fetch_investor_data']['unit']  				 = $fetch_investor_data[0]->unit;
			$data['fetch_investor_data']['custodian']  			 = $fetch_investor_data[0]->custodian;
			$data['fetch_investor_data']['directed_IRA_NAME']  	 = $fetch_investor_data[0]->directed_IRA_NAME;
			$data['fetch_investor_data']['self_dir_account']     = $fetch_investor_data[0]->self_dir_account;
			$data['fetch_investor_data']['tax_id']  			 = $fetch_investor_data[0]->tax_id;
			$data['fetch_investor_data']['city']  				 = $fetch_investor_data[0]->city;
			$data['fetch_investor_data']['state']  				 = $fetch_investor_data[0]->state;
			$data['fetch_investor_data']['zip']  				 = $fetch_investor_data[0]->zip;
			$data['fetch_investor_data']['vesting']  			 = $fetch_investor_data[0]->vesting;
			$data['fetch_investor_data']['bank_name']  			 = $fetch_investor_data[0]->bank_name;
			$data['fetch_investor_data']['account_name']  	 	 = $fetch_investor_data[0]->account_name;
			$data['fetch_investor_data']['account_number']  	 = $fetch_investor_data[0]->account_number;
			$data['fetch_investor_data']['routing_number']  	 = $fetch_investor_data[0]->routing_number;
			$data['fetch_investor_data']['ach_disbrusement']     = $fetch_investor_data[0]->ach_disbrusement;
			$data['fetch_investor_data']['check_disbrusement']   = $fetch_investor_data[0]->check_disbrusement;
			$where_1['lender_id'] 	= $investor_id;
			$fetch_lender_contact 	= $this->User_model->select_where_desc('lender_contact',$where_1,'contact_primary');
			if($fetch_lender_contact->num_rows() > 0){
				$fetch_lender_contact 	= $fetch_lender_contact->result();
				$data['fetch_lender_contact'] = $fetch_lender_contact;
			}
		}
		$data['page'] 	 = 'Lender form';
		$data['content'] = $this->load->view('view_lender_form',$data,true);
		$this->load->view('template_files/template',$data);
	}
	public function add_form()
	{
		$data['user_role'] 					= $this->session->userdata('user_role');
		$globalUserId						= $this->session->userdata('p_user_id');
		$fetch_logged_contact 				= $this->User_model->query("SELECT contact.* FROM lender_contact_type join contact on lender_contact_type.contact_id=contact.contact_id where lender_contact_type.contact_id='$globalUserId' ");
		$Logged_contact_array 				= $fetch_logged_contact->result();
		$data['logged_contct_id'] 			= $Logged_contact_array[0]->contact_id;
		$globalContactId					=$data['logged_contct_id'];
		$data['logged_contact_firstname'] 	= $Logged_contact_array[0]->contact_firstname;
		$data['logged_contact_middlename'] 	= $Logged_contact_array[0]->contact_middlename;
		$data['logged_contact_lastname'] 	= $Logged_contact_array[0]->contact_lastname;
		$data['logged_contct_email'] 		= $Logged_contact_array[0]->contact_email;
		$data['logged_contct_phone'] 		= $Logged_contact_array[0]->contact_phone;
		$fetch_all_contact 					= $this->User_model->query("SELECT contact.* from contact_relationship join contact on  contact_relationship.relationship_contact_id=contact.contact_id  where contact_relationship.show_lender_portal='1' AND contact_relationship.contact_id='$globalContactId' AND contact_relationship.relationship_contact_id!='$globalContactId' ");
		$data['fetch_all_contact'] 			= $fetch_all_contact->result();
		$data['page'] 						= 'Lender form';
		$data['content'] 					= $this->load->view('add_lender_form',$data,true);
		$this->load->view('template_files/template',$data);
	}
	function add_investor_data()
	{
		error_reporting(0);
		$investor_id 						= $this->input->post('investor_id');
		$data['fci_acct_service']    		= $this->input->post('fci_acct_service');
		$data['fci_acct']    				= $this->input->post('fci_acct');
		$data['name']    					= $this->input->post('investor_name');
		$data['investor_type']    			= $this->input->post('investor_type');
		$data['tax_id']    					= $this->input->post('tax_id');
		$data['entity_res_state']    		= $this->input->post('entity_res_state');
		$data['state_entity_no']    		= $this->input->post('state_entity_no');
		$data['self_dir_account']    		= $this->input->post('self_dir_account');
		$data['directed_IRA_NAME']    		= $this->input->post('directed_IRA_NAME');
		$data['entity_name']    			= $this->input->post('entity_name');
		$data['address']    				= $this->input->post('address');
		$data['unit']    					= $this->input->post('unit');
		$data['city']    					= $this->input->post('city');
		$data['state']    					= $this->input->post('state');
		$data['zip']    					= $this->input->post('zip');
		$data['vesting']    				= $this->input->post('vesting');
		$data['ach_disbrusement']    		= $this->input->post('ach_disbrusement');
		$data['bank_name']    				= $this->input->post('bank_name');
		$data['account_number']    			= $this->input->post('account_number');
		$data['routing_number']    			= $this->input->post('routing_number');
		$data['vesting']    				= $this->input->post('vesting');
		$data['t_user_id']    				= $this->session->userdata('p_user_id');
		$data['created_date']    			= date('m-d-Y');
		$data['account_status']    			= "1";
		$data['new_lender_status']    		= "1";
		$user_id 							= $this->session->userdata('p_user_id');
		$entity_date_trust=$this->input->post('entity_date_trust');
		if(!empty($entity_date_trust)){
			$dateArray=explode("/", $entity_date_trust);
			$entity_date_trust=$dateArray[2]."-".$dateArray[0]."-".$dateArray[1];
			$data['date_of_trust']=date('Y-m-d',strtotime($entity_date_trust));
		}	
		if($investor_id == ''){
			$this->User_model->insertdata('investor',$data);
			$insert_id = $this->db->insert_id();
			if($insert_id)
			{
				$contact_id							= $this->input->post('contact_row_id');
				$contact_email						= $this->input->post('contact_row_email');
				$contact_phone						= $this->input->post('contact_row_phone');
				if(is_array($contact_id)){
					$contact_id_arr = array_values(array_filter($contact_id));
					if($contact_id_arr){
						for($i=0;$i<count($contact_id);$i++)
						{
							$datecontact_re_date = "NULL";
							$contact_id_value=$contact_id[$i];
							$contact_phone_value=$contact_phone[$i];
							$contact_email_value=$contact_email[$i];
							$str_email.=$contact_email;
							$link_lender="2";
							$primary_contact="0";
							$contact_title= $this->input->post('account_title_name');
							$contact_role="";
							$required_sign=1;
							if($i==0){
								$primary_contact=1;
							}
							$sql = "INSERT INTO `lender_contact` ( `lender_id`, `contact_id`,`link_to_lender`,`contact_primary`, `c_title`,`contact_role`, `contact_phone`, `contact_email`, `contact_re_date`, `required_sign`, `t_user_id`) VALUES ( '".$insert_id."', '".$contact_id_value."', '".$link_lender."', '".$primary_contact."',  '".$contact_title."', '".$contact_role."', '".$contact_phone_value."',  '".$contact_email_value."', ".$datecontact_re_date.", '".$required_sign."', '".$t_user_id."' )";
							$this->User_model->query($sql);
						}
					}
				}
				$Message = '<p>Thank you for registering a new account with TaliMar Financial account. Please allow 2 to 3 days for a Lender Account Representative to review and approve your account information. Once approved, you may begin investing with this account. </p> 
				<p>Should you have any questions, please contact Investor Relations at (858) 242-4900. </p>';
				$dataMailA['from_email']  	= 'bvandenberg@talimarfinancial.com';
		        $dataMailA['from_name']   	= 'TaliMar Financial';
		        $dataMailA['cc']      		= '';
		        $dataMailA['bcc']      		= '';
		        $dataMailA['name']      	= '';
		        $dataMailA['subject']     	= 'New Lender Account';
		        $dataMailA['content_body']  = $Message;
		        $dataMailA['attach_file']   = '';
		        for($i=0;$i<count($contact_email);$i++)
				{
					$dataMailA['to']= $contact_email[$i];
		        	send_mail_template($dataMailA);
		        }
		        /*Send mail to admin*/
		        $Message = '<p>The following account has been registered.  </p> 
							<p>Account Name: '.$this->input->post("investor_name").'</p>
							<p>Contact Name: '.$this->input->post("first_contact_name").'</p>
							<p>Please review and approve the account. </p>';
				$dataMailA['from_email']  	= 'bvandenberg@talimarfinancial.com';
		        $dataMailA['from_name']   	= 'TaliMar Financial';
		        $dataMailA['cc']      		= '';
		        $dataMailA['bcc']      		= '';
		        $dataMailA['name']      	= '';
		        $dataMailA['subject']     	= 'New Lender Account';
		        $dataMailA['content_body']  = $Message;
		        $dataMailA['attach_file']   = '';
				$dataMailA['to']="payal@bitcot.com";
	        	send_mail_template($dataMailA);
				$status = true;
				$this->session->set_flashdata('success', ' Recorded added successfully!');
				redirect(base_url().'add_lender_user','refresh');
			}
			else
			{
				$this->session->set_flashdata('error','Something went wrong');
				redirect(base_url().'add_lender_user','refresh');
			}
		} 
	}
	public function add_new_contact(){
		$row_no 					= $this->input->post('row_no');
		$data['row_no']				= $row_no;
		$globalUserId				= $this->session->userdata('p_user_id');
		$fetch_logged_contact 		= $this->User_model->query("SELECT contact.* FROM lender_contact_type join contact on lender_contact_type.contact_id=contact.contact_id where lender_contact_id='$globalUserId' ");
		$Logged_contact_array 		= $fetch_logged_contact->result();
		$data['logged_contct_id'] 	= $Logged_contact_array[0]->contact_id;
		$globalContactId=$data['logged_contct_id'];
		$fetch_all_contact 			= $this->User_model->query("SELECT contact.* from contact_relationship join contact on  contact_relationship.relationship_contact_id=contact.contact_id  where contact_relationship.show_lender_portal='1' AND contact_relationship.contact_id='$globalContactId'  ");
		$fetch_all_contact 			= $fetch_all_contact->result();
		$data['fetch_all_contact']	= $fetch_all_contact;
		echo $this->load->view('ajax_lender_contact',$data,true);
	}
}

?>