<?php
Defined('BASEPATH') OR exit('No direct script access allowed');

Class Personal_details extends CI_Controller
{
	
	public function __construct(){
		
		parent:: __construct();
		
		$this->load->model('User_model');
		$this->load->library('email');
		$this->load->library('TCPDF');
		$this->load->library('Aws3','aws3');
		

		if($this->session->userdata('p_user_id') != ''){
			
		}
		else
		{
			redirect(base_url());
		}
	}

	
	public function update_notification(){

		$notify2 = $this->input->post('notify2');
		$notify7 = $this->input->post('notify7');
		$notify9 = $this->input->post('notify9');
		$notify13= $this->input->post('notify13');

		$useriD  = $this->session->userdata('p_user_id');
		$notification_setting 	= $this->config->item('notification_setting');

		if($notify2 == '' && $notify7 == '' && $notify9 == '' && $notify13 == ''){

			echo 1;

		}else{

			$createArray = array($notify2,$notify7,$notify9,$notify13);
			foreach ($notification_setting as $key => $value) {

				$fetchcontactTag = $this->User_model->query("SELECT * from contact_tags where contact_id = '".$useriD."' AND contact_email_blast = '".$key."'");
				if($fetchcontactTag->num_rows() > 0){

					if(in_array($key,$createArray))
					{
						
					}
					else
					{					
						$this->User_model->delete('contact_tags',array('contact_id'=>$useriD,'contact_email_blast'=>$key));
					}

				}else{

					if(in_array($key,$createArray))
					{
						$this->User_model->insertdata('contact_tags',array('contact_id'=>$useriD,'contact_email_blast'=>$key));
					}
				}
				
			}

			$notification_arr = $this->input->post('notification_arr');
			$checkedValue = '';
			if($notification_arr){
				$checkedValue = explode(',', $notification_arr);
			}

			$checkHtml = '';
			if($notify2){
				$noti1 = 'No';
				if(in_array(2, $checkedValue)){
					$noti1 = 'Yes';
				}
				$checkHtml .= '<tr><td>E-Mail</td><td>'.$noti1.'</td><td>Yes</td></tr>';
			}else{
				$noti1 = 'No';
				if(in_array(2, $checkedValue)){
					$noti1 = 'Yes';
				}
				$checkHtml .= '<tr><td>E-Mail</td><td>'.$noti1.'</td><td>No</td></tr>';
			}
			if($notify7){
				$noti1 = 'No';
				if(in_array(7, $checkedValue)){
					$noti1 = 'Yes';
				}
				$checkHtml .= '<tr><td>Mail</td><td>'.$noti1.'</td><td>Yes</td></tr>';
			}else{
				$noti1 = 'No';
				if(in_array(7, $checkedValue)){
					$noti1 = 'Yes';
				}
				$checkHtml .= '<tr><td>Mail</td><td>'.$noti1.'</td><td>No</td></tr>';
			}
			if($notify9){
				$noti1 = 'No';
				if(in_array(9, $checkedValue)){
					$noti1 = 'Yes';
				}
				$checkHtml .= '<tr><td>Text</td><td>'.$noti1.'</td><td>Yes</td></tr>';
			}else{
				$noti1 = 'No';
				if(in_array(9, $checkedValue)){
					$noti1 = 'Yes';
				}
				$checkHtml .= '<tr><td>Text</td><td>'.$noti1.'</td><td>No</td></tr>';
			}

			if($notify13){
				$noti1 = 'No';
				if(in_array(13, $checkedValue)){
					$noti1 = 'Yes';
				}
				$checkHtml .= '<tr><td>Text</td><td>'.$noti1.'</td><td>Yes</td></tr>';
			}else{
				$noti1 = 'No';
				if(in_array(13, $checkedValue)){
					$noti1 = 'Yes';
				}
				$checkHtml .= '<tr><td>Text</td><td>'.$noti1.'</td><td>No</td></tr>';
			}

			$contact_id = $this->session->userdata('p_user_id');
			$personal_data = $this->User_model->query("SELECT * FROM contact WHERE contact_id = '$contact_id'");
			$personal_data = $personal_data->row();
			$Fullname = $personal_data->contact_firstname.' '.$personal_data->contact_lastname;
			$messageBody  = '';
			$messageBody .= '<p>Some fields update by this user <b>'.$Fullname.'</b></p>';
			$messageBody .= '<p>Notification Settings updated!</p>';

			$messageBody .= '<table border="1" width="100%">';
			$messageBody .= '<tr><th>Field</th><th>Old Value</th><th>New Value</th></tr>';
			$messageBody .= $checkHtml;
			$messageBody .= '</table>';


			$maildata = array();
			$maildata['from_email'] = 'mail@talimarfinancial.com';
			$maildata['from_name'] = "Talimar Financial";
			$maildata['to'] = 'payal@bitcot.com';//'invest@talimarfinancial.com';  
			$maildata['name'] = 'Brock VandenBerg';
			$maildata['subject'] = $Fullname." - User update some fields!";
			$maildata['content_body'] = $messageBody;
			$maildata['button_url'] = '';
			$maildata['button_text'] = '';
			send_mail_template($maildata);

			echo 2;
			
		}
	}


	public function Updateusernameinfo(){

		$contact_id = $this->session->userdata('p_user_id');
		$username = $this->input->post('fieldval');

		$data1['username'] 		= $username;
		$where1['contact_id'] 	= $contact_id;
		$this->User_model->updatedata('lender_contact_type',$where1,$data1);
		echo '1';
	}


	public function UpdatePassword(){

		$oldpassword = $this->input->post('old_password');
		$newpassword = $this->input->post('new_password');
		$repassword = $this->input->post('re_password');
		$contact_id = $this->session->userdata('p_user_id');

		if($oldpassword && $newpassword && $repassword != ''){

			$where['password'] = $oldpassword;
			$where['contact_id'] = $contact_id;

			$checkPassword = $this->User_model->select_where('lender_contact_type',$where);
			if($checkPassword->num_rows() > 0){

				if($newpassword == $repassword){

					$data1['password'] = $newpassword;
					$data1['c_password'] = $newpassword;
					$where1['contact_id'] = $contact_id;
					$UpdatePassword = $this->User_model->updatedata('lender_contact_type',$where1,$data1);
					$this->session->set_flashdata('success','Password updated.');

					$this->session->unset_userdata('p_user_id');		
					$this->session->unset_userdata('p_user_fname');			
		
					//$this->session->sess_destroy(); // logout
					redirect(base_url());

				}else{
					$this->session->set_flashdata('error','New password and confirm password not match.');
					redirect(base_url().'view_details');
				}
			}else{
				$this->session->set_flashdata('error','Old password not match with your account.');
				redirect(base_url().'view_details');
			}
		}else{

			$this->session->set_flashdata('error','All fields required.');
			redirect(base_url().'view_details');
		}

	}


	public function view_details(){
		
		if($this->uri->segment(1)=='view_details'){
			if(!$this->uri->segment(2)){
				redirect(base_url().'view_details/1','refresh');		
			}
		}

		$personal_data = $this->User_model->query("Select * from contact where contact_id = '".$this->session->userdata('p_user_id')."'");
		$personal_data = $personal_data->result();
		$data['personal_view_details']	= $personal_data;


		$contact_tags = $this->User_model->query("Select * from contact_tags where contact_id = '".$this->session->userdata('p_user_id')."'");
		$contact_tags = $contact_tags->result();
		$data['contact_tags_details']	= $contact_tags;


		$contcat_id = $this->session->userdata('p_user_id');
		$contact_type = $this->User_model->query("SELECT username,password,ownership_type,ltv,position,lend_value,LoanTerm,loan,property_type,maximum_income,invest_per_deed,funded,looking_invest_per_deed,contact_id FROM lender_contact_type WHERE contact_id = '".$contcat_id."'");
		$data['lender_contact'] = $contact_type->row();


		$contact_id = $this->session->userdata('p_user_id');
		$fetchimg = $this->User_model->query("SELECT imgurl FROM `lender_contact_type` WHERE `contact_id`= '".$contact_id."' ");
		$fetchimg = $fetchimg->result();
		$imgurl = $fetchimg[0]->imgurl;
		$data['profile_image_remove'] = 'No';
		if($imgurl != ''){
			$data['profile_image_remove'] = 'Yes';
		    $Imgpath = $imgurl;
		}else{
		    $Imgpath = base_url().'assets/img/post/avatar0.jpg';
		}
		$data['profile_image'] = $Imgpath;


		$loginuser_detail = $this->fetch_login_user();
		$data['loginuser_detail'] = $loginuser_detail;
		$data['page'] = 'settings';
		$data['content'] = $this->load->view('Personal_detail/view_info',$data,true);
		$this->load->view('template_files/template',$data);

	
	}
	
	
	public function personal_setting(){
		if($this->uri->segment(1)=='personal_setting'){

		redirect(base_url().'view_details','refresh');
		
		}
		$personal_data = $this->User_model->query("Select * from contact where contact_id = '".$this->session->userdata('p_user_id')."'");
		$personal_data = $personal_data->result();
		$data['personal_data_details']	= $personal_data;


		$loginuser_detail = $this->fetch_login_user();
		$data['loginuser_detail'] = $loginuser_detail;
		$data['page'] = 'settings';
		$data['content'] = $this->load->view('Personal_detail/personal_info',$data,true);
		$this->load->view('template_files/template',$data);

	
	}

	public function fetch_login_user(){

		$contcat_id = $this->session->userdata('p_user_id');
		$fetch_user = $this->User_model->query("SELECT username,password FROM lender_contact_type WHERE contact_id = '".$contcat_id."'");
		$fetch_user = $fetch_user->result();

		return $fetch_user;
	}
	
	//.........edit personal setting start ..............//
	
	public function edit_profile($idd){

		
		if(isset($_POST['submit'])){

			
			$data['contact_firstname'] 	        = $this->input->post('fname');
			$data['contact_lastname'] 			= $this->input->post('lname');
	        $data['contact_email'] 				= $this->input->post('email');
			$data['contact_phone'] 			    = $this->input->post('p_contact');
			$data['alter_phone'] 				= $this->input->post('secondary_contact');
			$data['primary_option'] 			= $this->input->post('primary_type');
			$data['alter_option'] 				= $this->input->post('alter_type');
			$data['contact_fax'] 				= $this->input->post('fax');
			$data['contact_unit'] 				= $this->input->post('unit');
			$data['contact_city'] 				= $this->input->post('City');
			$data['contact_state'] 				= $this->input->post('State');
			$data['contact_zip'] 				= $this->input->post('Zip');
			$data['contact_personal_notes'] 	= $this->input->post('Contact');
			$data['contact_mailing_address'] 	= $this->input->post('Mailing_add');
			$data['contact_mailing_unit'] 		= $this->input->post('Mailing_unit');
			$data['contact_mailing_city']		= $this->input->post('Mailing_city');
			$data['contact_mailing_state']		= $this->input->post('Mailing_state');
			$data['contact_mailing_zip']		= $this->input->post('Mailing_zip');
			$data['contact_address']			= $this->input->post('street');
			$data['same_mailing_addresss']		= $this->input->post('same_mailing_addresss');
			$data['pemail_type']				= $this->input->post('email_type');
			$data['contact_suffix']				= $this->input->post('legal_lirst');
   
			$this->User_model->updatedata('contact',array('contact_id'=>$idd),$data);

			if($this->input->post('username') || $this->input->post('password')){

				$updata['username'] = $this->input->post('username');
				$updata['password'] = $this->input->post('password');
				$updata['c_password'] = $this->input->post('password');


				$this->User_model->updatedata('lender_contact_type',array('contact_id'=>$idd),$updata);

			}

			$this->session->set_flashdata('success','Account Updated Successfully!');

				$message	=''; 
		         	//$email 		=  $contact_email_onee;
		         	
		            $email 			='invest@talimarfinancial.com';
		            
		         	

		    	  
					$subject	= "Personal Setting"; 
					$header		= "From: mail@database.talimarfinancial.com"; 
					$message .='<p>'.$this->session->userdata('p_user_fname').' '.$this->session->userdata('p_user_lname').' has modified the settings on their account.</p>';				


					$maildata = array();
					$maildata['from_email'] = 'mail@talimarfinancial.com';
					$maildata['from_name'] = $subject;
					$maildata['to'] = $email;
					$maildata['name'] = $this->session->userdata('p_user_fname');
					$maildata['subject'] = $subject;
					$maildata['content_body'] = $message;
					$maildata['button_url'] = '';
					$maildata['button_text'] = '';
					send_mail_template($maildata);
			
			redirect(base_url().'personal_setting');
		
		
		}
	}	

	public function Updatelenderprofile(){

		$old_contact_suffix             = $this->input->post('old_contact_suffix');
		$old_contact_first_name 		= $this->input->post('old_contact_first_name');
		$old_contact_middle_initial 	= $this->input->post('old_contact_middle_initial');
		$old_contact_last_name 			= $this->input->post('old_contact_last_name');
		$old_contact_nickname 			= $this->input->post('old_contact_nickname');
		$old_contact_email_type 		= $this->input->post('old_contact_email_type');
		$old_contact_primary_phone 		= $this->input->post('old_contact_primary_phone');
		$old_contact_phone_type 		= $this->input->post('old_contact_phone_type');
		$old_primary_street_address 	= $this->input->post('old_primary_street_address');
		$old_primary_unit 				= $this->input->post('old_primary_unit');
		$old_primary_city 				= $this->input->post('old_primary_city');
		$old_primary_state 				= $this->input->post('old_primary_state');
		$old_primary_zip 				= $this->input->post('old_primary_zip');
		$old_mailing_street_address 	= $this->input->post('old_mailing_street_address');
		$old_mailing_unit 				= $this->input->post('old_mailing_unit');
		$old_mailing_city 				= $this->input->post('old_mailing_city');
		$old_mailing_state 				= $this->input->post('old_mailing_state');
		$old_mailing_zip 				= $this->input->post('old_mailing_zip');

		
		$contact_suffix             = $this->input->post('contact_suffix');
		$contact_firstname 			= $this->input->post('contact_first_name');
		$contact_middlename 		= $this->input->post('contact_middle_initial');
		$contact_lastname 			= $this->input->post('contact_last_name');
		$contact_nickname 			= $this->input->post('contact_nickname');

		$contact_primary_email 		= $this->input->post('contact_primary_email');

		$pemail_type 				= $this->input->post('contact_email_type');
		$contact_phone 				= $this->input->post('contact_primary_phone');
		$primary_option 			= $this->input->post('contact_phone_type');

		$contact_address 			= $this->input->post('primary_street_address');
		$contact_unit 				= $this->input->post('primary_unit');
		$contact_city 				= $this->input->post('primary_city');
		$contact_state 				= $this->input->post('primary_state');
		$contact_zip 				= $this->input->post('primary_zip');

		$contact_mailing_address 	= $this->input->post('mailing_street_address');
		$contact_mailing_unit 		= $this->input->post('mailing_unit');
		$contact_mailing_city 		= $this->input->post('mailing_city');
		$contact_mailing_state 		= $this->input->post('mailing_state');
		$contact_mailing_zip 		= $this->input->post('mailing_zip');

		$contact_number_option = $this->config->item('contact_number_option');
		$email_types_option = $this->config->item('email_types_option');

		$change_field_body = '';

		if($old_contact_suffix != $contact_suffix){
			$change_field_body .='<tr><td>Legal First Name</td><td>'.$old_contact_suffix.'</td><td>'.$contact_suffix.'</td></tr>';
		}

		if($old_contact_first_name != $contact_firstname){
			$change_field_body .='<tr><td>Legal First Name</td><td>'.$old_contact_first_name.'</td><td>'.$contact_firstname.'</td></tr>';
		}
		if($old_contact_middle_initial != $contact_middlename){
			$change_field_body .='<tr><td>Middle Initial</td><td>'.$old_contact_middle_initial.'</td><td>'.$contact_middlename.'</td></tr>';
		}
		if($old_contact_last_name != $contact_lastname){
			$change_field_body .='<tr><td>Last Name</td><td>'.$old_contact_last_name.'</td><td>'.$contact_lastname.'</td></tr>';
		}
		if($old_contact_nickname != $contact_nickname){
			$change_field_body .='<tr><td>Nickname</td><td>'.$old_contact_nickname.'</td><td>'.$contact_nickname.'</td></tr>';
		}
		if($old_contact_email_type != $pemail_type){
			$change_field_body .='<tr><td>E-Mail Type</td><td>'.$email_types_option[$old_contact_email_type].'</td><td>'.$email_types_option[$pemail_type].'</td></tr>';
		}
		if($old_contact_primary_phone != $contact_phone){
			$change_field_body .='<tr><td>Primary Phone</td><td>'.$old_contact_primary_phone.'</td><td>'.$contact_phone.'</td></tr>';
		}
		if($old_contact_phone_type != $primary_option){
			$change_field_body .='<tr><td>Phone Type</td><td>'.$contact_number_option[$old_contact_phone_type].'</td><td>'.$contact_number_option[$primary_option].'</td></tr>';
		}
		if($old_primary_street_address != $contact_address){
			$change_field_body .='<tr><td>Primary Street Address</td><td>'.$old_primary_street_address.'</td><td>'.$contact_address.'</td></tr>';
		}
		if($old_primary_unit != $contact_unit){
			$change_field_body .='<tr><td>Primary Unit #</td><td>'.$old_primary_unit.'</td><td>'.$contact_unit.'</td></tr>';
		}
		if($old_primary_city != $contact_city){
			$change_field_body .='<tr><td>Primary City</td><td>'.$old_primary_city.'</td><td>'.$contact_city.'</td></tr>';
		}
		if($old_primary_state != $contact_state){
			$change_field_body .='<tr><td>Primary State</td><td>'.$old_primary_state.'</td><td>'.$contact_state.'</td></tr>';
		}
		if($old_primary_zip != $contact_zip){
			$change_field_body .='<tr><td>Primary Zip</td><td>'.$old_primary_zip.'</td><td>'.$contact_zip.'</td></tr>';
		}
		if($old_mailing_street_address != $contact_mailing_address){
			$change_field_body .='<tr><td>Mailing Street Address</td><td>'.$old_mailing_street_address.'</td><td>'.$contact_mailing_address.'</td></tr>';
		}
		if($old_mailing_unit != $contact_mailing_unit){
			$change_field_body .='<tr><td>Mailing Unit #</td><td>'.$old_mailing_unit.'</td><td>'.$contact_mailing_unit.'</td></tr>';
		}
		if($old_mailing_city != $contact_mailing_city){
			$change_field_body .='<tr><td>Mailing City</td><td>'.$old_mailing_city.'</td><td>'.$contact_mailing_city.'</td></tr>';
		}
		if($old_mailing_state != $contact_mailing_state){
			$change_field_body .='<tr><td>Mailing State</td><td>'.$old_mailing_state.'</td><td>'.$contact_mailing_state.'</td></tr>';
		}
		if($old_mailing_zip != $contact_mailing_zip){
			$change_field_body .='<tr><td>Mailing Zip</td><td>'.$old_mailing_zip.'</td><td>'.$contact_mailing_zip.'</td></tr>';
		}

		if($change_field_body){			
			$messageBody  = '';
			$messageBody .= '<p>Some fields update by this user <b>'.$contact_firstname." ".$old_contact_last_name.'</b></p>';

			$messageBody .= '<table border="1" width="100%">';
			$messageBody .= '<tr><th>Field</th><th>Old Value</th><th>New Value</th></tr>';
			$messageBody .= $change_field_body;
			$messageBody .= '</table>';


			$maildata = array();
			$maildata['from_email'] = 'mail@talimarfinancial.com';
			$maildata['from_name'] = "Talimar Financial";
			$maildata['to'] = 'payal@bitcot.com';//'invest@talimarfinancial.com'; 
			
			$maildata['name'] = 'Brock VandenBerg';
			$maildata['subject'] = $contact_firstname." ".$old_contact_last_name." - User update some fields!";
			$maildata['content_body'] = $messageBody;
			$maildata['button_url'] = '';
			$maildata['button_text'] = '';
			send_mail_template($maildata);
		}

		$contactData['suffix'] 				= $contact_suffix;	
		$contactData['contact_firstname'] 	= $contact_firstname;
		$contactData['contact_middlename'] 	= $contact_middlename;
		$contactData['contact_lastname'] 	= $contact_lastname;
		$contactData['contact_nickname'] 	= $contact_nickname;

		$contactData['contact_phone'] 		= $contact_phone;
		$contactData['primary_option'] 		= $primary_option;

		$contactData['pemail_type']  		= $pemail_type;

		$contactData['contact_address'] 	= $contact_address;
		$contactData['contact_unit'] 		= $contact_unit;
		$contactData['contact_city'] 		= $contact_city;
		$contactData['contact_state'] 		= $contact_state;
		$contactData['contact_zip'] 		= $contact_zip;

		$contactData['contact_mailing_address'] = $contact_mailing_address;
		$contactData['contact_mailing_unit'] 	= $contact_mailing_unit;
		$contactData['contact_mailing_city'] 	= $contact_mailing_city;
		$contactData['contact_mailing_state'] 	= $contact_mailing_state;
		$contactData['contact_mailing_zip'] 	= $contact_mailing_zip;

		$contactWhere['contact_id'] 	= $this->session->userdata('p_user_id');

		$this->User_model->updatedata('contact',$contactWhere,$contactData);
		//echo "1";
		echo json_encode($contactData);
	}

	//$contact_id = $this->session->userdata('p_user_id');
	public function Updatelenderusername(){
		$contact_id 		   = $this->session->userdata('p_user_id');
		$contact_username      = $this->input->post('contact_username');
		$hide_contact_username = $this->input->post('hide_contact_username');

		if($contact_username == $hide_contact_username){
			$resultPos = array('type' => 'error', 'msg' => 'Please enter different username!');
		}else{
			if($contact_username == ''){
				$resultPos = array('type' => 'error', 'msg' => 'Please enter username!');
			}else{
				$personal_data = $this->User_model->query("SELECT * FROM lender_contact_type WHERE username = '$contact_username'");
				$personal_data = $personal_data->row();
				if(!$personal_data){
					$this->User_model->query("UPDATE lender_contact_type SET username = '$contact_username' WHERE contact_id = '$contact_id'");
					$resultPos = array('type' => 'success', 'msg' => 'Your new username updated successfully!');


					$personal_data = $this->User_model->query("SELECT * FROM contact WHERE contact_id = '$contact_id'");
					$personal_data = $personal_data->row();
					$Fullname = $personal_data->contact_firstname.' '.$personal_data->contact_lastname;
					$messageBody  = '';
					$messageBody .= '<p>Some fields update by this user <b>'.$Fullname.'</b></p>';

					$messageBody .= '<table border="1" width="100%">';
					$messageBody .= '<tr><th>Field</th><th>Old Value</th><th>New Value</th></tr>';
					$messageBody .= '<tr><td>Username</td><td>'.$hide_contact_username.'</td><td>'.$contact_username.'</td></tr>';
					$messageBody .= '</table>';


					$maildata = array();
					$maildata['from_email'] = 'mail@talimarfinancial.com';
					$maildata['from_name'] = "Talimar Financial";
					$maildata['to'] = 'payal@bitcot.com';//'brockvandenberg@gmail.com';
					$maildata['name'] = 'Brock VandenBerg';
					$maildata['subject'] = $Fullname." - User update some fields!";
					$maildata['content_body'] = $messageBody;
					$maildata['button_url'] = '';
					$maildata['button_text'] = '';
					send_mail_template($maildata);

				}else{
					$resultPos = array('type' => 'error', 'msg' => 'Please enter different username, this username already exist!');
				}
			}
		}
		echo json_encode($resultPos);

	}

	public function Updatelenderpassword(){
		$contact_id 	   = $this->session->userdata('p_user_id');
		
		$new_password      = $this->input->post('contact_new_password');
		$confirm_password  = $this->input->post('contact_confirm_password');

		if($new_password != $confirm_password){
			$resultPos = array('type' => 'error', 'msg' => 'Please enter same password!');
		}else{
			$this->User_model->query("UPDATE lender_contact_type SET password = '$new_password' WHERE contact_id = '$contact_id'");
			$resultPos = array('type' => 'success', 'msg' => 'Your new password updated successfully!');
		}
		echo json_encode($resultPos);
	}

	public function profile_image_upload(){

		$filename = $_FILES['file']['name'];
		if($filename != ''){

			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			if($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPEG'){

				$path = 'profile_image/'.$filename; //FCPATH
				//echo '<br>';
				//if(move_uploaded_file($_FILES["file"]["tmp_name"],$path)){
				$attachmentName=$this->aws3->uploadFile($path,$_FILES["file"]["tmp_name"]);
				if($attachmentName){

					$datas['imgurl'] = aws_s3_document_url('profile_image/'.$filename);
					$where['contact_id'] = $this->session->userdata('p_user_id');

					$this->User_model->updatedata('lender_contact_type',$where,$datas);
					
					echo json_encode(array('type' => 'success', 'msg' => 'Image uploaded successfully.'));
				}else{
					echo json_encode(array('type' => 'error', 'msg' => 'Error! image not uploaded.'));
				}

			}else{
				echo json_encode(array('type' => 'error', 'msg' => 'Upload only png, jpg & jpeg files.'));
			}

		}else{

			echo json_encode(array('type' => 'error', 'msg' => 'Please choose a file for upload.'));
		}
	}

	public function profile_image_remove(){
		$contact_id = $this->session->userdata('p_user_id');

		$fetchimg = $this->User_model->query("UPDATE lender_contact_type SET imgurl = '' WHERE `contact_id`= '".$contact_id."'");

		if($fetchimg){
			echo '1';
		}else{
			echo '0';
		}
		
	}
	public function contactLenderSurvey(){
		$lenderSurveyArray=array();
		$lenderSurveyArray['ownership_type']=$this->input->post('ownership_type');
		$lenderSurveyArray['ltv']			=$this->input->post('ltv');
		$lenderSurveyArray['position']		=$this->input->post('position');
		$lenderSurveyArray['lend_value']	=$this->input->post('lend_value');
		$lenderSurveyArray['LoanTerm']		=$this->input->post('LoanTerm');
		$lenderSurveyArray['looking_invest_per_deed']		=$this->input->post('looking_invest_per_deed');
		if(!empty($this->input->post('loan'))){
			$lenderSurveyArray['loan']			=implode(",",$this->input->post('loan'));
		}
		if(!empty($this->input->post('property_type'))){
			$lenderSurveyArray['property_type']	=implode(",",$this->input->post('property_type'));
		}
		/* ****************Add New Field***************************************/
		$lenderSurveyArray['maximum_income']	=$this->input->post('trust_deed');
		$lenderSurveyArray['maximum_income']=str_replace("$","",$lenderSurveyArray['maximum_income']);
		$lenderSurveyArray['maximum_income']=str_replace(",","",$lenderSurveyArray['maximum_income']);
		$lenderSurveyArray['invest_per_deed']=$this->input->post('invest_trust_deed');
		$lenderSurveyArray['invest_per_deed']=str_replace("$","",$lenderSurveyArray['invest_per_deed']);
		$lenderSurveyArray['invest_per_deed']=str_replace(",","",$lenderSurveyArray['invest_per_deed']);
		$lenderSurveyArray['funded']	=$this->input->post('founded_trust');
		$lenderSurveyArray['current_year_update']	=date('Y-m-d');
		//die();
		/* ****************Add New Field End***************************************/
		$contact_id 			=$this->input->post('contact_id');
		$where['contact_id'] 	=$contact_id;
		$this->User_model->updatedata('lender_contact_type',$where,$lenderSurveyArray);
		if(!empty($this->input->post('redirect_value'))){
			$this->session->set_flashdata('success', ' Lender Servey Updated Successfully!');
			redirect(base_url().'dashboard','refresh');
		}else{
			$this->session->set_flashdata('success', ' Lender Servey Updated Successfully!');
			redirect(base_url().'view_details/4','refresh');
		}
	}
	
}
	
?>