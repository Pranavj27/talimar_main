<?php 
$account_status_option = $this->config->item('account_status_option'); 
$LoanTerm_type = $this->config->item('LoanTerm_type');
$lender_property_type = $this->config->item('lender_property_type');
$yeild_percent = $this->config->item('yeild_percent');
$loan_tye = $this->config->item('loan_tye');
$position_optionn = $this->config->item('position_optionn');
$ltv_option = $this->config->item('ltv_option');
$lender_lend_value = $this->config->item('lender_lend_value');
?>
<style type="text/css">
	h4.deed_heading {
	    background-color: #ececec;
	    padding: 12px;
	}
	.hd-message-sn {
    display: flex;
    padding: 6px 15px;
}
.table.table-striped>tbody>tr>td, .table.table-striped>tbody>tr>th, .table.table-striped>tfoot>tr>td, .table.table-striped>tfoot>tr>th, .table.table-striped>thead>tr>td, .table.table-striped>thead>tr>th {
    
    padding: 8px;
}
.table.table-striped>thead>tr>th {
    min-width: 104px;
    font-size: 13px !important;
}
tfoot tr th {
	border:none;
}
table#data-table-basicss td {
    border:none;
}
body .table.table-striped tfoot {
    background-color: #fff !important;
}
</style>
	<div class="notika-status-area">

        <div class="container">

            <div class="row">
                <!-- <div class="col-xs-12 col-md-2 col-xl-2"></div> -->
               
                
                <div class="col-xs-12 col-md-12 col-xl-12" >
                <a href="https://talimarfinancial.com/talimar-income-fund-i/" target="_blank"> 
                    <img src="<?php echo base_url('../assets/Banners/5.png'); ?>" style="height: auto; margin-left: auto;margin-right: auto; display:block; ">
                </a>
                </div>
                <!-- <div class="col-xs-12 col-md-4 col-xl-4 dashboard_banner" ></div> -->
                <!-- <div class="col-xs-12 col-md-2 col-xl-2"></div> -->
				<!-- <div class="col-xs-12 col-md-12 col-xl-12">
					<h4 class="deed_heading">Dashboard</h4> 
					
					<h4>Dashboard</h4> 
				</div> -->
                <!-- If User Not Submiited Lender Servey -->
                <?php
                if(!empty($show_alert_section) && $show_alert_section=="yes"){
                    ?>
                    <div class="col-md-12">
                        <div class="alert">
                         <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                          <strong>Alert!</strong> Please Complete Lender Survey .
                          <button class="btn btn-primary" style="float: right" onclick="load_modal(this)">Submit Lender Survey</button>
                        </div>
                    </div>
                    <?php
                }
                else if(!empty($show_update_section) && $show_update_section=="yes"){
                    ?>
                    <div class="col-md-12">
                        <div class="alert">
                         <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                          <strong>Alert!</strong> Please Update Lender Survey For This Year.
                          <a href="<?php echo base_url();?>view_details/4"><button class="btn btn-primary" style="float: right">Update Lender Survey</button></a>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <?php if($this->session->flashdata('success')!=''){  ?>
                    <div class="col-md-12">
                        <div class="alert_success">
                         <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                          <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
                        </div>
                    </div>
                <?php } ?>
                <!-- End Lender Servey -->

                <div class="col-md-12">
                    <div class=" table-responsive dashboard_datatable">
                        <table id="data-table-basicss" class="table table-striped">
                        <thead class="Bgcolor_code_f2f2f2">
                            <tr>
                                <th style="width:20%">Account Name</th>
                                <!-- <th style="width:10%">Lender ID</th> -->
                                <th style="width:10%">Account #</th>
                                <!-- <th style="width:10%">Account Status</th> -->
                                <th style="width:10%"># of Loans</th>
                                <!-- <th style="width:10%">Active Balance</th> -->
                                <th style="width:10%">Outstanding Balance</th>
                                <th style="width:10%">Avg. Yield</th>
                                <th style="width:10%">Monthly Payment</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $count = 0;
                       
                        $total_active_balance = 0;
                        $total_monthly = 0;
                        $totaldeed_count = 0;
                        $total_monthly = 0;
                        $yield = 0.00;
                        
                        foreach($dashboard_data as $row){
                             $totalAVG = 0;
                        $count++;
                        $total_active_balance +=$row['active_bal'];
                        //$total_avrg_yield +=($row['avg_yieldd']);
                        $current_balance +=$row['active_bal'];
                        $totaldeed_count +=$row['total_count'];
                        $total_monthly +=$row['lender_payment'];

                        if($row['lender_payment']  > 0)
                        {
                            $totalAVG = (($row['lender_payment']*12) / $row['active_bal'])*100;
                        }
                            
                        
                        ?>
                            <tr>
                                <td><a href="<?php echo base_url().'active_trust_deeds/'.$row['id'];?>"><?php echo $row['account_name'];?></a></td>
                                
                                <td><?php echo $row['talimar_lender'];?></td>
                                
                                <td><?php echo $row['total_count'];?></td>
                                <td>$<?php echo number_format($row['active_bal'],2);?></td>
                                <td style="width:10%"><?php echo number_format($totalAVG, 2); ?>%</td>
                                <td>$<?php echo number_format($row['lender_payment'],2);?></td>
                            </tr>
                            
                        <?php  } ?>
                            
                             <!-- <tr>
                                <td colspan="9">
                                    <a href="<?php echo base_url();?>Home/addNewAccount" class="btn btn-primary pull-left waves-effect">Add Account</a>
                                </td>
                             </tr> -->
                        </tbody>
                        
                        <tfoot style="background-color: #ddd !important;">
                           <tr> 
                                <th>Total: <?php echo $count;?></th>
                            
                        
                                <th></th>
                                <th><?php echo $totaldeed_count;?></th>
                                
                                <th>$<?php echo number_format($current_balance,2);?></th>



                                <th><?php
                                    if($total_monthly > 0) 
                                    {
                                            $tPer = $total_monthly*12;
                                            $tPer = $tPer/$current_balance;
                                           echo  number_format($tPer*100, 2); 
                                    }
                                    else
                                    {
                                        echo  0.00;
                                    }
                                    ?>%
                                    </th>
                                <th>$<?php echo number_format($total_monthly, 2); ?></th>
                                
                            </tr>
                        
                        </tfoot>
                   </table>
                    </div>
                   
                </div>
                <!--<div class="col-md-12">
                	<a href="<?php echo base_url();?>add_edit_account" class="btn btn-primary pull-right waves-effect">[+] Add Account</a> 
                </div>-->

            </div>

            <div class="row">
            	<div class="col-md-12">
            		<span class="average_Representsweighted">* Represents weighted average calculated by total monthly payments / total active balance.</span>
            	</div>
            </div>

        </div>

    </div>
    <!-- /************************************************Lender Servey Model Start*************************/ -->
    <div class="modal fade bs-modal-lg" id="lender_data" tabindex="-1" role="large" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width: 67%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Lender Servey Data </h4>
                </div>
                <form method="POST" action="<?php echo base_url(); ?>Personal_details/contactLenderSurvey" onsubmit="return getValidate()" >
                    <div class="modal-body">
                            <div class="row">
                                <input type="hidden" name="redirect_value" value="dashboard">
                                <input type="hidden" name="contact_id" value="<?php echo $logged_contact_id;?>">
                                <div class="col-md-5" >
                                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Will you invest in a Fractional Interest?</strong></label>
                                </div>
                                <?php
                                $lender_ownership_type = $this->config->item('lender_ownership_type');
                                $ownership_type = explode(",", $fetch_lender_contact_type[0]->ownership_type);
                                ?>
                                <div class="col-md-3">
                                    <select name="ownership_type" id="ownership_type" class="form-control" >
                                        <option value="">Select One</option>
                                        <?php
                                        if(!empty($lender_ownership_type)){
                                            foreach ($lender_ownership_type as $key => $row) {?>
                                                <option value="<?php echo $key; ?>" ><?php echo $row; ?></option>
                                            <?php 
                                            }
                                        } ?>
                                    </select>
                                    <spam id="ownership_type_error" style="color: red"></spam>
                                </div>
                            </div>
                            <div class="row">&nbsp;&nbsp;</div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>What is your max LTV ratio?</strong></label>
                                </div>
                                <?php
                                $loan = explode(",", $fetch_lender_contact_type[0]->loan);
                                $ltv = explode(",", $fetch_lender_contact_type[0]->ltv);
                                $position = explode(",", $fetch_lender_contact_type[0]->position);
                                $yield = explode(",", $fetch_lender_contact_type[0]->yield);
                                $lender_property = explode(",", $fetch_lender_contact_type[0]->property_type);
                                $LoanTerm = explode(",", $fetch_lender_contact_type[0]->LoanTerm);
                                ?>
                                <div class="col-md-3">
                                    <select name="ltv" id="ltv" class="form-control" >
                                        <option value="">Select One</option>
                                        <?php
                                        if(!empty($ltv_option)){
                                            foreach ($ltv_option as $key => $row){?>
                                                <option value="<?php echo $key; ?>" ><?php echo $row; ?></option>
                                            <?php
                                            } 
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">&nbsp;&nbsp;</div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>What is your maximum loan position?</strong></label>
                                </div>
                                <div class="col-md-3">
                                    <select name="position" id="position" class="form-control" >
                                        <option value="">Select One</option>
                                        <?php
                                        if(!empty($position_optionn)){
                                            foreach ($position_optionn as $key => $row) {?>
                                                <option value="<?php echo $key; ?>" ><?php echo $row; ?></option>
                                            <?php 
                                            }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">&nbsp;&nbsp;</div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Will you lend on Completion Value *?</strong></label>
                                </div>
                                <div class="col-md-3">
                                    <select name="lend_value" id="lend_value" class="form-control" >
                                        <option value="">Select One</option>
                                        <?php
                                        if(!empty($lender_lend_value)){
                                            foreach ($lender_lend_value as $key => $row) {?>
                                                <option value="<?php echo $key; ?>" ><?php echo $row; ?></option>
                                            <?php 
                                            }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">&nbsp;&nbsp;</div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>What is your maximum Loan Term?</strong></label>
                                </div>
                                <div class="col-md-3">
                                    <select name="LoanTerm" id="LoanTerm" class="form-control" >
                                        <option value="">Select One</option>
                                        <?php
                                        if(!empty($LoanTerm_type)){
                                            foreach ($LoanTerm_type as $key => $row) {?>
                                                <option value="<?php echo $key; ?>" ><?php echo $row; ?></option>
                                            <?php 
                                            }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">&nbsp;&nbsp;</div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>What is your maximum investment per Trust Deed?</strong></label>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" name="trust_deed" id="trust_deed" class="form-control" value="$0">
                                    <?php
                                    /*
                                    <select name="trust_deed" id="trust_deed" class="form-control" >
                                        <option value="">Select One</option>
                                        <?php
                                        $trust_deed = $this->config->item('trust_deed');
                                        if(!empty($trust_deed)){
                                            foreach ($trust_deed as $key => $row) {?>
                                                <option value="<?php echo $key; ?>" ><?php echo $row; ?></option>
                                            <?php 
                                            }
                                        } ?>
                                    </select>
                                    */
                                    ?>
                                </div>
                            </div>
                           <!--  <div class="row">&nbsp;&nbsp;</div> -->
                            <div class="row" style="display: none;">
                                <div class="col-md-5" >
                                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>How much are you looking to invest in a single Trust Deeds </strong></label>
                                </div>
                                 <div class="col-md-3">
                                    <input type="text" name="looking_invest_per_deed" id="looking_invest_per_deed" class="form-control" value="">
                                </div>
                            </div>
                            <div class="row">&nbsp;&nbsp;</div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>How much are you looking to invest in Trust Deeds?</strong></label>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" name="invest_trust_deed" id="invest_trust_deed" class="form-control" value="$0">
                                    <?php
                                    /*
                                    <select name="invest_trust_deed" id="invest_trust_deed" class="form-control" >
                                        <option value="">Select One</option>
                                        <?php
                                        $trust_deed = $this->config->item('trust_deed');
                                        if(!empty($trust_deed)){
                                            foreach ($trust_deed as $key => $row) {?>
                                                <option value="<?php echo $key; ?>" ><?php echo $row; ?></option>
                                            <?php 
                                            }
                                        } ?>
                                    </select>
                                    */
                                    ?>
                                </div>
                            </div>
                            <div class="row">&nbsp;&nbsp;</div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>How many Trust Deeds have you funded?</strong></label>
                                </div>
                                 <div class="col-md-3">
                                    <input type="text" name="founded_trust" id="founded_trust" class="form-control" value="">
                                    <?php
                                    /*
                                    <select name="founded_trust" id="founded_trust" class="form-control" >
                                        <option value="">Select One</option>
                                        <?php
                                        $founded_trust_founded = $this->config->item('founded_trust_founded');
                                        if(!empty($founded_trust_founded)){
                                            foreach ($founded_trust_founded as $key => $row) {?>
                                                <option value="<?php echo $key; ?>" ><?php echo $row; ?></option>
                                            <?php 
                                            }
                                        } ?>
                                    </select>
                                    */
                                    ?>
                                </div>
                            </div>
                            <div class="row">&nbsp;&nbsp;</div>
                            <div class="row">
                                <div class="col-md-5">
                                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Select all loan types you would consider:</strong></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-9">
                                    <?php 
                                    if(!empty($loan_tye)){
                                        foreach ($loan_tye as $key => $row) {?>
                                            <div class="col-md-3" >
                                                <input type="checkbox" name="loan[]"  value="<?php echo $key; ?>" <?php if ($loan[0] == $key) {echo "checked";}if ($loan[1] == $key) {echo "checked";}if ($loan[2] == $key) {echo "checked";}?>/>&nbsp;<?php echo $row; ?>
                                            </div>
                                        <?php 
                                        }
                                    }?>
                                </div>
                            </div>
                            <div class="row">&nbsp;&nbsp;</div>
                            <!-- /////////////////lend compition move to under fractional interest -->
                            <div class="row">
                                <div class="col-md-5" style="margin-top: 10px;">
                                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Select all properties types you would consider:</strong></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-9">
                                    <?php 
                                    if(!empty($lender_property_type)){
                                        foreach ($lender_property_type as $key => $row) {?>
                                            <div class="col-md-3"  style="margin-top: 10px;">
                                                <input type="checkbox" name="property_type[]"  value="<?php echo $key; ?>" <?php if (in_array($key, $lender_property)) {echo 'checked';}?>/>&nbsp;<?php echo $row; ?>
                                            </div>
                                        <?php 
                                        }
                                    }?>
                                </div>
                            </div>
                            <div class="row">&nbsp;&nbsp;</div>
                            <div class="row">
                                <div class="col-md-12">
                                    <small>
                                        * Completion Value refers to the value of the property once the renovations are complete. Fix and Flip / Hold loans are generally unwritten to the value of the property once the renovations are complete and may included a Renovation Hold Back for future renovation costs. 
                                    </small>
                                </div>
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <small>
                                        ** Bridge Loan generally refers to loans in which the Borrower does not intend to renovate the property and/or the value is not based upon an After Repair Value. 
                                    </small>
                                </div>
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <small>
                                        *** Fix and Flip / Hold generally refers to loans in which the Borrower intends to renovate the property over the course of the loan term. The loan generally includes a Renovation Hold Back for future renovation costs and the Loan to Value ratio is based upon the Completion Value. 
                                    </small>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="popup_data" value="lender-data">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit"  name="modal_btn" value="save" class="btn btn-success">Save</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
     <!-- /************************************************Lender Servey Model End*************************/ -->
    <link rel="stylesheet" href="<?php echo base_url("assets/css/lender-survey/lender_survey.css"); ?>">  
    <script type="text/javascript" src="<?php echo base_url("assets/js/lender-survey/lender_survey.js"); ?>"></script>
    <script type="text/javascript">    	
    $(document).ready(function(){
		$('#data-table-basicss').DataTable({
            "searching":false,
            // "pageLength":25,
            "bLengthChange":false,

        });	
	});	
    </script>

    <!-- End Status area-->

   

  