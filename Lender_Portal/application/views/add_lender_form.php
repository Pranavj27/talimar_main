<?php
$STATE_USA                          = $this->config->item('STATE_USA');
$investor_type_option               = $this->config->item('investor_type_option');
?>
<div class="page-container">
    <!-- BEGIN PAGE HEAD -->
    <div class="container">
    	<!--------------------MESSEGE SHOW-------------------------->
        <?php if($this->session->flashdata('error')!=''){  ?>
         	<div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error');?></div>
         <?php } ?>
      	 <?php if($this->session->flashdata('success')!=''){  ?>
      	 	<div class="lender_acoout_form">           	                                                   
            	<div class="row">
                	<div class="col-lg-10 col-lg-offset-1 form-box">
	            		<div class="success_from_blc">
	                    	<div class="msg_inner_cont">
	                    		<p class="cst_para">Thank you for registering a new account with TaliMar Financial account. Please allow 2 to 3 days for a Lender Account Representative to review and approve your account information. Once approved, you may begin investing with this account.</p>
	                    	    <p class="cst_para mb-0">Should you have any questions, please contact Investor Relations at <span>(858) 242-4900.</span></p>
	                    	    <p class="cst_para mb-0">
	                    	    	<a href="<?php echo base_url();?>LenderUsers"><span><b>Back to Lender Accounts</b></span></a>
	                    	    </p>
	                    	</div>                        	
	                    </div>
	                </div>
	            </div>
	        </div>
        <?php } ?>
        <?php if($this->session->flashdata('success')==''){  ?>
	        <div class="lender_acoout_form">           	                                                   
	            <div class="row">
	                <div class="col-lg-10 col-lg-offset-1 form-box">
	                	<h3 class="form_title">Add New Lender Account</h3>
	                	<form role="form" action="<?php echo base_url();?>add_investor_data"  method="post" class="f1">
	                		<div class="f1-steps">
	                			<div class="f1-progress">
	                			    <div class="f1-progress-line" data-now-value="33.33" data-number-of-steps="4" style="width:33.33%;"></div>
	                			</div>
	                			<div class="f1-step active">
	                				<div class="f1-step-icon"></div>
	                			</div>
	                			<div class="f1-step">
	                				<div class="f1-step-icon"></div>
	                			</div>
	                		    <div class="f1-step">
	                				<div class="f1-step-icon"></div>
	                			</div>
	                			<div class="f1-step">
	                				<div class="f1-step-icon"></div>
	                			</div>
	                		</div>
	                		<fieldset class="form_wrap">
	                			<div class="row">
	                				<div class="col-md-6">
	                					    <div class="card_head">
			                    		    	<h4 class="frm_h4_title">Account & Basic Information</h4>
			                    		    </div> 
			                    			<div class="form-group">
			                    			   <label for="f1-first-name" class="label_mod">Please Select Account Types<sup class="text_danger">*</sup></label>
			                                   <select  name="investor_type" id="investor_type" class="form-control input_mod" onchange="investor_type_effect(this.value)" >
												   <?php
												    if(!empty($investor_type_option)){
														foreach($investor_type_option as $key => $row){
															?>
															<option value="<?php echo $key;?>"  ><?php echo $row;?></option>
															<?php
														}
													}
				                                    ?>
												</select>
												<span class="text_danger" id="investor_type_error"></span>
			                                </div>
	                				</div>
	                			</div>
	                            <div class="row">
	                            	<div class="col-md-4 col-lg-4">
	                            		 <div class="form-group">
		                                    <label for="f1-last-name" class="label_mod">Create Lender Name<sup class="text_danger">*</sup></label>
		                                     <input type="hidden" name="investor_id" id="investor_id" value="">
		                                    <input type="text" name="investor_name" id="investor_name" placeholder="Type here" class="f1-last-name form-control input_mod"  value ="">
		                                    <span class="text_danger" id="investor_name_error"></span>
		                                </div>
	                            	</div>
	                            	<div class="col-md-4 col-lg-4">
	                            		<div class="form-group">
		                                    <label for="f1-first-name" class="label_mod">Select Account Title<sup class="text_danger">*</sup></label>
		                                   <select type="text" class="form-control input_mod"   name="account_title" id="account_title" onchange="get_selected_account_title(this.value)">
											    <option value="">Select here</option>
												<option value="2">NOT Individual</option>
												<option value="1">Individual(s)</option>	
											</select>
											<span class="text_danger" id="account_title_error"></span>
		                                </div>
	                            	</div>
	                            	<div class="col-md-4 col-lg-4" id="account_title_section">
	                            		<div class="form-group">
		                                    <label for="f1-about-yourself" class="label_mod">What is your title </label>
		                                    <input name="account_title_name" id="account_title_name" placeholder="Type here" class="f1-about-yourself form-control input_mod" value="">
		                                </div>
	                            	</div>
	                            </div> 
	                            <div class="row">
	                            	<div class="col-md-4 col-lg-4">
	                            		<div class="form-group">
		                                    <label for="f1-about-yourself" class="label_mod">Will another person be required to sign disclosures for this account?<sup class="text_danger">*</sup></label>
		                                    <div class="cst_radio_btn">
		                                    	<label class="radio_title">Yes
												  <input type="radio" checked="checked" name="another_persion_signup" id="another_persion_signup_yes" value="1" onchange="another_persion_signup_action(this.value)">
												  <span class="checkmark"></span>
												</label>
												<label class="radio_title">No
												  <input type="radio"  name="another_persion_signup" id="another_persion_signup_no" value="0"  onchange="another_persion_signup_action(this.value)">
												  <span class="checkmark"></span>
												</label>
		                                    </div>
		                                </div>
	                            	</div>
	                            	<div class="col-md-6 col-lg-6" id="lender_contact_section">
	                            		<div class="form-group">
		                                    <label for="f1-first-name" class="label_mod">Contact Lender Portal<sup class="text_danger">*</sup></label>
		                                    <input type="hidden" name="first_contact_name" id="first_contact_name" value="">
		                                    <select type="text" class="form-control input_mod"  onchange="get_selected_data_option('0')" name="contact_id" id="contact_id_0" >
		                                    	<option value="<?php echo $logged_contct_id;?>" option_email="<?php echo $logged_contct_email;?>" option_phone="<?php echo $logged_contct_phone;?>" selected ><?php echo $logged_contact_firstname.' '.$logged_contact_middlename.' '.$logged_contact_lastname; ?></option>
											    	<?php 
													if(!empty($fetch_all_contact)){
														foreach($fetch_all_contact as $row)
														{
															?>
															<option value="<?php echo $row->contact_id;?>" option_email="<?php echo $row->contact_email;?>" option_phone="<?php echo $row->contact_phone;?>" <?php echo $selected;?> ><?php echo $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname; ?></option>
														<?php
														}
													}
	                                            	?>
											</select>
											<span class="text_danger" id="contact_id_error"></span>
											<input type="hidden" name="contact_row_id[]" id="contact_row_id_0" value="<?php echo $logged_contct_id;?>">
											<input type="hidden" name="contact_row_email[]" id="contact_row_email_0" value="<?php echo $logged_contct_email;?>">
											<input type="hidden" name="contact_row_phone[]" id="contact_row_phone_0" value="<?php echo $logged_contct_phone;?>">
		                                </div>
	                            	</div>   
	                            	<div class="col-md-2 col-lg-2">
	                            		<div class="add_btn_blc" style="margin-top: 26px;">
	                            			<a href="javascript:void(0)"  onclick="add_new_contact()"  >Add New</a><br>
	                            		</div> 
	                            	</div>                       
	                            </div>  
	                            <div class="row" id="contact_select_section" >
	                            </div>
	                            <div class="f1-buttons">
	                                <button type="button" class="btn btn-next mr-0">Next</button>
	                            </div>                          
	                        </fieldset>
	                        <fieldset class="form_wrap">
	                         	<div class="row">
	                    			<div class="col-md-12">
		                            	<div class="card_head">
		                    		    	<h4 class="frm_h4_title">Account Information</h4>
		                    		    </div> 
	                		   		</div>
	                	     	</div>
	                		    <div class="row">
	                		    	<div class="col-md-6 col-lg-4">
	                                	<div class="form-group">
		                                    <label for="" class="label_mod">Tax ID Number</label>
		                                    <input type="text" placeholder="Type here" class="form-control input_mod" name ="tax_id" id="tax_id" value ="">
		                                </div>
	                                 </div>
	                                <div class="col-md-6 col-lg-4">
		                                <div class="form-group">
		                                	<label for="" class="label_mod">Registered State </label>
		                                    <select type="text" class="form-control input_mod" name="entity_res_state" id="entity_res_state">
											  <option value="">Select</option>
												<?php
												if(!empty($STATE_USA)){
													foreach($STATE_USA as $key => $row){
														?>
														<option value="<?php echo $key;?>" ><?php echo $row;?></option>
														<?php
													}
												}
				                                ?>
											</select>
		                                </div>
	                                </div>
	                                <div class="col-md-6 col-lg-4">
		                                <div class="form-group">
		                                	<label for="" class="label_mod">State ID #</label>
		                                    <input type="text" class="form-control input_mod" id="state_entity_no" name="state_entity_no" value ="" >
		                                </div>
	                                </div>
	                                <div class="col-md-6 col-lg-4">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod">Date of trust </label>
		                                    <input type="text" name="entity_date_trust" id="entity_date_trust" placeholder="Select Date" class="form-control  entity_date_trust input_mod" autocomplete="off">
		                                </div>
	                                 </div>
	                                  	  <div class="col-md-6 col-lg-4">
	                                	<div class="form-group">
		                                    <label for="" class="label_mod">Custodion Name </label>
		                                    <input type="text" id="directed_IRA_NAME" name = "directed_IRA_NAME" value ="" placeholder="Type here" class="form-control input_mod">
		                                </div>
	                                  </div>
	                                 <div class="col-md-6 col-lg-4">
		                                <div class="form-group">
		                                	<label for="" class="label_mod">IRA Account #</label>
		                                    <input type="text" class="form-control input_mod" name="entity_name" id="entity_name"  value ="">
		                                </div>
	                                </div>
	                                <div class="col-md-6 col-lg-4">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod">Phone Number </label>
		                                    <input type="text" placeholder="Type here" class="form-control input_mod" name="self_dir_account" id="self_dir_account" value ="">
		                                </div>
	                                </div>
	                            </div>
	                            <div class="row">
	                                <div class="col-lg-4 mailing_add_wrap">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod"><b>Mailing Address </b></label>
		                                </div>
	                                 </div>
	                            </div>
	                            <div class="row">
	                                 <div class="col-md-6 col-lg-6">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod">Street Address </label>
		                                    <input type="text" placeholder="Type here" class="form-control input_mod" name = "address" id="address" value ="" >
		                                </div>
	                                 </div>
	                                   <div class="col-md-6 col-lg-6">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod">Unit#</label>
		                                    <input type="text" placeholder="Type here" class="form-control input_mod" name = "unit" id="unit" value ="">
		                                </div>
	                                 </div>
	                                  <div class="col-md-6 col-lg-4">
		                                <div class="form-group">
		                                	<label for="" class="label_mod">City </label>
		                                    <input type="text" class="form-control input_mod" name="city" id="city"  value ="">
		                                </div>
	                                 </div>
	                                 <div class="col-md-6 col-lg-4">
		                                <div class="form-group">
		                                	<label for="" class="label_mod">State </label>
		                                    <select type="text" class="form-control input_mod" name="state" id="state">
											    <?php 
												if(!empty($STATE_USA)){
													foreach($STATE_USA as $key => $row){
														?>
														<option value="<?php echo $key;?>"  ><?php echo $row;?></option>
														<?php
													}
												}
			                                    ?>
											</select>
		                                </div>
	                                </div>
	                                <div class="col-md-6 col-lg-4">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod">Zip</label>
		                                    <input type="text" placeholder="Type here" class="form-control input_mod"  name = "zip" id="zip"  value ="" >
		                                </div>
	                                </div>
	                                <div class="col-md-6 col-lg-4">
	                            		<div class="form-group">
		                                    <label for="f1-about-yourself" class="label_mod">Do you have an FCI Lender Services account for this new account</label>
		                                    <div class="cst_radio_btn">
		                                    	<label class="radio_title">Yes
												  <input type="radio" checked="checked"  name="fci_acct_service" id="fci_acct_service_yes" value="1" onchange="fci_lender_service_action(this.value)">
												  <span class="checkmark"></span>
												</label>
												<label class="radio_title">No
												  <input type="radio"  name="fci_acct_service" id="fci_acct_service_no" value="0" onchange="fci_lender_service_action(this.value)">
												  <span class="checkmark"></span>
												</label>
		                                    </div>
		                                </div>
	                            	</div>
	                                 <div class="col-md-6 col-lg-4 md_clear_both" id="fci_account_section">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod">What is the account number</label>
		                                    <input type="text" placeholder="Type here" class="form-control input_mod"  name ="fci_acct" id="fci_acct" value ="">
		                                </div>
	                                 </div>
	                		    </div>
	                            <div class="f1-buttons">
	                                <button type="button" class="btn btn-previous">Back</button>
	                                <button type="button" class="btn btn-next">Next</button>
	                            </div>
	                        </fieldset>
	                        <fieldset class="form_wrap">
	                           	<div class="row">
	                    			<div class="col-md-12">
		                            	<div class="card_head">
		                    		    	<h4 class="frm_h4_title">Deposit Information</h4>
		                    		    </div> 
	                    		   </div>
	                	      	</div>
	                	      	<div class="row">
	                	      	 	<div class="col-md-6 col-lg-4">
	                            		<div class="form-group">
		                                    <label for="f1-about-yourself" class="label_mod">Would you like distributions to be mailed to you by check or electronically deposited </label>
		                                    <div class="cst_radio_btn">
		                                    	<label class="radio_title">Mailed
												  <input type="radio" checked="checked" name="ach_disbrusement" id="ach_disbrusement_mailed" onclick="ach_disbrusement_value(this.value);"  value="Mailed">
												  <span class="checkmark"></span>
												</label>
												<label class="radio_title">ACH
												  <input type="radio" name="ach_disbrusement" id="ach_disbrusement_ach" onclick="ach_disbrusement_value(this.value);"  value="ACH">
												  <span class="checkmark"></span>
												</label>
		                                    </div>
		                                </div>
	                            	</div>
	                            	<div class="col-md-6 col-lg-4 ach_disbrusement_ach_section" >
		                                <div class="form-group">
		                                	<label for="" class="label_mod">Bank Name </label>
		                                    <input type="text" placeholder="Type here" class="form-control input_mod" name = "bank_name" id="bank_name" value ="">
		                                </div>
	                                </div>
	                                <div class="col-md-6 col-lg-4 ach_disbrusement_ach_section" >
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod">Routing Number</label>
		                                    <input type="text"  placeholder="Type here" class="form-control input_mod" name = "routing_number" id="routing_number" value ="">
		                                </div>
	                                </div>
	                                <div class="col-md-6 col-lg-4 ach_disbrusement_ach_section" >
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod">Account Number</label>
		                                    <input type="text" placeholder="Type here" class="form-control input_mod" name = "account_number" id="account_number" value ="">
		                                </div>
	                                </div>
	                                <div class="col-md-6 col-lg-8">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod">Vesting</label>
		                                    <textarea class="form-control" rows="3" name="vesting"></textarea>
		                                </div>
	                                </div>
	                	      	</div>
	                            <div class="f1-buttons">
	                                <button type="button" class="btn btn-previous">Back</button>
	                                <button type="button" class="btn btn-next" id="confirm_data_btn">Next</button>
	                            </div>
	                        </fieldset>
	                        <fieldset class="form_wrap">
	                        	<div class="row">
	                    			<div class="col-md-12">
		                            	<div class="card_head">
		                    		    	<h4 class="frm_h4_title">Confirmation</h4>
		                    		    </div> 
	                    		   </div>
	                	     	</div>
	                           	<div class="row">
	                           	  	<div class="col-sm-6 col-lg-4">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod font_bold">Account Name: <span class="int_info" id="confirm_account_name">Lender Account</span></label>
		                                   
		                                </div>
	                                </div>
	                                <div class="col-sm-6 col-lg-4">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod font_bold">Account Type: 
		                                    	<span class="int_info" id="confirm_account_type"></span>
		                                    </label>
		                                </div>
	                                </div>
	                                <div class="col-sm-6 col-lg-4">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod font_bold">Contact Name: <span class="int_info" id="confirm_contact_name"></span></label>
		                                </div>
	                                </div>
	                                <div class="col-sm-6 col-lg-4 class_confirm_contact_title">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod font_bold">Title:
		                                    	<span class="int_info" id="confirm_contact_title"></span></label>
		                                </div>
	                                </div>
	                                <div class="col-sm-6 col-lg-4">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod font_bold">Tax ID Number: 
		                                    	<span class="int_info" id="confirm_tax_id"></span></label>
		                                </div>
	                                </div>
	                            </div>
	                            <div class="row">
	                                <div class="col-sm-6 col-lg-6">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod font_bold">Street Address: 
		                                    	<span class="int_info" id="confirm_address"></span></label>
		                                </div>
	                                </div>
	                                <div class="col-sm-6 col-lg-6">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod font_bold">Unit#: 
		                                    	<span class="int_info" id="confirm_unit"></span></label>
		                                </div>
	                                </div>
	                                <div class="col-sm-6 col-lg-4">
		                                <div class="form-group">
		                                	<label for="" class="label_mod font_bold">City 
		                                		<span class="int_info"  id="confirm_city"></span>
		                                	</label>
		                                </div>
	                                </div>
	                                <div class="col-sm-6 col-lg-4">
		                                <div class="form-group">
		                                	<label for="" class="label_mod font_bold">State: 
		                                		<span class="int_info" id="confirm_state"></span></label>
		                                </div>
	                                </div>
	                                <div class="col-sm-6 col-lg-4">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod font_bold">Zip: 
		                                    	<span class="int_info"  id="confirm_zipcode"></span></label>
		                                </div>
	                                </div>
	                                <div class="col-sm-6 col-lg-4">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod font_bold">FCI Account: 
		                                    	<span class="int_info" id="confirm_fci_account"></span></label>
		                                </div>
	                                </div>
	                                <div class="col-sm-6 col-lg-4 class_fci_account" >
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod font_bold">FCI Account Number: 
		                                    	<span class="int_info" id="confirm_account_number"></span></label>
		                                </div>
	                                </div>
	                                <div class="col-sm-6 col-lg-4">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod font_bold ">Deposit Type: 
		                                    	<span class="int_info" id="confirm_deposite_type"></span></label>
		                                </div>
	                                </div>
	                                <div class="col-sm-6 col-lg-4 ach_depiste_info">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod font_bold ">Bank Name: 
		                                    	<span class="int_info" id="confirm_bank_name"></span></label>
		                                </div>
	                                </div>
	                                <div class="col-sm-6 col-lg-4 ach_depiste_info">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod font_bold ">Account #: 
		                                    	<span class="int_info" id="confirm_account_num"></span></label>
		                                </div>
	                                </div>
	                                <div class="col-sm-6 col-lg-4 ach_depiste_info">
	                                 	<div class="form-group">
		                                    <label for="" class="label_mod font_bold ">Routing #: 
		                                    	<span class="int_info" id="confirm_routing_num"></span></label>
		                                </div>
	                                </div>
	                            </div>
	                            <div class="f1-buttons">
	                                <button type="button" class="btn btn-previous">Back</button>
	                                <button type="submit" class="btn btn-next">Submit</button>
	                            </div>
	                        </fieldset>
	                	</form>
	                </div>
	            </div>                   
	        </div>
        <?php } ?>
    </div>
</div>
<link rel="stylesheet" href="<?php echo base_url()?>assets/extra_css_js/jquery-ui.css">
<script src="<?php echo base_url()?>assets/extra_css_js/jquery-ui.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/lender-account/lender_form.js"); ?>"></script>
                
            
              