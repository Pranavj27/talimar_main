<?php

$urival = $this->uri->segment(1);
if($urival == 'trust_deed'){
    $selectoption2 = 'selected';
}elseif($urival == 'trust_deeds'){
    $selectoption1 = 'selected';
}else{
    $selectoption1 = '';
    $selectoption2 = '';
}
            
$loan_type 			= $this->config->item('loan_type_option');
$position_option 	= $this->config->item('position_option');
$property_type 		= $this->config->item('property_modal_property_type');
$trust_deed_option      = $this->config->item('trust_deed_option');

?>

	<div class="data-table-area cpt_available_deeds">
        <div class="container">
			<div class="row">  
                <br>              
                <!-- <div class="col-xs-12">
                    <h3 class="heading_title_script">Available Trust Deeds</h3>
                </div> -->

				<div class="col-xs-12">
                    
                    <div class="col-xs-3 pull-right">
                        <select class="form-control" onchange="viewpagetype(this.value);">
                            <option value="1" <?php echo $selectoption1;?>>Table</option>
                            <option value="2" <?php echo $selectoption2;?>>Block</option>
                        </select>
                    </div>
                    <label class="pull-right">View As:</label>
                </div>    			
			</div>
            <div class="row">&nbsp;</div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive active_trust_deeds_table">
                        <table id="table_contactlisthhhhhh" class="table table-striped table-border">
                                <thead class="Bgcolor_code_f2f2f2">
                                    <tr>                                        
                                        <th>Loan #</th>
                                        <th>Location</th>
                                        <th>Position</th>
                                        <th>Lender Rate</th>
                                        <th>Loan<br>Amount</th>
                                        <th>Available<br>Balance</th>
                                        <th>Minimum<br>Investment</th>
                                        <th>LTV</th>
                                        <th>Closing<br>Date</th>
                                        <th>Term</th>
                                        <th>Trust Deed<br>Status </th>
                                        <th>Details</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
								
								<?php
								$total_loan_amount = 0;
								$count = 0;
								$lender_minimum = 0;
								$available = 0;
                                if(isset($trust_deed_schedule) && is_array($trust_deed_schedule)){
								foreach($trust_deed_schedule as $row){
									
									$ltv = ($row['loan_amount'] + $row['sel_ecum'])/$row['underwriting'] * 100;
                                    
									$total_loan_amount += $row['loan_amount'];
									$count++;
									$lender_minimum += $row['lender_minimum'];
									$available += $row['available'];


                                    if($row['trust_deed'] != 3){
                                                                            
                                        if($row['servicing_lender_rate'] == 'NaN' || $row['servicing_lender_rate'] == '' || $row['servicing_lender_rate'] == '0.00'){

                                            if($row['invester_yield_percent'] >0){

                                            $lender_ratessss = $row['invester_yield_percent'].'%';
                                        }}else{

                                           $lender_ratessss = $row['servicing_lender_rate'].'%';
                                        }
                                    }else{
                                        $lender_ratessss = 'TBD*';
                                    }


                                    $addclass = '';
                                    if($row['trust_deed'] == '2'){
                                        $addclass = '';
                                        $viewlink = base_url().'trust_deed_details/'.$row['talimar_loan'];
                                    }else{
                                        $addclass = 'disabled = "disabled"';
                                        $viewlink = '#';
                                        //$addclass = '';
                                    }



									
									?>
                                    <tr>
                                        <td><?php echo $row['talimar_loan'];?></td>
                                        <td><?php echo $row['city'].', '.$row['state'];?></td>
                                        <td><?php echo $position_option[$row['position']];?></td>
                                        <td><?php echo $lender_ratessss;?></td>
                                        <td>$<?php echo number_format($row['loan_amount']);?></td>
                                        <td>$<?php echo number_format($row['available']);?></td>
                                        <td>$<?php echo number_format($row['lender_minimum']);?></td>
                                        <td><?php echo number_format($ltv,2);?>%</td>
							
                                        <td><?php 

                                         $current_date=date('m-d-Y');
                                         $close_date=date('m-d-Y', strtotime($row['closing_date']));

                                        //if($close_date < $current_date){
                                          if($row['closing_status'] == '6'){
                                              
                                                echo "Active"; 
                                          }else{
                                              
                                               echo $close_date;
                                          }
                                          ?>
                                          
                                      
                                      </td> 
										<td><?php echo $row['term_month'];?></td>
                                        <td><?php echo $trust_deed_option[$row['trust_deed']];?></td>
                                        <td>
                                            <a href="<?php echo $viewlink;?>" title="View" class="btn btn-primary btn-xs" <?php echo $addclass;?>>View</a>
                                        </td>
                                       
                                        
                                    </tr>
								<?php } } ?>
								</tbody>
								
                                <!--<tfoot style="background-color: #f2f2f2;">
                                    <tr>
                                        <th>Total: <?php echo $count;?></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>$<?php echo number_format($available);?></th>
                                        <th>$<?php echo number_format($lender_minimum);?></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        
                                    </tr>
									<tr>
                                        <th>Average:</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>$<?php// echo number_format($lender_minimum/$count);?></th>
                                        <th>$<?php// echo number_format($available/$count);?></th>
                                        <th></th>
                                        
                                    </tr>
                                </tfoot>-->
                            </table>							
						
				    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <span class="">* TBD = To Be Determined.</span>
                </div>
            </div>

        </div>
    </div>
    <script type="text/javascript">
    
        function viewpagetype(val){

            if(val == 1){
                window.location.href = '<?php echo base_url();?>trust_deeds';
            }else{
                window.location.href = '<?php echo base_url();?>trust_deed';
            }
        }
    </script>