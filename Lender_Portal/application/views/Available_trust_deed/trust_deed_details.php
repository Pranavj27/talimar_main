<?php
$loan_type 						= $this->config->item('loan_type_option');
$marketing_loan_type_option 	= $this->config->item('marketing_loan_type_option');
$position_option 				= $this->config->item('position_option');
$property_type 					= $this->config->item('property_modal_property_type');
$yes_no_option 					= $this->config->item('yes_no_option');
$yes_no_option3 				= $this->config->item('yes_no_option3');
$yes_no_option4 				= $this->config->item('yes_no_option4');
$valuation_type_opt 			= $this->config->item('valuation_type');
$condition 						= $this->config->item('property_modal_property_condition');
$STATE_USA 						= $this->config->item('STATE_USA');
$borrower_type_option 			= $this->config->item('borrower_type_option');
$yes_how_many_times 			= $this->config->item('yes_how_many_times');
$fee_disbursement_list 			= $this->config->item('fee_disbursement_list');
$payment_gurranty_option 		= $this->config->item('payment_gurranty_option');
$usa_city_county 				= $this->config->item('usa_city_county');
$marketing_property_type 		= $this->config->item('marketing_property_type');
$borrower_employment_opt 		= $this->config->item('borrower_employment_status');
$valuation_based_upon 		= $this->config->item('valuation_based_upon');
$property_image_Erro = base_url() . 'assets/images/no_image.png';
?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/magnific-popup/magnific-popup.css">

	<div class="container opt_trust_deed_details">
            <div class="row">
				<div class="col-xs-12">
					<?php if($this->session->flashdata('success')!=''){ ?>
					<div class="alert alert-success alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="notika-icon notika-close"></i></span></button> <?php echo $this->session->flashdata('success');?>
					</div>
					<?php } ?>
					<?php if($this->session->flashdata('error')!=''){ ?>
					<div class="alert alert-danger alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="notika-icon notika-close"></i></span></button> <?php echo $this->session->flashdata('error');?>
					</div>
					<?php } ?>
				</div>
			</div>
			<?php

			if(isset($assignmentStatus) && is_array($assignmentStatus)){
				foreach ($assignmentStatus as $key => $value) {
					
					if(in_array($key, $allLenders)){

						if($value == '1'){
							$adddisable = '';
							$openpop = 'onclick="openPOPUPs();"';
						}else{
							$adddisable = 'disabled';
							$openpop = '';
						}
					}else{
						$adddisable = 'disabled';
						$openpop = '';
					}
				}
			}else{
				$adddisable = 'disabled';
				$openpop = '';
			}

			?>
		
			<div class="row">
				
				<div class="col-md-6 col-sm-6 col-xs-12" style="padding-top: 06px">
					<h3 Style="font-weight:400;margin-left: 10px;color:#FFF;">Trust Deed Details: <?php echo $select_property[0]->property_address;?></h3>					
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12" style="padding-top: 06px">
					<?php

						$availableModal = $trust_deed_loan_details[0]->loan_amount - $assignment_investment;
						if($availableModal > 0){
							$modalName = '#myModalfour';
							$addstyle = '';
						}else{
							$modalName = '#';
							$addstyle = 'disabled';
						}

					?>
					<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="<?php echo $modalName;?>" <?php echo $addstyle;?>>Subscribe</button>

					<button type="button" class="btn btn-primary pull-right margin_right12" data-toggle="modal" data-target="#help" >Question</button>
					
					<a class="btn btn-danger pull-right margin_right12"  <?php echo $adddisable;?> <?php echo $openpop;?> >Disclosures</a>
				</div>
			</div>
		
			<div class="row">&nbsp;</div>
			<div class="row">
				<div class="col-xs-12">
					<h4 class="deed_heading">Loan Information:</h4> 
				</div>
			</div>

			<div class="row">
				<div class="col-md-8">
				<div class="row">
	                <div class="col-md-6">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>Loan Amount: </label> <span>$<?php echo number_format($trust_deed_loan_details[0]->loan_amount);?></span>
						</div>
					</div>

					<div class="col-md-6">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<?php $available = $trust_deed_loan_details[0]->loan_amount - $assignment_investment; ?>
							<label>Available Balance: </label> <span>$<?php echo number_format($available);?></span>
						</div>
					</div>
									
				</div>

				<div class="row">

					<div class="col-md-6">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>Position: </label> <span><?php echo $position_option[$trust_deed_loan_details[0]->position];?></span>
						</div>
					</div>
	                <div class="col-md-6">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>Loan Term:</label> <span><?php echo $trust_deed_loan_details[0]->term_month;?> Months</span>
						</div>
					</div>				
									
				</div>

				<div class="row">
					
					<div class="col-md-6">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>Minimum Investment:</label> <span>$<?php echo number_format($trust_deed_loan_servicing_details[0]->lender_minimum);?></span>
						</div>
					</div>

					<?php

						
						$sumUnderwritting = 0;
						foreach ($select_property as $value) {
							$sumUnderwritting += $value->underwriting_value;
						}				
					?>

					<div class="col-md-6">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>Loan to Value:</label> <span><?php
							$ltv = (($trust_deed_loan_details[0]->loan_amount + $current_ecum)/$sumUnderwritting)*100;
							
							echo number_format($ltv,2);
							
							
							?>% of <?php echo $valuation_based_upon[$valuation_based]; ?></span>
						</div>
					</div>

				</div>
				
				
				<div class="row">

					<div class="col-md-6">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<?php 
						
						$servicing_fee = $trust_deed_loan_servicing_details[0]->servicing_fee ? $trust_deed_loan_servicing_details[0]->servicing_fee : '1.00%';
						
						if($trust_deed_loan_servicing_details[0]->servicing_lender_rate == 'NaN' || $trust_deed_loan_servicing_details[0]->servicing_lender_rate == '' || $trust_deed_loan_servicing_details[0]->servicing_lender_rate == '0.00'){

							$lender_rate1 = str_replace("%", "", $invester_yield_percent);
							if($lender_rate1 > 0){
						    	$lender_rate = $lender_rate1;
						    }else{
						    	$lender_rate = $trust_deed_loan_details[0]->intrest_rate - $servicing_fee;
						    }
						}else{

						    $lender_rate = $trust_deed_loan_servicing_details[0]->servicing_lender_rate;
						   
						}

						
						?>
							<label>Lender Rate:</label> <span><?php echo $lender_rate;?>%</span>
						</div>
					</div>

					<?php
						$lenderrateVAls = $lender_rate/100;
						$monthlyNewpay = ($lenderrateVAls * $trust_deed_loan_details[0]->loan_amount)/12;
					?>
					<div class="col-md-6">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>Monthly Payment:</label> <span>$<?php echo number_format($monthlyNewpay,2);?> *</span>
						</div>
					</div>
					
				</div>
				
				<div class="row">

					<div class="col-md-6">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<!--<label>Loan Type:</label> <span><?php echo $loan_type[$trust_deed_loan_details[0]->loan_type]; ?></span>-->
							<label>Loan Type:</label> 
							<span>
								<?php 
								$loan_type_optionadmin = $this->config->item('loan_type_optionadmin');
								 
								echo $loan_type_optionadmin[$trust_deed_loan_servicing_details[0]->marketing_loan_type];
								?>
							</span>
						</div>
					</div>

					<div class="col-md-6">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>Personal Guarantee:</label> <span><?php echo $payment_gurranty_option[$trust_deed_loan_details[0]->payment_gurranty]; ?></span>
						</div>
					</div>

				</div>
				
				<div class="row">
	                
					<?php

						$interest_reserveval = $trust_deed_loan_details[0]->interest_reserve;
						if($interest_reserveval == '1'){
							$payment_held = $trust_deed_loan_details[0]->payment_held_close. ' Payments';
						}else{
							$payment_held = '0 Payments';
						}

					?>
					

					<div class="col-md-6">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>Payment Reserve:</label> <span><?php echo $payment_held;?></span>
						</div>
					</div>

					<div class="col-md-6">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label># of Minimum Payments:</label> <span><?php echo $trust_deed_loan_details[0]->minium_intrest ? $trust_deed_loan_details[0]->minium_intrest.' Payments' : '0';?></span>
						</div>
					</div>
					
				</div>
				
				<div class="row">
	                <?php 

	                	if($multi_properties=='1'){

	                		$mutliple='Yes';
	               
	                }else{
	                	
	                		$mutliple='No';

	                }

	                ?>
					<div class="col-md-6">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>Cross Collateralized:</label> <span><?php echo $mutliple;?></span>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label># of Properties:</label> <span><?php echo $count_property; ?></span>
						</div>
					</div>
					
				</div>

				<div class="row"> 

	            	<?php

	            		if($trust_deed_loan_details[0]->extention_option == '1'){

	            			$extterm = $trust_deed_loan_details[0]->extention_month.' Months';
	            			$extcost = $trust_deed_loan_details[0]->extention_percent_amount.'%';
	            		}else{
	            			$extterm = 'N/A';
	            			$extcost = 'Not Applicable';
	            		}


	            	?>  
					<div class="col-md-6">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>Extension Option:</label> <span><?php echo $yes_no_option[$trust_deed_loan_details[0]->extention_option];?></span>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>Extension Term:</label> <span><?php echo $extterm;?></span>
						</div>
					</div>				
				</div>

				<div class="row">
	               
					<div class="col-md-6">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<?php $closing_date = $trust_deed_loan_details[0]->loan_funding_date ? date('m-d-Y', strtotime($trust_deed_loan_details[0]->loan_funding_date)) : '';?>
							<label>Closing Date:</label> <span><?php echo $closing_date; ?></span>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<?php $maturity_date = $trust_deed_loan_details[0]->maturity_date ? date('m-d-Y', strtotime($trust_deed_loan_details[0]->maturity_date)) : '';?>
							<label>Maturity Date:</label> <span><?php echo $maturity_date; ?></span>
						</div>
					</div>
					
				</div>
			
				<div class="row">
	                
					<div class="col-md-12">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						
							<label>Loan Purpose:</label> <span><?php echo $loan_purpose; ?></span>
						</div>
					</div>
				</div>
			

				<div class="row">
					<div class="col-md-12">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						
							<label>Exit Strategy:</label> <span><?php echo $trust_deed_loan_details[0]->exit_strategy; ?></span>
						</div>
					</div>
				</div>
			
				<div class="row">
	                
					<div class="col-md-12">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<small>* Monthly payment reflects the payment if a single lender funded the entire loan</small>
						</div>
					</div>
					
				</div>
				
				</div>	
					<?php 
						$countimg = 0;
						foreach($select_property_img as $row){

							$count++;
							if($count <= 2){
							
							if($row->image_name != 'blank.jpg'){

								$img_name = aws_s3_document_url('all_property_images/'.$row->talimar_loan.'/'.$row->property_home_id.'/'.$row->folder_id.'/'.$row->image_name);
						
							?>
							<div class="col-md-4">
								<div class="img-preview img-preview-custom">
									<img src="<?php echo $img_name;?>" style="min-width: 0px !important; min-height: 0px !important; max-width: none !important; max-height: none !important; width: 350px; height: 240px; margin-left: -35px; margin-top: -25px;" onerror="this.onerror=null;this.src='<?php echo $property_image_Erro; ?>';">
								<h4 style="text-align: center;font-size: 15px;margin-top: 8px;"><?php echo $row->description ? $row->description : 'Property image';?></h4>
								</div>
								<div class="clearfix"></div>
							</div>
							
						
					<?php } } }  ?>
				
				
			</div> <!--------->

			

			<!--------------- End of Loan information section ------------------------>
			<?php


			?>

			<?php 
			$count = 0;
			foreach ($select_property as $property) { 

				$count++;
				if($count == 1){
					$tagSection = 'Property Information:';
				}else{
					$tagSection = 'Additional Collateral';
				}

				//============ For Property Value Section ============================//
				$com_salesql = $this->User_model->query("SELECT * FROM `comparable_sale` WHERE `talimar_loan`= '".$property->talimar_loan."' AND `property_home_id` = '".$property->property_home_id."'");
				if($com_salesql->num_rows() > 0){

					$com_salesql = $com_salesql->result();
				}else{
					$com_salesql = '';
				}
			?>

			<div class="row">
				<div class="col-xs-12">
					<h4 class="deed_heading"><?php echo $tagSection;?></h4> 
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-6">
							<?php
								if($property->unit != ''){
									$pfullAddress = $property->property_address.' '.$property->unit.'; '.$property->city.', '.$property->state.' '.$property->zip;
								}else{
									$pfullAddress = $property->property_address.'; '.$property->city.', '.$property->state.' '.$property->zip;
								}

							?>
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
								<label>Property Address:</label> <span><?php echo $pfullAddress;?></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
								<label>Property Value:</label> $<span><?php echo number_format($property->underwriting_value);?></span>
							</div>
						</div>
					</div>
						
					<div class="row">
						<div class="col-md-6">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
								<label>Property Type:</label> <span><?php echo $marketing_property_type[$trust_deed_loan_servicing_details[0]->marketing_property_type];?></span>
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
								<label># of Units:</label> <span><?php echo $property->of_unit;?></span>
							</div>
						</div>					
						
					</div>

					<div class="row">
						
						<div class="col-md-6">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
								<label>Square Feet:</label> <span><?php echo $property->square_feet ? number_format($property->square_feet).' SF' : '';?></span>
							</div>
						</div>
				
						<div class="col-md-6">
							<?php 

							$squrefeet = str_replace(",", "", $property->square_feet);
							$totalSF = $property->underwriting_value/$squrefeet;

							 ?>
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
								<label>$ per SF:</label> <span>$<?php echo number_format($totalSF,2);?></span>
							</div>
						</div>
					</div>

					<div class="row">	
						<div class="col-md-6">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
								<label>Lot Size:</label> <span><?php echo number_format($property->lot_size);?> SF</span>
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
								<label>Year Built:</label> <span><?php echo $property->yr_built;?></span>
							</div>
						</div>
						
					</div>
					
					<div class="row">	
						<div class="col-md-6">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
								<label>Bedrooms:</label> <span><?php echo $property->bedrooms;?></span>
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
								<label>Bathrooms:</label> <span><?php echo $property->bathrooms;?></span>
							</div>
						</div>
						
					</div>				

					<div class="row">
						<div class="col-md-6">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
								<label>Condition:</label> <span><?php echo $condition[$property->conditions];?></span>
							</div>
						</div>

						<div class="col-md-6">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
								<label>County:</label> <span><?php echo $usa_city_county[$property->country];?></span>
							</div>
						</div>
												
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
								<label>HOA:</label> <span><?php echo $yes_no_option[$property->hoa];?></span>
							</div>
						</div>

						<?php
							if($property->hoa == '1'){
								$hoa_duesval = number_format($property->hoa_dues,2);
							}else{
								$hoa_duesval = '0.00';
							}
						?>

						<div class="col-md-6">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
								<label>HOA Dues:</label> <span>$<?php echo $hoa_duesval;?></span>
							</div>
						</div>
												
					</div>

				</div>

				<?php

				$addressstring = trim($property->property_address).' '.trim($property->city).' '.trim($property->state).' '.trim($property->zip);
				$address = str_replace(" ", "+", $addressstring);
				//echo $address = $addressstring;
				$region = "USA";

				$json = file_get_contents("https://maps.google.com/maps/api/geocode/json?key=AIzaSyBI3DVnAIUx1poD8p5VdFCF-mcm9UoaA8s&address=$address&sensor=false&region=$region");
				$json = json_decode($json);

				$lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
				$long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
				//echo $lat." == ".$long;

				?>
		
				<div class="col-md-4">
					<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDrPctpsu4vB6Js5H1FczyMPGjVjM11PK8"></script>-->
					
					<div id="map" style="width: 100%; height: 265px;"></div> 
					
					<script type="text/javascript">
				    function initMap() {
					  // The location of Uluru
					  var uluru = {lat: <?php echo $lat;?>, lng: <?php echo $long;?>};
					  // The map, centered at Uluru
					  var map = new google.maps.Map(
					      document.getElementById('map'), {zoom: 8, center: uluru});
					  // The marker, positioned at Uluru
					  var marker = new google.maps.Marker({position: uluru, map: map});
					}
				    </script>
				    <script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBI3DVnAIUx1poD8p5VdFCF-mcm9UoaA8s&callback=initMap"></script>
			

				</div>
				</div>

				<!--<div class="row">
					<div class="col-xs-12">
						<h4 class="deed_heading">Property Value: </h4> 
					</div>
				</div>-->
				
				<div class="row">
					<div class="col-md-12 ">

						<div class="col-md-12">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
								<label>APN:</label> <span><?php echo $property->apn;?></span>
							</div>
						</div>
					</div>
				</div>
				

				<div class="row">
					<div class="col-xs-12">
						<h4 class="deed_heading">Property Value: </h4> 
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
					
						
						<div class="col-md-4 addborder">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
									<label>Address:</label> <span><?php echo $select_property[0]->property_address;?></span>
							</div>
						</div>
						<div class="col-md-4 addborder">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0" style="margin-left: -6px;">
								<label>Valuation Type:</label> <span><?php echo $valuation_type_opt[$property->valuation_type];?></span>
							</div>
						</div>
						<div class="col-md-4 addborder">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0" style="margin-left: -6px;">
								<label>Valuation Date:</label> <span><?php if($property->valuation_date != ''){ echo date('m-d-Y', strtotime($property->valuation_date)); }?></span>
							</div>
						</div>


						<div class="col-xs-12">
				


						<?php if($property->valuation_type == '1' || $property->valuation_type == '2'){ ?>

							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
								<label>See Appraisal / Broker Price Opinion</label>
							</div>

						<?php }else{ ?>

						 <table class="table table-sc-ex" style="margin-left: 3px;">
						 	<thead>
						 	<tr>
						 		<th>Property Address</th>
						 		<th>Date Sold</th>
						 		<th>Property Size</th>
						 		<th>Beds</th>
						 		<th>Baths</th>
						 		<th># of Units</th>
						 		<th>Lot Size</th>
						 		<th>Year Built</th>
						 		<th>Sale Price</th>
						 		<th>$ / SQ FT</th>
						 	</tr>
						 	</thead>
						 	<tbody>
						 	<?php

							$fetch_propoerty_input_fields = $this->User_model->select_where('propoerty_input_fields', array('talimar_loan' => $property->talimar_loan, 'property_home_id' => $property->property_home_id));
							if($fetch_propoerty_input_fields->num_rows() > 0){
								$fetch_propoerty_input_fields = $fetch_propoerty_input_fields->result();

								/*echo '<pre>';
								print_r($fetch_propoerty_input_fields);
								echo '</pre>';*/

								if($fetch_propoerty_input_fields[0]->input_field == 'property_value_modification'){
									$property_value_modification = $fetch_propoerty_input_fields[0]->input_value;
								}

								if($fetch_propoerty_input_fields[1]->input_field == 'comp_sale_val_notes'){
									$comp_sale_val_notes = $fetch_propoerty_input_fields[1]->property_valuation_notes;
								}
							}else{
								$property_value_modification = 0;
								$comp_sale_val_notes = '';
							}


						 	$count_pv = 0;
						 	$prop_building = 0;
						 	$bedrooms = 0;
						 	$bathroom = 0;
						 	$lot_size = 0;
						 	$sale_price = 0;
						 	$per_sq_ft = 0;
						 	
						 		if(isset($com_salesql) && is_array($com_salesql)){
						 			foreach ($com_salesql as $value) {

						 				$count_pv++;
						 				$add=explode("-",$value->property_address);
										$adddress1 = $add[0].' '.$value->unit.';';
										$adddress2 = $add[1].''.$add[2].''.$add[3];

						 				/*echo '<pre>';
										print_r($value);
										echo '</pre>';*/

						 			 ?>

									 	<tr>
									 		<td><a target="_blank" href="<?php echo $value->prop_link;?>"><?php echo $adddress1.' '.str_replace(';', '', $adddress2);?><a></td>
									 		<td><?php echo $value->date_sold; ?></td>
									 		<td><?php echo number_format($value->prop_building);?> SF</td>
									 		<td><?php echo $value->bedrooms;?></td>
									 		<td><?php echo $value->bathroom;?></td>
									 		<td><?php echo $value->prop_unit;?></td>
									 		<td><?php echo $value->lot_size;?></td>
									 		<td><?php echo $value->year_built;?></td>
									 		<td>$<?php echo number_format($value->sale_price,2);?></td>
									 		<td>$<?php echo number_format($value->per_sq_ft,2);?></td>
									 		
									 	</tr>
									 	<?php  //calculation for average:

									 	$stringSF = str_replace("SF", "", $value->prop_building);
									 	$stringComma = str_replace(",", "", $stringSF);
									 	$prop_building += $stringComma;

									 	$bedrooms += $value->bedrooms;
									 	$bathroom += $value->bathroom;

									 	$lotstringSF = str_replace("SF", "", $value->lot_size);
									 	$lotstringComma = str_replace(",", "", $lotstringSF);
									 	$lot_size += $lotstringComma;

									 	$sale_price += $value->sale_price;
									 	$per_sq_ft += $value->per_sq_ft;

									 	?>
							<?php } ?>

									<tr>
										<td><b>Average:</b></td>
										<td></td>
										<td><b><?php echo number_format($prop_building / $count_pv);?></b></td>
										<td><b><?php echo number_format($bedrooms / $count_pv);?></b></td>
										<td><b><?php echo number_format($bathroom / $count_pv);?></b></td>
										<td></td>
										<td><b><?php echo number_format($lot_size / $count_pv);?></b></td>
										<td></td>
										<td><b>$<?php echo number_format($sale_price / $count_pv);?></b></td>
										<td><b>$<?php echo number_format($per_sq_ft / $count_pv,2);?></b></td>
										
									</tr>
									<tr>
										<td><b>Value Modification:</b></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td><b><?php echo number_format($property_value_modification,2);?>%</b></td>
									</tr>
									<tr>
										<td><b>Subject Property:</b></td>
										<td></td>
										<td><b><?php echo number_format($property->square_feet);?></b></td>
										<td><b><?php echo $property->bedrooms;?></b></td>
										<td><b><?php echo $property->bathrooms;?></b></td>
										<td></td>
										<td><b><?php echo number_format($property->lot_size);?></b></td>
										<td><b><?php echo $property->yr_built;?></b></td>

										<?php

											$avgSQFT = $per_sq_ft / $count_pv;
											$calculateallSQFT = ($avgSQFT * $property_value_modification/100) + $avgSQFT;

											$calculateSALEP = $calculateallSQFT * $property->square_feet;

										?>
										<td><b>$<?php echo number_format($calculateSALEP);?></b></td>
										<td><b>$<?php echo number_format($calculateallSQFT,2);?></b></td>
									</tr>
									<tr>
											<td colspan="10"><b>Property Value Explanation:</b> <?php echo $comp_sale_val_notes;?></td>
									</tr>


							<?php }else{ //else part...

								echo '<tr><td colspan="10">No data found!</td></tr>';
							} ?>
							</tbody>
						 </table>
						<?php } ?>
						<div class="property_appraisal_documents">
						<?php
                          if($appraisal_document){
                               foreach($appraisal_document as $contactDocuments) {
                               	$filename 		= $contactDocuments->document_path;
                               	$document_name 	= $contactDocuments->document_name;
                               	?>
                               	<div class="col-xs-2 text-center">
									<a href="<?php echo aws_s3_document_url($filename); ?>" target="_blank">
										<img src="<?php echo base_url();?>/assets/img/diligence/pdf.png">
										<h5><?php echo $document_name; ?></h5>
									</a>
								</div>
                               	<?php
                               }
                           }
						?>
						</div>

					</div>
					</div>
				</div>

		<?php } ?>	<!------------ end foreach loop ----------------->
	
			<div class="row">
				<div class="col-xs-12">
					<h4 class="deed_heading">Borrower Information: </h4> 
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
	                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 addborder">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>Borrower Name:</label> <span>●●●●●●●●
							 <?php //echo $borrower_data_details[0]->b_name;

							 	/*$stringExp = explode(' ', $borrower_data_details[0]->b_name);
								 	foreach($stringExp as $rowss)
								 	{
									 $shortCode = substr($rowss, 0, 1);
										 echo $shortCode;
									
									 $strlength = strlen($rowss) - 1;
									 for($i=1;$i<=$strlength;$i++)
									 {
										 echo '*';
									 }
								 	}*/
							    ?>


							 	
							 </span>
						</div>
					</div>
					<div class="col-lg-8 addborder">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>Borrower Type:</label> <span><?php echo $borrower_type_option[$borrower_data_details[0]->borrower_type];?></span>
						</div>
					</div>
				</div>
			</div>
			
			<?php $i=1;

			/*echo '<pre>';
			print_r($select_contact_details);
			echo '</pre>';*/
			if(isset($select_contact_details)){
			foreach ($select_contact_details as $key => $value) {
				$str = $value['contact_lname'];
 				$fname= $value['contact_fname'].' '.$value['contact_lname']; 

 				if($i>1){

 					$addClass = 'addbordertop';
 				}else{
 					$addClass = '';
 				}
						
			 ?>
			<div class="row">
				<div class="col-md-12">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 <?php echo $addClass;?>">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label>Contact Name:</label> <span>●●●●●●●●
						<?php 
                        // echo $fname;
                       /* $stringExp11 = explode(' ', $fname);
					 	foreach($stringExp11 as $rowss)
					 	{
						 $shortCode11 = substr($rowss, 0, 1);
							 echo $shortCode11;
						
						 $strlength11 = strlen($rowss) - 1;
						 for($a=1;$a<=$strlength11;$a++)
						 {
							 echo '*';
						 }
					 	}*/
				
						?>
						</span>
					</div>
				</div>
				
				
				<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 <?php echo $addClass;?>">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label>Credit Score:</label> <span><?php echo $value['credit_score'];?></span>
					</div>
				</div>
				
				
				</div>
			</div>
			
			<div class="row">
                <div class="col-xs-12">
	                <div class="col-lg-4">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label style="vertical-align:top;">Employment Status:</label> <span><?php echo $borrower_employment_opt[$value['employment_status']];?></span>
						</div>
					</div>

					<div class="col-lg-4">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label style="vertical-align:top;">Current Employer:</label> <span><?php echo $value['current_employer'];?></span>
						</div>
					</div>

					<div class="col-lg-4">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label style="vertical-align:top;">Job Title:</label> <span><?php echo $value['job_title'];?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
                <div class="col-xs-12">
	                <div class="col-lg-4">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label style="vertical-align:top;">Years at Job:</label> <span><?php echo $value['year_at_job'];?></span>
						</div>
					</div>

					<div class="col-lg-4">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label style="vertical-align:top;">Annual Income:</label> <span>$<?php echo number_format($value['annual_income'],2);?></span>
						</div>
					</div>

					<div class="col-lg-4">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label style="vertical-align:top;">Year:</label> <span><?php echo $value['year'];?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
                <div class="col-xs-12">
	                <div class="col-xs-12">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label style="vertical-align:top;">About Contact:</label> <span><?php echo $value['contact_about'];?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
                <div class="col-xs-12">
	                <div class="col-xs-12">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label style="vertical-align:top;">Credit Explanation:</label> <span><?php echo $value['credit_score_desc'];?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
                <div class="col-xs-12">
	                <div class="col-xs-12">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label style="vertical-align:top;">Borrower Disclosures:</label>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
                <div class="col-xs-12">
	                <div class="col-lg-12">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label style="vertical-align:top;margin-left: 10px;">Has the Contact declared bankruptcy in the last 7 years:</label> <span><?php echo $yes_no_option[$value['bankruptcy_year']];?></span>
						</div>
					</div>
				</div>
			</div>

			<?php if($value['bankruptcy_year'] == '1'){ ?>

				<div class="row">
	                <div class="col-xs-12">
		                <div class="col-lg-12">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0" style="margin-left: 15px;">
								<label style="vertical-align:top;">- </label> <span><?php echo $value['bank_year_text'];?></span>
							</div>
						</div>
					</div>
				</div>

			<?php } ?>

			<div class="row">
                <div class="col-xs-12">
	                <div class="col-lg-12">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label style="vertical-align:top;margin-left: 10px;">Is the Contact a US Citizen:</label> <span><?php echo $yes_no_option3[$value['us_citizen']];?></span>
						</div>
					</div>
				</div>
			</div>
			<?php if($value['us_citizen'] == '2'){ ?>

				<div class="row">
	                <div class="col-xs-12">
		                <div class="col-lg-12">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0" style="margin-left: 15px;">
								<label style="vertical-align:top;">- </label> <span><?php echo $value['us_citizen_desc'];?></span>
							</div>
						</div>
					</div>
				</div>

			<?php } ?>			
			<div class="row">
                <div class="col-xs-12">
	                <div class="col-lg-12">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label style="vertical-align:top;margin-left: 10px;">Does the contact have any pending litigation:</label> <span><?php echo $yes_no_option[$value['litigation']];?></span>
						</div>
					</div>
				</div>
			</div>
			<?php if($value['litigation'] == '1'){ ?>

				<div class="row">
	                <div class="col-xs-12">
		                <div class="col-lg-12">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0" style="margin-left: 15px;">
								<label style="vertical-align:top;">- </label> <span><?php echo $value['litigation_desc'];?></span>
							</div>
						</div>
					</div>
				</div>

			<?php } ?>
			<div class="row">
                <div class="col-xs-12">
	                <div class="col-lg-12">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label style="vertical-align:top;margin-left: 10px;">Has the contact been convicted of a felony:</label> <span><?php echo $yes_no_option[$value['felony']];?></span>
						</div>
					</div>
				</div>
			</div>

			<?php if($value['felony'] == '1'){ ?>

				<div class="row">
	                <div class="col-xs-12">
		                <div class="col-lg-12">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0" style="margin-left: 15px;">
								<label style="vertical-align:top;">- </label> <span><?php echo $value['felony_desc'];?></span>
							</div>
						</div>
					</div>
				</div>

			<?php } ?>

			<?php

			$loan_count = 0;
			$loan_amount = 0;
			foreach ($value['activeData'] as $active) {
				
				$loan_count += $active['count'];
				$loan_amount += $active['amount'];
			}

			$loan_count1 = 0;
			$loan_amount1 = 0;
			foreach ($value['PaidoffData'] as $values) {
				
				$loan_count1 += $values['counts'];
				$loan_amount1 += $values['amounts'];
			}

			?>


			<div class="row">
			<div class="col-md-12">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label>Contact History:</label>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label># of Current Loans:</label> <span><?php echo $loan_count; ?></span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label>$ of Current Loans:</label> <span>$<?php echo number_format($loan_amount); ?></span>
					</div>
				</div>
				
			</div>
			</div>

			<div class="row">
				<div class="col-md-12">
	                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
						
					</div>
					
					<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label># of Paid Off Loans:</label> <span><?php echo $loan_count1; ?></span>
						</div>
					</div>
					
					<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>$ of Paid Off Loans:</label> <span>$<?php echo number_format($loan_amount1); ?></span>
						</div>
					</div>
					
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
	                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
						
					</div>
					
					<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label># of Total Loans:</label> <span><?php echo number_format($loan_count + $loan_count1); ?></span>
						</div>
					</div>
					
					<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>$ of Total Loans:</label> <span>$<?php echo number_format($loan_amount + $loan_amount1); ?></span>
						</div>
					</div>
					
				</div>
			</div>

			<?php $i++; } } ?> <!-------- End contact loop ---------->


			<div class="row">
				<div class="col-xs-12">
					<h4 class="deed_heading">Guarantor:</h4> 
				</div>
			</div>			
			<?php 
			if ($fetch_payment_gurrantor) 
			{
				$ivx = 0;
				foreach ($fetch_payment_gurrantor as $key => $payment_gurrantor) 
				{
					$f_strlen = strlen($payment_gurrantor->contact_firstname);
					$l_strlen = strlen($payment_gurrantor->contact_lastname);	
					
					$full_name = $payment_gurrantor->contact_firstname . ' ' . $payment_gurrantor->contact_lastname;
					$credit_score = $payment_gurrantor->credit_score;
					$ivx++;
					?>
						<div class="row">
							<div class="col-md-12">
								<div class="col-sm-3">
									<label class="label-control">Name :</label>
									<span class = "view_inputs">
										<?php echo str_pad("*", (int)$f_strlen, "*", STR_PAD_LEFT)." ".str_pad("*", (int)$l_strlen, "*", STR_PAD_LEFT);  ?>
									</span>
						    	</div>
						   		<div class="col-sm-3">
									<label class="label-control phone-format">Credit Score :</label>
									<span class = "view_inputs"><?php echo $credit_score ? $credit_score : '0'; ?></span>
								</div>

								<div class="col-sm-6">
									&nbsp;
								</div>
							</div>
					    </div>
					    <div class="row">
							<div class="col-md-12">
								&nbsp;
							</div>
						</div>
				<?php 
				}
			}?>
				

			<!-- Project Portpolio start -->

			<div class="row">
				<div class="col-xs-12">
					<h4 class="deed_heading">Project Portfolio:</h4> 
				</div>
			</div>

				<?php if(isset($prior_project) && is_array($prior_project)){ ?>

						<div class="row">
							<div class="col-md-12">
				                <div class="col-md-12">
				                	<table class="table table-sc-ex">
										<thead>
										 	<tr>
										 		<th>Street Address</th>
										 		<!-- <th>State</th> -->
										 		<th>Project Link</th>
										 		<th>Project Image</th>
										 	</tr>
										</thead>
										<tbody>
											<?php foreach($prior_project as $pp){

												$address = '';
												if(!empty($pp->street))
												{
													$address .= $pp->street;
												}

												if(!empty($pp->unit))
												{
													$address .= !empty($address)?$pp->unit : $pp->unit;
												}

												if(!empty($pp->city))
												{
													$address .= !empty($address)?'; '.$pp->city : $pp->city;
												}

												if(!empty($pp->state))
												{
													$address .= !empty($address)?', '.$pp->state : $pp->state;
												}

												if(!empty($pp->zip))
												{
													$address .= !empty($address)?' '.$pp->zip : $pp->zip;
												}
													
											?>
											 	<tr>
											 		<td><?php echo $address ;?></td>
											 		<!-- <td><?php //echo $pp->state;?></td> -->
											 		<td><a target="_blank" href="<?php echo $pp->balink;?>">Link</a></td>
											 		<td>
														<?php if(!empty($pp->projectImage))
														{
															$explode = explode(',', $pp->projectImage);
															foreach ($explode as $key => $value) {
																echo '<img src="'.$value.'" style="width:60px;height:60px">&nbsp;';
															}
														}

														?>
													</td>
											 	</tr>
											 <?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>

				<?php }else{ ?>

						<div class="row">
							<div class="col-md-12">
				                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				                	<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
										<label>Not Applicable</label>
									</div>
								</div>
							</div>
						</div>

				<?php } ?>

			<!-- Project Portpolio end -->
			
			<div class="row">
				<div class="col-xs-12">
					<h4 class="deed_heading">Sources and Uses:</h4> 
				</div>
			</div>

			<?php 

			if(isset($select_loan_source_and_uses) && is_array($select_loan_source_and_uses)) {

				if($select_loan_source_and_uses[0]->activate_table != '1'){ ?>

				<div class="row">
					<div class="col-md-12">
	                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
	                	<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>Not Applicable</label>
						</div>
					</div>
					</div>
				</div>

			<?php }else{ ?>

	            <div class="row">
					<div class="col-md-12">
		                <div class="col-xs-12">
							
							<table class="table table-sc-ex">
								<thead>
									
								 	<tr>
								 		<th style="width: 30%;">Line Item</th>
								 		<th style="width: 15%;">Loan Funds</th>
								 		<th style="width: 5%;">+</th>
								 		<th style="width: 15%;">Borrower Funds</th>
								 		<th style="width: 5%;">=</th>
								 		<th style="width: 20%;">Project Costs</th>
								 		<!--<th>% of Costs</th>-->
								 	</tr>
							 	</thead>
							 	<tbody>
							 		<?php 
							 		$project_cost = 0;
							 		$borrower_funds = 0;
							 		$loan_funds = 0;
							 		 
							 				foreach ($select_loan_source_and_uses as $key => $value) {

								 				if($value->project_cost < 1){
													$percostvalue = 0.00;
												}else{
													$percostvalue = ($value->loan_funds / $value->project_cost) * 100;
												} 

												if($value->items == 'Interest Payments'){
													$lineItems = 'Debt Service';
												}else{
													$lineItems = $value->items;
												}
							 				?>

										 		<tr>
										 			<td><?php echo $lineItems; ?></td>
										 			<td>$<?php echo number_format($value->loan_funds,2); ?></td>
										 			<td></td>
										 			<td>$<?php echo number_format($value->borrower_funds,2); ?></td>
										 			<td></td>
										 			<td>$<?php echo number_format($value->project_cost,2); ?></td>
										 			<!--<td><?php echo number_format($percostvalue,2); ?>%</td>-->
										 		</tr>

										 		<?php //calculate the total...

										 		$project_cost += $value->project_cost;
										 		$borrower_funds += $value->borrower_funds;
										 		$loan_funds += $value->loan_funds;

										 		?>

								 	<?php } ?>

								 				<tr>
								 					<td><b>Total</b></td>
								 					<td><b>$<?php echo number_format($loan_funds,2); ?></b></td>
								 					<td></td>
								 					<td><b>$<?php echo number_format($borrower_funds,2); ?></b></td>
								 					<td></td>
								 					<td><b>$<?php echo number_format($project_cost,2); ?></b></td>
								 					<!--<td><b><?php echo number_format($loan_funds/$project_cost*100,2); ?>%</b></td>-->
								 				</tr>

								 				<tr>
								 					<td colspan="6">
								 						<small>* The purpose of the Sources and Uses table is to show the estimated total project costs, which costs will be funded by the Loan, and the total funds being contributed by the Borrower. The sum of the funds being contributed by the Loan and the Borrower will equal the total project costs.</small>
								 					</td>
								 				</tr>

							 	</tbody>
							</table>
						</div>
					</div>
				</div>

			<?php } }else{ ?>

					<div class="row">
						<div class="col-md-12">
		                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
		                	<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
								<label>Not Applicable</label>
							</div>
						</div>
						</div>
					</div>
			<?php } ?>

			<div class="row">
				<div class="col-xs-12">
					<h4 class="deed_heading">Other Encumbrances:</h4> 
				</div>
			</div>

			<?php 

			$count_ecum = count($select_ecumbrance);

			if($count_ecum == 0){ ?>

			<div class="row">
				<div class="col-md-12">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label>No Other Encumbrances</label>
					</div>
				</div>
				</div>
			</div>


			<?php }else{ ?>

			<?php

				// echo '<pre>';
				// print_r($select_ecumbrance);
				// echo '</pre>';

			?>

			<div class="row">
				<div class="col-md-12">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label>Lender Name:</label> <span><?php echo $select_ecumbrance[0]->lien_holder;?></span>
					</div>
				</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label>Original Balance:</label> <span><?php echo '$'.number_format($select_ecumbrance[0]->original_balance);?></span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label>Current Balance:</label> <span><?php echo '$'.number_format($select_ecumbrance[0]->current_balance);?></span>
					</div>
				</div>
				
				<!-- <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label>LTV:</label> <span><?php echo number_format($ltv,2);?>%</span>
					</div>
				</div> -->
				</div>
				
			</div>
			
			
			<div class="row">
				<div class="col-md-12">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label>Interest Rate:</label> <span><?php echo number_format($select_ecumbrance[0]->intrest_rate,2);?>%</span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label>Monthly Payment:</label> <span><?php echo '$'.number_format($select_ecumbrance[0]->monthly_payment,2);?></span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				</div>
				
			</div>
			
			<div class="row">
				<div class="col-md-12">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<?php $maturity_date = $select_ecumbrance[0]->maturity_date ? date('m-d-Y', strtotime($select_ecumbrance[0]->maturity_date)) : '';?>
						<label>Maturity Date:</label> <span><?php echo $maturity_date;?></span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				</div>
				
			</div>
		
			
			<!--<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label>Is this an Existing Loan?</label> <span><?php echo $yes_no_option3[$select_ecumbrance[0]->existing_lien];?></span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
			</div>
			
			<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label style="margin-left:20px;">What is the current Position:</label> <span><?php echo $position_option[$select_ecumbrance[0]->current_position];?></span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
			</div>
			<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label style="margin-left:20px;">Will the loan be paid off at close?</label> <span><?php echo $yes_no_option3[$select_ecumbrance[0]->will_it_remain];?></span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
			</div>
			<div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label style="margin-left:20px;">Has the loan been delinquent greater than 60 days in the last 12 months?</label> <span><?php echo $yes_no_option3[$select_ecumbrance_data[0]->payment_lates];?></span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
			</div>
			
			<div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label style="margin-left:40px;">How many times?</label> <span><?php echo $yes_how_many_times[$select_ecumbrance_data[0]->yes_how_many];?></span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
			</div>
			<div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label style="margin-left:40px;">Will any payments remain outstanding?</label> <span><?php echo $yes_no_option4[$select_ecumbrance_data[0]->payment_unpaid];?></span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
			</div>
			<div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label style="margin-left:40px;">Will the loan proceeds repay the delinquent payments?</label> <span><?php echo $yes_no_option4[$select_ecumbrance_data[0]->yes_proceeds_subject_loan];?></span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
			</div>
			<div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label style="margin-left:60px;">If not, what is the source of payments to bring the loan current?</label> <span><?php echo $select_ecumbrance_data[0]->no_source_of_fund;?></span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
			</div>
			<div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label>What will be the position of the loan after close?</label> <span><?php echo $position_option[$select_ecumbrance[0]->proposed_priority];?></span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
			</div>
			<div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label>Will the loan have priority over the proposed loan?</label> <span><?php echo $yes_no_option3[$select_ecumbrance[0]->existing_priority_to_talimar];?></span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
			</div>-->

		<?php } ?>
			
			<div class="row">
				<div class="col-xs-12">
					<h4 class="deed_heading">Cashflow Statement:</h4> 
				</div>
				
			</div>
			<?php if($select_property[0]->Cash_Flow == '2'){ ?>
			
			<div class="row">
				<div class="col-md-12">
				<div class="col-xs-12">
					<div class="col-xs-6">
				
						<h4>Income:</h4>
						
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Income (Monthly)</th>
									<th>Amount</th>
									
								</tr>
							</thead>
							<tbody>
							<?php
							$total_amount_income = 0;
							foreach($select_monthly_income as $row){

								$total_amount_income += $row->amount;
							?>
								<tr>
									<td><?php echo $row->items;?></td>
									<td>$<?php echo number_format($row->amount);?></td>
									
								</tr>
							<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<th>Total Income</th>
									<th>$<?php echo number_format($total_amount_income);?></th>
									
								</tr>
							</tfoot>
						</table>
					</div>
					<div class="col-xs-6">
				
						<h4>Expenses:</h4>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Expense (Monthly)</th>
									<th>Amount</th>
									
								</tr>
							</thead>
							<tbody>
							<?php
							$total_amount_expense = 0;
							foreach($select_monthly_expense as $row){
								$total_amount_expense += $row->amount;
							
							?>
								<tr>
									<td><?php echo $row->items;?></td>
									<td>$<?php echo number_format($row->amount);?></td>
									
								</tr>
							<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<th>Total Expense</th>
									<th>$<?php echo number_format($total_amount_expense);?></th>
									
								</tr>
							</tfoot>
						</table>
						
						<h5 style="text-align: right;">Net Operating Income: <?php echo '$'.number_format($total_amount_income - $total_amount_expense);?></h5>
						
					</div>
				</div>
				</div>
			</div>

			<?php }else{ ?>

				<div class="row">
					<div class="col-md-12">
	                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>Not Applicable</label>
						</div>
					</div>
					</div>

				</div>
			<?php } ?>

			<div class="row">
				<div class="col-xs-12">
					<h4 class="deed_heading">Proposed Improvements:</h4> 
				</div>
			</div>

			<?php if($select_property[0]->rehab_property == '1'){?>

				<div class="row">
					<div class="col-md-12">
		                <div class="col-lg-4">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
								<label>Project Budget:</label> <span>$<?php echo number_format($select_property[0]->construction_budget,2);?></span>
							</div>
						</div>
						
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="col-lg-12">
							<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
								<label>Scope of Work:</label> <span><?php echo $select_property[0]->desc_property_p_impro;?></span>
							</div>
						</div>

					</div>
				</div>

			<?php }else{ ?>

				<div class="row">
					<div class="col-md-12">
	                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>Not Applicable</label>
						</div>
					</div>
					</div>
				</div>

			<?php } ?>

			<div class="row">
				<div class="col-xs-12">
					<h4 class="deed_heading">Construction Funds Disbursement Schedule</h4> 
				</div>
			</div>

			<?php if($trust_deed_loan_details[0]->draws == '1'){ ?>

				<div class="row">
					<div class="col-md-12">
		                <div class="col-xs-12">
							
							<table class="table table-sc-ex">
								<thead>
								 	<tr>
								 		<th style="width: 10%;">Draw #</th> 
								 		<th style="width: 40%;">Description</th>
								 		<th style="width: 15%;">Amount</th>
								 		<th style="width: 15%;">Released</th>
								 		<th style="width: 20%;">Remaining</th>
								 	</tr>
							 	</thead>
							 	<tbody>
							 		<?php 
							 		$countDraw1 = 0; 
							 		$budget = 0; 
							 		$releaseAmoutT = 0; 
							 		if(isset($select_loan_draws) && is_array($select_loan_draws)){
							 				foreach($select_loan_draws as $row){ 

							 				$countDraw1++;
							 				$budget += $row->budget;
							 				$releaseAmout = $row->date1_amount + $row->date2_amount + $row->date3_amount + $row->date4_amount + $row->date5_amount + $row->date6_amount + $row->date7_amount + $row->date8_amount + $row->date9_amount + $row->date10_amount + $row->date11_amount + $row->date12_amount + $row->date13_amount + $row->date14_amount + $row->date15_amount;
							 				$releaseAmoutT += $releaseAmout;
							 		?>
									 		<tr>
									 			<td><?php echo $countDraw1;?></td>
									 			<td><?php echo nl2br($row->draws_description);?></td>
									 			<td>$<?php echo number_format($row->budget);?></td>
									 			<td>$<?php echo number_format($releaseAmout);?></td>
									 			<td>$<?php echo number_format($row->budget - $releaseAmout);?></td>
									 		</tr>

									 <?php } ?>

									 		<tr>
									 			<td><b></b></td>
									 			<td><b>Total: <?php echo $countDraw1;?></b></td>
									 			<td><b>$<?php echo number_format($budget);?></b></td>
									 			<td><b>$<?php echo number_format($releaseAmoutT);?></b></td>
									 			<td><b>$<?php echo number_format($budget - $releaseAmoutT);?></b></td>
									 		</tr>

									<?php }else{ ?>
									 		<tr>
									 			<td colspan="5">No data found!</td>
									 		</tr>
									 <?php } ?>
							 	</tbody>
							 	
							</table>
						</div>
					</div>
				</div>

			<?php }else{ ?>

				<div class="row">
					<div class="col-md-12">
	                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>Not Applicable</label>
						</div>
					</div>
					</div>
				</div>

			<?php } ?>

			<div class="row">
				<div class="col-xs-12">
					<h4 class="deed_heading">Servicing Information / Other Disclosures:</h4> 
				</div>
			</div>
			<div class="row">
			<?php
			if($trust_deed_loan_servicing_details[0]->sub_servicing_agent == '14'){
				$servicer_name = 'FCI Lender Services';
			}elseif($trust_deed_loan_servicing_details[0]->sub_servicing_agent == '15'){
				$servicer_name = 'TaliMar Financial Inc.';
			}elseif($trust_deed_loan_servicing_details[0]->sub_servicing_agent == '16'){
				$servicer_name = 'La Mesa Fund Control';
			}elseif($trust_deed_loan_servicing_details[0]->sub_servicing_agent == '19'){
				$servicer_name = 'Del Toro Loan Servicing';
			}else{
				$servicer_name = '';
			}
			
			
			?>
			<div class="col-md-12">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label>Loan Servicer:</label> <span><?php echo $servicer_name; ?></span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label>Security:</label> <span>Deed of Trust / Assignment</span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label>Document Custodian:</label> <span>TaliMar Financial</span>
					</div>
				</div>
				
			</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label>Actual Note Rate:</label> <span><?php echo number_format($trust_deed_loan_details[0]->intrest_rate,2);?>%</span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					<?php $servicing_fee = $trust_deed_loan_servicing_details[0]->servicing_fee ? number_format($trust_deed_loan_servicing_details[0]->servicing_fee,2) : '1.00';

						$new_val = $trust_deed_loan_details[0]->intrest_rate - $lender_rate;

					?>
					<label>Servicing Fee:</label> <span><?php echo number_format($new_val,2);?>%</span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
					
					<?php $note_rate = $trust_deed_loan_details[0]->intrest_rate - $servicing_fee; ?>
						<label>Lender Note Rate:</label> <span><?php echo number_format($lender_rate,2);?>%</span>
					</div>
				</div>
				</div>
				
			</div>
			<div class="row">
				<div class="col-md-12">
				<?php
									
					foreach($fee_disbursement_list as $key => $row)
					{
						$fee_disbursement_item = isset($fee_disbursement_data) ? $fee_disbursement_data[$key]->items : $row ;
						
						if($fee_disbursement_item == 'Late Fee')
						{
							$fieldvalue = "late_fee";
							
							$l_broker_value = isset($fee_disbursement_data) ? number_format($fee_disbursement_data[$key]->broker) : '50';
							
							

						}
						else if($fee_disbursement_item == 'Extension Fee')
						{
							$fieldvalue = "extention_fee";
							
							$e_broker_value = isset($fee_disbursement_data) ? number_format($fee_disbursement_data[$key]->broker) : '75';
						
							
					
							
						}
						else if($fee_disbursement_item == 'Default Rate')
						{
							$fieldvalue = "default_rate";
							
							
							$d_broker_value = isset($fee_disbursement_data) ? number_format($fee_disbursement_data[$key]->broker): '50';
						
							
							

						}
					
						
					}
				?>
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
						<label>Fees Paid to TaliMar Financial:</label> <span></span>
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
					
				</div>
				</div>
				
			</div>

			
			
			<div class="row">
				<div class="col-md-12">
				<div class="col-md-4">
					
						<label style="margin-left:20px;">Late Fee:</label> <span><?php echo number_format($l_broker_value,2)?>%</span>
					
				</div>
				
				<div class="col-md-4">
					
						<label style="margin-left:20px;">Extension Fee:</label> <span><?php echo number_format($e_broker_value,2)?>%</span>
					
				</div>
				
				<div class="col-md-4">
					
						<label style="margin-left:20px;">Default Rate:</label> <span><?php echo number_format($d_broker_value,2)?>%</span>
					
				</div>
				</div>
				
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">
							<label>Other Disclosures:</label> <span></span>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0" style="margin-left:20px;">
							<span><?php echo $trust_deed_loan_details[0]->add_broker_data_desc;?></span>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="search-engine-int sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0" style="margin-left:20px;">
							<span><?php echo $trust_deed_loan_details[0]->add_broker_data_desc;?></span>
						</div>
					</div>
				</div>
			</div>

			<?php //new 03-10-2021 ?>
			<div class="row">
				<div class="col-xs-12">
					<h4 class="deed_heading">Escrow / Title Information:</h4> 
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					
					<div class="col-sm-4">
						<strong>Escrow Company: </strong><span><?php echo $escrow_contact_name['company']; ?></span>
					</div>
					<div class="col-sm-4">
						<strong>Title Company: </strong></span><?php echo $fetch_title_data->company;; ?></span>
					</div>
					<div class="col-sm-4">
					</div>
					
				</div>
				<div class="clearfix"></div>
				<br>
				
				<div class="col-sm-12"><br/></div>
				
			</div>


			
			
		
				




			<?php //new 03-10-2021 ?>

			<div class="row">
				<div class="col-xs-12">
					<h4 class="deed_heading">Property Images:</h4> 
				</div>
			</div>
			
			<?php 
			
			
				foreach($select_property_img as $row){
					
					if($row->image_name != 'blank.jpg'){

						$img_name = 'all_property_images/'.$row->talimar_loan.'/'.$row->property_home_id.'/'.$row->folder_id.'/'.$row->image_name;
				
					?>
					<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
						<div class="img-preview img-preview-custom">
							<img src="<?php echo aws_s3_document_url($img_name);?>" style="min-width: 0px !important; min-height: 0px !important; max-width: none !important; max-height: none !important; width: 350px; height: 240px; margin-left: -35px; margin-top: -25px;" onerror="this.onerror=null;this.src='<?php echo $property_image_Erro; ?>';">
						<h4 style="text-align: center;font-size: 15px;margin-top: 8px;"><?php echo $row->description ? $row->description : 'Property image';?></h4>
						</div>
						<div class="clearfix"></div>
					</div>
				
			<?php } }  ?>

			<?php 
			/*
				Date:20-07-2021
				Title: Bitcot new changes
				Details:New changes according to optimization code
				Update date-20-07-2021
			*/
			?>
			
			<div class="row">
				<div class="col-xs-12">
					<h4 class="deed_heading">Due Diligence Items:</h4> 
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<?php
					if(!empty($diligence_document)){
						foreach ($diligence_document as $diligenceDocument) {
							$document_name      = $diligenceDocument->document_name;
							$diligence_document = $diligenceDocument->capital_diligence_document;
							
							$fileextenstion = pathinfo(basename($diligence_document), PATHINFO_EXTENSION);
							if($fileextenstion == 'pdf'){
							?>
								<div class="col-xs-2 text-center">
									<a href="<?php echo aws_s3_document_url($diligence_document); ?>" target="_blank">
										<img src="<?php echo base_url();?>/assets/img/diligence/pdf.png">
										<h5><?php echo $document_name; ?></h5>
									</a>
								</div>							
							<?php
							}elseif($fileextenstion == 'doc'){
							?>
								<div class="col-xs-2 text-center">
									<a href="<?php echo aws_s3_document_url($diligence_document); ?>" target="_blank">
										<img src="<?php echo base_url();?>/assets/img/diligence/microsoft-word.png">
										<h5><?php echo $document_name; ?></h5>
									</a>
								</div>
							<?php
							}elseif($fileextenstion == 'docx'){
							?>
								<div class="col-xs-2 text-center">
									<a href="<?php echo aws_s3_document_url($diligence_document); ?>" target="_blank">
										<img src="<?php echo base_url();?>/assets/img/diligence/microsoft-word.png">
										<h5><?php echo $document_name; ?></h5>
									</a>
								</div>
							<?php
							}else{
							?>
								<div class="col-xs-2 text-center">
									<a href="<?php echo aws_s3_document_url($diligence_document); ?>" target="_blank">
										<img src="<?php echo base_url();?>/assets/img/diligence/document.png">
										<h5><?php echo $document_name; ?></h5>
									</a>
								</div>
							<?php
							}
						}
					}else{
						?>
						<div class="col-sm-4">
							<span>Document Not Found</span>
						</div>
						<?php
					}
					?>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-12"><br/></div>
			</div>
			
		</div>
	</div>
	
	
	<!---------- Help Modal start ------------>
	<div class="modal animated bounce" id="help" role="dialog">
		<div class="modal-dialog modals-default modal-sm">
			<div class="modal-content">
			<form method="POST" action="<?php echo base_url();?>Available_deed/questionData">
				<div class="modal-header">
					<h4 style="margin-left: 15px;">Submit Question:</h4>
					<!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
				</div>
				<div class="modal-body">
					<?php //print_r($this->session->all_userdata());?>
					<div class="row applymargin">
						<div class="col-md-12">
							<label>User Name:</label>
							<input type="text" class="form-control" name="username" value="<?php echo $this->session->userdata('p_user_fname').' '.$this->session->userdata('p_user_lname');?>" readonly="readonly">
							<input type="hidden" class="form-control" name="talimar_loan" value="<?php echo $trust_deed_loan_details[0]->talimar_loan;?>" readonly="readonly">
						</div>
					</div>
					<?php
						if($select_property[0]->unit !=''){
							$fullPAddress = $select_property[0]->property_address.' '.$select_property[0]->unit.'; '.$select_property[0]->city.', '.$select_property[0]->state.' '.$select_property[0]->zip;
						}else{
							$fullPAddress = $select_property[0]->property_address.'; '.$select_property[0]->city.', '.$select_property[0]->state.' '.$select_property[0]->zip;
						}
					?>

					<div class="row applymargin">
						<div class="col-md-12">
							<label>Property Address:</label>
							<input type="text" class="form-control" name="loannumber" value="<?php echo $fullPAddress;?>" readonly="readonly">
						</div>
					</div>

					<div class="row applymargin">
						<div class="col-md-12">
							<label>Your Question:</label>
							<textarea class="form-control" rows="5" type="text" name="question" required="required"></textarea>
						</div>
					</div>

				</div>
				<div class="modal-footer" style="margin-top: 15px !important; margin-right: 15px !important;">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit</button>
					
				</div>
			</form>
			</div>
		</div>
	</div>


	<!---------- Subscribe Modal start ------------>
	<div class="modal animated bounce" id="myModalfour" role="dialog">
		<div class="modal-dialog modals-default modal-sm">
			<div class="modal-content">
			<!--<form method="POST" action="<?php echo base_url();?>confirm_order">-->
				<div class="modal-header">
					<!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
				</div>
				<div class="modal-body">
					<h2 class="mtitme_modal">Trust Deed Subscription:</h2>
					
					<div class="row">&nbsp;</div>
					<div class="row">
						<div class="col-xs-5">
							<label>Loan Amount:</label>
						</div>
						<div class="col-xs-7">
							<div class="form-group">
								<div class="form-single nk-int-st widget-form">
									<!-- <input type="text" name="loan_amount" class="form-control" placeholder="Loan Amount" value="$<?php//echo number_format($trust_deed_loan_details[0]->loan_amount);?>" > -->
									<span>$<?php echo number_format($trust_deed_loan_details[0]->loan_amount);?></span>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-5">
							<label>Available:</label>
						</div>
						<div class="col-xs-7">
							<div class="form-group">
							<?php $available = $trust_deed_loan_details[0]->loan_amount - $assignment_investment; ?>
								<div class="form-single nk-int-st widget-form">
									<!-- <input type="text" name="available" id="available_amount" class="form-control" placeholder="Available" value="$<?php// echo number_format($available);?>" readonly> -->
									<span>$<?php echo number_format($available);?></span>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-5">
							<label>Lender Minimum:</label>
						</div>
						<div class="col-xs-7">
							<div class="form-group">
								<div class="form-single nk-int-st widget-form">
									<!-- <input type="text" name="lender_minimum" id="minimum_amount" class="form-control" placeholder="Lender Minimum" value="$<?php //echo number_format($trust_deed_loan_servicing_details[0]->lender_minimum);?>" readonly> -->
									<span>$<?php echo number_format($trust_deed_loan_servicing_details[0]->lender_minimum);?></span>
								</div>
							</div>
						</div>
					</div>

					<div class="row">&nbsp;</div>
					
					
					<div class="row">
						<div class="col-xs-5">
							<label>Account:</label>
						</div>
						<div class="col-xs-7">
							<div class="form-group">
								<div class="form-single nk-int-st widget-form">
									<select class="form-control" name="lender_accounts" id="lender_accounts">
									<option value="0">Select One</option>
									<?php foreach($select_portal_account as $key => $value){ ?>
											<option value="<?php echo $value->lender_id;?>"><?php echo $value->name;?></option>
									<?php }  ?>
									</select>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-5">
							<label>$ Commitment:</label>
						</div>
						<div class="col-xs-7">
							<div class="form-group">
								<div class="form-single nk-int-st widget-form">
									
									<input type="text" name="lender_commitment" onchange="allow(this)" id="lender_commitment" class="form-control number-only" placeholder="Lender Commitment">
								</div>
							</div>
						</div>
					</div>

					<div class="row" id="error_div">
						<div class="col-xs-12">
							<span style="color: red;display: none;text-align: center;"></span>
						</div>
					</div>
					
					<div class="row">&nbsp;</div>
					
				</div>
				<div class="modal-footer" style="margin-right: 15px;">
					<button type="button" onclick="refresh_window(this);" class="btn nk-indigo" data-dismiss="modal">Close</button>
					<button type="submit" onclick="this_order_details(this);" class="btn nk-indigo">Review Order</button>
					
				</div>
			<!--</form>-->
			</div>
		</div>
	</div>
	
	
	<!---------- Subscribe Modal end ------------>
	
	
	<!---------- Review Modal end ------------>
	<div class="modal animated bounce" id="review_order" role="dialog">
		<div class="modal-dialog modals-default modal-sm">
			<div class="modal-content">
			<form method="POST" action="<?php echo base_url();?>confirm_order">
				<input type="hidden" name="talimar_loan" value="<?php echo $talimar_loan;?>">
				<div class="modal-header">
					<!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
				</div>
				<div class="modal-body">
					<h2 style="font-weight:600px;font-size:20px;">Confirmation:</h2>
					<p>Please review the following information carefully.<br>By selecting <b>Confirm</b>, you are agreeing to fund the trust deed as shown below.</p>
					
					<div class="row">&nbsp;</div>
					<div class="row">
						<div class="col-xs-5">
							<label>Property Address:</label>
						</div>
						<div class="col-xs-7">
							<div class="form-group">
								<div class="form-single nk-int-st widget-form">
									<!-- <input type="text" name="property_address" class="form-control" placeholder="Property Address" value="" readonly> -->
								
									<span><?php echo $select_property[0]->property_address.' '.$select_property[0]->unit.'<br>'.$select_property[0]->city.', '.$select_property[0]->state.' '.$select_property[0]->zip; ?></span>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-5">
							<label>Account:</label>
						</div>
						<div class="col-xs-7">
							<div class="form-group">
								<div class="form-single nk-int-st widget-form">
									<!--<input type="hidden" class="form-control" name="lender_accounts" id="select_account" value="<?php echo $value->lender_id;?>">-->
									

									<span id="val"><?php echo $value->name;?></span>
								</div>
							</div>
						</div>
					</div>
					
					
					<div class="row">
						<div class="col-xs-5">
							<label>Commitment:</label>
						</div>
						<div class="col-xs-7">
							<div class="form-group">
								<div class="form-single nk-int-st widget-form">
									<!-- <input type="text" name="investment_amount" id="investment_amount" class="form-control number-only" placeholder="Investment Amount" readonly> -->
									 <input type="hidden" name="investment_amount" id="investment_a" value="" class="number-only"> 
									<span class="number-only" id="investment_amount"></span>
								</div>
							</div>
						</div>
					</div>
								
					<div class="row">
						<div class="col-xs-5">
							<label>Lender Rate:</label>
						</div>
						<div class="col-xs-7">
							<div class="form-group">
								<div class="form-single nk-int-st widget-form">
									<span id="lenderrateview"></span>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-5">
							<label>Monthly Payment:</label>
						</div>
						<div class="col-xs-7">
							<span id="monthlyPaymentnewvl"></span>
						</div>
					</div>
					
					<input type="hidden" name="percent_loan" id="percent_loan">
					<input type="hidden" name="invester_yield_percent" id="invester_yield_percent">
					<input type="hidden" name="payment" id="payment">
					<input type="hidden" name="lender_id" id="lender_id">
					<input type="hidden" name="lender_ratenew" id="lender_ratenew">
					
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn nk-indigo" data-dismiss="modal">Edit</button>
					<button type="submit" name="submit" value="order" class="btn nk-indigo">Confirm</button>
					<!--<button class="btn nk-indigo" id="sa-success">Confirm</button>-->
				</div>
			</form>
			</div>
		</div>
	</div>
	<!---------- Review Modal end ------------>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script>

function this_order_details(that){
	
	//----------------------- for the calculation ---------------//
	
	var lender_investment 	= parseFloat($('input#lender_commitment').val());
	var loan_amount 		= parseFloat('<?php echo $trust_deed_loan_details[0]->loan_amount;?>');
	
	var percent_loan = parseFloat((lender_investment / loan_amount)*100);
	$('input#percent_loan').val(number_format_percent_loan(percent_loan)+'%');
	
	var interest_rate = parseFloat('<?php echo $trust_deed_loan_details[0]->intrest_rate;?>');
	var servicing_fee = parseFloat('<?php echo $servicing_fee;?>');
	
	var invester_yield_percent = interest_rate - servicing_fee;
	$('input#invester_yield_percent').val(number_format(invester_yield_percent)+'%');
	
	var payment = ((invester_yield_percent/100) * lender_investment) / 12;
	$('input#payment').val(number_format(payment));
	
	//----------------------- for the calculation ---------------//
	
	
	
	var available_amount 	= parseFloat('<?php echo $available;?>');
	var minimum_amount 		= parseFloat('<?php echo $trust_deed_loan_servicing_details[0]->lender_minimum;?>');
	var lender_commitment 	= parseFloat($('input#lender_commitment').val());
	var lender_accounts   	= $('select#lender_accounts').val();
	var lender_fullname   	= $('select#lender_accounts option[value='+lender_accounts+']').text();

	
	if(available_amount < lender_commitment){
		
		//swal("Error!", "The investment must be less than the Available Balance of $"+available_amount+".");
		$('#error_div span').text('The investment must be less than the Available Balance of $'+available_amount.toLocaleString()+'.');
		$('#error_div span').css('display','block');
		
	}else if((minimum_amount > lender_commitment) && (available_amount > minimum_amount)){
		
		//swal("Error!", "The investment must be greater than the Lender Minimum of $"+minimum_amount+".");
		$('#error_div span').text('The investment must be greater than the Lender Minimum of $'+minimum_amount.toLocaleString()+'.');
		$('#error_div span').css('display','block');
		
	}else if(isNaN(lender_commitment) == true){
		
		//swal("Error!", "Please enter some investment balance!");
		$('#error_div span').text('Please enter some investment balance!');
		$('#error_div span').css('display','block');
		
	}else if(lender_accounts == '0'){
		
		//swal("Error!", "Please choose an account for investment!");
		$('#error_div span').text('Please choose an account for investment.');
		$('#error_div span').css('display','block');
		
	}else{
	
		$('div#review_order span#investment_amount').text('$' +lender_commitment.toLocaleString());
		$('div#review_order input#investment_a').val(lender_commitment);
		$('div#review_order input#lender_id').val(lender_accounts);
		
		
		$('div#review_order span#val').text(lender_fullname);
	
		$('#error_div span').css('display','none');

		//for monthly pament
		var lenderrate = '<?php echo $lender_rate;?>';
		var lenderrateval = lenderrate/100;

		$('div#review_order input#lender_ratenew').val(lenderrate);
		$('div#review_order span#lenderrateview').text(lenderrate.toLocaleString(undefined, {minimumFractionDigits: 2})+'%');

		var monthlyPaymentnewvl = (lenderrateval * lender_commitment)/12;
		$('div#review_order span#monthlyPaymentnewvl').text('$' +monthlyPaymentnewvl.toLocaleString(undefined, {minimumFractionDigits: 2}));
		
		$('div#review_order').modal('show');
	
	}

	
}
function allow(th){
	
var value=	$(th).val().replace(',','').replace('$','');
$(th).val(value);
}

$(document).ready(function () {
   $(".number-only").keydown(function (event) {
      
        if (/^-?\d*[,]?\d{0,2}$/.test(event.key) ){

        }
		else if(event.shiftKey || event.keyCode == 8) 
		{
              
        }else {
          
			if ( (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)){
				
				swal("Error!", "Only Numbers Allowed!");
				event.preventDefault();
            }

       }
   });
});

function refresh_window(that){
	
	window.location.reload();
}

function number_format_percent_loan(n) { 
	
    return n.toFixed(8).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "" + c : c;
    });
}

function number_format(n) { 
	
    return n.toFixed(2).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "" + c : c;
    });
}

function number_format_comma(n)
{
	    return n.replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}


function openPOPUPs(){

	let params = `scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=1000,height=500,
	left=100,top=100`;

	open('<?php echo $fetchDocusign[0]->docusign_url;?>', 'test', params);

}
</script>
<!-- Page JS Plugins -->
<script src="<?php echo base_url();?>assets/magnific-popup/jquery.magnific-popup.min.js"></script>
<!-- Page JS Helpers (Magnific Popup Plugin) -->
<script>jQuery(function(){ One.helpers('magnific-popup'); });</script>
	

			