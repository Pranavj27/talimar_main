<?php
$loan_type 						= $this->config->item('loan_type_option');
$marketing_loan_type_option 	= $this->config->item('marketing_loan_type_option');
$position_option 				= $this->config->item('position_option');
$property_type 					= $this->config->item('property_modal_property_type');
$yes_no_option 					= $this->config->item('yes_no_option');
$yes_no_option3 				= $this->config->item('yes_no_option3');
$yes_no_option4 				= $this->config->item('yes_no_option4');
$valuation_type_opt 			= $this->config->item('valuation_type');
$condition 						= $this->config->item('property_modal_property_condition');
$STATE_USA 						= $this->config->item('STATE_USA');
$borrower_type_option 			= $this->config->item('borrower_type_option');
$yes_how_many_times 			= $this->config->item('yes_how_many_times');
$fee_disbursement_list 			= $this->config->item('fee_disbursement_list');
$payment_gurranty_option 		= $this->config->item('payment_gurranty_option');
$usa_city_county 				= $this->config->item('usa_city_county');
$marketing_property_type 		= $this->config->item('marketing_property_type');
$borrower_employment_opt 		= $this->config->item('borrower_employment_status');
$valuation_based_upon 		= $this->config->item('valuation_based_upon');
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$property_image_Erro = base_url() . 'assets/images/no_image.png';
?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/magnific-popup/magnific-popup.css">


<div class="container cpt_mortgageFund">
    <div class="row">
		<div class="col-xs-12">
			<?php if($this->session->flashdata('success')!=''){ ?>
			<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="notika-icon notika-close"></i></span></button> <?php echo $this->session->flashdata('success');?>
			</div>
			<?php } ?>
			<?php if($this->session->flashdata('error')!=''){ ?>
			<div class="alert alert-danger alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="notika-icon notika-close"></i></span></button> <?php echo $this->session->flashdata('error');?>
			</div>
			<?php } ?>
		</div>
	</div>

	<div class="col-xs-12 col-md-12 col-xl-12">
	    <a href="https://talimarfinancial.com/talimar-income-fund-i/" target="_blank"> 
	        <img src="<?php echo base_url('../assets/Banners/5.png'); ?>" style="height: auto; margin-left: auto;margin-right: auto; display:block; ">
	    </a>
    </div>
    <div class="clearfix"></div>
    <br>
	
	<div class="row border-top" id="mortgageFundAccounts">
		<h4 class="deed_heading" style="height:60px">Mortgage Fund Accounts</h4>


		<div class="col-md-6">
			<table class="table table-condensed flip-content mortgageFundAccounts">
			<thead>
				<tr>
					<td style="border: none;"><strong>Account Name:</strong></td>
					<td style="border: none;"><strong>Amount:</strong></td>
				</tr>
			</thead>
			<tbody>
				<?php $amountT = 0; if(!empty($investerData)){ ?>
				<tr>
					<td style="border: none;"><?php echo $investerData->name; ?></td>
					<td style="border: none;">$<?php echo number_format($investerData->amountTotal); ?></td>
				</tr>

			<?php  } ?>
			</tbody>

			</table>				 
		</div>
		<div class="col-md-6">
							 
		</div>
		
		<div class="clearfix"></div>
	</div>
	<br>
	<div class="row">
		
		<h4 class="deed_heading">Current Assets in Fund</h4>
		<div class="table-responsive active_trust_deeds_table">
			<table id="table" class="table table-striped">
				<thead>
					<tr>
						<th>Loan #</th>
						<th>City</th>
						<th>State</th>
						<th>Property Type</th>
						<th>Loan Type</th>
						<th>Loan Balance</th>
						<th>LTV</th>
						<th>$ Ownership</th>
						<th>% Ownership</th>				
						<th>Lender Rate</th>
						
						<th>Monthly Payment</th>
						<th>Loan Status</th>
					</tr>
				</thead>
				<tbody>
				<?php $total = 0; $loan_amount = 0; $lender_ratessss = 0.00; $current_balance = 0; $intrest_rate = 0; $OwnershipTotal = 0; $ltvTotal = 0;  if(!empty($LoanDetails)){ foreach ($LoanDetails as $key => $value) {
					$loan_type = $value->loan_type;
					if((double)$value->investment > 0)
					{
						$per = (double)$value->investment/(double)$value->current_balance_t;
						$per = $per*100;
					}
					else
					{
						$per = 0.00;
					}

					if($value->servicing_lender_rate == 'NaN' || $value->servicing_lender_rate == '' || $value->servicing_lender_rate == '0.00'){

					if($value->invester_yield_percent >0){

						$lender_ratessss = $value->invester_yield_percent;
					}}else{

					   $lender_ratessss = $value->servicing_lender_rate;
					}

					$ltvTotal = $ltvTotal + $value->loanToValue;

					
				 ?>						
					<tr>
						<td><?php echo $value->talimar_loan; ?></td>
						<td><?php echo $value->city; ?></td>
						<td><?php echo $value->state; ?></td>
						<td><?php echo isset($value->property_type)?$property_type_option[$value->property_type]:''; ?></td>
						<td><?php echo !empty($loan_type_option[$loan_type]) ? $loan_type_option[$loan_type] : ''; ?></td>
						<td>$<?php echo number_format(round($value->current_balance_t)); ?></td>
						<td><?php echo $value->loanToValue; ?>%</td>
						<td>$<?php echo number_format(round($value->investment)); ?></td>
						<td><?php echo number_format($per, 2); ?>%</td>
						<td><?php echo !empty($lender_ratessss)?number_format((double)$lender_ratessss, 3):0.00; ?>%</td>
						<td>$<?php echo number_format((double)$value->paymenttotal, 2); ?></td>
						
						<td>
						<?php
							$condition = $this->config->item('serviceing_condition_option');
							echo $condition[$value->condition];
						?>											
						</td>
					</tr>
					<?php $total++;
					$loan_amount = $loan_amount + (double)$value->paymenttotal;
					$current_balance = $current_balance + $value->current_balance_t;
					$intrest_rate = (double)$intrest_rate + (double)!empty($lender_ratessss)?number_format((double)$lender_ratessss, 3):0.00;
					$OwnershipTotal = $OwnershipTotal + $value->investment;
					
				 }}else{ ?>
					<tr>
						<td colspan="13">Data not found</td>
					</tr>
				<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<th>Total</th>
						<th><?php echo $total;?></th>
						<th></th>
						<th></th>
						<th></th>
						
						<th>$<?php echo number_format(round($current_balance)); ?></th>
						<th></th>
						<th>$<?php echo number_format(round($OwnershipTotal)); ?></th>
						<th></th>
						<th></th>
						<th>$<?php echo number_format((double)$loan_amount, 2); ?></th>
						<th></th>		
					</tr>
					<tr>
						<th>Average:</th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						
						<th>$<?php echo number_format($total > 0?(double)$current_balance/$total:0); ?></th>
						<th><?php echo number_format($total > 0?(double)$ltvTotal/$total:0.00, 2); ?>%</th>
						<th>$<?php echo number_format($total > 0 ?(double)$OwnershipTotal/$total:0); ?></th>
						<th><?php echo number_format($OwnershipTotal >0?((double)$OwnershipTotal/$current_balance)*100:0.000, 2); ?>%</th>
						<th><?php  $tInve = (double)$loan_amount > 0 ?((double)($loan_amount*12)/$OwnershipTotal)*100:0.00;
								echo number_format($tInve, 3);
						?>%</th>
						<th>$<?php echo number_format((double)$loan_amount > 0 ?(double)$loan_amount/$total:0, 2); ?></th>
						<th></th>		
					</tr>
				</tfoot>
			</table>
		</div>
					
		
	</div> <!--------->
</div>

<script type="text/javascript">
	$(document).ready(function() {
  
	$('#table').DataTable({
		searching: false
	});
});
</script>