<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Login | Lender Portal</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/img/favicon1.png">
    <!-- Google Fonts
		============================================ -->
    <!--<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <!-- font awesome CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <!--<link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">-->
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/normalize.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/wave/waves.min.css">
    <!-- Notika icon CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/notika-custom-icon.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<style>
* {
    font-family: 'Open Sans', sans-serif !important;
}
h2{
	color:#123657 !important;
}
a{
    cursor: pointer;
}
</style>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Login Register area Start-->
	
    <div class="login-content">
	
        <!-- Login -->
        <div class="nk-block toggled" id="l-login">
		<div class="input-group mg-t-15">
			<img src="<?php echo base_url();?>assets/img/logo/logo-big.png" alt="TaliMar Financial">
		</div>
		
		<div class="input-group mg-t-15">&nbsp;</div>
            <div class="nk-form">
			<h2>Lender Portal</h2><br>
			<?php if($this->session->flashdata('success')!=''){ ?>
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="notika-icon notika-close"></i></span></button> <?php echo $this->session->flashdata('success');?>
                </div>
			<?php } ?>
			
			<?php if($this->session->flashdata('error')!=''){ ?>
				<div class="alert alert-danger alert-dismissible alert-mg-b-0" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="notika-icon notika-close"></i></span></button> <?php echo $this->session->flashdata('error');?>
                </div>
			<?php } ?>
			
			<form method="post" action="<?php echo base_url();?>login">
                <div class="form-group">
                    <label class="pull-left">Username</label>
                    <div class="">
                        <input type="text" name="email" class="form-control" placeholder="Username" required>
                    </div>
                </div>
                <div class="form-group">
                     <label class="pull-left">Password</label>
                    <div class="">
                        <input type="password" name="password" class="form-control" placeholder="Password" required>
                    </div>
                </div>
                <div class="fm-checkbox">
                    <!--<label><input type="checkbox" class="i-checks"> <i></i> Keep me signed in</label>-->
                </div>
				<div class="form-example-int mg-t-15">
					
                    <a data-ma-action="nk-login-switch" data-ma-block="#l-reset-password" class="text-primary pull-left">Forgot Password</a><br>
                    <a data-ma-action="nk-login-switch" data-ma-block="#l-register" class="text-primary pull-left">Not Registered? Create an Account</a>

					<button class="btn btn-primary notika-btn-success waves-effect pull-right" type="submit" name="submit" style="margin-top: -16px;">Sign In</button>
					
                </div>
                <div class="form-example-int mg-t-15">&nbsp;</div>
			</form>
               <!-- <a href="#l-register" data-ma-action="nk-login-switch" data-ma-block="#l-register" class="btn btn-login btn-success btn-float"><i class="notika-icon notika-right-arrow right-arrow-ant"></i></a>-->
            </div>

           <div class="nk-navigation nk-lg-ic">
               <!-- <a href="#" data-ma-action="nk-login-switch" data-ma-block="#l-register"><i class="notika-icon notika-plus-symbol"></i><span>Sign up</span></a>-->
                <!--<a href="#" data-ma-action="nk-login-switch" data-ma-block="#l-forget-password"><i>?</i><span>Forgot Password</span></a>-->
            </div>
			
			
			
        </div>

		
		
        <!-- Register --->
        <div class="nk-block" id="l-register">
		<div class="input-group mg-t-15">
			<img src="<?php echo base_url();?>assets/img/logo/logo-big.png" alt="TaliMar Financial">
		</div>
		
		<div class="input-group mg-t-15">&nbsp;</div>
            <div class="nk-form">
			<h2>Sign Up</h2><br>
			<form method="post" action="<?php echo base_url();?>register">

                <div class="form-group">
                    <label class="pull-left">First Name</label>
                    <div class="">
                        <input type="text" name="fname" class="form-control" placeholder="First Name" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="pull-left">Last Name</label>
                    <div class="">
                        <input type="text" name="lname" class="form-control" placeholder="Last Name" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="pull-left">Phone Number</label>
                    <div class="">
                        <input type="text" name="phone" class="form-control phone-format" placeholder="Phone Number">
                    </div>
                </div>
                <div class="form-group">
                    <label class="pull-left">E-Mail Address</label>
                    <div class="">
                        <input type="email" name="email" class="form-control" placeholder="E-Mail Address" required>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="pull-left">Username</label>
                    <div class="">
                        <input type="text" name="username" class="form-control" placeholder="User Name" minlength="6" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="pull-left">Password</label>
                    <div class="">
                        <input type="password" name="password" class="form-control" placeholder="Password" minlength="8" required>
                    </div>
                </div>

				<div class="form-example-int mg-t-15">
                    <button class="btn btn-primary notika-btn-success waves-effect pull-right btn-block" type="submit">Register</button>
                     <a data-ma-action="nk-login-switch" data-ma-block="#l-login" class="text-primary btn-block pull-left" style="margin-top: 10px;">Back</a>
                </div>
                <div class="form-example-int mg-t-15">&nbsp;</div>
			</form>
                
            </div>
        </div>


        <!------ Reset password -------->
        <div class="nk-block" id="l-reset-password">
        <div class="input-group mg-t-15">
            <img src="<?php echo base_url();?>assets/img/logo/logo-big.png" alt="TaliMar Financial">
        </div>
        
        <div class="input-group mg-t-15">&nbsp;</div>
        <div class="nk-form">
        <form method="post" action="<?php echo base_url();?>Home/resetPassword">
            <h2>Lender Portal</h2>
                <p class="text-center">Please provide the e-mail associated with this username and we will send you a new password.</p>
                <div class="form-group">
                    <label class="pull-left">Enter Your E-mail</label>
                    <div class="">
                        <input type="email" name="emailreset" class="form-control" placeholder="E-mail Address" required>
                    </div>
                </div>
                
                <div class="form-example-int mg-t-15">

                    <a data-ma-action="nk-login-switch" data-ma-block="#l-login" class="text-primary pull-left" style="margin-top: 10px;">Sign In</a>

                    <button class="btn btn-primary notika-btn-success waves-effect pull-right" type="submit" name="email">Send E-mail</button>
                </div>
                <div class="form-example-int mg-t-15">&nbsp;</div>
            </form>
            </div>
        </div>
    
		
		

        <!-- Forgot Password -->
        <div class="nk-block" id="l-forget-password">
		<div class="input-group mg-t-15">
			<img src="<?php echo base_url();?>assets/img/logo/logo-big.png" alt="TaliMar Financial">
		</div>
		<div class="input-group mg-t-15">&nbsp;</div>
		
		<div class="input-group mg-t-15">&nbsp;</div>
		<form method="post" action="<?php echo base_url();?>forget_password">
            <div class="nk-form">
			<h2>Lender Portal</h2><br>
                <p class="text-center"><mark>Enter Registraed E-mail Address Below.</mark></p>

                <div class="form-group">
                    <label class="pull-left">E-mail</label>
                    <div class="">
                        <input type="email" name="email" class="form-control" placeholder="E-mail Address" required>
                    </div>
                </div>
				<div class="form-group">
                    <label class="pull-left">Enter New Password</label>
                    <div class="">
                        <input type="password" name="password" class="form-control" placeholder="Enter New Password" minlength="6" required>
                    </div>
                </div>
				<div class="form-group">
                    <label class="pull-left">Re-Type Password</label>
                    <div class="">
                        <input type="password" name="c_password" class="form-control" placeholder="Re-Type Password" minlength="6" required>
                    </div>
                </div>
				<div class="form-example-int mg-t-15">
                    <button class="btn btn-primary notika-btn-success waves-effect pull-right" type="submit">Submit</button>
                </div>
                <div class="form-example-int mg-t-15">&nbsp;</div>
                <!--<a href="#l-login" data-ma-action="nk-login-switch" data-ma-block="#l-login" class="btn btn-login btn-success btn-float"><i class="notika-icon notika-right-arrow"></i></a>-->
            </div>
		</form>
            <div class="nk-navigation nk-lg-ic rg-ic-stl">
                <a href="" data-ma-action="nk-login-switch" data-ma-block="#l-login"><i class="notika-icon notika-right-arrow"></i> <span>Sign in</span></a>
               <!-- <a href="" data-ma-action="nk-login-switch" data-ma-block="#l-register"><i class="notika-icon notika-plus-symbol"></i> <span>Sign up</span></a>-->
            </div>
        </div>
    </div>
    <!-- Login Register area End-->
    <!-- jquery
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/jquery-price-slider.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/jquery.scrollUp.min.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/meanmenu/jquery.meanmenu.js"></script>
    <!-- counterup JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/counterup/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/counterup/waypoints.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/counterup/counterup-active.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/sparkline/jquery.sparkline.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/sparkline/sparkline-active.js"></script>
    <!-- flot JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/js/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url();?>assets/js/flot/flot-active.js"></script>
    <!-- knob JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/knob/jquery.knob.js"></script>
    <script src="<?php echo base_url();?>assets/js/knob/jquery.appear.js"></script>
    <script src="<?php echo base_url();?>assets/js/knob/knob-active.js"></script>
    <!--  Chat JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/chat/jquery.chat.js"></script>
    <!--  wave JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/wave/waves.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/wave/wave-active.js"></script>
    <!-- icheck JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/icheck/icheck.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/icheck/icheck-active.js"></script>
    <!--  todo JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/todo/jquery.todo.js"></script>
    <!-- Login JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/login/login-action.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/main.js"></script>

    <script type="text/javascript">
        

    $(document).ready(function(){
      /***phone number format***/
      $(".phone-format").keypress(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
              return false;
            }
            var curchr = this.value.length;
            var curval = $(this).val();
            if (curchr == 3 && curval.indexOf("(") <= -1) {
              $(this).val("(" + curval + ")" + "-");
            } else if (curchr == 4 && curval.indexOf("(") > -1) {
              $(this).val(curval + ")-");
            } else if (curchr == 5 && curval.indexOf(")") > -1) {
              $(this).val(curval + "-");
            } else if (curchr == 9) {
              $(this).val(curval + "-");
              $(this).attr('maxlength', '14');
            }
          });
    });
    </script>

    

</body>

</html>