<?php
$contact_username = $lender_contact->username;
?>
<div class="row Securitysettings_ct">
	<div class="col-sm-6">
		<form action="" name="view_details_set_2_username" id="view_details_set_2_username">
			<div class="form-group mb20"><h4>Change Username</h4></div>
		        <div class="form-group">
		            <label for="contact_username">Username</label>
		            <input type="text" class="form-control" name="contact_username" id="contact_username" placeholder="Username" value="<?php echo $contact_username; ?>" onkeypress="return AvoidSpace(event)">
		            <input type="hidden" class="form-control" name="hide_contact_username" id="hide_contact_username"  value="<?php echo $contact_username; ?>" >  	        
		    </div>
		    <div class="mt45 form-group">
		    	<button type="button" class="theme_btn theme_primary" name="submit_view_username" id="submit_view_username">Save</button>
		    </div>
		</form>
	</div>
	<div class="col-sm-6">
		<div class="form-group mb20"><h4>Change Password</h4></div>
		<form action="" name="view_details_set_2_password" id="view_details_set_2_password">
			<div class="form-group">		        
		        
		            <label for="contact_new_password">New Password</label>
		            <input type="password" class="form-control" id="contact_new_password" name="contact_new_password" placeholder="New Password" >
		         </div>
		        <div class="form-group">
		            <label for="contact_confirm_password">Confirm Password</label>
		            <input type="password" class="form-control" id="contact_confirm_password" name="contact_confirm_password" placeholder="Confirm Password" >
		        </div>
		     
		    <div class="mt45 form-group">
		    	<button type="button" class="theme_btn theme_primary" name="submit_view_password" id="submit_view_password">Save</button>
		    </div>
		</form>
	</div>
	
</div>
