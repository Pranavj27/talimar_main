<?php
$contact_number_option = $this->config->item('contact_number_option');
$email_types_option = $this->config->item('email_types_option');
$STATE_USA = $this->config->item('STATE_USA');

$contact_firstname 		= $personal_view_details[0]->contact_firstname;
$contact_middlename 	= $personal_view_details[0]->contact_middlename;
$contact_lastname 		= $personal_view_details[0]->contact_lastname;
$contact_suffix 		= $personal_view_details[0]->suffix;
$contact_nickname 		= $personal_view_details[0]->contact_suffix;

$contact_address 		= $personal_view_details[0]->contact_address;
$contact_unit 			= $personal_view_details[0]->contact_unit;
$contact_city 			= $personal_view_details[0]->contact_city;
$contact_state 			= $personal_view_details[0]->contact_state;
$contact_zip 			= $personal_view_details[0]->contact_zip;

$same_mailing_address 	= $personal_view_details[0]->same_mailing_address;

$contact_mailing_address = $personal_view_details[0]->contact_mailing_address;
$contact_mailing_unit 	= $personal_view_details[0]->contact_mailing_unit;
$contact_mailing_city 	= $personal_view_details[0]->contact_mailing_city;
$contact_mailing_state 	= $personal_view_details[0]->contact_mailing_state;
$contact_mailing_zip 	= $personal_view_details[0]->contact_mailing_zip;

$contact_phone 			= $personal_view_details[0]->contact_phone;
$primary_option 		= $personal_view_details[0]->primary_option;

$pemail_type 			= $personal_view_details[0]->pemail_type;
$contact_email          = $personal_view_details[0]->contact_email;


?>


<div class="col-md-3">	
	<form name="uploaddprofileimage" id="uploaddprofileimage" method="post" enctype="multipart/form-data">
		<div class="col-sm-12">
			<center>
				<?php if($profile_image_remove == 'Yes'){ ?>
				<button type="button" class="remove_image_profile">x</button>
				<?php } ?>
				<label class="upload_image_set_lab">
					<img src="<?php echo $profile_image; ?>" class="img-responsive img-fluid img-thumbnail">
					<span class="add_image_optionP">+</span>
					<input type="file" name="file" class="form-control form_set_control_file" accept="image/*" >

				</label>
				<button type="submit" class="btn btn-primary upload_image_set">Upload <span class="loading_spin_image loading_image"><img src="<?php echo base_url("assets/css/profile/load_spin1.gif"); ?>"></span></button>
				
			</center>
		</div>
	</form>
</div>
<div class="col-md-9">
	&nbsp;
</div>
<div class="clearfix">&nbsp;</div>
<form action="" name="view_details_set_1" id="view_details_set_1">
	<div class=" Profilesettings_ct">
		<div class="row">

			<div class="col-sm-4">
	        	<div class="form-group ">
	            <label for="contact_first_name">Suffix</label>
	            <input type="text" class="form-control" name="contact_suffix" id="contact_suffix" placeholder="Suffix" value="<?php echo $contact_suffix; ?>">
	            </div>
	        </div>
	        <div class="clearfix"></div>
			<div class="col-md-12">&nbsp;</div>
			
	        <div class="col-sm-4">
	        	<div class="form-group ">
	            <label for="contact_first_name">First Name</label>
	            <input type="text" class="form-control" name="contact_first_name" id="contact_first_name" placeholder="First name" value="<?php echo $contact_firstname; ?>">
	        	</div>
	        </div>

	        <div class="col-sm-4">
	        	<div class="form-group ">
	            <label for="contact_middle_initial">Middle Initial</label>
	            <input type="text" class="form-control" id="contact_middle_initial" name="contact_middle_initial" placeholder="Middle Initial" value="<?php echo $contact_middlename; ?>">
		        </div>
		    </div>

	        <div class="col-sm-4">
	        	<div class="form-group ">
	            <label for="contact_last_name">Last Name</label>
	            <input type="text" class="form-control" id="contact_last_name" name="contact_last_name" placeholder="Last Name" value="<?php echo $contact_lastname; ?>">
		        </div>
		    </div>
	    </div>

	     
	        	<div class="form-group">
	            <label for="contact_nickname">Legal First Name</label>
	            <input type="text" class="form-control" id="contact_nickname" name="contact_nickname" placeholder="Nickname" value="<?php echo $contact_nickname; ?>">
	        </div>
	        
	   

	    <div class="row">
	        <div class="col-sm-6">
	        	<div class="form-group ">
	            <label for="contact_primary_email">E-Mail</label>
	            <input type="text" class="form-control" id="contact_primary_email" name="contact_primary_email" placeholder="Primary E-Mail" readonly="true" value="<?php echo $contact_email; ?>">
	        </div></div>
	        <div class="col-sm-6">
	        	<div class="form-group ">
	            <label for="contact_email_type">E-Mail Type</label>
	            <select class="form-control cstSelect" id="contact_email_type" name="contact_email_type">
	            <?php
	            foreach ($email_types_option as $ETypesId => $ETypes) {
	            	$selectedE = '';
	            	if($pemail_type){
	            		if($pemail_type == $ETypesId){
	            			$selectedE = 'selected';
	            		}
	            	}
	            	?>
	            	<option value="<?php echo $ETypesId; ?>" <?php echo $selectedE; ?> ><?php echo $ETypes; ?></option>
	            	<?php
	            }
	            ?>
	            </select>	
	            </div>            
	        </div>
	    </div>

	    <div class="row">
	        <div class="col-sm-6">
	        	<div class="form-group ">
	            <label for="contact_primary_phone">Primary Phone</label>
	            <input type="text" class="form-control" id="contact_primary_phone" name="contact_primary_phone" placeholder="Primary Phone" value="<?php echo $contact_phone; ?>">
	        </div></div>
	        <div class="col-sm-6">
	        	<div class="form-group ">
	            <label for="contact_phone_type">Phone Type</label>
	            <select class="form-control cstSelect" id="contact_phone_type" name="contact_phone_type">
	            <?php
	            foreach ($contact_number_option as $ETypesId => $ETypes) {
	            	$selected = '';
	            	if($primary_option){
	            		if($primary_option == $ETypesId){
	            			$selected = 'selected';
	            		}
	            	}
	            	?>
	            	<option value="<?php echo $ETypesId; ?>" <?php echo $selected; ?>><?php echo $ETypes; ?></option>
	            	<?php
	            }
	            ?>
	            </select>
	        </div></div>
	    </div>

	    <div class="form-group mt25"><h4>Primary Address</h4></div>

	    <div class="row">
	        <div class="col-sm-6">
	        	<div class="form-group ">
	            <label for="primary_street_address">Street Address</label>
	            <input type="text" class="form-control" id="primary_street_address" name="primary_street_address" placeholder="Street Address" value="<?php echo $contact_address; ?>">
	        </div></div>
	        <div class="col-sm-6">
	        	<div class="form-group ">
	            <label for="primary_unit">Unit #</label>
	            <input type="text" class="form-control" id="primary_unit" name="primary_unit" placeholder="Unit #" value="<?php echo $contact_unit; ?>">
	        </div></div>
	    </div>

	    <div class="row">
	        <div class="col-sm-4">
	        	<div class="form-group ">
	            <label for="primary_city">City</label>
	            <input type="text" class="form-control" id="primary_city" name="primary_city" placeholder="City" value="<?php echo $contact_city; ?>">
	        </div>
	        </div>
	        <div class="col-sm-4">
	        	<div class="form-group ">
	            <label for="primary_state">State</label>
	            <select class="form-control cstSelect" id="primary_state" name="primary_state">
	            <?php
	            foreach ($STATE_USA as $EstateId => $Estate) {
	            	$selected = '';
	            	if($contact_state){
	            		if($contact_state == $EstateId){
	            			$selected = 'selected';
	            		}
	            	}
	            	?>
	            	<option value="<?php echo $EstateId; ?>" <?php echo $selected; ?> ><?php echo $Estate; ?></option>
	            	<?php
	            }
	            ?>
	            </select>	            
	        </div></div>

	        <div class="col-sm-4">
	        	<div class="form-group ">
	            <label for="primary_zip">Zip</label>
	            <input type="text" class="form-control" id="primary_zip" name="primary_zip" placeholder="Zip" value="<?php echo $contact_zip; ?>">
	        </div></div>
	    </div>

	    <div class="form-group checkboxYgap">
	    	 <label class="checkCont">Check if same as above
				  <input type="checkbox"   name="same_as_above_address" id="same_as_above_address">
				  <span class="themeCheckmark"></span>
			</label>
	    		<!-- <label><input type="checkbox" name="same_as_above_address" id="same_as_above_address">  Check if same as above</label> -->
	    </div>

	    <div class="form-group mt25">
	    	<h4>Mailing Address </h4>
	    </div>
	    

	    <div class="row">
	        <div class="col-sm-6">
	        	<div class="form-group ">
	            <label for="mailing_street_address">Street Address</label>
	            <input type="text" class="form-control" id="mailing_street_address" name="mailing_street_address" placeholder="Street Address" value="<?php echo $contact_mailing_address; ?>">
	        </div></div>
	        <div class="col-sm-6">
	        	<div class="form-group ">
	            <label for="mailing_unit">Unit #</label>
	            <input type="text" class="form-control" id="mailing_unit" name="mailing_unit" placeholder="Unit #" value="<?php echo $contact_mailing_unit; ?>">
	        </div></div>
	    </div>

	    <div class="row">
	        <div class="col-sm-4">
	        	<div class="form-group ">
	            <label for="mailing_city">City</label>
	            <input type="text" class="form-control" id="mailing_city" name="mailing_city" placeholder="City" value="<?php echo $contact_mailing_city; ?>">
	        </div></div>
	        <div class="col-sm-4">
	        	<div class="form-group ">
	            <label for="mailing_state">State</label>
	            <select class="form-control cstSelect" id="mailing_state" name="mailing_state">
	            <?php
	            foreach ($STATE_USA as $EstateId => $Estate) {
	            	$selected = '';
	            	if($contact_mailing_state){
	            		if($contact_mailing_state == $EstateId){
	            			$selected = 'selected';
	            		}
	            	}
	            	?>
	            	<option value="<?php echo $EstateId; ?>" <?php echo $selected; ?>><?php echo $Estate; ?></option>
	            	<?php
	            }
	            ?>
	            </select>	          
	        </div></div>
	        <div class="col-sm-4">
	        	<div class="form-group ">
	            <label for="mailing_zip">Zip</label>
	            <input type="text" class="form-control" id="mailing_zip" name="mailing_zip" placeholder="Zip" value="<?php echo $contact_mailing_zip; ?>">
	        </div></div>
	    </div>

	    <div class="form-group text-center mt45">
	    	<input type="hidden" id="old_contact_suffix" value="<?php echo $contact_suffix; ?>">
	    	<input type="hidden" id="old_contact_first_name" value="<?php echo $contact_firstname; ?>">
			<input type="hidden" id="old_contact_middle_initial" value="<?php echo $contact_middlename; ?>">
			<input type="hidden" id="old_contact_last_name" value="<?php echo $contact_lastname; ?>">
			<input type="hidden" id="old_contact_nickname" value="<?php echo $contact_nickname; ?>">
			<input type="hidden" id="old_contact_email_type"  value="<?php echo $pemail_type; ?>">
			<input type="hidden" id="old_contact_primary_phone"  value="<?php echo $contact_phone; ?>">
			<input type="hidden" id="old_contact_phone_type" value="<?php echo $primary_option; ?>">
			<input type="hidden" id="old_primary_street_address" value="<?php echo $contact_address; ?>">
			<input type="hidden" id="old_primary_unit" value="<?php echo $contact_unit; ?>">
			<input type="hidden" id="old_primary_city" value="<?php echo $contact_city; ?>">
			<input type="hidden" id="old_primary_state" value="<?php echo $contact_state; ?>">
			<input type="hidden" id="old_primary_zip" value="<?php echo $contact_zip; ?>">
			<input type="hidden" id="old_mailing_street_address" value="<?php echo $contact_mailing_address; ?>">
			<input type="hidden" id="old_mailing_unit" value="<?php echo $contact_mailing_unit; ?>">
			<input type="hidden" id="old_mailing_city" value="<?php echo $contact_mailing_city; ?>">
			<input type="hidden" id="old_mailing_state" value="<?php echo $contact_mailing_state; ?>">
			<input type="hidden" id="old_mailing_zip" value="<?php echo $contact_mailing_zip; ?>">

	    	<button type="button" class="theme_btn theme_primary" name="submit_view_details_set" id="submit_view_details_set">Save</button>
	    </div>
	</div>
</form>
