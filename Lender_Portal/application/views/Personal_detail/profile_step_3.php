<?php
$TagarrayArr = array();
foreach ($contact_tags_details as $value) {
	if($value->contact_email_blast !=''){
		$TagarrayArr[$value->contact_email_blast] = $value->contact_email_blast;			
	}
}
$notification_arr = array();
?>
<div class=" Notificationsettings_ct">

	 
	    <div class="mb20">
			<small>* Text notifications will be sent to the phone number listed in the Contact Information section. Please note, only mobile phones can be used when selecting text message notifications.</small>
		 
	</div>
	<div class="row">
	<div class="col-sm-6 mx-auto">	
		
		<form action="" name="view_details_set_2_username" id="view_details_set_2_username">
			<div class="checkboxInputWraper">
		        

		        	<?php foreach($notification_setting as $key => $row){ 
						if(in_array($key, $TagarrayArr)){
							$checked = 'checked="checked"';
							$notification_arr[] = $key;
						}else{
							$checked = '';
						} ?>
			            <div class="checkbox_input">
							<label class="checkCont">
								<input type="checkbox" class="form-control" name="notify_by_<?php echo $key;?>" value="<?php echo $key;?>" required="required" <?php echo $checked;?> > 
								<span><?php echo $row;?></span>
								<span class="themeCheckmark"></span>
							</label>
							 
						</div>
					<?php } ?>
		        	        	        
		    </div>
		    <div class="mt45">
		    	<input type="hidden" name="notification_arr" id="notification_arr" value="<?php echo implode(',', $notification_arr); ?>">
		    	<button type="button" class="theme_btn theme_primary" name="submit_view_notification" id="submit_view_notification" onclick="SaveNotificatioVAl(this);">Save</button>
		    </div>
		</form>
	</div>
</div>
</div>


