<?php 
$account_status_option = $this->config->item('account_status_option'); 
$LoanTerm_type = $this->config->item('LoanTerm_type');
$lender_property_type = $this->config->item('lender_property_type');
$yeild_percent = $this->config->item('yeild_percent');
$loan_tye = $this->config->item('loan_tye');
$position_optionn = $this->config->item('position_optionn');
$ltv_option = $this->config->item('ltv_option');
$lender_lend_value = $this->config->item('lender_lend_value');
$lender_ownership_type = $this->config->item('lender_ownership_type');
$loan=array();
$ownership_type=array();
if(!empty($lender_contact->loan)){
	$loan = explode(",", $lender_contact->loan);
}
if(!empty($lender_contact->property_type)){
	$lender_property = explode(",", $lender_contact->property_type);
}
?>
<div class=" Notificationsettings_ct">
	<?php if($this->session->flashdata('success')!=''){  ?>
        <div class="col-md-12" style="margin-bottom: 10px;">
            <div class="alert_success">
             <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
              <strong>Success!</strong> <?php echo $this->session->flashdata('success');?>
            </div>
        </div>
    <?php } ?>
	<form method="POST" action="<?php echo base_url(); ?>Personal_details/contactLenderSurvey" onsubmit="return getValidate()" >
		<div class="row">
            <input type="hidden" name="contact_id" value="<?php echo $lender_contact->contact_id;?>">
            <div class="col-md-5" >
                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Will you invest in a Fractional Interest?</strong></label>
            </div>
            <div class="col-md-3">
                <select name="ownership_type" id="ownership_type" class="form-control" >
                    <option value="">Select One</option>
                    <?php
                    if(!empty($lender_ownership_type)){
                        foreach ($lender_ownership_type as $key => $row) {?>
                            <option value="<?php echo $key; ?>" <?php if(!empty($lender_contact->ownership_type) && $lender_contact->ownership_type==$key){ echo "selected"; }?>><?php echo $row; ?></option>
                        <?php
                        } 
                    } ?>
                </select>
                <spam id="ownership_type_error" style="color: red"></spam>
            </div>
        </div>
        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">
            <div class="col-md-5">
                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>What is your max LTV ratio?</strong></label>
            </div>
            <div class="col-md-3">
                <select name="ltv" id="ltv" class="form-control" >
                    <option value="">Select One</option>
                    <?php
                    if(!empty($ltv_option)){
                        foreach ($ltv_option as $key => $row){?>
                            <option value="<?php echo $key; ?>" <?php if(!empty($lender_contact->ltv) && $lender_contact->ltv==$key){ echo "selected"; }?>><?php echo $row; ?></option>
                        <?php 
                        }
                    } ?>
                </select>
            </div>
        </div>
        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">
            <div class="col-md-5">
                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>What is your maximum loan position?</strong></label>
            </div>
            <div class="col-md-3">
                <select name="position" id="position" class="form-control" >
                    <option value="">Select One</option>
                    <?php
                    if(!empty($position_optionn)){
                        foreach ($position_optionn as $key => $row) {?>
                            <option value="<?php echo $key;?>"<?php if(!empty($lender_contact->position) && $lender_contact->position==$key){ echo "selected"; }?>><?php echo $row; ?></option>
                        <?php 
                        }
                    } ?>
                </select>
            </div>
        </div>
        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">
            <div class="col-md-5">
                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Will you lend on Completion Value *?</strong></label>
            </div>
            <div class="col-md-3">
                <select name="lend_value" id="lend_value" class="form-control" >
                    <option value="">Select One</option>
                    <?php
                    if(!empty($lender_lend_value)){
                        foreach ($lender_lend_value as $key => $row) {?>
                            <option value="<?php echo $key; ?>" <?php if(!empty($lender_contact->lend_value) && $lender_contact->lend_value==$key){ echo "selected"; }?>><?php echo $row; ?></option>
                        <?php 
                        }
                    } ?>
                </select>
            </div>
        </div>
        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">
            <div class="col-md-5">
                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>What is your maximum Loan Term?</strong></label>
            </div>
            <div class="col-md-3">
                <select name="LoanTerm" id="LoanTerm" class="form-control" >
                    <option value="">Select One</option>
                    <?php
                    if(!empty($LoanTerm_type)){
                        foreach ($LoanTerm_type as $key => $row) {?>
                            <option value="<?php echo $key; ?>" <?php if(!empty($lender_contact->LoanTerm) && $lender_contact->LoanTerm==$key){ echo "selected"; }?>><?php echo $row; ?></option>
                        <?php 
                        }
                    } ?>
                </select>
            </div>
        </div>
        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">
            <div class="col-md-5">
                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>What is your maximum investment per Trust Deed?</strong></label>
            </div>
            <div class="col-md-3">
                <input type="text" name="trust_deed" id="trust_deed" class="form-control" value="<?php if(!empty($lender_contact->maximum_income)){ echo '$'.number_format((double)$lender_contact->maximum_income); }else { echo "$0";}?>">
                <?php /*
                <select name="trust_deed" id="trust_deed" class="form-control" >
                    <option value="">Select One</option>
                    <?php
                    $trust_deed = $this->config->item('trust_deed');
                    if(!empty($trust_deed)){
                        foreach ($trust_deed as $key => $row) {?>
                            <option value="<?php echo $key; ?>" <?php if(!empty($lender_contact->maximum_income) && $lender_contact->maximum_income==$key){ echo "selected"; }?>><?php echo $row; ?></option>
                        <?php 
                        }
                    } ?>
                </select>
                */?>
            </div>
        </div>

        <!-- <div class="row">&nbsp;&nbsp;</div> -->
        <div class="row" style="display: none">
            <div class="col-md-5">
                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>How much are you looking to invest in Trust Deeds?</strong></label>
            </div>
             <div class="col-md-3">
                <input type="text" name="looking_invest_per_deed" id="looking_invest_per_deed" class="form-control" value="<?php if(!empty($lender_contact->looking_invest_per_deed)){ echo '$'.number_format((double)$lender_contact->looking_invest_per_deed); }else { echo "$0";}?>">
            </div>
        </div>

        <div class="row">&nbsp;&nbsp;</div>

        <div class="row">
            <div class="col-md-5">
                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>How much are you looking to invest in Trust Deeds?</strong></label>
            </div>
            <div class="col-md-3">
                <input type="text" name="invest_trust_deed" id="invest_trust_deed" class="form-control" value="<?php if(!empty($lender_contact->invest_per_deed)){ echo '$'.number_format((double)$lender_contact->invest_per_deed); }else { echo "$0";}?>">
                <?php /*
                <select name="invest_trust_deed" id="invest_trust_deed" class="form-control" >
                    <option value="">Select One</option>
                    <?php
                    $trust_deed = $this->config->item('trust_deed');
                    if(!empty($trust_deed)){
                        foreach ($trust_deed as $key => $row) {?>
                            <option value="<?php echo $key; ?>" <?php if(!empty($lender_contact->invest_per_deed) && $lender_contact->invest_per_deed==$key){ echo "selected"; }?>><?php echo $row; ?></option>
                        <?php
                        } 
                    } ?>
                </select>
                 */?>
            </div>
        </div>
        
        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">
            <div class="col-md-5">
                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>How many Trust Deeds have you funded?</strong></label>
            </div>
             <div class="col-md-3">
                <input type="text" name="founded_trust" id="founded_trust" class="form-control" value="<?php if(!empty($lender_contact->funded)){ echo number_format((double)$lender_contact->funded); }?>">
                <?php /*
                <select name="founded_trust" id="founded_trust" class="form-control" >
                    <option value="">Select One</option>
                    <?php
                    $founded_trust_founded = $this->config->item('founded_trust_founded');
                    if(!empty($founded_trust_founded)){
                        foreach ($founded_trust_founded as $key => $row) {?>
                            <option value="<?php echo $key; ?>" <?php if(!empty($lender_contact->funded) && $lender_contact->funded==$key){ echo "selected"; }?>><?php echo $row; ?></option>
                        <?php 
                        }
                    } ?>
                </select>
                 */?>
            </div>
        </div>

        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">
            <div class="col-md-5">
                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Select all loan types you would consider:</strong></label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-9">
                <?php 
                if(!empty($loan_tye)){
                    foreach ($loan_tye as $key => $row) {
                        ?>
                        <div class="col-md-3"  style="margin-top: 10px;">
                            <input type="checkbox" name="loan[]"  value="<?php echo $key; ?>"  <?php if(!empty($loan) && in_array($key,$loan)){ echo 'checked';}?>/>&nbsp;<span class="loan_type"><?php echo $row; ?></span>
                        </div>
                    <?php 
                    }
                }?>
            </div>
        </div>
        <div class="row">&nbsp;&nbsp;</div>
        <!-- /////////////////lend compition move to under fractional interest -->
        <div class="row">
            <div class="col-md-5" style="margin-top: 10px;">
                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Select all properties types you would consider:</strong></label>
            </div>
        </div>
        <div class="row">
             <div class="col-md-2"></div>
            <div class="col-md-9">
                <?php 
                if(!empty($lender_property_type)){
                    foreach ($lender_property_type as $key => $row) {
                        ?>
                        <div class="col-md-3"  style="margin-top: 10px;">
                            <input type="checkbox" name="property_type[]"  value="<?php echo $key; ?>" <?php if(!empty($lender_property) && in_array($key,$lender_property)){ echo 'checked';}?>/>&nbsp;<?php echo $row; ?>
                        </div>
                    <?php
                    } 
                }?>
            </div>
        </div>
        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">
            <div class="col-md-12">
                <small>
                    * Completion Value refers to the value of the property once the renovations are complete. Fix and Flip / Hold loans are generally unwritten to the value of the property once the renovations are complete and may included a Renovation Hold Back for future renovation costs. 
                </small>
            </div>
            </br>
            <div class="col-md-12" style="margin-top: 20px;">
                <small>
                    ** Bridge Loan generally refers to loans in which the Borrower does not intend to renovate the property and/or the value is not based upon an After Repair Value. 
                </small>
            </div>
            <div class="col-md-12"  style="margin-top: 20px;">
                <small>
                    *** Fix and Flip / Hold generally refers to loans in which the Borrower intends to renovate the property over the course of the loan term. The loan generally includes a Renovation Hold Back for future renovation costs and the Loan to Value ratio is based upon the Completion Value. 
                </small>
            </div>
        </div>
        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">
        	<div class="col-md-10">
        		<button type="submit"  name="modal_btn" value="save" class="btn btn-success">Update Data</button>
        	</div>
        </div>
    </form>
</div>
<link rel="stylesheet" href="<?php echo base_url("assets/css/lender-survey/lender_survey.css"); ?>">  
<script type="text/javascript" src="<?php echo base_url("assets/js/lender-survey/lender_survey.js");?>"></script>


