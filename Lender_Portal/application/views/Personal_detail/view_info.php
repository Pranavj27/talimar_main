<?php
error_reporting(0);
$phone_type_option 		= $this->config->item('phone_type_option');
$notification_setting 	= $this->config->item('notification_setting');
$page_c_id = $this->uri->segment(2);
?>
<div class="container">
	<div class="row">

		<div class="modal_my_profile_setting bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-lg">		  	
		    <div class="modal-content">
		    	<button type="button" onclick="window.history.back();" class="btn btn-primary pull-right" data-toggle="modal" data-target="#chekmodallnoti" style="margin-right: 5px;">Back</button>
		    	<div class="modal-header-setting">
		    		<h4>My Profile Settings</h4>
		    
			  		<div class="tabsWrap group_link_actions">
					  <a href="<?php echo base_url();?>view_details/1" class="tabLink <?php if($page_c_id == 1){ echo "group_setting_active"; } ?>">Contact Information</a>
					  <a href="<?php echo base_url();?>view_details/2" class="tabLink <?php if($page_c_id == 2){ echo "group_setting_active"; } ?>">Security Settings</a>
					  <a href="<?php echo base_url();?>view_details/3" class="tabLink <?php if($page_c_id == 3){ echo "group_setting_active"; } ?>">Notification Settings</a>
					   <a href="<?php echo base_url();?>view_details/4" class="tabLink <?php if($page_c_id == 4){ echo "group_setting_active"; } ?>">Lender Survey</a>
					</div>
			  	</div>
			  	
		      	<div class="tabsContent">
		      		<?php
					include "profile_step_".$page_c_id.".php";
				?>
		      	</div>

		    </div>
		  </div>
		</div>
			
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url("assets/js/profile/profile.js"); ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/profile/profile.css"); ?>">