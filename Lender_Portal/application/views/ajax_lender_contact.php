<div class="col-md-6 col-lg-6" >
    <div class="form-group">
        <label for="f1-first-name" class="label_mod">Contact Lender Portal</label>
        <select type="text" class="form-control input_mod"  onchange="get_selected_data_option('<?php echo $row_no;?>')" name="contact_id" id="contact_id_<?php echo $row_no;?>" >
            <option value="">Select One</option>
            <?php
            foreach($fetch_all_contact as $row)
            {
            ?>
                <option value="<?php echo $row->contact_id;?>" option_email="<?php echo $row->contact_email;?>" option_phone="<?php echo $row->contact_phone;?>"  ><?php echo $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname; ?></option>
            <?php
            }
            ?>
        </select>
        <input type="hidden" name="contact_row_id[]" id="contact_row_id_<?php echo $row_no;?>" value="">
        <input type="hidden" name="contact_row_email[]" id="contact_row_email_<?php echo $row_no;?>" value="">
        <input type="hidden" name="contact_row_phone[]" id="contact_row_phone_<?php echo $row_no;?>" value="">
    </div>
</div>  