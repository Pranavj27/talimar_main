
<!DOCTYPE html>
 
<html lang="en-US" dir="ltr">
  <head>
    <title>TaliMar Financial Lender Portal</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>/assets/img/favicon1.png">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover" /> 
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
    
<style> 
* {
    font-family: 'Open Sans', sans-serif !important;
}
  .cont {
    float: left;
    width: 100%;
}

.col-6 {
    float: left;
    width: 50%;
}
body.theme-open-up {
    padding: 0;
    margin: 0;
}
.css-1upilqn {
    margin-bottom: 12px;
}
.input-lg input[type], .form-lg input[type] {
    height: 48px;
    padding: 15px 20px;
}
.bg-image {
    height: 100vh;
    float: left;
    width: 100%;
    overflow: hidden;
}
h2 {
    color: #123657 !important;
}
.form-outer {
    width: 60% !important;
    padding: 30px 10vh 3vh 48px;
}

.form-outer label {
        width: 100%;
        float: left;
        color: rgb(4,13,20);
        padding: 2px 0px;
        font-size: 13px;
        font-weight: 700;  
}
.form-outer input[type="text"], input[type="email"], input[type="password"] {
    width: 100%;
    float: left;
    background: rgb(245 248 250);
    border: 1px solid #3379b5 !important;
    font-size: 13px; 
    padding:8px 7px;
    border-radius: 4px;
}
.form-outer input[type="text"]:focus, input[type="email"]:focus, input[type="password"]:focus {
    width: 100%;
    float: left;
    background: #ffffff;
    border: 1px solid #3379b5 !important;
    font-size: 13px; 
    padding:8px 7px;
    border-radius: 4px;
    outline: none;
}
button._1OsoaRGpMCXh9KT8s7wtwm._2GHn41jUsfSSC9HmVWT-eg {
    color: #fff;
    background-color: #3379b5;
    border-color: #3379b5;
    border: 0;
    padding: 8px 37px;
    border-radius: 4px;
    cursor: pointer;
}
.form-div p a { 
    color: #3379b5;
    font-size: 14px;
    text-decoration: none;
    padding: 14px 0px 0px 0px;
    float: left;
    width: 100%;
}

a{
    cursor: pointer;
    font-size: 13px;
    text-decoration: none;
}

p.error {
    color: red;
    font-weight: 600;
    font-size: 13px;
}

p.successs {
    color: green;
    font-weight: 600;
    font-size: 13px;
}

.btn {
    border-width: 0;
    padding: 8px 14px;
    font-size: 13px;
    outline: none !important;
    background-image: none !important;
    filter: none;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
    text-shadow: none;
    border-radius: 4px;
}

.btn-primary {
    color: #fff;
    background-color: #428bca;
    border-color: #357ebd;
}




.form-outer h2 {
    border-bottom: solid 1px #d3d0d0;
    padding-bottom: 16px;
}

.form-div label {
    margin-top: 13px;
}

p.space_b a {
    margin-top: 9px;
}

.bg-image img {
    height: 100vh;
}


@media (max-width: 767px) {

.col-6 {
    float: left;
    width: 100%;
}

.bg-image {
    height: initial !important;
}
.form-outer {
    width: 88%;
    padding: 30px;
}
.bg-image img {
    height: inherit;
}
}

/*03-02-2021*/
.username_errors img {
    width: 24px;
}
.username_errors {
    position: inherit;
    float: right;
    margin-top: -30px;
}

/*03-02-2021*/
</style>


  </head>
  <body class="theme-open-up">
    <div class="cont">
		<div class="row">
			<div class="col-6">
				<div class="bg-image">
					<img src="<?php echo base_url()?>assets/images/TrustDeed.jpg" width="100%">
				</div> 
			</div>
			<div class="col-6">
				<div class="form-outer">
					<h2>Welcome to TaliMar Financial</h2>
                    <form method="POST" action="<?php echo base_url();?>register">
    					<div class="form-div">

                        <?php if($this->session->flashdata('success')!=''){ ?>
                            <p class="successs"><?php echo $this->session->flashdata('success');?></p>
                        <?php } ?>
                        <?php if($this->session->flashdata('error')!=''){ ?>
                            <p class="error"><?php echo $this->session->flashdata('error');?></p>
                        <?php } ?>

                        <?php
                        $readOnly = '';
                        if($lender_contact_id){
                            $readOnly = 'readonly';
                        }
                        ?>

    						<label>First Name:</label>
    						<input type="text" name="fname" placeholder="Enter Your First Name" autocomplete="off" value="<?php echo $contact_firstname; ?>"  required>
    						<label>Last Name:</label>
    						<input type="text" name="lname" placeholder="Enter Your Last Name" autocomplete="off" value="<?php echo $contact_lastname; ?>" >
    						<label>Phone Number:</label>
    						<input type="text" class="phone-format" name="phone" placeholder="Enter Your Phone Number" autocomplete="off" value="<?php echo $contact_phone; ?>" >	
    						<label>E-Mail Address:</label>
    						<input type="email" name="email" placeholder="Enter Your Email Address" autocomplete="off" value="<?php echo $contact_email; ?>" <?php echo $readOnly; ?> required>
    						<label>Username:</label>
    						<input type="text" name="username" placeholder="Enter Your Username" class="username_input" autocomplete="off" required>
                            <div class="username_errors"></div>
    						<label>Password:</label>
    						<input type="password" name="password" placeholder="Enter Your Password" autocomplete="off" required>
    						<p><a></a></p> 
                            <input type="hidden" name="lender_contact_id" value="<?php echo $lender_contact_id; ?>">
                            <input type="hidden" name="ref_base" value="<?php $ref_base; ?>">
    						<button type="submit" name="submit" id="submit" class="_1OsoaRGpMCXh9KT8s7wtwm _2GHn41jUsfSSC9HmVWT-eg submit_register">Register</button>
                            <a class="btn btn-primary" href="<?php echo base_url()?>">Back</a>

                            
    				    </div>
                    </form>
			</div>
			
		</div>
	</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script>
    <script type="text/javascript">
        
        $(document).ready(function(){
          /***phone number format***/
            $(".phone-format").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                  return false;
                }
                var curchr = this.value.length;
                var curval = $(this).val();
                if (curchr == 3 && curval.indexOf("(") <= -1) {
                  $(this).val("(" + curval + ")" + "-");
                } else if (curchr == 4 && curval.indexOf("(") > -1) {
                  $(this).val(curval + ")-");
                } else if (curchr == 5 && curval.indexOf(")") > -1) {
                  $(this).val(curval + "-");
                } else if (curchr == 9) {
                  $(this).val(curval + "-");
                  $(this).attr('maxlength', '14');
                }
            });

        });
    </script>
	</body>
</html>
