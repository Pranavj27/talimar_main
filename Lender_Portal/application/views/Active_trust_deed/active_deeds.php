<?php
$loan_type 			= $this->config->item('loan_type_option');
$position_option 	= $this->config->item('position_option');
$property_type 		= $this->config->item('property_modal_property_type');
$loan_status_option 		= $this->config->item('loan_status_option');
$servicing_agent_option         = $this->config->item('servicing_agent_option');
$trustdeed_filter_option 		= $this->config->item('trustdeed_filter_option');
$loan_payment_status        = $this->config->item('loan_payment_status');

?>

	<div class="data-table-area ctp_active_deeds">
        <div class="container">
		
		
			<div class="row">
             <div class="col-xs-12 col-md-12 col-xl-12">
                <a href="https://talimarfinancial.com/talimar-income-fund-i/" target="_blank"> 
                    <img src="<?php echo base_url('../assets/Banners/5.png'); ?>" style="height: auto; margin-left: auto;margin-right: auto; display:block; ">
                </a>
            </div>
            <!-- <div class="clearfix"></div>
			<br>
            <div class="col-xs-12">
                <h3 class="heading_title_script">Trust Deed Portfolio</h3> 
            </div> -->
        
			</div>
            <div class="row">&nbsp;</div>
            <div class="row">
                <form id="trustdeed_form" method="post" action="<?php echo base_url();?>active_trust_deeds">

                    <div class="col-md-3 trust_deeds_Account" >
                        <label> Account Name:</label>
                        <select name="account_option" class="form-control" onchange="account_value(this.value);">
                            <option value="0">Select All</option>
                            <?php foreach($all_investor_name as $key => $rows){?>
                                <option value="<?php echo $rows->id;?>" <?php if($account_option_val == $rows->id){echo 'selected'; }?>><?php echo $rows->name;?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <!--
                        Description : staus value set proper
                        Author      : Bitcot
                        Created     : 18-03-2021
                        Modified    :
                    -->
                   <div class="col-md-2 trust_deedsLoan" >
                        <label>Loan Status:</label>
                        <select name="status_option" class="form-control" onchange="status_value(this.value);">
                            <?php
                            unset($loan_status_option[3]);
                            unset($loan_status_option[4]);
                            unset($loan_status_option[5]);
                             foreach($loan_status_option as $key => $row){?>
                                <option value="<?php echo $key;?>" <?php if($status_option_val == $key){echo 'Selected';}?>><?php echo $row;?></option>
                            <?php } ?>
                        </select>
                    </div>

                                        
                </form>
            </div>
            <div class="row">&nbsp;<br></div>

            <!------------------- PENDING SECTION ------------------------>
           
            <div class="row">
                <div class="col-md-12">
                        <div class="table-responsive active_trust_deeds_table">
                            <table id="trustdeed_table222222" class="table table-striped table-border">
                                <thead class="Bgcolor_code_f2f2f2">
                                    <tr>
                                        <th>Location</th>
                                        <th>Position</th>
                                        <th>Loan Amount</th>
                                        <th>$ Ownership</th>                                  
                                        <th>% Ownership</th>
                                        <th>Lender Rate</th>
                                        <th>Payment</th>
                                        <th>Loan Status</th>
                                       <!-- <th>Trust Deed<br>Status</th>-->
                                        <th>Payment Status</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php
								$count = 0;
								$loan_amount = 0;
								$total_payment = 0;
                                $Ownership = 0;
                                $perOwnership = 0;
                                $percent_yield = 0;

								foreach($active_trust_deed_schedule as $row){
                                    $c_loan_amount = 0;
                                    $c_investment = 0;
                                    $c_lender_payment = 0;
                                    $c_percent_yield = 0;
                                    if($row['loan_amount']){
                                        $c_loan_amount = $row['loan_amount'];
                                    }                                    
                                    if($row['lender_payment']){
                                        $c_lender_payment = $row['lender_payment'];
                                    }
                                    if($row['investment']){
                                        $c_investment = $row['investment'];
                                    }
                                    if($row['percent_yield']){
                                        $c_percent_yield = $row['percent_yield'];
                                    }

									$count++;
									$loan_amount += $c_loan_amount;
                                    $total_payment += $c_lender_payment;
                                    $Ownership += $c_investment;

                                        if($row['loan_status'] == '3'){

                                            $text = 'Paid Off';
                                            $gotolink = '#'; 

                                        }else{

                                            if($row['nfiles_status'] == '1'){
                                               // $class='black_color';
                                                $text='Active';
                                            }else{

                                                $text='Pending';
                                                //$class='red_color';
                                            }

                                            $gotolink = base_url().'trust_deed_details/'.$row['talimar_loan']; 
                                        }

                                        if($row['payment_status'] == '1'){

                                            $Latetext = 'Current';
                                        }elseif($row['payment_status'] == '2' || $row['payment_status'] == '3'){
                                            $Latetext = 'Late';
                                        }else{
                                            $Latetext = '';
                                        }

                                        if($row['unit'] != ''){

                                            $fulladdress = $row['property_address'].' '.$row['unit'].'; '.$row['city'].', '.$row['state'].' '.$row['zip'];
                                        }else{
                                            $fulladdress = $row['property_address'].'; '.$row['city'].', '.$row['state'].' '.$row['zip'];
                                        }

                                        $percent_ownership = 0;
                                        if($c_loan_amount > 0){


                                            $percent_ownership = ($c_investment/$c_loan_amount)*100;
                                        }
                                           

                                        $percent_ownership = ($row['investment']/$row['loan_amount'])*100;

                                        $perOwnership +=  $percent_ownership;
                                        $percent_yield +=  $c_percent_yield;
								?>
									<tr>
                                        <td class="width_30_per"><a href="<?php echo $gotolink;?>"><?php echo $fulladdress;?></a></td>
                                        <td ><?php echo $position_option[$row['position']];?></td>
                                        <td >$<?php echo number_format($c_loan_amount);?></td>
                                        <td >$<?php echo number_format($c_investment);?></td>
                                        <td ><?php echo number_format($percent_ownership,2);?>%</td>
                                        <td ><?php echo number_format($c_percent_yield,2);?>%</td>
                                        <td >$<?php echo number_format($c_lender_payment,2);?></td>
                                        <td ><?php echo $loan_status_option[$row['loan_status']];?></td>
                                        <!--<td style="width:10%"><?php //echo $text;?></td>-->
                                        <td ><?php echo $Latetext;?></td>
                                    </tr>
								<?php }  ?>
								</tbody>
								
                                <tfoot class="Bgcolor_code_f2f2f2">
                                    <tr>
                                        <th>Total: <?php echo $count;?></th>
                                        
                                        <th></th>
                                        <th>$<?php echo number_format($loan_amount);?></th>
                                        <th>$<?php echo number_format($Ownership);?></th>
                                        <th></th>
                                        <th></th>
                                        <th>$<?php echo number_format($total_payment,2);?></th>
                                        <th></th>
                                        <!--<th></th>-->
                                        <th></th>
                                        
                                    </tr>
									<tr>
                                        <th>Average:</th>
                                        
                                        <th></th>
                                        <th>$<?php 
                                            if($count > 0){
                                                echo number_format($loan_amount/$count);
                                            }else{
                                                echo number_format(0);
                                            }                                            
                                            ?>
                                        </th>
                                        <th>$<?php 
                                            if($count > 0){
                                                echo number_format($Ownership/$count);
                                            }else{
                                                echo number_format(0);
                                            }                                            
                                            ?>                                                
                                        </th>
                                        <th>
                                            <?php 
                                            if($loan_amount > 0){
                                                echo number_format($Ownership/$loan_amount*100,2);
                                            }else{
                                                echo number_format(0);
                                            }                                            
                                            ?>% *
                                        </th>
                                        <th>
                                            <?php 
                                            if($Ownership > 0){
                                                echo number_format(($total_payment*12)/$Ownership*100,2);
                                            }else{
                                                echo number_format(0);
                                            }                                            
                                            ?>% **                                            
                                        </th>
                                        <th>$<?php 
                                            if($count > 0){
                                                echo number_format($total_payment/$count,2);
                                            }else{
                                                echo number_format(0);
                                            }                                            
                                            ?>
                                            </th>
                                        <th></th>
                                        <!--<th></th>-->
                                        <th></th>
                                        
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    
                </div>
            </div>

            

            <div class="row"><br></div>
            <div class="row">
                <div class="col-md-12">
                    
                    <span class="">* Represents weighted average of Total $ Ownership / Total Loan Amount. </span>
                    <br>
                    <span class="">** Represents weighted average calculated by total monthly payments / total active balance.</span>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        
        function status_value(that){

            $('form#trustdeed_form').submit();
        }

        function account_value(that){

            $('form#trustdeed_form').submit();
        }

    </script>