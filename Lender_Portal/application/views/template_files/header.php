<?php
$contact_id = $this->session->userdata('p_user_id');
$fetchimg = $this->User_model->query("SELECT imgurl FROM `lender_contact_type` WHERE `contact_id`= '".$contact_id."' ");
$fetchimg = $fetchimg->result();
$imgurl = $fetchimg[0]->imgurl;
if($imgurl != ''){

    $Imgpath = $imgurl;
}else{
    $Imgpath = base_url().'assets/img/post/avatar0.jpg';
}

$investor = "SELECT DISTINCT lender_contact_type.mortgageActivate, lender_contact.lender_id, investor.amountInvested FROM lender_contact JOIN lender_contact_type ON lender_contact.contact_id = lender_contact_type.contact_id JOIN investor ON lender_contact.lender_id = investor.id WHERE lender_contact.contact_id = '".$contact_id."' AND lender_contact.link_to_lender = '2'  ORDER BY lender_contact.lender_id ASC ";
$investor = $this->User_model->query($investor);

if($investor->num_rows() > 0)
{
    $investor  = $investor->row();
    
    $this->session->set_userdata('lender_id', $investor->lender_id);
    $this->session->set_userdata('amountInvested' , $investor->amountInvested);
    $this->session->set_userdata('mortgageActivate' , $investor->mortgageActivate);    
}

?>
<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>TaliMar Financial | Lender Portal</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/img/favicon1.png">
    <!-- Google Fonts
		============================================ -->
    <!--<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">-->

	<link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">

    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.transitions.css">
    <!-- meanmenu CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/meanmenu/meanmenu.min.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/normalize.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- jvectormap CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jvectormap/jquery-jvectormap-2.0.3.css">
    <!-- notika icon CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/notika-custom-icon.css">
    <!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/wave/waves.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/style.css">
    <!-- responsive CSS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/mobile-responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/vendor/modernizr-2.8.3.min.js"></script>
	
	<!-- bootstrap select CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-select/bootstrap-select.css">
    <!-- datapicker CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/datapicker/datepicker3.css">
	<!-- main CSS Chosen
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/chosen/chosen.css">
	 <!-- Data Table JS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.dataTables.min.css">
	
	<!-- dialog CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/dialog/sweetalert2.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/dialog/dialog.css">

    <!-- stepper form css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/stepper_form.css">
   
    <style type="text/css">
        ul>li.active>a{
            border-radius: 0px!important;
            margin-top: 1px!important;
            margin-bottom: 0px!important;
        }
        .nav-tabs>li.active>a:hover{
            color: #fff!important;
        }
    </style>
</head>


<body>
   
    <!-- Start Header Top Area -->
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo-area">
                        <a href="<?php echo base_url();?>dashboard"><img src="<?php echo base_url();?>assets/img/logo/logo-big.png" alt="TaliMar Financial" style="max-width:45% !important;"/></a>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    
                </div>
            </div>
        </div>
    </div>
    <!-- End Header Top Area -->
    <!-- Mobile Menu start -->
    <div class="header-top-menu talimar_profile_mobile_ct">
        <ul class="nav navbar-nav notika-top-nav">

            <?php
              $loggedinUser  = $this->session->userdata('p_user_fname').' '.$this->session->userdata('p_user_lname');
              if($loggedinUser != ''){
                $loggedinUserVAl = $loggedinUser;
              }else{
                $loggedinUserVAl = 'Trust Deed';
              }
            ?>
           
            <li class="nav-item"><a id="username_bk" href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle" style="color:#333;">
                <img src="<?php echo $Imgpath; ?>" style="border-radius: 50%;width: 46px;height: 45px;">&nbsp; <?php echo $loggedinUserVAl;?> </a> 
                <div role="menu" class="dropdown-menu message-dd chat-dd animated zoomIn" style="left: 0px !important;padding: 6px;width: 100%; ">
                    <div class="hd-message-info">
                           
                       <a href="<?php echo base_url();?>view_details">
                            <div class="hd-message-sn">
                                
                                <div class="hd-mg-ctn">
                                    <h3><i class="notika-icon notika-support"></i> &nbsp; My Profile</h3>                                                 
                                </div>
                            </div>
                        </a>
                        <a href="<?php echo base_url();?>view_details/4">
                            <div class="hd-message-sn">
                                
                                <div class="hd-mg-ctn">
                                    <h3><i class="notika-icon notika-home"></i> &nbsp; Lender Survey</h3>                                                 
                                </div>
                            </div>
                        </a>
                        <a href="<?php echo base_url()?>logout">
                            <div class="hd-message-sn">                                   
                                <div class="hd-mg-ctn">
                                    <h3><i class="notika-icon notika-next"></i> &nbsp; Log Out</h3>                                                 
                                </div>
                            </div>
                        </a>
                        
                    </div>
                    
                </div>
            </li>
        </ul>
    </div>

    <div class="mobile-menu-area talimar_mobile_ct">
        <div class="container">
            <div class="row">
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <div class="mobile-menu">

                        <nav id="dropdown">


                            <ul class="mobile-menu-nav">
                                <li class="<?php echo ($page=='dashboard'?'active':''); ?>">
                                    <a href="<?php echo base_url();?>dashboard">Dashboard</a>
                                </li>
                                <li class="<?php echo ($page=='trustDeedPortfolio'?'active':''); ?>">
                                    <a href="<?php echo base_url();?>active_trust_deeds">Trust Deed Portfolio</a>
                                </li>
                                <li class="<?php echo ($page=='availableTrustDeeds'?'active':''); ?>">
                                    <a href="<?php echo base_url();?>trust_deed">Available Trust Deeds</a>                      
                                </li>
                                <!-- <li class="<?php //echo ($page=='settings'?'active':''); ?>">
                                    <a href="<?php //echo base_url();?>LenderUsers">Lender Account</a>                         
                                </li>   -->
                                 <?php if($this->session->userdata('mortgageActivate') != '' && $this->session->userdata('mortgageActivate') != 'no'){ echo $this->session->userdata('amountInvested'); ?>
                                <li class="<?php echo ($page=='mortgageFund'?'active':''); ?>">
                                    <a href="<?php echo base_url();?>Available_deed/mortgageFund">Mortgage Fund</a>
                                </li>
                                <?php } else { ?>
                                <li class="<?php echo ($page=='mortgageFund'?'active':''); ?>" onclick="alert('The Mortgage Fund tab is available to investors in the mortgage fund. To learn more about the mortgage fund, please contact Investor Relations at (858) 242-4900')">
                                    <a href="javaScript:void(0)">Mortgage Fund</a>
                                </li>
                                <?php } ?>
                                <li class="<?php echo ($page=='accounts'?'active':''); ?>">
                                    <a href="<?php echo base_url();?>accounts">Account Settings</a>                         
                                </li>                                
                            </ul>
                        </nav>
						
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu end -->
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                        
                        <li class="<?php echo ($page=='dashboard'?'active':''); ?>">
							<a href="<?php echo base_url();?>dashboard">Dashboard</a>
                        </li>
                        <li class="<?php echo ($page=='trustDeedPortfolio'?'active':''); ?>">
							<a href="<?php echo base_url();?>active_trust_deeds">Trust Deed Portfolio</a>
                        </li>
                        <li class="<?php echo ($page=='availableTrustDeeds'?'active':''); ?>">
							<a href="<?php echo base_url();?>trust_deed">Available Trust Deeds</a>						
						</li>
                         <?php if($this->session->userdata('mortgageActivate') != '' && $this->session->userdata('mortgageActivate') != 'no'){ echo $this->session->userdata('amountInvested'); ?>
                        <li class="<?php echo ($page=='mortgageFund'?'active':''); ?>">
                            <a href="<?php echo base_url();?>Available_deed/mortgageFund">Mortgage Fund</a>
                        </li>
                        <?php } else { ?>
                        <li class="<?php echo ($page=='mortgageFund'?'active':''); ?>" onclick="alert('The Mortgage Fund tab is available to investors in the mortgage fund. To learn more about the mortgage fund, please contact Investor Relations at (858) 242-4900')">
                            <a href="javaScript:void(0)">Mortgage Fund</a>
                        </li>
                        <?php } ?>
						<li class="<?php echo ($page=='accounts'?'active':''); ?>">
							<a href="<?php echo base_url();?>accounts">Account Settings</a>							
						</li>
                        <!-- <li class="<?php echo ($page=='settings'?'active':''); ?>">
                                    <a href="<?php //echo base_url();?>LenderUsers">Lender Account</a>                         
                                </li> -->
						<!--<li>
							<a href="<?php echo base_url();?>view_details">Personal Settings</a>														
						</li>-->

                    </ul>
					<div class="header-top-menu">
                        <ul class="nav navbar-nav notika-top-nav">

                            <?php
                              $loggedinUser  = $this->session->userdata('p_user_fname').' '.$this->session->userdata('p_user_lname');
                              if($loggedinUser != ''){
                                $loggedinUserVAl = $loggedinUser;
                              }else{
                                $loggedinUserVAl = 'Trust Deed';
                              }
                            ?>
                           
                            <li class="nav-item"><a id="username_bk" href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle" style="color:#333;">
                                <img src="<?php echo $Imgpath; ?>" style="border-radius: 50%;width: 29px;height: 29px;">&nbsp; <?php echo $loggedinUserVAl;?> </a> 
                                <div role="menu" class="dropdown-menu message-dd chat-dd animated zoomIn" style="left: 0px !important;padding: 6px;width: 180px !important; ">
                                    <div class="hd-message-info">
                                           
                                       <a href="<?php echo base_url();?>view_details">
                                            <div class="hd-message-sn">
												
                                                <div class="hd-mg-ctn">
                                                    <h3><i class="notika-icon notika-support"></i> &nbsp; My Profile</h3>                                                 
                                                </div>
                                            </div>
                                        </a>
                                        <a href="<?php echo base_url();?>view_details/4">
                                            <div class="hd-message-sn">
                                                
                                                <div class="hd-mg-ctn">
                                                    <h3><i class="notika-icon notika-bar-chart"></i> &nbsp; Lender Survey</h3>                                                 
                                                </div>
                                            </div>
                                        </a>
										<a href="<?php echo base_url()?>logout">
                                            <div class="hd-message-sn">                                   
                                                <div class="hd-mg-ctn">
                                                    <h3><i class="notika-icon notika-next"></i> &nbsp; Log Out</h3>                                                 
                                                </div>
                                            </div>
                                        </a>
                                        
                                    </div>
                                    
                                </div>
                            </li>
                        </ul>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- Main Menu area End-->