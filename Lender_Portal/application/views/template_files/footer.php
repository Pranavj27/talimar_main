  <!-- Start Footer area-->
    <div class="footer-copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-copy-right">
                        <p>Copyright © <?php echo date('Y'); ?>. All rights reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer area-->
    <!-- jquery
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/jquery-price-slider.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/jquery.scrollUp.min.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/meanmenu/jquery.meanmenu.js"></script>
    <!-- counterup JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/counterup/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/counterup/waypoints.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/counterup/counterup-active.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- jvectormap JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="<?php echo base_url();?>assets/js/jvectormap/jvectormap-active.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/sparkline/jquery.sparkline.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/sparkline/sparkline-active.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/js/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url();?>assets/js/flot/curvedLines.js"></script>
    <script src="<?php echo base_url();?>assets/js/flot/flot-active.js"></script>
    <!-- knob JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/knob/jquery.knob.js"></script>
    <script src="<?php echo base_url();?>assets/js/knob/jquery.appear.js"></script>
    <script src="<?php echo base_url();?>assets/js/knob/knob-active.js"></script>
	
	 <!-- Input Mask JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/jasny-bootstrap.min.js"></script>
    <!-- icheck JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/icheck/icheck.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/icheck/icheck-active.js"></script>
    
    <!-- datapicker JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>assets/js/datapicker/datepicker-active.js"></script>
	 <!--  chosen JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/chosen/chosen.jquery.js"></script>
	
    <!--  wave JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/wave/waves.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/wave/wave-active.js"></script>
    <!--  todo JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/todo/jquery.todo.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
	<!--  Chat JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/chat/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/chat/jquery.chat.js"></script>
	<script src="<?php echo base_url();?>assets/js/dialog/sweetalert2.min.js"></script> 
	<script src="<?php echo base_url();?>assets/js/dialog/dialog-active.js"></script>
    <!-- main JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
	<!-- Data Table JS
		============================================ -->
     <script src="<?php echo base_url();?>assets/js/data-table/jquery.dataTables.min.js"></script>
	
    <script src="<?php echo base_url();?>assets/js/data-table/data-table-act.js"></script>
	<!-- tawk chat JS
		============================================ -->
    <!--<script src="<?php echo base_url();?>assets/js/tawk-chat.js"></script>-->
</body>

</html>
<script>

//......................edit account...ach_disbrusement start...//
$(document).ready(function(){

	$('#table_contactlist').DataTable({
		"searching":false,
		// "pageLength":25,
		 "bLengthChange":false,
		 "aaSorting": [[6, 'desc']]

	});	

	$('#trustdeed_table').DataTable({
		"searching":false,
		// "pageLength":25,
		"bLengthChange":false,

	});	 

	
});



$(document).ready(function(){

$('.form-single').removeClass('form-single nk-int-st widget-form');

	});	 
	
	


$(document).ready(function(){


	var check = $('input#check_disbrusement_val').val();
	if(check == '1'){
		$('input#ach_disbrusement').attr('disabled',true);
		$('input#ach_disbrusement').val('0');
	}else{
		$('input#ach_disbrusement').attr('disabled',false);
	}
	
	var ach = $('input#ach_disbrusement_val').val();
	if(ach == '1'){
		$('input#check_disbrusement').attr('disabled',true);
		$('input#check_disbrusement').val('0');
	}else{
		$('input#check_disbrusement').attr('disabled',false);
	}
	
});

$(document).ready(function(){
    var investor_type  = $("#investor_type").val();
	
if(investor_type == '1')
	{
		$('#entity_state').hide();
		$('#self').hide();
		$('#s_e_n').hide();
		
	}else if(investor_type == '2'){


		$('#s_e_n').show();
		$('#entity_state').show();
		$('#self').hide();

	}
	else if(investor_type == '6'){


		$('#s_e_n').show();
		$('#entity_state').show();
		$('#self').hide();

	}


	else if(investor_type == '7'){


		$('#s_e_n').show();
		$('#entity_state').show();
		$('#self').hide();

	}
	else if(investor_type == '3'){


		$('#s_e_n').hide();
		$('#entity_state').hide();
		$('#self').show();

	}

	else if(investor_type == '4'){


		$('#s_e_n').hide();
		$('#entity_state').hide();
		$('#self').hide();

	}


	else if(investor_type == '5'){


		$('#s_e_n').hide();
		$('#entity_state').hide();
		$('#self').hide();

	}else{
		
		$('#s_e_n').show();
		$('#entity_state').show();
		$('#self').show();

	}

	// if(investor_type == '2' )
	// {
	// 	$('.contact_title input').prop("readonly", false);
	// 	$('#investor_type_based input').prop("readonly", false);
	// 	$('#investor_type_based select').attr("disabled", false);
		
	// }
	// else if(investor_type == '1' || investor_type == '3')
	// {
	// 	$('.contact_title input').prop("readonly", true);
	// 	$('#investor_type_based input').prop("readonly", true);
	// 	$('#investor_type_based input').val('');
	// 	$('#investor_type_based select').val('');
	// 	// $('#investor_type_based input').prop("readonly", true);
	// 	$('#investor_type_based select').attr("disabled","disabled");
	// }
	// else
	// {
	// 	$('.contact_title input').prop("readonly", false);
	// 	$('#investor_type_based input').val('');
	// 	$('#investor_type_based input').prop("readonly", true);
	// 	$('#investor_type_based select').val('');
	// 	// $('#investor_type_based input').prop("readonly", true);
	// 	$('#investor_type_based select').attr("disabled","disabled");
	// }
	// if(investor_type == '3'){
	// 	$('.lender_3_otpn').show();
	// 	$('#investor_type_based').hide();
	// 	$('#self').show();
		
	// }else{
	// 	$('.lender_3_otpn').hide();
	// 	$('#investor_type_based').show();
	// 	$('#self').hide();
	// 	// $('input[name=custodian]').val();
		
	// }
	
	
	// //for Self Directed IRA Name,Custodian,Account...
	// if(investor_type == '1' || investor_type == '2'){
		
	// 	$('input#directed_IRA_NAME').attr('readonly',true);
	// 	$('input#custodian').attr('readonly',true);
	// 	$('input#self_dir_account').attr('readonly',true);
		
	// }else{
		
	// 	$('input#directed_IRA_NAME').attr('readonly',false);
	// 	$('input#custodian').attr('readonly',false);
	// 	$('input#self_dir_account').attr('readonly',false);
		
	// }
});


// $('#investor_type').change(function(){ 
//  var investor_type  = $("#investor_type").val();
// 	if(investor_type == '2' )
// 	{
// 		$('.contact_title input').prop("readonly", false);
// 		$('#investor_type_based input').prop("readonly", false);
// 		$('#investor_type_based select').attr("disabled", false);
// 		$('input[name=entity_name]').val('<?php echo isset($fetch_data_account[0]->entity_name) ? $fetch_data_account[0]->entity_name: '';?>');
// 		$('select[name=entity_type]').val('<?php echo isset($fetch_data_account[0]->entity_type)? $fetch_data_account[0]->entity_name:'' ;?>');
// 		$('select[name=entity_res_state]').val('<?php echo isset($fetch_data_account[0]->entity_res_state) ? $fetch_data_account[0]->entity_name : '';?>');
		
// 	}
// 	else if(investor_type == '1'|| investor_type == '3')
// 	{
// 		$('.contact_title input').prop("readonly", true);
// 		$('#investor_type_based input').val('');
// 		$('#investor_type_based input').prop("readonly", true);
// 		$('#investor_type_based select').val('');
// 		// $('#investor_type_based input').prop("readonly", true);
// 		$('#investor_type_based select').attr("disabled","disabled");
// 	}
// 	else
// 	{
// 		$('.contact_title input').prop("readonly", false);
// 		$('#investor_type_based input').val('');
// 		$('#investor_type_based input').prop("readonly", true);
// 		$('#investor_type_based select').val('');
// 		// $('#investor_type_based input').prop("readonly", true);
// 		$('#investor_type_based select').attr("disabled","disabled");
// 	}
	
// 	if(investor_type == '3'){
// 		$('.lender_3_otpn').show();
// 		$('#investor_type_based').hide();
// 		$('#self').show();
// 	}else{
// 		$('.lender_3_otpn').hide();
// 		$('#investor_type_based').show();
// 		$('#self').hide();

	
		
// 	}
	
	
// 	//for Self Directed IRA Name,Custodian,Account...
// 	if(investor_type == '1' || investor_type == '2'){
		
// 		$('input#directed_IRA_NAME').attr('readonly',true);
// 		$('input#custodian').attr('readonly',true);
// 		$('input#self_dir_account').attr('readonly',true);
		
// 	}else{
		
// 		$('input#directed_IRA_NAME').attr('readonly',false);
// 		$('input#custodian').attr('readonly',false);
// 		$('input#self_dir_account').attr('readonly',false);
		
// 	}
// });


$('#investor_type').change(function(){ 
 var investor_type  = $("#investor_type").val();
	if(investor_type == '1')
	{
		$('#entity_state').hide();
		$('#self').hide();
		$('#s_e_n').hide();
		
	}else if(investor_type == '2'){


		$('#s_e_n').show();
		$('#entity_state').show();
		$('#self').hide();

	}
	else if(investor_type == '6'){


		$('#s_e_n').show();
		$('#entity_state').show();
		$('#self').hide();

	}


	else if(investor_type == '7'){


		$('#s_e_n').show();
		$('#entity_state').show();
		$('#self').hide();

	}
	else if(investor_type == '3'){


		$('#s_e_n').hide();
		$('#entity_state').hide();
		$('#self').show();

	}

	else if(investor_type == '4'){


		$('#s_e_n').hide();
		$('#entity_state').hide();
		$('#self').hide();

	}


	else if(investor_type == '5'){


		$('#s_e_n').hide();
		$('#entity_state').hide();
		$('#self').hide();

	}else{
		
		$('#s_e_n').show();
		$('#entity_state').show();
		$('#self').show();

	}
	// if(investor_type == '3'){
	// 	$('.lender_3_otpn').show();
	// 	$('#investor_type_based').hide();
	// 	$('#self').show();
	// }else{
	// 	$('.lender_3_otpn').hide();
	// 	$('#investor_type_based').show();
	// 	$('#self').hide();

	
		
	// }
	
	
	
});



function show_hide_selectbox(vall){
  var value2=vall;
 
  if(value2=='1'){
  	$('#bank_details').css('display','block');
  	$('#check_hide_div').css('display','none'); 
   }else if(value2=='2'){
	$('#check_hide_div').css('display','block');
 	$('#bank_details').css('display','none'); 
	}
 
 }


 $(document).ready(function(){
	
	
if($('#samee_mailing_chkbx').is(':checked')) {
		
	var street_address 	= $('#directed_IRA_NAME').val();
	var unit 			= $('#custodian').val();
	var city 			= $('input[name="City"]').val();
	var state 			= $('input[name="State"]').val();
	var zip 			= $('input[name="Zip"]').val();
	
	 $('input[name="Mailing_add"]').val(street_address);
	 $('input[name="Mailing_unit"]').val(unit);
     $('input[name="Mailing_city"]').val(city);
	 $('input[name="Mailing_state"]').val(state);
	 $('input[name="Mailing_zip"]').val(zip);
	 $('#same_mailing_addresss').val('1');
	
}else{
	
	
	 $('input[name="Mailing_add"]').val('');
	 $('input[name="Mailing_unit"]').val('');
     $('input[name="Mailing_city"]').val('');
	 $('input[name="Mailing_state"]').val('');
	 $('input[name="Mailing_zip"]').val('');
	 $('#same_mailing_addresss').val('0');
}
  var val=$('#ach_and_check').val();
  show_hide_selectbox(val);



   var check='<?php echo $this->uri->segment(2);?>';

   if(check !=''){

   $('input[name="investor_name"]').attr('readonly',true);
   $('input[name="tax_id"]').attr('readonly',true);
   $('input[name="entity_name"]').attr('readonly',true);
   $('input[name="state_entity_no"]').attr('readonly',true);
   $('input[name="address"]').attr('readonly',true);
   $('input[name="unit"]').attr('readonly',true);
   $('input[name="city"]').attr('readonly',true);
   $('input[name="zip"]').attr('readonly',true);
   $('input[name="vesting"]').attr('readonly',true);
   $('input[name="bank_name"]').attr('readonly',true);
   $('input[name="account_number"]').attr('readonly',true);
   $('input[name="routing_number"]').attr('readonly',true);
   $('input[name="mail_address"]').attr('readonly',true);
   $('input[name="mail_unit"]').attr('readonly',true);
   $('input[name="mail_city"]').attr('readonly',true);
   $('input[name="mail_zip"]').attr('readonly',true);
   $('select[name="mail_state"]').css({"pointer-events":"none","cursor":"not-allowed","background-color":"#e9e9e9"});
   $('select[name="fci_acct_service"]').css({"pointer-events":"none","cursor":"not-allowed","background-color":"#e9e9e9"});
   $('select[name="del_acct_service"]').css({"pointer-events":"none","cursor":"not-allowed","background-color":"#e9e9e9"});
   $('select[name="state"]').css({"pointer-events":"none","cursor":"not-allowed","background-color":"#e9e9e9"});
   $('select[name="entity_type"]').css({"pointer-events":"none","cursor":"not-allowed","background-color":"#e9e9e9"});
   $('select[name="entity_res_state"]').css({"pointer-events":"none","cursor":"not-allowed","background-color":"#e9e9e9"});
   $('select[name="investor_type"]').css({"pointer-events":"none","cursor":"not-allowed","background-color":"#e9e9e9"});
   $('select[name="ach_and_check_drop"]').css({"pointer-events":"none","cursor":"not-allowed","background-color":"#e9e9e9"});

     }
});



  function lock_page(th){

   $('input[name="investor_name"]').attr('readonly',false);
   $('input[name="tax_id"]').attr('readonly',false);
   $('input[name="entity_name"]').attr('readonly',false);
   $('input[name="state_entity_no"]').attr('readonly',false);
   $('input[name="address"]').attr('readonly',false);
   $('input[name="unit"]').attr('readonly',false);
   $('input[name="city"]').attr('readonly',false);
   $('input[name="zip"]').attr('readonly',false);
   $('input[name="vesting"]').attr('readonly',false);
   $('input[name="bank_name"]').attr('readonly',false);
   $('input[name="account_number"]').attr('readonly',false);
   $('input[name="routing_number"]').attr('readonly',false);
   $('input[name="mail_address"]').attr('readonly',false);
   $('input[name="mail_unit"]').attr('readonly',false);
   $('input[name="mail_city"]').attr('readonly',false);
   $('input[name="mail_zip"]').attr('readonly',false);
   $('select[name="mail_state"]').css({"pointer-events":"","cursor":"","background-color":""});
   $('select[name="fci_acct_service"]').css({"pointer-events":"","cursor":"","background-color":""});
   $('select[name="del_acct_service"]').css({"pointer-events":"","cursor":"","background-color":""});
   $('select[name="state"]').css({"pointer-events":"","cursor":"","background-color":""});
   $('select[name="entity_type"]').css({"pointer-events":"","cursor":"","background-color":""});
   $('select[name="entity_res_state"]').css({"pointer-events":"","cursor":"","background-color":""});
   $('select[name="investor_type"]').css({"pointer-events":"","cursor":"","background-color":""});
   $('select[name="ach_and_check_drop"]').css({"pointer-events":"","cursor":"","background-color":""});
 


  }

//......................edit account...ach_disbrusement end...//

</script>