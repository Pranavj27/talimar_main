<div class="page-container contactlist-outmost outmost_ajax_table">
    <!-- BEGIN PAGE HEAD -->
    <div class="container">
        <div class="tab-pane">
            <!--------------------MESSEGE SHOW-------------------------->
            <?php if($this->session->flashdata('error')!=''){ ?>
                <div id='error'><i class='fa fa-thumbs-o-down'></i>
                    <?php echo $this->session->flashdata('error');?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('success')!=''){ ?>
                <div id='success'><i class='fa fa-thumbs-o-up'></i> 
                    <?php echo $this->session->flashdata('success');?>
                </div>
            <?php } 
            ?>
            <div id="chkbox_err"></div>
            <!-------------------END OF MESSEGE-------------------------->
            <!-- BEGIN PAGE HEAD -->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Lenders <small></small></h1>
                </div>
                <!-- END PAGE HEAD -->
                <div class="page-container">
                    <div class="contactlist-res-div">
                        <div class="contactlist_left filter_Upp" id="contactlist_div">
                            <div class="row">                           
                                <div class="col-md-12">
                                    <div id="b_line" align="left" class="col-Fil">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-10"></div>
                                                <div class="col-md-2">                   
                                                    <div class="main_select_div-btn-1" style="margin-top: 20px;float: right;margin-bottom: 20px;" >
                                                        <a href="<?php echo base_url();?>add_lender_user" class="btn btn-primary">Add Lender</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-upper-section ajax_table_talimar table_All">
                                <table id="ajax_table_contactlist" class="table-responsive table table-bordered table-striped table-condensed flip-content dataTable no-footer">
                                    <thead>
                                        <tr>
                                            <th>Lender Name</th>
                                            <th>Status</th>
                                            <th width="15%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i=0;
                                        if(count($lender_Data)>0)
                                        {
                                            foreach ($lender_Data as $lender){
                                                $status="";
                                                if($lender->account_status=="1"){
                                                    $status="Processing";
                                                }
                                                ?>
                                                <tr>
                                                    <td><?php echo $lender->name;?></td>
                                                    <td><?php echo $status;?></td>
                                                    <td>
                                                        <a href="<?php echo base_url();?>view_lender_user/view/<?php echo $lender->id;?>" class="btn btn-primary bu_Block">View</a>
                                                    </td>
                                                </tr>
                                            <?php
                                            }
                                        }else{
                                            echo "<tr ><td colspan='3'>No Record Found</td></tr>";
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
