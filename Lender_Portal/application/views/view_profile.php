<?php

$STATE_USA = $this->config->item('STATE_USA');

?>
	<div class="contact-area">
        <div class="container">
            <div class="row">
					<?php if($this->session->flashdata('success')!=''){ ?>
						<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="notika-icon notika-close"></i></span></button> <?php echo $this->session->flashdata('success');?>
						</div>
					<?php } ?>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="contact-form sm-res-mg-t-30 tb-res-mg-t-30 tb-res-mg-t-0">		
                        <div class="contact-hd sm-form-hd">
                            <h2>User Profile</h2>
                        </div>
                        <div class="contact-form-int">
						
						<form method="post" action="<?php echo base_url()?>view_profile">
							<input type="hidden" name="id" value="<?php echo $portal_user[0]->id;?>">
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" name="fname" class="form-control" placeholder="First Name" value="<?php echo $portal_user[0]->fname ? $portal_user[0]->fname : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" name="lname" class="form-control" placeholder="Last Name" value="<?php echo $portal_user[0]->lname ? $portal_user[0]->lname : ''; ?>">
                                </div>
                            </div>
							<div class="form-group">
                                <div class="form-single nk-int-st widget-form">
									<input type="text" name="phone"  class="form-control" data-mask="(999) 999-9999" placeholder="Primary Phone" value="<?php echo $portal_user[0]->phone ? $portal_user[0]->phone : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="email" name="email" class="form-control" placeholder="Email Address" value="<?php echo $portal_user[0]->email ? $portal_user[0]->email : ''; ?>">
                                </div>
                            </div>
                            
							 <div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" name="street_address" class="form-control" placeholder="Street Address" value="<?php echo $portal_user[0]->street_address ? $portal_user[0]->street_address : ''; ?>">
                                </div>
                            </div>
							<div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" name="unit" class="form-control" placeholder="Suite or Unit #" value="<?php echo $portal_user[0]->unit ? $portal_user[0]->unit : ''; ?>">
                                </div>
                            </div>
							<div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" name="city" class="form-control" placeholder="City" value="<?php echo $portal_user[0]->city ? $portal_user[0]->city : ''; ?>">
                                </div>
                            </div>
							<div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <select class="form-control" name="state">
									<?php foreach($STATE_USA as $key => $row){ ?>
										<option value="<?php echo $key;?>" <?php if($key == $portal_user[0]->state){echo 'Selected';}?>><?php echo $row; ?></option>
									<?php } ?>
									</select>
                                </div>
								
                            </div>
							<div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="text" name="zip" class="form-control" placeholder="Zip" value="<?php echo $portal_user[0]->zip ? $portal_user[0]->zip : ''; ?>">
                                </div>
                            </div>
							<!--<div class="form-group">
                                <div class="form-single nk-int-st widget-form">
                                    <input type="file" name="file" class="file" >
                                </div>
                            </div>-->
							
							
                            <div class="contact-btn">
                                <button class="btn btn-primary waves-effect" type="submit" name="submit">Submit</button>
                            </div>
						</form>
                        </div>
                    </div>
                </div>
				
				<!--<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="contact-list">
                        <div class="contact-win">
                            <div class="contact-img">
                                <img src="<?php echo base_url();?>assets/img/post/1.jpg" alt="">
                            </div>
                            
                        </div>
                        <div class="contact-ctn">
                            <div class="contact-ad-hd">
								<h2><?php echo $portal_user[0]->fname .' '.$portal_user[0]->lname;?></h2>
								<p class="ctn-ads"><?php echo $portal_user[0]->city .' '.$portal_user[0]->state;?></p>
							</div>
                            
                        </div>
                        
                    </div>
                </div>-->
            </div>
        </div>
    </div>
    <!-- End Status area-->
   
  