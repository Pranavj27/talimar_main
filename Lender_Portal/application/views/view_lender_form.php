<?php
$STATE_USA                          = $this->config->item('STATE_USA');
$investor_type_option               = $this->config->item('investor_type_option');
$yes_no_with_not_applicable         = $this->config->item('yes_no_with_not_applicable');
?>
<div class="page-container" id="page-invester-view">
    <!-- BEGIN PAGE HEAD -->
    <div class="container">
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Lender Accounts<small></small></h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb hide">
            <li>
                <a href="#">Home</a><i class="fa fa-circle"></i>
            </li>
            <li class="active">
                 Dashboard
            </li>
        </ul>
        <div class="main_left_div">
            <div class="row">
                <!-- Back Button -->
                <div class="form-group fo" style="margin-right:14px;margin-top:26px;">
                    <a href="<?php echo base_url();?>LenderUsers" class="btn btn-primary waves-effect">
                        Back to Lender Accounts
                    </a>
                </div>
                <!-- Button -->
            </div>
            <div class="row border-top">
                <h4 >Lender Status</h4>
                <div class="col-md-12">
                    <label>Account Status:</label>
                    <?php
                    $status="";
                    if($fetch_investor_data['account_status']=="1"){
                        $status="Processing";
                    }
                    ?>
                    <span class="view_inputs">&nbsp;<?php echo $status;?></span>
                </div>
            </div>
            <div class="row border-top">
                <h4 style="font-family: sans-serif;">Lender Information</h4>
                <div class="col-md-12">
                    <label>Lender Name:</label>
                    <span class="view_inputs">&nbsp;
                        <?php 
                        if(!empty($fetch_investor_data['name'])){
                            echo $fetch_investor_data['name'];
                        }
                        ?> 
                    </span>
                </div>
            </div>
            <div class="row" id="investor_type_based">
                <div class="col-md-12" <?php if($fetch_investor_data['investor_type']=="1"){ echo "style='display:none'; ";}?>">
                    <label>Legal Entity / IRA Name:</label> 
                    <span class="view_inputs"><?php echo isset($fetch_investor_data['entity_name']) ? $fetch_investor_data['entity_name'] : '';?></span>
                </div>
            </div>  
            <div class="row">
                <div class="col-md-4" <?php if($fetch_investor_data['investor_type']!="3"){ echo "style='display:none'; ";}?>>
                    <label>Account #:</label> 
                    <span class="view_inputs"><?php echo isset($fetch_investor_data['self_dir_account']) ? $fetch_investor_data['self_dir_account'] : '';?></span>
                </div>
                <div class="col-md-4">
                    <label>Date Created:</label>
                    <span class="view_inputs">&nbsp;<?php echo date('m/d/Y'); ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label>Lender Type:</label>
                    <span class="view_inputs"> 
                        <?php 
                        $optn = isset($fetch_investor_data['investor_type']) ? $fetch_investor_data['investor_type'] : 0;  
                        if(!empty($investor_type_option[$optn])){
                            echo $investor_type_option[$optn];
                        }else{
                            echo "";
                        }     
                        ?> 
                    </span>
                </div>
                <div class="col-md-4">
                    <label>Tax ID:</label> 
                    <span class="view_inputs"><?php echo isset($fetch_investor_data['tax_id']) ? $fetch_investor_data['tax_id'] : '';?></span>
                </div>             
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label>Date Of Trust:</label> 
                    <span class="view_inputs">
                        <?php
                        if(!empty($fetch_investor_data['date_of_trust'])){
                            echo date('m/d/Y',strtotime($fetch_investor_data['date_of_trust']));
                        }
                        ?> 
                    </span>
                </div>
            </div>
            <div class="row" id="investor_type_based" >
               <div class="col-md-4" <?php if($fetch_investor_data['investor_type']=="1" || $fetch_investor_data['investor_type']=="3"){  echo "style='display:none'; ";}?>>
                    <label>Registered State:</label> 
                    <span class="view_inputs">
                        <?php
                        $optn = isset($fetch_investor_data['entity_res_state']) ? $fetch_investor_data['entity_res_state'] : '';
                       if(!empty($STATE_USA[$optn]) && $STATE_USA[$optn]!="Select One"){
                            echo $STATE_USA[$optn]; 
                        }?>
                    </span>
                </div>
                <div class="col-md-4" <?php if($fetch_investor_data['investor_type']=="1" || $fetch_investor_data['investor_type']=="3"){ echo "style='display:none'; ";}?>>
                    <label>State Entity #:</label> 
                    <span class="view_inputs"><?php echo isset($fetch_investor_data['state_entity_no']) ? $fetch_investor_data['state_entity_no'] : '';?></span>
                </div>
            </div>
            <div class="row" >
                <div class="col-md-4" <?php if($fetch_investor_data['investor_type']!="3"){ echo "style='display:none'; ";}?>>
                    <label>Custodian:</label> 
                    <span class="view_inputs"><?php echo isset($fetch_investor_data['directed_IRA_NAME']) ? $fetch_investor_data['directed_IRA_NAME'] : '';?></span>
                </div>                  
            </div> 
            <div class="row">
                <div class="col-md-8">
                    <label>Street:</label> 
                <span class="view_inputs"><?php echo isset($fetch_investor_data['address']) ? $fetch_investor_data['address'] : '';?></span>
                </div>
                <div class="col-md-4">
                    <label>Unit #:</label> 
                    <span class="view_inputs"><?php echo isset($fetch_investor_data['unit']) ? $fetch_investor_data['unit'] : '';?></span>
                </div>
            </div>
            <div class="row ">
                <div class="col-md-4">
                    <label>City:</label>
                    <span class="view_inputs"> 
                        <?php echo isset($fetch_investor_data['city']) ? $fetch_investor_data['city'] : '';?>
                    </span>
                </div>             
                <div class="col-md-4">
                    <label>State:</label> 
                    <span class="view_inputs"> 
                        <?php 
                        $optn = isset($fetch_investor_data['state']) ? $fetch_investor_data['state'] :'none';
                        if(!empty($STATE_USA[$optn]!="Select One")){
                            echo $STATE_USA[$optn];
                        }?>
                    </span>
                </div>
                <div class="col-md-4">
                    <label>Zip:</label> 
                    <span class="view_inputs"> 
                        <?php echo isset($fetch_investor_data['zip']) ? $fetch_investor_data['zip'] : '';?>
                    </span>
                </div>
            </div>
            <div class="row" id="">
                <div class="col-md-12">
                    <label>Vesting:</label>
                    <span class="view_inputs">  
                        <?php echo isset($fetch_investor_data['vesting']) ? $fetch_investor_data['vesting'] : '';?>
                    </span>
                </div> 
            </div>
            <div class="row border-top">
                <!--<h2>Primary Contact Information</h2>-->
                <h4>Lender Contacts</h4>
            </div>
            <div class="lender_information_div ">
                <?php
                if(isset($fetch_lender_contact))
                {
                    $primary_count = 0;
                    foreach($fetch_lender_contact as $key_4=> $row_data)
                    {
                        foreach($fetch_all_contact as $row)
                        {
                            if($row->contact_id == $row_data->contact_id){ 
                                $cname = $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname;
                            }
                            if($row->contact_id == $row_data->contact_id){ 
                                $cphone = $row->contact_phone;
                            }
                            if($row->contact_id == $row_data->contact_id){ 
                                $cemail = $row->contact_email;
                            }
                        }
                        $primary_count++;
                        $contact_heading = '';
                        if($key_4 == 0)
                        {
                            $contact_heading = 'Primary';
                        }
                        elseif($key_4 > 1)
                        {
                            $contact_heading = 'Secondary';
                        }
                        if($primary_count == '1'){
                            $primary_count_name = 'Primary';
                        }else{
                            $primary_count_name = 'Secondary';
                        }  
                        if($search_contact_id){
                            if( $row_data->contact_id == $search_contact_id){
                                $contact_class = "active-class";
                            }else {
                                $contact_class = "inactive-class";
                            }
                        }else{
                            $contact_class = "no-class";
                        }
                        ?>
                        <div class="contacts_div <?php echo $contact_class;?>">
                            <div class="row">
                                <div class="col-md-4">                              
                                    <label>Contact #:</label> 
                                    <span class="view_inputs"> Contact <?php echo $primary_count;?></span>
                                </div>
                            </div>
                            <div class="row <?php echo $contact_class;?>">
                                <div class="col-md-4">                              
                                    <label>Contact Name:</label> 
                                    <a href="<?php echo base_url();?>viewcontact/<?php echo $row_data->contact_id; ?>">
                                        <span class="view_inputs"><?php echo isset($row->contact_id) ? $cname : ''; ?> &nbsp; </span>
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <label>Title:</label>
                                    <span class="view_inputs"><?php echo $row_data->c_title;?></span>
                                </div>
                                <div class="col-md-4" >
                                    <label>Primary Contact:</label> 
                                        <?php
                                        if($row_data->contact_primary == '1'){
                                            $c_p='Yes';
                                        }else{
                                            $c_p='No';
                                        }
                                        ?>
                                    <span class="view_inputs"><?php echo $c_p; ?></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">                              
                                    <label>Phone:</label>
                                    <span class="view_inputs">
                                        <?php echo isset($row->contact_id) ? $cphone : ''; ?>
                                    </span>
                                </div>
                                <div class="col-md-4">                              
                                    <label>Email:</label> 
                                    <span class="view_inputs">
                                        <?php echo isset($row->contact_id) ? $cemail : ''; ?>
                                    </span>
                                </div>
                                <div class="col-md-4">                              
                                    <label>Link to Lender Portal:</label> 
                                    <span class="view_inputs"><?php echo "Yes";?></span>
                                </div> 
                            </div>
                        </div>
                    <?php  
                    }
                }  
                ?>
            </div> 
            <div class="row">
                <div class="col-md-4">  
                    <label>FCI Account:</label> 
                    <span class="view_inputs">
                        <?php
                        $optn  = isset($fetch_investor_data['fci_acct_service']) ?  $fetch_investor_data['fci_acct_service'] : '';
                        if($optn=="2"){
                            $yes_no_with_not_applicable[$optn]="Yes";
                        }else{
                            $yes_no_with_not_applicable[$optn]="No";
                        }
                        if(isset($yes_no_with_not_applicable)) {
                            echo $yes_no_with_not_applicable[$optn];  
                        }
                        ?>
                    </span>
                </div> 
                <div class="col-md-4" >
                    <label>FCI Account #:</label>
                    <span class="view_inputs"> 
                        <?php echo isset($fetch_investor_data['fci_acct']) ? $fetch_investor_data['fci_acct'] : '';?>
                    </span>
                </div>
            </div>
            <?php
            if(!empty($fetch_investor_data['bank_name']) || $fetch_investor_data['account_number'] || $fetch_investor_data['routing_number']){?>
                <div class="row border-top">
                    <h4>Broker Authorization and Approvals</h4>
                </div>
                <div class="row ach_dependent"> 
                    <div class="row ach_dependent">             
                        <div class="col-md-4">                              
                            <label>Bank Name:</label>
                            <span class = "view_inputs"><?php echo isset($fetch_investor_data['bank_name']) ? $fetch_investor_data['bank_name'] : '';?></span>
                        </div>
                        <div class="col-md-4">                          
                            <label>Account #:</label>
                            <span class = "view_inputs"><?php echo isset($fetch_investor_data['account_number']) ? $fetch_investor_data['account_number'] : '';?></span>
                        </div>
                        <div class="col-md-4">                              
                            <label>Routing Number:</label>
                            <span class = "view_inputs"><?php echo isset($fetch_investor_data['routing_number']) ? $fetch_investor_data['routing_number'] : '';?></span>
                        </div>
                    </div>
                </div>
            <?php 
            }?>
        </div>  
    </div>
</div>     
<link rel="stylesheet" href="<?php echo base_url("assets/css/lender-account/lender_view.css"); ?>">    

   


