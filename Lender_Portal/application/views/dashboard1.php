<?php $account_status_option = $this->config->item('account_status_option'); ?>
<style type="text/css">
	h4.deed_heading {
	    background-color: #ececec;
	    padding: 12px;
	}
	.hd-message-sn {
    display: flex;
    padding: 6px 15px;
}
.table.table-striped>tbody>tr>td, .table.table-striped>tbody>tr>th, .table.table-striped>tfoot>tr>td, .table.table-striped>tfoot>tr>th, .table.table-striped>thead>tr>td, .table.table-striped>thead>tr>th {
    
    padding: 8px;
}
.table.table-striped>thead>tr>th {
    min-width: 104px;
    font-size: 13px !important;
}
tfoot tr th {
	border:none;
}
table#data-table-basicss td {
    border:none;
}
body .table.table-striped tfoot {
    background-color: #fff !important;
}
</style>
	<div class="notika-status-area">

        <div class="container">

            <div class="row">
                <!-- <div class="col-xs-12 col-md-2 col-xl-2"></div> -->
                
                <div class="col-xs-12 col-md-12 col-xl-12" >
                <a href="https://talimarfinancial.com/talimar-income-fund-i/" target="_blank"> 
                    <img src="<?php echo base_url('../assets/Banners/5.png'); ?>" style="height: auto; margin-left: auto;margin-right: auto; display:block; ">
                </a>
                </div>
                <!-- <div class="col-xs-12 col-md-4 col-xl-4 dashboard_banner" ></div> -->
                <!-- <div class="col-xs-12 col-md-2 col-xl-2"></div> -->
				<!-- <div class="col-xs-12 col-md-12 col-xl-12">
					<h4 class="deed_heading">Dashboard</h4> 
					
					<h4>Dashboard</h4> 
				</div> -->
                
                

                <div class="col-md-12">
                    <div class=" table-responsive dashboard_datatable">
                        <table id="data-table-basicss" class="table table-striped">
                        <thead class="Bgcolor_code_f2f2f2">
                            <tr>
                                <th style="width:20%">Account Name</th>
                                <!-- <th style="width:10%">Lender ID</th> -->
                                <th style="width:10%">Account #</th>
                                <!-- <th style="width:10%">Account Status</th> -->
                                <th style="width:10%"># of Loans</th>
                                <!-- <th style="width:10%">Active Balance</th> -->
                                <th style="width:10%">Outstanding Balance</th>
                                <th style="width:10%">Avg. Yield</th>
                                <th style="width:10%">Monthly Payment</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $count = 0;
                        $total_avrg_yieldd = 0;
                        $total_active_balance = 0;
                        $total_monthly = 0;
                        $totaldeed_count = 0;
                        $total_monthly = 0;
                        $yield = 0.00;
                        
                        foreach($dashboard_data as $row){

                        $count++;
                        $total_active_balance +=$row['active_bal'];
                        //$total_avrg_yield +=($row['avg_yieldd']);
                        $current_balance +=$row['active_bal'];
                        $totaldeed_count +=$row['total_count'];
                        $total_monthly +=$row['lender_payment'];

                        

                            $total_avrg_yieldd += ($row['lender_payment']*12) / $row['lender_payment'] *100;
                        
                        ?>
                            <tr>
                                <td><a href="<?php echo base_url().'active_trust_deeds/'.$row['id'];?>"><?php echo $row['account_name'];?></a></td>
                                
                                <td><?php echo $row['talimar_lender'];?></td>
                                
                                <td><?php echo $row['total_count'];?></td>
                                <td>$<?php echo number_format($row['active_bal'],2);?></td>

                            
                                <td style="width:10%"><?php echo number_format($row['avg_yieldd'], 2); ?>%</td>


                                
                                <td>$<?php echo number_format($row['lender_payment'],2);?></td>
                            </tr>
                            
                        <?php  } ?>
                            
                             <!-- <tr>
                                <td colspan="9">
                                    <a href="<?php echo base_url();?>Home/addNewAccount" class="btn btn-primary pull-left waves-effect">Add Account</a>
                                </td>
                             </tr> -->
                        </tbody>
                        
                        <tfoot style="background-color: #ddd !important;">
                           <tr> 
                                <th>Total: <?php echo $count;?></th>
                            
                        
                                <th></th>
                                <th><?php echo $totaldeed_count;?></th>
                                
                                <th>$<?php echo number_format($current_balance,2);?></th>
                                
                                <th><?php
                                    if($total_monthly > 0) 
                                    {
                                            $tPer = $total_monthly*12;
                                            $tPer = $tPer/$current_balance;
                                           echo  number_format($tPer*100, 2); 
                                    }
                                    else
                                    {
                                        echo  0.00;
                                    }
                                    ?>%
                                    </th>
                                <th>$<?php echo number_format($total_monthly, 2); ?></th>
                                
                            </tr>
                        
                        </tfoot>
                   </table>
                    </div>
                   
                </div>
                <!--<div class="col-md-12">
                	<a href="<?php echo base_url();?>add_edit_account" class="btn btn-primary pull-right waves-effect">[+] Add Account</a> 
                </div>-->

            </div>

            <div class="row">
            	<div class="col-md-12">
            		<span class="average_Representsweighted">* Represents weighted average calculated by total monthly payments / total active balance.</span>
            	</div>
            </div>

        </div>

    </div>
    <script type="text/javascript">    	
    $(document).ready(function(){
		$('#data-table-basicss').DataTable({
            "searching":false,
            // "pageLength":25,
            "bLengthChange":false,

        });	
	});	
    </script>

    <!-- End Status area-->

   

  