var ajaxBaseUrl = 'https://qa.talimarfinancial.com/Lender_Portal/';
function SaveNotificatioVAl(that){
	var notify2 = $("input[name='notify_by_2']:checked").val();
	var notify7 = $("input[name='notify_by_7']:checked").val();
	var notify9 = $("input[name='notify_by_9']:checked").val();
	var notify13 = $("input[name='notify_by_13']:checked").val();
	var notification_arr = $("input[name='notification_arr']").val();

	var notifyArr = [];
	if(notify2){
		notifyArr.push(notify2);
	}
	if(notify7){
		notifyArr.push(notify7);
	}
	if(notify9){
		notifyArr.push(notify9);
	}
	if(notify13){
		notifyArr.push(notify13);
	}

	$.ajax({
			type : 'POST',
			url  : ajaxBaseUrl+'Personal_details/update_notification',
			data : {'notify2':notify2, 'notify7':notify7, 'notify9':notify9,'notify13':notify13,'notification_arr':notification_arr},
			success : function(response){
				if(response == '1'){
					swal('Error', 'Please select an notification option.', 'error');
				}else{
					$("input[name='notification_arr']").val(notifyArr);
					swal('Success', 'Notification updated successfully.', 'success');
				}
			}
	});	
}

function AvoidSpace(event) {
    var k = event ? event.which : window.event.keyCode;
    if (k == 32) return false;
}

$(document).ready(function($) {

	
	$('.remove_image_profile').click(function(event){
		swal({
		    title: "Are you sure?",
		    type: "warning",
		    showCancelButton: true,
		    confirmButtonColor: '#DD6B55',
		    confirmButtonText: 'Yes, I am sure!',
		    cancelButtonText: "Cancel"
		 }).then(
	       function () {
	       	var notify2 = 'image';
	    	$.ajax({
					type : 'POST',
					url  : ajaxBaseUrl+'profile_image_remove',
					data : {'notify2':notify2},
					success : function(response){
						if(response == '1'){
							swal('Success', 'Profile Image removed successfully!', 'success');
							window.location.reload();
						}else{
							swal('Error', 'Some error occured please try again!', 'error');
						}
					}
			});
	    },
		function () { 
			return false; 
		});
	});

	$('#uploaddprofileimage').submit(function(event){	
		event.preventDefault();
		$('.loading_spin_image').addClass('block_active_load');
		$.ajax({
	             type:"post",
	             url: ajaxBaseUrl+"profile_image_upload",
	             data:new FormData(this),
	             enctype: 'multipart/form-data',
	             processData:false,
	             contentType:false,
	             cache:false,
	             async:false,
	             timeout: 600000,
	             success: function(response){	
	             	var objRes = JSON.parse(response);
					var oky = 'Success';
					if(objRes.type == 'error'){
						oky = 'Error';
					}
					swal(oky, objRes.msg, objRes.type);		
					$('.loading_spin_image').removeClass('block_active_load');
					if(objRes.type == 'success'){
						window.location.reload();
					}
				}
	    }); 
	});

	$('body').on('change', 'label.upload_image_set_lab input[name="file"]', function(){
		var input = this;
	    var url = $(this).val();
	    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
	    if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
	     {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	           $('label.upload_image_set_lab img').attr('src', e.target.result);
	        }
	       reader.readAsDataURL(input.files[0]);
	       $('.upload_image_set').show();
	    }
	    else
	    {
	      $('label.upload_image_set_lab img').attr('src', ajaxBaseUrl+'assets/img/post/avatar0.jpg');
	    }
	});

	$('#submit_view_details_set').click(function(){
		var contact_suffix          = $('#contact_suffix').val();
		var contact_first_name 		= $('#contact_first_name').val();
		var contact_middle_initial 	= $('#contact_middle_initial').val();
		var contact_last_name 		= $('#contact_last_name').val();
		var contact_nickname 		= $('#contact_nickname').val();
		var contact_primary_email 	= $('#contact_primary_email').val();
		var contact_email_type 		= $('#contact_email_type').val();
		var contact_primary_phone 	= $('#contact_primary_phone').val();
		var contact_phone_type 		= $('#contact_phone_type').val();

		var primary_street_address 	= $('#primary_street_address').val();
		var primary_unit 			= $('#primary_unit').val();
		var primary_city 			= $('#primary_city').val();
		var primary_state 			= $('#primary_state').val();
		var primary_zip 			= $('#primary_zip').val();

		var mailing_street_address 	= $('#mailing_street_address').val();
		var mailing_unit 			= $('#mailing_unit').val();
		var mailing_city 			= $('#mailing_city').val();
		var mailing_state 			= $('#mailing_state').val();
		var mailing_zip 			= $('#mailing_zip').val();


		var old_contact_suffix         	= $('#old_contact_suffix').val();
		var old_contact_first_name 		= $('#old_contact_first_name').val();
		var old_contact_middle_initial 	= $('#old_contact_middle_initial').val();
		var old_contact_last_name 		= $('#old_contact_last_name').val();
		var old_contact_nickname 		= $('#old_contact_nickname').val();
		var old_contact_email_type 		= $('#old_contact_email_type').val();
		var old_contact_primary_phone 	= $('#old_contact_primary_phone').val();
		var old_contact_phone_type 		= $('#old_contact_phone_type').val();
		var old_primary_street_address 	= $('#old_primary_street_address').val();
		var old_primary_unit 			= $('#old_primary_unit').val();
		var old_primary_city 			= $('#old_primary_city').val();
		var old_primary_state 			= $('#old_primary_state').val();
		var old_primary_zip 			= $('#old_primary_zip').val();
		var old_mailing_street_address 	= $('#old_mailing_street_address').val();
		var old_mailing_unit 			= $('#old_mailing_unit').val();
		var old_mailing_city 			= $('#old_mailing_city').val();
		var old_mailing_state 			= $('#old_mailing_state').val();
		var old_mailing_zip 			= $('#old_mailing_zip').val();

		$.ajax({
			type : 'POST',
			url  : ajaxBaseUrl+'update_user_profile',
			data : {
				'contact_suffix'            : contact_suffix,
				'contact_first_name' 		: contact_first_name,
				'contact_middle_initial' 	: contact_middle_initial,
				'contact_last_name' 		: contact_last_name,
				'contact_nickname' 			: contact_nickname,
				'contact_primary_email' 	: contact_primary_email,
				'contact_email_type' 		: contact_email_type,
				'contact_primary_phone' 	: contact_primary_phone,
				'contact_phone_type' 		: contact_phone_type,

				'primary_street_address' 	: primary_street_address,
				'primary_unit' 				: primary_unit,
				'primary_city' 				: primary_city,
				'primary_state' 			: primary_state,
				'primary_zip' 				: primary_zip,

				'mailing_street_address' 	: mailing_street_address,
				'mailing_unit' 				: mailing_unit,
				'mailing_city' 				: mailing_city,
				'mailing_state' 			: mailing_state,
				'mailing_zip' 				: mailing_zip,

				'old_contact_suffix'            : old_contact_suffix,
				'old_contact_first_name' 		: old_contact_first_name,
				'old_contact_middle_initial' 	: old_contact_middle_initial,
				'old_contact_last_name' 		: old_contact_last_name,
				'old_contact_nickname' 			: old_contact_nickname,
				'old_contact_email_type' 		: old_contact_email_type,
				'old_contact_primary_phone' 	: old_contact_primary_phone,
				'old_contact_phone_type' 		: old_contact_phone_type,
				'old_primary_street_address' 	: old_primary_street_address,
				'old_primary_unit' 				: old_primary_unit,
				'old_primary_city' 				: old_primary_city,
				'old_primary_state' 			: old_primary_state,
				'old_primary_zip' 				: old_primary_zip,
				'old_mailing_street_address' 	: old_mailing_street_address,
				'old_mailing_unit' 				: old_mailing_unit,
				'old_mailing_city' 				: old_mailing_city,
				'old_mailing_state' 			: old_mailing_state,
				'old_mailing_zip' 				: old_mailing_zip
			},
			success : function(response){

				$('#old_contact_first_name').val(contact_first_name);
				$('#old_contact_middle_initial').val(contact_middle_initial);
				$('#old_contact_last_name').val(contact_last_name);
				$('#old_contact_nickname').val(contact_nickname);
				$('#old_contact_email_type').val(contact_email_type);
				$('#old_contact_primary_phone').val(contact_primary_phone);
				$('#old_contact_phone_type').val(contact_phone_type);
				$('#old_primary_street_address').val(primary_street_address);
				$('#old_primary_unit').val(primary_unit);
				$('#old_primary_city').val(primary_city);
				$('#old_primary_state').val(primary_state);
				$('#old_primary_zip').val(primary_zip);
				$('#old_mailing_street_address').val(mailing_street_address);
				$('#old_mailing_unit').val(mailing_unit);
				$('#old_mailing_city').val(mailing_city);
				$('#old_mailing_state').val(mailing_state);
				$('#old_mailing_zip').val(mailing_zip);

				swal("Success", "User details updated successfully!", "success");
			}
		});
	});

	$('#same_as_above_address').click(function(){
		if($(this).prop("checked") == true){
			var primary_street_address 	= $('#primary_street_address').val();
			var primary_unit 			= $('#primary_unit').val();
			var primary_city 			= $('#primary_city').val();
			var primary_state 			= $('#primary_state').val();
			var primary_zip 			= $('#primary_zip').val();

			$('#mailing_street_address').val(primary_street_address);
			$('#mailing_unit').val(primary_unit);
			$('#mailing_city').val(primary_city);
			$('#mailing_state').val(primary_state);
			$('#mailing_zip').val(primary_zip);
		}else{
			$('#mailing_street_address').val('');
			$('#mailing_unit').val('');
			$('#mailing_city').val('');
			$('#mailing_state').val('');
			$('#mailing_zip').val('');
		}
	});

	$('#submit_view_username').click(function(){
		var contact_username      = $('#contact_username').val();
		var hide_contact_username = $('#hide_contact_username').val();
		$.ajax({
			type : 'POST',
			url  : ajaxBaseUrl+'update_username',
			data : {
				'contact_username' 		    : contact_username,
				'hide_contact_username' 	: hide_contact_username				
			},
			success : function(response){
				var objRes = JSON.parse(response);
				var oky = 'Success';
				if(objRes.type == 'error'){
					oky = 'Error';
				}
				swal(oky, objRes.msg, objRes.type);
			}
		});

	});

	$('#submit_view_password').click(function(){

		var contact_new_password 	 = $('#contact_new_password').val();
		var contact_confirm_password = $('#contact_confirm_password').val();

		$.ajax({
			type : 'POST',
			url  : ajaxBaseUrl+'update_password',
			data : {
				'contact_new_password' 			: contact_new_password,				
				'contact_confirm_password' 		: contact_confirm_password
			},
			success : function(responseCall){
				var objRes = JSON.parse(responseCall);
				var oky = 'Success';
				if(objRes.type == 'error'){
					oky = 'Error';
				}
				swal(oky, objRes.msg, objRes.type);
			}
		});

	});
});