var rowNo=0;
var today = new Date();
$("#entity_date_trust").datepicker({
    changeMonth: false,
    changeYear: false,
    minDate: today // set the minDate to the today's date
    // you can add other options here
});
function add_new_contact(){
	rowNo=parseInt(rowNo)+1;
	$.ajax({
        type : 'post',
        url  : 'LenderUsers/add_new_contact',
        data : {'row_no':rowNo},
        success : function(response){
            $("#contact_select_section").append(response);
        }
    });
}
function get_selected_account_title(account_title){
	if(account_title=="1"){
		$("#account_title_section").hide();
	}else{
		$("#account_title_section").show();
	}
}
function another_persion_signup_action(signup_check){
	if(signup_check=="1"){
		$("#lender_contact_section").show();
		$(".add_btn_blc").show();
	}else{
		$("#lender_contact_section").hide();
		$(".add_btn_blc").hide();
	}
}
function fci_lender_service_action(fci_option){
	if(fci_option=="1"){
		$("#fci_account_section").show();
	}else{
		$("#fci_account_section").hide();
	}
}
function ach_disbrusement_value(ach_value){
	if(ach_value=="Mailed"){
		$(".ach_disbrusement_ach_section").each(function(){
		        $(this).hide();
		});
	}else{
		$(".ach_disbrusement_ach_section").each(function(){
		        $(this).show();
		});
	}
}
$('#confirm_data_btn').on('click', function() {
	var account_title_type=$("#account_title").val();
	var investor_name=$("#investor_name").val();
	var investor_type=$("#investor_type option:selected").text();
	var contact_first_name=$("#contact_id_0 option:selected").text();
	var account_title_name=$("#account_title_name").val();
	var tax_id=$("#tax_id").val();
	var address=$("#address").val();
	var unit=$("#unit").val();
	var city=$("#city").val();
	var state=$("#state").val();
	var zip=$("#zip").val();
	var entity_ira_account=$("#fci_acct").val();
	var fci_acct=$("#fci_acct").val();
	var ach_disbrusement=$("#ach_disbrusement").val();
	var bank_name=$("#bank_name").val();
	var account_number=$("#account_number").val();
	var routing_number=$("#routing_number").val();
	var fci_acct_service=$('input[name="fci_acct_service"]:checked').val();
	if(fci_acct_service=="0"){
		fci_acct_service="No";
	}else{
		fci_acct_service="Yes";
	}
	$("#confirm_account_name").html(investor_name);
	$("#confirm_account_type").html(investor_type);
	$("#confirm_contact_name").html(contact_first_name);
	$("#first_contact_name").val(contact_first_name);
	if(account_title_type=="1"){
		$(".class_confirm_contact_title").hide();
	}
	$("#confirm_contact_title").html(account_title_name);
	$("#confirm_tax_id").html(tax_id);
	$("#confirm_address").html(address);
	$("#confirm_unit").html(unit);
	$("#confirm_city").html(city);
	$("#confirm_state").html(state);
	$("#confirm_zipcode").html(zip);
	$("#confirm_fci_account").html(fci_acct_service);
	if(fci_acct_service=="No"){
		$(".class_fci_account").hide();
	}
	$("#confirm_account_number").html(fci_acct);
	var ach_value=$('input[name="ach_disbrusement"]:checked').val();
	$("#confirm_deposite_type").html(ach_value);
	if(ach_value=="Mailed"){
		$(".ach_depiste_info").each(function(){
			        $(this).hide();
		});
	}
	else{
		$(".ach_depiste_info").each(function(){
			        $(this).show();
		});
	}
	$("#confirm_bank_name").html(bank_name);
	$("#confirm_account_num").html(account_number);
	$("#confirm_routing_num").html(routing_number);
});
function get_selected_data_option(row_no){
	var option_email= $("#contact_id_"+row_no+" option:selected").attr("option_email");
	var option_phone= $("#contact_id_"+row_no+" option:selected").attr("option_phone");
	var option_id= $("#contact_id_"+row_no).val();
	$("#contact_row_id_"+row_no).val(option_id);
	$("#contact_row_email_"+row_no).val(option_email);
	$("#contact_row_phone_"+row_no).val(option_phone);
}
function investor_type_effect(typeValue){
	if(typeValue!="" && typeValue>0){
		if(typeValue=="1"){
			$("#state_entity_no").prop("readonly", true);
			$("#entity_res_state").prop("disabled", true);
			$("#entity_name").prop("readonly", true);
			$("#directed_IRA_NAME").prop("readonly", true);
			$("#self_dir_account").prop("readonly", true);
		}
		else if(typeValue=="3"){
			$("#entity_res_state").prop("disabled", true);
			$("#state_entity_no").prop("readonly", true);
			$("#entity_name").prop("readonly", false);
			$("#directed_IRA_NAME").prop("readonly", false);
			$("#self_dir_account").prop("readonly", false);
		}else{
			$("#entity_name").prop("readonly", false);
			$("#entity_res_state").prop("disabled", false);
			$("#state_entity_no").prop("readonly", false);
			$("#directed_IRA_NAME").prop("readonly", true);
			$("#self_dir_account").prop("readonly", true);
		}
	}
}
function scroll_to_class(element_class, removed_height) {
	var scroll_to = $(element_class).offset().top - removed_height;
	if($(window).scrollTop() != scroll_to) {
		$('html, body').stop().animate({scrollTop: scroll_to}, 0);
	}
}
function bar_progress(progress_line_object, direction) {
	var number_of_steps = progress_line_object.data('number-of-steps');
	var now_value = progress_line_object.data('now-value');
	var new_value = 0;
	if(direction == 'right') {
		new_value = now_value + ( 100 / number_of_steps );
	}
	else if(direction == 'left') {
		new_value = now_value - ( 100 / number_of_steps );
	}
	progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
}
jQuery(document).ready(function() {
    /*
        Fullscreen background
    */
	    $('#top-navbar-1').on('shown.bs.collapse', function(){
	    	$.backstretch("resize");
	    });
	    $('#top-navbar-1').on('hidden.bs.collapse', function(){
	    	$.backstretch("resize");
	    });
    /*
        Form
    */
	    $('.f1 fieldset:first').fadeIn('slow');
	    
	    $('.f1 input[type="text"], .f1 input[type="password"], .f1 textarea').on('focus', function() {
	    	$(this).removeClass('input-error');
	    });
    // next step
	    $('.f1 .btn-next').on('click', function() {
	    	var parent_fieldset = $(this).parents('fieldset');
	    	var next_step = true;
	    	// navigation steps / progress steps
	    	var current_active_step = $(this).parents('.f1').find('.f1-step.active');
	    	var progress_line = $(this).parents('.f1').find('.f1-progress-line');
	    	// *********************************validation section**********************//
	    	var have_error=0;
			if($("#investor_type").val()=="0"){
				$("#investor_type").focus();
				$("#investor_type_error").html("Please select account type");
				have_error=1;
			}else{
				$("#investor_type_error").html("");
			}
			if($("#investor_name").val()==""){
				$("#investor_name").focus();
				$("#investor_name_error").html("Please enter investor name");
				have_error=1;
			}else{
				$("#investor_name_error").html("");
			}
			if($("#account_title").val()==""){
				$("#account_title").focus();
				$("#account_title_error").html("Please select account title");
				have_error=1;
			}else{
				$("#account_title_error").html("");
			}
			var another_persion_signup=$('input[name="another_persion_signup"]:checked').val();
			if(another_persion_signup=="1"){
				if($("#contact_id_0").val()=="null" || $("#contact_id_0").val()==null){
					$("#contact_id_0").focus();
					$("#contact_id_error").html("Please select one primary contact");
					have_error=1;
				}else{
					$("#contact_id_error").html("");
				}
			}
			if(have_error=="1"){
				$(this).addClass('input-error');
	    			next_step = false;
			}else{
				$(this).removeClass('input-error');
			}
	    	// *********************************validation section End**********************//
	    	if( next_step ) {
	    		parent_fieldset.fadeOut(400, function() {
	    			// change icons
	    			current_active_step.removeClass('active').addClass('activated').next().addClass('active');
	    			// progress bar
	    			bar_progress(progress_line, 'right');
	    			// show next step
		    		$(this).next().fadeIn();
		    		// scroll window to beginning of the form
	    			scroll_to_class( $('.f1'), 20 );
		    	});
	    	}
	    });
    // previous step
	    $('.f1 .btn-previous').on('click', function() {
	    	// navigation steps / progress steps
	    	var current_active_step = $(this).parents('.f1').find('.f1-step.active');
	    	var progress_line = $(this).parents('.f1').find('.f1-progress-line');
	    	
	    	$(this).parents('fieldset').fadeOut(400, function() {
	    		// change icons
	    		current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
	    		// progress bar
	    		bar_progress(progress_line, 'left');
	    		// show previous step
	    		$(this).prev().fadeIn();
	    		// scroll window to beginning of the form
				scroll_to_class( $('.f1'), 20 );
	    	});
	    });
    // submit form
	    $('.f1').on('submit', function(e) {
	    	
	    });    
});
