$(document).ready(function() {
	var baseUrl = 'https://qa.talimarfinancial.com/Lender_Portal';
	$('body').on('keyup', '.username_input', function(){
		$('.submit_register').prop( "disabled", false );
		var username = $(this).val();
		if(username){
			$('.username_errors').html('<span class="wait_load">&nbsp;<img src="'+baseUrl+'/assets/images/loading.gif" alt="" /></span>');
			$.ajax({
	            type: 'post',
	            url: baseUrl+'/user_name_checker', 
	            //dataType: 'json',
	            data: 'username='+username,
	            beforeSend: function() {
	                $('.submit_register').prop( "disabled", true );  
	                $('.username_errors').html('<span class="wait_load">&nbsp;<img src="'+baseUrl+'/assets/images/loading.gif" alt="" /></span>');
	            },
	            complete: function() {
	                //$('.submit_register').addClass('disabled_btn');                
	            },  
	            success: function(res_data){
	            	setTimeout(function(){
		                if(res_data == 'exist'){
		                	$('.username_errors').html('<span class="wait_load">&nbsp;<img src="'+baseUrl+'/assets/images/Cross.png" alt="" /></span>');
		                	$('.submit_register').prop( "disabled", true );
		                }else{
		                	$('.submit_register').prop( "disabled", false );
		                	$('.username_errors').html('<span class="wait_load">&nbsp;<img src="'+baseUrl+'/assets/images/check.png" alt="" /></span>');
		                }
		            }, 1500);
	            }
	        });
		}
	});

	var haveFirst = false;
	$('.username_input').on('keypress', function (event) {
		if( $(this).val().length === 0 ) {
		     haveFirst = false;
		}
		var regex = new RegExp("^[a-z0-9_]+$");
		var first = new RegExp("^[a-z]+$");
		var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		if(!first.test(key) && haveFirst == false){
		   event.preventDefault();
		   return false;
		}else if(regex.test(key)){
		   haveFirst = true;
		}
		if (!regex.test(key)) {
		   event.preventDefault();
		   return false;
		}
	}); 	
});