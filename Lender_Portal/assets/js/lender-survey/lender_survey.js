function load_modal(taa){
    $('#lender_data').modal('show');
}
function getValidate(){
    var haveError=0;
    if($("#ownership_type").val()==""){
        $("#ownership_type_error").html("Please Select Owner Type");
        $("#ownership_type").focus();
        haveError=1;
    }else{
        $("#ownership_type_error").html("");
    }
    if(haveError==1){
        return false;
    }else{
        return true;
    }
}
