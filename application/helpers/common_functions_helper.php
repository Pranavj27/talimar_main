<?php

// If access is requested from anywhere other than index.php then exit
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_LTV_value'))
{
	function get_LTV_value($talimar_loanN, $loan_amount){
		$CI = &get_instance();				    
		$CI->load->model('User_model');		
		$key = 0;
		$total_pipeline_available = 0;
		$total_interst_rate = 0;
		$total_loan_amount = 0;
		$total_ltv = 0;
		$total_term = 0;
		$total_avi_balance = 0;

		$talimar_loan_id['talimar_loan'] = $talimar_loanN;
		$fetch_property_home = $CI->User_model->select_where_asc('property_home', $talimar_loan_id, 'primary_property');
		$fetch_propertyHome = $fetch_property_home->result();
		$total_uw_value = 0;
		$total_sl_value = 0;
		$sum_secondary_uw = 0;
		$sum_secondary_sl = 0;
		$total_of_senior_lien = 0;

		if (isset($fetch_propertyHome)) {
			foreach ($fetch_propertyHome as $keyP => $rowP) {

					$sql = "SELECT COUNT(*) as count_encum, SUM(current_balance) as sum_current_balance, existing_priority_to_talimar, original_balance FROM encumbrances WHERE property_home_id = '" . $rowP->id . "' AND talimar_loan = '" . $talimar_loanN . "' AND will_it_remain = 0 ";
					$fetch_ecum_right = $CI->User_model->query($sql);
					$fetch_ecum_right = $fetch_ecum_right->row();

					if ($rowP->primary_property == 1) {
						$primary_uw_value = $rowP->future_value;
					}

					if ($rowP->primary_property == 2) {
						$sum_secondary_uw += $rowP->future_value;
					}

					if($fetch_ecum_right->existing_priority_to_talimar == '1'){
						$total_of_senior_lien = $total_of_senior_lien + $fetch_ecum_right->sum_current_balance;
					}
			}
			$total_uw_value = $primary_uw_value + $sum_secondary_uw;
		}
		return ($loan_amount + $total_of_senior_lien) / $total_uw_value * 100;
	}
}

if ( ! function_exists('task_due_count'))
{
	function task_due_count($user_id){		
		$CI = &get_instance();				    
		$CI->load->model('User_model');
		$_datas = $CI->User_model->query("SELECT * FROM contact_tasks WHERE (t_user_id='$user_id' OR  FIND_IN_SET($user_id,add_people) ) AND contact_status='Active' AND  DATE(due_date) < DATE(NOW()) ");
		$record = $_datas->num_rows();
		return $record;
	}
}

if ( ! function_exists('get_loan_street_address'))
{
	function get_loan_street_address($talimar_loan){
		$CI = &get_instance();				    
		$CI->load->model('User_model');

		$talimarAddress = array();
		$talimarAddress['talimar_loan'] = $talimar_loan;
		$fetch_loanProperty 			= $CI->User_model->select_where('loan_property',$talimarAddress);
		$fetch_loanProperty 			= $fetch_loanProperty->row();

		$fulladdress = '';
		if($fetch_loanProperty){
	        $fulladdress = $fetch_loanProperty->property_address;
	    }else{
	        $fulladdress = '';
	    }
	    return $fulladdress;
	}
}

if ( ! function_exists('amount_PMT'))
{
	function amount_PMT($i, $n, $p) {
	 return $i * $p * pow((1 + $i), $n) / (1 - pow((1 + $i), $n));
	}
}
?>