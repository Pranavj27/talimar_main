<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/Sendgridtalimar.php';
function send_mail_template($data = array()){
	ob_start();

	$from_email 	= '';
	$from_name 		= '';
	$to 			= '';
	$name 			= '';
	$subject 		= '';
	$content_body 	= '';
	$button_url 	= '';
	$button_text 	= '';

	if($data['from_email']){
		$from_email = $data['from_email'];
	}
	if($data['from_name']){
		$from_name = $data['from_name'];
	}
	if($data['to']){
		$to = $data['to'];
	}
	if($data['name']){
		$name = $data['name'];
	}
	if($data['subject']){
		$subject = $data['subject'];
	}
	if($data['content_body']){
		$content_body = $data['content_body'];
	}
	if($data['button_url']){
		$button_url = $data['button_url'];
		$button_text = $data['button_text'];
	}

	?>
	<!doctype html>
		<html>

		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta name="format-detection" content="telephone=no" />
			<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;" />
			<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
			<title>Welcome To Talimar Financial</title>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">
			<style>
				@import url("https://fonts.googleapis.com/css?family=Poppins:400,500,100%,700&display=swap");
				.cst-btn {
				background-color: #4eb8f6;
				/* font-family: '."'".'Poppins'."'". */
				}
				.cst-btn:hover {
				background-color: #108ed8;
				}
				@media only screen and (max-width:600px) {
				table[class=box] {
				width: 100%
				}
				td[class=head-title] {
				font-size: 24px !important;
				padding-bottom: 10px !important;
				line-height: 32px !important;
				}
				table[class=mb_Super] {
				width: 100%;
				}
				div[class=mb_View] {
				width: 96% !important;
				padding-left: 2%;
				padding-right: 2%; 
				}
				}
				@media only screen and (max-width:480px) {
				td[class=head-title] {
				font-size: 18px !important;
				}
				a[class=cst-btn] {	
				padding-left: 12px !important;
				padding-right: 12px !important;
				font-size: 13px !important;
				}
				img[class=logo_img] {
				width: 95px !important;
				}
				}
				@media only screen and (max-width:400px) {
				td[class=title1] {
				font-size: 18px !important;
				}
				td[class=footer-items] {
				font-size: 11px !important;
				}
				td[class=ic-text] {
				font-size: 16px !important;
				}
				td[class=link-item] {
				font-size: 18px !important;
				}

				td[class=up_Number] {
				text-align: right;
				}
				td[class=redu_Space] {
				padding-left: 15px;
				padding-right: 15px;
				}
				}
			</style>
		</head>

		<body style="margin:0px; padding:0px; font-family: 'Poppins',sans-serif!important; color:#333;" bgcolor="#f4f4f4">
			<table align="center" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td>
						<div class="mb_View" style="background-color: #f4f4f4 !important; max-width:600px; margin: 0 auto;">&nbsp;
							<br>&nbsp;
							<br>
							<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td>
										<table width="100%" cellpadding="0" cellspacing="0">
											<tr>
												<td class="logo_block" bgcolor="" style="padding-top:5px; padding-bottom:26px; text-align: center;">	<a href="#m_-1778674465433749821_m_-1945850375391101209_m_-6818727870886611857_" target="_blank" target="_blank"> 
				<img src="<?php echo base_url(); ?>assets/images/logo-big.png" width="136px" class="logo_img"> 
				</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" cellpadding="0" cellspacing="0" align="center">
											<tr>
												<td style="padding-left:20px; padding-right:20px; padding-top:37px; padding-bottom:40px; border-radius: 5px 5px 0 0;" bgcolor="#FFFFFF">
													<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
														<tbody>
															<tr>
																<td>
																	<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
																		<tbody>
																			<tr>
																				<td class="head-title" style="font-family: 'Poppins',sans-serif !important; font-size:20px; padding-bottom:20px; color:#000; line-height:30px; font-weight:500; text-align:center;"></td>
																			</tr>
																			<tr>
																				<td style="padding-top:1px; padding-bottom:7px; font-family: 'Poppins',sans-serif !important; color:#233047; font-size:15px; line-height:24px; font-weight: 500">
																					<?php if($name){ ?>
																					Hi <?php echo $name; ?>,
																					<?php } ?>
																				</td>

																			</tr>
																			<tr>
																				<td style="font-family: 'Poppins',sans-serif !important; font-size:14px; padding-top:10px;  color:#141414; line-height:24px; padding-bottom:30px;" class="para1">
																				<?php echo $content_body; ?>
																				</td>
																			</tr>
																			<tr>
																				<td style="padding-bottom: 20px;">
																					<?php if($button_url){ ?>
																					<table style="background-color: #093164; border: 1px solid #093164; border-radius: 4px;" cellspacing="0" cellpadding="0" align="center">
																						<tbody>
																							<tr>
																								<td style="padding-left: 15px; padding-right: 15px; text-align: center;" valign="middle" height="50" width="164">
																									<?php if($button_url){ ?>
																									<a href="<?php echo $button_url; ?>" target="_blank"  style="font-family: 'Poppins',sans-serif !important; border-radius: 4px; font-size: 14px; line-height: 14px; color: #fff; text-decoration: none;">
																				                        <span><?php echo $button_text; ?>
																				                        </span>
																				                      </a>
																				                  	<?php } ?>
																								</td>
																							</tr>
																						</tbody>
																					</table>
																					<?php } ?>
																				</td>
																			</tr>
																			<tr>
																				<td style="padding-top:0px; padding-bottom:0px; font-family: 'Poppins',sans-serif !important; color:#141414; font-size:14px; line-height:24px;">Regards,</td>
																			</tr>
																			<tr>
																				<td style="padding-top:0px; padding-bottom:0px; font-family: 'Poppins',sans-serif !important; color:#141414; font-size:14px; line-height:24px;">
																				<?php
																				if(isset($data['regards'])){
																					echo $data['regards'];
																				}else{
																					echo 'Investor Relations';
																				}
																				?>
																				
																			</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td bgcolor="#efeeed" style="padding-left:11px; padding-right:10px; padding-top:15px; padding-bottom:15px; border-radius: 0 0 5px 5px;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td style="text-align:center; font-size:14px; line-height:14px; font-family: 'Poppins',sans-serif !important; color:#141414; line-height:23px;">TaliMar Financial Inc. | <a href="https://www.talimarfinancial.com/">talimarfinancial.com</a> | <a href="tel:(858) 242-4900">(858) 242-4900</a> | CA DRE # 01889802</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</table>
						</div>
				</tr>
				</td>
			</table>
		</body>

		</html>
	<?php

	$content = ob_get_contents();
	ob_end_clean();
	/*$CI =& get_instance();
	$CI->load->library('email');

	$CI->email->initialize(SMTP_SENDGRID);
    $CI->email->from($from_email, $from_name);
    $CI->email->to($to);        
    $CI->email->subject($subject);
    $CI->email->message($content);
    
    if(isset($data['cc'])){
    	if($data['cc']){
			$CI->email->cc($data['cc']);
		}
	}
	if(isset($data['bcc'])){
    	if($data['bcc']){
			$CI->email->bcc($data['bcc']);
		}
	}
	if($data['attach_file']){
		if($data['attach_file']){
			$CI->email->attach($data['attach_file']);
		}
	}

    $CI->email->set_mailtype('html'); 
    $CI->email->send();*/

    $toEmail = $to; 
    $fromEmail = $from_email; 
    $fromName = $from_name;
    $subject = $subject; 
    $message = $content; 
    $toCc = '';
    $toBcc = '';
    $attachFile = '';
    
    $userId = '';
	$lenderId = '';
	$borrowerId = '';
	$loan_id = '';
	$mailTracking = 'no';
	$trackingType = '';


    if(isset($data['cc'])){
    	if($data['cc']){
			$toCc = $data['cc'];
		}
	}
	if(isset($data['bcc'])){
    	if($data['bcc']){
			$toBcc = $data['bcc'];
		}
	}
	if(isset($data['attach_file'])){
		if($data['attach_file']){
			$attachFile = $data['attach_file'];
		}
	}

	/************************************/
	if(isset($data['mail_tracking'])){
		if($data['mail_tracking']){
			$mailTracking = $data['mail_tracking'];
		}
	}
	if(isset($data['loan_id'])){
		if($data['loan_id']){
			$loan_id = $data['loan_id'];
		}
	}
	if(isset($data['borrower_id'])){
		if($data['borrower_id']){
			$borrowerId = $data['borrower_id'];
		}
	}
	if(isset($data['lender_id'])){
		if($data['lender_id']){
			$lenderId = $data['lender_id'];
		}
	}
	if(isset($data['user_id'])){
		if($data['user_id']){
			$userId = $data['user_id'];
		}
	}
	if(isset($data['tracking_type'])){
		if($data['tracking_type']){
			$trackingType = $data['tracking_type'];
		}
	}

	$CI =& get_instance();
	$CI->load->library('email');

	$CI->email->initialize(SMTP_SENDGRID);
    $CI->email->from($fromEmail, $fromName);
    $CI->email->to($toEmail);        
    $CI->email->subject($subject);
    $CI->email->message($message);
    $CI->email->set_mailtype('html'); 
    $CI->email->send();

    //$sendgrid = new Sendgridtalimar();    
	//$response = $sendgrid->send_mail_sendgrid($toEmail, $fromEmail, $fromName, $subject, $message, $toCc, $toBcc, $attachFile, $mailTracking, $trackingType, $loan_id, $borrowerId, $lenderId, $userId);
	
	return $CI->email->send(); //$content;
}

if ( ! function_exists('get_loan_property_address'))
{
	function get_loan_property_address($talimar_loan){
		$CI = &get_instance();				    
		$CI->load->model('User_model');

		$talimarAddress = array();
		$talimarAddress['talimar_loan'] = $talimar_loan;
		$fetch_loanProperty 			= $CI->User_model->select_where('loan_property',$talimarAddress);
		$fetch_loanProperty 			= $fetch_loanProperty->row();

		 $addressName = '';

		if(!empty($fetch_loanProperty->property_address))
		{
			$addressName .= $fetch_loanProperty->property_address;
		}

		if(!empty($fetch_loanProperty->unit))
		{
			$addressName .=!empty($addressName)?' '.$fetch_loanProperty->unit : ' '.$fetch_loanProperty->unit;
		}

		if(!empty($fetch_loanProperty->city))
		{
			$addressName .= !empty($addressName)?'; '.$fetch_loanProperty->city : $fetch_loanProperty->city;
		}

		if(!empty($fetch_loanProperty->state))
		{
			$addressName .= !empty($addressName)?', '.$fetch_loanProperty->state : $fetch_loanProperty->state;
		}

		if(!empty($fetch_loanProperty->zip))
		{
			$addressName .= !empty($addressName)?' '.$fetch_loanProperty->zip : $fetch_loanProperty->zip;
		}
	    return $addressName;
	}
}

if ( ! function_exists('get_borrower_address'))
{
	function get_borrower_address($id){
		$CI = &get_instance();				    
		$CI->load->model('User_model');

		$talimarAddress = array();
		
		$talimarAddress['id'] = $id;
		$fetch_borrowerAdd 			= $CI->User_model->select_where('borrower_data',$talimarAddress);
		$fetch_borrowerAdd 			= $fetch_borrowerAdd->row();

		 $addressName = '';

		if(!empty($fetch_borrowerAdd->b_address))
		{
			$addressName .= $fetch_borrowerAdd->b_address;
		}

		if(!empty($fetch_borrowerAdd->b_unit))
		{
			$addressName .=!empty($addressName)?' '.$fetch_borrowerAdd->b_unit : ' '.$fetch_borrowerAdd->b_unit;
		}

		if(!empty($fetch_borrowerAdd->b_city))
		{
			$addressName .= !empty($addressName)?'; '.$fetch_borrowerAdd->b_city : $fetch_borrowerAdd->b_city;
		}

		if(!empty($fetch_borrowerAdd->b_state))
		{
			$addressName .= !empty($addressName)?', '.$fetch_borrowerAdd->b_state : $fetch_borrowerAdd->b_state;
		}

		if(!empty($fetch_borrowerAdd->b_zip))
		{
			$addressName .= !empty($addressName)?' '.$fetch_borrowerAdd->b_zip : $fetch_borrowerAdd->b_zip;
		}
	    return $addressName;
	}
}


if( ! function_exists('get_lender_address'))
{
	function get_lender_address($id){
		$CI = &get_instance();				    
		$CI->load->model('User_model');

		$talimarAddress = array();
		
		$talimarAddress['id'] = $id;
		$fetch_lenderAdd 			= $CI->User_model->select_where('investor',$talimarAddress);
		$fetch_lenderAdd 			= $fetch_lenderAdd->row();

		 $addressName = '';

		if(!empty($fetch_lenderAdd->address))
		{
			$addressName .= $fetch_lenderAdd->address;
		}

		if(!empty($fetch_lenderAdd->unit))
		{
			$addressName .=!empty($addressName)?' '.$fetch_lenderAdd->unit : ' '.$fetch_lenderAdd->unit;
		}

		if(!empty($fetch_lenderAdd->city))
		{
			$addressName .= !empty($addressName)?'; '.$fetch_lenderAdd->city : $fetch_lenderAdd->city;
		}

		if(!empty($fetch_lenderAdd->state))
		{
			$addressName .= !empty($addressName)?', '.$fetch_lenderAdd->state : $fetch_lenderAdd->state;
		}

		if(!empty($fetch_lenderAdd->zip))
		{
			$addressName .= !empty($addressName)?' '.$fetch_lenderAdd->zip : $fetch_lenderAdd->zip;
		}
	    return $addressName;
	}
}

if ( ! function_exists('input_sanitize'))
{
	function input_sanitize($input){
		$CI = &get_instance();
		return mysqli_real_escape_string($CI->db->conn_id, $input);
	}	
}

if ( ! function_exists('input_date_format'))
{
	function input_date_format($doc_date){
		if($doc_date){
			$dates = explode('-', $doc_date);
		    $monthA = $dates[0];
			$dayA = $dates[1];
			$yearA = $dates[2];
			return $newDateString = $yearA.'-'.$monthA.'-'.$dayA;	
		}
		return '';		
	}	
}