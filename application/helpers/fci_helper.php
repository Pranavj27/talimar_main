<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
if (!function_exists('get_loan_details_by_fci_id')) {

    function get_loan_details_by_fci_id($loanAccount){
        // Next payment get api code***************************************************
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://api.trustfci.com:8065/graphql',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{"query":"{ \\r\\n  getLoanInformation(\\r\\n        loanaccount:\\"'.$loanAccount.'\\",\\r\\n        offset:0,\\r\\n        orderby: \\"nextDueDate\\",\\r\\n        order: \\"desc\\"\\r\\n    )\\r\\n        {\\r\\n            loanAccount\\r\\n          nextDueDate\\r\\n        }\\r\\n}","variables":{}}',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer TnTaHakqp440vi0iNc7XfXLjBK9zymmdSLnfraquGb0',
            'Content-type:  application/json'
          ),
        ));
        //totalPayment\\n
        $responseNextDate = curl_exec($curl);

        $NextDateArray  = json_decode($responseNextDate);

        curl_close($curl);

        $arrayNext = array();
        if(!empty($NextDateArray->data->getLoanInformation))
        {
            $NextDate = $NextDateArray->data->getLoanInformation;
            return date('Y-m-d', strtotime($NextDate[0]->nextDueDate));
        }
        else
        {
            return '';
        }
        
        return $arrayNext;
    }
}

if(!function_exists('getLoanPortfolio'))
{
    function getLoanPortfolio($status = '', $days = '')
    {
        if($days != '')
        {
            $where = 'active:true, dayslate:'.$days;
        }
        else{
            $where = 'active:true';
        }
        
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://api.trustfci.com:8080/graphql',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{"query":"{\\n  getLoanPortfolio('.$where.')\\n  {\\n    loanAccount\\n    lenderAccount\\n    originalBalance\\n    currentBalance\\n    daysLate\\n    maturityDate\\n    nextDueDate\\n    totalPayment\\n    loanStatus\\n    \\n  }\\n}","variables":{}}',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer TnTaHakqp440vi0iNc7XfXLjBK9zymmdSLnfraquGb0',
            'Content-type:  application/json'
          ),
        ));
        //totalPayment\\n
        $responseNextDate = curl_exec($curl);

        $NextDateArray  = json_decode($responseNextDate);

        curl_close($curl);

        $array = array();
        if(!empty($NextDateArray->data->getLoanPortfolio))
        {
            $array = $NextDateArray->data->getLoanPortfolio;
        }       
        return $array;
    }
}

if(!function_exists('cronJobStatus'))
{
    function cronJobStatus($name = '', $url = '', $status = ''){
        $CI = &get_instance();                  
        $CI->load->model('Common_model', 'CM');

        if(!empty($url) && !empty($name) && !empty($status)){
            $param = array('name' => $name, 'url' => $url, 'status' => $status, 'createdAt' => date('Y-m-d H:i:s'));

            return $CI->CM->insertdata('cronJobHistory', $param, '');
        }

        return false;
    }
}

if(!function_exists('loanDetailsFci'))
{
    function loanDetailsFci($fci = '')
    {
        if(!empty($fci))
        {
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => 'https://api.trustfci.com:8080/graphql',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS =>'{"query":"{\\n  getLoanDetails(account:\\"'.$fci.'\\"){\\n    account\\n    aCHStatus\\n    aCHStatusEnum    \\n    loanMaturity\\n    nextPaymentDue   \\n    payment    \\n    totalPayment\\n  }\\n}\\n","variables":{}}',
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Bearer TnTaHakqp440vi0iNc7XfXLjBK9zymmdSLnfraquGb0'
              ),
            ));

            $responseNextDate = curl_exec($curl);

            $NextDateArray  = json_decode($responseNextDate);

            curl_close($curl);

            $array = array();
            if(!empty($NextDateArray->data->getLoanDetails))
            {
                $array = $NextDateArray->data->getLoanDetails;
            }       
            return $array;

        }
        return false;
    }
}


?>