<?php

class Contact_marketing extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper(array('form', 'url'));
	
		$this->load->model('User_model');
		$this->load->library('Aws3','aws3');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');
		if($this->session->userdata('t_user_id') != '' )
		{
		}
		else
		{
			redirect(base_url()."home",'refresh');
		}

	}


	public function changeFormat($date){

		//$date = DateTime::createFromFormat('m-d-Y', $date);
		//$date ? $date->format('Y-m-d') : date('Y-m-d');
		$start_date = input_date_format($date);

		return $start_date;
	}


	public function removerow(){

		$id = $this->input->post('id');
		$this->User_model->query("DELETE FROM `contact_marketing` WHERE `id` = '".$id."'");
		echo '1';
	}

	/*
		Description : This function use for add and update marketing material data - add one field requested_by 
		Author      : Bitcot
		Created     : 
		Modified    : 06-04-2021
	*/

	public function Contactmarketingdata(){
		
		if(isset($_POST['submit'])){

			$contact_id_page = $this->input->post('contact_id_page');
			$contact_id = $this->input->post('contact_id');
			$swag = $this->input->post('swag');
			$date = $this->input->post('date');
			$note = $this->input->post('note');
			$title = $this->input->post('title_status');
			$rowid = $this->input->post('rowid');

			foreach ($rowid as $key => $value) {

				if($value == 'new'){

					$start_date = $this->changeFormat($date[$key]);
					$sql = "INSERT INTO `contact_marketing`(`contact_id`, `requested_by`, `swag`, `date`, `note`, `title_status`) VALUES ('".$contact_id_page."', '".$contact_id[$key]."', '".$swag[$key]."', '".$start_date."', '".$note[$key]."', '".$title[$key]."')";
					$this->User_model->query($sql);

				}else{

					$start_date = $this->changeFormat($date[$key]);
					$sql = "UPDATE `contact_marketing` SET `contact_id`='".$contact_id_page."', `requested_by`='".$contact_id[$key]."', `swag`='".$swag[$key]."', `date`='".$start_date."', `note`='".$note[$key]."', `title_status`='".$title[$key]."' WHERE id = '".$value."'";
					$this->User_model->query($sql);
				}
			}
			
			$this->session->set_flashdata('success', 'Contact marketing updated successfully!');
			redirect(base_url(). 'viewcontact/'.$contact_id_page);

		}
		
	}

	public function amount_format($n) {
		$n = str_replace(',', '', $n);
		$a = trim($n, '$');
		$b = trim($a, ',');
		$c = trim($b, '%');
		return $c;
	}

	public function hardmoneyprogram(){

		if(isset($_POST['submit'])){

			$data['contact_id'] 		= $this->input->post('contact_id');
			$data['hm_company'] 		= $this->input->post('hm_company');
			$data['hm_brodge_loan'] 	= $this->input->post('hm_brodge_loan');
			$data['hm_b_ltv'] 			= $this->amount_format($this->input->post('hm_b_ltv'));
			$data['hm_b_lmin'] 			= $this->amount_format($this->input->post('hm_b_lmin'));
			$data['hm_b_lmax'] 			= $this->amount_format($this->input->post('hm_b_lmax'));
			$data['hm_b_mterm'] 		= $this->input->post('hm_b_mterm');
			$data['hm_construct_loan'] 	= $this->input->post('hm_construct_loan');
			$data['hm_c_ltv'] 			= $this->amount_format($this->input->post('hm_c_ltv'));
			$data['hm_c_lmin'] 			= $this->amount_format($this->input->post('hm_c_lmin'));
			$data['hm_c_lmax'] 			= $this->amount_format($this->input->post('hm_c_lmax'));
			$data['hm_c_mterm'] 		= $this->input->post('hm_c_mterm');
			$data['hm_fixflip'] 		= $this->input->post('hm_fixflip');
			$data['hm_ff_ltv'] 			= $this->amount_format($this->input->post('hm_ff_ltv'));
			$data['hm_ff_lmin'] 		= $this->amount_format($this->input->post('hm_ff_lmin'));
			$data['hm_ff_lmax'] 		= $this->amount_format($this->input->post('hm_ff_lmax'));
			$data['hm_ff_mterm'] 		= $this->input->post('hm_ff_mterm');
			
			$hm_rowid = $this->input->post('hm_rowid');

			if($hm_rowid == 'new'){

				$this->User_model->insertdata('hard_money_program', $data);
				$this->session->set_flashdata('success', 'Hard money program added successfully!');
				
			}else{

				$this->User_model->updatedata('hard_money_program', array('id'=>$hm_rowid), $data);
				$this->session->set_flashdata('success', 'Hard money program updated successfully!');
			}
			redirect(base_url(). 'viewcontact/'.$this->input->post('contact_id'));
		}
	}



}
?>