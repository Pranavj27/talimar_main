<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjob extends CI_Controller{

	public function __construct() {
        parent::__construct();

		$this->load->database();
		$this->load->model('User_model');
		$this->load->helper('url');
		//$this->load->library('session');
		$this->load->library('email');
		$this->load->library('MYPDF');
		$this->load->library('Aws3','aws3');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');
		  
		ini_set("memory_limit", "-1"); 
    }
	
	public function index()
	{
		echo 'test mail';
		
	}


	public function daily_reports(){
		
		//-----------------------------------------------------
		//
		//	cron-job details: https://database.talimarfinancial.com/Cronjob/daily_reports
		//	
		//	check cpanel for cronjob details.
		//	
		//  curl --silent https://database.talimarfinancial.com/Cronjob/daily_reports
		//---------------------------------------------------------

		error_reporting(0);

		$position_option = $this->config->item('position_option');

		$all_loan = $this->User_model->query("select *,l.borrower as borrower_id,l.id as loan_id from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status = '1' ORDER BY l.loan_funding_date ASC");
		$all_loan = $all_loan->result();

		$count = 0;
		$portfolio_bal = 0;
		$intrest_rate = 0;
		$term_month = 0;
		$ltv = 0;
		$intrest_ratesss = 0;

		foreach($all_loan as $row){

			$talimar_loan = $row->talimar_loan;
			$borrower_id = $row->borrower_id;
			$market_trust_deed = $row->market_trust_deed;
			$intrest_ratesss = number_format($row->intrest_rate,3);
			$position = $row->position;
			
			$property = $this->User_model->select_where('loan_property',array('talimar_loan'=>$talimar_loan));
			$property = $property->result();
			$full_address = $property[0]->property_address.' '.$property[0]->unit.'<br>'.$property[0]->city.', '.$property[0]->state.' '.$property[0]->zip;

			$val = $property[0]->underwriting_value;


			$borrower_name = $this->User_model->select_where('borrower_data', array('id'=>$borrower_id));
			$borrower_name = $borrower_name->result();
			$b_name = $borrower_name[0]->b_name;


			//for Portfolio Overview only
			//$underwriting_value = $this->User_model->select_where('loan_property', array("talimar_loan"=>$row->talimar_loan));
			//$underwriting_value = $underwriting_value->result();
			//$val = $underwriting_value[0]->underwriting_value;

			$ecum = $this->User_model->query("SELECT SUM(`current_balance`) as total_bal FROM `encumbrances` WHERE `talimar_loan`='".$row->talimar_loan."' AND `property_home_id` !=''");
			$ecum = $ecum->result();
			$total_ecum = $ecum[0]->total_bal;

			$each_loan_ltv = ($row->loan_amount + $total_ecum)/$val*100;

			$ltv += ($row->loan_amount + $total_ecum)/$val*100;

			$count++;
			$portfolio_bal += $row->loan_amount;
			$intrest_rate += number_format($row->intrest_rate,3);
			$term_month += $row->term_month;


			$sum_invest = $this->User_model->query("Select SUM(investment) as total_investment from loan_assigment where talimar_loan = '".$talimar_loan."'");
			$sum_invest = $sum_invest->result();
			$sum_invest_amount = $sum_invest[0]->total_investment;

			$all_pipeline_info[] = array(

											"b_name" 		=> $b_name,
											"full_address" 	=> $property[0]->property_address,
											"loan_id" 		=> $row->loan_id,
											"loan_amount" 	=> $row->loan_amount,
											"closing_date"  => $row->loan_funding_date,
											"intrest_rate"  => number_format($intrest_rate,3),
											"intrest_ratesss"  => number_format($intrest_ratesss,3),
											"each_loan_ltv" => $each_loan_ltv,
											"market_trust_deed"  	=> $market_trust_deed,
											"sum_invest_amount"  	=> $sum_invest_amount,
											"closing_status"  		=> $row->closing_status,
											"position"  			=> $position,

										);


			$all_pipeline_info_new[$row->closing_status][] = array(

											"b_name" 		=> $b_name,
											"full_address" 	=> $property[0]->property_address,
											"loan_id" 		=> $row->loan_id,
											"loan_amount" 	=> $row->loan_amount,
											"closing_date"  => $row->loan_funding_date,
											"intrest_rate"  => number_format($intrest_rate,3),
											"intrest_ratesss"  => number_format($intrest_ratesss,3),
											"each_loan_ltv" => $each_loan_ltv,
											"market_trust_deed"  	=> $market_trust_deed,
											"sum_invest_amount"  	=> $sum_invest_amount,
											"closing_status"  		=> $row->closing_status,
											"position"  			=> $position,

										);
		}
		$daily_pipeline_report = $all_pipeline_info;
		$daily_pipeline_report_new = $all_pipeline_info_new;



		$position_option = $this->config->item('position_option');

		$all_loan = $this->User_model->query("select *,l.borrower as borrower_id,l.id as loan_id from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status ='6' ORDER BY l.loan_funding_date ASC");
		$all_loan = $all_loan->result();

		$count = 0;
		$portfolio_bal = 0;
		$intrest_rate = 0;
		$term_month = 0;
		$ltv = 0;
		$intrest_ratesss = 0;

		foreach($all_loan as $row){

			$talimar_loan = $row->talimar_loan;
			$borrower_id = $row->borrower_id;
			$market_trust_deed = $row->market_trust_deed;
			$intrest_ratesss = number_format($row->intrest_rate,3);
			$position = $row->position;
			
			$property = $this->User_model->select_where('loan_property',array('talimar_loan'=>$talimar_loan));
			$property = $property->result();
			$full_address = $property[0]->property_address.' '.$property[0]->unit.'; '.$property[0]->city.', '.$property[0]->state.' '.$property[0]->zip;

			$val = $property[0]->underwriting_value;


			$borrower_name = $this->User_model->select_where('borrower_data', array('id'=>$borrower_id));
			$borrower_name = $borrower_name->result();
			$b_name = $borrower_name[0]->b_name;


			//for Portfolio Overview only
			//$underwriting_value = $this->User_model->select_where('loan_property', array("talimar_loan"=>$row->talimar_loan));
			//$underwriting_value = $underwriting_value->result();
			//$val = $underwriting_value[0]->underwriting_value;



			$ecum = $this->User_model->query("SELECT *,SUM(`current_balance`) as total_bal FROM `encumbrances` WHERE `talimar_loan`='".$row->talimar_loan."' AND `property_home_id` !=''");
			$ecum = $ecum->result();
			$total_ecum = $ecum[0]->total_bal;
            

											if($position == '2'){
												
											//	$loan_to_value = (($row->loan_amount + $total_ecum) / $val) * 100;
										$loan_to_value = ($row->loan_amount / $val) * 100;
												
											}else{
										
												$loan_to_value = ($row->loan_amount / $val) * 100;
											}
										$loan_to_value = is_nan($loan_to_value) ? 0 : $loan_to_value;	







			$each_loan_ltv =$loan_to_value;

			$ltv += ($row->loan_amount + $total_ecum)/$val*100;

			$count++;
			$portfolio_bal += $row->loan_amount;
			$intrest_rate += number_format($row->intrest_rate,3);
			$term_month += $row->term_month;


			$sum_invest = $this->User_model->query("Select SUM(investment) as total_investment from loan_assigment where talimar_loan = '".$talimar_loan."'");
			$sum_invest = $sum_invest->result();
			$sum_invest_amount = $sum_invest[0]->total_investment;

			$all_pipeline_infooo[] = array(

											"b_name" 		=> $b_name,
											"full_address" 	=> $full_address,
											"loan_id" 		=> $row->loan_id,
											"loan_amount" 	=> $row->loan_amount,
											"closing_date"  => $row->loan_funding_date,
											"intrest_rate"  => number_format($intrest_rate,3),
											"intrest_ratesss"  => number_format($intrest_ratesss,3),
											"each_loan_ltv" => $each_loan_ltv,
											"market_trust_deed"  	=> $market_trust_deed,
											"sum_invest_amount"  	=> $sum_invest_amount,
											"closing_status"  		=> $row->closing_status,
											"term_sheet_status"     =>$row->term_sheet_status,
											"position"  			=> $position,

										);


		}
	
		$daily_pipeline_report_newss = $all_pipeline_infooo;
		//---------------------------------------------------
		//	Portfolio Overview Daily Reports
		//----------------------------------------------------
		//$loan_hold = 'SELECT COUNT(*) as count_holded, SUM(loan.loan_amount) as holded_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = "2" AND loan_servicing.board_complete ="2"';
		$loan_hold = 'SELECT COUNT(*) as count_holded, SUM(loan.loan_amount) as holded_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan JOIN input_datas as ida ON ida.talimar_loan=loan_servicing.talimar_loan WHERE loan_servicing.loan_status = "2" AND ida.input_name="checklist_complete" AND ida.page="loan_servicing_checklist" AND ida.value !="1"';
		$loan_hold_fetch = $this->User_model->query($loan_hold);
		$loan_hold_fetch = $loan_hold_fetch->result();


		$count_loan_hold_fetch = $loan_hold_fetch[0]->count_holded;
		$sum_loan_hold_fetch = $loan_hold_fetch[0]->holded_amount;		

		$p_count 			= $count;
		$ltv				= $ltv;
		$p_portfolio_bal 	= $portfolio_bal;
		$p_intrest_rate 	= number_format($intrest_rate,3);
		$p_term_month 		= $term_month;


		//---------------------------------------------------
		//	Loan Assignment Daily Reports
		//----------------------------------------------------

		$sql_join = "SELECT *,loan.id as loan_id,loan.borrower as borrower_id FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status IN('1','2') ORDER BY loan.id DESC";
		$fetch_all_loan 		= $this->User_model->query($sql_join);
		$fetch_all_loan 		= $fetch_all_loan->result();
		foreach($fetch_all_loan as $row)
		{


			$talimar_loan 					= $row->talimar_loan;
			$loan_id 						= $row->loan_id;
			$loan_amount 					= $row->loan_amount;
			$borrower_id					= $row->borrower_id;
			
			$loan_property 	= $this->User_model->select_where('loan_property',array('talimar_loan'=>$talimar_loan));
			$loan_property  = $loan_property->result();
			$property  = $loan_property[0]->property_address;

			$borrower_names = $this->User_model->select_where('borrower_data', array('id'=>$borrower_id));
			$borrower_names = $borrower_names->result();
			$b_name = $borrower_names[0]->b_name;
			
			$all_assigment_data	= $this->User_model->query("SELECT * FROM `loan_assigment` WHERE `talimar_loan`='".$talimar_loan."'");
			$all_assigment_data = $all_assigment_data->result();
			$file_close = $all_assigment_data[0]->file_close;
			
			$total_investment = 0;
			foreach ($all_assigment_data as $key => $value) {
				$total_investment += $value->investment;
			}

			if($file_close !='1'){
			
				$all_assigment_result[] = array(

												'loan_id' 				=> $loan_id ,
												'property' 				=> $property,
												'b_name' 				=> $b_name,
												'loan_amount' 			=> $loan_amount,
												'investment' 			=> $total_investment,
												
											);
				
			}
			
			
		}

		$all_assigment_result	= $all_assigment_result;

		//---------------------------------------------------
		// Loan Servicing daily report
		//---------------------------------------------------

		$sql_servicing = $this->User_model->query("SELECT COUNT(*) as total, SUM(l.loan_amount) as amount FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.demand_requested = '1' AND ls.loan_status = '2' ORDER BY ls.demand_requested_date DESC");
		$sql_servicing = $sql_servicing->result();

		$count = $sql_servicing[0]->total;
		$amount = $sql_servicing[0]->amount;

		$loan_default = $this->User_model->query("SELECT COUNT(*) as total_count, SUM(loan.loan_amount) as total_amount FROM `loan`JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '2' AND loan_servicing.condition = '1'");
		$loan_default = $loan_default->result();

		$total_count = $loan_default[0]->total_count;
		$total_amount = $loan_default[0]->total_amount;

		//---------------------------------------------------
		//	Investor Figures Daily Reports
		//----------------------------------------------------

		$loan_investor = $this->User_model->query("SELECT COUNT(*) as total,SUM(la.investment) as amount FROM `loan_assigment` as la JOIN loan_servicing as ls ON la.talimar_loan = ls.talimar_loan where ls.loan_status = '2'");
		$loan_investor = $loan_investor->result();

		$total_counts = $loan_investor[0]->total;
		$total_amounts = $loan_investor[0]->amount;




					$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

						// set document information
						$pdf->SetCreator(PDF_CREATOR);
						
						// set default header data
						 // $pdf->SetHeaderData("include/icons/logo-light.png", 30, '', '');

						// set header and footer fonts
						$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
						$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

						// set default monospaced font
						$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

						// set margins
						$pdf->SetMargins(2, 20, 2);
						// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
						$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
						$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

						// set auto page breaks
						$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

						// set image scale factor
						$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
						
						$pdf->SetFont('times', '', 7);

						// add a page
						$pdf->AddPage('L', 'A4');
						// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
						$message ='<h1 style="color:#003468;font-size:18px;"> Daily Report: '.date('m-d-Y').'</h1>';
						$message .= '<style>
						.table {
							width: 100%;
							max-width: 100%;
							margin-bottom: 20px;
							border-collapse:collapse;
						}
						.table-bordered {
						border: 1px solid #ddd;
						}
						table {
							border-spacing: 0;
							border-collapse: collapse;
						}
						.table td{
							height : 25px;
							font-size:12px;
						}
						tr.table_header td{
							height:25px;
							font-size:12px;
							font-family: "Times New Roman", Georgia, Serif;
							text-align:center;
							background-color:#bfcfe3;
							text-decoration:underline;

						}
						
						tr.table_bottom td{
							font-size:12px;
							font-family: "Times New Roman", Georgia, Serif;
							text-align:left;
							background-color:#bfcfe3;
						}
						tr.odd td{
							background-color:#ededed;
						}
						</style>
						';

		//--------------------------------------------------------
		// All Term sheet loans data
		//--------------------------------------------------------

		$termsheet_loans = $this->User_model->query("Select * from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status = '6'");
		$termsheet_loans = $termsheet_loans->result();

		$ts_count = 0;
		$ts_loan_amount = 0;
		$ts_loan_month = 0;
		$ts_ltv = 0;
		$ts_ecum = 0;
		$ts_val = 0;
		$ts_intrest_rate = 0;
		foreach($termsheet_loans as $ts_loan){

			$ts_count++;
			$ts_loan_amount += $ts_loan->loan_amount;

			$ts_loan_amount_single = $ts_loan->loan_amount;
			$ts_loan_month += $ts_loan->term_month;
			$ts_intrest_rate += number_format($ts_loan->intrest_rate,3);

			$underwriting_value = $this->User_model->query("SELECT SUM(`underwriting_value`) as total_val FROM `loan_property` WHERE `talimar_loan`='".$ts_loan->talimar_loan."'");
			$underwriting_value = $underwriting_value->result();
			$ts_val = $underwriting_value[0]->total_val;
          


			$ecum = $this->User_model->query("SELECT SUM(`current_balance`) as total_ecums FROM `encumbrances` WHERE `talimar_loan`= '".$ts_loan->talimar_loan."' AND `property_home_id` !=''");
			$ecum = $ecum->result();
			$ts_ecum = $ecum[0]->total_ecums;

			// $yield = $this->User_model->query("SELECT SUM(`invester_yield_percent`) as total_yeild FROM `loan_assigment` WHERE `talimar_loan`= '".$p_loan->talimar_loan."'");
			// $yield = $yield->result();
			// $total_yeilds += $yield[0]->total_yeild;

			//$ts_ltvs += (($ts_loan_amount_single + $ts_ecum)/$ts_val)*100;
			
				if($ts_loan->position == '2'){

				$loan_to_value = (($ts_loan_amount_single + $ts_ecum) / $ts_val) * 100;

				}else{

				$loan_to_value = ($ts_loan_amount_single / $ts_val) * 100;
				}

				$loan_to_value = is_infinite($loan_to_value) ? 0 : $loan_to_value;


			$ts_ltvs +=$loan_to_value;


		}



		//--------------------------------------------------------
		// All pipeline loans data
		//--------------------------------------------------------

		$pipeline_loans = $this->User_model->query("Select * from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status = '1' AND ls.closing_status!='0'");
		$pipeline_loans = $pipeline_loans->result();

		$p_count = 0;
		$p_loan_amount = 0;
		$p_loan_month = 0;
		$ltv = 0;
		$total_ecum = 0;
		$val = 0;
		$intrest_rate = 0;
		$sum_broker = 0;
		$avaliable_bal_pipe = 0;
		foreach($pipeline_loans as $p_loan){



			$p_count++;
			$p_loan_amount += $p_loan->loan_amount;
			$p_loan_amount_single = $p_loan->loan_amount;
			$p_loan_month += $p_loan->term_month;
			$intrest_rate += number_format($p_loan->intrest_rate,3);

		  	$sum_investmm = $this->User_model->query("Select SUM(investment) as total_investmentt from loan_assigment where talimar_loan = '".$p_loan->talimar_loan."'");
			$sum_investt = $sum_investmm->result();
			$s_invest_amount = $sum_investt[0]->total_investmentt;
			$avaliable_bal_pipe =$avaliable_bal_pipe +($p_loan->loan_amount-$s_invest_amount);
		//echo $f = $p_loan->talimar_loan.'<br>';
		

			$sqld = "SELECT * FROM closing_statement_items WHERE talimar_loan='".$p_loan->talimar_loan."' ";
			
			$fetch_sum_closing_statement_item = $this->User_model->query($sqld);
			$fetch_sum_closing_statement_item = $fetch_sum_closing_statement_item->result(); 
			
			foreach($fetch_sum_closing_statement_item as $d){
			
          
           $sum_broker +=$d->paid_to_broker; 
			

						}
			
			$underwriting_value = $this->User_model->query("SELECT SUM(`underwriting_value`) as total_val FROM `loan_property` WHERE `talimar_loan`='".$p_loan->talimar_loan."'");
			$underwriting_value = $underwriting_value->result();
			$val = $underwriting_value[0]->total_val;

			$ecum = $this->User_model->query("SELECT SUM(`current_balance`) as total_ecums FROM `encumbrances` WHERE `talimar_loan`= '".$p_loan->talimar_loan."' AND `property_home_id` !=''");
			$ecum = $ecum->result();
			$total_ecum = $ecum[0]->total_ecums;

			// $yield = $this->User_model->query("SELECT SUM(`invester_yield_percent`) as total_yeild FROM `loan_assigment` WHERE `talimar_loan`= '".$p_loan->talimar_loan."'");
			// $yield = $yield->result();
			// $total_yeilds += $yield[0]->total_yeild;

			$ltvs += ($p_loan_amount_single + $total_ecum)/$val*100;

		}

		//--------------------------------------------------------
		// All Active loans data
		//--------------------------------------------------------

		$active_loans = $this->User_model->query("Select * from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status = '2'");
		$active_loans = $active_loans->result();

		$a_count = 0;
		$a_loan_amount = 0;
		$afff_loan_amount = 0;
		$a_loan_month = 0;
		$a_ltv = 0;
		$a_total_ecum = 0;
		$a_val = 0;
		$a_intrest_rate = 0;
		$avaliable_bal_active = 0;

		$count_active_loans = 0;
		$total_new_balance = 0;
		foreach($active_loans as $p_loan){

			$a_count++;
			$afff_loan_amount += $p_loan->loan_amount;
			$a_loan_amount += $p_loan->current_balance;
			$a_loan_amount_single = $p_loan->loan_amount;
			$a_loan_month += $p_loan->term_month;
			$a_intrest_rate += number_format($p_loan->intrest_rate,3);

			$activesum_investmm = $this->User_model->query("Select SUM(investment) as total_investmen from loan_assigment where talimar_loan = '".$p_loan->talimar_loan."'");
			$active_sum_inves = $activesum_investmm->result();

			

			$actives_invest_amount = $active_sum_inves[0]->total_investmen;
			$avaliable_bal_active=$avaliable_bal_active+($p_loan->loan_amount-$actives_invest_amount);

			//echo $p_loan->loan_amount .' - '.$actives_invest_amount .'<br>';
			
			$available_amont_check = $p_loan->loan_amount - $actives_invest_amount;

			if($available_amont_check > 0){

				//echo $available_amont_check.' - '.$p_loan->talimar_loan.'<br>';
				$count_active_loans++;
				$total_new_balance += $available_amont_check;
			}
			


			$underwriting_value = $this->User_model->query("SELECT SUM(`underwriting_value`) as total_val FROM `loan_property` WHERE `talimar_loan`='".$p_loan->talimar_loan."'");
			$underwriting_value = $underwriting_value->result();
			$a_val = $underwriting_value[0]->total_val;

			$ecum = $this->User_model->query("SELECT SUM(`current_balance`) as total_ecums FROM `encumbrances` WHERE `talimar_loan`= '".$p_loan->talimar_loan."' AND `property_home_id` !=''");
			$ecum = $ecum->result();
			$a_total_ecum = $ecum[0]->total_ecums;

			// $yield = $this->User_model->query("SELECT SUM(`invester_yield_percent`) as total_yeild FROM `loan_assigment` WHERE `talimar_loan`= '".$p_loan->talimar_loan."'");
			// $yield = $yield->result();
			// $a_total_yeilds += $yield[0]->total_yeild;

			$a_livs += ($a_loan_amount_single + $a_total_ecum)/$a_val*100;

		}

		//--------------------------------------------------------
		// All Paid-Off loans data
		//--------------------------------------------------------

		$paid_loans = $this->User_model->query("Select * from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status = '3'");
		$paid_loans = $paid_loans->result();

		$po_count = 0;
		$po_loan_amount = 0;
		$po_loan_month = 0;
		$po_ltv = 0;
		$po_total_ecum = 0;
		$po_val = 0;
		$po_intrest_rate = 0;
		foreach($paid_loans as $p_loan){

			$po_count++;
			$po_loan_amount += $p_loan->loan_amount;
			$po_loan_month += $p_loan->term_month;
			$po_intrest_rate += number_format($p_loan->intrest_rate,3);
			
			$po_loan_amount_single= $p_loan->loan_amount;
			$po_loan_position = $p_loan->position;

			$underwriting_value = $this->User_model->query("SELECT SUM(`underwriting_value`) as total_val FROM `loan_property` WHERE `talimar_loan`='".$p_loan->talimar_loan."'");
			$underwriting_value = $underwriting_value->result();
			$po_val = $underwriting_value[0]->total_val;
			

			$ecum = $this->User_model->query("SELECT SUM(`current_balance`) as total_ecums FROM `encumbrances` WHERE `talimar_loan`= '".$p_loan->talimar_loan."' AND `property_home_id` !=''");
			$ecum = $ecum->result();
			$po_total_ecum = $ecum[0]->total_ecums;

			// $yield = $this->User_model->query("SELECT SUM(`invester_yield_percent`) as total_yeild FROM `loan_assigment` WHERE `talimar_loan`= '".$p_loan->talimar_loan."'");
			// $yield = $yield->result();
			// $po_total_yeilds += $yield[0]->total_yeild;
			
			$fetch_encumbrance_totals1  = $this->User_model->query("SELECT * FROM ecumbrances_totals WHERE talimar_loan = '".$p_loan->talimar_loan ."' ");
			if($fetch_encumbrance_totals1->num_rows() > 0)
			{
				$fetch_encumbrance_totals1 	= $fetch_encumbrance_totals1->row();
				$seniorr_sum_current = $fetch_encumbrance_totals1->e_total_senior_plus_talimar_current;
			}
			else
			{
				$seniorr_sum_current = '0.00';
				
			}
			
			$uv_value	  = $underwriting_value[0]->uv_value ? $fetch_property_data[0]->uv_value : 0;
			
			//fetch underwriting_value and property_address...
			$property_data = "SELECT sum(`underwriting_value`) as uv_value,property_address FROM `loan_property` WHERE `talimar_loan`='".$p_loan->talimar_loan."'";
			
			$fetch_property_data  	= $this->User_model->query($property_data);
			$fetch_property_data 	= $fetch_property_data->result();
			$property_address       = $fetch_property_data[0]->property_address;
			$uv_value			    = $fetch_property_data[0]->uv_value;
			$uv_value			    = $fetch_property_data[0]->uv_value ? $fetch_property_data[0]->uv_value : 0;
			
			//fetch property home id...
			$select_property_home		= $this->User_model->select_where('property_home',array('talimar_loan'=>$p_loan->talimar_loan));
			$select_property_home 		= $select_property_home->result();
			$select_property_home 		= $select_property_home[0]->id;
			
			//fetch sum of emcumbrances current value...
			$sql = "SELECT sum(`current_balance`) as total FROM `encumbrances` WHERE talimar_loan ='".$p_loan->talimar_loan."' AND `property_home_id`='".$select_property_home ."'";
		
			$select_ecum 			= $this->User_model->query($sql);
			$select_ecum 			= $select_ecum->result();
			$select_ecum 			= $select_ecum[0]->total ? $select_ecum[0]->total : 0;
			
			// $calculate_ltv = ($seniorr_sum_current/$property_arv)*100; // SHOW IN PAIDOFF REPORT
			
			if($po_loan_position == 2)
			{
				$calculate_ltv = (($po_loan_amount_single + $select_ecum) / $uv_value )*100;
			}
			else
			{
				$calculate_ltv = ($po_loan_amount_single / $uv_value)*100;
			}
			
			$calculate_ltv = (is_infinite($calculate_ltv) || is_nan($calculate_ltv)) ? 0 : $calculate_ltv;
			
			// $calculate_ltv = (($po_loan_amount_single + $select_ecum) / $po_val )*100;

			// $po_ltvs += ($po_loan_amount_single + $po_total_ecum)/$po_val*100;
			$po_ltvs += $calculate_ltv;
		}


		$message .='<h2 style="width:100%;background-color:#efefef;padding:5px;"><strong>Portfolio Overview:</strong></h2>';
		$message .='<table class="table table-bordered th_text_align_center">
							
								<tr class="table_header">
									<td><strong>Status</strong></td>
									<td><strong># of Loans</strong></td>
									<td><strong>Loan Balance</strong></td>
									<td><strong>Available Balance</strong></td>
									<td><strong>Avg. Balance</strong></td>
									<td><strong>Avg. LTV </strong></td>
									<td><strong>Average Rate</strong></td>
									<td><strong>Avg. Term</strong></td>
								</tr>
						
							<tbody>';
			
			
			//echo 'ltv: '.$ltvs/$p_count.'<br>';
			//echo 'ltv: '.$a_livs/$a_count.'<br>';
			//echo 'ltv: '.$po_ltvs/$po_count.'<br>';
		 	//$ltv = ($p_loan_amount + $total_ecum)/$val*100;	
		 	//$a_ltv = ($a_loan_amount + $a_total_ecum)/$a_val*100;	
		 	//$po_ltv = ($po_loan_amount + $po_total_ecum)/$po_val*100;	

						
										// if(is_nan($ts_ltvs/$ts_count)){

										// 	$avg_term=0;	
										// }else{

										// 	$avg_term=$ts_ltvs/$ts_count;	
										// }

			// $message .='<tr class="odd">
			// 					<td>Term Sheet</td>
			// 					<td>'.$ts_count.'</td>
			// 					<td>$'.number_format($ts_loan_amount).'</td>
			// 					<td>N/A</td>
			// 					<td>$'.number_format(is_nan($ts_loan_amount/$ts_count) ? 0: $ts_loan_amount/$ts_count).'</td>
			// 					<td>'.number_format(is_nan($ts_ltvs/$ts_count) ? 0:$ts_ltvs/$ts_count,2).'%</td>
			// 					<td>'.number_format(is_nan($ts_intrest_rate/$ts_count) ? 0 :$ts_intrest_rate/$ts_count,2).'%</td>
			// 					<td>'.number_format(is_nan($ts_loan_month/$ts_count) ? 0:$ts_loan_month/$ts_count).' Months</td>
								
			// 			</tr>';
			$message .='<tr>
								<td>Pipeline</td>
								<td>'.$p_count.'</td>
								<td>$'.number_format($p_loan_amount).'</td>
								<td>$'.number_format($avaliable_bal_pipe).'</td>
								<td>$'.number_format($p_loan_amount/$p_count).'</td>
								<td>'.number_format($ltvs/$p_count,2).'%</td>
								<td>'.number_format($intrest_rate/$p_count,3).'%</td>
								<td>'.number_format($p_loan_month/$p_count).' Months</td>
								
						</tr>';
			$message .='<tr class="odd">
								<td>Active</td>
								<td>'.$a_count.'</td>
								<td>$'.number_format($a_loan_amount).'</td>
								<td>$'.number_format($avaliable_bal_active).'</td>
								<td>$'.number_format($a_loan_amount/$a_count).'</td>
								<td>'.number_format($a_livs/$a_count,2).'%</td>
								<td>'.number_format($a_intrest_rate/$a_count,3).'%</td>
								<td>'.number_format($a_loan_month/$a_count).' Months</td>
						</tr>';
			$message .='<tr>
								<td>Paid Off</td>
								<td>'.$po_count.'</td>
								<td>$'.number_format($po_loan_amount).'</td>
								<td>N/A</td>
								<td>$'.number_format($po_loan_amount/$po_count).'</td>
								<td>'.number_format($po_ltvs/$po_count,2).'%</td>
								<td>'.number_format($po_intrest_rate/$po_count,3).'%</td>
								<td>'.number_format($po_loan_month/$po_count).' Months</td>
							</tr>';

						
			$message .='</tbody></table>';

		//-------------------------------------------------
		// Talimar Fees
		//-------------------------------------------------


		$sql_fee = "SELECT * FROM `loan_servicing` as ls JOIN loan_servicing_checklist as lsc ON ls.talimar_loan = lsc.talimar_loan WHERE ls.loan_status ='1' AND lsc.hud = '1014' AND lsc.checklist IN ('0','1')";
		$fetch_fees = $this->User_model->query($sql_fee);
		$fetch_fees = $fetch_fees->result();

		$fetch_total = 0;
		$fetch_total_count = 0;
		foreach($fetch_fees as $row){

			$talimar_loan = $row->talimar_loan;
			$term_sheet		= $row->term_sheet;

			if($term_sheet == 2){

				$fetch_total_count++;

				$closing = "SELECT SUM(`paid_to_broker`) as total FROM `closing_statement_items` WHERE `talimar_loan`='".$talimar_loan."'";
				$fetch_closing = $this->User_model->query($closing);
				$fetch_closing = $fetch_closing->result();
				$fetch_total	+= $fetch_closing[0]->total;
			}
		}

		//echo $fetch_total;
		//echo $fetch_total_count;

		$message .='<br>';

		$message .='<h2 style="width:100%;background-color:#efefef;padding:5px;"><strong>Pipeline (Term Sheet signed):</strong></h2>';
		
		$message .= '<table class="table">';
		
		// $message .= '<tr>';
		// $message .= '<td style="width:20%;"><strong># of Loans</strong></td>';
		// $message .= '<td style="width:15%;">'.$p_count.'</td>';
		// $message .= '<td style="width:20%;"><strong>$ of Pipeline</strong></td>';
		// $message .= '<td style="width:15%;">$'.number_format($p_loan_amount).'</td>';
	
		// $message .= '<td style="width:15%;"><strong>Avg. $</strong></td>';
		// $message .= '<td style="width:15%;">$'.number_format($p_loan_amount/$p_count).'</td>';
		// $message .= '</tr>';

		
		$message .= '<tr>';
		$message .= '<td style="width:20%;"><strong>Total Fees</strong></td>';
		$message .= '<td style="width:15%;">$'.number_format($sum_broker,2).'</td>';
		$message .= '<td style="width:20%;"><strong>Avg Fee / Loan:</strong></td>';
		$message .= '<td style="width:15%;">$'.number_format($sum_broker/$p_count,2).'</td>';

		$current_date   = date('Y-m-d');
		$after_five_day = date('Y-m-d', strtotime('+5 days'));

		$five_days_loans = $this->User_model->query("Select l.id, l.talimar_loan, l.loan_amount, ls.loan_status, ls.closing_status from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status = '1' AND ls.`closing_status` IN('2','3','4','5','6','8') AND l.loan_funding_date BETWEEN '".$current_date."' AND '".$after_five_day."'");

			if($five_days_loans->num_rows() > 0){

				$five_days_loans = $five_days_loans->result();

				foreach($five_days_loans as $row){

					$talimar_loan = $row->talimar_loan;

					$avilable = $this->User_model->query("SELECT SUM(`investment`) as total_investment FROM `loan_assigment` WHERE `talimar_loan` = '".$talimar_loan."'");
					$avilable = $avilable->result();
					$total_investment = $avilable[0]->total_investment;

					$avilable_grater_then_zero = $row->loan_amount - $total_investment;

					if($avilable_grater_then_zero > 0){

						$capital_requirements[] = array(

														"talimar_loan" 		=> $row->talimar_loan,
														"loan_amount" 		=> $row->loan_amount,
														"total_investment" 	=> $total_investment,

													);
					}
					
				}
				
			}
			
		$capital_requirements_fiveday = $capital_requirements;

		$loan_amount = 0;
		$total_invest = 0;
		foreach($capital_requirements_fiveday as $row){

			$loan_amount += $row['loan_amount'];
			$total_invest += $row['total_investment'];

		}



		$message .= '<td style="width:15%;"><strong>Capital Req (5 Days)</strong></td>';
	$message .= '<td style="width:15%;">$'.number_format($loan_amount - $total_invest).'</td>';
		$message .= '</tr>';


		//------------------------------------------------
		// for 5 days...
		//-------------------------------------------------

	

		// $message .= '<tr>';
		// $message .= '<td style="width:20%;"><strong></strong></td>';
		// $message .= '<td style="width:15%;">$'.number_format($loan_amount - $total_invest).'</td>';
		// // $message .= '<td style="width:20%;"><strong>Est. Month Fees</strong></td>';
		// // $message .= '<td style="width:15%;"></td>';
		// //$message .= '<td style="width:20%;"><strong>Capital Req (15 Days)</strong></td>';
		// //$message .= '<td style="width:15%;">$'.number_format($loan_amount_fifteen - $total_invest_fifteen).'</td>';
		// //$message .= '<td style="width:15%;"><strong>Capital Req (30 Days)</strong></td>';
		// //$message .= '<td style="width:15%;">$'.number_format($loan_amount_thirty - $total_invest_thirty).'</td>';
		// $message .= '</tr>';

		$message .= '</table>';

		$message .='<br>';
		

		
		$loan_pre_boarding = 'SELECT COUNT(*) as count, SUM(loan.loan_amount) as amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = "2" AND loan_servicing.loan_submited ="2"';
		$fetch_pre_boarding = $this->User_model->query($loan_pre_boarding);
		$fetch_pre_boarding = $fetch_pre_boarding->result();

		$count_pre_boarding_loan = $fetch_pre_boarding[0]->count;
		$amount_pre_boarding_loan = $fetch_pre_boarding[0]->amount;
		
		$both_loan_services_sql_with_bs = 'SELECT COUNT(*) as count_fci , SUM(loan.loan_amount) as loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = "2" AND loan_servicing.loan_submited ="1" AND loan_servicing.loan_board="2"';
				
		$both_loan_services_result_with_bs = $this->User_model->query($both_loan_services_sql_with_bs);
		$both_loan_services_result_with_bs = $both_loan_services_result_with_bs->result();




		$all_bording_count 				= $both_loan_services_result_with_bs[0]->count_fci;
		$all_bording_amount 			= $both_loan_services_result_with_bs[0]->loan_amount;
		

		$message .='<h2 style="width:100%;background-color:#efefef;padding:5px;"><strong>Loan Servicing:</strong></h2>';
		
		$message .= '<table class="table">';

		$avg_gross=$this->get_servicing_income();
		$servicng_income=$this->get_servicing_income();
		$avgt_spread=$this->avg_spread();

		$message .= '<tr>';
		$message .= '<td style="width:20%;"><strong>Loan Servicing Income</strong></td>';
		$message .= '<td style="width:15%;">$'.number_format($servicng_income['total_servicing_income'],2).'</td>';
		 $message .= '<td style="width:20%;"><strong>Avg Gross:</strong></td>';
		 $message .= '<td style="width:15%;">'.number_format($avgt_spread,2).'%</td>';
		 $message .= '<td style="width:15%;"><strong>Avg Net:</strong></td>';
		 $net_income=$this->get_servicing_income();
		 $total_net_spread=($net_income['total_servicing_income']*100*12)/$a_loan_amount;
		 $message .= '<td style="width:15%;">'.number_format($total_net_spread,2).'%</td>';

		$message .= '</tr>';

		$message .= '<tr>';
		$message .= '<td style="width:20%;"><strong># of Loans in Holding</strong></td>';
		$message .= '<td style="width:15%;">'.$count_loan_hold_fetch.'</td>';
		$message .= '<td style="width:20%;"><strong> $ of Loans in Holding</strong></td>';
		$message .= '<td style="width:15%;">$'.number_format($sum_loan_hold_fetch).'</td>';
		$message .= '<td style="width:15%;"><strong></strong></td>';
		$message .= '<td style="width:15%;"><strong></strong></td>';
		$message .= '</tr>';

		$maturity_count45 = 0;
		$maturity_total45 = 0;
		$maturity_data = $this->maturity_data();
		foreach ($maturity_data as $value) {
			$maturity_count45++;
			$maturity_total45 += $value['loan_amount'];
		}

		
		$message .= '<tr>';
		$message .= '<td style="width:20%;"><strong>45 Day Maturity #</strong></td>';
		$message .= '<td style="width:15%;">'.$maturity_count45.'</td>';
		$message .= '<td style="width:20%;"><strong>45 Day Maturity $</strong></td>';
		$message .= '<td style="width:15%;">$'.number_format($maturity_total45).'</td>';
		$message .= '<td style="width:15%;"><strong></strong></td>';
		$message .= '<td style="width:15%;"><strong></strong></td>';
		$message .= '</tr>';
		
		// $message .= '<tr>';
		// $message .= '<td style="width:20%;"><strong># of Loans Not Boarded</strong></td>';
		// $message .= '<td style="width:15%;">'.$all_bording_count.'</td>';
		// $message .= '<td style="width:20%;"><strong>$ of Loans No Boarded</strong></td>';
		// $message .= '<td style="width:15%;">$'.number_format($all_bording_amount).'</td>';
		// $message .= '<td style="width:15%;"><strong></strong></td>';
		// $message .= '<td style="width:15%;"><strong></strong></td>';
		// $message .= '</tr>';
		
		// $message .= '<tr>';
		// $message .= '<td><strong>Avg. Per Loan:</strong></td>';

		// $message .= '<td>$'.number_format($this->get_servicing_income()/$a_count,2).'</td>';
		// $message .= '</tr>';
		
		$message .= '<tr>';
		$message .= '<td><strong># of Upcoming Payoffs</strong></td>';
		$message .= '<td>'.$count.'</td>';
		$message .= '<td><strong>$ of Upcoming Payoffs</strong></td>';
		$message .= '<td>$'.number_format($amount).'</td>';
		$message .= '</tr>';
		
		$message .= '<tr>';
		$message .= '<td><strong># of Loans in Default</strong></td>';
		$message .= '<td>'.$total_count.'</td>';
		$message .= '<td><strong>$ of Loans in Default</strong></td>';
		$message .= '<td>$'.number_format($total_amount).'</td>';
		$message .= '<td><strong>% of Portfolio ($)</strong></td>';
		$message .= '<td>'.number_format(($total_amount/$a_loan_amount)*100,2).'%</td>';
		$message .= '</tr>';

		$message .= '<tr>';
		$message .= '<td><strong># of Loans Held by TaliMar</strong></td>';
		$message .= '<td>'.$count_active_loans.'</td>';
		$message .= '<td><strong>$ of Loans Held by TaliMar</strong></td>';
		$message .= '<td>$'.number_format($total_new_balance).'</td>';
		$message .= '<td></td>';
		$message .= '<td></td>';
		$message .= '</tr>';
		
		$message .= '</table>';
		
		//---------------------------------------
		// Count lender that are Trust deed investors
		//---------------------------------------

		$trust_deed = $this->User_model->query("SELECT * FROM contact_tags as ct JOIN contact as c ON c.contact_id = ct.contact_id WHERE ct.contact_specialty='16' GROUP BY c.contact_id");
		$trust_deed = $trust_deed->result();

		$count_trust_deed = count($trust_deed);

		$total_inve_amountss = 0;
		$total_lamountss = 0;
		foreach($trust_deed as $row){

			
			$lender_id = $row->lender_id;

			$all_loan_investor = $this->User_model->query("SELECT COUNT(*) as total,SUM(la.investment) as invest_amount,SUM(l.loan_amount) as loan_amount FROM `loan_assigment` as la JOIN loan as l ON l.talimar_loan = la.talimar_loan where la.lender_name = '".$lender_id."'");
			$all_loan_investor = $all_loan_investor->result();

			
			$total_inve_amountss += $all_loan_investor[0]->invest_amount;
			$total_lamountss += $all_loan_investor[0]->loan_amount;


		}
		
		$z_sql = "SELECT lc.contact_id as contact_id FROM `lender_contact` as lc JOIN contact_tags as ct ON lc.contact_id = ct.contact_id WHERE lc.contact_primary ='1' AND ct.contact_specialty = '16' group by lc.contact_id ";
		$fetch_funds_lender = $this->User_model->query($z_sql);
		$fetch_funds_lender = $fetch_funds_lender->result();
		foreach($fetch_funds_lender as $row)
		{
			$contact_ids_rows[] = $row->contact_id;
		}
		
		$implode_contact_id = implode(',',$contact_ids_rows);
		// echo $z_sql;
		
		
		$sql = "SELECT SUM(la.investment) as investment,lc.contact_id as contact_id FROM loan_assigment as la JOIN lender_contact as lc on lc.lender_id = la.lender_name JOIN loan_servicing as ls on la.talimar_loan = ls.talimar_loan WHERE ls.loan_status='2' AND lc.contact_id IN (".$implode_contact_id.") GROUP BY  lc.contact_id";
		
		$fetch_investment_contact = $this->User_model->query($sql);
		$fetch_investment_contact = $fetch_investment_contact->result();
		foreach($fetch_investment_contact as $row)
		{
			$contact_investment[$row->contact_id] = $row->investment;
		}
		// echo $sql;
		
		$fetch_committed_funds = $this->User_model->query('SELECT committed_funds,contact_id FROM lender_contact_type WHERE contact_id IN ('.$implode_contact_id.') GROUP BY contact_id');
		$fetch_committed_funds = $fetch_committed_funds->result();
		foreach($fetch_committed_funds as $row)
		{
			$contact_committed_funds[$row->contact_id] = $row->committed_funds;
		}
		
		foreach($contact_ids_rows as $row)
		{
			$calculation_capital[$row] = $contact_committed_funds[$row] - $contact_investment[$row];
		}
		
		
		
		$available_capital_fund = array_sum($calculation_capital);
		$active_lenders = $this->User_model->query("SELECT * FROM `loan_assigment` as la JOIN loan_servicing as ls ON la.talimar_loan = ls.talimar_loan JOIN loan as l ON l.talimar_loan=la.talimar_loan WHERE ls.loan_status = '2' GROUP BY la.lender_name");
		$active_lenderss = $active_lenders->result();

		$count_active_lenders = 0;
		$atotal_amount = 0;
	
		foreach($active_lenderss as $row){

			$count_active_lenders++;
			$atotal_amount +=$row->loan_amount;
		
		}

		$message .='<br>';
		$message .='<h2 style="width:100%;background-color:#efefef;padding:5px;"><strong>Investor Figures:</strong></h2>';
		
		$message .= '<table class="table">';
		
		$message .= '<tr>';
		$message .= '<td style="width:20%;"><strong># of TD Lender Contacts</strong></td>';
		$message .= '<td style="width:15%;">'.number_format($count_trust_deed).'</td>';
		//$message .= '<td style="width:20%;"><strong>Available Funds</strong></td>';
		//$message .= '<td style="width:15%;">$'.number_format($available_capital_fund).'</td>';
		$message .= '<td style="width:20%;"><strong># of Lender Accounts</strong></td>';
		$message .= '<td style="width:15%;">'.number_format($total_counts).'</td>';
		$message .= '<td style="width:15%;"><strong># of Active Lender Accounts</strong></td>';
		$message .= '<td style="width:15%;">'.$count_active_lenders.'</td>';
		$message .= '</tr>';

		//-------------------------------
		// fetch Active Lenders
		//-------------------------------

		$fetch_all_active = $this->User_model->query("select count(*) as ax_count from loan_servicing where loan_status='2'");
		$fetch_all_active = $fetch_all_active->result();

		$lender_count_sql = 'SELECT COUNT(*) as lender_count FROM loan_assigment JOIN loan_servicing ON loan_servicing.talimar_loan = loan_assigment.talimar_loan WHERE loan_servicing.loan_status = "2" ';
		$lender_active_count_result = $this->User_model->query($lender_count_sql);
		$lender_active_count_result = $lender_active_count_result->result();
		
		$lender_count_active 			= $lender_active_count_result[0]->lender_count;


		$active_lenders = $this->User_model->query("SELECT * FROM `loan_assigment` as la JOIN loan_servicing as ls ON la.talimar_loan = ls.talimar_loan JOIN loan as l ON l.talimar_loan=la.talimar_loan WHERE ls.loan_status = '2' GROUP BY la.lender_name");
		$active_lenderss = $active_lenders->result();

		$count_active_lenders = 0;
		$atotal_amount = 0;
	
		foreach($active_lenderss as $row){

			$count_active_lenders++;
			$atotal_amount +=$row->loan_amount;
		
		}
		// echo $a_loan_amount;
   		  $av_cal=$lender_count_active/$fetch_all_active[0]->ax_count;
		$message .= '<tr>';
		$message .= '<td><strong># of Assigned Interests</strong></td>';
		$message .= '<td>'.$lender_count_active.'</td>';

		$message .= '<td><strong>Avg. Investment $</strong></td>';
		$message .= '<td>$'.number_format($total_amounts/$total_counts).'</td>';

		$message .= '<td style="width:15%;"><strong>Avg # of Lenders/Loan</strong></td>';
		$message .= '<td style="width:15%;">'.number_format($av_cal,2).'</td>';
		
		$message .= '</tr>';
		
		$message .= '</table>';
		
		
		$message .='<br pagebreak="true" />';

		$message .='<h2 style="width:100%;background-color:#efefef;padding:5px;"><strong>Pipeline:</strong></h2>';
		$message .='<table class="table table-bordered th_text_align_center">
							
								<tr class="table_header">
									<td style="width:20%"><strong>Borrower Name</strong></td>
									<td style="width:20%"><strong>Property Address</strong></td>
									<td style="width:5%"><strong>Position</strong></td>
									<td style="width:10%"><strong>Loan Amount</strong></td>
									<td style="width:10%"><strong>Available Balance <br></strong></td>
									<td style="width:8%"><strong>Note Rate</strong></td>
									<td style="width:8%"><strong>LTV Ratio</strong></td>
									<td style="width:9%"><strong>Status</strong></td>
									<td style="width:10%"><strong>Closing Date</strong></td>
								</tr>
						
							<tbody>';
						$count = 0;
						$loan_amount = 0;
						$intrest_rate = 0;
						$each_loan_ltv = 0;
						$avlble_bal_total = 0;

						

							$array = array(6,8,5,4,3,9,2,1,7);
							foreach($array as $number)
							{

								if(isset($daily_pipeline_report_new[$number]))
								{

									foreach($daily_pipeline_report_new[$number] as $row)
									{

										// echo '<pre>';
										// print_r($row);
										// echo '</pre>';

										$daily_pipeline_report_news[]  = array(

															"b_name" 		=> $row['b_name'],
															"full_address" 	=> $row['full_address'],
															"loan_id" 		=> $row['loan_id'],
															"loan_amount" 	=> $row['loan_amount'],
															"closing_date"  => $row['closing_date'],
															"intrest_rate"  => number_format($row['intrest_rate'],3),
															"intrest_ratesss"  => number_format($row['intrest_ratesss'],3),
															"each_loan_ltv" => $row['each_loan_ltv'],
															"market_trust_deed"  	=> $row['market_trust_deed'],
															"sum_invest_amount"  	=> $row['sum_invest_amount'],
															"closing_status"  		=> $row['closing_status'],
															"position"  			=> $row['position'],

														);
									}
								}
							}

						// echo '<pre>';
						// print_r($daily_pipeline_report_news);
						// echo '</pre>';

						
						foreach($daily_pipeline_report_news as $row){



							$count++;
							$loan_amount += $row['loan_amount'];
							$intrest_rate += number_format($row['intrest_ratesss'],3);
							$each_loan_ltv += $row['each_loan_ltv'];
							$avlble_bal_total =$avlble_bal_total+($row['loan_amount']-$row['sum_invest_amount']);


							$class = 'even';
							if($count % 2 == 0){
								$class = 'even';
							}else{
								$class = 'odd';
							}
			$closing_status_option = $this->config->item('closing_status_option');
			$message .='<tr class='.$class.'>
								<td>'.$row['b_name'].'</td>
								<td>'.$row['full_address'].'</td>
								<td>'.$position_option[$row['position']].'</td>
								<td>$'.number_format($row['loan_amount']).'</td>
								<td>$'.number_format($row['loan_amount']-$row['sum_invest_amount']).'</td>
								<td>'.number_format($row['intrest_ratesss'],3).'%</td>
								<td>'.number_format($row['each_loan_ltv'],2).'%</td>	
								<td>'.$closing_status_option[$row['closing_status']].'</td>							
								<td>'.date('m-d-Y', strtotime($row['closing_date'])).'</td>
							</tr>';

						}

			$message .='<tr class="table_bottom">
									<td><strong>Total: '.$count.'</strong></td>
									<td><strong></strong></td>
									<td><strong></strong></td>
									<td><strong>$'.number_format($loan_amount).'</strong></td>
									<td><strong>$'.number_format($avlble_bal_total).'</strong></td>
									<td><strong></strong></td>
									<td><strong></strong></td>
									<td><strong></strong></td>
									<td><strong></strong></td>
								</tr>';
								
			$message .='<tr class="table_bottom">
									<td><strong>Average</strong></td>
									<td><strong></strong></td>
									<td><strong></strong></td>
									<td><strong></strong></td>
									<td><strong></strong></td>
									<td><strong>'.number_format($intrest_rate/$count,3).'%</strong></td>
									<td><strong>'.number_format($each_loan_ltv/$count,2).'%</strong></td>
									<td><strong></strong></td>
									<td><strong></strong></td>
								</tr>';
							
			$message .='</tbody>
			</table>';

			$message .='<br pagebreak="true" />';

			//Maturity Schedule data...
			$Maturity_Schedule_data = $this->Maturity_Schedule_data();

			$message .='<h2 style="width:100%;background-color:#efefef;padding:5px;"><strong>Maturity Schedule:</strong></h2>';
			$message .='<table class="table table-bordered th_text_align_center">
							
								<tr class="table_header">
									<td style="width:14%"><strong>Property Address</strong></td>
									<td style="width:13%"><strong>Borrower Name</strong></td>
									<td style="width:13%"><strong>Contact Name</strong></td>
									<td style="width:10%"><strong>Loan Amount</strong></td>
									<td style="width:5%"><strong>Note Rate</strong></td>
									<td style="width:10%"><strong>Position</strong></td>
									<td style="width:10%"><strong>Maturity Date</strong></td>
									<td style="width:7%"><strong>Days to Maturity</strong></td>
									<td style="width:8%"><strong>Extension Fee (%)</strong></td>
									<td style="width:10%"><strong>Extension Fee ($)</strong></td>
								</tr>
						
							<tbody>';

			$position_option = $this->config->item('position_option');
			array_multisort(array_column($Maturity_Schedule_data, 'duration'), SORT_ASC, $Maturity_Schedule_data);
			if(isset($Maturity_Schedule_data) && is_array($Maturity_Schedule_data)){

				$count = 0;
				$extension_feeTotal = 0;
				$loan_amountTotal = 0;
				foreach ($Maturity_Schedule_data as $value) {
					$count++;
					$class = 'even';
					if($count % 2 == 0){
						$class = 'even';
					}else{
						$class = 'odd';
					}

					if($value['extention_option'] == '1'){
						$extension_fee = '$' . number_format(($value['extention_percent_amount'] / 100) * $value['loan_amount'], 2);

						$extension_feessss = ($value['extention_percent_amount'] / 100) * $value['loan_amount'];
						$extension_feeTotal += $extension_feessss;
					}else{
						$extension_fee = 'N/A';
					}

					$loan_amountTotal += $value['loan_amount'];

					$message .='<tr class='.$class.'>
								<td>'.$value['full_address'].'</td>
								<td>'.$value['borrower_name'].'</td>
								<td>'.$value['b_contact_name'].'</td>
								
								<td>$'.number_format($value['loan_amount']).'</td>
								<td>'.number_format($value['intrest_rate'],3).'%</td>
								<td>'.$position_option[$value['position']].'</td>
								<td>'.date('m-d-Y', strtotime($value['maturity_date'])).'</td>
								<td>'.$value['duration'].'</td>
								<td>'.number_format($value['extention_percent_amount'],2).'%</td>
								<td>'.$extension_fee.'</td>
								
						</tr>';
				}

				$message .='<tr class="table_bottom">
									<td><strong>Total: '.$count.'</strong></td>
									<td><strong></strong></td>
									<td><strong></strong></td>
									<td><strong></strong></td>
									
									<td><strong></strong></td>
									<td><strong></strong></td>
									<td><strong></strong></td>
									<td><strong></strong></td>
									<td><strong></strong></td>
									<td><strong>$'.number_format($extension_feeTotal).'</strong></td>
								</tr>';

				$message .='<tr class="table_bottom">
									<td><strong>Average</strong></td>
									<td><strong></strong></td>
									
									<td><strong></strong></td>
									<td><strong>'.number_format($loan_amountTotal/$count,2).'%</strong></td>
									<td><strong></strong></td>
									<td><strong></strong></td>
									<td><strong></strong></td>
									<td><strong></strong></td>
									<td><strong></strong></td>
									<td><strong>'.number_format($extension_feeTotal/$count,2).'%</strong></td>
								</tr>';

			}else{

				$message .='<tr>
									<td colspan="10">No data found!</td>
							</tr>';
			}

			$message .='</tbody></table>';

			$message .='<br pagebreak="true"/>';



		// 	$message .='<h2 style="width:100%;background-color:#efefef;padding:5px;"><strong>Term Sheet:</strong></h2>';
		// $message .='<table class="table table-bordered th_text_align_center">
							
		// 						<tr class="table_header">
		// 							<td style="width:20%"><strong>Borrower Name</strong></td>
		// 							<td style="width:20%"><strong>Property Address</strong></td>
		// 							<td style="width:5%"><strong>Position</strong></td>
		// 							<td style="width:10%"><strong>Loan Amount</strong></td>
		// 							<td style="width:10%"><strong>Available Balance <br></strong></td>
		// 							<td style="width:8%"><strong>Note Rate</strong></td>
		// 							<td style="width:8%"><strong>LTV Ratio</strong></td>
		// 							<td style="width:9%"><strong>Term Sheet</strong></td>
		// 							<td style="width:10%"><strong>Closing Date</strong></td>
		// 						</tr>
						
		// 					<tbody>';
						
		// 				$count = 0;
		// 				$loan_amount = 0;
		// 				$intrest_rate = 0;
		// 				$each_loan_ltv = 0;
		// 				$total_t_avble_bals=0;

						
		// 				foreach($daily_pipeline_report_newss as $row){



		// 					$count++;
		// 					$loan_amount += $row['loan_amount'];
		// 					$intrest_rate += $row['intrest_ratesss'];
		// 					$each_loan_ltv += $row['each_loan_ltv'];
		// 					$total_t_avble_bals=$total_t_avble_bals+($row['loan_amount']-$row['sum_invest_amount']);


		// 					$class = 'even';
		// 					if($count % 2 == 0){
		// 						$class = 'even';
		// 					}else{
		// 						$class = 'odd';		
		// 					}
		// 						$closing_status_option = $this->config->item('closing_status_option');
		// 						$term_sheet_option = $this->config->item('term_sheet');
		// 						$message .='<tr class='.$class.'>
		// 						<td>'.$row['b_name'].'</td>
		// 						<td>'.$row['full_address'].'</td>
		// 						<td>'.$position_option[$row['position']].'</td>
		// 						<td>$'.number_format($row['loan_amount']).'</td>
		// 						<td>$'.number_format($row['loan_amount']-$row['sum_invest_amount']).'</td>
		// 						<td>'.number_format($row['intrest_ratesss'],2).'%</td>
		// 						<td>'.number_format($row['each_loan_ltv'],2).'%</td>	
		// 						<td>'.$term_sheet_option[$row['term_sheet_status']].'</td>							
		// 						<td>'.date('m-d-Y', strtotime($row['closing_date'])).'</td>
		// 					</tr>';

		// 				}

		// 	$message .='<tr class="table_bottom">
		// 							<td><strong>Total: '.$count.'</strong></td>
		// 							<td><strong></strong></td>
		// 							<td><strong></strong></td>
		// 							<td><strong>$'.number_format($loan_amount).'</strong></td>
		// 							<td><strong>$'.number_format($total_t_avble_bals).'</strong></td>
		// 							<td><strong></strong></td>
		// 							<td><strong></strong></td>
		// 							<td><strong></strong></td>
		// 							<td><strong></strong></td>
		// 						</tr>';
								
		// 	$message .='<tr class="table_bottom">
		// 							<td><strong>Average</strong></td>
		// 							<td><strong></strong></td>
		// 							<td><strong></strong></td>
		// 							<td><strong></strong></td>
		// 							<td><strong></strong></td>
		// 							<td><strong>'.number_format($intrest_rate/$count,2).'%</strong></td>
		// 							<td><strong>'.number_format($each_loan_ltv/$count,2).'%</strong></td>
		// 							<td><strong></strong></td>
		// 							<td><strong></strong></td>
		// 						</tr>';
							
		// 	$message .='</tbody>
		// 			</table>';
		// 	$message .='<br pagebreak="true"/>';


	//Top 10 lenders by active balance...

	$fetch_top_lenders = $this->User_model->query("SELECT id,name from investor");
	$fetch_top_lenders = $fetch_top_lenders->result();

	foreach($fetch_top_lenders as $row){

        $investor_id = $row->id;

        $sql13 = "SELECT contact_id FROM lender_contact WHERE lender_id = '".$investor_id."' AND contact_primary='1'";
        $contact_fetch       = $this->User_model->query($sql13);  
        $contact_fetch       = $contact_fetch->result();

        $sql_con=$this->User_model->query("select contact_firstname,contact_middlename,contact_lastname,contact_id from contact where contact_id='".$contact_fetch[0]->contact_id."'");
        $sql_con       = $sql_con->result();
        $contact_name   = $sql_con[0]->contact_firstname .' '.$sql_con[0]->contact_middlename .' '.$sql_con[0]->contact_lastname;

        //active
        $sql_assign = "SELECT count(*) as total,SUM(la.investment) as loan_amountt FROM `loan_assigment` as la JOIN loan_servicing as ls ON la.talimar_loan = ls.talimar_loan JOIN loan as l ON l.talimar_loan = la.talimar_loan WHERE ls.loan_status ='2' AND la.lender_name ='".$investor_id."'";
        
        $fetch_assignment_data      = $this->User_model->query($sql_assign);    
        $fetch_assignment_data      = $fetch_assignment_data->result();

        $fetch_total_loan           = $fetch_assignment_data[0]->total;
        $total_current_bal          = $fetch_assignment_data[0]->loan_amountt;
    
        //paid off loans
        $sql_assign = "SELECT count(*) as total,SUM(la.investment) as loan_amountt FROM `loan_assigment` as la JOIN loan_servicing as ls ON la.talimar_loan = ls.talimar_loan JOIN loan as l ON l.talimar_loan = la.talimar_loan WHERE ls.loan_status ='3' AND la.lender_name ='".$investor_id."'";
        
        $fetch_assignment_data      = $this->User_model->query($sql_assign);    
        $fetch_assignment_data      = $fetch_assignment_data->result();
        $fetch_total_loans          = $fetch_assignment_data[0]->total;
        $total_current_bals         = $fetch_assignment_data[0]->loan_amountt;


         $lender_schedule_array[]  = array(
                                                    
                                        'lender_name'      => $row->name,
                                        'contact_name'      => $contact_name,
                                        'active_td'        => $fetch_total_loan,
                                        'active_td_bal'      => $total_current_bal,
                                        'paid_off_td'       => $fetch_total_loans,
                                        'paid_off_bal'  	=> $total_current_bals,
                                        'total_td_count'      => $fetch_total_loan+$fetch_total_loans,
                                        'total_td_bal'     	 => $total_current_bal+$total_current_bals,
                                       
                                        
                                    ); 
        



    }


   array_multisort(array_column($lender_schedule_array, 'active_td_bal'), SORT_DESC, $lender_schedule_array);




	$message .='<h2 style="width:100%;background-color:#efefef;padding:5px;"><strong>Top 10 Contacts by Active Balance</strong></h2>';
	$message .='<table class="table table-bordered th_text_align_center">
							
				<tr class="table_header">
					
					<td style="width:20%"><strong>Contact Name</strong></td>
					<td style="width:13%"><strong># of Active TD’s</strong></td>
					<td style="width:13%"><strong>$ of Active TD’s</strong></td>
					<td style="width:13%"><strong># of Paid Off TD’s</strong></td>
					<td style="width:14%"><strong>$ of Paid Off TD’s</strong></td>
					<td style="width:13%"><strong># of Total TD’s</strong></td>
					<td style="width:14%"><strong>$ of Total TD’s</strong></td>
					
				</tr>
		
				<tbody>';
	
				$count = 0;
				$t_active = 0;
				$t_active_bal = 0;
				$t_paid_bal = 0;
				$tt_paid_bal = 0;
				$tt_apaid= 0;
				foreach($lender_schedule_array as $row){
					$count++;
					$t_active +=$row['active_td'];
					$t_active_bal +=$row['active_td_bal'];
					$t_paid +=$row['paid_off_td'];
					$t_paid_bal +=$row['paid_off_bal'];
					$tt_paid_bal +=$row['total_td_bal'];
					$tt_apaid +=$row['total_td_count'];

					$class = 'even';
					if($count % 2 == 0){
						$class = 'even';
					}else{
						$class = 'odd';		
					}
						
						$message .='<tr class='.$class.'>
						
						<td>'.$row['contact_name'].'</td>
						<td>'.$row['active_td'].'</td>
						<td>$'.number_format($row['active_td_bal']).'</td>
						<td>'.$row['paid_off_td'].'</td>
						<td>$'.number_format($row['paid_off_bal']).'</td>
						<td>'.$row['total_td_count'].'</td>
						<td>$'.number_format($row['total_td_bal']).'</td>
					
						
					</tr>';
					if($count==10){
						break;
					}

				}

				
	$message .='<tr class="table_bottom">
							<td><strong>Total: '.$count.'</strong></td>
							<td><strong>'.$t_active.'</strong></td>
							<td><strong>$'.number_format($t_active_bal).'</strong></td>
							<td><strong>'.$t_paid.'</strong></td>
							<td><strong>$'.number_format($t_paid_bal).'</strong></td>
							<td><strong>'.$tt_apaid.'</strong></td>
							<td><strong>$'.number_format($tt_paid_bal).'</strong></td>
							
						</tr>';	
	$message .='</tbody>
			</table>';


		$message .='<br pagebreak="true"/>';

		$message .='<h2 style="width:100%;background-color:#efefef;padding:5px;"><strong>Top 10 Contacts by Total Funded</strong></h2>';
		$message .='<table class="table table-bordered th_text_align_center">
							
								<tr class="table_header">
									
									<td style="width:20%"><strong>Contact Name</strong></td>
									<td style="width:13%"><strong># of Active TD’s</strong></td>
									<td style="width:13%"><strong>$ of Active TD’s</strong></td>
									<td style="width:13%"><strong># of Paid Off TD’s</strong></td>
									<td style="width:14%"><strong>$ of Paid Off TD’s</strong></td>
									<td style="width:13%"><strong># of Total TD’s</strong></td>
									<td style="width:14%"><strong>$ of Total TD’s</strong></td>
									
								</tr>
						
							<tbody>';
					   array_multisort(array_column($lender_schedule_array, 'total_td_bal'), SORT_DESC, $lender_schedule_array);	
						$count = 0;
						$t_active = 0;
						$t_active_bal = 0;
						$t_paid_bal = 0;
						$tt_paid_bal = 0;
						$tt_apaid= 0;
						foreach($lender_schedule_array as $row){
							$count++;
							$t_active +=$row['active_td'];
							$t_active_bal +=$row['active_td_bal'];
							$t_paid +=$row['paid_off_td'];
							$t_paid_bal +=$row['paid_off_bal'];
							$tt_paid_bal +=$row['total_td_bal'];
							$tt_apaid +=$row['total_td_count'];

							$class = 'even';
							if($count % 2 == 0){
								$class = 'even';
							}else{
								$class = 'odd';		
							}
								
								$message .='<tr class='.$class.'>
								
								<td>'.$row['contact_name'].'</td>
								<td>'.$row['active_td'].'</td>
								<td>$'.number_format($row['active_td_bal']).'</td>
								<td>'.$row['paid_off_td'].'</td>
								<td>$'.number_format($row['paid_off_bal']).'</td>
								<td>'.$row['total_td_count'].'</td>
								<td>$'.number_format($row['total_td_bal']).'</td>
							
								
							</tr>';
							if($count==10){
								break;
							}

						}

						
			$message .='<tr class="table_bottom">
									<td><strong>Total: '.$count.'</strong></td>
									
									<td><strong>'.$t_active.'</strong></td>
									<td><strong>$'.number_format($t_active_bal).'</strong></td>
									<td><strong>'.$t_paid.'</strong></td>
									<td><strong>$'.number_format($t_paid_bal).'</strong></td>
									<td><strong>'.$tt_apaid.'</strong></td>
									<td><strong>$'.number_format($tt_paid_bal).'</strong></td>
									
								</tr>';	
			$message .='</tbody>
					</table>';			

		//---------------------------------------------------------------
		// Loan Markrting only Active and pipeline loans 
		//---------------------------------------------------------------

		//$loan_marketing = $this->User_model->query("select *,l.borrower as borrower_id,l.id as loan_id from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status IN ('1','2') AND ls.market_trust_deed = '1'");

		$loan_marketing = $this->User_model->query("select *,l.borrower as borrower_id,l.id as loan_id from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.`closing_status` IN('2','3','4','5','8') AND ls.loan_status IN('1','2') ORDER BY l.loan_funding_date");
		
		$loan_marketing = $loan_marketing->result();

		foreach($loan_marketing as $row){

			$talimar_loan = $row->talimar_loan;
			$borrower_id = $row->borrower_id;

			$property = $this->User_model->select_where('loan_property',array('talimar_loan'=>$talimar_loan));
			$property = $property->result();
			$full_address = $property[0]->property_address.' '.$property[0]->unit.'; '.$property[0]->city.', '.$property[0]->state.' '.$property[0]->zip;

			$borrower_name = $this->User_model->select_where('borrower_data', array('id'=>$borrower_id));
			$borrower_name = $borrower_name->result();
			$b_names = $borrower_name[0]->b_name;

			$sum_invest = $this->User_model->query("Select SUM(investment) as total_investment from loan_assigment where talimar_loan = '".$talimar_loan."'");
			$sum_invest = $sum_invest->result();
			$sum_invest_amount = $sum_invest[0]->total_investment;

			$avilable_amount = $row->loan_amount - $sum_invest_amount;


			/*$all_loan_marketing[] = array(
											"b_name"	  => $b_names,
											"full_address"	=> $full_address,
											"sum_invest_amount"	=> $sum_invest_amount,
											"loan_amount"	=> $row->loan_amount,
											"closing_date"	=> $row->loan_funding_date,
											"market_trust_deed"	=> $row->market_trust_deed,
											"trust_deed_posted_on_website" => $row->trust_deed_posted_on_website,
											"loan_status" 		=> $row->loan_status,
											"closing_status" 	=> $row->closing_status,

											);*/

			$all_loan_marketing_new[$row->closing_status][] = array(
											"b_name"	  => $b_names,
											"full_address"	=> $full_address,
											"sum_invest_amount"	=> $sum_invest_amount,
											"loan_amount"	=> $row->loan_amount,
											"closing_date"	=> $row->loan_funding_date,
											"market_trust_deed"	=> $row->market_trust_deed,
											"trust_deed_posted_on_website" => $row->trust_deed_posted_on_website,
											"loan_status" 		=> $row->loan_status,
											"closing_status" 	=> $row->closing_status,
											"avilable_amount" 	=> $avilable_amount,

											);
		}
		
		$all_loan_marketings = $all_loan_marketing;
		$all_loan_marketings_new = $all_loan_marketing_new;
		
		
		$count				= 0;
		$sum_loan_amount 	= 0;
		$sum_available 		= 0;
		
		
		//send report
			$pdf->WriteHTML($message);
			$file_path = FCPATH.'daily_reports/daily_report_'.date('m-d-Y').'.pdf';
			$pdf->Output($file_path, 'F');
			//echo $message;
			$this->send_report();
		
	}

	public function Maturity_Schedule_data(){
		error_reporting(0);
		date_default_timezone_set('America/Los_Angeles');
		$current_date = date('d-m-Y');
		$maturity_date_45 = strtotime($current_date . ' +45 days');
		$maturity_date_45 = date("Y-m-d", $maturity_date_45);
		$current_date1 = date('Y-m-d');

		$sql = "SELECT * FROM loan WHERE maturity_date <='" . $maturity_date_45 . "' ";
		

		$fetch_loan_data = $this->User_model->query($sql);
		if ($fetch_loan_data->num_rows() > 0) {
			$fetch_loan_data = $fetch_loan_data->result();
			foreach ($fetch_loan_data as $row) {
				$talimar_loan = $row->talimar_loan;
				$fci = $row->fci;
				$loan_amount = $row->loan_amount;
				$intrest_rate = number_format($row->intrest_rate,3);
				$maturity_date = $row->maturity_date;
				$loan_document_date = $row->loan_document_date;
				$extention_option = $row->extention_option;
				$position = $row->position;
				$extention_percent_amount = $row->extention_percent_amount;

				$myDateTime = DateTime::createFromFormat('Y-m-d', $maturity_date);
				$newDateString = $myDateTime->format('d-m-Y');

				$now = time(); // or your date as well
				$your_date = strtotime($newDateString);
				$datediff = $your_date - $now;
				$duration = floor($datediff / (60 * 60 * 24));

				$borrower_id['id'] = $row->borrower;

				//fetch borrower...
				$fetch_borrower_name = $this->User_model->select_where('borrower_data', $borrower_id);
				$fetch_borrower_name = $fetch_borrower_name->result();

				$borrower_name = $fetch_borrower_name[0]->b_name;


			$borrower_iddd['borrower_id'] = $row->borrower;
			$fetch_borrower_d = $this->User_model->select_where('borrower_contact', $borrower_iddd);
			$fetch_borrower_d_fetch = $fetch_borrower_d->result();
			$cont_id['contact_id'] = $fetch_borrower_d_fetch[0]->contact_id;
			// echo '<pre>';	
			// print_r($fetch_borrower_d_fetch);
			// echo '</pre>';	

			$fetch_borrower_con = $this->User_model->select_where('contact', $cont_id);
			$fetch_borrower_cont_data = $fetch_borrower_con->result();



			$b_contact_name = $fetch_borrower_cont_data[0]->contact_firstname . ' ' . $fetch_borrower_cont_data[0]->contact_lastname;
			$b_contact_phone = $fetch_borrower_cont_data[0]->contact_phone;
			$b_contact_email = $fetch_borrower_cont_data[0]->contact_email;
			$contact_id = $fetch_borrower_cont_data[0]->contact_id;


				// fetch data from property
				$talimar['talimar_loan'] = $row->talimar_loan;
				$fetch_property = $this->User_model->select_where('loan_property', $talimar);
				$fetch_property = $fetch_property->result();

				$property_address = $fetch_property[0]->property_address;
				$state = $fetch_property[0]->state;
				$zip = $fetch_property[0]->zip;
				$city = $fetch_property[0]->city;
				$unit = $fetch_property[0]->unit;
				$square_feet = $fetch_property[0]->square_feet;

				// FETCH Servicing data
				$fetch_servicing_data = $this->User_model->select_where('loan_servicing', $talimar);
				$fetch_servicing_data = $fetch_servicing_data->result();
				$loan_status = $fetch_servicing_data[0]->loan_status;
				if ($duration != 0) {
					$duration = $duration + 1;
				}
				if ($duration < 46 && $loan_status == 2) {
					$maturity_report_data_result[] = array(
						'talimar_loan' => $talimar_loan,
						'fci' => $fci,
						'borrower_name' => $borrower_name,
						'loan_amount' => $loan_amount,
						'maturity_date' => $maturity_date,
						'duration' => $duration,
						'loan_document_date' => date('m-d-Y', strtotime($loan_document_date)),
						'property_address' => $property_address,
						'state' => $state,
						'city' => $city,
						'zip' => $zip,
						'unit' => $unit,
						'square_feet' => $square_feet,
						'b_contact_name' => $b_contact_name,
						'b_contact_phone' => $b_contact_phone,
						'b_contact_email' => $b_contact_email,
						'contact_id' => $contact_id,
						'extention_option' => $extention_option,
						'extention_percent_amount' => $extention_percent_amount,
						'position' => $position,
						'intrest_rate' => number_format($intrest_rate,3),
						'full_address' => $property_address.' '.$unit.' <br>'.$city.', '.$state.' '.$zip,

					);
				}
			}

			$maturity_report_data = $maturity_report_data_result;
		} else {
			$maturity_report_data = '';
		}

		return $maturity_report_data;
	}
	
	public function get_servicing_income(){
	
		$join_sql = "SELECT *, loan.id as loan_id FROM loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan_servicing.loan_status = '2'";
		
		$fetch_all_loan 	= $this->User_model->query($join_sql);
		$fetch_all_loan 	= $fetch_all_loan->result();
	
		foreach ($fetch_all_loan as $row) {
			$talimar['talimar_loan'] = $row->talimar_loan;
			$talimar_loan = $row->talimar_loan;
			$fci = $row->fci;
			$loan_amount = $row->loan_amount;
			$current_balance = $row->current_balance;

			//  fetch data from loan servicing

			$fetch_servicing = $this->User_model->select_where('loan_servicing', $talimar);
			$fetch_servicing = $fetch_servicing->result();
			$servicing_fee = $fetch_servicing[0]->servicing_fee;
			$broker_servicing_fees = $fetch_servicing[0]->broker_servicing_fees;
			$minimum_servicing_fee = $fetch_servicing[0]->minimum_servicing_fee;
			$who_pay_servicing = $fetch_servicing[0]->who_pay_servicing;
			//$sub_servicing_agent          = $fetch_servicing[0]->sub_servicing_agent ? $serviceing_sub_agent_option[$fetch_servicing[0]->sub_servicing_agent] : ''; // old code

			$sub_servicing_agent_result = $fetch_servicing[0]->sub_servicing_agent;

			$vendor_name_result = $this->User_model->query("SELECT vendor_name from vendors WHERE vendor_id ='" . $sub_servicing_agent_result . "' ")->result();
			$sub_servicing_agent = $vendor_name_result[0]->vendor_name;

			$property = $this->User_model->select_where('loan_property', $talimar);
			$property = $property->result();
			$property = $property[0]->property_address;

			$sql = "SELECT count(*) as countrow from loan_assigment WHERE talimar_loan = '" . $talimar_loan . "' ";
			$count_lender = $this->User_model->query($sql);
			if ($count_lender->num_rows() > 0) {
				$count_lender = $count_lender->result();
				$count_lender = $count_lender[0]->countrow;
			} else {
				$count_lender = 0;
			}

			$fetch_loan_assigment = $this->User_model->select_where('loan_assigment', $talimar);
			$fetch_loan_assigment = $fetch_loan_assigment->result();

			$all_monthly_income_result[] = array(
				'talimar_loan' => $talimar_loan,
				'fci' => $fci,
				'loan_amount' => $loan_amount,
				'current_balance' => $current_balance,
				'servicing_fee' => $servicing_fee,
				'broker_servicing_fees' => $broker_servicing_fees,
				'sub_servicing_agent' => $sub_servicing_agent,
				'who_pay_servicing' => $who_pay_servicing,
				'minimum_servicing_fee' => $minimum_servicing_fee,
				'count_lender' => $count_lender,
				'property' => $property,
				'interest_rate' => number_format($row->intrest_rate,3),
				'servicing_lender_rate' => number_format($fetch_servicing[0]->servicing_lender_rate,3),
				'invester_yield_percent' => $fetch_loan_assigment[0]->invester_yield_percent,
			);

		}

		$all_monthly_income = $all_monthly_income_result;

		$total_amount = 0;
		$total_servicing_income = 0;
		$total_servicing_cost = 0;
		$total_minimum_servicing_fee = 0;
		$total_gross_servicing = 0;
		$total_count_lender = 0;
		$avg_servicing_fee = 0;
		$total_pro_fees = 0;
		$toal_brokr_disbursment_fee = 0;
		$total_net_servicing_new = 0;
		foreach ($all_monthly_income as $key => $row) {

			$payment_pro_fees = $all_monthly_income[$key]['loan_amount'];

			$pro_fees = '0';
			if ($payment_pro_fees > 0 && $payment_pro_fees <= 400000) {
				$pro_fees = '0';
			} elseif ($payment_pro_fees >= 400001 && $payment_pro_fees <= 500000) {
				$pro_fees = '25.00';
			} elseif ($payment_pro_fees >= 500001 && $payment_pro_fees <= 600000) {
				$pro_fees = '35.00';
			} elseif ($payment_pro_fees >= 600001 && $payment_pro_fees <= 700000) {
				$pro_fees = '45.00';
			} elseif ($payment_pro_fees >= 700001 && $payment_pro_fees <= 800000) {
				$pro_fees = '55.00';
			} elseif ($payment_pro_fees >= 800001 && $payment_pro_fees <= 900000) {
				$pro_fees = '65.00';
			} elseif ($payment_pro_fees >= 900001 && $payment_pro_fees <= 1000000) {
				$pro_fees = '75.00';
			} elseif ($payment_pro_fees >= 1000001 && $payment_pro_fees <= 2000000) {

				$pro_fees = '95.00';
			} elseif ($payment_pro_fees >= 2000001 && $payment_pro_fees <= 3000000) {

				$pro_fees = '115.00';
			} elseif ($payment_pro_fees >= 3000001 && $payment_pro_fees <= 4000000) {

				$pro_fees = '135.00';
			} elseif ($payment_pro_fees >= 4000001 && $payment_pro_fees <= 5000000) {

				$pro_fees = '155.00';
			} elseif ($payment_pro_fees >= 5000001 && $payment_pro_fees <= 6000000) {

				$pro_fees = '175.00';
			} elseif ($payment_pro_fees >= 6000001 && $payment_pro_fees <= 7000000) {

				$pro_fees = '195.00';
			} elseif ($payment_pro_fees >= 7000001 && $payment_pro_fees <= 8000000) {

				$pro_fees = '215.00';
			} elseif ($payment_pro_fees >= 8000001 && $payment_pro_fees <= 9000000) {

				$pro_fees = '235.00';
			} elseif ($payment_pro_fees >= 9000001 && $payment_pro_fees <= 10000000) {

				$pro_fees = '255.00';
			} elseif ($payment_pro_fees >= 10000001 && $payment_pro_fees <= 11000000) {

				$pro_fees = '275.00';
			} elseif ($payment_pro_fees >= 11000001 && $payment_pro_fees <= 12000000) {

				$pro_fees = '295.00';
			}else {

				$pro_fees = '0';
			}

			$total_pro_fees += $pro_fees;

			$loan_amount = $all_monthly_income[$key]['loan_amount'];
			$servicing_fee = $all_monthly_income[$key]['servicing_fee'];
			//$gross_servicing      = ($loan_amount * ($servicing_fee / 100 ))/12;
			$minimum_servicing_fee = $all_monthly_income[$key]['minimum_servicing_fee'];
			//$servicing_cost       = $all_monthly_income[$key]['count_lender'] *$minimum_servicing_fee;


			$loan_amount = $all_monthly_income[$key]['loan_amount'];
			$servicing_fee = $all_monthly_income[$key]['servicing_fee'];

			//$gross_servicing  = ($loan_amount * ($servicing_fee / 100 ))/12;

			if ($all_monthly_income[$key]['servicing_lender_rate'] == 'NaN' || $all_monthly_income[$key]['servicing_lender_rate'] == '') {

				$lender_ratessss = $all_monthly_income[$key]['invester_yield_percent'];
			} else {

				$lender_ratessss = $all_monthly_income[$key]['servicing_lender_rate'];
			}

			$nnew_val = $all_monthly_income[$key]['interest_rate'] - $lender_ratessss;
			$nnew_valss = $nnew_val / 100;

			$gross_servicing = ($all_monthly_income[$key]['current_balance'] * $nnew_valss) / 12;

			$minimum_servicing_fee = $all_monthly_income[$key]['minimum_servicing_fee'];

			// $servicing_income=$gross_servicing - $servicing_cost;
			$lender_servicing_fee = $all_monthly_income[$key]['count_lender'] * $all_monthly_income[$key]['minimum_servicing_fee'];

			if ($lender_servicing_fee > $pro_fees) {

				$pro_fees = 0;

			} else {

				$lender_servicing_fee = 0;

			}

			$servicing_cost = $lender_servicing_fee + $pro_fees + $all_monthly_income[$key]['broker_servicing_fees'];
			$servicing_income = $gross_servicing - $servicing_cost;

			if ($all_monthly_income[$key]['who_pay_servicing'] == 2) {

				$gross_servicing = $gross_servicing;
				$servicing_cost = 0;
				$servicing_income = $servicing_income;
				$lender_servicing_fee = 0;
				$all_monthly_income[$key]['servicing_fee'] = $servicing_fee;
				$all_monthly_income[$key]['minimum_servicing_fee'] = 0;
			}

			$all_incomee[] = array(

				'sub_servicing_agent' => $row['sub_servicing_agent'],
				'fci' => $row['fci'],
				'talimar_loan' => $row['talimar_loan'],
				'loan_amount' => $row['loan_amount'],
				'current_balancee' => $row['current_balance'],
				'property' => $row['property'],
				'broker_servicing_fees' => $row['broker_servicing_fees'],
				'who_pay_servicing' => $serviceing_who_pay_servicing_option[$all_monthly_income[$key]['who_pay_servicing']],
				'servicing_fee' => $row['servicing_fee'],
				'total_g' => $gross_servicing,
				'count_lender' => $lender_servicing_fee,
				'minimum_servicing_fee' => number_format($row['minimum_servicing_fee']),
				'servicing_cost' => number_format($servicing_cost),
				'total_servicing_income' => number_format($servicing_income, 2),
				'total_servicing_incomenew' => $servicing_income,
				'pro_fees' => $pro_fees,
				'interest_rate' => number_format($row['interest_rate'],3),
				'servicing_lender_rate' => number_format($row['servicing_lender_rate'],3),
				'invester_yield_percent' => $row['invester_yield_percent'],

			);

		}


		$total_servicing_income = 0;
		$total_g = 0;
		foreach ($all_incomee as $value) {
			
			$total_servicing_income += $value['total_servicing_incomenew']; 
			$total_g += $value['total_g']; 

		}

		$final_array = array('total_g'=>$total_g,'total_servicing_income'=>$total_servicing_income);

		// echo '<pre>';
		// print_r($all_incomee);
		// echo '</pre>';
	 	return $final_array;

	}


	public function maturity_data(){

		error_reporting(0);
		$current_date = date('d-m-Y');
		$maturity_date_45 = strtotime($current_date . ' +45 days');
		$maturity_date_45 = date("Y-m-d", $maturity_date_45);
		$current_date1 = date('Y-m-d');


		$sql = "SELECT * FROM loan WHERE maturity_date <='" . $maturity_date_45 . "' order by maturity_date";
		$fetch_loan_data = $this->User_model->query($sql);
		if ($fetch_loan_data->num_rows() > 0) {
			$fetch_loan_data = $fetch_loan_data->result();

			foreach ($fetch_loan_data as $row) {

				$talimar_loan = $row->talimar_loan;
				$loan_id = $row->id;
				$fci = $row->fci;
				$loan_amount = $row->loan_amount;
				$maturity_date = $row->maturity_date;
				$loan_document_date = $row->loan_document_date;
				$borrower_id['id'] = $row->borrower;

				$myDateTime = DateTime::createFromFormat('Y-m-d', $maturity_date);
				$newDateString = $myDateTime->format('d-m-Y');
				$now = time(); // or your date as well
				$your_date = strtotime($newDateString);
				$datediff = $your_date - $now;
				$duration = floor($datediff / (60 * 60 * 24));
				$talimar_maturity['talimar_loan'] = $row->talimar_loan;

				// FETCH Servicing data
				$fetch_servicing_data = $this->User_model->select_where('loan_servicing', $talimar_maturity);

				$fetch_servicing_data = $fetch_servicing_data->result();

				$loan_status = $fetch_servicing_data[0]->loan_status;

				$ex_borrower = $fetch_servicing_data[0]->extension_borrower;
				$letter_sent = $fetch_servicing_data[0]->extension_letter_sent;
				$sign_letter = $fetch_servicing_data[0]->signed_extension_letter;
				$fee_received = $fetch_servicing_data[0]->extension_fee_received;
				$sent_servicer = $fetch_servicing_data[0]->extension_request_sent_servicer;

				$property = $this->User_model->query("Select property_address from loan_property where talimar_loan = '" . $row->talimar_loan . "'");
				$property = $property->result();
				$s_property = $property[0]->property_address;

				// echo $duration.' - '.$maturity_date.' - '.$loan_status.'<br>';

				if ($duration < 46 && $loan_status == 2) {

					$maturity_report_data[] = array(
						'talimar_loan' => $talimar_loan,
						'loan_id' => $loan_id,
						'fci' => $fci,
						'loan_amount' => $loan_amount,
						'maturity_date' => date('m-d-Y', strtotime($maturity_date)),
						'duration' => $duration,
						's_property' => $s_property,

					);
				}
			}

		}else{

			$maturity_report_data = '';
		}

		return $maturity_report_data;

	}

	public function avg_spread(){

			$servicing_spred = $this->User_model->query("Select l.talimar_loan, l.intrest_rate, ls.loan_status, ls.servicing_lender_rate, la.invester_yield_percent from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan JOIN loan_assigment as la ON l.talimar_loan = la.talimar_loan WHERE ls.loan_status = '2' GROUP BY l.talimar_loan");
				$servicing_spred = $servicing_spred->result();

				$count = 0;
				$new_val = 0;
				$interest_rate = 0;
				$lender_ratessss = 0;
				foreach($servicing_spred as $row){

					$count++;
					$interest_rate += number_format($row->intrest_rate,3);

					if($row->servicing_lender_rate == 'NaN' || $row->servicing_lender_rate == ''){

						$lender_ratessss += $row->invester_yield_percent;
					}else{

					    $lender_ratessss += $row->servicing_lender_rate;
					}

					$new_rates = number_format($lender_ratessss,3);
				}

				$spred_val = $interest_rate - $new_rates;
				return $spred_val_avg = $spred_val/$count;

	}

	public function fetch_all_users($id){

		$user_data = $this->User_model->select_where('user',array('id'=>$id));
		$user_data = $user_data->result();

		return $user_data;
	}
	
	
	public function send_report(){


		// $fetch_setting = $this->User_model->select_star('user_settings');
		// if($fetch_setting->num_rows() > 0){

		// 	$fetch_setting = $fetch_setting->result();
		// 	foreach($fetch_setting as $row){

		// 		if($row->daily_report == 1){

		// 			$usersdata = $this->fetch_all_users($row->user_id);
		// 			$email = $usersdata[0]->email_address; 
			
					$subject  = "TaliMar Financial - Daily Report ".date("m-d-Y");
					$header	  = "From: mail@database.talimarfinancial.com";
					$message1 = 'Attached is your morning report.';
					$attach_file_name = FCPATH.'daily_reports/daily_report_'.date('m-d-Y').'.pdf';
					
					$email ='bvandenberg@talimarfinancial.com';
					
			  		//$email   ='anjul.wartiz@gmail.com';
			  		$email_to ='bvandenberg@talimarfinancial.com';

			  		/*$this->email->initialize(SMTP_SENDGRID);
					$this->email
							->from('mail@talimarfinancial.com', $subject)
							->to($email)
							->bcc($email_to)
							->subject($subject)
							->message($message1)
							->attach($attach_file_name)
							->set_mailtype('html');
		
							$this->email->send();*/
					$dataMailA = array();
			        $dataMailA['from_email']  	= 'mail@talimarfinancial.com';
			        $dataMailA['from_name']   	= 'TaliMar Financial';
			        $dataMailA['to']      		= $email_to;
			        $dataMailA['cc']      		= '';
			        $dataMailA['name']      	= '';
			        $dataMailA['subject']     	= $subject;
			        $dataMailA['content_body']  = $message1;
			        $dataMailA['button_url']  	= '';
			        $dataMailA['button_text']   = '';
			        $dataMailA['attach_file']   = $attach_file_name;
			        send_mail_template($dataMailA);
							
							
							//echo 'Works';
	// 			}
	// 		}
		
	
	// 	}
	 }



	public function prior_five_day_email(){
		error_reporting(0);
		//--------------- automatic email send to active loan 5 day prior start --------------------//
		//https://database.talimarfinancial.com/Cronjob/prior_five_day_email

	    $emaill = "SELECT *,loan.borrower as borrower_id FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status='2'";
		
		$email_fetch_loan_data=$this->User_model->query($emaill);
		$email_fetch_loan_data=$email_fetch_loan_data->result();

		foreach($email_fetch_loan_data as $row)
		{
	                
	        $email_due_dateeee =$row->first_payment_date;

	      	$email_due_date    =date('F dS, Y',strtotime( "+1 month", strtotime($email_due_dateeee)));

			$five_day_previous=date('Y-m-d', strtotime('-5 days', strtotime($email_due_dateeee))); 
			$borrower_id=$row->borrower_id;
	        $talimar=$row->talimar_loan;
			$email_countt=$row->email_count;
			$payment_notificationn=$row->payment_notification;
			$renovation=$row->renovation;
			$contact_email = "SELECT *  FROM borrower_contact JOIN contact ON contact.contact_id = borrower_contact.contact_id WHERE  borrower_contact.borrower_id='".$borrower_id."'";
			

			$email_fetch_loan_d=$this->User_model->query($contact_email);
			$email_fetch_loan_d=$email_fetch_loan_d->result();  
			$contact_email_one=$email_fetch_loan_d[0]->contact_email;
		 	$current_date=date('Y-m-d');

		 	$select_property_insurance = $this->User_model->query("SELECT * FROM `loan_property_insurance` WHERE `talimar_loan`='".$talimar."'");
			$select_property_insurance = $select_property_insurance->result();

			$loan_property_tex = $this->User_model->query("SELECT * FROM `loan_property_taxes` WHERE `talimar_loan`='".$talimar."'");
			$loan_property_tex = $loan_property_tex->result();

			$loan_loan_property = $this->User_model->query("SELECT * FROM `loan_property` WHERE `talimar_loan`='".$talimar."'");
			$loan_loan_property = $loan_loan_property->result();
			$full_address = $loan_loan_property[0]->property_address.' '.$loan_loan_property[0]->unit.'; '.$loan_loan_property[0]->city.', '.$loan_loan_property[0]->state.' '.$loan_loan_property[0]->zip;


		 	$select_payment = $this->User_model->query("SELECT * FROM `impound_account` WHERE `talimar_loan`='".$talimar."'");
			$select_payment = $select_payment->result();

			$first_payment_amount = 0;
			foreach($select_payment as $data1){


				if($data1->items == 'Mortgage Payment'){

					$first_payment_amount += ($row->intrest_rate/100) * ($row->loan_amount /12);

				}
				if($data1->items == 'Property / Hazard Insurance Payment')
				{
					if($data1->impounded == 1){
						$first_payment_amount += isset($select_property_insurance) ? ($select_property_insurance->annual_insurance_payment / 12) : 0;
					}
				}
										
				if($data1->items == 'County Property Taxes')
				{
					if($data1->impounded == 1){
						$first_payment_amount += isset($loan_property_tex) ? ($loan_property_tex[0]->annual_tax / 12) : 0;
					}
				}
				if($data1->items == 'Flood Insurance')
				{
					if($data1->impounded == 1){
						$first_payment_amount += isset($data1->amount) ? ($data1->amount) : 0;
					}
				}
										
				if($data1->items == 'Mortgage Insurance')
				{
					if($data1->impounded == 1){
						$first_payment_amount += isset($data1->amount) ? ($data1->amount) : 0;
					}
				}
	  
			}

			$first_monyly_payment = number_format($first_payment_amount,2);		
		 	
		if($payment_notificationn == '1')
		{
			
			if($current_date == $five_day_previous)
			{			
		  
			    if($email_countt != '1')		
			    {
			       	  
			       	   	$message	=''; 
			         	$email 		=  $contact_email_one;
			    	  //  $email       ='servicing@talimarfinancial.com';
						$subject	= "TaliMar Financial – Loan Servicing"; 
						$header		= "From: mail@database.talimarfinancial.com"; 
						//$message	= 'Your Payment date '.$email_due_date.' is Due. So,Please clear the due as soon as possible.<br>';
						$message .= 'This e-mail is a reminder that payment in the amount of $'.$first_monyly_payment.'  for the loan secured on '.$full_address.' is due on '.$email_due_date.'<br> Please make the payment directly to FCI Lender Services by clicking on the link below:<br>';
						$message .='https://www.trustfci.com/BorrowerPaymentOptions.html';
						$message .='<br>';
						$message .='Should you have any questions regarding the payment,  please contact TaliMar Financial at 858-201-3253.<br>';
						
						/*$this->email->initialize(SMTP_SENDGRID);
							$this->email
							->from('mail@talimarfinancial.com', $subject)
							->to($email)
							// ->cc('pankaj.wartiz@gmail.com')
							->subject($subject)
							->message($message)
							->set_mailtype('html');
							
							$this->email->send();*/

						$dataMailA = array();
				        $dataMailA['from_email']  	= 'mail@talimarfinancial.com';
				        $dataMailA['from_name']   	= 'TaliMar Financial';
				        $dataMailA['to']      		= $email;
				        $dataMailA['cc']      		= '';
				        $dataMailA['name']      	= '';
				        $dataMailA['subject']     	= $subject;
				        $dataMailA['content_body']  = $message;
				        $dataMailA['button_url']  	= '';
				        $dataMailA['button_text']   = '';
				        $dataMailA['attach_file']   = '';
				        $dataMailA['regards']   = 'Loan Servicing';
				        send_mail_template($dataMailA);

						$update_email_count_query=$this->User_model->query("UPDATE loan set email_count='1' WHERE talimar_loan='".$talimar."'");
			      
				}else{

						echo "";
				}	
			}else{

					echo "";
			}
		}else{

			echo "";
		}
		// 	echo '<pre>';
		// print_r($email_fetch_loan_d);
		// echo '</pre>';
			
		}

	}


	//Autometic renovation mail...
	//https://database.talimarfinancial.com/Cronjob/renovation_mail

	public function renovation_mail(){
		error_reporting(0);
		$emailll = "SELECT loan.payment_count,loan.talimar_loan,loan_servicing.renovation,loan.borrower as borrower_id FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan";
		
		$email_fetch_loan_dataa=$this->User_model->query($emailll);
		$email_fetch_loan_dataa=$email_fetch_loan_dataa->result();


		foreach($email_fetch_loan_dataa as $roww)
		{
	        $borrower_idd=$roww->borrower_id;
			$renovation_value=$roww->renovation;
			$email_counttt=$roww->payment_count;
	 		$talimarr=$roww->talimar_loan;
	 
	 		$contact_emailk = "SELECT borrower_contact.contact_email FROM borrower_contact JOIN contact ON contact.contact_id = borrower_contact.contact_id WHERE  borrower_contact.borrower_id='".$borrower_idd."'";
			
			$email_fetch_loan_dd=$this->User_model->query($contact_emailk);
			$email_fetch_loan_dd=$email_fetch_loan_dd->result();  
			$contact_email_onee=$email_fetch_loan_dd[0]->contact_email;

			$loan_loan_property = $this->User_model->query("SELECT * FROM `loan_property` WHERE `talimar_loan`='".$talimarr."'");
			$loan_loan_property = $loan_loan_property->result();
			$full_address = $loan_loan_property[0]->property_address.' '.$loan_loan_property[0]->unit.'; '.$loan_loan_property[0]->city.', '.$loan_loan_property[0]->state.' '.$loan_loan_property[0]->zip;

	        if($renovation_value == '1')
	        {  
	        		
			    if($email_counttt != '1')		
			    {
			    	 
			       	   	$message	=''; 
			         	$email 		=  $contact_email_onee;
			         	//$email 		= 'anjul.wartiz@gmail.com';
			    	    $email_to    ='servicing@talimarfinancial.com';
			    	    //$email_t    ='pankaj.wartiz@gmail.com';
						$subject	= "TaliMar Financial – Loan Servicing"; 
						$header		= "From: mail@database.talimarfinancial.com"; 
						
						$message .='Hi'.' '.' '.$b_contact[0]->contact_firstname;

						$message .='<br>';
						$message .='<p>Would you please respond with a written status update on '.$full_address.'.';
						$message .=' Please include the status of the construction and marketing of the property. Should you have any questions, please contact loan servicing at (858) 201-3253.</p>';
						
						
				
						$regards ='Loan Servicing<br>';
					
						$regards .='TaliMar Financial<br>';
						$regards .='Phone: (858) 201-3253<br>';
						$regards .='16880 West Bernardo Ct., #140<br>';
						$regards .='San Diego, CA 92127<br>';
						$regards .='www.talimarfinancial.com';

						/*$this->email->initialize(SMTP_SENDGRID);
							$this->email
							->from('mail@talimarfinancial.com', $subject)
							->to($email)
							->cc($email_t)
							->bcc($email_to)
							->subject($subject)
							->message($message)
							->set_mailtype('html');
							
							$this->email->send();*/
						$dataMailA = array();
				        $dataMailA['from_email']  	= 'mail@talimarfinancial.com';
				        $dataMailA['from_name']   	= 'TaliMar Financial';
				        $dataMailA['to']      		= $email;
				        $dataMailA['cc']      		= $email_t;
				        $dataMailA['bcc']      		= $email_to;
				        $dataMailA['name']      	= '';
				        $dataMailA['subject']     	= $subject;
				        $dataMailA['content_body']  = $message;
				        $dataMailA['button_url']  	= '';
				        $dataMailA['button_text']   = '';
				        $dataMailA['attach_file']   = '';
				        $dataMailA['regards'] = $regards;
				        send_mail_template($dataMailA);

						$update_email_count_query=$this->User_model->query("UPDATE loan set payment_count='1' WHERE talimar_loan='".$talimarr."'");
			      
				}else{

						echo "";
				}	
			}else{

			$update_email_count_query=$this->User_model->query("UPDATE loan set payment_count='0' WHERE talimar_loan='".$talimarr."'");
				echo "";
			}

		}

	}



}

?>
