<?php
class Fund_data extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('User_model');
		$this->load->library('Aws3','aws3');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');
		
		 if($this->session->userdata('t_user_id') != '' )
		{
			
		}
		else
		{
			redirect(base_url()."home",'refresh');
		} 
	}
	
	public function index()
	{
		 error_reporting(0);
		$query 	= $this->User_model->query("select * from fund_data ORDER BY CAST(`available_credit` AS UNSIGNED) DESC");
		if($query->num_rows()>0){

			$fetch_data=$query->result();
			$data['fund_data_result']=$fetch_data;

		}else{

		$data['fund_data_result']='';	
		}
		$data['content'] 	= $this->load->view('fund_data/index',$data,true);
		
		$this->load->view('template_files/template',$data);
	
	}

 public function add_update_data(){

 error_reporting(0);
 	$account_name=$this->input->post('account_name');
 	$available_credit=$this->input->post('available_credit');
 	$drawn=$this->input->post('drawn');
 	$remaining=$this->input->post('remaining');
 	$t_user_id=$this->session->userdata('t_user_id');
 	$id=$this->input->post('id');
 	
  
 	foreach ($id as $key => $row) 
 	{
       if($account_name[$key] != '')
       {
       
	 		if($row !='new')
	 		{

	 			 $sql="UPDATE `fund_data` SET `account_name` = '".$account_name[$key]."', `available_credit`='".$this->amount_format($available_credit[$key])."', `drawn`='".$this->amount_format($drawn[$key])."',`remaining`='".$this->amount_format($remaining[$key])."',`user_id`='".$t_user_id."' WHERE  id ='".$row."'";
	 			
	 		}else{


				  $sql="INSERT INTO `fund_data` ( `account_name`,`available_credit`,`drawn`,`remaining`,`user_id`) VALUES ( '".$account_name[$key]."', '".$this->amount_format($available_credit[$key])."','".$this->amount_format($drawn[$key])."','".$this->amount_format($remaining[$key])."','".$t_user_id."')";

 		        }
      
      
      		$this->User_model->query($sql);
      		$this->session->set_flashdata('success', 'Credit Line Updated Successfully');
      	}
 	}

 	
	redirect(base_url().'credit_line','refresh');


 }


 public function delete_fund_data(){
 error_reporting(0);
 	$id['id']=$this->input->post('id');
		
	$this->User_model->delete('fund_data',$id);
    
 }

 	public function amount_format($n)
	{
		$n = str_replace(',','',$n);
		$a = trim($n , '$');
		$b = trim($a , ',');
		$c = trim($b , '%');
		return $c;
	}


	
	
}


