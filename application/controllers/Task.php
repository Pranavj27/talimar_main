<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Task extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('Aws3','aws3');
		$this->load->helper('aws3_helper.php');
		$this->load->helper('mailhtml_helper.php');
		$this->load->database();
		$this->load->helper(array('form', 'url'));
		$this->load->model('User_model');
		$this->load->library('MYPDF');
		$this->load->library('email');
		$this->load->helper('common_functions.php');
		if ($this->session->userdata('t_user_id') != '') {
		} else {
			redirect(base_url() . "home", 'refresh');
		}
	}
	
	function index()
	{
		$this->load->view('dashboard_view');
	}
	public function task_view(){
		$loginUserId					= $this->session->userdata('t_user_id');
		$data['task_status']			= '';
		if(!empty($loginUserId))
		{
			$data['user_id'] 			= $loginUserId;
			$selct_status				= $this->input->post('selct_status');			
			$where 						= " (ct.t_user_id='".$loginUserId."' OR FIND_IN_SET($loginUserId, ct.add_people)) "; 
			if(!empty($selct_status)){
				$where.= " AND ct.contact_status='".$selct_status."' ";
				$data['task_status']	= $selct_status;
			}
			//$sql="SELECT * FROM `contact_tasks` as ct  WHERE $where ORDER BY ct.contact_date DESC ";
            // echo $sql;
			$task_sql 					= $this->User_model->query("SELECT * FROM `contact_tasks` as ct  WHERE $where
 ORDER BY ct.contact_date DESC ");
			$taskDataList				= $task_sql->result();
			$data['fetch_check_sql']	= array();
			if(!empty($taskDataList)){
				$data['fetch_check_sql']= $taskDataList;
			}
		}
		else
		{
			$data['fetch_check_sql']	= array();
		}

		$data['content'] 			= $this->load->view('tasks/task_view',$data,true);
		$this->load->view('template_files/template',$data);
	}
	public function contact_name_lists1(){
		  	$page= $_GET['page'];
		  	// $page= $_GET['page']
		  	// term
		    $resultCount = 10;
		    $end = ($page - 1) * $resultCount;       
		    $start = $end + $resultCount;
		    if ($_GET['term'] == trim($_GET['term']) && strpos($_GET['term'], ' ') !== false) {

		    	$sql=	"SELECT * 
					 FROM contact
					 WHERE   '".$_GET['term']."%' LIKE Concat(Concat('%',contact_firstname),'%') OR '".$_GET['term']."%' LIKE  Concat(Concat('%',contact_lastname),'%') LIMIT {$end},{$start} ";
			}else{
				$_GET['term']=trim($_GET['term']);
				$sql=	"SELECT * FROM contact WHERE contact_firstname LIKE '".$_GET['term']."%' LIMIT {$end},{$start}";
			} 
		   
		    //$stmt = $db_con->query("SELECT col,col FROM table WHERE col LIKE '".$_GET['term']."%' LIMIT {$end},{$start}");
		   $fetch_contact = $this->User_model->query($sql);
		    //$fetch_contact 				= $this->User_model->select_star('contact');
		    
		    $count = $fetch_contact->num_rows();

			$fetch_contact 				= $fetch_contact->result();
			$data = [];
			foreach($fetch_contact as $row){

					$text = $row->contact_firstname;
					if($row->contact_middlename != '')
					{
						$text .= ' '.$row->contact_middlename;
					}

					if($row->contact_lastname != '')
					{
						$text .= ' '.$row->contact_lastname;
					}


					$fetch_search_option[] = array(
										'id' => $row->contact_id,
										'text' => $text,
										'email'=> $row->contact_email,
										'phone'=> $row->contact_phone
									);
					$data[] = ['id'=>$row->contact_id, 'col'=>$text, 'total_count'=>$count];
				}

		    // $stmt->execute();
		    // $count = $stmt->rowCount();
		        // $data = [];
		        // while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		        //     $data[] = ['id'=>$row['id'], 'col'=>$row['col'], 'total_count'=>$count];
		        // }
		        // IF SEARCH TERM IS NOT FOUND DATA WILL BE EMPTY SO
		        if (empty($data)){
		            $empty[] = ['id'=>'', 'col'=>'', 'total_count'=>'']; 
		            echo json_encode($empty);
		        }else{ 
		            echo json_encode($data);
		        }

	}

	public function fetch_name_by_id(){
		$id = $_POST['id'];
		  	// $page= $_GET['page']
		  	// term
		  		 
		   $fetch_contact = $this->User_model->query("SELECT * FROM contact where contact_id = ".$id);
		    //$fetch_contact 				= $this->User_model->select_star('contact');
		    
		    //$count = $fetch_contact->num_rows();

			$fetch_contact 				= $fetch_contact->result();
			$data = [];
			foreach($fetch_contact as $row){

					$text = $row->contact_firstname;
					if($row->contact_middlename != '')
					{
						$text .= ' '.$row->contact_middlename;
					}

					if($row->contact_lastname != '')
					{
						$text .= ' '.$row->contact_lastname;
					}


					$fetch_search_option[] = array(
										'id' => $row->contact_id,
										'text' => $text,
										'email'=> $row->contact_email,
										'phone'=> $row->contact_phone
									);
					$data[] = ['id'=>$row->contact_id, 'text'=>$text, 'email'=>$row->contact_email,'phone'=>$row->contact_phone];
				}

		  		if (empty($data)){
		            $empty[] = ['id'=>'', 'text'=>'', 'email'=>'', 'phone'=>'']; 
		            echo json_encode($empty);
		        }else{ 
		            echo json_encode($data);
		        }

	}
	public function contact_name_lists(){

		$all_user_contact = $this->userlist();
		$data['all_contact_name_list'] =   $all_user_contact['fetch_search_option'];

		$array = array('DataAll' => $all_user_contact['fetch_search_option']);
		// echo json_encode($array);

		echo $this->load->view('tasks/ajax_contact_name_list_data',$data,true);
	
	}

	public function userlist(){

		// $sql_all_user1 = $this->User_model->query("SELECT * FROM user where account='1'");
		// $data['all_users_data'] = $sql_all_user1->result();
		$fetch_contact 				= $this->User_model->select_star('contact');
				$fetch_contact 				= $fetch_contact->result();
		// echo '<pre>??'; print_r($fetch_contact); 
				foreach($fetch_contact as $row){

					$text = $row->contact_firstname;
					if($row->contact_middlename != '')
					{
						$text .= ' '.$row->contact_middlename;
					}

					if($row->contact_lastname != '')
					{
						$text .= ' '.$row->contact_lastname;
					}


					$fetch_search_option[] = array(
										'id' => $row->contact_id,
										'text' => $text,
										'email'=> $row->contact_email,
										'phone'=> $row->contact_phone
									);
				}
				$data['fetch_search_option'] = $fetch_search_option;
		return 	$data;

	}



	public function add_contact_note(){
		$contact_id = $this->uri->segment(2);
		$_notes_id = $this->uri->segment(3);
		$data['notes_id']= $_notes_id; 
		if($_notes_id > 0){
			$contact_note = $this->User_model->query("SELECT * FROM contact_notes where contact_id = '".$contact_id."' AND id= ".$_notes_id);
			$data['contact_note_details'] = $contact_note->result();
		}
		// $data['post_d'] = $_GET['edit'];
		$all_user_contact = $this->userlist();

		$data['all_contacts'] = $all_user_contact['fetch_search_option'];

		
		
		$data['val'] = $contact_id;
		$data['contact_id'] = $contact_id;


		$data['selected_data'] = '';
		$dataWhere['contact_id']= $contact_id;			
		$fetch_contact_data 		= $this->User_model->select_where('contact',$dataWhere);
		if($fetch_contact_data->num_rows() > 0)
			{
				$data['fetch_contact_data'] 		= $fetch_contact_data->result();
			}
		$all_user1 = $this->User_model->query("SELECT * FROM user");
		$data['all_users'] = $all_user1->result();

		$crnt_userid = $this->session->userdata('t_user_id');
		$curuntuser = $this->User_model->query("SELECT fname,lname,middle_name FROM user WHERE id = '".$crnt_userid."'");

		$data['curnt_user_data'] = $curuntuser->result();

		/*fetch all street address*/
		$fetch_property_search = $this->User_model->query("select lp.talimar_loan, ph.property_address from loan_property as lp JOIN property_home as ph ON lp.property_home_id = ph.id WHERE ph.primary_property = '1' order by lp.id desc");

		if ($fetch_property_search->num_rows() > 0) {

					$fetch_property_search = $fetch_property_search->result();
					foreach ($fetch_property_search as $row) {
						//$p_ta_loan['talimar_loan'] 	= $row->talimar_loan;

						$property_search = $this->User_model->query("SELECT id from loan WHERE talimar_loan='" . $row->talimar_loan . "' ");
						$property_search = $property_search->result();

						$loanStatus = $this->User_model->query("SELECT loan_status FROM loan_servicing WHERE talimar_loan = '".$row->talimar_loan."' ");

						$loanStatuss = $loanStatus->row();
						// echo '<pre>';print_R($loanStatuss->loan_status);
						if($loanStatuss){
											

							if ($loanStatuss->loan_status == 1) {
								$loan_status = 'P';
							} elseif ($loanStatuss->loan_status == 2) {
								$loan_status = 'A';
							} elseif ($loanStatuss->loan_status == 3) {
								$loan_status = 'P';
							} elseif ($loanStatuss->loan_status == 4) {
								$loan_status = 'C';
							} elseif ($loanStatuss->loan_status == 5) {
								$loan_status = 'B';
							} elseif ($loanStatuss->loan_status == 6) {
								$loan_status = 'T';
							} elseif ($loanStatuss->loan_status == 7) {
								$loan_status = 'R';
							} else {
								$loan_status = 'N/A';
							}
						}	

						$fetch_search_option[] = array(
							'id' => $property_search[0]->id,
							'text' => $row->property_address,
							'loan_status' => $loan_status
						);
					}
					$data['fetch_search_option'] = $fetch_search_option;
					// $data['loanStatus']= $stats_lon;					
				}

		$sql_all_user11 = $this->User_model->query("SELECT id,fname,middle_name,lname FROM user");
		$data['all_users_data'] = $sql_all_user11->result();



		$data['user_id'] = $this->session->userdata('t_user_id');
		$data['content'] = $this->load->view('tasks/add_contact_note',$data,true);
		$this->load->view('template_files/template',$data);
	}
	public function add_task(){
	 	$task_id = $this->uri->segment(2);
	 	$all_user_contact = $this->userlist();

	 	$duedate_counts = $this->duedatecounts();
	 	$data['duedate_counts'] = $duedate_counts;

	 	// $sql_all_user1 = $this->User_model->query("SELECT * FROM user where account='1'");
	 	$sql_all_user1 = $this->User_model->query("SELECT id,fname,middle_name,lname FROM user");
		$data['all_users_data'] = $sql_all_user1->result();

	 	// echo '<pre>';print_r( $all_user_contact['fetch_search_option']);echo '</pre>';
	 	// $opt = '';
	 	// foreach ($all_user_contact['fetch_search_option'] as $value) {
	 		
	 	// 		$opt .= '<option value="'.$value['id'].'" data-email ="'.$value['email'].'" data-phone="'.$value['phone'].'">'.$value['text'].'</option>';
	 	// }
	 	$data['fetch_all_users'] =   $all_user_contact['fetch_search_option'];
	 	// $data['fetch_all_contacts_opt'] = $opt;

	 	if($task_id>1){
	 			$condition = array();
				$condition['select'] = '*';
				$condition['where'] = array('id' => $task_id);				
				$mydata = $this->CM->getdata('contact_tasks', $condition);
				$data['selected_data'] = $mydata;
				// echo '<pre>';print_r($mydata);echo '</pre>';
				$c_data=array();
				if(!empty($mydata->contact_id)){
					$_contact_id = $mydata->contact_id;
					$condition1['select'] = '*';
					$condition1['where'] = array('contact_id' => $_contact_id);			
					$c_data = $this->CM->getdata('contact', $condition1);
					
				}
				$data['contact_data'] = $c_data;
				
	 	}
	 	$data['view_content_page']=false;
	 	if($this->uri->segment(3)=='view'){
	 		$data['view_content_page']=true;
	 	}
 		$data['val'] = $task_id;
		$data['user_id'] = $this->session->userdata('t_user_id');
		$data['content'] = $this->load->view('tasks/task',$data,true);
		$this->load->view('template_files/template',$data);
	 	
	 	
	 }

	public function delete_task(){
	 	$task_id = $this->uri->segment(3);

	 	error_reporting(0);
    	$result = false;
    	if($task_id == false){
    		$this->session->set_flashdata('error', 'Something went wrong !');
    		redirect(base_url('task_view_list'));
    	}else{
    		$where = array('id'=>$task_id);
			$numRows = $this->CM->datacount('contact_tasks', $where);
				
			if($numRows > 0)
			{
    			$wheres = array('id'=>$task_id);
				$result = $this->CM->deletedata('contact_tasks', $wheres);
				$msg = 'Task Deleted Successfully!';
			}
    	}

    	if($result){
			$this->session->set_flashdata('success',$msg); 
			redirect(base_url('task_view_list'));                
		}else{
			$this->session->set_flashdata('error', 'Something went wrong !');
			redirect(base_url('task_view_list'));   
		}


	} 	


	public function add_update_contact_notes($val=false,$id){
		error_reporting(0);
    	$result = false;
    	$rep = 1;
    	

    	if($val == false){
    		$this->session->set_flashdata('error', 'Something went wrong !');
    		redirect(base_url('task_view_list'));
    	}else{

    		echo 'data available here';
    		$date_notes= $this->input->post('date_notes');
    		if($date_notes){		
				$date_notesDateString 				= input_date_format($date_notes);			
				$_date_notes 		= $date_notesDateString ? $date_notesDateString : '';
			}

    		$enter_by_user= $this->input->post('enter_by_user');
    		$notes_type= $this->input->post('notes_type');
    		if($notes_type == '0'){
    			$_notes_type = '';
    		}else if($notes_type == '1'){
				$_notes_type = 'Email';
    		}else if($notes_type == '2'){
    			$_notes_type = 'Phone';
    		}else if($notes_type == '3'){
    			$_notes_type = 'Letter';
    		}else if($notes_type == '4'){
    			$_notes_type = 'Meeting';
    		}else if($notes_type == '5'){
    			$_notes_type = 'Other';
    		}
    		$purpose_type= $this->input->post('purpose_type');
    		$note_description= $this->input->post('note_description');
    		$property_address= $this->input->post('property_address');
    		$follow_up_task= $this->input->post('follow_up_task');
    		$follow_up_type= $this->input->post('follow_up_type');
    		$follow_up_date= $this->input->post('follow_up_date');
    		$loan_type= $this->input->post('loan_type');
    		if($follow_up_date){		
				$follow_up_dateDateString 		= input_date_format($follow_up_date);			
				$_follow_up_date = $follow_up_dateDateString ? $follow_up_dateDateString : '';
			}


    		$follow_up_status= $this->input->post('follow_up_status');
    		if($follow_up_status == 'outstanding'){
				$_status = 'Active';
    		}else if($follow_up_status == 'complete'){
    				$_status = 'Complete';
    		}else{
    			$_status = 'Active';
    		}

    		$Assign_task= $this->input->post('Assign_task');
    		// print_R($Assign_task);
    		$follow_up_description = $this->input->post('follow_up_description');
    		
    		$assign_by_user_id = $this->input->post('assign_by_user_id');
    		$contact_task_id =$this->input->post('ab_contact_task_id'); 
    		$contac_loan_notes_id =$this->input->post('contac_loan_notes_id');

    		
    		$condition['select'] = '*';
			$condition['where'] = array('contact_id' => $assign_by_user_id);				
			$fetch_contact_data_by_id = $this->CM->getdata('contact', $condition);
			$cont_full_name = $fetch_contact_data_by_id->contact_firstname.' '.$fetch_contact_data_by_id->contact_middlename.' '.$fetch_contact_data_by_id->contact_lastname;
			$primary_phone=$fetch_contact_data_by_id->contact_phone;
			$primary_email=$fetch_contact_data_by_id->contact_email;

			$tuser_id = $this->session->userdata('t_user_id');

    			$array_data = array(
						// "id" =>,	
						"contact_id" =>$assign_by_user_id,	
						"notes_date" =>$_date_notes,		 
						"notes_type" =>$notes_type,		 
						"purpose_type" =>$purpose_type,	 
						"loan_id" => $property_address,		 
						"notes_text" =>$note_description,		 
						"user_id" =>$Assign_task,		 
						"entry_by" =>$enter_by_user,	 
						"loan_note_type" =>$loan_type,	 
						"com_type" =>$notes_type,

						"ab_follow_up_task"=> $follow_up_task,
						"ab_follow_up_type"=> $follow_up_type,
						"ab_follow_up_date"=> $_follow_up_date,
						"ab_follow_up_status"=>$follow_up_status,						
						"ab_follow_up_description"=>$follow_up_description,
						"ab_property_address" => $property_address
						);

    			$task_data = array(
						"contact_status" => $_status,
						"contact_id" => $assign_by_user_id,
						"contact_task" => $follow_up_type,
						"t_user_id" => $tuser_id,						
						"due_date" => $_follow_up_date,
						"contact_date" => $_date_notes,
						"contact_user" => $cont_full_name,
						"primary_phone" => $primary_phone,
						"primary_mail" => $primary_email,
						"task_type" => $follow_up_type,
						"add_people" => $Assign_task,
						"task_notes" => $note_description,
						// "result" => $Result,
						//"date_completed" => $_follow_up_date,						
						);
    		if($id < 0){
				echo 'insert';
				if(!empty($task_data) && $follow_up_task=="Yes"){
					$results = $this->CM->insertdata('contact_tasks', $task_data);
					//array_push($array_data, array('ab_contact_task_id'=>$results));
					$array_data = array_merge($array_data, array('ab_contact_task_id'=>$results));
				}

				if(!empty($array_data)){
					$result1 = $this->CM->insertdata('contact_loan_notes', $array_data);

					$array_data = array_merge($array_data, array('ab_contact_loan_note_id'=>$result1));

					$result = $this->CM->insertdata('contact_notes', $array_data);

					

					
				}
								
				$msg = "Contact Notes created succesfully!";
				$redirects_url = base_url('task_view_list');
				$rep = $result;
				// $this->data();
			}else{
				echo 'update........';

				$where_tsk = array('id'=>$contact_task_id);
				$numRow_s = $this->CM->datacount('contact_tasks', $where_tsk);
				if($numRow_s > 0)
				{
					$results_tsk = $this->CM->updateWithOutAffdata('contact_tasks', $task_data, $where_tsk);
				}else{
					$results = $this->CM->insertdata('contact_tasks', $task_data);
					$array_data = array_merge($array_data, array('ab_contact_task_id'=>$results));
				}



				$where = array('id'=>$id);
				$numRows = $this->CM->datacount('contact_notes', $where);
				// echo '???<pre>'; print_r($val);echo '</pre>'; 
				// echo '???<pre>'; print_r($numRows);echo '</pre>'; 
				if($numRows > 0)
				{
					$result = $this->CM->updateWithOutAffdata('contact_notes', $array_data, $where);

				}


				$where_ln_notes = array('id'=>$contac_loan_notes_id);
				$numRows_ln_notes = $this->CM->datacount('contact_loan_notes', $where_ln_notes);

				if($numRows_ln_notes > 0)
				{
					$result = $this->CM->updateWithOutAffdata('contact_loan_notes', $array_data, $where_ln_notes);
				}


				$rep = $val;
				$redirects_url = base_url('/add_contact_note/'.$assign_by_user_id.'/'.$id);
				

				$msg = "Contact Notes Update succesfully!";
				
			}	


    	}

    	if($result){
    		if($follow_up_task=="Yes"){
    			$redirects_url = base_url('task_view_list');
    		}else{
    			$redirects_url = base_url('/viewcontact/'.$val); 
    		}
			$this->session->set_flashdata('success',$msg); 
			 
			redirect($redirects_url);              
		}else{
			$this->session->set_flashdata('error', 'Something went wrong !');
			// redirect(base_url('task_view'));   
		}

		$data['content'] = $this->load->view('tasks/add_contact_note',$data,true);
		$this->load->view('template_files/template',$data);

	}

	
	public function task_form($val=false) {
    	// print_r($this->input->post());

    	error_reporting(0);
    	$result = false;
    	$rep = 1;
    	$redirects_url = base_url('task_view_list');
    	if($val == false){
    		$this->session->set_flashdata('error', 'Something went wrong !');
    		redirect(base_url('task_view_list'));
    	}else{
    		// echo $this->uri->segment(3);
    		$status = $this->input->post('status');
			$date_ent = $this->input->post('date_entered');
			$date_entered = $date_ent[0];

			$date_due = $this->input->post('date_due');
			$due_date = $date_due[0];

			$contact_name = $this->input->post('contact_name');
			$primary_phone = $this->input->post('primary_phone');
			$primary_email = $this->input->post('primary_email');
			$task_type = $this->input->post('task_type');
			$add_people = $this->input->post('add_people');
			$add_people_array=$add_people;
			$add_people=implode(",", $add_people);
			$add_people=trim($add_people);
			$description = $this->input->post('description');
			$Result = $this->input->post('Result');
			$d_completed = $this->input->post('date_completed');
			$date_completed = $d_completed[0];

			$contact_id = $contact_name;
			$user_id = $this->session->userdata('t_user_id');
			$note_title = $this->input->post('note_title');
			$conditions['select'] = '*';
				$conditions['where'] = array('contact_id' => $contact_id);				
				$contacts = $this->CM->getdata('contact', $conditions);
				// echo '<pre>??';print_r($contacts); echo '</pre>';
				$_contacts_nm = $contacts->contact_firstname;
					if($contacts->contact_middlename != '')
					{
						$_contacts_nm .= ' '.$contacts->contact_middlename;
					}

					if($contacts->contact_lastname != '')
					{
						$_contacts_nm .= ' '.$contacts->contact_lastname;
					}




			if($date_entered){		
				//$myDateTime 				= DateTime::createFromFormat('m-d-Y', $contact_date);
				//$newDateString 				= $myDateTime->format('Y-m-d');
				$enteredDateString 				= input_date_format($date_entered);
			
				$_date_entered 		= $enteredDateString ? $enteredDateString : '';
			}

			if($due_date){				
				$dueDateString 				= input_date_format($due_date);
				$_due_date 		= $dueDateString ? $dueDateString : '';
			}
			$_date_completed="";
			if($date_completed){				
				$compDateString 				= input_date_format($date_completed);
				$_date_completed 		= $compDateString ? $compDateString : '';
			}
			



			$array3 = array(
						"contact_status" => $status,
						"contact_id" => $contact_id,
						"contact_task" => $task_type,
						"t_user_id" => $user_id,						
						"due_date" => $_due_date,
						"contact_date" => $_date_entered,
						"contact_user" => $_contacts_nm,
						"primary_phone" => $primary_phone,
						"primary_mail" => $primary_email,
						"task_type" => $task_type,
						"add_people" => $add_people,
						"task_notes" => $description,
						"result" => $Result,
						"note_title"=>$note_title,
						"date_completed" => $_date_completed						
						);

			
				// echo '<pre>'; print_r($array3);			
			$data['user_id'] = $this->session->userdata('t_user_id');
			if($val == 'add'){
				if(!empty($array3)){
					$result = $this->CM->insertdata('contact_tasks', $array3);
					//Mail
					if(!empty($add_people_array)){
						foreach ($add_people_array as $key => $value) {
							$que="SELECT id,fname,middle_name,lname,email_address FROM user where id='$value' ";
							$getAllUsersSql = $this->User_model->query($que);
							$usersData = $getAllUsersSql->result();
							if(!empty($usersData)){
								$fullName="";
								if(!empty($usersData[0]->fname)){
									$fullName=$usersData[0]->fname;
									$email=$usersData[0]->email_address;
									$contentMail = '<p>Hi '.$fullName.',</p>';
									$contentMail.= '<p>You have been added to the following task</p>';
									$contentMail.= '<p><a href="'.base_url().'add_task/'.$result.'/view">'.$note_title.'</p>';
									$dataMail = array();
							        $dataMail['from_email']  	= 'amarjain@bitcot.com';
							        $dataMail['from_name']   	= 'TaliMar Financial';
							        $dataMail['to']      		= $email;
							        $dataMail['cc']      		= '';
							        $dataMail['bcc']      		= '';
							        $dataMail['name']      		= '';
							        $dataMail['button_url']  	= '';
							        $dataMail['attach_file']   	= '';
							        $dataMail['subject']     	= "Added to Task";
							        $dataMail['content_body']  	= $contentMail;
							        send_mail_template($dataMail);
								}
							}
						}
					}
					//Mail End
				}				
				$msg = "Task created succesfully!";
				$rep = $result;
				// $this->data();
				redirect(base_url('task_view_list'));  
			}else{
				//echo 'update';
				//$Result = $this->CM->updateWithOutAffdata('lender_mortgage', $array3);
				//$data = array('mortgageActivate' => 'yes');
				$where = array('id'=>$val);
				$numRows = $this->CM->datacount('contact_tasks', $where);
				// echo '???<pre>'; print_r($val);echo '</pre>'; 
				// echo '???<pre>'; print_r($numRows);echo '</pre>'; 
				if($numRows > 0)
				{
					$result = $this->CM->updateWithOutAffdata('contact_tasks', $array3, $where);

				}
				$msg = "Update Successfully!";
				$rep = $val;
				$redirects_url = base_url('/add_task/'.$rep);
			}
    	}
    	
    	if($result){
			$this->session->set_flashdata('success',$msg); 
			// redirect(base_url('/add_task/'.$rep));  
			redirect($redirects_url);              
		}else{
			$this->session->set_flashdata('error', 'Something went wrong !');
			redirect(base_url('task_view_list'));  
		}
    	
			$data['content'] = $this->load->view('tasks/task',$data,true);
			$this->load->view('template_files/template',$data);
    	
		
		
	}

	public function all_task_view(){

		$condition['select'] = '*';
				// $condition['where'] = array('id' => $task_id);				
		$alldata = $this->CM->getdata('contact_tasks', $condition);
		$data['all_task_data'] = $alldata;
		$data['content'] = $this->load->view('tasks/task_view',$data,true);
			$this->load->view('template_files/template',$data);
	}

	/*------------------------------------------------
	* 09-07-2021
	* get count on contact_task if Task 
	* Due Date is today or past due, make button red
	*
	*------------------------------------------------*/
	public function duedatecounts(){
		// $condition['select'] = '*';
		// 		$condition['where'] = array('id' => $task_id);				
		// $alldata = $this->CM->getdata('contact_tasks', $condition);
		// $data['all_task_data'] = $alldata;

		// $_datas = $this->User_model->query("SELECT * FROM contact_tasks WHERE DATE(due_date) >= DATE(NOW())");
		$_datas = $this->User_model->query("SELECT * FROM contact_tasks WHERE DATE(due_date) >= DATE(NOW())");
		// $record = $_datas->row();
		$record = $_datas->num_rows();
		return $record;
		// $data['content'] = $this->load->view('tasks/task',$data,true);
		// 	$this->load->view('template_files/template',$data);
		// $where = array('id'=>$post['id']);
		// 	$numRows = $this->CM->datacount('investor', $where);
				
		// 	if($numRows > 0)
		// 	{}
	}
	function add_new_people_select(){
		$people_row_count= $this->input->post('people_row_count');
		$selected_value= $this->input->post('selected_value');
		$where="";
		if(!empty($selected_value)){
			$where=implode(",", $selected_value);
		}
		if(!empty($where)){
			$que="SELECT id,fname,middle_name,lname FROM user where id not in ($where)";
		}else{
			$que="SELECT id,fname,middle_name,lname FROM user";
		}
		$data['people_row_count']=$people_row_count;
		$getAllUsersSql = $this->User_model->query($que);
		$data['all_users_data'] = $getAllUsersSql->result();
		$content = $this->load->view('tasks/ajax_add_people',$data,true);
		echo $content;
	}
	
}


