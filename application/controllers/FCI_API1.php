<?php

class FCI_API extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		//$this->load->helper(array('form', 'url'));
		$this->load->model('User_model');
		$this->load->library('Aws3','aws3');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');

		if($this->session->userdata('t_user_id') != '' )
		{
		}
		else
		{
			redirect(base_url()."home",'refresh');
		}
		ini_set("memory_limit", "-1");
	}

	//testing url: https://database.talimarfinancial.com/FCI_API/testAPI
	public function save_datain_fci(){

		if(isset($_POST['submitFCI'])){

			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => 'https://iapi.trustfci.com:8075/graphql',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_POSTFIELDS =>'{"query":"mutation{\\n   insertBoarding\\n   (\\n       insertLoan:\\n       {\\n            prevAccount:\\"'.$this->input->post('talimar_loan').'\\",\\n            lienPosition:'.$this->input->post('position').'\\n            lenderAccount:\\"\\",\\n            fundingDate:\\"'.$this->input->post('loan_funding_date').'\\",\\n            firstPaymentDate:\\"'.$this->input->post('first_payment_date').'\\",\\n            paidToDate:\\"'.$this->input->post('paid_to_date').'\\"\\n            nextDueDate:\\"'.$this->input->post('next_due_date').'\\"\\n            originalBalance:'.$this->input->post('original_balance').'\\n            principalBalance:'.$this->input->post('loan_amount').'\\n            lateChargesDays:'.$this->input->post('late_charges').'\\n            payment:'.$this->input->post('payment').'\\n            paymentImpound:'.$this->input->post('payment_impound').'\\n            paymentFrequency:'.$this->input->post('payment_frequency').'\\n            maturityDate:\\"'.$this->input->post('maturity_date').'\\"\\n            prepaymentExpDate:\\"'.$this->input->post('exp_date').'\\"\\n            prepaymentTerm:'.$this->input->post('term_month').'\\n            amortizationType:'.$this->input->post('amortization_type').'\\n            rateType:'.$this->input->post('rate_type').'\\n            noteRate:'.$this->input->post('note_rate').'\\n            soldRate:'.$this->input->post('sold_rate').'\\n            setBorrower:\\n            [\\n                {\\n                fullName:\\"'.$this->input->post('b_full_name').'\\"\\n                firstName:\\"\\"\\n                middleName:\\"\\"\\n                lastName:\\"\\"\\n                street:\\"'.$this->input->post('b_street').'\\"\\n                city:\\"'.$this->input->post('b_city').'\\"\\n                state:\\"'.$this->input->post('b_state').'\\"\\n                zipCode:\\"'.$this->input->post('b_zip').'\\"\\n                homePhone:\\"'.$this->input->post('b_home_phone').'\\"\\n                workPhone:\\"'.$this->input->post('b_work_phone').'\\"\\n                mobilePhone:\\"'.$this->input->post('b_mobile').'\\"\\n                fax:\\"'.$this->input->post('b_fax').'\\"\\n                tin:\\"'.$this->input->post('b_tin').'\\"\\n                email:\\"'.$this->input->post('b_email').'\\"\\n                isCompany:true\\n                contactName:\\"'.$this->input->post('b_contact_name').'\\"\\n                isPrimary:true\\n                company:\\"'.$this->input->post('b_company').'\\"\\n                }\\n            ]\\n            setProperties:\\n            [\\n                {\\n                description:\\"'.$this->input->post('pro_description').'\\"\\n                street:\\"'.$this->input->post('property_address').'\\"\\n                city:\\"'.$this->input->post('property_city').'\\"\\n                state:\\"'.$this->input->post('property_state').'\\"\\n                zipCode:\\"'.$this->input->post('property_zip').'\\"\\n                country:\\"'.$this->input->post('pro_country').'\\"\\n                type:'.$this->input->post('property_type').'\\n                isPrimary:true\\n                occupancyStatus:'.$this->input->post('occupancy_status').'\\n                }\\n            ]\\n        }\\n    )\\n}","variables":{}}',
			  CURLOPT_HTTPHEADER => array(
			    'Authorization: Bearer TnTaHakqp440vi0iNc7XfXLjBK9zymmdSLnfraquGb0',
			    'Content-type: application/json'
			  ),
			));

			$response = curl_exec($curl);

			if(curl_errno($curl))
			{
			    $this->session->set_flashdata('error', 'Curl Error: ' . curl_error($curl));
				redirect(base_url().'load_data/'.$this->input->post('loan_id'));
			}else{
				
				$APIResult = json_decode($response);
				if($APIResult->data->insertBoarding != ''){

					//echo 'res is: '.$APIResult->data->insertBoarding;
					$checkcode['talimar_loan'] 	= $this->input->post('talimar_loan');
					$checkcode['access_code'] 	= $APIResult->data->insertBoarding;
					$checkcode['datetime'] 		= date('Y-m-d h:i:s');

					$this->User_model->insertdata('fci_boarding',$checkcode);

					$this->session->set_flashdata('success', 'Data send to FCI successfully: '.$APIResult->data->insertBoarding);
					$this->session->set_flashdata('popup_modal_cv', array('FCI_Documents'));
					redirect(base_url().'load_data/'.$this->input->post('loan_id'));
				}else{
				
					$this->session->set_flashdata('error', 'Error: Before send to FCI please check all fields values are filled or not');
					$this->session->set_flashdata('popup_modal_cv', array('FCI_Documents'));
					redirect(base_url().'load_data/'.$this->input->post('loan_id'));
				}
			}
		}
	}


	public function monthly_payment($aa) {
		error_reporting(0);
		$talimar_no['talimar_loan'] = $aa;

		$fetch_loan_data = $this->User_model->select_where('loan', $talimar_no);
		$fetch_loan_data = $fetch_loan_data->result();

		$fetch_impound_account1 = $this->User_model->select_where_asc('impound_account', $talimar_no, 'position');
		if ($fetch_impound_account1->num_rows() > 0) {
			$fetch_impound_account = $fetch_impound_account1->result();
		}

		$fetch_property_sql = "SELECT * FROM loan_property JOIN property_home ON property_home.id = loan_property.property_home_id WHERE loan_property.talimar_loan = '" . $talimar_loan . "' AND  property_home.primary_property = '1' ";
		$fetch_property_data = $this->User_model->query($fetch_property_sql);
		$fetch_property_data = $fetch_property_data->result();

		$aa = 0;

		if (isset($fetch_impound_account)) {

			foreach ($fetch_impound_account as $key => $row) {
				$impound_amount = $row->amount;
				$readonly = '';

				if ($row->items == 'Mortgage Payment') {

					$aa += ($fetch_loan_data[0]->intrest_rate / 100) * $fetch_loan_data[0]->loan_amount / 12;
				}
				if ($row->items == 'Property / Hazard Insurance Payment') {
					if ($row->impounded == 1) {
						// $aa += isset($fetch_property_data) ? ($loan_property_insurance_result->annual_insurance_payment / 12) : 0;
					}
				}

				if ($row->items == 'County Property Taxes') {
					if ($row->impounded == 1) {
						$aa += isset($fetch_property_data) ? ($fetch_property_data[0]->annual_property_taxes / 12) : 0;
					}
				}
				if ($row->items == 'Flood Insurance') {
					if ($row->impounded == 1) {
						$aa += isset($row->amount) ? ($row->amount) : 0;
					}
				}

				if ($row->items == 'Mortgage Insurance') {
					if ($row->impounded == 1) {
						$aa += isset($row->amount) ? ($row->amount) : 0;
					}
				}
			}
		}

		return $aa;
	}

	public function FCIBoardingDataCSVFILE() {

		$talimar_loan = $this->input->post('talimar_no');
		$loan_type_option = $this->config->item('loan_type_option');
		$position_option = $this->config->item('position_option');
		$usa_city_county = $this->config->item('usa_city_county');
		$property_type_opt = $this->config->item('property_modal_property_type');
		$occupancy_value_option 	= $this->config->item('occupancy_value_option');
		$owner_purpose 	= $this->config->item('loan_purpose_option_type');
		$rate_type_option 	= $this->config->item('loan_intrest_type_option');
		$payment_sech_option 	= $this->config->item('loan_payment_schedule_option');
		$am_option 	= $this->config->item('loan_payment_type_option');
		$who_pays 	= $this->config->item('serviceing_who_pay_servicing_option');
		$pay_setup_fee 	= $this->config->item('who_pay_setup_fees');
		$how_setup_fee 	= $this->config->item('how_setup_fees_paid');
		$contact_number_option = $this->config->item('contact_number_option');

		//loan & loan_servicing
		$fetch_all_datafor_loan = $this->User_model->query("SELECT *, l.borrower as borrower_id FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE l.talimar_loan = '".$talimar_loan."'");
		$fetch_all_datafor_loan = $fetch_all_datafor_loan->result();

		if($fetch_all_datafor_loan[0]->min_payment == 1){
			$min_payment_value = 'Yes';
			$has_payment_value = $fetch_all_datafor_loan[0]->minium_intrest;
		}else{
			$min_payment_value = 'No';
			$has_payment_value = 0;
		}

		//loan_property
		$fetch_all_property_loan = $this->User_model->query("SELECT * FROM loan_property WHERE talimar_loan = '".$talimar_loan."'");
		$fetch_all_property_loan = $fetch_all_property_loan->result();

		

		$contact_firstname = '';
		$contact_lastname  = '';
		$primary_option    = '';
		$Borrower_all_info = '';
		$fetch_contactdata = '';

		if($fetch_all_datafor_loan){
			if($fetch_all_datafor_loan[0]->borrower_id > 0){
				//borrower_data
				$Borrower_all_info = $this->User_model->query("SELECT * FROM borrower_data WHERE id = '".$fetch_all_datafor_loan[0]->borrower_id."'");
				$Borrower_all_info = $Borrower_all_info->result();

				//Contact data
				$fetch_contactdata = $this->User_model->query("SELECT c.contact_firstname, c.contact_lastname, c.primary_option FROM borrower_contact as bc JOIN contact as c ON bc.contact_id = c.contact_id WHERE bc.borrower_id = '".$fetch_all_datafor_loan[0]->borrower_id."'");
				$fetch_contactdata = $fetch_contactdata->result();

				if($fetch_contactdata){
					$contact_firstname = $fetch_contactdata[0]->contact_firstname;
					$contact_lastname  = $fetch_contactdata[0]->contact_lastname;
					$primary_option  = $fetch_contactdata[0]->primary_option;
				}
			}
		}

		//loan_assignment
		$fetch_all_assignment = $this->User_model->query("SELECT * FROM loan_assigment WHERE talimar_loan = '".$talimar_loan."'");
		$fetch_all_assignment = $fetch_all_assignment->result();

		$lender_ratessss = 0;
		if($fetch_all_datafor_loan){
				if($fetch_all_datafor_loan[0]->servicing_lender_rate == 'NaN' || $fetch_all_datafor_loan[0]->servicing_lender_rate == '' || $fetch_all_datafor_loan[0]->servicing_lender_rate == '0.00'){
					if($fetch_all_assignment){
						if($fetch_all_assignment[0]->invester_yield_percent > 0){

							$lender_ratessss = number_format($fetch_all_assignment[0]->invester_yield_percent,3);
						}
					}
				}else{

					$lender_ratessss = number_format($fetch_all_datafor_loan[0]->servicing_lender_rate,3);
				}
		}
		

		//
		$impound_amount = $this->User_model->query("SELECT `impounded` FROM `impound_account` WHERE `talimar_loan`='".$talimar_loan."'");
		$impound_amount = $impound_amount->result();
		if($impound_amount[0]->impounded == '1'){
			$impound = 'Yes';
		}else{
			$impound = 'No';
		}

		//late fee
		$getlenderfee = $this->User_model->query("SELECT broker FROM `fee_disbursement` WHERE `talimar_loan`='".$talimar_loan."' AND `items`='Late Fee' ");
		if($getlenderfee->num_rows() > 0){
			$getlenderfee = $getlenderfee->result();
			$lender_fee_val = $getlenderfee[0]->broker/100;
		}else{
			$lender_fee_val = 50/100;
		}

		//Default Rate
		$getdddlenderfee = $this->User_model->query("SELECT broker FROM `fee_disbursement` WHERE `talimar_loan`='".$talimar_loan."' AND `items`='Default Rate' ");
		if($getdddlenderfee->num_rows() > 0){
			$getdddlenderfee = $getdddlenderfee->result();
			$ddlender_fee_val = $getdddlenderfee[0]->broker/100;
		}else{
			$ddlender_fee_val = 75/100;
		}

		//MinimumInterest
		$MinimumInterestfee = $this->User_model->query("SELECT broker FROM `fee_disbursement` WHERE `talimar_loan`='".$talimar_loan."' AND `items`='Minimum Interest' ");
		if($MinimumInterestfee->num_rows() > 0){
			$MinimumInterestfee = $MinimumInterestfee->result();
			$MinimumInterest = $MinimumInterestfee[0]->broker/100;
		}else{
			$MinimumInterest = 0;
		}

		$talimar_no['talimar_loan'] = $talimar_loan;

		$paid_to_other_814 = 0;
		$paid_to_other_1005 = 0;
		$paid_to_other_1006 = 0;
		$fetch_closing_statement_items = $this->User_model->select_where_asc('closing_statement_items', $talimar_no, 'hud');
		$fetch_closing_statement_items = $fetch_closing_statement_items->result();
		
		foreach ($fetch_closing_statement_items as $row) {
			if ($row->hud == 814) {
				$paid_to_other_814 = $row->paid_to_other;
			}

			if ($row->hud == 1005) {
				$paid_to_other_1005 = $row->paid_to_other;
			}

			if ($row->hud == 1006) {
				$paid_to_other_1006 = $row->paid_to_other;
			}
		}

		//PaidtoDate
		$paidToDatedata = $this->User_model->query("SELECT PaidtoDate FROM `loan_recording_information` WHERE `talimar_loan`='".$talimar_loan."'");
		$paidToDatedata = $paidToDatedata->result();

		//default_rate
		if($fetch_all_datafor_loan[0]->default_rate > 0){
			$default_rate = $fetch_all_datafor_loan[0]->default_rate/100;
		}else{
			$default_rate = 17/100;
		}


		require_once APPPATH."/third_party/excel/PHPExcel.php";
		$objPHPExcel = new PHPExcel();
        
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Furkan Kahveci")
							 ->setLastModifiedBy("Furkan Kahveci")
							 ->setTitle("Office 2007 XLS Test Document")
							 ->setSubject("Office 2007 XLS Test Document")
							 ->setDescription("Description for Test Document")
							 ->setKeywords("phpexcel office codeigniter php")
							 ->setCategory("Test result file");

        // Create a first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', "FCI Loan No #");
        $objPHPExcel->getActiveSheet()->setCellValue('B1', "Originator (Entity receiving spread)");
        $objPHPExcel->getActiveSheet()->setCellValue('C1', "# of Lenders");
        $objPHPExcel->getActiveSheet()->setCellValue('D1', "Borrower Name per Note  (Not Guarantor)");
        $objPHPExcel->getActiveSheet()->setCellValue('E1', "Borrower EIN or SSN");
        $objPHPExcel->getActiveSheet()->setCellValue('F1', "Contact First Name");
        $objPHPExcel->getActiveSheet()->setCellValue('G1', "Contact Last Name");
        $objPHPExcel->getActiveSheet()->setCellValue('H1', "Contact Address/Street");
        $objPHPExcel->getActiveSheet()->setCellValue('I1', "Contact Unit #");
        $objPHPExcel->getActiveSheet()->setCellValue('J1', "Contact Address/City");
        $objPHPExcel->getActiveSheet()->setCellValue('K1', "Contact Address/State");
        $objPHPExcel->getActiveSheet()->setCellValue('L1', "Contact Address/Zip");
        $objPHPExcel->getActiveSheet()->setCellValue('M1', "Contact Work Phone");
        $objPHPExcel->getActiveSheet()->setCellValue('N1', "Phone Type");
        $objPHPExcel->getActiveSheet()->setCellValue('O1', "Contact Email Address");
        $objPHPExcel->getActiveSheet()->setCellValue('P1', "Property Address/Street");
        $objPHPExcel->getActiveSheet()->setCellValue('Q1', "Property Unit #");
        $objPHPExcel->getActiveSheet()->setCellValue('R1', "Property Address/City");
        $objPHPExcel->getActiveSheet()->setCellValue('S1', "Property Address/State");
        $objPHPExcel->getActiveSheet()->setCellValue('T1', "Property Address/Zip");
        $objPHPExcel->getActiveSheet()->setCellValue('U1', "Property Address/County");
        $objPHPExcel->getActiveSheet()->setCellValue('V1', "Property Type");
        $objPHPExcel->getActiveSheet()->setCellValue('W1', "Occupancy Type");
        $objPHPExcel->getActiveSheet()->setCellValue('X1', "Loan Purpose");
        $objPHPExcel->getActiveSheet()->setCellValue('Y1', "Loan Position");
        $objPHPExcel->getActiveSheet()->setCellValue('Z1', "Rate Type");
        $objPHPExcel->getActiveSheet()->setCellValue('AA1', "Prepaid Interest Paid at Settlement: $");
        $objPHPExcel->getActiveSheet()->setCellValue('AB1', "Funding Date");
        $objPHPExcel->getActiveSheet()->setCellValue('AC1', "1st payment due date");
        $objPHPExcel->getActiveSheet()->setCellValue('AD1', "Paid to Date");
        $objPHPExcel->getActiveSheet()->setCellValue('AE1', "Next Payment Due Date");
        $objPHPExcel->getActiveSheet()->setCellValue('AF1', "Original Loan Amount");
        $objPHPExcel->getActiveSheet()->setCellValue('AG1', "Current Principal Bal");
        $objPHPExcel->getActiveSheet()->setCellValue('AH1', "Grace days prior to Late Fee Assessement");
        $objPHPExcel->getActiveSheet()->setCellValue('AI1', "Late Charge Percentage");
        $objPHPExcel->getActiveSheet()->setCellValue('AJ1', "Grace days prior to Default Interest Assessment");
        $objPHPExcel->getActiveSheet()->setCellValue('AK1', "Default Interest Rate");
        $objPHPExcel->getActiveSheet()->setCellValue('AL1', "Default Rate Replaces Late Fee?");
        $objPHPExcel->getActiveSheet()->setCellValue('AM1', "Activate Default Rate at Setup");
        $objPHPExcel->getActiveSheet()->setCellValue('AN1', "Current monthly payment amount");
        $objPHPExcel->getActiveSheet()->setCellValue('AO1', "Payments: Monthly/Quarterly/Other");
        $objPHPExcel->getActiveSheet()->setCellValue('AP1', "Note Maturity Date");
        $objPHPExcel->getActiveSheet()->setCellValue('AQ1', "Minimum Payment");
        $objPHPExcel->getActiveSheet()->setCellValue('AR1', "Prepayment Terms");
        $objPHPExcel->getActiveSheet()->setCellValue('AS1', "Amortization Type");
        $objPHPExcel->getActiveSheet()->setCellValue('AT1', "Note Interest Rate");
        $objPHPExcel->getActiveSheet()->setCellValue('AU1', "Sold Rate  (to Investor/Lender)");
        $objPHPExcel->getActiveSheet()->setCellValue('AV1', "FCI Servicing Fee paid by Broker or Invstor?");
        $objPHPExcel->getActiveSheet()->setCellValue('AW1', "Escrow for Taxes and/or Ins included in Borrower Payment Y/N");
        $objPHPExcel->getActiveSheet()->setCellValue('AX1', "Late Fee Disbursement to FCI: %");
        $objPHPExcel->getActiveSheet()->setCellValue('AY1', "Late Fee Disbursement to Broker: %");
        $objPHPExcel->getActiveSheet()->setCellValue('AZ1', "Late Fee Disbursement to Lender: %");
        $objPHPExcel->getActiveSheet()->setCellValue('BA1', "Default Rate Disbursement Broker %");
        $objPHPExcel->getActiveSheet()->setCellValue('BB1', "Default Rate Disbursement Lender: %");
        $objPHPExcel->getActiveSheet()->setCellValue('BC1', "Prepayment Penalty Broker: %");
        $objPHPExcel->getActiveSheet()->setCellValue('BD1', "Prepayment Penalty Lender: %");
        $objPHPExcel->getActiveSheet()->setCellValue('BE1', "Set up fee paid by Broker or Lender");
        $objPHPExcel->getActiveSheet()->setCellValue('BF1', "Set Up fee:  Billed or Take from first distribution");
        $objPHPExcel->getActiveSheet()->setCellValue('BG1', "Trust/Suspense Balance");
        $objPHPExcel->getActiveSheet()->setCellValue('BH1', "Payment Reserve");
        $objPHPExcel->getActiveSheet()->setCellValue('BI1', "Renovation Reserve");
        $objPHPExcel->getActiveSheet()->setCellValue('BJ1', "Fund Control Draw Fees");
        $objPHPExcel->getActiveSheet()->setCellValue('BK1', "Escrow Balance/Int");


   		$objPHPExcel->getActiveSheet()->setCellValue('A2', $fetch_all_datafor_loan ?  $fetch_all_datafor_loan[0]->fci : '');
        $objPHPExcel->getActiveSheet()->setCellValue('B2', "TaliMar Financial Inc.");
        $objPHPExcel->getActiveSheet()->setCellValue('C2', COUNT($fetch_all_assignment));
        $objPHPExcel->getActiveSheet()->setCellValue('D2', $Borrower_all_info ?  $Borrower_all_info[0]->b_name : '');
        $objPHPExcel->getActiveSheet()->setCellValue('E2', $Borrower_all_info ?  $Borrower_all_info[0]->b_tax_id : '');
        $objPHPExcel->getActiveSheet()->setCellValue('F2', $contact_firstname);
        $objPHPExcel->getActiveSheet()->setCellValue('G2', $contact_lastname);
        $objPHPExcel->getActiveSheet()->setCellValue('H2', $Borrower_all_info ?  $Borrower_all_info[0]->b_address : '');
        $objPHPExcel->getActiveSheet()->setCellValue('I2', $Borrower_all_info ?  $Borrower_all_info[0]->b_unit : '');
        $objPHPExcel->getActiveSheet()->setCellValue('J2', $Borrower_all_info ?  $Borrower_all_info[0]->b_city : '');
        $objPHPExcel->getActiveSheet()->setCellValue('K2', $Borrower_all_info ?  $Borrower_all_info[0]->b_state : '');
        $objPHPExcel->getActiveSheet()->setCellValue('L2', $Borrower_all_info ?  $Borrower_all_info[0]->b_zip : '');
        $objPHPExcel->getActiveSheet()->setCellValue('M2', $Borrower_all_info ?  $Borrower_all_info[0]->c_phone : '');
        $objPHPExcel->getActiveSheet()->setCellValue('N2', $primary_option ?  $contact_number_option[$primary_option] : '');
        $objPHPExcel->getActiveSheet()->setCellValue('O2', $Borrower_all_info ?  $Borrower_all_info[0]->c_email : '');
        $objPHPExcel->getActiveSheet()->setCellValue('P2', $fetch_all_property_loan ?  $fetch_all_property_loan[0]->property_address : '');
        $objPHPExcel->getActiveSheet()->setCellValue('Q2', $fetch_all_property_loan ?  $fetch_all_property_loan[0]->unit : '');
        $objPHPExcel->getActiveSheet()->setCellValue('R2', $fetch_all_property_loan ?  $fetch_all_property_loan[0]->city : '');
        $objPHPExcel->getActiveSheet()->setCellValue('S2', $fetch_all_property_loan ?  $fetch_all_property_loan[0]->state : '');
        $objPHPExcel->getActiveSheet()->setCellValue('T2', $fetch_all_property_loan ?  $fetch_all_property_loan[0]->zip : '');
        $objPHPExcel->getActiveSheet()->setCellValue('U2', $fetch_all_property_loan ?  $usa_city_county[$fetch_all_property_loan[0]->country] : '');
        $objPHPExcel->getActiveSheet()->setCellValue('V2', $fetch_all_property_loan ?  $property_type_opt[$fetch_all_property_loan[0]->property_type] : '');
        $objPHPExcel->getActiveSheet()->setCellValue('W2', $occupancy_value_option ?  $occupancy_value_option[$fetch_all_property_loan[0]->occupancy_type] : '');
        $objPHPExcel->getActiveSheet()->setCellValue('X2', $fetch_all_datafor_loan ?  $owner_purpose[$fetch_all_datafor_loan[0]->owner_purpose] : '');
        $objPHPExcel->getActiveSheet()->setCellValue('Y2', $fetch_all_datafor_loan ?  $position_option[$fetch_all_datafor_loan[0]->position] : '');
        $objPHPExcel->getActiveSheet()->setCellValue('Z2', $fetch_all_datafor_loan ?  $rate_type_option[$fetch_all_datafor_loan[0]->intrest_type] : '');
        $objPHPExcel->getActiveSheet()->setCellValue('AA2', "");
        $objPHPExcel->getActiveSheet()->setCellValue('AB2', $fetch_all_datafor_loan ?  date('m-d-Y', strtotime($fetch_all_datafor_loan[0]->loan_funding_date)) : '');
        $objPHPExcel->getActiveSheet()->setCellValue('AC2', $fetch_all_datafor_loan ?  date('m-d-Y', strtotime($fetch_all_datafor_loan[0]->first_payment_date)) : '');
        if($paidToDatedata){
        	$objPHPExcel->getActiveSheet()->setCellValue('AD2', $paidToDatedata ?  date('m-d-Y', strtotime($paidToDatedata[0]->PaidtoDate)) : '');
        }else{
        	$objPHPExcel->getActiveSheet()->setCellValue('AD2', '');
        }      
        $objPHPExcel->getActiveSheet()->setCellValue('AE2', $fetch_all_datafor_loan ?  date('m-d-Y', strtotime($fetch_all_datafor_loan[0]->first_payment_date)) : '');
        $objPHPExcel->getActiveSheet()->setCellValue('AF2', $fetch_all_datafor_loan ?  $fetch_all_datafor_loan[0]->loan_amount : '');
        $objPHPExcel->getActiveSheet()->setCellValue('AG2', $fetch_all_datafor_loan ?  $fetch_all_datafor_loan[0]->current_balance : '');
        $objPHPExcel->getActiveSheet()->setCellValue('AH2', $fetch_all_datafor_loan ?  $fetch_all_datafor_loan[0]->grace_period : '');
        $objPHPExcel->getActiveSheet()->setCellValue('AI2', $fetch_all_datafor_loan ?  $fetch_all_datafor_loan[0]->late_charges_due/100 : '');
        $objPHPExcel->getActiveSheet()->setCellValue('AJ2', $fetch_all_datafor_loan ?  $fetch_all_datafor_loan[0]->grace_period : '');
        $objPHPExcel->getActiveSheet()->setCellValue('AK2', number_format($default_rate,3));
        $objPHPExcel->getActiveSheet()->setCellValue('AL2', "No");
        $objPHPExcel->getActiveSheet()->setCellValue('AM2', "No");
        $objPHPExcel->getActiveSheet()->setCellValue('AN2', $this->monthly_payment($talimar_loan));
        $objPHPExcel->getActiveSheet()->setCellValue('AO2', $fetch_all_datafor_loan ?  $payment_sech_option[$fetch_all_datafor_loan[0]->payment_sechdule] : '');
        $objPHPExcel->getActiveSheet()->setCellValue('AP2', $fetch_all_datafor_loan ?  date('m-d-Y', strtotime($fetch_all_datafor_loan[0]->maturity_date)) : '');
        $objPHPExcel->getActiveSheet()->setCellValue('AQ2', $min_payment_value);
        $objPHPExcel->getActiveSheet()->setCellValue('AR2', $has_payment_value);
        $objPHPExcel->getActiveSheet()->setCellValue('AS2', $fetch_all_datafor_loan ?  $am_option[$fetch_all_datafor_loan[0]->payment_type] : '');
        $objPHPExcel->getActiveSheet()->setCellValue('AT2', $fetch_all_datafor_loan ?  number_format(($fetch_all_datafor_loan[0]->intrest_rate/100),4) : '');
        $objPHPExcel->getActiveSheet()->setCellValue('AU2', number_format(($lender_ratessss/100),4));
        $objPHPExcel->getActiveSheet()->setCellValue('AV2', $fetch_all_datafor_loan ?  $who_pays[$fetch_all_datafor_loan[0]->who_pay_servicing] : '');
        $objPHPExcel->getActiveSheet()->setCellValue('AW2', $impound);
        $objPHPExcel->getActiveSheet()->setCellValue('AX2', "");
        $objPHPExcel->getActiveSheet()->setCellValue('AY2', $lender_fee_val);
        $objPHPExcel->getActiveSheet()->setCellValue('AZ2', number_format(1 - $lender_fee_val,2));
        $objPHPExcel->getActiveSheet()->setCellValue('BA2', $ddlender_fee_val);
        $objPHPExcel->getActiveSheet()->setCellValue('BB2', number_format(1 - $ddlender_fee_val,2));
        $objPHPExcel->getActiveSheet()->setCellValue('BC2', $MinimumInterest);
        $objPHPExcel->getActiveSheet()->setCellValue('BD2', "1");
        $objPHPExcel->getActiveSheet()->setCellValue('BE2', $fetch_all_datafor_loan ?  $pay_setup_fee[$fetch_all_datafor_loan[0]->pay_setup_fee] : '');
        $objPHPExcel->getActiveSheet()->setCellValue('BF2', $fetch_all_datafor_loan ?  $how_setup_fee[$fetch_all_datafor_loan[0]->how_setup_fee_pay] : '');
        $objPHPExcel->getActiveSheet()->setCellValue('BG2', "");
        $objPHPExcel->getActiveSheet()->setCellValue('BH2', $paid_to_other_1006);
        $objPHPExcel->getActiveSheet()->setCellValue('BI2', $paid_to_other_1005);
        $objPHPExcel->getActiveSheet()->setCellValue('BJ2', $paid_to_other_814);
        $objPHPExcel->getActiveSheet()->setCellValue('BK2', "");

        $objPHPExcel->getActiveSheet()->setTitle('Loan Information');
        

        // Set Font Color, Font Style and Font Alignment
        $styleArray = array(
						'font'  => array(
							'bold'  => true,
							'size'  => 10,
							'name'  => 'Verdana'
							
						)
					);
        $objPHPExcel->getActiveSheet()->getStyle('A1:BK1')->applyFromArray($styleArray);


        $objPHPExcel->createSheet(1);
        $objPHPExcel->setActiveSheetIndex(1);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Loan number');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Start Date');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Lender Name');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Lender Account Number');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Security Type');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Amount owned');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', '% Owned');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Lender Sold Rate');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Lender Type');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Tax ID #');
		$objPHPExcel->getActiveSheet()->setCellValue('K1', 'Contact First');
		$objPHPExcel->getActiveSheet()->setCellValue('L1', 'Contact Last');
		$objPHPExcel->getActiveSheet()->setCellValue('M1', 'Contact Phone');
		$objPHPExcel->getActiveSheet()->setCellValue('N1', 'Phone Type');
		$objPHPExcel->getActiveSheet()->setCellValue('O1', 'Contact E-Mail');
		$objPHPExcel->getActiveSheet()->setCellValue('P1', 'Street Address');
		$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Unit #');
		$objPHPExcel->getActiveSheet()->setCellValue('R1', 'City');
		$objPHPExcel->getActiveSheet()->setCellValue('S1', 'State');
		$objPHPExcel->getActiveSheet()->setCellValue('T1', 'Zip');
		$objPHPExcel->getActiveSheet()->setCellValue('U1', 'Payment Distribution');
		$objPHPExcel->getActiveSheet()->setCellValue('V1', 'Bank Name');
		$objPHPExcel->getActiveSheet()->setCellValue('W1', 'Account #');
		$objPHPExcel->getActiveSheet()->setCellValue('X1', 'Routing #');
		$objPHPExcel->getActiveSheet()->setCellValue('Y1', 'Account Type');
		$objPHPExcel->getActiveSheet()->setCellValue('Z1', 'Loan Payoff');
		$objPHPExcel->getActiveSheet()->setCellValue('AA1', 'Loan Reinstatement');
		$objPHPExcel->getActiveSheet()->setCellValue('AB1', 'Chage Fees / Terms');
		$objPHPExcel->getActiveSheet()->setCellValue('AC1', 'Start Foreclosure');
		$objPHPExcel->getActiveSheet()->setCellValue('AD1', 'Servicer Status');


		$investor_type_option 	= $this->config->item('investor_type_option');
		$account_type_option 	= $this->config->item('account_type_option');
		$Broker_Autho_option    = $this->config->item('Broker_Authorization_option');
		$assi_servicer_status	= $this->config->item('assi_servicer_status');
		$assi_security_type		= $this->config->item('assi_security_type');

		$i = 1;
		$allinvesment = 0;
		$persentloan = 0;
		$COUNTlENDER = 0;

		foreach ($fetch_all_assignment as $value) {

			$i++;
			$COUNTlENDER++;
			$allinvesment += $value->investment;

			$persentloanvalue = str_replace('%', '', $value->percent_loan)/100;
			$persentloan += $persentloanvalue;

			//investor
			$fetchlenderinfo = $this->User_model->query("SELECT * FROM `investor` WHERE `id`= '".$value->lender_name."' ");
			$fetchlenderinfo = $fetchlenderinfo->result();


			$fetchlenderinfo1 = $this->User_model->query("SELECT * FROM `investor` WHERE `id`= '".$value->lender_name."' ");
			$fetchlenderinfo1 = $fetchlenderinfo1->row();




			if($fetchlenderinfo[0]->ach_disbrusement == '1'){
			   $paymenttype 	= 'ACH';
			   $bank_name 		= $fetchlenderinfo[0]->bank_name;
			   $account_number 	= $fetchlenderinfo[0]->account_number;
			   $routing_number 	= $fetchlenderinfo[0]->routing_number;
			   $account_type 	= $account_type_option[$fetchlenderinfo[0]->account_type];

			}elseif($fetchlenderinfo[0]->check_disbrusement == '1'){
				$paymenttype 	= 'Checking';
				$bank_name 		= '';
				$account_number = '';
				$routing_number = '';
				$account_type 	= '';

			}else{
				$paymenttype 	= '';
				$bank_name 		= '';
				$account_number = '';
				$routing_number = '';
				$account_type 	= '';
			}

			$explode_bro_talimar = explode(',', $fetchlenderinfo[0]->bro_talimar);
			$explode_bro_lender = explode(',', $fetchlenderinfo[0]->bro_lender);

			foreach($Broker_Autho_option as $keys => $bro_opt){ 

				if(in_array($keys, $explode_bro_talimar)){
					$bro_talimar[$keys] = 'TaliMar';
				}

				if(in_array($keys, $explode_bro_lender)){
					$bro_talimar[$keys] = 'Lender';
				}
			}

			//lender contact
			$fetch_lcontactdata = $this->User_model->query("SELECT c.contact_firstname, c.contact_lastname, c.contact_phone, c.contact_email, c.primary_option FROM lender_contact as lc JOIN contact as c ON lc.contact_id = c.contact_id WHERE lc.lender_id = '".$fetchlenderinfo[0]->id."' ORDER BY lc.contact_primary DESC");
			$fetch_lcontactdata = $fetch_lcontactdata->result();
			$lcontact_firstname = $fetch_lcontactdata[0]->contact_firstname;
			$lcontact_lastname  = $fetch_lcontactdata[0]->contact_lastname;
			$contact_phone  = $fetch_lcontactdata[0]->contact_phone;
			$contact_email  = $fetch_lcontactdata[0]->contact_email;
			$primary_option  = $fetch_lcontactdata[0]->primary_option;

			
			$fetchfcihash = $this->User_model->query("SELECT fci, loan_amount, FundingEntity from loan WHERE talimar_loan = '".$value->talimar_loan."'");
			$fetchfcihash = $fetchfcihash->result();
			$fci = $fetchfcihash[0]->fci;

			if($assi_servicer_status[$value->nservicer_status] == ''){
				$servicer_value = 'Not Submitted';
			}else{
				$servicer_value = $assi_servicer_status[$value->nservicer_status];
			}

			if($value->nsecurity_type == ''){
				$security_value = '';
			}else{
				$security_value = $assi_security_type[$value->nsecurity_type];
			}

			if($value->start_date == ''){
				$startDate = '';
			}else{
				$startDate = $value->start_date;
			}

			if($fetchlenderinfo[0]->fci_acct == ''){
				$fcilenacc = 'New';
			}else{
				$fcilenacc = $fetchlenderinfo[0]->fci_acct;
			}


			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $fci);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $startDate);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $fetchlenderinfo ?  $fetchlenderinfo[0]->name : '');
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $fcilenacc);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $security_value);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $value->investment);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, str_replace('%', '', $value->percent_loan)/100);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, str_replace('%', '', $value->invester_yield_percent)/100);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $investor_type_option[$fetchlenderinfo[0]->investor_type]);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, !empty($fetchlenderinfo1->tax_id)?$fetchlenderinfo1->tax_id:'');
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $lcontact_firstname);
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $lcontact_lastname);
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $contact_phone);
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $contact_number_option[$primary_option]);
			$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $contact_email);
			$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $fetchlenderinfo[0]->address);
			$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $fetchlenderinfo[0]->unit);
			$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $fetchlenderinfo[0]->city);
			$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, $fetchlenderinfo[0]->state);
			$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, $fetchlenderinfo[0]->zip);
			$objPHPExcel->getActiveSheet()->setCellValue('U'.$i, $paymenttype);
			$objPHPExcel->getActiveSheet()->setCellValue('V'.$i, $bank_name);
			$objPHPExcel->getActiveSheet()->setCellValue('W'.$i, $account_number);
			$objPHPExcel->getActiveSheet()->setCellValue('X'.$i, $routing_number);
			$objPHPExcel->getActiveSheet()->setCellValue('Y'.$i, $account_type);
			$objPHPExcel->getActiveSheet()->setCellValue('Z'.$i, $bro_talimar[1]);
			$objPHPExcel->getActiveSheet()->setCellValue('AA'.$i, $bro_talimar[2]);
			$objPHPExcel->getActiveSheet()->setCellValue('AB'.$i, $bro_talimar[3]);
			$objPHPExcel->getActiveSheet()->setCellValue('AC'.$i, $bro_talimar[4]);
			$objPHPExcel->getActiveSheet()->setCellValue('AD'.$i, $servicer_value);
			
		}

		//check for Funding Entity...
		$FundingEntitycheck = $this->User_model->query("SELECT * FROM investor WHERE id='".$fetchfcihash[0]->FundingEntity."'");
		if($FundingEntitycheck->num_rows() > 0){

			$FundingEntitycheck = $FundingEntitycheck->result();

			$FundingEntitycheck1 = $this->User_model->query("SELECT * FROM investor WHERE id='".$fetchfcihash[0]->FundingEntity."'");
			$FundingEntitycheck1 = $FundingEntitycheck1->result();



			if($FundingEntitycheck[0]->ach_disbrusement == '1'){
			   $paymenttype1 		= 'ACH';
			   $bank_name1 			= $FundingEntitycheck[0]->bank_name;
			   $account_number1 	= $FundingEntitycheck[0]->account_number;
			   $routing_number1 	= $FundingEntitycheck[0]->routing_number;
			   $account_type1 		= $account_type_option[$FundingEntitycheck[0]->account_type];

			}elseif($FundingEntitycheck[0]->check_disbrusement == '1'){
				$paymenttype1 		= 'Checking';
				$bank_name1 		= '';
				$account_number1 	= '';
				$routing_number1 	= '';
				$account_type1 		= '';
			}else{
				$paymenttype1 		= '';
				$bank_name1 		= '';
				$account_number1 	= '';
				$routing_number1 	= '';
				$account_type1 		= '';
			}

			$explode_bro_talimar1 = explode(',', $FundingEntitycheck[0]->bro_talimar);
			$explode_bro_lender1 = explode(',', $FundingEntitycheck[0]->bro_lender);

			foreach($Broker_Autho_option as $keys => $bro_opt){ 

				if(in_array($keys, $explode_bro_talimar1)){
					$bro_talimar1[$keys] = 'TaliMar';
				}

				if(in_array($keys, $explode_bro_lender1)){
					$bro_talimar1[$keys] = 'Lender';
				}
			}

			if($FundingEntitycheck[0]->fci_acct == ''){
				$fcilenacc1 = 'New';
			}else{
				$fcilenacc1 = $FundingEntitycheck[0]->fci_acct;
			}

			//lender contact
			$fetch_lcontactdata1 = $this->User_model->query("SELECT c.contact_firstname, c.contact_lastname, c.contact_phone, c.contact_email, c.primary_option FROM lender_contact as lc JOIN contact as c ON lc.contact_id = c.contact_id WHERE lc.lender_id = '".$FundingEntitycheck[0]->id."' ORDER BY lc.contact_primary DESC");
			$fetch_lcontactdata1 = $fetch_lcontactdata1->result();
			$lcontact_firstname1 = $fetch_lcontactdata1[0]->contact_firstname;
			$lcontact_lastname1  = $fetch_lcontactdata1[0]->contact_lastname;
			$contact_phone1  = $fetch_lcontactdata1[0]->contact_phone;
			$contact_email1  = $fetch_lcontactdata1[0]->contact_email;
			$primary_option1  = $fetch_lcontactdata1[0]->primary_option;
		

			$Is = $i+1;

			$objPHPExcel->getActiveSheet()->setCellValue('A'.$Is, "");
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$Is, "");
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$Is, $FundingEntitycheck[0]->name);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$Is, $fcilenacc1);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$Is, "Deed of Trust");
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$Is, $fetchfcihash[0]->loan_amount - $allinvesment);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$Is, 1 - $persentloan);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$Is, "");
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$Is, $investor_type_option[$FundingEntitycheck[0]->investor_type]);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$Is, !empty($FundingEntitycheck1->tax_id)?$FundingEntitycheck1->tax_id:'');
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$Is, $lcontact_firstname1);
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$Is, $lcontact_lastname1);
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$Is, $contact_phone1);
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$Is, $contact_number_option[$primary_option1]);
			$objPHPExcel->getActiveSheet()->setCellValue('O'.$Is, $contact_email1);
			$objPHPExcel->getActiveSheet()->setCellValue('P'.$Is, $FundingEntitycheck[0]->address);
			$objPHPExcel->getActiveSheet()->setCellValue('Q'.$Is, $FundingEntitycheck[0]->unit);
			$objPHPExcel->getActiveSheet()->setCellValue('R'.$Is, $FundingEntitycheck[0]->city);
			$objPHPExcel->getActiveSheet()->setCellValue('S'.$Is, $FundingEntitycheck[0]->state);
			$objPHPExcel->getActiveSheet()->setCellValue('T'.$Is, $FundingEntitycheck[0]->zip);
			$objPHPExcel->getActiveSheet()->setCellValue('U'.$Is, $paymenttype1);
			$objPHPExcel->getActiveSheet()->setCellValue('V'.$Is, $bank_name1);
			$objPHPExcel->getActiveSheet()->setCellValue('W'.$Is, $account_number1);
			$objPHPExcel->getActiveSheet()->setCellValue('X'.$Is, $routing_number1);
			$objPHPExcel->getActiveSheet()->setCellValue('Y'.$Is, $account_type1);
			$objPHPExcel->getActiveSheet()->setCellValue('Z'.$Is, $bro_talimar1[1]);
			$objPHPExcel->getActiveSheet()->setCellValue('AA'.$Is, $bro_talimar1[2]);
			$objPHPExcel->getActiveSheet()->setCellValue('AB'.$Is, $bro_talimar1[3]);
			$objPHPExcel->getActiveSheet()->setCellValue('AC'.$Is, $bro_talimar1[4]);
			$objPHPExcel->getActiveSheet()->setCellValue('AD'.$Is, "");
			
		}

		//FOR TOTAL ROW...
		$IsS = $i+3;

		$objPHPExcel->getActiveSheet()->setCellValue('A'.$IsS, "Total");
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$IsS, $COUNTlENDER);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$IsS, $allinvesment);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$IsS, $persentloan);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('O'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('P'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('Q'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('R'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('S'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('T'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('U'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('V'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('W'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('X'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('Y'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('Z'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('AA'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('AB'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('AC'.$IsS, "");
		$objPHPExcel->getActiveSheet()->setCellValue('AD'.$IsS, "");



		$objPHPExcel->getActiveSheet()->setTitle('Lender Information');

		// Set Font Color, Font Style and Font Alignment
        $styleArray1 = array(
						'font'  => array(
							'bold'  => true,
							'size'  => 10,
							'name'  => 'Verdana'
						)
					);



        $objPHPExcel->getActiveSheet()->getStyle('A1:AC1')->applyFromArray($styleArray1);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$IsS.':AC'.$IsS)->applyFromArray($styleArray1);

        $objPHPExcel->setActiveSheetIndex(0);
        // Save Excel xls File
        $filename="FCI-Boarding-Data-".$talimar_loan.".xls";
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename='.$filename);
        $objWriter->save('php://output');

        exit;
	
	}


}

?>