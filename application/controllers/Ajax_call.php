<?php
class Ajax_call extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('User_model');
		$this->load->library('Aws3','aws3');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');
		
		if($this->session->userdata('t_user_id') != '' )
		{
		}
		else
		{
			redirect(base_url()."home",'refresh');
		}
	}
	
	public function index()
	{
		
	}
	
	public function ajax_calculate_maturity_date()
	{
		
		error_reporting(0);
		$doc_date 	= $_POST['doc_date'];
		$month 		= $_POST['month'];
		if($month > 0 && $doc_date != '')
		{
		//$myDateTime 	= DateTime::createFromFormat('m-d-Y', $doc_date);
		//$newDateString 	= $myDateTime->format('d-m-Y');

		$dates = explode('-', $doc_date);
	    $monthA = $dates[0];
		$dayA = $dates[1];
		$yearA = $dates[2];
		$newDateString = $dayA.'-'.$monthA.'-'.$yearA;
	
		$add_month_date	= date('d-m-Y', strtotime("+".$month." months", strtotime($newDateString)));
		
		$myDateTime1 	= DateTime::createFromFormat('d-m-Y', $add_month_date);
		$newDateString1 = $myDateTime1->format('m-d-Y');
		echo $newDateString1;
		}
		else
		{
			echo '';
		}
	}   
	
	public function calculate_date_intrest_paid()
	{
		$first_payment 	= $_POST['first_payment'];
		$current_date 	= date('m-d-Y');
		$new_date		= date('m-d-Y',strtotime($first_payment));
		// echo "new date =>>".$new_date;
		// echo "current_date date =>>".$current_date;
		if(strtotime($new_date) < strtotime($current_date))
		{
			$date_result = date('m-d-Y', strtotime("+1 months", strtotime($new_date)));
		}
		else
		{
			$date_result = $first_payment;
		}
		echo $date_result;
	}
	
	public function ajax_select_investor()
	{
		$id['id'] 	= $_POST['id'];
		$result 	= $this->User_model->select_where('investor' , $id);
		$result_new = $result->result();
		foreach($result_new as $row)
		{
			// $aa['name'] = $row->first_name;
			$aa['talimar_lender'] = $row->talimar_lender;
			// $aa['address'] = $row->address;
			// $aa['unit'] = $row->unit;
			// $aa['zip'] = $row->zip;
			// $aa['city'] = $row->city;
			// $aa['tax_id'] = $row->tax_id;
		}
		print_r(json_encode($aa));
	}
		public function fetch_contact_detailss()
	{
		$contact_id['contact_id']	= $_POST['contact_id'];
		$fetch_contact_data 		= $this->User_model->select_where('contact',$contact_id);
		$fetch_contact_data 		= $fetch_contact_data->result();
		foreach($fetch_contact_data as $row)
		{
			$data['firstname']	= $row->contact_firstname;
			$data['middlename']	= $row->contact_middlename;
			$data['lastname']	= $row->contact_lastname;
			$data['email']		= $row->contact_email;
			 $data['title']		= $row->contact_title;
			$data['phone']		= $row->contact_phone;
			$data['contact_address']	= $row->contact_address;
			$data['contact_unit']	= $row->contact_unit;
			$data['contact_city']	= $row->contact_city;
			$data['contact_state']	= $row->contact_state;
			$data['contact_zip']	= $row->contact_zip;
		}
		print_r(json_encode($data));
	}
	

	public function fetch_contact_details()
	{
		$contact_id['contact_id']	= $_POST['contact_id'];
		$fetch_contact_data 		= $this->User_model->select_where('contact',$contact_id);
		$fetch_contact_data 		= $fetch_contact_data->result();
		foreach($fetch_contact_data as $row)
		{
			$data['firstname']	= $row->contact_firstname;
			$data['middlename']	= $row->contact_middlename;
			$data['lastname']	= $row->contact_lastname;
			$data['email']		= $row->contact_email;
			 $data['title']		= $row->contact_title;
			$data['phone']		= $row->contact_phone;
		}
		print_r(json_encode($data));
	}
	
	public function fetch_lender_contact_details()
	{
		$contact_id['contact_id']	= $_POST['contact_id'];
		$fetch_contact_data 		= $this->User_model->select_where('contact',$contact_id);
		$fetch_contact_data 		= $fetch_contact_data->result();
		foreach($fetch_contact_data as $row)
		{
			$data['firstname']	= $row->contact_firstname;
			$data['middlename']	= $row->contact_middlename;
			$data['lastname']	= $row->contact_lastname;
			$data['email']		= $row->contact_email;
			// $data['title']		= $row->contact_title;
			$data['phone']		= $row->contact_phone;
			
			$fetch_lender_contact_type = $this->User_model->select_where('lender_contact_type',$contact_id);
			if($fetch_lender_contact_type->num_rows() > 0)
			{
				$fetch_lender_contact_type 	= $fetch_lender_contact_type->result();
				$data['re_date']			= $fetch_lender_contact_type[0]->lender_RE_date ?  date('m-d-Y',strtotime($fetch_lender_contact_type[0]->lender_RE_date)) : '';
			}
			
		}
		print_r(json_encode($data));
	}
	
	public function set_borrower_fields()
	{
		error_reporting(0);
		$fetch_all_borrower = $this->User_model->select_star('borrower_data');
		$fetch_all_borrower = $fetch_all_borrower->result();
		
		foreach($fetch_all_borrower as $row)
		{
			if($row->c_first_name)
			{
				$contact_c1['borrower_id'] 			= $row->id;
				$contact_c1['contact_firstname'] 	= $row->c_first_name;
				$contact_c1['contact_middlename'] 	= $row->c_middle_initial;
				$contact_c1['contact_lastname'] 	= $row->c_last_name;
				$contact_c1['contact_address'] 		= $row->c_street;
				$contact_c1['contact_unit'] 		= $row->c_unit;
				$contact_c1['contact_city'] 		= $row->c_city;
				$contact_c1['contact_state'] 		= $row->c_state;
				$contact_c1['contact_zip'] 			= $row->c_zip;
				$contact_c1['contact_title'] 		= $row->c_title;
				$contact_c1['contact_phone'] 		= $row->c_phone;
				$contact_c1['contact_email'] 		= $row->c_email;
				$contact_c1['contact_dob'] 			= $row->c_dob;
				$contact_c1['borrower_contact_type'] 	= 1;
				$contact_c1['user_id'] 					= 1;
				echo '<pre>';
				print_r($contact_c1);
				echo '</pre>';
				// die('stop');
				$this->User_model->insertdata('contact',$contact_c1);
				$insert_c1 = $this->db->insert_id();
				if($insert_c1)
				{
					$borrower_contact_type['contact_id'] 			= $insert_c1;
					$borrower_contact_type['user_id'] 				= 1;
					$borrower_contact_type['borrower_driver_license'] 		= $row->c_licence;
					$borrower_contact_type['borrower_exp_date'] 			= $row->c_l_expiration_date;
					$borrower_contact_type['borrower_ss_num'] 				= $row->c_ss;
					$borrower_contact_type['borrower_credit_score'] 		= $row->c_credit_score;
					$borrower_contact_type['borrower_credit_score_date'] 	= $row->c_credit_score_date;
					$borrower_contact_type['borrower_credit_score_desc'] 	= $row->c_credit_explaination;
					$borrower_contact_type['borrower_bankruptcy'] 			= $row->c_bk_declare;
					$borrower_contact_type['borrower_BK_desc'] 				= $row->c_bk_dismis;
					
					$this->User_model->insertdata('borrower_contact_type',$borrower_contact_type);
					
					
					// insert in lender contact
					
					$borrower_contact['borrower_id']		= $row->id;
					$borrower_contact['contact_id']			= $insert_c1;
					$borrower_contact['contact_name']		= $insert_c1;
					$borrower_contact['contact_title']		= $row->c_title;
					$borrower_contact['contact_phone']		= $row->c_phone;
					$borrower_contact['contact_email']		= $row->c_email;
					$borrower_contact['contact_gurrantor']	= $row->c_guarantor;
					$borrower_contact['t_user_id']			= 1;
					
					echo '<pre>';
					print_r($borrower_contact);
					echo '</pre>';
					
					$this->User_model->insertdata('borrower_contact',$borrower_contact);
				}
			}
			if($row->c2_first_name)
			{
				$contact_c2['borrower_id'] 			= $row->id;
				$contact_c2['user_id'] 				= 1;
				$contact_c2['contact_firstname'] 	= $row->c2_first_name;
				$contact_c2['contact_middlename'] 	= $row->c2_middle_initial;
				$contact_c2['contact_lastname'] 	= $row->c2_last_name;
				$contact_c2['contact_address'] 		= $row->c2_address;
				$contact_c2['contact_unit'] 		= $row->c2_unit;
				$contact_c2['contact_city'] 		= $row->c2_city;
				$contact_c2['contact_state'] 		= $row->c2_state;
				$contact_c2['contact_zip'] 			= $row->c2_zip;
				$contact_c2['contact_title'] 		= $row->c2_title;
				$contact_c2['contact_phone'] 		= $row->c2_phone;
				$contact_c2['contact_email'] 		= $row->c2_email;
				$contact_c2['contact_dob'] 			= $row->c2_dob;
				$contact_c2['borrower_contact_type'] 	= 1;
				
					echo '<pre>';
					print_r($contact_c2);
					echo '</pre>';
				$this->User_model->insertdata('contact',$contact_c2);
				$insert_c2 = $this->db->insert_id();
				if($insert_c2)
				{
					$borrower_contact_type2['contact_id'] 		= $insert_c2;
					$borrower_contact_type2['user_id'] 			= 1;
					$borrower_contact_type2['borrower_driver_license'] 		= $row->c2_licence;
					$borrower_contact_type2['borrower_exp_date'] 			= $row->c2_l_expiration_date;
					$borrower_contact_type2['borrower_ss_num'] 				= $row->c2_ss;
					$borrower_contact_type2['borrower_credit_score'] 		= $row->c2_credit_score;
					$borrower_contact_type2['borrower_credit_score_date'] 	= $row->c2_credit_score_date;
					$borrower_contact_type2['borrower_credit_score_desc'] 	= $row->c2_credit_explaination;
					$borrower_contact_type2['borrower_bankruptcy'] 			= $row->c2_bk_declare;
					$borrower_contact_type2['borrower_BK_desc'] 			= $row->c2_bk_dismis;
					echo '<pre>';
					print_r($borrower_contact_type2);
					echo '</pre>';
					$this->User_model->insertdata('borrower_contact_type',$borrower_contact_type2);
					
					
					// insert in lender contact 
					
					$borrower_contact2['borrower_id']			= $row->id;
					$borrower_contact2['contact_id']			= $insert_c1;
					$borrower_contact2['contact_name']			= $insert_c1;
					$borrower_contact2['contact_title']			= $row->c2_title;
					$borrower_contact2['contact_phone']			= $row->c2_phone;
					$borrower_contact2['contact_email']			= $row->c2_email;
					$borrower_contact2['contact_gurrantor']		= $row->c2_guarantor;
					$borrower_contact2['t_user_id']				= 1;
					echo '<pre>';
					print_r($borrower_contact2);
					echo '</pre>';
					$this->User_model->insertdata('borrower_contact',$borrower_contact2);
				}
			}
		}
	}
	
	public function ajax_delete_ecumbrances()
	{
		$data['id'] 	= $_POST['id'];
		$this->User_model->delete('current_encumbrances',$data);
		echo '1';
	}
	
}


