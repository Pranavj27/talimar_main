<?php
class LoanPortfolioPopup extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('email');
		$this->load->model('User_model');
		$this->load->library('MYPDF');
		$this->load->library('Aws3','aws3');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');

		
		if ($this->session->userdata('t_user_id') != '') {
		} else {
			redirect(base_url() . "home", 'refresh');
		}
		
		ini_set('memory_limit', '-1');
	}

	public function GetDocusignAPIDocs($envID){

		
		$integratorKey = DOCUSIGN_INTEGRATOR_KEY;
		$email = DOCUSIGN_EMAIL;
		$password = DOCUSIGN_PASSWORD;
		$baseUrl 	= BASE_URL;

		// construct the authentication header:
		$header = "<DocuSignCredentials><Username>" . $email . "</Username><Password>" . $password . "</Password><IntegratorKey>" . $integratorKey . "</IntegratorKey></DocuSignCredentials>";

		//get document status...
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $baseUrl.'/envelopes/'.$envID,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
		    'Accept: application/json',
		    "X-DocuSign-Authentication: $header"
		  ),
		));

		$response = curl_exec($curl);
		$responsesss = json_decode($response);

		return $responsesss;
	}

	public function config_variable()
	{
		$data = array();
		$data['servicer_charge_paid_from'] = $this->config->item('servicer_charge_paid_from');
		$data['servicer_charge_paid_to']   = $this->config->item('servicer_charge_paid_to');
		$data['servicer_charge_description']   = $this->config->item('servicer_charge_description');
		$data['ach_activated']   = $this->config->item('ach_activated');

		$data['vendor_types']   = $this->config->item('vendor_type');
		$data['l_s_c']   = $this->config->item('l_s_c');
		
		$data['STATE_USA']   = $this->config->item('STATE_USA'); 
		$data['usa_city_county']   = $this->config->item('usa_city_county'); 
		$data['yes_no_option']   = $this->config->item('yes_no_option');
		$data['garge_option']   = $this->config->item('garge_option');
		$data['ext_PaymentType']   = $this->config->item('ext_PaymentType');
		$data['position_option']   = $this->config->item('position_option');
		$data['all_countary']   = $this->config->item('all_countary');
		$data['loan_type_option']   = $this->config->item('loan_type_option');
		$data['dead_reason_option']   = $this->config->item('dead_reason_option');
		$data['loan_status_option']   = $this->config->item('loan_status_option');
		$data['closing_status_option']   = $this->config->item('closing_status_option');
		$data['boarding_status_option']   = $this->config->item('boarding_status_option');
		$data['serviceing_condition_option']   = $this->config->item('serviceing_condition_option');
		$data['loan_payment_status']   = $this->config->item('loan_payment_status');
		$data['serviceing_reason_option']   = $this->config->item('serviceing_reason_option');
		$data['serviceing_sub_agent_option']   = $this->config->item('serviceing_sub_agent_option');
		$data['serviceing_who_pay_servicing_option']   = $this->config->item('serviceing_who_pay_servicing_option');
		$data['how_setup_fees_paid']   = $this->config->item('how_setup_fees_paid');
		$data['serviceing_agent_option']   = $this->config->item('serviceing_agent_option');
		$data['broker_capacity_option']   = $this->config->item('broker_capacity_option');
		$data['account_type_option']   = $this->config->item('account_type_option');
		$data['ach_account_type_option']   = $this->config->item('ach_account_type_option');
		$data['fee_disbursement_list']   = $this->config->item('fee_disbursement_list');
		$data['paidoff_status_option']   = $this->config->item('paidoff_status_option');
		$data['construction_status_option']   = $this->config->item('construction_status_option');
		$data['will_option']   = $this->config->item('will_option');
		$data['yes_no_option2']   = $this->config->item('yes_no_option2');
		$data['yes_no_option3']   = $this->config->item('yes_no_option3');
		$data['yes_no_option4']   = $this->config->item('yes_no_option4');
		$data['naveda_county']   = $this->config->item('naveda_counties_options');
		$data['junior_senior_option']   = $this->config->item('junior_senior_option');
		$data['encumbrances_loan_type']   = $this->config->item('encumbrances_loan_type');
		$data['loan_doc_type']   = $this->config->item('loan_doc_type');
		$data['com_type']   = $this->config->item('com_type');
		
		$data['extension_yes_no']   = $this->config->item('extension_yes_no');
		$data['extension_agreement']   = $this->config->item('extension_agreement');
		$data['extension_loan_servicer']   = $this->config->item('extension_loan_servicer');
		$data['extension_fee']   = $this->config->item('extension_fee');
		$data['loan_servicing_checklist']   = $this->config->item('loan_servicing_checklist');
		$data['underwriting_items_option']   = $this->config->item('underwriting_items_option');
		$data['boarding_brocker_disbrusement']   = $this->config->item('boarding_brocker_disbrusement');
		$data['affiliated_option']   = $this->config->item('affiliated_option');
		$data['loan_affiliated_parties']   = $this->config->item('loan_affiliated_parties');
		$data['reason_for_default']   = $this->config->item('reason_for_default');
		$data['foreclosuer_status']   = $this->config->item('foreclosuer_status');
		$data['foreclosuer_processor']   = $this->config->item('foreclosuer_processor');
		$data['estimale_complete_option']   = $this->config->item('estimale_complete_option');
		$data['loan_note_option']   = $this->config->item('loan_note_option');
		$data['property_modal_property_type']   = $this->config->item('property_modal_property_type');
		$data['valuation_type']   = $this->config->item('valuation_type');
		$data['transaction_type_option']   = $this->config->item('transaction_type_option');
		$data['property_typee']   = $this->config->item('property_typee');		
		$data['loan_typee']   = $this->config->item('loan_type_option');
		$data['payment_remind']   = $this->config->item('payment_remind');
		$data['closing_statement_item_loads']   = $this->config->item('closing_statement_item_loads');
		$data['new_servicer_option']   = $this->config->item('new_servicer_option');
		$data['newsub_servicer_option']   = $this->config->item('newsub_servicer_option');
        $data['fractional']   = $this->config->item('fractional');
		$data['responsibility']   = $this->config->item('responsibility');
        $data['primary_contact_option']   = $this->config->item('primary_contact_option');
        $data['pay_off_processing_one']   = $this->config->item('pay_off_processing_one');
        $data['new_option']   = $this->config->item('new_option');
        $data['extension_status']   = $this->config->item('extension_status');
        $data['loan_source']   = $this->config->item('loan_source');
        $data['not_applicable_option']   = $this->config->item('not_applicable_option');
		
        $data['assi_security_type']   = $this->config->item('assi_security_type');
        $data['assi_disclouser_status']   = $this->config->item('assi_disclouser_status');
        $data['assi_servicer_status']   = $this->config->item('assi_servicer_status');
        $data['assi_fund_received']   = $this->config->item('assi_fund_received');
        $data['assi_file_status']   = $this->config->item('assi_file_status');
        $data['term_sheet_option']   = $this->config->item('term_sheet');
        $data['term_sheet_closing_option']   = $this->config->item('closing_term_sheet');
        $data['yes_no_option_verified']   = $this->config->item('yes_no_option_verified');
        $data['market_trust_deed_new_option']   = $this->config->item('market_trust_deed_new_option');
        $data['template_yes_no_option']   = $this->config->item('template_yes_no_option');
        $data['marketing_before_after_option']   = $this->config->item('marketing_before_after_option');
        $data['before_after_option_new']   = $this->config->item('before_after_option_new');
        $data['no_yes_option']   = $this->config->item('no_yes_option');
        $data['debit_account_type']   = $this->config->item('debit_account_type');
        $data['payment_due_option']   = $this->config->item('payment_due_option');
        $data['yes_no_option_67']   = $this->config->item('yes_no_option_67');
        $data['foreclosuer_step']   = $this->config->item('foreclosuer_step');
        $data['recording_no_yes_option']   = $this->config->item('recording_no_yes_option');
        $data['Ass_status_paidto']   = $this->config->item('Ass_status_paidto');
        $data['Ass_Recording_Status']   = $this->config->item('Ass_Recording_Status');
        $data['who_pay_setup_fees']   = $this->config->item('who_pay_setup_fees');
        $data['lender_app_status']   = $this->config->item('lender_app_status');
        $data['lender_app_request']   = $this->config->item('lender_app_request');
        $data['ext_lender_app']   = $this->config->item('ext_lender_app');
        $data['ext_process_option']   = $this->config->item('ext_process_option');
        $data['ext_borrower_approval']   = $this->config->item('ext_borrower_approval');
        $data['ext_PaymentAmount_opt']   = $this->config->item('ext_PaymentAmount_opt');
        $data['ext_PaymentCalculation']   = $this->config->item('ext_PaymentCalculation');
        $data['loan_maturity_status']   = $this->config->item('loan_maturity_status');
        $data['process_payoff_type_opt']   = $this->config->item('process_payoff_type_opt');       
		$data['template_yes_no_option_3']   = array(2 => 'Not Complete',1 => 'Complete');

		$data['search_by_option'] 		  = $this->config->item('search_by_option');
		$data['borrower_option']  		  = $this->config->item('borrower_option');
		$data['yes_no_option'] 	  		  = $this->config->item('yes_no_option');
		$data['yes_no_option1']   		  = $this->config->item('yes_no_option1');
		$data['yes_no_option3']   		  = $this->config->item('yes_no_option3');
		$data['yes_no_not_applicable_option'] = $this->config->item('yes_no_not_applicable_option');
		$data['loan_type_option'] 		  = $this->config->item('loan_type_option');
		$data['transaction_type_option']  = $this->config->item('transaction_type_option');
		$data['position_option'] 	 	  = $this->config->item('position_option');
		$data['loan_payment_type_option'] = $this->config->item('loan_payment_type_option');
		$data['loan_payment_schedule_option']= $this->config->item('loan_payment_schedule_option');
		$data['loan_intrest_type_option'] = $this->config->item('loan_intrest_type_option');
		$data['loan_purpose_option'] 	  = $this->config->item('loan_purpose_option');
		$data['escrow_account_option']    = $this->config->item('escrow_account_option');
		$data['calc_day_option'] 		  = $this->config->item('calc_day_option');
		$data['min_bal_type_option'] 	  = $this->config->item('min_bal_type_option');
		$data['STATE_USA'] 				  = $this->config->item('STATE_USA'); 
		$data['loan_purpose_option_type'] = $this->config->item('loan_purpose_option_type'); 
		$data['payment_due_option'] 	  = $this->config->item('payment_due_option'); 
		$data['loan_source_option'] 	  = $this->config->item('loan_source_option'); 
		$data['loan_cross_collateralized']= $this->config->item('loan_cross_collateralized'); 
		$data['no_yes_option'] 			  = $this->config->item('no_yes_option'); 

		$data['calc_based_option'] 		  = array(""=>"Select One","1"=>" Full Loan Amount","2"=>"Outstanding Balance");


		$data['closing_status_option'] 	  = $this->config->item('closing_status_option');
		$data['construction_status_option']= $this->config->item('construction_status_option');
		$data['loan_status_option']       = $this->config->item('loan_status_option');
		$data['boarding_status_option']   = $this->config->item('boarding_status_option');
		$data['property_type_option']     = $this->config->item('property_modal_property_type');
		$data['usa_city_county']          = $this->config->item('usa_city_county');

		$data['paidoff_status_option']    = $this->config->item('paidoff_status_option');
		$data['affiliated_option']        = $this->config->item('affiliated_option');
		$data['loan_reserve_draw_status'] = $this->config->item('loan_reserve_draw_status');
		$data['template_yes_no_option']   = $this->config->item('template_yes_no_option');
		$data['recording_no_yes_option']  = $this->config->item('recording_no_yes_option');

		$data['fci_doc_options'] = $this->config->item('fci_doc_options');
		$data['occupancy_value_option'] = $this->config->item('occupancy_value_option');

		return $data;
	}

	public function loan_data_by_id($loan_id)
	{
		$fetch = array();
		$fetch['id'] = $loan_id;
		$fetch_loan_data = $this->User_model->select_where('loan',$fetch);
		$row = $fetch_loan_data->row();
		return $row;
	}

	public function loan_data_by_address($talimar_loan)
	{
		$fetch_property_addresss = $this->User_model->query("SELECT property_address FROM property_home WHERE talimar_loan = '" . $talimar_loan . "' AND `primary_property`= '1'");
		$fetch_property_addresss = $fetch_property_addresss->row();
		return $fetch_property_addresss->property_address;
	}

	public function modal_loan_terms()
	{
		error_reporting(0);
		$loan_id = $this->input->post('loan_id');
		$data = $this->config_variable();
		$data['property_addresss'] = '';
		$data['servicing_contact_details'] = '';
		if(is_numeric($loan_id))
		{ 
			$loan_data = $this->loan_data_by_id($loan_id);

			$data['loan_id'] 			= $loan_id;
			$data['talimar_loan'] 		= $loan_data->talimar_loan;
			$data['FundingEntity'] 		= $loan_data->FundingEntity;
			$data['fci'] 				= $loan_data->fci;
			$data['borrower'] 			= $loan_data->borrower;
			$data['loan_source'] 		= $loan_data->loan_source;
			$data['rep_contact'] 		= $loan_data->rep_contact;
			$data['loan_amount'] 		= $loan_data->loan_amount;
			$data['current_balance'] 	= $loan_data->current_balance;
			$data['lender_fee'] 		= $loan_data->lender_fee.'%';
			$data['intrest_rate'] 		= $loan_data->intrest_rate ? $loan_data->intrest_rate.'' : '10';
			$data['term_month'] 		= $loan_data->term_month;
			$data['per_payment_period'] = $loan_data->per_payment_period;
			$data['payment_held_close'] = $loan_data->payment_held_close;
			$data['calc_of_year'] 		= $loan_data->calc_of_year;
			$data['loan_type']			= $loan_data->loan_type;
			$data['transaction_type'] 	= $loan_data->transaction_type;
			$data['position'] 			= $loan_data->position;
			$data['payment_type'] 		= $loan_data->payment_type ? $loan_data->payment_type : 1;
			$data['exit_strategy'] 		= $loan_data->exit_strategy;
			$data['loan_conditions'] 	= $loan_data->loan_conditions;
			$data['first_payment_collect'] = $loan_data->first_payment_collect;
			$data['loan_highlights'] 	= $loan_data->loan_highlights;
			$data['baloon_payment_type'] = $loan_data->baloon_payment_type;
			$data['payment_sechdule'] 	= $loan_data->payment_sechdule;
			$data['intrest_type'] 		= $loan_data->intrest_type;
			$data['payment_amount'] 	= $loan_data->payment_amount;
			$data['minium_intrest'] 	= $loan_data->minium_intrest ? $loan_data->minium_intrest : '0';
			$data['min_intrest_month'] 	= $loan_data->min_intrest_month;
			$data['min_payment']	 	= $loan_data->min_payment;
			$data['owner_purpose'] 		= $loan_data->owner_purpose;
			$data['costruction_loan'] 	= $loan_data->costruction_loan;
			$data['project_cost'] 		= $loan_data->project_cost;
			$data['total_project_cost'] = $loan_data->total_project_cost;
			$data['borrower_enquity'] 	= $loan_data->borrower_enquity;
			$data['grace_period'] 		= $loan_data->grace_period;
			$data['late_charges_due'] 	= $loan_data->late_charges_due;
			$data['replace_late_fee'] 	= $loan_data->replace_late_fee;
			$data['default_rate'] 		= $loan_data->default_rate;
			$data['how_many_days']		= $loan_data->how_many_days;
			$data['return_check_rate'] 	= $loan_data->return_check_rate;
			$data['refi_existing'] 		= $loan_data->refi_existing;
			$data['property'] 			= $loan_data->property;
			$data['interest_reserve'] 	= $loan_data->interest_reserve;
			$data['loan_document_date'] = $loan_data->loan_document_date ? date('m-d-Y',strtotime($loan_data->loan_document_date)) : '';
			$data['loan_funding_date'] 	= $loan_data->loan_funding_date  ? date('m-d-Y',strtotime($loan_data->loan_funding_date)) : '';
			$data['ucc_filing'] 		= $loan_data->ucc_filing;
			$data['draws'] 				= $loan_data->draws;
			$data['phase'] 				= $loan_data->phase;
			$data['auto_payment'] 		= $loan_data->auto_payment;
			$data['t_user_id'] 			= $loan_data->t_user_id;
			$data['calc_based'] 		= $loan_data->calc_based;
			$data['cross_collateralized'] = $loan_data->loan_cross_collateralized;
			$data['cross_provision'] 	= $loan_data->cross_collateralized_provision;
			
			$interest_reserve 			= $loan_data->interest_reserve;
			$term_month 				= $loan_data->term_month;

			 if($interest_reserve == '1'){
				$data['first_payment_date'] = $loan_data->first_payment_date ? date('m-d-Y',strtotime($loan_data->first_payment_date)) : '';				
			 }
			 else if($interest_reserve == '2')
			 {
			 	$data['first_payment_date'] = $loan_data->first_payment_date ? date('m-d-Y',strtotime($loan_data->first_payment_date)) : ''; 
			}else{
				$data['first_payment_date'] = $loan_data->first_payment_date ? date('m-d-Y',strtotime($loan_data->first_payment_date)) : '';
			}
			
			$data['maturity_date'] 			=  date('m-d-Y',strtotime($loan_data->loan_funding_date.' +'.$term_month.' months'));
			$data['modify_maturity_date'] 	= $loan_data->modify_maturity_date ? date('m-d-Y',strtotime($loan_data->modify_maturity_date)) : '';
			$data['payment_due'] 			= $loan_data->payment_due;
			$data['extention_option'] 		= $loan_data->extention_option;
			$data['extention_month'] 		= $loan_data->extention_month;
			$data['extention_percent_amount'] = $loan_data->extention_percent_amount;
			$data['ballon_payment'] 		= $loan_data->ballon_payment;
			
			$data['trustee'] 				= $loan_data->trustee;
			$data['promissory_note_val'] 	= $loan_data->promissory_note_val;
			$data['payment_requirement'] 	= $loan_data->payment_requirement;
			$data['add_broker_data_desc'] 	= $loan_data->add_broker_data_desc;
			$data['min_bal_type'] 			= $loan_data->min_bal_type;
			$data['payment_gurranty'] 		= $loan_data->payment_gurranty;
			$data['payment_gurr_explain'] 	= $loan_data->payment_gurr_explain;
			$data['interest_reserve'] 		= $loan_data->interest_reserve;
			$data['additional_gra'] 		= $loan_data->additional_gra;
			$data['add_gra_c_id'] 		    = $loan_data->add_gra_c_id;

			$talimar_loan = $loan_data->talimar_loan;

			
			$data['property_addresss'] = $this->loan_data_by_address($talimar_loan);

			$servicing_contact = $this->User_model->query("SELECT * FROM `loan` where talimar_loan = '" . $talimar_loan . "'");
			$data['servicing_contact_details'] = $servicing_contact->row();

		}else
		{
			$data['loan_id'] 				= '';
			$data['talimar_loan'] 			= '';	
			$data['fci']					= '';
			$data['FundingEntity'] 			= '';
			$data['add_gra_c_id'] 			= '';
			$data['additional_gra'] 		= '';
			$data['borrower'] 				= '';
			$data['loan_source'] 			= '';
			$data['rep_contact']			= '';
			$data['loan_amount'] 			= '00';
			$data['current_balance'] 		= '00';
			$data['lender_fee'] 			= '2.5%';
			$data['intrest_rate'] 			= '10';
			$data['term_month'] 			= '12';
			$data['per_payment_period'] 	= '12';
			$data['payment_held_close'] 	= '6';
			$data['calc_of_year'] 			= '1';
			$data['loan_type'] 				= '';
			$data['transaction_type'] 		= '';
			$data['position']				= '1';
			$data['payment_type'] 			= '1';
			$data['payment_sechdule'] 		= '1';
			$data['intrest_type'] 			= '1';
			$data['payment_amount'] 		= '0.00';
			$data['minium_intrest'] 		= '0';
			$data['min_intrest_month'] 		= '';
			$data['owner_occupied'] 		= '';
			$data['min_payment']	 		= '';
			$data['owner_purpose'] 			= '1';
			$data['costruction_loan'] 		= '';
			$data['project_cost'] 			= '0.00';
			$data['total_project_cost'] 	= '0.00';
			$data['borrower_enquity'] 		= '0.00';
			$data['grace_period'] 			= '10';
			$data['late_charges_due'] 		= '10';
			$data['replace_late_fee'] 		= '';
			$data['default_rate'] 			= '17';
			$data['how_many_days'] 			= '10';
			$data['return_check_rate'] 		= '150';
			$data['refi_existing']			= '0';
			$data['property'] 			    = '';
			$data['loan_document_date'] 	= '';
			$data['loan_funding_date']		= '';
			// $data['loan_funding_date'] 		= date('m-d-Y');
			$data['first_payment_date'] 	= '';
			$data['maturity_date'] 			= '';
			// $data['maturity_date'] 			=  date('m-d-Y', strtotime('+1 years'));
			$data['payment_due'] 			= '';
			$data['extention_option'] 		= '';
			$data['extention_month'] 		= '6';
			$data['extention_percent_amount'] = '1%';
			$data['ballon_payment'] 		= '0.00';
			$data['trustee'] 				= 'FCI Lender Services';
			// $data['trustee'] 				= '';
			$data['promissory_note_val'] 	= '';
			$data['baloon_payment_type'] 	= '1';
			$data['payment_requirement'] 	= '0';
			$data['exit_strategy'] 			= 'Sale of Property';
			$data['loan_conditions']		= '';
			$data['loan_highlights'] 		= '';
			$data['add_broker_data_desc'] 	= '';
			$data['min_bal_type'] 			= '1';
			$data['payment_gurranty'] 		= '0'; // by default set with 1...
			$data['payment_gurr_explain'] 	= '';
			$data['first_payment_collect'] 	= '';
			$data['interest_reserve'] 		= '';
			$data['auto_payment'] 			= '';
			$data['t_user_id']				= '';
			$data['calc_based'] 			= '';
			$data['cross_collateralized'] 	= '';
			$data['cross_provision'] 		= '';
		}

		$result = $this->User_model->select_star('vendors');
		$result_new = $result->result();
		$data['vendor_name'] = $result_new;

		$all_user = $this->User_model->query("SELECT * FROM user WHERE account='1'");
		$data['all_user'] = $all_user->result();

		

		$this->load->view('template_files/loan_portfolio/modal_loan_terms', $data);
	}

	public function modal_save_documents()
	{
		error_reporting(0);
		$loan_id 	= $this->input->post('loan_id');
		$loan_data  = $this->loan_data_by_id($loan_id);
		$talimar_loan = $loan_data->talimar_loan;

		$data['property_addresss'] = $this->loan_data_by_address($talimar_loan);
		$data['talimar_loan'] = $talimar_loan;
		$data['loan_id'] = $loan_id;
		$this->load->view('template_files/loan_portfolio/modal_save_documents', $data);
	}

	public function modal_print_documents()
	{
		error_reporting(0);
		$loan_id 	= $this->input->post('loan_id');
		$loan_data  = $this->loan_data_by_id($loan_id);
		$talimar_loan = $loan_data->talimar_loan;
		$borrower_id = $loan_data->borrower;

		$data['property_addresss'] = $this->loan_data_by_address($talimar_loan);
		$data['talimar_loan'] = $talimar_loan;
		$data['loan_id'] = $loan_id;

		$fetch_loan_borrower = $this->User_model->select_where('borrower_data', array('id' => $borrower_id));
		$data['fetch_loan_borrower'] = $fetch_loan_borrower->row();

		$this->load->view('template_files/loan_portfolio/modal_print_documents', $data);
	}

	public function modal_loan_reserve()
	{
		error_reporting(0);
		$data = $this->config_variable();
		$loan_id 	= $this->input->post('loan_id');
		$loan_data  = $this->loan_data_by_id($loan_id);
		$talimar_loan = $loan_data->talimar_loan;
		$borrower_id = $loan_data->borrower;

		$arrayLoanArgId = array('id' => $loan_id);
		$arrayLoanArg = array('talimar_loan' => $talimar_loan);

		$loan_borrower_id_data['id'] = $borrower_id;
		$fetch_loan_borrower = $this->User_model->select_where('borrower_data', $loan_borrower_id_data);
		$data['fetch_loan_borrower'] = '';
		if ($fetch_loan_borrower->num_rows() > 0) {
			$fetch_loan_borrower = $fetch_loan_borrower->row();
			$data['fetch_loan_borrower'] = $fetch_loan_borrower;
		}

		$reserve_extra_field =$this->User_model->select_where('loan_reserve_extra_field', $arrayLoanArg);
		$reserve_extra_field = $reserve_extra_field->result();
		$data['fetch_reserve_extra_field'] = $reserve_extra_field;

		$fetch_data1 = "SELECT * FROM loan_reserve_draws WHERE talimar_loan = '".$talimar_loan."'";
		$fetch_data1 = $this->User_model->query($fetch_data1);
		$data['reserve_draws'] = '';
		$data['total_of_date1'] = '0.00';
		$data['total_of_date2'] = '0.00';
		$data['total_of_date3'] = '0.00';
		$data['total_of_date4'] = '0.00';
		$data['total_of_date5'] = '0.00';
		$data['total_of_date6'] = '0.00';
		$data['total_of_date7'] = '0.00';
		$data['total_of_date8'] = '0.00';
		$data['total_of_date9'] = '0.00';
		$data['total_of_date10'] = '0.00';
		$data['total_of_date11'] = '0.00';
		$data['total_of_date12'] = '0.00';
		$data['total_of_date13'] = '0.00';
		$data['total_of_date14'] = '0.00';
		$data['total_of_date15'] = '0.00';
		
		if ($fetch_data1->num_rows() > 0) {
			$fetch_data1 = $fetch_data1->result();
			$data['reserve_draws'] = $fetch_data1;
		

			for ($i=1; $i <=15 ; $i++) { 
				$key_sql = 'date'.$i.'_amount';
				$r_key_sql = 'total_of_date'.$i;
				$fetch_sum_1 = "SELECT sum($key_sql) AS date1_amount FROM loan_reserve_draws WHERE talimar_loan = '".$talimar_loan."'";
				$fetch_sum_1 = $this->User_model->query($fetch_sum_1);
				$fetch_sum_1 = $fetch_sum_1->row();
				$data[$r_key_sql] = $fetch_sum_1->date1_amount;
			}
		}

		$fetch_data = "SELECT * FROM loan_reserve_date WHERE talimar_loan = '" . $talimar_loan . "'";
		$fetch_data = $this->User_model->query($fetch_data);
		$data['reserve_date'] = '';
		$fetch_data = $fetch_data->result();
		if($fetch_data){
			$data['reserve_date'] = $fetch_data;	
		}	
		

		$fetch_data2 = "SELECT * FROM loan_reserve_checkbox WHERE talimar_loan = '".$talimar_loan."'";
		$fetch_data2 = $this->User_model->query($fetch_data2);
		$data['reserve_checkbox'] = '';
		if ($fetch_data2->num_rows() > 0) {
			$fetch_data2 = $fetch_data2->result();
			$data['reserve_checkbox'] = $fetch_data2;
		}

		$fetch_reserve_folders = $this->User_model->select_where('loan_reserve_folder', $arrayLoanArg);
		$data['loan_reserve_folders'] = '';
		if ($fetch_reserve_folders->num_rows() > 0) {
			$fetch_reserve_folders_result = $fetch_reserve_folders->result();
			$data['loan_reserve_folders'] = $fetch_reserve_folders_result;
		}

		$fetch_loan_data = $this->User_model->select_where('loan', $arrayLoanArgId);
		$fetch_loan_result = $fetch_loan_data->result();
		foreach ($fetch_loan_result as $row) {

			$data['loan_amountt'] = $row->loan_amount;
			$current_balance = $row->current_balance;
			$lender_fee = $row->lender_fee . '%';
			$data['fci'] = $row->fci;

			$data['intrest_ratee'] = number_format($row->intrest_rate,3);
			$data['c_date'] = $row->loan_funding_date;
			$data['payment_gurranty'] = $row->payment_gurranty;
		}

		$result = $this->User_model->select_star('vendors');
		$result_new = $result->result();
		$data['all_vendor_data'] = $result_new;

		$reserve_extra_field = $this->User_model->select_where('loan_reserve_extra_field', $arrayLoanArg);
		$reserve_extra_field = $reserve_extra_field->result();
		$data['fetch_reserve_extra_field'] = $reserve_extra_field;

		$data['select_borrower_contact1'] = '';
		$data['select_borrower_contact1_email'] = '';
		$data['select_borrower_contact1_phone'] = '';
		$data['select_borrower_contact1_title'] = '';

		$data['select_borrower_contact2'] = '';
		$data['select_borrower_contact2_email'] = '';
		$data['select_borrower_contact2_phone'] = '';
		$data['select_borrower_contact2_title'] = '';


		$borrower_id = isset($borrower_id) ? $borrower_id : '';
		$fetch_borrower_contact1 = $this->User_model->query("select * from borrower_contact where borrower_id='".$borrower_id."' order by contact_responsibility  asc");

		if ($fetch_borrower_contact1->num_rows() > 0) {
			$fetch_borrower_contact = $fetch_borrower_contact1->result();

			foreach ($fetch_borrower_contact as $ky => $vae) {

					$abc[$vae->contact_id]=$vae->contact_responsibility;
				}

			foreach ($fetch_borrower_contact as $key => $value) {
				
				$b_con_id['contact_id'] = $value->contact_id;
			
				$fetch_b_contact = $this->User_model->select_where('contact', $b_con_id);

				$b_contacts = $fetch_b_contact->result();
				foreach ($b_contacts as $key => $values) {
				
						$b_contact[]=array(

									"first"=>$values->contact_firstname,
									"middle"=>$values->contact_middlename,
									"last"=>$values->contact_lastname,
									"contact_id"=>$values->contact_id,
									"contact_email"=>$values->contact_email,
									"contact_phone"=>$values->contact_phone,
									"b_signed"=>$abc[$values->contact_id],


									);

				

			}
		  }			$data['b_contact']=$b_contact;

				
				
		}

		if ($fetch_borrower_contact1->num_rows() > 0) {
			$fetch_borrower_contact = $fetch_borrower_contact1->result();
			// ---------------------- Borrower Contact 1 Section ------------------


			if (isset($fetch_borrower_contact[0]->contact_id)) {
				$b_con_id['contact_id'] = $fetch_borrower_contact[0]->contact_id;
				$fetch_b_contact = $this->User_model->select_where('contact', $b_con_id);

				if ($fetch_b_contact->num_rows() > 0) {
					$b_contact = $fetch_b_contact->result();
					$data['select_borrower_contact1'] = $b_contact[0]->contact_firstname . ' ' . $b_contact[0]->contact_middlename . ' ' . $b_contact[0]->contact_lastname;

					$data['select_borrower_contact1_id'] = $b_contact[0]->contact_id;
					$data['select_borrower_contact1_email'] = $b_contact[0]->contact_email;
					$data['select_borrower_contact1_phone'] = $b_contact[0]->contact_phone;
					$data['select_borrower_contact1_title'] = $fetch_borrower_contact[0]->c_title;
				}

			}
			// ---------------------- Borrower Contact 2 Section -----------------

			if (isset($fetch_borrower_contact[1]->contact_id)) {
				$b_con_id['contact_id'] = $fetch_borrower_contact[1]->contact_id;
				$fetch_b_contact = $this->User_model->select_where('contact', $b_con_id);
				if ($fetch_b_contact->num_rows() > 0) {
					$b_contact = $fetch_b_contact->result();
					$data['select_borrower_contact2'] = $b_contact[0]->contact_firstname . ' ' . $b_contact[0]->contact_middlename . ' ' . $b_contact[0]->contact_lastname;

					$data['select_borrower_contact2_id'] = $b_contact[0]->contact_id;
					$data['select_borrower_contact2_email'] = $b_contact[0]->contact_email;
					$data['select_borrower_contact2_phone'] = $b_contact[0]->contact_phone;
					$data['select_borrower_contact2_title'] = $fetch_borrower_contact[1]->c_title;
				}

			}

			// ---------------------- Borrower Contact 3 Section -----------------

			if (isset($fetch_borrower_contact[2]->contact_id)) {
				$b_con_id['contact_id'] = $fetch_borrower_contact[2]->contact_id;
				$fetch_b_contact = $this->User_model->select_where('contact', $b_con_id);
				if ($fetch_b_contact->num_rows() > 0) {
					$b_contact = $fetch_b_contact->result();
					$data['select_borrower_contact3'] = $b_contact[0]->contact_firstname . ' ' . $b_contact[0]->contact_middlename . ' ' . $b_contact[0]->contact_lastname;

					$data['select_borrower_contact3_id'] = $b_contact[0]->contact_id;
					$data['select_borrower_contact3_email'] = $b_contact[0]->contact_email;
					$data['select_borrower_contact3_phone'] = $b_contact[0]->contact_phone;
					$data['select_borrower_contact3_title'] = $fetch_borrower_contact[2]->c_title;
				}

			}

			// ---------------------- Borrower Contact 4 Section -----------------

			if (isset($fetch_borrower_contact[3]->contact_id)) {
				$b_con_id['contact_id'] = $fetch_borrower_contact[3]->contact_id;
				$fetch_b_contact = $this->User_model->select_where('contact', $b_con_id);
				if ($fetch_b_contact->num_rows() > 0) {
					$b_contact = $fetch_b_contact->result();
					$data['select_borrower_contact4'] = $b_contact[0]->contact_firstname . ' ' . $b_contact[0]->contact_middlename . ' ' . $b_contact[0]->contact_lastname;

					$data['select_borrower_contact4_id'] = $b_contact[0]->contact_id;
					$data['select_borrower_contact4_email'] = $b_contact[0]->contact_email;
					$data['select_borrower_contact4_phone'] = $b_contact[0]->contact_phone;
					$data['select_borrower_contact4_title'] = $fetch_borrower_contact[3]->c_title;
				}

			}
		}

		$fetch_reserve_folders = $this->User_model->select_where('loan_reserve_folder', array('talimar_loan' => $talimar_loan));
		if ($fetch_reserve_folders->num_rows() > 0) {
			$fetch_reserve_folders_result = $fetch_reserve_folders->result();
			$data['loan_reserve_folders'] = $fetch_reserve_folders_result;
			foreach ($fetch_reserve_folders_result as $row_p_ff) {
				// fetch loan_property_images
				$fetch_loan_res_images = $this->User_model->select_where('loan_reserve_images', array('talimar_loan' => $talimar_loan, 'title' => $row_p_ff->id));
				if ($fetch_loan_res_images->num_rows() > 0) {
					$data['fetch_loan_res_images'][$row_p_ff->id] = $fetch_loan_res_images->result();
				}
			}
		}

		$data['id'] = $loan_id;
		$data['property_addresss'] = $this->loan_data_by_address($talimar_loan);
		$data['talimar_loan'] = $talimar_loan;
		$data['loan_id'] = $loan_id;
		$this->load->view('template_files/loan_portfolio/modal_loan_reserve', $data);
	}

	public function modal_recording_information(){
		error_reporting(0);
		$data = $this->config_variable();
		$loan_id 	= $this->input->post('loan_id');
		$loan_data  = $this->loan_data_by_id($loan_id);
		$talimar_loan = $loan_data->talimar_loan;
		$borrower_id = $loan_data->borrower;

		$recording_informationn = $this->User_model->select_where('loan_recording_information', array('talimar_loan' => $talimar_loan));
		$recording_information_dataa = $recording_informationn->result();
		$data['recording_information'] = $recording_information_dataa;

		foreach ($data['recording_information'] as $rt) {
			$data['rn'] = $rt->record_number;
			$data['rd'] = $rt->recorded_date;
			$data['ro'] = $rt->record_option;
		}

		$data['id'] = $loan_id;
		$data['property_addresss'] = $this->loan_data_by_address($talimar_loan);
		$data['talimar_loan'] = $talimar_loan;
		$data['loan_id'] = $loan_id;
		$this->load->view('template_files/loan_portfolio/modal_recording_information', $data);
	}

	public function modal_construcion_type_sources_and_uses(){
		error_reporting(0);
		$data = $this->config_variable();
		$loan_id 	= $this->input->post('loan_id');
		$loan_data  = $this->loan_data_by_id($loan_id);
		$talimar_loan = $loan_data->talimar_loan;
		$borrower_id = $loan_data->borrower;

		$fetch_loan_source_and_uses1 = $this->User_model->select_where_asc('loan_source_and_uses', array('talimar_loan' => $talimar_loan), 'id');
		$data['fetch_loan_source_and_uses'] = '';
		if ($fetch_loan_source_and_uses1->num_rows() > 0) {
			$fetch_loan_source_and_uses = $fetch_loan_source_and_uses1->result();
			$data['fetch_loan_source_and_uses'] = $fetch_loan_source_and_uses;
		}

		$data['id'] = $loan_id;
		$data['property_addresss'] = $this->loan_data_by_address($talimar_loan);
		$data['talimar_loan'] = $talimar_loan;
		$data['loan_id'] = $loan_id;
		$this->load->view('template_files/loan_portfolio/modal_construcion_type_sources_and_uses', $data);
	}

	public function modal_assigment_main(){
		error_reporting(0);
		$data = $this->config_variable();

		$userId = $this->session->userdata('t_user_id');

		$userData = $this->User_model->select_where('user', array('id'=>$userId,'role'=>1));
		if($userData->num_rows() > 0)
		{
			$userData = $userData->row();
			$data['remove_lender_from_loan'] = $userData->remove_lender_from_loan;
		}
		else
		{
			$data['remove_lender_from_loan'] = 'no';
		}

		$loan_id 	= $this->input->post('loan_id');
		$loan_data  = $this->loan_data_by_id($loan_id);
		$talimar_loan = $loan_data->talimar_loan;
		$borrower_id = $loan_data->borrower;

		$investor_all_data = $this->User_model->select_star_asc('investor', 'name');
		$investor_all_data = $investor_all_data->result();
		$data['investor_all_data'] = $investor_all_data;

		$loan_servicingRec = $this->User_model->select_where('loan_servicing', array('talimar_loan' => $talimar_loan));
		$loan_servicingRec = $loan_servicingRec->row_array();
		$data['loan_servicing'] = $loan_servicingRec;

		$sql = "SELECT * FROM `loan_assigment` WHERE `talimar_loan` = '" . $talimar_loan . "' ORDER BY `position` ASC";
		$loan_assigment_data = $this->User_model->query($sql);
		$loan_assigment_data = $loan_assigment_data->result();

		$loan_assigmentdata = array();
		foreach ($loan_assigment_data as $keyEID => $row) {

			$lender_id = $row->lender_name;
			$fetch_investor_of_assigmnet = $this->User_model->select_where('investor', array('id' => $lender_id));
			if ($fetch_investor_of_assigmnet->num_rows() > 0) {
				$investor_of_assigmnet = $fetch_investor_of_assigmnet->row();
				$data['all_lender_name_of_assigment'][] = $investor_of_assigmnet->name;
			}

			$sql = "SELECT * FROM lender_contact JOIN contact ON lender_contact.contact_id = contact.contact_id WHERE lender_contact.lender_id = '" . $lender_id . "' ";
			$fetch_lender_email_contact1 = $this->User_model->query($sql);
			if ($fetch_lender_email_contact1->num_rows() > 0) {
				$fetch_lender_email_contact = $fetch_lender_email_contact1->result();
				foreach ($fetch_lender_email_contact as $contact_data) {
					$data['email_contact'][$lender_id][] = array(
						"contact_name" => $contact_data->contact_firstname . ' ' . $contact_data->contact_middlename . ' ' . $contact_data->contact_lastname,
						"title" => $contact_data->c_title,
						"email" => $contact_data->contact_email,
						"phone" => $contact_data->contact_phone,
						"contact_id" => $contact_data->contact_id,
					);
				}
			}
			if(!empty($talimar_loan)){		
				
				//call to signNo API
				$EnvolopID = $this->User_model->query("SELECT * FROM `signnow_api_doc` WHERE talimar_loan = '".$talimar_loan."' AND lender_id = '" . $lender_id . "' ORDER BY id DESC");			

				if($EnvolopID->num_rows() > 0){			
					
					$EnvolopID = $EnvolopID->row();			
					$DocumentData = $this->GetDocusignAPIDocs($EnvolopID->documentID);			

					if(!empty($DocumentData->status))
					{
						$row->EnvolopStatus = $DocumentData->status;
					}
					else
					{
						$row->EnvolopStatus = "Not Ready";
					}
				}
				else
				{					
					$row->EnvolopStatus = "Not Ready";
				}
				$loan_assigmentdata[$keyEID] = $row;
			}
		}
		$data['loan_assigment_data'] = $loan_assigmentdata;

		$fetch_loan_data = $this->User_model->select_where('loan', array('talimar_loan' => $talimar_loan));
		$fetch_loan_result = $fetch_loan_data->result();
		$data['fetch_loan_result'] = $fetch_loan_result;

		$fee_disbursement_data = $this->User_model->query("SELECT * FROM fee_disbursement WHERE talimar_loan = '" . $talimar_loan . "' ORDER by id ");
		$data['fee_disbursement_data'] = array();
		if ($fee_disbursement_data->num_rows() > 0) {
			$fee_disbursement_data = $fee_disbursement_data->result();
			$data['fee_disbursement_data'] = $fee_disbursement_data;
		}

		$fetch_LenderApproval = $this->User_model->query("SELECT * FROM `lender_approval` WHERE talimar_loan = '" . $talimar_loan . "'");
		if($fetch_LenderApproval->num_rows() > 0){
			$fetch_LenderApproval = $fetch_LenderApproval->result();
			$data['LenderAppDAta'] = $fetch_LenderApproval;
		}else{
			$data['LenderAppDAta'] = '';
		}

		$fetch_extra_details = $this->User_model->select_where('extra_details', array('talimar_loan' => $talimar_loan));
		$fetch_extra_details = $fetch_extra_details->result();
		$data['fetch_extra_details'] = $fetch_extra_details;

		$where_contact_1['lender_contact_type'] = 1;
		$fetch_all_lender_contact = $this->User_model->select_where_asc('contact', $where_contact_1, 'contact_firstname');

		$loan_borrower_data = $this->User_model->query("SELECT bd.b_name, bc.contact_id,c.contact_firstname , c.contact_lastname, c.contact_email  FROM borrower_data as bd JOIN borrower_contact as bc JOIN contact as c ON bd.id = bc.borrower_id AND bc.contact_id = c.contact_id WHERE bd.id = '" . $borrower_id . "'");

		
		if ($loan_borrower_data->num_rows() > 0) {

			$loan_borrower_result = $loan_borrower_data->row();
			$data['draws_borrower_name'] = $loan_borrower_result->b_name;
			$data['draws_contact_name'] = $loan_borrower_result->contact_firstname . ' ' . $loan_borrower_result->contact_lastname;

			$data['draws_contact_first_name'] = $loan_borrower_result->contact_firstname;
			$data['draws_contact_emailssss'] = $loan_borrower_result->contact_email;
		}

		if ($fetch_all_lender_contact->num_rows() > 0) {
			$data['fetch_all_lender_contact'] = $fetch_all_lender_contact->result();
			$data['email_lender'] = $loan_borrower_result->contact_email;
		}

		$getdocuments = $this->User_model->query("SELECT * FROM `signnow_api_doc` WHERE talimar_loan = '".$talimar_loan."' ORDER BY id desc" );
		if($getdocuments->num_rows() > 0){
			
			$getdocuments = $getdocuments->result();

			$timeccchhh = date("H");
			foreach ($getdocuments as $valueID) {
				//if($timeccchhh == '12'){
					$DocumentData = $this->GetDocusignAPIDocs($valueID->documentID);
					//$data['lenderSignDoc'][] = $DocumentData;
					$data['lenderSignDoc'][$valueID->lender_id][] = $DocumentData;
				//}

			}
		}

		$data['id'] = $loan_id;
		$data['property_addresss'] = $this->loan_data_by_address($talimar_loan);
		$data['talimar_loan'] = $talimar_loan;
		$data['loan_id'] = $loan_id;
		$this->load->view('template_files/loan_portfolio/modal_assigment_main', $data);
	}

	public function modal_FCI_Documents(){
		error_reporting(0);
		$data = $this->config_variable();
		$loan_id 	= $this->input->post('loan_id');
		$loan_data  = $this->loan_data_by_id($loan_id);
		$talimar_loan = $loan_data->talimar_loan;
		$borrower_id = $loan_data->borrower;
		
		$fciboarding = $this->User_model->query("SELECT * FROM fci_boarding WHERE talimar_loan='".$talimar_loan."' ORDER BY datetime DESC");
		if($fciboarding->num_rows() > 0){
			$data['fciboardingdata'] = $fciboarding->result();
		}

		if(is_numeric($loan_id))
		{ 
			$loan_data = $this->loan_data_by_id($loan_id);

			$data['loan_id'] 			= $loan_id;
			$data['talimar_loan'] 		= $loan_data->talimar_loan;
			$data['FundingEntity'] 		= $loan_data->FundingEntity;
			$data['fci'] 				= $loan_data->fci;
			$data['borrower'] 			= $loan_data->borrower;
			$data['loan_source'] 		= $loan_data->loan_source;
			$data['rep_contact'] 		= $loan_data->rep_contact;
			$data['loan_amount'] 		= $loan_data->loan_amount;
			$data['current_balance'] 	= $loan_data->current_balance;
			$data['lender_fee'] 		= $loan_data->lender_fee.'%';
			$data['intrest_rate'] 		= $loan_data->intrest_rate ? $loan_data->intrest_rate.'' : '10';
			$data['term_month'] 		= $loan_data->term_month;
			$data['per_payment_period'] = $loan_data->per_payment_period;
			$data['payment_held_close'] = $loan_data->payment_held_close;
			$data['calc_of_year'] 		= $loan_data->calc_of_year;
			$data['loan_type']			= $loan_data->loan_type;
			$data['transaction_type'] 	= $loan_data->transaction_type;
			$data['position'] 			= $loan_data->position;
			$data['payment_type'] 		= $loan_data->payment_type ? $loan_data->payment_type : 1;
			$data['exit_strategy'] 		= $loan_data->exit_strategy;
			$data['loan_conditions'] 	= $loan_data->loan_conditions;
			$data['first_payment_collect'] = $loan_data->first_payment_collect;
			$data['loan_highlights'] 	= $loan_data->loan_highlights;
			$data['baloon_payment_type'] = $loan_data->baloon_payment_type;
			$data['payment_sechdule'] 	= $loan_data->payment_sechdule;
			$data['intrest_type'] 		= $loan_data->intrest_type;
			$data['payment_amount'] 	= $loan_data->payment_amount;
			$data['minium_intrest'] 	= $loan_data->minium_intrest ? $loan_data->minium_intrest : '0';
			$data['min_intrest_month'] 	= $loan_data->min_intrest_month;
			$data['min_payment']	 	= $loan_data->min_payment;
			$data['owner_purpose'] 		= $loan_data->owner_purpose;
			$data['costruction_loan'] 	= $loan_data->costruction_loan;
			$data['project_cost'] 		= $loan_data->project_cost;
			$data['total_project_cost'] = $loan_data->total_project_cost;
			$data['borrower_enquity'] 	= $loan_data->borrower_enquity;
			$data['grace_period'] 		= $loan_data->grace_period;
			$data['late_charges_due'] 	= $loan_data->late_charges_due;
			$data['replace_late_fee'] 	= $loan_data->replace_late_fee;
			$data['default_rate'] 		= $loan_data->default_rate;
			$data['how_many_days']		= $loan_data->how_many_days;
			$data['return_check_rate'] 	= $loan_data->return_check_rate;
			$data['refi_existing'] 		= $loan_data->refi_existing;
			$data['property'] 			= $loan_data->property;
			$data['interest_reserve'] 	= $loan_data->interest_reserve;
			$data['loan_document_date'] = $loan_data->loan_document_date ? date('m-d-Y',strtotime($loan_data->loan_document_date)) : '';
			$data['loan_funding_date'] 	= $loan_data->loan_funding_date  ? date('m-d-Y',strtotime($loan_data->loan_funding_date)) : '';
			$data['ucc_filing'] 		= $loan_data->ucc_filing;
			$data['draws'] 				= $loan_data->draws;
			$data['phase'] 				= $loan_data->phase;
			$data['auto_payment'] 		= $loan_data->auto_payment;
			$data['t_user_id'] 			= $loan_data->t_user_id;
			$data['calc_based'] 		= $loan_data->calc_based;
			$data['cross_collateralized'] = $loan_data->loan_cross_collateralized;
			$data['cross_provision'] 	= $loan_data->cross_collateralized_provision;
			
			$interest_reserve 			= $loan_data->interest_reserve;
			$term_month 				= $loan_data->term_month;

			 if($interest_reserve == '1'){
				$data['first_payment_date'] = $loan_data->first_payment_date ? date('m-d-Y',strtotime($loan_data->first_payment_date)) : '';				
			 }
			 else if($interest_reserve == '2')
			 {
			 	$data['first_payment_date'] = $loan_data->first_payment_date ? date('m-d-Y',strtotime($loan_data->first_payment_date)) : ''; 
			}else{
				$data['first_payment_date'] = $loan_data->first_payment_date ? date('m-d-Y',strtotime($loan_data->first_payment_date)) : '';
			}
			
			$data['maturity_date'] 			=  date('m-d-Y',strtotime($loan_data->loan_funding_date.' +'.$term_month.' months'));
			$data['modify_maturity_date'] 	= $loan_data->modify_maturity_date ? date('m-d-Y',strtotime($loan_data->modify_maturity_date)) : '';
			$data['payment_due'] 			= $loan_data->payment_due;
			$data['extention_option'] 		= $loan_data->extention_option;
			$data['extention_month'] 		= $loan_data->extention_month;
			$data['extention_percent_amount'] = $loan_data->extention_percent_amount;
			$data['ballon_payment'] 		= $loan_data->ballon_payment;
			
			$data['trustee'] 				= $loan_data->trustee;
			$data['promissory_note_val'] 	= $loan_data->promissory_note_val;
			$data['payment_requirement'] 	= $loan_data->payment_requirement;
			$data['add_broker_data_desc'] 	= $loan_data->add_broker_data_desc;
			$data['min_bal_type'] 			= $loan_data->min_bal_type;
			$data['payment_gurranty'] 		= $loan_data->payment_gurranty;
			$data['payment_gurr_explain'] 	= $loan_data->payment_gurr_explain;
			$data['interest_reserve'] 		= $loan_data->interest_reserve;
			$data['additional_gra'] 		= $loan_data->additional_gra;
			$data['add_gra_c_id'] 		    = $loan_data->add_gra_c_id;

			$talimar_loan = $loan_data->talimar_loan;

			
			$data['property_addresss'] = $this->loan_data_by_address($talimar_loan);

			$servicing_contact = $this->User_model->query("SELECT * FROM `loan` where talimar_loan = '" . $talimar_loan . "'");
			$data['servicing_contact_details'] = $servicing_contact->row();

		}else
		{
			$data['loan_id'] 				= '';
			$data['talimar_loan'] 			= '';	
			$data['fci']					= '';
			$data['FundingEntity'] 			= '';
			$data['add_gra_c_id'] 			= '';
			$data['additional_gra'] 		= '';
			$data['borrower'] 				= '';
			$data['loan_source'] 			= '';
			$data['rep_contact']			= '';
			$data['loan_amount'] 			= '00';
			$data['current_balance'] 		= '00';
			$data['lender_fee'] 			= '2.5%';
			$data['intrest_rate'] 			= '10';
			$data['term_month'] 			= '12';
			$data['per_payment_period'] 	= '12';
			$data['payment_held_close'] 	= '6';
			$data['calc_of_year'] 			= '1';
			$data['loan_type'] 				= '';
			$data['transaction_type'] 		= '';
			$data['position']				= '1';
			$data['payment_type'] 			= '1';
			$data['payment_sechdule'] 		= '1';
			$data['intrest_type'] 			= '1';
			$data['payment_amount'] 		= '0.00';
			$data['minium_intrest'] 		= '0';
			$data['min_intrest_month'] 		= '';
			$data['owner_occupied'] 		= '';
			$data['min_payment']	 		= '';
			$data['owner_purpose'] 			= '1';
			$data['costruction_loan'] 		= '';
			$data['project_cost'] 			= '0.00';
			$data['total_project_cost'] 	= '0.00';
			$data['borrower_enquity'] 		= '0.00';
			$data['grace_period'] 			= '10';
			$data['late_charges_due'] 		= '10';
			$data['replace_late_fee'] 		= '';
			$data['default_rate'] 			= '17';
			$data['how_many_days'] 			= '10';
			$data['return_check_rate'] 		= '150';
			$data['refi_existing']			= '0';
			$data['property'] 			    = '';
			$data['loan_document_date'] 	= '';
			$data['loan_funding_date']		= '';
			// $data['loan_funding_date'] 		= date('m-d-Y');
			$data['first_payment_date'] 	= '';
			$data['maturity_date'] 			= '';
			// $data['maturity_date'] 			=  date('m-d-Y', strtotime('+1 years'));
			$data['payment_due'] 			= '';
			$data['extention_option'] 		= '';
			$data['extention_month'] 		= '6';
			$data['extention_percent_amount'] = '1%';
			$data['ballon_payment'] 		= '0.00';
			$data['trustee'] 				= 'FCI Lender Services';
			// $data['trustee'] 				= '';
			$data['promissory_note_val'] 	= '';
			$data['baloon_payment_type'] 	= '1';
			$data['payment_requirement'] 	= '0';
			$data['exit_strategy'] 			= 'Sale of Property';
			$data['loan_conditions']		= '';
			$data['loan_highlights'] 		= '';
			$data['add_broker_data_desc'] 	= '';
			$data['min_bal_type'] 			= '1';
			$data['payment_gurranty'] 		= '0'; // by default set with 1...
			$data['payment_gurr_explain'] 	= '';
			$data['first_payment_collect'] 	= '';
			$data['interest_reserve'] 		= '';
			$data['auto_payment'] 			= '';
			$data['t_user_id']				= '';
			$data['calc_based'] 			= '';
			$data['cross_collateralized'] 	= '';
			$data['cross_provision'] 		= '';
		}

		$fetch_loan_borrower = $this->User_model->select_where('borrower_data', array('id' => $borrower_id));
		$data['fetch_loan_borrower'] = $fetch_loan_borrower->row();

		$fetchborrowercontact = $this->User_model->query("SELECT c.contact_firstname, c.contact_lastname, c.contact_phone, c.contact_email, c.contact_fax, c.company_name_1 FROM borrower_contact as bc JOIN contact as c ON bc.contact_id = c.contact_id WHERE bc.borrower_id = '".$borrower_id."' AND bc.contact_primary = '1'");
		$data['fetchborrowercontact'] = '';
		if($fetchborrowercontact->num_rows() > 0){
			$data['fetchborrowercontact'] = $fetchborrowercontact->result_array();
		}

		$outlook_query = $this->User_model->query("SELECT * FROM loan as l JOIN loan_property as lp on l.talimar_loan = lp.talimar_loan WHERE l.talimar_loan = '" . $talimar_loan . "'");
		$outlook_query = $outlook_query->result();
		$data['outlook_property'] = $outlook_query;

		$sql = "SELECT SUM(amount) as amount FROM loan_monthly_payment WHERE talimar_loan = '".$talimar_loan."'";
		$amount_monthly_payment = $this->User_model->query($sql);
		$amount_monthly_payment = $amount_monthly_payment->result();

		$data['amount_monthly_payment'] = $amount_monthly_payment;




		$data['id'] = $loan_id;
		$data['property_addresss'] = $this->loan_data_by_address($talimar_loan);
		$data['talimar_loan'] = $talimar_loan;
		$data['loan_id'] = $loan_id;
		$this->load->view('template_files/fci_apidoc', $data);
	}

	public function modal_title(){
		error_reporting(0);
		$data = $this->config_variable();
		$loan_id 	= $this->input->post('loan_id');
		$loan_data  = $this->loan_data_by_id($loan_id);
		$talimar_loan = $loan_data->talimar_loan;
		$borrower_id = $loan_data->borrower;

		$fetch_loan_borrower = $this->User_model->select_where('borrower_data', array('id' => $borrower_id));
		$data['fetch_loan_borrower'] = $fetch_loan_borrower->row();

		$fetch_title_data = $this->User_model->select_where('loan_title', array('talimar_loan' => $talimar_loan));
		$fetch_title_data = $fetch_title_data->result();
		$data['fetch_title_data'] = $fetch_title_data;

		if(count($fetch_title_data) > 0)
		{
			foreach($fetch_title_data as $row)
			{
				
				$data['title_company'] 		= $row->company;
				$data['title_title'] 		= $row->title;
				$data['title_first_name'] 	= $row->first_name;
				$data['title_last_name'] 	= $row->last_name;
				$data['title_phone'] 	    = $row->phone;
				$data['title_email'] 		= $row->email;
				$data['title_address'] 		= $row->address;
				//$data['title_address_b'] 	= $row->address_b;
				$data['title_unit'] 	    = $row->unit;
				$data['title_city'] 		= $row->city;
				$data['title_state']		= $row->state;
				$data['title_zip'] 			= $row->zip;
				$data['prelim_receive'] 			= $row->prelim_receive;
				$data['prelim_approve'] 			= $row->prelim_approve;
				$data['title_lender_policy_type'] 			= $row->lender_policy_type;
				$data['title_policy_amount_in_percent'] 	= $row->policy_amount_in_percent.'%';
				$data['title_policy_amount_in_dolar'] 		= $row->policy_amount_in_dolar;
				$data['title_endorsement'] 					= $row->endorsement;
				$data['title_exception'] 					= $row->exception;
				$data['title_report_elimination'] 			= $row->report_elimination;
				$data['title_ptr_date'] 					= $row->ptr_date;
				$data['title_order_no']					= $row->title_order_no;
			}
		} 
		else
		{
				$data['title_company'] 						= '';
				$data['title_title']						= '';
				$data['title_first_name'] 					= '';
				$data['title_last_name'] 					= '';
				$data['title_phone']	 					= '';
				$data['title_email']	 					= '';
				$data['title_address'] 						= '';
				//$data['title_address_b']					= '';
				$data['title_unit'] 						= '';
				$data['title_state'] 						= '';
				$data['title_city'] 						= '';
				$data['title_zip'] 							= '';
				$data['title_lender_policy_type'] 			= '';
				$data['title_policy_amount_in_percent'] 	= '';
				$data['title_policy_amount_in_dolar'] 		= '0.00';
				//$data['title_endorsement'] 					= '104.1 – Assignment Endorsement';
				$data['title_endorsement'] 					= '';
				$data['title_exception'] 					= '';
				$data['title_report_elimination'] 			= '';
				$data['title_order_no'] 			= '';
				$data['prelim_approve'] 			= '';
				$data['prelim_receive'] 			= '';
		}

		$fetch_loan_borrower = $this->User_model->select_where('borrower_data', array('id' => $borrower_id));
		$data['fetch_loan_borrower'] = $fetch_loan_borrower->result();

		$data['borrower_id'] = $loan_id;
		$data['id'] = $loan_id;
		$data['property_addresss'] = $this->loan_data_by_address($talimar_loan);
		$data['talimar_loan'] = $talimar_loan;
		$data['loan_id'] = $loan_id;
		$this->load->view('template_files/loan_portfolio/modal_title', $data);
	}

	public function modal_escrow(){
		error_reporting(0);
		$data = $this->config_variable();
		$loan_id 	= $this->input->post('loan_id');
		$loan_data  = $this->loan_data_by_id($loan_id);
		$talimar_loan = $loan_data->talimar_loan;
		$borrower_id = $loan_data->borrower;

		$fetch_escrow_data = $this->User_model->select_where('loan_escrow', array('talimar_loan' => $talimar_loan));
		$data['fetch_escrow_data'] = '';
		if ($fetch_escrow_data->num_rows() > 0) {
			$fetch_escrow_data = $fetch_escrow_data->result();
			$data['fetch_escrow_data'] = $fetch_escrow_data;
		}

		if((isset($fetch_escrow_data)) && ($fetch_escrow_data)) 
		{
			foreach($fetch_escrow_data as $row)
			{
				$data['escrow_opened']			= $row->escrow_opened;
				$data['e_company']				= $row->company;
				$data['e_escrow']				= $row->escrow;
				$data['e_first_name']		= $row->first_name;
				$data['e_last_name']			= $row->last_name;
				$data['e_phone']				= $row->phone;
				$data['e_address']				= $row->address;
				$data['e_address_b']			= $row->address_b;
				$data['e_city']					= $row->city;
				$data['e_state']				= $row->state;
				$data['e_zip']					= $row->zip;
				$data['e_email']				= $row->email;
				$data['first_payment_close']	= $row->first_payment_close;
				$data['e_net_fund']				= $row->net_fund;
				$data['e_tax_service']			= $row->tax_service;
				$data['e_expiration']			= $row->expiration;
				$data['e_instruction']			= $row->instruction;
				$data['financingApp']			= $row->financingApp;
				$data['record_assigment_close']	= $row->record_assigment_close;
				
			}
		}
		else
		{
				$data['escrow_opened']			= '';
				$data['e_company']				= '';
				$data['e_escrow']				= '';
				$data['e_first_name']			= '';
				$data['e_last_name']			= '';
				$data['e_phone']				= '';
				$data['e_address']				= '';
				$data['e_address_b']			= '';
				$data['e_city']					= '';
				$data['e_state']				= '';
				$data['e_zip']					= '';
				$data['e_email']				= '';
				$data['first_payment_close']	= '';
				$data['e_net_fund']				= '';
				$data['e_tax_service']			= '';
				$data['e_expiration']			= '';
				$data['e_instruction']			= '';
				$data['financingApp']			= '';
				$data['record_assigment_close']			= '';
		}

		$fetch_wire_instruction = $this->User_model->select_where('wire_instruction', array('talimar_loan' => $talimar_loan, 'loan_id' => $loan_id));
		$data['fetch_wire_instruction'] = '';
		if ($fetch_wire_instruction->num_rows() > 0) {
			$data['fetch_wire_instruction'] = $fetch_wire_instruction->result();
		}

		$loan_borrower_id_data['id'] = $borrower_id;
		$fetch_loan_borrower = $this->User_model->select_where('borrower_data', $loan_borrower_id_data);
		$data['fetch_loan_borrower'] = '';
		if ($fetch_loan_borrower->num_rows() > 0) {
			$fetch_loan_borrower = $fetch_loan_borrower->result();
			$data['fetch_loan_borrower'] = $fetch_loan_borrower;
		}

		$primary_property_query = $this->User_model->query("SELECT * FROM property_home as h JOIN loan_property as lp on lp.property_home_id = h.id WHERE h.talimar_loan = '" . $talimar_loan . "' AND primary_property = '1' ");

		$primary_property_result = $primary_property_query->result();
		$data['primary_property_result'] = $primary_property_result;
	
		$data['borrower_id'] = $loan_id;
		$data['id'] = $loan_id;
		$data['property_addresss'] = $this->loan_data_by_address($talimar_loan);
		$data['talimar_loan'] = $talimar_loan;
		$data['loan_id'] = $loan_id;
		$this->load->view('template_files/loan_portfolio/modal_escrow', $data);
	}


	public function modal_impound_accounts(){
		error_reporting(0);
		$data = $this->config_variable();
		$loan_id 	= $this->input->post('loan_id');
		$loan_data  = $this->loan_data_by_id($loan_id);
		$talimar_loan = $loan_data->talimar_loan;
		$borrower_id = $loan_data->borrower;

		$fetch_impound_account = $this->User_model->select_where_asc('impound_account', array('talimar_loan' => $talimar_loan), 'position');
		$data['fetch_impound_account'] = '';
		if ($fetch_impound_account->num_rows() > 0) {
			$data['fetch_impound_account'] = $fetch_impound_account->result();
		}

		$fetch_loan_data = $this->User_model->select_where('loan', array('talimar_loan' => $talimar_loan));
		$fetch_loan_result = $fetch_loan_data->result();
		$data['fetch_loan_result'] = $fetch_loan_result;

		$fetch_table_loan_property_insurance1 = $this->User_model->query("SELECT sum(annual_insurance_payment) as annual_insurance_payment FROM loan_property_insurance WHERE talimar_loan = '" . $talimar_loan . "' AND insurance_type = 1 ");

			// annual_insurance_payment
		$loan_property_insurance_result1 = $fetch_table_loan_property_insurance1->row();
		$data['loan_property_insurance_result_with_1'] = $loan_property_insurance_result1;

		$fetch_property_home = $this->User_model->select_where_asc('property_home', array('talimar_loan' => $talimar_loan), 'primary_property');
		if ($fetch_property_home->num_rows() > 0) {
			$property_home = $fetch_property_home->result();
			// echo'<pre>';
			// print_r($property_home);
			// echo'</pre>';
			$data['fetch_property_home'] = $fetch_property_home->result();
		}

		if (isset($property_home)) {
			foreach ($property_home as $row) {
				if ($row->primary_property == 1) {
					$fetch_primary_loan_property_taxes = $this->User_model->select_where('loan_property_taxes', array('talimar_loan' => $talimar_loan, 'property_home_id' => $row->id));
					$data['primary_loan_property_taxes'] = $fetch_primary_loan_property_taxes->row();
				}
			}
		}

		$sql = "SELECT SUM(amount) as sum_impounded FROM impound_account WHERE talimar_loan = '" . $talimar_loan . "' AND items != 'Mortgage Payment' AND impounded = '1' ";

		// echo "SELECT SUM(amount) as sum_impounded FROM impound_account WHERE impounded = '1' AND talimar_loan = '".$talimar_loan_number."' AND items != 'Mortgage Payment'";
		$fetch_sum_impound_account = $this->User_model->query($sql);
		$fetch_sum_impound_account = $fetch_sum_impound_account->result();
		$data['sum_impound_account'] = $fetch_sum_impound_account[0]->sum_impounded;

		$fetch_all_actives_query = $this->User_model->query("SELECT current_balance FROM loan WHERE talimar_loan='" . $talimar_loan . "' ");
		if ($fetch_all_actives_query->num_rows() > 0) {

			$active_loan_am = $fetch_all_actives_query->result();
			$data['outstanding_balance'] = $active_loan_am[0]->current_balance ? $active_loan_am[0]->current_balance : '';

		}

		$data['borrower_id'] = $loan_id;
		$data['id'] = $loan_id;
		$data['property_addresss'] = $this->loan_data_by_address($talimar_loan);
		$data['talimar_loan'] = $talimar_loan;
		$data['loan_id'] = $loan_id;
		$this->load->view('template_files/loan_portfolio/modal_impound_accounts', $data);
	}

	public function modal_closing_statement_new11(){
		error_reporting(0);
		$data = $this->config_variable();
		$loan_id 	= $this->input->post('loan_id');
		$loan_data  = $this->loan_data_by_id($loan_id);
		$talimar_loan = $loan_data->talimar_loan;
		$borrower_id = $loan_data->borrower;


		$fetch_closing_statement_items1 = $this->User_model->select_where_asc('closing_statement_items', array('talimar_loan' => $talimar_loan), 'hud');

		if ($fetch_closing_statement_items1->num_rows() > 0) {
			$fetch_closing_statement_items1 = $fetch_closing_statement_items1->result();
			$data['fetch_closing_statement_items_data'] = $fetch_closing_statement_items1;
		}

		$fetch_loan_data = $this->User_model->select_where('loan', array('talimar_loan' => $talimar_loan));
		$fetch_loan_result = $fetch_loan_data->result();
		$data['fetch_loan_result'] = $fetch_loan_result;


		$sql = "SELECT SUM(paid_to_other) as sum_paid_to_other, SUM(paid_to_broker) as sum_paid_to_broker FROM closing_statement_items WHERE talimar_loan = '".$talimar_loan."' ";
		$fetch_sum_closing_statement_item = $this->User_model->query($sql);
		$data['fetch_sum_closing_statement_item'] = $fetch_sum_closing_statement_item->result();

		$fetch_sum_closing_statement_item = $this->User_model->query("SELECT SUM(paid_to_other) as sum_paid_to_other, SUM(paid_to_broker) as sum_paid_to_broker FROM closing_statement_items WHERE talimar_loan = '" . $talimar_loan . "' AND direct_to_servicer = '1' ");

		if ($fetch_sum_closing_statement_item->num_rows() > 0) {
			$data['fetch_suspense_balance'] = $fetch_sum_closing_statement_item->result();
		} else {
			$fetch_sum_closing_statement_item = $this->User_model->query("SELECT SUM(paid_to_other) as sum_paid_to_other, SUM(paid_to_broker) as sum_paid_to_broker FROM closing_statement_items WHERE talimar_loan = '" . $talimar_loan . "' AND hud IN(813,814)");
			$data['fetch_suspense_balance'] = $fetch_sum_closing_statement_item->result();
		}

		$fetch_input_datas1 = $this->User_model->select_where('input_datas', array('talimar_loan' => $talimar_loan, 'page' => 'closing_statement'));
		if ($fetch_input_datas1->num_rows() > 0) {
			$fetch_input_datas = $fetch_input_datas1->result();
			foreach ($fetch_input_datas as $key => $row) {
				$data['fetch_closing_input_datas'][$row->input_name] = $row->value;
			}
		}

		$fetch['id'] = $loan_id;
		$fetch_loan_data = $this->User_model->select_where('loan', $fetch);
		$data['fetch_loan_result'] = $fetch_loan_data->result();

		$fetch_closing_deduction = $this->User_model->select_where('closing_statement_deductions', array('talimar_loan' => $talimar_loan));
		if ($fetch_closing_deduction->num_rows() > 0) {
			$data['fetch_closing_deduction'] = $fetch_closing_deduction->result();
		}

		$data['borrower_id'] = $loan_id;
		$data['id'] = $loan_id;
		$data['id'] = $borrower_id;
		$data['property_addresss'] = $this->loan_data_by_address($talimar_loan);
		$data['talimar_loan'] = $talimar_loan;
		$data['loan_id'] = $loan_id;
		$this->load->view('template_files/loan_portfolio/modal_closing_statement_new11', $data);
	}

	public function modal_payment_gurrantor(){
		error_reporting(0);
		$data = $this->config_variable();
		$loan_id 	= $this->input->post('loan_id');
		$loan_data  = $this->loan_data_by_id($loan_id);
		$talimar_loan = $loan_data->talimar_loan;
		$borrower_id = $loan_data->borrower;

		$query_payemnt_grantor = $this->User_model->select_where('payment_gurrantor', array('talimar_loan' => $talimar_loan));
		if ($query_payemnt_grantor->num_rows() > 0) {
			$data['fetch_payment_gurrantor'] = $query_payemnt_grantor->result();
		} else {

			$data['fetch_payment_gurrantor'] = '';
		}

		$fetch_all_contact = $this->User_model->select_star('contact');
		$data['fetch_all_contact'] = $fetch_all_contact->result();

		$data['borrower_id'] = $loan_id;
		$data['id'] = $loan_id;
		$data['id'] = $borrower_id;
		$data['property_addresss'] = $this->loan_data_by_address($talimar_loan);
		$data['talimar_loan'] = $talimar_loan;
		$data['loan_id'] = $loan_id;
		$this->load->view('template_files/loan_portfolio/modal_payment_gurrantor', $data);
	}

	public function modal_loan_notes(){
		error_reporting(0);
		$data = $this->config_variable();
		$loan_id 	= $this->input->post('loan_id');
		$loan_data  = $this->loan_data_by_id($loan_id);
		$talimar_loan = $loan_data->talimar_loan;
		$borrower_id = $loan_data->borrower;
		
		/*22-07-2021 By@Aj contact notes flow*/
		// $sql = $this->User_model->query("SELECT * FROM `contact_notes` as cn JOIN contact as c ON cn.contact_id = c.contact_id WHERE cn.loan_id='" . $loan_id . "' ORDER BY notes_date DESC");

		$sql = $this->User_model->query("SELECT * FROM `contact_loan_notes` as cn JOIN contact as c ON cn.contact_id = c.contact_id WHERE cn.loan_id='" . $loan_id . "' ORDER BY id DESC");
		$data['contact_notes_users'] = '';
		if ($sql->num_rows() > 0) {
			$contact_notes_fetch1 = $sql->result();
			foreach ($contact_notes_fetch1 as $row) {

				$users = $this->User_model->query("Select * from user where id = '" . $row->user_id . "'");
				$fetch_user = $users->result();
				$user_name = $fetch_user[0]->fname . ' ' . $fetch_user[0]->middle_name . ' ' . $fetch_user[0]->lname;

				$contact_notes_users[] = array(
					"id" => $row->id,
					"contact_id" => $row->contact_id,
					"notes_date" => $row->notes_date,
					"notes_text" => $row->notes_text,
					"c_name" => $row->contact_firstname . ' ' . $row->contact_middlename . ' ' . $row->contact_lastname,
					"entry_by" => $row->entry_by,
					"loan_note_type" => $row->loan_note_type,
					"com_type" => $row->com_type,

				);

			}

			$data['contact_notes_users'] = $contact_notes_users;
		}

		$data['borrower_id'] = $loan_id;
		$data['id'] = $loan_id;
		$data['id'] = $borrower_id;
		$data['property_addresss'] = $this->loan_data_by_address($talimar_loan);
		$data['talimar_loan'] = $talimar_loan;
		$data['loan_id'] = $loan_id;
		$this->load->view('template_files/loan_portfolio/modal_loan_notes', $data);
	}

	public function modal_diligence_materials(){
		error_reporting(0);
		$data = $this->config_variable();
		$loan_id 	= $this->input->post('loan_id');
		$loan_data  = $this->loan_data_by_id($loan_id);
		$talimar_loan = $loan_data->talimar_loan;
		$borrower_id = $loan_data->borrower;

		$fetch_datas = array();
		$fetch_all_input_datas1 = $this->User_model->select_where('input_datas', array('talimar_loan' => $talimar_loan));
		if ($fetch_all_input_datas1->num_rows() > 0) {
			$fetch_all_input_datas = $fetch_all_input_datas1->result();
			if($fetch_all_input_datas){
				foreach ($fetch_all_input_datas as $key => $row) {
					$fetch_datas[$row->page][$row->input_name] = $row->value;
				}
			}			
		}
		$data['fetch_all_input_datas'] = $fetch_datas;

		$data['fetch_underwriting1'] = '';
		$fetch_underwriting = $this->User_model->select_where('underwriting', array('talimar_loan' => $talimar_loan));
		if ($fetch_underwriting->num_rows() > 0) {
			$fetch_underwriting1 = $fetch_underwriting->result();
			$data['fetch_underwriting1'] = $fetch_underwriting1;
		}

		$data['borrower_id'] = $loan_id;
		$data['id'] = $loan_id;
		$data['id'] = $borrower_id;
		$data['property_addresss'] = $this->loan_data_by_address($talimar_loan);
		$data['talimar_loan'] = $talimar_loan;
		$data['loan_id'] = $loan_id;
		$this->load->view('template_files/loan_portfolio/modal_diligence_materials', $data);
	}

	public function modal_loan_servicing(){
		error_reporting(0);
		$data = $this->config_variable();
		$loan_id 	= $this->input->post('loan_id');
		$loan_data  = $this->loan_data_by_id($loan_id);
		$talimar_loan = $loan_data->talimar_loan;
		$borrower_id = $loan_data->borrower;


		$loan_servicingRec = $this->User_model->select_where('loan_servicing', array('talimar_loan' => $talimar_loan));
		$loan_servicingRec = $loan_servicingRec->row_array();
		$data['loan_servicing'] = $loan_servicingRec;

		$fetch_loan_data = $this->User_model->select_where('loan', array('talimar_loan' => $talimar_loan));
		$fetch_loan_result = $fetch_loan_data->result();
		$data['fetch_loan_result'] = $fetch_loan_result;

		/*E-mail Scripts*/
		$fetch_impound_account = $this->User_model->select_where_asc('impound_account', array('talimar_loan' => $talimar_loan), 'position');
		if ($fetch_impound_account->num_rows() > 0) {
			$data['fetch_impound_account'] = $fetch_impound_account->result();
		}

		$fetch = array();
		$fetch['id'] = $loan_id;
		$fetch_loan_data = $this->User_model->select_where('loan', $fetch);
		$data['fetch_loan_result'] = $fetch_loan_data->result();

		$outlook_query = $this->User_model->query("SELECT * FROM loan as l JOIN loan_property as lp on l.talimar_loan = lp.talimar_loan WHERE l.talimar_loan = '".$talimar_loan."'");

		$outlook_query = $outlook_query->result();
		$data['outlook_property'] = $outlook_query;
		foreach ($outlook_query as $row) {
			$outlook_borrower = $this->User_model->query("Select * from borrower_data where id = '" . $row->borrower . "'");
			$data['outlook_borrower'] = $outlook_borrower->result();
		}

		$loan_servicing_data = $this->User_model->select_where('loan_servicing', array('talimar_loan' => $talimar_loan));
		if ($loan_servicing_data->num_rows() > 0) {
			$loan_servicing_data = $loan_servicing_data->result();
			$data['loan_servicing_data'] = $loan_servicing_data;
		}

		$fetch_closing_statement_items1 = $this->User_model->select_where_asc('closing_statement_items', array('talimar_loan' => $talimar_loan), 'hud');

		if ($fetch_closing_statement_items1->num_rows() > 0) {
			$fetch_closing_statement_items1 = $fetch_closing_statement_items1->result();
			$data['fetch_closing_statement_items_data'] = $fetch_closing_statement_items1;
		}

		$sql_draws = "SELECT * FROM  `loan_draws` WHERE talimar_loan =  '".$talimar_loan."' ORDER BY convert(`draws`, decimal) ASC ";
		$data['loan_draws_data'] = '';
		$loan_draws_data = $this->User_model->query($sql_draws);
		if ($loan_draws_data->num_rows() > 0) {
			$loan_draws_data = $loan_draws_data->result();
			$data['loan_draws_data'] = $loan_draws_data;
		}

		$loan_borrower_data = $this->User_model->query("SELECT bd.b_name, bc.contact_id,c.contact_firstname , c.contact_lastname, c.contact_email  FROM borrower_data as bd JOIN borrower_contact as bc JOIN contact as c ON bd.id = bc.borrower_id AND bc.contact_id = c.contact_id WHERE bd.id = '".$borrower_id."'");


		if ($loan_borrower_data->num_rows() > 0) {

			$loan_borrower_result = $loan_borrower_data->row();
			$data['draws_borrower_name'] = $loan_borrower_result->b_name;
			$data['draws_contact_name'] = $loan_borrower_result->contact_firstname . ' ' . $loan_borrower_result->contact_lastname;

			$data['draws_contact_first_name'] = $loan_borrower_result->contact_firstname;
			$data['draws_contact_emailssss'] = $loan_borrower_result->contact_email;
		}

		$outlook_query = $this->User_model->query("SELECT * FROM loan as l JOIN loan_property as lp on l.talimar_loan = lp.talimar_loan WHERE l.talimar_loan = '".$talimar_loan."'");

		$outlook_query = $outlook_query->result();
		$data['outlook_property'] = $outlook_query;

		$loan_servicing_data = $this->User_model->select_where('loan_servicing', array('talimar_loan' => $talimar_loan));
		if ($loan_servicing_data->num_rows() > 0) {
			$loan_servicing_data = $loan_servicing_data->result();
			$data['loan_servicing_data'] = $loan_servicing_data;
		}

		$fetch_escrow_data = $this->User_model->select_where('loan_escrow', array('talimar_loan' => $talimar_loan));
		if ($fetch_escrow_data->num_rows() > 0) {
			$fetch_escrow_data = $fetch_escrow_data->result();

			foreach ($fetch_escrow_data as $row) {

				$q = "select * from contact where contact_firstname ='" . $row->first_name . "' AND contact_lastname = '" . $row->last_name . "'";
				$fetch_contact = $this->User_model->query($q);
				$fetch_contact = $fetch_contact->result();

				$c_idd = $fetch_contact[0]->contact_id;

				$sql = "select contact_id,contact_firstname,contact_middlename,contact_lastname from contact where contact_id = '" . $row->first_name . "'";
				$fetch_contact = $this->User_model->query($sql);
				$fetch_contact = $fetch_contact->result();

				$contact = $fetch_contact ? $fetch_contact[0]->contact_firstname . ' ' . $fetch_contact[0]->contact_middlename . ' ' . $fetch_contact[0]->contact_lastname : '0';
				$contact1 = $fetch_contact[0]->contact_firstname;
				$c_id = $fetch_contact[0]->contact_id;

				$escrow_contact_name = array(
					"id" => $row->id,
					"talimar_loan" => $row->talimar_loan,
					"first_name" => $row->first_name,
					"last_name" => $row->last_name,
					"company" => $row->company,
					"escrow" => $row->escrow,
					"phone" => $row->phone,
					"address" => $row->address,
					"address_b" => $row->address_b,
					"city" => $row->city,
					"state" => $row->state,
					"zip" => $row->zip,
					"email" => $row->email,
					"first_payment" => $row->first_payment_close,
					"net_fund" => $row->net_fund,
					"tax_service" => $row->tax_service,
					"tax_service" => $row->tax_service,
					"record_assigment_close" => $row->record_assigment_close,
					"expiration" => $row->expiration,
					"instruction" => $row->instruction,
					"t_user_id" => $row->t_user_id,
					"full_name" => $contact,
					"full_fname" => $contact1,
					"c_id" => $c_id,
					"c_idd" => $c_idd,
				);
			}
			$data['escrow_contact_name'] = $escrow_contact_name;
		}

		//Fetch loan_escrow tab...
		$fetch_escrow_data = $this->User_model->select_where('loan_escrow', array('talimar_loan' => $talimar_loan));
		if ($fetch_escrow_data->num_rows() > 0) {
			$fetch_escrow_data = $fetch_escrow_data->result();
			$data['fetch_escrow_data'] = $fetch_escrow_data;

		}

		$fetch_property_home = $this->User_model->select_where_asc('property_home', array('talimar_loan' => $talimar_loan), 'primary_property');
		if ($fetch_property_home->num_rows() > 0) {
			$property_home = $fetch_property_home->result();
			// echo'<pre>';
			// print_r($property_home);
			// echo'</pre>';
			$data['fetch_property_home'] = $fetch_property_home->result();
		}
		$data['primary_loan_property'] = '';
		if (isset($property_home)) {
			
			foreach ($property_home as $row) {
				
				$fetch_table_loan_property = $this->User_model->select_where('loan_property', array('property_home_id' => $row->id));

				if ($fetch_table_loan_property->num_rows() > 0) {
					$loan_property_result[$row->id] = $fetch_table_loan_property->result();
					// For primary Property

					$data['property_of_home_id'][$row->id] = $fetch_table_loan_property->row();

					if ($row->primary_property == 1) {
						$data['primary_loan_property'] = $fetch_table_loan_property->row();
					}
				}

				// fetch loan_property_taxes...
				$loan_property_taxes = $this->User_model->query("select * from loan_property_taxes where talimar_loan = '" . $row->talimar_loan . "' AND property_home_id = '" . $row->id . "'");
				$loan_property_taxes = $loan_property_taxes->result();
				foreach ($loan_property_taxes as $tax) {

					$talimar_loan = $tax->talimar_loan;
					$property_home_id = $tax->property_home_id;
					$loan_id = $tax->loan_id;
					$annual_tax = $tax->annual_tax;
					$mello_roos = $tax->mello_roos;
					$delinquent = $tax->delinquent;
					$delinquent_amount = $tax->delinquent_amount;
					$post_delinquent = $tax->post_delinquent;
					$post_delinquent_amount = $tax->post_delinquent_amount;
					$tax_assessor_phone = $tax->tax_assessor_phone;

					$loan_property_taxes_data = array(
						"talimar_loan" => $talimar_loan,
						"property_home_id" => $property_home_id,
						"loan_id" => $loan_id,
						"annual_tax" => $annual_tax,
						"mello_roos" => $mello_roos,
						"delinquent" => $delinquent,
						"delinquent_amount" => $delinquent_amount,
						"post_delinquent" => $post_delinquent,
						"post_delinquent_amount" => $post_delinquent_amount,
						"tax_assessor_phone" => $tax_assessor_phone,

					);
				}

				$data['property_taxes'] = $loan_property_taxes_data;
				$recording_informationn = $this->User_model->select_where('loan_recording_information', array('talimar_loan' => $talimar_loan));
				$recording_information_dataa = $recording_informationn->result();
				$data['recording_information'] = $recording_information_dataa;

				foreach ($data['recording_information'] as $rt) {

					$data['rn'] = $rt->record_number;
					$data['rd'] = $rt->recorded_date;
					$data['ro'] = $rt->record_option;
				}

				//-------------------Fetch eucmbrances data---------------
				$fetch_ecumbrances = $this->User_model->query("SELECT * FROM encumbrances WHERE talimar_loan = '" . $talimar_loan_number . "' AND property_home_id = '" . $row->id . "' order by proposed_priority");

				$fetch_ecumbrances = $fetch_ecumbrances->result();
				$ecumbrances_data[$row->id] = $fetch_ecumbrances;

				//-------- get senior liens sum from 'encumbrances' table -------
				$fetch_property_senior_liens = $this->User_model->query("SELECT SUM(current_balance) as current_balance FROM encumbrances WHERE talimar_loan = '" . $talimar_loan_number . "' AND property_home_id = '" . $row->id . "'  AND existing_priority_to_talimar = 1 ");

				$result_fetch_property_senior_liens = $fetch_property_senior_liens->row();

				$data['property_senior_liens'][$row->id] =

				$result_fetch_property_senior_liens->current_balance;

				$sql = "SELECT COUNT(*) as count_encum, SUM(current_balance) as sum_current_balance, existing_priority_to_talimar, original_balance FROM encumbrances WHERE property_home_id = '" . $row->id . "' AND talimar_loan = '" . $talimar_loan_number . "' AND will_it_remain = 0 ";
				$fetch_ecum_right = $this->User_model->query($sql);
				$fetch_ecum_right = $fetch_ecum_right->row();
				$data['ecum_right_data'][$row->id]['count_encum'] = $fetch_ecum_right->count_encum ? $fetch_ecum_right->count_encum : 0;
				$data['ecum_right_data'][$row->id]['sum_current_balance'] = $fetch_ecum_right->sum_current_balance ? $fetch_ecum_right->sum_current_balance : 0;
				$data['ecum_right_data'][$row->id]['existing_priority_to_talimar'] = $fetch_ecum_right->existing_priority_to_talimar ? $fetch_ecum_right->existing_priority_to_talimar : 0;
				$data['ecum_right_data'][$row->id]['original_balance'] = $fetch_ecum_right->original_balance ? $fetch_ecum_right->original_balance : 0;

				// Fetch _monthly income
				$fetch_monthly_income = $this->User_model->select_where('monthly_income', array('talimar_loan' => $talimar_loan_number, 'property_home_id' => $row->id));
				if ($fetch_monthly_income->num_rows() > 0) {
					$fetch_monthly_income_result[$row->id] = $fetch_monthly_income->result();
				}

				// Fetch Expense income
				$fetch_monthly_expense = $this->User_model->select_where('monthly_expense', array('talimar_loan' => $talimar_loan_number, 'property_home_id' => $row->id));
				if ($fetch_monthly_expense->num_rows() > 0) {
					$fetch_monthly_expense_result[$row->id] = $fetch_monthly_expense->result();
				}

				// FEtch Comparable_sale data
				$fetch_comparable_sale = $this->User_model->select_where_asc('comparable_sale', array('talimar_loan' => $talimar_loan_number, 'property_home_id' => $row->id), 'id');
				if ($fetch_comparable_sale->num_rows() > 0) {
					$fetch_comparable_sale_result[$row->id] = $fetch_comparable_sale->result();
				}

				// fetch propoerty_input_fields
				$fetch_propoerty_input_fields = $this->User_model->select_where('propoerty_input_fields', array('talimar_loan' => $talimar_loan_number, 'property_home_id' => $row->id));
				if ($fetch_propoerty_input_fields->num_rows() > 0) {
					$fetch_propoerty_input_fields = $fetch_propoerty_input_fields->result();

					foreach ($fetch_propoerty_input_fields as $p_i_f) {
						$data['propoerty_input_fields'][$row->id][$p_i_f->input_field] = $p_i_f->input_value;
						$data['propoerty_input_fieldss'][$row->id][$p_i_f->input_field] = $p_i_f->property_valuation_notes;
					}
				}
				//--------Fetch data from table loan_property_insurance----------//

				$property_loan['talimar_loan'] = $data['modal_talimar_loan'];
				$fetch_table_loan_property_insurance = $this->User_model->select_where('loan_property_insurance', $property_loan);

				$loan_property_insurance_result = $fetch_table_loan_property_insurance->row();
				$data['loan_property_insurance_result'] = $loan_property_insurance_result;

				//--------Fetch data from table loan_property_insurance----------//
				$loan_property_insurance_type = $this->config->item('loan_property_insurance_type');
				foreach ($loan_property_insurance_type as $insurance_key => $insurance_row) {
					$fetch_table_loan_property_insurance = $this->User_model->select_where('loan_property_insurance', array('talimar_loan' => $talimar_loan_number, 'property_home_id' => $row->id, 'insurance_type' => $insurance_key));
					if ($fetch_table_loan_property_insurance->num_rows() > 0) {
						$data['loan_property_insurance'][$row->id][$insurance_key] = $fetch_table_loan_property_insurance->row();
					}
				}

				// fetch loan_property_taxes
				$fetch_loan_property_taxes = $this->User_model->select_where('loan_property_taxes', array('talimar_loan' => $talimar_loan_number, 'property_home_id' => $row->id));

				if ($fetch_loan_property_taxes->num_rows() > 0) {
					$data['loan_property_taxes'][$row->id] = $fetch_loan_property_taxes->row();
				}

				if ($row->primary_property == 1) {
					// fetch loan_property_taxes
					$fetch_primary_loan_property_taxes = $this->User_model->select_where('loan_property_taxes', array('talimar_loan' => $talimar_loan_number, 'property_home_id' => $row->id));

					$data['primary_loan_property_taxes'] = $fetch_primary_loan_property_taxes->row();
				}

				// fetch property_folders
				$fetch_property_folders = $this->User_model->select_where('property_folders', array('talimar_loan' => $talimar_loan_number, 'property_home_id' => $row->id));
				if ($fetch_property_folders->num_rows() > 0) {
					$property_folders_result = $fetch_property_folders->result();
					$data['property_folders'][$row->id] = $property_folders_result;
					foreach ($property_folders_result as $row_p_f) {
						// fetch loan_property_images
						$fetch_loan_property_images = $this->User_model->select_where('loan_property_images', array('talimar_loan' => $talimar_loan_number, 'property_home_id' => $row->id, 'folder_id' => $row_p_f->id));
						if ($fetch_loan_property_images->num_rows() > 0) {
							$data['loan_property_images'][$row->id][$row_p_f->id] = $fetch_loan_property_images->result();
						}
					}
				}

			}

			$data['loan_property_result'] = $loan_property_result;
			$data['ecumbrances_data'] = $ecumbrances_data;
			if (isset($fetch_monthly_income_result)) {
				$data['fetch_monthly_income'] = $fetch_monthly_income_result;
			}

			if (isset($fetch_monthly_expense_result)) {
				$data['fetch_monthly_expense'] = $fetch_monthly_expense_result;
			}

			if (isset($fetch_comparable_sale_result)) {
				$data['fetch_comparable_sale'] = $fetch_comparable_sale_result;
			}

			//--------Fetch data from table loan_property_insurance with insurance type = 1 ----------//

			$property_loan1['talimar_loan'] = $data['modal_talimar_loan'];
			$property_loan1['insurance_type'] = 1;

			$fetch_table_loan_property_insurance1 = $this->User_model->query("SELECT sum(annual_insurance_payment) as annual_insurance_payment FROM loan_property_insurance WHERE talimar_loan = '" . $data['modal_talimar_loan'] . "' AND insurance_type = 1 ");

			// annual_insurance_payment
			$loan_property_insurance_result1 = $fetch_table_loan_property_insurance1->row();
			$data['loan_property_insurance_result_with_1'] = $loan_property_insurance_result1;

			//------------------------------------------------------//

			//--------Fetch data from table loan_property_insurance with insurance type = 2 ----------//

			$property_loan2['talimar_loan'] = $data['modal_talimar_loan'];
			$property_loan2['insurance_type'] = 2;

			// $fetch_table_loan_property_insurance2 	= $this->User_model->select_where('loan_property_insurance',$property_loan2);
			$fetch_table_loan_property_insurance2 = $this->User_model->query("SELECT sum(annual_insurance_payment) as annual_insurance_payment FROM loan_property_insurance WHERE talimar_loan = '" . $data['modal_talimar_loan'] . "' AND insurance_type = 2 ");

			$loan_property_insurance_result2 = $fetch_table_loan_property_insurance2->row();
			$data['loan_property_insurance_result_with_2'] = $loan_property_insurance_result2;

			$sql = "SELECT SUM(current_value) as sum_current_value, SUM(underwriting_value) as sum_underwriting_value FROM loan_property WHERE talimar_loan = '" . $data['modal_talimar_loan'] . "'";

			$fetch_sum = $this->User_model->query($sql);
			if ($fetch_sum->num_rows() > 0) {
				$fetch_sum = $fetch_sum->row();
				$data['loan_property_sum_current_value'] = $fetch_sum->sum_current_value;
				$data['loan_property_sum_underwriting_value'] = $fetch_sum->sum_underwriting_value;
			}

			//------------------------------------------------------//

			//--------Fetch data from table loan_property_insurance with insurance type = 3 ----------//

			$property_loan3['talimar_loan'] = $data['modal_talimar_loan'];
			$property_loan3['insurance_type'] = 3;

			// $fetch_table_loan_property_insurance3 	= $this->User_model->select_where('loan_property_insurance',$property_loan3);
			$fetch_table_loan_property_insurance3 = $this->User_model->query("SELECT sum(annual_insurance_payment) as annual_insurance_payment FROM loan_property_insurance WHERE talimar_loan = '" . $data['modal_talimar_loan'] . "' AND insurance_type = 3 ");

			$loan_property_insurance_result3 = $fetch_table_loan_property_insurance3->row();
			$data['loan_property_insurance_result_with_3'] = $loan_property_insurance_result3;

			//------------------------------------------------------//

		}

		$result = $this->User_model->select_star('vendors');
		$result_new = $result->result();
		$data['all_vendor_data'] = $result_new;

		$fetch_closing_statement_items1 = $this->User_model->select_where_asc('closing_statement_items', array('talimar_loan' => $talimar_loan), 'hud');

		if ($fetch_closing_statement_items1->num_rows() > 0) {
			$fetch_closing_statement_items1 = $fetch_closing_statement_items1->result();
			$data['fetch_closing_statement_items_data'] = $fetch_closing_statement_items1;
		}

		$fetch_loan_borrower = $this->User_model->select_where('borrower_data', array('id' => $borrower_id));
		$data['fetch_loan_borrower'] = $fetch_loan_borrower->result();

		$affiliated_parties = $this->User_model->select_where('loan_affiliated_parties', array('talimar_loan' => $talimar_loan));
		if ($affiliated_parties->num_rows() > 0) {
			$data['affiliated_parties_data'] = $affiliated_parties->result();
		} else {

			$data['affiliated_parties_data'] = '';
		}

		$sql = "SELECT * FROM `loan_assigment` WHERE `talimar_loan` = '".$talimar_loan."' ORDER BY `position` ASC";
		$loan_assigment_data = $this->User_model->query($sql);
		$loan_assigment_data = $loan_assigment_data->result();

		$loan_assigmentdata = array();
		foreach ($loan_assigment_data as $keyEID => $row) {

			$lender_id = $row->lender_name;
			$fetch_investor_of_assigmnet = $this->User_model->select_where('investor', array('id' => $lender_id));
			if ($fetch_investor_of_assigmnet->num_rows() > 0) {
				$investor_of_assigmnet = $fetch_investor_of_assigmnet->row();
				$data['all_lender_name_of_assigment'][] = $investor_of_assigmnet->name;
			}

			$sql = "SELECT * FROM lender_contact JOIN contact ON lender_contact.contact_id = contact.contact_id WHERE lender_contact.lender_id = '" . $lender_id . "' ";
			$fetch_lender_email_contact1 = $this->User_model->query($sql);
			if ($fetch_lender_email_contact1->num_rows() > 0) {
				$fetch_lender_email_contact = $fetch_lender_email_contact1->result();
				foreach ($fetch_lender_email_contact as $contact_data) {
					$data['email_contact'][$lender_id][] = array(
						"contact_name" => $contact_data->contact_firstname . ' ' . $contact_data->contact_middlename . ' ' . $contact_data->contact_lastname,
						"title" => $contact_data->c_title,
						"email" => $contact_data->contact_email,
						"phone" => $contact_data->contact_phone,
						"contact_id" => $contact_data->contact_id,
					);
				}
			}
			if(!empty($talimar_loan)){		
				
				//call to signNo API
				$EnvolopID = $this->User_model->query("SELECT * FROM `signnow_api_doc` WHERE talimar_loan = '".$talimar_loan."' AND lender_id = '" . $lender_id . "' ORDER BY id DESC");			

				if($EnvolopID->num_rows() > 0){			
					
					$EnvolopID = $EnvolopID->row();			
					$DocumentData = $this->GetDocusignAPIDocs($EnvolopID->documentID);			

					if(!empty($DocumentData->status))
					{
						$row->EnvolopStatus = $DocumentData->status;
					}
					else
					{
						$row->EnvolopStatus = "Not Ready";
					}
				}
				else
				{					
					$row->EnvolopStatus = "Not Ready";
				}
				$loan_assigmentdata[$keyEID] = $row;
			}
		}
		$data['loan_assigment_data'] = $loan_assigmentdata;


		$loan_assigment_data = $this->User_model->select_where_asc('loan_assigment', array('talimar_loan' => $talimar_loan), 'position');
		$data['count_row_assigment'] = $loan_assigment_data->num_rows();
		$loan_assigment_data = $loan_assigment_data->result();
		foreach ($loan_assigment_data as $keyEID => $row) {

			$lender_id = $row->lender_name;

			$fetch_investor_of_assigmnet = $this->User_model->select_where('investor', array('id' => $lender_id));
			if ($fetch_investor_of_assigmnet->num_rows() > 0) {
				$investor_of_assigmnet = $fetch_investor_of_assigmnet->row();
				$data['all_lender_name_of_assigment'][] = $investor_of_assigmnet->name;
			}

			$sql = "SELECT * FROM lender_contact JOIN contact ON lender_contact.contact_id = contact.contact_id WHERE lender_contact.lender_id = '" . $lender_id . "' ";
			$fetch_lender_email_contact1 = $this->User_model->query($sql);
			if ($fetch_lender_email_contact1->num_rows() > 0) {
				$fetch_lender_email_contact = $fetch_lender_email_contact1->result();
				foreach ($fetch_lender_email_contact as $contact_data) {
					$data['email_contact'][$lender_id][] = array(
						"contact_name" => $contact_data->contact_firstname . ' ' . $contact_data->contact_middlename . ' ' . $contact_data->contact_lastname,
						"title" => $contact_data->c_title,
						"email" => $contact_data->contact_email,
						"phone" => $contact_data->contact_phone,
						"contact_id" => $contact_data->contact_id,
					);
				}
			}

			if(!empty($row->talimar_loan)){
			
				
				//call to signNo API
				$EnvolopID = $this->User_model->query("SELECT * FROM `signnow_api_doc` WHERE talimar_loan = '".$row->talimar_loan."' AND lender_id = '" . $lender_id . "' ORDER BY id DESC");

			

				if($EnvolopID->num_rows() > 0){
					
					
					$EnvolopID = $EnvolopID->row();

			
					$DocumentData = $this->GetDocusignAPIDocs($EnvolopID->documentID);
				

					if(!empty($DocumentData->status))
					{
						$row->EnvolopStatus = $DocumentData->status;
					}
					else
					{
						$row->EnvolopStatus = "Not Ready";
					}
				}
				else
				{
					
					$row->EnvolopStatus = "Not Ready";
				}
				

				$loan_assigment_data[$keyEID] = $row;
			}
		}
		/*E-mail Scripts*/

		$data['borrower_id'] = $loan_id;
		$data['id'] = $loan_id;
		$data['id'] = $borrower_id;
		$data['property_addresss'] = $this->loan_data_by_address($talimar_loan);
		$data['talimar_loan'] = $talimar_loan;
		$data['loan_id'] = $loan_id;
		$this->load->view('template_files/loan_portfolio/modal_loan_servicing', $data);
	}
	public function wholeSaleDeal(){
		error_reporting(0);
		$data 						= $this->config_variable();
		$loan_id 					= $this->input->post('loan_id');
		$loan_data 					= $this->loan_data_by_id($loan_id);
		$talimar_loan 				= $loan_data->talimar_loan;
		$borrower_id 				= $loan_data->borrower;
		$data['STATE_USA'] 			= $this->config->item('STATE_USA'); 
		$data['borrower_id'] 		= $borrower_id;
		$data['property_addresss'] 	= $this->loan_data_by_address($talimar_loan);
		$data['talimar_loan'] 		= $talimar_loan;
		$data['loan_id'] 			= $loan_id;
		$sql = "SELECT * FROM loan_wholesale_deal  WHERE loan_id = '" . $loan_id . "' ";
		$fetch_wholesale_deal 		= $this->User_model->query($sql);
		$fetch_wholesale_deal		= $fetch_wholesale_deal->result();
		$data['fetch_wholesale_deal']= $fetch_wholesale_deal;
		$wholesale_id 				= $fetch_wholesale_deal[0]->wholesale_id;
		$sql = "SELECT * FROM loan_wholesale_Interest_list  WHERE loan_id = '" . $loan_id . "' and wholesale_id= '" . $wholesale_id . "' ";
		$fetch_interest_deal 		= $this->User_model->query($sql);
		$fetch_interest_deal		= $fetch_interest_deal->result();
		$data['fetch_interest_deal']= $fetch_interest_deal;
		$this->load->view('template_files/loan_portfolio/model_wholesale_deal', $data);
	}
	public function delete_wholesale_interest_row(){
		$interested_id 					= $this->input->post('interested_id');
		$delete_data['interest_id'] 	= $interested_id;
		$this->User_model->delete('loan_wholesale_Interest_list', $delete_data);

	}
	public function fetch_contact_details() {
		if(!empty($_POST['contact_list'])){
			$contact_list=$_POST['contact_list'];
			$contact_list = trim($contact_list,",");
			if(!empty($contact_list)){
				$sql="SELECT contact_id,contact_phone,contact_email,contact_firstname,contact_middlename,contact_lastname FROM contact WHERE contact_id NOT IN ($contact_list) ORDER BY contact_id ASC ";
			}else{
				$sql="SELECT contact_id,contact_phone,contact_email,contact_firstname,contact_middlename,contact_lastname FROM contact ORDER BY contact_id ASC ";
			}
		}else{
			$sql="SELECT contact_id,contact_phone,contact_email,contact_firstname,contact_middlename,contact_lastname FROM contact ORDER BY contact_id ASC ";
		}		
		$fetch_all_contact = $this->User_model->query($sql);
		$fetch_all_contact = $fetch_all_contact->result();
		echo '<option value="">Select One</option>';
		foreach ($fetch_all_contact as $key => $values_appendw) {

			echo '<option value=' . $values_appendw->contact_id . ' phone='.$values_appendw->contact_phone.' email='.$values_appendw->contact_email.'>' . $values_appendw->contact_firstname . ' ' . $values_appendw->contact_middlename . ' ' . $values_appendw->contact_lastname . '</option>';

		}

	}
	public function loanLenderPayment(){
		error_reporting(0);
		$data 						= $this->config_variable();
		$loan_id 					= $this->input->post('loan_id');
		$lender_id 					= $this->input->post('lender_id');
		$loan_data 					= $this->loan_data_by_id($loan_id);
		$talimar_loan 				= $loan_data->talimar_loan;
		$data['borrower_id'] 		= $loan_data->borrower; 
		$data['property_addresss'] 	= $this->loan_data_by_address($talimar_loan);
		$data['talimar_loan'] 		= $talimar_loan;
		$data['loan_id'] 			= $loan_id;
		$sql = "SELECT * FROM loan_assigment  WHERE talimar_loan = '" . $talimar_loan . "' AND lender_name='" . $lender_id . "' ";
		$fetchLenderPayment 		= $this->User_model->query($sql);
		$fetchLenderPayment			= $fetchLenderPayment->result();
		$investor_all_data = $this->User_model->select_star_asc('investor', 'name');
		$investor_all_data = $investor_all_data->result();
		$data['investor_all_data'] = $investor_all_data;
		$data['lenderPayment']		= $fetchLenderPayment;
		
		$this->load->view('template_files/loan_portfolio/loan_lender_payment_model', $data);
	}
	public function conditionPageSection(){
		error_reporting(0);
		$data 						= $this->config_variable();
		$loan_id 					= $this->input->post('loan_id');
		$loan_data 					= $this->loan_data_by_id($loan_id);
		$talimar_loan 				= $loan_data->talimar_loan;
		$borrower_id 				= $loan_data->borrower;
		$data['borrower_id'] 		= $borrower_id;
		$data['property_addresss'] 	= $this->loan_data_by_address($talimar_loan);
		$data['talimar_loan'] 		= $talimar_loan;
		$data['loan_id'] 			= $loan_id;
		$sql = "SELECT * FROM loan_condition_page  WHERE loan_id = '" . $loan_id . "' ";
		$fetch_condition_page 		= $this->User_model->query($sql);
		$fetch_condition_page		= $fetch_condition_page->result();
		if(!empty($fetch_condition_page)){
			$data['fetch_condition_page']= $fetch_condition_page;	
		}else{
			$data['fetch_condition_page']= array();	
		}
		$this->load->view('template_files/loan_portfolio/model_condition_page', $data);
	}
	public function delete_condition_page_row(){
		$condition_id 					= $this->input->post('condition_id');
		$delete_data['codition_id'] 	= $condition_id;
		$this->User_model->delete('loan_condition_page', $delete_data);
	}

	public function modal_mytestmodal()
	{
		echo "check";
	}
}