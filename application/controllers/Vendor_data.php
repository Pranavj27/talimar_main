<?php
class Vendor_data extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->model('User_model');
		$this->load->library('Aws3','aws3');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');

		if ($this->session->userdata('t_user_id') != '') {

		} else {
			redirect(base_url() . "home", 'refresh');
		}
		
		ini_set('memory_limit', '-1');
	}

	public function index() {
		//error_reporting(0);
		$vendor_name = $this->uri->segment(2);
		$vendor_id = $this->uri->segment(3);
		$data['user_role'] = $this->session->userdata('user_role');
		$data['vendor_option'] = $this->uri->segment(3);

		if ($vendor_name == 'id') {

			$result = $this->User_model->query('SELECT * FROM vendors ORDER BY vendor_name ASC');
			//$this->User_model->select_star('vendors');
			$result_new = $result->result();

			foreach ($result_new as $row) {
				$fetch_search_option[] = array(
					'id' => $row->vendor_id,
					'text' => $row->vendor_name,
				);
			}
			if ($result_new) {

				$data['all_vendor_data'] = $fetch_search_option;
			} else {
				$data['all_vendor_data'] = '';

			}

			// query to display result of particular vendor ...

			$fetch_vendors_data = $this->User_model->query("SELECT * FROM vendors WHERE vendor_id = '" . $vendor_id . "' ");

			$fetch_vendors_data = $fetch_vendors_data->row();

			$data['vendor'] = $fetch_vendors_data;
         
          $fetch_vendors_contact = $this->User_model->query("SELECT * FROM vendor_contact WHERE ref_vendor_id = '" . $vendor_id . "' ");

		 $fetch_vendors_contact = $fetch_vendors_contact->result();
      foreach ($fetch_vendors_contact as $key => $value) {

   
		 $fetch_vendors_contacts = $this->User_model->query("SELECT * FROM contact WHERE contact_id = '" . $value->contact_id . "' ");
		 if($fetch_vendors_contacts->num_rows()>0){
			$fetch_vendors_contactsss = $fetch_vendors_contacts->result();
			
			$data['vendor_contact'][] = array(

											"id"=>$value->id,	
											"contact_id"=>$fetch_vendors_contactsss[0]->contact_id,	
											"contact_title"=>$value->contact_title,	
											"contact_department"=>$value->contact_department,	
										
											"contact_phone"=>$fetch_vendors_contactsss[0]->contact_phone,	
											"primary_option"=>$fetch_vendors_contactsss[0]->primary_option,	
											"alter_phone"=>$fetch_vendors_contactsss[0]->alter_phone,	
											"alter_option"=>$fetch_vendors_contactsss[0]->alter_option,	
											"contact_email"=>$fetch_vendors_contactsss[0]->contact_email,	
											"pemail_type"=>$fetch_vendors_contactsss[0]->pemail_type,	
											"contact_email2"=>$fetch_vendors_contactsss[0]->contact_email2,	
											"aemail_type"=>$fetch_vendors_contactsss[0]->aemail_type,	
											 "contact_firstname"=>$fetch_vendors_contactsss[0]->contact_firstname,	
											 "contact_lastname"=>$fetch_vendors_contactsss[0]->contact_lastname,	
											"contact_middlename"=>$fetch_vendors_contactsss[0]->contact_middlename,	
									
											 );

			}else{
				$data['vendor_contact'][] ='';

				}



			}	

			$data['search_by'] = '1';

		} else if ($vendor_name == 'role') {

			$result_new = array(

				1 => 'Loan Servicer',
				2 => 'Fund Control Administrator',
				3 => 'Escrow Company',
				4 => 'Title Company',
				5 => 'IRA Custodian',
			);

			$fetch_search_option[] = array(
				'id' => 1,
				'text' => 'Loan Servicer',
			);
			$fetch_search_option[] = array(
				'id' => 2,
				'text' => 'Fund Control Administrator',
			);
			$fetch_search_option[] = array(
				'id' => 3,
				'text' => 'Escrow Company',
			);
			$fetch_search_option[] = array(
				'id' => 4,
				'text' => 'Title Company',
			);
			$fetch_search_option[] = array(
				'id' => 5,
				'text' => 'IRA Custodian',
			);
			//$data['all_vendor_data'] = $fetch_search_option;
			$data['all_vendor_data'] = $this->config->item('vendor_type');

			$fetch_vendors_roles = $this->User_model->query("SELECT * FROM vendors WHERE vendor_role LIKE '%" . $vendor_id . "%' ");

			$fetch_vendors_roles = $fetch_vendors_roles->result();

			$data['roles'] = $fetch_vendors_roles;
			$data['search_by'] = '2';

		} else {

			$result = $this->User_model->query('SELECT * FROM vendors ORDER BY vendor_name ASC');
			//$this->User_model->select_star('vendors');
			$result_new = $result->result();

			foreach ($result_new as $row) {
				$fetch_search_option[] = array(
					'id' => $row->vendor_id,
					'text' => $row->vendor_name,
				);
			}
			if ($result_new) {

				$data['all_vendor_data'] = $fetch_search_option;
			} else {
				$data['all_vendor_data'] = '';

			}
			$data['search_by'] = '1';

		}

		// fetch all contact names...
		$fetch_contact = $this->User_model->select_star('contact');
		$fetch_contact = $fetch_contact->result();
		$data['fetch_all_contact'] = $fetch_contact;
		$data['content'] = $this->load->view('vendor_data/index', $data, true);

		$this->load->view('template_files/template', $data);

	}

	public function vendor_table() {
		error_reporting(0);
		$data['user_role'] = $this->session->userdata('user_role');

		$vendor_type = $this->input->post('vendor_type');
		// $data['type'] = $vendor_type;
		if ($vendor_type != '') {

			$where['vendor_type'] = $vendor_type;
			$fetch_vendors_data = $this->User_model->select_where('vendors', $where);
		} else {

			$fetch_vendors_data = $this->User_model->query('SELECT * FROM vendors ORDER BY vendor_name ASC');
			//$this->User_model->select_star('vendors');

		}

		$data['vendor_type'] = $vendor_type;

		if ($fetch_vendors_data->num_rows() > 0) {

			$fetch_vendors = $fetch_vendors_data->result();

			//$data['fetch_vendors_data'] = $fetch_vendors;

			foreach ($fetch_vendors as $key => $row) {

				$fetch_vendors_contact = $this->User_model->query("SELECT * FROM vendor_contact WHERE ref_vendor_id = '" . $row->vendor_id . "' ");

				$fetch_vendors_contact = $fetch_vendors_contact->result();
				
			   	$fetch_vendors_contacts = $this->User_model->query("SELECT * FROM contact WHERE contact_id = '" . $fetch_vendors_contact[0]->contact_id . "' ");
			   	 	
				$fetch_vendors_contacts = $fetch_vendors_contacts->result();
				$data['fetch_vendors_data'][] =array(

															"vendor_id"=> $row->vendor_id,
															"vendor_name" =>$row->vendor_name,
															 "vendor_type"=>$row->vendor_type,
															 "vendor_contact"=>$fetch_vendors_contact[0]->contact_id,
															 "vendor_contact_phone"=>$fetch_vendors_contacts[0]->contact_phone,
															 "vendor_contact_email"=>$fetch_vendors_contacts[0]->contact_email,



														);

			         
			}


		}
		$result = $this->User_model->query('SELECT * FROM vendors ORDER BY vendor_name ASC');
		//$this->User_model->select_star('vendors');
		$result_new = $result->result();


		$data['all_vendor_data_result'] = $result_new;
		$data['content'] = $this->load->view('vendor_data/vendor_table', $data, true);
		$this->load->view('template_files/template', $data);
	}

	public function vendor_data_view() {
error_reporting(0);
		$vendor_name = $this->uri->segment(2) ? $this->uri->segment(2) : 'role';
		$vendor_id = $this->uri->segment(3);
		$data['user_role'] = $this->session->userdata('user_role');
		$data['vendor_option'] = $this->uri->segment(3);

		if ($vendor_name == 'id') {
			$data['selected_vender_id'] = $vendor_id;
			$result = $this->User_model->query('SELECT * FROM vendors ORDER BY vendor_name ASC');
			//$this->User_model->select_star('vendors');
			$result_new = $result->result();

			$fetch_search_option[] = array(
				'id' => 1,
				'text' => 'Loan Servicer',
			);
			$fetch_search_option[] = array(
				'id' => 2,
				'text' => 'Fund Control Administrator',
			);
			$fetch_search_option[] = array(
				'id' => 3,
				'text' => 'Escrow Company',
			);
			$fetch_search_option[] = array(
				'id' => 4,
				'text' => 'Title Company',
			);
			$fetch_search_option[] = array(
				'id' => 5,
				'text' => 'IRA Custodian',
			);
			$data['all_vendor_data'] = $fetch_search_option;

			// query to display result of particular vendor ...

			$fetch_vendors_data = $this->User_model->query("SELECT * FROM vendors WHERE vendor_id = '" . $vendor_id . "' ");

			$fetch_vendors_data = $fetch_vendors_data->row();

			$data['vendor'] = $fetch_vendors_data;

     $fetch_vendors_contact = $this->User_model->query("SELECT * FROM vendor_contact WHERE ref_vendor_id = '" . $vendor_id . "' ");

		 $fetch_vendors_contact = $fetch_vendors_contact->result();
      foreach ($fetch_vendors_contact as $key => $value) {

   
		 $fetch_vendors_contacts = $this->User_model->query("SELECT * FROM contact WHERE contact_id = '" . $value->contact_id . "' ");
		 if($fetch_vendors_contacts->num_rows()>0){
			$fetch_vendors_contactsss = $fetch_vendors_contacts->result();
			
			$data['vendor_contact'][] = array(

											"id"=>$value->id,	
											"contact_id"=>$fetch_vendors_contactsss[0]->contact_id,	
											"contact_title"=>$value->contact_title,	
											"contact_department"=>$value->contact_department,	
										
											"contact_phone"=>$fetch_vendors_contactsss[0]->contact_phone,	
											"primary_option"=>$fetch_vendors_contactsss[0]->primary_option,	
											"alter_phone"=>$fetch_vendors_contactsss[0]->alter_phone,	
											"alter_option"=>$fetch_vendors_contactsss[0]->alter_option,	
											"contact_email"=>$fetch_vendors_contactsss[0]->contact_email,	
											"pemail_type"=>$fetch_vendors_contactsss[0]->pemail_type,	
											"contact_email2"=>$fetch_vendors_contactsss[0]->contact_email2,	
											"aemail_type"=>$fetch_vendors_contactsss[0]->aemail_type,	
											 "contact_firstname"=>$fetch_vendors_contactsss[0]->contact_firstname,	
											 "contact_lastname"=>$fetch_vendors_contactsss[0]->contact_lastname,	
											"contact_middlename"=>$fetch_vendors_contactsss[0]->contact_middlename,	
									
											 );

			}else{
				$data['vendor_contact'][] ='';

				}



			}	

		

			$result = $this->User_model->query('SELECT * FROM vendors ORDER BY vendor_name ASC');
			//$this->User_model->select_star('vendors');
			$result_new = $result->result();
			$data['all_vendor_data_result'] = $result_new;

		} else if ($vendor_name == 'role') {

			$data['selected_vender_role'] = $vendor_id;
			$result_new = array(

				1 => 'Loan Servicer',
				2 => 'Fund Control Administrator',
				3 => 'Escrow Company',
				4 => 'Title Company',
				5 => 'IRA Custodian',
			);

			$fetch_search_option[] = array(
				'id' => 1,
				'text' => 'Loan Servicer',
			);
			$fetch_search_option[] = array(
				'id' => 2,
				'text' => 'Fund Control Administrator',
			);
			$fetch_search_option[] = array(
				'id' => 3,
				'text' => 'Escrow Company',
			);
			$fetch_search_option[] = array(
				'id' => 4,
				'text' => 'Title Company',
			);
			$fetch_search_option[] = array(
				'id' => 5,
				'text' => 'IRA Custodian',
			);
			$data['all_vendor_data'] = $fetch_search_option;

			if ($vendor_id) {
				$fetch_vendors_roles = $this->User_model->query("SELECT * FROM vendors WHERE vendor_role LIKE '%" . $vendor_id . "%' ");

				$fetch_vendors_roles = $fetch_vendors_roles->result();

				$data['roles'] = $fetch_vendors_roles;
				$data['all_vendor_data_result'] = $fetch_vendors_roles;
			} else {
				$result = $this->User_model->query('SELECT * FROM vendors ORDER BY vendor_name ASC'); 
				//$this->User_model->select_star('vendors');
				$result_new = $result->result();

				$data['all_vendor_data_result'] = $result_new;

			}
			// $result 					= $this->User_model->select_star('vendors');
			// $result_new 				= $result->result();

			// foreach($result_new as $row){
			// $fetch_search_option[] = array(
			// 'id' => $row->vendor_id,
			// 'text' => $row->vendor_name
			// );
			// }
			// if($result_new){

			// $data['all_vendor_data_result'] 	= $fetch_vendors_roles;
			// }
			// else
			// {
			// $data['all_vendor_data_result'] 	= '';

			// }

			$data['search_by'] = '';
		} else {

			$result = $this->User_model->query('SELECT * FROM vendors ORDER BY vendor_name ASC');//$this->User_model->select_star('vendors');
			$result_new = $result->result();

			foreach ($result_new as $row) {
				$fetch_search_option[] = array(
					'id' => $row->vendor_id,
					'text' => $row->vendor_name,
				);
			}
			if ($result_new) {

				$data['all_vendor_data'] = $fetch_search_option;
			} else {
				$data['all_vendor_data'] = '';

			}
			$data['search_by'] = '1';

		}

		// fetch all contact names...
		$fetch_contact = $this->User_model->select_star('contact');
		$fetch_contact = $fetch_contact->result();
		$data['fetch_all_contact'] = $fetch_contact;

		$data['content'] = $this->load->view('vendor_data/view', $data, true);

		$this->load->view('template_files/template', $data);
	}

	public function add_vendor_data() {
		error_reporting(0);
		
		if (isset($_POST['save'])) {

			$vendor_id = $this->input->post('vendor_id');
			$data['vendor_name'] 	= $this->input->post('vendor_name');
			$data['v_phone'] 		= $this->input->post('v_phone');
			$data['v_email'] 		= $this->input->post('v_email');
			$data['v_phone_type'] 	= $this->input->post('v_phone_type');
			$data['v_email_type'] 	= $this->input->post('v_email_type');
			
			$data['v_phone1'] 		= $this->input->post('v_phone1');
			$data['v_email1'] 		= $this->input->post('v_email1');
			$data['v_phone_type1'] 	= $this->input->post('v_phone_type1');
			$data['v_email_type1'] 	= $this->input->post('v_email_type1');
			
			$data['email'] 			= $this->input->post('email');
			$data['phone_number'] 	= $this->input->post('phone_number');
			$data['phone_number2'] 	= $this->input->post('phone_number2');
			$data['address'] 		= $this->input->post('address');
			$data['website'] 		= $this->input->post('website');
			$data['attn'] 			= $this->input->post('attn');
			$data['street_address'] = $this->input->post('street_address');
			$data['unit'] 			= $this->input->post('unit');
			$data['city'] 			= $this->input->post('city');
			$data['state'] 			= $this->input->post('state');
			$data['zip'] 			= $this->input->post('zip');
			$data['same_mailing_address'] = $this->input->post('same_mailing_address');

			$data['mailing_attn'] 	= $this->input->post('attn');
			$data['mailing_street_address'] = $this->input->post('mailing_street_address');
			$data['mailing_unit'] 	= $this->input->post('mailing_unit');
			$data['mailing_city'] 	= $this->input->post('mailing_city');
			$data['mailing_state'] 	= $this->input->post('mailing_state');
			$data['mailing_zip'] 	= $this->input->post('mailing_zip');
			$data['tax_id'] 		= $this->input->post('tax_id');
			$data['vendor_type'] 	= $this->input->post('vendor_type');
			$data['phone_type'] 	= $this->input->post('phone_type');

 			$contact_id = $this->input->post('contact_name');
 		 	$contact_title = $this->input->post('contact_title');
			$contact_department = $this->input->post('contact_department');
 			$id = $this->input->post('id');
 				
			
			if ($this->input->post('role')) {
				$data['vendor_role'] = implode(',', $this->input->post('role'));
			}

			// first check vendor exist or not...
			if ($vendor_id) {

				$check_vendor_id['vendor_id'] = $this->input->post('vendor_id');

				// update table vendors...
				$this->User_model->updatedata('vendors', $check_vendor_id, $data);

					if($id){
						
						foreach($id as $key => $row)
						{
							if($contact_id[$key] != ''){
							
								$check_where11['id'] 				= $row;
				
								$check_where11['ref_vendor_id'] 	= $vendor_id;
								$fetch_check_ctact = $this->User_model->select_where('vendor_contact',$check_where11);

								if($fetch_check_ctact->num_rows() > 0)
								{
									$sql = "UPDATE `vendor_contact` SET contact_id = '".$contact_id[$key]."',contact_title='".$contact_title[$key]."',contact_department='".$contact_department[$key]."' WHERE  ref_vendor_id ='".$vendor_id."' AND id='".$row."' ";
											
								}else{

									 $sql = "INSERT INTO `vendor_contact` ( `ref_vendor_id`, `contact_id`,`contact_title`,`contact_department`) VALUES ( '".$vendor_id."', '".$contact_id[$key]."', '".$contact_title[$key]."', '".$contact_department[$key]."')";
								
								}
								
								$this->User_model->query($sql);
							}
						}
					}

				$this->session->set_flashdata('success', ' Vendor Updated successfully!');
				redirect(base_url() . 'vendor_data_view/id/' . $vendor_id, 'refresh');

			} else {

				echo "<pre>";
				print_r($data);
				echo "</pre>";
				die();
				// insert into 'vendor_data' table...
				$this->User_model->insertdata('vendors', $data);

				$insert_id = $this->db->insert_id();

				if ($insert_id) {

					if($contact_id){
				
						foreach($contact_id as $key => $row)
						{
							if($row != ''){
							
								$sql = "INSERT INTO vendor_contact ( ref_vendor_id, contact_id,contact_title,contact_department) VALUES ( '".$insert_id."', '".$row."', '".$contact_title[$key]."', '".$contact_department[$key]."')";
														
								$this->User_model->query($sql);
							}
						}
					}

					$this->session->set_flashdata('success', ' Records added successfully');
					redirect(base_url() . 'vendor_data_view/id/' . $vendor_id, 'refresh');

				}

			}

		}
	}

	public function fetch_contact_details() {
		error_reporting(0);
		$contact_id = $this->input->post('id');

		// fetch contact details by contact_id from "contact" table...
		$sqsl = $this->User_model->query("SELECT * FROM vendor_contact WHERE contact_id = '" . $contact_id . "' ");
		$resultss = $sqsl->result();
		foreach ($resultss as $val) {

		$con[$val->contact_id]=$val->contact_title;
		$dep[$val->contact_id]=$val->contact_department;

		}
		$sql = $this->User_model->query("SELECT * FROM contact WHERE contact_id = '" . $contact_id . "' ");
	
		$results = $sql->result();

		foreach ($results as $result) {

			$output['contact_name'] = $result->contact_id;
			//$output['contact_name'] = $result->contact_firstname . ' ' . $result->contact_middlename . ' ' . $result->contact_lastname;

			//$output['contact_title'] = $result->contact_title;
			$output['contact_title'] = isset($con[$result->contact_id]) ? $con[$result->contact_id]:$result->contact_title;
			$output['contact_department'] = isset($dep[$result->contact_id]) ? $dep[$result->contact_id]:'';
			$output['contact_email'] = $result->contact_email;
			$output['contact_phone'] = $result->contact_phone;
			$output['contact_phone2'] = $result->alter_phone;
			$output['contact_email2'] = $result->contact_email2;
			$output['alter_option'] = $result->alter_option;
			$output['pemail_type'] = $result->pemail_type;
			$output['aemail_type'] = $result->aemail_type;
			$output['primary_option'] = $result->primary_option;

		}
		print_r(json_encode($output));
	}

	public function delete_vendor(){
		$vendor_id = $this->uri->segment(2);
		$this->delete_vendor_data($vendor_id);

	}

	public function delete_vendor_data($vendor_id) {
		error_reporting(0);
		$delete = $this->User_model->query("DELETE FROM vendors WHERE vendor_id = '" . $vendor_id . "' ");
		$deletes = $this->User_model->query("DELETE FROM vendor_contact WHERE ref_vendor_id = '" . $vendor_id . "' ");

		if ($delete && $deletes) {
			$this->session->set_flashdata('success', ' Vendor  Deleted successfully!');
			redirect(base_url() . 'vendor-primary', 'refresh');
		}
	}

	public function check_duplicacy() {
		$vendor_name = $this->input->post('value');

		// check whether vendor name already exists or not....
		$sql = $this->User_model->query("SELECT * FROM vendors WHERE vendor_name = '" . $vendor_name . "' ");
		if ($sql->num_rows() > 0) {

			echo '1'; // yes, row occurs, means duplicacy

		} else {

			echo '0'; // No, there is no duplicacy

		}

	}

	public function get_name_of_vendor() {

		$id = $this->input->post('search_by_id');

		$sqll = $this->User_model->query("SELECT * FROM vendors WHERE vendor_type = '" . $id . "' ");
		if ($sqll->num_rows() > 0) {

			$fetch_v_data = $sqll->result();

			$v_id = $fetch_v_data[0]->vendor_id;

			echo $v_id;

		}

	}

	public function delete_vendor_contact(){

		$rowid = $this->input->post('rowid');
		$this->User_model->delete('vendor_contact',array('id'=>$rowid));
		echo '1';
	}

}
?>
