<?php
class Lender_Access extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('email');
		$this->load->model('User_model');
    $this->load->library('Aws3','aws3');
    $this->load->library('email');
    $this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');
		if ($this->session->userdata('t_user_id') != '') {
		} else {
			redirect(base_url() . "home", 'refresh');
		}

	}

	public function generate_pwd($length) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return substr(str_shuffle($chars), 0, $length);
  }

	public function lender_forgot_password(){

		$postiD = $this->input->post('contact_id');
		if($postiD != ''){

			$getcontact = $this->User_model->query("SELECT `contact_firstname`, `contact_email` FROM `contact` WHERE `contact_id`='".$postiD."'");
			$getcontact = $getcontact->result();

			$fname = $getcontact[0]->contact_firstname;
			$email_address = $getcontact[0]->contact_email;

			$getcontacttype = $this->User_model->query("SELECT `username`,`password` FROM `lender_contact_type` WHERE `contact_id`='".$postiD."'");
			$getcontacttype = $getcontacttype->result();

			$username = $getcontacttype[0]->username;
			$password = $getcontacttype[0]->password;

      //generate new password...
      $newPasswords = $this->generate_pwd(10);

			//note time
			$updation['reset_time']       = date('Y-m-d h:i:s');
      $updation['password']         = $newPasswords;
      $where['contact_id']          = $postiD;
			$this->User_model->updatedata('lender_contact_type',$where,$updation);

			$security   = $this->generate_pwd(16);
			//$reset_link = base_url().'Lender_Access/reset_pass_link/'.$security.'/'.$postiD.'/'.$security;
      $reset_link = base_url().'Lender_Portal/';

			$password_message = '<div style="background-color: #eeeeef; padding: 50px 0; "><div style="max-width:640px; margin:0 auto; "><div style="color: #fff; text-align: center; background-color:#4880af; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;"><h2>TaliMar Portal Login Information</h2></div><div style="padding: 20px; background-color: rgb(255, 255, 255); color:#555;"><p style="font-size: 14px;"> Hello '.$fname.' –<br><br>Below is your Username and Temporary Password. Please change your Temporary Password after you have successfully logged in by selecting My Profile under your Name in the top right corner. Should you have any questions, please contact Investor Relations at (888) 868-8467.<br><br><b>Username:</b> '.$username.' <br><b>Password:</b> '.$newPasswords.' </p><br><p style="text-align:center;font-size: 14px;"><a href="'.$reset_link.'" target="_blank" style="text-align:center; display: inline-block; padding: 10px 80px; font-family: Source Sans Pro, sans-serif; font-size: 14px; color: #ffffff; text-decoration: none; border-radius: 0px;background-color: #4880af;">Login Here</a></p><p></p><p style="font-size: 14px;"></p></div></div></div>';
                /*$this->email->initialize(SMTP_SENDGRID);
                $this->email->from('invest@talimarfinancial.com', 'TaliMar Financial');
                $this->email->to($email_address);
                $this->email->subject('TaliMar Portal Login Information');
                $this->email->message($password_message);
                $this->email->set_mailtype('html'); 
                $this->email->send(); */
        $subject = 'TaliMar Portal Login Information';
        
        $dataMailA = array();
        $dataMailA['from_email']  = 'mail@talimarfinancial.com';
        $dataMailA['from_name']   = 'TaliMar Financial';
        $dataMailA['to']      = $email_address;
        $dataMailA['cc']      = '';
        $dataMailA['name']      = '';
        $dataMailA['subject']     = $subject;
        $dataMailA['content_body']  = $password_message;
        $dataMailA['button_url']  = '';
        $dataMailA['button_text']   = '';
        send_mail_template($dataMailA);

                echo '1';
		}

	}

	public function reset_pass_link(){

		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		$uri5 = $this->uri->segment(5);

		//echo $uri3.' - '.$uri4.' - '.$uri5;

        if($uri3 && $uri4 && $uri5 != '')
        {
          
          $user['contact_id']  = $uri4;

          $fetch_user_data  = $this->User_model->select_where('lender_contact_type',$user);
          if($fetch_user_data->num_rows() > 0)
          {
            $fetch_user_data    = $fetch_user_data->result();
            $id           		= $fetch_user_data[0]->contact_id;
            $forget_pass_time   = $fetch_user_data[0]->reset_time;

            $current_time       =  date('Y-m-d H:i:s');
            $forget_time        =  strtotime($forget_pass_time);
            $now_time         	=  strtotime($current_time);

            $check_forget_pass_time = round(abs($forget_time - $now_time) / 60,2);

            if($check_forget_pass_time < 1440){  

              $data['user_id'] = $id;
              $data['status'] = 'true';
              $data['msg'] = '';
              $data['msgerror'] = '';
              $this->load->view('lender_password',$data);
              
            }else{

              $data['status'] = 'false';
              $data['msg'] = 'Link expire. Time out!';
              $data['user_id'] = '';
              $this->load->view('lender_password',$data);
            }

          }else{

              $data['status'] = 'false';
              $data['msg'] = 'Link not valid!';
              $data['user_id'] = '';
              $this->load->view('lender_password',$data);
          }

        }else{
              $data['status'] = 'false';
              $data['msg'] = 'Link not valid!';
              $data['user_id'] = '';
              $this->load->view('lender_password',$data);
        }
	}

	public function newPassword(){

        $input = $this->input->post();

            if($input['contact_id'] && $input['new_password'] && $input['re_password'] !=''){

                if($input['new_password'] == $input['re_password']){

                    $password                 = $input['new_password'];
                    $data['password']         = $password;
                    $data['c_password'] 	  = $password;
                    $data['reset_time']    	  = NULL;
                    $where['contact_id']      = $input['contact_id'];

                    $this->User_model->updatedata('lender_contact_type',$where,$data);
                    
                    $data['status'] = 'true';
                    $data['user_id'] = $input['contact_id'];
                    $data['msg'] = 'Password updated successfully';
                    $data['msgerror'] = '';
                    $this->load->view('lender_password',$data);

                }else{

                    $data['status'] = 'true';
                    $data['msgerror'] = 'Password not match';
                    $data['msg'] = '';
                    $data['user_id'] = $input['contact_id'];
                    $this->load->view('lender_password',$data);
                }

            }else{

                  $data['status'] = 'true';
                  $data['msgerror'] = 'All fields required';
                  $data['msg'] = '';
                  $data['user_id'] = $input['contact_id'];
                  $this->load->view('lender_password',$data);
            }

    }

}
?>