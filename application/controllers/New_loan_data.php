<?php
class New_loan_data extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('email');
		$this->load->model('User_model');
		$this->load->library('MYPDF');
		$this->load->library('Aws3','aws3');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');
		
		
		if ($this->session->userdata('t_user_id') != '') {
		} else {
			redirect(base_url() . "home", 'refresh');
		}
	}

	public function index() {
		
	}
	public function servicer_charge_delete(){
		$loan_id 			= str_replace("/", "", $this->input->post('loan_id'));
		$charges_id 		= $this->input->post('servicer_charge_delete_id');
		$this->User_model->query("DELETE FROM loan_boarding_data WHERE id = '$charges_id'");
		$this->session->set_flashdata('success', 'Servicer Charges Data Deleted Successfully!');
		$this->session->set_flashdata('popup_modal_hit', 'LoanBoarding');
		redirect(base_url() . 'load_data/' . $loan_id);
	}
	public function loan_boarding_data(){

		if($this->input->post('submitBoarding')){
			$servicer_charge_description 			= $this->config->item('servicer_charge_description');
		    $loan_id 					= str_replace("/", "", $this->input->post('loan_id'));

		    $talimar_loan 		= $this->input->post('talimar_loan');
		    $BoardingFeesOpt 	= $this->input->post('BoardingFeesOpt'); 
		    $bordingFee 		= $this->amount_format($this->input->post('ServicerCharge_amount')); 

		    $bordingText 		= $servicer_charge_description[$this->input->post('ServicerCharge_description')];
		    $charges_id         = $this->input->post('ServicerCharge_charges_id');

		    $dates = explode('-', $this->input->post('ServicerCharge_date'));
		    $month = $dates[0];
			$day = $dates[1];
			$year = $dates[2];
			$servicercharge_date = $year.'-'.$month.'-'.$day;			
		    
		    $paidfrom 			= $this->input->post('ServicerCharge_paidfrom');
		    $paidto 			= $this->input->post('ServicerCharge_paidto');
		    $servicercharge_description= $this->input->post('ServicerCharge_description'); 
		    $invoice  			= $this->input->post('ServicerCharge_invoice');
		    $created_at 		= date("Y-m-d H:i:s");
		    
		    if($charges_id > 0){
		    	$this->User_model->query("UPDATE loan_boarding_data SET 
		    		talimar_loan = '$talimar_loan', 
		    		BoardingFeesOpt = '$BoardingFeesOpt', 
		    		bordingFee = '$bordingFee', 
		    		bordingText = '$bordingText', 
		    		servicercharge_date = '$servicercharge_date', 
		    		paidfrom = '$paidfrom', 
		    		paidto = '$paidto', 
		    		servicercharge_description = '$servicercharge_description', 
		    		invoice = '$invoice' 
		    		WHERE id = '$charges_id'
		    		");		    	
		    }else{
		    	$this->User_model->query("INSERT INTO loan_boarding_data (talimar_loan, BoardingFeesOpt, bordingFee, bordingText, servicercharge_date, paidfrom, paidto, servicercharge_description, invoice, created_at) VALUES ('$talimar_loan', '$BoardingFeesOpt', '$bordingFee', '$bordingText', '$servicercharge_date', '$paidfrom', '$paidto', '$servicercharge_description', '$invoice', '$created_at')");		    	
		    }

		    $this->session->set_flashdata('success', 'Servicer Charges Data Updated Successfully!');
		    $this->session->set_flashdata('popup_modal_hit', 'LoanBoarding');
			redirect(base_url() . 'load_data/' . $loan_id);
		}
	}

	public function Removeboarding() {
		
		$id = $this->input->post('id');
		$this->User_model->query("DELETE from loan_boarding_data where id = '".$id."'");
		echo '1';
		
	}

	public function removeLenderappData() {
		
		$id = $this->input->post('rowid');
		$this->User_model->query("DELETE from lender_approval where id = '".$id."'");
		echo '1';
	}
	
	public function uploadLoanDoc() {

		echo 'working 123123123';


	}

	public function LoanDocumentsUpload() {

		echo 'working ';

	}

	public function form_wait_list() {

		$loan_id = $this->input->post('loan_id');
		$talimar_loan = $this->input->post('talimar_no');
		$fractional = $this->input->post('fractional');
		$confirmed = $this->input->post('confirmed');
		$array_id = $this->input->post('id');
		$lender_name = $this->input->post('contact_name');
		$amount = $this->input->post('amount');
		$date_request = $this->input->post('date_request');

		foreach ($array_id as $key => $id) {
			if ($id) {
				$new_date_request = NULL;
				if ($date_request[$key]) {
					//$myDateTime = DateTime::createFromFormat('m-d-Y', $date_request[$key]);
					//$new_date_request = $myDateTime->format('Y-m-d');
					$new_date_request = input_date_format($date_request[$key]);
				}
				if ($id == 'new') {
					$data['lender_name'] = $lender_name[$key];
					$data['fractional'] = $fractional[$key];
					$data['confirmed'] = $confirmed[$key];
					$data['amount'] = $this->amount_format($amount[$key]);
					$data['date_request'] = $new_date_request;
					$data['talimar_loan'] = $talimar_loan;

					$data['loan_id'] = $loan_id;
					$this->User_model->insertdata('wait_list', $data);
					unset($data);
				} else {
					$data['lender_name'] = $lender_name[$key];
					$data['amount'] = $this->amount_format($amount[$key]);
					$data['date_request'] = $new_date_request;
					$data['fractional'] = $fractional[$key];
					$data['confirmed'] = $confirmed[$key];
					$where['id'] = $id;
					$where['loan_id'] = $loan_id;

					$this->User_model->updatedata('wait_list', $where, $data);
				}
			}
		}
		$this->session->set_flashdata('popup_modal_cv', array('assigment_main', 'assigment_wait_list'));
		$this->session->set_flashdata('success', ' Wait List Updated Successfully!');
		$this->session->set_flashdata('popup_modal_hit', 'waitless');
		redirect(base_url() . 'load_data/' . $loan_id);
	}

	public function load_ajax_wait_list_data() {
		error_reporting(0);
		$loan_id = $this->input->post('loan_id');
		$fetch_loan = $this->User_model->select_where('loan', array('id' => $loan_id));
		$fetch_loan = $fetch_loan->row();
		$data['jhfdshksdfhsdfkjsdhfsdkjdfhsf'] = 1;
		$talimar_loan = $fetch_loan->talimar_loan;

		$fetch_wait_list = $this->User_model->select_where('wait_list', array('loan_id' => $loan_id, 'talimar_loan' => $talimar_loan));
		if ($fetch_wait_list->num_rows() > 0) {
			$fetch_wait_list = $fetch_wait_list->result();
			$data['wait_list_result'] = $fetch_wait_list;
		}

		$fetch_contact = $this->User_model->select_star('contact');
		$fetch_contact = $fetch_contact->result();

		foreach ($fetch_contact as $row) {
			$fetch_search_option[] = array(
				'id' => $row->contact_id,
				'text' => $row->contact_firstname . ' ' . $row->contact_middlename . ' ' . $row->contact_lastname,
			);
		}
		$data['fetch_search_option'] = $fetch_search_option;

		echo $this->load->view('wait_list/load_ajax_wait_list_data', $data, true);
	}

	public function amount_format($n) {
		$n = str_replace(',', '', $n);
		$a = trim($n, '$');
		$b = trim($a, ',');
		$c = trim($b, '%');
		return $c;
	}

	public function loan_draw_schedule(){

		if(isset($_POST['save'])){

			$data['amount'] 			= $this->amount_format($this->input->post('amount'));
			$data['description'] 		= $this->input->post('description');

			$amount_Req 		= $this->input->post('amount_Req');
			$amount_app 		= $this->input->post('amount_app');
			$status 			= $this->input->post('status');
			$approve 			= NULL;
			$requested 			= NULL;
			$draw_rel_id 		= $this->input->post('draw_rel_id');

			$drawID = $this->input->post('drawID');

			if($drawID != 'new'){

				$where['id'] = $drawID;
				$where['talimar_loan'] = $this->input->post('talimar_loan');
				//update here
				$this->User_model->updatedata('loan_draw_schedule',$where,$data);

				//loan_draw_released
				foreach ($draw_rel_id as $key => $drawvalue) {

					$date_Req = $this->input->post('date_Req');
					$date_Released = $this->input->post('date_Released');
					$date_submitted = $this->input->post('date_submitted');

					if($date_Req[$key] !=''){
						//$myDateTime = DateTime::createFromFormat('m-d-Y', $date_Req[$key]);
						//$new_date_Req[$key] = $myDateTime->format('Y-m-d');
						$new_date_Req[$key] = input_date_format($date_Req[$key]);
					}else{  $new_date_Req[$key] = ''; }

					if($date_Released[$key] !=''){
						//$myDateTime = DateTime::createFromFormat('m-d-Y', $date_Released[$key]);
						//$new_date_Released[$key] = $myDateTime->format('Y-m-d');
						$new_date_Released[$key] = input_date_format($date_Released[$key]);
					}else{ $new_date_Released[$key] = ''; }

					if($date_submitted[$key] !=''){
						//$myDateTimes = DateTime::createFromFormat('m-d-Y', $date_submitted[$key]);
						//$new_date_submitted[$key] = $myDateTimes->format('Y-m-d');
						$new_date_submitted[$key] = input_date_format($date_submitted[$key]);
					}else{ $new_date_submitted[$key] = ''; }

					if($amount_Req[$key] != ''){
					
						if($drawvalue == 'new'){

							//insert
							$this->User_model->query("INSERT INTO `loan_draw_released`(`draw_id`, `talimar_loan`, `amount_Req`, `requested`, `date_Req`, `date_Released`, `approve`, `amount_app`, `date_submitted`, `status`) VALUES ('".$drawID."', '".$this->input->post('talimar_loan')."', '".$this->amount_format($amount_Req[$key])."', '".$requested[$key]."', '".$new_date_Req[$key]."', '".$new_date_Released[$key]."', '".$approve[$key]."', '".$this->amount_format($amount_app[$key])."', '".$new_date_submitted[$key]."', '".$status[$key]."')");
						}else{

							//update
							$this->User_model->query("UPDATE `loan_draw_released` SET `amount_Req`='".$this->amount_format($amount_Req[$key])."', `requested`='".$requested[$key]."', `date_Req`='".$new_date_Req[$key]."', `date_Released`='".$new_date_Released[$key]."', `approve`='".$approve[$key]."', `amount_app`='".$this->amount_format($amount_app[$key])."', `date_submitted`='".$new_date_submitted[$key]."', `status`='".$status[$key]."' WHERE id = '".$drawvalue."'");
						}
					}
				}

				//send request email...
				$check_already_send = $this->User_model->query("SELECT * FROM loan_draw_released WHERE talimar_loan = '".$this->input->post('talimar_loan')."' AND draw_id = '".$drawID."' ");
				if($check_already_send->num_rows() > 0){

					$check_already_send = $check_already_send->result();

					//$sendEmailToArray = $this->AllAdminEmails();
					//$sendEmailTo = implode(', ', $sendEmailToArray);
					$sendEmailTo = 'bvandenberg@talimarfinancial.com';
					foreach ($check_already_send as $value) {
						
						if($value->requested == '1' && $value->req_email != '1'){

							$fetchaddressdata = $this->User_model->query("SELECT property_address, unit, city, state, zip, loan_id FROM loan_property WHERE talimar_loan = '".$value->talimar_loan."'");
							$fetchaddressdata = $fetchaddressdata->result();
							$fullAddress = $fetchaddressdata[0]->property_address.' '.$fetchaddressdata[0]->unit.'; '.$fetchaddressdata[0]->city.', '.$fetchaddressdata[0]->state.' '.$fetchaddressdata[0]->zip;
							$loan_id = $fetchaddressdata[0]->loan_id;

							$clickhereLink = base_url().'load_data/'.$loan_id.'#loan_reserve';
							
							$email 		= $sendEmailTo;
							$ccemail    = 'servicing@talimarfinancial.com';
							$subject 	= "Draw requested - ".$fetchaddressdata[0]->property_address;

							$message 	 = '';
							$message 	.= '<p>Construction Draw #'.$value->draw_id.' has been requested for '.$fullAddress.' in the amount of $'.number_format($value->amount_Req,2).'.</p>';
							$message 	.= '<p>Please review the request by <a href="'.$clickhereLink.'">clicking here</a></p>';

							/*$this->email->initialize(SMTP_SENDGRID);
							$this->email
								->from('mail@talimarfinancial.com', $subject)
								->to($email)
								->cc($ccemail)
								->subject($subject)
								->message($message)
								->set_mailtype('html');
							$this->email->send();*/

							$dataMailA = array();
							$dataMailA['from_email'] 	= 'mail@talimarfinancial.com';
							$dataMailA['from_name'] 	= 'TaliMar Financial';
							$dataMailA['to'] 			= $email;
							$dataMailA['to'] 			= $ccemail;
							$dataMailA['name'] 			= '';
							$dataMailA['subject'] 		= $subject;
							$dataMailA['content_body'] 	= $message;
							$dataMailA['button_url'] 	= '';
							$dataMailA['button_text'] 	= '';
							send_mail_template($dataMailA);

							// update req_email status...
							$this->User_model->updatedata('loan_draw_released',array('id'=>$value->id),array('req_email'=>1));
						}

						//send approve email...
						if($value->approve == '1' && $value->app_email != '1'){

							$fetchaddressdata = $this->User_model->query("SELECT property_address, unit, city, state, zip, loan_id FROM loan_property WHERE talimar_loan = '".$value->talimar_loan."'");
							$fetchaddressdata = $fetchaddressdata->result();
							$fullAddress = $fetchaddressdata[0]->property_address.' '.$fetchaddressdata[0]->unit.'; '.$fetchaddressdata[0]->city.', '.$fetchaddressdata[0]->state.' '.$fetchaddressdata[0]->zip;
							$loan_id = $fetchaddressdata[0]->loan_id;

							$clickhereLink = base_url().'load_data/'.$loan_id.'#loan_reserve';
							
							$email 		= $sendEmailTo;
							$ccemail    = 'servicing@talimarfinancial.com';
							$subject 	= "Draw approved - ".$fetchaddressdata[0]->property_address;

							$message 	 = '';
							$message 	.= '<p>Construction Draw #'.$value->draw_id.' has been approved for '.$fullAddress.' in the amount of $'.number_format($value->amount_app,2).'.</p>';
							$message 	.= '<p>Please review the approval by <a href="'.$clickhereLink.'">clicking here</a></p>';
							/*$this->email->initialize(SMTP_SENDGRID);
							$this->email
								->from('mail@talimarfinancial.com', $subject)
								->to($email)
								->cc($ccemail)
								->subject($subject)
								->message($message)
								->set_mailtype('html');
							$this->email->send();*/

							$dataMailA = array();
							$dataMailA['from_email'] 	= 'mail@talimarfinancial.com';
							$dataMailA['from_name'] 	= 'TaliMar Financial';
							$dataMailA['to'] 			= $email;
							$dataMailA['cc'] 			= $ccemail;
							$dataMailA['name'] 			= '';
							$dataMailA['subject'] 		= $subject;
							$dataMailA['content_body'] 	= $message;
							$dataMailA['button_url'] 	= '';
							$dataMailA['button_text'] 	= '';
							send_mail_template($dataMailA);

							// update req_email status...
							$this->User_model->updatedata('loan_draw_released',array('id'=>$value->id),array('app_email'=>1));
						}

					}
				}

				

				$this->session->set_flashdata('success','Loan draw updated successfully!');

			}else{ //Main else part...

				$data['talimar_loan'] = $this->input->post('talimar_loan');
				//insert here
				$this->User_model->insertdata('loan_draw_schedule',$data);
				$last_insert_id = $this->db->insert_id();

				//loan_draw_released
				foreach ($draw_rel_id as $key => $drawvalue) {

					$date_Req = $this->input->post('date_Req');
					$date_Released = $this->input->post('date_Released');
					$date_submitted = $this->input->post('date_submitted');

					if($date_Req[$key] !=''){
						//$myDateTime = DateTime::createFromFormat('m-d-Y', $date_Req[$key]);
						//$new_date_Req[$key] = $myDateTime->format('Y-m-d');
						$new_date_Req[$key] = input_date_format($date_Req[$key]);
					}else{  $new_date_Req[$key] = ''; }

					if($date_Released[$key] !=''){
						//$myDateTime = DateTime::createFromFormat('m-d-Y', $date_Released[$key]);
						//$new_date_Released[$key] = $myDateTime->format('Y-m-d');
						$new_date_Released[$key] = input_date_format($date_Released[$key]);
					}else{ $new_date_Released[$key] = ''; }

					if($date_submitted[$key] !=''){
						//$myDateTimes = DateTime::createFromFormat('m-d-Y', $date_submitted[$key]);
						//$new_date_submitted[$key] = $myDateTimes->format('Y-m-d');
						$new_date_submitted[$key] = input_date_format($date_submitted[$key]);
					}else{ $new_date_submitted[$key] = ''; }
					
					if($amount_Req[$key] != ''){

						if($drawvalue == 'new'){

							//insert
							$this->User_model->query("INSERT INTO `loan_draw_released`(`draw_id`, `talimar_loan`, `amount_Req`, `requested`, `date_Req`, `date_Released`, `approve`, `amount_app`, `date_submitted`, `status`) VALUES ('".$last_insert_id."', '".$this->input->post('talimar_loan')."', '".$this->amount_format($amount_Req[$key])."', '".$requested[$key]."', '".$new_date_Req[$key]."', '".$new_date_Released[$key]."', '".$approve[$key]."', '".$this->amount_format($amount_app[$key])."', '".$new_date_submitted[$key]."', '".$status[$key]."')");
						}else{

							//update
							$this->User_model->query("UPDATE `loan_draw_released` SET `amount_Req`='".$this->amount_format($amount_Req[$key])."', `requested`='".$requested[$key]."', `date_Req`='".$new_date_Req[$key]."', `date_Released`='".$new_date_Released[$key]."', `approve`='".$approve[$key]."', `amount_app`='".$this->amount_format($amount_app[$key])."', `date_submitted`='".$new_date_submitted[$key]."', `status`='".$status[$key]."' WHERE id = '".$drawvalue."'");
						}
					}
				}

				//send request email...
				$check_already_send = $this->User_model->query("SELECT * FROM loan_draw_released WHERE talimar_loan = '".$this->input->post('talimar_loan')."' AND draw_id = '".$last_insert_id."' ");
				if($check_already_send->num_rows() > 0){

					$check_already_send = $check_already_send->result();

					//$sendEmailToArray = $this->AllAdminEmails();
					//$sendEmailTo = implode(', ', $sendEmailToArray);
					$sendEmailTo = 'bvandenberg@talimarfinancial.com';
					foreach ($check_already_send as $value) {
						
						if($value->requested == '1' && $value->req_email != '1'){

							$fetchaddressdata = $this->User_model->query("SELECT property_address, unit, city, state, zip, loan_id FROM loan_property WHERE talimar_loan = '".$value->talimar_loan."'");
							$fetchaddressdata = $fetchaddressdata->result();
							$fullAddress = $fetchaddressdata[0]->property_address.' '.$fetchaddressdata[0]->unit.'; '.$fetchaddressdata[0]->city.', '.$fetchaddressdata[0]->state.' '.$fetchaddressdata[0]->zip;
							$loan_id = $fetchaddressdata[0]->loan_id;

							$clickhereLink = base_url().'load_data/'.$loan_id.'#loan_reserve';
							
							$email 		= $sendEmailTo;
							$ccemail    = 'servicing@talimarfinancial.com';
							$subject 	= "Draw requested - ".$fetchaddressdata[0]->property_address;

							$message 	 = '';
							$message 	.= '<p>Construction Draw #'.$value->draw_id.' has been requested for '.$fullAddress.' in the amount of $'.number_format($value->amount_Req,2).'.</p>';
							$message 	.= '<p>Please review the request by <a href="'.$clickhereLink.'">clicking here</a></p>';
							/*$this->email->initialize(SMTP_SENDGRID);
							$this->email
								->from('mail@talimarfinancial.com', $subject)
								->to($email)
								->cc($ccemail)
								->subject($subject)
								->message($message)
								->set_mailtype('html');
							$this->email->send();*/

							$dataMailA = array();
							$dataMailA['from_email'] 	= 'mail@talimarfinancial.com';
							$dataMailA['from_name'] 	= 'TaliMar Financial';
							$dataMailA['to'] 			= $email;
							$dataMailA['cc'] 			= $ccemail;
							$dataMailA['name'] 			= '';
							$dataMailA['subject'] 		= $subject;
							$dataMailA['content_body'] 	= $message;
							$dataMailA['button_url'] 	= '';
							$dataMailA['button_text'] 	= '';
							send_mail_template($dataMailA);

							// update req_email status...
							$this->User_model->updatedata('loan_draw_released',array('id'=>$value->id),array('req_email'=>1));
						}

						//send approve email...
						if($value->approve == '1' && $value->app_email != '1'){

							$fetchaddressdata = $this->User_model->query("SELECT property_address, unit, city, state, zip, loan_id FROM loan_property WHERE talimar_loan = '".$value->talimar_loan."'");
							$fetchaddressdata = $fetchaddressdata->result();
							$fullAddress = $fetchaddressdata[0]->property_address.' '.$fetchaddressdata[0]->unit.'; '.$fetchaddressdata[0]->city.', '.$fetchaddressdata[0]->state.' '.$fetchaddressdata[0]->zip;
							$loan_id = $fetchaddressdata[0]->loan_id;

							$clickhereLink = base_url().'load_data/'.$loan_id.'#loan_reserve';
							
							$email 		= $sendEmailTo;
							$ccemail    = 'servicing@talimarfinancial.com';
							$subject 	= "Draw approved - ".$fetchaddressdata[0]->property_address;

							$message 	 = '';
							$message 	.= '<p>Construction Draw #'.$value->draw_id.' has been approved for '.$fullAddress.' in the amount of $'.number_format($value->amount_app,2).'.</p>';
							$message 	.= '<p>Please review the approval by </p>';

							/*$this->email->initialize(SMTP_SENDGRID);
							$this->email
								->from('mail@talimarfinancial.com', $subject)
								->to($email)
								->cc($ccemail)
								->subject($subject)
								->message($message)
								->set_mailtype('html');
							$this->email->send();*/

							$dataMailA = array();
							$dataMailA['from_email'] 	= 'mail@talimarfinancial.com';
							$dataMailA['from_name'] 		= 'TaliMar Financial';
							$dataMailA['to'] 			= $email;
							$dataMailA['cc'] 			= $ccemail;
							$dataMailA['name'] 			= '';
							$dataMailA['subject'] 		= $subject;
							$dataMailA['content_body'] 	= $message;
							$dataMailA['button_url'] 	= $clickhereLink;
							$dataMailA['button_text'] 	= 'clicking here';
							send_mail_template($dataMailA);

							// update req_email status...
							$this->User_model->updatedata('loan_draw_released',array('id'=>$value->id),array('app_email'=>1));
						}

					}
				}

				

				$this->session->set_flashdata('success','Loan draw added successfully!');
			}


			$this->session->set_flashdata('popup_modal_hit', 'loan_draw_data');
			redirect(base_url().'load_data/'.$this->input->post('loan_id'));

		}
	}


	public function loan_reqdraw_schedule(){

		if(isset($_POST['submit'])){

			$data['amount_Req'] = $this->amount_format($this->input->post('AmountRequested'));
			$data['amount_app'] = $this->amount_format($this->input->post('AmountApproved'));

			if($this->input->post('DateRequested') !=''){
				//$myDateTime = DateTime::createFromFormat('m-d-Y', $this->input->post('DateRequested'));
				//$new_date_Req = $myDateTime->format('Y-m-d');
				$new_date_Req = input_date_format($this->input->post('DateRequested'));
			}else{  $new_date_Req = ''; }

			if($this->input->post('DateApproved') !=''){
				//$myDateTime = DateTime::createFromFormat('m-d-Y', $this->input->post('DateApproved'));
				//$new_date_Released = $myDateTime->format('Y-m-d');
				$new_date_Released = input_date_format($this->input->post('DateApproved'));
			}else{ $new_date_Released = ''; }

			$data['date_Req']  		= $new_date_Req;
			$data['date_Released']  = $new_date_Released;
			$data['status']  		= $this->input->post('draw_status');
			$data['approve']  		= $this->input->post('app_status');

			$reqrowid = $this->input->post('reqrowid');

			if($reqrowid != 'new'){

				$this->User_model->updatedata('loan_draw_released',array('id'=>$reqrowid), $data);
				$this->session->set_flashdata('success','Loan draw updated successfully!');
			}else{

				$data['draw_id']  = $this->input->post('draw_id');
				$data['talimar_loan']  = $this->input->post('talimar_loan');

				$this->User_model->insertdata('loan_draw_released', $data);
				$this->session->set_flashdata('success','Loan draw added successfully!');
			}

			$this->session->set_flashdata('popup_modal_hit', 'loan_draw_data');
			redirect(base_url().'load_data/'.$this->input->post('loan_id'));
		}
	}


	public function AllAdminEmails(){

		$fetchemail = $this->User_model->query("SELECT `email_address` FROM `user` WHERE `role`='2'");
		$fetchemail = $fetchemail->result();
		foreach ($fetchemail as $value) {
			
			$allEmails[] = $value->email_address;
		}

		return $allEmails;
	}

	public function remove_draw_rel(){

		$rowid 		= $this->input->post('rowid');
		$this->User_model->delete('loan_draw_released',array('id'=>$rowid));
		echo '1';
	}

	public function get_loandraw_schedule_info(){

		$rowid 		= $this->input->post('rowid');
		$getdraw 	= $this->User_model->select_where('loan_draw_schedule',array('id'=>$rowid));
		$getdraw 	= $getdraw->result();

		foreach ($getdraw as  $value) {
			
			$getdrawreleased 	= $this->User_model->select_where('loan_draw_released',array('draw_id'=>$value->id));
			$getdrawreleased 	= $getdrawreleased->result();

			$myres['drawid'] 		= $value->id;
			$myres['amount'] 		= $value->amount;
			$myres['description'] 	= $value->description;
			$myres['drawrel'] 		= $getdrawreleased;
		}

		echo json_encode($myres);
	}

	public function requestdatainfo(){

		$rowid 		= $this->input->post('rowid');
		$getdrawreleased 	= $this->User_model->query("SELECT * FROM loan_draw_released WHERE id = '".$rowid."'");
		$getdrawreleased 	= $getdrawreleased->row();

		echo json_encode($getdrawreleased);
	}

	public function remove_loan_draw(){

		$rowid 		= $this->input->post('rowid');
		$this->User_model->delete('loan_draw_schedule',array('id'=>$rowid));
		$this->User_model->delete('loan_draw_released',array('draw_id'=>$rowid));
		echo '1';
	}

	

	public function Assignments_appORdeny(){

		if(isset($_POST['save'])){

			$rowid 					= $this->input->post('lrowid');
			$loan_idd 				= $this->input->post('loan_idd');
			

			$data['LP_status'] 			= $this->input->post('status');
			$data['LP_request'] 		= $this->input->post('request');
			$data['LP_description'] 	= $this->input->post('description');

			if($this->input->post('date_req') !=''){
				//$myDateTime = DateTime::createFromFormat('m-d-Y', $this->input->post('date_req'));
				//$new_date_Req = $myDateTime->format('Y-m-d');
				$new_date_Req = input_date_format($this->input->post('date_req'));
			}else{  $new_date_Req = ''; }

			if($this->input->post('date_complete') !=''){
				//$myDateTime = DateTime::createFromFormat('m-d-Y', $this->input->post('date_complete'));
				//$new_date_Released = $myDateTime->format('Y-m-d');
				$new_date_Released = input_date_format($this->input->post('date_complete'));
			}else{ $new_date_Released = ''; }

			$data['LP_date_req'] 		= $new_date_Req;
			$data['LP_date_complete'] 	= $new_date_Released;
			$LProwid 					= $this->input->post('rowid');

			if($LProwid != 'new'){
				$this->User_model->updatedata('lender_approval',array('id'=>$LProwid),$data);

				$last_insert_id = $LProwid;
				//insert into checkbox table
				foreach ($rowid as $key => $value) {

					$talimar_loan 	= $this->input->post('talimar_no');
					$assignment_appORdeny 	= $this->input->post('assignment_appORdeny_'.$value);

					$wherechk['talimar_loan'] 	= $talimar_loan;
					$wherechk['lender_id'] 		= $value;
					$wherechk['approvalid'] 	= $last_insert_id;
					$getdetails = $this->User_model->select_where('lender_approval_checkboxes',$wherechk);
					if($getdetails->num_rows() > 0){

						$this->User_model->query("UPDATE `lender_approval_checkboxes` SET `chk_value`= '".$assignment_appORdeny[0]."' WHERE talimar_loan = '".$talimar_loan."' AND lender_id = '".$value."' AND approvalid = '".$last_insert_id."'");
					}else{
						$this->User_model->query("INSERT INTO `lender_approval_checkboxes`(`lender_id`, `talimar_loan`, `chk_value`,`approvalid`) VALUES ('".$value."', '".$talimar_loan."', '".$assignment_appORdeny[0]."','".$last_insert_id."')");
					}
				}

			}else{

				$data['talimar_loan'] 	= $this->input->post('talimar_no');
				$this->User_model->insertdata('lender_approval',$data);
				$last_insert_id = $this->db->insert_id();

				//insert into checkbox table
				if($rowid){
					foreach ($rowid as $key => $value) {

						$talimar_loan 	= $this->input->post('talimar_no');
						$assignment_appORdeny 	= $this->input->post('assignment_appORdeny_'.$value);

						$wherechk['talimar_loan'] 	= $talimar_loan;
						$wherechk['lender_id'] 		= $value;
						$wherechk['approvalid'] 	= $last_insert_id;
						$getdetails = $this->User_model->select_where('lender_approval_checkboxes',$wherechk);
						if($getdetails->num_rows() > 0){

							$this->User_model->query("UPDATE `lender_approval_checkboxes` SET `chk_value`= '".$assignment_appORdeny[0]."', 'approvalid' = '".$last_insert_id."' WHERE talimar_loan = '".$talimar_loan."' AND lender_id = '".$value."'");
						}else{
							$this->User_model->query("INSERT INTO `lender_approval_checkboxes`(`lender_id`, `talimar_loan`, `chk_value`,`approvalid`) VALUES ('".$value."', '".$talimar_loan."', '".$assignment_appORdeny[0]."','".$last_insert_id."')");
						}
					}
				}
			}

			$this->session->set_flashdata('popup_modal_cv', array('assigment_main', 'lender_approval_main', 'lender_approval_view'));
			$this->session->set_flashdata('success', 'Lender approval submit successfully!');
			redirect(base_url() . 'load_data' . $loan_idd);
		}
	}


	public function get_loanApp_info(){

		$rowid = $this->input->post('rowid');
		$fetchlenderApp = $this->User_model->select_where('lender_approval', array('id'=>$rowid));
		$fetchlenderApp = $fetchlenderApp->result();

		echo json_encode($fetchlenderApp);
	}

	public function get_loanApp_chkinfo(){

		$rowid = $this->input->post('rowid');
		$fetchlenderAppchk = $this->User_model->select_where('lender_approval_checkboxes', array('approvalid'=>$rowid));
		if($fetchlenderAppchk->num_rows() > 0){

			$fetchlenderAppchk = $fetchlenderAppchk->result();
			echo json_encode($fetchlenderAppchk);
		}else{
			echo '1';
		}
	}


	public function remove_table_wait_lists() {
		$id = $this->input->post('id');
		$this->User_model->delete('wait_list', array('id' => $id));
		echo 'deleted';
	}

	public function auto_debit_payment() {

		if (isset($_POST['submit'])) {

			$loan_id = str_replace('/', '', $this->input->post('loan_id'));
			$data['loan_id'] = $loan_id;
			$data['account_type'] = $this->input->post('account_type');
			$data['account_name'] = $this->input->post('account_name');
			$data['bank_name'] = $this->input->post('bank_name');
			$data['account_no'] = $this->input->post('account_no');
			$data['routing_no'] = $this->input->post('routing_no');
			$data['pament_date'] = $this->input->post('pament_date');
			$data['payment_due_date'] = $this->input->post('payment_due_date');
			$data['debit_amount'] = $this->amount_format($this->input->post('debit_amount'));
			$data['monthly_payment'] = $this->amount_format($this->input->post('monthly_payment'));

			$where['talimar_loan'] = $this->input->post('talimar_no');

			$fetch_debit = $this->User_model->select_where('auto_debit_payment', $where);
			if ($fetch_debit->num_rows() > 0) {

				$this->User_model->updatedata('auto_debit_payment', $where, $data);
				$this->session->set_flashdata('success', 'Auto debit payment updated successfully!');
			} else {

				$data['talimar_loan'] = $this->input->post('talimar_no');
				$this->User_model->insertdata('auto_debit_payment', $data);
				$this->session->set_flashdata('success', 'Auto debit payment added successfully!');
			}

			redirect(base_url() . 'load_data/' . $loan_id);
		}

	}

	public function print_loan_servicing_checklist() {
		error_reporting(0);
		$loan_id = $this->uri->segment(3);

		//fetch talimar loan number...
		$sql = "SELECT talimar_loan,borrower FROM `loan` WHERE `id`='" . $loan_id . "'";
		$fetch_talimar_loan = $this->User_model->query($sql);
		$fetch_talimar_loan = $fetch_talimar_loan->row();
		$talimar_loan = $fetch_talimar_loan->talimar_loan;

		//fetch all loan table info...
		$sql1 = "SELECT * FROM `loan` WHERE `id`='" . $loan_id . "'";
		$fetch_loan_data = $this->User_model->query($sql1);
		$fetch_loan_data = $fetch_loan_data->result();

		//fetch borrower name...
		$borrower_name = "SELECT * FROM `borrower_data` WHERE id='" . $fetch_loan_data[0]->borrower . "'";
		$fetch_borrower_name = $this->User_model->query($borrower_name);
		$fetch_borrower_name = $fetch_borrower_name->row();
		$fetch_borrower_name = $fetch_borrower_name->b_name;

		//fetch_borrower_contact details...
		$sql3 = "SELECT * FROM `borrower_contact` WHERE borrower_id = '" . $fetch_loan_data[0]->borrower . "'";
		$fetch_b_contact = $this->User_model->query($sql3);
		$fetch_b_contact = $fetch_b_contact->result();

		$fetch_contact_id = $fetch_b_contact[0]->contact_id;
		$contact_phone = $fetch_b_contact[0]->contact_phone;
		$contact_email = $fetch_b_contact[0]->contact_email;

		//fetch_contact name...
		$contact = "select * from contact where contact_id = '" . $fetch_contact_id . "'";
		$contact = $this->User_model->query($contact);
		$contact = $contact->row();
		$contact_name = $contact->contact_firstname . ' ' . $contact->contact_middlename . ' ' . $contact->contact_lastname;

		//fetch loan property address...
		$property = "SELECT * FROM `property_home` WHERE talimar_loan='" . $talimar_loan . "' AND `primary_property` ='1'";
		$fetch_property = $this->User_model->query($property);
		$fetch_property = $fetch_property->result();
		$address = $fetch_property[0]->property_address;

		$loan_property = "SELECT * FROM loan_property WHERE talimar_loan = '" . $talimar_loan . "'";
		$fetch_address = $this->User_model->query($loan_property);
		$fetch_address = $fetch_address->result();
		$fetch_address = $fetch_address[0]->property_address . '' . $fetch_address[0]->unit . '; ' . $fetch_address[0]->city . ', ' . $fetch_address[0]->state . ' ' . $fetch_address[0]->zip;

		//fetch loan_servicing_checklist data...

		$sql = "SELECT * FROM loan_servicing_checklist WHERE talimar_loan = '" . $talimar_loan . "'";
		$checklist = $this->User_model->query($sql);
		$checklist = $checklist->result();

		$loan_type_option = $this->config->item('loan_type_option');

		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'ISO-8859-1', false);
		$pdf->SetCreator(PDF_CREATOR);

		// set default header data
		// $pdf->SetHeaderData("include/icons/logo-light.png", 30, '', '');

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(2, 20, 2);
		// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		$pdf->SetFont('times', '', 7);

		// add a page
		//$pdf->AddPage('L', 'A4');
		$pdf->AddPage('P', 'A4');
		// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
		$header = '<h1 style="color:#003468;font-size:18px;text-align:center;"> Loan Servicing Checklist</h1>';
		$header .= '<style>
        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 20px;
            border-collapse:collapse;
        }
        .table-bordered {
        border: 1px solid #ddd;
        }
        table {
            border-spacing: 0;
            border-collapse: collapse;
        }
        .table td{
            height : 25px;
            font-size:12px;
        }
        tr.table_header th{
            height:25px;
            font-size:12px;
            font-family: "Times New Roman", Georgia, Serif;
            text-align:center;
            background-color:#bfcfe3;
        }

        tr.table_bottom th{
            font-size:12px;
            font-family: "Times New Roman", Georgia, Serif;
            text-align:left;
            background-color:#bfcfe3;
        }
        tr.odd td{
            background-color:#ededed;
        }
        </style>
        ';

		$header .= '<table class="table">';

		$header .= '<tr class="table_header">';
		$header .= '<td style="width:100%;"><strong>Property Address:</strong> ' . $fetch_address . '</td>';
		$header .= '</tr>';

		$header .= '<tr class="table_header">';
		$header .= '<td style="width:50%;"><strong>Loan Amount:</strong> $' . number_format($fetch_loan_data[0]->loan_amount) . '</td>';
		$header .= '<td style="width:50%;"><strong>Loan Type:</strong> ' . $loan_type_option[$fetch_loan_data[0]->loan_type] . '</td>';
		$header .= '</tr>';

		$header .= '<tr class="table_header">';

		$header .= '<td style="width:50%;"><strong>TaliMar Loan #:</strong> ' . $fetch_loan_data[0]->talimar_loan . '</td>';
		
		$header .= '<td style="width:50%;"><strong>Services Loan #:</strong> ' . $fetch_loan_data[0]->fci . '</td>';
	
		$header .= '</tr>';


		$header .= '<tr class="table_header">';
		$header .= '<td style="width:50%;"><strong>Borrower Name:</strong> ' . $fetch_borrower_name . '</td>';
		$header .= '<td style="width:50%;"><strong>Contact Name:</strong> ' . $contact_name . '</td>';
		
		
		$header .= '</tr>';

		$header .= '<tr class="table_header">';
		$header .= '<td style="width:50%;"><strong>Phone Number:</strong> ' . $contact_phone . '</td>';
		$header .= '<td style="width:50%;"><strong>E-mail Address:</strong> ' . $contact_email . '</td>';
		$header .= '<td style="width:100%;border-top:1px solid #333;"></td>';
		$header .= '</tr>';

		$header .= '</table>';

		$header .= '<table class="table table-bordered th_text_align_center">';

		$header .= '<tr class="table_header">';
		$header .= '<th style="width:20%;"><strong>Complete</strong></th>';
		$header .= '<th style="width:20%;"><strong>Not Applicable</strong></th>';
		$header .= '<th style="width:60%;"><strong>Description</strong></th>';
		$header .= '</tr>';

		/*****************Loan Origination******************/

			$header .= '<tr>';
			$header .= '<td style="width:100%;font-size:15px;"><strong>Loan Origination</strong></td>';
			$header .= '</tr>';

			$item_name = $this->config->item('loan_servicing_checklist');

			$servicing_array_final = array();
			foreach ($item_name as $key => $value) {

				$new_loan_servicing_checklist[$key][] = $value;

			}

			//$array =array(101,102,103,104,105,107,134,108,109,117,125,128,131,130);
			$array = array(101,102,2000,1050,145,1051,1058);
			if (isset($array)) {

				foreach ($array as $number) {

					foreach ($new_loan_servicing_checklist[$number] as $row) {

						$servicing_array_final[$number] = $row;
					}
				}
			}

			$count = 0;
			foreach ($servicing_array_final as $ke => $val) {

				foreach ($checklist as $row) {

					if ($ke == $row->hud) {

						$count++;
						if ($count % 2 == 0) {
							$class_add = "even";
						} else if ($count == 0) {
							$class_add = "even";
						} else {
							$class_add = "odd";
						}

						$check_checklist = '';
						if ($row->checklist == '1') {
							$check_checklist = 'X';

						} else {
							$check_checklist = '';

						}

						$check_not_applicable = '';
						if ($row->not_applicable == '1') {

							$check_not_applicable = 'X';
						} else {

							$check_not_applicable = '';
						}

						$header .= '<tr class="' . $class_add . '">';
						$header .= '<td style="width:20%;text-align:center;">' . $check_checklist . '</td>';
						$header .= '<td style="width:20%;text-align:center;">' . $check_not_applicable . '</td>';
						if(in_array($row->hud, $checklist_Complete)){
							$header .= '<td style="width:60%;"><b>' . $item_name[$row->hud] . '</b></td>';
						}else{
							$header .= '<td style="width:60%;">' . $item_name[$row->hud] . '</td>';
						}
						$header .= '</tr>';

					}

				}
			}


		/*****************Loan Origination******************/

		/*****************Underwritingn******************/



		$header .= '<tr>';
		$header .= '<td style="width:100%;font-size:15px;"><strong>Underwriting</strong></td>';
		$header .= '</tr>';

		$item_name = $this->config->item('loan_servicing_checklist');
		$servicing_array_final = array();
		foreach ($item_name as $key => $value) {

			$new_loan_servicing_checklist[$key][] = $value;

		}

		$checklist_Complete = array(1058,135,1056,1055,1037,138);

		
		//$array = array(127,1044,108,1045,1052,1053,1046,144,128,1011,125,131,130,135);
		//$array =array(127,1044,108,1045,1061,1062,1063,1064,1065,1052,1046,128,1011,125,131,130,135);
		$array =array(127,1044,108,1045,1061,1062,1063,1065,128,1066,1011,125,131,130,135);
		if (isset($array)) {

			foreach ($array as $number) {

				foreach ($new_loan_servicing_checklist[$number] as $row) {

					$servicing_array_final[$number] = $row;
				}
			}
		}

		$count = 0;
		foreach ($servicing_array_final as $ke => $val) {

			foreach ($checklist as $row) {

				if ($ke == $row->hud) {

					$count++;
					if ($count % 2 == 0) {
						$class_add = "even";
					} else if ($count == 0) {
						$class_add = "even";
					} else {
						$class_add = "odd";
					}

					$check_checklist = '';
					if ($row->checklist == '1') {
						$check_checklist = 'X';

					} else {
						$check_checklist = '';

					}

					$check_not_applicable = '';
					if ($row->not_applicable == '1') {

						$check_not_applicable = 'X';
					} else {

						$check_not_applicable = '';
					}

					$header .= '<tr class="' . $class_add . '">';
					$header .= '<td style="width:20%;text-align:center;">' . $check_checklist . '</td>';
					$header .= '<td style="width:20%;text-align:center;">' . $check_not_applicable . '</td>';
					if(in_array($row->hud, $checklist_Complete)){
						$header .= '<td style="width:60%;"><b>' . $item_name[$row->hud] . '</b></td>';
					}else{
						$header .= '<td style="width:60%;">' . $item_name[$row->hud] . '</td>';
					}
					
					$header .= '</tr>';

				}

			}
		}

		/*****************Underwritingn******************/

		/*****************Investor Relations******************/

		$header .= '<tr>';
		$header .= '<td style="width:100%;font-size:15px;"><strong>Investor Relations</strong></td>';
		$header .= '</tr>';
		
		$item_names = $this->config->item('loan_servicing_checklist');
		
		foreach ($item_names as $key => $value) {

			$new_loan_servicing_checklists[$key][] = $value;

		}

		//$arrayy =array(111,113,114,116,124,126,132,1003,1008,1009,1014,1026,1023,1024);
		//$arrayy = array(141,142,111, 113, 114, 116, 124, 126, 132, 1003, 1008, 1029, 1009, 1014, 1009, 1023, 1024, 136);
		$arrayy = array(1047,141,142,111,1060,116,1054,126,132,1003,1009,1024,1056);
		if (isset($arrayy)) {

			foreach ($arrayy as $numbers) {

				foreach ($new_loan_servicing_checklists[$numbers] as $rows) {

					$servicing_array_finals[$numbers] = $rows;
				}
			}
		}

		$count = 0;
		foreach ($servicing_array_finals as $ke => $val) {

			foreach ($checklist as $rows) {

				if ($ke == $rows->hud) {

					$count++;
					if ($count % 2 == 0) {
						$class_adds = "even";
					} else if ($count == 0) {
						$class_adds = "even";
					} else {
						$class_adds = "odd";
					}

					$check_checklist = '';
					if ($rows->checklist == '1') {
						$check_checklist = 'X';

					} else {
						$check_checklist = '';

					}

					$check_not_applicable = '';
					if ($rows->not_applicable == '1') {

						$check_not_applicable = 'X';
					} else {

						$check_not_applicable = '';
					}

					$header .= '<tr class="' . $class_adds . '">';
					$header .= '<td style="width:20%;text-align:center;">' . $check_checklist . '</td>';
					$header .= '<td style="width:20%;text-align:center;">' . $check_not_applicable . '</td>';

					if(in_array($row->hud, $checklist_Complete)){
						$header .= '<td style="width:60%;"><b>' . $item_name[$rows->hud] . '</b></td>';
					}else{
						$header .= '<td style="width:60%;">' . $item_name[$rows->hud] . '</td>';
					}					
					$header .= '</tr>';

				}
			}
		}

		/*****************Investor Relations******************/

		/*****************Loan Closing******************/
		$header .= '<tr>';
		$header .= '<td style="width:100%;font-size:15px;"><strong>Loan Closing</strong></td>';
		$header .= '</tr>';

		$item_namessss = $this->config->item('loan_servicing_checklist');

		foreach ($item_namessss as $keyy => $valuee) {

			$new_loan_servicing_checklistsssss[$keyy][] = $valuee;

		}

		//$arrayyy =array(106,110,112,115,129,1002,1005,1006);
		//$arrayyy = array(106, 109, 110, 133, 112, 115, 129, 1030, 1002, 1004, 1005, 1012, 1006, 137);
		$arrayyy =array(106,1059,109,1039,110,112,115,129,1030,1002,1006,1055);
		if (isset($arrayyy)) {

			foreach ($arrayyy as $numbersss) {

				foreach ($new_loan_servicing_checklistsssss[$numbersss] as $rowsss) {

					$servicing_array_finalssss[$numbersss] = $rowsss;
				}
			}
		}

		$count = 0;
		foreach ($servicing_array_finalssss as $kee => $vall) {

			foreach ($checklist as $rowssss) {

				if ($kee == $rowssss->hud) {

					$count++;
					if ($count % 2 == 0) {
						$class_adds = "even";
					} else if ($count == 0) {
						$class_adds = "even";
					} else {
						$class_adds = "odd";
					}

					$check_checklist = '';
					if ($rowssss->checklist == '1') {
						$check_checklist = 'X';

					} else {
						$check_checklist = '';

					}

					$check_not_applicable = '';
					if ($rowssss->not_applicable == '1') {

						$check_not_applicable = 'X';
					} else {

						$check_not_applicable = '';
					}

					$header .= '<tr class="' . $class_adds . '">';
					$header .= '<td style="width:20%;text-align:center;">' . $check_checklist . '</td>';
					$header .= '<td style="width:20%;text-align:center;">' . $check_not_applicable . '</td>';
					if(in_array($rowssss->hud, $checklist_Complete)){
					    $header .= '<td style="width:60%;"><b>' . $item_namessss[$rowssss->hud] . '</b></td>';
					}else{
						$header .= '<td style="width:60%;">' . $item_namessss[$rowssss->hud] . '</td>';
					}
					$header .= '</tr>';

				}
			}
		}

		/*****************Loan Closing******************/

		/*****************Post Loan Closing******************/

		$header .= '<tr>';
		$header .= '<td style="width:100%;font-size:15px;"><strong>Post Loan Closing</strong></td>';
		$header .= '</tr>';

		$item_namess = $this->config->item('loan_servicing_checklist');

		foreach ($item_namess as $key => $value) {

			$new_loan_servicing_checklistss[$key][] = $value;

		}

		//	$arrayyy =array(127,133,1025,1004,1007,1010,1011,1012,1013,1015,1016,1017,1018,1019);
		//$arrayyy = array(133, 1013, 1019, 1025, 1007, 1010, 1015, 1016, 1026,1031, 138);
		$arrayyy =array(1005,1036,1004,1043,1067,1014,1021,1034,1037);
		if (isset($arrayyy)) {
			foreach ($arrayyy as $numberss) {

				foreach ($new_loan_servicing_checklistss[$numberss] as $rowss) {

					$servicing_array_finalss[$numberss] = $rowss;
				}
			}
		}

		$count = 0;
		foreach ($servicing_array_finalss as $ke => $val) {

			foreach ($checklist as $rowss) {

				if ($ke == $rowss->hud) {

					$count++;
					if ($count % 2 == 0) {
						$class_adds = "even";
					} else if ($count == 0) {
						$class_adds = "even";
					} else {
						$class_adds = "odd";
					}

					$check_checklist = '';
					if ($rowss->checklist == '1') {
						$check_checklist = 'X';

					} else {
						$check_checklist = '';

					}

					$check_not_applicable = '';
					if ($rowss->not_applicable == '1') {

						$check_not_applicable = 'X';
					} else {

						$check_not_applicable = '';
					}

					$header .= '<tr class="' . $class_adds . '">';
					$header .= '<td style="width:20%;text-align:center;">' . $check_checklist . '</td>';
					$header .= '<td style="width:20%;text-align:center;">' . $check_not_applicable . '</td>';
					if(in_array($rowss->hud, $checklist_Complete)){
						$header .= '<td style="width:60%;"><b>' . $item_namess[$rowss->hud] . '</b></td>';
					}else{
						$header .= '<td style="width:60%;">' . $item_namess[$rowss->hud] . '</td>';
					}
					$header .= '</tr>';

				}
			}

		}

		/*****************Post Loan Closing******************/

		/*****************Loan Servicing******************/
		$header .= '<tr>';
		$header .= '<td style="width:100%;font-size:15px;"><strong>Loan Servicing</strong></td>';
		$header .= '</tr>';

		$item_namess = $this->config->item('loan_servicing_checklist');

		foreach ($item_namess as $kesy => $valuaea) {

			$new_loan_servicing_checklistass[$kesy][] = $valuaea;

		}

		//	$arrayyy =array(127,133,1025,1004,1007,1010,1011,1012,1013,1015,1016,1017,1018,1019);
		//$arrayyyb = array(1017, 1027, 1028);
		$arrayyyb =array(1008,1041,1048,147,1013,1007,1042,1029,1010,1035,1016,1026,1049,1015,146,138);
		if (isset($arrayyyb)) {
			foreach ($arrayyyb as $numberssa) {

				foreach ($new_loan_servicing_checklistass[$numberssa] as $rowsas) {

					$servicing_array_finalsasds[$numberssa] = $rowsas;
				}
			}
		}

		$count = 0;
		foreach ($servicing_array_finalsasds as $ake => $vsal) {

			foreach ($checklist as $rowss) {

				if ($ake == $rowss->hud) {

					$count++;
					if ($count % 2 == 0) {
						$class_adds = "even";
					} else if ($count == 0) {
						$class_adds = "even";
					} else {
						$class_adds = "odd";
					}

					$check_checklist = '';
					if ($rowss->checklist == '1') {
						$check_checklist = 'X';

					} else {
						$check_checklist = '';

					}

					$check_not_applicable = '';
					if ($rowss->not_applicable == '1') {

						$check_not_applicable = 'X';
					} else {

						$check_not_applicable = '';
					}

					$header .= '<tr class="' . $class_adds . '">';
					$header .= '<td style="width:20%;text-align:center;">' . $check_checklist . '</td>';
					$header .= '<td style="width:20%;text-align:center;">' . $check_not_applicable . '</td>';
					if(in_array($rowss->hud, $checklist_Complete)){
						$header .= '<td style="width:60%;"><b>' . $item_namess[$rowss->hud] . '</b></td>';
					}else{
						$header .= '<td style="width:60%;">' . $item_namess[$rowss->hud] . '</td>';
					}
					$header .= '</tr>';

				}
			}

		}
		/*****************Loan Servicing******************/

		$header .= '</table>';

		// echo $header;
		$pdf->WriteHTML($header);

		$pdf->Output('Loan_ervicing_checklist.pdf', 'D');
	}

}

?>
