<?php

class Load_data extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('email');
		$this->load->model('User_model');
		$this->load->library('MYPDF');
		$this->load->library('Aws3','aws3');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');
		$this->load->helper('fci_helper.php');
		
		if ($this->session->userdata('t_user_id') != '') {
		} else {
			redirect(base_url() . "home", 'refresh');
		}
		
		ini_set('memory_limit', '-1');
	}



	public function GetDocusignAPIDocs($envID){

		
		$integratorKey = DOCUSIGN_INTEGRATOR_KEY;
		$email = DOCUSIGN_EMAIL;
		$password = DOCUSIGN_PASSWORD;
		$baseUrl 	= BASE_URL;

		// construct the authentication header:
		$header = "<DocuSignCredentials><Username>" . $email . "</Username><Password>" . $password . "</Password><IntegratorKey>" . $integratorKey . "</IntegratorKey></DocuSignCredentials>";

		/////////////////////////////////////////////////////////////////////////////////////////////////
	

		//get document status...
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $baseUrl.'/envelopes/'.$envID,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
		    'Accept: application/json',
		    "X-DocuSign-Authentication: $header"
		  ),
		));

		$response = curl_exec($curl);
		$responsesss = json_decode($response);

		return $responsesss;
	}

	public function downloadPdf()
	{
        $Download = $this->input->get('url');



		$integratorKey = DOCUSIGN_INTEGRATOR_KEY;
		$email = DOCUSIGN_EMAIL;
		$password = DOCUSIGN_PASSWORD;
		$baseUrl 	= BASE_URL.$Download;

		// construct the authentication header:
		$header = "<DocuSignCredentials><Username>" . $email . "</Username><Password>" . $password . "</Password><IntegratorKey>" . $integratorKey . "</IntegratorKey></DocuSignCredentials>";

		/////////////////////////////////////////////////////////////////////////////////////////////////
		// STEP 1 - Login (to retrieve baseUrl and accountId)
		/////////////////////////////////////////////////////////////////////////////////////////////////
		
		$curl = curl_init();


		curl_setopt_array($curl, array(
			  CURLOPT_URL => $baseUrl,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'GET',
			  CURLOPT_HTTPHEADER => array(
			  	 'Accept: application/pdf',
			    "X-DocuSign-Authentication: $header",
			    'is_download: true'
			  ),
		));

		$Result = curl_exec($curl);

		curl_close($curl);	

		header('Cache-Control: public'); 
		header('Content-type: application/pdf');
		header('Content-Disposition: attachment; filename="document.pdf"');
		header('Content-Length: '.strlen($Result));

		echo $Result;
	}
	

	public function index() {
		error_reporting(0);

		$userId = $this->session->userdata('t_user_id');

		$userData = $this->User_model->select_where('user', array('id'=>$userId,'role'=>1));
		if($userData->num_rows() > 0)
		{
			$userData = $userData->row();
			$data['remove_lender_from_loan'] = $userData->remove_lender_from_loan;
		}
		else
		{
			$data['remove_lender_from_loan'] = 'no';
		}

		$input_data_closing_statement = $this->config->item('input_data_closing_statement');

		//for loan originator...
		if ($this->session->userdata('user_role') == 1) {

			$user_settings = $this->User_model->query("SELECT loan_organization FROM `user_settings` WHERE `user_id`= '" . $this->session->userdata('t_user_id') . "' AND `loan_organization`='1'");
			if ($user_settings->num_rows() > 0) {
				$loginuser_loans = 2;
				$data['userLoginStatus']=1;
			} else {
				$loginuser_loans = 1;
				$data['userLoginStatus']=0;
			}
		} else {
			$loginuser_loans = 1;
			$data['userLoginStatus']=0;
		}

		//echo $loginuser_loans;

		if ($this->input->post('search_by')) {
			$option_select = $this->input->post('search_by');
			//echo $option_select;
			if ($option_select == 1) {
				if ($loginuser_loans == 1) {
					$perticularUserData=array();
					$fetch_option_data = $this->User_model->query("SELECT id,talimar_loan FROM loan ORDER BY id DESC");
				} else {
					$perticularUserData= $this->User_model->query("SELECT id,talimar_loan FROM loan WHERE t_user_id = '" . $this->session->userdata('t_user_id') . "' ORDER BY id DESC");
					$perticularUserData = $perticularUserData->result();
					$fetch_option_data = $this->User_model->query("SELECT id,talimar_loan FROM loan  ORDER BY id DESC");
				}

				if ($fetch_option_data->num_rows() > 0) {

					$fetch_option_data = $fetch_option_data->result();
					foreach ($fetch_option_data as $row) {

						$loanStatus = $this->User_model->query("SELECT loan_status FROM loan_servicing WHERE talimar_loan = '".$row->talimar_loan."' ");

						$loanStatus = $loanStatus->row();

						if ($loanStatus->loan_status == 1) {
							$loan_status = 'P';
						} elseif ($loanStatus->loan_status == 2) {
							$loan_status = 'A';
						} elseif ($loanStatus->loan_status == 3) {
							$loan_status = 'P';
						} elseif ($loanStatus->loan_status == 4) {
							$loan_status = 'C';
						} elseif ($loanStatus->loan_status == 5) {
							$loan_status = 'B';
						} elseif ($loanStatus->loan_status == 6) {
							$loan_status = 'T';
						} elseif ($loanStatus->loan_status == 7) {
							$loan_status = 'R';
						} else {
							$loan_status = 'N/A';
						}

						$fetch_search_option[] = array(
							'id' => $row->id,
							'text' => $row->talimar_loan,
							'loan_status' => $loan_status
						);
					}
					$data['fetch_search_option'] = $fetch_search_option;
					$data['all_particular_data'] = $perticularUserData;
					$data['selected_by'] = 1;
				} else {
					$this->session->set_flashdata('error', 'Error! No data found!');
					redirect(base_url() . "load_data", 'refresh');
				}
			} else if ($option_select == 2) {

				if ($loginuser_loans == 1) {
					$perticularUserData=array();
					$fetch_option_data = $this->User_model->query('SELECT id,fci from loan WHERE fci != "" ');
				} else {
					$perticularUserData= $this->User_model->query('SELECT id,fci from loan WHERE fci != "" AND t_user_id = ' . $this->session->userdata('t_user_id') . '');
					$perticularUserData = $perticularUserData->result();
					$fetch_option_data = $this->User_model->query('SELECT id,fci from loan WHERE fci != "" ');
				}

				if ($fetch_option_data->num_rows() > 0) {

					$fetch_option_data = $fetch_option_data->result();
					foreach ($fetch_option_data as $row) {

						$loanStatus = $this->User_model->query("SELECT loan_status FROM loan_servicing WHERE talimar_loan = '".$row->talimar_loan."' ");

						$loanStatus = $loanStatus->row();

						if ($loanStatus->loan_status == 1) {
							$loan_status = 'P';
						} elseif ($loanStatus->loan_status == 2) {
							$loan_status = 'A';
						} elseif ($loanStatus->loan_status == 3) {
							$loan_status = 'P';
						} elseif ($loanStatus->loan_status == 4) {
							$loan_status = 'C';
						} elseif ($loanStatus->loan_status == 5) {
							$loan_status = 'B';
						} elseif ($loanStatus->loan_status == 6) {
							$loan_status = 'T';
						} elseif ($loanStatus->loan_status == 7) {
							$loan_status = 'R';
						} else {
							$loan_status = 'N/A';
						}

						$fetch_search_option[] = array(
							'id' => $row->id,
							'text' => $row->fci,
							'loan_status' => $loan_status
						);
					}
					$data['fetch_search_option'] = $fetch_search_option;
					$data['all_particular_data'] = $perticularUserData;
					$data['selected_by'] = 2;
				} else {
					$this->session->set_flashdata('error', 'Error! No data found!');
					redirect(base_url() . "load_data", 'refresh');
				}
			} else if ($option_select == 3) {
				if ($loginuser_loans == 1) {
					$perticularUserData =array();
					$fetch_property_search = $this->User_model->query("select lp.talimar_loan, ph.property_address from loan_property as lp JOIN property_home as ph ON lp.property_home_id = ph.id WHERE ph.primary_property = '1' order by lp.id desc");
				} else {
					$perticularUserData= $this->User_model->query("select lp.loan_id as id,lp.talimar_loan, ph.property_address from loan_property as lp JOIN property_home as ph ON lp.property_home_id = ph.id JOIN loan ON lp.talimar_loan = loan.talimar_loan WHERE ph.primary_property = '1' AND (lp.t_user_id = '" . $this->session->userdata('t_user_id') . "' OR loan.loan_originator = '" . $this->session->userdata('t_user_id') . "') order by lp.id desc");
					$perticularUserData = $perticularUserData->result();
					$fetch_property_search = $this->User_model->query("select lp.talimar_loan, ph.property_address from loan_property as lp JOIN property_home as ph ON lp.property_home_id = ph.id JOIN loan ON lp.talimar_loan = loan.talimar_loan WHERE ph.primary_property = '1'  order by lp.id desc");
				
				}

				

				if ($fetch_property_search->num_rows() > 0) {

					$fetch_property_search = $fetch_property_search->result();
					foreach ($fetch_property_search as $row) {
						//$p_ta_loan['talimar_loan'] 	= $row->talimar_loan;

						$property_search = $this->User_model->query("SELECT id from loan WHERE talimar_loan='" . $row->talimar_loan . "' ");
						$property_search = $property_search->result();

						$loanStatus = $this->User_model->query("SELECT loan_status FROM loan_servicing WHERE talimar_loan = '".$row->talimar_loan."' ");

						$loanStatus = $loanStatus->row();

						if ($loanStatus->loan_status == 1) {
							$loan_status = 'P';
						} elseif ($loanStatus->loan_status == 2) {
							$loan_status = 'A';
						} elseif ($loanStatus->loan_status == 3) {
							$loan_status = 'P';
						} elseif ($loanStatus->loan_status == 4) {
							$loan_status = 'C';
						} elseif ($loanStatus->loan_status == 5) {
							$loan_status = 'B';
						} elseif ($loanStatus->loan_status == 6) {
							$loan_status = 'T';
						} elseif ($loanStatus->loan_status == 7) {
							$loan_status = 'R';
						} else {
							$loan_status = 'N/A';
						}

						$fetch_search_option[] = array(
							'id' => $property_search[0]->id,
							'text' => $row->property_address,
							'loan_status' => $loan_status
						);
					}
					$data['fetch_search_option'] = $fetch_search_option;
					$data['all_particular_data'] = $perticularUserData;
					$data['selected_by'] = 3;

				} else {

					$this->session->set_flashdata('error', 'Error! No data found!');
					redirect(base_url() . "load_data", 'refresh');
				}

			} else if ($option_select == 4) {
				$fetch_all_contact = $this->User_model->query("SELECT borrower_id,contact_firstname,contact_middlename,contact_lastname FROM contact WHERE borrower_id != ''");
				$fetch_all_contact = $fetch_all_contact->result();

				foreach ($fetch_all_contact as $c_row) {

					$loan_search = $this->User_model->query("SELECT loan.id FROM loan JOIN borrower_data as b JOIN borrower_contact as bc on bc.borrower_id = b.id AND b.id = loan.borrower WHERE bc.borrower_id = '" . $c_row->borrower_id . "' ");
					$loan_search = $loan_search->row();



					$fetch_search_option[] = array(
						'id' => $loan_search->id,
						'text' => $c_row->contact_firstname . ' ' . $c_row->contact_middlename . ' ' . $c_row->contact_lastname,
					);

				}

				$data['fetch_search_option'] = $fetch_search_option;
				$data['all_particular_data'] = array();
				$data['selected_by'] = 4;
			} else if ($option_select == 5) {

				$fetch_all_borrower = $this->User_model->query("select id from borrower_data");
				$fetch_all_borrower = $fetch_all_borrower->result();
				foreach ($fetch_all_borrower as $row) {
					//$p_ta_loan['borrower'] 		= $row->id;

					$borrower_search = $this->User_model->query("select id,b_name from loan where borrower='" . $row->id . "'");

					$borrower_search = $borrower_search->row();

					$fetch_search_option[] = array(
						'id' => $borrower_search->id,
						'text' => $row->b_name,
					);
				}
				
				$data['fetch_search_option'] = $fetch_search_option;
				$data['all_particular_data'] = array();
				$data['selected_by'] = 5;
			}
		} else {

			
			if ($loginuser_loans == 1) {
				$perticularUserData=array();
				$fetch_property_search = $this->User_model->query("select lp.talimar_loan, ph.property_address from loan_property as lp JOIN property_home as ph ON lp.property_home_id = ph.id WHERE ph.primary_property = '1' order by lp.id desc");
			} else {
				$perticularUserData= $this->User_model->query("select lp.loan_id as id,lp.talimar_loan, ph.property_address from loan_property as lp JOIN property_home as ph ON lp.property_home_id = ph.id JOIN loan ON lp.talimar_loan = loan.talimar_loan WHERE ph.primary_property = '1' AND (lp.t_user_id = '" . $this->session->userdata('t_user_id') . "' OR loan.loan_originator = '" . $this->session->userdata('t_user_id') . "') order by lp.id desc");
					$perticularUserData = $perticularUserData->result();
				$fetch_property_search = $this->User_model->query("select lp.talimar_loan, ph.property_address from loan_property as lp JOIN property_home as ph ON lp.property_home_id = ph.id JOIN loan ON lp.talimar_loan = loan.talimar_loan WHERE ph.primary_property = '1'  order by lp.id desc");
			}



			if ($fetch_property_search->num_rows() > 0) {

				$fetch_property_search = $fetch_property_search->result();
				foreach ($fetch_property_search as $row) {
					
					$property_search = $this->User_model->query("select id from loan where talimar_loan='" . $row->talimar_loan . "'");
					$property_search = $property_search->result();

					$loanStatus = $this->User_model->query("SELECT loan_status FROM loan_servicing WHERE talimar_loan = '".$row->talimar_loan."' ");

						$loanStatus = $loanStatus->row();

						if ($loanStatus->loan_status == 1) {
							$loan_status = 'P';
						} elseif ($loanStatus->loan_status == 2) {
							$loan_status = 'A';
						} elseif ($loanStatus->loan_status == 3) {
							$loan_status = 'P';
						} elseif ($loanStatus->loan_status == 4) {
							$loan_status = 'C';
						} elseif ($loanStatus->loan_status == 5) {
							$loan_status = 'B';
						} elseif ($loanStatus->loan_status == 6) {
							$loan_status = 'T';
						} elseif ($loanStatus->loan_status == 7) {
							$loan_status = 'R';
						} else {
							$loan_status = 'N/A';
						}

					$fetch_search_option[] = array(
						'id' => $property_search[0]->id,
						'text' => $row->property_address,
						'loan_status' => $loan_status
					);
				}
				$data['fetch_search_option'] = $fetch_search_option;
				$data['all_particular_data'] = $perticularUserData;
			}

		}

		//  END SEARCH OPTION
		$loan_id = $this->uri->segment(2);
		$loan_borrower_id = '';
		$talimar_borrower_id = '';
		if ($loan_id == 'new') {
			$data['btn_name'] = 'new';

		} else if ($loan_id) {
			$loan_id = $this->uri->segment(2);
			$fetch['id'] = $loan_id;
			$fetch_loan = $this->User_model->select_where('loan', $fetch);

			if ($fetch_loan->num_rows() > 0) {

				$fetch_loan_result = $fetch_loan->result();
				$data['fetch_loan_result'] = $fetch_loan_result;
				foreach ($fetch_loan_result as $row) {

					$data['modal_talimar_loan'] = $row->talimar_loan;
					$data['modal_priority'] = $row->position;
					$data['modal_intrest_rate'] = number_format($row->intrest_rate,3);
					$data['modal_org_amount'] = $row->loan_amount;
					$data['modal_current_balance'] = $row->current_balance;
					$data['modal_maturity_date'] = $row->maturity_date;
					$data['modal_baloon_payment_type'] = $row->baloon_payment_type;
					$data['modal_ballon_payment'] = $row->ballon_payment;

					$talimar_borrower_id = $row->borrower;
					$talimar_loan_number = $row->talimar_loan;
					$position_number = $row->position;

					// Check impound_account data
					$set_impund_account_values = $this->load_impound_account_data($talimar_loan_number);

					// Check closing_statement_load_items data
					$closing_statement1 = $this->load_closing_statement_data($talimar_loan_number);

					// Check under_writing_items data
					$underwriting = $this->underwriting_option_insert($talimar_loan_number);

					// Check servicing_checklist data
					$servicing_checklist = $this->servicing_checklist($talimar_loan_number);

					// Check input_datas_data data
					$input_datas_data1 = $this->input_datas_data($talimar_loan_number, $input_data_closing_statement, 'closing_statement');
					$loan_borrower_id = $row->borrower;

					if ($row->draws == 1) {

						$this->User_model->query("UPDATE `loan_reserve_checkbox` SET `date1_draw_request`= '1' where talimar_loan = '" . $talimar_loan_number . "'");
					}
				}

			} else {

				$this->session->set_flashdata('error', 'Not Found');
				redirect(base_url() . 'load_data');
			}

		} else {
			$data['modal_talimar_loan'] = '';
			$loan_id = '';
			$talimar_loan_number = '';
		}

		$talimar_loan_id['talimar_loan'] = $data['modal_talimar_loan'];
		// Count total property home with prroperty_address...

		$fetch_total_property_home = $this->User_model->query("SELECT COUNT(*) as total_row,property_address FROM property_home WHERE talimar_loan = '" . $data['modal_talimar_loan'] . "' AND property_address IS NOT NULL ");

		$result_total_property_home = $fetch_total_property_home->row();

		$data['fetch_total_property_home'] = $result_total_property_home->total_row;



		$data['fetch_total_property_home'] = $result_total_property_home->total_row;

		//fetch property address for every page...
		$fetch_property_addresss = $this->User_model->query("SELECT property_address FROM property_home WHERE talimar_loan = '" . $data['modal_talimar_loan'] . "' AND `primary_property`= '1'");
		$fetch_property_addresss = $fetch_property_addresss->row();
		$data['property_addresss'] = $fetch_property_addresss->property_address;

		//Lender Approval
		$fetch_LenderApproval = $this->User_model->query("SELECT * FROM `lender_approval` WHERE talimar_loan = '" . $data['modal_talimar_loan'] . "'");
		if($fetch_LenderApproval->num_rows() > 0){
			$fetch_LenderApproval = $fetch_LenderApproval->result();
			$data['LenderAppDAta'] = $fetch_LenderApproval;
		}else{
			$data['LenderAppDAta'] = '';
		}


		$fetch_proper = $this->User_model->query("Select id from property_home where talimar_loan='" . $talimar_loan_number . "'");
		if ($fetch_proper->num_rows() > 0) {
			$property_homee = $fetch_proper->result();

			foreach ($property_homee as $roow) {

				$p_id = $roow->id;

				$fetch_pro_query = $this->User_model->query("Select * from  loan_property where property_home_id='" . $p_id . "'");
				$property_homee_data = $fetch_pro_query->result();
				$data['property_homee_deta'][] = $property_homee_data;

			}

		}



		// Fetch property home
		$fetch_property_home = $this->User_model->select_where_asc('property_home', $talimar_loan_id, 'primary_property');
		if ($fetch_property_home->num_rows() > 0) {
			$property_home = $fetch_property_home->result();
			
			$data['fetch_property_home'] = $fetch_property_home->result();
		}

		$fetch_property_home = $this->User_model->select_where_asc('property_home', $talimar_loan_id, 'primary_property');
		if ($fetch_property_home->num_rows() > 0) {
			$property_home = $fetch_property_home->result();
			
			$data['fetch_property_home'] = $fetch_property_home->result();
		}
		$loan_iddd = $this->uri->segment(2);
		$fetch_loann = $this->User_model->query("Select talimar_loan from  loan where id='" . $loan_iddd . "'");

		$fetch_loan_resultt = $fetch_loann->result();
		$talii = $fetch_loan_resultt[0]->talimar_loan;
		$fetch_property_count = $this->User_model->query("SELECT count(*) as cout FROM `property_home` WHERE `talimar_loan`='" . $talii . "' AND `property_address`!=''");
		if ($fetch_property_count->num_rows() > 0) {

			$property_home_c = $fetch_property_count->result();

			$data['multii'] = $property_home_c[0]->cout;
		}

		if (isset($property_home)) {
			
			foreach ($property_home as $row) {
				
				$fetch_table_loan_property = $this->User_model->select_where('loan_property', array('property_home_id' => $row->id));

				if ($fetch_table_loan_property->num_rows() > 0) {
					$loan_property_result[$row->id] = $fetch_table_loan_property->result();
					// For primary Property

					$data['property_of_home_id'][$row->id] = $fetch_table_loan_property->row();

					if ($row->primary_property == 1) {
						$data['primary_loan_property'] = $fetch_table_loan_property->row();
					}
				}

				// fetch loan_property_taxes...
				$loan_property_taxes = $this->User_model->query("select * from loan_property_taxes where talimar_loan = '" . $row->talimar_loan . "' AND property_home_id = '" . $row->id . "'");
				$loan_property_taxes = $loan_property_taxes->result();
				foreach ($loan_property_taxes as $tax) {

					$talimar_loan = $tax->talimar_loan;
					$property_home_id = $tax->property_home_id;
					$loan_id = $tax->loan_id;
					$annual_tax = $tax->annual_tax;
					$mello_roos = $tax->mello_roos;
					$delinquent = $tax->delinquent;
					$delinquent_amount = $tax->delinquent_amount;
					$post_delinquent = $tax->post_delinquent;
					$post_delinquent_amount = $tax->post_delinquent_amount;
					$tax_assessor_phone = $tax->tax_assessor_phone;

					$loan_property_taxes_data = array(
						"talimar_loan" => $talimar_loan,
						"property_home_id" => $property_home_id,
						"loan_id" => $loan_id,
						"annual_tax" => $annual_tax,
						"mello_roos" => $mello_roos,
						"delinquent" => $delinquent,
						"delinquent_amount" => $delinquent_amount,
						"post_delinquent" => $post_delinquent,
						"post_delinquent_amount" => $post_delinquent_amount,
						"tax_assessor_phone" => $tax_assessor_phone,

					);
				}

				$data['property_taxes'] = $loan_property_taxes_data;
				$recording_informationn = $this->User_model->select_where('loan_recording_information', $talimar_loan_id);
				$recording_information_dataa = $recording_informationn->result();
				$data['recording_information'] = $recording_information_dataa;

				foreach ($data['recording_information'] as $rt) {

					$data['rn'] = $rt->record_number;
					$data['rd'] = $rt->recorded_date;
					$data['ro'] = $rt->record_option;
				}

				//-------------------Fetch eucmbrances data---------------
				$fetch_ecumbrances = $this->User_model->query("SELECT * FROM encumbrances WHERE talimar_loan = '" . $talimar_loan_number . "' AND property_home_id = '" . $row->id . "' order by proposed_priority");

				$fetch_ecumbrances = $fetch_ecumbrances->result();
				$ecumbrances_data[$row->id] = $fetch_ecumbrances;

				//-------- get senior liens sum from 'encumbrances' table -------
				$fetch_property_senior_liens = $this->User_model->query("SELECT SUM(current_balance) as current_balance FROM encumbrances WHERE talimar_loan = '" . $talimar_loan_number . "' AND property_home_id = '" . $row->id . "'  AND existing_priority_to_talimar = 1 ");

				$result_fetch_property_senior_liens = $fetch_property_senior_liens->row();

				$data['property_senior_liens'][$row->id] =

				$result_fetch_property_senior_liens->current_balance;

				$sql = "SELECT COUNT(*) as count_encum, SUM(current_balance) as sum_current_balance, existing_priority_to_talimar, original_balance FROM encumbrances WHERE property_home_id = '" . $row->id . "' AND talimar_loan = '" . $talimar_loan_number . "' AND will_it_remain = 0 ";
				$fetch_ecum_right = $this->User_model->query($sql);
				$fetch_ecum_right = $fetch_ecum_right->row();
				$data['ecum_right_data'][$row->id]['count_encum'] = $fetch_ecum_right->count_encum ? $fetch_ecum_right->count_encum : 0;
				$data['ecum_right_data'][$row->id]['sum_current_balance'] = $fetch_ecum_right->sum_current_balance ? $fetch_ecum_right->sum_current_balance : 0;
				$data['ecum_right_data'][$row->id]['existing_priority_to_talimar'] = $fetch_ecum_right->existing_priority_to_talimar ? $fetch_ecum_right->existing_priority_to_talimar : 0;
				$data['ecum_right_data'][$row->id]['original_balance'] = $fetch_ecum_right->original_balance ? $fetch_ecum_right->original_balance : 0;

				// Fetch _monthly income
				$fetch_monthly_income = $this->User_model->select_where('monthly_income', array('talimar_loan' => $talimar_loan_number, 'property_home_id' => $row->id));
				if ($fetch_monthly_income->num_rows() > 0) {
					$fetch_monthly_income_result[$row->id] = $fetch_monthly_income->result();
				}

				// Fetch Expense income
				$fetch_monthly_expense = $this->User_model->select_where('monthly_expense', array('talimar_loan' => $talimar_loan_number, 'property_home_id' => $row->id));
				if ($fetch_monthly_expense->num_rows() > 0) {
					$fetch_monthly_expense_result[$row->id] = $fetch_monthly_expense->result();
				}

				// FEtch Comparable_sale data
				$fetch_comparable_sale = $this->User_model->select_where_asc('comparable_sale', array('talimar_loan' => $talimar_loan_number, 'property_home_id' => $row->id), 'id');
				if ($fetch_comparable_sale->num_rows() > 0) {
					$fetch_comparable_sale_result[$row->id] = $fetch_comparable_sale->result();
				}

				// fetch propoerty_input_fields
				$fetch_propoerty_input_fields = $this->User_model->select_where('propoerty_input_fields', array('talimar_loan' => $talimar_loan_number, 'property_home_id' => $row->id));
				if ($fetch_propoerty_input_fields->num_rows() > 0) {
					$fetch_propoerty_input_fields = $fetch_propoerty_input_fields->result();

					foreach ($fetch_propoerty_input_fields as $p_i_f) {
						$data['propoerty_input_fields'][$row->id][$p_i_f->input_field] = $p_i_f->input_value;
						$data['propoerty_input_fieldss'][$row->id][$p_i_f->input_field] = $p_i_f->property_valuation_notes;
					}
				}
				//--------Fetch data from table loan_property_insurance----------//

				$property_loan['talimar_loan'] = $data['modal_talimar_loan'];
				$fetch_table_loan_property_insurance = $this->User_model->select_where('loan_property_insurance', $property_loan);

				$loan_property_insurance_result = $fetch_table_loan_property_insurance->row();
				$data['loan_property_insurance_result'] = $loan_property_insurance_result;

				//--------Fetch data from table loan_property_insurance----------//
				$loan_property_insurance_type = $this->config->item('loan_property_insurance_type');
				foreach ($loan_property_insurance_type as $insurance_key => $insurance_row) {
					$fetch_table_loan_property_insurance = $this->User_model->select_where('loan_property_insurance', array('talimar_loan' => $talimar_loan_number, 'property_home_id' => $row->id, 'insurance_type' => $insurance_key));
					if ($fetch_table_loan_property_insurance->num_rows() > 0) {
						$data['loan_property_insurance'][$row->id][$insurance_key] = $fetch_table_loan_property_insurance->row();
					}
				}

				// fetch loan_property_taxes
				$fetch_loan_property_taxes = $this->User_model->select_where('loan_property_taxes', array('talimar_loan' => $talimar_loan_number, 'property_home_id' => $row->id));

				if ($fetch_loan_property_taxes->num_rows() > 0) {
					$data['loan_property_taxes'][$row->id] = $fetch_loan_property_taxes->row();
				}

				if ($row->primary_property == 1) {
					// fetch loan_property_taxes
					$fetch_primary_loan_property_taxes = $this->User_model->select_where('loan_property_taxes', array('talimar_loan' => $talimar_loan_number, 'property_home_id' => $row->id));

					$data['primary_loan_property_taxes'] = $fetch_primary_loan_property_taxes->row();
				}

				// fetch property_folders
				$fetch_property_folders = $this->User_model->select_where('property_folders', array('talimar_loan' => $talimar_loan_number, 'property_home_id' => $row->id));
				if ($fetch_property_folders->num_rows() > 0) {
					$property_folders_result = $fetch_property_folders->result();
					$data['property_folders'][$row->id] = $property_folders_result;
					foreach ($property_folders_result as $row_p_f) {
						// fetch loan_property_images
						$fetch_loan_property_images = $this->User_model->select_where('loan_property_images', array('talimar_loan' => $talimar_loan_number, 'property_home_id' => $row->id, 'folder_id' => $row_p_f->id));
						if ($fetch_loan_property_images->num_rows() > 0) {
							$data['loan_property_images'][$row->id][$row_p_f->id] = $fetch_loan_property_images->result();
						}
					}
				}

			}

			$data['loan_property_result'] = $loan_property_result;
			$data['ecumbrances_data'] = $ecumbrances_data;
			if (isset($fetch_monthly_income_result)) {
				$data['fetch_monthly_income'] = $fetch_monthly_income_result;
			}

			if (isset($fetch_monthly_expense_result)) {
				$data['fetch_monthly_expense'] = $fetch_monthly_expense_result;
			}

			if (isset($fetch_comparable_sale_result)) {
				$data['fetch_comparable_sale'] = $fetch_comparable_sale_result;
			}

			//--------Fetch data from table loan_property_insurance with insurance type = 1 ----------//

			$property_loan1['talimar_loan'] = $data['modal_talimar_loan'];
			$property_loan1['insurance_type'] = 1;

			$fetch_table_loan_property_insurance1 = $this->User_model->query("SELECT sum(annual_insurance_payment) as annual_insurance_payment FROM loan_property_insurance WHERE talimar_loan = '" . $data['modal_talimar_loan'] . "' AND insurance_type = 1 ");

			// annual_insurance_payment
			$loan_property_insurance_result1 = $fetch_table_loan_property_insurance1->row();
			$data['loan_property_insurance_result_with_1'] = $loan_property_insurance_result1;

			//------------------------------------------------------//

			//--------Fetch data from table loan_property_insurance with insurance type = 2 ----------//

			$property_loan2['talimar_loan'] = $data['modal_talimar_loan'];
			$property_loan2['insurance_type'] = 2;

			// $fetch_table_loan_property_insurance2 	= $this->User_model->select_where('loan_property_insurance',$property_loan2);
			$fetch_table_loan_property_insurance2 = $this->User_model->query("SELECT sum(annual_insurance_payment) as annual_insurance_payment FROM loan_property_insurance WHERE talimar_loan = '" . $data['modal_talimar_loan'] . "' AND insurance_type = 2 ");

			$loan_property_insurance_result2 = $fetch_table_loan_property_insurance2->row();
			$data['loan_property_insurance_result_with_2'] = $loan_property_insurance_result2;

			$sql = "SELECT SUM(current_value) as sum_current_value, SUM(underwriting_value) as sum_underwriting_value FROM loan_property WHERE talimar_loan = '" . $data['modal_talimar_loan'] . "'";

			$fetch_sum = $this->User_model->query($sql);
			if ($fetch_sum->num_rows() > 0) {
				$fetch_sum = $fetch_sum->row();
				$data['loan_property_sum_current_value'] = $fetch_sum->sum_current_value;
				$data['loan_property_sum_underwriting_value'] = $fetch_sum->sum_underwriting_value;
			}

			//------------------------------------------------------//

			//--------Fetch data from table loan_property_insurance with insurance type = 3 ----------//

			$property_loan3['talimar_loan'] = $data['modal_talimar_loan'];
			$property_loan3['insurance_type'] = 3;

			// $fetch_table_loan_property_insurance3 	= $this->User_model->select_where('loan_property_insurance',$property_loan3);
			$fetch_table_loan_property_insurance3 = $this->User_model->query("SELECT sum(annual_insurance_payment) as annual_insurance_payment FROM loan_property_insurance WHERE talimar_loan = '" . $data['modal_talimar_loan'] . "' AND insurance_type = 3 ");

			$loan_property_insurance_result3 = $fetch_table_loan_property_insurance3->row();
			$data['loan_property_insurance_result_with_3'] = $loan_property_insurance_result3;

			//------------------------------------------------------//

		}

		$fetch_reserve_folders = $this->User_model->select_where('loan_reserve_folder', array('talimar_loan' => $talimar_loan_number));
		if ($fetch_reserve_folders->num_rows() > 0) {
			$fetch_reserve_folders_result = $fetch_reserve_folders->result();
			$data['loan_reserve_folders'] = $fetch_reserve_folders_result;
			foreach ($fetch_reserve_folders_result as $row_p_ff) {
				// fetch loan_property_images
				$fetch_loan_res_images = $this->User_model->select_where('loan_reserve_images', array('talimar_loan' => $talimar_loan_number, 'title' => $row_p_ff->id));
				if ($fetch_loan_res_images->num_rows() > 0) {
					$data['fetch_loan_res_images'][$row_p_ff->id] = $fetch_loan_res_images->result();
				}
			}
		}

		//fetch all users...
		$all_user = $this->User_model->query("Select * from user where account='1'");
		$data['all_user'] = $all_user->result();

		//fetch loan servicing contacts
		$servicing_contact = $this->User_model->query("SELECT * FROM `loan` where talimar_loan = '" . $data['modal_talimar_loan'] . "'");
		$data['servicing_contact_details'] = $servicing_contact->result();

		$internal_contact_query = $this->User_model->query("SELECT * FROM `loan_servicing_contacts` where talimar_loan = '" . $data['modal_talimar_loan'] . "'");
		$data['internal_contact_details'] = $internal_contact_query->result();

		// Fetch property home
		$fetch_current_balance = $this->User_model->query("SELECT SUM(current_balance) as current_balance FROM encumbrances WHERE position IN (1,2) AND talimar_loan = '" . $data['modal_talimar_loan'] . "' GROUP by talimar_loan");

		if ($fetch_current_balance->num_rows() > 0) {
			$fetch_sum_current_balance = $fetch_current_balance->row();

			$data['fetch_sum_current_balance'] = $fetch_sum_current_balance->current_balance;
		}

		//-----------Fetch table loan_monthly_payment  Data ---
		$monthly_payment['talimar_loan'] = $data['modal_talimar_loan'];
		$fetch_monthy_payment = $this->User_model->select_where('loan_monthly_payment', $monthly_payment);
		$result_monthly_payment = $fetch_monthy_payment->result();
		//-------------------------------------------------------------

		//-------------------Fetch Total amount from loan_monthly_payment table --
		$sql = "SELECT SUM(amount) as amount FROM loan_monthly_payment WHERE talimar_loan = '" . $data['modal_talimar_loan'] . "'";
		$amount_monthly_payment = $this->User_model->query($sql);
		$amount_monthly_payment = $amount_monthly_payment->result();

		$data['amount_monthly_payment'] = $amount_monthly_payment;

		//----------------------------------------------------------------------

		$servicing_default = $this->User_model->query("Select * from loan_servicing_default where talimar_loan = '" . $data['modal_talimar_loan'] . "'");
		$servicing_default = $servicing_default->result();
		$data['servicing_default'] = $servicing_default;

		$fetch_encumbrance_data1 = $this->User_model->select_where('encumbrances_data', $talimar_loan_id);
		if ($fetch_encumbrance_data1->num_rows() > 0) {
			$data['fetch_encumbrance_data_table'] = $fetch_encumbrance_data1->row();
		}

		$sql = "SELECT SUM(  `original_balance` ) AS sum_original, SUM(  `current_balance` ) AS sum_current, SUM(  `monthly_payment` ) AS sum_month, SUM(  `ballon_payment_b` ) AS sum_ballon FROM  `encumbrances` WHERE talimar_loan =  '" . $talimar_loan_number . "' AND  position = '1' AND senior_to_talimar = '1' ";
		
		$fetch_senior_tali = $this->User_model->query($sql);
		if ($fetch_senior_tali->num_rows() > 0) {
			$data['fetch_senior_tali'] = $fetch_senior_tali->result();
		}

		$fetch_encumbrance_totals1 = $this->User_model->select_where('ecumbrances_totals', $talimar_loan_id);
		if ($fetch_encumbrance_totals1->num_rows() > 0) {
			$data['fetch_encumbrance_totals'] = $fetch_encumbrance_totals1->row();
		}

		//---------------------------------------------------------

		//-------------------Fetch current eucmbrances data---------------
		$fetch_current_ecumbrances = $this->User_model->select_where_asc('current_encumbrances', $talimar_loan_id, 'priority,id');
		if ($fetch_current_ecumbrances->num_rows() > 0) {
			$fetch_current_ecumbrances = $fetch_current_ecumbrances->result();
			$data['fetch_current_ecumbrances'] = $fetch_current_ecumbrances;
		}

		//-------------------Fetch current eucmbrances data---------------
		$fetch_ecumbrances_tali_fac = $this->User_model->select_where('encumbrances', array('talimar_loan' => $talimar_loan_number, 'lien_holder' => 'TaliMar Financial'));
		if ($fetch_ecumbrances_tali_fac->num_rows() > 0) {
			$fetch_ecumbrances_tali_fac = $fetch_ecumbrances_tali_fac->result();
			$data['fetch_ecumbrances_tali_fac'] = $fetch_ecumbrances_tali_fac;
		}

		//-------------------Fetch closing_statement_items data---------------
		$fetch_closing_statement_items1 = $this->User_model->select_where_asc('closing_statement_items', $talimar_loan_id, 'hud');

		if ($fetch_closing_statement_items1->num_rows() > 0) {
			$fetch_closing_statement_items1 = $fetch_closing_statement_items1->result();
			$data['fetch_closing_statement_items_data'] = $fetch_closing_statement_items1;
		}

		$queryy = $this->User_model->query("SELECT talimar_loan from loan where id='" . $loan_id . "'");
		$queryy = $queryy->result();
		$talim = $queryy[0]->talimar_loan;

		$fetch_closing_statement_items11 = $this->User_model->query("select * from closing_statement_items where talimar_loan='" . $talim . "' AND hud='1005'");

		if ($fetch_closing_statement_items11->num_rows() > 0) {
			$fetch_closing_statement_items11 = $fetch_closing_statement_items11->result();
			$data['fetch_closing_statement_items_dataa'] = $fetch_closing_statement_items11;
		}

		$fetch_closing_statement_items111 = $this->User_model->query("select * from closing_statement_items where talimar_loan='" . $talim . "' AND hud='813'");

		if ($fetch_closing_statement_items111->num_rows() > 0) {
			$fetch_closing_statement_items111 = $fetch_closing_statement_items111->result();
			$data['fetch_closing_statement_items_dataaa'] = $fetch_closing_statement_items111;
		}

		$fetch_closing_statement_items11111 = $this->User_model->query("select * from closing_statement_items where talimar_loan='" . $talim . "' AND hud='1006'");

		if ($fetch_closing_statement_items11111->num_rows() > 0) {
			$fetch_closing_statement_items11111 = $fetch_closing_statement_items11111->result();
			$data['fetch_closing_statement_items_data_2'] = $fetch_closing_statement_items11111;
		}

		$loan_affiliated_parties = $this->config->item('loan_affiliated_parties');
		foreach ($loan_affiliated_parties as $keys => $insurance_row) {
			$affiliated_parties = $this->User_model->select_where('loan_affiliated_parties', array('talimar_loan' => $talimar_loan_number, 'position' => $keys));
			if ($affiliated_parties->num_rows() > 0) {
				$data['affiliated_parties'][$keys] = $affiliated_parties->row();
			}
		}

		$affiliated_parties = $this->User_model->select_where('loan_affiliated_parties', array('talimar_loan' => $talimar_loan_number));
		if ($affiliated_parties->num_rows() > 0) {
			$data['affiliated_parties_data'] = $affiliated_parties->result();
		} else {

			$data['affiliated_parties_data'] = '';
		}

		$query_payemnt_grantor = $this->User_model->select_where('payment_gurrantor', array('talimar_loan' => $talimar_loan_number));
		if ($query_payemnt_grantor->num_rows() > 0) {
			$data['fetch_payment_gurrantor'] = $query_payemnt_grantor->result();
		} else {

			$data['fetch_payment_gurrantor'] = '';
		}

		//...............add_extension data.................//

		$add_extension_d = $this->User_model->select_where('add_extension', array('talimar_loan' => $talimar_loan_number));
		if ($add_extension_d->num_rows() > 0) {
			$data['add_extension_data'] = $add_extension_d->result();
		}

		//...............add_forclosure data.................//

		$add_forclosure = $this->User_model->select_where('add_foreclosure_request', array('talimar_loan' => $talimar_loan_number));
		if ($add_forclosure->num_rows() > 0) {
			$data['add_forclosure_array'] = $add_forclosure->result();
			//echo $add_forclosure[0]->id;

		}

		//--------------------Fetch SUM closing_statement_item -----------------------
		$sql = "SELECT SUM(paid_to_other) as sum_paid_to_other, SUM(paid_to_broker) as sum_paid_to_broker FROM closing_statement_items WHERE talimar_loan = '" . $talimar_loan_number . "' ";

		$fetch_sum_closing_statement_item = $this->User_model->query($sql);
		$data['fetch_sum_closing_statement_item'] = $fetch_sum_closing_statement_item->result();

		//--------------------Fetch SUM closing_statement_item for direct_to_servicer checked...  -----------------------

		$fetch_sum_closing_statement_item = $this->User_model->query("SELECT SUM(paid_to_other) as sum_paid_to_other, SUM(paid_to_broker) as sum_paid_to_broker FROM closing_statement_items WHERE talimar_loan = '" . $talimar_loan_number . "' AND direct_to_servicer = '1' ");

		if ($fetch_sum_closing_statement_item->num_rows() > 0) {
			$data['fetch_suspense_balance'] = $fetch_sum_closing_statement_item->result();
		} else {

			$fetch_sum_closing_statement_item = $this->User_model->query("SELECT SUM(paid_to_other) as sum_paid_to_other, SUM(paid_to_broker) as sum_paid_to_broker FROM closing_statement_items WHERE talimar_loan = '" . $talimar_loan_number . "' AND hud IN(813,814)");

			$data['fetch_suspense_balance'] = $fetch_sum_closing_statement_item->result();

		}

		//--------------------Fetch SUM closing_statement_item -----------------------
		$fetch_input_datas1 = $this->User_model->select_where('input_datas', array('talimar_loan' => $talimar_loan_number, 'page' => 'closing_statement'));
		if ($fetch_input_datas1->num_rows() > 0) {
			$fetch_input_datas = $fetch_input_datas1->result();
			foreach ($fetch_input_datas as $key => $row) {
				$data['fetch_closing_input_datas'][$row->input_name] = $row->value;
			}

		}

		/*-------------- fetch Loan Boarding data --------------------*/

		$fetch_loan_boarding_data = $this->User_model->select_where_asc('loan_boarding_data', array('talimar_loan' => $talimar_loan_number), 'id');
		if ($fetch_loan_boarding_data->num_rows() > 0) {
			$fetch_loan_boarding_data = $fetch_loan_boarding_data->result();
			$data['loan_boarding_data'] = $fetch_loan_boarding_data;
		}else{
			$data['loan_boarding_data'] = '';
		}
		

		/*-------------- fetch loan_draw_schedule --------------------*/

		$fetch_loan_draw_schedule = $this->User_model->select_where('loan_draw_schedule', $talimar_loan_id);
		if ($fetch_loan_draw_schedule->num_rows() > 0) {
			$fetch_loan_draw_schedule = $fetch_loan_draw_schedule->result();
			foreach ($fetch_loan_draw_schedule as $valuedraw) {

				$fetchloan_draw_released = $this->User_model->query("SELECT SUM(amount_Req) as total_req, SUM(amount_app) as total_app, status FROM loan_draw_released WHERE draw_id = '".$valuedraw->id."'");
				if($fetchloan_draw_released->num_rows() > 0){
					$fetchloan_draw_released = $fetchloan_draw_released->result();
					$reqAmount = $fetchloan_draw_released[0]->total_req;
					$appAmount = $fetchloan_draw_released[0]->total_app;
					$status = $fetchloan_draw_released[0]->status;
				}else{
					$reqAmount = 0;
					$appAmount = 0;
					$status = '';
				}
				

				$loandrawdata[] = array(
											"id" => $valuedraw->id,
											"amount" => $valuedraw->amount,
											"description" => $valuedraw->description,
											"reqAmount" => $reqAmount,
											"appAmount" => $appAmount,
											"status" => $status,
											"remaining" => $valuedraw->amount - $appAmount,
										);
			}
			$data['fetch_loan_draw_schedule'] = $loandrawdata;
		}
		
		/*-------------- fetch forecloser in loan servicing --------------------*/

		$fetch_foreclosure = $this->User_model->select_where('foreclosure_cost', $talimar_loan_id);
		if ($fetch_foreclosure->num_rows() > 0) {
			$fetch_foreclosure = $fetch_foreclosure->result();
			$data['fetch_foreclosure_result'] = $fetch_foreclosure;
		}

		//--------------------All inputs of properties----------------------
		$fetch_input_datas1 = $this->User_model->select_where('input_datas', array('talimar_loan' => $talimar_loan_number, 'page' => 'property_insurance'));
		if ($fetch_input_datas1->num_rows() > 0) {
			$fetch_input_datas = $fetch_input_datas1->result();
			foreach ($fetch_input_datas as $key => $row) {
				$data['all_input_datas_of_property_modal'][$row->property_home_id][$row->page][$row->input_name] = $row->value;
			}

		}

		// -------------------- Fetch senior_ecumbrances ----------------
		$fetch_senior_ecumbrances = $this->User_model->select_where('senior_ecumbrances', array('talimar_loan' => $talimar_loan_number));
		if ($fetch_senior_ecumbrances->num_rows() > 0) {
			$data['senior_ecumbrances'] = $fetch_senior_ecumbrances->row();
		}

		//--------------------Fetch SUM closing_statement_item -----------------------
		$fetch_all_input_datas1 = $this->User_model->select_where('input_datas', array('talimar_loan' => $talimar_loan_number));
		if ($fetch_all_input_datas1->num_rows() > 0) {
			$fetch_all_input_datas = $fetch_all_input_datas1->result();
			foreach ($fetch_all_input_datas as $key => $row) {
				$data['fetch_all_input_datas'][$row->page][$row->input_name] = $row->value;
			}

		}

		//-------------------Fetch underwriting data---------------//

		

		$fetch_underwriting = $this->User_model->select_where('underwriting', array('talimar_loan' => $talimar_loan_number));
		if ($fetch_underwriting->num_rows() > 0) {
			$fetch_underwriting1 = $fetch_underwriting->result();
			$data['fetch_underwriting1'] = $fetch_underwriting1;
		}

		//-------------------Fetch loan_servicing_checklist data---------------//

		$sql = "SELECT * FROM `loan_servicing_checklist` WHERE `talimar_loan`='" . $talimar_loan_number . "' ORDER BY hud ASC";
		$servicing_checklist = $this->User_model->query($sql);

		if ($servicing_checklist->num_rows() > 0) {
			$servicing_checklist = $servicing_checklist->result();
			$data['servicing_checklist'] = $servicing_checklist;
		}

		//-------------------Fetch data for boarding status data---------------//
		$sqlboarding = "SELECT hud, `checklist` FROM `loan_servicing_checklist` WHERE `hud` IN('1008','1029','1035','1015') AND `talimar_loan`='" . $talimar_loan_number . "' ORDER BY hud ASC";
		$servicing_boarding = $this->User_model->query($sqlboarding);
		$servicing_boarding = $servicing_boarding->result();
		foreach ($servicing_boarding as $value) {
			
			$allArrayboarding[$value->hud] = $value->checklist;
		}
		$data['servicing_boarding'] = $allArrayboarding;



		$result_noticce = $this->User_model->query("select request_for_notice_talimar from encumbrances where loan_id='" . $this->uri->segment(2) . "'");

		$result_noticce = $result_noticce->result();

		$data['dummy'] = array(

			'req' => $result_noticce[0]->request_for_notice_talimar,

		);

		$sql = "SELECT SUM(current_balance) as sum_current_balance, SUM(original_balance) as sum_original_balance, SUM(monthly_payment) as sum_monthly_payment, SUM(ballon_payment_amount) as sum_ballon_payment_amount from current_encumbrances WHERE talimar_loan = '" . $talimar_loan_number . "' ";

		$fetch_sum_current_ecum = $this->User_model->query($sql);
		if ($fetch_sum_current_ecum->num_rows() > 0) {
			$data['fetch_sum_current_ecum'] = $fetch_sum_current_ecum->result();
		}
		//---------------------------------------------------------

		//------------------Fetch closing_statement data -------------
		$fetch_closing_statement = $this->User_model->select_where_asc('closing_statement', $talimar_loan_id, 'position');

		$fetch_closing_statement = $fetch_closing_statement->result();

		$data['closing_statement_data'] = $fetch_closing_statement;



		//------------------------------------------------------------
          

		$fetch_direct_to_servicer = $this->User_model->query("select SUM(paid_to_other) as sum_direct_to_servicer from closing_statement_items where talimar_loan='".$data['modal_talimar_loan']."' AND direct_to_servicer='1'");

		$fetch_direct_to_servicer = $fetch_direct_to_servicer->result();


		$data['sum_direct_to_servicer']=$fetch_direct_to_servicer[0]->sum_direct_to_servicer;




		$fetch_closing_deduction = $this->User_model->select_where('closing_statement_deductions', $talimar_loan_id);
		if ($fetch_closing_deduction->num_rows() > 0) {
			$data['fetch_closing_deduction'] = $fetch_closing_deduction->result();
		}


		//------------------Fetch extra_details data -------------
		$fetch_extra_details = $this->User_model->select_where('extra_details', $talimar_loan_id);

		$fetch_extra_details = $fetch_extra_details->result();

		$data['fetch_extra_details'] = $fetch_extra_details;
		//------------------------------------------------------------

		//------------------Fetch  loan_reserve_extra_field data -------------
		$reserve_extra_field = $this->User_model->select_where('loan_reserve_extra_field', $talimar_loan_id);

		$reserve_extra_field = $reserve_extra_field->result();

		$data['fetch_reserve_extra_field'] = $reserve_extra_field;

		//servicing_checklist_approval...
		$checklist_approval = $this->User_model->select_where('servicing_checklist_approval', $talimar_loan_id);
		$checklist_approval = $checklist_approval->result();
		$data['fetch_checklist_approval'] = $checklist_approval;

		//auto_debit_payment...
		$auto_debit_payment = $this->User_model->select_where('auto_debit_payment', $talimar_loan_id);
		$auto_debit_payment = $auto_debit_payment->result();
		$data['auto_debit_payment'] = $auto_debit_payment;

		//------------------Fetch ach_deposite data -------------
		$fetch_ach_deposite_details = $this->User_model->select_where('ach_deposite', $talimar_loan_id);

		$fetch_ach_deposite_details = $fetch_ach_deposite_details->result();

		$data['fetch_ach_deposite_details'] = $fetch_ach_deposite_details;
		//------------------------------------------------------------

		//------------------Fetch loan_escrow data for loan overview--------------------

		$fetch_escrow_data = $this->User_model->select_where('loan_escrow', $talimar_loan_id);
		if ($fetch_escrow_data->num_rows() > 0) {
			$fetch_escrow_data = $fetch_escrow_data->result();

			foreach ($fetch_escrow_data as $row) {

				$q = "select * from contact where contact_firstname ='" . $row->first_name . "' AND contact_lastname = '" . $row->last_name . "'";
				$fetch_contact = $this->User_model->query($q);
				$fetch_contact = $fetch_contact->result();

				$c_idd = $fetch_contact[0]->contact_id;

				$sql = "select contact_id,contact_firstname,contact_middlename,contact_lastname from contact where contact_id = '" . $row->first_name . "'";
				$fetch_contact = $this->User_model->query($sql);
				$fetch_contact = $fetch_contact->result();

				$contact = $fetch_contact ? $fetch_contact[0]->contact_firstname . ' ' . $fetch_contact[0]->contact_middlename . ' ' . $fetch_contact[0]->contact_lastname : '0';
				$contact1 = $fetch_contact[0]->contact_firstname;
				$c_id = $fetch_contact[0]->contact_id;

				$escrow_contact_name = array(
					"id" => $row->id,
					"talimar_loan" => $row->talimar_loan,
					"first_name" => $row->first_name,
					"last_name" => $row->last_name,
					"company" => $row->company,
					"escrow" => $row->escrow,
					"phone" => $row->phone,
					"address" => $row->address,
					"address_b" => $row->address_b,
					"city" => $row->city,
					"state" => $row->state,
					"zip" => $row->zip,
					"email" => $row->email,
					"first_payment" => $row->first_payment_close,
					"net_fund" => $row->net_fund,
					"tax_service" => $row->tax_service,
					"tax_service" => $row->tax_service,
					"record_assigment_close" => $row->record_assigment_close,
					"expiration" => $row->expiration,
					"instruction" => $row->instruction,
					"t_user_id" => $row->t_user_id,
					"full_name" => $contact,
					"full_fname" => $contact1,
					"c_id" => $c_id,
					"c_idd" => $c_idd,
				);
			}
			$data['escrow_contact_name'] = $escrow_contact_name;
		}

		//Fetch loan_escrow tab...
		$fetch_escrow_data = $this->User_model->select_where('loan_escrow', $talimar_loan_id);
		if ($fetch_escrow_data->num_rows() > 0) {
			$fetch_escrow_data = $fetch_escrow_data->result();
			$data['fetch_escrow_data'] = $fetch_escrow_data;

		}

		//-------------- end Fetch loan_escrow data ---------------//

		//----------------- start Fetch loan_title for loan overview -------------------------//

		$fetch_title_data = $this->User_model->select_where('loan_title', $talimar_loan_id);
		$fetch_title_data = $fetch_title_data->result();
		foreach ($fetch_title_data as $row) {

			$sql = "select contact_id,contact_firstname,contact_middlename,contact_lastname from contact where contact_id = '" . $row->first_name . "'";
			$fetch_title_contact = $this->User_model->query($sql);
			$fetch_title_contact = $fetch_title_contact->result();
			$id = $fetch_title_contact[0]->contact_id;

			$t_contact = $fetch_title_contact ? $fetch_title_contact[0]->contact_firstname . ' ' . $fetch_title_contact[0]->contact_middlename . ' ' . $fetch_title_contact[0]->contact_lastname : '0';

			$title_contact = array(
				"company" => $row->company,
				"first_name" => $row->first_name,
				"last_name" => $row->last_name,

				"phone" => $row->phone,
				"email" => $row->email,
				"full_name" => $t_contact,
				"idd" => $id,
				"prelim_receive" => $row->prelim_receive,
				"prelim_approve" => $row->prelim_approve,

			);

		}
		$data['title_contact'] = $title_contact;

		//Fetch loan_title...
		$fetch_title_data = $this->User_model->select_where('loan_title', $talimar_loan_id);
		$fetch_title_data = $fetch_title_data->result();
		$data['fetch_title_data'] = $fetch_title_data;

		//----------------- end Fetch loan_title for loan overview --------------------------------//

		

		//-----------------Fetch accured_charges data -----------------------------------------
		$fetch_accured_data = $this->User_model->select_where('accured_charges', $talimar_loan_id);
		$fetch_accured_data = $fetch_accured_data->result();

		$data['fetch_accured_data'] = $fetch_accured_data;

		//------------------ fetch wire instruction data ------------

		$fetch_wire_instruction = $this->User_model->select_where('wire_instruction', array('talimar_loan' => $talimar_loan_number, 'loan_id' => $loan_id));

		if ($fetch_wire_instruction->num_rows() > 0) {
			$data['fetch_wire_instruction'] = $fetch_wire_instruction->result();
		}
		//------------------ fetch draw wire instruction data ------------

		$fetch_draw_wire_instruction = $this->User_model->select_where('draw_wire_instructions', array('talimar_loan' => $talimar_loan_number, 'loan_id' => $loan_id));

		if ($fetch_draw_wire_instruction->num_rows() > 0) {
			$data['fetch_draw_wire_instruction'] = $fetch_draw_wire_instruction->result();
		}

		//------------------ fetch draw wire instruction data ------------

		$fetch_draw_wire_instruction = $this->User_model->select_where('wire_instruction2', array('talimar_loan' => $talimar_loan_number, 'loan_id' => $loan_id));

		if ($fetch_draw_wire_instruction->num_rows() > 0) {
			$data['fetch_draw_wire_instruction2'] = $fetch_draw_wire_instruction->result();
		}

		// FEtch borrower_data according to loan
		$loan_borrower_id_data['id'] = $loan_borrower_id;
		$fetch_loan_borrower = $this->User_model->select_where('borrower_data', $loan_borrower_id_data);
		if ($fetch_loan_borrower->num_rows() > 0) {

			$fetch_loan_borrower = $fetch_loan_borrower->result();
			foreach ($fetch_loan_borrower as $value) {
				
				$fetchborrowercontact = $this->User_model->query("SELECT c.contact_firstname, c.contact_lastname, c.contact_phone, c.contact_email, c.contact_fax, c.company_name_1 FROM borrower_contact as bc JOIN contact as c ON bc.contact_id = c.contact_id WHERE bc.borrower_id = '".$value->id."' AND bc.contact_primary = '1'");
				if($fetchborrowercontact->num_rows() > 0){
					$fetchborrowercontact = $fetchborrowercontact->result();

					$fetch_borrower_primary_contact[] = array(

										"contact_name" => $fetchborrowercontact[0]->contact_firstname.' '.$fetchborrowercontact[0]->contact_lastname,
										"contact_phone" => $fetchborrowercontact[0]->contact_phone,
										"contact_email" => $fetchborrowercontact[0]->contact_email,
										"contact_fax" => $fetchborrowercontact[0]->contact_fax,
										"company_name_1" => $fetchborrowercontact[0]->company_name_1,

									);

				}
			}
			$data['fetch_loan_borrower'] = $fetch_loan_borrower;
			$data['fetchborrowercontact'] = $fetch_borrower_primary_contact;
		}
		//---------------------------------------------------------------------------

		//---------------Fetch Borower_data table detail ----------------------------

		//echo $loginuser_loans;
		if ($loginuser_loans == 1) {
			//$fetch_borrower_data 		= $this->User_model->select_star_asc('borrower_data','b_name');
			$fetch_borrower_data = $this->User_model->query("select b_name,id from borrower_data ORDER BY b_name");
		} else {
			$where['t_user_id'] = $this->session->userdata('t_user_id');
			$fetch_borrower_data = $this->User_model->select_where_asc('borrower_data', $where, 'b_name');
		}

		if ($fetch_borrower_data->num_rows() > 0) {

			$fetch_borrower_data = $fetch_borrower_data->result();
			$data['fetch_borrower_data'] = $fetch_borrower_data;
		} else {
			$data['fetch_borrower_data'] = '';
		}

		//---------------------------------------------------------------------------

		//---------------Fetch user table detail ----------------------------
		$fetch_user_data = $this->User_model->query("select id,fname,middle_name,lname,remove_lender_from_loan from user");
		$fetch_user_data = $fetch_user_data->result();

		$data['fetch_userr'] = $fetch_user_data;

		//---------------------------------------------------------------------------

		//---------------Fetch loan_record_information table detail ----------------------------
		$recording_information = $this->User_model->select_where('loan_recording_information', $talimar_loan_id);
		$recording_information_data = $recording_information->result();
		$data['recording_information'] = $recording_information_data;

		// -------------- Fetch primary property data popup --------------------------------

		$primary_property_query = $this->User_model->query("SELECT * FROM property_home as h JOIN loan_property as lp on lp.property_home_id = h.id WHERE h.talimar_loan = '" . $data['modal_talimar_loan'] . "' AND primary_property = '1' ");

		$primary_property_result = $primary_property_query->result();
		$data['primary_property_result'] = $primary_property_result;

		//// -------------- Fetch loan fci and property address for outlook data popup --------------------------------

		$outlook_query = $this->User_model->query("SELECT * FROM loan as l JOIN loan_property as lp on l.talimar_loan = lp.talimar_loan WHERE l.talimar_loan = '" . $data['modal_talimar_loan'] . "'");

		$outlook_query = $outlook_query->result();
		$data['outlook_property'] = $outlook_query;
		foreach ($outlook_query as $row) {

			$outlook_borrower = $this->User_model->query("Select * from borrower_data where id = '" . $row->borrower . "'");
			$data['outlook_borrower'] = $outlook_borrower->result();

		}

		//..........fetch draw_request new amount...........//

		$loan_id = $this->uri->segment(2);
		$select_draww = $this->User_model->query("select *,SUM(date1_amount+date2_amount+date3_amount+date4_amount+date5_amount+date6_amount+date7_amount+date8_amount+date9_amount+date10_amount+date11_amount+date12_amount+date13_amount+date14_amount+date15_amount) as total_amount from  loan_reserve_draws  where loan_id='" . $loan_id . "'");

		$data['select_draww'] = $select_draww->result();

		//fetch additional loan...
		$fetch_additional_loan = $this->User_model->select_where('additional_loan', $talimar_loan_id);
		if ($fetch_additional_loan->num_rows() > 0) {
			$fetch_additional_loan = $fetch_additional_loan->result();
			$data['fetch_additional_loan_result'] = $fetch_additional_loan;

		}

		//---------------load purpose fetch data start ----------------------------//
		$fetch_additional_loann = $this->User_model->select_where('servicing_purpose', $talimar_loan_id);
		if ($fetch_additional_loann->num_rows() > 0) {
			$fetch_additional_loann = $fetch_additional_loann->result();
			$data['fetch_additional_loan_resultt'] = $fetch_additional_loann;

		}
		$a = $this->User_model->query("select talimar_loan from loan where id='" . $loan_id . "'");
		if ($a->num_rows() > 0) {
			$a = $a->result();
			$x = $a[0]->talimar_loan;

		}

		$lender_hash = $this->User_model->query("select count(*) as c from loan_assigment where talimar_loan='" . $x . "'");
		if ($lender_hash->num_rows() > 0) {
			$lender_hash = $lender_hash->result();
			$lc = $lender_hash[0]->c;
			$data['lender_co'] = $lc;

		}

		//---------------loanpurpose fetch data end ----------------------------//

		//---------------Fetch loan_source_and_uses table detail ----------------------------
		$fetch_loan_source_and_uses1 = $this->User_model->select_where_asc('loan_source_and_uses', $talimar_loan_id, 'id');
		if ($fetch_loan_source_and_uses1->num_rows() > 0) {
			$fetch_loan_source_and_uses = $fetch_loan_source_and_uses1->result();
			$data['fetch_loan_source_and_uses'] = $fetch_loan_source_and_uses;
		}

		// fetch sum of project cost from 'total_project_cost'  table...
		$fetch_total_project_cost = $this->User_model->query("SELECT SUM(project_cost) as total_project_cost FROM loan_source_and_uses WHERE talimar_loan = '" . $data['modal_talimar_loan'] . "' GROUP by talimar_loan");
		$result_total_project_cost = $fetch_total_project_cost->row();

		if ($fetch_total_project_cost->num_rows() > 0) {
			$data['total_project_cost_of_su'] = $result_total_project_cost->total_project_cost;
		} else {
			$data['total_project_cost_of_su'] = '';
		}
		//---------------------------------------------------------------------------

		// --------------Fetch servicing estimate lender data --------

		$servicing_estimate_lender = $this->User_model->select_where_asc('servicing_estimate_lender', $talimar_loan_id, 'position');

		if ($servicing_estimate_lender->num_rows() > 0) {
			$servicing_estimate_lender = $servicing_estimate_lender->result();
		} else {
			$servicing_estimate_lender = '';
		}
		$data['servicing_estimate_lender'] = $servicing_estimate_lender;

		//------------------------------------------------------------

		//---------------Fetch loan_serviing table detail ----------------------------
		$fetch['id'] = $this->uri->segment(2);
		$fetch_loan_data = $this->User_model->select_where('loan', $fetch);
		$fetch_loan_result = $fetch_loan_data->result();
		foreach ($fetch_loan_result as $row) {

			$data['loan_amountt'] = $row->loan_amount;
			$current_balance = $row->current_balance;
			$lender_fee = $row->lender_fee . '%';
			$data['fci'] = $row->fci;

			$data['intrest_ratee'] = number_format($row->intrest_rate,3);
			$data['c_date'] = $row->loan_funding_date;
			$data['payment_gurranty'] = $row->payment_gurranty;
		}

		$loan_servicing_data = $this->User_model->select_where('loan_servicing', $talimar_loan_id);

		if ($loan_servicing_data->num_rows() > 0) {

			$loan_servicing_data = $loan_servicing_data->result();
			$data['loan_servicing_data'] = $loan_servicing_data;

		}

		foreach ($data['loan_servicing_data'] as $xb) {

			$data['borad'] = $xb->boarding_fee_paid;
			$data['ser'] = $xb->servicing_disb;
		}

		$pay_off_processing = $this->User_model->select_where('pay_off_processing', $talimar_loan_id);

		if ($pay_off_processing->num_rows() > 0) {

			$pay_off_processing = $pay_off_processing->result_array();
			$data['pay_off_processing'] = $pay_off_processing;

		}

		$queryy = $this->User_model->query("SELECT talimar_loan from loan where id='" . $loan_id . "'");
		$queryy = $queryy->result();
		$talim = $queryy[0]->talimar_loan;

		$query = $this->User_model->query("SELECT investment,lender_name FROM `loan_assigment` WHERE `talimar_loan`='" . $talim . "'");
		$query = $query->result();

		foreach ($query as $roww) {
			$sum_invest = $roww->investment;
			$lender_id = $roww->lender_name;

			$queryyy = $this->User_model->query("SELECT vesting FROM `investor` WHERE `id`='" . $lender_id . "'");
			$queryyy = $queryyy->result();
			$vesting = $queryyy[0]->vesting;

			$data['final_array'][] = array(

				'sum_invest' => $sum_invest,
				'vesting' => $vesting,
			);

		}

		//---------- Fetch impound_account data---------------------

		$fetch_impound_account = $this->User_model->select_where_asc('impound_account', $talimar_loan_id, 'position');
		if ($fetch_impound_account->num_rows() > 0) {
			$data['fetch_impound_account'] = $fetch_impound_account->result();
		}

		$i_m_sum_sql = "SELECT SUM(amount) as sum_amount FROM impound_account WHERE talimar_loan = '" . $talimar_loan_number . "' AND ( impounded = '1' OR items = 'Mortgage Payment' ) ";

		$fetch_sum_imp_acc = $this->User_model->query($i_m_sum_sql);
		if ($fetch_sum_imp_acc->num_rows() > 0) {
			$fetch_sum_imp_acc = $fetch_sum_imp_acc->result();
			$data['sum_imp_acc'] = $fetch_sum_imp_acc[0]->sum_amount;
		}

		$sql = "SELECT SUM(amount) as sum_impounded FROM impound_account WHERE talimar_loan = '" . $talimar_loan_number . "' AND items != 'Mortgage Payment' AND impounded = '1' ";

		// echo "SELECT SUM(amount) as sum_impounded FROM impound_account WHERE impounded = '1' AND talimar_loan = '".$talimar_loan_number."' AND items != 'Mortgage Payment'";
		$fetch_sum_impound_account = $this->User_model->query($sql);
		$fetch_sum_impound_account = $fetch_sum_impound_account->result();
		$data['sum_impound_account'] = $fetch_sum_impound_account[0]->sum_impounded;
		//---------------Fetch loan_assigment table detail ----------------------------
		$loan_assigment_data = $this->User_model->select_where_asc('loan_assigment', $talimar_loan_id, 'position');

		$data['count_row_assigment'] = $loan_assigment_data->num_rows();

		$loan_assigment_data = $loan_assigment_data->result();
		// $data['loan_assigment_data'] = $loan_assigment_data;


		// Fetch all lender contacts from contact
		$where_contact_1['lender_contact_type'] = 1;
		$fetch_all_lender_contact = $this->User_model->select_where_asc('contact', $where_contact_1, 'contact_firstname');
		if ($fetch_all_lender_contact->num_rows() > 0) {
			$data['fetch_all_lender_contact'] = $fetch_all_lender_contact->result();
			$data['email_lender'] = $loan_borrower_result->contact_email;

		}



		foreach ($loan_assigment_data as $keyEID => $row) {



			$lender_id = $row->lender_name;

			$fetch_investor_of_assigmnet = $this->User_model->select_where('investor', array('id' => $lender_id));
			if ($fetch_investor_of_assigmnet->num_rows() > 0) {
				$investor_of_assigmnet = $fetch_investor_of_assigmnet->row();
				$data['all_lender_name_of_assigment'][] = $investor_of_assigmnet->name;
			}

			$sql = "SELECT * FROM lender_contact JOIN contact ON lender_contact.contact_id = contact.contact_id WHERE lender_contact.lender_id = '" . $lender_id . "' ";
			$fetch_lender_email_contact1 = $this->User_model->query($sql);
			if ($fetch_lender_email_contact1->num_rows() > 0) {
				$fetch_lender_email_contact = $fetch_lender_email_contact1->result();
				foreach ($fetch_lender_email_contact as $contact_data) {
					$data['email_contact'][$lender_id][] = array(
						"contact_name" => $contact_data->contact_firstname . ' ' . $contact_data->contact_middlename . ' ' . $contact_data->contact_lastname,
						"title" => $contact_data->c_title,
						"email" => $contact_data->contact_email,
						"phone" => $contact_data->contact_phone,
						"contact_id" => $contact_data->contact_id,
					);
				}
			}

			if(!empty($row->talimar_loan)){
			
				
				//call to signNo API
				$EnvolopID = $this->User_model->query("SELECT * FROM `signnow_api_doc` WHERE talimar_loan = '".$row->talimar_loan."' AND lender_id = '" . $lender_id . "' ORDER BY id DESC");

			

				if($EnvolopID->num_rows() > 0){
					
					
					$EnvolopID = $EnvolopID->row();

			
					$DocumentData = $this->GetDocusignAPIDocs($EnvolopID->documentID);
				

					if(!empty($DocumentData->status))
					{
						$row->EnvolopStatus = $DocumentData->status;
					}
					else
					{
						$row->EnvolopStatus = "Not Ready";
					}
				}
				else
				{
					
					$row->EnvolopStatus = "Not Ready";
				}
				

				$loan_assigment_data[$keyEID] = $row;
			}



		}
		$data['loan_assigment_data'] = $loan_assigment_data;	
		unset($talimar_loan);
		$talimar_loan['talimar_loan'] = $talimar_loan_number;

		$fetch_property_data = $this->User_model->select_where('loan_property', $talimar_loan);
		if ($fetch_property_data->num_rows() > 0) {
			$fetch_property_data = $fetch_property_data->result();

			$underwriting_valuee = $fetch_property_data[0]->underwriting_value;

		} else {

			$underwriting_valuee = '0';
		}

		$fetch_borrower = $this->User_model->select_where('loan', $talimar_loan);
		$fetch_borrower = $fetch_borrower->result();

		$loan_amountt = $fetch_borrower[0]->loan_amount;

		$loan_amount = $loan_amountt;

		$underwriting_value = $underwriting_valuee;
		$ltv = ($loan_amount / $underwriting_value) * 100;

		$data['ltv'] = $ltv;

		$sql_draws = "SELECT * FROM  `loan_draws` WHERE talimar_loan =  '" . $talimar_loan_number . "' ORDER BY convert(`draws`, decimal) ASC ";

		$loan_draws_data = $this->User_model->query($sql_draws);

		// fetch borrower details...

		$loan_borrower_data = $this->User_model->query("SELECT bd.b_name, bc.contact_id,c.contact_firstname , c.contact_lastname, c.contact_email  FROM borrower_data as bd JOIN borrower_contact as bc JOIN contact as c ON bd.id = bc.borrower_id AND bc.contact_id = c.contact_id WHERE bd.id = '" . $talimar_borrower_id . "'");

		$loan_draws_data = $loan_draws_data->result();
		$data['loan_draws_data'] = $loan_draws_data;
		if ($loan_borrower_data->num_rows() > 0) {

			$loan_borrower_result = $loan_borrower_data->row();
			$data['draws_borrower_name'] = $loan_borrower_result->b_name;
			$data['draws_contact_name'] = $loan_borrower_result->contact_firstname . ' ' . $loan_borrower_result->contact_lastname;

			$data['draws_contact_first_name'] = $loan_borrower_result->contact_firstname;
			$data['draws_contact_emailssss'] = $loan_borrower_result->contact_email;
		}

		// $loan_borrower_datas = $this->User_model->query("SELECT bd.b_name, bc.contact_id,c.contact_firstname , c.contact_lastname, c.contact_email  FROM borrower_data as bd JOIN borrower_contact as bc JOIN contact as c
		// 	ON bd.id = bc.borrower_id AND bc.contact_id = c.contact_id WHERE bd.id = '" . $talimar_borrower_id . "' ORDER BY bc.id ASC");

		/*08-07-2021 By @aj*/
		$loan_borrower_datas = $this->User_model->query("SELECT bd.b_name, bc.contact_id,c.contact_firstname ,bc.contact_responsibility, c.contact_lastname, c.contact_email  FROM borrower_data as bd JOIN borrower_contact as bc JOIN contact as c
			ON bd.id = bc.borrower_id AND bc.contact_id = c.contact_id WHERE bd.id = '" . $talimar_borrower_id . "' ORDER BY bc.contact_responsibility ASC");
		/*End*/

		if ($loan_borrower_datas->num_rows() > 0) {

			$loan_borrower_results = $loan_borrower_datas->result();

			$data['fetch_draws_contact_email'] = $loan_borrower_results;
			// $data['draws_contact_email2'] = $loan_borrower_results[1]->contact_email;

		}

		//---------------Fetch fee_disbursement table detail ----------------------------
		// $fee_disbursement_data = $this->User_model->select_where('fee_disbursement',$talimar_loan_id);
		$fee_disbursement_data = $this->User_model->query("SELECT * FROM fee_disbursement WHERE talimar_loan = '" . $data['modal_talimar_loan'] . "' ORDER by id ");

		if ($fee_disbursement_data->num_rows() > 0) {
			$fee_disbursement_data = $fee_disbursement_data->result();
			$data['fee_disbursement_data'] = $fee_disbursement_data;
		}
		$fetch['id'] = $this->uri->segment(2);
		$fetch_loan_data = $this->User_model->select_where('loan', $fetch);
		$data['fetch_loan_result'] = $fetch_loan_data->result();

		// count number of assignment...
		$loan_assigment_count = $this->User_model->select_where('loan_assigment', $talimar_loan_id);
		if ($loan_assigment_count->num_rows() > 0) {
			$data['loan_assigment_count'] = $loan_assigment_count->num_rows();
			$l_count = $loan_assigment_count->num_rows();
		}
		// FETCH PROPERT ALBUM DATA

		$fetch_property_album = $this->User_model->select_where_asc('property_album', $talimar_loan_id, 'id');
		if ($fetch_property_album->num_rows() > 0) {
			$fetch_property_album = $fetch_property_album->result();
			$data['fetch_property_album'] = $fetch_property_album;

			foreach ($fetch_property_album as $row) {
				$sql = 'SELECT * from property_images WHERE talimar_loan = "' . $row->talimar_loan . '" AND title = "' . $row->album_name . '" order by id';
				$fetch_image_album = $this->User_model->query($sql);
				$fetch_image_album = $fetch_image_album->result();

				$data['fetch_image_album_id'][$row->id] = $fetch_image_album;
			}
		}

		// Fetch Property Images
		$where_user1['marketing'] = '0';
		$where_user1['title'] = 'under_writing';
		$where_user1['talimar_loan'] = $talimar_loan_number;
		$fetch_property_images_data = $this->User_model->select_where('property_images', $where_user1);
		if ($fetch_property_images_data->num_rows() > 0) {
			$data['fetch_property_images'] = $fetch_property_images_data->result();
		}

		$sql = 'SELECT * FROM property_images WHERE talimar_loan = "' . $talimar_loan_number . '" AND marketing = "1"  AND title="under_writing" ';
		$fetch_marketing_property_image1 = $this->User_model->query($sql);
		if ($fetch_marketing_property_image1->num_rows() > 0) {
			$fetch_marketing_property_image = $fetch_marketing_property_image1->result();
			$data['fetch_marketing_property_image1'] = $fetch_marketing_property_image;
		}

		$sql = 'SELECT * FROM property_images WHERE talimar_loan = "' . $talimar_loan_number . '" AND marketing = "2" AND title="under_writing" ';
		$fetch_marketing_property_image2 = $this->User_model->query($sql);
		if ($fetch_marketing_property_image2->num_rows() > 0) {
			$fetch_marketing_property_image2 = $fetch_marketing_property_image2->result();
			$data['fetch_marketing_property_image2'] = $fetch_marketing_property_image2;
		}
		// Fetch Property Images
		$where_user['title'] = 'user';
		$where_user['talimar_loan'] = $talimar_loan_number;

		$fetch_property_images_user = $this->User_model->select_where('property_images', $where_user);
		if ($fetch_property_images_user->num_rows() > 0) {
			$data['fetch_user_property_images'] = $fetch_property_images_user->result();
		}

		$sql = "SELECT * FROM property_images WHERE talimar_loan = '" . $talimar_loan_number . "' AND title = 'user' AND marketing = '1' ";
		$fetch_user_street_level = $this->User_model->query($sql);
		if ($fetch_user_street_level->num_rows() > 0) {
			$fetch_user_street_level = $fetch_user_street_level->result();
			$data['fetch_user_street_level'] = $fetch_user_street_level;
		}

		$sql = "SELECT * FROM property_images WHERE talimar_loan = '" . $talimar_loan_number . "' AND title = 'user' AND marketing = '2' ";
		$fetch_user_area_map = $this->User_model->query($sql);
		if ($fetch_user_area_map->num_rows() > 0) {
			$fetch_user_area_map = $fetch_user_area_map->result();
			$data['fetch_user_area_map'] = $fetch_user_area_map;
		}

		// FEtch Loan distribution data

		$fetch_distribution_data = $this->User_model->select_where_asc('loan_distribution', $talimar_loan_id, 'id');
		if ($fetch_distribution_data->num_rows() > 0) {
			$fetch_distribution_data = $fetch_distribution_data->result();
			$data['fetch_distribution_data'] = $fetch_distribution_data;
		}

		// FETCH all loan_document_docs data ...

		$fetch_loan_document_docs = $this->User_model->query("SELECT * FROM loan_document_docs WHERE talimar_loan = '" . $talimar_loan_number . "' ");
		if ($fetch_loan_document_docs->num_rows() > 0) {
			$fetch_loan_document = $fetch_loan_document_docs->result();
			$data['fetch_loan_document'] = $fetch_loan_document;
		}
		// FETCH loan_document_docs data where doc_type  equal 1
		$sql = "SELECT * FROM loan_document_docs WHERE talimar_loan = '" . $talimar_loan_number . "' AND doc_type ='1' ";
		$fetch_loan_document_docs1 = $this->User_model->query($sql);
		if ($fetch_loan_document_docs1->num_rows() > 0) {
			$fetch_loan_document_1 = $fetch_loan_document_docs1->result();
			$data['fetch_loan_document_1'] = $fetch_loan_document_1;
		}

		// FETCH loan_document_docs data where doc_type  equal 2
		$sql = "SELECT * FROM loan_document_docs WHERE talimar_loan = '" . $talimar_loan_number . "' AND doc_type ='2' ";
		$fetch_loan_document_docs2 = $this->User_model->query($sql);
		if ($fetch_loan_document_docs2->num_rows() > 0) {
			$fetch_loan_document_2 = $fetch_loan_document_docs2->result();
			$data['fetch_loan_document_2'] = $fetch_loan_document_2;
		}

		// FETCH loan_document_docs data where doc_type  equal 3
		$sql = "SELECT * FROM loan_document_docs WHERE talimar_loan = '" . $talimar_loan_number . "' AND doc_type ='3' ";
		$fetch_loan_document_docs3 = $this->User_model->query($sql);
		if ($fetch_loan_document_docs3->num_rows() > 0) {
			$fetch_loan_document_3 = $fetch_loan_document_docs3->result();
			$data['fetch_loan_document_3'] = $fetch_loan_document_3;
		}

		// FETCH loan_document_docs data where doc_type  equal 4
		$sql = "SELECT * FROM loan_document_docs WHERE talimar_loan = '" . $talimar_loan_number . "' AND doc_type = '4' ";
		$fetch_loan_document_docs4 = $this->User_model->query($sql);
		if ($fetch_loan_document_docs4->num_rows() > 0) {
			$fetch_loan_document_4 = $fetch_loan_document_docs4->result();
			$data['fetch_loan_document_4'] = $fetch_loan_document_4;
		}

		// FETCH loan_document_docs data where doc_type  equal 5
		$sql = "SELECT * FROM loan_document_docs WHERE talimar_loan = '" . $talimar_loan_number . "' AND doc_type = '5' ";
		$fetch_loan_document_docs5 = $this->User_model->query($sql);
		if ($fetch_loan_document_docs5->num_rows() > 0) {
			$fetch_loan_document_5 = $fetch_loan_document_docs5->result();
			$data['fetch_loan_document_5'] = $fetch_loan_document_5;
		}

		// FETCH loan_document_docs data where doc_type  equal 6
		$sql = "SELECT * FROM loan_document_docs WHERE talimar_loan = '" . $talimar_loan_number . "' AND doc_type = '6' ";
		$fetch_loan_document_docs6 = $this->User_model->query($sql);
		if ($fetch_loan_document_docs6->num_rows() > 0) {
			$fetch_loan_document_6 = $fetch_loan_document_docs6->result();
			$data['fetch_loan_document_6'] = $fetch_loan_document_6;
		}

		// FETCH loan_document_docs data where doc_type equal 0
		$sql = "SELECT * FROM loan_document_docs WHERE talimar_loan = '" . $talimar_loan_number . "' AND doc_type = '0' ";
		$fetch_loan_document_docs_0 = $this->User_model->query($sql);
		if ($fetch_loan_document_docs_0->num_rows() > 0) {
			$fetch_loan_document_0 = $fetch_loan_document_docs_0->result();
			$data['fetch_loan_document_0'] = $fetch_loan_document_0;
		}

		// Fetch Sum comparable_sale
		$sql = "SELECT COUNT(*) as count_comparable, SUM(sq_ft) as sum_sq_ft, SUM(year_built) as sum_year_built, SUM(bedrooms) as sum_bedrooms, SUM(bathroom) as sum_bathroom, SUM(lot_size) as sum_lot_size, SUM(sale_price) as sum_sale_price, SUM(per_sq_ft) as sum_per_sq_ft FROM comparable_sale WHERE talimar_loan = '" . $talimar_loan_number . "' ";
		$fetch_sum_comparable_data = $this->User_model->query($sql);
		$fetch_sum_comparable_data = $fetch_sum_comparable_data->result();
		$data['fetch_sum_comparable_data'] = $fetch_sum_comparable_data;

		$sql = 'SELECT SUM(disbursement) as total_disbursement FROM loan_distribution WHERE talimar_loan = "' . $talimar_loan_number . '" ';
		$sum_total_disbursement = $this->User_model->query($sql);
		$sum_total_disbursement = $sum_total_disbursement->result();
		$total_sum_disbursement = $sum_total_disbursement[0]->total_disbursement;
		$data['total_sum_disbursement'] = $total_sum_disbursement;
		//---------------Fetch total of draws_amount in loan_draws table
		$sql = "SELECT SUM(draws_amount) as draws_amount , SUM(draws_release_amount) as  draws_release_amount , SUM(draws_remaining) as draws_remaining FROM loan_draws WHERE talimar_loan = '" . $talimar_loan_number . "'";
		$total_draws_amount = $this->User_model->query($sql);
		if ($total_draws_amount->num_rows() > 0) {
			$total_draws_amount = $total_draws_amount->result();
			$data['total_draws_amount'] = $total_draws_amount[0]->draws_amount;
			$data['total_draws_release_amount'] = $total_draws_amount[0]->draws_release_amount;
			$data['total_draws_remaining'] = $total_draws_amount[0]->draws_remaining;
		} else {
			$data['total_draws_amount'] = '';
		}
		//--------------------------fetch all data for Investor---------------------------------

		$investor_all_data = $this->User_model->select_star_asc('investor', 'name');

		$investor_all_data = $investor_all_data->result();
		$data['investor_all_data'] = $investor_all_data;

		//---------------------------------------------------------------------------

		//---------------Fetch loan_notes table detail ----------------------------
		$loan_notes_fetch = $this->User_model->select_where_desc('loan_notes', $talimar_loan_id, 'notes_date');

		if ($loan_notes_fetch->num_rows() > 0) {
			$loan_notes_fetch = $loan_notes_fetch->result();
			$data['loan_notes_fetch'] = $loan_notes_fetch;
		} else {
			$data['loan_notes_fetch'] = '';
		}
		//---------------------------------------------------------------------------

		//fetch contact notes for logged in user...

		$sql = $this->User_model->query("SELECT * FROM `contact_notes` as cn JOIN contact as c ON cn.contact_id = c.contact_id WHERE cn.user_id='" . $this->session->userdata('t_user_id') . "'");
		

		if ($sql->num_rows() > 0) {
			$contact_notes_fetch = $sql->result();
			$data['contact_notes_fetch'] = $contact_notes_fetch;
		} else {
			$data['contact_notes_fetch'] = '';
		}

		//fetch contact notes for each talimar loan...

		 $sql = $this->User_model->query("SELECT * FROM `contact_notes` as cn JOIN contact as c ON cn.contact_id = c.contact_id WHERE cn.loan_id='" . $this->uri->segment(2) . "' ORDER BY notes_date DESC");



		if ($sql->num_rows() > 0) {
			$contact_notes_fetch1 = $sql->result();
			foreach ($contact_notes_fetch1 as $row) {

				$users = $this->User_model->query("Select * from user where id = '" . $row->user_id . "'");
				$fetch_user = $users->result();
				$user_name = $fetch_user[0]->fname . ' ' . $fetch_user[0]->middle_name . ' ' . $fetch_user[0]->lname;

				$contact_notes_users[] = array(
					"id" => $row->id,
					"contact_id" => $row->contact_id,
					"notes_date" => $row->notes_date,
					"notes_text" => $row->notes_text,
					"c_name" => $row->contact_firstname . ' ' . $row->contact_middlename . ' ' . $row->contact_lastname,
					"entry_by" => $row->entry_by,
					"loan_note_type" => $row->loan_note_type,
					"com_type" => $row->com_type,

				);

			}

			$data['contact_notes_users'] = $contact_notes_users;
		}




		//add_contact_loan_note table data show in loan notes...

		$sql1 = $this->User_model->query("SELECT * FROM `add_contact_loan_note` as acln JOIN contact as c ON acln.contact_id = c.contact_id WHERE acln.talimar_loan='" . $talimar_loan_number . "' ORDER BY today_date DESC");

		if ($sql1->num_rows() > 0) {
			$add_contact_loan_note = $sql1->result();
			$data['add_contact_loan_note'] = $add_contact_loan_note;
		} else {
			$data['add_contact_loan_note'] = '';
		}

		//fetch loan_reserve_date and loan_reserve_draws data...

		$fetch_data = "SELECT * FROM loan_reserve_date WHERE talimar_loan = '" . $talimar_loan_number . "'";
		$fetch_data = $this->User_model->query($fetch_data);
		$fetch_data = $fetch_data->result();
		$data['reserve_date'] = $fetch_data;

		$fetch_data1 = "SELECT * FROM loan_reserve_draws WHERE talimar_loan = '" . $talimar_loan_number . "'";
		$fetch_data1 = $this->User_model->query($fetch_data1);
		$fetch_data1 = $fetch_data1->result();
		$data['reserve_draws'] = $fetch_data1;

		$fetch_data2 = "SELECT * FROM loan_reserve_checkbox WHERE talimar_loan = '" . $talimar_loan_number . "'";
		$fetch_data2 = $this->User_model->query($fetch_data2);
		$fetch_data2 = $fetch_data2->result();
		$data['reserve_checkbox'] = $fetch_data2;

		// ----------Fetch borrower Contact detail---------------------

		$borrower_id = isset($fetch_loan_result) ? $fetch_loan_result[0]->borrower : '';
		$fetch_borrower_contact1 = $this->User_model->query("select * from borrower_contact where borrower_id='".$borrower_id."' order by contact_responsibility  asc");

		if ($fetch_borrower_contact1->num_rows() > 0) {
			$fetch_borrower_contact = $fetch_borrower_contact1->result();

			foreach ($fetch_borrower_contact as $ky => $vae) {

					$abc[$vae->contact_id]=$vae->contact_responsibility;
				}

			foreach ($fetch_borrower_contact as $key => $value) {
				
				$b_con_id['contact_id'] = $value->contact_id;
			
				$fetch_b_contact = $this->User_model->select_where('contact', $b_con_id);

				$b_contacts = $fetch_b_contact->result();
				foreach ($b_contacts as $key => $values) {
				
						$b_contact[]=array(

									"first"=>$values->contact_firstname,
									"middle"=>$values->contact_middlename,
									"last"=>$values->contact_lastname,
									"contact_id"=>$values->contact_id,
									"contact_email"=>$values->contact_email,
									"contact_phone"=>$values->contact_phone,
									"b_signed"=>$abc[$values->contact_id],


									);

				

			}
		  }			$data['b_contact']=$b_contact;

				
				
		}

		if ($fetch_borrower_contact1->num_rows() > 0) {
			$fetch_borrower_contact = $fetch_borrower_contact1->result();
			// ---------------------- Borrower Contact 1 Section ------------------

			$data['fetch_b1_responsbility'] = $fetch_borrower_contact[0]->contact_responsibility;
			$data['fetch_b2_responsbility'] = $fetch_borrower_contact[1]->contact_responsibility;
			$data['fetch_b3_responsbility'] = $fetch_borrower_contact[2]->contact_responsibility;
			$data['fetch_b4_responsbility'] = $fetch_borrower_contact[3]->contact_responsibility;

			if (isset($fetch_borrower_contact[0]->contact_id)) {
				$b_con_id['contact_id'] = $fetch_borrower_contact[0]->contact_id;
				$fetch_b_contact = $this->User_model->select_where('contact', $b_con_id);

				if ($fetch_b_contact->num_rows() > 0) {
					$b_contact = $fetch_b_contact->result();
					$data['select_borrower_contact1'] = $b_contact[0]->contact_firstname . ' ' . $b_contact[0]->contact_middlename . ' ' . $b_contact[0]->contact_lastname;

					$data['select_borrower_contact1_id'] = $b_contact[0]->contact_id;
					$data['select_borrower_contact1_email'] = $b_contact[0]->contact_email;
					$data['select_borrower_contact1_phone'] = $b_contact[0]->contact_phone;
					$data['select_borrower_contact1_title'] = $fetch_borrower_contact[0]->c_title;
				}

			}
			// ---------------------- Borrower Contact 2 Section -----------------

			if (isset($fetch_borrower_contact[1]->contact_id)) {
				$b_con_id['contact_id'] = $fetch_borrower_contact[1]->contact_id;
				$fetch_b_contact = $this->User_model->select_where('contact', $b_con_id);
				if ($fetch_b_contact->num_rows() > 0) {
					$b_contact = $fetch_b_contact->result();
					$data['select_borrower_contact2'] = $b_contact[0]->contact_firstname . ' ' . $b_contact[0]->contact_middlename . ' ' . $b_contact[0]->contact_lastname;

					$data['select_borrower_contact2_id'] = $b_contact[0]->contact_id;
					$data['select_borrower_contact2_email'] = $b_contact[0]->contact_email;
					$data['select_borrower_contact2_phone'] = $b_contact[0]->contact_phone;
					$data['select_borrower_contact2_title'] = $fetch_borrower_contact[1]->c_title;
				}

			}

			// ---------------------- Borrower Contact 3 Section -----------------

			if (isset($fetch_borrower_contact[2]->contact_id)) {
				$b_con_id['contact_id'] = $fetch_borrower_contact[2]->contact_id;
				$fetch_b_contact = $this->User_model->select_where('contact', $b_con_id);
				if ($fetch_b_contact->num_rows() > 0) {
					$b_contact = $fetch_b_contact->result();
					$data['select_borrower_contact3'] = $b_contact[0]->contact_firstname . ' ' . $b_contact[0]->contact_middlename . ' ' . $b_contact[0]->contact_lastname;

					$data['select_borrower_contact3_id'] = $b_contact[0]->contact_id;
					$data['select_borrower_contact3_email'] = $b_contact[0]->contact_email;
					$data['select_borrower_contact3_phone'] = $b_contact[0]->contact_phone;
					$data['select_borrower_contact3_title'] = $fetch_borrower_contact[2]->c_title;
				}

			}

			// ---------------------- Borrower Contact 4 Section -----------------

			if (isset($fetch_borrower_contact[3]->contact_id)) {
				$b_con_id['contact_id'] = $fetch_borrower_contact[3]->contact_id;
				$fetch_b_contact = $this->User_model->select_where('contact', $b_con_id);
				if ($fetch_b_contact->num_rows() > 0) {
					$b_contact = $fetch_b_contact->result();
					$data['select_borrower_contact4'] = $b_contact[0]->contact_firstname . ' ' . $b_contact[0]->contact_middlename . ' ' . $b_contact[0]->contact_lastname;

					$data['select_borrower_contact4_id'] = $b_contact[0]->contact_id;
					$data['select_borrower_contact4_email'] = $b_contact[0]->contact_email;
					$data['select_borrower_contact4_phone'] = $b_contact[0]->contact_phone;
					$data['select_borrower_contact4_title'] = $fetch_borrower_contact[3]->c_title;
				}

			}
		}

		$borrower_contact_ids1 = $data['select_borrower_contact1_id'] ? $data['select_borrower_contact1_id'] : '';
		$borrower_contact_ids_data = $data['select_borrower_contact2_id'] ? $borrower_contact_ids1 . ',' . $data['select_borrower_contact2_id'] : $borrower_contact_ids1;

		$fetch_contact_notes_sql = "SELECT cn.contact_id, cn.notes_date, cn.notes_type ,cn.notes_text FROM contact_notes as cn WHERE loan_id = '" . $loan_id . "' AND contact_id IN (" . $borrower_contact_ids_data . ") ORDER BY cn.notes_date DESC";

		if ($borrower_contact_ids_data) {
			$fetch_contact_notes_result = $this->User_model->query($fetch_contact_notes_sql);
			if ($fetch_contact_notes_result->num_rows() > 0) {
				$data['contact_notes_result'] = $fetch_contact_notes_result->result();
			}
		}

		//fetch fci boarding data...
		$fciboarding = $this->User_model->query("SELECT * FROM fci_boarding WHERE talimar_loan='".$talimar_loan_number."' ORDER BY datetime DESC");
		if($fciboarding->num_rows() > 0){
			$data['fciboardingdata'] = $fciboarding->result();
		}

		// fetch vendors details...

		$result = $this->User_model->select_star('vendors');
		$result_new = $result->result();
		$data['all_vendor_data'] = $result_new;

		$user_data['t_user_id'] = $this->session->userdata('t_user_id');
		$result = $this->User_model->select_star('loan');
		$result_new = $result->result();

		$data['fetch_loans'] = $result_new;
		$data['result_monthly_payment'] = $result_monthly_payment;

		// $data['loan_id']	= $loan_id;

		$fetch_all_actives_query = $this->User_model->query("SELECT current_balance FROM loan WHERE talimar_loan='" . $data['modal_talimar_loan'] . "' ");
		if ($fetch_all_actives_query->num_rows() > 0) {

			$active_loan_am = $fetch_all_actives_query->result();
			$data['outstanding_balance'] = $active_loan_am[0]->current_balance ? $active_loan_am[0]->current_balance : '';

		}

		//------------------ automatic email send to active loan 5 day prior end ---------------------//

		if($talimar_loan_number !=''){
			$this->updateallloans_paytotal($talimar_loan_number);  //call to update paymenttotal
			
			//call to signNo API
			$getdocuments = $this->User_model->query("SELECT * FROM `signnow_api_doc` WHERE talimar_loan = '".$talimar_loan_number."' ORDER BY id desc" );
			if($getdocuments->num_rows() > 0){
				
				$getdocuments = $getdocuments->result();

				$timeccchhh = date("H");
				foreach ($getdocuments as $valueID) {

					//if($timeccchhh == '12'){

						$DocumentData = $this->GetDocusignAPIDocs($valueID->documentID);
						//$data['lenderSignDoc'][] = $DocumentData;
						$data['lenderSignDoc'][$valueID->lender_id][] = $DocumentData;
					//}

				}
			}

			$fciData = $this->User_model->query('SELECT * FROM lender_payment_history where talimar_loan = "'.$talimar_loan_number.'" ORDER BY id DESC');

			if($fciData->num_rows() > 0)
			{
				$fciData = $fciData->row();
				$data['fciData'] = $fciData;
			}
			else{
				$data['fciData'] = array();
			}

			$data['loanDetailsFCI'] = loanDetailsFci($fciData->loanAccount);


		}




		$data['content'] = $this->load->view('loan_overview/index', $data, true);
		$this->load->view('template_files/template_with_sidebar', $data);

	}

	public function updateallloans_paytotal($talimar_loan_number){

		$getassignment = $this->User_model->select_where('loan_assigment',array('talimar_loan'=>$talimar_loan_number));
		if($getassignment->num_rows() > 0){

			$getassignment = $getassignment->result();

			$fetchloanAmount = $this->User_model->query("SELECT loan_amount FROM loan WHERE talimar_loan = '".$talimar_loan_number."'");
			$fetchloanAmount = $fetchloanAmount->result();
			$totalLoanAmount = $fetchloanAmount[0]->loan_amount;


			$fetchservicing = $this->User_model->query("SELECT servicing_lender_rate FROM loan_servicing WHERE talimar_loan = '".$talimar_loan_number."'");
			$fetchservicing = $fetchservicing->result();
			$servicing_lender_ratess = $fetchservicing[0]->servicing_lender_rate;

			if($servicing_lender_ratess == 'NaN' || $servicing_lender_ratess == '' || $servicing_lender_ratess == '0.00'){

				$investor_yeild = str_replace("%", "", $getassignment[0]->invester_yield_percent);
			}else{

				$investor_yeild = number_format($servicing_lender_ratess,3);
			}

			foreach ($getassignment as $value) {

				$paymenttotal = ($value->investmenttotal*(str_replace('%', '', $investor_yeild)/12))/100;
				$percent_loan = ($value->investmenttotal / $totalLoanAmount) *100;

				$this->User_model->query("UPDATE loan_assigment SET percent_loan = '".$percent_loan."', invester_yield_percent = '".$investor_yeild."', paymenttotal = '".$paymenttotal."', payment = '".$paymenttotal."' WHERE id = '".$value->id."'");
				//echo $paymenttotal.' = '.$value->investmenttotal .' = '.$percent_loan .' = '.$investor_yeild; echo '<br>';
			}
			//die('stop');
		}

		return;
	}


	public function add_vendor_data() {

		$loan_id = $this->input->post('loan_id');

		$vendor_id = $this->input->post('id');
		$data['talimar_loan'] = $this->input->post('talimar_no');
		$data['vendor_name'] = $this->input->post('vendor_name');
		$data['contact_name'] = $this->input->post('contact_name');
		$data['fax'] = $this->input->post('fax');
		$data['email'] = $this->input->post('email');
		$data['address'] = $this->input->post('address');
		$data['website'] = $this->input->post('website');
		$data['street_address'] = $this->input->post('street_address');
		$data['unit'] = $this->input->post('unit');
		$data['city'] = $this->input->post('city');
		$data['state'] = $this->input->post('state');
		$data['zip'] = $this->input->post('zip');
		$data['tax_id'] = $this->input->post('tax_id');
		$data['vendor_type'] = $this->input->post('vendor_type');

		// first check vendor exist or not...

		if ($vendor_id) {
			$check_vendor_id['id'] = $this->input->post('vendor_id');
			// update table vendors...
			$this->User_model->updatedata('vendors', $check_vendor_id, $data);
			$this->session->set_flashdata('success', ' Vendor Updated successfully!');
			redirect(base_url() . 'load_data' . $loan_id, 'refresh');
		} else {

			// insert into 'vendor_data' table...
			$this->User_model->insertdata('vendors', $data);
			$insert_id = $this->db->insert_id();

			if ($insert_id) {
				$this->session->set_flashdata('success', ' Vendor added successfully!');
				redirect(base_url() . 'vendor-primary');
			}

		}
		// }
	}

	public function add_accured_charges() {

		$button = $this->input->post('button');
		$accured_date = $this->input->post('accured_date');
		$loan_id = $this->input->post('loan_id');
		// $loan_id						= trim($loan_id,'/');

		$accured_id = $this->input->post('accured_id');

		$accured_amount = $this->input->post('accured_amount');
		$accured_description = $this->input->post('accured_description');
		$accured_paid_to = $this->input->post('accured_paid_to');
		$paid_amount = $this->input->post('paid_amount');
		//$checkPaid = $this->input->post('checkPaid');

		$accured_datee = $accured_date;

		if ($button == 'save') {
			
			foreach ($accured_description as $key => $description) {

				$data['talimar_loan'] = $this->input->post('talimar_no');
				$data['accured_amount'] = $this->amount_format($accured_amount[$key]);

				//$myDateTime = DateTime::createFromFormat('m-d-Y', $accured_datee[$key]);
				//$newDateString = $myDateTime->format('Y-m-d');
				$newDateString = input_date_format($accured_datee[$key]);

				$data['accured_date'] = $newDateString ? $newDateString : '';
				$data['accured_description'] = $accured_description[$key];
				$data['accured_paid_to'] = $accured_paid_to[$key];
				$data['paid_amount'] = $this->amount_format($paid_amount[$key]);
				//$data['checkPaid'] = $checkPaid[$key];

				if ($accured_id[$key]) {
					//update it...
					$check_accured_id['id'] = $accured_id[$key];
					$this->User_model->updatedata('accured_charges', $check_accured_id, $data);

				} else {

					$this->User_model->insertdata('accured_charges', $data);

					$insert_id = $this->db->insert_id();

				}
			}

		}
		// if($insert_id){
		$this->session->set_flashdata('success', ' Accured Charges updated successfully!');
		$this->session->set_flashdata('popup_modal_hit', 'accured_charges_data');
		redirect(base_url() . 'load_data' . $loan_id, 'refresh');
		// }
	}
	
	public function delete_accured_id($id) {

		$delete_query = $this->User_model->query("DELETE  FROM accured_charges WHERE id = '" . $id . "' ");
		// echo "DELETE FROM accured_charges WHERE id = '".$id."' ";
		if ($delete_query) {
			echo '1';
		} else {
			echo '0';
		}
	}

	Public function add_load_data_form() // add new loan data...
	{

		error_reporting(0);
		$tali_mar_loan = $this->input->post('talimar_loan');
		$f_t_user_id = $this->session->userdata('t_user_id');

		$loan_id = $this->input->post('loan_id');
		$button = $this->input->post('form_button');
		$data = array();
		$form_button = $this->input->post('modal_hit');

		if ($form_button == 'modal_loan_terms') {
			$this->session->set_flashdata('loan_term_hit', '1');
		}

		$data['talimar_loan'] = $this->input->post('talimar_loan');
		$data['fci'] = $this->input->post('fci_loan_no');
		$data['borrower'] = $this->input->post('borrower');
		$data['property'] = $this->input->post('property');
		$data['loan_amount'] = $this->amount_format($this->input->post('loan_amount'));
		$data['current_balance'] = $this->amount_format($this->input->post('current_balance'));
		$data['lender_fee'] = $this->amount_format($this->input->post('lender_fee'));
		$data['intrest_rate'] = $this->amount_format($this->input->post('intrest_rate'));
		$data['term_month'] = $this->input->post('term_month');
		$data['per_payment_period'] = $this->input->post('per_payment_period');
		$data['interest_reserve'] = $this->input->post('interest_reserve');
		$data['payment_held_close'] = $this->input->post('payment_held_close');
		$data['payment_requirement'] = $this->input->post('payment_requirement');
		$data['calc_of_year'] = $this->input->post('calc_of_year');
		$data['calc_based'] = $this->input->post('calc_based');
		$data['loan_type'] = $this->input->post('loan_type');
		$data['transaction_type'] = $this->input->post('transaction_type');
		$data['exit_strategy'] = $this->input->post('exit_strategy');
		$data['loan_conditions'] = $this->input->post('loan_conditions');
		$data['first_payment_collect'] = $this->input->post('first_payment_collect');
		$data['add_broker_data_desc'] = $this->input->post('add_broker_data_desc');
		$data['loan_highlights'] = $this->input->post('loan_highlights');
		$data['position'] = $this->input->post('position');
		$data['payment_type'] = $this->input->post('payment_type');
		$data['baloon_payment_type'] = $this->input->post('baloon_payment_type');
		$data['payment_sechdule'] = $this->input->post('payment_sechdule');
		$data['intrest_type'] = $this->input->post('intrest_type');
		$data['payment_amount'] = $this->amount_format($this->input->post('payment_amount'));
		$data['min_payment'] = $this->input->post('min_payment');
		$data['minium_intrest'] = $this->input->post('minium_intrest');
		$data['min_intrest_month'] = $this->input->post('min_intrest_month');
		$data['min_bal_type'] = $this->input->post('min_bal_type');
		$data['payment_gurranty'] = $this->input->post('payment_gurranty');
		$data['payment_gurr_explain'] = $this->input->post('payment_gurr_explain');
		$data['owner_purpose'] = $this->input->post('owner_purpose');
		$data['loan_cross_collateralized'] = $this->input->post('cross_collateralized');
		$data['cross_collateralized_provision'] = $this->input->post('cross_provision');
		$data['trustee'] = $this->input->post('trustee');
		$data['how_many_days'] = $this->input->post('how_many_days');
		$data['promissory_note_val'] = $this->input->post('promissory_note_val');
		$data['project_cost'] = $this->amount_format($this->input->post('project_cost'));
		$data['total_project_cost'] = $this->amount_format($this->input->post('total_project_cost'));
		$data['borrower_enquity'] = $this->amount_format($this->input->post('borrower_enquity'));
		$data['grace_period'] = $this->input->post('grace_period');
		$data['late_charges_due'] = $this->amount_format($this->input->post('late_charges_due'));
		$data['default_rate'] = $this->amount_format($this->input->post('default_rate'));
		$data['return_check_rate'] = $this->amount_format($this->input->post('return_check_rate'));
		$data['refi_existing'] = $this->input->post('refi_existing');
		$data['replace_late_fee'] = $this->input->post('replace_late_fee');
		$data['additional_gra'] = $this->input->post('add_adddiditonal_grad');
		$subb = $this->input->post('add_gra_c_id');
		$araay_to_string = implode(',', $subb);

		$data['add_gra_c_id'] = $araay_to_string;

		$loan_document_date = $this->input->post('loan_document_date');
		if ($loan_document_date) {
			//$myDateTime = DateTime::createFromFormat('m-d-Y', $loan_document_date);
			//$newDateString = $myDateTime->format('Y-m-d');
			$newDateString = input_date_format($loan_document_date);
			$data['loan_document_date'] = $newDateString;
		}

		$loan_funding_date = $this->input->post('loan_funding_date');
		if ($loan_funding_date) {

			//$myDateTime = DateTime::createFromFormat('m-d-Y', $loan_funding_date);
			//$newDateString = $myDateTime->format('Y-m-d');
			$newDateString = input_date_format($loan_funding_date);
			$data['loan_funding_date'] = $newDateString ? $newDateString : '';
		}

		$first_payment_date = $this->input->post('first_payment_date');
		if ($first_payment_date) {
			//$myDateTime = DateTime::createFromFormat('m-d-Y', $first_payment_date);
			//$newDateString = $myDateTime->format('Y-m-d');
			$newDateString = input_date_format($first_payment_date);
			$data['first_payment_date'] = $newDateString ? $newDateString : '';
		}

		// $data['maturity_date'] 		= $this->input->post('maturity_date');

		$maturity = $this->input->post('maturity_date');
		if ($maturity) {
			//$myDateTime = DateTime::createFromFormat('m-d-Y', $maturity);
			//$newDateString = $myDateTime->format('Y-m-d');
			$newDateString = input_date_format($maturity);
			$data['maturity_date'] = $newDateString ? $newDateString : '';
		}
		/*21-07-2021 By@Aj*/
		$new_maturity = $this->input->post('new_maturity_date');
		if ($new_maturity) {
			//$myDateTime = DateTime::createFromFormat('m-d-Y', $maturity);
			//$newDateString = $myDateTime->format('Y-m-d');
			$newDateString = input_date_format($new_maturity);
			$data['new_maturity_date'] = $newDateString ? $newDateString : '';
		}
		/*end*/
		$modify_maturity = $this->input->post('modify_maturity_date');

		if ($modify_maturity) {
			//$myDateTime = DateTime::createFromFormat('m-d-Y', $modify_maturity);
			//$newDateString = $myDateTime->format('Y-m-d');
			$newDateString = input_date_format($modify_maturity);
			$data['modify_maturity_date'] = $newDateString ? $newDateString : '';
		}

		$data['payment_due'] = $this->input->post('payment_due');
		$data['auto_payment'] = $this->input->post('auto_payment');
		$data['extention_option'] = $this->input->post('extention_option');
		$data['extention_month'] = $this->input->post('extention_month');
		$data['extention_percent_amount'] = $this->input->post('extention_percent_amount');
		$data['ballon_payment'] = $this->amount_format($this->input->post('ballon_payment'));

		$data['draws'] = $this->input->post('draws') ? $this->input->post('draws') : '';
		$data['ucc_filing'] = $this->input->post('ucc_filing') ? $this->input->post('ucc_filing') : '';
		$data['phase'] = $this->input->post('phase') ? $this->input->post('phase') : '';

		$user_id = $this->session->userdata('t_user_id');
		if ($data['talimar_loan'] == '') {
			$this->session->set_flashdata('error', 'Please fill TaliMar loan');
			redirect(base_url() . 'load_data', 'refresh');
			die('');
		}

		if ($button == 'save') {
			if (($loan_id == '') || ($loan_id == 'new')) {
			

				$check_loan['talimar_loan'] = $this->input->post('talimar_loan');
				$check_loan_query = $this->User_model->select_where('loan', $check_loan);
				if ($check_loan_query->num_rows() > 0) {
					$this->session->set_flashdata('error', ' Talimar Loan number already exist');
					redirect(base_url() . 'load_data', 'refresh');
					die('');
				}

				
				// Also insert in loan momthly payments
				$data['t_user_id'] = $this->session->userdata('t_user_id');
				$data['loan_originator'] = $this->input->post('loan_originator');

				
				$this->User_model->insertdata('loan', $data);

				$insert_id = $this->db->insert_id();

				if ($insert_id) {

					$servicingdata = array();
					$servicingdata['loan_id'] = $insert_id;
					$servicingdata['loan_originator'] = $this->input->post('loan_originator');
					$servicingdata['t_user_id'] = $this->session->userdata('t_user_id');
					$servicingdata['talimar_loan'] = $data['talimar_loan'];
					$this->User_model->insertdata('loan_servicing_contacts', $servicingdata);

					
					$insert_servicing = array(
						'talimar_loan' => $this->input->post('talimar_loan'),
						'loan_status' => 1,
						'closing_status' => 11,
						'boarding_status' => 0,

					);
					// insert into 'loan_servicing' table...
					$this->User_model->insertdata('loan_servicing', $insert_servicing);

					$ecum_add = $this->add_data_on_ecumbrances($tali_mar_loan);

					$loan_source_and_uses_items = $this->load_loan_source_and_uses_items($tali_mar_loan);


					/* Add required field in loan_monthly_payment table */
					$monthly_payment = array('Mortgage Payment', 'Interest Payment', 'Property Tax Payment');
					foreach ($monthly_payment as $row) {
						if ($row == 'Mortgage Payment') {
							$data_monthly_amount = $this->amount_format($this->input->post('payment_amount'));
						} else {
							$data_monthly_amount = '';
						}
						$this->User_model->query('INSERT INTO `loan_monthly_payment`(`talimar_loan`, `items`, `amount`, `t_user_id`) VALUES ( "' . $tali_mar_loan . '" , "' . $row . '" , "' . $data_monthly_amount . '" , "' . $f_t_user_id . '" )');
					}

					$closing_statement_items = array('Lender Fee', 'Wire Fee', 'Loan Doc Fee', 'Credit Check Fee', 'Renovation Reserve', 'Interest Reserve', 'Renovation Draw Fee', 'Renovation Reserve Setup Fee');

					$key = 1;

					foreach ($closing_statement_items as $row) {
						if ($row == 'Lender Fee') {
							$data_loan_amount = $this->amount_format($this->input->post('loan_amount'));
							$datalender_fees = $this->amount_format($this->input->post('lender_fee'));
							$add_data_amount = $data_loan_amount * ($datalender_fees / 100);
							$data_p_finance_charge = 1;
							// $data_amount =
						} else if ($row == 'Wire Fee') {
							$add_data_amount = 35;
							$data_p_finance_charge = 1;
						} else if ($row == 'Renovation Draw Fee') {
							$add_data_amount = '';
							$data_p_finance_charge = 1;
						} else if ($row == 'Credit Check Fee') {
							$add_data_amount = 25;
							$data_p_finance_charge = 1;
						} else if ($row == 'Loan Doc Fee') {
							$add_data_amount = 350;
							$data_p_finance_charge = 1;
						} else if ($row == 'Renovation Reserve Setup Fee') {
							$add_data_amount = '';
							$data_p_finance_charge = 1;
						} else {
							$add_data_amount = '';
							$data_p_finance_charge = 0;
						}
						$this->User_model->query('INSERT INTO `closing_statement`(`talimar_loan`, `items`, `paid_to`, `amount`, `net_funding`, `direct_to_servicer`, `prepaid_finance_charge`, `position`, `t_user_id`) VALUES ( "' . $tali_mar_loan . '" , "' . $row . '" ,"TaliMar Financial" , "' . $add_data_amount . '" , "0" ,"0", "' . $data_p_finance_charge . '" ,"' . $key . '" ,"' . $f_t_user_id . '" )');

						$key++;
					}

					$loan_distribution = array('Purchase Price', 'Renovation Reserve', 'Interest Reserve');
					foreach ($loan_distribution as $row) {
						$insert_distribution['items'] = $row;
						$insert_distribution['disbursement'] = 0;
						$insert_distribution['t_user_id'] = $f_t_user_id;
						$insert_distribution['talimar_loan'] = $tali_mar_loan;
						$insert_distribution['required'] = 1;

						$this->User_model->insertdata('loan_distribution', $insert_distribution);
					}

					// $where_31['items'] = '104.1 Endorsement';
					$where_31['items'] = 'None';
					$where_31['amount'] = '0.00';
					$where_31['position'] = '1';
					$where_31['talimar_loan'] = $tali_mar_loan;

					$this->User_model->insertdata('servicing_estimate_lender', $where_31);
					$this->session->set_flashdata('success', ' Records added successfully');
					$loan_purpose = str_replace("'", "", $this->input->post('promissory_textt'));

					$hidden_id = $this->input->post('hidden_id');

					foreach ($hidden_id as $key => $row) {

						if ($row == 'new') {

							$sql = "INSERT INTO `servicing_purpose`(`talimar_loan`, `t_user_id`, `loan_id`, `loan_purpose`) VALUES ('" . $tali_mar_loan . "', '" . $f_t_user_id . "', '" . $loan_id . "', '" . $loan_purpose[$key] . "')";

						} else {

							$sql = "UPDATE `servicing_purpose` SET `loan_purpose`='" . $loan_purpose[$key] . "' WHERE servicing_purpose_id='" . $row . "'";
						}
						$this->User_model->query($sql);

					}

					$promissory_text = $this->input->post('promissory_text');
					$hidden_idd = $this->input->post('hidden_idd');

					foreach ($hidden_idd as $keyy => $roww) {

						//$id = $hidden_id[$key];

						if ($roww == 'new') {

							$sqll = "INSERT INTO `additional_loan`(`talimar_loan`, `t_user_id`, `loan_id`, `promissory_text`) VALUES ('" . $tali_mar_loan . "', '" . $f_t_user_id . "', '" . $loan_id . "', '" . $promissory_text[$keyy] . "')";

						} else {

							$sqll = "UPDATE `additional_loan` SET `promissory_text`='" . $promissory_text[$keyy] . "' WHERE id='" . $roww . "'";

						}

						$this->User_model->query($sqll);

					}

					redirect(base_url() . 'load_data/' . $insert_id . '', 'refresh');

				} else {
					// echo "false insert id";
					$this->session->set_flashdata('error', 'Something went wrong');
					redirect(base_url() . 'load_data', 'refresh');
				}

			} else {
				$update_id['id'] = $loan_id;
				$data['loan_originator'] = $this->input->post('loan_originator');
				$this->User_model->updatedata('loan', $update_id, $data);

				$loan_purpose = str_replace("'", "", $this->input->post('promissory_textt'));

				$hidden_id = $this->input->post('hidden_id');

				foreach ($hidden_id as $key => $row) {

					if ($loan_purpose[$key] != '') {

						if ($row == 'new') {

							$sql = "INSERT INTO `servicing_purpose`(`talimar_loan`, `t_user_id`, `loan_id`, `loan_purpose`) VALUES ('" . $tali_mar_loan . "', '" . $f_t_user_id . "', '" . $loan_id . "', '" . $loan_purpose[$key] . "')";

						} else {

							$sql = "UPDATE `servicing_purpose` SET `loan_purpose`='" . $loan_purpose[$key] . "',`loan_id`='" . $loan_id . "' WHERE servicing_purpose_id='" . $row . "'";
						}
						$this->User_model->query($sql);
					}
				}

				$promissory_text = $this->input->post('promissory_text');
				$hidden_idd = $this->input->post('hidden_idd');

				foreach ($hidden_idd as $keyy => $roww) {

					//$id = $hidden_id[$key];

					if ($roww == 'new') {

						$sqll = "INSERT INTO `additional_loan`(`talimar_loan`, `t_user_id`, `loan_id`, `promissory_text`) VALUES ('" . $tali_mar_loan . "', '" . $f_t_user_id . "', '" . $loan_id . "', '" . $promissory_text[$keyy] . "')";

					} else {

						$sqll = "UPDATE `additional_loan` SET `promissory_text`='" . $promissory_text[$keyy] . "',`loan_id`='" . $loan_id . "' WHERE id='" . $roww . "'";

					}

					$this->User_model->query($sqll);

				}

				$this->session->set_flashdata('success', ' Loan Updated Successfully!');
				$data['loan_id'] = $loan_id;

				redirect(base_url() . 'load_data/' . $loan_id, 'refresh');
			}

		}

	}


	public function form_wire_information2() {

		$talimar_loan = $this->input->post('talimar_no');
		$data['loan_id'] = $this->input->post('loan_id');
		$data['account_name'] = $this->input->post('account_name');
		$data['bank_name'] = $this->input->post('bank_name');
		$data['bank_street'] = $this->input->post('bank_street');
		$data['bank_unit'] = $this->input->post('bank_unit');
		$data['bank_city'] = $this->input->post('bank_city');
		$data['bank_state'] = $this->input->post('bank_state');
		$data['bank_zip'] = $this->input->post('bank_zip');
		$data['routing_number'] = $this->input->post('routing_number');
		$data['account_number'] = $this->input->post('account_number');

		$fetch_wire_instruction2 = $this->User_model->query("Select * from wire_instruction2 where talimar_loan = '" . $talimar_loan . "'");
		if ($fetch_wire_instruction2->num_rows() > 0) {

			$where['talimar_loan'] = $this->input->post('talimar_no');
			$this->User_model->updatedata('wire_instruction2', $where, $data);
		} else {

			$data['talimar_loan'] = $this->input->post('talimar_no');
			$this->User_model->insertdata('wire_instruction2', $data);
		}

		$this->session->set_flashdata('success', 'Wire Instructions Updated Successfully!');
		redirect(base_url() . 'load_data/' . $data['loan_id'], 'refresh');

	}

	public function form_wire_information() {

		$where['talimar_loan'] = $this->input->post('talimar_no');
		$data['draw_borrower_name'] = $this->input->post('draw_borrower_name');
		$data['draw_bank_name'] = $this->input->post('draw_bank_name');
		$data['draw_bank_unit'] = $this->input->post('draw_bank_unit');
		$data['draw_bank_street'] = $this->input->post('draw_bank_street');
		$data['draw_bank_city'] = $this->input->post('draw_bank_city');
		$data['draw_bank_state'] = $this->input->post('draw_bank_state');
		$data['draw_bank_zip'] = $this->input->post('draw_bank_zip');
		$data['draw_routing_number'] = $this->input->post('draw_routing_number');
		$data['draw_account_number'] = $this->input->post('draw_account_number');
		$data['loan_id'] = $this->input->post('loan_id');

		$fetch_result = $this->User_model->query("SELECT * FROM draw_wire_instructions WHERE talimar_loan = '" . $data['talimar_loan'] . "' ");
		if ($fetch_result->num_rows() > 0) {

			// update into 'draw_wire_instructions' table...
			$this->User_model->query(" Update draw_wire_instructions SET draw_borrower_name = '" . $data['draw_borrower_name'] . "', draw_bank_name = '" . $data['draw_bank_name'] . "',  draw_bank_unit= '" . $data['draw_bank_unit'] . "', draw_bank_street = '" . $data['draw_bank_street'] . "',  draw_bank_city = '" . $data['draw_bank_city'] . "' ,   draw_bank_state = '" . $data['draw_bank_state'] . "' ,  draw_bank_zip = '" . $data['draw_bank_zip'] . "' , draw_routing_number = '" . $data['draw_routing_number'] . "' ,draw_account_number = '" . $data['draw_account_number'] . "' WHERE talimar_loan = '" . $data['talimar_loan'] . "' AND loan_id = '" . $data['loan_id'] . "' ");
			$this->session->set_flashdata('success', ' Loan Draws wire instruction Updated successfully! ');
			redirect(base_url() . 'load_data/' . $data['loan_id'], 'refresh');

		} else {

			// insert into 'draw_wire_instructions' table...
			$this->User_model->insertdata('draw_wire_instructions', $data);
			$insert_id = $this->db->insert_id();
			if ($insert_id) {

				$this->session->set_flashdata('success', ' Loan Draws wire instruction added successfully! ');
				redirect(base_url() . 'load_data/' . $data['loan_id'], 'refresh');
			} else {
				$this->session->set_flashdata('error', ' Loan Draws wire instruction not added, please try again!');
				redirect(base_url() . 'load_data/' . $data['loan_id'], 'refresh');
			}
		}
	}

	public function calculate_equity($loan, $arv_finished) {
		$loan = $this->amount_format($loan);
		$arv_finished = $this->amount_format($arv_finished);
		$result = $arv_finished - $loan;
		return $result;
	}

	public function loan_property() {
		$data['a'] = '';
		$data['content'] = $this->load->view('load_data/loan_property', $data, true);
		$this->load->view('template_files/template_with_sidebar', $data);
	}

	public function amount_format($n) {
		$n = str_replace(',', '', $n);
		$a = trim($n, '$');
		$b = trim($a, ',');
		$c = trim($b, '%');
		return $c;
	}

	public function cal_per_day($n) {
		$n = str_replace(',', '', $n);
		$a = trim($n, '$');
		$b = trim($a, ',');
		$c = trim($b, '%');
		$c = trim($c, 'per day');
		$c = trim($c, 'per');
		$c = trim($c, 'day');
		$c = trim($c, 'NaN');
		$c = trim($c);
		return $c;
	}

	public function add_property() {
		// error_reporting(0);

		
		$property_position = $this->input->post('property_position');
		$senior_liens = $this->amount_format($this->input->post('senior_liens'));

		$property_home_id = $this->input->post('property_home_id');
		$loan_amount = $this->input->post('hidden_loan_amount');
		$finised_arv = $this->input->post('arv');
		$equity['borrower_enquity'] = $this->calculate_equity($loan_amount, $finised_arv);
		$equity_id['talimar_loan'] = $this->input->post('talimar_no');
		$loan_id = str_replace('/', '', $this->input->post('loan_id'));
		$talimar_no = $this->input->post('talimar_no');
		$button = $this->input->post('button');
		$data['property_address'] = $this->input->post('property_address');
		$data['unit'] = $this->input->post('unit');
		$data['city'] = $this->input->post('city');
		$data['state'] = $this->input->post('state');
		$data['zip'] = $this->input->post('zip');
		$data['country'] = ($this->input->post('state') == 'NV') ? $this->input->post('country') : $this->input->post('county');
		$data['yr_built'] = $this->input->post('yr_built');
		$data['apn'] = $this->input->post('apn');
		$prop = str_replace('$', '', $this->input->post('property_tax'));
		$data['property_tax'] = str_replace(',', '', $prop);

		$data['property_type'] = $this->input->post('property_type');
		$data['square_feet'] = $this->amount_format($this->input->post('square_feet'));
		$data['bedrooms'] = $this->input->post('bedrooms');
		$data['bathrooms'] = $this->input->post('bathrooms');
		$data['lot_size'] = $this->amount_format($this->input->post('lot_size'));
		$data['garage'] = $this->amount_format($this->input->post('garage'));
		$data['pool'] = $this->input->post('pool');
		$data['conditions'] = $this->input->post('conditions');
		$data['occupancy_type'] = $this->input->post('occupancy_type');
		$data['Cash_Flow'] = $this->input->post('Cash_Flow');
		$data['zoning'] = $this->input->post('zoning');
		$data['re851d_data'] = $this->input->post('re851d_data');
		$data['rehab_property'] = $this->input->post('rehab_property');
		$data['construction_budget'] = $this->amount_format($this->input->post('construction_budget'));

		$data['contractor_party'] = $this->input->post('contractor_party');
		$data['contractor_name'] = $this->input->post('contractor_name');
		$data['contractor_license'] = $this->input->post('contractor_license');

		// $data['gross_monthly_income'] 	= $this->amount_format($this->input->post('gross_monthly_income'));
		// $data['gross_monthly_expense'] 	= $this->amount_format($this->input->post('gross_monthly_expense'));
		// $data['net_income'] 			= $this->amount_format($this->input->post('net_income'));
		$data['purchase_price'] = $this->amount_format($this->input->post('purchase_price'));
		$data['current_option'] = $this->input->post('current_option');

		// $data['property_senior_lien'] 		= $this->amount_format($this->input->post('property_senior_lien'));
		// $data['senior_liens'] 				= $this->amount_format($this->input->post('senior_liens'));
		$data['current_value'] = $this->amount_format($this->input->post('current_value'));
		$data['underwriting_option'] = $this->input->post('underwriting_option');
		$data['underwriting_value'] = $this->amount_format($this->input->post('underwriting_value'));

		$data['as_value_option'] = $this->input->post('as_value_option');
		$data['as_value_amount'] = $this->amount_format($this->input->post('as_value_amount'));
		$data['completion_option'] = $this->input->post('completion_option');
		$data['completion_amount'] = $this->amount_format($this->input->post('completion_amount'));

		// $data['renovation_cost'] 		= $this->amount_format($this->input->post('renovation_cost'));
		// $data['arv'] 					= $this->amount_format($this->input->post('arv'));
		// $data['annual_property_taxes']	= $this->amount_format($this->input->post('annual_property_taxes'));
		// $data['property_taxes_current']	= $this->input->post('property_taxes_current');
		// $data['yes_p_tax_cur']			= $this->amount_format($this->input->post('yes_p_tax_cur'));
		// $data['insurance_carrier'] 		= $this->input->post('insurance_carrier');
		// $data['insurance_expiration'] 	= $this->input->post('insurance_expiration');
		// $data['annual_insurance_payment'] 	= $this->amount_format($this->input->post('annual_insurance_payment'));
		$data['valuation_based'] = $this->input->post('valuation_based');
		$data['valuation_type'] = $this->input->post('valuation_type');
		$data['p_phone'] = $this->input->post('p_phone');
		$data['p_email'] = $this->input->post('p_email');
		$data['relationship_type'] = $this->input->post('relationship_type');
		$data['valuation_status'] = $this->input->post('valuation_status');
		//$data['current_market_value'] 		= $this->amount_format($this->input->post('current_market_value'));

		if ($this->input->post('valuation_date')) {
			//$valuation_datetime = DateTime::createFromFormat('m-d-Y', $this->input->post('valuation_date'));
			//$valuation_date = $valuation_datetime->format('Y-m-d');
			$valuation_date = input_date_format($this->input->post('valuation_date'));
		} else {
			$valuation_date = NULL;
		}

		$data['valuation_date'] = $valuation_date;

		if ($this->input->post('ex_com_date')) {
			//$ex_com_date = DateTime::createFromFormat('m-d-Y', $this->input->post('ex_com_date'));
			//$ex_com_dates = $ex_com_date->format('Y-m-d');
			$ex_com_dates = input_date_format($this->input->post('ex_com_date'));
		} else {
			$ex_com_dates = NULL;
		}

		$data['ex_com_date'] = $ex_com_dates;


		$data['valuation_vendor'] = $this->input->post('valuation_vendor');
		$data['valuation_company'] = $this->input->post('valuation_company');

		$data['appraiser_name'] = $this->input->post('appraiser_name');
		//$data['appraiser_address'] 		= $this->input->post('appraiser_address');
		$data['appraiser_s_address'] = $this->input->post('appraiser_s_address');
		$data['appraiser_unit'] = $this->input->post('appraiser_unit');
		$data['appraiser_state'] = $this->input->post('appraiser_state');
		$data['appraiser_city'] = $this->input->post('appraiser_city');
		$data['appraiser_zip'] = $this->input->post('appraiser_zip');

		// $data['apraisal_date'] 			= $this->input->post('apraisal_date');

		$data['of_unit'] = $this->input->post('of_unit');
		$data['hoa'] = $this->input->post('hoa');
		$data['hoa_stand'] = $this->input->post('hoa_stand');
		$data['hoa_dues'] = $this->amount_format($this->input->post('hoa_dues'));
		$data['sales_refinance_value'] = $this->amount_format($this->input->post('sales_refinance_value'));
		$data['construction_type'] = $this->input->post('construction_type');
		$data['desc_property_impro'] = $this->input->post('desc_property_impro');
		$data['desc_property_p_impro'] = $this->input->post('desc_property_p_impro');
		$data['property_link'] = $this->input->post('property_link');
		//$data['property_notes'] 		= $this->input->post('property_valuation_notes');

		$data['legal_description'] = $this->input->post('legal_description');

		// echo "<pre>";
		// print_r($_POST);
		// echo "</pre>";

		// die('fdzxfc');

		if ($button == 'save' || $button == 'add' || $button == 'close') {

			$check_talimar_no['talimar_loan'] = $talimar_no;
			$check_talimar_no['property_home_id'] = $this->input->post('property_home_id');
			$check_result = $this->User_model->select_where('loan_property', $check_talimar_no);

			if ($check_result->num_rows() > 0) {
				$this->User_model->updatedata('loan_property', $check_talimar_no, $data);
				//   --- Update Equity query   -----------

				$this->User_model->updatedata('loan', $equity_id, $equity);

				// Update Property Home also
				// $p_home_data['purchase_price'] 	= $data['purchase_price'];

				$p_home_data['current_value'] = $data['current_value'];
				// $p_home_data['future_value'] 		= $data['arv'];
				$p_home_data['future_value'] = $data['underwriting_value'];
				$p_home_data['sold_refi_value'] = $data['sales_refinance_value'];
				$p_home_data['property_address'] = $data['property_address'];
				$p_home_data['senior_liens'] = $senior_liens;
				$p_home_data['position'] = $property_position;

				$this->User_model->updatedata('property_home', array('id' => $property_home_id), $p_home_data);

				$this->session->set_flashdata('success', 'Property Updated Successfully!');
			} else {

				//for add more then 4 property using add more button...
				/*if($this->input->post('property_home_id') == 'addnew'){

					//first add to property home table...
					$p_home['talimar_loan'] 	= $talimar_no;
					$p_home['loan_id'] 			= $loan_id;
					$p_home['t_user_id'] 		= $this->session->userdata('t_user_id');
					$p_home['property_address'] = $data['property_address'];
					$p_home['future_value'] 	= $data['underwriting_value'];
					$p_home['sold_refi_value'] 	= $data['sales_refinance_value'];
					$p_home['primary_property'] = '2';

					$this->User_model->insertdata('property_home', $p_home);
					$last_insert_phome = $this->db->insert_id();

					//insert data in loan_property table...
					$data['talimar_loan'] = $talimar_no;
					$data['property_home_id'] = $last_insert_phome;
					$data['loan_id'] = $loan_id;
					$data['t_user_id'] = $this->session->userdata('t_user_id');

					$this->User_model->insertdata('loan_property', $data);
					$this->session->set_flashdata('success', 'Property record added successfully!');
				}*/
				

					$data['talimar_loan'] = $talimar_no;
					$data['property_home_id'] = $this->input->post('property_home_id');
					$data['loan_id'] = $loan_id;
					$data['t_user_id'] = $this->session->userdata('t_user_id');

					$this->User_model->insertdata('loan_property', $data);
					//   --- Update Equity query   -----------

					$p_home_data['current_value'] = $data['current_value'];
					// $p_home_data['future_value'] 		= $data['arv'];
					$p_home_data['future_value'] = $data['underwriting_value'];
					$p_home_data['sold_refi_value'] = $data['sales_refinance_value'];
					$p_home_data['property_address'] = $data['property_address'];
					$p_home_data['senior_liens'] = $senior_liens;
					$p_home_data['position'] = $property_position;

					$this->User_model->updatedata('property_home', array('id' => $property_home_id), $p_home_data);

					$this->User_model->updatedata('loan', $equity_id, $equity);
					$this->session->set_flashdata('success', 'Property record added successfully');

					//*********************************************** Call to Sharepoint function ***************************************//

					$where['talimar_loan'] = $talimar_no;
					$where['id'] = $property_home_id;
					$select_primary_property = $this->User_model->select_where('property_home', $where);
					$select_primary_property = $select_primary_property->result();
					$check_primary_property = $select_primary_property[0]->primary_property;

					// if($check_primary_property == 1){

					// 	$property = $this->input->post('property_address') ? $this->input->post('property_address') : 'New property';
					// 	$this->auth_login($property);
					// }

					//*********************************************** Call to Sharepoint function ***************************************//
				

			}

			$this->session->set_flashdata('property_home_id', $property_home_id);
			$this->session->set_flashdata('popup_modal_hit', 'property_home');
		} else {

			$this->session->set_flashdata('error', 'Please fill all the fields');

		}

		if ($button == 'close') {
			redirect(base_url() . 'load_data/' . $loan_id . '#property-data-home-page');
		} else {
			// redirect(base_url().'load_data/'.$loan_id.'#property-'.$property_home_id);
			redirect(base_url() . 'load_data/' . $loan_id);
		}
	}

	public function property_insurance() {
		error_reporting(0);

		$loan_amount = $this->input->post('hidden_loan_amount');
		$loan_id = $this->input->post('loan_id');
		$talimar_loan = $this->input->post('talimar_no');
		$button = $this->input->post('button');

		$data['talimar_loan'] = $this->input->post('talimar_no');
		$data['policy_number'] = $this->input->post('policy_number');
		$data['request_updated_insurance'] = $this->input->post('request_updated_insurance');
		$data['insurance_carrier'] = $this->input->post('insurance_carrier');
		$data['insurance_expiration'] = $this->input->post('insurance_expiration');
		$data['annual_insurance_payment'] = $this->amount_format($this->input->post('annual_insurance_payment'));
		$data['contact_name'] = $this->input->post('contact_name');
		// $data['contact_name']					= $this->input->post('contact_name');
		$data['contact_number'] = $this->input->post('contact_number');
		$data['contact_email'] = $this->input->post('contact_email');

		if ($button == 'save' || $button == 'add') {
			$check_talimar_no['talimar_loan'] = $talimar_loan;
			$check_result = $this->User_model->select_where('loan_property_insurance', $check_talimar_no);

			if ($check_result->num_rows() > 0) {
				$this->User_model->updatedata('loan_property_insurance', $check_talimar_no, $data);
				//   --- Update Equity query   -----------
				$this->session->set_flashdata('success', 'Property Updated Successfully!');
				redirect(base_url() . 'load_data' . $loan_id, 'refresh');
			} else {

				$this->User_model->insertdata('loan_property_insurance', $data);

				$insert_id = $this->db->insert_id();

				if ($insert_id) {
					$this->session->set_flashdata('success', 'Property Insurance record added successfully');
					redirect(base_url() . 'load_data' . $loan_id, 'refresh');
				} else {
					$this->session->set_flashdata('error', 'Please fill all the fields');
					redirect(base_url() . 'load_data' . $loan_id, 'refresh');
				}

			}

		} else {
			$this->session->set_flashdata('error', 'Please fill all the fields');
			redirect(base_url() . 'load_data' . $loan_id, 'refresh');
		}

	}

	public function form_encumbrances() {

		// echo '<pre>';
		// print_r($_POST);
		// echo '</pre>';
		// die();

		error_reporting(0);
		$loan_id = str_replace('/', '', $this->input->post('loan_id'));
		// $button						= $this->input->post('button');
		$talimar_loan = $this->input->post('talimar_no');
		$property_home_id = $this->input->post('property_home_id');
		$other_ecum_notes = $this->input->post('other_ecum_notes');

		$ecum_id = $this->input->post('ecum_id');
		$position = $this->input->post('position');
		$lien_holder = $this->input->post('lien_holder');
		$original_balance = $this->input->post('original_balance');
		$current_balance = $this->input->post('current_balance');
		$monthly_payment = $this->input->post('monthly_payment');

		// echo '<pre>';
		// print_r($ecum_id);
		// echo '</pre>';
		foreach ($ecum_id as $key => $row) {
			if ($row == "new") {
				$sql = "INSERT INTO `encumbrances`(`talimar_loan`, `property_home_id`, `loan_id`, `lien_holder`, `position`, `original_balance`, `current_balance`, `monthly_payment`, `set_position`) VALUES ( '" . $talimar_loan . "', '" . $property_home_id . "', '" . $loan_id . "', '" . $lien_holder[$key] . "', '" . $position[$key] . "', '" . $this->amount_format($original_balance[$key]) . "', '" . $this->amount_format($current_balance[$key]) . "', '" . $this->amount_format($monthly_payment[$key]) . "', '" . $key . "'  )";
			} else {
				$sql = "UPDATE encumbrances SET  lien_holder = '" . $lien_holder[$key] . "', position = '" . $position[$key] . "',  original_balance = '" . $this->amount_format($original_balance[$key]) . "', current_balance = '" . $this->amount_format($current_balance[$key]) . "', monthly_payment = '" . $this->amount_format($monthly_payment[$key]) . "',  set_position = '" . $key . "' WHERE id = '" . $row . "' ";
			}

			if ($lien_holder[$key] != '') {
				$this->User_model->query($sql);
			}
		}

		if ($ecum_id != '') {
			//for other financial approval notes...
			$ecumbrances_notes = $this->User_model->query("UPDATE encumbrances SET other_ecum_approval_notes = '" . $other_ecum_notes . "' WHERE talimar_loan = '" . $talimar_loan . "' AND property_home_id ='" . $property_home_id . "'");

		} else {

			$this->session->set_flashdata('error', 'Error! Please Add Encumbrances First!');
			redirect(base_url() . 'load_data/' . $loan_id, 'refresh');
		}

		$this->session->set_flashdata('popup_modal_hit', 'property_data');
		$this->session->set_flashdata('property_home_id', $property_home_id);

		$this->session->set_flashdata('success', 'Encumbrances Updated Successfully! ');
		// redirect(base_url().'load_data/'.$loan_id.'#property-'.$property_home_id);
		redirect(base_url() . 'load_data/' . $loan_id);

	}

	public function form_closing_statement() {
		error_reporting();
		$loan_id = $this->input->post('loan_id');
		$talimar_loan = $this->input->post('talimar_no');
		$items = $this->input->post('items');
		$paid_to = $this->input->post('paid_to');
		$amount = $this->input->post('amount');
		$net_funding = $this->input->post('net_funding');
		$direct_to_servicer = $this->input->post('direct_to_servicer');
		$finance_charge = $this->input->post('finance_charge');
		$e_d['checkbox_pre_paid_per_deim'] = $this->input->post('checkbox_pre_paid_per_deim') ? '1' : '0';

		if ($this->input->post('checkbox_pre_paid_per_deim')) {
			$e_d['pre_paid_per_deim'] = $this->cal_per_day($this->input->post('pre_paid_per_deim'));
		} else {
			$e_d['pre_paid_per_deim'] = '';
		}
		$t_user_id = $this->session->userdata('t_user_id');

		// Extra detail edit data

		$fetch_extra_detail = $this->User_model->select_where('extra_details', array('talimar_loan' => $talimar_loan));
		if ($fetch_extra_detail->num_rows() > 0) {
			$this->User_model->updatedata('extra_details', array('talimar_loan' => $talimar_loan), $e_d);
		} else {
			$e_d['talimar_loan'] = $talimar_loan;
			$this->User_model->insertdata('extra_details', $e_d);
		}

		// for delete those lien holder which are not found
		$f_delete['talimar_loan'] = $talimar_loan;
		$data_fetch_all = $this->User_model->select_where('closing_statement', $f_delete);
		$data_fetch_all = $data_fetch_all->result();
		$avilable = array();
		$come_array = array();
		foreach ($data_fetch_all as $row) {
			$avilable[] = $row->items;
		}
		foreach ($items as $row) {
			$come_array[] = $row;
		}
		$difference = array_diff($avilable, $come_array);

		foreach ($difference as $row) {
			$delete_data['items'] = $row;
			$delete_data['talimar_loan'] = $this->input->post('talimar_no');

			$this->User_model->delete('closing_statement', $delete_data);
		}
		//----------------------------------------
		$count = 1;
		foreach ($items as $key => $row) {
			if ($row != '') {
				$check_available['talimar_loan'] = $talimar_loan;
				$check_available['items'] = $row;
				$check_update = $this->User_model->select_where('closing_statement', $check_available);
				if ($check_update->num_rows() > 0) {
					$sql = "UPDATE closing_statement SET position = '" . $count . "' , amount = '" . $this->amount_format($amount[$key]) . "' , paid_to = '" . $paid_to[$key] . "' , net_funding = '" . $net_funding[$key] . "' , direct_to_servicer = '" . $direct_to_servicer[$key] . "' , prepaid_finance_charge = '" . $finance_charge[$key] . "'  WHERE talimar_loan = '" . $talimar_loan . "' AND items = '" . $row . "' ";
				} else {
					$sql = "INSERT INTO `closing_statement` (`talimar_loan`, `items`, `paid_to`, `amount`, `net_funding`, `direct_to_servicer`, `prepaid_finance_charge`, `position`, `t_user_id`) VALUES ( '" . $talimar_loan . "' ,  '" . $row . "' ,   '" . $paid_to[$key] . "' ,   '" . $this->amount_format($amount[$key]) . "' ,   '" . $net_funding[$key] . "'  ,  '" . $direct_to_servicer[$key] . "'  ,   '" . $finance_charge[$key] . "' ,   '" . $count . "' , '" . $t_user_id . "' )";
				}

				$this->User_model->query($sql);
			}
			$count++;
		}
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			$this->session->set_flashdata('success', 'Closing statement Record added successfully ');
			redirect(base_url() . 'load_data' . $loan_id, 'refresh');
		} else {
			$this->session->set_flashdata('success', 'Closing statement Updated Successfully! ');
			redirect(base_url() . 'load_data' . $loan_id, 'refresh');
		}
	}

	public function form_monthly_payment() {

		$button = $this->input->post('button');
		$items = $this->input->post('items');
		$talimar_loan = $this->input->post('talimar_no');
		$loan_id = $this->input->post('loan_id');

		$t_user_id = $this->session->userdata('t_user_id');
		$monthly_payment = $this->input->post('monthly_amount');
		$data['talimar_loan'] = $this->input->post('talimar_no');

		//      ----first delete all with same loan
		$data_fetch_all = $this->User_model->select_where('loan_monthly_payment', $data);
		$data_fetch_all = $data_fetch_all->result();
		$avilable = array();
		$come_array = array();
		foreach ($data_fetch_all as $row) {
			$avilable[] = $row->items;
		}
		foreach ($items as $row) {
			$come_array[] = $row;
		}
		$difference = array_diff($avilable, $come_array);

		foreach ($difference as $row) {
			$delete_data['items'] = $row;
			$delete_data['talimar_loan'] = $this->input->post('talimar_no');

			$this->User_model->delete('loan_monthly_payment', $delete_data);
		}

		foreach ($items as $key => $row) {

			if ($items[$key] != '') {
				//-----------------------------
				$fetch = "SELECT * FROM loan_monthly_payment WHERE talimar_loan = '" . $talimar_loan . "' AND items = '" . $items[$key] . "'";
				$fetch_result = $this->User_model->query($fetch);
				if ($fetch_result->num_rows() > 0) {
					$update = "UPDATE loan_monthly_payment SET amount = '" . $this->amount_format($monthly_payment[$key]) . "' WHERE talimar_loan = '" . $talimar_loan . "' AND items = '" . $items[$key] . "' ";
					$this->User_model->query($update);
				} else {
					$sql = "INSERT INTO loan_monthly_payment (talimar_loan , items , amount , t_user_id) VALUES ( '" . $talimar_loan . "' , '" . $items[$key] . "' , '" . $this->amount_format($monthly_payment[$key]) . "' , '" . $t_user_id . "')";
					$this->User_model->query($sql);
				}
				//------------------------
			}

		}
		$insert_id = $this->db->insert_id();

		$where_1['talimar_loan'] = $talimar_loan;

		$fetch_extra_detail = $this->User_model->select_where('extra_details', $where_1);
		if ($fetch_extra_detail->num_rows() > 0) {
			$update_extra_detail['escrow_impound_account'] = $this->input->post('escrow_impound_account');
			$this->User_model->updatedata('extra_details', $where_1, $update_extra_detail);
		} else {
			$insert_extra['talimar_loan'] = $talimar_loan;
			$insert_extra['escrow_impound_account'] = $this->input->post('escrow_impound_account');
			$this->User_model->insertdata('extra_details', $insert_extra);
		}

		if ($insert_id) {
			$this->session->set_flashdata('success', 'Monthly Payment records add successfully');
			redirect(base_url() . "load_data" . $loan_id, 'refresh');
		} else {
			$this->session->set_flashdata('success', 'Monthly payment Updated Successfully!');
			redirect(base_url() . "load_data" . $loan_id, 'refresh');
		}

	}

	public function form_escrow() {
		$talimar_no = $this->input->post('talimar_no');
		$loan_id = $this->input->post('loan_id');

		$data['escrow_opened'] = $this->input->post('escrow_opened');
		$data['company'] = $this->input->post('company');
		$data['escrow'] = $this->input->post('escrow');
		$data['first_name'] = $this->input->post('first_name');
		$data['last_name'] = $this->input->post('last_name');
		$data['phone'] = $this->input->post('phone');
		$data['address'] = $this->input->post('address');
		$data['address_b'] = $this->input->post('address_b');
		$data['city'] = $this->input->post('city');
		$data['state'] = $this->input->post('state');
		$data['zip'] = $this->input->post('zip');
		$data['email'] = $this->input->post('email');

		/* 		if($this->input->post('first_payment_close'))
		{ $first_payment_close 	= '1'; }
		else
		{ $first_payment_close 	= '0'; } */

		if ($this->input->post('net_fund')) {$net_fund = '1';} else { $net_fund = '0';}

		if ($this->input->post('tax_service')) {$tax_service = '1';} else { $tax_service = '0';}

		if ($this->input->post('record_assigment_close')) {$record_assigment_close = '1';} else { $record_assigment_close = '0';}

		//$data['first_payment_close']		= $first_payment_close;
		$data['net_fund'] = $net_fund;
		$data['tax_service'] = $tax_service;
		$data['record_assigment_close'] = $record_assigment_close;
		$data['expiration'] = $this->input->post('expiration');
		$data['instruction'] = $this->input->post('instruction');
		$data['financingApp'] = $this->input->post('financingApp');
		$data['t_user_id'] = $this->session->userdata('t_user_id');

		$check_t_l['talimar_loan'] = $talimar_no;

		$check_talimar_loan = $this->User_model->select_where('loan_escrow', $check_t_l);
		//$this->session->set_flashdata('popup_modal_hit','escrow');
		$this->session->set_flashdata('popup_modal_cv', array('escrow'));
		if ($check_talimar_loan->num_rows() > 0) {
			$this->User_model->updatedata('loan_escrow', $check_t_l, $data);
			$this->session->set_flashdata('success', 'Escrow Updated Successfully!');
			redirect(base_url() . 'load_data' . $loan_id, 'refresh');
		} else {
			$data['talimar_loan'] = $talimar_no;
			$this->User_model->insertdata('loan_escrow', $data);
			$insert_id = $this->db->insert_id();
			if ($insert_id) {
				$this->session->set_flashdata('success', 'Escrow record added successfully');
				redirect(base_url() . 'load_data' . $loan_id, 'refresh');

			} else {
				$this->session->set_flashdata('error', ' Somenthing went wrong');
				redirect(base_url() . 'load_data' . $loan_id, 'refresh');
			}
		}

	}

	public function delete_talimar_loan() {
		$loan_id['id'] = $this->input->post('loan_id');
		//echo $loan_id['id'];
		$talimar_result = $this->User_model->select_where('loan', $loan_id);
		$talimar_result = $talimar_result->result();
		$talimar_loan['talimar_loan'] = $talimar_result[0]->talimar_loan;

		$sql = "SELECT DISTINCT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME IN ('talimar_loan') AND TABLE_SCHEMA =  'talimardatabase'";
		$fetch_table = $this->User_model->query($sql);
		if ($fetch_table->num_rows() > 0) {
			$fetch_table = $fetch_table->result();
			foreach ($fetch_table as $table) {
				// delete loan from all table where coloum talimar_loan available
				$this->User_model->delete($table->TABLE_NAME, $talimar_loan);
			}
		}

		$this->session->set_flashdata('success', 'Deleted');
		redirect(base_url() . 'load_data', 'refresh');
	}

	public function form_loan_title() {
	
		error_reporting(0);
		// 	echo '<pre>';
		// print_r($this->input->post());
		// echo '</pre>';
		// die('halt');
		$talimar_no = $this->input->post('talimar_no');
		$loan_id = $this->input->post('loan_id');
		$data['company'] = $this->input->post('company');
		$ptr_date = '';
		if ($this->input->post('ptr_date')) {
			//$myDateTime = DateTime::createFromFormat('m-d-Y', $this->input->post('ptr_date'));
			//$ptr_date = $myDateTime->format('Y-m-d');
			$ptr_date = input_date_format($this->input->post('ptr_date'));
		}
		$data['ptr_date'] = $ptr_date ? $ptr_date : NULL;
		// $data['title']  			= $this->input->post('title');
		$data['title_order_no'] = $this->input->post('title_order_no');
		$data['first_name'] = $this->input->post('first_name');
		$data['last_name'] = $this->input->post('last_name');

		$data['phone'] = $this->input->post('phone');
		$data['email'] = $this->input->post('email');

		$data['address'] = $this->input->post('address');

		//$data['address_b']  		= $this->input->post('address_b');
		$data['unit'] = $this->input->post('unit');

		$data['city'] = $this->input->post('city');
		$data['state'] = $this->input->post('state');
		$data['zip'] = $this->input->post('zip');

		$data['prelim_receive'] = $this->input->post('prelim_receive');
		$data['prelim_approve'] = $this->input->post('prelim_approve');
	



		$data['lender_policy_type'] = $this->input->post('lender_policy_type');
		$data['policy_amount_in_percent'] = $this->amount_format($this->input->post('policy_amount_in_percent'));
		$data['policy_amount_in_dolar'] = $this->amount_format($this->input->post('policy_amount_in_dolar'));
		$data['endorsement'] = $this->input->post('endorsement');
		$data['exception'] = $this->input->post('exception');
		$data['report_elimination'] = $this->input->post('report_elimination');
		$data['t_user_id'] = $this->session->userdata('t_user_id');
		$different['b_vesting'] = $this->input->post('vesting');

		$check_b['id']=$this->input->post('borrower_id');
		$check_borrwoer = $this->User_model->select_where('borrower_data', $check_b);

		if ($check_borrwoer->num_rows() > 0) {
		$this->User_model->updatedata('borrower_data', $check_b, $different);

	    }

		$check_t_l['talimar_loan'] = $talimar_no;

		$check_talimar_loan = $this->User_model->select_where('loan_title', $check_t_l);
		$this->session->set_flashdata('popup_modal_cv', array('title'));
		if ($check_talimar_loan->num_rows() > 0) {
			$this->User_model->updatedata('loan_title', $check_t_l, $data);
			$this->session->set_flashdata('success', 'Title Updated Successfully!');
			redirect(base_url() . 'load_data' . $loan_id, 'refresh');
		} else {
			$data['talimar_loan'] = $talimar_no;
			$this->User_model->insertdata('loan_title', $data);
			$insert_id = $this->db->insert_id();
			if ($insert_id) {
				$this->session->set_flashdata('success', 'Title record added successfully');
				redirect(base_url() . 'load_data' . $loan_id, 'refresh');

			} else {
				$this->session->set_flashdata('error', ' Somenthing went wrong');
				redirect(base_url() . 'load_data' . $loan_id, 'refresh');
			}
		}
	}

	public function form_recording_information() {
		$talimar_no = $this->input->post('talimar_no');
		$loan_id = $this->input->post('loan_id');
		// $data['recorded']				= $this->input->post('recorded');
		$data['record_option'] = $this->input->post('record_option');

		$data['recorded_date'] = NULL;
		if ($this->input->post('recorded_date')) {
			if ($this->check_date_format($this->input->post('recorded_date')) == 1) {
				//$myDateTime = DateTime::createFromFormat('m-d-Y', $this->input->post('recorded_date'));
				//$newDateString = $myDateTime->format('Y-m-d');
				$newDateString = input_date_format($this->input->post('recorded_date'));
				$data['recorded_date'] = $newDateString;
			} else {
				$data['recorded_date'] = NULL;
			}
		}

		$data['PaidtoDate'] = NULL;
		if ($this->input->post('PaidtoDate')) {
			if ($this->check_date_format($this->input->post('PaidtoDate')) == 1) {
				//$myDateTime = DateTime::createFromFormat('m-d-Y', $this->input->post('PaidtoDate'));
				//$newDateString1 = $myDateTime->format('Y-m-d');
				$newDateString1 = input_date_format($this->input->post('PaidtoDate'));
				$data['PaidtoDate'] = $newDateString1;
			} else {
				$data['PaidtoDate'] = NULL;
			}
		}

		$data['record_number'] = $this->input->post('record_number');
		$data['t_user_id'] = $this->session->userdata('t_user_id');
		$check_t_l['talimar_loan'] = $talimar_no;

		$check_talimar_loan = $this->User_model->select_where('loan_recording_information', $check_t_l);

		$this->session->set_flashdata('popup_modal_cv', array('recording_information'));
		if ($check_talimar_loan->num_rows() > 0) {
			$this->User_model->updatedata('loan_recording_information', $check_t_l, $data);
			$this->session->set_flashdata('success', 'Recording information Updated Successfully!');
			redirect(base_url() . 'load_data' . $loan_id, 'refresh');
		} else {
			$data['talimar_loan'] = $talimar_no;
			$this->User_model->insertdata('loan_recording_information', $data);
			$insert_id = $this->db->insert_id();
			if ($insert_id) {
				$this->session->set_flashdata('success', 'Record information record added successfully');
				redirect(base_url() . 'load_data' . $loan_id, 'refresh');

			} else {
				$this->session->set_flashdata('error', ' Somenthing went wrong');
				redirect(base_url() . 'load_data' . $loan_id, 'refresh');
			}
		}
	}

	public function additional_forecloser() {

		error_reporting(0);
		$tali_mar_loan = $this->input->post('talimar_loan');
		$loan_id = str_replace('/', '', $this->input->post('loan_id'));
		$forecloser_text = $this->input->post('forecloser_text');
		$forecloser_digit = $this->input->post('forecloser_digit');

		$hidden_id = $this->input->post('hidden_id');

		foreach ($forecloser_text as $key => $row) {

			if ($row != '') {

				$sel_cost = $this->User_model->query("select * from foreclosure_cost where talimar_loan = '" . $tali_mar_loan . "' AND id = '" . $hidden_id[$key] . "'");

				if ($sel_cost->num_rows() > 0) {

					$sql = "UPDATE `foreclosure_cost` SET `forecloser_text`='" . $forecloser_text[$key] . "',`forecloser_digit`='" . $this->amount_format($forecloser_digit[$key]) . "' WHERE id='" . $hidden_id[$key] . "' AND talimar_loan = '" . $tali_mar_loan . "'";

				} else {

					$sql = "INSERT INTO `foreclosure_cost`(`talimar_loan`,`loan_id`, `forecloser_text`,`forecloser_digit`) VALUES ('" . $tali_mar_loan . "','" . $loan_id . "', '" . $forecloser_text[$key] . "','" . $this->amount_format($forecloser_digit[$key]) . "')";

				}

			}
			$this->User_model->query($sql);
		}
		$this->session->set_flashdata('success', 'Foreclosure Cost updated successfully');
		$this->session->set_flashdata('popup_modal_hit', 'ts_number');
		redirect(base_url() . 'load_data/' . $loan_id);

	}

	public function loan_servicing_default_status() {

		// echo'<pre>';
		// print_r($_POST);
		// echo'</pre>';
		// die();

		$for_id = $this->input->post('for_ex_idd');
		$forr_name = $this->input->post('forr_name');
		$status_option = $this->input->post('select_for');
		$talimar_loan = $this->input->post('talimar_no');
		$loan_id = str_replace('/', '', $this->input->post('loan_id'));
		$reason_for_default = $this->input->post('reason_for_default');
		$active_forclosure = $this->input->post('active_forclosure');
		$foreclosuer_status = $this->input->post('foreclosuer_status');
		$foreclosuer_step = $this->input->post('foreclosuer_step');
		$foreclosuer_processor = $this->input->post('foreclosuer_processor');

		$contact_name = $this->input->post('contact_name');
		$c_phone = $this->input->post('c_phone');
		$c_email = $this->input->post('c_email');
		$ts_number = $this->input->post('ts_number');
		$status_update = $this->input->post('status_update');

		$borrower_outreach_option = $this->input->post('borrower_outreach_option');
		$nod_recorded_option = $this->input->post('nod_recorded_option');
		$nos_recorded_option = $this->input->post('nos_recorded_option');
		$sale_date_option = $this->input->post('sale_date_option');

		$borrower_outreach = $this->input->post('borrower_outreach');
		$borrower_outreach_new = '';
		if ($borrower_outreach) {
			//$myDateTime = DateTime::createFromFormat('m-d-Y', $borrower_outreach);
			//$newDateString = $myDateTime->format('Y-m-d');
			$newDateString = input_date_format($borrower_outreach);
			$borrower_outreach_new = $newDateString ? $newDateString : '';
		}

		$nod_recorded = $this->input->post('nod_recorded');
		$nod_recorded_new = '';
		if ($nod_recorded) {
			//$myDateTime = DateTime::createFromFormat('m-d-Y', $nod_recorded);
			//$newDateString = $myDateTime->format('Y-m-d');
			$newDateString = input_date_format($nod_recorded);
			$nod_recorded_new = $newDateString ? $newDateString : '';
		}

		$nos_recorded = $this->input->post('nos_recorded');
		$nos_recorded_new = '';
		if ($nos_recorded) {
			//$myDateTime = DateTime::createFromFormat('m-d-Y', $nos_recorded);
			//$newDateString = $myDateTime->format('Y-m-d');
			$newDateString = input_date_format($nos_recorded);
			$nos_recorded_new = $newDateString ? $newDateString : '';
		}

		$sale_date = $this->input->post('sale_date');
		$sale_date_new = '';
		if ($sale_date) {
			//$myDateTime = DateTime::createFromFormat('m-d-Y', $sale_date);
			//$newDateString = $myDateTime->format('Y-m-d');
			$newDateString = input_date_format($sale_date);
			$sale_date_new = $newDateString ? $newDateString : '';
		}

		if ($for_id == 'new') {

			$q_sql = "INSERT INTO `add_foreclosure_request`(`talimar_loan`, `loan_id`,`foreclosure_name`, `status_option`) VALUES ('" . $talimar_loan . "','" . $loan_id . "', '" . $forr_name . "', '" . $status_option . "')";

			$this->User_model->query($q_sql);
			$insert_for_id = $this->db->insert_id();

			// die();
			if ($insert_for_id) {

				$data['loan_id'] = $loan_id;
				$data['reason_for_default'] = $reason_for_default;
				$data['active_forclosure'] = $active_forclosure;
				$data['foreclosuer_status'] = $foreclosuer_status;
				$data['foreclosuer_step'] = $foreclosuer_step;
				$data['foreclosuer_processor'] = $foreclosuer_processor;
				$data['borrower_outreach'] = $borrower_outreach_new;
				$data['nod_recorded'] = $nod_recorded_new;
				$data['nos_recorded'] = $nos_recorded_new;
				$data['sale_date'] = $sale_date_new;

				$data['contact_name'] = $contact_name;
				$data['c_phone'] = $c_phone;
				$data['c_email'] = $c_email;
				$data['ts_number'] = $ts_number;
				$data['status_update'] = $status_update;

				$data['borrower_outreach_option'] = $borrower_outreach_option;
				$data['nod_recorded_option'] = $nod_recorded_option;
				$data['nos_recorded_option'] = $nos_recorded_option;
				$data['sale_date_option'] = $sale_date_option;
				$data['add_forclosure_id'] = $insert_for_id;
				$data['talimar_loan'] = $talimar_loan;

				$this->User_model->insertdata('loan_servicing_default', $data);

			}

		} else {

			$sqlll = "UPDATE `add_foreclosure_request` SET `talimar_loan`='" . $talimar_loan . "', `loan_id`='" . $loan_id . "', `foreclosure_name`='" . $forr_name . "', `status_option`='" . $status_option . "' WHERE id = '" . $for_id . "'";
			$this->User_model->query($sqlll);

			$datsa['loan_id'] = $loan_id;
			$datsa['active_forclosure'] = $active_forclosure;
			$datsa['reason_for_default'] = $reason_for_default;
			$datsa['foreclosuer_status'] = $foreclosuer_status;
			$datsa['foreclosuer_processor'] = $foreclosuer_processor;
			$datsa['foreclosuer_step'] = $foreclosuer_step;
			$datsa['borrower_outreach'] = $borrower_outreach_new;
			$datsa['nod_recorded'] = $nod_recorded_new;
			$datsa['nos_recorded'] = $nos_recorded_new;
			$datsa['sale_date'] = $sale_date_new;

			$datsa['contact_name'] = $contact_name;
			$datsa['c_phone'] = $c_phone;
			$datsa['c_email'] = $c_email;
			$datsa['ts_number'] = $ts_number;
			$datsa['status_update'] = $status_update;

			$datsa['borrower_outreach_option'] = $borrower_outreach_option;
			$datsa['nod_recorded_option'] = $nod_recorded_option;
			$datsa['nos_recorded_option'] = $nos_recorded_option;
			$datsa['sale_date_option'] = $sale_date_option;

			$datsa['talimar_loan'] = $talimar_loan;

			$where['add_forclosure_id'] = $for_id;
			$select_default = $this->User_model->query("Select * from loan_servicing_default where add_forclosure_id = '" . $for_id . "'");
			if ($select_default->num_rows() > 0) {

				$this->User_model->updatedata('loan_servicing_default', $where, $datsa);

			}
		}
		$this->session->set_flashdata('success', 'Servicing Default Status Updated Successfully!');
		$this->session->set_flashdata('popup_modal_hit', 'Foreclosure_modal');
		redirect(base_url() . 'load_data/' . $loan_id);

	}

	public function fetch_data_forcloser() {

		$id = $this->input->post('for_id');

		$sqll = "select * from loan_servicing_default  inner join  add_foreclosure_request on add_foreclosure_request.id=loan_servicing_default.add_forclosure_id where loan_servicing_default.add_forclosure_id='" . $id . "'";

		$loan_servicing_default_datas = $this->User_model->query($sqll);

		if ($loan_servicing_default_datas->num_rows() > 0) {

			$loan_servicing_default_da = $loan_servicing_default_datas->result();
			foreach ($loan_servicing_default_da as $row) {

				$data['add_forclosure_id'] = $row->add_forclosure_id;
				$data['talimar_loan'] = $row->talimar_loan;
				$data['reason_for_default'] = $row->reason_for_default;
				$data['foreclosuer_status'] = $row->foreclosuer_status;
				$data['foreclosuer_step'] = $row->foreclosuer_step;
				$data['foreclosuer_processor'] = $row->foreclosuer_processor;
				$data['borrower_outreach'] = date('m-d-Y', strtotime($row->borrower_outreach));
				$data['nod_recorded'] = date('m-d-Y', strtotime($row->nod_recorded));
				$data['nos_recorded'] = date('m-d-Y', strtotime($row->nos_recorded));
				$data['sale_date'] = date('m-d-Y', strtotime($row->sale_date));
				$data['contact_name'] = $row->contact_name;
				$data['c_phone'] = $row->c_phone;
				$data['c_email'] = $row->c_email;
				$data['ts_number'] = $row->ts_number;
				$data['status_update'] = $row->status_update;
				$data['borrower_outreach_option'] = $row->borrower_outreach_option;
				$data['nod_recorded_option'] = $row->nod_recorded_option;
				$data['nos_recorded_option'] = $row->nos_recorded_option;
				$data['sale_date_option'] = $row->sale_date_option;
				$data['add_forclosure_id'] = $row->add_forclosure_id;
				$data['foreclosure_name'] = $row->foreclosure_name;
				$data['active_forclosure'] = $row->active_forclosure;
				$data['active_forclosure'] = $row->active_forclosure;

			}
			echo json_encode($data);

		} else {

			$data['add_forclosure_id'] = '';
			$data['talimar_loan'] = '';
			$data['reason_for_default'] = '';
			$data['foreclosuer_status'] = '';
			$data['foreclosuer_processor'] = '';
			$data['borrower_outreach'] = '';
			$data['nod_recorded'] = '';
			$data['nos_recorded'] = '';
			$data['sale_date'] = '';
			$data['contact_name'] = '';
			$data['c_phone'] = '';
			$data['c_email'] = '';
			$data['ts_number'] = '';
			$data['status_update'] = '';
			$data['borrower_outreach_option'] = '';
			$data['nod_recorded_option'] = '';
			$data['nos_recorded_option'] = '';
			$data['sale_date_option'] = '';
			$data['add_forclosure_id'] = '';
			$data['foreclosure_name'] = '';
			$data['active_forclosure'] = '';

			echo json_encode($data);

		}

	}

	public function delete_record_information() {
		$data['id'] = $_POST['id'];
		$loan_id = $_POST['loan_id'];

		//   delete query
		$this->User_model->delete('loan_recording_information', $data);
		$this->session->set_flashdata('success', ' Record information deleted');
		// echo 'Deleted';
	}
	
	public function getBorrowername($borrowerid){
		
		$getBorrowername = $this->User_model->query("SELECT b_name from borrower_data Where id = '".$borrowerid."'");
		$getBorrowername = $getBorrowername->result();
		$borrowerName = $getBorrowername[0]->b_name;
		
		return $borrowerName;
	}
	
	public function getcontactNames($borrowerid){
		
		$getcontactname = $this->User_model->query("SELECT c.contact_firstname, c.contact_lastname, c.contact_phone from contact as c JOIN borrower_contact as bc ON bc.contact_id = c.contact_id Where bc.borrower_id = '".$borrowerid."'");
		$getcontactname = $getcontactname->result();
		$ContactName['cfname'] = $getcontactname[0]->contact_firstname;
		$ContactName['clname'] = $getcontactname[0]->contact_lastname;
		$ContactName['cphone'] = $getcontactname[0]->contact_phone;
		
		return $ContactName;
	}
	
	public function getLoanServicing($talimar_loan){
		
		$LoanServicing = $this->User_model->query("SELECT construction_status, list_market, servicing_agent from loan_servicing Where talimar_loan = '".$talimar_loan."'");
		$LoanServicing = $LoanServicing->result();
		
		$returndata['construction_status'] = $LoanServicing[0]->construction_status;
		$returndata['list_market'] = $LoanServicing[0]->list_market;
		
		return $returndata;
	}

	public function loan_servicing_form() {
		error_reporting(0);
		
		// echo '<pre>';
		// print_r($_POST);
		// echo '</pre>';
		
		// die('stop');

		$talimar_no = $this->input->post('talimar_no');
		$loan_id = $this->input->post('loan_id');
		$form_type = $this->input->post('form_type');

		$loan_idpp = str_replace("/", "", $loan_id);
		$FundingEntity = $this->input->post('FundingEntity');
		if(isset($FundingEntity)){
			if($FundingEntity){
				$this->User_model->query("UPDATE loan SET FundingEntity = '$FundingEntity' WHERE id = '$loan_idpp'");
			}
		}
		

		if($this->input->post('Newlate_fee') || $this->input->post('NewExtension_fee') || $this->input->post('NewMin_interest') ||$this->input->post('NewDefault_rate')){

			$Newlate_fee 		= str_replace("%", "", $this->input->post('Newlate_fee'));
			$NewExtension_fee 	= str_replace("%", "", $this->input->post('NewExtension_fee'));
			$NewMin_interest 	= str_replace("%", "", $this->input->post('NewMin_interest'));
			$NewDefault_rate 	= str_replace("%", "", $this->input->post('NewDefault_rate'));
			$loan_idss 			= str_replace("/", "", $loan_id);

			$fee_disbursement_list = $this->config->item('fee_disbursement_list');
			$myvaluearray = array($Newlate_fee,$NewExtension_fee,$NewMin_interest,$NewDefault_rate);

			$fetch_fee_disbursement = $this->User_model->select_where('fee_disbursement', array('talimar_loan'=>$talimar_no));
			if($fetch_fee_disbursement->num_rows() > 0){

				$fetch_fee_disbursement = $fetch_fee_disbursement->result();
				foreach ($fee_disbursement_list as $key => $value) {
					
					$insertrecord = $this->User_model->query("UPDATE fee_disbursement SET broker = '".$myvaluearray[$key]."' WHERE items = '".$value."' AND talimar_loan = '".$talimar_no."'");
				}

			}else{
				
				foreach ($fee_disbursement_list as $key => $value) {
					
					$insertrecord = $this->User_model->query("INSERT INTO `fee_disbursement`(`talimar_loan`, `items`, `broker`, `loan_servicer`, `lender`, `loan_id`, `t_user_id`) VALUES ('".$talimar_no."', '".$value."', '".$myvaluearray[$key]."', '0','0','".$loan_idss."', '".$this->session->userdata('t_user_id')."')");
				}
			}
		}


		if ($form_type == 'loan_servicing') {
			$this->session->set_flashdata('popup_modal_cv', array('loan_servicing'));
			$this->session->set_flashdata('popup_modal_hit', 'loan_servicing');
			$frm_name = 'Loan Servicing';
			$data['ach_activated'] = $this->input->post('ach_activated');
			$data['loan_status'] = $this->input->post('loan_status');
			$data['term_sheet'] = $this->input->post('term_sheet');
			$data['condition'] = $this->input->post('condition') ? $this->input->post('condition') : '';
			$data['loan_payment_status'] = $this->input->post('loan_payment_status') ? $this->input->post('loan_payment_status') : '';
			$data['loan_maturity_status'] = $this->input->post('loan_maturity_status') ? $this->input->post('loan_maturity_status') : '';
			$data['term_sheet_status'] = $this->input->post('term_sheet_status') ? $this->input->post('term_sheet_status') : '';
			$data['construction_status'] = $this->input->post('construction_status') ? $this->input->post('construction_status') : '0';

			$data['reason'] = $this->input->post('reason') ? $this->input->post('reason') : '';
			$data['nod_record_date'] = $this->input->post('nod_record_date') ? $this->input->post('nod_record_date') : '';
			$data['nod_sale_date'] = $this->input->post('nod_sale_date') ? $this->input->post('nod_sale_date') : '';
			$data['list_market'] = $this->input->post('list_market') ? $this->input->post('list_market') : '';
			$data['list_market_SalePrice'] = $this->input->post('list_market_SalePrice') ? $this->amount_format($this->input->post('list_market_SalePrice')) : '';
			$data['demand_requested'] = $this->input->post('demand_requested') ? $this->input->post('demand_requested') : '';
			$demand_requested_date = $this->input->post('demand_requested_date') ? $this->input->post('demand_requested_date') : '';

			if ($demand_requested_date) {
				//$myDateTime = DateTime::createFromFormat('m-d-Y', $demand_requested_date);
				//$newDateString = $myDateTime->format('Y-m-d');
				$newDateString = input_date_format($demand_requested_date);
				$data['demand_requested_date'] = $newDateString ? $newDateString : '';
			} else {

				$data['demand_requested_date'] = NULL;
			}

			$data['payoff_request'] = $this->input->post('payoff_request') ? $this->input->post('payoff_request') : '';

			$payoff_date = $this->input->post('payoff_date') ? $this->input->post('payoff_date') : '';
			if ($payoff_date) {

				//$myDateTime = DateTime::createFromFormat('m-d-Y', $payoff_date);
				//$newDateString = $myDateTime->format('Y-m-d');
				$newDateString = input_date_format($payoff_date);
				$data['payoff_date'] = $newDateString ? $newDateString : '';
			}

			$cancelled_date = $this->input->post('cancelled_date') ? $this->input->post('cancelled_date') : '';
			if ($cancelled_date) {

				//$myDateTimee = DateTime::createFromFormat('m-d-Y', $cancelled_date);
				//$newDateStringg = $myDateTimee->format('Y-m-d');
				$newDateStringg = input_date_format($cancelled_date);
				$data['cancelled_date'] = $newDateStringg ? $newDateStringg : '';
			}

			$data['paidoff_status'] = $this->input->post('paidoff_status');
			$data['dead_reason'] = $this->input->post('dead_reason') ? $this->input->post('dead_reason') : '';

			$data['closing_status'] = $this->input->post('closing_status');
			$data['boarding_status'] = $this->input->post('boarding_status');
			$data['payment_notification'] = $this->input->post('payment_notification');
			$data['payment_Exhausted'] = $this->input->post('payment_Exhausted');
			//$data['renovation'] 				= $this->input->post('renovation');
			$data['renovation'] = $this->input->post('renovation');

			$submitted_date = $this->input->post('submitted_date') ? $this->input->post('submitted_date') : '';
			if ($submitted_date) {
				//$myDateTime = DateTime::createFromFormat('m-d-Y', $submitted_date);
				//$newDateString = $myDateTime->format('Y-m-d');
				$newDateString = input_date_format($submitted_date);
				$data['submitted_date'] = $newDateString ? $newDateString : '';
			}

			$data['servicing_agent'] = $this->input->post('servicing_agent');
			$data['sub_servicing_agent'] = $this->input->post('sub_servicing_agent');
			$data['broker_disbrusement'] = $this->input->post('broker_disbrusement');
			$data['boarding_fee_paid'] = $this->input->post('boarding_fee_paid');
			$data['servicing_disb'] = $this->input->post('servicing_disb');
			$data['payment_reserve_opt'] = $this->input->post('payment_reserve_opt');
			$data['servicing_desp_verified'] = $this->input->post('servicing_desp_verified');
			$data['loan_submited'] = $this->input->post('loan_submited');

			$data['board_complete'] = $this->input->post('board_complete');
			$data['loan_board'] = $this->input->post('loan_board');
			$data['closing_checklist_complete'] = $this->input->post('closing_checklist_complete');

			// $data_user=$this->User_model->query("select role,email_address from user where role='2'");
			// $fetch_user=$data_user->result();

			// foreach($fetch_user as $key => $r) {
			// 	$admin[]=$r->email_address;
			// }
			//  $adminemails=implode(",",$admin);
			//  $emails="'".$adminemails."'";

			$position_option = $this->config->item('position_option');
			$loan_da = $this->User_model->query("select borrower,position,loan_funding_date,loan_amount from loan WHERE talimar_loan='" . $talimar_no . "'");
			$fetch_loan_result = $loan_da->result();

			$data_assigment = $this->User_model->query("select *,count(*) as lcoun,SUM(investment) as inves from loan_assigment WHERE talimar_loan='" . $talimar_no . "'");
			$fetch_ass = $data_assigment->result();
			$investment = $fetch_ass[0]->inves;
			$count_ass = $fetch_ass[0]->lcoun;

			$avaliable_balance = $fetch_loan_result[0]->loan_amount - $investment;

			$closing_date = date('m-d-Y', strtotime($fetch_loan_result[0]->loan_funding_date));
			$position = $fetch_loan_result[0]->position;

			$propertydata = $this->selected_loan_property_datas($talimar_no);
			$underwriting_value = $propertydata[0]->underwriting_value;
			$ecumdata = $this->selected_ecumbrance_datas($talimar_loan);
			$total_ecum = 0;
			foreach ($ecumdata as $value) {

				if ($value->property_home_id != '') {

					$total_ecum += $value->original_balance;
				}
			}

			$ltv = ($fetch_loan_result[0]->loan_amount + $total_ecum) / $underwriting_value * 100;

			$full_address = $propertydata[0]->property_address;
			$unit = $propertydata[0]->unit;
			$city = $propertydata[0]->city;
			$state = $propertydata[0]->state;
			$zip = $propertydata[0]->zip;

			$half_addres=$propertydata[0]->property_address.' '.$propertydata[0]->unit;
			$full_adres=$full_address.' '.$unit .'; '.$city. ', '.$state.' '.$zip;

			//internal contact...
			$select_contacts_queryy = $this->User_model->query("Select loan_originator, underwriter, loan_closer, investor_relations, loan_servicer from loan_servicing_contacts where talimar_loan = '" . $talimar_no . "'");
			$dart_fetch = $select_contacts_queryy->result();

			$inves_rel = $dart_fetch[0]->investor_relations;
			$l_servicer = $dart_fetch[0]->loan_servicer;
			$l_originator = $dart_fetch[0]->loan_originator;
			$l_underwriter = $dart_fetch[0]->underwriter;
			$l_closer = $dart_fetch[0]->loan_closer;

			$email_array = array($inves_rel, $l_servicer, $l_originator, $l_underwriter, $l_closer);
			$email_array_unique = array_unique($email_array);
			$explode_array = implode("','", $email_array_unique);

			$Ready_doc = $this->User_model->query("Select email_address from user where id IN('" . $explode_array . "')")->result();

			//construction complete...
			$all_user_12 = $this->User_model->query("select email_address from user where id ='" . $inves_rel . "'");
			$dfetch_user = $all_user_12->result();
			$email_inves = $dfetch_user[0]->email_address;

			$all_user_13 = $this->User_model->query("select email_address from user where id ='" . $l_originator . "'");
			$ddfetch_user = $all_user_13->result();
			$email_originator = $ddfetch_user[0]->email_address;

			if ($this->input->post('closing_status') == '9') {
				foreach ($Ready_doc as $key => $value) {

					$message = '';
					$email = $value->email_address;
					$subject = "Ready for Documents –" . $full_address;
					$header = "From: mail@database.talimarfinancial.com";

					$message .= 'Hi,';

					$message .= '<br>';
					$message .= '<p>The loan secured on ' . $full_address . ' is ready for loan documents. The loan amount is $' . number_format($fetch_loan_result[0]->loan_amount, 2) . ' and is secured in ' . $position_option[$position] . ' position at ' . $ltv . '%. We have ' . $count_ass . ' lenders funding the loan with $' . number_format($avaliable_balance, 2) . ' to be funded. The loan is scheduled to close on ' . $closing_date . '</p>';

					

					$regards = 'Loan Servicing<br>';

					$regards .= 'TaliMar Financial<br>';
					$regards .= 'Phone: (858) 201-3253<br>';
					$regards .= '16880 West Bernardo Ct., #140<br>';
					$regards .= 'San Diego, CA 92127<br>';
					$regards .= 'www.talimarfinancial.com';

					/*$this->email->initialize(SMTP_SENDGRID);
					$this->email
						->from('mail@talimarfinancial.com', $subject)
						->to($email)
						->subject($subject)
						->message($message)
						->set_mailtype('html');*/

					//$this->email->send();

					$dataMailA = array();
					$dataMailA['from_email'] 	= 'mail@talimarfinancial.com';
					$dataMailA['from_name'] 	= 'TaliMar Financial';
					$dataMailA['to'] 			= $email;
					$dataMailA['cc'] 			= '';
					$dataMailA['name'] 			= '';
					$dataMailA['subject'] 		= $subject;
					$dataMailA['content_body'] 	= $message;
					$dataMailA['button_url'] 	= '';
					$dataMailA['button_text'] 	= '';
					$dataMailA['regards'] 	= $regards;
					send_mail_template($dataMailA);

				}

			}
			
			//exist or not...
			$Checkloanservicinfdata = $this->getLoanServicing($talimar_no);
			
			$loanServicingEmail = '';
			if($Checkloanservicinfdata['servicing_agent'] == '14'){
				
				$loanServicingEmail = 'asullivan@trustfci.com';
				
			}elseif($Checkloanservicinfdata['servicing_agent'] == '15'){
				
				$loanServicingEmail = ''; // allready added this email;
				
			}elseif($Checkloanservicinfdata['servicing_agent'] == '19'){
				
				$loanServicingEmail = 'edpat@suddenlink.net';
			}else{
				$loanServicingEmail = '';
			}
			
			
			//Listed on Market is selected Yes, send following e-mail to Admin, Originator, and Investor Relations.
			if ($this->input->post('list_market') == '1' && $Checkloanservicinfdata['list_market'] !='1') {
				
				//$emailAdmin = 'pankaj.wartiz@gmail.com';
				$emailAdmin = 'bvandenberg@talimarfinancial.com';
				$allEmailsArray = array($email_inves,$email_originator,$emailAdmin);
				$allEmailsArray_unique = array_unique($allEmailsArray);
				$implode_array = implode(", ", $allEmailsArray_unique);
				
				$getBorrowername = $this->getBorrowername($fetch_loan_result[0]->borrower);
				$getcontactNames = $this->getcontactNames($fetch_loan_result[0]->borrower);
				
				$messagemarket = '';
				$email = $implode_array;
				//$email_t = $email_servicer;
				$subject = "Status Update – ".$full_address."";
				$header = "From: mail@database.talimarfinancial.com";

				$messagemarket .= '<p>The property on ' . $full_address . ' ' . $unit . '; ' . $city . ', ' . $state . ' ' . $zip . ' has been listed. The Borrower is '.$getBorrowername.' and the contact is '.$getcontactNames['cfname'].' '.$getcontactNames['clname'].'. '.$getcontactNames['cfname'].' can be reached at '.$getcontactNames['cphone'].'. </p>';
				$messagemarket .= '<p>Please update the Lenders on the status of the loan.</p>';
				
				/*$this->email->initialize(SMTP_SENDGRID);
				$this->email
					->from('mail@talimarfinancial.com', $subject)
					//->to('pankaj.wartiz@gmail.com')
					->to($loanServicingEmail)
					->bcc($email)
					->subject($subject)
					->message($messagemarket)
					->set_mailtype('html');

				$this->email->send();*/

				$dataMailA = array();
				$dataMailA['from_email'] 	= 'mail@talimarfinancial.com';
				$dataMailA['from_name'] 	= 'TaliMar Financial';
				$dataMailA['to'] 			= $loanServicingEmail;
				$dataMailA['cc'] 			= '';
				$dataMailA['name'] 			= '';
				$dataMailA['subject'] 		= $subject;
				$dataMailA['content_body'] 	= $messagemarket;
				$dataMailA['button_url'] 	= '';
				$dataMailA['button_text'] 	= '';
				$dataMailA['regards'] 	= 'Database';
				send_mail_template($dataMailA);

			}
			
			
			//Construction Status is selected as Complete, send following e-mail to Admin, Originator, and Investor Relations
			if ($this->input->post('construction_status') == '2' && $Checkloanservicinfdata['construction_status'] !='2') {
				
				//$emailAdmin = 'pankaj.wartiz@gmail.com';
				$emailAdmin = 'bvandenberg@talimarfinancial.com';
				$allEmailsArray = array($email_inves,$email_originator,$emailAdmin);
				$allEmailsArray_unique = array_unique($allEmailsArray);
				$implode_array = implode(", ", $allEmailsArray_unique);
				
				$getBorrowername = $this->getBorrowername($fetch_loan_result[0]->borrower);
				$getcontactNames = $this->getcontactNames($fetch_loan_result[0]->borrower);
				
				$messageconst = '';
				$email = $implode_array;
				//$email_t = $email_servicer;
				$subject = "Status Update – ".$full_address."";
				$header = "From: mail@database.talimarfinancial.com";

				$messageconst .= '<p>The renovations on ' . $full_address . ' ' . $unit . '; ' . $city . ', ' . $state . ' ' . $zip . ' are complete. The Borrower is '.$getBorrowername.' and the contact is '.$getcontactNames['cfname'].' '.$getcontactNames['clname'].'. '.$getcontactNames['cfname'].' can be reached at '.$getcontactNames['cphone'].'. </p>';
				$messageconst .= '<p>Update the Lenders on the status of the loan.</p>';
				

				/*$this->email->initialize(SMTP_SENDGRID);
				$this->email
					->from('mail@talimarfinancial.com', $subject)
					//->to('pankaj.wartiz@gmail.com')
					->to($loanServicingEmail)
					->bcc($email)
					->subject($subject)
					->message($messageconst)
					->set_mailtype('html');

				$this->email->send();*/

				$dataMailA = array();
				$dataMailA['from_email'] 	= 'mail@talimarfinancial.com';
				$dataMailA['from_name'] 	= 'TaliMar Financial';
				$dataMailA['to'] 			= $loanServicingEmail;
				$dataMailA['cc'] 			= '';
				$dataMailA['name'] 			= '';
				$dataMailA['subject'] 		= $subject;
				$dataMailA['content_body'] 	= $messageconst;
				$dataMailA['button_url'] 	= '';
				$dataMailA['button_text'] 	= '';
				$dataMailA['regards'] 	= 'Database';
				send_mail_template($dataMailA);

			}

			$this->load->helper('auto_email_helper.php');

			$select_contacts_query = $this->User_model->query("Select * from loan_servicing_contacts where talimar_loan = '" . $talimar_no . "'")->result();
			$contact_id = array();
			$loan_closer = array();
			$new_under_value = array();
			foreach ($select_contacts_query as $row) {
				if ($row != '') {

					$contact_id[] = $row->loan_servicer;
					$new_under_value[] = $row->underwriter;
					$contact_id[] = $row->investor_relations;
					$loan_closer[] = $row->loan_closer;
					$loan_closer[] = $row->underwriter;
				}
			}

			$abv = array_unique($contact_id);
			$abvvvv = array_unique($loan_closer);
			$new_under_arr = array_unique($new_under_value);

			$abc_id = implode("','", $abv);
			$abc_id_2 = implode("','", $abvvvv);
			$under_writing_im = implode("','", $new_under_arr);

			$all_user_2 = $this->User_model->query("Select email_address from user where id IN('" . $abc_id_2 . "')")->result();
			$get_under_writig = $this->User_model->query("Select email_address from user where id IN('" . $under_writing_im . "')")->result();

			$all_u = array();
			$all_user = $this->User_model->query("Select email_address from user where id IN('" . $abc_id . "')")->result();

			foreach ($all_user as $roww) {
				if ($roww != '') {

					$all_u[] = $roww->email_address;

				}

			}

			$all_usss = array();
			foreach ($all_user_2 as $rowws) {
				if ($rowws != '') {

					$all_usss[] = $rowws->email_address;

				}

			}

			$all_under = array();
			foreach ($get_under_writig as $g) {
				if ($g != '') {

					$all_under[] = $g->email_address;

				}

			}

			$name_array = implode(",", $all_u);
			$all_user_2 = implode(",", $all_usss);
			$getted_under_wtiter = implode(",", $all_under);
			// print_r($name_array);
			// die();
			//...........helper function..............//
			$l_i=str_replace("/"," ",$this->input->post('loan_id'));
			auto_email_notification($this->input->post('closing_status'), $talimar_no, $name_array, $full_address, $closing_date, $fetch_loan_result[0]->loan_amount, $unit, $city, $state, $zip, $all_user_2);

			auto_email_closing_approved($this->input->post('closing_status'), $talimar_no, $name_array, $full_address, $all_user_2);

			auto_email_document_released($this->input->post('closing_status'), $full_address, $getted_under_wtiter);
			auto_email_loan_payoff($this->input->post('loan_status'),$talimar_no,$full_adres,$half_addres,$name_array,$l_i);

		}

		if ($form_type == 'marketing_option') {

			$frm_name = 'Loan Servicing';

			//$data['comming_soon'] 					= $this->input->post('comming_soon');
			$data['market_trust_deed'] = $this->input->post('market_trust_deed');
			$data['release_social_media'] = $this->input->post('release_social_media');
			$data['trust_deed_posted_on_website'] = $this->input->post('trust_deed_posted_on_website');
			$data['new_market_trust_deed'] = $this->input->post('new_market_trust_deed');

			$data['no_address'] = $this->input->post('no_address');
			$data['posted_on_website'] = $this->input->post('posted_on_website');
			$data['posted_site_sign'] = $this->input->post('posted_site_sign');
			$data['marketing_message'] = $this->input->post('marketing_message');
			$data['loan_article'] = $this->input->post('loan_article');
			$data['youtube_url'] = $this->input->post('youtube_url');
			$data['marketing_property_type'] = $this->input->post('property_typee');
			$data['marketing_loan_type'] = $this->input->post('loan_typee');

			$data['process_bai'] 	= $this->input->post('process_bai');
			$data['compleate_bai'] 	= $this->input->post('compleate_bai');
			$data['posted_bai'] 	= $this->input->post('posted_bai');
			$data['blasted_bai'] 	= $this->input->post('blasted_bai');

			/*05-07-2021*/
			$data['image_ab_activate'] 	= $this->input->post('activate');
			$date_com =  $this->input->post('date_completed');
			//echo '<pre>??';print_r($date_com);echo '</pre>';
			$data['image_ab_date_completed'] 	= $date_com[0];
			$data['image_ab_suggested_url'] 	= $this->input->post('suggested_url');
			$data['image_ab_actual_url'] 	= $this->input->post('actual_url');
			$data['image_ab_Posted'] 	= $this->input->post('hiddn_posted');
			$data['image_ab_youtube_url'] 	= $this->input->post('add_youtube_url');

			//$implode_available_release = implode(",",$this->input->post('BAvideo_status'));
			$available_release_status="";
			if($data['process_bai']=="1"){
				$available_release_status="Requested";
			}else if($data['compleate_bai']=="1"){
				$available_release_status="Completed";
			}else if($data['posted_bai']=="1"){
				$available_release_status="Posted";
			}else{
				$available_release_status="";
			}

			$data['available_release'] = $available_release_status;
			$data['loan_closing_post'] = $this->input->post('loan_closing_postin');
			$data['loan_closing_not_post'] = $this->input->post('loan_closing_not_postin');

			$data['trust_deed_posted_on_website_not_applicable'] = $this->input->post('trust_deed_posted_on_website_not_applicable');

			$data['release_social_media_not_applicable'] = $this->input->post('release_social_media_not_applicable');

			$data['posted_on_website_not_applicable'] = $this->input->post('posted_on_website_not_applicable');

			$data['posted_site_sign_not_applicable'] = $this->input->post('posted_site_sign_not_applicable');

			$data['marketing_complete'] = $this->input->post('marketing_complete');
			$data['loan_posted'] = $this->input->post('loan_posted');
			
			// Update loan servicing checklist here start...
			$wherecheck['talimar_loan'] = $this->input->post('talimar_no');
			$select_checklistss = $this->User_model->select_where('loan_servicing_checklist', $wherecheck);
			if ($select_checklistss->num_rows() > 0) {

				$select_checklistss = $select_checklistss->result();

				foreach ($select_checklistss as $row) {

					if ($this->input->post('id_' . $row->id)) {

						$where1['id'] = $this->input->post('id_' . $row->id);
						//$where2['loan_id'] 	 =$loan_iddd;
						$update['checklist'] = $this->input->post('check_' . $row->id);
						$update['not_applicable'] = $this->input->post('not_applicable_' . $row->id);

						$this->User_model->updatedata('loan_servicing_checklist', $where1, $update);
					}
				}
			}
			
			$this->session->set_flashdata('popup_modal_cv', array('loan_servicing'));
			$this->session->set_flashdata('popup_modal_hit', 'marketing_option');
			// Update loan servicing checklist here end...
		}
		
		
		// if($form_type == 'marketing_extention'){

		// $frm_name = 'Loan Servicing';

		// $data['extension_borrower'] 			= $this->input->post('extension_borrower');
		// $data['extension_letter_sent'] 			= $this->input->post('extension_letter_sent');
		// $data['signed_extension_letter'] 		= $this->input->post('signed_extension_letter');
		// $data['extension_fee_received'] 		= $this->input->post('extension_fee_received');
		// $data['extension_request_sent_servicer'] = $this->input->post('extension_request_sent_servicer');

		// }

		// if($form_type == 'marketing_extention')
		// {

		// $frm_name = 'Loan Servicing';

		// $myDateTime 					    = DateTime::createFromFormat('m-d-Y',$this->input->post('ex_date'));
		// $newDateString_1 					= $myDateTime->format('Y-m-d');

		// $myDateTimee 					    = DateTime::createFromFormat('m-d-Y',$this->input->post('ex_date_2'));

		// $newDateString_2 					= $myDateTimee->format('Y-m-d');

		// $data['extension_offered'] 					= $this->input->post('extension_agreement_offered');
		// $data['extension_signed'] 					= $this->input->post('extension_agreement_signed');
		// $data['extension_received'] 				= $this->input->post('extension_agreement_payment_received');
		// $data['extension_processed'] 				= $this->input->post('extension_option_processed');
		// $data['extension_disbursed'] 				= $this->input->post('extension_payment_disbursed');
		// $data['extension_notes'] 					= $this->input->post('extension_notes');
		// $data['add_extension_id'] 	                = $insert_iddd;
		// $data['extension_date_from'] 	            = $newDateString_2;
		// $data['extension_to'] 	                    = $newDateString_1;
		// $data['extension_complete'] 	            = $this->input->post('extension_complete');

		// }

		if ($form_type == 'loan_assignment') {

			$frm_name = 'Lender Assignment';

			$data['lender_minimum'] = $this->amount_format($this->input->post('lender_minimum'));

			$data['servicing_fee'] = $this->amount_format($this->input->post('servicing_fee'));

			$data['servicing_lender_rate'] = $this->amount_format($this->input->post('servicing_lender_rate'));

			$data['minimum_servicing_fee'] = $this->amount_format($this->input->post('minimum_servicing_fee'));

			$data['late_fee'] = $this->amount_format($this->input->post('late_fee'));

			$data['extention_fee'] = $this->amount_format($this->input->post('extention_fee'));

			$data['min_interest'] = $this->amount_format($this->input->post('min_interest'));

			$data['default_rate'] = $this->amount_format($this->input->post('default_rate'));

			$data['broker_servicing_fees'] = $this->amount_format($this->input->post('broker_servicing_fees'));

			$data['pay_setup_fee'] = $this->input->post('pay_setup_fee');
			$data['who_pay_servicing'] = $this->input->post('who_pay_servicing');
			$data['is_multi_lender'] = $this->input->post('is_multi_lender');
			$data['lender_waives_apr'] = $this->input->post('lender_waives_apr');
			$data['loan_source'] = $this->input->post('loan_sources');
			$data['servicing_arrangent'] = $this->input->post('servicing_arrangent');
			$data['is_broker_s_agent'] = $this->input->post('is_broker_s_agent');
			$data['d_multi_property_loan'] = $this->input->post('d_multi_property_loan');

			$data['fund_intent_cell'] = $this->input->post('fund_intent_cell');
			$data['fund_broker_controll'] = $this->input->post('fund_broker_controll');
			$data['low_loan_doc'] = $this->input->post('low_loan_doc');

			$data['subordination_provision'] = $this->input->post('subordination_provision');
			$data['lender_approval'] = $this->input->post('lender_approval');
			$data['optional_insurance'] = $this->input->post('optional_insurance');
			$data['how_setup_fee_pay'] = $this->input->post('how_setup_fee_pay');
			$data['lsc'] = $this->input->post('loan_summary_complete');

			$this->session->set_flashdata('popup_modal_cv', array('assigment_main'));
		}

		$data['assigment_record_close'] = $this->input->post('assigment_record_close');
		$data['document_released'] = $this->input->post('document_released') ? '1' : '0';
		$data['document_sign'] = $this->input->post('document_sign') ? '1' : '0';
		$data['released_to_closed'] = $this->input->post('released_to_closed') ? '1' : '0';

		$data['servicer_submitted'] = $this->input->post('servicer_submitted') ? '1' : '0';
		$data['lender_fees_received'] = $this->input->post('lender_fees_received') ? '1' : '0';
		$data['website_updated'] = $this->input->post('website_updated') ? '1' : '0';
		$data['interest_payment_received'] = $this->input->post('interest_payment_received') ? '1' : '0';
		$data['renovation_reserve_received'] = $this->input->post('renovation_reserve_received') ? '1' : '0';

		$data['loan_recorded'] = $this->input->post('loan_recorded') ? '1' : '0';
		$data['fund_released'] = $this->input->post('fund_released') ? '1' : '0';

		$data['selling_price'] = $this->amount_format($this->input->post('selling_price'));
		$data['fractional_intrest'] = $this->input->post('fractional_intrest');
		$data['min_lender_investment'] = $this->amount_format($this->input->post('min_lender_investment'));
		$data['date_inetrest_paid'] = $this->input->post('date_inetrest_paid');

		$data['trust_deed_funded'] = $this->input->post('trust_deed_funded');

		$data['taxes_deliquent'] = $this->input->post('taxes_deliquent');
		$data['broker_inquiry'] = $this->input->post('broker_inquiry') ? '1' : '0';
		$data['borrower'] = $this->input->post('borrower') ? '1' : '0';
		$data['credit_report'] = $this->input->post('credit_report') ? '1' : '0';
		$data['seller_note'] = $this->input->post('seller_note') ? '1' : '0';
		$data['trustor'] = $this->input->post('trustor') ? '1' : '0';
		$data['broker_capacity'] = $this->input->post('broker_capacity');
		$data['broker_involved'] = $this->input->post('broker_involved');
		$data['a_d_behalf_another'] = $this->input->post('a_d_behalf_another') ? '1' : '0';
		$data['a_d_principal_exit_loan'] = $this->input->post('a_d_principal_exit_loan') ? '1' : '0';
		$data['a_d_arrange_sale_portion'] = $this->input->post('a_d_arrange_sale_portion') ? '1' : '0';
		$data['l_o_behalf_another'] = $this->input->post('l_o_behalf_another') ? '1' : '0';

		$data['l_o_principal_borrower'] = $this->input->post('l_o_principal_borrower') ? '1' : '0';
		$data['l_o_portion_loan'] = $this->input->post('l_o_portion_loan') ? '1' : '0';

		$data['broker_capacity_explain'] = $this->input->post('broker_capacity_explain') ? $this->input->post('broker_capacity_explain') : '';

		$data['n_i_payment_more_then'] = $this->input->post('n_i_payment_more_then');
		$data['n_i_if_yes'] = $this->input->post('n_i_if_yes');
		$data['deliquences'] = $this->input->post('deliquences');
		$data['deliq_no_explain'] = $this->input->post('deliq_no_explain');
		$data['n_subordination_explain'] = $this->input->post('n_subordination_explain') ? $this->input->post('n_subordination_explain') : '';

		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";

	
		
		$data_one['payoff_processing'] =2;
		$daa_one['talimar_loan'] = $talimar_no;

		$check_t_l['talimar_loan'] = $talimar_no;
	

		$check_talimar_loan = $this->User_model->select_where('loan_servicing', $check_t_l);

		if ($check_talimar_loan->num_rows() > 0) {
			$this->User_model->updatedata('loan_servicing', $check_t_l, $data);

		   if($this->input->post('loan_status')=='3'){

		  $check_rows_payprocessing = $this->User_model->select_where('pay_off_processing', $check_t_l);
	
			if ($check_rows_payprocessing->num_rows() == 0) {

		// $data_one['payoff_processing'] =2;
		// $daa_one['talimar_loan'] = $talimar_no;
		
			//$this->User_model->insertdata('pay_off_processing', $data_one);
		$sql = "INSERT INTO `pay_off_processing`(`talimar_loan`, `payoff_processing`,`investor_payoff`,`cabinet`,`active_loan_folder`,`active_paid_off`,`email_paid_off`,`update_sales`,`thanku_email`) VALUES ('" . $talimar_no . "','2','1','1','1','1','1','1','1')";
			$this->User_model->query($sql);

	
		    }else{

		    	echo "";
		    }

		  }
			// Update loan_assigment data
			$update_loan_assigment = $this->update_all_assigment($talimar_no);


			$this->session->set_flashdata('success', ' ' . $frm_name . ' Updated Successfully!');
			if ($this->input->post('action') == 'save') {
				
				$this->session->set_flashdata('popup_modal_hit', $form_type);

			}

			redirect(base_url() . 'load_data' . $loan_id);
		} else {

			$data['talimar_loan'] = $talimar_no;
			$data['t_user_id'] = $this->session->userdata('t_user_id');
			$this->User_model->insertdata('loan_servicing', $data);
		
		 if($this->input->post('loan_status')=='3'){
		 	
		   $sqls = "INSERT INTO `pay_off_processing`(`talimar_loan`, `payoff_processing`,`investor_payoff`,`cabinet`,`active_loan_folder`,`active_paid_off`,`email_paid_off`,`update_sales`,`thanku_email`) VALUES ('" . $talimar_no . "','2','1','1','1','1','1','1','1')";
			$this->User_model->query($sqls);

		   }

			

			// Update loan_assigment data
			$update_loan_assigment = $this->update_all_assigment($talimar_no);

			$insert_id = $this->db->insert_id();
			if ($insert_id) {
				$this->session->set_flashdata('success', '' . $frm_name . ' record added successfully');
				if ($this->input->post('action') == 'save') {

					$this->session->set_flashdata('popup_modal_hit', $form_type);
				}
				redirect(base_url() . 'load_data' . $loan_id, 'refresh');

			} else {
				$this->session->set_flashdata('error', ' Something went wrong');
				redirect(base_url() . 'load_data' . $loan_id, 'refresh');
			}
		}

	}

	public function insert_extension_request() {

		$loan_id = str_replace('/', '', $this->input->post('loan_id'));

		$frm_namee = 'Loan Extension';
		//$ex_id 	        	  = $this->input->post('ex_idd');
		$ex_id = $this->input->post('ex_idd');

		$extension_name = $this->input->post('exten_name');
		$status_box = $this->input->post('select_box_id');
		$talimar_loan = $this->input->post('talimar_no');

		// echo'<pre>';
		// print_r($_POST);
		// echo'</pre>';die();

		if ($ex_id == 'new') {

			$sql = "INSERT INTO `add_extension`(`talimar_loan`, `loan_id`,`extension_name`, `status_box`) VALUES ('" . $talimar_loan . "','" . $loan_id . "', '" . $extension_name . "', '" . $status_box . "')";

			$this->User_model->query($sql);
			$insert_iddd = $this->db->insert_id();

			if ($insert_iddd) {

				//$myDateTime = DateTime::createFromFormat('m-d-Y', $this->input->post('ex_date'));
				//$newDateString_1 = $myDateTime->format('Y-m-d');
				$newDateString_1 = input_date_format($this->input->post('ex_date'));

				//$myDateTimee = DateTime::createFromFormat('m-d-Y', $this->input->post('ex_date_2'));
				//$newDateString_2 = $myDateTimee->format('Y-m-d');
				$newDateString_2 = input_date_format($this->input->post('ex_date_2'));

				if ($this->input->post('extension_agreement_offered') != '') {

					$extension_agreement_offered = $this->input->post('extension_agreement_offered');
				} else {

					$extension_agreement_offered = '';
				}

				$dataa['extension_offered'] = $extension_agreement_offered;

				if ($this->input->post('extension_agreement_signed') != '') {

					$extension_agreement_signed = $this->input->post('extension_agreement_signed');
				} else {

					$extension_agreement_signed = '';
				}

				$dataa['extension_signed'] = $extension_agreement_signed;

				if ($this->input->post('extension_agreement_servicer_submitted') != '') {

					$extension_agreement_servicer_submitted = $this->input->post('extension_agreement_servicer_submitted');
				} else {

					$extension_agreement_servicer_submitted = '';
				}
				$dataa['extension_submitted'] = $extension_agreement_servicer_submitted;
				if ($this->input->post('extension_agreement_payment_received') != '') {

					$extension_agreement_payment_received = $this->input->post('extension_agreement_payment_received');
				} else {

					$extension_agreement_payment_received = '';
				}
				$dataa['extension_received'] = $extension_agreement_payment_received;

				if ($this->input->post('extension_option_processed') != '') {

					$extension_option_processed = $this->input->post('extension_option_processed');
				} else {

					$extension_option_processed = '';
				}

				$dataa['extension_processed'] = $extension_option_processed;

				if ($this->input->post('extension_payment_disbursed') != '') {
					$extension_payment_disbursed = $this->input->post('extension_payment_disbursed');
				} else {
					$extension_payment_disbursed = '';
				}

				$dataa['extension_disbursed'] = $extension_payment_disbursed;

				if ($this->input->post('update_loan_term') != '') {
					$updated_loan_term = $this->input->post('update_loan_term');
				} else {
					$updated_loan_term = '';
				}

				$dataa['update_loan_term'] = $updated_loan_term;


				if ($this->input->post('confirm_maturity') != '') {
					$confirm_maturity = $this->input->post('confirm_maturity');
				} else {
					$confirm_maturity = '';
				}
				$dataa['confirm_maturity'] = $confirm_maturity;


				if ($this->input->post('ext_req_borrower') != '') {
					$ext_req_borrower = $this->input->post('ext_req_borrower');
				} else {
					$ext_req_borrower = '';
				}
				$dataa['ext_req_borrower'] = $ext_req_borrower;

				if ($this->input->post('ext_app_rec_lender') != '') {
					$ext_app_rec_lender = $this->input->post('ext_app_rec_lender');
				} else {
					$ext_app_rec_lender = '';
				}
				$dataa['ext_app_rec_lender'] = $ext_app_rec_lender;




				$dataa['extension_notes'] = $this->input->post('extension_notes');

				if ($this->input->post('extension_complete') != '') {

					$extension_complete = $this->input->post('extension_complete');
				} else {

					$extension_complete = '';
				}

				$dataa['extension_complete'] = $extension_complete;

				if ($this->input->post('notify_loanext') != '') {
					$notify_loanext = $this->input->post('notify_loanext');
				} else {
					$notify_loanext = '';
				}
				$dataa['notify_loanext'] = $notify_loanext;
				

				if ($this->input->post('ext_accrued') != '') {

					$ext_accrued = $this->input->post('ext_accrued');
				} else {

					$ext_accrued = '';
				}

				$dataa['ext_accrued'] = $ext_accrued;
				$dataa['PaymentType'] = $this->input->post('PaymentType');
				$dataa['ext_lender_app'] = $this->input->post('ext_lender_app');
				$dataa['ext_process_option'] = $this->input->post('ext_process_option');
				$dataa['BorrowerApproval'] = $this->input->post('BorrowerApproval');
				$dataa['pay_amt_val'] = $this->amount_format($this->input->post('pay_amt_val'));
				$dataa['pay_amt_opt'] = $this->input->post('pay_amt_opt');
				$dataa['PaymentCal'] = $this->input->post('PaymentCal');


				$dataa['add_extension_id'] = $insert_iddd;
				$dataa['t_user_id'] = $this->session->userdata('t_user_id');
				$dataa['extension_date_from'] = $newDateString_2;
				$dataa['extension_to'] = $newDateString_1;

				$dataa['talimar_loan'] = $talimar_loan;

				$this->User_model->insertdata('extension_section', $dataa);

			}

		} else {

			$sqll = "UPDATE `add_extension` SET `talimar_loan`='" . $talimar_loan . "', `loan_id`='" . $loan_id . "', `extension_name`='" . $extension_name . "', `status_box`='" . $status_box . "' WHERE id = '" . $ex_id . "'";
			$this->User_model->query($sqll);

			//$myDateTime = DateTime::createFromFormat('m-d-Y', $this->input->post('ex_date'));
			//$newDateString_1 = $myDateTime->format('Y-m-d');
			$newDateString_1 = input_date_format($this->input->post('ex_date'));

			//$myDateTimee = DateTime::createFromFormat('m-d-Y', $this->input->post('ex_date_2'));
			//$newDateString_2 = $myDateTimee->format('Y-m-d');
			$newDateString_2 = input_date_format($this->input->post('ex_date_2'));

			if ($this->input->post('extension_agreement_offered') != '') {

				$extension_agreement_offered = $this->input->post('extension_agreement_offered');
			} else {

				$extension_agreement_offered = '';
			}

			$dataaa['extension_offered'] = $extension_agreement_offered;

			if ($this->input->post('extension_agreement_signed') != '') {

				$extension_agreement_signed = $this->input->post('extension_agreement_signed');
			} else {

				$extension_agreement_signed = '';
			}

			$dataaa['extension_signed'] = $extension_agreement_signed;
			if ($this->input->post('extension_agreement_servicer_submitted') != '') {
				$extension_agreement_servicer_submitted = $this->input->post('extension_agreement_servicer_submitted');
			} else {
				$extension_agreement_servicer_submitted = '';
			}
			$dataaa['extension_submitted'] = $extension_agreement_servicer_submitted;
			if ($this->input->post('extension_agreement_payment_received') != '') {

				$extension_agreement_payment_received = $this->input->post('extension_agreement_payment_received');
			} else {

				$extension_agreement_payment_received = '';
			}

			$dataaa['extension_received'] = $extension_agreement_payment_received;

			if ($this->input->post('extension_option_processed') != '') {

				$extension_option_processed = $this->input->post('extension_option_processed');
			} else {

				$extension_option_processed = '';
			}

			$dataaa['extension_processed'] = $extension_option_processed;

			if ($this->input->post('extension_payment_disbursed') != '') {

				$extension_payment_disbursed = $this->input->post('extension_payment_disbursed');
			} else {

				$extension_payment_disbursed = '';
			}

			$dataaa['extension_disbursed'] = $extension_payment_disbursed;

			$dataaa['extension_notes'] = $this->input->post('extension_notes');

			if ($this->input->post('extension_complete') != '') {

				$extension_complete = $this->input->post('extension_complete');
			} else {

				$extension_complete = '';
			}

			$dataaa['extension_complete'] = $extension_complete;



			if ($this->input->post('update_loan_term') != '') {
				$updated_loan_term = $this->input->post('update_loan_term');
			} else {
				$updated_loan_term = '';
			}

			$dataaa['update_loan_term'] = $updated_loan_term;


			if ($this->input->post('confirm_maturity') != '') {
				$confirm_maturity = $this->input->post('confirm_maturity');
			} else {
				$confirm_maturity = '';
			}
			$dataaa['confirm_maturity'] = $confirm_maturity;


			if ($this->input->post('ext_req_borrower') != '') {
				$ext_req_borrower = $this->input->post('ext_req_borrower');
			} else {
				$ext_req_borrower = '';
			}
			$dataaa['ext_req_borrower'] = $ext_req_borrower;

			if ($this->input->post('ext_app_rec_lender') != '') {
				$ext_app_rec_lender = $this->input->post('ext_app_rec_lender');
			} else {
				$ext_app_rec_lender = '';
			}
			$dataaa['ext_app_rec_lender'] = $ext_app_rec_lender;


			if ($this->input->post('notify_loanext') != '') {
				$notify_loanext = $this->input->post('notify_loanext');
			} else {
				$notify_loanext = '';
			}
			$dataaa['notify_loanext'] = $notify_loanext;


			if ($this->input->post('ext_accrued') != '') {

				$ext_accrued = $this->input->post('ext_accrued');
			} else {

				$ext_accrued = '';
			}

			$dataaa['ext_accrued'] = $ext_accrued;
			$dataaa['PaymentType'] = $this->input->post('PaymentType');
			$dataaa['ext_lender_app'] = $this->input->post('ext_lender_app');
			$dataaa['ext_process_option'] = $this->input->post('ext_process_option');
			$dataaa['BorrowerApproval'] = $this->input->post('BorrowerApproval');
			$dataaa['pay_amt_val'] = $this->amount_format($this->input->post('pay_amt_val'));
			$dataaa['pay_amt_opt'] = $this->input->post('pay_amt_opt');
			$dataaa['PaymentCal'] = $this->input->post('PaymentCal');

			//$dataaa['add_extension_id'] 	                = $insert_iddd;
			$dataaa['extension_date_from'] = $newDateString_2;
			$dataaa['extension_to'] = $newDateString_1;
			//$dataaa['extension_complete'] 	            = $this->input->post('extension_complete');

			$dataaa['talimar_loan'] = $talimar_loan;
			$dataaa['t_user_id'] = $this->session->userdata('t_user_id');

			$check_t_lll['add_extension_id'] = $ex_id;
			$check_ta = $this->User_model->select_where('extension_section', $check_t_lll);

			if ($check_ta->num_rows() > 0) {

				$this->User_model->updatedata('extension_section', $check_t_lll, $dataaa);

			}
		}

		$this->session->set_flashdata('popup_modal_hit', 'extension_load');
		$this->session->set_flashdata('success', ' ' . $frm_namee . '  Successfully!');
		redirect(base_url() . 'load_data/' . $loan_id);

	}

	public function fetch_data_extension() {

		$id = $this->input->post('ext_id');

		$sql = "select * from extension_section  inner join  add_extension on add_extension.id=extension_section.add_extension_id where extension_section.add_extension_id='" . $id . "'";

		$loan_servicing_datas = $this->User_model->query($sql);

		if ($loan_servicing_datas->num_rows() > 0) {

			$loan_servicing_datas = $loan_servicing_datas->result();
			foreach ($loan_servicing_datas as $row) {

				$data['add_extension_id'] = $row->add_extension_id;
				$data['talimar_loan'] = $row->talimar_loan;
				$data['extension_to'] = date('m-d-Y', strtotime($row->extension_to));
				$data['extension_date_from'] = date('m-d-Y', strtotime($row->extension_date_from));
				$data['extension_complete'] = $row->extension_complete;
				$data['extension_disbursed'] = $row->extension_disbursed;
				$data['extension_processed'] = $row->extension_processed;
				$data['extension_received'] = $row->extension_received;
				$data['extension_submitted'] = $row->extension_submitted;
				$data['extension_offered'] = $row->extension_offered;
				$data['extension_signed'] = $row->extension_signed;
				$data['extension_name'] = $row->extension_name;
				$data['extension_notes'] = $row->extension_notes;
				$data['PaymentType'] 		= $row->PaymentType;
				$data['ext_accrued'] 		= $row->ext_accrued;
				$data['update_loan_term'] 	= $row->update_loan_term;
				$data['confirm_maturity'] 	= $row->confirm_maturity;
				$data['ext_lender_app'] 	= $row->ext_lender_app;
				$data['ext_process_option'] = $row->ext_process_option;
				$data['ext_req_borrower']   = $row->ext_req_borrower;
				$data['ext_app_rec_lender'] = $row->ext_app_rec_lender;
				$data['BorrowerApproval'] = $row->BorrowerApproval;
				$data['pay_amt_opt'] = $row->pay_amt_opt;
				$data['pay_amt_val'] = $row->pay_amt_val;
				$data['PaymentCal'] = $row->PaymentCal;
				$data['notify_loanext'] = $row->notify_loanext;

			}
			echo json_encode($data);

		} else {

			$data['add_extension_id'] = '';
			$data['talimar_loan'] =
			$data['extension_to'] = '';
			$data['extension_date_from'] = '';
			$data['extension_complete'] = '';
			$data['extension_disbursed'] = '';
			$data['extension_processed'] = '';
			$data['extension_received'] = '';
			$data['extension_offered'] = '';
			$data['extension_signed'] = '';
			$data['extension_name'] = '';
			$data['extension_notes'] = '';
			$data['PaymentType'] 		= '';
			$data['ext_accrued'] 		= '';

			echo json_encode($data);

		}

	}

	public function pay_off_processing_form() {

		$talimar_no = $this->input->post('talimar_no');
		$loan_id = $this->input->post('loan_id');
		$form_type_value = $this->input->post('pay_off_button');
		$data_one['investor_payoff'] = ($this->input->post('investor_payoff') == 'on') ? 2 : 1;
		$data_one['cabinet'] = ($this->input->post('cabinet') == 'on') ? 2 : 1;
		$data_one['active_loan_folder'] = ($this->input->post('active_loan_folder') == 'on') ? 2 : 1;
		$data_one['active_paid_off'] = ($this->input->post('active_paid_off') == 'on') ? 2 : 1;
		$data_one['email_paid_off'] = ($this->input->post('email_paid_off') == 'on') ? 2 : 1;
		$data_one['update_sales'] = ($this->input->post('update_sales') == 'on') ? 2 : 1;
		$data_one['thanku_email'] = ($this->input->post('thanku_email') == 'on') ? 2 : 1;
		$data_one['payoff_processing'] = ($this->input->post('payoff_processing') == 'on') ? 1 : 2;
		$data_one['loan_folder'] = ($this->input->post('loan_folder') == 'on') ? 2 : 1;

		$data_one['lender_folder'] = ($this->input->post('lender_folder') == 'on') ? 2 : 1;
		/*21-07-2021 By@Aj*/
		$data_one['process_before_after_img'] = ($this->input->post('process_before_after_img') == 'on') ? 2 : 1;
		/*End*/
		/*21-07-2021 By@Aj*/
		$data_one['auto_notification_off'] = ($this->input->post('auto_notification_off') == 'on') ? 2 : 1;
		/*End*/
		$data_one['de_Payoff_Request'] = $this->input->post('de_Payoff_Request');
		if($this->input->post('de_Good_Thru_Date') != ''){
			//$myDateTime = DateTime::createFromFormat('m-d-Y', $this->input->post('de_Good_Thru_Date'));
			//$newDateString = $myDateTime->format('Y-m-d');
			$newDateString = input_date_format($this->input->post('de_Good_Thru_Date'));
			$data_one['de_Good_Thru_Date'] = $newDateString;
		}else{
			$data_one['de_Good_Thru_Date'] = '';
		}

		$data_one['payoff_type'] = $this->input->post('payoff_type');
		$data_one['de_Servicing_Approved'] = $this->input->post('de_Servicing_Approved');
		$data_one['de_Admin_Approved'] = $this->input->post('de_Admin_Approved');

		$daa_one['talimar_loan'] = $talimar_no;

		if ($form_type_value == 'checksave') {

			$frm_nam = 'Pay off Processing';
			$check = $this->User_model->select_where('pay_off_processing', $daa_one);

			if ($check->num_rows() > 0) {

				$this->User_model->updatedata('pay_off_processing', $daa_one, $data_one);
				$this->session->set_flashdata('success', ' ' . $frm_nam . ' Updated Successfully!');
				redirect(base_url() . 'load_data' . $loan_id);
			} else {
				$data_one['talimar_loan'] = $talimar_no;

				$this->User_model->insertdata('pay_off_processing', $data_one);
				$this->session->set_flashdata('success', '' . $frm_nam . ' record added successfully');
				redirect(base_url() . 'load_data' . $loan_id);

			}

		}

	}

	public function loan_notes() {
		$talimar_loan = $this->input->post('talimar_no');
		$talimar_no['talimar_loan'] = $this->input->post('talimar_no');
		$loan_id = $this->input->post('loan_id');
		$notes_date = $this->input->post('notes_date');
		$notes = $this->input->post('notes');
		$t_user_id = $this->session->userdata('t_user_id');

		$f_delete['talimar_loan'] = $talimar_loan;
		$data_fetch_all = $this->User_model->select_where('loan_notes', $f_delete);
		$data_fetch_all = $data_fetch_all->result();
		$avilable = array();
		$come_array = array();
		foreach ($data_fetch_all as $row) {
			$avilable[] = $row->notes_date;
		}
		foreach ($notes_date as $row) {
			$come_array[] = $row;
		}
		if (count($come_array) < 1) {
			$this->User_model->delete('loan_notes', $talimar_no);
			$this->session->set_flashdata('success', ' Deleted');
			redirect(base_url() . 'load_data' . $loan_id, 'refresh');
			die();
		}
		$difference = array_diff($avilable, $come_array);

		foreach ($difference as $row) {
			$delete_data['notes_date'] = $row;
			$delete_data['talimar_loan'] = $this->input->post('talimar_no');

			$this->User_model->delete('loan_notes', $delete_data);
		}
		//----------------------------------------
		$count = 1;
		foreach ($notes_date as $key => $row) {

			if ($row != '') {
				//$myDateTime = DateTime::createFromFormat('m-d-Y', $notes_date[$key]);
				//$newDateString = $myDateTime->format('Y-m-d');
				$newDateString = input_date_format($notes_date[$key]);

				$check_available['talimar_loan'] = $talimar_loan;
				$check_available['notes_date'] = $row;
				$check_update = $this->User_model->select_where('loan_notes', $check_available);
				if ($check_update->num_rows() > 0) {
					$sql = "UPDATE loan_notes SET notes = '" . $notes[$key] . "'  , position = '" . $count . "' WHERE talimar_loan = '" . $talimar_loan . "' AND notes_date = '" . $newDateString . "' ";
				} else {
					$sql = "INSERT INTO loan_notes (talimar_loan, notes_date , notes , position , t_user_id) VALUES ('" . $talimar_loan . "' , '" . $newDateString . "' , '" . $notes[$key] . "' ,  '" . $count . "' , '" . $t_user_id . "')";
				}
				$this->User_model->query($sql);
			}
			$count++;
		}
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			$this->session->set_flashdata('success', ' Loan Notes Records Added Successfully ');
			redirect(base_url() . 'load_data' . $loan_id, 'refresh');
		} else {
			$this->session->set_flashdata('success', ' Loan Notes Updated Successfully! ');
			redirect(base_url() . 'load_data' . $loan_id, 'refresh');
		}
	}

	public function delete_loan_notes() {
		$id = $this->input->post('id');
		$this->User_model->delete('contact_notes', array('id' => $id));
		echo 1;
	}

	public function delete_underwriting_item() {
		$id = $this->input->post('id');
		$this->User_model->delete('underwriting', array('id' => $id));
		echo 1;
	}

	public function loan_assigment() {
		error_reporting(0);

		$talimar_loan = $this->input->post('talimar_no');
		$talimar_no['talimar_loan'] = $this->input->post('talimar_no');
		$loan_id = $this->input->post('loan_id');

		// if($this->input->post('set_talimar_data'))
		// {
		// $lender_id['id'] = '10';
		// $fetch_lender_data = $this->User_model->select_where('investor',$lender_id);
		// $fetch_lender_data = $fetch_lender_data->result();
		// $talimar_lender = $fetch_lender_data[0]->talimar_lender;

		// $fetch_loan_data = $this->User_model->select_where('loan',$talimar_no);
		// $fetch_loan_data = $fetch_loan_data->result();

		// $loan_amount 		= $fetch_loan_data[0]->loan_amount;
		// $intrest_rate 		= $fetch_loan_data[0]->intrest_rate;
		// $fetch_servicing 			= $this->User_model->select_where('loan_servicing',$talimar_no);
		// $fetch_servicing 			= $fetch_servicing->result();
		// $servicing_fee				= $fetch_servicing[0]->servicing_fee;
		// $invester_yield_percent1		= $intrest_rate - $servicing_fee;
		// $payment1	= (($invester_yield_percent1/100) * $loan_amount)/12;
		// $check_assig['lender_name'] 	= 10;
		// $check_assig['talimar_loan'] 	= $talimar_loan;

		// $check_assign_avilable = $this->User_model->select_where('loan_assigment',$check_assig);
		// if($check_assign_avilable->num_rows() > 0)
		// {
		// $this->session->set_flashdata('error','TaliMar Finacial Inc already exist in assigment');
		// redirect(base_url().'load_data'.$loan_id , 'refresh');
		// }
		// else
		// {
		// $insert_assign['talimar_loan'] 		= $talimar_loan;
		// $insert_assign['lender_name'] 		= 10;
		// $insert_assign['lender_account'] 	= $talimar_lender;
		// $insert_assign['investment'] 		= $loan_amount;
		// $insert_assign['percent_loan'] 		= 100;
		// $insert_assign['invester_yield_percent'] 		= $invester_yield_percent1;
		// $insert_assign['payment'] 		= $payment1;
		// $this->User_model->insertdata('loan_assigment',$insert_assign);

		// $this->session->set_flashdata('success','Updated');
		// redirect(base_url().'load_data'.$loan_id , 'refresh');
		// }

		// }
		$lender_name = $this->input->post('lender_name');
		$lender_account = $this->input->post('lender_account');
		$investment = $this->input->post('investment');
		$investmenttotal = $this->input->post('investmenttotal');
		$percent_loan = $this->input->post('percent_loan');
		$invester_yield_percent = $this->input->post('invester_yield_percent');
		$payment 		= $this->input->post('payment');
		$paymenttotal 	= $this->input->post('paymenttotal');
		$start_date = $this->input->post('start_date');
		$secured_dot = '0';
		$disclosures_submit = '0';
		$disclosures_execute = '0';
		$assigment_submit = '0';
		$servicer_submit = '0';
		$file_close = '0';
		$fund_received = '0';
		//$fund_released 				= $this->input->post('fund_released');
		// $recived_loan_doc 				= $this->input->post('recived_loan_doc');
		$servicer_confirm = '0';
		$this_id = $this->input->post('this_id');
		$t_user_id = $this->session->userdata('t_user_id');

		$f_delete['talimar_loan'] = $talimar_loan;
		$data_fetch_all = $this->User_model->select_where('loan_assigment', $f_delete);
		$data_fetch_all = $data_fetch_all->result();
		$avilable = array();
		$come_array = array();
		if ($this->input->post('set_talimar_data')) {
			$check_extra_detail = $this->User_model->select_where('extra_details', $talimar_no);
			if ($check_extra_detail->num_rows() > 0) {
				$sql = "UPDATE extra_details SET draft_talimar_inc = '1' WHERE talimar_loan = '" . $talimar_loan . "' ";
			} else {
				$sql = "INSERT INTO extra_details('talimar_loan, draft_talimar_inc', t_user_id) VALUES('" . $talimar_loan . "', '1', '" . $t_user_id . "')";
			}
			$this->User_model->query($sql);
		} else {

			$sql = "UPDATE extra_details SET draft_talimar_inc = NULL WHERE talimar_loan = '" . $talimar_loan . "' ";
			$this->User_model->query($sql);
		}

		foreach ($data_fetch_all as $row) {
			$avilable[] = $row->id;
		}
		foreach ($this_id as $row) {
			$come_array[] = $row;
		}
		if (count($come_array) < 1) {
			$this->User_model->delete('loan_assigment', $talimar_no);
			$this->session->set_flashdata('success', ' Deleted');
			redirect(base_url() . 'load_data' . $loan_id, 'refresh');
			die();
		}
		$difference = array_diff($avilable, $come_array);

		foreach ($difference as $row) {
			$delete_data['id'] = $row;
			// $delete_data['talimar_loan'] 	=  $this->input->post('talimar_no');
			$this->User_model->delete('loan_assigment', $delete_data);
		}
		//----------------------------------------
		$count = 1;

		// foreach($lender_account as $key => $row)
		foreach ($lender_name as $key => $row) {
			if ($row != '') {

				$paymentnew = ($this->amount_format($investment[$key])*($this->amount_format($invester_yield_percent[$key])/12))/100;

				$paymenttotalnew = ($this->amount_format($investmenttotal[$key])*($this->amount_format($invester_yield_percent[$key])/12))/100;

				if ($this_id[$key] != '') {
					$check_available['talimar_loan'] = $talimar_loan;
					// $check_available['lender_account'] 	= $row;
					$check_available['lender_name'] = $row;
					$check_update = $this->User_model->select_where('loan_assigment', $check_available);

					$sql = "UPDATE loan_assigment SET lender_name = '" . $lender_name[$key] . "' ,lender_account = '" . $lender_account[$key] . "', investment = '" . $this->amount_format($investment[$key]) . "' , investmenttotal ='".$this->amount_format($investmenttotal[$key])."', percent_loan = '" . $this->amount_format($percent_loan[$key]) . "' , invester_yield_percent = '" . $this->amount_format($invester_yield_percent[$key]) . "' , payment = '" . $this->amount_format($paymentnew) . "' , paymenttotal = '".$this->amount_format($paymenttotalnew)."', position = '" . $count . "' WHERE id = '" . $this_id[$key] . "' ";

				} else {

					$sql = "INSERT INTO `loan_assigment`(`talimar_loan`, `lender_name`, `lender_account`, `investment`,`investmenttotal`, `percent_loan`, `invester_yield_percent`, `payment`, `paymenttotal`,`secured_dot`, `disclosures_submit`, `disclosures_execute`,  `fund_received`, `fund_released`, `assigment_submit`, `servicer_submit`, `servicer_confirm` , `file_close` , `position` , `t_user_id`) VALUES ('" . $talimar_loan . "' , '" . $lender_name[$key] . "' , '" . $lender_account[$key] . "' , '" . $this->amount_format($investment[$key]) . "' , '".$this->amount_format($investmenttotal[$key])."', '" . $this->amount_format($percent_loan[$key]) . "' , '" . $invester_yield_percent[$key] . "' , '" . $this->amount_format($paymentnew) . "' , '".$this->amount_format($paymenttotalnew)."' , '" . $secured_dot[$key] . "', '" . $disclosures_submit[$key] . "' , '" . $disclosures_execute[$key] . "' ,  '" . $fund_received[$key] . "' , '" . $fund_released[$key] . "' , '" . $assigment_submit[$key] . "' , '" . $servicer_submit[$key] . "' , '" . $servicer_confirm[$key] . "' , '" . $file_close[$key] . "', '" . $count . "' , '" . $t_user_id . "')";
				}

				$this->User_model->query($sql);
			}
			$count++;
		}
		$insert_id = $this->db->insert_id();

		$this->session->set_flashdata('popup_modal_cv', array('assigment_main', 'assigment'));

		if ($insert_id) {
			$this->session->set_flashdata('success', ' Assignments Records Added Successfully! ');
			if ($this->input->post('action') == 'save') {

				$this->session->set_flashdata('popup_modal_hit', 'assignment');
			}
			redirect(base_url() . 'load_data' . $loan_id, 'refresh');
		} else {
			$this->session->set_flashdata('success', ' Assignments Updated Successfully! ');
			if ($this->input->post('action') == 'save') {

				$this->session->set_flashdata('popup_modal_hit', 'assignment');
			}
			redirect(base_url() . 'load_data' . $loan_id, 'refresh');
		}

	}

	public function loan_assigment_checkbox() {

		$talimar_loan = $this->input->post('talimar_noo');
		$talimar_no['talimar_loan'] = $this->input->post('talimar_noo');
		$loan_id = $this->input->post('loan_idd');

		$start_date = $this->input->post('start_date');
		$secured_dot = $this->input->post('secured_dot');
		$disclosures_submit = $this->input->post('disclosures_submit');
		$disclosures_execute = $this->input->post('disclosures_execute');
		$assigment_submit = $this->input->post('assigment_submit');
		$servicer_submit = $this->input->post('servicer_submit');
		$file_close = $this->input->post('file_close');
		$fund_received = $this->input->post('fund_received');
		
		$servicer_confirm = $this->input->post('servicer_confirm');
		$this_id = $this->input->post('this_idd');
		$t_user_id = $this->session->userdata('t_user_id');

		$nsecurity_type = $this->input->post('nsecurity_type');
		$ndisclouser_status = $this->input->post('ndisclouser_status');
		$nfunds_received = $this->input->post('nfunds_received');
		$nservicer_status = $this->input->post('nservicer_status');
		$nfiles_status = $this->input->post('nfiles_status');

		$paid_to = $this->input->post('paid_to');
		$recording_status = $this->input->post('recording_status');

		//  print_r($this->input->post());
		//
		//	die();
		// foreach($lender_account as $key => $row)
		foreach ($this_id as $key => $row) {

			$sql = "UPDATE loan_assigment SET  start_date= '" . $start_date[$key] . "', secured_dot= '" . $secured_dot[$key] . "', disclosures_submit = '" . $disclosures_submit[$key] . "' , disclosures_execute = '" . $disclosures_execute[$key] . "' , fund_received = '" . $fund_received[$key] . "'  ,assigment_submit = '" . $assigment_submit[$key] . "' ,  servicer_submit = '" . $servicer_submit[$key] . "' , file_close = '" . $file_close[$key] . "', servicer_confirm= '" . $servicer_confirm[$key] . "', nsecurity_type='" . $nsecurity_type[$key] . "', ndisclouser_status='" . $ndisclouser_status[$key] . "',nfunds_received='" . $nfunds_received[$key] . "', nservicer_status='" . $nservicer_status[$key] . "', nfiles_status='" . $nfiles_status[$key] . "', paid_to='" . $paid_to[$key] . "', recording_status='" . $recording_status[$key] . "'  WHERE id = '" . $this_id[$key] . "'";

			$this->User_model->query($sql);
		}

		$this->session->set_flashdata('success', ' Assignments Updated Successfully! ');
		if ($this->input->post('actionn') == 'savee') {

			$this->session->set_flashdata('popup_modal_hit', 'assignment_checkbox');
		}

		$this->session->set_flashdata('popup_modal_cv', array('assigment_main', 'assigment', 'status'));
		redirect(base_url() . 'load_data' . $loan_id, 'refresh');

	}

	/*
		Description : This function use for save Loan Reserve - Add "Draw Approved by Loan Servicing" section field	
		Author      : Bitcot
		Created     : 
		Modified    : 07-04-2021
	*/

	public function loan_reserve_draws() {

		

		$talimar_loan = $this->input->post('talimar_loan');
		$loan_id = $this->input->post('loan_id');
		$draws = $this->input->post('draws'); /*this draws value is loan_reserve_draws table id...*/
		$enter_date = $this->input->post('enter_date');
		$draw_description = $this->input->post('draw_description');
		$budget = $this->input->post('budget');
		$enter_amount_1 = $this->input->post('enter_amount_1');
		$enter_amount_2 = $this->input->post('enter_amount_2');
		$enter_amount_3 = $this->input->post('enter_amount_3');
		$enter_amount_4 = $this->input->post('enter_amount_4');
		$enter_amount_5 = $this->input->post('enter_amount_5');
		$enter_amount_6 = $this->input->post('enter_amount_6');
		$enter_amount_7 = $this->input->post('enter_amount_7');
		$enter_amount_8 = $this->input->post('enter_amount_8');
		$enter_amount_9 = $this->input->post('enter_amount_9');
		$enter_amount_10 = $this->input->post('enter_amount_10');
		$enter_amount_11 = $this->input->post('enter_amount_11');
		$enter_amount_12 = $this->input->post('enter_amount_12');
		$enter_amount_13 = $this->input->post('enter_amount_13');
		$enter_amount_14 = $this->input->post('enter_amount_14');
		$enter_amount_15 = $this->input->post('enter_amount_15');
		$lock_loan_reserve = $this->input->post('lock_loan_reserve');

		$draw_request_loan = $this->input->post('draw_request_loan');

		$draw_approved = $this->input->post('draw_approved');
		$draw_request = $this->input->post('draw_request');
		$draw_released = $this->input->post('draw_released');
		$draw_submit = $this->input->post('draw_submit');

		$fund_servicer = $this->input->post('fund_servicer');
		$fund_account = $this->input->post('fund_account');
		$draw_cost_per = $this->amount_format($this->input->post('draw_cost_per'));

		if ($enter_date[0] || $enter_date[1] || $enter_date[2] || $enter_date[3] || $enter_date[4] || $enter_date[5] || $enter_date[6] || $enter_date[7] || $enter_date[8] || $enter_date[9] || $enter_date[10] || $enter_date[11] || $enter_date[12] || $enter_date[13] || $enter_date[14] != '') {

			//$date = DateTime::createFromFormat('m-d-Y', $enter_date[0]);
			$start_date0 = input_date_format($enter_date[0]);//$date ? $date->format('Y-m-d') : NULL;

			//$date = DateTime::createFromFormat('m-d-Y', $enter_date[1]);
			$start_date1 = input_date_format($enter_date[1]);//$date ? $date->format('Y-m-d') : NULL;

			//$date = DateTime::createFromFormat('m-d-Y', $enter_date[2]);
			$start_date2 = input_date_format($enter_date[2]);//$date ? $date->format('Y-m-d') : NULL;

			//$date = DateTime::createFromFormat('m-d-Y', $enter_date[3]);
			$start_date3 = input_date_format($enter_date[3]);//$date ? $date->format('Y-m-d') : NULL;

			//$date = DateTime::createFromFormat('m-d-Y', $enter_date[4]);
			$start_date4 = input_date_format($enter_date[4]);//$date ? $date->format('Y-m-d') : NULL;

			//$date = DateTime::createFromFormat('m-d-Y', $enter_date[5]);
			$start_date5 = input_date_format($enter_date[5]);//$date ? $date->format('Y-m-d') : NULL;

			//$date = DateTime::createFromFormat('m-d-Y', $enter_date[6]);
			$start_date6 = input_date_format($enter_date[6]);//$date ? $date->format('Y-m-d') : NULL;

			//$date = DateTime::createFromFormat('m-d-Y', $enter_date[7]);
			$start_date7 = input_date_format($enter_date[7]);//$date ? $date->format('Y-m-d') : NULL;

			//$date = DateTime::createFromFormat('m-d-Y', $enter_date[8]);
			$start_date8 = input_date_format($enter_date[8]);//$date ? $date->format('Y-m-d') : NULL;

			//$date = DateTime::createFromFormat('m-d-Y', $enter_date[9]);
			$start_date9 = input_date_format($enter_date[9]);//$date ? $date->format('Y-m-d') : NULL;

			//$date = DateTime::createFromFormat('m-d-Y', $enter_date[10]);
			$start_date10 = input_date_format($enter_date[10]);//$date ? $date->format('Y-m-d') : NULL;

			//$date = DateTime::createFromFormat('m-d-Y', $enter_date[11]);
			$start_date11 = input_date_format($enter_date[11]);//$date ? $date->format('Y-m-d') : NULL;

			//$date = DateTime::createFromFormat('m-d-Y', $enter_date[12]);
			$start_date12 = input_date_format($enter_date[12]);//$date ? $date->format('Y-m-d') : NULL;

			//$date = DateTime::createFromFormat('m-d-Y', $enter_date[13]);
			$start_date13 = input_date_format($enter_date[13]);//$date ? $date->format('Y-m-d') : NULL;

			//$date = DateTime::createFromFormat('m-d-Y', $enter_date[14]);
			$start_date14 = input_date_format($enter_date[14]);//$date ? $date->format('Y-m-d') : NULL;

			foreach ($enter_date as $row) {

				$fetch_loan_reserve_date = $this->User_model->query("Select * from loan_reserve_date where talimar_loan = '" . $talimar_loan . "'");
				if ($fetch_loan_reserve_date->num_rows() > 0) {

				

					$this->User_model->updatedata('loan_reserve_date', array('talimar_loan' => $talimar_loan), array('date_1' => $start_date0, 'date_2' => $start_date1, 'date_3' => $start_date2, 'date_4' => $start_date3, 'date_5' => $start_date4, 'date_6' => $start_date5, 'date_7' => $start_date6, 'date_8' => $start_date7, 'date_9' => $start_date8, 'date_10' => $start_date9, 'date_11' => $start_date10, 'date_12' => $start_date11, 'date_13' => $start_date12, 'date_14' => $start_date13, 'date_15' => $start_date14));

					

				} else {

					$loan_reserve_date = "INSERT INTO `loan_reserve_date` ( `talimar_loan`, `loan_id`, `date_1`, `date_2`, `date_3`, `date_4`, `date_5`, `date_6`, `date_7`, `date_8`, `date_9`, `date_10`, `date_11`, `date_12`, `date_13`, `date_14`, `date_15`) VALUES('" . $talimar_loan . "', '" . $loan_id . "', '" . $start_date0 . "', '" . $start_date1 . "', '" . $start_date2 . "', '" . $start_date3 . "', '" . $start_date4 . "', '" . $start_date5 . "',  '" . $start_date6 . "', '" . $start_date7 . "', '" . $start_date8 . "', '" . $start_date9 . "', '" . $start_date10 . "', '" . $start_date11 . "', '" . $start_date12 . "', '" . $start_date13 . "', '" . $start_date14 . "')";

					$this->User_model->query($loan_reserve_date);

				}

			}
		}

		foreach ($draw_description as $key => $row) {

			if ($row != '') {

				$check_update = $this->User_model->query("select * from loan_reserve_draws where talimar_loan = '" . $talimar_loan . "' AND id = '" . $draws[$key] . "'");

				if ($check_update->num_rows() > 0) {
					$sql = "UPDATE loan_reserve_draws SET `draws_description`='" . $row . "', `budget`='" . $this->amount_format($budget[$key]) . "', `date1_amount`='" . $this->amount_format($enter_amount_1[$key]) . "', `date2_amount`='" . $this->amount_format($enter_amount_2[$key]) . "', `date3_amount`='" . $this->amount_format($enter_amount_3[$key]) . "', `date4_amount`='" . $this->amount_format($enter_amount_4[$key]) . "', `date5_amount`='" . $this->amount_format($enter_amount_5[$key]) . "', `date6_amount`='" . $this->amount_format($enter_amount_6[$key]) . "', `date7_amount`='" . $this->amount_format($enter_amount_7[$key]) . "', `date8_amount`='" . $this->amount_format($enter_amount_8[$key]) . "', `date9_amount`='" . $this->amount_format($enter_amount_9[$key]) . "', `date10_amount`='" . $this->amount_format($enter_amount_10[$key]) . "', `date11_amount`='" . $this->amount_format($enter_amount_11[$key]) . "', `date12_amount`='" . $this->amount_format($enter_amount_12[$key]) . "', `date13_amount`='" . $this->amount_format($enter_amount_13[$key]) . "', `date14_amount`='" . $this->amount_format($enter_amount_14[$key]) . "', `date15_amount`='" . $this->amount_format($enter_amount_15[$key]) . "', `lock_loan_reserve` = '".$lock_loan_reserve."' WHERE talimar_loan='" . $talimar_loan . "' AND id='" . $draws[$key] . "'";

					//echo $sql;

				} else {

					$sql = "INSERT INTO `loan_reserve_draws`(`talimar_loan`, `loan_id`, `draws`, `draws_description`, `budget`, `date1_amount`, `date2_amount`, `date3_amount`, `date4_amount`, `date5_amount`, `date6_amount`, `date7_amount`, `date8_amount`, `date9_amount`, `date10_amount`, `date11_amount`, `date12_amount`, `date13_amount`, `date14_amount`, `date15_amount`, `lock_loan_reserve`) VALUES ('" . $talimar_loan . "', '" . $loan_id . "', null, '" . $row . "', '" . $this->amount_format($budget[$key]) . "', '" . $this->amount_format($enter_amount_1[$key]) . "', '" . $this->amount_format($enter_amount_2[$key]) . "', '" . $this->amount_format($enter_amount_3[$key]) . "', '" . $this->amount_format($enter_amount_4[$key]) . "', '" . $this->amount_format($enter_amount_5[$key]) . "', '" . $this->amount_format($enter_amount_6[$key]) . "', '" . $this->amount_format($enter_amount_7[$key]) . "', '" . $this->amount_format($enter_amount_8[$key]) . "', '" . $this->amount_format($enter_amount_9[$key]) . "', '" . $this->amount_format($enter_amount_10[$key]) . "', '" . $this->amount_format($enter_amount_11[$key]) . "', '" . $this->amount_format($enter_amount_12[$key]) . "', '" . $this->amount_format($enter_amount_13[$key]) . "', '" . $this->amount_format($enter_amount_14[$key]) . "', '" . $this->amount_format($enter_amount_15[$key]) . "', '".$lock_loan_reserve."')";

					// echo $sql;
					// die('hhh');
				}
				$this->User_model->query($sql);
			}

		}

		if ($draw_approved || $draw_request || $draw_released || $draw_submit != '') {

			$checkbox_select = $this->User_model->query("select * from loan_reserve_checkbox where talimar_loan = '" . $talimar_loan . "'");

			if ($checkbox_select->num_rows() > 0) {

				$checkbox = "UPDATE `loan_reserve_checkbox` SET `date1_draw_approved`='" . $draw_approved[0] . "', `date2_draw_approved`='" . $draw_approved[1] . "', `date3_draw_approved`='" . $draw_approved[2] . "', `date4_draw_approved`='" . $draw_approved[3] . "', `date5_draw_approved`='" . $draw_approved[4] . "',  `date6_draw_approved`='" . $draw_approved[5] . "', `date7_draw_approved`='" . $draw_approved[6] . "', `date8_draw_approved`='" . $draw_approved[7] . "', `date9_draw_approved`='" . $draw_approved[8] . "', `date10_draw_approved`='" . $draw_approved[9] . "', `date11_draw_approved`='" . $draw_approved[10] . "', `date12_draw_approved`='" . $draw_approved[11] . "', `date13_draw_approved`='" . $draw_approved[12] . "', `date14_draw_approved`='" . $draw_approved[13] . "', `date15_draw_approved`='" . $draw_approved[14] . "', `date1_draw_request`='" . $draw_request[0] . "', `date2_draw_request`='" . $draw_request[1] . "', `date3_draw_request`='" . $draw_request[2] . "', `date4_draw_request`='" . $draw_request[3] . "', `date5_draw_request`='" . $draw_request[4] . "', `date6_draw_request`='" . $draw_request[5] . "', `date7_draw_request`='" . $draw_request[6] . "', `date8_draw_request`='" . $draw_request[7] . "', `date9_draw_request`='" . $draw_request[8] . "', `date10_draw_request`='" . $draw_request[9] . "', `date11_draw_request`='" . $draw_request[10] . "', `date12_draw_request`='" . $draw_request[11] . "', `date13_draw_request`='" . $draw_request[12] . "', `date14_draw_request`='" . $draw_request[13] . "', `date15_draw_request`='" . $draw_request[14] . "', `date1_draw_released`='" . $draw_released[0] . "', `date2_draw_released`='" . $draw_released[1] . "', `date3_draw_released`='" . $draw_released[2] . "', `date4_draw_released`='" . $draw_released[3] . "', `date5_draw_released`='" . $draw_released[4] . "', `date6_draw_released`='" . $draw_released[5] . "', `date7_draw_released`='" . $draw_released[6] . "', `date8_draw_released`='" . $draw_released[7] . "', `date9_draw_released`='" . $draw_released[8] . "', `date10_draw_released`='" . $draw_released[9] . "', `date11_draw_released`='" . $draw_released[10] . "', `date12_draw_released`='" . $draw_released[11] . "', `date13_draw_released`='" . $draw_released[12] . "', `date14_draw_released`='" . $draw_released[13] . "', `date15_draw_released`='" . $draw_released[14] . "', `date1_draw_submit` = '" . $draw_submit[0] . "', `date2_draw_submit` = '" . $draw_submit[1] . "', `date3_draw_submit` = '" . $draw_submit[2] . "', `date4_draw_submit` = '" . $draw_submit[3] . "', `date5_draw_submit` = '" . $draw_submit[4] . "', `date6_draw_submit` = '" . $draw_submit[5] . "', `date7_draw_submit` = '" . $draw_submit[6] . "', `date8_draw_submit` = '" . $draw_submit[7] . "', `date9_draw_submit` = '" . $draw_submit[8] . "', `date10_draw_submit` = '" . $draw_submit[9] . "', `date11_draw_submit` = '" . $draw_submit[10] . "', `date12_draw_submit` = '" . $draw_submit[11] . "', `date13_draw_submit` = '" . $draw_submit[12] . "', `date14_draw_submit` = '" . $draw_submit[13] . "', `date15_draw_submit` = '" . $draw_submit[14] . "', `date1_request_loan` = '" . $draw_request_loan[0] . "', `date2_request_loan` = '" . $draw_request_loan[1] . "', `date3_request_loan` = '" . $draw_request_loan[2] . "', `date4_request_loan` = '" . $draw_request_loan[3] . "', `date5_request_loan` = '" . $draw_request_loan[4] . "', `date6_request_loan` = '" . $draw_request_loan[5] . "', `date7_request_loan` = '" . $draw_request_loan[6] . "', `date8_request_loan` = '" . $draw_request_loan[7] . "', `date9_request_loan` = '" . $draw_request_loan[8] . "', `date10_request_loan` = '" . $draw_request_loan[9] . "', `date11_request_loan` = '" . $draw_request_loan[10] . "', `date12_request_loan` = '" . $draw_request_loan[11] . "', `date13_request_loan` = '" . $draw_request_loan[12] . "', `date14_request_loan` = '" . $draw_request_loan[13] . "', `date15_request_loan` = '" . $draw_request_loan[14] . "' WHERE talimar_loan = '" . $talimar_loan . "'";

			} else {

				$checkbox = "INSERT INTO `loan_reserve_checkbox`(`talimar_loan`, `loan_id`, `date1_draw_approved`, `date2_draw_approved`, `date3_draw_approved`, `date4_draw_approved`, `date5_draw_approved`, `date6_draw_approved`, `date7_draw_approved`, `date8_draw_approved`, `date9_draw_approved`, `date10_draw_approved`, `date11_draw_approved`, `date12_draw_approved`, `date13_draw_approved`, `date14_draw_approved`, `date15_draw_approved`, `date1_draw_request`, `date2_draw_request`, `date3_draw_request`, `date4_draw_request`, `date5_draw_request`, `date6_draw_request`, `date7_draw_request`, `date8_draw_request`, `date9_draw_request`, `date10_draw_request`, `date11_draw_request`, `date12_draw_request`, `date13_draw_request`, `date14_draw_request`, `date15_draw_request`, `date1_draw_released`, `date2_draw_released`, `date3_draw_released`, `date4_draw_released`, `date5_draw_released`, `date6_draw_released`, `date7_draw_released`, `date8_draw_released`, `date9_draw_released`, `date10_draw_released`, `date11_draw_released`, `date12_draw_released`, `date13_draw_released`, `date14_draw_released`, `date15_draw_released`, `date1_draw_submit`, `date2_draw_submit`, `date3_draw_submit`, `date4_draw_submit`, `date5_draw_submit`, `date6_draw_submit`, `date7_draw_submit`, `date8_draw_submit`, `date9_draw_submit`, `date10_draw_submit`, `date11_draw_submit`, `date12_draw_submit`, `date13_draw_submit`, `date14_draw_submit`, `date15_draw_submit`, `date1_request_loan`, `date2_request_loan`, `date3_request_loan`, `date4_request_loan`, `date5_request_loan`, `date6_request_loan`, `date7_request_loan`, `date8_request_loan`, `date9_request_loan`, `date10_request_loan`, `date11_request_loan`, `date12_request_loan`, `date13_request_loan`, `date14_request_loan`, `date15_request_loan`) VALUES ('" . $talimar_loan . "', '" . $loan_id . "', '" . $draw_approved[0] . "', '" . $draw_approved[1] . "', '" . $draw_approved[2] . "', '" . $draw_approved[3] . "', '" . $draw_approved[4] . "', '" . $draw_approved[5] . "', '" . $draw_approved[6] . "', '" . $draw_approved[7] . "', '" . $draw_approved[8] . "', '" . $draw_approved[9] . "', '" . $draw_approved[10] . "', '" . $draw_approved[11] . "', '" . $draw_approved[12] . "', '" . $draw_approved[13] . "', '" . $draw_approved[14] . "', '" . $draw_request[0] . "', '" . $draw_request[1] . "', '" . $draw_request[2] . "', '" . $draw_request[3] . "', '" . $draw_request[4] . "', '" . $draw_request[5] . "', '" . $draw_request[6] . "', '" . $draw_request[7] . "', '" . $draw_request[8] . "', '" . $draw_request[9] . "', '" . $draw_request[10] . "', '" . $draw_request[11] . "', '" . $draw_request[12] . "', '" . $draw_request[13] . "', '" . $draw_request[14] . "', '" . $draw_released[0] . "', '" . $draw_released[1] . "', '" . $draw_released[2] . "', '" . $draw_released[3] . "', '" . $draw_released[4] . "', '" . $draw_released[5] . "', '" . $draw_released[6] . "', '" . $draw_released[7] . "', '" . $draw_released[8] . "', '" . $draw_released[9] . "', '" . $draw_released[10] . "', '" . $draw_released[11] . "', '" . $draw_released[12] . "', '" . $draw_released[13] . "', '" . $draw_released[14] . "', '" . $draw_submit[0] . "', '" . $draw_submit[1] . "', '" . $draw_submit[2] . "', '" . $draw_submit[3] . "', '" . $draw_submit[4] . "', '" . $draw_submit[5] . "', '" . $draw_submit[6] . "', '" . $draw_submit[7] . "', '" . $draw_submit[8] . "', '" . $draw_submit[9] . "', '" . $draw_submit[10] . "', '" . $draw_submit[11] . "', '" . $draw_submit[12] . "', '" . $draw_submit[13] . "', '" . $draw_submit[14] . "', '" . $draw_request_loan[0] . "', '" . $draw_request_loan[1] . "', '" . $draw_request_loan[2] . "', '" . $draw_request_loan[3] . "', '" . $draw_request_loan[4] . "', '" . $draw_request_loan[5] . "', '" . $draw_request_loan[6] . "', '" . $draw_request_loan[7] . "', '" . $draw_request_loan[8] . "', '" . $draw_request_loan[9] . "', '" . $draw_request_loan[10] . "', '" . $draw_request_loan[11] . "', '" . $draw_request_loan[12] . "', '" . $draw_request_loan[13] . "', '" . $draw_request_loan[14] . "')";

			}

			$this->User_model->query($checkbox);
		}
		
		if ($fund_servicer || $fund_account || $draw_cost_per != '') {

			$data['loan_id'] = $loan_id;
			$data['fund_servicer'] = $fund_servicer;
			$data['fund_account'] = $fund_account;
			$data['draw_cost_per'] = $draw_cost_per;

			$select_extra_field = $this->User_model->query("Select * from loan_reserve_extra_field where talimar_loan = '" . $talimar_loan . "'");
			if ($select_extra_field->num_rows() > 0) {

				$where['talimar_loan'] = $talimar_loan;
				$this->User_model->updatedata('loan_reserve_extra_field', $where, $data);

			} else {

				$data['talimar_loan'] = $talimar_loan;
				$this->User_model->insertdata('loan_reserve_extra_field', $data);

			}
		}
		$this->session->set_flashdata('popup_modal_cv', array('test'));
		$this->session->set_flashdata('popup_modal_hit', 'testmodal');
		$this->session->set_flashdata('success', ' Loan Reserve Draws Updated Successfully! ');
		redirect(base_url() . 'load_data/' . $loan_id.'#test');

	}

	public function draws_form() {
		error_reporting(0);
		$loan_id = $this->input->post('loan_id');
		$talimar_loan = $this->input->post('talimar_loan');
		$draws = $this->input->post('draws');
		$draws_amount = $this->input->post('draws_amount');
		$draws_require = $this->input->post('draws_require');
		$draws_release_amount = $this->input->post('draws_release_amount');
		$draws_remaining = $this->input->post('draws_remaining');
		$draws_release = $this->input->post('draws_release');
		$draws_notes = $this->input->post('draws_notes');
		$draws_date = $this->input->post('draws_date');
		$funds_disbursed = $this->input->post('funds_disbursed');
		$request_submitted = $this->input->post('request_submitted');
		$t_user_id = $this->session->userdata('t_user_id');
		$draw_escrow_account = $this->input->post('draw_escrow_account');
		$draw_escrow_other = $this->input->post('draw_escrow_other');
		$draw_cost_per = $this->amount_format($this->input->post('draw_cost_per'));

		$f_delete['talimar_loan'] = $talimar_loan;
		$data_fetch_all = $this->User_model->select_where('loan_draws', $f_delete);
		$data_fetch_all = $data_fetch_all->result();

		$avilable = array();
		$come_array = array();
		foreach ($data_fetch_all as $row) {

			$avilable[] = $row->draws;
		}

		foreach ($draws as $row) {
			$come_array[] = $row;
		}

		$difference = array_diff($avilable, $come_array);
		foreach ($difference as $row) {
			$delete_data['draws'] = $row;
			$delete_data['talimar_loan'] = $talimar_loan;

			$this->User_model->delete('loan_draws', $delete_data);
		}
		//----------------------------------------
		$count = 1;
		foreach ($draws as $key => $row) {
			if ($row != '') {
				$check_available['talimar_loan'] = $talimar_loan;
				$check_available['draws'] = $row;
				$check_update = $this->User_model->select_where('loan_draws', $check_available);
				if ($check_update->num_rows() > 0) {

					$sql = "UPDATE loan_draws SET draws_amount = '" . $this->amount_format($draws_amount[$key]) . "' , draws_release_amount = '" . $this->amount_format($draws_release_amount[$key]) . "' ,  draws_remaining = '" . $this->amount_format($draws_remaining[$key]) . "' , draws_require = '" . $draws_require[$key] . "' , draws_notes = '" . $draws_notes[$key] . "' , draws_release = '" . $draws_release[$key] . "' , funds_disbursed = '" . $funds_disbursed[$key] . "' , request_submitted = '" . $request_submitted[$key] . "' , draws_date = '" . $draws_date[$key] . "' , position = '" . $count . "' ,t_user_id = '" . $t_user_id . "' WHERE talimar_loan = '" . $talimar_loan . "' AND draws = '" . $row . "' ";

				} else {

					$sql = "INSERT INTO loan_draws ( talimar_loan , draws , draws_amount , draws_release_amount , draws_remaining, draws_require , draws_notes , draws_release , funds_disbursed , request_submitted , draws_date , position , t_user_id ) VALUES ('" . $talimar_loan . "' , '" . $row . "' , '" . $this->amount_format($draws_amount[$key]) . "', '" . $this->amount_format($draws_release_amount[$key]) . "' , '" . $this->amount_format($draws_remaining[$key]) . "' , '" . $draws_require[$key] . "' , '" . $draws_notes[$key] . "' , '" . $draws_release[$key] . "' , '" . $funds_disbursed[$key] . "' , '" . $request_submitted[$key] . "' , '" . $draws_date[$key] . "' , '" . $count . "' , '" . $t_user_id . "')";

				}

				// echo '<br>'.$sql;
				// die('stop');

				$this->User_model->query($sql);
				$loan_talimar['talimar_loan'] = $talimar_loan;
				$loan_draw['draws'] = '1';
				$this->User_model->updatedata('loan', $loan_talimar, $loan_draw);
			}
			$count++;
		}
		$insert_id = $this->db->insert_id();

		$sql_fund_released = "SELECT SUM( draws_amount ) AS total FROM loan_draws WHERE talimar_loan =  '" . $talimar_loan . "' AND draws_release =  '1' ";

		$fetch_fund_released = $this->User_model->query($sql_fund_released);
		$fetch_fund_released = $fetch_fund_released->result();

		$fund_released = $fetch_fund_released[0]->total;

		$sql_fund_avaible = "SELECT SUM( draws_amount ) AS total FROM loan_draws WHERE talimar_loan =  '" . $talimar_loan . "' AND draws_release =  '0' OR draws_release = '' ";

		$fetch_avaible_released = $this->User_model->query($sql_fund_avaible);
		$fetch_avaible_released = $fetch_avaible_released->result();

		$fund_available = $fetch_avaible_released[0]->total;

		$where_11['talimar_loan'] = $talimar_loan;

		$fetch_extra_details = $this->User_model->select_where('extra_details', $where_11);
		// $fetch_extra_details = $fetch_extra_details->result();
		if ($fetch_extra_details->num_rows() > 0) {
			$where_update_extra['talimar_loan'] = $talimar_loan;
			$update_extra['draw_fund_released'] = $fund_released;
			$update_extra['draw_fund_avilable'] = $fund_available;
			$update_extra['draw_escrow_account'] = $draw_escrow_account;
			$update_extra['draw_escrow_other'] = $draw_escrow_other;
			$update_extra['draw_cost_per'] = $draw_cost_per;

			$this->User_model->updatedata('extra_details', $where_update_extra, $update_extra);
		} else {
			$insert_data['talimar_loan'] = $talimar_loan;
			$insert_data['draw_fund_released'] = $fund_released;
			$insert_data['draw_fund_avilable'] = $fund_available;
			$insert_data['draw_escrow_account'] = $draw_escrow_account;
			$insert_data['draw_escrow_other'] = $draw_escrow_other;
			$insert_data['draw_cost_per'] = $draw_cost_per;

			$this->User_model->insertdata('extra_details', $insert_data);
		}

		if ($insert_id) {
			$this->session->set_flashdata('success', ' Loan Draws records added successfully ');
			redirect(base_url() . 'load_data/' . $loan_id, 'refresh');
		} else {
			$this->session->set_flashdata('success', ' Loan Draws Updated Successfully! ');
			redirect(base_url() . 'load_data/' . $loan_id, 'refresh');
		}
	}

	public function estimate_lender_cost() {
		$loan_id = $this->input->post('loan_id');
		$talimar_loan = $this->input->post('talimar_no');
		$items = $this->input->post('items');
		$amount = $this->input->post('amount');
		$t_user_id = $this->session->userdata('t_user_id');

		$f_delete['talimar_loan'] = $talimar_loan;
		$data_fetch_all = $this->User_model->select_where('servicing_estimate_lender', $f_delete);
		$data_fetch_all = $data_fetch_all->result();
		$avilable = array();
		$come_array = array();
		foreach ($data_fetch_all as $row) {
			$avilable[] = $row->items;
		}

		foreach ($items as $row) {
			$come_array[] = $row;
		}

		$difference = array_diff($avilable, $come_array);
		foreach ($difference as $row) {
			$delete_data['items'] = $row;
			$delete_data['talimar_loan'] = $talimar_loan;

			$this->User_model->delete('servicing_estimate_lender', $delete_data);
		}
		//----------------------------------------
		$count = 1;
		foreach ($items as $key => $row) {
			if ($row != '') {

				$check_available['talimar_loan'] = $talimar_loan;
				$check_available['items'] = $row;
				$check_update = $this->User_model->select_where('servicing_estimate_lender', $check_available);
				if ($check_update->num_rows() > 0) {
					$sql = "UPDATE servicing_estimate_lender SET amount = '" . $this->amount_format($amount[$key]) . "' , position = '" . $count . "'  WHERE talimar_loan = '" . $talimar_loan . "' AND items = '" . $row . "' ";
				} else {
					$sql = "INSERT INTO servicing_estimate_lender ( talimar_loan , items , amount , position , t_user_id ) VALUES ('" . $talimar_loan . "' , '" . $row . "' , '" . $this->amount_format($amount[$key]) . "' , '" . $count . "' , '" . $t_user_id . "')";
				}

				$this->User_model->query($sql);

			}
			$count++;
		}
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			$this->session->set_flashdata('success', ' Records added successfully ');
			redirect(base_url() . 'load_data' . $loan_id . '#loan_servicing', 'refresh');
		} else {
			$this->session->set_flashdata('success', ' Updated ');
			redirect(base_url() . 'load_data' . $loan_id . '#loan_servicing', 'refresh');
		}
	}

	public function form_ach_deposite() {
		if ($this->input->post('talimar_no')) {
			$loan_id = $this->input->post('loan_id');
			$data['account_name'] = $this->input->post('account_name');
			$data['bank_name'] = $this->input->post('bank_name');
			$data['account_number'] = $this->input->post('account_number');
			$data['routing_number'] = $this->input->post('routing_number');
			$data['account_type'] = $this->input->post('account_type');

			$where['talimar_loan'] = $this->input->post('talimar_no');

			$check_numrows = $this->User_model->select_where('ach_deposite', $where);

			if ($check_numrows->num_rows() > 0) {
				$this->User_model->updatedata('ach_deposite', $where, $data);
				$this->session->set_flashdata('success', 'ACH deposite Updated Successfully!');
				redirect(base_url() . 'load_data' . $loan_id, 'refresh');
			} else {
				$data['talimar_loan'] = $this->input->post('talimar_no');
				$this->User_model->insertdata('ach_deposite', $data);
				$this->session->set_flashdata('success', 'ACH deposite records add successfully');
				redirect(base_url() . 'load_data' . $loan_id, 'refresh');
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong');
			redirect(base_url() . 'load_data' . $loan_id, 'refresh');
		}
	}

	public function lender_email() {
		$loan_id = $this->input->post('loan_id');
		$contact_id = $this->input->post('contact_id');
		foreach ($contact_id as $row) {
			$subject = "TaliMar Finacial";
			$message = 'Talimar Loan : ' . $this->input->post('talimar_no') . '<br>';
			$message .= nl2br($this->input->post('message'));
			$where['contact_id'] = $row;
			$fetch_contact_data = $this->User_model->select_where('contact', $where);
			$fetch_contact_data = $fetch_contact_data->result();
			$mailto = $fetch_contact_data[0]->contact_email;
			// $headers  = "From: TaliMar Financial < mail@database.talimarfinancial.com >\n";
			// $headers .= "Cc: TaliMar Financial < mail@database.talimarfinancial.com >\n";
			// $headers .= "X-Sender: TaliMar Financial < mail@database.talimarfinancial.com >\n";
			// $headers .= "X-Priority: 1\n"; // Urgent message!
			// $headers .= "Return-Path: mail@database.talimarfinancial.com\n";
			// $headers .= "MIME-Version: 1.0\r\n";
			// $headers .= "Content-Type: text/html; charset=iso-8859-1\n";
			// mail($mailto, $subject, $message, $headers);

			/*$this->load->library('email');

			$this->email->initialize(SMTP_SENDGRID);
			// prepare email
			$this->email
				->from('mail@talimarfinancial.com', $subject)
				->to($mailto)
			//->bcc($mailto)
				->subject($subject)
				->message($message)
				->set_mailtype('html');

			// send email
			$this->email->send();*/

			$dataMailA = array();
			$dataMailA['from_email'] 	= 'mail@talimarfinancial.com';
			$dataMailA['from_name'] 	= 'TaliMar Financial';
			$dataMailA['to'] 			= $mailto;
			$dataMailA['cc'] 			= '';
			$dataMailA['name'] 			= '';
			$dataMailA['subject'] 		= $subject;
			$dataMailA['content_body'] 	= $message;
			$dataMailA['button_url'] 	= '';
			$dataMailA['button_text'] 	= '';
			send_mail_template($dataMailA);
		}
		$this->session->set_flashdata('popup_modal_cv', array('assigment_main', 'assigment', 'email_to_lenders'));
		$this->session->set_flashdata('success', ' Email Sent');
		redirect(base_url() . 'load_data' . $loan_id, 'refresh');
	}

	public function form_loan_distibution() {
		$button = $this->input->post('button');
		$items = $this->input->post('items');
		$talimar_loan = $this->input->post('talimar_no');
		$description = $this->input->post('description');
		$loan_id = $this->input->post('loan_id');

		$t_user_id = $this->session->userdata('t_user_id');
		$disbursement = $this->input->post('disbursement');
		$data['talimar_loan'] = $this->input->post('talimar_no');

		//      ----first delete all with same loan
		$data_fetch_all = $this->User_model->select_where('loan_distribution', $data);
		$data_fetch_all = $data_fetch_all->result();
		$avilable = array();
		$come_array = array();
		foreach ($data_fetch_all as $row) {
			$avilable[] = $row->items;
		}
		foreach ($items as $row) {
			$come_array[] = $row;
		}
		$difference = array_diff($avilable, $come_array);

		foreach ($difference as $row) {
			$delete_data['items'] = $row;
			$delete_data['talimar_loan'] = $this->input->post('talimar_no');

			$this->User_model->delete('loan_distribution', $delete_data);
		}

		foreach ($items as $key => $row) {

			if ($items[$key] != '') {
				//-----------------------------
				$fetch = "SELECT * FROM loan_distribution WHERE talimar_loan = '" . $talimar_loan . "' AND items = '" . $items[$key] . "'";
				$fetch_result = $this->User_model->query($fetch);
				if ($fetch_result->num_rows() > 0) {
					$update = "UPDATE loan_distribution SET disbursement = '" . $this->amount_format($disbursement[$key]) . "', description = '" . $description[$key] . "' WHERE talimar_loan = '" . $talimar_loan . "' AND items = '" . $items[$key] . "' ";

					$this->User_model->query($update);
				} else {
					$sql = "INSERT INTO loan_distribution (talimar_loan , items , disbursement ,description , t_user_id) VALUES ( '" . $talimar_loan . "' , '" . $items[$key] . "',  '" . $this->amount_format($disbursement[$key]) . "' , '" . $description[$key] . "', '" . $t_user_id . "')";

					$this->User_model->query($sql);
				}
				//------------------------
			}

		}
		$insert_id = $this->db->insert_id();

		// $where_1['talimar_loan'] = $talimar_loan;

		// $fetch_extra_detail = $this->User_model->select_where('extra_details',$where_1);
		// if($fetch_extra_detail->num_rows() > 0)
		// {
		// $update_extra_detail['escrow_impound_account'] = $this->input->post('escrow_impound_account');
		// $this->User_model->updatedata('extra_details',$where_1,$update_extra_detail);
		// }
		// else
		// {
		// $insert_extra['talimar_loan'] 			= $talimar_loan;
		// $insert_extra['escrow_impound_account'] = $this->input->post('escrow_impound_account');
		// $this->User_model->insertdata('extra_details',$insert_extra);
		// }

		if ($insert_id) {
			$this->session->set_flashdata('success', 'Loan Distribution records add successfully');
			redirect(base_url() . "load_data" . $loan_id, 'refresh');
		} else {
			$this->session->set_flashdata('success', 'Loan Distribution Updated Successfully!');
			redirect(base_url() . "load_data" . $loan_id, 'refresh');
		}
	}

	public function form_comparable_sale() {

		$property_address = $this->input->post('property_address');
		$sq_ft = $this->input->post('sq_feet');
		$bedrooms = $this->input->post('bedrooms');
		$bathroom = $this->input->post('bathrooms');
		$lot_size = $this->input->post('lot_size');
		$year_built = $this->input->post('year_built');
		$sale_price = $this->input->post('sale_price');
		$per_sq_ft = $this->input->post('per_sq_feet');
		$comp_sale_per_sq_ft = $this->input->post('comp_sale_per_sq_ft');
		$date_sold = $this->input->post('date_sold');

		$talimar_loan = $this->input->post('talimar_no');
		$loan_id = str_replace('/', '', $this->input->post('loan_id'));
		$property_home_id = $this->input->post('property_home_id');

		$t_user_id = $this->session->userdata('t_user_id');
		$comparable_id = $this->input->post('comparable_id');
		$property_value_modification = $this->amount_format($this->input->post('property_value_modification'));
		$data['talimar_loan'] = $this->input->post('talimar_no');

		if($property_address){
			foreach ($property_address as $key => $row) {

				if ($comparable_id[$key] != 'new') {
					$update = "UPDATE `comparable_sale` SET `talimar_loan`='" . $talimar_loan . "', `property_address`='" . $property_address[$key] . "', `sq_ft`='" . $this->amount_format($sq_ft[$key]) . "', `bedrooms`= '" . $bedrooms[$key] . "', `bathroom`= '" . $bathroom[$key] . "', `lot_size`= '" . $lot_size[$key] . "', `year_built`= '" . $year_built[$key] . "', `sale_price`= '" . $this->amount_format($sale_price[$key]) . "', `per_sq_ft`= '" . $this->amount_format($per_sq_ft[$key]) . "', `t_user_id`= '" . $t_user_id . "' , `date_sold`= '" . $date_sold[$key] . "' WHERE id = '" . $comparable_id[$key] . "' ";
					$this->User_model->query($update);
				} else {
					if ($property_address[$key] != '') {
						$sql = "INSERT INTO `comparable_sale`( `talimar_loan`,`property_home_id`,`loan_id`, `property_address`, `date_sold`, `sq_ft`, `bedrooms`, `bathroom`, `lot_size`, `year_built` , `sale_price`, `per_sq_ft`, `t_user_id`) VALUES ('" . $talimar_loan . "', '" . $property_home_id . "', '" . $loan_id . "', '" . $property_address[$key] . "', '" . $date_sold[$key] . "',  '" . $this->amount_format($sq_ft[$key]) . "',  '" . $bedrooms[$key] . "',  '" . $bathroom[$key] . "' , '" . $lot_size[$key] . "' , '" . $year_built[$key] . "' ,  '" . $this->amount_format($sale_price[$key]) . "' ,  '" . $this->amount_format($per_sq_ft[$key]) . "' , '" . $t_user_id . "' )";
						$this->User_model->query($sql);
					}
				}

			}
		}
		
		$property_value_modification = $this->amount_format($this->input->post('property_value_modification'));
		// Update property_value_modification
		$fetch_propoerty_input_fields = $this->User_model->select_where('propoerty_input_fields', array('talimar_loan' => $talimar_loan, 'property_home_id' => $property_home_id, 'input_field' => 'property_value_modification'));
		if ($fetch_propoerty_input_fields->num_rows() > 0) {
			$this->User_model->updatedata('propoerty_input_fields', array('talimar_loan' => $talimar_loan, 'property_home_id' => $property_home_id, 'input_field' => 'property_value_modification'), array('input_value' => $property_value_modification));
		} else {
			$this->User_model->insertdata('propoerty_input_fields', array('talimar_loan' => $talimar_loan, 'loan_id' => $loan_id, 'property_home_id' => $property_home_id, 'input_field' => 'property_value_modification', 'input_value' => $property_value_modification));
		}

		$comp_sale_val_notes = $this->input->post('comp_sale_val_notes');
		$property_valuation_notess = $this->input->post('property_valuation_notes');

		// Update comp_sale_val_notes
		$fetch_propoerty_input_fields = $this->User_model->select_where('propoerty_input_fields', array('talimar_loan' => $talimar_loan, 'property_home_id' => $property_home_id, 'input_field' => 'comp_sale_val_notes'));
		if ($fetch_propoerty_input_fields->num_rows() > 0) {
			$this->User_model->updatedata('propoerty_input_fields', array('talimar_loan' => $talimar_loan, 'property_home_id' => $property_home_id, 'input_field' => 'comp_sale_val_notes'), array('input_value' => $comp_sale_val_notes, 'property_valuation_notes' => $property_valuation_notess));
		} else {
			$this->User_model->insertdata('propoerty_input_fields', array('talimar_loan' => $talimar_loan, 'loan_id' => $loan_id, 'property_home_id' => $property_home_id, 'input_field' => 'comp_sale_val_notes', 'input_value' => $comp_sale_val_notes, 'property_valuation_notes' => $property_valuation_notess));
		}

		$this->session->set_flashdata('success', 'Comparable Sales Updated Successfully!');
		// redirect(base_url()."load_data/".$loan_id.'#property-'.$property_home_id);
		$this->session->set_flashdata('popup_modal_hit', 'comparable_sales');
		$this->session->set_flashdata('property_home_id', $property_home_id);

		redirect(base_url() . 'load_data/' . $loan_id);
	}

	public function form_property_images() {
		$loan_id = $this->input->post('loan_id');
		$t_user_id = $this->session->userdata('t_user_id');
		$discription = $this->input->post('discription');
		$talimar = $this->input->post('talimar_loan');
		$image_id = $this->input->post('image_id');
		$marketing = $this->input->post('marketing');

		$folder = 'property_images/' . $talimar . '/'; //FCPATH

		/*if (!is_dir($folder)) {
			mkdir($folder, 0777, true);
		}*/
		// Update Privious Images discription
		foreach ($image_id as $key => $row) {
			$up_date['discription'] = $discription[$key];
			$where1['id'] = $row;
			$this->User_model->updatedata('property_images', $where1, $up_date);
		}

		$total_image = $_FILES['files'];
		$t_user_id = $this->session->userdata('t_user_id');
		foreach ($total_image['name'] as $key => $row_data) {
			if ($row_data) {
				$ext = strtolower(pathinfo($row_data, PATHINFO_EXTENSION));
				if ($ext == 'jpeg' || $ext == 'jpg' || $ext == 'png') {
					$new_name = $talimar . "-" . rand(0, 9999) . "." . $ext;
					$target_dir = $folder . $new_name;

					$property_images['talimar_loan'] = $talimar;
					$property_images['discription'] = $discription[$key];
					$property_images['marketing'] = $marketing[$key];
					$property_images['image_name'] = $new_name;
					$property_images['title'] = 'under_writing';
					$property_images['t_user_id'] = $t_user_id;
					$this->User_model->insertdata('property_images', $property_images);

					//if (move_uploaded_file($_FILES["files"]["tmp_name"][$key], $target_dir) === TRUE) {
					$attachmentName=$this->aws3->uploadFile($target_dir,$_FILES["files"]["tmp_name"][$key]);
					if($attachmentName){
					}
				} else {
					$this->session->set_flashdata('error', 'Please upload only jpeg, jpg, png format images');
					redirect(base_url() . 'load_data/' . $loan_id, 'refresh');
				}
			}
		}
		$this->session->set_flashdata('success', 'Property Images Updated Successfully!');
		redirect(base_url() . 'load_data' . $loan_id, 'refresh');

	}

	public function form_property_user_images() {
		error_reporting(0);
		$loan_id = $this->input->post('loan_id');
		$t_user_id = $this->session->userdata('t_user_id');
		$discription = $this->input->post('discription');
		$talimar = $this->input->post('talimar_loan');
		$image_id = $this->input->post('image_id');
		$marketing = $this->input->post('marketing');

		$folder = 'property_images/' . $talimar . '/'; //FCPATH
		/*if (!is_dir($folder)) {
			mkdir($folder, 0777, true);
		}*/
		// Update Privious Images discription
		foreach ($image_id as $key => $row) {
			$up_date['discription'] = $discription[$key];
			$where1['id'] = $row;
			$this->User_model->updatedata('property_images', $where1, $up_date);
		}

		$total_image = $_FILES['files'];
		$t_user_id = $this->session->userdata('t_user_id');

		foreach ($total_image['name'] as $key => $row_data) {
			if ($row_data) {
				$ext = strtolower(pathinfo($row_data, PATHINFO_EXTENSION));
				if ($ext == 'jpeg' || $ext == 'jpg' || $ext == 'png') {
					$new_name = $talimar . "-" . rand(0, 9999) . "." . $ext;
					$target_dir = $folder . $new_name;

					$property_images['talimar_loan'] = $talimar;
					$property_images['discription'] = $discription[$key];
					$property_images['marketing'] = $marketing[$key];
					$property_images['image_name'] = $new_name;
					$property_images['title'] = 'user';
					$property_images['t_user_id'] = $t_user_id;
					$this->User_model->insertdata('property_images', $property_images);

					//if (move_uploaded_file($_FILES["files"]["tmp_name"][$key], $target_dir) === TRUE) {
					$attachmentName=$this->aws3->uploadFile($target_dir,$_FILES["files"]["tmp_name"][$key]);
					if($attachmentName){
					}
				}
			}
		}
		$this->session->set_flashdata('success', 'Property Images Updated Successfully!');
		redirect(base_url() . 'load_data' . $loan_id, 'refresh');

	}

	public function form_loan_documents() {
		// echo "<pre>";
		// print_r($_POST);
		// echo "</pre>";
		$loan_id = $this->input->post('loan_id');
		$t_user_id = $this->session->userdata('t_user_id');
		$discription = $this->input->post('description');
		$talimar = $this->input->post('talimar_loan');
		$doc_id = $this->input->post('doc_id') ? $this->input->post('doc_id') : '';
		$doc_type = $this->input->post('doc_type');

		$doc_id = $this->input->post('doc_id') ? array_values(array_filter($this->input->post('doc_id'))) : '';

		if ($doc_id) {

			foreach ($doc_id as $id) {

				$fetch_loan_doc = $this->User_model->query("SELECT * FROM loan_document_docs WHERE id = '" . $id . "' ");
				if ($fetch_loan_doc->num_rows() > 0) {
					$fetch_loan_document = $fetch_loan_doc->result();

					$filename = $fetch_loan_document[0]->doc_name;
					$path = 'loan_documents/' . $filename;

					$this->aws3->deleteObject($path);
					/*if (file_exists($path)) {
						unlink($path);
					}*/
					// $this->User_model->delete('loan_document_docs',$id);
					$this->User_model->query("DELETE  FROM loan_document_docs WHERE id = '" . $id . "'");

				} else {
					$this->session->set_flashdata('error', ' Something Went wrong');
					redirect(base_url() . 'load_data/' . $loan_id, 'refresh');
				}
			}

			$this->session->set_flashdata('popup_modal_hit', 'loan_document');
			$this->session->set_flashdata('success', ' Deleted');
			redirect(base_url() . 'load_data' . $loan_id, 'refresh');

		}
		$folder = 'loan_documents/' . $talimar . '/'; //FCPATH
		/*if (!is_dir($folder)) {
			mkdir($folder, 0777, true);
		}*/
		// Update Privious Images discription
		if (isset($doc_id)) {
			foreach ($doc_id as $key => $row) {
				$up_date['discription'] = $discription[$key];
				$where1['id'] = $row;
				$this->User_model->updatedata('loan_document_docs', $where1, $up_date);
			}
		}
		$total_files = $_FILES['files'];
		$t_user_id = $this->session->userdata('t_user_id');

		foreach ($total_files['name'] as $key => $row_data) {
			if ($row_data) {
				$ext = strtolower(pathinfo($row_data, PATHINFO_EXTENSION));
				// echo $ext;
				// die('stop');
				if ($ext = 'doc' || $ext = 'docx' || $ext = 'pdf') {
					if ($doc_type[$key] == 1 || $doc_type[$key] == 2 || $doc_type[$key] == 3 || $doc_type[$key] == 4 || $doc_type[$key] == 5 || $doc_type[$key] == 6 || $doc_type[$key] == 7) {
						$delete['talimar_loan'] = $talimar;
						$delete['doc_type'] = $doc_type[$key];
						$this->User_model->delete('loan_document_docs', $delete);
					}

					$new_name = $talimar . "-" . rand(0, 999999) . "-" . $row_data;
					$target_dir = $folder . $new_name;

					$documents['talimar_loan'] = $talimar;
					$documents['discription'] = $discription[$key];
					$documents['doc_type'] = $doc_type[$key];
					$documents['doc_name'] = $new_name;
					$documents['modified_date'] = date('Y-m-d');
					$documents['t_user_id'] = $t_user_id;
					$this->User_model->insertdata('loan_document_docs', $documents);

					//if (move_uploaded_file($_FILES["files"]["tmp_name"][$key], $target_dir) === TRUE) {
					$attachmentName=$this->aws3->uploadFile($target_dir,$_FILES["files"]["tmp_name"][$key]);
					if($attachmentName){
					}
				} else {
					$this->session->set_flashdata('error', 'Please upload only doc/docx file');
					redirect(base_url() . 'load_data/' . $loan_id, 'refresh');
				}
			}
		}

		$this->session->set_flashdata('success', 'Loan Documents Updated Successfully!');
		$loan_id = str_replace("/", "", $this->input->post('loan_id'));
		redirect(base_url() . 'load_data/' . $loan_id, 'refresh');

	}

	public function delete_loan_document() {

		$loan_id = $this->uri->segment(3);
		$id['id'] = $this->uri->segment(2);

		$fetch_loan_doc = $this->User_model->select_where('loan_document_docs', $id);
		if ($fetch_loan_doc->num_rows() > 0) {
			$fetch_loan_document = $fetch_loan_doc->result();

			$filename = $fetch_loan_document[0]->doc_name;
			$path = 'loan_documents/' . $filename; //FCPATH

			$this->aws3->deleteObject($path);
			/*if (file_exists($path)) {
				unlink($path);
			}*/
			$this->User_model->delete('loan_document_docs', $id);
			$this->session->set_flashdata('success', ' Deleted');
			redirect(base_url() . 'load_data/' . $loan_id, 'refresh');
		} else {
			$this->session->set_flashdata('error', ' Something Went wrong');
			redirect(base_url() . 'load_data/' . $loan_id, 'refresh');
		}

	}

	public function form_create_album() {
		$t_user_id = $this->session->userdata('t_user_id');
		$loan_id = $this->input->post('loan_id');
		$talimar_loan = $this->input->post('talimar_loan');
		$album_name = $this->input->post('album_name');

		$where['talimar_loan'] = $talimar_loan;
		$where['album_name'] = $album_name;
		$check_album_name = $this->User_model->select_where('property_album', $where);
		if ($check_album_name->num_rows() > 0) {
			$this->session->set_flashdata('error', 'Album Name already exist');
			redirect(base_url() . 'load_data/' . $loan_id);
		} else {
			$insert['talimar_loan'] = $talimar_loan;
			$insert['album_name'] = $album_name;
			$insert['t_user_id'] = $t_user_id;
			$this->User_model->insertdata('property_album', $insert);

			if ($this->db->insert_id()) {
				$this->session->set_flashdata('success', 'New Album created successfully');
				redirect(base_url() . 'load_data' . $loan_id);
			}
		}

	}

	public function form_album_images() {
		$loan_id = $this->input->post('loan_id');
		$t_user_id = $this->session->userdata('t_user_id');
		$discription = $this->input->post('discription');
		$talimar = $this->input->post('talimar_loan');
		$image_id = $this->input->post('image_id');
		$title = $this->input->post('title');

		$folder = 'property_images/' . $talimar . '/'; //FCPATH

		/*if (!is_dir($folder)) {
			mkdir($folder, 0777, true);
		}*/

		// Update Privious Images discription
		if (isset($image_id)) {
			foreach ($image_id as $key => $row) {
				$up_date['discription'] = $discription[$key];
				$where1['id'] = $row;
				$this->User_model->updatedata('property_images', $where1, $up_date);
			}
		}
		$total_image = $_FILES['files'];

		// echo '<pre>';
		// print_r($total_image);
		// echo '</pre>';
		// die('stop');

		$t_user_id = $this->session->userdata('t_user_id');
		foreach ($total_image['name'] as $key => $row_data) {
			if ($row_data) {
				$ext = strtolower(pathinfo($row_data, PATHINFO_EXTENSION));
				// echo $total_image['error'][$key];
				// echo $ext;
				// die('stop');
				if ($total_image['error'][$key] == 1) {
					$this->session->set_flashdata('error', ' Error found in image ');
					redirect(base_url() . 'load_data/' . $loan_id, 'refresh');
				}
				if ($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg') {
					$new_name = $talimar . "-album-" . rand(0, 9999) . "." . $ext;
					$target_dir = $folder . $new_name;

					$property_images['talimar_loan'] = $talimar;
					$property_images['discription'] = $discription[$key];
					$property_images['image_name'] = $new_name;
					$property_images['title'] = $title[$key];
					$property_images['t_user_id'] = $t_user_id;
					$this->User_model->insertdata('property_images', $property_images);

					//if (move_uploaded_file($_FILES["files"]["tmp_name"][$key], $target_dir) === TRUE) {
					$attachmentName=$this->aws3->uploadFile($target_dir,$_FILES["files"]["tmp_name"][$key]);
					if($attachmentName){
					}
					$this->session->set_flashdata('success', 'Property Images Updated Successfully!');
				} else {
					$this->session->set_flashdata('error', 'Please upload only .jpg, .jpeg, .png Images only ');
				}
			}
		}
		redirect(base_url() . 'load_data' . $loan_id, 'refresh');

	}

	public function delete_property_image() {
		$loan_id = $this->uri->segment(3);
		$id['id'] = $this->uri->segment(2);

		$fetch_pro_img = $this->User_model->select_where('property_images', $id);
		if ($fetch_pro_img->num_rows() > 0) {
			$fetch_pro_img = $fetch_pro_img->result();

			$filename = $fetch_pro_img[0]->image_name;
			$path = 'property_images/' . $fetch_pro_img[0]->talimar_loan . '/' . $filename; //FCPATH
			$this->aws3->deleteObject($path);
			/*if (file_exists($path)) {
				unlink($path);
			}*/

			$this->User_model->delete('property_images', $id);
			$this->session->set_flashdata('success', ' Deleted');
			redirect(base_url() . 'load_data/' . $loan_id, 'refresh');
		} else {
			$this->session->set_flashdata('error', ' Something Went wrong');
			redirect(base_url() . 'load_data/' . $loan_id, 'refresh');
		}
	}

	public function form_fee_disbursement() {

		/*  echo "<pre>";
			print_r($_POST);
		echo "</pre>" */;

		$fee_disbursement_list = $this->config->item('fee_disbursement_list');
		$items = $this->input->post('items');
		$broker = $this->input->post('broker');
		$loan_servicer = $this->input->post('loan_servicer');
		$lender = $this->input->post('lender');

		$custom_fields = $this->input->post('custom_field');

		$talimar_loan = $this->input->post('talimar_no');
		$loan_id = $this->input->post('loan_id');
		$t_user_id = $this->session->userdata('t_user_id');

		$fci_loan_setup = $this->input->post('fci_loan_setup');
		$fci_paid_option = $this->input->post('fci_paid_option');

		$del_loan_setup = $this->input->post('del_loan_setup');
		$del_paid_option = $this->input->post('del_paid_option');

		// echo '<pre>';
		// print_r($items);
		// echo '</pre>';
		foreach ($fee_disbursement_list as $key => $row) {
			$where['items'] = $row;
			$where['talimar_loan'] = $talimar_loan;

			$check_fee_disbursement = $this->User_model->select_where('fee_disbursement', $where);
			if ($check_fee_disbursement->num_rows() > 0) {
				$update_fee_d['broker'] = $this->amount_format($broker[$key]);
				$update_fee_d['loan_servicer'] = $this->amount_format($loan_servicer[$key]);

				$update_fee_d['lender'] = $this->amount_format($lender[$key]);
				$update_fee_d['t_user_id'] = $t_user_id;
				$this->User_model->updatedata('fee_disbursement', $where, $update_fee_d);
			} else {
				$insert_fee_d['talimar_loan'] = $talimar_loan;
				$insert_fee_d['loan_id'] = trim($loan_id, '/');
				$insert_fee_d['items'] = $row;
				$insert_fee_d['broker'] = $this->amount_format($broker[$key]);
				$insert_fee_d['loan_servicer'] = $this->amount_format($loan_servicer[$key]);
				$insert_fee_d['lender'] = $this->amount_format($lender[$key]);
				$insert_fee_d['t_user_id'] = $t_user_id;

				$this->User_model->insertdata('fee_disbursement', $insert_fee_d);
			}
		}

		foreach ($custom_fields as $key => $fields) {
			$check_talimar['talimar_loan'] = $talimar_loan;

			$update_loan_servicing[$fields] = $this->amount_format($broker[$key]);

			$this->User_model->updatedata('loan_servicing', $check_talimar, $update_loan_servicing);

		}
		/*$loansetup_data['fci_loan_setup'] 	= $this->input->post('fci_loan_setup');
			$loansetup_data['fci_paid_option'] 	= $this->input->post('fci_paid_option');
			$loansetup_data['del_loan_setup'] 	= $this->input->post('del_loan_setup');
			$loansetup_data['del_paid_option'] 	= $this->input->post('del_paid_option');
			$loansetup_data['talimar_loan'] 	= $talimar_loan;

			$check_talimar['talimar_loan']		= $talimar_loan;

			$sql = 'SELECT * FROM loan_servicing WHERE talimar_loan = "'.$talimar_loan.'" ';
			$fetch_loan_servicing_data = $this->User_model->query($sql);
			 if($fetch_loan_servicing_data->num_rows() > 0)
			{
				// echo 'update';
				$this->User_model->updatedata('loan_servicing',$check_talimar,$loansetup_data);
				//update
			}else{

				$this->User_model->insertdata('loan_servicing',$loansetup_data);
		*/

		$this->session->set_flashdata('success', ' Record Updated!');
		redirect(base_url() . 'load_data' . $loan_id, 'refresh');
	}

	public function additional_broker_data() {

		$this->session->set_flashdata('popup_modal_cv', array('assigment_main', 'additional_broker_data'));
		$loan_id = trim($this->input->post('loan_id'), '/');
		$talimar_loan = $this->input->post('talimar_loan');
		if ($loan_id) {
			$data['add_broker_data_desc'] = $this->input->post('add_broker_data_desc');
			$where_loan['id'] = $loan_id;
			$check_loan = $this->User_model->select_where('loan', $where_loan);
			if ($check_loan->num_rows() > 0) {
				$this->User_model->updatedata('loan', $where_loan, $data);
				$this->session->set_flashdata('success', ' Additional Lender Disclosures Updated Successfully!');
				redirect(base_url() . 'load_data/' . $loan_id, 'refresh');
			} else {
				$this->session->set_flashdata('error', ' Something went wrong');
				redirect(base_url() . 'load_data/' . $loan_id, 'refresh');
			}
		} else {
			$this->session->set_flashdata('error', ' Something went wrong');
			redirect(base_url() . 'load_data', 'refresh');
		}

	}

	public function update_all_assigment_data() {

		/* $loan_id 	= $this->uri->segment(2);
			$lender_id 	= $this->uri->segment(3) ? $this->uri->segment(3) : '';
		*/
		$loan_id = $this->input->post('loan_id');
		$lender_id = $this->input->post('lender_id') ? $this->input->post('lender_id') : '';

		if ($lender_id) {
			$sql = "SELECT * FROM loan_assigment as la JOIN loan as l on l.talimar_loan = la.talimar_loan WHERE la.lender_name = '" . $lender_id . "' AND l.id = '" . $loan_id . "' ";
		} else {
			// $sql = "SELECT * FROM loan_assigment";
			$sql = "SELECT * FROM loan_assigment as la JOIN loan as l on l.talimar_loan = la.talimar_loan WHERE l.id = '" . $loan_id . "'";
		}
		// echo $sql;
		// $fetch_all_assigment_data = $this->User_model->select_star('loan_assigment');
		$fetch_all_assigment_data = $this->User_model->query($sql);

		// echo "SELECT * FROM loan_assigment ".$cond."";

		$fetch_all_assigment_data = $fetch_all_assigment_data->result();

		foreach ($fetch_all_assigment_data as $row) {
			$ass_id = $row->id;
			$ass_investvest = $row->investment;
			$ass_investvesttotal = $row->investmenttotal;
			$ass_percent_loan = $row->percent_loan;
			$ass_talimar = $row->talimar_loan;
			$invester_yield_percent = $row->invester_yield_percent;

			$sql = 'SELECT * FROM loan WHERE talimar_loan = "' . $ass_talimar . '" ';
			$fetch_loan_data = $this->User_model->query($sql);
			if ($fetch_loan_data->num_rows() > 0) {
				$servicing_fee = 0;
				$servicing_lender_rate = 0;
				$fetch_loan_servicing = $this->User_model->select_where('loan_servicing', array('talimar_loan' => $ass_talimar));

				if ($fetch_loan_servicing->num_rows() > 0) {
					$fetch_loan_servicing = $fetch_loan_servicing->result();
					$servicing_fee = $fetch_loan_servicing[0]->servicing_fee ? $fetch_loan_servicing[0]->servicing_fee : 0;
					$servicing_lender_rate = number_format($fetch_loan_servicing[0]->servicing_lender_rate,3);
				}

				$fetch_loan_data = $fetch_loan_data->result();

				$intrest_rate = number_format($fetch_loan_data[0]->intrest_rate,3);

				//$percent_yield 		= $intrest_rate - $servicing_fee;
				$percent_yield = $invester_yield_percent ? $invester_yield_percent : $servicing_lender_rate;

				$loan_amount = $fetch_loan_data[0]->loan_amount;

				$percent_loan = ($ass_investvesttotal / $loan_amount) * 100;

				$payment = ($ass_investvest * ($percent_yield/12))/100;
				$paymenttotal = ($ass_investvesttotal * ($percent_yield/12))/100;

				$update_sql = 'UPDATE loan_assigment SET percent_loan = "'.$percent_loan.'" , invester_yield_percent = "'.$percent_yield.'" , payment = "'.$payment.'", paymenttotal = "'.$paymenttotal.'"  WHERE id = "'.$ass_id.'"';

				$this->User_model->query($update_sql);

			}
		}

		if (!$lender_id) {

			/* $this->session->set_flashdata('success','All Assignments Records Updated Successfully!');
			redirect(base_url().'load_data/'.$loan_id); */

			echo '2';

		} else {

			echo '1';
		}

	}

	public function update_all_assigment($talimar) {
		$where['talimar_loan'] = $talimar;
		$fetch_all_assigment_data = $this->User_model->select_where('loan_assigment', $where);
		$fetch_all_assigment_data = $fetch_all_assigment_data->result();
		foreach ($fetch_all_assigment_data as $row) {
			$ass_id = $row->id;
			$ass_investvest = $row->investment;
			$ass_percent_loan = $row->percent_loan;
			$ass_talimar = $row->talimar_loan;
			$invester_yield_percent = $row->invester_yield_percent;

			$sql = 'SELECT * FROM loan WHERE talimar_loan = "' . $ass_talimar . '" ';
			$fetch_loan_data = $this->User_model->query($sql);
			if ($fetch_loan_data->num_rows() > 0) {
				$servicing_fee = 0;
				$fetch_loan_servicing = $this->User_model->select_where('loan_servicing', array('talimar_loan' => $ass_talimar));
				if ($fetch_loan_servicing->num_rows() > 0) {
					$fetch_loan_servicing = $fetch_loan_servicing->result();
					$servicing_fee = $fetch_loan_servicing[0]->servicing_fee ? $fetch_loan_servicing[0]->servicing_fee : 0;
				}

				$fetch_loan_data = $fetch_loan_data->result();
				$intrest_rate = number_format($fetch_loan_data[0]->intrest_rate,3);
				//$percent_yield 		= $intrest_rate - $servicing_fee;
				$percent_yield = $invester_yield_percent;
				$loan_amount = $fetch_loan_data[0]->loan_amount;

				$percent_loan = ($ass_investvest / $loan_amount) * 100;

				$payment = (($percent_yield / 100) * $ass_investvest) / 12;
				$update_sql = 'UPDATE loan_assigment SET percent_loan = "' . $percent_loan . '" , invester_yield_percent = "' . $percent_yield . '" , payment = "' . $payment . '"  WHERE id = "' . $ass_id . '"';

				// echo $update_sql.'-----------Previous =>  '.$ass_percent_loan.'<br>';

				$this->User_model->query($update_sql);
			}
		}

		return 1;
	}

	public function form_wired_instruction() {
		$loan_id = trim($this->input->post('loan_id'), '/');
		$talimar_loan = $this->input->post('talimar_no');

		if ($loan_id) {
			$where['loan_id'] = $loan_id;
			$where['talimar_loan'] = $talimar_loan;

			$data['bank_name'] = $this->input->post('bank_name');
			$data['bank_street'] = $this->input->post('bank_street');
			$data['bank_unit'] = $this->input->post('bank_unit');
			$data['bank_city'] = $this->input->post('bank_city');
			$data['bank_state'] = $this->input->post('bank_state');
			$data['bank_zip'] = $this->input->post('bank_zip');

			$data['recipient_name'] = $this->input->post('recipient_name');
			$data['recipient_phone_no'] = $this->input->post('recipient_phone_no');
			$data['recipient_street'] = $this->input->post('recipient_street');
			$data['recipient_unit'] = $this->input->post('recipient_unit');
			$data['recipient_city'] = $this->input->post('recipient_city');
			$data['recipient_state'] = $this->input->post('recipient_state');
			$data['recipient_zip'] = $this->input->post('recipient_zip');

			$data['routing_number'] = $this->input->post('routing_number');
			$data['account_number'] = $this->input->post('account_number');
			$data['credit_to'] = $this->input->post('credit_to');

			$data['wire_tali_lender'] = $this->input->post('wire_tali_lender') ? 1 : 0;
			$data['wire_confirmed'] = $this->input->post('wire_confirmed') ? 1 : 0;

			$fetch_wire_instruction = $this->User_model->select_where('wire_instruction', $where);

			if ($fetch_wire_instruction->num_rows() > 0) {
				$this->User_model->updatedata('wire_instruction', $where, $data);
				$this->session->set_flashdata('success', ' Updated');
			} else {
				$data['talimar_loan'] = $talimar_loan;
				$data['loan_id'] = $loan_id;
				$this->User_model->insertdata('wire_instruction', $data);
				$this->session->set_flashdata('success', ' Records add successfully');
			}
			$this->session->set_flashdata('popup_modal_cv', array('escrow', 'wired_instruction'));
			redirect(base_url() . 'load_data/' . $loan_id);
		} else {
			$this->session->set_flashdata('popup_modal_cv', array('escrow', 'wired_instruction'));
			$this->session->set_flashdata('error', ' Somenthing went Wrong');
			redirect(base_url() . 'load_data');
		}
	}

	public function load_impound_account_data($talimar) {
		
		$items = array('Mortgage Payment', 'County Property Taxes', 'Property / Hazard Insurance Payment', 'Mortgage Insurance', 'Flood Insurance','School Tax','City Tax','Water / Sewer Tax','Township Tax','Other Tax','Wind Insurance');
		foreach ($items as $key => $row) {

			$fetch_impound_account = $this->User_model->select_where('impound_account', array('talimar_loan' => $talimar,'items'=>$row));
			if ($fetch_impound_account->num_rows() == 0) {

				$insert['items'] = $row;
				$insert['amount'] = 0;
				$insert['impounded'] = 0;
				$insert['position'] = $key;
				$insert['talimar_loan'] = $talimar;
				$this->User_model->insertdata('impound_account', $insert);
			}
		}
	}

	public function form_impound_accounts() {
		// echo "<pre>";
			// print_r($_POST);
		// echo "</pre>";
		
		
		// die('ssss');
		error_reporting(0);
		// perform delete action....
		$delete_monthly_payment_id = array_values(array_filter($this->input->post('checkbox_delete')));
		$impount_acct_id = array_values(array_filter($this->input->post('impount_acct_id')));

		//delete impound_account....
		if ($delete_monthly_payment_id) {
			$loan_id = $this->input->post('loan_id');
			foreach ($delete_monthly_payment_id as $key => $mp) {

				// delete query ....

				$this->User_model->query("DELETE FROM impound_account WHERE id = '" . $impount_acct_id[$key] . "' ");
			}

			$this->session->set_flashdata('popup_modal_cv', array('impound_accounts'));
			$this->session->set_flashdata('impound_action', '1');

			redirect(base_url() . 'load_data' . $loan_id);

		} else {

			$loan_id = $this->input->post('loan_id');
			$talimar_loan = $this->input->post('talimar_no');
			$impound_yesno = $this->input->post('impound_yesno');
			$where['talimar_loan'] = $this->input->post('talimar_no');

			// $fetch_impound_account 	= $this->User_model->select_where('impound_account',$where);
			$fetch_impound_account = $this->User_model->query("SELECT * FROM impound_account WHERE talimar_loan = '" . $talimar_loan . "' ORDER by position ASC");

			// update into "impound_account" table...
			if ($fetch_impound_account->num_rows() > 0) {
				$fetch_impound_account = $fetch_impound_account->result();
				foreach ($fetch_impound_account as $row) {
					$update['amount'] = $this->amount_format($this->input->post('amount_' . $row->id));

					$update['impounded'] = $impound_yesno;

					$this->User_model->updatedata('impound_account', array('id' => $row->id), $update);
				}

			}

			// lender_impound_payment in extra detail

			$e_d['lender_impound_payment'] = $this->input->post('lender_impound_payment');

			$fetch_extra_detail = $this->User_model->select_where('extra_details', $where);
			if ($fetch_extra_detail->num_rows() > 0) {
				$this->User_model->updatedata('extra_details', $where, $e_d);
			} else {
				$e_d['talimar_loan'] = $this->input->post('talimar_no');
				$this->User_model->insertdata('extra_details', $e_d);
			}

			$items = $this->input->post('items');
			$amount = $this->input->post('amount');
			//$impounded = $this->input->post('impounded');
			$impound_yesno = $this->input->post('$impound_yesno');

			foreach ($items as $key => $row) {
				if ($items[$key] != '') {
					$insert = "INSERT INTO `impound_account`(`talimar_loan`, `items`, `amount`, `impounded`) VALUES ( '" . $talimar_loan . "', '" . $items[$key] . "', '" . $this->amount_format($amount[$key]) . "', '" . $impound_yesno . "')";
					$this->User_model->query($insert);
				}
			}
			$this->session->set_flashdata('popup_modal_cv', array('impound_accounts'));
			$this->session->set_flashdata('success', 'Monthly Payment Updated Successfully!');
			redirect(base_url() . 'load_data' . $loan_id);
		}
	}

	public function load_closing_statement_data($talimar) {
		$closing_statement_item_loads = $this->config->item('closing_statement_item_loads');

		foreach ($closing_statement_item_loads as $key => $row) {
			/* echo'<pre>';
				print_r($key);
				print_r($row);
			*/
			$where['talimar_loan'] = $talimar;
			$where['hud'] = $key;
			$fetch_closing_items = $this->User_model->select_where('closing_statement_items', $where);
			if ($fetch_closing_items->num_rows() == 0) {
				$insert['talimar_loan'] = $talimar;
				$insert['hud'] = $key;
				$insert['items'] = $row;
				$insert['paid_to_other'] = 0;
				$insert['paid_to_broker'] = 0;
				$insert['paid_to'] = '';
				$insert['net_funding'] = 0;
				$insert['direct_to_servicer'] = 0;
				$insert['prepaid_charge'] = 0;

				$this->User_model->insertdata('closing_statement_items', $insert);
			}
		}

		return 1;
	}

	public function underwriting_option_insert($talimar) {

		$underwriting_items_option = $this->config->item('underwriting_items_option');

		foreach ($underwriting_items_option as $key => $row) {

			/* echo'<pre>';
				print_r($underwriting_items_option);
			*/

			$where['talimar_loan'] = $talimar;
			$where['items_id'] = $key;

			$fetch_underwriting = $this->User_model->select_where('underwriting', $where);

			if ($fetch_underwriting->num_rows() == 0) {
				$insert['items_id'] = $key;
				$insert['talimar_loan'] = $talimar;
				$insert['items'] = $row;
				$insert['required'] = 0;
				$insert['received'] = 0;

				$insert['loan_id'] = $this->uri->segment(2);
				if ($key < 100) {
					$type = 1;
				} elseif ($key > 100 && $key < 200) {
					$type = 2;
				} elseif ($key > 200 && $key < 300) {
					$type = 3;
				}
				elseif ($key > 300 && $key < 400) {
					$type = 4;
				}
				elseif ($key > 400 && $key < 500) {
					$type = 5;
				}elseif ($key > 500 && $key < 600) {
					$type = 6;
				}
				$insert['type'] = $type;
				$this->User_model->insertdata('underwriting', $insert);
			}
		}

		return 1;
	}

	public function servicing_checklist($talimar) {

		$loan_servicing_checklist = $this->config->item('loan_servicing_checklist');
		foreach ($loan_servicing_checklist as $key => $row) {

			$where['talimar_loan'] = $talimar;
			$where['hud'] = $key;

			$loan_servicing = $this->User_model->select_where('loan_servicing_checklist', $where);

			if ($loan_servicing->num_rows() == 0) {

				$insert['talimar_loan'] = $talimar;
				$insert['loan_id'] = $this->uri->segment(2);

				if ($key < 1000) {

					$type = 'pre wire';

				} else {

					$type = 'post wire';
				}

				$insert['type'] = $type;
				$insert['hud'] = $key;
				$insert['items'] = $row;
				$insert['checklist'] = 0;

				$this->User_model->insertdata('loan_servicing_checklist', $insert);
			}

		}
		return 1;
	}

	public function add_checklist_value() {

		$loan_idd = $this->input->post('loan_id');
		$loan_iddd = str_replace("/", "", $loan_idd);

		$talimar_loan = $this->input->post('talimar_no');
		$where['talimar_loan'] = $this->input->post('talimar_no');

		$select_checklist = $this->User_model->select_where('loan_servicing_checklist', $where);
		if ($select_checklist->num_rows() > 0) {

			$select_checklist = $select_checklist->result();

			foreach ($select_checklist as $row) {

				if ($this->input->post('id_' . $row->id)) {

					$where1['id'] = $this->input->post('id_' . $row->id);
					//$where2['loan_id'] 	 =$loan_iddd;
					$update['checklist'] = $this->input->post('check_' . $row->id);
					$update['not_applicable'] = $this->input->post('not_applicable_' . $row->id);

					$this->User_model->updatedata('loan_servicing_checklist', $where1, $update);

				}

			}
		}

		$hud = $this->input->post('new_checklist_items');

		$new_check_text = $this->input->post('new_check_text');
		$new_applied = $this->input->post('new_applied');
		$new_checklist = $this->input->post('new_checklist');
		$loan = $loan_iddd; //str_replace("/", "", $loan_id);

		foreach ($new_check_text as $key => $row) {

			if ($row != '') {

				if ($hud[$key] < 1000) {
					$type = 'pre wire';
				} else {
					$type = 'post wire';
				}

				$sql = "INSERT INTO `loan_servicing_checklist`(`talimar_loan`, `loan_id`, `type`, `hud`, `items`, `checklist`, `not_applicable`) VALUES ('" . $talimar_loan . "', '" . $loan . "', '" . $type . "', '" . $hud[$key] . "', '" . $new_check_text[$key] . "', '" . $new_checklist[$key] . "', '" . $new_applied[$key] . "')";

				// echo $sql;
				// die('kl');
				$this->User_model->query($sql);
			}
		}

		$checklist_complete = $this->input->post('checklist_complete');

		$fetch_input_datas = $this->User_model->select_where('input_datas', array('talimar_loan' => $talimar_loan, 'page' => 'loan_servicing_checklist', 'input_name' => 'checklist_complete'));

		if ($fetch_input_datas->num_rows() > 0) {

			$this->User_model->updatedata('input_datas', array('talimar_loan' => $talimar_loan, 'page' => 'loan_servicing_checklist', 'input_name' => 'checklist_complete'), array('value' => $checklist_complete));

		} else {

			$this->User_model->insertdata('input_datas', array('talimar_loan' => $talimar_loan, 'page' => 'loan_servicing_checklist', 'input_name' => 'checklist_complete', 'value' => $checklist_complete));
		}

		//----------------------------------
		// For Final approval email
		//----------------------------------

		$request_final_approval = $this->input->post('request_final_approval');
		$final_closing_approval = $this->input->post('final_closing_approval');

		$data_app['request_final_approval'] = $request_final_approval;
		$data_app['final_closing_approval'] = $final_closing_approval;

		$select_approval = $this->User_model->select_where('servicing_checklist_approval', array('talimar_loan' => $talimar_loan));
		if ($select_approval->num_rows() > 0) {

			$where['talimar_loan'] = $talimar_loan;
			$this->User_model->updatedata('servicing_checklist_approval', $where, $data_app);

		} else {

			$data_app['talimar_loan'] = $talimar_loan;
			$this->User_model->insertdata('servicing_checklist_approval', $data_app);
		}

		$this->session->set_flashdata('success', 'Servicing Checklist Updated Successfully!');
		$this->session->set_flashdata('popup_modal_hit', 'checklisting');

		redirect(base_url() . 'load_data/' . $loan_iddd.'#loan_closing_checklist');

	}

	public function loan_servicing_contacts() {

		$talimar_loan = $this->input->post('talimar_loan');
		$loan_id = str_replace('/', '', $this->input->post('loan_id'));
		$loan_originator = $this->input->post('loan_originator');
		$underwriter = $this->input->post('underwriter');
		$loan_closer = $this->input->post('loan_closer');
		$investor_relations = $this->input->post('investor_relations');
		$loan_servicer = $this->input->post('loan_servicer');
		$t_user_id = $this->session->userdata('t_user_id');

		// echo '<pre>';
		// print_r($_POST);
		// echo '</pre>';

		// die();

		$data['loan_id'] = $loan_id;
		$data['loan_originator'] = $loan_originator;
		$data['underwriter'] = $underwriter;
		$data['loan_closer'] = $loan_closer;
		$data['investor_relations'] = $investor_relations;
		$data['loan_servicer'] = $loan_servicer;
		$data['t_user_id'] = $t_user_id;
		
		$select_contacts = $this->User_model->query("Select * from loan_servicing_contacts where talimar_loan = '" . $talimar_loan . "'");
		if ($select_contacts->num_rows() > 0) {
			$where['talimar_loan'] = $talimar_loan;
			$this->User_model->updatedata('loan_servicing_contacts', $where, $data);

		} else {

			$data['talimar_loan'] = $talimar_loan;
			//echo "<pre>";
		//print_r($data);
			$this->User_model->insertdata('loan_servicing_contacts', $data);
			//echo "in else";
			//die();

		}

		$this->session->set_flashdata('success', 'Internal Contacts Updated Successfully!');
		redirect(base_url() . 'load_data/' . $loan_id);

	}

	public function selected_loan_property_datas($talimar_loan) {

		$where['talimar_loan'] = $talimar_loan;
		$property = $this->User_model->select_where('loan_property', $where);
		return $property = $property->result();
	}

	public function selected_ecumbrance_datas($talimar_loan) {

		$where['talimar_loan'] = $talimar_loan;
		$encumbrances = $this->User_model->select_where('encumbrances', $where);
		return $encumbrances = $encumbrances->result();
	}

	public function send_mail_to_user() {

		$loan_closer = $this->input->post('closer_id');
		$investor_relations = $this->input->post('investor_relations');
		$talimar_loan = $this->input->post('talimar_loan');
		$servicing = $this->input->post('servicing');

		$select_users = $this->User_model->query("Select * from user where id IN('" . $loan_closer . "','" . $investor_relations . "')");
		$select_users = $select_users->result();

		// echo '<pre>';
		// print_r($select_users);
		// echo '</pre>';
		foreach ($select_users as $key => $row) {

			$email_address = $row->email_address;
			$email_fname = $row->fname;
			//$bcc_emails = 'pankaj.wartiz@gmail.com';

			$loan_type_option = $this->config->item('loan_type_option');

			$selectloan = $this->User_model->select_where('loan', array('talimar_loan' => $talimar_loan));
			$selectloan = $selectloan->result();

			$propertydata = $this->selected_loan_property_datas($talimar_loan);
			$full_address = $propertydata[0]->property_address . '' . $propertydata[0]->unit . '; ' . $propertydata[0]->city . ', ' . $propertydata[0]->state . ' ' . $propertydata[0]->zip;
			$underwriting_value = $propertydata[0]->underwriting_value;

			$ecumdata = $this->selected_ecumbrance_datas($talimar_loan);
			$total_ecum = 0;
			foreach ($ecumdata as $value) {

				if ($value->property_home_id != '') {

					$total_ecum += $value->original_balance;
				}
			}

			$ltv = ($selectloan[0]->loan_amount + $total_ecum) / $underwriting_value * 100;

			$subject = "Status Update – " . $talimar_loan;
			$header = "From: mail@database.talimarfinancial.com";

			$message = 'Hi ' . $email_fname . ':<br><br>';

			$message .= $talimar_loan . ' has been updated to underwriting.<br><br>';

			$message .= 'Property Address: ' . $full_address . '<br>';
			$message .= 'Loan Amount: $' . number_format($selectloan[0]->loan_amount, 2) . '<br>';
			$message .= 'Estimated Closing Date: ' . date('m-d-Y', strtotime($selectloan[0]->loan_funding_date)) . '<br>';
			$message .= 'Loan Type: ' . $loan_type_option[$selectloan[0]->loan_type] . '<br>';
			$message .= 'Rate: ' . number_format($selectloan[0]->intrest_rate - $servicing, 3) . '%<br>';
			$message .= 'LTV: ' . number_format($ltv, 2) . '%<br><br>';

			$message .= 'You may access the loan by '; //<a target="_blank" href="' . base_url() . 'load_data/' . $selectloan[0]->id . '">clicking here</a> <br>';

			/*$this->email->initialize(SMTP_SENDGRID);
			$this->email
				->from('mail@talimarfinancial.com', $subject)
				->to($email_address)
				->subject($subject)
				->message($message)
				->set_mailtype('html');

			$this->email->send();*/

			$dataMailA = array();
			$dataMailA['from_email'] 	= 'mail@talimarfinancial.com';
			$dataMailA['from_name'] 	= 'TaliMar Financial';
			$dataMailA['to'] 			= $email_address;
			$dataMailA['cc'] 			= '';
			$dataMailA['name'] 			= '';
			$dataMailA['subject'] 		= $subject;
			$dataMailA['content_body'] 	= $message;
			$dataMailA['button_url'] 	= base_url() . 'load_data/' . $selectloan[0]->id;
			$dataMailA['button_text'] 	= 'clicking here';
			send_mail_template($dataMailA);
		}
		echo '1';
		//echo $message;
		//echo $email_address;

	}

	public function add_affiliated_parties() {
		// echo '<pre>';
		// print_r($_POST);
		// print_r($this->input->post('id'));
		// echo '</pre>';
		// die();
		$talimar_loan = $this->input->post('talimar_no');
		$loan_id = str_replace('/', '', $this->input->post('loan_id'));

		$id = $this->input->post('id');
		$position = $this->input->post('position');
		$name = $this->input->post('name');
		$affiliation = $this->input->post('affiliation');
		$affiliation_text = $this->input->post('affiliation_text');
		$phone = $this->input->post('phone');
		$email = $this->input->post('email');
		$dis_loan_term = $this->input->post('dis_loan_term');

		//die();
		// foreach($_POST['id'] as $key => $row){

		// if($row == 'new'){

		// insert into name '".$_POST['name'][$key]."'
		// }

		// }

		foreach ($id as $key => $row) {

			if ($row == 'new') {

				$sql = "INSERT INTO `loan_affiliated_parties`(`talimar_loan`, `loan_id`, `name`, `dis_loan_term`, `affiliation`, `affiliation_text`, `phone`, `email`) VALUES ('" . $talimar_loan . "', '" . $loan_id . "', '" . $name[$key] . "', '" . $dis_loan_term[$key] . "', '" . $affiliation[$key] . "', '" . $affiliation_text[$key] . "', '" . $phone[$key] . "', '" . $email[$key] . "')";

			} else {

				$sql = "UPDATE `loan_affiliated_parties` SET `talimar_loan`='" . $talimar_loan . "', `loan_id`='" . $loan_id . "', `name`='" . $name[$key] . "', `dis_loan_term`='" . $dis_loan_term[$key] . "', `affiliation`='" . $affiliation[$key] . "', `affiliation_text`='" . $affiliation_text[$key] . "', `phone`='" . $phone[$key] . "', `email`='" . $email[$key] . "' WHERE id = '" . $row . "'";

			}

			$this->User_model->query($sql);

		}

		$this->session->set_flashdata('success', 'Affiliated Parties Updated Successfully!');
		redirect(base_url() . 'load_data/' . $loan_id);
	}

	// public function add_loan_purpose(){

	// 	$tali_mar_loan				=	$this->input->post('talimar_loan');
	// 	$f_t_user_id				=	$this->session->userdata('t_user_id');
	//     $loan_id 				    =   $this->input->post('loan_id');
	// 	$loan_purpose 			    =   str_replace("'","",$this->input->post('promissory_textt'));

	//      $hidden_id 				=   $this->input->post('hidden_id');

	// 	foreach($hidden_id as $key => $row){

	// 		if($loan_purpose[$key] != ''){

	// 			if($row == 'new'){

	// 			$sql = "INSERT INTO `servicing_purpose`(`talimar_loan`, `t_user_id`, `loan_id`, `loan_purpose`) VALUES ('".$tali_mar_loan."', '".$f_t_user_id."', '".$loan_id."', '".$loan_purpose[$key]."')";

	// 			}else{

	// 				$sql = "UPDATE `servicing_purpose` SET `loan_purpose`='".$loan_purpose[$key]."' WHERE servicing_purpose_id='".$row."'";
	// 			}
	// 				$this->User_model->query($sql);
	// 		}
	// 	}

	// 	$this->session->set_flashdata('success','Loan Purpose Updated Successfully');
	// 	redirect(base_url().'load_data/'.$loan_id);
	// }

	public function delete_loan_purpose() {
		$id = $this->input->post('id');
		$this->User_model->delete('servicing_purpose', array('servicing_purpose_id' => $id));
		echo 1;
	}

	public function form_underwriting_items() {

		$loan_id = $this->input->post('loan_id');
		$talimar_loan = $this->input->post('talimar_loan');
		$where['talimar_loan'] = $this->input->post('talimar_loan');

		$fetch_underwriting_item = $this->User_model->select_where('underwriting', $where);
		if ($fetch_underwriting_item->num_rows() > 0) {

			$fetch_underwriting_item = $fetch_underwriting_item->result();
			foreach ($fetch_underwriting_item as $row) {

				if ($this->input->post('id_' . $row->id)) {
					$where1['id'] = $this->input->post('id_' . $row->id);

					$update['required'] = $this->input->post('required_' . $row->id);

					$update['received'] = $this->input->post('received_' . $row->id);

					$this->User_model->updatedata('underwriting', $where1, $update);

				}

			}

		}

		$new_items = $this->input->post('new_items');
		$input_type = $this->input->post('input_items_val');
		$new_required = $this->input->post('new_required');
		$new_type = $this->input->post('items_id');
		$new_received = $this->input->post('new_received');
		$loan = str_replace("/", "", $loan_id);

		foreach ($input_type as $key => $row) {

			if ($row != '') {
				if ($input_type[$key] < 100) {
					$type = 1;
				} elseif ($input_type[$key] > 100 && $input_type[$key] < 200) {
					$type = 2;
				} elseif ($input_type[$key] > 200 && $input_type[$key] < 300) {
					$type = 3;
				} elseif ($input_type[$key] > 300 && $input_type[$key] < 400) {
					$type = 4;
				} elseif ($input_type[$key] > 400 && $input_type[$key] < 500) {
					$type = 5;
				}elseif ($input_type[$key] > 500 && $input_type[$key] < 600) {
					$type = 6;
				}elseif ($input_type[$key] > 1000) {
					$type = 10;
				}
				if(empty($new_required[$key])){
					$new_required[$key]='';
				}
				$sql = "INSERT INTO `underwriting`(`talimar_loan`, `loan_id`, `type`, `required`, `received`, `items_id`, `items`) VALUES ('" . $talimar_loan . "','" . $loan . "', '" . $type . "', '" . $new_required[$key] . "', '" . $new_received[$key] . "', '" . $input_type[$key] . "', '" . $new_items[$key] . "')";

				// echo $sql.'<br>';
				// die('kl');
				$this->User_model->query($sql);
			}
		}

		$delligenge_required = $this->input->post('delligenge_required');
		$fetch_input_datas = $this->User_model->select_where('input_datas', array('talimar_loan' => $talimar_loan, 'page' => 'underwriting_page', 'input_name' => 'delligenge_required'));
		if ($fetch_input_datas->num_rows() > 0) {
			$this->User_model->updatedata('input_datas', array('talimar_loan' => $talimar_loan, 'page' => 'underwriting_page', 'input_name' => 'delligenge_required'), array('value' => $delligenge_required));
		} else {
			$this->User_model->insertdata('input_datas', array('talimar_loan' => $talimar_loan, 'page' => 'underwriting_page', 'value' => $delligenge_required, 'input_name' => 'delligenge_required'));
		}

		$this->session->set_flashdata('success', 'Underwriting Updated Successfully!');

		if ($this->input->post('action') == 'Save') {

			$this->session->set_flashdata('popup_modal_hit', 'diligence_materials');

		}
		$this->session->set_flashdata('popup_modal_cv', array('diligence_materials'));
		redirect(base_url() . 'load_data' . $loan_id);
	}

	public function delete_affilated_contact() {

		$id = $this->input->post('id');

		$delete_contact = $this->User_model->query("DELETE FROM loan_affiliated_parties WHERE id = '" . $id . "'");
		echo '1';

	}

	public function form_closing_statement_items() {

		error_reporting(E_ALL);
		$loan_id = $this->input->post('loan_id');
		$talimar_loan = $this->input->post('talimar_no');
		$where['talimar_loan'] = $this->input->post('talimar_no');
		$fetch_closing_statement_item = $this->User_model->select_where('closing_statement_items', $where);

		// echo $fetch_closing_statement_item->num_rows();

		if ($fetch_closing_statement_item->num_rows() > 0) {
			$fetch_closing_statement_item = $fetch_closing_statement_item->result();
			foreach ($fetch_closing_statement_item as $row) {
				if ($this->input->post('id_' . $row->id)) {
					$where1['id'] = $this->input->post('id_' . $row->id);
					// $update['id'] 					= $this->input->post('id_'.$row->id);
					$update['paid_to_other'] = $this->amount_format($this->input->post('paid_to_other_' . $row->id));
					$update['paid_to_broker'] = $this->amount_format($this->input->post('paid_to_broker_' . $row->id));
					$update['paid_to'] = $this->input->post('paid_to_' . $row->id);
					$update['net_funding'] = $this->input->post('net_funding_' . $row->id);
					$update['direct_to_servicer'] = $this->input->post('direct_to_servicer_' . $row->id);

					$update['loan_reserves'] = $this->input->post('loan_reserves_' . $row->id);

					$update['prepaid_charge'] = $this->input->post('prepaid_charge_' . $row->id);
					$update['lender_statement'] = $this->input->post('lender_statement_' . $row->id);

					$this->User_model->updatedata('closing_statement_items', $where1, $update);
					// echo '<pre>';
					// print_r($update);
					// echo '</pre>';
				}

			}
		}
		// die('stop');
		$hud = $this->input->post('hud');
		$paid_to_broker = $this->input->post('paid_to_broker');
		$paid_to_other = $this->input->post('paid_to_other');
		$paid_to = $this->input->post('paid_to');
		$prepaid_charge = $this->input->post('prepaid_charge');
		$net_funding = $this->input->post('net_funding');
		$direct_to_servicer = $this->input->post('direct_to_servicer');
		$loan_reserves = $this->input->post('loan_reserves');
		$items = $this->input->post('items');
		$lender_statement = $this->input->post('lender_statement');

		foreach ($hud as $key => $row) {

			if ($row != '') {
				$sql = "INSERT INTO `closing_statement_items`( `talimar_loan`, `hud`, `items`, `paid_to_other`, `paid_to_broker`, `paid_to`, `net_funding`,`direct_to_servicer`,`loan_reserves`, `prepaid_charge`, `lender_statement`) VALUES ( '" . $talimar_loan . "' ,'" . $hud[$key] . "' ,  '" . $items[$key] . "' , '" . $this->amount_format($paid_to_other[$key]) . "' ,  '" . $this->amount_format($paid_to_broker[$key]) . "' , '" . $paid_to[$key] . "' ,  '" . $net_funding[$key] . "' ,  '" . $direct_to_servicer[$key] . "' , '" . $loan_reserves[$key] . "' , '" . $prepaid_charge[$key] . "',  '" . $lender_statement[$key] . "' ) ";

				$this->User_model->query($sql);
			}
		}

		$fetch_input_datas1 = $this->User_model->select_where('input_datas', array('talimar_loan' => $talimar_loan, 'page' => 'closing_statement'));

		if((int)$fetch_input_datas1->num_rows() > 0) {
			$fetch_input_datas = $fetch_input_datas1->result();
			foreach ($fetch_input_datas as $row) {
				$update_input_datas['value'] = $this->amount_format((double)$this->input->post($row->input_name));
				$where_input_datas['id'] = $row->id;
				$sql = "UPDATE input_datas SET value = '" . $this->amount_format((double)$this->input->post($row->input_name)) . "' WHERE id = '" . $row->id . "' ";
				
				$this->User_model->query($sql);
			}
		}

		$a901_input1 = $this->amount_format((double)$this->input->post('901_input1'));
		$a901_input2 = $this->amount_format((double)$this->input->post('901_input2'));
		$input_paid_901 = $a901_input1 * $a901_input2;

		$this->User_model->updatedata('closing_statement_items', array('talimar_loan' => $talimar_loan, 'hud' => '901'), array('paid_to_other' => $input_paid_901));

		$item_deduction = $this->input->post('item_deduction');
		$amount_deduction = $this->input->post('amount_deduction');

		foreach ($item_deduction as $key => $ded) {
			if ($ded != '') {
				$sql_ded = "INSERT INTO closing_statement_deductions ( `talimar_loan`, `items`, `value`) VALUES ('" . $talimar_loan . "', '" . $ded . "','" . $this->amount_format($amount_deduction[$key]) . "' )";
				$this->User_model->query($sql_ded);
			}
		}

		$fetch_closing_statement_deduction = $this->User_model->select_where('closing_statement_deductions', array('talimar_loan' => $talimar_loan));
		if ($fetch_closing_statement_deduction->num_rows() > 0) {
			$fetch_closing_deduction = $fetch_closing_statement_deduction->result();
			foreach ($fetch_closing_deduction as $row) {
				$ded_item = $this->input->post('item_deduction_' . $row->id);
				$ded_amount = $this->amount_format($this->input->post('amount_deduction_' . $row->id));
				$this->User_model->updatedata('closing_statement_deductions', array('id' => $row->id), array('items' => $ded_item, 'value' => $ded_amount));
			}
		}
		if ($this->input->post('action') == 'save') {

			$this->session->set_flashdata('popup_modal_hit', 'closing_statement');
		}
		$this->session->set_flashdata('popup_modal_cv', array('closing_statement_new11'));
		$this->session->set_flashdata('success', 'Closing Statement Updated Successfully!');
		redirect(base_url() . 'load_data' . $loan_id);
	}

	public function input_datas_data($talimar, $input_fields, $page) {
		foreach ($input_fields as $key => $row) {
			$fetch_input_datas = $this->User_model->select_where('input_datas', array('talimar_loan' => $talimar, 'input_name' => $key, 'page' => $page));
			if ($fetch_input_datas->num_rows() == 0) {
				$insert['input_name'] = $key;
				$insert['value'] = $row;
				$insert['talimar_loan'] = $talimar;
				$insert['page'] = $page;
				$this->User_model->insertdata('input_datas', $insert);
			}
		}

	}

	public function check_date_format($d) {
		// date format should be m-d-Y
		if (preg_match("/^(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])-[0-9]{4}$/", $d)) {
			return 1;
		} else {
			return 0;
		}
	}

	// public function updatedatabase_encumbrances()
	// {
	// WE use this function to add loan data in ecumbrance
	// $fetch_loan_data = $this->User_model->select_star('loan');
	// $fetch_loan_data = $fetch_loan_data->result();
	// foreach($fetch_loan_data as $loan)
	// {
	// $fetch_monthy_payment = $this->User_model->query("SELECT SUM(amount) as monthly_payment FROM impound_account WHERE talimar_loan = '".$loan->talimar_loan."' ");
	// $fetch_monthy_payment = $fetch_monthy_payment->result();
	// $impound_amount = ($loan->intrest_rate/100) * $loan->loan_amount / 12;
	// $monthly_payment = $impound_amount + $fetch_monthy_payment[0]->monthly_payment;

	// $ballon_payment_b = $monthly_payment + $loan->current_balance;

	// $sql = "INSERT INTO encumbrances ( `talimar_loan`, `lien_holder`, `position`, `intrest_rate`, `supirior`, `original_balance`, `current_balance`, `monthly_payment`, `maturity_date`, `ballon_payment`, `ballon_payment_b`, `set_position`) VALUES ( '".$loan->talimar_loan."' , 'TaliMar Financial' , '".$loan->position."' , '".$loan->intrest_rate."', '0' , '".$loan->loan_amount."'  , '".$loan->current_balance."' , '".$monthly_payment."' , '".$loan->maturity_date."' , '".$loan->baloon_payment_type."' , '".$loan->ballon_payment."' , '0') ";

	// $sql = "UPDATE encumbrances SET ballon_payment_b = '".$ballon_payment_b."' WHERE talimar_loan = '".$loan->talimar_loan."' AND lien_holder = 'TaliMar Financial' ";
	// $this->User_model->query($sql);
	// echo $sql.'<br>';
	// }

	// echo 'All ecumbrances updated';
	// }

	public function add_data_on_ecumbrances($talimar) {
		error_reporting(0);
		$fetch_loan_data = $this->User_model->select_where('loan', array('talimar_loan' => $talimar));
		if ($fetch_loan_data->num_rows() > 0) {
			$fetch_loan_data = $fetch_loan_data->result();
			foreach ($fetch_loan_data as $loan) {

				$impound_amount = ($loan->intrest_rate / 100) * $loan->loan_amount / 12;
				$monthly_payment = $impound_amount;

				$sql = "INSERT INTO encumbrances ( `talimar_loan`, `lien_holder`, `position`, `intrest_rate`,  `original_balance`, `current_balance`, `monthly_payment`, `maturity_date`, `ballon_payment`, `ballon_payment_b`, `set_position`,`instrument`,`recording_date`,`beneficiary_name`,`trustee`) VALUES ( '" . $loan->talimar_loan . "' , 'TaliMar Financial' , '" . $loan->position . "' , '" . $loan->intrest_rate . "',   '" . $loan->loan_amount . "'  , '" . $loan->current_balance . "' , '" . $monthly_payment . "' , '" . $loan->maturity_date . "' , '" . $loan->baloon_payment_type . "' , '" . $loan->ballon_payment . "' , '0', '" . $loan->instrument . "', '" . $loan->recording_date . "', '" . $loan->beneficiary_name . "', '" . $loan->trustee . "') ";
				$this->User_model->query($sql);

			}
		}

		return 1;
	}

	public function delete_ecumbrance_id() {
		$where['id'] = $this->input->post('id');
		$this->User_model->delete('encumbrances', $where);
		echo '1';
	}

	public function delete_for_section() {
		$where['id'] = $this->input->post('ids');
		$where1['add_forclosure_id'] = $this->input->post('ids');

		$this->User_model->delete('add_foreclosure_request', $where);
		$this->User_model->delete('loan_servicing_default', $where1);
		echo '1';
	}

	public function delete_extension_section() {
		$where['id'] = $this->input->post('ids');
		$where1['add_extension_id'] = $this->input->post('ids');

		$this->User_model->delete('add_extension', $where);
		$this->User_model->delete('extension_section', $where1);
		echo '1';
	}

	public function load_ecumbrances_questions_ajax() {
		error_reporting(0);
		$ecumbrance_id = $this->input->post('ecum_id');
		$where['ecumbrance_id'] = $this->input->post('ecum_id');
		$data['ecumbrance_id'] = $this->input->post('ecum_id');
		$fetch_ecumbrances_data = $this->User_model->select_where('encumbrances_data', $where);
		if ($fetch_ecumbrances_data->num_rows() > 0) {
			$data['fetch_encumbrance_data_table'] = $fetch_ecumbrances_data->row();
		}

		$fetch_ecumbrances = $this->User_model->select_where('encumbrances', array('id' => $ecumbrance_id));
		if ($fetch_ecumbrances->num_rows() > 0) {
			$data['fetch_ecumbrances'] = $fetch_ecumbrances->row();
		}

		//fetch ecum contact...
		$fetch_all_contact_ecum = $this->User_model->select_star('contact');
		$data['fetch_all_contact_ecum'] = $fetch_all_contact_ecum->result();

		echo $this->load->view('load_data/load_ecumbrance_data', $data, true);

	}

	public function form_sumbit_ecumbrance_data() {
		$talimar_loan = $this->input->post('talimar_no');
		$loan_id = $this->input->post('loan_id');
		$where['talimar_loan'] = $talimar_loan;
		$where['ecumbrance_id'] = $this->input->post('ecumbrance_id');
		if ($this->input->post('payment_lates') == 0) {
			$insert['payment_lates'] = 0;
			$insert['yes_how_many'] = NULL;
			$insert['payment_unpaid'] = NULL;
			$insert['yes_proceeds_subject_loan'] = NULL;
			$insert['no_source_of_fund'] = NULL;
		} else {
			$insert['payment_lates'] = $this->input->post('payment_lates') ? $this->input->post('payment_lates') : '';
			$insert['yes_how_many'] = $this->input->post('yes_how_many') ? $this->input->post('yes_how_many') : '';
			$insert['payment_unpaid'] = $this->input->post('payment_unpaid') ? $this->input->post('payment_unpaid') : '';
			$insert['yes_proceeds_subject_loan'] = $this->input->post('yes_proceeds_subject_loan') ? $this->input->post('yes_proceeds_subject_loan') : '';
			$insert['no_source_of_fund'] = $this->input->post('no_source_of_fund') ? $this->input->post('no_source_of_fund') : '';
		}

		$fetch_encumbrance_data = $this->User_model->select_where('encumbrances_data', $where);
		if ($fetch_encumbrance_data->num_rows() > 0) {
			$this->User_model->updatedata('encumbrances_data', $where, $insert);
			$status = 'Update';
		} else {
			$insert['talimar_loan'] = $talimar_loan;
			$insert['ecumbrance_id'] = $this->input->post('ecumbrance_id');
			$this->User_model->insertdata('encumbrances_data', $insert);
			$status = 'Insert';
		}

		$ecum_id = $this->input->post('ecumbrance_id');

		$lien_holder = $this->input->post('lien_holder');
		$position = $this->input->post('position');
		$priority = $this->input->post('priority');
		$existing_priority = $this->input->post('existing_priority');
		$existing_priority_to_talimar = $this->input->post('existing_priority_to_talimar');
		$Request_for_Notice_talimar = $this->input->post('Request_for_Notice_talimar');

		$intrest_rate = $this->amount_format($this->input->post('intrest_rate'));
		$original_balance = $this->amount_format($this->input->post('original_balance'));
		$current_balance = $this->amount_format($this->input->post('current_balance'));
		$balance_at_close = $this->amount_format($this->input->post('balance_at_close'));
		$monthly_payment = $this->amount_format($this->input->post('monthly_payment'));
		$maturity_date = $this->input->post('maturity_date');
		if ($maturity_date) {
			//$myDateTime1 = DateTime::createFromFormat('m-d-Y', $maturity_date);
			$newDateString = input_date_format($maturity_date);//$myDateTime1->format('Y-m-d');
			$maturity_date = $newDateString ? $newDateString : '';
		}

		$document_date = $this->input->post('document_date');
		if ($document_date) {

			//$document = DateTime::createFromFormat('m-d-Y', $document_date);
			$new_document = input_date_format($document_date);//$document->format('Y-m-d');
			$my_document_date = $new_document ? $new_document : '';
		}

		$ballon_payment_b = $this->amount_format($this->input->post('ballon_payment_b'));
		$existing_lien = $this->input->post('existing_lien');
		$will_it_remain = $this->input->post('will_it_remain');
		$est_payoff = $this->amount_format($this->input->post('est_payoff'));
		$proposed_priority = $this->input->post('proposed_priority');
		$rate_type = $this->input->post('rate_type');
		$max_rate = $this->amount_format($this->input->post('max_rate'));
		$variable = $this->input->post('variable');
		$current_position = $this->input->post('current_position');
		$senior_to_talimar = $this->input->post('priority_to_talimar');

		$instrument = $this->input->post('instrument');
		$recording_date = $this->input->post('recording_date');

		if ($recording_date) {
			//$myDateTime = DateTime::createFromFormat('m-d-Y', $recording_date);
			$newDateString = input_date_format($recording_date);//$myDateTime->format('Y-m-d');
			$recording_date = $newDateString ? $newDateString : '';
		}

		$beneficiary_name = $this->input->post('beneficiary_name');
		$trustee = $this->input->post('trustee');
		$beneficiary_vesting = $this->input->post('beneficiary_vesting');

		$b_contact_name = $this->input->post('b_contact_name');
		$b_title = $this->input->post('b_title');

		$sql = "UPDATE `encumbrances` SET `lien_holder`= '" . $lien_holder . "', `position`='" . $position . "',`priority`='" . $priority . "', `existing_priority`='" . $existing_priority . "',`existing_priority_to_talimar`='" .
			$existing_priority_to_talimar . "',`request_for_notice_talimar`='" . $Request_for_Notice_talimar . "',`intrest_rate`= '" . $intrest_rate . "', `original_balance`= '" . $original_balance . "',`current_balance`='" . $current_balance . "',`balance_at_close`='" . $balance_at_close . "',`monthly_payment`='" . $monthly_payment . "',`maturity_date`= '" . $maturity_date . "', `ballon_payment_b`='" . $ballon_payment_b . "', `existing_lien`='" . $existing_lien . "', `will_it_remain`='" . $will_it_remain . "',`est_payoff`='" . $est_payoff . "',`proposed_priority`='" . $proposed_priority . "', `rate_type`='" . $rate_type . "', `max_rate`='" . $max_rate . "', `variable`='" . $variable . "', `current_position`='" . $current_position . "', `senior_to_talimar` = '" . $senior_to_talimar . "',`instrument`='" . $instrument . "', `recording_date`='" . $recording_date . "',`beneficiary_name`='" . $beneficiary_name . "',`trustee`='" . $trustee . "',`beneficiary_vesting`='" . $beneficiary_vesting . "',`document_date`='" . $my_document_date . "',`b_contact_name`='" . $b_contact_name . "',`b_title`='" . $b_title . "' WHERE id = '" . $ecum_id . "' ";

		$this->User_model->query($sql);

		$fetch_encumbrances = $this->User_model->select_where('encumbrances', array('id' => $ecum_id));
		if ($fetch_encumbrances->num_rows() == 1) {
			$fetch_encumbrances = $fetch_encumbrances->row();
			$json['property_home_id'] = $fetch_encumbrances->property_home_id;
		}

		$yes_no_option = $this->config->item('yes_no_option');
		$position_option = $this->config->item('position_option');
		// echo $status;
		$json['id'] = $ecum_id;
		$json['status'] = $status;
		$json['lien_holder'] = $lien_holder;
		$json['position'] = $position_option[$current_position];
		$json['will_it_remain'] = $yes_no_option[$will_it_remain];
		$json['proposed_priority'] = $position_option[$proposed_priority];
		$json['original_balance'] = '$' . number_format($original_balance, 2);
		$json['current_balance'] = '$' . number_format($current_balance, 2);
		$json['monthly_payment'] = '$' . number_format($monthly_payment, 2);
		print_r(json_encode($json));
	}

	public function load_ecumbrances_request_notice_ajax() {
		error_reporting(0);
		$where['ecumbrance_id'] = $this->input->post('ecum_id');
		$data['ecumbrance_id'] = $this->input->post('ecum_id');
		$fetch_ecumbrances = $this->User_model->select_where('encumbrances_request_notice', $where);
		if ($fetch_ecumbrances->num_rows() > 0) {
			$data['encumbrance_request_notice'] = $fetch_ecumbrances->row();
		}
		echo $this->load->view('load_data/load_ecumbrance_request_notice', $data, true);
	}

	public function form_sumbit_ecumbrance_request_notice() {
		$talimar_loan = $this->input->post('talimar_no');
		$loan_id = $this->input->post('loan_id');
		$where['talimar_loan'] = $talimar_loan;
		$where['ecumbrance_id'] = $this->input->post('ecumbrance_id');
		$insert['instrument'] = $this->input->post('instrument');
		$insert['recording_date'] = $this->input->post('recording_date');
		$insert['county'] = $this->input->post('county');
		$insert['trustor'] = $this->input->post('trustor');
		$insert['beneficiary'] = $this->input->post('beneficiary');
		$insert['executed_by'] = $this->input->post('executed_by');
		$insert['trustee'] = $this->input->post('trustee');

		$fetch_encumbrance_data = $this->User_model->select_where('encumbrances_request_notice', $where);
		if ($fetch_encumbrance_data->num_rows() > 0) {
			$this->User_model->updatedata('encumbrances_request_notice', $where, $insert);
			$status = 'Update';
		} else {
			$insert['talimar_loan'] = $talimar_loan;
			$insert['ecumbrance_id'] = $this->input->post('ecumbrance_id');
			$this->User_model->insertdata('encumbrances_request_notice', $insert);
			$status = 'Insert';
		}
		echo $status;
	}

	public function test_loan_ecumbrance() {
		// add_data_on_ecumbrances

		$fetch_all_loan = $this->User_model->select_star('loan');
		$fetch_all_loan = $fetch_all_loan->result();
		foreach ($fetch_all_loan as $row) {
			$ecum_cond['talimar_loan'] = $row->talimar_loan;
			$ecum_cond['lien_holder'] = 'TaliMar Financial';
			$fetch_ecumbrances = $this->User_model->select_where('encumbrances', $ecum_cond);
			if ($fetch_ecumbrances->num_rows() == 0) {
				$abc = $this->add_data_on_ecumbrances($row->talimar_loan);
			}
		}
	}

	public function calculate_ecumbrance_total_data() {
		$fetch_all_loan = $this->User_model->select_star('loan');
		$fetch_all_loan = $fetch_all_loan->result();

		foreach ($fetch_all_loan as $loan) {
			$loan_talimar_loan = $loan->talimar_loan;
			$loan_loan_amount = $loan->loan_amount;

			$fetch_ecumbrances = $this->User_model->select_where('encumbrances', array('talimar_loan' => $loan_talimar_loan));

			if ($fetch_ecumbrances->num_rows() > 0) {
				$fetch_ecumbrances = $fetch_ecumbrances->result();

				//  calculate total parts

				// --------------------- start of ecumbrances original balances ---------------------
				// for Total lien to be Paid Off
				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$existing_lien = $ecum->existing_lien;
					$will_it_remain = $ecum->will_it_remain;
					if ($existing_lien == '1' && $will_it_remain == '0') {
						$sum = $sum + $ecum->original_balance;
					}
				}
				$data['e_original_balance_total_paidoff'] = $sum;

				// for Total Liens to Remain:
				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$existing_lien = $ecum->existing_lien;
					$will_it_remain = $ecum->will_it_remain;
					$senior_to_talimar = $ecum->senior_to_talimar;

					if ($existing_lien == '1' && $will_it_remain == '1' && $senior_to_talimar == '0') {
						$sum = $sum + $ecum->original_balance;
					}
				}
				$data['e_total_lien_remain_original'] = $sum;

				// for Total Liens Senior to TaliMar:
				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$senior_to_talimar = $ecum->senior_to_talimar;
					if ($senior_to_talimar == '1') {
						$sum = $sum + $ecum->original_balance;
					}
				}
				$data['e_total_senior_to_talimar_original'] = $sum;

				// Total Liens Senior + TaliMar Loan:
				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$senior_to_talimar = $ecum->senior_to_talimar;
					if ($senior_to_talimar == '1') {
						$sum = $sum + $ecum->original_balance;
					}
				}

				$sum = $sum + $loan_loan_amount;
				$data['e_total_senior_plus_talimar_original'] = $sum;

				// Total Subordinate Liens:
				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$senior_to_talimar = $ecum->senior_to_talimar;
					$lien_holder = $ecum->lien_holder;
					if ($senior_to_talimar == '0' && $lien_holder != 'TaliMar Financial') {
						$sum = $sum + $ecum->original_balance;
					}
				}

				$data['e_total_subordinate_lien_original'] = $sum;
				// ---- End of ecumbrances original balances ---------------------

				// ---- Start totals Monthly Payment ---------------------

				// for Total lien to be Paid Off

				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$existing_lien = $ecum->existing_lien;
					$senior_to_talimar = $ecum->senior_to_talimar;
					$will_it_remain = $ecum->will_it_remain;

					if ($existing_lien == '1' && $will_it_remain == '0') {
						$sum = $sum + $ecum->monthly_payment;
					}
				}

				$data['e_total_monthly_payment_paidoff'] = $sum;

				// for Total Liens to Remain:
				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$existing_lien = $ecum->existing_lien;
					$will_it_remain = $ecum->will_it_remain;
					$senior_to_talimar = $ecum->senior_to_talimar;
					if ($existing_lien == '1' && $will_it_remain == '1' && $senior_to_talimar == '0') {
						$sum = $sum + $ecum->monthly_payment;
					}
				}
				$data['e_total_lien_remain_month'] = $sum;

				// for Total Liens Senior to TaliMar:
				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$senior_to_talimar = $ecum->senior_to_talimar;
					if ($senior_to_talimar == '1') {
						$sum = $sum + $ecum->monthly_payment;
					}
				}

				$data['e_total_senior_to_talimar_month'] = $sum;

				// Total Liens Senior + TaliMar Loan:
				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$senior_to_talimar = $ecum->senior_to_talimar;
					$lien_holder = $ecum->lien_holder;
					if ($senior_to_talimar == '1' || $lien_holder == 'TaliMar Financial') {
						$sum = $sum + $ecum->monthly_payment;
					}
				}
				$data['e_total_senior_plus_talimar_month'] = $sum;

				// Total Subordinate Liens:
				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$senior_to_talimar = $ecum->senior_to_talimar;
					$lien_holder = $ecum->lien_holder;

					if ($senior_to_talimar == '0' && $lien_holder != 'TaliMar Financial') {
						$sum = $sum + $ecum->monthly_payment;
					}
				}

				$data['e_total_subordinate_lien_month'] = $sum;

				// ---------------------End totals Monthly Payment ---------------------

				// ---------------------Start Total Current Balance ---------------------

				// for Total lien to be Paid Off
				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$existing_lien = $ecum->existing_lien;
					$will_it_remain = $ecum->will_it_remain;

					if ($existing_lien == '1' && $will_it_remain == '0') {
						$sum = $sum + $ecum->current_balance;
					}
				}
				$data['e_total_current_balance_paidoff'] = $sum;

				// for Total Liens to Remain:
				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$existing_lien = $ecum->existing_lien;
					$will_it_remain = $ecum->will_it_remain;
					$senior_to_talimar = $ecum->senior_to_talimar;
					if ($existing_lien == '1' && $will_it_remain == '1' && $senior_to_talimar == '0') {
						$sum = $sum + $ecum->current_balance;
					}
				}
				$data['e_total_lien_remain_current'] = $sum;

				// for Total Liens Senior to TaliMar:
				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$senior_to_talimar = $ecum->senior_to_talimar;
					if ($senior_to_talimar == '1') {
						$sum = $sum + $ecum->current_balance;
					}
				}
				$data['e_total_senior_to_talimar_current'] = $sum;

				// Total Liens Senior + TaliMar Loan:
				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$senior_to_talimar = $ecum->senior_to_talimar;

					if ($senior_to_talimar == '1') {
						$sum = $sum + $ecum->current_balance;
					}
				}

				$sum = $sum + $loan_loan_amount;
				$data['e_total_senior_plus_talimar_current'] = $sum;

				// Total Subordinate Liens:
				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$senior_to_talimar = $ecum->senior_to_talimar;
					$lien_holder = $ecum->lien_holder;
					if ($senior_to_talimar == '0' && $lien_holder != 'TaliMar Financial') {
						$sum = $sum + $ecum->current_balance;
					}
				}
				$data['e_total_subordinate_lien_current'] = $sum;

				// ---------End Total Current Balance ---------------------

				// ---------Start Total Balloon payment ---------------------

				// for Total lien to be Paid Off
				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$senior_to_talimar = $ecum->senior_to_talimar;
					$existing_lien = $ecum->existing_lien;
					$will_it_remain = $ecum->will_it_remain;

					if ($existing_lien == '1' && $will_it_remain == '0') {
						$sum = $sum + $ecum->ballon_payment_b;
					}
				}

				$data['e_total_ballon_payment_paidoff'] = $sum;

				// for Total Liens to Remain:
				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$senior_to_talimar = $ecum->senior_to_talimar;
					$existing_lien = $ecum->existing_lien;
					$will_it_remain = $ecum->will_it_remain;
					if ($existing_lien == '1' && $will_it_remain == '1' && $senior_to_talimar == '0') {
						$sum = $sum + $ecum->ballon_payment_b;
					}
				}
				$data['e_total_lien_remain_ballon'] = $sum;

				// for Total Liens Senior to TaliMar:
				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$senior_to_talimar = $ecum->senior_to_talimar;

					if ($senior_to_talimar == '1') {
						$sum = $sum + $ecum->ballon_payment_b;
					}
				}
				$data['e_total_senior_to_talimar_ballon'] = $sum;

				// Total Liens Senior + TaliMar Loan:
				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$senior_to_talimar = $ecum->senior_to_talimar;
					$lien_holder = $ecum->lien_holder;
					if ($senior_to_talimar == '1' || $lien_holder == 'TaliMar Financial') {
						$sum = $sum + $ecum->ballon_payment_b;
					}
				}
				$data['e_total_senior_plus_talimar_ballon'] = $sum;

				// Total Subordinate Liens:
				$sum = 0;
				foreach ($fetch_ecumbrances as $ecum) {
					$senior_to_talimar = $ecum->senior_to_talimar;
					$lien_holder = $ecum->lien_holder;

					if ($senior_to_talimar == '0' && $lien_holder != 'TaliMar Financial') {
						$sum = $sum + $ecum->ballon_payment_b;
					}
				}
				$data['e_total_subordinate_lien_ballon'] = $sum;

				// ---------------------End Total Balloon payment ---------------------

				// End of calculate total part

				$fetch_encumbrance_totals = $this->User_model->select_where('ecumbrances_totals', array('talimar_loan' => $loan_talimar_loan));
				if ($fetch_encumbrance_totals->num_rows() > 0) {
					$this->User_model->updatedata('ecumbrances_totals', array('talimar_loan' => $loan_talimar_loan), $data);
				} else {
					$data['talimar_loan'] = $loan_talimar_loan;
					$this->User_model->insertdata('ecumbrances_totals', $data);
				}

			}

			$all_ecum_total[$loan->talimar_loan] = $data;
		}

		// echo '<pre>';
		// print_r($all_ecum_total);
		// echo '</pre>';
	}

	public function load_loan_source_and_uses_items($talimar_loan) {
		$items_array = array('Purchase Price', 'Construction Costs', 'Property Insurance', 'Property Taxes', 'Utilities', 'Interest Payments', 'Lender Fees', 'Escrow / Title Closing Costs', 'HOA Dues', 'Broker Fee', 'Contingency');

		foreach ($items_array as $row) {
			$data['talimar_loan'] = $talimar_loan;
			$data['items'] = $row;
			$data['project_cost'] = '';
			$data['borrower_funds'] = '';
			$data['loan_funds'] = '';
			$this->User_model->insertdata('loan_source_and_uses', $data);
		}

		return 1;

	}

	public function add_contact_notes() {

		$loan_id = str_replace("/", "", $this->input->post('loan_id'));
		$talimar_loan = $this->input->post('talimar_no');
		$today_date = $this->input->post('today_date');
		$contact_id = $this->input->post('contact_id');
		$contact_note = $this->input->post('contact_note');
		$entry_by = $this->input->post('entry_by');
		$note_type = $this->input->post('note_type');
		$com_type = $this->input->post('com_type');

		//$myDateTime = DateTime::createFromFormat('m-d-Y', $today_date);
		//$new_date = $myDateTime->format('Y-m-d');
		$new_date = input_date_format($today_date);

		$data['loan_id'] = $loan_id;
		$data['notes_date'] = $new_date;
		$data['contact_id'] = $contact_id;
		$data['notes_text'] = $contact_note;
		$data['user_id'] = $this->session->userdata('t_user_id');
		$data['entry_by'] = $entry_by;
		$data['loan_note_type'] = $note_type;
		$data['com_type'] = $com_type;

		$this->session->set_flashdata('popup_modal_cv', array('loan_notes', 'add_contact_note'));
		// $this->User_model->insertdata('contact_notes', $data);
		$this->User_model->insertdata('contact_loan_notes', $data);
		$this->session->set_flashdata('success', 'Loan Contact Notes Added Successfully!');
		redirect(base_url() . 'load_data/' . $loan_id);

	}

	public function fetch_notes_details() {

		$id = $this->input->post('id');

		// $select_notes = $this->User_model->query("SELECT * FROM `contact_notes` WHERE id = '" . $id . "'");

		$select_notes = $this->User_model->query("SELECT * FROM `contact_loan_notes` WHERE id = '" . $id . "'");
		$select_notes = $select_notes->result();

		foreach ($select_notes as $row) {

			$data['id'] = $row->id;
			$data['contact_id'] = $row->contact_id;
			$data['notes_date'] = date('m-d-Y', strtotime($row->notes_date));
			$data['notes_text'] = $row->notes_text;
			$data['entry_by'] = $row->entry_by;
			$data['loan_note_type'] = $row->loan_note_type;
			$data['com_type'] = $row->com_type;
		}

		echo json_encode($data);

	}

	public function edit_loan_contact_notes() {

		error_reporting(0);
		$loan_id = $this->input->post('loan_id');
		$loan_id_new = str_replace("/", "", $loan_id);
		$notes_date = $this->input->post('notes_date');
		$contact_id = $this->input->post('contact_id');
		$notes_text = $this->input->post('notes_text');
		$updata_id = $this->input->post('updata_id');
		$user_id = $this->session->userdata('t_user_id');
		$entry_by = $this->input->post('entry_by');
		$loan_note_type = $this->input->post('note_type');
		$com_type = $this->input->post('com_type');

		//$myDateTime = DateTime::createFromFormat('m-d-Y', $notes_date);
		//$new_date = $myDateTime->format('Y-m-d');
		$new_date = input_date_format($notes_date);
		//echo $new_date;

		// $sql = "UPDATE contact_notes SET contact_id = '" . $contact_id . "', notes_date = '" . $new_date . "', notes_type = null, loan_id = '" . $loan_id_new . "', notes_text = '" . $notes_text . "', user_id='" . $user_id . "', entry_by='" . $entry_by . "', loan_note_type='" . $loan_note_type . "', com_type='" . $com_type . "' WHERE id = '" . $updata_id . "'";

		$sql = "UPDATE contact_loan_notes SET contact_id = '" . $contact_id . "', notes_date = '" . $new_date . "', notes_type = null, loan_id = '" . $loan_id_new . "', notes_text = '" . $notes_text . "', user_id='" . $user_id . "', entry_by='" . $entry_by . "', loan_note_type='" . $loan_note_type . "', com_type='" . $com_type . "' WHERE id = '" . $updata_id . "'";

		$fetch_note = $this->User_model->query($sql);
		//$fetch_note = $fetch_note->result();
		$this->session->set_flashdata('success', 'Loan Contact Notes Updated Successfully!');
		redirect(base_url() . 'load_data' . $loan_id);

	}

	public function form_loan_source_and_uses() {

		/*echo '<pre>';
		print_r($_POST);
		echo '</pre>';

		die('ddd');*/

		$loan_id = $this->input->post('loan_id');
		$talimar_loan = $this->input->post('talimar_loan');

		$id_data = $this->input->post('id');
		$project_cost = $this->input->post('project_cost');
		$borrower_funds = $this->input->post('borrower_funds');
		$loan_funds = $this->input->post('loan_funds');
		$items = $this->input->post('items');

		
		foreach ($id_data as $key => $id) {	
			$calculate_borrower_fund = 0;
			if($this->amount_format($project_cost[$key]) > 0 && $this->amount_format($loan_funds[$key]) > 0){
				$calculate_borrower_fund = $this->amount_format($project_cost[$key]) - $this->amount_format($loan_funds[$key]);
			}		
			
			$data['items'] = $items[$key];
			$data['project_cost'] = $this->amount_format($project_cost[$key]);
			$data['borrower_funds'] = $this->amount_format($borrower_funds[$key]);//$this->amount_format($calculate_borrower_fund);
			$data['loan_funds'] = $this->amount_format($loan_funds[$key]);
			if ($id == 'new') {
				if ($items[$key]) {
					$data['talimar_loan'] = $talimar_loan;
					$this->User_model->insertdata('loan_source_and_uses', $data);
				}
				// for insert new data
			} else {
				$where['id'] = $id;
				$this->User_model->updatedata('loan_source_and_uses', $where, $data);
			}
		}

		//Update table Active or not.... 
		$activate_source_and_users_table = $this->input->post('activate_source_and_users_table');
		$dataactive['activate_table'] = $activate_source_and_users_table;
		$whereactive['talimar_loan'] = $talimar_loan;
		$this->User_model->updatedata('loan_source_and_uses', $whereactive, $dataactive);

		$this->session->set_flashdata('popup_modal_cv', array('construcion_type_sources_and_uses'));
		$this->session->set_flashdata('popup_modal_hit', 'construcion_type_sources_and_uses');
		$this->session->set_flashdata('success', 'Sources & Uses Updated Successfully!');
		redirect(base_url() . 'load_data' . $loan_id);
	}

	public function delete_comparable_sale_id() {
		$delete['id'] = $this->input->post('comp_id');
		$this->User_model->delete('comparable_sale', $delete);
		echo 'deleted';
	}

	public function borrower_data_loading() {
		error_reporting(0);
		$borrower_id = $this->input->post('borrower_id');
		if ($borrower_id) {
			$id['id'] = $borrower_id;
			$fetch_borrower_id_data = $this->User_model->select_where('borrower_data', $id);
			$fetch_borrower_id_data = $fetch_borrower_id_data->result();
			$data['fetch_borrower_id_data'] = $fetch_borrower_id_data;

			$contact_id['borrower_id'] = $borrower_id;
			$borrower_contact_data = $this->User_model->select_where('borrower_contact', $contact_id);
			if ($borrower_contact_data->num_rows() > 0) {
				$borrower_contact_data = $borrower_contact_data->result();
				$data['borrower_contact_data'] = $borrower_contact_data;
			}

			$sql_paidoff = "SELECT COUNT(*) as count_loan , SUM(loan.loan_amount) as total_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '3' AND loan.borrower = '" . $borrower_id . "' ";
			$fetch_paidoff_loan = $this->User_model->query($sql_paidoff);
			if ($fetch_paidoff_loan->num_rows() > 0) {
				$fetch_paidoff_loan = $fetch_paidoff_loan->result();
				$data['fetch_paidoff_loan'] = array(
					'count_loan' => $fetch_paidoff_loan[0]->count_loan,
					'total_loan_amount' => $fetch_paidoff_loan[0]->total_loan_amount,
				);
			}

			$sql_active = "SELECT COUNT(*) as count_loan , SUM(loan.loan_amount) as total_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '2' AND loan.borrower = '" . $borrower_id . "' ";
			$fetch_active_loan = $this->User_model->query($sql_active);
			if ($fetch_active_loan->num_rows() > 0) {
				$fetch_active_loan = $fetch_active_loan->result();
				$data['fetch_active_loan'] = array(
					'count_loan' => $fetch_active_loan[0]->count_loan,
					'total_loan_amount' => $fetch_active_loan[0]->total_loan_amount,
				);
			}

		}
		$user = array();
		$user['t_user_id'] = $this->session->userdata('t_user_id');
		// $result 					= $this->User_model->select_where('borrower_contact',$user);
		// $result_new				= $result->result();
		// $data['fetch_result']	= $result_new;

		$where1['borrower_contact_type'] = 1;
		// $fetch_all_contact 					= $this->User_model->select_where_asc('contact',$where1,'contact_firstname');
		$fetch_all_contact = $this->User_model->select_star('contact');
		$data['fetch_all_contact'] = $fetch_all_contact->result();
		//-------------------Fetch borrower data_dable ---------------------------

		$all_borrower_data = $this->User_model->select_star('borrower_data');
		$all_borrower_data = $all_borrower_data->result();
		$data['all_borrower_data'] = $all_borrower_data;

		$data['a'] = '1';
		$content = $this->load->view('borrower_data/index', $data);
		$content .= '<style>
		div#Borrower_modal div#load-borrower-content div.row.borrower_data_row{padding:3px 6px;} div#Borrower_modal div#load-borrower-content div.page-head{display:none;}  div#Borrower_modal div#load-borrower-content div#bordered_div_section{margin:0px;}  div#Borrower_modal div#load-borrower-content .row.padding_200{padding:0px 3px;}
		div#Borrower_modal div#load-borrower-content input[type="button"]{
			display:none;
		}
		div#Borrower_modal div#load-borrower-content input[type="submit"]{
			display:none;
		}
		div#Borrower_modal div#load-borrower-content button{
			display:none;
		}
		div#Borrower_modal div#load-borrower-content a{
			display:none;
		}
			</style>';
		$content .= '<script>';
		$content .= "$('div#Borrower_modal div#load-borrower-content form#formID input').attr('disabled',true);";
		$content .= "$('div#Borrower_modal div#load-borrower-content form#formID select').attr('disabled',true);";
		$content .= "$('div#Borrower_modal div#load-borrower-content form#formID button').attr('disabled',true);";
		$content .= "$('div#Borrower_modal div#load-borrower-content form#formID a').attr('disabled',true);";
		$content .= "$('div#Borrower_modal div#load-borrower-content form#formID textarea').attr('disabled',true);";
		$content .= '</script>';
		echo $content;

	}

	public function delete_additional_loan() {
		$id = $this->input->post('id');
		$this->User_model->delete('additional_loan', array('id' => $id));
		echo 1;
	}

	public function print_lender_pdf() {
		$loan_id = $this->uri->segment(2);
		$sql = "SELECT talimar_loan,loan_amount,current_balance,intrest_rate FROM `loan` WHERE `id`='" . $loan_id . "'";
		$talimar_loans = $this->User_model->query($sql);
		$talimar_loans = $talimar_loans->row();
		$talimar_loan = $talimar_loans->talimar_loan;
		$loan_amount = $talimar_loans->loan_amount;
		$current_balance = $talimar_loans->current_balance;
		$intrest_rate = number_format($talimar_loans->intrest_rate,3);

		//echo $talimar_loan;
		$sql = "SELECT * FROM `loan_assigment` WHERE `talimar_loan` = '" . $talimar_loan . "' ORDER BY `position` ASC";
		$loan_assigment_data = $this->User_model->query($sql);
		$loan_assigment_data = $loan_assigment_data->result();

		//echo $talimar_loan;
		$sql11 = "SELECT * FROM `loan_property` WHERE `talimar_loan` = '" . $talimar_loan . "'";
		$loan_property = $this->User_model->query($sql11);
		$loan_property = $loan_property->result();

		$property_address = $loan_property[0]->property_address.' '.$loan_property[0]->unit . '; ' .$loan_property[0]->city .', ' . $loan_property[0]->state . ' ' . $loan_property[0]->zip;

		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);

		// set default header data
		// $pdf->SetHeaderData("include/icons/logo-light.png", 30, '', '');

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(2, 20, 2);
		// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
		$pdf->SetFont('times', '', 7);

		// add a page
		$pdf->AddPage('L', 'A4');
		// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
		$header = '<h1 style="color:#003468;font-size:18px;text-align:center;">LENDER SCHEDULE</h1><br>';
		$header .= '<span style="font-size:13px;"><strong>Property: </strong>' . $property_address . '</span><br><br>';
		$header .= '<span style="font-size:13px;"><strong>Loan Amount: </strong>$' . number_format($loan_amount, 2) . '</span><br><br>';
		$header .= '<span style="font-size:13px;"><strong>Outstanding Balance: </strong>$' . number_format($current_balance, 2) . '</span><br><br>';
		$header .= '<span style="font-size:13px;"><strong>Note Rate: </strong>' . number_format($intrest_rate, 3) . '%</span><br>';

		$header .= '<p style="font-size:13px;"></p>';

		$header .= '<style>
						.table {
							width: 100%;
							max-width: 100%;
							margin-bottom: 20px;
							border-collapse:collapse;
						}
						.table-bordered {
						border: 1px solid #ddd;
						}
						table {
							border-spacing: 0;
							border-collapse: collapse;
						}

						.th_text_align_center th {
							text-align: left;
						}
						.th_text_align_center td {
							text-align: left;
						}

						.td_large{
							width:17%;
						}
						.small_td{
							width:4%;
						}

						.td_medium{
							width:5%;
						}
						tr.dark_border th{
							border: 1px solid gray;
						}
						.table td{
							font-size:12px;
							font-family: "Times New Roman", Georgia, Serif;
							height:25px;
						}
						.table th{
							height : 20px;
							font-size : 12px;
						}

						tr.table_header th{
							font-size:12px;
							font-family: "Times New Roman", Georgia, Serif;
							text-align:center;
							background-color:#bfcfe3;
						}

						tr.table_bottom th{
							font-size:12px;
							font-family: "Times New Roman", Georgia, Serif;
							text-align:left;
							background-color:#bfcfe3;
						}
						tr.odd td{
							background-color:#ededed;
						}
						</style>
						';

		$header .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';

		$header .= '<tr class="table_header">';

		$header .= '<th  style="width:20%;text-decoration:underline;"><strong>Lender Name</strong></th>';

		$header .= '<th  style="width:15%;text-decoration:underline;"><strong>Contact Name</strong></th>';

		$header .= '<th  style="width:10%;text-decoration:underline;"><strong>Contact Number</strong></th>';

		$header .= '<th  style="width:15%;text-decoration:underline;"><strong>E-mail</strong></th>';

		$header .= '<th  style="width:10%;text-decoration:underline;"><strong>Investment $</strong></th>';

		$header .= '<th  style="width:10%;text-decoration:underline;"><strong>Investment %</strong></th>';

		$header .= '<th  style="width:10%;text-decoration:underline;"><strong>Net Rate %</strong></th>';

		$header .= '<th  style="width:10%;text-decoration:underline;"><strong>Net Payment $</strong></th>';

		//$header .= '<th  style="width:7%;text-decoration:underline;"><strong>Start Date</strong></th>';

		//$header .= '<th  style="width:9%;text-decoration:underline;"><strong>Recorded <br>on DOT.</strong></th>';

		$header .= '</tr>';

		$count = 0;
		$investment = 0;
		$percent_loan = 0;
		$payment = 0;
		foreach ($loan_assigment_data as $key => $row) {
			// echo'<pre>';
			// print_r($row);
			// echo'</pre>';

			$sql = "SELECT `name` FROM `investor` WHERE id='" . $row->lender_name . "'";
			$lender_name = $this->User_model->query($sql);
			$lender_name = $lender_name->row();
			$lender_name = $lender_name->name;

			$sql1 = "SELECT * FROM `lender_contact` WHERE `lender_id`='" . $row->lender_name . "'";
			$fetch_sql1 = $this->User_model->query($sql1);
			$fetch_res = $fetch_sql1->result();

			$contact_id = $fetch_res[0]->contact_id;

			$sql11 = "SELECT * FROM `contact` WHERE `contact_id`='" . $contact_id . "'";
			$fetch_sql1 = $this->User_model->query($sql11);
			$fetch_resd = $fetch_sql1->result();
			$contact_1 = $fetch_resd[0]->contact_firstname;
			$contact_2 = $fetch_resd[0]->contact_lastname;
			$contact_name = $contact_1 . ' ' . $contact_2;
			$contact_phone = $fetch_resd[0]->contact_phone;
			$contact_email = $fetch_resd[0]->contact_email;

			$servicing_lender_rate = $this->User_model->query("SELECT servicing_lender_rate FROM loan_servicing WHERE talimar_loan = '" . $row->talimar_loan . "'");
			$servicing_lender_rate = $servicing_lender_rate->result();
			$servicing_lender_rate_val = $servicing_lender_rate[0]->servicing_lender_rate;

			if ($servicing_lender_rate_val == 'NaN' || $servicing_lender_rate_val == '' || $servicing_lender_rate_val == '0.00') {

				$lender_ratessss = number_format($row->invester_yield_percent,3);
			} else {

				$lender_ratessss = number_format($servicing_lender_rate_val,3);
			}

			$secured_dot = '';
			if ($row->secured_dot == '1') {
				$secured_dot = 'Yes';
			} else {
				$secured_dot = 'No';
			}

			if ($key % 2 == 0) {
				$class_add = "even";
			} else if ($key == 0) {
				$class_add = "even";
			} else {
				$class_add = "odd";
			}

			$header .= '<tr class="' . $class_add . '">';

			$header .= '<td>' . $lender_name . '</td>';
			$header .= '<td>' . $contact_name . '</td>';
			$header .='<td>'.$contact_phone.'</td>';
			$header .='<td>'.$contact_email.'</td>';
			$header .= '<td>$' . number_format($row->investment, 2) . '</td>';
			$header .= '<td>' . number_format($row->percent_loan, 2) . '%</td>';
			$header .= '<td>' . number_format($lender_ratessss,3) . '%</td>';
			$header .= '<td>$' . number_format($row->payment, 2) . '</td>';
			//$header .='<td>'.$row->start_date.'</td>';
			//$header .='<td>'.$secured_dot.'</td>';

			$header .= '</tr>';

			$count++;
			$investment += $row->investment;
			$percent_loan += $row->percent_loan;
			$payment += $row->payment;

		}

		$header .= '<tr class="table_bottom">';
		$header .= '<th><strong>Total: ' . $count . '</strong></th>';
		$header .= '<th></th>';
		$header .= '<th></th>';
		$header .= '<th></th>';
		$header .= '<th><strong>$' . number_format($investment, 2) . '</strong></th>';
		$header .= '<th><strong></strong></th>';
		$header .= '<th></th>';
		$header .= '<th><strong>$' . number_format($payment, 2) . '</strong></th>';
		//$header .= '<th><strong></strong></th>';
		$header .= '</tr>';

		$header .= '<tr class="table_bottom">';
		$header .= '<th><strong>Average:</strong></th>';
		$header .= '<th></th>';
		$header .= '<th></th>';
		$header .= '<th></th>';
		$header .= '<th><strong><strong>$' . number_format($investment / $count, 2) . '</strong></strong></th>';
		$header .= '<th><strong>' . number_format($percent_loan) . '%</strong></th>';
		$header .= '<th></th>';
		$header .= '<th><strong>$' . number_format($payment / $count, 2) . '</strong></th>';
		//$header .= '<th><strong></strong></th>';
		$header .= '</tr>';

		$header .= '</table>';

		$pdf->WriteHTML($header);

		$pdf->Output('Lender_schedule.pdf', 'D');

	}

	public function wire_instructions() {
		error_reporting(0);
		header("Cache-Control: "); // leave blank to avoid IE errors
		header("Pragma: "); // leave blank to avoid IE errors
		header("Content-type: application/octet-stream");
		// header("Content-type: application/vnd.ms-word");

		header("content-disposition: attachment;filename=TaliMar_wire_instructions.doc");
		echo '<html xmlns:v="urn:schemas-microsoft-com:vml"
				xmlns:o="urn:schemas-microsoft-com:office:office"
				xmlns:w="urn:schemas-microsoft-com:office:word"
				xmlns="http://www.w3.org/TR/REC-html40">

				<head>
				<meta http-equiv=Content-Type content="text/html; charset=utf-8">
				<meta name=ProgId content=Word.Document>
				<meta name=Generator content="Microsoft Word 9">
				<meta name=Originator content="Microsoft Word 9">
				<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->
				<title>title</title>
				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				body{
					font-size:10pt;
				}
				</style>
				</head>
				<body>';

		echo '<style>

		table.table{
			width:100%;
			padding : 3px 0px;
		}
		table.table th{
			text-align : left;
			font-size : 10pt;

		}
		table.table td{
			font-size:10pt;
			font-size : 10pt;

		}
		table.two_contact_signer td{
			margin : 3px;
			font-size:10pt;
		}
		h1{
			text-align:center;
		}
		h2{
			text-align:center;
			font-family: Times New Roman;
			font-size: 18px;
		}
		p{
			font-size:10pt;
			word-spacing : 10px;
			padding : 2px 0px;

		}
		table{
			font-size:10pt;
		}
		</style>';

		$loan_id = $this->uri->segment(3);

		//fetch loan table data;

		$where['id'] = $loan_id;
		$fetch_loan = $this->User_model->select_where('loan', $where);
		$fetch_loan = $fetch_loan->result();
		$talimar_loan = $fetch_loan[0]->talimar_loan;

		//fetch loan_property table data...
		$loan_property = $this->User_model->query("select * from loan_property where talimar_loan = '" . $talimar_loan . "'");
		$loan_property = $loan_property->result();
		$street_address = $loan_property[0]->property_address;
		$unit = $loan_property[0]->unit;
		$city = $loan_property[0]->city;
		$state = $loan_property[0]->state;
		$zip = $loan_property[0]->zip;

		//fetch loan_assigment table data...

		$sql = "select * from loan_assigment as la JOIN wire_instruction as wi ON la.talimar_loan = wi.talimar_loan where la.talimar_loan = '" . $talimar_loan . "'";

		$loan_assigment = $this->User_model->query($sql);

		if ($loan_assigment->num_rows() > 0) {

			$loan_assigment = $loan_assigment->result();

		} else {

			$sql_2 = "select * from  wire_instruction  where talimar_loan = '" . $talimar_loan . "'";
			$loan_assigment_2 = $this->User_model->query($sql_2);

			$loan_assigment = $loan_assigment_2->result();
		}

		// echo '<pre>';
				// 			print_r($loan_assigment);

		// echo '</pre>';

		foreach ($loan_assigment as $row) {

			$lender_name = $row->lender_name;
			$talimar_loan = $row->talimar_loan;

			$investor = $this->User_model->query("select * from investor where id = '" . $lender_name . "'");
			$investor = $investor->result();
			$investor_name = $investor[0]->name;

			$loan_property = $this->User_model->query("select * from loan_property where talimar_loan = '" . $talimar_loan . "'");
			$loan_property = $loan_property->result();

			$l_street_address = $loan_property[0]->property_address;
			$l_unit = $loan_property[0]->unit;
			$l_city = $loan_property[0]->city;
			$l_state = $loan_property[0]->state;
			$l_zip = $loan_property[0]->zip;

			$loan_title = $this->User_model->query("select * from loan_title where talimar_loan = '" . $talimar_loan . "'");
			$loan_title = $loan_title->result();
			$loan_title = $loan_title[0]->title_order_no;

			$loan = $this->User_model->query("select * from loan where talimar_loan = '" . $talimar_loan . "'");
			$loan = $loan->result();
			$borrower = $loan[0]->borrower;

			$borrower_data = $this->User_model->query("select * from borrower_data where id = '" . $borrower . "'");
			$borrower_data = $borrower_data->result();
			$borrower_name = $borrower_data[0]->b_name;

			$invetment = $row->investment;
			$bank_name = $row->bank_name;
			$bank_address = $row->bank_street . '' . $row->bank_unit . '; ' . $row->bank_city . ' ' . $row->bank_state . ' ' . $row->bank_zip;

			$recipient_name = $row->recipient_name;
			$recipient_phone = $row->recipient_phone_no;
			$recipient_address = $row->recipient_street . '' . $row->recipient_unit . '; ' . $row->recipient_city . ' ' . $row->recipient_state . ' ' . $row->recipient_zip;

			$routing_number = $row->routing_number;
			$account_number = $row->account_number;
			$credit_to = $row->credit_to;

			$all_wire_instruction[] = array(

				"investor_name" => $investor_name,
				"invetment" => $invetment,
				"bank_name" => $bank_name,
				"bank_address" => $bank_address,
				"recipient_name" => $recipient_name,
				"recipient_phone" => $recipient_phone,
				"recipient_address" => $recipient_address,
				"routing_number" => $routing_number,
				"account_number" => $account_number,
				"credit_to" => $credit_to,
				"loan_title" => $loan_title,
				"borrower_name" => $borrower_name,
				"l_street_address" => $l_street_address,
				"l_unit" => $l_unit,
				"l_city" => $l_city,
				"l_state" => $l_state,
				"l_zip" => $l_zip,

			);

		}

		$wire_instruction_res = $all_wire_instruction;

		$wire_instructions = '';

		$wire_instructions .= '<table style="width:100%;font-size:8pt;">';
		$wire_instructions .= '<tr>';
		$wire_instructions .= '<td style="width:50%;margin-left:50px;"><img src="' . base_url() . 'assets/admin/layout4/img/logo-light.png" ></td>';
		$wire_instructions .= '<td style="width:50%;color:gray;margin-right:50px;"><p style="text-align:right;font-size:8pt;">TaliMar Financial, Inc.<br>16880 West Bernardo Drive, Suite #140<br> San Diego, CA 92127<br>Tel:858.613.0111<br>Toll Free:888.868.8467<br>www.talimarfinancial.com</p></td>';
		$wire_instructions .= '</tr>';
		$wire_instructions .= '</table>';

		$wire_instructions .= '<div style="margin:50px 50px 0px; 50px;font-size:10pt;">';

		$wire_instructions .= '<h2 style="font-family:Times New Roman;font-size:18px;">Wire Instructions</h2>';

		$wire_instructions .= '<table class="table" style="width:100%; vertical-align:top;">';

		$wire_instructions .= '<tr>';
		$wire_instructions .= '<td><strong>Date:</strong> ' . date('m-d-Y') . '</td>';
		$wire_instructions .= '</tr>';

		$wire_instructions .= '<tr>';
		$wire_instructions .= '<td><strong>Loan:</strong> ' . $fetch_loan[0]->talimar_loan . '</td>';
		$wire_instructions .= '</tr>';

		$wire_instructions .= '<tr>';
		$wire_instructions .= '<td><strong>Property Address:</strong> ' . $street_address . '' . $unit . '; ' . $city . ' ' . $state . ' ' . $zip . '</td>';
		$wire_instructions .= '</tr>';

		$wire_instructions .= '</table>';

		$wire_instructions .= '<hr>';

		//foreach($wire_instruction_res as $row){

		$wire_instructions .= '<table class="table" style="width:100%;vertical-align:top;">';

		// $wire_instructions 	 .= '<tr>';
		// $wire_instructions 	 .= '<td><strong>Lender Name:</strong></td>';
		// $wire_instructions 	 .= '<td>'.$row['investor_name'].'</td>';
		// $wire_instructions 	 .= '</tr>';

		// $wire_instructions 	 .= '<tr>';
		// $wire_instructions 	 .= '<td><strong>Lender Investment:</strong></td>';
		// $wire_instructions 	 .= '<td>$'.number_format($row['invetment']).'</td>';
		// $wire_instructions 	 .= '</tr>';

		$wire_instructions .= '<tr>';
		$wire_instructions .= '<td><strong>Bank Name:</strong></td>';
		$wire_instructions .= '<td>' . $wire_instruction_res[0]['bank_name'] . '</td>';
		$wire_instructions .= '</tr>';

		$wire_instructions .= '<tr>';
		$wire_instructions .= '<td><strong>Bank Address:</strong></td>';
		$wire_instructions .= '<td>' . $wire_instruction_res[0]['bank_address'] . '</td>';
		$wire_instructions .= '</tr>';

		$wire_instructions .= '<tr>';
		$wire_instructions .= '<td><strong>Recipient Name:</strong></td>';
		$wire_instructions .= '<td>' . $wire_instruction_res[0]['recipient_name'] . '</td>';
		$wire_instructions .= '</tr>';

		$wire_instructions .= '<tr>';
		$wire_instructions .= '<td><strong>Recipient Address:</strong></td>';
		$wire_instructions .= '<td>' . $wire_instruction_res[0]['recipient_address'] . '</td>';
		$wire_instructions .= '</tr>';

		$wire_instructions .= '<tr>';
		$wire_instructions .= '<td><strong>Recipient Phone:</strong></td>';
		$wire_instructions .= '<td>' . $wire_instruction_res[0]['recipient_phone'] . '</td>';
		$wire_instructions .= '</tr>';

		$wire_instructions .= '<tr>';
		$wire_instructions .= '<td><strong>Account Number:</strong></td>';
		$wire_instructions .= '<td>' . $wire_instruction_res[0]['account_number'] . '</td>';
		$wire_instructions .= '</tr>';

		$wire_instructions .= '<tr>';
		$wire_instructions .= '<td><strong>Wire Routing Number:</strong></td>';
		$wire_instructions .= '<td>' . $wire_instruction_res[0]['routing_number'] . '</td>';
		$wire_instructions .= '</tr>';

		$wire_instructions .= '<tr>';
		$wire_instructions .= '<td><strong>Further Credit:</strong></td>';
		$wire_instructions .= '<td>' . $wire_instruction_res[0]['credit_to'] . '</td>';
		$wire_instructions .= '</tr>';

		$wire_instructions .= '<tr>';
		$wire_instructions .= '<td style="vertical-align:top;"><strong>Reference:</strong></td>';
		$wire_instructions .= '<td>' . $wire_instruction_res[0]['borrower_name'] . '<br>' . $wire_instruction_res[0]['l_street_address'] . '' . $wire_instruction_res[0]['l_unit'] . '; ' . $wire_instruction_res[0]['l_city'] . ' ' . $wire_instruction_res[0]['l_state'] . ' ' . $wire_instruction_res[0]['l_zip'] . '</td>';
		$wire_instructions .= '</tr>';

		$wire_instructions .= '<tr>';
		$wire_instructions .= '<td><strong>Title #:</strong></td>';
		$wire_instructions .= '<td>' . $wire_instruction_res[0]['loan_title'] . '</td>';
		$wire_instructions .= '</tr>';

		$wire_instructions .= '</table>';

		//}

		$wire_instructions .= '<hr>';

		$wire_instructions .= '<p><strong>To reduce the risk of wire fraud, we highly recommend that you contact ' . $wire_instruction_res[0]['recipient_name'] . ' at  ' . $wire_instruction_res[0]['recipient_phone'] . '  to confirm the wire instructions.</strong></p>';

		$wire_instructions .= '<p>Once you have submitted your wire request, please send a confirmation e-mail to&nbsp;<span style="color:blue;">servicing@talimarfinancial.com</span></p>';
		$wire_instructions .= '<p>Should you have any questions regarding the wire, please contact Brock VandenBerg at (858) 613-0111.</p>';

		$wire_instructions .= '</div>';

		//$wire_instructions .= '<br style="page-break-before:always">';
		echo $wire_instructions;

	}


	public function Printforclouserdata(){

		error_reporting(0);
		header("Cache-Control: "); // leave blank to avoid IE errors
		header("Pragma: "); // leave blank to avoid IE errors
		header("Content-type: application/octet-stream");
		// header("Content-type: application/vnd.ms-word");

		header("content-disposition: attachment;filename=TaliMar_Foreclosure.doc");
		echo '<html xmlns:v="urn:schemas-microsoft-com:vml"
				xmlns:o="urn:schemas-microsoft-com:office:office"
				xmlns:w="urn:schemas-microsoft-com:office:word"
				xmlns="http://www.w3.org/TR/REC-html40">

				<head>
				<meta http-equiv=Content-Type content="text/html; charset=utf-8">
				<meta name=ProgId content=Word.Document>
				<meta name=Generator content="Microsoft Word 9">
				<meta name=Originator content="Microsoft Word 9">
				<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->
				<title>title</title>
				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				body{
					font-size:10pt;
				}
				</style>
				</head>
				<body>';

		echo '<style>

				table.table{
					width:100%;
					padding : 3px 0px;
				}
				table.table th{
					text-align : left;
					font-size : 10pt;

				}
				table.table td{
					font-size:10pt;
					font-size : 10pt;

				}
				table.two_contact_signer td{
					margin : 3px;
					font-size:10pt;
				}
				h1{
					text-align:center;
				}
				h2{
					text-align:center;
					font-family: Times New Roman;
					font-size: 18px;
				}
				p{
					font-size:10pt;
					word-spacing : 10px;
					padding : 2px 0px;

				}
				table{
					font-size:10pt;
				}
		</style>';

		$foreclosure_update = '';
		$foreclosure_update .= '<table style="width:100%;vertical-align:top;font-size:8pt;">';
		$foreclosure_update .= '<tr>';
		$foreclosure_update .= '<td style="width:50%;"><img src="' . base_url() . 'assets/admin/layout4/img/logo-light.png" ></td>';
		$foreclosure_update .= '<td style="width:50%;color:gray;"><p style="text-align:right;font-size:8pt;">TaliMar Financial, Inc.<br>16880 West Bernardo Drive, Suite #140<br>#210 San Diego, CA 92127<br>Tel:858.613.0111<br>Toll Free:888.868.8467<br>www.talimarfinancial.com</p></td>';
		$foreclosure_update .= '</tr>';
		$foreclosure_update .= '</table>';

		$foreclosure_update .= '<table style="font-size:12pt;width:100%;color:white;background:#003468;font-family:Calibri, sans-serif;">';
		$foreclosure_update .= '<tr>';
		$foreclosure_update .= '<td style="font-size:12pt;text-align:center;">Foreclosure Costs</td>';
		$foreclosure_update .= '</tr>';
		$foreclosure_update .= '</table>';


		$loan_id = $this->uri->segment(3);

		$fetchaddress = $this->User_model->query("SELECT `property_address`,`unit`,`city`,`state`,`zip` FROM `loan_property` WHERE `loan_id`='".$loan_id."'");
		$fetchaddress = $fetchaddress->result();
		$full_address = $fetchaddress[0]->property_address.''.$fetchaddress[0]->unit.'; '.$fetchaddress[0]->city.', '.$fetchaddress[0]->state.' '.$fetchaddress[0]->zip;

		$fetchborrowername = $this->User_model->query("SELECT bd.b_name FROM loan as l JOIN borrower_data as bd ON l.borrower = bd.id WHERE l.id = '".$loan_id."'");
		$fetchborrowername = $fetchborrowername->result();
		$b_name = $fetchborrowername[0]->b_name;

		$feth_foreclouserdata = $this->User_model->query("SELECT * FROM `foreclosure_cost` WHERE loan_id = '".$loan_id."'");
		if($feth_foreclouserdata->num_rows() > 0){

			$feth_foreclouserdata = $feth_foreclouserdata->result();
			

			$foreclosure_update .= '<table class="table" style="width:100%;vertical-align:top;">';
			$foreclosure_update .= '<tr>';
			$foreclosure_update .= '<td style="width:30%;"><strong>Date:</strong></td>';
			$foreclosure_update .= '<td style="width:70%;">' . date('m-d-Y') . '</td>';
			$foreclosure_update .= '</tr>';

			$foreclosure_update .= '<tr>';
			$foreclosure_update .= '<td style="width:30%;"><strong>Borrower Name:</strong></td>';
			$foreclosure_update .= '<td style="width:70%;">'.$b_name.'</td>';
			$foreclosure_update .= '</tr>';

			$foreclosure_update .= '<tr>';
			$foreclosure_update .= '<td style="width:30%;"><strong>Property Address:</strong></td>';
			$foreclosure_update .= '<td style="width:70%;">'.$full_address.'</td>';
			$foreclosure_update .= '</tr>';

			$foreclosure_update .= '</table>';

			$foreclosure_update .= '<p><br></p>';

			$foreclosure_update .= '<table class="table" style="width:100%;vertical-align:top;">';

			$foreclosure_update .= '<tr>';
			$foreclosure_update .= '<td style="width:10%;"></td>';
			$foreclosure_update .= '<td style="width:40%;"><strong>Line Item</strong></td>';
			$foreclosure_update .= '<td style="width:40%;"><strong>Amount</strong></td>';
			$foreclosure_update .= '<td style="width:10%;"></td>';
			$foreclosure_update .= '</tr>';

			$total_amount = 0;
			foreach($feth_foreclouserdata as $value){

				$total_amount += $value->forecloser_digit;

				$foreclosure_update .= '<tr>';
				$foreclosure_update .= '<td style="width:10%;"></td>';
				$foreclosure_update .= '<td style="width:40%;">'.$value->forecloser_text.'</td>';
				$foreclosure_update .= '<td style="width:40%;">$'.number_format($value->forecloser_digit).'</td>';
				$foreclosure_update .= '<td style="width:10%;"></td>';
				$foreclosure_update .= '</tr>';

			}

			$foreclosure_update .= '<tr>';
			$foreclosure_update .= '<td style="width:10%;"></td>';
			$foreclosure_update .= '<td style="width:40%;text-align:right;"><strong>Total:</strong></td>';
			$foreclosure_update .= '<td style="width:40%;">$'.number_format($total_amount).'</td>';
			$foreclosure_update .= '<td style="width:10%;"></td>';
			$foreclosure_update .= '</tr>';

			$foreclosure_update .= '</table>';

			
		}else{

			$foreclosure_update .= '<p>No Foreclosure Costs found for this loan.</p>';
		}

		echo $foreclosure_update;
	}


	public function print_default_status() {
		error_reporting(0);
		header("Cache-Control: "); // leave blank to avoid IE errors
		header("Pragma: "); // leave blank to avoid IE errors
		header("Content-type: application/octet-stream");
		// header("Content-type: application/vnd.ms-word");

		header("content-disposition: attachment;filename=TaliMar_Draw_reserve.doc");
		echo '<html xmlns:v="urn:schemas-microsoft-com:vml"
				xmlns:o="urn:schemas-microsoft-com:office:office"
				xmlns:w="urn:schemas-microsoft-com:office:word"
				xmlns="http://www.w3.org/TR/REC-html40">

				<head>
				<meta http-equiv=Content-Type content="text/html; charset=utf-8">
				<meta name=ProgId content=Word.Document>
				<meta name=Generator content="Microsoft Word 9">
				<meta name=Originator content="Microsoft Word 9">
				<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->
				<title>title</title>
				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				body{
					font-size:10pt;
				}
				</style>
				</head>
				<body>';

		echo '<style>

				table.table{
					width:100%;
					padding : 3px 0px;
				}
				table.table th{
					text-align : left;
					font-size : 10pt;

				}
				table.table td{
					font-size:10pt;
					font-size : 10pt;

				}
				table.two_contact_signer td{
					margin : 3px;
					font-size:10pt;
				}
				h1{
					text-align:center;
				}
				h2{
					text-align:center;
					font-family: Times New Roman;
					font-size: 18px;
				}
				p{
					font-size:10pt;
					word-spacing : 10px;
					padding : 2px 0px;

				}
				table{
					font-size:10pt;
				}
		</style>';

		$loan_id = $this->uri->segment(3);
		//$id = $this->input->post('for_ex_idd');

		//fetch loan data...
		$loan_data = $this->User_model->query("Select * from loan where id = '" . $loan_id . "'");
		$loan_data = $loan_data->result();
		$talimar_loan = $loan_data[0]->talimar_loan;
		$borrower = $loan_data[0]->borrower;

		//fetch borrwoer data...
		$borrower_data = $this->User_model->query("select * from borrower_data where id = '" . $borrower . "'");
		$borrower_data = $borrower_data->result();
		$b_name = $borrower_data[0]->b_name;

		//fetch loan_property data...
		$loan_property = $this->User_model->query("select * from loan_property where talimar_loan = '" . $talimar_loan . "'");
		$loan_property = $loan_property->result();
		$property_address = $loan_property[0]->property_address . '; ' . $loan_property[0]->city . ', ' . $loan_property[0]->state . ' ' . $loan_property[0]->zip;
		$conditions = $loan_property[0]->conditions;

		//fetch loan servicing default loan data...
		$default_loan = $this->User_model->query("Select * from loan_servicing_default where talimar_loan = '" . $talimar_loan . "' AND add_forclosure_id='" . $id . "'");
		$default_loan = $default_loan->result();
		$foreclosuer_status = $default_loan[0]->foreclosuer_status;
		$forclosure_process = $default_loan[0]->foreclosuer_processor;

		//config item...
		$property_modal_property_condition = $this->config->item('property_modal_property_condition');
		$foreclosuer_processor = $this->config->item('foreclosuer_processor');
		$foreclosuer_status_option = $this->config->item('foreclosuer_status');
		$estimale_complete_option = $this->config->item('estimale_complete_option');

		$foreclosure_update = '';
		$foreclosure_update .= '<table style="width:100%;vertical-align:top;font-size:8pt;">';
		$foreclosure_update .= '<tr>';
		$foreclosure_update .= '<td style="width:50%;"><img src="' . base_url() . 'assets/admin/layout4/img/logo-light.png" ></td>';
		$foreclosure_update .= '<td style="width:50%;color:gray;"><p style="text-align:right;font-size:8pt;">TaliMar Financial, Inc.<br>16880 West Bernardo Drive, Suite #140<br> San Diego, CA 92127<br>Tel:858.613.0111<br>Toll Free:888.868.8467<br>www.talimarfinancial.com</p></td>';
		$foreclosure_update .= '</tr>';
		$foreclosure_update .= '</table>';

		$foreclosure_update .= '<table style="font-size:12pt;width:100%;color:white;background:#003468;font-family:Calibri, sans-serif;">';
		$foreclosure_update .= '<tr>';
		$foreclosure_update .= '<td style="font-size:12pt;">FORECLOSURE SUMMARY</td>';
		$foreclosure_update .= '</tr>';
		$foreclosure_update .= '</table>';

		$foreclosure_update .= '<table class="table" style="width:100%;vertical-align:top;">';

		$foreclosure_update .= '<tr>';
		$foreclosure_update .= '<td style="width:30%;"><strong>Date:</strong></td>';
		$foreclosure_update .= '<td style="width:70%;">' . date('m-d-Y') . '</td>';
		$foreclosure_update .= '</tr>';

		$foreclosure_update .= '<tr>';
		$foreclosure_update .= '<td style="width:30%;"><strong>Borrower Name:</strong></td>';
		$foreclosure_update .= '<td style="width:70%;">' . $b_name . '</td>';
		$foreclosure_update .= '</tr>';

		$foreclosure_update .= '<tr>';
		$foreclosure_update .= '<td style="width:30%;"><strong>Property Address:</strong></td>';
		$foreclosure_update .= '<td style="width:70%;">' . $property_address . '</td>';
		$foreclosure_update .= '</tr>';

		$foreclosure_update .= '<tr>';
		$foreclosure_update .= '<td style="width:30%;"><strong>Property Condition:</strong></td>';
		$foreclosure_update .= '<td style="width:70%;">' . $property_modal_property_condition[$conditions] . '</td>';
		$foreclosure_update .= '</tr>';

		$foreclosure_update .= '<tr>';
		$foreclosure_update .= '<td style="width:30%;"><strong>Foreclosure Processor:</strong></td>';
		$foreclosure_update .= '<td style="width:70%;">' . $foreclosuer_processor[$forclosure_process] . '</td>';
		$foreclosure_update .= '</tr>';

		$foreclosure_update .= '<tr>';
		$foreclosure_update .= '<td style="width:30%;vertical-align:top;"><strong>Foreclosure Status:</strong></td>';
		$foreclosure_update .= '<td style="width:70%;">' . $foreclosuer_status_option[$foreclosuer_status] . '</td>';
		$foreclosure_update .= '</tr>';

		$foreclosure_update .= '<tr>';
		$foreclosure_update .= '<td style="width:30%;vertical-align:top;"><strong>Key Dates:</strong></td>';
		$foreclosure_update .= '<td style="width:70%;">';

		$foreclosure_update .= '<table class="table" style="width:100%;">';

		$foreclosure_update .= '<tr>';
		$foreclosure_update .= '<td><strong>Date</strong></td>';
		$foreclosure_update .= '<td><strong>Discription</strong></td>';
		$foreclosure_update .= '<td><strong>Status</strong></td>';
		$foreclosure_update .= '</tr>';

		$foreclosure_update .= '<tr>';
		$foreclosure_update .= '<td>' . date('m-d-Y', strtotime($default_loan[0]->borrower_outreach)) . '</td>';
		$foreclosure_update .= '<td>Foreclosure fees paid by TaliMar</td>';
		$foreclosure_update .= '<td>' . $estimale_complete_option[$default_loan[0]->borrower_outreach_option] . '</td>';
		$foreclosure_update .= '</tr>';

		$foreclosure_update .= '<tr>';
		$foreclosure_update .= '<td>' . date('m-d-Y', strtotime($default_loan[0]->nod_recorded)) . '</td>';
		$foreclosure_update .= '<td>Notice of Default Recorded</td>';
		$foreclosure_update .= '<td>' . $estimale_complete_option[$default_loan[0]->nod_recorded_option] . '</td>';
		$foreclosure_update .= '</tr>';

		$foreclosure_update .= '<tr>';
		$foreclosure_update .= '<td>' . date('m-d-Y', strtotime($default_loan[0]->nos_recorded)) . '</td>';
		$foreclosure_update .= '<td>Record Notice of Sale</td>';
		$foreclosure_update .= '<td>' . $estimale_complete_option[$default_loan[0]->nos_recorded_option] . '</td>';
		$foreclosure_update .= '</tr>';

		$foreclosure_update .= '<tr>';
		$foreclosure_update .= '<td>' . date('m-d-Y', strtotime($default_loan[0]->sale_date)) . '</td>';
		$foreclosure_update .= '<td>Date of Sale</td>';
		$foreclosure_update .= '<td>' . $estimale_complete_option[$default_loan[0]->sale_date_option] . '</td>';
		$foreclosure_update .= '</tr>';

		$foreclosure_update .= '</table>';

		$foreclosure_update .= '</td>';
		$foreclosure_update .= '</tr>';

		//Foreclosure Fees and Costs
		$foreclosure_update .= '<tr>';
		$foreclosure_update .= '<td style="width:30%;vertical-align:top;"><strong>Foreclosure Fees and Costs:</strong></td>';
		$foreclosure_update .= '<td style="width:70%;">';

		$foreclosure_update .= '<table class="table" style="width:100%;">';

		$foreclosure_update .= '<tr>';
		$foreclosure_update .= '<td><strong>Date</strong></td>';
		$foreclosure_update .= '<td><strong>Discription</strong></td>';
		$foreclosure_update .= '<td><strong>Status</strong></td>';
		$foreclosure_update .= '</tr>';

		$foreclosure_update .= '<tr><td colspan="3">&nbsp;</td></tr>';
		$foreclosure_update .= '<tr><td colspan="3">&nbsp;</td></tr>';
		$foreclosure_update .= '<tr><td colspan="3">&nbsp;</td></tr>';
		$foreclosure_update .= '<tr><td colspan="3">&nbsp;</td></tr>';

		$foreclosure_update .= '<tr>';
		$foreclosure_update .= '<td></td>';
		$foreclosure_update .= '<td><strong>Total Fees/Costs paid by TaliMar:</strong></td>';
		$foreclosure_update .= '<td></td>';
		$foreclosure_update .= '</tr>';

		$foreclosure_update .= '</table>';

		$foreclosure_update .= '</td>';
		$foreclosure_update .= '</tr>';

		$foreclosure_update .= '</table>';

		echo $foreclosure_update;

	}

	public function print_draw_schedule() {

		error_reporting(0);

		header("Cache-Control: "); // leave blank to avoid IE errors
		header("Pragma: "); // leave blank to avoid IE errors
		header("Content-type: application/octet-stream");
		// header("Content-type: application/vnd.ms-word");

		header("content-disposition: attachment;filename=TaliMar_Draw_reserve.doc");
		echo '<html xmlns:v="urn:schemas-microsoft-com:vml"
				xmlns:o="urn:schemas-microsoft-com:office:office"
				xmlns:w="urn:schemas-microsoft-com:office:word"
				xmlns="http://www.w3.org/TR/REC-html40">

				<head>
				<meta http-equiv=Content-Type content="text/html; charset=utf-8">
				<meta name=ProgId content=Word.Document>
				<meta name=Generator content="Microsoft Word 9">
				<meta name=Originator content="Microsoft Word 9">
				<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->
				<title>title</title>
				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				body{
					font-size:10pt;
				}
				</style>
				</head>
				<body>';

		echo '<style>

		table.table{
			width:100%;
			padding : 3px 0px;
		}
		table.table th{
			text-align : left;
			font-size : 10pt;

		}
		table.table td{
			font-size:10pt;
			font-size : 10pt;

		}
		table.two_contact_signer td{
			margin : 3px;
			font-size:10pt;
		}
		h1{
			text-align:center;
		}
		h2{
			text-align:center;
			font-family: Times New Roman;
			font-size: 18px;
		}
		p{
			font-size:10pt;
			word-spacing : 10px;
			padding : 2px 0px;

		}
		table{
			font-size:10pt;
		}
		</style>';

		$loan_id = $this->uri->segment(3);

		//fetch loan data...
		$loan = $this->User_model->query("select * from loan where id = '" . $loan_id . "'");
		$loan = $loan->result();
		$borrower = $loan[0]->borrower;

		//fetch borrower data...
		$borrowers = $this->User_model->query("select * from borrower_data where id = '" . $borrower . "' ");
		$borrowers = $borrowers->result();
		$borrowers = $borrowers[0]->b_name;

		//fetch borrower_contact data...
		$borrower_contact = $this->User_model->query("select * from borrower_contact where borrower_id = '" . $borrower . "'");
		$borrower_contact = $borrower_contact->result();
		$contact_id = $borrower_contact[0]->contact_id;
		$contact_title = $borrower_contact[0]->c_title;

		//fetch contact data...
		$contact = $this->User_model->query("select * from contact where contact_id = '" . $contact_id . "'");
		$contact = $contact->result();
		$contact_name = $contact[0]->contact_firstname . ' ' . $contact[0]->contact_middlename . ' ' . $contact[0]->contact_lastname;

		// fetch loan_draws data...
		$draws = $this->User_model->query("select * from loan_draws where talimar_loan = '" . $loan[0]->talimar_loan . "' order by position asc");
		$fetch_draws = $draws->result();

		// print doc ...
		$print_draw_schedule = '';
		$print_draw_schedule .= '<h2>RIDER TO PROMISSORY NOTE<br>CONSTRUCTION RESERVE ACCOUNT</h2>';

		$print_draw_schedule .= '<table class="table" style="width:100%;vertical-align:top;">';

		$print_draw_schedule .= '<tr>';
		$print_draw_schedule .= '<td style="width:30%;"><strong>Loan Document Date:</strong></td>';
		$print_draw_schedule .= '<td style="width:70%;">' . $loan[0]->loan_document_date . '</td>';
		$print_draw_schedule .= '</tr>';

		$print_draw_schedule .= '<tr>';
		$print_draw_schedule .= '<td style="width:30%;"><strong>Loan Number:</strong></td>';
		$print_draw_schedule .= '<td style="width:70%;">' . $loan[0]->talimar_loan . '</td>';
		$print_draw_schedule .= '</tr>';

		$print_draw_schedule .= '<tr>';
		$print_draw_schedule .= '<td style="width:30%;"><strong>Borrower Name:</strong></td>';
		$print_draw_schedule .= '<td style="width:70%;">' . $borrowers . '</td>';
		$print_draw_schedule .= '</tr>';

		$print_draw_schedule .= '</table>';

		$total_draw = 0;
		foreach ($fetch_draws as $row) {

			$total_draw += $row->draws_amount;
		}

		$print_draw_schedule .= '<p>This loan includes a Construction Reserve Account totaling $' . number_format($total_draw, 2) . '. Lender agrees to release funds to the Borrower based upon the Loan Draw Schedule shown below.<strong> Any changes requested to the draw schedule must be approved by the Lender and will result in a $150 modification fee.</strong></p>';

		$print_draw_schedule .= '<p>Borrower agrees to use the funds for the sole purpose of improving the property(ies) securing this loan. Funds used for any other purpose, will result in an immediate default of the loan.</p>';

		$print_draw_schedule .= '<p>Borrower is responsible for obtaining any and all mechanic lien releases from all interested parties associated with this project.</p>';

		$print_draw_schedule .= '<p>Borrower agrees to meet TaliMar Financial or any affiliate at the property(ies) securing this loan within 48 hours of written request. Additionally, Borrower agrees to provide an update on the progress of any renovations within 48 hours of any request. The update shall include but not be limited to a statement on the progress of the improvements, the major items required for completion, a detailed reconciliation of the expenses incurred up to the  date of the request, a detailed line item budget of the costs to complete the improvements, and a timeline to completion. If the borrower is unable to accommodate this request within the allowed time, TaliMar Financial, its successors and/or assignees, has/have the right to utilize a third party to obtain the requested information at the cost of the borrower.</p>';

		$print_draw_schedule .= '<p>The following is the agreed upon draw schedule: </p>';

		$print_draw_schedule .= '<table class="table" style="width:100%;">';
		$total_draw = 0;
		foreach ($fetch_draws as $row) {

			$print_draw_schedule .= '<tr>';
			$print_draw_schedule .= '<td style="vertical-align:top;width:10%;">' . $row->draws . '</td>';
			$print_draw_schedule .= '<td style="vertical-align:top;width:20%;">$' . number_format($row->draws_amount, 2) . '</td>';
			$print_draw_schedule .= '<td style="vertical-align:top;width:70%;">' . nl2br($row->draws_require) . '</td>';
			$print_draw_schedule .= '</tr>';

			$total_draw += $row->draws_amount;
		}

		$print_draw_schedule .= '<tr style="background-color:#e5e5e5;>';
		$print_draw_schedule .= '<td style="vertical-align:top;width:10%;"></td>';
		$print_draw_schedule .= '<td style="vertical-align:top;width:20%;"><strong>$' . number_format($total_draw, 2) . '</strong></td>';
		$print_draw_schedule .= '<td style="vertical-align:top;width:70%;"><strong></strong></td>';
		$print_draw_schedule .= '</tr>';

		$print_draw_schedule .= '</table>';

		$print_draw_schedule .= '<p>&nbsp;<br><br></p>';
		$print_draw_schedule .= '<p>' . $borrowers . '</p>';
		$print_draw_schedule .= '<p>&nbsp;<br></p>';

		$print_draw_schedule .= '<table class="table" style="width:100%;vertical-align:top;">';

		$print_draw_schedule .= '<tr>';
		$print_draw_schedule .= '<td style="width:50%;border-top:1px solid black;">' . $contact_name . '<br>' . $contact_title . '</td> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span>Date</span>';
		$print_draw_schedule .= '<td style="width:50%;">&nbsp;</td>';
		$print_draw_schedule .= '</tr>';

		$print_draw_schedule .= '</table>';

		echo $print_draw_schedule;
	}

	public function request_for_notice() {
		error_reporting(0);
		header("Cache-Control: "); // leave blank to avoid IE errors
		header("Pragma: "); // leave blank to avoid IE errors
		header("Content-type: application/octet-stream");
		// header("Content-type: application/vnd.ms-word");

		header("content-disposition: attachment;filename=TaliMar_$talimar_loan.doc");
		echo '<html xmlns:v="urn:schemas-microsoft-com:vml"
				xmlns:o="urn:schemas-microsoft-com:office:office"
				xmlns:w="urn:schemas-microsoft-com:office:word"
				xmlns="http://www.w3.org/TR/REC-html40">

				<head>
				<meta http-equiv=Content-Type content="text/html; charset=utf-8">
				<meta name=ProgId content=Word.Document>
				<meta name=Generator content="Microsoft Word 9">
				<meta name=Originator content="Microsoft Word 9">
				<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->
				<title>title</title>
				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				body{
					font-size:10pt;
				}
				</style>
				</head>
				<body>';

		echo '<style>

				table.table{
					width:100%;
					padding : 3px 0px;
				}
				table.table th{
					text-align : left;
					font-size : 10pt;

				}
				table.table td{
					font-size:10pt;
					font-size : 10pt;

				}
				table.two_contact_signer td{
					margin : 3px;
					font-size:10pt;
				}
				h1{
					text-align:center;
				}
				h2{
					text-align:center;
				}
				p{
					font-size:10pt;
					word-spacing : 10px;
					padding : 2px 0px;
				}
				table{
					font-size:10pt;
				}
		</style>';

		$ecum_id = $this->uri->segment(3);
		//echo $ecum_id;

		//fetch ecum data...
		$sel_ecum1 = $this->User_model->query("SELECT * FROM `encumbrances` WHERE `id` = '" . $ecum_id . "'");
		$sel_ecum1 = $sel_ecum1->result();
		$talimar_loan = $sel_ecum1[0]->talimar_loan;

		//fetch loan table data...
		$where['talimar_loan'] = $talimar_loan;
		$loan = $this->User_model->select_where('loan', $where);
		$loan_result = $loan->result();
		$borrower1 = $loan_result[0]->borrower;

		//fetch loan property data...
		$fetch_property_data = $this->User_model->select_where('loan_property', $where);
		$fetch_property_data = $fetch_property_data->result();

		//fetch loan_assigment data...
		$fetch_assignment_data = $this->User_model->select_where('loan_assigment', $where);
		$fetch_assignment_data = $fetch_assignment_data->result();

		//fetch loan_assigment data...
		$fetch_servicing_data = $this->User_model->select_where('loan_servicing', $where);
		$fetch_servicing_data = $fetch_servicing_data->result();

		//fetch_borrower data...
		$borrower = $this->User_model->query("select * from borrower_data where id = '" . $borrower1 . "'");
		$borrower = $borrower->result();
		$b_name = $borrower[0]->b_name;
		$b_vesting = $borrower[0]->b_vesting;

		//fetch_borrower_contact details...
		$sql3 = "SELECT * FROM `borrower_contact` WHERE borrower_id = '" . $borrower1 . "'";
		$fetch_b_contact = $this->User_model->query($sql3);
		$fetch_borrower_contact = $fetch_b_contact->result();

		$tali_draft_checked = '';
		if (isset($fetch_extra_details[0]->draft_talimar_inc)) {
			if ($fetch_extra_details[0]->draft_talimar_inc == 1) {
				$tali_draft_checked = 1;
				$tali_draft_lender_id['id'] = 10;
				$fetch_draft_lender_data = $this->User_model->select_where('investor', $tali_draft_lender_id);
				$fetch_draft_lender_data = $fetch_draft_lender_data->result();

				$draft_lender_name = $fetch_draft_lender_data[0]->name;
				$draft_lender_vesting = $fetch_draft_lender_data[0]->vesting;
				$draft_lender_entity_name = $fetch_draft_lender_data[0]->entity_name;

				if ($fetch_draft_lender_data[0]->investor_type == 3) {
					$draft_lender_entity_name = $fetch_draft_lender_data[0]->directed_IRA_NAME;
				}

				$draft_lender_talimar_lender = $fetch_draft_lender_data[0]->talimar_lender;
				$draft_lender_fci_lender = $fetch_draft_lender_data[0]->fci_lender;
				$draft_lender_del_toro_no = $fetch_draft_lender_data[0]->del_toro_no;
				$draft_lender_address = $fetch_draft_lender_data[0]->address;
				$draft_lender_unit = $fetch_draft_lender_data[0]->unit;
				$draft_lender_tax_id = $fetch_draft_lender_data[0]->tax_id;
				$draft_lender_re_date = $fetch_draft_lender_data[0]->re_date;
				$draft_lender_city = $fetch_draft_lender_data[0]->city;
				$draft_lender_state = $fetch_draft_lender_data[0]->state;
				$draft_lender_zip = $fetch_draft_lender_data[0]->zip;

				$draft_lender_yield_percent = $loan_result[0]->intrest_rate - $fetch_servicing_data[0]->servicing_fee;

				$draft_lender_monthly_payment = (($draft_lender_yield_percent / 100) * $loan_result[0]->loan_amount) / 12;

				$draft_lender_full_address = $fetch_draft_lender_data[0]->address . '; ' . $fetch_draft_lender_data[0]->city . ', ' . $fetch_draft_lender_data[0]->state . ' ' . $fetch_draft_lender_data[0]->zip;

				$lender_contact_id['lender_id'] = 10;
				$fetch_lender_contact = $this->User_model->select_where('lender_contact', $lender_contact_id);
				if ($fetch_lender_contact->num_rows() > 0) {
					$fetch_lender_contact = $fetch_lender_contact->result();

					$draft_lender_contact1_title = $fetch_lender_contact[0]->contact_title ? $fetch_lender_contact[0]->contact_title : '';

					$draft_lender_contact2_title = $fetch_lender_contact[1]->contact_title ? $fetch_lender_contact[1]->contact_title : '';
					foreach ($fetch_lender_contact as $row) {
						$lender_contact_id['contact_id'] = $row->contact_id;

						// FETCH CONTACT DATA FROM contact table
						$fetch_lender_contact_data = $this->User_model->select_where('contact', $lender_contact_id);
						if ($fetch_lender_contact_data->num_rows() > 0) {

							$fetch_lender_contact_data = $fetch_lender_contact_data->result();

							$draft_lender_contact1 = $fetch_lender_contact_data[0]->contact_firstname . ' ' . $fetch_lender_contact_data[0]->contact_middlename . ' ' . $fetch_lender_contact_data[0]->contact_lastname;

							$draft_lender_contact1_phone = $fetch_lender_contact_data[0]->contact_phone ? $fetch_lender_contact_data[0]->contact_phone : '';

							$draft_lender_contact1_email = $fetch_lender_contact_data[1]->contact_email ? $fetch_lender_contact_data[0]->contact_email : '';
						}
					}
				}
			}
		}

		$select_investor_name = $tali_draft_checked ? $draft_lender_name : $string_lender;

		$select_investor_vesting = $tali_draft_checked ? $draft_lender_vesting : $fetch_investor_data[0]->vesting;

		$select_investor_talimar_lender = $tali_draft_checked ? $draft_lender_talimar_lender : $fetch_investor_data[0]->talimar_lender;

		$select_investor_tax_id = $tali_draft_checked ? $draft_lender_tax_id : $fetch_investor_data[0]->tax_id;

		$select_investor_fci_lender = $tali_draft_checked ? $draft_lender_fci_lender : $fetch_investor_data[0]->fci_lender;

		$select_investor_contact1_phone = $tali_draft_checked ? $draft_lender_contact1_phone : $fetch_investor_data[0]->phone_1;

		$select_investor_entity_name = $tali_draft_checked ? $draft_lender_entity_name : $fetch_investor_data[0]->entity_name;

		$select_investor_contact1_title = $tali_draft_checked ? $draft_lender_contact1_title : $fetch_investor_data[0]->title;

		$select_investor_contact2_title = $tali_draft_checked ? $draft_lender_contact2_title : $fetch_investor_data[0]->c2_title;

		$select_investor_contact1_email = $tali_draft_checked ? $draft_lender_contact1_email : $fetch_investor_data[0]->email;

		$select_investor_contact2_email = $tali_draft_checked ? $draft_lender_contact2_email : $fetch_investor_data[0]->c2_email;

		$select_investor_contact2_phone = $tali_draft_checked ? $draft_lender_contact2_phone : $fetch_investor_data[0]->c2_phone;

		$select_investor_contact1 = $tali_draft_checked ? $draft_lender_contact1 : $fetch_investor_data[0]->first_name . ' ' . $fetch_investor_data[0]->middle_initial . ' ' . $fetch_investor_data[0]->last_name;

		$select_investor_contact2 = $tali_draft_checked ? $draft_lender_contact2 : $fetch_investor_data[0]->c2_first_name . ' ' . $fetch_investor_data[0]->c2_middle_initial . ' ' . $fetch_investor_data[0]->c2_last_name;

		$select_investor_full_address = $tali_draft_checked ? $draft_lender_full_address : $fetch_investor_data[0]->address . '; ' . $fetch_investor_data[0]->city . ', ' . $fetch_investor_data[0]->state . ' ' . $fetch_investor_data[0]->zip;

		$request_for_notice = '<style>
			table.request_for_notice td{
				width : 200px;
			}
			</style>';
		$request_for_notice .= '<table class="table request_for_notice" style="border-collapse:collapse;">';

		$request_for_notice .= '<tr>';
		$request_for_notice .= '<td style="border-right:1px solid black;border-bottom:1px solid black;"><div>';
		$request_for_notice .= '<p><strong>Recording Requested By:</strong><br><br><br></p>';

		$request_for_notice .= '<p><strong>When Recorded Mail to:</strong><br><br>TaliMar Financial, Inc.<br>11440 West Bernardo Ct.,<br>Suite #210<br>San Diego, CA 92127<br><br><strong>Loan Number:</strong> ' . $talimar_loan . '</p>';

		$request_for_notice .= '</div></td>';
		$request_for_notice .= '<td style="border-bottom:1px solid black;vertical-align:bottom;">(This space for recorders use only)';
		$request_for_notice .= '</td>';
		$request_for_notice .= '</tr>';

		$request_for_notice .= '</table>';

		$request_for_notice .= '<table style="width:100%;margin:0px 0px 0px 0px;"><tr><td style="padding:15px;border:1px solid black;width:100%;">';

		$request_for_notice .= '<h2 style="font-family:Times New Roman;font-size:18px;">REQUEST FOR NOTICE<br>CALIFORNIA CIVIL CODE SECTION 2924b</h2>';

		$request_for_notice .= '</td></tr></table>';

		$request_for_notice .= '<p>In accordance with section 2924b, Civil Code of the State of California, request is hereby made that a copy of any Notice of Default and a copy of any Notice of Sale under the Deed of Trust recorded as instrument No. <span style="text-decoration:underline;">&nbsp;&nbsp;&nbsp;&nbsp;' . $sel_ecum1[0]->instrument . '&nbsp;&nbsp;&nbsp;&nbsp;</span>, on <span style="text-decoration:underline;">&nbsp;&nbsp;&nbsp;&nbsp;' . date('m-d-Y', strtotime($sel_ecum1[0]->recording_date)) . ' &nbsp;&nbsp;&nbsp;&nbsp; </span>, of Official Records in the County</p>';

		$usa_city_county = $this->config->item('usa_city_county');
		$request_for_notice .= '<p>Recorder’s Office of <span>&nbsp;&nbsp;' . $usa_city_county[$fetch_property_data[0]->country] . '&nbsp;&nbsp;</span> County, California, describing the real estate property therein as follows.</p>';

		$request_for_notice .= '<p style="margin-left:18px;">See attached Legal Description

		Executed by ' . $b_vesting . ', as Trustor in which

			See attached Lender Schedule

		is named as Beneficiary and ' . $sel_ecum1[0]->trustee . ', as Trustee, be mailed to: TaliMar Financial, Inc. at 11440 West Bernardo Ct., Suite #210; San Diego, CA 92127.</p>';

		$request_for_notice .= '<p><strong>NOTICE:</strong> A copy of any Notice of Default or any Notice of Sale will be sent only to the address contained in this recorded request. If your address changes, a new request must be recorded.</p>';

		$request_for_notice .= '<p>&nbsp;</br></p>';
		$request_for_notice .= '<p>' . $b_name . '</p>';
		$request_for_notice .= '<p>&nbsp;</br></br></p>';

		$request_for_notice .= '<table class="table" style="width:50%;vertical-align:top;">';

		foreach ($fetch_borrower_contact as $row) {

			if ($row->contact_responsibility == '1') {

				$contact = "SELECT * FROM contact WHERE contact_id = '" . $row->contact_id . "'";
				$fetch_contact = $this->User_model->query($contact);
				$fetch_contact = $fetch_contact->row();
				$fetch_name = $fetch_contact->contact_suffix . ' ' . $fetch_contact->contact_middlename . ' ' . $fetch_contact->contact_lastname;

				$request_for_notice .= '<tr>';
				$request_for_notice .= '<td style="border-top:1px solid black;">';
				$request_for_notice .= isset($fetch_name) ? $fetch_name : '';
				$request_for_notice .= '<br>';

				$request_for_notice .= isset($row->c_title) ? $row->c_title : '';

				$request_for_notice .= '</td>';
				$request_for_notice .= '<td style="border-top:1px solid black;text-align:right;vertical-align:top;">Date</td>';

				$request_for_notice .= '</tr>';
				$request_for_notice .= '<tr style="margin-bottom:10px;">&nbsp;<br></tr>';
				$request_for_notice .= '<tr style="margin-bottom:10px;">&nbsp;<br></tr>';
				$request_for_notice .= '<tr style="margin-bottom:10px;">&nbsp;<br></tr>';
				$request_for_notice .= '<tr style="margin-bottom:10px;">&nbsp;<br></tr>';
				$request_for_notice .= '<tr style="margin-bottom:10px;">&nbsp;<br></tr>';
				$request_for_notice .= '<tr style="margin-bottom:10px;">&nbsp;<br></tr>';
				$request_for_notice .= '<tr style="margin-bottom:10px;">&nbsp;<br></tr>';

			}

		}

		$request_for_notice .= '</table>';

		$request_for_notice .= '<table><br></table>';

		// $request_for_notice	.='<h2 style="font-family:Times New Roman;font-size:13.5px;">Lender Schedule</h2>';

		// if($tali_draft_checked)
		// {
		// $request_for_notice	.= '<p> '.$select_investor_vesting.', as to an undivided 100% interest</p>';
		// }
		// else
		// {
		// foreach($fetch_assignment_data as $row)
		// {
		// $fetch_invester_id['id']	= $row->lender_name;
		// $deed_trust_assigment_len_investment_percent = ($row->investment * 100)/$loan_result[0]->loan_amount;
		// $fetch_invester_data_1		= $this->User_model->select_where('investor',$fetch_invester_id);
		// $fetch_invester_data_1		= $fetch_invester_data_1->result();

		// $request_for_notice	.= '<p> '.$fetch_invester_data_1[0]->vesting.', as to an undivided '.number_format($deed_trust_assigment_len_investment_percent ,8).'% interest</p>';
		// }
		// }

		// $request_for_notice	.= '<table>&nbsp;<br>&nbsp;<br>&nbsp;<br></table>';
		// $request_for_notice .= '<br style="page-break-before: always">';

		$request_for_notice .= '<table style="width:115%;margin:0px -50px 0px -50px;"><tr><td style="padding:15px;border:2px solid black;width:115%;">';
		$request_for_notice .= '<h2 style="font-family:Times New Roman;font-size:18px;">ACKNOWLEDGMENT</h2>';
		$request_for_notice .= '<div style="border:1px solid black;margin-right:260px;padding:5px;">';
		$request_for_notice .= '<p>A notary public or other officer completing this certificate verifies only the identity of the individual who signed the document to which this certificate is attached, and not the truthfulness, accuracy, or validity of that document.</p>';
		$request_for_notice .= '</div>';

		$request_for_notice .= '<p>State of California</p>';

		$request_for_notice .= '<p>County of____________________________________)</p>';

		$request_for_notice .= '<table style="width:100%;">
								<tr>
									<td style="width:2%;">on</td>
									<td style="width:36%;border-bottom:1px solid black;"></td>
									<td style="width:12%;">before, me</td>
									<td style="width:50%;border-bottom:1px solid black;"></td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td style="text-align:center;vertical-align:top">(insert name and title of the officer)</td>
								</tr>
								</table>';

		$request_for_notice .= '<table style="width:100%;">
								<tr>
									<td style="width:20%;">personally appeared</td>
									<td style="width:80%;border-bottom:1px solid black;"></td>

								</tr>
								</table>';

		$request_for_notice .= '<p>who proved to me on the basis of satisfactory evidence to be the person(s) whose name(s) is/are subscribed to the within instrument and acknowledged to me that he/she/they executed the same in his/her/their authorized capacity(ies), and that by his/her/their signature(s) on the instrument the person(s), or the entity upon behalf of which the person(s) acted, executed the instrument.</p>';

		$request_for_notice .= '<p>I certify under PENALTY OF PERJURY under the laws of the State of California that the foregoing paragraph is true and correct.</p>';

		$request_for_notice .= '<p>WITNESS my hand and official seal.<br><br></p>';

		$request_for_notice .= '<p>Signature____________________<strong>(Seal)</strong><br><br></p>';

		$request_for_notice .= '</td></tr></table>';

		$request_for_notice .= '<table></table>';

		echo $request_for_notice;

	}

	public function subordination_agreement() {

		header("Cache-Control: "); // leave blank to avoid IE errors
		header("Pragma: "); // leave blank to avoid IE errors
		header("Content-type: application/octet-stream");
		// header("Content-type: application/vnd.ms-word");

		header("content-disposition: attachment;filename=TaliMar_$talimar_loan.doc");
		echo '<html xmlns:v="urn:schemas-microsoft-com:vml"
				xmlns:o="urn:schemas-microsoft-com:office:office"
				xmlns:w="urn:schemas-microsoft-com:office:word"
				xmlns="http://www.w3.org/TR/REC-html40">

				<head>
				<meta http-equiv=Content-Type content="text/html; charset=utf-8">
				<meta name=ProgId content=Word.Document>
				<meta name=Generator content="Microsoft Word 9">
				<meta name=Originator content="Microsoft Word 9">
				<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->
				<title>title</title>
				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				body{
					font-size:10pt;
				}
				</style>
				</head>
				<body>';

		echo '<style>

				table.table{
					width:100%;
					padding : 3px 0px;
				}
				table.table th{
					text-align : left;
					font-size : 10pt;

				}
				table.table td{
					font-size:10pt;
					font-size : 10pt;

				}
				table.two_contact_signer td{
					margin : 3px;
					font-size:10pt;
				}
				h1{
					text-align:center;
				}
				h2{
					text-align:center;
				}
				p{
					font-size:10pt;
					word-spacing : 10px;
					padding : 2px 0px;
				}
				table{
					font-size:10pt;
				}
		</style>';

		$ecum_id = $this->uri->segment(3);
		//echo $ecum_id;

		//fetch ecum data...
		$sel_ecum = $this->User_model->query("SELECT * FROM `encumbrances` WHERE `id` = '" . $ecum_id . "'");
		$sel_ecum = $sel_ecum->result();
		$talimar_loan = $sel_ecum[0]->talimar_loan;

		//fetch loan table data...
		$where['talimar_loan'] = $talimar_loan;
		$loan = $this->User_model->select_where('loan', $where);
		$loan_result = $loan->result();
		$borrower = $loan_result[0]->borrower;

		//fetch_borrower data...
		$borrower = $this->User_model->query("select * from borrower_data where id = '" . $borrower . "'");
		$borrower = $borrower->result();
		$b_name = $borrower[0]->b_name;
		$b_vesting = $borrower[0]->b_vesting;

		$subordination_agreement = '<style>
			table.deed_trust_assigment_rents td{
				width : 200px;
			}

			</style>';
		$subordination_agreement .= '<table class="table deed_trust_assigment_rents">';

		$subordination_agreement .= '<tr>';
		$subordination_agreement .= '<td style="border-right:1px solid black;border-bottom:1px solid black;"><div>';
		$subordination_agreement .= '<p><strong>RECORDING REQUESTED BY:</strong><br><br><br></p>';
		$subordination_agreement .= '<p><strong>WHEN RECORDED MAIL TO:</strong><br>TaliMar Financial, Inc.<br>11440 West Bernardo Ct.,<br>Suite #210<br>San Diego, CA 92127<br>Loan No.: ' . $loan_result[0]->talimar_loan . ' </p>';
		$subordination_agreement .= '</div></td>';
		$subordination_agreement .= '<td style="border-bottom:1px solid black;vertical-align:bottom;">(This space for recorders use only)';
		$subordination_agreement .= '</td>';
		$subordination_agreement .= '</tr>';

		$subordination_agreement .= '</table>';

		$subordination_agreement .= '<h2 style="font-family:Times New Roman;font-size:18px;">SUBORDINATION AGREEMENT</h2>';

		$subordination_agreement .= '<p><strong>NOTICE: THIS SUBORDINATION AGREEMENT RESULTS IN YOUR SECURITY INTEREST IN THE PROPERTTY BECOMING SUBJECT TO AND OF LOWER PRIORITY THAN THE LIEN OF SOME OTHER OR LATER SECURITY INSTRUMENT.</strong></p>';

		$subordination_agreement .= '<p>THIS AGREEMENT, made this ' . date('m-d-Y') . '</p>';
		$subordination_agreement .= '<p>by ' . $b_vesting . ' </p>';
		$subordination_agreement .= '<p>owner of the land hereinafter described and hereinafter referred to as “Owner”, and ' . $sel_ecum[0]->lien_holder . '.</p>';

		$subordination_agreement .= '<p>present owner and holder of the deed of trust and note first hereinafter described and hereinafter referred to as “Beneficiary.”</p>';

		$subordination_agreement .= '<p style="text-align:center;"><strong>WITNESSETH</strong></p>';

		$subordination_agreement .= '<p>THAT WHEREAS, Owner has executed a deed of trust, dated ' . date('m-d-Y', strtotime($sel_ecum[0]->document_date)) . ', to ' . $sel_ecum[0]->beneficiary_vesting . ', as trustee, covering: See Exhibit A attached hereto and made a part hereof.</p>';

		$subordination_agreement .= '<p>to secure a note in the sum of $' . number_format($sel_ecum[0]->original_balance) . ' dated ' . date('m-d-Y', strtotime($sel_ecum[0]->document_date)) . ' in favor of ' . $sel_ecum[0]->beneficiary_vesting . ', which deed of trust was recorded ' . date('m-d-Y', strtotime($sel_ecum[0]->recording_date)) . ' as Instrument ' . $sel_ecum[0]->instrument . ', Official Records of said county; and</p>';

		$subordination_agreement .= '<p>WHEREAS, Owner has executed, or is about to execute, a deed of trust and note in the sum of  $,  dated  , in favor of  , hereinafter referred to as “Lender”, payable with interest and upon the terms and conditions described therein, which deed of trust is to be recorded concurrently herewith; and</p>';

		$subordination_agreement .= '<p>WHEREAS, it is a condition precedent to obtaining said loan that said deed of trust last above mentioned shall unconditionally be and remain at all times a lien or charge upon the land hereinbefore described, prior and superior to the lien or charge of the deed of trust first above mentioned; and</p>';

		$subordination_agreement .= '<p>WHEREAS, Lender is willing to make said loan provided the deed of trust securing the same is a lien or charge upon the above described property prior and superior to the lien or charge of the deed of trust first above mentioned and provided that Beneficiary will specifically and unconditionally subordinate the lien or charge of the deed of trust first above mentioned to the lien or charge of the deed of trust in favor of Lender; and </p>';

		$subordination_agreement .= '<p>WHEREAS, it is to the mutual benefit of the parties hereto that Lender make such loan to Owner; and Beneficiary is willing that the deed of trust securing the same shall, when recorded, constitute a lien or charge upon said land which is unconditionally superior to the lien or charge of the deed of trust first above mentioned.</p>';

		$subordination_agreement .= '<p>NOW THEREFORE, in consideration of the mutual benefits accruing to the parties hereto and other valuable consideration, the receipt and sufficiency of which consideration is hereby acknowledged, and in order to induce Lender to make the loan above referred to, it is hereby declared, understood and agreed as follows:</p>';

		$subordination_agreement .= '<p>(1) That said deed of trust securing said note in favor of Lender, and any renewals or extensions thereof, shall unconditionally be and remain at all times a lien or charge on the property therein described, prior and superior to the lien or charge of the deed of trust first above mentioned. </p>';

		$subordination_agreement .= '<p>(2) That Lender would not make it loan above described without this subordination agreement.</p>';

		$subordination_agreement .= '<p>(3) That this agreement shall be the whole and only agreement with regard to the subordination of the lien or charge of the deed of trust first above mentioned to the lien or charge of the deed of trust in favor of Lender above referred to and shall supersede and cancel, but only insofar as would affect the priority between the deeds of trust hereinbefore specifically described, any prior agreement as to such subordination including, but not limited to, those provisions, if any, contained in the deed of trust first above mentioned, which provide for the subordination of the lien or charge thereof to another deed or deeds of trust or to another mortgage or mortgages. </p>';

		$subordination_agreement .= '<p>Beneficiary declares, agrees and acknowledges that</p>';

		$subordination_agreement .= '<p>(a) He/She consents to and approves (i) all provisions of the note and deed of trust in favor of Lender above referred to, and (ii) all agreements, including but not limited to any loan or escrow agreements, between Owner and Lender for the disbursement of the proceeds of Lender’s loan;</p>';

		$subordination_agreement .= '<p>(b) Lender in making disbursements pursuant to any such agreement is under no obligation or duty to, nor has the Lender represented that it will, see to the application of such proceeds by the person or persons to whom Lender disburses such proceeds and any application or use of such proceeds for purposes other than those provided for in such agreement or agreements shall not defeat the subordination herein made in whole or in part; </p>';

		$subordination_agreement .= '<p>(c) He/She intentionally and unconditionally waives, relinquishes and subordinates the lien or charge of the deed of trust first above mentioned in favor of the lien or charge upon said land of the deed of trust in favor of Lender above referred to and understands that in reliance upon, and in consideration of, this waiver, relinquishment and subordination specific loans and advances are being and will be entered into which would not be made or entered into for said reliance upon this waiver, relinquishment and subordination; and</p>';

		$subordination_agreement .= '<p>(d) An endorsement has been placed upon the note secured by the deed of trust first above mentioned that said deed of trust has by this instrument been subordinated to the lien or charge of the deed of trust in favor of the Lender above referred to. </p>';

		$subordination_agreement .= '<p><strong>NOTICE: THIS SUBORDINATION AGREEMENT CONTAINS A PROVISION WHICH ALLOWS THE PERSON OBLIGATED ON YOUR REAL PROPERTY SECURITY TO OBTAIN A LOAN A PORTION OF WHICH MAY BE EXPENDED FOR OTHER PURPOSES THAN IMPROVEMENT OF THE LAND.</strong></p>';

		$subordination_agreement .= '<p>&nbsp;</p>';

		$subordination_agreement .= '<table class="table" style="width:100%;">';

		$subordination_agreement .= '<tr>';
		$subordination_agreement .= '<td style="width:45%;">Owner:</td>';
		$subordination_agreement .= '<td style="width:10%;"></td>';
		$subordination_agreement .= '<td style="width:45%;">Beneficiary:</td>';
		$subordination_agreement .= '</tr>';

		$subordination_agreement .= '<tr>';
		$subordination_agreement .= '<td style="width:45%;">&nbsp;</td>';
		$subordination_agreement .= '<td style="width:10%;">&nbsp;</td>';
		$subordination_agreement .= '<td style="width:45%;">&nbsp;</td>';
		$subordination_agreement .= '</tr>';
		$subordination_agreement .= '<tr>';
		$subordination_agreement .= '<td style="width:45%;">&nbsp;</td>';
		$subordination_agreement .= '<td style="width:10%;">&nbsp;</td>';
		$subordination_agreement .= '<td style="width:45%;">&nbsp;</td>';
		$subordination_agreement .= '</tr>';

		$subordination_agreement .= '<tr>';
		$subordination_agreement .= '<td style="width:45%;text-align:right;border-top:1px solid black;">Date</td>';
		$subordination_agreement .= '<td style="width:10%;text-align:right;"></td>';
		$subordination_agreement .= '<td style="width:45%;text-align:right;border-top:1px solid black;">Date</td>';
		$subordination_agreement .= '</tr>';

		$subordination_agreement .= '</table>';

		$subordination_agreement .= '<p style="text-align:center;">(All Signatures must be Acknowledged)</p>';

		$subordination_agreement .= '<table style="width:115%;margin:0px -50px 0px -50px;"><tr><td style="padding:15px;border:2px solid black;width:115%;">';
		$subordination_agreement .= '<h2 style="font-family:Times New Roman;font-size:18px;">ACKNOWLEDGMENT</h2>';
		$subordination_agreement .= '<div style="border:1px solid black;margin-right:260px;padding:5px;">';
		$subordination_agreement .= '<p>A notary public or other officer completing this certificate verifies only the identity of the individual who signed the document to which this certificate is attached, and not the truthfulness, accuracy, or validity of that document.</p>';
		$subordination_agreement .= '</div>';

		$subordination_agreement .= '<p>State of California</p>';

		$subordination_agreement .= '<p>County of____________________________________)</p>';

		$subordination_agreement .= '<table style="width:100%;">
								<tr>
									<td style="width:2%;">on</td>
									<td style="width:36%;border-bottom:1px solid black;"></td>
									<td style="width:12%;">before, me</td>
									<td style="width:50%;border-bottom:1px solid black;"></td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td style="text-align:center;vertical-align:top">(insert name and title of the officer)</td>
								</tr>
								</table>';

		$subordination_agreement .= '<table style="width:100%;">
								<tr>
									<td style="width:20%;">personally appeared</td>
									<td style="width:80%;border-bottom:1px solid black;"></td>

								</tr>
								</table>';

		$subordination_agreement .= '<p>who proved to me on the basis of satisfactory evidence to be the person(s) whose name(s) is/are subscribed to the within instrument and acknowledged to me that he/she/they executed the same in his/her/their authorized capacity(ies), and that by his/her/their signature(s) on the instrument the person(s), or the entity upon behalf of which the person(s) acted, executed the instrument.</p>';

		$subordination_agreement .= '<p>I certify under PENALTY OF PERJURY under the laws of the State of California that the foregoing paragraph is true and correct.</p>';

		$subordination_agreement .= '<p>WITNESS my hand and official seal.<br><br></p>';

		$subordination_agreement .= '<p>Signature____________________<strong>(Seal)</strong><br><br></p>';

		$subordination_agreement .= '</td></tr></table>';

		$subordination_agreement .= '<table></table>';

		$subordination_agreement .= '<br style="page-break-before: always">';

		echo $subordination_agreement;

	}

	

	public function selected_borrower_datas($borrower_id) {

		$where['id'] = $borrower_id;
		$borrower = $this->User_model->select_where('borrower_data', $where);
		return $borrower_res = $borrower->result();
	}

	

	public function print_loan_reserve2() {

		$loan_id = $this->uri->segment(3);
		//echo $loan_id;

		$fetch_date = $this->User_model->query("SELECT * FROM `loan_reserve_date` WHERE `loan_id`='" . $loan_id . "'");
		if ($fetch_date->num_rows() > 0) {
			$fetch_date = $fetch_date->result();
			$data['fetch_dates'] = $fetch_date;
		}

		$fetch_draws = $this->User_model->query("SELECT * FROM `loan_reserve_draws` WHERE `loan_id` = '" . $loan_id . "'");
		if ($fetch_draws->num_rows() > 0) {
			$fetch_draws = $fetch_draws->result();
			$data['fetch_draws'] = $fetch_draws;
		}

		// $fetch_checkbox = $this->User_model->query("SELECT * FROM `loan_reserve_checkbox` WHERE `loan_id` = '".$loan_id."'");
		// if($fetch_checkbox->num_rows() > 0){
		// $fetch_checkbox = $fetch_checkbox->result();
		// }else{
		// $fetch_checkbox = '1';
		// }

		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A3', true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		// $pdf->SetAuthor('Nicola Asuni');
		// $pdf->SetTitle('TCPDF Example 004');
		// $pdf->SetSubject('TCPDF Tutorial');
		// $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

		// set default header data
		// $pdf->SetHeaderData("include/icons/logo-light.png", 30, '', '');

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(2, 20, 2);
		// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
		$pdf->SetFont('times', '', 7);

		// add a page
		//$pdf->AddPage('L', 'A4');
		$pdf->AddPage('L', 'A3');
		// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
		$header = '<h1 style="color:#003468;font-size:18px;"> Loan Reserve Schedule</h1></br>';

		$header .= '<style>
			.table {
				width: 100%;
				max-width: 100%;
				margin-bottom: 20px;
				border-collapse:collapse;
			}
			.table-bordered {
			border: 1px solid #ddd;
			}
			table {
				border-spacing: 0;
				border-collapse: collapse;
			}

			.th_text_align_center th {
				text-align: left;
			}
			.th_text_align_center td {
				text-align: left;
			}

			.td_large{
				width:17%;
			}
			.small_td{
				width:4%;
			}

			.td_medium{
				width:5%;
			}
			tr.dark_border th{
				border: 1px solid gray;
			}
			.table td{
				font-size:12px;
				font-family: "Times New Roman", Georgia, Serif;
				height:25px;
			}
			.table th{
				height : 20px;
				font-size : 12px;
			}

			tr.table_header th{
				font-size:12px;
				font-family: "Times New Roman", Georgia, Serif;
				text-align:center;
				background-color:#bfcfe3;

			}

			tr.table_bottom th{
				font-size:12px;
				font-family: "Times New Roman", Georgia, Serif;
				text-align:left;
				background-color:#bfcfe3;

			}
			tr.odd td{
				background-color:#ededed;
			}
			</style>
			';

		$header .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';
		$header .= '<tr class="table_header">';

		$header .= '<th style="width:10%;text-decoration:underline;"><strong>Draw Description</strong></th>';
		$header .= '<th style="width:5%;text-decoration:underline;"><strong>Budget</strong></th>';
		$header .= '<th style="width:5%;text-decoration:underline;"><strong>Drawn to<br>Date</strong></th>';
		$header .= '<th style="width:5%;text-decoration:underline;"><strong>Remaining </strong></th>';

		$date_1 = $fetch_date[0]->date_1 ? date('m-d-Y', strtotime($fetch_date[0]->date_1)) : 'Enter Date';
		$date_2 = $fetch_date[0]->date_2 ? date('m-d-Y', strtotime($fetch_date[0]->date_2)) : 'Enter Date';
		$date_3 = $fetch_date[0]->date_3 ? date('m-d-Y', strtotime($fetch_date[0]->date_3)) : 'Enter Date';
		$date_4 = $fetch_date[0]->date_4 ? date('m-d-Y', strtotime($fetch_date[0]->date_4)) : 'Enter Date';
		$date_5 = $fetch_date[0]->date_5 ? date('m-d-Y', strtotime($fetch_date[0]->date_5)) : 'Enter Date';
		$date_6 = $fetch_date[0]->date_6 ? date('m-d-Y', strtotime($fetch_date[0]->date_6)) : 'Enter Date';
		$date_7 = $fetch_date[0]->date_7 ? date('m-d-Y', strtotime($fetch_date[0]->date_7)) : 'Enter Date';
		$date_8 = $fetch_date[0]->date_8 ? date('m-d-Y', strtotime($fetch_date[0]->date_8)) : 'Enter Date';
		$date_9 = $fetch_date[0]->date_9 ? date('m-d-Y', strtotime($fetch_date[0]->date_9)) : 'Enter Date';
		$date_10 = $fetch_date[0]->date_10 ? date('m-d-Y', strtotime($fetch_date[0]->date_10)) : 'Enter Date';
		$date_11 = $fetch_date[0]->date_11 ? date('m-d-Y', strtotime($fetch_date[0]->date_11)) : 'Enter Date';
		$date_12 = $fetch_date[0]->date_12 ? date('m-d-Y', strtotime($fetch_date[0]->date_12)) : 'Enter Date';
		$date_13 = $fetch_date[0]->date_13 ? date('m-d-Y', strtotime($fetch_date[0]->date_13)) : 'Enter Date';
		$date_14 = $fetch_date[0]->date_14 ? date('m-d-Y', strtotime($fetch_date[0]->date_14)) : 'Enter Date';
		$date_15 = $fetch_date[0]->date_15 ? date('m-d-Y', strtotime($fetch_date[0]->date_15)) : 'Enter Date';

		$header .= '<th style="width:5%;text-decoration:underline;"><strong>' . $date_1 . '</strong></th>';
		$header .= '<th style="width:5%;text-decoration:underline;"><strong>' . $date_2 . '</strong></th>';
		$header .= '<th style="width:5%;text-decoration:underline;"><strong>' . $date_3 . '</strong></th>';
		$header .= '<th style="width:5%;text-decoration:underline;"><strong>' . $date_4 . '</strong></th>';
		$header .= '<th style="width:5%;text-decoration:underline;"><strong>' . $date_5 . '</strong></th>';
		$header .= '<th style="width:5%;text-decoration:underline;"><strong>' . $date_6 . '</strong></th>';
		$header .= '<th style="width:5%;text-decoration:underline;"><strong>' . $date_7 . '</strong></th>';
		$header .= '<th style="width:5%;text-decoration:underline;"><strong>' . $date_8 . '</strong></th>';
		$header .= '<th style="width:5%;text-decoration:underline;"><strong>' . $date_9 . '</strong></th>';
		$header .= '<th style="width:5%;text-decoration:underline;"><strong>' . $date_10 . '</strong></th>';
		$header .= '<th style="width:5%;text-decoration:underline;"><strong>' . $date_11 . '</strong></th>';
		$header .= '<th style="width:5%;text-decoration:underline;"><strong>' . $date_12 . '</strong></th>';
		$header .= '<th style="width:5%;text-decoration:underline;"><strong>' . $date_13 . '</strong></th>';
		$header .= '<th style="width:5%;text-decoration:underline;"><strong>' . $date_14 . '</strong></th>';
		$header .= '<th style="width:5%;text-decoration:underline;"><strong>' . $date_15 . '</strong></th>';

		$header .= '</tr>';

		$date1_total = 0;
		$date2_total = 0;
		$date3_total = 0;
		$date4_total = 0;
		$date5_total = 0;
		$date6_total = 0;
		$date7_total = 0;
		$date8_total = 0;
		$date9_total = 0;
		$date10_total = 0;
		$date11_total = 0;
		$date12_total = 0;
		$date13_total = 0;
		$date14_total = 0;
		$date15_total = 0;

		foreach ($fetch_draws as $key => $rows) {

			if ($key % 2 == 0) {
				$class_add = "even";
			} else if ($key == 0) {
				$class_add = "even";
			} else {
				$class_add = "odd";
			}

			$date1_total += $fetch_draws[$key]->date1_amount;
			$date2_total += $fetch_draws[$key]->date2_amount;
			$date3_total += $fetch_draws[$key]->date3_amount;
			$date4_total += $fetch_draws[$key]->date4_amount;
			$date5_total += $fetch_draws[$key]->date5_amount;
			$date6_total += $fetch_draws[$key]->date6_amount;
			$date7_total += $fetch_draws[$key]->date7_amount;
			$date8_total += $fetch_draws[$key]->date8_amount;
			$date9_total += $fetch_draws[$key]->date9_amount;
			$date10_total += $fetch_draws[$key]->date10_amount;
			$date11_total += $fetch_draws[$key]->date11_amount;
			$date12_total += $fetch_draws[$key]->date12_amount;
			$date13_total += $fetch_draws[$key]->date13_amount;
			$date14_total += $fetch_draws[$key]->date14_amount;
			$date15_total += $fetch_draws[$key]->date15_amount;

			$header .= '<tr class="' . $class_add . '">';

			$date1_amount = $rows->date1_amount ? '$' . number_format($rows->date1_amount) : '';
			$date2_amount = $rows->date2_amount ? '$' . number_format($rows->date2_amount) : '';
			$date3_amount = $rows->date3_amount ? '$' . number_format($rows->date3_amount) : '';
			$date4_amount = $rows->date4_amount ? '$' . number_format($rows->date4_amount) : '';
			$date5_amount = $rows->date5_amount ? '$' . number_format($rows->date5_amount) : '';
			$date6_amount = $rows->date6_amount ? '$' . number_format($rows->date6_amount) : '';
			$date7_amount = $rows->date7_amount ? '$' . number_format($rows->date7_amount) : '';
			$date8_amount = $rows->date8_amount ? '$' . number_format($rows->date8_amount) : '';
			$date9_amount = $rows->date9_amount ? '$' . number_format($rows->date9_amount) : '';
			$date10_amount = $rows->date10_amount ? '$' . number_format($rows->date10_amount) : '';
			$date11_amount = $rows->date11_amount ? '$' . number_format($rows->date11_amount) : '';
			$date12_amount = $rows->date12_amount ? '$' . number_format($rows->date12_amount) : '';
			$date13_amount = $rows->date13_amount ? '$' . number_format($rows->date13_amount) : '';
			$date14_amount = $rows->date14_amount ? '$' . number_format($rows->date14_amount) : '';
			$date15_amount = $rows->date15_amount ? '$' . number_format($rows->date15_amount) : '';

			$drawn_to_date = $rows->date1_amount + $rows->date2_amount + $rows->date3_amount + $rows->date4_amount + $rows->date5_amount + $rows->date6_amount + $rows->date7_amount + $rows->date8_amount + $rows->date9_amount + $rows->date10_amount + $rows->date11_amount + $rows->date12_amount + $rows->date13_amount + $rows->date14_amount + $rows->date15_amount;

			$remaining = $rows->budget - ($rows->date1_amount + $rows->date2_amount + $rows->date3_amount + $rows->date4_amount + $rows->date5_amount + $rows->date6_amount + $rows->date7_amount + $rows->date8_amount + $rows->date9_amount + $rows->date10_amount + $rows->date11_amount + $rows->date12_amount + $rows->date13_amount + $rows->date14_amount + $rows->date15_amount);

			$header .= "<td>" . nl2br($rows->draws_description) . "</td>";
			$header .= "<td>$" . number_format($rows->budget) . "</td>";
			$header .= "<td>$" . number_format($drawn_to_date) . "</td>";
			$header .= "<td>$" . number_format($remaining) . "</td>";

			$header .= "<td>" . $date1_amount . "</td>";
			$header .= "<td>" . $date2_amount . "</td>";
			$header .= "<td>" . $date3_amount . "</td>";
			$header .= "<td>" . $date4_amount . "</td>";
			$header .= "<td>" . $date5_amount . "</td>";
			$header .= "<td>" . $date6_amount . "</td>";
			$header .= "<td>" . $date7_amount . "</td>";
			$header .= "<td>" . $date8_amount . "</td>";
			$header .= "<td>" . $date9_amount . "</td>";
			$header .= "<td>" . $date10_amount . "</td>";
			$header .= "<td>" . $date11_amount . "</td>";
			$header .= "<td>" . $date12_amount . "</td>";
			$header .= "<td>" . $date13_amount . "</td>";
			$header .= "<td>" . $date14_amount . "</td>";
			$header .= "<td>" . $date15_amount . "</td>";

			$header .= '</tr>';
		}

		$header .= '<tr class="table_bottom">';

		$header .= '<th><strong>Total:</strong></th>';
		$header .= '<th></th>';
		$header .= '<th></th>';
		$header .= '<th></th>';
		$header .= '<th><strong>$' . number_format($date1_total) . '</strong></th>';
		$header .= '<th><strong>$' . number_format($date2_total) . '</strong></th>';
		$header .= '<th><strong>$' . number_format($date3_total) . '</strong></th>';
		$header .= '<th><strong>$' . number_format($date4_total) . '</strong></th>';
		$header .= '<th><strong>$' . number_format($date5_total) . '</strong></th>';
		$header .= '<th><strong>$' . number_format($date6_total) . '</strong></th>';
		$header .= '<th><strong>$' . number_format($date7_total) . '</strong></th>';
		$header .= '<th><strong>$' . number_format($date8_total) . '</strong></th>';
		$header .= '<th><strong>$' . number_format($date9_total) . '</strong></th>';
		$header .= '<th><strong>$' . number_format($date10_total) . '</strong></th>';
		$header .= '<th><strong>$' . number_format($date11_total) . '</strong></th>';
		$header .= '<th><strong>$' . number_format($date12_total) . '</strong></th>';
		$header .= '<th><strong>$' . number_format($date13_total) . '</strong></th>';
		$header .= '<th><strong>$' . number_format($date14_total) . '</strong></th>';
		$header .= '<th><strong>$' . number_format($date15_total) . '</strong></th>';

		$header .= '</tr>';

		
		$header .= '</table>';

		$pdf->WriteHTML($header);
		$pdf->Output('Loan_reserve2.pdf', 'D');

	}

	

	public function print_instructions($talimar_loan) {

		error_reporting(0);
		header("Cache-Control: "); // leave blank to avoid IE errors
		header("Pragma: "); // leave blank to avoid IE errors
		header("Content-type: application/octet-stream");
		// header("Content-type: application/vnd.ms-word");

		header("content-disposition: attachment;filename=TaliMar_$talimar_loan.doc");
		echo '<html xmlns:v="urn:schemas-microsoft-com:vml"
				xmlns:o="urn:schemas-microsoft-com:office:office"
				xmlns:w="urn:schemas-microsoft-com:office:word"
				xmlns="http://www.w3.org/TR/REC-html40">

				<head>
				<meta http-equiv=Content-Type content="text/html; charset=utf-8">
				<meta name=ProgId content=Word.Document>
				<meta name=Generator content="Microsoft Word 9">
				<meta name=Originator content="Microsoft Word 9">
				<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->
				<title>title</title>
				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				body{
					font-size:10pt;
				}
				</style>
				</head>
				<body>';

		echo '<style>

		table.table{
			width:100%;
			padding : 3px 0px;
		}
		table.table th{
			text-align : left;
			font-size : 10pt;

		}
		table.table td{
			font-size:10pt;
			font-size : 10pt;

		}
		table.two_contact_signer td{
			margin : 3px;
			font-size:10pt;
		}
		h1{
			text-align:center;
		}
		h2{
			text-align:center;
		}
		p{
			font-size:10pt;
			word-spacing : 10px;
			padding : 2px 0px;
		}
		table{
			font-size:10pt;
		}
		</style>';

		$talimar_no['talimar_loan'] = $talimar_loan;
		// fetch loan data
		$fetch_loan_data = $this->User_model->select_where('loan', $talimar_no);
		$fetch_loan_data = $fetch_loan_data->result();
		// Fetch poperty data
		$fetch_property_sql = "SELECT * FROM loan_property JOIN property_home ON property_home.id = loan_property.property_home_id WHERE loan_property.talimar_loan = '" . $talimar_loan . "' AND  property_home.primary_property = '1' ";
		$fetch_property_data = $this->User_model->query($fetch_property_sql);
		$fetch_property_data = $fetch_property_data->result();

		$fetch_extra_details = $this->User_model->select_where('extra_details', $talimar_no);
		$fetch_extra_details = $fetch_extra_details->result();

		if ($fetch_extra_details[0]->draw_escrow_account == '1') {

			$draw_escrow_agent = 'La Mesa Fund Control';
			$draw_escrow_account = $fetch_extra_details[0]->draw_escrow_other;

		} else if ($fetch_extra_details[0]->draw_escrow_account == '2') {

			$draw_escrow_agent = 'Del Toro Loan Servicing';
			$draw_escrow_account = $fetch_extra_details[0]->draw_escrow_other;

		} else if ($fetch_extra_details[0]->draw_escrow_account == '3') {

			$draw_escrow_agent = 'FCI Lender Services';
			$draw_escrow_account = $fetch_loan_data[0]->fci;

		} else if ($fetch_extra_details[0]->draw_escrow_account == '4') {

			$draw_escrow_agent = 'Other';
			$draw_escrow_account = $fetch_extra_details[0]->draw_escrow_other;

		} else {

			$draw_escrow_agent = '';
			$draw_escrow_account = $fetch_extra_details[0]->draw_escrow_other;
		}
		$fetch['talimar_loan'] = $talimar_loan;
		$fetch_loan_d = $this->User_model->select_where('loan', $fetch);
		$fetch_loan_d = $fetch_loan_d->result();

		foreach ($fetch_loan_d as $row) {
			$fci = $row->fci;

		}
		// fetch borrower draw_wire_instructions
		$borrower_id_from_loan['id'] = $fetch_loan_data[0]->borrower;

		$fetch_borrower_draw_info = $this->User_model->query("SELECT * FROM wire_instruction2 WHERE talimar_loan = '" . $talimar_loan . "'");

		$fetch_borrower_draw_info = $fetch_borrower_draw_info->result();

		// echo '<pre>';
		// print_r($fetch_borrower_draw_info);
		// echo '</pre>';
		//fetch vendors details...
		$vendors_name = '';
		if ($fetch_loan_data[0]->trustee == 14) {
			$vendors_name = 'FCI Lender Services';

		} elseif ($fetch_loan_data[0]->trustee == 15) {
			$vendors_name = 'TaliMar Financial Inc.';

		} elseif ($fetch_loan_data[0]->trustee == 16) {
			$vendors_name = 'La Mesa Fund Control';

		} elseif ($fetch_loan_data[0]->trustee == 19) {
			$vendors_name = 'Del Toro Loan Servicing';

		} else {
			$vendors_name = '';
		}

		// ----------Fetch borrower Contact detail---------------------

		// fetch borrower data
		$borrower_id_from_loan['id'] = $fetch_loan_data[0]->borrower;
		$fetch_borrower_data = $this->User_model->select_where('borrower_data', $borrower_id_from_loan);
		$fetch_borrower_data = $fetch_borrower_data->result();
		$select_borrower_contact1 = '';
		$select_borrower_contact1_email = '';
		$select_borrower_contact1_phone = '';
		$select_borrower_contact1_title = '';

		$select_borrower_contact2 = '';
		$select_borrower_contact2_email = '';
		$select_borrower_contact2_phone = '';
		$select_borrower_contact2_title = '';

		$where_b_id['borrower_id'] = $fetch_loan_data[0]->borrower;
		$fetch_borrower_contact1 = $this->User_model->select_where('borrower_contact', $where_b_id);

		if ($fetch_borrower_contact1->num_rows() > 0) {
			$fetch_borrower_contact = $fetch_borrower_contact1->result();
			// ---------------------- Borrower Contact 1 Section --------------------------------------------
			if ($fetch_borrower_contact[0]->contact_id) {
				$b_con_id['contact_id'] = $fetch_borrower_contact[0]->contact_id;

				$fetch_b_contact = $this->User_model->select_where('contact', $b_con_id);
				if ($fetch_b_contact->num_rows() > 0) {
					$b_contact = $fetch_b_contact->result();
					$select_borrower_contact1 = $b_contact[0]->contact_firstname . ' ' . $b_contact[0]->contact_middlename . ' ' . $b_contact[0]->contact_lastname;

					$select_borrower_contact1_email = $b_contact[0]->contact_email;
					$select_borrower_contact1_phone = $b_contact[0]->contact_phone;
					$select_borrower_contact1_title = $fetch_borrower_contact[0]->c_title;
				}

			}

		}

		$construction_draw_wire_instruction = '';

		$construction_draw_wire_instruction = '<style>
											table.construction_draw_form td
											{
												padding-bottom:7px;
											}
											</style>';

		$construction_draw_wire_instruction .= '<table style="width:100%;vertical-align:top;">';
		$construction_draw_wire_instruction .= '<tr>';
		$construction_draw_wire_instruction .= '<td style="width:50%;"><img src="' . base_url() . 'assets/admin/layout4/img/logo-light.png" ></td>';
		$construction_draw_wire_instruction .= '<td style="width:50%;color:gray;"><p style="text-align:right;font-size:8pt;">TaliMar Financial, Inc.<br>16880 West Bernardo Drive, Suite #140<br> San Diego, CA 92127<br>Tel:858.613.0111<br>Toll Free:888.868.8467<br>www.TALIMARFINANCIAL.com</p></td>';
		$construction_draw_wire_instruction .= '</tr>';
		$construction_draw_wire_instruction .= '</table>';
		$construction_draw_wire_instruction .= '<h3 align="center" style = "font-size:18pt;">CONSTRUCTION DRAW <br>WIRE INSTRUCTIONS </h3> <br>';

		$construction_draw_wire_instruction .= '<p style="font-size:14pt;background:#e5e5e5;">Fund Control Information </p>';

		$construction_draw_wire_instruction .= '<table class="construction_draw_form" style="width:100%;vertical-align:top;">';

		/* $construction_draw_wire_instruction 	 .= '<tr>';
			$construction_draw_wire_instruction 	 .= '<td style="width:30%;vertical-align:top;"><strong>Date of Draw Request: </strong></td>';
			$construction_draw_wire_instruction 	 .= '<td  style="width:30%;vertical-align:top;">'.date('m-d-Y').'</td>';
			$construction_draw_wire_instruction 	 .= '</tr>';*/

		$construction_draw_wire_instruction .= '<tr>';
		$construction_draw_wire_instruction .= '<td style="width:30%;vertical-align:top;"><strong>Fund Control Account #: </strong></td>';
		// $construction_draw_wire_instruction 	 .= '<td  style="width:30%;vertical-align:top;">'.$select_borrower_contact1.'</td>';
		$construction_draw_wire_instruction .= '<td  style="width:30%;vertical-align:top;">' . $fci . '</td>';
		$construction_draw_wire_instruction .= '</tr>';

		$escrow_account_option = $this->config->item('escrow_account_option');

		$construction_draw_wire_instruction .= '<tr>';
		$construction_draw_wire_instruction .= '<td style="width:30%;vertical-align:top;"><strong>Fund Control Agent: </strong></td>';
		// $construction_draw_wire_instruction 	 .= '<td  style="width:30%;vertical-align:top;">'.$fetch_property_data[0]->property_address.'; '.$fetch_property_data[0]->city.', '.$fetch_property_data[0]->state.' '.$fetch_property_data[0]->zip.'</td>';
		$construction_draw_wire_instruction .= '<td  style="width:30%;vertical-align:top;">' . $vendors_name . '</td>';
		$construction_draw_wire_instruction .= '</tr>';

		$construction_draw_wire_instruction .= '</table>';

		$construction_draw_wire_instruction .= '<p style="font-size:14pt;background:#e5e5e5;">Property Information </p>';

		$construction_draw_wire_instruction .= '<table class="construction_draw_form" style="width:100%;vertical-align:top;">';

		$construction_draw_wire_instruction .= '<tr>';
		$construction_draw_wire_instruction .= '<td style="width:30%;vertical-align:top;"><strong>Property Address:</strong></td>';
		$construction_draw_wire_instruction .= '<td  style="width:30%;vertical-align:top;">' . $fetch_property_data[0]->property_address . '; ' . $fetch_property_data[0]->city . ', ' . $fetch_property_data[0]->state . ' ' . $fetch_property_data[0]->zip . '</td>';
		$construction_draw_wire_instruction .= '</tr>';

		$construction_draw_wire_instruction .= '</table>';

		$construction_draw_wire_instruction .= '<p style="font-size:14pt;background:#e5e5e5;">Borrower Information </p>';

		$construction_draw_wire_instruction .= '<table class="construction_draw_form" style="width:100%;vertical-align:top;">';

		$construction_draw_wire_instruction .= '<tr>';
		$construction_draw_wire_instruction .= '<td style="width:30%;vertical-align:top;"><strong>Borrower Name: </strong></td>';
		$construction_draw_wire_instruction .= '<td  style="width:30%;vertical-align:top;">' . $fetch_borrower_data[0]->b_name . '</td>';
		$construction_draw_wire_instruction .= '</tr>';

		$construction_draw_wire_instruction .= '<tr>';
		$construction_draw_wire_instruction .= '<td style="width:30%;vertical-align:top;"><strong>Contact Name: </strong></td>';
		$construction_draw_wire_instruction .= '<td  style="width:30%;vertical-align:top;">' . $select_borrower_contact1 . '</td>';
		$construction_draw_wire_instruction .= '</tr>';

		$construction_draw_wire_instruction .= '<tr>';
		$construction_draw_wire_instruction .= '<td style="width:30%;vertical-align:top;"><strong>Contact Phone: </strong></td>';
		$construction_draw_wire_instruction .= '<td  style="width:30%;vertical-align:top;">' . $select_borrower_contact1_phone . '</td>';
		$construction_draw_wire_instruction .= '</tr>';

		$construction_draw_wire_instruction .= '<tr>';
		$construction_draw_wire_instruction .= '<td style="width:30%;vertical-align:top;"><strong>Contact Email: </strong></td>';
		$construction_draw_wire_instruction .= '<td  style="width:30%;vertical-align:top;">' . $select_borrower_contact1_email . '</td>';
		$construction_draw_wire_instruction .= '</tr>';

		$construction_draw_wire_instruction .= '</table>';
		$construction_draw_wire_instruction .= '<p style="font-size:14pt;background:#e5e5e5;">Bank Information </p>';

		$construction_draw_wire_instruction .= '<table class="construction_draw_form" style="width:100%;vertical-align:top;">';

		$construction_draw_wire_instruction .= '<tr>';
		$construction_draw_wire_instruction .= '<td style="width:30%;vertical-align:top;"><strong>Bank Name: </strong></td>';
		$construction_draw_wire_instruction .= '<td  style="width:30%;vertical-align:top;">' . $fetch_borrower_draw_info[0]->bank_name . '</td>';
		$construction_draw_wire_instruction .= '</tr>';

		$construction_draw_wire_instruction .= '<tr>';
		//$construction_draw_wire_instruction 	 .= '<td style="width:30%;vertical-align:top;"><strong>Bank Address: </strong></td>';
		//$construction_draw_wire_instruction 	 .= '<td  style="width:30%;vertical-align:top;">'.$fetch_borrower_draw_info[0]->bank_street.' '.$fetch_borrower_draw_info[0]->bank_unit.'; '.$fetch_borrower_draw_info[0]->bank_city.', '.$fetch_borrower_draw_info[0]->bank_state.' '.$fetch_borrower_draw_info[0]->bank_zip.'</td>';
		//$construction_draw_wire_instruction 	 .= '</tr>';

		$construction_draw_wire_instruction .= '<tr>';
		$construction_draw_wire_instruction .= '<td style="width:30%;vertical-align:top;"><strong>Name on Account:</strong></td>';
		$construction_draw_wire_instruction .= '<td  style="width:30%;vertical-align:top;">' . ucwords($fetch_borrower_draw_info[0]->account_name) . '</td>';
		$construction_draw_wire_instruction .= '</tr>';
		$construction_draw_wire_instruction .= '<tr>';
		$construction_draw_wire_instruction .= '<td style="width:30%;vertical-align:top;"><strong>Account #: </strong></td>';
		$construction_draw_wire_instruction .= '<td  style="width:30%;vertical-align:top;">' . $fetch_borrower_draw_info[0]->account_number . '</td>';
		$construction_draw_wire_instruction .= '</tr>';

		$construction_draw_wire_instruction .= '<tr>';
		$construction_draw_wire_instruction .= '<td style="width:30%;vertical-align:top;"><strong>Routing #: </strong></td>';
		$construction_draw_wire_instruction .= '<td  style="width:30%;vertical-align:top;">' . $fetch_borrower_draw_info[0]->routing_number . '</td>';
		$construction_draw_wire_instruction .= '</tr>';

		$construction_draw_wire_instruction .= '</table>';
		$construction_draw_wire_instruction .= '<br><p>TaliMar Financial highly recommends that you request the Borrower to verbally confirm the wire instructions prior to releasing the funds. </p>';

		echo $construction_draw_wire_instruction;

	}


	//...................marketing script start...............//

	public function message($talimar_loa) {

		error_reporting(0);
		header("Cache-Control: "); // leave blank to avoid IE errors
		header("Pragma: "); // leave blank to avoid IE errors
		header("Content-type: application/octet-stream");
		// header("Content-type: application/vnd.ms-word");

		header("content-disposition: attachment;filename=TaliMar_$talimar_loan.doc");
		echo '<html xmlns:v="urn:schemas-microsoft-com:vml"
				xmlns:o="urn:schemas-microsoft-com:office:office"
				xmlns:w="urn:schemas-microsoft-com:office:word"
				xmlns="http://www.w3.org/TR/REC-html40">

				<head>
				<meta http-equiv=Content-Type content="text/html; charset=utf-8">
				<meta name=ProgId content=Word.Document>
				<meta name=Generator content="Microsoft Word 9">
				<meta name=Originator content="Microsoft Word 9">
				<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->
				<title>title</title>
				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				body{
					font-size:10pt;
				}
				</style>
				</head>
				<body>';

		echo '<style>

		h1{
			text-align:center;
		}
		h2{
			text-align:center;
		}
		p{
			font-size:10pt;
			word-spacing : 10px;
			padding : 2px 0px;
		}

		</style>';

		$property_typee = $this->config->item('property_typee');
		$loan_typee = $this->config->item('loan_typee');

		$talimar_no['talimar_loan'] = $talimar_loa;
		$fetch_loan_data = $this->User_model->select_where('loan', $talimar_no);
		$fetch_loan_data = $fetch_loan_data->result();
		$loan_servicing_dataa = $this->User_model->select_where('loan_servicing', $talimar_no);
		$loan_servicing_dataa = $loan_servicing_dataa->result();

		$property_query = $this->User_model->query("SELECT * FROM loan as l JOIN loan_property as lp on l.talimar_loan = lp.talimar_loan WHERE l.talimar_loan = '" . $talimar_loa . "'");

		$property_query = $property_query->result();

		$message = '';
		$message .= '<div>';
		$message .= '<h3 style="text-align:left;">Marketing Scripts</h3><br>';
		$message .= '<p>TaliMar Closing: $' . number_format($fetch_loan_data[0]->loan_amount) . ' ' . $loan_typee[$loan_servicing_dataa[0]->marketing_loan_type] . ' loan.</p>';
		$message .= '<p>Description: TaliMar Financial is pleased to announce its most recent funding of a $' . number_format($fetch_loan_data[0]->loan_amount) . ' ' . $loan_typee[$loan_servicing_dataa[0]->marketing_loan_type] . ' loan secured on a ' . $loan_typee[$loan_servicing_dataa[0]->marketing_property_type] . ' in ' . $property_query[0]->city . ',' . $property_query[0]->state . '.<br><br>';

		$message .= $loan_servicing_dataa[0]->marketing_message . '</p>';

		$message .= '<p>TaliMar Financial is a California hard money lender that specializes in funding Fix & Flip, Construction, and Bridge loans. As a direct lender, we offer aggressive financing options and can fund within less than 5 business days.</p>';
		$message .= 'Contact TaliMar Financial at (858) 201-3523 or visit www.talimarfinancial.com to learn more about our hard money lending program.</p><br>';

		$message .= 'Twitter';
		$message .= '<p>TaliMar Financial is pleased to announce its most recent funding of a $' . number_format($fetch_loan_data[0]->loan_amount) . ' ' . $loan_typee[$loan_servicing_dataa[0]->marketing_loan_type] . ' loan secured on a ' . $loan_typee[$loan_servicing_dataa[0]->marketing_property_type] . ' in ' . $property_query[0]->city . ',' . $property_query[0]->state . ' ' . $loan_servicing_dataa[0]->loan_article . '</p>';

		$message .= '<br style="page-break-before:always">';
		$message .= '</div>';

		echo $message;

	}

	//...................marketing script end...............//

	//......email..........................//

	/*
		Description : auto notifications setting
		Author      : Bitcot
		Created     : 
		Modified    : 04-05-2021
	*/

	

	/*
		Description : auto notifications setting
		Author      : Bitcot
		Created     : 
		Modified    : 04-05-2021
	*/

	public function auto_notifications_setting() {
		$NotificationType    = $this->input->post('NotificationType');
		$NotificationValue   = $this->input->post('NotificationValue');
		$load_id             = $this->input->post('load_id');

		//remainderStatus
		$update_id['id'] = $load_id;
		$data[$NotificationType] = $NotificationValue;
		$this->User_model->updatedata('loan', $update_id, $data);
		echo 1;
	}

	/*
		Description : This function use for send mail - Draw Requested by Borrower
		Author      : Bitcot
		Created     : 
		Modified    : 07-04-2021
	*/

	public function email_draw_loan_services() {

		$amount = $this->input->post('amount');
		$loan_id = $this->input->post('loan_id');
		$loan_borrower_id = $this->input->post('loan_borrower_id');
		$loan_borrower_id_data = array();
		$loan_borrower_id_data['id'] = $loan_borrower_id;
		$fetch_loan_borrower = $this->User_model->select_where('borrower_data', $loan_borrower_id_data);
		$fetch_loan_borrower = $fetch_loan_borrower->result();

		$email = 'servicing@talimarfinancial.com';
		//$email = 'deepesh@bitcot.com';

		$adddressArr = array();
		if($fetch_loan_borrower[0]->b_state){
			$adddressArr[] = ''.$fetch_loan_borrower[0]->b_state;
		}
		if($fetch_loan_borrower[0]->c_unit){
			$adddressArr[] = '#'.$fetch_loan_borrower[0]->c_unit.';';
		}
		if($fetch_loan_borrower[0]->b_city){
			$adddressArr[] = ''.$fetch_loan_borrower[0]->b_city.',';
		}
		if($fetch_loan_borrower[0]->b_state){
			$adddressArr[] = ''.$fetch_loan_borrower[0]->b_state.'';
		}
		if($fetch_loan_borrower[0]->b_zip){
			$adddressArr[] = ''.$fetch_loan_borrower[0]->b_zip.'';
		}
		
		$subject = "Draw Requested by Borrower – ".implode(' ', $adddressArr);
		$header = "From: mail@database.talimarfinancial.com";
		$message .= '<p>A renovation draw request in the amount of $'.$amount.' has been requested by the '.$fetch_loan_borrower[0]->b_name.' on '.implode(' ', $adddressArr).'.<br>Please review and approve the draw request. Once approved, please check "Draw Approved by Loan Servicing" on the Loan Reserve page.</p><br>';
		
		//$message .= '<center><a target="_blank" href="' . base_url() . 'load_data/' . $loan_id . '">Loan Reserve Page</a></center> <br>';

		/*$this->email->initialize(SMTP_SENDGRID);
		$this->email
			->from('mail@talimarfinancial.com', $subject)
			->to($email)
			->subject($subject)
			->message($message)
			->set_mailtype('html');

		$this->email->send();*/
		$dataMailA = array();
		$dataMailA['from_email'] 	= 'mail@talimarfinancial.com';
		$dataMailA['from_name'] 	= 'TaliMar Financial';
		$dataMailA['to'] 			= $email;
		$dataMailA['cc'] 			= '';
		$dataMailA['name'] 			= '';
		$dataMailA['subject'] 		= $subject;
		$dataMailA['content_body'] 	= $message;
		$dataMailA['button_url'] 	= base_url() . 'load_data/' . $loan_id;
		$dataMailA['button_text'] 	= 'Loan Reserve Page';
		send_mail_template($dataMailA);
	}


	public function email_script() {

		$ttalimar_loan_number = $this->input->post('tali');
		$loan_idd = $this->input->post('loa');
		$fetch_total_property_homee = $this->User_model->query("SELECT COUNT(*) as total_row,property_address FROM property_home WHERE talimar_loan = '" . $ttalimar_loan_number . "' AND property_address IS NOT NULL ");

		$result_total_property_homee = $fetch_total_property_homee->row();

		$prop = $result_total_property_homee->property_address;

		$fetch_amount_1 = $this->input->post('amount');

		$message = '';
		
		//$email = 'brockvandenberg1@gmail.com';
		$email = 'servicing@talimarfinancial.com';
		
		$subject = "Draw Requested by Borrower - ".$prop;
		$header = "From: mail@database.talimarfinancial.com";
		$message .= '<p>A Draw request in the amount of $'.$fetch_amount_1.' has been submitted by the Borrower on '.$prop.'. Please review and approve the draw request.</p><br>';
		$message .= '<p>Once approved, please check "Draw Approved by Loan Servicing" on the Loan Reserve page.</p><br>';
		

		/*$this->email->initialize(SMTP_SENDGRID);
		$this->email
			->from('mail@talimarfinancial.com', $subject)
			->to($email)
			->subject($subject)
			->message($message)
			->set_mailtype('html');

		$this->email->send();*/

		$dataMailA = array();
		$dataMailA['from_email'] 	= 'mail@talimarfinancial.com';
		$dataMailA['from_name'] 	= 'TaliMar Financial';
		$dataMailA['to'] 			= $email;
		$dataMailA['cc'] 			= '';
		$dataMailA['name'] 			= '';
		$dataMailA['subject'] 		= $subject;
		$dataMailA['content_body'] 	= $message;
		$dataMailA['button_url'] 	= '';
		$dataMailA['button_text'] 	= '';
		$dataMailA['regards'] 	= 'Talimar Financial';
		send_mail_template($dataMailA);
	
	}

	/*
		Description : This function use for send mail - Draw Requested by Draw Approved by Admin
		Author      : Bitcot
		Created     : 
		Modified    : 07-04-2021
	*/

	public function approval_email() {

		$talimar = $this->input->post('talimar_loan');
		$amount = $this->input->post('amount');

		$select_address = $this->User_model->select_where('loan_property', array('talimar_loan' => $talimar));
		$select_address = $select_address->result();
		$full_address = $select_address[0]->property_address . '' . $select_address[0]->unit . '; ' . $select_address[0]->city . ', ' . $select_address[0]->state . ' ' . $select_address[0]->zip;

		$loan_servicing = $this->User_model->select_where('loan_servicing_contacts', array('talimar_loan' => $talimar));
		$loan_servicing = $loan_servicing->result();
		$loan_servicer = $loan_servicing[0]->loan_servicer;

		$email_user = $this->User_model->select_where('user', array('id' => $loan_servicer));
		$email_user = $email_user->result();
		$servicer_email = $email_user[0]->email_address;

		$message = '';

		//$email = 'brockvandenberg1@gmail.com';
		 $email = 'servicing@talimarfinancial.com';
		$email_to    ='servicing@talimarfinancial.com';
		$subject = "Draw Approved by Admin - ".$full_address;
		$header = "From: mail@database.talimarfinancial.com";

		$message .= '<p>A Draw in the amount of $'.$amount.' has been approved for '.$full_address.' by Admin. You may release the construction draw to the Borrower.</p><br>';
		$message .= '<p>Please check “Draw Released to Borrower” on the Loan Reserve page once the draw has been released and note the loan account.</p><br>';
		

		/*$this->email->initialize(SMTP_SENDGRID);
		$this->email
			->from('mail@talimarfinancial.com', $subject)
			->to($email)
			->subject($subject)
			->message($message)
			->set_mailtype('html');

		$this->email->send();*/

		$dataMailA = array();
		$dataMailA['from_email'] 	= 'mail@talimarfinancial.com';
		$dataMailA['from_name'] 	= 'TaliMar Financial';
		$dataMailA['to'] 			= $email;
		$dataMailA['cc'] 			= '';
		$dataMailA['name'] 			= '';
		$dataMailA['subject'] 		= $subject;
		$dataMailA['content_body'] 	= $message;
		$dataMailA['button_url'] 	= '';
		$dataMailA['button_text'] 	= '';
		$dataMailA['regards'] 	= 'Talimar Financial';
		send_mail_template($dataMailA);
		
	}

	public function update_active_boarding_fee_paid() {
		$sql = "UPDATE loan_servicing SET boarding_fee_paid = '1' WHERE loan_status = '2' ";
		$this->User_model->query($sql);
	}

	public function update_multi_checkk() {

		$id = $this->input->post('loan_id');
		$talimar_loan = $this->input->post('talimar_loan');
		$multi_properties = $this->input->post('checkbox_val');
		$multi_properties_2 = $this->input->post('checkbox_val');

		if ($multi_properties_2 == '1') {

			$multi_propert = '1';
		} else {

			$multi_propert = '2';
		}
		$this->User_model->query("update property_home set multi_properties='" . $multi_properties . "' where loan_id='" . $id . "' AND primary_property <> 1");
		$this->User_model->query("update loan_servicing set d_multi_property_loan='" . $multi_propert . "' where talimar_loan='" . $talimar_loan . "'");
		echo "1";

	}

	/***************************************************** START SHAREPOINT API FUNCTIONS *********************************************/

	// public function auth_login($property_address){

	// 	try {

	// 		$settings['Url'] = "https://talimarfinancialcom.sharepoint.com/sites/Test_talimar/";
	// 		$settings['UserName'] = "adavre@talimarfinancial.com";
	// 		$settings['Password'] = "kamboj@1986";

	// 	    $authCtx = new AuthenticationContext($settings['Url']);
	// 	    $authCtx->acquireTokenForUser($settings['UserName'],$settings['Password']);
	// 	    $ctx = new ClientContext($settings['Url'],$authCtx);
	// 	    //echo 'Login success';

	// 		$targetFolderUrl = "Shared Documents/testing/Pipeline";
	//     	//$this->createSubFolder($ctx,$targetFolderUrl,"Active");
	//     	$this->createSubFolder($ctx,$targetFolderUrl,$property_address);

	//     	//Create Subfolder under the property address...

	//     	$targetsubFolderUrl = "Shared Documents/testing/Pipeline/".$property_address."";
	//     	$subfoldername = array(
	//     								1 => 'Due Diligence Folder',
	//     								2 => 'Loan Documents Folder',
	//     								3 => 'Title Documents',
	//     								4 => 'Lender Disclosures',
	//     								5 => 'Property Insurance Folder',

	//     							);

	//     	foreach($subfoldername as $key => $row){

	//     		$this->createSubFolder($ctx,$targetsubFolderUrl,$row);

	//     	}

	//    	}catch (Exception $e) {
	// 	    echo 'Error: ',  $e->getMessage(), "\n";
	// 	}

	// }

	// public function createSubFolder(ClientContext $ctx,$parentFolderUrl,$folderName){

	//     $files = $ctx->getWeb()->getFolderByServerRelativeUrl($parentFolderUrl)->getFiles();
	//     $ctx->load($files);
	//     $ctx->executeQuery();
	//     //print files info
	//     /* @var $file \Office365\PHP\Client\SharePoint\File */
	//     foreach ($files->getData() as $file) {
	//         print "File name: '{$file->getProperty("ServerRelativeUrl")}'\r\n";
	//     }

	//     $parentFolder = $ctx->getWeb()->getFolderByServerRelativeUrl($parentFolderUrl);
	//     $childFolder = $parentFolder->getFolders()->add($folderName);
	//     $ctx->executeQuery();

	//     return;
	//     //print "Child folder {$childFolder->getProperty("ServerRelativeUrl")} has been created ";
	// }

	function delte_load_pay() {

		$id = $this->input->post('id');
		$this->User_model->query("DELETE FROM impound_account WHERE id = '" . $id . "'");

	}

	function delete_hudd() {

		$id = $this->input->post('id');
		$this->User_model->query("DELETE FROM closing_statement_items WHERE id = '" . $id . "'");

	}


	public function insert_new_add_property() {

		/*echo '<pre>';
		print_r($_POST);
		echo '</pre>';

		echo '=======================';*/

		$property_home_id = $this->input->post('property_home_id');
		$loan_id = str_replace('/', '', $this->input->post('loan_id'));
		$street = trim(str_replace('-', '—', $this->input->post('prop_street')));
		$state = trim($this->input->post('prop_state'));
		$city = trim($this->input->post('prop_city'));
		$zip = trim($this->input->post('prop_zip'));
		$data['property_address'] = $street . '-' . $city . ',- ' . $state . '- ' . $zip;
		$data['prop_type'] = $this->input->post('prop_type');
		$data['unit'] = $this->input->post('uni');
		$data['prop_unit'] = $this->input->post('prop_unit');
		$data['prop_building'] = $this->input->post('prop_building');
		$data['lot_size'] = $this->input->post('prop_lot');
		$data['bedrooms'] = $this->input->post('prop_beds');
		$data['bathroom'] = $this->input->post('prop_baths');
		$data['year_built'] = $this->input->post('prop_year');
		// $data['sale_price']=$this->input->post('prop_sale_price');

		$dollar_replace_salePrice = str_replace(",", "", $this->input->post('prop_sale_price'));
		$dollar_replace_salePrice_1 = str_replace(",", "", $dollar_replace_salePrice);
		$data['sale_price'] = str_replace("$", "", $dollar_replace_salePrice_1);
		$data['unit'] = trim($this->input->post('uni'));

		$prop_new_sale_date = '';
		if ($this->input->post('prop_sale_date')) {

			//$myDateTime = DateTime::createFromFormat('m-d-Y', $this->input->post('prop_sale_date'));
			//$newDateString = $myDateTime->format('m-d-Y');
			$newDateString = input_date_format($this->input->post('prop_sale_date'));
			$prop_new_sale_date = $newDateString ? $newDateString : date('m-d-Y');
		}

		$data['date_sold'] = $prop_new_sale_date;

		$p_bulid = $this->amount_format($this->input->post('prop_building'));
		//$prop_s_price=$this->input->post('prop_sale_price');

		$prop_s_price = str_replace("$", "", $dollar_replace_salePrice_1);

		$prop_s_price." / ".$p_bulid;
		
		$data['per_sq_ft'] = $prop_s_price / $p_bulid;

		$p_unit = $this->input->post('prop_unit');
		$data['prop_per_unit'] = $prop_s_price / $p_unit;
		$data['prop_link'] = $this->input->post('prop_link');

		$data['t_user_id'] = $this->session->userdata('t_user_id');
		$data['loan_id'] = $loan_id;
		$data['talimar_loan'] = $this->input->post('talimar_no');
		$data['property_home_id'] = $property_home_id;


		$this->User_model->insertdata('comparable_sale', $data);
		$this->session->set_flashdata('success', 'Property comparable sale inserted successfully!');
		$this->session->set_flashdata('popup_modal_hit', 'comparable_sales');
		$this->session->set_flashdata('property_home_id', $property_home_id);
		redirect(base_url() . 'load_data/' . $loan_id);

	}

	public function custom_doc_upload_function() {

		$loan_id = $this->input->post('loan_id');

		if ($_FILES) {

			$loan_id = $this->input->post('loan_id');
			$my_data = $this->User_model->query("select * from loan where id='" . $loan_id . "'");
			$my_data = $my_data->result();
			$talim = $my_data[0]->talimar_loan;

			$folder = 'due_dillege_document/' . $talim . '/'; //FCPATH

			/*if (!is_dir($folder)) {
				mkdir($folder, 0777, true);
				chmod($folder, 0777);
			}*/

			foreach ($_FILES['upload_docc']['name'] as $key => $row_data) {

				if ($row_data) {

					$ext = strtolower(pathinfo($row_data, PATHINFO_EXTENSION));

					if ($ext == 'pdf' || $ext == 'docx' || $ext == 'doc') {

						$new_name = basename($row_data);
						$my_file_name 	= str_replace('.'.$ext, '_'.time().'.'.$ext, $new_name);
						$target_dir = $folder . $my_file_name;

						//move_uploaded_file($_FILES["upload_docc"]["tmp_name"][$key], $target_dir);
						$attachmentName=$this->aws3->uploadFile($target_dir,$_FILES["upload_docc"]["tmp_name"][$key]);

					}

				}
			}

			$this->session->set_flashdata('popup_modal_cv', array('diligence_materials', 'upload_docto_sharepoint'));
			$this->session->set_flashdata('success', ' Document Uploaded!');
			redirect(base_url() . 'load_data/' . $loan_id, 'refresh');

		}

	}

	public function title_saved_document() {

		$loan_id = $this->input->post('loan_id');

		if ($_FILES) {

			$loan_id = $this->input->post('loan_id');
			$my_data = $this->User_model->query("select * from loan where id='" . $loan_id . "'");
			$my_data = $my_data->result();
			$talim = $my_data[0]->talimar_loan;

			$folder =  'title_document/' . $talim . '/'; //FCPATH

			/*if (!is_dir($folder)) {
				mkdir($folder, 0777, true);
				chmod($folder, 0777);
			}*/

			foreach ($_FILES['t_upload_doc']['name'] as $key => $row_data) {

				if ($row_data) {

					$ext = strtolower(pathinfo($row_data, PATHINFO_EXTENSION));

					if ($ext == 'pdf' || $ext == 'docx' || $ext == 'doc') {

						$my_file_name 	= str_replace('.'.$ext, '_'.time().'.'.$ext, $new_name);
						$target_dir = $folder . $my_file_name;

						//move_uploaded_file($_FILES["t_upload_doc"]["tmp_name"][$key], $target_dir);
						$attachmentName=$this->aws3->uploadFile($target_dir,$_FILES["t_upload_doc"]["tmp_name"][$key]);

					}

				}
			}
			$this->session->set_flashdata('popup_modal_cv', array('title', 'title_docto_sharepoint'));
			$this->session->set_flashdata('success', ' Title Document Uploaded!');
			redirect(base_url() . 'load_data/' . $loan_id, 'refresh');

		}

	}

	public function upload_property_insurance_document() {

		$loan_id = $this->input->post('loan_id');
		$talimar_no = $this->input->post('talimar_no');
		$property_home_id = $this->input->post('property_home_id');

		if ($_FILES['upload_docc']['name'] != '') {

			$folder = 'property_insurance_doc/' . $talimar_no . '/' . $property_home_id . '/'; //FCPATH

			/*if (!is_dir($folder)) {
				mkdir($folder, 0777, true);
				chmod($folder, 0777);
			}*/

			foreach ($_FILES['upload_docc'] as $key => $row_data) {

				if ($row_data) {

					$ext = strtolower(pathinfo($row_data, PATHINFO_EXTENSION));

					if ($ext == 'pdf' || $ext == 'docx' || $ext == 'doc') {

						$new_name = basename($row_data);
						$my_file_name 	= str_replace('.'.$ext, '_'.time().'.'.$ext, $new_name);
						$target_dir = $folder . $my_file_name;

						//move_uploaded_file($_FILES["upload_docc"]["tmp_name"], $target_dir);
						$attachmentName=$this->aws3->uploadFile($target_dir,$_FILES["upload_docc"]["tmp_name"]);

					}

				}
			}

			$this->session->set_flashdata('success', 'Property insurance document uploaded!');
			redirect(base_url() . 'load_data' . $loan_id, 'refresh');

		} else {

			$this->session->set_flashdata('error', 'Please choose document to uploaded!');
			redirect(base_url() . 'load_data' . $loan_id, 'refresh');
		}

	}

	public function delete_reseve() {

		$id = $this->input->post('id');

		$select = $this->User_model->query("select loan_id from loan_reserve_draws where id='" . $id . "'")->result();

		$sec_id = $select[0]->loan_id;

		$delete1 = $this->User_model->query("delete  from loan_reserve_draws where id='" . $id . "'");
		$delete2 = $this->User_model->query("delete  from loan_reserve_date where loan_id='" . $sec_id . "'");
		$delete3 = $this->User_model->query("delete  from loan_reserve_checkbox where loan_id='" . $sec_id . "'");
		echo "1";
	}

	public function add_payment_gurrantor() {

		$talimar_loan = $this->input->post('talimar_no');
		$loan_id = str_replace('/', '', $this->input->post('loan_id'));

		$id = $this->input->post('id');
		$vesting = $this->input->post('vesting');
		$name = $this->input->post('name');
		$address = $this->input->post('address');
		$unit = $this->input->post('unit');
		$zip = $this->input->post('zip');
		$state = $this->input->post('state');
		$phone = $this->input->post('phone');
		$email = $this->input->post('email');
		$city = $this->input->post('city');

		// echo '<pre>';
		// print_r($_POST);
		// echo '</pre>';
		// 	die();
		foreach ($id as $key => $row) {

			if ($row == 'new' && $row != '') {

				$sql = "INSERT INTO `payment_gurrantor`(`loan_id`, `talimar_loan`, `contact_name`, `vesting`,`address`, `unit`,`zip`,`state`,`phone`,`email`,`city`) VALUES ('" . $loan_id . "', '" . $talimar_loan . "', '" . $name[$key] . "', '" . $vesting . "', '" . $address[$key] . "', '" . $unit[$key] . "', '" . $zip[$key] . "', '" . $state[$key] . "', '" . $phone[$key] . "', '" . $email[$key] . "', '" . $city[$key] . "')";

			} else {

				$sql = "UPDATE `payment_gurrantor` SET `loan_id`='" . $loan_id . "', `talimar_loan`='" . $talimar_loan . "', `contact_name`='" . $name[$key] . "', `vesting`='" . $vesting . "', `address`='" . $address[$key] . "', `unit`='" . $unit[$key] . "', `zip`='" . $zip[$key] . "', `state`='" . $state[$key] . "', `phone`='" . $phone[$key] . "' , `email`='" . $email[$key] . "' , `city`='" . $city[$key] . "' WHERE id = '" . $row . "'";

			}

			$this->User_model->query($sql);

		}

		$this->session->set_flashdata('popup_modal_cv', array('payment_gurrantor'));
		$this->session->set_flashdata('success', 'Payment  Guarantor Updated Successfully!');
		redirect(base_url() . 'load_data/' . $loan_id);

	}

	public function delete_payment_data() {

		$id = $this->input->post('id');

		$delete_contact = $this->User_model->query("DELETE FROM payment_gurrantor WHERE id = '" . $id . "'");
		echo '1';

	}

	//............................................................................Ajax load data code start here............................................................//

	public function escrow_fetch_contac() {

		$fetch_all_contact = $this->User_model->select_star('contact', 'id');
		$fetch_all_contact = $fetch_all_contact->result();
		echo '<option value="">Select One</option>';
		foreach ($fetch_all_contact as $key => $value) {

			if ($this->input->post('escrow_id') == $value->contact_id) {

				$selected = 'Selected';

			} else {

				$selected = '';
			}

			echo '<option value="' . $value->contact_id . '" ' . $selected . '>' . $value->contact_firstname . ' ' . $value->contact_middlename . ' ' . $value->contact_lastname . '</option>';

		}

	}

	public function propertyCall() {

		$fetchAllproperty = $this->User_model->query("SELECT id, property_address FROM loan_property");
		$fetchAllproperty = $fetchAllproperty->result();
		
		foreach($fetchAllproperty as $value) {

			if ($this->input->post('property') == $value->id) {

				$selected = 'Selected';

			} else {

				$selected = '';
			}
			
			echo '<option value="' . $value->id . '" ' . $selected . '>' . $value->property_address. '</option>';
	
		}

	}


	public function rep_contact() {

		$fetch_all_contact = $this->User_model->select_star('contact', 'id');
		$fetch_all_contact = $fetch_all_contact->result();
		echo '<option value="">Select One</option>';
		foreach ($fetch_all_contact as $key => $value) {

			if ($this->input->post('rep_contact') == $value->contact_id) {

				$selected = 'Selected';

			} else {

				$selected = '';
			}

			$disabled = '';
			if($this->input->post('loan_source') == '1') {

				$disabled = 'disabled';

			} else {

				$disabled = '';
			}

			echo '<option value="' . $value->contact_id . '" ' . $selected .' '. $disabled. '>' . $value->contact_firstname . ' ' . $value->contact_middlename . ' ' . $value->contact_lastname . '</option>';

		}

	}

	public function fetch_contac_title() {

		$fetch_all_contact = $this->User_model->select_star('contact', 'id');
		$fetch_all_contact = $fetch_all_contact->result();
		echo '<option value="">Select One</option>';
		foreach ($fetch_all_contact as $key => $values) {
			if ($this->input->post('title_id') == $values->contact_id) {

				$selected_t = 'Selected';

			} else {

				$selected_t = '';
			}

			echo '<option value=' . $values->contact_id . ' ' . $selected_t . '>' . $values->contact_firstname . ' ' . $values->contact_middlename . ' ' . $values->contact_lastname . '</option>';

		}

	}

	public function ajax_afflilation_data() {

		$fetch_all_contact = $this->User_model->select_star('contact', 'id');
		$fetch_all_contact = $fetch_all_contact->result();
		echo '<option value="">Select One</option>';
		foreach ($fetch_all_contact as $key => $values_a) {
			if ($this->input->post('aff_id') == $values_a->contact_id) {

				$selected_a = 'Selected';

			} else {

				$selected_a = '';
			}

			echo '<option value=' . $values_a->contact_id . ' ' . $selected_a . '>' . $values_a->contact_firstname . ' ' . $values_a->contact_middlename . ' ' . $values_a->contact_lastname . '</option>';

		}

	}

	public function ajax_affilation_append() {

		$fetch_all_contact = $this->User_model->select_star('contact', 'id');
		$fetch_all_contact = $fetch_all_contact->result();
		echo '<option value="">Select One</option>';
		foreach ($fetch_all_contact as $key => $values_append) {

			echo '<option value=' . $values_append->contact_id . '>' . $values_append->contact_firstname . ' ' . $values_append->contact_middlename . ' ' . $values_append->contact_lastname . '</option>';

		}

	}

	public function fetch_contact_notee() {

		$fetch_all_contact = $this->User_model->select_star('contact', 'id');
		$fetch_all_contact = $fetch_all_contact->result();
		echo '<option value="">Select One</option>';
		foreach ($fetch_all_contact as $key => $values_appendw) {

			echo '<option value=' . $values_appendw->contact_id . '>' . $values_appendw->contact_firstname . ' ' . $values_appendw->contact_middlename . ' ' . $values_appendw->contact_lastname . '</option>';

		}

	}

	public function fetch_contact_notee_2() {

		$fetch_all_contact = $this->User_model->select_star('contact', 'id');
		$fetch_all_contact = $fetch_all_contact->result();
		echo '<option value="">Select One</option>';
		foreach ($fetch_all_contact as $key => $pay_append_2) {

			if ($this->input->post('id') == $pay_append_2->contact_id) {

				$selected_p_2 = 'Selected';

			} else {

				$selected_p_2 = '';

			}

			echo '<option value=' . $pay_append_2->contact_id . ' ' . $selected_p_2 . '>' . $pay_append_2->contact_firstname . ' ' . $pay_append_2->contact_middlename . ' ' . $pay_append_2->contact_lastname . '</option>';

		}

	}

	public function ajax_payment_append() {

		$fetch_all_contact = $this->User_model->select_star('contact', 'id');
		$fetch_all_contact = $fetch_all_contact->result();
		echo '<option value="">Select One</option>';
		foreach ($fetch_all_contact as $key => $values_appendd) {

			echo '<option value=' . $values_appendd->contact_id . '>' . $values_appendd->contact_firstname . ' ' . $values_appendd->contact_middlename . ' ' . $values_appendd->contact_lastname . '</option>';

		}

	}

	public function fetch_property_app() {

		$fetch_all_contact = $this->User_model->select_star('contact', 'id');
		$fetch_all_contact = $fetch_all_contact->result();

		echo '<option value="">Select One</option>';
		foreach ($fetch_all_contact as $key => $ppay_append) {

			if ($this->input->post('pay_pro') == $ppay_append->contact_id) {

				$selected_pp = 'Selected';

			} else {

				$selected_pp = '';

			}

			echo '<option value=' . $ppay_append->contact_id . ' ' . $selected_pp . '>' . $ppay_append->contact_firstname . ' ' . $ppay_append->contact_middlename . ' ' . $ppay_append->contact_lastname . '</option>';

		}

	}

	public function ajax_payment_data() {

		$fetch_all_contact = $this->User_model->select_star('contact', 'id');
		$fetch_all_contact = $fetch_all_contact->result();

		echo '<option value="">Select One</option>';
		foreach ($fetch_all_contact as $key => $pay_append) {

			if ($this->input->post('pay_id') == $pay_append->contact_id) {

				$selected_p = 'Selected';

			} else {

				$selected_p = '';

			}

			echo '<option value=' . $pay_append->contact_id . ' ' . $selected_p . '>' . $pay_append->contact_firstname . ' ' . $pay_append->contact_middlename . ' ' . $pay_append->contact_lastname . '</option>';

		}

	}

	//............................................................................Ajax load data code start end............................................................//

	public function ajax_for_data() {

		$fetch_all_contact = $this->User_model->query("select contact_lastname,contact_middlename,contact_firstname,contact_id from contact");
		$fetch_all_contact = $fetch_all_contact->result();

		echo '<option value="">Select One</option>';
		foreach ($fetch_all_contact as $key => $pay_appendc) {

			if ($this->input->post('pay_pro') == $pay_appendc->contact_id) {

				$selectsed_ps = 'Selected';

			} else {

				$selectsed_ps = '';

			}

			echo '<option value=' . $pay_appendc->contact_id . ' ' . $selectsed_ps . '>' . $pay_appendc->contact_firstname . ' ' . $pay_appendc->contact_middlename . ' ' . $pay_appendc->contact_lastname . '</option>';

		}

	}


	public function getPaymentHistoryFCI()
	{
		$array = array();
		$result = '';
		if(!empty($this->input->post('loanAccount')))
		{


			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => 'https://api.trustfci.com:8065/graphql',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			 CURLOPT_POSTFIELDS =>'{"query":"{\\n  getLoanActivities(\\n        loanaccount:\\"'.$this->input->post('loanAccount').'\\",\\n        offset:0,\\n        orderby: \\"dateDue\\",\\n        order: \\"desc\\"\\n    ) {\\n    loanAccount\\n    clearingDate\\n    dateDeposited\\n    dateDue\\n    dateReceived\\n    reference\\n    toInterest\\n    toPrincipal\\n    toReserve\\n    totalPayment\\n    description\\n  }\\n}","variables":{}}',
			  CURLOPT_HTTPHEADER => array(
			    'Authorization:  Bearer TnTaHakqp440vi0iNc7XfXLjBK9zymmdSLnfraquGb0',
			    'Content-type:  application/json'
			  ),
			));

			$response = curl_exec($curl);

			if(curl_errno($curl))
			{
			    $array['status'] = 0;
			}else{
				
				$APIResult = json_decode($response);
				if(!empty($APIResult->data->getLoanActivities))
				{
					$data = $APIResult->data->getLoanActivities;

					foreach ($data as $key => $value) {

						if($value->description != 'Funding'){

							$earlier = new DateTime($value->dateDue);
							$later = new DateTime($value->dateReceived);

							$diff = $later->diff($earlier)->format("%a");

							$mount = $value->totalPayment;

							if (strpos($mount, '-') !== false) {

								$finalAmount = str_replace("-","-$",number_format($mount, 2,'.', ','));
								
							}
							else
							{
								$finalAmount = "$".number_format($mount, 2,'.', ',');
							}

							$loanAccount = $this->input->post('loanAccount');

					
							/*<td>'.$value->description.'</td>*/
							$result .= '
									
									<tr class="">
									    <td>'.date('m-d-Y', strtotime($value->dateDue)).'</td>
									   <td>'.date('m-d-Y', strtotime($value->dateReceived)).'</td>
									   <td>'.$diff.'</td>
									   <td>'.$value->description.'</td>
									   <td><a href="javascript:void(0)" onclick="getPaymentHistoryDetails('."'$loanAccount'".')">'.$finalAmount.'</a></td>
									</tr>
									
							';
						}
						
					}

					$array['status'] = 1;
					$array['result'] = $result;
				}
				else
				{
					$array['status'] = 0;
					$array['response'] = $APIResult;
				}	
			}
			
		}
		else
		{
			$array['status1'] = 0;
		
		}

		curl_close($curl);

		// Next payment get api code***************************************************

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://api.trustfci.com:8065/graphql',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS =>'{"query":"{ \\r\\n  getLoanInformation(\\r\\n        loanaccount:\\"'.$this->input->post('loanAccount').'\\",\\r\\n        offset:0,\\r\\n        orderby: \\"nextDueDate\\",\\r\\n        order: \\"desc\\"\\r\\n    )\\r\\n        {\\r\\n            loanAccount\\r\\n            nextDueDate\\r\\n        }\\r\\n}","variables":{}}',
		  CURLOPT_HTTPHEADER => array(
		    'Authorization: Bearer TnTaHakqp440vi0iNc7XfXLjBK9zymmdSLnfraquGb0',
		    'Content-type:  application/json'
		  ),
		));

		$responseNextDate = curl_exec($curl);


		$NextDateArray  = json_decode($responseNextDate);
		if(!empty($NextDateArray->data->getLoanInformation))
		{
			$NextDate = $NextDateArray->data->getLoanInformation;
			$array['NextDate'] = date('m-d-Y', strtotime($NextDate[0]->nextDueDate));
		}
		else
		{
			$array['NextDate'] = '00-00-0000';
		}


			curl_close($curl);

		 echo json_encode($array);
	}

	public function getPaymentHistoryDetailsFCI()
	{
		$array = array();
		$result = '';
		if(!empty($this->input->post('loanAccount')))
		{


			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => 'https://api.trustfci.com:8065/graphql',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			 CURLOPT_POSTFIELDS =>'{"query":"{\\n  getLoanActivities\\n  (\\n        loanaccount:\\"'.$this->input->post('loanAccount').'\\",\\n        offset:0,\\n        orderby: \\"dateDue\\",\\n        order: \\"desc\\"\\n    ) \\n    {\\n        loanAccount\\n        dateReceived\\n        dateDue\\n        description\\n        totalPayment\\n        interestPaidTo\\n        toPrincipal\\n        toChargesPrincipal\\n        lateCharge\\n        toReserve\\n        toUnpaidEscrowInt\\n        toChargesPrincipal\\n        toBrokerFee\\n        toLenderFee\\n        toOtherTaxable\\n        toOtherTaxFree\\n        toOtherPayments\\n        toUnpaidInterest\\n        notes\\n        pppPayment\\n        dayVariance\\n    }\\n}","variables":{}}',
			  CURLOPT_HTTPHEADER => array(
			    'Authorization:  Bearer TnTaHakqp440vi0iNc7XfXLjBK9zymmdSLnfraquGb0',
			    'Content-type:  application/json'
			  ),
			));

			$response = curl_exec($curl);

			if(curl_errno($curl))
			{
			    $array['status'] = 0;
			}else{
				
				$APIResult = json_decode($response);
				if(!empty($APIResult->data->getLoanActivities))
				{
					$data = $APIResult->data->getLoanActivities;

					foreach ($data as $key => $value) {
						if($value->description != 'Funding'){

							$earlier = new DateTime($value->dateDue);
							$later = new DateTime($value->dateReceived);

							$diff = $later->diff($earlier)->format("%a");

							$mount = $value->totalPayment;

							if (strpos($mount, '-') !== false) {
								$finalAmount = str_replace("-","-$",number_format($mount, 2,'.', ','));	
							}
							else
							{
								$finalAmount = "$".number_format($mount, 2,'.', ',');
							}

							$pppPayment = $value->pppPayment;

							if (strpos($mount, '-') !== false) {
								$pppPayment = str_replace("-","-$",number_format($pppPayment, 2,'.', ','));	
							}
							else
							{
								$pppPayment = "$".number_format($pppPayment, 2,'.', ',');
							}

							$loanAccount = $this->input->post('loanAccount');

					

							$result .= '
									
									<tr class="">
									    <td>'.date('m-d-Y', strtotime($value->dateReceived)).'</td>
									   <td>'.date('m-d-Y', strtotime($value->dateDue)).'</td>
									   <td>'.$value->dayVariance.'</td>
									   <td>'.$value->description.'</td>
									   <td><a data-toggle="modal" href="#PaymentHistoryDetails">'.$finalAmount.'</a></td>
									   <td>'.$value->interestPaidTo.'</td>
									   <td>'.$value->toPrincipal.'</td>
									   <td>'.$value->toChargesPrincipal.'</td>
									   <td>'.$value->lateCharge.'</td>
									   <td>'.$value->toReserve.'</td>
									   <td>'.$value->toUnpaidEscrowInt.'</td>
									   <td>'.$pppPayment.'</td>

									   <td>'.$value->toChargesPrincipal.'</td>
									   <td>'.$value->toBrokerFee.'</td>
									   <td>'.$value->toLenderFee.'</td>
									   <td>'.$value->toOtherTaxable.'</td>
									   <td>'.$value->toOtherTaxFree.'</td>
									   <td>'.$value->toOtherPayments.'</td>
									   <td>'.$value->toUnpaidInterest.'</td>
									   <td>'.$value->notes.'</td>


									</tr>
									
							';
						}
					}

					$array['status'] = 1;
					$array['result'] = $result;
				
				
				}
				else
				{
					$array['status1'] = 0;
					$array['response'] = $APIResult;
				}	
			}
			
		}
		else
		{

			$array['status'] = 0;
		
		}

		curl_close($curl);

		

		echo json_encode($array);
	}

	/*
		Today update this method date this methode using for Lender payment history get in FCI portal
		Date 24-03-2021
	*/

	public function getLenderPaymentHistory()
	{
		$array = array();
		$result = '';
		if(!empty($this->input->post('talimar_loan')) && !empty($this->input->post('lender')))
		{
			$loadData = $this->User_model->query("SELECT id,talimar_loan,fci FROM loan WHERE fci != '' AND  talimar_loan = '" . $this->input->post('talimar_loan') . "' ORDER BY id DESC");
			$loadData = $loadData->row();

			$investor = $this->User_model->query("SELECT fci_acct FROM investor WHERE fci_acct != '' AND id = '" . $this->input->post('lender') . "' ORDER BY id DESC");
			$investor = $investor->row();

			if(!empty($loadData->fci) && !empty($investor->fci_acct))
			{

					$curl = curl_init();

					curl_setopt_array($curl, array(
					  CURLOPT_URL => 'https://iapi.trustfci.com:8080/graphql',
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => '',
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 0,
					  CURLOPT_FOLLOWLOCATION => true,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => 'POST',
					  CURLOPT_POSTFIELDS =>'{"query":"{\\n  getPaymentListToLender(account:\\"'.$loadData->fci.'\\", investor:\\"'.$investor->fci_acct.'\\"){\\n   checkDate\\n   checkNo\\n   checkMemo\\n   account\\n   paymentType\\n   checkAmount\\n  }\\n}","variables":{}}',
					  CURLOPT_HTTPHEADER => array(
					    'Content-Type: application/json',
					    'Authorization: Bearer TnTaHakqp440vi0iNc7XfXLjBK9zymmdSLnfraquGb0'
					  ),
					));

					$response = curl_exec($curl);

					if(curl_errno($curl))
					{
					    $array['status3'] = curl_errno($curl);
					}else{
						
						$lenderId = $this->input->post('talimar_loan');

						$APIResult = json_decode($response);
						if(!empty($APIResult->data->getPaymentListToLender))
						{
							$data = $APIResult->data->getPaymentListToLender;
							$a = 1;
							$totalAmount = 0;
							foreach ($data as $key => $value) 
							{
								// if($value->checkMemo == 'Investor Disbursement' )
								// {
									/* $dataFci = $this->getBorrowerPaymentHistory($value->account); */
									$mount = $value->checkAmount;
									if (strpos($mount, '-') !== false)
									{
										$totalAmount = $totalAmount  - (double)str_replace('-', '', $mount);
										$finalAmount = str_replace("-","-$", number_format((double)$mount, 2,'.', ','));							
									}
									else
									{
										$totalAmount = $totalAmount + $mount;
										$finalAmount = "$".number_format((double)$mount, 2,'.', ',');
									}

									

									$loanAccount = $this->input->post('loanAccount');				
									$result .= '
											
											<tr class="">
											   <td>'.date('m-d-Y', strtotime($value->checkDate)).'</td>
											   <td>'.$finalAmount.'</td>
											</tr>		
									';								
								// }
								$a++;	
							}

							$result .= '		
								<tr class="">
								   <th>Total : '.$a.'</th>
								   <th style="text-align:center!important">$'.$totalAmount.'</th>
								</tr>		
							';

							$array['status'] = 1;
							$array['result'] = $result;
							$array['lenderId'] = $investor->fci_acct; 
						
						}
						else
						{
							$array['status4'] = 0;
							$array['response'] = $loadData;
						}	
					}
					curl_close($curl);
			}
			else
			{
				$array['status2'] = 0;
			}	
		}
		else
		{
			$array['status1'] = 0;
		
		}
		// $array['dd'] = $response;
		echo json_encode($array);
	}

	public function getBorrowerPaymentHistory($loanaccount)
	{
		$data = array();
		$curl = curl_init();


		curl_setopt_array($curl, array(
			  CURLOPT_URL => 'https://api.trustfci.com:8065/graphql',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_POSTFIELDS =>'{"query":"{\\n  getLoanActivities(\\n        loanaccount:\\"'.$loanaccount.'\\",\\n        limit:1,\\n        offset:0,\\n        orderby: \\"dateDue\\",\\n        order: \\"desc\\"\\n    ) {\\n    loanAccount\\n    clearingDate\\n    dateDeposited\\n    dateDue\\n    dateReceived\\n    reference\\n    toInterest\\n    toPrincipal\\n    toReserve\\n    totalPayment\\n  }\\n}","variables":{}}',
			  CURLOPT_HTTPHEADER => array(
			    'Authorization:  Bearer TnTaHakqp440vi0iNc7XfXLjBK9zymmdSLnfraquGb0',
			    'Content-type:  application/json'
			  ),
			));

			$response = curl_exec($curl);

			if(curl_errno($curl))
			{
			   return $data;
			}else{
				$APIResult = json_decode($response);
				if(!empty($APIResult->data->getLoanActivities))
				{
					$data = $APIResult->data->getLoanActivities;	
					return $data;
				}
				else
				{
					return $data;
				}
			}

			
	}

	public function capital_diligence_documents(){

		$talimar_no    	=  $this->input->post('talimar_no');
		$loan_id     	=  $this->input->post('loan_id');
		$document_name  =  input_sanitize($this->input->post('document_name'));
		
		$this->session->set_flashdata('popup_modal_cv', array('assigment_main', 'diligence_documents'));
		if($_FILES)
		{
			foreach($_FILES['contact_upload_doc']['name'] as $key => $row_data)
			{					
				if($row_data)
				{
					
					$folder = 'capital_diligence_document/'.$loan_id.'/' ;
					/*if(!is_dir($folder))
					{
						mkdir($folder, 0777, true);
					}*/

					$ext = strtolower(pathinfo($row_data, PATHINFO_EXTENSION));						
					// only docx files are allowed to uploaded!						
					// if($ext == 'docx' || $ext == 'doc')
					if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc')
					{
								
						// $new_name = 'borrower-'.$insert_id.'_'.$cnt++.'.'.$ext;
						//$new_name = $row_data;
						$new_name = str_replace('.'.$ext, '_'.time().'.'.$ext, $row_data);
						$target_dir 	= $folder.time().$new_name;
								
						$contact_document['loan_id']					=	$loan_id;
						$contact_document['talimar_no']					=	$talimar_no;
						$contact_document['document_name']				=	$document_name;
						$contact_document['capital_diligence_document']	=	$target_dir;
						$contact_document['user_id']					=	$this->session->userdata('t_user_id');
						$contact_document['created_at']					=	date("Y-m-d H:i:s");
						
						$attachmentName=$this->aws3->uploadFile($target_dir,$_FILES["contact_upload_doc"]["tmp_name"][$key]);
						//if(move_uploaded_file($_FILES["contact_upload_doc"]["tmp_name"][$key], $target_dir) === TRUE )
						if($attachmentName)
						{
							$this->User_model->insertdata('capital_diligence_document',$contact_document);
						}
						$this->session->set_flashdata('success', ' Document uploaded!');
						redirect(base_url() . "load_data/".$loan_id."#diligence_documents", 'refresh');
					}else{
						$this->session->set_flashdata('error', 'File format invalid!');
						redirect(base_url() . "load_data/".$loan_id."", 'refresh');
					}
				}
			}
		}else{
			$this->session->set_flashdata('error', 'Please upload document!');
			redirect(base_url() . "load_data/".$loan_id."", 'refresh');
		}
	}

	public function delete_capital_diligence_documents(){

		$id = $this->input->post('id');		
		$fetch_document = $this->User_model->select_where('capital_diligence_document',array('id'=>$id));
		$fetch_document = $fetch_document->result();
		if($this->aws3->deleteObject($fetch_document[0]->capital_diligence_document)){
			$delete = $this->User_model->delete('capital_diligence_document',array('id'=>$id));
		}

		echo 1;

	}

	public function delete_property_content(){

		$property_id = $this->input->post('property_id');

		echo 'success';
	}

	public function property_appraisal_document(){

		$talimar_loan			= $this->input->post('talimar_loan');
		$property_id			= $this->input->post('property_id');
		$loan_id				= $this->input->post('loan_id');
		$document_name			=input_sanitize($this->input->post('document_name'));
		if($_FILES)
		{			
		
			$folder = 'appraisal_document/'.$property_id.'/' ; //FCPATH
			/*if(!is_dir($folder))
			{
				mkdir($folder, 0777, true);
				chmod($folder,0777);
			}*/			
			
			foreach($_FILES['appraisal_document_upload']['name'] as $key => $row_data){
				
				if($row_data)
				{
					
					$ext = strtolower(pathinfo($row_data, PATHINFO_EXTENSION));
					// only docx files are allowed to uploaded!
					// if($ext == 'docx' || $ext == 'doc')
					if($ext == 'pdf')
					{
							
						$new_name 		= basename($row_data);
						$my_file_name 	= str_replace('.'.$ext, '_'.time().'.'.$ext, $new_name);
						$target_dir 	= $folder.time().$my_file_name;
						
						$lender_document['property_id']			=	$property_id;
						$lender_document['loan_id']				=	$loan_id;
						$lender_document['talimar_loan']		=	$talimar_loan;
						$lender_document['document_path']		=	$target_dir;
						$lender_document['user_id']				=	$this->session->userdata('t_user_id');
						$lender_document['document_name']		=	$document_name;
							
						//if(move_uploaded_file($_FILES["appraisal_document_upload"]["tmp_name"][$key], $target_dir) === TRUE )
						$attachmentName=$this->aws3->uploadFile($target_dir,$_FILES["appraisal_document_upload"]["tmp_name"][$key]);
						if($attachmentName){
							$this->User_model->insertdata('property_appraisal_document',$lender_document);
						}
					}else{
						$this->session->set_flashdata('error', ' Only PDF format are allowed to upload !');
						$this->session->set_flashdata('popup_modal_hit', 'appraisal_document');
						$this->session->set_flashdata('popup_modal_prt', $property_id);
						redirect(base_url().'load_data/'.$loan_id,'refresh');
					}					
				}
			}			
			$this->session->set_flashdata('success', ' Document uploaded !');
			$this->session->set_flashdata('popup_modal_hit', 'appraisal_document');
			$this->session->set_flashdata('popup_modal_prt', $property_id);
			redirect(base_url().'load_data/'.$loan_id,'refresh');			
			
		}else{
			$this->session->set_flashdata('error', ' Please upload document!');
			$this->session->set_flashdata('popup_modal_hit', 'appraisal_document');
			$this->session->set_flashdata('popup_modal_prt', $property_id);
			redirect(base_url().'load_data/'.$loan_id,'refresh');
		}
	}
	
	public function delete_appraisal_document(){
		$id = $this->input->post('id');

		$fetch_document = $this->User_model->select_where('property_appraisal_document',array('id'=>$id));
		$fetch_document = $fetch_document->result();
		if($this->aws3->deleteObject($fetch_document[0]->document_path)){
			$delete = $this->User_model->delete('property_appraisal_document',array('id'=>$id));
		}
		echo 1;
	}
	public function add_wholesale_deal() {
		if(!empty($_POST)){
			$insertArray=$_POST;
			$wholeSalesArray=array();
			$loan_id 											=$this->input->post('loan_id');
			$talimar 											=$this->input->post('talimar_no');
			$wholeSalesArray['loan_id']							=$this->input->post('loan_id');
			$wholeSalesArray['talimar_loan']					=$this->input->post('talimar_no');
			$wholeSalesArray['wholesale_post']					=$this->input->post('wholesale_post');
			$wholeSalesArray['wholesale_status']				=$this->input->post('wholesale_status');
			$wholeSalesArray['wholesaler']						=$this->input->post('wholesaler');
			$wholeSalesArray['wholesaler_city']					=$this->input->post('wholesaler_city');
			$wholeSalesArray['wholesaler_state']				=$this->input->post('wholesaler_state');
			$wholeSalesArray['wholesaler_purchase_price']		=$this->input->post('wholesaler_purchase_price');
			$wholeSalesArray['wholesaler_rehab_cost']			=$this->input->post('wholesaler_rehab_cost');
			$wholeSalesArray['wholesaler_completion_value']		=$this->input->post('wholesaler_completion_value');
			$wholeSalesArray['wholesaler_close_date']			='';
			$wholeSalesArray['wholesaler_purchase_price']		=str_replace(",","",$wholeSalesArray['wholesaler_purchase_price']);
			$wholeSalesArray['wholesaler_rehab_cost']			=str_replace(",","",$wholeSalesArray['wholesaler_rehab_cost']);
			$wholeSalesArray['wholesaler_completion_value']		=str_replace(",","",$wholeSalesArray['wholesaler_completion_value']);
			if(!empty($this->input->post('wholesaler_close_date'))){
				$wholesaler_close_date							=$this->input->post('wholesaler_close_date');
				$wholesaler_close_date 							=explode("-", $wholesaler_close_date);
				$wholesaler_close_date							=$wholesaler_close_date[2]."-".$wholesaler_close_date[0]."-".$wholesaler_close_date[1];
				$wholeSalesArray['wholesaler_close_date']		=$wholesaler_close_date;
			}
			$wholeSalesArray['wholesaler_purchase_price']		=str_replace("$","",$wholeSalesArray['wholesaler_purchase_price']);
			$wholeSalesArray['wholesaler_rehab_cost']			=str_replace("$","",$wholeSalesArray['wholesaler_rehab_cost']);
			$wholeSalesArray['wholesaler_completion_value']		=str_replace("$","",$wholeSalesArray['wholesaler_completion_value']);
			$total_image 										= $_FILES['image_name'];
			$folder 											= 'property_images/' . $talimar . '/'; 
			if(!is_dir($folder))
			{
				mkdir($folder, 0777, true);
			}
			foreach ($_FILES['image_name'] as $key => $row_data) {
				if ($row_data) {
					$ext = strtolower(pathinfo($row_data, PATHINFO_EXTENSION));
					if ($ext == 'jpeg' || $ext == 'jpg' || $ext == 'png') {
						$new_name = basename($row_data);
						$my_file_name 	= str_replace('.'.$ext, '_'.time().'.'.$ext, $new_name);
						$target_dir = $folder . $my_file_name;
						$wholeSalesArray['image_name']='';
						if(move_uploaded_file($_FILES["image_name"]["tmp_name"], $target_dir) === TRUE) {
							$wholeSalesArray['image_name'] = $my_file_name;
						}
						//$attachmentName=$this->aws3->uploadFile($target_dir,$_FILES["image_name"]["tmp_name"]);
					}
				}
			}
			if(empty($this->input->post('wholesale_id'))){
				$this->User_model->insertdata('loan_wholesale_deal', $wholeSalesArray);
				$insert_id = $this->db->insert_id();
				$wholesaleInterest=array();
				for($i=0;$i<count($_POST['wholesaler_interest_id']);$i++){
					if(!empty($_POST['wholesaler_contact_list'][$i])){
						$wholesaleInterest=array(
												"loan_id"			=>$this->input->post('loan_id'),
												"talimar_loan"		=>$this->input->post('talimar_no'),
												"wholesale_id"		=>$insert_id,
												"contact_id" 		=>$_POST['wholesaler_contact_list'][$i],
												"contact_phone" 	=>$_POST['wholesaler_contact_phone'][$i],
												"contact_email" 	=>$_POST['wholesaler_contact_email'][$i],
												"contact_status" 	=>$_POST['wholesaler_contact_status'][$i],
											);
						$this->User_model->insertdata('loan_wholesale_Interest_list', $wholesaleInterest);
					}					
				}
				$meaasge="Whole Saler Inserted Successfully";
			}else{
				$wholesale_id =	$this->input->post('wholesale_id');
				$updateWhere['wholesale_id'] = $wholesale_id;
				$this->User_model->updatedata('loan_wholesale_deal', $updateWhere, $wholeSalesArray);
				for($i=0;$i<count($_POST['wholesaler_interest_id']);$i++){
					if(!empty($_POST['wholesaler_interest_id'][$i])){
						$wholesaler_interest_id=$_POST['wholesaler_interest_id'][$i];
						$updateWhere['interest_id'] = $wholesaler_interest_id;
						$wholesaleInterest=array(
												"loan_id"			=>$this->input->post('loan_id'),
												"talimar_loan"		=>$this->input->post('talimar_no'),
												"wholesale_id"		=>$this->input->post('wholesale_id'),
												"contact_id" 		=>$_POST['wholesaler_contact_list'][$i],
												"contact_phone" 	=>$_POST['wholesaler_contact_phone'][$i],
												"contact_email" 	=>$_POST['wholesaler_contact_email'][$i],
												"contact_status" 	=>$_POST['wholesaler_contact_status'][$i],
											);
						$this->User_model->updatedata('loan_wholesale_Interest_list', $updateWhere,$wholesaleInterest);
					}else{
						$wholesaleInterest=array(
												"loan_id"			=>$this->input->post('loan_id'),
												"talimar_loan"		=>$this->input->post('talimar_no'),
												"wholesale_id"		=>$this->input->post('wholesale_id'),
												"contact_id" 		=>$_POST['wholesaler_contact_list'][$i],
												"contact_phone" 	=>$_POST['wholesaler_contact_phone'][$i],
												"contact_email" 	=>$_POST['wholesaler_contact_email'][$i],
												"contact_status" 	=>$_POST['wholesaler_contact_status'][$i],
											);
						$this->User_model->insertdata('loan_wholesale_Interest_list', $wholesaleInterest);
					}					
				}
				$meaasge="Whole Saler Updated Successfully";				
			}
			$this->session->set_flashdata('success', $meaasge);
			$this->session->set_flashdata('popup_modal_hit', 'wholesale_deal');
			redirect(base_url().'load_data/'.$loan_id,'refresh');	
		}
		
	}
	public function add_condition_page() {
		if(!empty($_POST)){
			$insertArray					= $_POST;
			$condidtionArray				= array();
			$loan_id 						= $this->input->post('loan_id');
			$talimar 						= $this->input->post('talimar_no');
			if(!empty($insertArray['codition_id'])){
				for($i=0;$i<count($insertArray['codition_id']);$i++){
					$condidtionArray=array(
										"loan_id"				=> $loan_id,
										"talimar_loan"			=> $talimar,
										"condition_status"		=> $insertArray['condition_status'][$i],
										"condition_department"	=> $insertArray['condition_department'][$i],
										"condition_desc"		=> $insertArray['condition_desc'][$i]
									);
					if(empty($insertArray['codition_id'][$i])){
						$meaasge="Condition Added Successfully";
						$this->User_model->insertdata('loan_condition_page', $condidtionArray);
					}else{
						$meaasge="Condition Updated Successfully";
						$updateWhere['codition_id'] = $insertArray['codition_id'][$i];
						$this->User_model->updatedata('loan_condition_page', $updateWhere,$condidtionArray);
					}
				}
			}
			$this->session->set_flashdata('success', $meaasge);
			$this->session->set_flashdata('popup_modal_hit', 'condition_page_section');
			redirect(base_url() . "load_data/".$loan_id."#condition_page_section", 'refresh');
		}
	}

}

?>