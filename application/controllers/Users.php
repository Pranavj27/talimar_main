<?php
class Users extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('User_model');
		$this->load->library('Aws3','aws3');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');
		if($this->session->userdata('t_user_id') != '' )
		{
		}
		else
		{
			redirect(base_url()."home",'refresh');
		}
		
		ini_set('memory_limit', '-1');
	}
	
	public function index()
	{
		$fetch_all_contact 	= $this->User_model->select_star('contact','id');
		$fetch_all_contact 	= $fetch_all_contact->result();
		$data['fetch_all_contact'] 	= $fetch_all_contact;
		//lname		

		$fetch_users 			=$this->User_model->query("SELECT * FROM `user` ORDER BY lname ASC");
		$fetch_users 			= $fetch_users->result();		
		$data['fetch_users'] 	= $fetch_users;
		$data['content'] 		= $this->load->view('users/user_create',$data,true);
		$this->load->view('template_files/template',$data);
	}
	
	public function form_user()
	{
		error_reporting(0);
		// check username
		$where['email'] = $this->input->post('username');
		
		$check_user		= $this->User_model->select_where('user',$where);
		if($check_user->num_rows() > 0)
		{
			$this->session->set_flashdata('error','Username already exists');
			redirect(base_url().'users','refresh');
			// die('stop');
		}
		
		// Check password match
		
		$password	= $this->input->post('password');
		$c_password	= $this->input->post('c_password');
		
		if($password != $c_password)
		{
			$this->session->set_flashdata('error','Password and Confirm pasword not match');
			redirect(base_url().'users','refresh');
			// die('stop');
		}
		
		if(strlen($password) < 8)
		{
			$this->session->set_flashdata('error','Password length should be minimum 8');
			redirect(base_url().'users','refresh');
			// die('stop');
		}
		
		$data['fname']			= $this->input->post('fname');
		$data['middle_name']	= $this->input->post('middle');
		$data['lname']			= $this->input->post('lname');
		$data['email']			= $this->input->post('username');
		$data['email_address']	= $this->input->post('email');
		$data['phone']			= $this->input->post('phone');
		$data['password']		= md5($this->input->post('password'));
		$data['role']			= $this->input->post('role');
		// print_r($data);
		$this->User_model->insertdata('user',$data);
		
		if($this->db->insert_id())
		{
			$this->session->set_flashdata('success','User add successfully');
			redirect(base_url().'users','refresh');
		}
	}
	
	public function form_edit_users()
	{
		$fetch_all_contact 	= $this->User_model->select_star('contact','id');
		$fetch_all_contact 	= $fetch_all_contact->result();
		$data['fetch_all_contact'] 	= $fetch_all_contact; 
		
		$where['id'] 			= $this->input->post('user_id');
		$data['fname'] 			= $this->input->post('fname');
		$data['middle_name'] 	= $this->input->post('middle_name');
		$data['lname'] 			= $this->input->post('lname');
		$data['phone'] 			= $this->input->post('phone');
		$data['email_address'] 	= $this->input->post('email_address');
		$submit 				= $this->input->post('submit');
		
		if($submit == 'Save')
		{
			$this->User_model->updatedata('user',$where,$data);
			$this->session->set_flashdata('success',' User updated');
			redirect(base_url().'users', 'refresh');
		}
		else if($submit == 'Delete')
		{
			$this->User_model->delete('user',$data);
			$this->session->set_flashdata('success',' Deleted');
			redirect(base_url().'users', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('error',' Something went wrong');
			redirect(base_url().'users', 'refresh');
		}
		
		
		
	}
	
	public function user_login_detail()
	{
		error_reporting(0);
		$user_login_detail 			= $this->User_model->select_star_desc('login_details','id');
		$user_login_detail 			= $user_login_detail->result();
		$data['user_login_detail'] 	= $user_login_detail;
		
		$user_detail = $this->User_model->select_star_desc('user','id');
		$user_detail = $user_detail->result();
		foreach($user_detail as $row)
		{
			$user_data[$row->id] = array(
											'name' 		=> $row->fname.' '.$row->middle_name.' '.$row->lname,
											'username' 	=> $row->email
											);
		}
		$data['user_data'] 	= $user_data;
		$data['content'] 	= $this->load->view('users/user_login_details',$data,true);
		$this->load->view('template_files/template',$data);
	}

	public function portal_login_detail()
	{
		error_reporting(0);

		$checkAccess = $this->fetch_setting_data_for_track();

		if($this->session->userdata('user_role') == 2 || $checkAccess[0]->lender_portal_track == 1){

			$user_login_detail 			= $this->User_model->select_star_desc('portal_login_details','id');
			$user_login_detail 			= $user_login_detail->result();
			$data['portal_login_detailss'] 	= $user_login_detail;

			$trust_deed_views 		= $this->User_model->query("SELECT * FROM `trust_deed_view` ORDER BY date DESC, time DESC");
			$trust_deed_views 			= $trust_deed_views->result();
			$data['trust_deed_views'] 	= $trust_deed_views;

			//$track_subscriptions_views 	= $this->User_model->select_star_desc('track_subscriptions','id');
			$track_subscriptions_views 	= $this->User_model->query("SELECT *, ts.id as ts_id, ts.lender_name as ts_lenderName FROM track_subscriptions as ts JOIN loan_assigment as la ON ts.talimar_loan = la.talimar_loan AND ts.lender_id = la.lender_name ORDER BY ts.datetime DESC");
			if($track_subscriptions_views->num_rows() > 0){
				$track_subscriptions_views 	= $track_subscriptions_views->result();
				$data['subscriptionsData'] 	= $track_subscriptions_views;
			}else{
				$data['subscriptionsData'] 	= '';
			}

			
			$user_detail = $this->User_model->select_star_desc('contact','contact_id');
			$user_detail = $user_detail->result();
			foreach($user_detail as $row)
			{
				$user_data[$row->contact_id] = array(
												'name' 		=> $row->contact_firstname.' '.$row->contact_lastname,
												'phone' 	=> $row->contact_phone,
												'email' 	=> $row->contact_email,
												);
			}
			$data['user_data'] 	= $user_data;

			//Assignment
			foreach($track_subscriptions_views as $assi)
			{
				$fetchAssi = $this->User_model->query("SELECT nfiles_status FROM loan_assigment WHERE talimar_loan = '".$assi->talimar_loan."' AND lender_name = '".$assi->lender_id."' ");
				$fetchAssi = $fetchAssi->result();
				$assstatus = $fetchAssi[0]->nfiles_status;

				if($assstatus == '0'){
					$statusVAl = 'Processing';
				}elseif($assstatus == '1'){
					$statusVAl = 'Complete';
				}else{
					$statusVAl = 'Processing';
				}

				$assi_data[$assi->ts_id] = $statusVAl;

			}
			$data['assi_status'] 	= $assi_data;

			$data['content'] 	= $this->load->view('users/portal_login_details',$data,true);
			$this->load->view('template_files/template',$data);

		}else{

			redirect(base_url());
		}
	}

	public function lock_accounts(){

		$t_u_id = $this->input->post('t_id');
		$value = $this->input->post('value');

		if($value == 'lock'){

			$valuess = '2';

		}elseif($value == 'unlock'){

			$valuess = '1';
		}else{
			$valuess = '2';
		}

		$update_user = $this->User_model->query("UPDATE `user` SET `account`= '".$valuess."' WHERE id = '".$t_u_id."'");
		echo '1';
	}


	public function user_settings($id){

			$setting_data = $this->fetch_setting_data();
			$data['user_setting_data'] = $setting_data;

			$data['user_datazz'] 	= '1';
			$data['content'] 	= $this->load->view('users/settings',$data,true);
			$this->load->view('template_files/template',$data);

		
	}

	public function add_setting(){

		if(isset($_POST['submit'])){

			$data['user_id']			= $this->input->post('user_id');
			$data['loan_organization'] 	= $this->input->post('loan_origination');
			$data['loan_servicing'] 	= $this->input->post('loan_servicing');
			$data['investor_relation'] 	= $this->input->post('investor_relations');
			$data['marketing'] 			= $this->input->post('marketing');
			$data['daily_report'] 		= $this->input->post('daily_report');

			$update_id			 		= $this->input->post('update_id');

			if($update_id == 'new'){

				$this->User_model->insertdata('user_settings',$data);
				$this->session->set_flashdata('success','User setting inserted successfully!');
			}else{

				$where['id'] = $update_id;
				$this->User_model->updatedata('user_settings',$where,$data);
				$this->session->set_flashdata('success','User setting updated successfully!');
			}

			
			redirect(base_url().'users');
		}
	}


	public function fetch_setting_data(){

		//$user_id = $this->session->userdata('t_user_id');
		$user_id = $this->uri->segment(2);

		$fetch_setting = $this->User_model->select_where('user_settings', array('user_id'=>$user_id));
		if($fetch_setting->num_rows() > 0){

			$fetch_setting = $fetch_setting->result();
		}else{

			$fetch_setting = '';
		}

		return $fetch_setting;
	}

	public function fetch_setting_data_for_track(){

		//$user_id = $this->session->userdata('t_user_id');
		$user_id = $this->session->userdata('t_user_id');

		$fetch_setting = $this->User_model->select_where('user_settings', array('user_id'=>$user_id));
		if($fetch_setting->num_rows() > 0){

			$fetch_setting = $fetch_setting->result();
		}else{

			$fetch_setting = '';
		}

		return $fetch_setting;
	}



	public function edit_users(){

	    $fetch_all_contact 	= $this->User_model->select_star('contact','id');
		$fetch_all_contact 	= $fetch_all_contact->result();
		$data['fetch_all_contact'] 	= $fetch_all_contact;

		$where['id']=$this->uri->segment('2');
		$fetch_users 			= $this->User_model->select_where('user',$where);
		$fetch_users 			= $fetch_users->result();
		$data['fetch_userss'] 	= $fetch_users;

          $setting_data = $this->fetch_setting_data();
		  $data['user_setting_data'] = $setting_data;
			$data['user_datazz'] 	= '1';
			$data['content'] 	= $this->load->view('users/edit_user',$data,true);
			$this->load->view('template_files/template',$data);
	}

	public function update_users(){


		$update_id['id']	    = $this->input->post('user_id');
		$id			            = $this->input->post('id');
		$data['fname']			= $this->input->post('fname');
		$data['middle_name'] 	= $this->input->post('middle_name');
		$data['lname'] 			= $this->input->post('lname');
		$data['phone'] 			= $this->input->post('phone');
		$data['email_address'] 	= $this->input->post('email_address');
		$data['role'] 			= $this->input->post('role');
		$data['account'] 		= $this->input->post('lacoount');
		$data['Ext'] 			= $this->input->post('Ext');


		$data['cell_phone'] 		= $this->input->post('cell_phone');
		$data['personal_address'] 	= $this->input->post('personal_address');
		$data['work_address'] 		= $this->input->post('work_address');
		$data['work_unit'] 			= $this->input->post('work_unit');
		$data['work_city'] 			= $this->input->post('work_city');
		$data['work_state'] 		= $this->input->post('work_state');
		$data['work_zip'] 			= $this->input->post('work_zip');
		$data['home_address'] 		= $this->input->post('home_address');
		$data['home_unit'] 			= $this->input->post('home_unit');
		$data['home_city'] 			= $this->input->post('home_city');
		$data['home_state'] 		= $this->input->post('home_state');
		$data['home_zip'] 			= $this->input->post('home_zip');
		
		$data_1['user_id']				= $this->input->post('user_id');
		$data_1['loan_organization'] 	= $this->input->post('loan_origination');
		$data_1['loan_servicing'] 		= $this->input->post('loan_servicing');
		$data_1['investor_relation'] 	= $this->input->post('investor_relations');
		$data_1['marketing'] 			= $this->input->post('marketing');
		$data_1['lender_portal_track'] 	= $this->input->post('lender_portal_track');
		$data_1['daily_report'] 		= $this->input->post('daily_report');

	    $update_id_1			 		= $this->input->post('update_idd');
        
        $data['loan_draws'] 	= $this->input->post('loan_draws');
        $data['close_out_loan_files'] 	= $this->input->post('close_out_loan_files');
        $data['remove_lender_from_loan'] = $this->input->post('remove_lender_from_loan');
        $data['mortgage_status'] = $this->input->post('mortgage_status') == 'yes'?'yes':'no';

         if($id==1){

		   $this->User_model->updatedata('user',$update_id,$data);
     

			if($update_id_1 == 'new'){

				$this->User_model->insertdata('user_settings',$data_1);
			
			}else{

				$where['id'] = $update_id_1;
				$this->User_model->updatedata('user_settings',$where,$data_1);
				
			}

		    echo "1";
	}    
	elseif($this->input->post('id')==2){

             $this->User_model->delete('user',$update_id);
             $where['id'] = $update_id_1;
             $this->User_model->delete('user_settings',$where);
			  echo "2";


	    }else{

	echo "";	
	}
 
	
 	}


 	public function changePassword()
 	{
 		$this->load->library('form_validation');

		$this->form_validation->set_rules('oldPassword', 'oldPassword', 'required|min_length[6]');
		$this->form_validation->set_rules('newPassword', 'newPassword', 'required|min_length[6]');
		$this->form_validation->set_rules('confirmPassword', 'confirmPassword Confirmation', 'required|matches[newPassword]');
		$this->form_validation->set_rules('id', 'userId', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            echo validation_errors();
        }
        else
        {

            $password 	= md5($this->input->post('newPassword'));
            $old_password = md5($this->input->post('oldPassword'));
            $id 		= $this->input->post('id');

            $userQueryC = $this->User_model->query("SELECT * FROM user WHERE id = '$id' AND password = '$old_password'")->row();
            if($userQueryC){
            	$userQuery = $this->User_model->query("UPDATE user SET password = '$password' WHERE id = '$id' AND password = '$old_password'");
	            if($userQuery)
	            {
	            	$array = array(
	            		"status" => 1,
	            		"message" => '<div class="alert alert-success alert-dismissible">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Success!</strong> Password change successfully.
							</div>'  
	            	);
	            }
	            else
	            {
	            	$array = array(
	            		"status" => 0,
	            		"message" => '<div class="alert alert-danger alert-dismissible">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Danger!</strong> Some error occuped please try again.
							</div>'  
	            	);
	            } 
            }else{
            	$array = array(
            		"status" => 0,
            		"message" => '<div class="alert alert-danger alert-dismissible">
						  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  <strong>Danger!</strong> Invalid old password.
						</div>'  
            	);
            }

            

            echo json_encode($array);           
        }    
 	}


 	public function forget_password() {

		$email = $this->input->post('email');
	
		$sql = "SELECT * FROM user WHERE email_address = '" . $email . "' ";
		$result = $this->User_model->query($sql);
		if ($result->num_rows() > 0) {
			$result_data = $result->result();

			$email_new = $email;

			$security = $this->generate_pwd(8);

			$update_id['id'] = $result_data[0]->id;
			$updation['forget_pass_request'] = '1';
			$updation['forget_pass_time'] = date('Y-m-d h:i:s');
			$updation['pass_security'] = $security;

			$subject = "Talimar Financial - Password Request";
			$header = "From: info@wartiz.com";
			$message = 'You are request for new password<br>';

			$this->User_model->updatedata('user', $update_id, $updation);

			
			/*$this->email->initialize(SMTP_SENDGRID);
			$this->email
				->from('mail@talimarfinancial.com', $subject)
				->to($email)
				->subject($subject)
				->message($message)
				->set_mailtype('html');

			$this->email->send();*/
			$dataMailA = array();
			$dataMailA['from_email'] 	= 'mail@talimarfinancial.com';
			$dataMailA['from_name'] 	= 'TaliMar Financial';
			$dataMailA['to'] 			= $email;
			$dataMailA['name'] 			= '';
			$dataMailA['subject'] 		= $subject;
			$dataMailA['content_body'] 	= $message;
			$dataMailA['button_url'] 	= base_url() . 'forget_password_secure/' . $security . '-' . $result_data[0]->id;
			$dataMailA['button_text'] 	= 'Click to link';
			send_mail_template($dataMailA);

			$array = array(
            		"status" => 1,
            		"message" => '<div class="alert alert-success alert-dismissible">
						  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  <strong>Success!</strong> Change password link send successfully.
						</div>'  
            	);

		} else {
			$array = array(
            		"status" => 0,
            		"message" => '<div class="alert alert-danger alert-dismissible">
						  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  <strong>Danger!</strong>  Change password link send failed.
						</div>'  
            	);
		}

		echo json_encode($array);
	}

	public function generate_pwd($length) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKL0123456789";
		return substr(str_shuffle($chars), 0, $length);
	}

	public function update_username() {
		$user_id 		= $this->session->userdata('t_user_id');
		$edit_username 	= $this->input->post('edit_username');

		$fetch_users    = $this->User_model->query("SELECT * FROM `user` WHERE email = '$edit_username' AND  id <> '$user_id'");
		$fetch_users    = $fetch_users->row();
		if($fetch_users){
			echo 'error';
		}else{
			$this->User_model->query("UPDATE user SET email = '$edit_username' WHERE id = '$user_id'");
			echo 'success';
		}

	}

}


