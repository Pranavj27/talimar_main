<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
	public function __construct() {
		parent::__construct();
	
		ini_set('memory_limit', '1024M');
		$this->load->model('User_model');
		$this->load->library('Aws3','aws3');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');
		
	}

	public function getError504()
	{
		for ($i=1; $i > 0 ; $i++) { 
			echo $i;
		}
	}

	public function loan_organization_setting() {

		if ($this->session->userdata('user_role') == 1) {

			$check_setting = $this->User_model->query("SELECT * FROM `user_settings` WHERE `user_id` = '" . $this->session->userdata('t_user_id') . "' AND `loan_organization`='1'");
			if ($check_setting->num_rows() > 0) {
				$loginuser_dashboard = 2;
			} else {
				$loginuser_dashboard = 1;
			}

		} else {

			$loginuser_dashboard = 1;
		}

		return $loginuser_dashboard;

	}

	public function index() {
		error_reporting(0);
		if ($this->session->userdata('t_user_id') != '') {

			$data['d'] = '';
			$data['a'] = 'aaaaaaaaaaaa';

			//fetch data according to user setting...
			$loginuser_dashboard = $this->loan_organization_setting();

			$loan_status_active['loan_status'] = '2';

			if ($loginuser_dashboard == 1) {

				$fetch_all_active = $this->User_model->select_where('loan_servicing', $loan_status_active);

			} else {

				$fetch_all_active = $this->User_model->query("select * from loan_servicing as ls JOIN loan as l ON ls.talimar_loan=l.talimar_loan JOIN loan_servicing_contacts as lsc ON lsc.talimar_loan=ls.talimar_loan where ls.loan_status='2' AND (l.loan_originator= '" . $this->session->userdata('t_user_id') . "' OR lsc.loan_originator= '" . $this->session->userdata('t_user_id') . "')");

			}
			$fetch_all_active = $fetch_all_active->result();

			$active_senior_balance_sql = "SELECT SUM(`supirior_original`) as sum_original, SUM(`supirior_current`) as sum_current FROM extra_details JOIN loan_servicing ON extra_details.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '2'";
			$active_senior_balance_result = $this->User_model->query($active_senior_balance_sql);
			$active_senior_balance_result = $active_senior_balance_result->result();

			$count = 0;
			$loan_amount = 0;
			$intrest_rate = 0;
			$arv = 0;
			$term_month = 0;
			$selling_price = 0;
			$future_value = 0;
			$current_balance = 0;
			foreach ($fetch_all_active as $active_loans) {
				$active_talimar_loan['talimar_loan'] = $active_loans->talimar_loan;

				// Fetch data from Loan
				$loan_data = $this->User_model->select_where('loan', $active_talimar_loan);
				$loan_data = $loan_data->result();
				foreach ($loan_data as $loan_row) {
					$loan_amount = $loan_amount + $loan_row->loan_amount;
					$current_balance += $loan_row->current_balance;
					$term_month = $term_month + $loan_row->term_month;
					$intrest_rate = $intrest_rate + $loan_row->intrest_rate;

				}

				// fetch property_data

				$property_data = $this->User_model->select_where('loan_property', $active_talimar_loan);
				$property_data = $property_data->result();
				foreach ($property_data as $property_row) {

					$arv = $arv + $property_row->arv;

					$future_value = $future_value + $property_row->underwriting_value;

				}

				$where_11['talimar_loan'] = $active_loans->talimar_loan;
				$fetch_loan_servicing = $this->User_model->select_where('loan_servicing', $where_11);
				$fetch_loan_servicing = $fetch_loan_servicing->result();
				foreach ($fetch_loan_servicing as $row) {
					$selling_price = $selling_price + $row->selling_price;
				}

				$count++;
				$select_ecum_total = $this->User_model->query("SELECT SUM(`current_balance`) as total_ecum FROM `encumbrances` as e JOIN property_home as ph ON e.`property_home_id` = ph.id where ph.primary_property = '1' AND e.talimar_loan = '" . $active_loans->talimar_loan . "'");
				$select_ecum_total = $select_ecum_total->result();
				$ecum_total = $select_ecum_total[0]->total_ecum;
			}

			$fetch_sum_extra = '';

			$active_loan_result = array(
				"active_count" => $count,
				"total_loan_amount" => $loan_amount,
				"total_current_balance" => $current_balance,

				"selling_price" => $selling_price,
				"total_intrest_rate" => number_format(($intrest_rate / $count),3),
				"total_arv" => $arv,
				"total_future_value" => $future_value,
				"total_term_month" => ($term_month) / $count,
				"senior_current" => $active_senior_balance_result[0]->sum_current,
				"senior_original" => $active_senior_balance_result[0]->sum_original,

			);

			$data['active_loan_result'] = $active_loan_result;

			//default loan...
			$laon_default_forculare = $this->User_model->query("SELECT * FROM loan_servicing_default INNER JOIN add_foreclosure_request ON loan_servicing_default.talimar_loan=add_foreclosure_request.talimar_loan INNER JOIN loan_servicing ON loan_servicing.talimar_loan=add_foreclosure_request.talimar_loan JOIN loan as l ON l.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status='2' AND loan_servicing.condition='1' group by loan_servicing_default.talimar_loan");

			$laon_default_forculares = $laon_default_forculare->result();
			$total_default = 0;
			$total_amount = 0;
			foreach ($laon_default_forculares as $value) {
				if($value->foreclosuer_status == '1'){
					$total_default++;
					$total_amount += $value->loan_amount;
				}
			}

			$data['fetch_total_default'] = $total_default;
			$data['fetch_total_amount'] = $total_amount;

			//  Fetch All pipeliane loan data
			$loan_status_pipeline['loan_status'] = '1';

			if ($loginuser_dashboard == 1) {
				$fetch_all_pipeline = $this->User_model->query("select * from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status IN('1','6') GROUP BY ls.talimar_loan ORDER BY ls.closing_status DESC, l.loan_funding_date ASC");
			} else {

				$fetch_all_pipeline = $this->User_model->query("select *, l.loan_originator from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan JOIN loan_servicing_contacts as lsc ON lsc.talimar_loan=ls.talimar_loan where ls.loan_status IN('1','6') AND (l.loan_originator= '" . $this->session->userdata('t_user_id') . "' OR lsc.loan_originator= '" . $this->session->userdata('t_user_id') . "') GROUP BY ls.talimar_loan ORDER BY ls.closing_status DESC, l.loan_funding_date ASC");
			}



			$fetch_all_pipeline = $fetch_all_pipeline->result();



			$pipeline_senior_balance_sql = "SELECT SUM(`supirior_original`) as sum_original, SUM(`supirior_current`) as sum_current FROM extra_details JOIN loan_servicing ON extra_details.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '1'";
			$pipeline_senior_balance_result = $this->User_model->query($pipeline_senior_balance_sql);
			$pipeline_senior_balance_result = $pipeline_senior_balance_result->result();

			$count = 0;
			$loan_amount = 0;
			$intrest_rate = 0;
			$arv = 0;
			$term_month = 0;
			$selling_price = 0;
			$underwriting_value = 0;
			$ltvvount = 0;
			$calculate_ltv_av = array();

			foreach ($fetch_all_pipeline as $pkey => $pipeline_loans) {
				$pipeline_talimar_loan['talimar_loan'] = $pipeline_loans->talimar_loan;


				$all_pipeline_loan[] = $pipeline_loans->talimar_loan;
				// Fetch data from Loan
				$loan_data = $this->User_model->query("SELECT * FROM loan WHERE talimar_loan = '".$pipeline_loans->talimar_loan."'");
				$loan_data = $loan_data->result();



				foreach ($loan_data as $loan_row) {
					$loan_amount = $loan_amount + $loan_row->loan_amount;
					$term_month = $term_month + $loan_row->term_month;
					$lender_fee = $loan_row->lender_fee;
					$intrest_rate = $intrest_rate + $loan_row->intrest_rate;

					$calculate_ltv_av['loan_amount'][$pkey] = $loan_row->loan_amount;

				}

				// fetch property_data
				$property_data = $this->User_model->select_where('loan_property', $pipeline_talimar_loan);
				$property_data = $property_data->result();

				foreach ($property_data as $property_row) {
					$arv = $arv + $property_row->arv;
					$underwriting_value = $underwriting_value + $property_row->underwriting_value;

					$calculate_ltv_av['underwriting_value'][$pkey] = $property_row->underwriting_value;
					$ltvvount++;

				}
				$where_12['talimar_loan'] = $pipeline_loans->talimar_loan;
				$fetch_loan_servicing = $this->User_model->select_where('loan_servicing', $where_12);
				$fetch_loan_servicing = $fetch_loan_servicing->result();
				foreach ($fetch_loan_servicing as $row) {
					$selling_price = $selling_price + $row->selling_price;
				}
				$count++;

			}

			// calculate LTV Avg

			$ltv_key = 0;
			$total_ltv = 0;

			foreach ($calculate_ltv_av['loan_amount'] as $ltv_val) {
				$loan_amount1 = $calculate_ltv_av['loan_amount'][$ltv_key];
				$underwriting_value = $calculate_ltv_av['underwriting_value'][$ltv_key];
				$ltv = ($loan_amount1 / $underwriting_value) * 100;

				$total_ltv = $total_ltv + $ltv;

				$ltv_key++;

			}

			$ltv_av = $total_ltv / $ltvvount;

			$pipeline_loan_result = array(
				"pipeline_count" => $count,
				"total_loan_amount" => $loan_amount,
				"lender_fee" => $lender_fee,
				"selling_price" => $selling_price,
				"total_intrest_rate" => number_format($intrest_rate,3),
				'ltv_av' => $ltv_av,
				"total_arv" => $arv,
				"total_term_month" => $term_month / $count,
				"senior_current" => $pipeline_senior_balance_result[0]->sum_current,
				"senior_original" => $pipeline_senior_balance_result[0]->sum_original,
			);

			$data['pipeline_loan_result'] = $pipeline_loan_result;

			$pipeline_loan_result = '';
			//  End of Fetch all pipeline loan data

			// fetch all pipeline loan...
			$total_pipeline_lender_fees_amount = 0;
			$uv_value = 0;
			$select_ecum = 0;
			foreach ($all_pipeline_loan as $row) {
				$borrower_name="";
				$p_talimar_loan['talimar_loan'] = $row;

				$fetch_loan_data = $fetch_loan_data = $this->User_model->query("SELECT * FROM loan WHERE talimar_loan = '".$row."'");

				$fetch_loan_data = $fetch_loan_data->result();


				$l_talimar_loan = $fetch_loan_data[0]->talimar_loan;
				$fetch_talimar['talimar_loan'] = $fetch_loan_data[0]->talimar_loan;
				$fetch_borrower['id'] = $fetch_loan_data[0]->borrower;
				$l_loan_amount = $fetch_loan_data[0]->loan_amount;

				$l_loan_type = $fetch_loan_data[0]->loan_type;
				$l_intrest_rate = $fetch_loan_data[0]->intrest_rate;
				$l_funding_date = $fetch_loan_data[0]->loan_funding_date;
				$l_lender_fee = $fetch_loan_data[0]->lender_fee;
				$month = $fetch_loan_data[0]->term_month;
				$position = $fetch_loan_data[0]->position;

				$pipeline_lender_fees_amount = $l_loan_amount * ($l_lender_fee / 100);
				$total_pipeline_lender_fees_amount = $total_pipeline_lender_fees_amount + $pipeline_lender_fees_amount;
				$l_loan_originator =  $fetch_loan_data[0]->loan_originator;
				$fetch_user_que = $this->User_model->query("SELECT fname,middle_name,lname FROM user WHERE id = '".$l_loan_originator."'");
				$fetch_user_res = $fetch_user_que->result();
				$user_full_name="";
				if(!empty($fetch_user_res[0]->fname)){
					$user_full_name=$fetch_user_res[0]->fname." ".$fetch_user_res[0]->lname;
				}
				/*if($fetch_talimar['talimar_loan']=="O1RF0A"){
					echo "l_loan_originator--- ".$user_full_name;
					die();
				}*/
				
				//fetch underwriting_value and property_address...
				$property_data = "SELECT sum(`underwriting_value`) as uv_value,property_address,unit,city,state,country,zip FROM `loan_property` WHERE `talimar_loan`='" . $l_talimar_loan . "'";

				$fetch_property_data = $this->User_model->query($property_data);
				$fetch_property_data = $fetch_property_data->result();
				$property_address = $fetch_property_data[0]->property_address;
				$full_property_address="";
				if(!empty($fetch_property_data[0]->property_address)){
					$full_property_address=$fetch_property_data[0]->property_address." ".$fetch_property_data[0]->unit."; ".$fetch_property_data[0]->city.", ".$fetch_property_data[0]->state." ".$fetch_property_data[0]->zip;
				}
				
				$uv_value = $fetch_property_data[0]->uv_value;
				$uv_value = $fetch_property_data[0]->uv_value ? $fetch_property_data[0]->uv_value : 0;

				//fetch borrower name...
				$fetch_borrower_data = $this->User_model->select_where('borrower_data', $fetch_borrower);
				if($fetch_borrower_data->num_rows() > 0){
					$fetch_borrower_data = $fetch_borrower_data->result();

					$borrower_name = $fetch_borrower_data[0]->b_name;
					$borrower_id = $fetch_borrower_data[0]->id;
				}

				//fetch closing_status...
				$fetch_loan_servicing = $this->User_model->select_where('loan_servicing', $p_talimar_loan);
				$fetch_loan_servicing = $fetch_loan_servicing->result();
				$closing_status = $fetch_loan_servicing[0]->closing_status;
				$term_sheet = $fetch_loan_servicing[0]->term_sheet;


				//fetch property home id...
				$select_property_home = $this->User_model->select_where('loan_property', $p_talimar_loan);
				$select_property_home = $select_property_home->result();
				$property_home_id = $select_property_home[0]->property_home_id;
				$lssss_talimar_loan['talimar_loan']=$l_talimar_loan;
			

			$fetch_property_homez = $this->User_model->select_where_asc('property_home', $lssss_talimar_loan, 'primary_property');
	
			$property_homesssss = $fetch_property_homez->result();
				
 			
			$property_home_idsss=array();
				foreach($property_homesssss as $key=>$vv){

					 $property_home_idsss[]=$vv->id;

				}
		
				$implode=implode("','",$property_home_idsss);

						

				 $sql = "SELECT  SUM(current_balance) as total FROM encumbrances WHERE property_home_id IN ('" . $implode . "') AND talimar_loan = '" . $l_talimar_loan . "' AND will_it_remain = 0 ";

				$select_ecums = $this->User_model->query($sql);
				$select_ecums = $select_ecums->result();
				$select_ecum = $select_ecums[0]->total ? $select_ecums[0]->total : 0;

				$sum_investment_assigment_sql = "SELECT SUM(investment) as total_investment FROM loan_assigment WHERE talimar_loan = '" . $l_talimar_loan . "' ";

				$sum_investment_assigment_result = $this->User_model->query($sum_investment_assigment_sql);
				$sum_investment_assigment_result = $sum_investment_assigment_result->result();
				$pileline_available = $sum_investment_assigment_result[0]->total_investment;

				$all_customize_pipeline_data[$closing_status][] = array(
					"loan_id" => $fetch_loan_data[0]->id,
					"talimar_loan" => $l_talimar_loan,
					"loan_amount" => $l_loan_amount,
					"l_loan_type" => $l_loan_type,
					"intrest_rate" => number_format($l_intrest_rate,3),
					"funding_date" => $l_funding_date,
					"lender_fee" => $l_lender_fee,
					"term_month" => $month,
					"position" => $position,
					"property_address" => $property_address,
					"uv_value" => $uv_value,
					"select_ecum" => $select_ecum,
					"borrower_name" => $borrower_name,
					"closing_status" => $closing_status,
					"avilable" => $l_loan_amount - $pileline_available,
					"borrower_id" => $borrower_id,
					"term_sheet" => $term_sheet,
					"full_property_address" => $full_property_address,
					"user_full_name" => $user_full_name,
					"l_loan_originator" =>$l_loan_originator,
				);

				if ($closing_status != 7) {
					$all_pipeline_data[] = array(
						"loan_id" => $fetch_loan_data[0]->id,
						"talimar_loan" => $l_talimar_loan,
						"loan_amount" => $l_loan_amount,
						"l_loan_type" => $l_loan_type,
						"intrest_rate" => number_format($l_intrest_rate,3),
						"funding_date" => $l_funding_date,
						"lender_fee" => $l_lender_fee,
						"term_month" => $month,
						"position" => $position,
						"property_address" => $property_address,
						"uv_value" => $uv_value,
						"select_ecum" => $select_ecum,
						"borrower_name" => $borrower_name,
						"closing_status" => $closing_status,
						"avilable" => $l_loan_amount - $pileline_available,
						"borrower_id" => $borrower_id,
						"term_sheet" => $term_sheet,
					);
				}
				if ($closing_status == 7) {
					$all_pipeline_data_closing_status7[] = array(
						"loan_id" => $fetch_loan_data[0]->id,
						"talimar_loan" => $l_talimar_loan,
						"loan_amount" => $l_loan_amount,
						"l_loan_type" => $l_loan_type,
						"intrest_rate" => number_format($l_intrest_rate,3),
						"funding_date" => $l_funding_date,
						"lender_fee" => $l_lender_fee,
						"term_month" => $month,
						"position" => $position,
						"property_address" => $property_address,
						"uv_value" => $uv_value,
						"select_ecum" => $select_ecum,
						"borrower_name" => $borrower_name,
						"closing_status" => $closing_status,
						"avilable" => $l_loan_amount - $pileline_available,
						"borrower_id" => $borrower_id,
						"term_sheet" => $term_sheet,
					);
				}

			}

			if (isset($all_pipeline_data_closing_status7)) {
				foreach ($all_pipeline_data_closing_status7 as $key => $row) {

					$all_pipeline_data[] = array(
						"loan_id" => $row['loan_id'],
						"talimar_loan" => $row['talimar_loan'],
						"loan_amount" => $row['loan_amount'],
						"l_loan_type" => $row['l_loan_type'],
						"intrest_rate" => number_format($row['intrest_rate'],3),
						"funding_date" => $row['funding_date'],
						"lender_fee" => $row['lender_fee'],
						"term_month" => $row['term_month'],
						"position" => $row['position'],
						"property_address" => $row['property_address'],
						"uv_value" => $row['uv_value'],
						"select_ecum" => $row['select_ecum'],
						"borrower_name" => $row['borrower_name'],
						"closing_status" => $row['closing_status'],
						"avilable" => $row['avilable'],
						"borrower_id" => $row['borrower_id'],
						"term_sheet" => $row['term_sheet'],

					);
				}
			}
			if (isset($all_customize_pipeline_data)) {
				$array = array(8, 6, 5, 4, 3, 10, 9, 2, 1, 7, 11,12, 0);
				foreach ($array as $number) {
					if (isset($all_customize_pipeline_data[$number])) {

						foreach ($all_customize_pipeline_data[$number] as $row) {

							$all_pipeline_data_new[] = array(
								"loan_id" => $row['loan_id'],
								"talimar_loan" => $row['talimar_loan'],
								"loan_amount" => $row['loan_amount'],
								"l_loan_type" => $row['l_loan_type'],
								"intrest_rate" => number_format($row['intrest_rate'],3),
								"funding_date" => $row['funding_date'],
								"lender_fee" => $row['lender_fee'],
								"term_month" => $row['term_month'],
								"position" => $row['position'],
								"property_address" => $row['property_address'],
								"uv_value" => $row['uv_value'],
								"select_ecum" => $row['select_ecum'],
								"borrower_name" => $row['borrower_name'],
								"closing_status" => $row['closing_status'],
								"avilable" => $row['avilable'],
								"borrower_id" => $row['borrower_id'],
								"term_sheet" => $row['term_sheet'],
								"full_property_address"=>$row['full_property_address'],
								"user_full_name"=>$row['user_full_name'],
								"l_loan_originator"=>$l_loan_originator
							);
						}
					}
				}
			}
			$data['all_pipeline_data'] = $all_pipeline_data_new;
			$data['total_pipeline_lender_fees_amount'] = $total_pipeline_lender_fees_amount;

			/*
				Description : This function use for dashboar data get - Update loan status condition 
				Author      : Bitcot
				Created     : 
				Modified    : 03-05-2021
			*/ 
			//loan holding schedule...
			
			/*if ($loginuser_dashboard == 1) {
				
				$holding = $this->User_model->query("
						SELECT * FROM loan_servicing AS ls 
						JOIN input_datas as ida ON ls.talimar_loan = ida.talimar_loan 
						WHERE ls.loan_status IN ('2') AND ida.input_name='checklist_complete' AND ida.page='loan_servicing_checklist' AND ida.value !='1' ");

			} else {
				
				$holding = $this->User_model->query("
					SELECT * FROM loan_servicing AS ls 
					JOIN input_datas as ida ON ls.talimar_loan = ida.talimar_loan 
					WHERE ls.loan_status IN ('2') AND ida.input_name='checklist_complete' AND ida.page='loan_servicing_checklist' AND ida.value !='1' AND ls.t_user_id = '" . $this->session->userdata('t_user_id') . "'");

			}

			if ($holding->num_rows() > 0) {

				$input_data = $holding->result();


				foreach ($input_data as $row) {

					$talimar_loan = $row->talimar_loan;

					$loan_recording = $this->User_model->query("SELECT `recorded_date` FROM `loan_recording_information` WHERE `talimar_loan` = '" . $talimar_loan . "'");
					if($loan_recording->num_rows() > 0){
						$date = $loan_recording->result();
						$rdate = $date[0]->recorded_date;

						if($rdate != ''){

							$recordingdate = date('m-d-Y', strtotime($rdate));
						}else{
							$recordingdate = 'Error';
						}
					}else{

						$recordingdate = 'Error';
					}


					$address = $this->User_model->query("select property_address from loan_property where talimar_loan = '" . $talimar_loan . "'");
					$address = $address->result();
					$p_address = $address[0]->property_address;

					$loan_status = $this->User_model->query("SELECT loan_status FROM loan_servicing WHERE talimar_loan = '" . $talimar_loan . "' ");
					$loan_status = $loan_status->result();
					$status = $loan_status[0]->loan_status;

					$current_date = date('Y-m-d');
					$recordingData = $this->User_model->query("SELECT recorded_date FROM `loan_recording_information` WHERE talimar_loan = '".$talimar_loan."'");
					if($recordingData->num_rows() > 0){

						$recordingData = $recordingData->result();
						$recorded_date = $recordingData[0]->recorded_date;

						$diff = '-'.$this->dateDiff($recorded_date, $current_date);
					}else{
						$diff = '';
					}

					$loan = $this->User_model->query("select * from loan where talimar_loan = '" . $talimar_loan . "'");
					$loan = $loan->result();
					$loan_id = $loan[0]->id;
					$amount = $loan[0]->loan_amount;
					$servicer = $loan[0]->fci;
					$closing_date = $loan[0]->loan_funding_date;
					

					$sql = "SELECT * FROM `loan_servicing_checklist` WHERE `talimar_loan`='" . $talimar_loan . "' AND type='post wire' AND hud='1008'";
					$servicing_checklist = $this->User_model->query($sql);

					if ($servicing_checklist->num_rows() > 0) {
						$servicing_checklist = $servicing_checklist->result();

					}

					$holding_again[] = array(

						"talimar_loan" => $talimar_loan,
						"loan_id" => $loan_id,
						"p_address" => $p_address,
						"status" => $status,
						"loan_amount" => $amount,
						"servicer" => $servicer,
						"closing_date" => $closing_date,
						"diff" => $diff,
						"check" => $servicing_checklist[0]->checklist,
						"recordingdate" => $recordingdate
					);
				}
				array_multisort(array_column($holding_again, 'diff'), SORT_ASC, $holding_again);
				$data['holding_loan'] = $holding_again;
			} else {
				$data['holding_loan'] = '';
			}

			//$disb_verified = $this->User_model->select_where('loan_servicing',$where_disb);
			$disb_verified = $this->User_model->query("select * from loan_servicing where servicing_disb='2' AND loan_status='2'");

			$disb_verified = $disb_verified->result();
			$count_disb = count($disb_verified);
			$data['all_count_disb'] = $count_disb;

			//FETCH MARKETING COMPLETE from loan servicing...

			$sql = "SELECT COUNT(*) as total FROM `loan_servicing` WHERE `loan_status`='2' AND `marketing_complete` !='1'";
			$fetch_marketing = $this->User_model->query($sql);
			$fetch_marketing = $fetch_marketing->result();
			$fetch_marketing = $fetch_marketing[0]->total;
			$data['fetch_marketing'] = $fetch_marketing;

			//  Monthly income servicing loan

			$join_sql = "SELECT *, loan.id as loan_id FROM loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan_servicing.loan_status = '2' ";
			$fetch_all_loan = $this->User_model->query($join_sql);
			$fetch_all_loan = $fetch_all_loan->result();
			foreach ($fetch_all_loan as $row) {
				$talimar['talimar_loan'] = $row->talimar_loan;
				$talimar_loan = $row->talimar_loan;
				$loan_id = $row->loan_id;
				$fci = $row->fci;
				$loan_amount = $row->loan_amount;
				$current_balancee = $row->current_balance;

				//  fetch data from loan servicing

				$fetch_servicing = $this->User_model->select_where('loan_servicing', $talimar);
				$fetch_servicing = $fetch_servicing->result();
				
				$fetch_loan_assigment = $this->User_model->select_where('loan_assigment', $talimar);
			    $fetch_loan_assigment = $fetch_loan_assigment->result();

				$servicing_fee = $fetch_servicing[0]->servicing_fee;
				$who_pay_servicing = $fetch_servicing[0]->who_pay_servicing;
				$sub_servicing_agent = $fetch_servicing[0]->sub_servicing_agent;
				$minimum_servicing_fee = $fetch_servicing[0]->minimum_servicing_fee;
				$broker_servicing_fees = $fetch_servicing[0]->broker_servicing_fees;
				$servicing_lender_rate = number_format($fetch_servicing[0]->servicing_lender_rate,3);
				$sql = "SELECT count(*) as countrow from loan_assigment WHERE talimar_loan = '" . $talimar_loan . "' ";
				$count_lender = $this->User_model->query($sql);
				if ($count_lender->num_rows() > 0) {
					$count_lender = $count_lender->result();
					$count_lender = $count_lender[0]->countrow;
				} else {
					$count_lender = 0;
				}

				$all_monthly_income[] = array(
					'talimar_loan' => $talimar_loan,
					'loan_id' => $loan_id,
					'fci' => $fci,
					'loan_amount' => $loan_amount,
					'current_balancee' => $current_balancee,
					'sub_servicing_agent' => $sub_servicing_agent,
					'who_pay_servicing' => $who_pay_servicing,
					'servicing_fee' => $servicing_fee,
					'minimum_servicing_fee' => $minimum_servicing_fee,
					'broker_servicing_fees' => $broker_servicing_fees,
					'count_lender' => $count_lender,
					'interest_rate' => number_format($row->intrest_rate,3),
					'servicing_lender_rate' => number_format($servicing_lender_rate,3),
					'invester_yield_percent' => $fetch_loan_assigment[0]->invester_yield_percent,

				);
			}
			
				
			$total_servicing_income = 0;
			$total_servicing_cost = 0;
			foreach ($all_monthly_income as $key => $row) {
				
				$payment_pro_fees = $all_monthly_income[$key]['loan_amount'];

				$pro_fees = '0';
				if ($payment_pro_fees > 0 && $payment_pro_fees <= 400000) {
					$pro_fees = '0';
				} elseif ($payment_pro_fees >= 400001 && $payment_pro_fees <= 500000) {
					$pro_fees = '25.00';
				} elseif ($payment_pro_fees >= 500001 && $payment_pro_fees <= 600000) {
					$pro_fees = '35.00';
				} elseif ($payment_pro_fees >= 600001 && $payment_pro_fees <= 700000) {
					$pro_fees = '45.00';
				} elseif ($payment_pro_fees >= 700001 && $payment_pro_fees <= 800000) {
					$pro_fees = '55.00';
				} elseif ($payment_pro_fees >= 800001 && $payment_pro_fees <= 900000) {
					$pro_fees = '65.00';
				} elseif ($payment_pro_fees >= 900001 && $payment_pro_fees <= 1000000) {
					$pro_fees = '75.00';
				} elseif ($payment_pro_fees >= 1000001 && $payment_pro_fees <= 2000000) {

					$pro_fees = '95.00';
				} elseif ($payment_pro_fees >= 2000001 && $payment_pro_fees <= 3000000) {

					$pro_fees = '115.00';
				} elseif ($payment_pro_fees >= 3000001 && $payment_pro_fees <= 4000000) {

					$pro_fees = '135.00';
				} elseif ($payment_pro_fees >= 4000001 && $payment_pro_fees <= 5000000) {

					$pro_fees = '155.00';
				} elseif ($payment_pro_fees >= 5000001 && $payment_pro_fees <= 6000000) {

					$pro_fees = '175.00';
				} elseif ($payment_pro_fees >= 6000001 && $payment_pro_fees <= 7000000) {

					$pro_fees = '195.00';
				} elseif ($payment_pro_fees >= 7000001 && $payment_pro_fees <= 8000000) {

					$pro_fees = '215.00';
				} elseif ($payment_pro_fees >= 8000001 && $payment_pro_fees <= 9000000) {

					$pro_fees = '235.00';
				} elseif ($payment_pro_fees >= 9000001 && $payment_pro_fees <= 10000000) {

					$pro_fees = '255.00';
				} elseif ($payment_pro_fees >= 10000001 && $payment_pro_fees <= 11000000) {

					$pro_fees = '275.00';
				} elseif ($payment_pro_fees >= 11000001 && $payment_pro_fees <= 12000000) {

					$pro_fees = '295.00';
					
				}else {

					$pro_fees = '0';
				}
				
				
				$loan_amount = $all_monthly_income[$key]['loan_amount'];
				$servicing_fee = $all_monthly_income[$key]['servicing_fee'];
				//$minimum_servicing_fee = $all_monthly_income[$key]['minimum_servicing_fee'];
				//$servicing_cost = $all_monthly_income[$key]['count_lender'] * $minimum_servicing_fee;
				
				if ($all_monthly_income[$key]['servicing_lender_rate'] == 'NaN' || $all_monthly_income[$key]['servicing_lender_rate'] == '') {

					$lender_ratessss = $all_monthly_income[$key]['invester_yield_percent'];
				} else {

					$lender_ratessss = $all_monthly_income[$key]['servicing_lender_rate'];
				}
				
				$nnew_val = $all_monthly_income[$key]['interest_rate'] - $lender_ratessss;
				$nnew_valss = $nnew_val / 100;

				$gross_servicing = ($all_monthly_income[$key]['current_balancee'] * $nnew_valss) / 12;

				$minimum_servicing_fee = $all_monthly_income[$key]['minimum_servicing_fee'];

				// $servicing_income=$gross_servicing - $servicing_cost;
				$lender_servicing_fee = $all_monthly_income[$key]['count_lender'] * $all_monthly_income[$key]['minimum_servicing_fee'];

				if ($lender_servicing_fee > $pro_fees) {

					$pro_fees = 0;

				} else {

					$lender_servicing_fee = 0;

				}
				
				$current_balance = $all_monthly_income[$key]['current_balancee'];

				$servicing_cost = $lender_servicing_fee + $pro_fees + $all_monthly_income[$key]['broker_servicing_fees'];
				$servicing_income = $gross_servicing - $servicing_cost;

				if ($all_monthly_income[$key]['who_pay_servicing'] == 2) {

					$gross_servicing = $gross_servicing;
					$servicing_cost = 0;
					$servicing_income = $servicing_income;
					$lender_servicing_fee = 0;
					$all_monthly_income[$key]['servicing_fee'] = $servicing_fee;
					$all_monthly_income[$key]['minimum_servicing_fee'] = 0;
				}
				
				$all_incomee[] = array(
				
									'gross_servicingg' => $gross_servicing,
									'current_balancee' => $current_balance,
								 );
				
				
				
				
				//extra...
				$gross_servicingxx = ($loan_amount * ($servicing_fee / 100)) / 12;
				$servicing_income = $gross_servicingxx - $servicing_cost;
			}
				
			$total_gross_servicing = 0;
			$total_amount = 0;
			foreach ($all_incomee as $key => $all_income) {
				
				$total_gross_servicing = $total_gross_servicing + $all_income['gross_servicingg'];
				$total_amount = $total_amount + $all_income['current_balancee'];
			}
			//echo $total_gross_servicing;
			//echo $total_amount;
			$data['spred_val_avg'] = number_format((($total_gross_servicing*12)/$total_amount)*100,2);

			$spread_sqll = "SELECT SUM(loan_servicing.servicing_fee) as sum_servicingg, count(*) as count_loanss FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '2' ";
			$fetch_spread_dataa = $this->User_model->query($spread_sqll);
			if ($fetch_spread_dataa->num_rows() > 0) {
				$fetch_spread_dataa = $fetch_spread_dataa->result();
				$data['fetch_spread_dataa'] = array(
					'count_loanss' => $fetch_spread_dataa[0]->count_loanss,
					'sum_servicingg' => $fetch_spread_dataa[0]->sum_servicingg,
				);
			}


			$current_date = date('d-m-Y');
			$maturity_date_30 = strtotime($current_date . ' +30 days');
			$maturity_date_30 = date("Y-m-d", $maturity_date_30);

			$fetch_property_loan = $this->User_model->query("SELECT l.talimar_loan, l.id, l.borrower, lpi.policy_number, lpi.insurance_expiration, lpi.contact_name, lpi.request_updated_insurance, lpi.insurance_type, ls.loan_status, ph.primary_property FROM loan as l JOIN loan_property_insurance as lpi ON l.talimar_loan = lpi.talimar_loan JOIN loan_servicing as ls ON ls.talimar_loan = l.talimar_loan JOIN property_home as ph ON ph.talimar_loan = l.talimar_loan WHERE ls.loan_status = 2 AND ph.primary_property = '1' AND lpi.insurance_type = '1' AND lpi.required IN ('1','0') GROUP BY l.talimar_loan");

			$fetch_property_loan = $fetch_property_loan->result_array();
			

			$data['fetch_property_loan'] = $fetch_property_loan;

			//re870_audit_report...
			$current_date = date('Y-m-d');
			$last_12_months = date('Y-m-d', strtotime('-1 year', strtotime($current_date)));

			$audit_report = $this->User_model->query("Select * from lender_contact_type where (lender_RE_date <= '" . $last_12_months . "' OR lender_RE_date = '' OR lender_RE_date IS NULL) group by contact_id");
			$audit_report = $audit_report->result();
			$count = 0;
			foreach ($audit_report as $row) {

				$contact_idd = $row->contact_id;

				$lender_active_balance = $this->User_model->query("SELECT COUNT(*) as total_re870 FROM loan_assigment as la JOIN lender_contact as lc on lc.lender_id = la.lender_name JOIN loan_servicing as ls on la.talimar_loan = ls.talimar_loan WHERE lc.contact_id = '" . $contact_idd . "' AND ls.loan_status = '2' AND lc.required_sign = '1' group by lc.contact_id");
				$lender_active_balance = $lender_active_balance->result();

				foreach ($lender_active_balance as $rows) {
					$count++;
				}

			}
			$data['re870_audit_report'] = $count;

			//Upcoming Maturity Schedule...
			//error_reporting(0);
			$current_date = date('d-m-Y');
			$maturity_date_45 = strtotime($current_date . ' +45 days');
			$maturity_date_45 = date("Y-m-d", $maturity_date_45);
			$current_date1 = date('Y-m-d');

			$upcoming = "SELECT *,Count(*) as total_upcoming FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE l.maturity_date <='" . $maturity_date_45 . "' AND ls.loan_status = '2' order by l.maturity_date";
			$fetch_upcoming = $this->User_model->query($upcoming);
			$fetch_upcoming = $fetch_upcoming->result();
			$fetch_upcoming_count = $fetch_upcoming[0]->total_upcoming;
			$data['fetch_upcoming_count'] = $fetch_upcoming_count;

			//Deliquent_property_taxes
			$sel = $this->User_model->query("SELECT COUNT(*) as total_tax FROM `loan_property_taxes` as lpt JOIN loan_servicing as ls ON lpt.talimar_loan = ls.talimar_loan WHERE lpt.`post_delinquent` = '1' AND ls.loan_status = '2'");
			$property_tax = $sel->result();
			$property_tax = $property_tax[0]->total_tax;
			$data['tax'] = $property_tax;

			//loan_assigment
			$assigment_in_process_sql = "SELECT COUNT(*) as count_assigment FROM loan_assigment JOIN loan ON loan.talimar_loan = loan_assigment.talimar_loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE  loan_servicing.loan_status IN('1','2') AND loan_assigment.secured_dot != '1' AND loan_assigment.file_close != '1' ";

			$assigment_in_process_result = $this->User_model->query($assigment_in_process_sql);
			$assigment_in_process_result = $assigment_in_process_result->result();

			$data['count_assigment_in_process'] = $assigment_in_process_result[0]->count_assigment;
			//$data['investment_assigment_in_process'] = $assigment_in_process_result[0]->investment_assigment_in_process;

			// ----------------------- Outstatnding draws table  ----------------------
			$data['outstanding_draws_count'] = 0;

			// fetch Upcoming payoff schedule amount and count
			$payoff_schdule = "SELECT COUNT(*) as count_payoff, SUM(l.loan_amount) as sum_loan_amount  FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.demand_requested = '1' AND ls.loan_status = '2'";
			$fetch_payoff_data = $this->User_model->query($payoff_schdule);
			$fetch_payoff_data = $fetch_payoff_data->row();

			$data['payoff_report_amount'] = $fetch_payoff_data->sum_loan_amount ? $fetch_payoff_data->sum_loan_amount : 0;
			$data['payoff_report_count_payoff'] = $fetch_payoff_data->count_payoff ? $fetch_payoff_data->count_payoff : 0;

			$join_sql = "SELECT *,l.id as loan_id FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.`closing_status` IN('2','3','4','5','6','8') AND ls.loan_status IN('1','2')";

			$fetch_all_loans = $this->User_model->query($join_sql);
			$fetch_all_loans = $fetch_all_loans->result();

			//echo $count = count($fetch_all_loans);

			foreach ($fetch_all_loans as $row) {

				$talimar_rows['talimar_loan'] = $row->talimar_loan;

				//fetch lender details...
				$fetch_lender = $this->User_model->query("SELECT * FROM loan_servicing as ls JOIN loan_assigment as la JOIN investor as i on ls.talimar_loan = la.talimar_loan AND la.lender_name = i.id WHERE ls.talimar_loan = '" . $row->talimar_loan . "' ");
				$results = $fetch_lender->row();

				// fetch assigment data sum of total investment
				$sql = "SELECT SUM(investment) as investment_total FROM loan_assigment WHERE talimar_loan = '" . $row->talimar_loan . "' ";
				$assigment_total_investment = $this->User_model->query($sql);
				$assigment_total_investment = $assigment_total_investment->result();
				$assigment_total_investment = $assigment_total_investment[0]->investment_total;

				// Count lender from assigment
				$sql = "SELECT COUNT(*) as count_lender FROM loan_assigment WHERE talimar_loan = '" . $row->talimar_loan . "' ";
				$count_assigment_lender = $this->User_model->query($sql);
				$count_assigment_lender = $count_assigment_lender->result();
				$count_assigment_lender = $count_assigment_lender[0]->count_lender;

				// fetch property_data

				$fetch_property_data = $this->User_model->query("select property_address from loan_property where talimar_loan = '" . $row->talimar_loan . "'");
				$fetch_property_data = $fetch_property_data->result();

				$avilable_deed_trust = $row->loan_amount - $assigment_total_investment;

				//echo $avilable_deed_trust.'<br>';
				//if($avilable_deed_trust > 0 || $row->loan_status == 1)
				if ($avilable_deed_trust > 0 && ($row->closing_status == '2' || $row->closing_status == '9' || $row->closing_status == '3' || $row->closing_status == '4' || $row->closing_status == '5' || $row->closing_status == '8' || $row->closing_status == '6')) {

					$trust_deed[] = array(
						'talimar_loan' => $row->talimar_loan,
						'loan_id' => $row->loan_id,
						'loan_amount' => $row->loan_amount,
						'intrest_rate' => number_format($row->intrest_rate,3),
						'market_trust_deed' => $row->market_trust_deed,
						'market_trust_deed' => $row->market_trust_deed,
						'trust_deed_posted_on_website' => $row->trust_deed_posted_on_website,
						'total_investment' => $assigment_total_investment,

						'comming_soon' => $row->comming_soon,
						'property_address' => $fetch_property_data[0]->property_address,
						'avilable_deed_trust' => $avilable_deed_trust,

					);
				}

			}
			*/


			$data['loginuser_dashboard'] = $this->loan_organization_setting();
			$data['trust_deed'] = $trust_deed;
			$data['content'] = $this->load->view('dashboard_view_new', $data, true);
			$this->load->view('template_files/template', $data);

		} else { //main else part...

	
			$email = $this->input->post("email");
			$password = $this->input->post("password");

			if (isset($_POST['submit'])) {

				if ($email != '' && $password != '') {
					// echo "jfvjf";
					$sql = "SELECT * FROM user WHERE email = '" . $email . "' AND password = '" . md5($password) . "' ";
					$result = $this->User_model->query($sql);
					$result1 = $result->result();
					if ($result->num_rows() > 0) {
						if ($result1[0]->account == 1) {

							foreach ($result1 as $row) {

								$user_id = $row->id;
								$fname = $row->fname;
								$lname = $row->lname;
								$role = $row->role;
								$mortgage_status = $row->mortgage_status;
							}

							$add_login['t_user_id'] = $user_id;
							$add_login['status'] = 'Logged In';
							$add_login['date'] = date('Y-m-d');
							$add_login['time'] = date('h:i:s');

							$this->User_model->insertdata('login_details', $add_login);
							
							$data['t_user_id'] = $this->session->set_userdata('t_user_id', $user_id);
							$this->session->set_userdata('user_name', $fname . ' ' . $lname);
							$this->session->set_userdata('user_role', $role);
							$this->session->set_userdata('mortgage_status', $mortgage_status);
							redirect(base_url());

						} else {

							$this->session->set_flashdata('error', 'Your account has been locked!');
							redirect(base_url());
						}

					} else {
						$this->session->set_flashdata('error', 'Username or Password is in-correct!');
						redirect(base_url());
						//$this->load->view('home_view');
					}
				} else {

					$this->session->set_flashdata('error', 'All fields are Necessary to fill it!');

					redirect(base_url());

				}
			} else {
				$this->load->view('home_view');
			}
		}

	}

	public function dateDiff($d1, $d2) {

		return round(abs(strtotime($d1) - strtotime($d2)) / 86400);

	}

	public function msort($array, $key, $sort_flags = SORT_REGULAR) {
		if (is_array($array) && count($array) > 0) {
			if (!empty($key)) {
				$mapping = array();
				foreach ($array as $k => $v) {
					$sort_key = '';
					if (!is_array($key)) {
						$sort_key = $v[$key];
					} else {
						// @TODO This should be fixed, now it will be sorted as string
						foreach ($key as $key_key) {
							$sort_key .= $v[$key_key];
						}
						$sort_flags = SORT_REGULAR;
					}
					$mapping[$k] = $sort_key;
				}
				arsort($mapping, $sort_flags);
				$sorted = array();
				foreach ($mapping as $k => $v) {
					$sorted[] = $array[$k];
				}
				return $sorted;
			}
		}
		return $array;
	}

	function investmentsortByOrder($a, $b) {
		return $a['total_investment'] - $b['total_investment'];
	}

	public function logged_out() {
		$add_login['t_user_id'] = $this->session->userdata('t_user_id');
		$add_login['status'] = 'Logged Out';
		$add_login['date'] = date('Y-m-d');
		$add_login['time'] = date('h:i:s');

		$this->User_model->insertdata('login_details', $add_login);

		$this->session->unset_userdata('t_user_id');
		$this->session->unset_userdata('user_role');
		$this->session->unset_userdata('user_name');
		//unset ($_SESSION);

		redirect(base_url());
	}

	public function forget_password() {
		$email = $this->input->post('email');
		$sql = "SELECT * FROM user WHERE email_address = '" . $email . "' ";
		$result = $this->User_model->query($sql);
		if ($result->num_rows() > 0) {
			$result_data = $result->result();

			$email_new = $email;
			$security = $this->generate_pwd(8);

			$update_id['id'] = $result_data[0]->id;
			$updation['forget_pass_request'] = '1';
			$updation['forget_pass_time'] = date('Y-m-d h:i:s');
			$updation['pass_security'] = $security;
			$url=base_url() . 'forget_password_secure/' . $security . '-' . $result_data[0]->id;
			$subject = "Talimar Financial - Password Request";
			$header = "From: info@wartiz.com";
			$message = 'You are request for new password<br>';
			$message.= 'Click to link<br>';
			$message.= '<a href="'.$url.'" > '. base_url() . 'forget_password_secure/' . $security . '-' . $result_data[0]->id.'</a>';

			$this->User_model->updatedata('user', $update_id, $updation);
			/*$this->email->initialize(SMTP_SENDGRID);
			$this->email
				->from('mail@talimarfinancial.com', $subject)
				->to($email)
				->subject($subject)
				->message($message)
				->set_mailtype('html');

			$this->email->send();*/

			$dataMailA = array();
	        $dataMailA['from_email']  	= 'mail@talimarfinancial.com';
	        $dataMailA['from_name']   	= 'TaliMar Financial';
	        $dataMailA['to']      		= $email;
	        $dataMailA['cc']      		= '';
	        $dataMailA['name']      	= '';
	        $dataMailA['subject']     	= $subject;
	        $dataMailA['content_body']  = $message;
	        $dataMailA['button_url']  	= '';
	        $dataMailA['button_text']   = '';
	        send_mail_template($dataMailA);

			$this->session->set_flashdata('success', ' Password sent on email ' . $email);
			redirect(base_url(), 'refresh');

		} else {
			$this->session->set_flashdata('error', ' Wrong email id');
			redirect(base_url(), 'refresh');
		}
	}

	public function forget_password_secure() {
		error_reporting(0);
		$data['m'] = '';
		$url_data = $this->uri->segment(2);
		if ($url_data) {
			$code = explode('-', $url_data);
			$id = $code[1];
			$secutity = $code[0];

			$user['id'] = $id;
			$user['pass_security'] = $secutity;
			$current_time = date('Y-m-d h:i:s');

			$fetch_user_data = $this->User_model->select_where('user', $user);
			if ($fetch_user_data->num_rows() > 0) {
				$fetch_user_data = $fetch_user_data->result();
				$forget_pass_time = $fetch_user_data[0]->forget_pass_time;
				$forget_pass_request = $fetch_user_data[0]->forget_pass_request;

				$to_time = $forget_pass_time;
				$from_time = $current_time;
				$minutes = round(abs($to_time - $from_time) / 60, 2);

				if ($minutes > 60) {
					$this->session->set_flashdata('error', ' Time out');
					redirect(base_url(), 'refresh');
				} else {
					$data['user_id'] = $id;
					$data['content'] = $this->load->view('new_password', $data, true);
					$this->load->view('template_files/template_forget', $data);
				}

			} else {
				$this->session->set_flashdata('error', ' Time out');
				redirect(base_url(), 'refresh');
			}

		}
	}

	public function form_create_newpassword() {
		$password = $this->input->post('password');
		$c_password = $this->input->post('c_password');
		if (strlen($password) < 8) {
			$this->session->set_flashdata('error', 'Minimum Password length should be 8');
			redirect(base_url());
		}
		if ($password != $c_password) {
			$this->session->set_flashdata('error', 'Confirm password not Match');
			redirect(base_url());
		} else {
			$id['id'] = $this->input->post('user_id');
			$update['password'] = md5($this->input->post('password'));
			$update['forget_pass_time'] = NULL;
			$update['forget_pass_request'] = 0;
			$update['pass_security'] = NULL;

			$this->User_model->updatedata('user', $id, $update);

			$this->session->set_flashdata('success', ' Password successfully changed');
			redirect(base_url(), 'refresh');
		}

	}

	public function generate_pwd($length) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKL0123456789";
		return substr(str_shuffle($chars), 0, $length);
	}

	public function form_profile_images() {
		$user_id = $this->session->userdata('t_user_id');
		// echo $user_id;
		if ($_FILES['files']) {
			$image = $_FILES['files'];
			$new_name = $user_id . "-" . rand(0, 9999) . ".jpg";
			$target_dir = "profile/images/" . $new_name; //FCPATH

			// Support images only
			$supported_image = array(
				'gif',
				'jpg',
				'jpeg',
				'png',
			);

			$src_file_name = $_FILES['files']['name'];
			$ext = strtolower(pathinfo($src_file_name, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
			if (in_array($ext, $supported_image)) {

			} else {
				$this->session->set_flashdata('error', 'Please Upload image only');
				redirect(base_url() . 'profile', 'refresh');
			}

			// Check profile pic avilablility
			$where['t_user_id'] = $this->session->userdata('t_user_id');

			$fetch_profile_images = $this->User_model->select_where('profile_image', $where);

			if ($fetch_profile_images->num_rows() > 0) {
				$target_dir_unlink = "profile/images/" . $new_name; //FCPATH
				$target_unlink = "profile/images/" . $new_name;
				// if profile  pic avilable then check same file name (create by random function) if same then chow error message
				if (file_exists($target_dir_unlink)) {
					$this->session->set_flashdata('error', ' Please try again');
					redirect(base_url() . 'profile', 'refresh');
				}

				$last_image_name = $fetch_profile_images->result();
				$last_image_name1 = $last_image_name[0]->image;

				// Delete previous images
				$target_dir_unlink2 = "profile/images/" . $last_image_name1; //FCPATH
				$target_unlink2 = "profile/images/" . $last_image_name1;

				if(getAwsUrl($target_dir_unlink2)){
					$this->aws3->deleteObject($target_unlink2);
				}
				/*if (file_exists($target_dir_unlink2)) {

					if (unlink($target_unlink2) === TRUE) {
					}
				}*/
				$data['image'] = $new_name;
				$this->User_model->updatedata('profile_image', $where, $data);
				$attachmentName=$this->aws3->uploadFile($target_dir,$_FILES["files"]["tmp_name"]);
				if($attachmentName){
				//if (move_uploaded_file($_FILES["files"]["tmp_name"], $target_dir) === TRUE) {
					$this->session->set_flashdata('success', ' Image Updated');
					redirect(base_url() . 'profile', 'refresh');
				}
			} else {
				// Else insert new profile images
				$data['image'] = $new_name;
				$data['t_user_id'] = $this->session->userdata('t_user_id');
				$this->User_model->insertdata('profile_image', $data);			
				$attachmentName=$this->aws3->uploadFile($target_dir,$_FILES["files"]["tmp_name"]);
				if($attachmentName){
					$this->session->set_flashdata('success', ' Image Updated');
					redirect(base_url() . 'profile', 'refresh');
				}
			}

		} else {
			$this->session->set_flashdata('error', ' Please choose image first');
			redirect(base_url() . 'profile', 'refresh');
		}

	}

	public function form_profile_update() {

			$data['fname'] = $this->input->post('fname');
			$data['lname'] = $this->input->post('lname');
			$data['middle_name'] = $this->input->post('middle_initial');
			$data['phone'] = $this->input->post('phone');
			$data['cell_phone'] = $this->input->post('cell_phone');
			$data['email_address'] = $this->input->post('email_address');
			$data['personal_address'] = $this->input->post('personal_address');
			$data['home_address'] = $this->input->post('home_address');
			$data['home_unit'] = $this->input->post('home_unit');
			$data['home_city'] = $this->input->post('home_city');
			$data['home_state'] = $this->input->post('home_state');
			$data['home_zip'] = $this->input->post('home_zip');
			$data['work_address'] = $this->input->post('work_address');
			$data['work_unit'] = $this->input->post('work_unit');
			$data['work_city'] = $this->input->post('work_city');
			$data['work_state'] = $this->input->post('work_state');
			$data['work_zip'] = $this->input->post('work_zip');

			$where['id'] = $this->session->userdata('t_user_id');

			$this->User_model->updatedata('user', $where, $data);

			$this->session->set_flashdata('success', 'Profile Updated successfully!');
			redirect(base_url() . 'profile', 'refresh');
	}


	public function Update_User_password(){
		
		$newpass = $this->input->post('newpass');
		$repass = $this->input->post('repass');
		$userID = $this->session->userdata('t_user_id');

		if($newpass && $repass !=''){

			if($newpass == $repass){

				$where1['id'] = $userID;
				$data1['password'] = md5($newpass);

				$this->User_model->updatedata('user', $where1, $data1);
				$this->session->set_flashdata('success', 'Password Updated successfully!');
				redirect(base_url() . 'profile', 'refresh');


			}else{

				$this->session->set_flashdata('error', 'New password and retype password not match!');
				redirect(base_url() . 'profile', 'refresh');
			}
		}else{

			$this->session->set_flashdata('error', 'All fields required!');
			redirect(base_url() . 'profile', 'refresh');
		}
	}

	public function ajax_portfolio_divertification() {
		//fetch data for Portfolio Diversification...
		error_reporting(0);
		$sql = "SELECT count(*) as counts FROM `loan` as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.loan_status='2' AND l.loan_type = '1';";
		$fetch_type_1 = $this->User_model->query($sql);
		$fetch_type_1 = $fetch_type_1->result();
		$data['fetch_type_1'] = $fetch_type_1[0]->counts;

		$sql = "SELECT count(*) as counts FROM `loan` as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.loan_status='2' AND l.loan_type = '2'";
		$fetch_type_2 = $this->User_model->query($sql);
		$fetch_type_2 = $fetch_type_2->result();
		$data['fetch_type_2'] = $fetch_type_2[0]->counts;

		$sql = "SELECT count(*) as counts FROM `loan` as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.loan_status='2' AND l.loan_type = '3'";
		$fetch_type_3 = $this->User_model->query($sql);
		$fetch_type_3 = $fetch_type_3->result();
		$data['fetch_type_3'] = $fetch_type_3[0]->counts;


		$sql4 = "SELECT count(*) as counts FROM `loan` as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.loan_status='2' AND l.loan_type = '4'";
		$fetch_type_4 = $this->User_model->query($sql4);
		$fetch_type_4 = $fetch_type_4->result();
		$data['fetch_type_4'] = $fetch_type_4[0]->counts;

		$sql = "SELECT count(*) as total FROM `loan` as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.loan_status='2'";
		$fetch_total = $this->User_model->query($sql);
		$fetch_total = $fetch_total->result();
		$data['fetch_total'] = $fetch_total[0]->total;

		$content = $this->load->view('dashboard/ajax_portfolio_divertification', $data, true);
		echo $content;

	}



	public function All_investors() {

		$fetch_investors = $this->User_model->select_star('investor');
		$fetch_investors = $fetch_investors->result();
		foreach ($fetch_investors as $value) {
			
			$Allname[$value->id] = $value->name;
		}

		return $Allname;
	}

	public function ajax_IncomingFundsdata() {

		$fetchlender = $this->User_model->query("SELECT lender_name, investment, nfunds_received, talimar_loan from loan_assigment WHERE paid_to = '2' AND nfunds_received IN('1','2')");
		if($fetchlender->num_rows() > 0){
			$fetchlender = $fetchlender->result();
			foreach($fetchlender as $rowdata){

				$fetchfunds = $this->User_model->query("SELECT l.id as loan_id, l.loan_amount, ls.loan_status FROM loan_servicing as ls JOIN loan as l ON ls.talimar_loan = l.talimar_loan WHERE l.talimar_loan = '".$rowdata->talimar_loan."'");
				$fetchfunds = $fetchfunds->result();
				$loan_status = $fetchfunds[0]->loan_status;

				//if($loan_status == '2'){

					$fundsArray[] = array(
											"lender_name" => $rowdata->lender_name,
											"talimar_loan" => $rowdata->talimar_loan,
											"loan_id" => $fetchfunds[0]->loan_id,
											"fund" => $rowdata->nfunds_received,
											"loan_amount" => $fetchfunds[0]->loan_amount,
											"investment" => $rowdata->investment,
											
										);

					$data['fundsArray'] = $fundsArray;
				//}
			}
		}

		
		$data['All_investors'] = $this->All_investors();
		$data['ex'] = '1';
		$content1 = $this->load->view('dashboard/ajax_incoming_funds', $data, true);
		echo $content1;
	}


	public function ajax_titleIncomingFundsdata() {

		$fetchlender = $this->User_model->query("SELECT lender_name, investment, nfunds_received, talimar_loan from loan_assigment WHERE paid_to = '1' AND nfunds_received IN('1','2')");
		if($fetchlender->num_rows() > 0){
			$fetchlender = $fetchlender->result();
			foreach($fetchlender as $rowdata){

				$fetchfunds = $this->User_model->query("SELECT l.id as loan_id, l.loan_amount, ls.loan_status FROM loan_servicing as ls JOIN loan as l ON ls.talimar_loan = l.talimar_loan WHERE l.talimar_loan = '".$rowdata->talimar_loan."'");
				$fetchfunds = $fetchfunds->result();
				$loan_status = $fetchfunds[0]->loan_status;

				//if($loan_status == '2'){

					$fundsArray[] = array(
											"lender_name" => $rowdata->lender_name,
											"talimar_loan" => $rowdata->talimar_loan,
											"loan_id" => $fetchfunds[0]->loan_id,
											"fund" => $rowdata->nfunds_received,
											"loan_amount" => $fetchfunds[0]->loan_amount,
											"investment" => $rowdata->investment,
											
										);

					$data['fundsArray'] = $fundsArray;
				//}
			}
		}

		
		$data['All_investors'] = $this->All_investors();
		$data['ex'] = '1';
		$content1 = $this->load->view('dashboard/ajax_title_income', $data, true);
		echo $content1;
	}
	
	
	public function ajax_MarketingNewdata() {

		$fetchmarketing = $this->User_model->query("SELECT l.id as loan_id, l.loan_amount, lp.property_address FROM loan as l join loan_servicing as ls ON l.talimar_loan = ls.talimar_loan JOIN loan_property as lp ON l.talimar_loan = lp.talimar_loan JOIN loan_servicing_checklist as lsc ON l.talimar_loan = lsc.talimar_loan WHERE ls.loan_status = '2' AND lsc.hud = '1033' AND lsc.checklist != '1' ");
		if($fetchmarketing->num_rows() > 0){

			$data['fetchmarketing'] = $fetchmarketing->result();
		}else{
			$data['fetchmarketing'] = '';
		}
		array_multisort(array_column($data['fetchmarketing'], 'loan_amount'), SORT_DESC, $data['fetchmarketing']);
		$data['ex'] = '1';
		$content1 = $this->load->view('dashboard/ajax_MarketingNew', $data, true);
		echo $content1;
	}


	public function ajax_SocialMediaBlastData() {

		$SocialMediaBlastData = $this->User_model->query("SELECT ls.talimar_loan FROM loan_servicing as ls JOIN loan_servicing_checklist as lsc ON ls.talimar_loan = lsc.talimar_loan WHERE ls.loan_status = '2' AND lsc.hud = '1017' AND lsc.checklist = '1'");
		if($SocialMediaBlastData->num_rows() > 0){

			$SocialMediaBlastData = $SocialMediaBlastData->result();
			
			foreach ($SocialMediaBlastData as $value) {

						$fetchaddressloan = $this->User_model->query("SELECT l.loan_amount, l.id as loan_id,l.loan_funding_date, lp.property_address FROM loan as l JOIN loan_property as lp ON l.talimar_loan = lp.talimar_loan WHERE l.talimar_loan = '".$value->talimar_loan."'");
						$fetchaddressloan = $fetchaddressloan->result();

						$checklistdata1 = $this->User_model->query("SELECT checklist from loan_servicing_checklist WHERE talimar_loan = '".$value->talimar_loan."' AND hud = '1027'");
						$checklistdata1 = $checklistdata1->result();
						$checkvalue = $checklistdata1[0]->checklist;

						if($checkvalue != '1'){
							$loan_funding_date ="";
							if($fetchaddressloan[0]->loan_funding_date!="" || $fetchaddressloan[0]->loan_funding_date!='0000-00-00' || $fetchaddressloan[0]->loan_funding_date!='NULL'){
								$loan_funding_date = $fetchaddressloan[0]->loan_funding_date ? date('m/d/Y', strtotime($fetchaddressloan[0]->loan_funding_date)) : '';
							}
							$all_array[] = array(
												"loan_id" => $fetchaddressloan[0]->loan_id,
												"loan_amount" => $fetchaddressloan[0]->loan_amount,
												"property_address" => $fetchaddressloan[0]->property_address,
												"loan_funding_date" => $loan_funding_date
											);

							$data['SocialMediaBlast'] = $all_array;
						}else{

							/* $data['SocialMediaBlast'] = ''; */
						}
			}
		}else{
			$data['SocialMediaBlast'] = '';
		}

		if(!empty($data['SocialMediaBlast'])){
			array_multisort(array_column($data['SocialMediaBlast'], 'loan_amount'), SORT_DESC, $data['SocialMediaBlast']);
		}
		$data['ex'] = '1';
		$content1 = $this->load->view('dashboard/ajax_Social-Media-Blast', $data, true);
		echo $content1;
	}

	public function ajax_LoanClosingPostedData() {

		$LoanClosingPostedData = $this->User_model->query("SELECT ls.talimar_loan FROM loan_servicing as ls JOIN loan_servicing_checklist as lsc ON ls.talimar_loan = lsc.talimar_loan WHERE ls.loan_status = '2' AND lsc.hud = '1038' AND lsc.checklist = '1'");
		if($LoanClosingPostedData->num_rows() > 0){

			$LoanClosingPostedData = $LoanClosingPostedData->result();
			foreach ($LoanClosingPostedData as $value) {

				$checklistdata = $this->User_model->query("SELECT talimar_loan from loan_servicing_checklist WHERE talimar_loan = '".$value->talimar_loan."' AND hud = '1032' AND checklist = '1'");
				if($checklistdata->num_rows() > 0){
					$checklistdata = $checklistdata->result();
					foreach ($checklistdata as $value1) {
						
						$checklistdata17 = $this->User_model->query("SELECT checklist from loan_servicing_checklist WHERE talimar_loan = '".$value1->talimar_loan."' AND hud = '1017'");
						$checklistdata17 = $checklistdata17->result();
						$checkvalue17 = $checklistdata17[0]->checklist;

						if($checkvalue17 !='1'){

							$fetchaddressloan = $this->User_model->query("SELECT l.loan_amount, l.id as loan_id, lp.property_address FROM loan as l JOIN loan_property as lp ON l.talimar_loan = lp.talimar_loan WHERE l.talimar_loan = '".$value1->talimar_loan."'");

							$fetchaddressloan = $fetchaddressloan->result();
						
							$all_array[] = array(
												"loan_id" => $fetchaddressloan[0]->loan_id,
												"loan_amount" => $fetchaddressloan[0]->loan_amount,
												"property_address" => $fetchaddressloan[0]->property_address,
											);

							$data['LoanClosingPosted'] = $all_array;
						}else{
							//echo 'talimar else:'.$value1->talimar_loan;
							//$data['LoanClosingPosted'] = '';
						} 

					}
				}else{
					$data['LoanClosingPosted'] = '';
				}
			}
		}else{
			$data['LoanClosingPosted'] = '';
		}

		$data['exss'] = '1';
		$content1 = $this->load->view('dashboard/ajax_loan-closing-posted', $data, true);
		echo $content1;
	}

	public function ajax_ClosingAdvertisementdata() {

		$ClosingAdvertisementsql = $this->User_model->query("SELECT ls.talimar_loan FROM loan_servicing as ls JOIN loan_servicing_checklist as lsc ON ls.talimar_loan = lsc.talimar_loan WHERE ls.loan_status = '2' AND lsc.hud = '1038' AND lsc.checklist != '1'");
		if($ClosingAdvertisementsql->num_rows() > 0){

			$ClosingAdvertisementsql = $ClosingAdvertisementsql->result();
			foreach ($ClosingAdvertisementsql as $value) {
				
				$fetchaddressloan = $this->User_model->query("SELECT l.loan_amount, l.id as loan_id, lp.property_address FROM loan as l JOIN loan_property as lp ON l.talimar_loan = lp.talimar_loan WHERE l.talimar_loan = '".$value->talimar_loan."'");

				$fetchaddressloan = $fetchaddressloan->result();

				$checklistdata = $this->User_model->query("SELECT checklist from loan_servicing_checklist WHERE talimar_loan = '".$value->talimar_loan."' AND hud = '1038'");
				$checklistdata = $checklistdata->result();
				$checkvalue = $checklistdata[0]->checklist;
				
				$all_array[] = array(
										"loan_id" => $fetchaddressloan[0]->loan_id,
										"loan_amount" => $fetchaddressloan[0]->loan_amount,
										"property_address" => $fetchaddressloan[0]->property_address,
										"checkvalue" => $checkvalue,
									);

				$data['ClosingAdvertisementsql'] = $all_array;

			}

		}else{

			$data['ClosingAdvertisementsql'] = '';
		}

		$data['ex'] = '1';
		$content1 = $this->load->view('dashboard/ClosingAdvertisement', $data, true);
		echo $content1;
	}


	public function ajax_SignNotPostedData() {

		$SignNotPostedsql = $this->User_model->query("SELECT ls.talimar_loan FROM loan_servicing as ls JOIN loan_servicing_checklist as lsc ON ls.talimar_loan = lsc.talimar_loan WHERE ls.loan_status = '2' AND lsc.hud = '1028' AND lsc.checklist != '1'");
		if($SignNotPostedsql->num_rows() > 0){

			$SignNotPostedsql = $SignNotPostedsql->result();
			foreach ($SignNotPostedsql as $value) {
				
				$fetchaddressloan = $this->User_model->query("SELECT l.loan_amount, l.id as loan_id, lp.property_address FROM loan as l JOIN loan_property as lp ON l.talimar_loan = lp.talimar_loan WHERE l.talimar_loan = '".$value->talimar_loan."'");

				$fetchaddressloan = $fetchaddressloan->result();

				$all_array[] = array(
										"loan_id" => $fetchaddressloan[0]->loan_id,
										"loan_amount" => $fetchaddressloan[0]->loan_amount,
										"property_address" => $fetchaddressloan[0]->property_address,
									);

				$data['SignNotPosted'] = $all_array;

			}

		}else{

			$data['SignNotPosted'] = '';

		}
		
		$data['ex'] = '1';
		$content1 = $this->load->view('dashboard/ajax_SignNotPosted', $data, true);
		echo $content1;

	}

	public function ajax_ClosingTombstoneData() {

		$ClosingTombstonesql = $this->User_model->query("SELECT ls.talimar_loan FROM loan_servicing as ls JOIN loan_servicing_checklist as lsc ON ls.talimar_loan = lsc.talimar_loan WHERE ls.loan_status = '2' AND lsc.hud = '1032' AND lsc.checklist != '1'");
		if($ClosingTombstonesql->num_rows() > 0){

			$ClosingTombstonesql = $ClosingTombstonesql->result();
			foreach ($ClosingTombstonesql as $value) {
				
				$fetchaddressloan = $this->User_model->query("SELECT l.loan_amount, l.id as loan_id, lp.property_address FROM loan as l JOIN loan_property as lp ON l.talimar_loan = lp.talimar_loan WHERE l.talimar_loan = '".$value->talimar_loan."'");

				$fetchaddressloan = $fetchaddressloan->result();

				$all_array[] = array(
										"loan_id" => $fetchaddressloan[0]->loan_id,
										"loan_amount" => $fetchaddressloan[0]->loan_amount,
										"property_address" => $fetchaddressloan[0]->property_address,
									);

				$data['ClosingTombstone'] = $all_array;

			}

		}else{

			$data['ClosingTombstone'] = '';

		}

		
		$data['ex'] = '1';
		$content1 = $this->load->view('dashboard/ajax_ClosingTombstone', $data, true);
		echo $content1;
	}

	public function ajax_GoogleReviewRequestData() {

		$GoogleReviewRequestsql = $this->User_model->query("SELECT ls.talimar_loan FROM loan_servicing as ls JOIN loan_servicing_checklist as lsc ON ls.talimar_loan = lsc.talimar_loan WHERE ls.loan_status = '2' AND   lsc.hud='1025' AND ((lsc.checklist='0' OR lsc.checklist='') AND (lsc.not_applicable='0' OR lsc.not_applicable='')) ");
		if($GoogleReviewRequestsql->num_rows() > 0){

			$GoogleReviewRequestsql = $GoogleReviewRequestsql->result();
			foreach ($GoogleReviewRequestsql as $value) {
				
				$fetchaddressloan = $this->User_model->query("SELECT l.loan_amount, l.id as loan_id, lp.property_address FROM loan as l JOIN loan_property as lp ON l.talimar_loan = lp.talimar_loan WHERE l.talimar_loan = '".$value->talimar_loan."'");

				$fetchaddressloan = $fetchaddressloan->result();

				$all_array[] = array(
										"loan_id" => $fetchaddressloan[0]->loan_id,
										"loan_amount" => $fetchaddressloan[0]->loan_amount,
										"property_address" => $fetchaddressloan[0]->property_address,
									);

				$data['GoogleReviewRequest'] = $all_array;

			}

		}else{

			$data['GoogleReviewRequest'] = '';

		}

		$data['ex'] = '1';
		$content1 = $this->load->view('dashboard/ajax_GoogleReviewRequest', $data, true);
		echo $content1;

	}

	public function ajax_loanclosing_checklist() {
		
		//fetch checklist
		$fetchdata = $this->User_model->query("SELECT l.loan_amount, l.id as loan_id, l.talimar_loan from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.loan_status ='2'");
		$fetchdata = $fetchdata->result();
		
		foreach($fetchdata as $row){
			
			$paddress = $this->User_model->query("SELECT lp.`property_address`, lp.unit, lp.city,lp.state,lp.zip FROM `property_home` as ph JOIN loan_property as lp On ph.talimar_loan = lp.talimar_loan Where ph.`primary_property` = '1' AND lp.talimar_loan = '".$row->talimar_loan."'");
			$paddress = $paddress->result();
			
			$fetchvalue = $this->User_model->query("SELECT value as checkvalue FROM `input_datas` WHERE `talimar_loan`='".$row->talimar_loan."' AND `page` = 'loan_servicing_checklist'");
			if($fetchvalue->num_rows() > 0){
				
				$fetchvalue = $fetchvalue->result();
				$checkval = $fetchvalue[0]->checkvalue;
			}else{
				$checkval = '0';
			}
			
			$for135 = $this->User_model->query("SELECT `talimar_loan` FROM `loan_servicing_checklist` WHERE `hud`= 135 AND `talimar_loan`= '".$row->talimar_loan."' AND (`checklist` = 1 OR `not_applicable` = 1)");
			if($for135->num_rows() > 0){
				$for135val = 'Yes';
			}else{
				$for135val = 'No';
			}
			
			$for136 = $this->User_model->query("SELECT `talimar_loan` FROM `loan_servicing_checklist` WHERE `hud`= 136 AND `talimar_loan`= '".$row->talimar_loan."' AND (`checklist` = 1 OR `not_applicable` = 1)");
			if($for136->num_rows() > 0){
				$for136val = 'Yes';
			}else{
				$for136val = 'No';
			}
			
			$for137 = $this->User_model->query("SELECT `talimar_loan` FROM `loan_servicing_checklist` WHERE `hud`= 137 AND `talimar_loan`= '".$row->talimar_loan."' AND (`checklist` = 1 OR `not_applicable` = 1)");
			if($for137->num_rows() > 0){
				$for137val = 'Yes';
			}else{
				$for137val = 'No';
			}
			
			$for138 = $this->User_model->query("SELECT `talimar_loan` FROM `loan_servicing_checklist` WHERE `hud`= 138 AND `talimar_loan`= '".$row->talimar_loan."' AND (`checklist` = 1 OR `not_applicable` = 1)");
			if($for138->num_rows() > 0){
				$for138val = 'Yes';
			}else{
				$for138val = 'No';
			}
			
			if($checkval == '' || $checkval == '0'){
			
				$checklistArray[] = array(
												"talimar_loan" => $row->talimar_loan,
												"value" => $checkval,
												"loan_id" => $row->loan_id,
												"loan_amount" => $row->loan_amount,
												"property_address" => $paddress[0]->property_address,
												"unit" => $paddress[0]->unit,
												"city" => $paddress[0]->city,
												"state" => $paddress[0]->state,
												"zip" => $paddress[0]->zip,
												"underwriting" => $for135val,
												"investor" => $for136val,
												"clouser" => $for137val,
												"serviving" => $for138val,
											);
			
				$data['checklistArray'] = $checklistArray;
			}
		}
		$data['ex'] = '1';
		$content1 = $this->load->view('dashboard/ajax_loanclosing_checklist', $data, true);
		echo $content1;

	}
	
	public function ajax_default_loans() {
		$laon_default_forculare = $this->User_model->query("SELECT loan_servicing.*,loan_servicing_default.foreclosuer_status,loan_servicing_default.foreclosuer_step FROM loan_servicing  LEFT JOIN loan_servicing_default ON loan_servicing.talimar_loan=loan_servicing_default.talimar_loan  WHERE loan_servicing.loan_status='2' AND  loan_servicing.condition='1' ");
		$loan_default_forculare = $laon_default_forculare->result();
		$data['default_loan'] = '';
		if(!empty($loan_default_forculare)){
			foreach ($loan_default_forculare as $key => $value) {
				$loginuser_dashboard = $this->loan_organization_setting();
				if ($loginuser_dashboard == 1) {
					$fetch_servicing_data = $this->User_model->query("select id,loan_amount from loan where talimar_loan='" . $value->talimar_loan . "'");
					$fetch_address_data = $this->User_model->query("select property_address from loan_property where talimar_loan='" . $value->talimar_loan . "'");
				} else {
					$fetch_servicing_data = $this->User_model->query("select id,loan_amount from loan where t_user_id='" . $this->session->userdata('t_user_id') . "' AND talimar_loan='" . $value->talimar_loan . "'");
					$fetch_address_data = $this->User_model->query("select property_address from loan_property where t_user_id='" . $this->session->userdata('t_user_id') . "' AND talimar_loan='" . $value->talimar_loan . "'");
				}
				$fetch_servicing_data = $fetch_servicing_data->result();
				$fetch_address_data = $fetch_address_data->result();
				$default_loan_array=array();
				if(!empty($fetch_servicing_data)){
					$default_loan_array[] = array(
						'loan_amount' => $fetch_servicing_data[0]->loan_amount,
						'loan_id' => $fetch_servicing_data[0]->id,
						'property_address' => $fetch_address_data[0]->property_address,
						'foreclosuer_status' => $value->foreclosuer_status,
						'foreclosuer_step' => $value->foreclosuer_step,

					);
				}
				$data['default_loan'] = $default_loan_array;
			}
		}
		$data['ex'] = '1';
		$content1 = $this->load->view('dashboard/ajax_default_loans', $data, true);
		echo $content1;

	}

	// public function ajax_default_loans()
	// {
	// 	error_reporting(0);
	// 		// Fetch All dead/default loans

	// 		$loan_status_active['loan_status'] 	= '2';
	// 		$loan_status_active['condition'] 	= '';

	// 		$loginuser_dashboard = $this->loan_organization_setting();
	// 		if($loginuser_dashboard == 1){
	// 			$fetch_servicing_data	= $this->User_model->query("select * from loan_servicing");
	// 		}else{
	// 			$loan_status_active['t_user_id'] 	= $this->session->userdata('t_user_id');
	// 			$fetch_servicing_data	= $this->User_model->select_where('loan_servicing',$loan_status_active);
	// 		}

	// 		if($fetch_servicing_data->num_rows() > 0){
	// 			$fetch_servicing_data	= $fetch_servicing_data->result();
	// 			foreach($fetch_servicing_data as $row)
	// 			{
	// 				$dead_talimar[] = $row->talimar_loan;
	// 			}

	// 			foreach($dead_talimar as $row)
	// 			{
	// 				$talimar_rows['talimar_loan'] = $row;

	// 				// Fetch loan data

	// 				$fetch_loan_data = $this->User_model->select_where('loan',$talimar_rows);
	// 				$fetch_loan_data = $fetch_loan_data->result();

	// 				// fetch sevicing data
	// 				$fetch_servicing_data 	= $this->User_model->select_where('loan_servicing',$talimar_rows);
	// 				$fetch_servicing_data	= $fetch_servicing_data->result();

	// 				// fetch property_data

	// 				$fetch_property_data = $this->User_model->select_where('loan_property',$talimar_rows);
	// 				$fetch_property_data = $fetch_property_data->result();

	// 				//default loan forclouser status...

	// 				$add_forclosure= $this->User_model->query("select * from add_foreclosure_request where talimar_loan='".$row."'  group by talimar_loan");
	// 					if($add_forclosure->num_rows()>0){
	// 				$add_forclosure_array = $add_forclosure->result();
	// 				foreach ($add_forclosure_array as $f_row) {

	// 					$loan_servicing_default = $this->User_model->query("select * from loan_servicing_default where talimar_loan='".$f_row->talimar_loan."' AND add_forclosure_id='".$f_row->id."' AND foreclosuer_status!='5' group by talimar_loan");
	// 					$loan_servicing_default = $loan_servicing_default->result();
	// 					$foreclosuer_status = $loan_servicing_default[0]->foreclosuer_status;

	// 					$all_default_loan_customize[$foreclosuer_status][] = array(
	// 											'talimar_loan' 		=> $row,
	// 											'loan_id' 			=> $fetch_loan_data[0]->id,
	// 											'loan_amount' 		=> $fetch_loan_data[0]->loan_amount,
	// 											'servicing_reason' 	=> $fetch_servicing_data[0]->reason,
	// 											'nod_record_date' 	=> $fetch_servicing_data[0]->nod_record_date,
	// 											'nod_sale_date' 	=> $fetch_servicing_data[0]->nod_sale_date,
	// 											'property_address' 	=> $fetch_property_data[0]->property_address,
	// 											'foreclosuer_status'=> $foreclosuer_status,

	// 											);

	// 					$default_loan[] = array(
	// 											'talimar_loan' 		=> $row,
	// 											'loan_id' 			=> $fetch_loan_data[0]->id,
	// 											'loan_amount' 		=> $fetch_loan_data[0]->loan_amount,
	// 											'servicing_reason' 	=> $fetch_servicing_data[0]->reason,
	// 											'nod_record_date' 	=> $fetch_servicing_data[0]->nod_record_date,
	// 											'nod_sale_date' 	=> $fetch_servicing_data[0]->nod_sale_date,
	// 											'property_address' 	=> $fetch_property_data[0]->property_address,
	// 											'foreclosuer_status'=> $foreclosuer_status,

	// 											);

	// 				}}

	// 			}

	// 			$data['default_loan'] = $default_loan;
	// 		}

	// 		$data['ex'] = '1';
	// 		$content1 = $this->load->view('dashboard/ajax_default_loans',$data,true);
	// 		echo $content1;

	// 	}

	public function ajax_servicing_companies() {

		//fetch vendor names...

		$fetch_fci_vendors = $this->User_model->query("SELECT * FROM vendors ORDER by vendor_id");
		$fetch_fci_vendors = $fetch_fci_vendors->result();
		foreach ($fetch_fci_vendors as $key => $row) {

			if (($row->vendor_name == 'FCI Lender Services')) {

				$fci_vendor_id = $row->vendor_id;
			}
			if ($row->vendor_name == 'Del Toro Loan Servicing') {

				$del_toro_vendor_id = $row->vendor_id;
			}
			if ($row->vendor_name == 'TaliMar Financial Inc.') {

				$talimar_vendor_id = $row->vendor_id;
			}
			if (($row->vendor_name == 'FCI Lender Services') || ($row->vendor_name == 'Del Toro Loan Servicing')) {

				$vendor_id[] = $row->vendor_id;
			}
		}

		$vendor_ids = implode(',', $vendor_id);

		//FCI Lender Services & Del Toro with boarding_status 1...
		//$both_loan_services_sql_with_bs = 'SELECT COUNT(*) as count_fci , SUM(loan_amount) as loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.sub_servicing_agent IN ('.$vendor_ids.') AND loan_servicing.loan_status = "2" AND loan_servicing.boarding_status  IN(1)  ';

		$both_loan_services_sql_with_bs = "SELECT COUNT(*) as count_fci , SUM(loan_amount) as loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '2' AND loan_servicing.boarding_status ='1'";

		$both_loan_services_result_with_bs = $this->User_model->query($both_loan_services_sql_with_bs);
		$both_loan_services_result_with_bs = $both_loan_services_result_with_bs->result();

		$data['count_both_loan_services_with_bs'] = $both_loan_services_result_with_bs[0]->count_fci;
		$data['both_loan_services_total_amount_with_bs'] = $both_loan_services_result_with_bs[0]->loan_amount;

		//Loan in pre-boarding...

		$loan_pre_boarding = 'SELECT COUNT(*) as count, SUM(loan_amount) as amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = "2" AND loan_servicing.boarding_status = "0"';
		$fetch_pre_boarding = $this->User_model->query($loan_pre_boarding);
		$fetch_pre_boarding = $fetch_pre_boarding->result();

		$data['count_pre_boarding_loan'] = $fetch_pre_boarding[0]->count;
		$data['amount_pre_boarding_loan'] = $fetch_pre_boarding[0]->amount;

		//FCI Lender Services & Del Toro...
		$both_loan_services_sql = 'SELECT COUNT(*) as count_fci , SUM(loan_amount) as loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.sub_servicing_agent IN (' . $vendor_ids . ') AND loan_servicing.loan_status = "2" AND loan_servicing.boarding_status  IN(2,3)  ';
		$both_loan_services_result = $this->User_model->query($both_loan_services_sql);
		$both_loan_services_result = $both_loan_services_result->result();

		$data['count_both_loan_services'] = $both_loan_services_result[0]->count_fci;
		$data['both_loan_services_total_amount'] = $both_loan_services_result[0]->loan_amount;

		//FCI Lender Services...
		/*
		Comment : Code Comment : 11-06-2021
		$fci_loan_sqls = 'SELECT COUNT(*) as count_fci , SUM(loan_amount) as fci_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.sub_servicing_agent = "' . $fci_vendor_id . '" AND loan_servicing.loan_status = "2" AND loan_servicing.boarding_status  IN(2,3)  ';*/
		/*
		date : 11-06-2021
		col Q: 1 (or bhi SQL put ki gyi hai find "col Q1")
		Date: 11-06-2021
		New code implement as per : 1200456766908287
		Condition : 
		# of Loans = Active loans with Confirm Loan has been added to Loan Servicer website checked (either check box)
        $ of Loans = Sum of Active Loans with Confirm Loan has been added to Loan Servicer website checked (either check box)
		*/
		$fci_loan_sqls = $this->User_model->query("
			SELECT COUNT(*) as count_fci , SUM(loan.current_balance) as fci_loan_amount FROM loan  
			JOIN loan_servicing as ls ON loan.talimar_loan = ls.talimar_loan 
			JOIN loan_servicing_checklist as lsc ON ls.talimar_loan = lsc.talimar_loan 
			WHERE ls.loan_status ='2' AND ls.boarding_status  IN(2,3) AND lsc.hud ='1035' AND (lsc.checklist ='1' OR lsc.not_applicable ='1')");
		//current_balance
		// $fci_loan_sqls = 'SELECT COUNT(*) as count_fci , SUM(loan_amount) as fci_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = "2" AND loan_servicing.boarding_status ="2"';

		$fci_loan_result = $this->User_model->query($fci_loan_sqls);
		$fci_loan_result = $fci_loan_result->result();

		$data['count_fci_loan'] = $fci_loan_result[0]->count_fci;
		$data['fci_loans_total_amount'] = $fci_loan_result[0]->fci_loan_amount;

		//del toro loans
		//$deltoto_loan_sql = 'SELECT COUNT(*) as count_del_toro , SUM(loan_amount) as del_toro_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.sub_servicing_agent = "'.$del_toro_vendor_id.'" AND loan_servicing.loan_status = "2" AND loan_servicing.boarding_status IN(2,3) ';

		/*
		Comment : Code Comment : 11-06-2021
		$deltoto_loan_sql = 'SELECT COUNT(*) as count_del_toro , SUM(loan_amount) as del_toro_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = "2" AND loan_servicing.boarding_status IN(2) AND loan_servicing.sub_servicing_agent = "19"';
		*/
		/*
		date : 11-06-2021
		col Q: 1 (or bhi SQL put ki gyi hai find "col Q1")
		Date: 11-06-2021
		New code implement as per : 1200456766908287
		Condition : 
		# of Loans = Active loans with Confirm Loan has been added to Loan Servicer website checked (either check box)
        $ of Loans = Sum of Active Loans with Confirm Loan has been added to Loan Servicer website checked (either check box)
		*/
		$deltoto_loan_sql = "
			SELECT COUNT(*) as count_del_toro_loan , SUM(loan.current_balance) as del_toro_loans_total_amount FROM loan  
			JOIN loan_servicing as ls ON loan.talimar_loan = ls.talimar_loan 
			JOIN loan_servicing_checklist as lsc ON ls.talimar_loan = lsc.talimar_loan 
			WHERE ls.loan_status ='2' AND ls.boarding_status  IN(2) AND ls.sub_servicing_agent = '19' AND lsc.hud ='1035' AND (lsc.checklist ='1' OR lsc.not_applicable ='1')";



		$deltoro_loan_sql_loan_result = $this->User_model->query($deltoto_loan_sql);
		$deltoro_loan_sql_loan_result = $deltoro_loan_sql_loan_result->result();

		$data['count_del_toro_loan'] = $deltoro_loan_sql_loan_result[0]->count_del_toro;
		$data['del_toro_loans_total_amount'] = $deltoro_loan_sql_loan_result[0]->del_toro_loan_amount;

		//talimar financial  loans
		/*
		Comment : Code Comment : 11-06-2021
		$talimar_loan_sql = 'SELECT COUNT(*) as count_del_toro , SUM(loan_amount) as del_toro_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.sub_servicing_agent = "' . $talimar_vendor_id . '" AND loan_servicing.loan_status = "2" AND loan_servicing.boarding_status IN(2,3) ';
		*/
		/*
		date : 11-06-2021
		col Q: 1 (or bhi SQL put ki gyi hai find "col Q1")
		Date: 11-06-2021
		New code implement as per : 1200456766908287
		Condition : 
		# of Loans = Active loans with Confirm Loan has been added to Loan Servicer website checked (either check box)
        $ of Loans = Sum of Active Loans with Confirm Loan has been added to Loan Servicer website checked (either check box)
		*/
		$talimar_loan_sql = "
			SELECT COUNT(*) as count_talimar_loan , SUM(loan.current_balance) as talimar_loans_total_amount FROM loan  
			JOIN loan_servicing as ls ON loan.talimar_loan = ls.talimar_loan 
			JOIN loan_servicing_checklist as lsc ON ls.talimar_loan = lsc.talimar_loan 
			WHERE ls.loan_status ='2' AND ls.boarding_status  IN(2,3) AND ls.sub_servicing_agent = '19' AND lsc.hud ='1035' AND (lsc.checklist ='1' OR lsc.not_applicable ='1')";


		$talimar_loan_sql_loan_result = $this->User_model->query($talimar_loan_sql);
		$talimar_loan_sql_loan_result = $talimar_loan_sql_loan_result->result();

		$data['count_talimar_loan'] = $talimar_loan_sql_loan_result[0]->count_del_toro;
		$data['talimar_loans_total_amount'] = $talimar_loan_sql_loan_result[0]->del_toro_loan_amount;

		// Loan_serviced
		$serviced_loan_sql = 'SELECT COUNT(*) as count_serviced , SUM(loan.loan_amount) as serviced_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = "2" AND loan_servicing.boarding_status = "2" AND loan_servicing.sub_servicing_agent IN (1,3)';
		$serviced_sql_loan_result = $this->User_model->query($serviced_loan_sql);
		$serviced_sql_loan_result = $serviced_sql_loan_result->result();

		$data['count_serviced_loan'] = $serviced_sql_loan_result[0]->count_serviced;
		$data['serviced_loans_total_amount'] = $serviced_sql_loan_result[0]->serviced_loan_amount;

		$content1 = $this->load->view('dashboard/ajax_servicing_companies', $data, true);
		echo $content1;

	}

	public function ajax_outstanding_draw() {

		//outstanding_draws...
		error_reporting(0);
		$loginuser_dashboard = $this->loan_organization_setting();
		if ($loginuser_dashboard == 1) {
			$select_address = $this->User_model->query("select *,l.id as loan_id from loan as l JOIN loan_property as ls ON l.talimar_loan = ls.talimar_loan join loan_servicing as lsv on lsv.talimar_loan=l.talimar_loan where lsv.loan_status = '2' GROUP BY ls.talimar_loan");
		} else {
			$select_address = $this->User_model->query("select *,l.id as loan_id from loan as l JOIN loan_property as ls ON l.talimar_loan = ls.talimar_loan join loan_servicing as lsv on lsv.talimar_loan=l.talimar_loan where l.t_user_id = '" . $this->session->userdata('t_user_id') . "' AND lsv.loan_status = '2' GROUP BY ls.talimar_loan");
		}

		if ($select_address->num_rows() > 0) {

			$select_address = $select_address->result();

			foreach ($select_address as $row) {

				$talimar_loan = $row->talimar_loan;
				$loan_id = $row->loan_id;
				$borrower = $row->borrower;
				$property_street = $row->property_address;

				$select_draw = $this->User_model->query("select * from loan_reserve_checkbox join loan_reserve_draws  on loan_reserve_checkbox.talimar_loan=loan_reserve_draws.talimar_loan where loan_reserve_draws.loan_id='" . $loan_id . "'");

				$select_draw = $select_draw->result();
				$draws_amount_1 = 0;
				$draws_amount_2 = 0;
				$draws_amount_3 = 0;
				$draws_amount_4 = 0;
				$draws_amount_5 = 0;
				$draws_amount_6 = 0;
				$draws_amount_7 = 0;
				$draws_amount_8 = 0;
				$draws_amount_9 = 0;
				$draws_amount_10 = 0;
				$draws_amount_11 = 0;
				$draws_amount_12 = 0;
				$draws_amount_13 = 0;
				$draws_amount_14 = 0;
				$draws_amount_15 = 0;

				foreach ($select_draw as $datas) {

					//$draws_amount 			= $datas->total_amount;
					$draws_amount_1 += $datas->date1_amount;
					$draws_amount_2 += $datas->date2_amount;
					$draws_amount_3 += $datas->date3_amount;
					$draws_amount_4 += $datas->date4_amount;
					$draws_amount_5 += $datas->date5_amount;
					$draws_amount_6 += $datas->date6_amount;
					$draws_amount_7 += $datas->date7_amount;
					$draws_amount_8 += $datas->date8_amount;
					$draws_amount_9 += $datas->date9_amount;
					$draws_amount_10 += $datas->date10_amount;
					$draws_amount_11 += $datas->date11_amount;
					$draws_amount_12 += $datas->date12_amount;
					$draws_amount_13 += $datas->date13_amount;
					$draws_amount_14 += $datas->date14_amount;
					$draws_amount_15 += $datas->date15_amount;

					$date1_draw_request = $datas->date1_draw_request;
					$date2_draw_request = $datas->date2_draw_request;
					$date3_draw_request = $datas->date3_draw_request;
					$date4_draw_request = $datas->date4_draw_request;
					$date5_draw_request = $datas->date5_draw_request;
					$date6_draw_request = $datas->date6_draw_request;
					$date7_draw_request = $datas->date7_draw_request;
					$date8_draw_request = $datas->date8_draw_request;
					$date9_draw_request = $datas->date9_draw_request;
					$date10_draw_request = $datas->date10_draw_request;
					$date11_draw_request = $datas->date11_draw_request;
					$date12_draw_request = $datas->date12_draw_request;
					$date13_draw_request = $datas->date13_draw_request;
					$date14_draw_request = $datas->date14_draw_request;
					$date15_draw_request = $datas->date15_draw_request;

					$date1_draw_released = $datas->date1_draw_released;
					$date2_draw_released = $datas->date2_draw_released;
					$date3_draw_released = $datas->date3_draw_released;
					$date4_draw_released = $datas->date4_draw_released;
					$date5_draw_released = $datas->date5_draw_released;
					$date6_draw_released = $datas->date6_draw_released;
					$date7_draw_released = $datas->date7_draw_released;
					$date8_draw_released = $datas->date8_draw_released;
					$date9_draw_released = $datas->date9_draw_released;
					$date10_draw_released = $datas->date10_draw_released;
					$date11_draw_released = $datas->date11_draw_released;
					$date12_draw_released = $datas->date12_draw_released;
					$date13_draw_released = $datas->date13_draw_released;
					$date14_draw_released = $datas->date14_draw_released;
					$date15_draw_released = $datas->date15_draw_released;

					$date1_draw_submit = $datas->date1_draw_submit;
					$date2_draw_submit = $datas->date2_draw_submit;
					$date3_draw_submit = $datas->date3_draw_submit;
					$date4_draw_submit = $datas->date4_draw_submit;

					$date5_draw_submit = $datas->date5_draw_submit;
					$date6_draw_submit = $datas->date6_draw_submit;
					$date7_draw_submit = $datas->date7_draw_submit;
					$date8_draw_submit = $datas->date8_draw_submit;
					$date9_draw_submit = $datas->date9_draw_submit;
					$date10_draw_submit = $datas->date10_draw_submit;
					$date11_draw_submit = $datas->date11_draw_submit;
					$date12_draw_submit = $datas->date12_draw_submit;
					$date13_draw_submit = $datas->date13_draw_submit;
					$date14_draw_submit = $datas->date14_draw_submit;
					$date15_draw_submit = $datas->date15_draw_submit;

					$date1_draw_approved = $datas->date1_draw_approved;
					$date2_draw_approved = $datas->date2_draw_approved;
					$date3_draw_approved = $datas->date3_draw_approved;
					$date4_draw_approved = $datas->date4_draw_approved;
					$date5_draw_approved = $datas->date5_draw_approved;
					$date6_draw_approved = $datas->date6_draw_approved;
					$date7_draw_approved = $datas->date7_draw_approved;
					$date8_draw_approved = $datas->date8_draw_approved;
					$date9_draw_approved = $datas->date9_draw_approved;
					$date10_draw_approved = $datas->date10_draw_approved;
					$date11_draw_approved = $datas->date11_draw_approved;
					$date12_draw_approved = $datas->date12_draw_approved;
					$date13_draw_approved = $datas->date13_draw_approved;
					$date14_draw_approved = $datas->date14_draw_approved;
					$date15_draw_approved = $datas->date15_draw_approved;

				}

				$outstanding_draws_datas[] = array(

					"loan_id" => $loan_id,
					"property_street" => $property_street,
					//	"draws_amount"			=> $draws_amount,
					"draws_amount_1" => $draws_amount_1,
					"draws_amount_2" => $draws_amount_2,
					"draws_amount_3" => $draws_amount_3,
					"draws_amount_4" => $draws_amount_4,
					"draws_amount_5" => $draws_amount_5,
					"draws_amount_6" => $draws_amount_6,
					"draws_amount_7" => $draws_amount_7,
					"draws_amount_8" => $draws_amount_8,
					"draws_amount_9" => $draws_amount_9,
					"draws_amount_10" => $draws_amount_10,
					"draws_amount_11" => $draws_amount_11,
					"draws_amount_12" => $draws_amount_12,
					"draws_amount_13" => $draws_amount_13,
					"draws_amount_14" => $draws_amount_14,
					"draws_amount_15" => $draws_amount_15,

					"date1_draw_request" => $date1_draw_request,
					"date2_draw_request" => $date2_draw_request,
					"date3_draw_request" => $date3_draw_request,
					"date4_draw_request" => $date4_draw_request,
					"date5_draw_request" => $date5_draw_request,
					"date6_draw_request" => $date6_draw_request,
					"date7_draw_request" => $date7_draw_request,
					"date8_draw_request" => $date8_draw_request,
					"date9_draw_request" => $date9_draw_request,
					"date10_draw_request" => $date10_draw_request,
					"date11_draw_request" => $date11_draw_request,
					"date12_draw_request" => $date12_draw_request,
					"date13_draw_request" => $date13_draw_request,
					"date14_draw_request" => $date14_draw_request,
					"date15_draw_request" => $date15_draw_request,

					"date1_draw_released" => $date1_draw_released,
					"date2_draw_released" => $date2_draw_released,
					"date3_draw_released" => $date3_draw_released,
					"date4_draw_released" => $date4_draw_released,
					"date5_draw_released" => $date5_draw_released,
					"date6_draw_released" => $date6_draw_released,
					"date7_draw_released" => $date7_draw_released,
					"date8_draw_released" => $date8_draw_released,
					"date9_draw_released" => $date9_draw_released,
					"date10_draw_released" => $date10_draw_released,
					"date11_draw_released" => $date11_draw_released,
					"date12_draw_released" => $date12_draw_released,
					"date13_draw_released" => $date13_draw_released,
					"date14_draw_released" => $date14_draw_released,
					"date15_draw_released" => $date15_draw_released,

					"date1_draw_submit" => $date1_draw_submit,
					"date2_draw_submit" => $date2_draw_submit,
					"date3_draw_submit" => $date3_draw_submit,
					"date4_draw_submit" => $date4_draw_submit,

					"date5_draw_submit" => $date5_draw_submit,
					"date6_draw_submit" => $date6_draw_submit,
					"date7_draw_submit" => $date7_draw_submit,
					"date8_draw_submit" => $date8_draw_submit,
					"date9_draw_submit" => $date9_draw_submit,
					"date10_draw_submit" => $date10_draw_submit,
					"date11_draw_submit" => $date11_draw_submit,
					"date12_draw_submit" => $date12_draw_submit,
					"date13_draw_submit" => $date13_draw_submit,
					"date14_draw_submit" => $date14_draw_submit,
					"date15_draw_submit" => $date15_draw_submit,

					"date1_draw_approved" => $date1_draw_approved,
					"date2_draw_approved" => $date2_draw_approved,
					"date3_draw_approved" => $date3_draw_approved,
					"date4_draw_approved" => $date4_draw_approved,

					"date5_draw_approved" => $date5_draw_approved,
					"date6_draw_approved" => $date6_draw_approved,
					"date7_draw_approved" => $date7_draw_approved,
					"date8_draw_approved" => $date8_draw_approved,
					"date9_draw_approved" => $date9_draw_approved,
					"date10_draw_approved" => $date10_draw_approved,
					"date11_draw_approved" => $date11_draw_approved,
					"date12_draw_approved" => $date12_draw_approved,
					"date13_draw_approved" => $date13_draw_approved,
					"date14_draw_approved" => $date14_draw_approved,
					"date15_draw_approved" => $date15_draw_approved,

				);
			}

			// echo '<pre>';
			// print_r($outstanding_draws_datas);
			// echo '</pre>';

			$data['outstanding_draws'] = $outstanding_draws_datas;
		}
		$content1 = $this->load->view('dashboard/ajax_outstanding_draw', $data, true);
		echo $content1;
	}




	public function ajax_construction_loan() {

		// Fetch Construction loan data
		$construction_sql = "SELECT COUNT(*) as count_loan, SUM(loan_amount) as total_loan_amount FROM loan WHERE draws = '1'";
		$fetch_construction_loans = $this->User_model->query($construction_sql);
		$fetch_construction_loans = $fetch_construction_loans->result();
		$data['count_construction_loans'] = $fetch_construction_loans[0]->count_loan;
		$data['construction_total_loan_amount'] = $fetch_construction_loans[0]->total_loan_amount;

		$totalReserve = $this->User_model->query("SELECT SUM(`amount`) as totalAmount FROM `loan_draw_schedule`");
		$totalReserve = $totalReserve->result();
		$data['budget'] = $totalReserve[0]->totalAmount;


		$totalReservereq = $this->User_model->query("SELECT SUM(`amount_Req`) as reqAmount FROM `loan_draw_released`");
		$totalReservereq = $totalReservereq->result();
		$data['total_draws_amount'] = $totalReservereq[0]->reqAmount;


		$content1 = $this->load->view('dashboard/ajax_construction_loan', $data, true);
		echo $content1;
	}

	public function AssignmentProcessData() {
		$fetchAssdatass = array();
		$fetchAssdata = $this->User_model->query("SELECT la.talimar_loan, la.lender_name, la.investment, la.nfunds_received, la.recording_status, la.nservicer_status, la.ndisclouser_status FROM loan_assigment as la JOIN loan_servicing as ls ON la.talimar_loan = ls.talimar_loan WHERE ls.loan_status = '2' AND la.nsecurity_type = '2' AND nfiles_status = '0' ORDER BY ls.talimar_loan ASC");
		if($fetchAssdata->num_rows() > 0){
			$fetchAssdata = $fetchAssdata->result();



			foreach ($fetchAssdata as $value) {
				
				$fetchadd = $this->User_model->query("SELECT property_address, loan_id FROM loan_property WHERE talimar_loan = '".$value->talimar_loan."'");
				$fetchadd = $fetchadd->row();
				$fulladd = $fetchadd->property_address;
				$loan_id = $fetchadd->loan_id;
				$lender_id = $value->lender_name;

				if(!empty($value->talimar_loan)){
					//call to signNo API
					$EnvolopID = $this->User_model->query("SELECT * FROM `signnow_api_doc` WHERE talimar_loan = '".$value->talimar_loan."' AND lender_id = '" . $lender_id . "' ORDER BY id DESC");

					if($EnvolopID->num_rows() > 0){
						$EnvolopID = $EnvolopID->row();
						$DocumentData = $this->GetDocusignAPIDocs($EnvolopID->documentID);

						if($DocumentData->status != '')
						{
							$value->ndisclouser_status = $DocumentData->status;
							if($DocumentData->status == "completed")
							{
								$value->ndisclouser_status = "Signed";

							}elseif($DocumentData->status == "sent")
							{
									$value->ndisclouser_status = "Sent";
							}
							else
							{
									$value->ndisclouser_status = "Not Ready";
							}
						}
						else
						{
							$value->ndisclouser_status = "Not Ready";
						}
					}
					else
					{
						$value->ndisclouser_status = "Not Ready";
					}	
				}
				else
				{
					$value->ndisclouser_status = "Not Ready";
				}


				$fetchAssdatass[] = array(
											"lender_name" => $value->lender_name,
											"investment" => $value->investment,
											"nfunds_received" => $value->nfunds_received,
											"recording_status" => $value->recording_status,
											"nservicer_status" => $value->nservicer_status,
											"ndisclouser_status" => $value->ndisclouser_status,
											"fulladd" => $fulladd,
											"loan_id" => $loan_id,

										);
			}

			$data['AssignmentsProData'] = $fetchAssdatass;
		}else{
			$data['AssignmentsProData'] = $fetchAssdatass;
		}

		$data['investorsName'] = $this->All_investors();

		$content1 = $this->load->view('dashboard/AssignmentProcess', $data, true);
		echo $content1;
	}

	public function GetDocusignAPIDocs($envID){

		// Client live(production) info here:
		/*$integratorKey = '300ee20a-8530-4522-aa84-4ba09da14264';
		$email = 'bvandenberg@talimarfinancial.com';
		//$password = '797988Aa!!'; //for demo
		$password = '797988Aa!!!';*/

		$integratorKey = DOCUSIGN_INTEGRATOR_KEY;
		$email = DOCUSIGN_EMAIL;
		$password = DOCUSIGN_PASSWORD;
		$baseUrl 	= BASE_URL;

		// construct the authentication header:
		$header = "<DocuSignCredentials><Username>" . $email . "</Username><Password>" . $password . "</Password><IntegratorKey>" . $integratorKey . "</IntegratorKey></DocuSignCredentials>";

		/////////////////////////////////////////////////////////////////////////////////////////////////
		
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $baseUrl.'/envelopes/'.$envID,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
		    'Accept: application/json',
		    "X-DocuSign-Authentication: $header"
		  ),
		));

		$response = curl_exec($curl);
		$responsesss = json_decode($response);

		return $responsesss;
	
	}

	public function value() {

		$fetchAssdata = $this->User_model->query("SELECT la.talimar_loan, la.lender_name, la.investment, la.nfunds_received, la.recording_status, la.nservicer_status, la.ndisclouser_status FROM loan_assigment as la JOIN loan_servicing as ls ON la.talimar_loan = ls.talimar_loan WHERE ls.loan_status = '2' AND la.nsecurity_type = '2' AND nfiles_status = '0' ORDER BY ls.talimar_loan ASC");
		if($fetchAssdata->num_rows() > 0){
			$fetchAssdata = $fetchAssdata->result();

			foreach ($fetchAssdata as $value) {
				
				$fetchadd = $this->User_model->query("SELECT property_address, loan_id FROM loan_property WHERE talimar_loan = '".$value->talimar_loan."'");
				$fetchadd = $fetchadd->row();
				$fulladd = $fetchadd->property_address;
				$loan_id = $fetchadd->loan_id;
				$lender_id = $value->lender_name;

				if(!empty($value->talimar_loan)){
					//call to signNo API
					$EnvolopID = $this->User_model->query("SELECT * FROM `signnow_api_doc` WHERE talimar_loan = '".$value->talimar_loan."' AND lender_id = '" . $lender_id . "' ORDER BY id DESC");

					if($EnvolopID->num_rows() > 0){
						$EnvolopID = $EnvolopID->row();
						$DocumentData = $this->GetDocusignAPIDocs($EnvolopID->documentID);
						if(!empty($DocumentData->status))
						{
							$value->ndisclouser_status = $DocumentData->status;
							if($value->EnvolopStatus == "completed")
							{
								$value->ndisclouser_status = "Signed";

							}elseif($value->EnvolopStatus == "sent")
							{
									$value->ndisclouser_status = "Sent";
							}
							else
							{
									$value->ndisclouser_status = "Not Ready";
							}
						}
						else
						{
							$value->ndisclouser_status = "Not Ready";
						}
					}
					else
					{
						$value->ndisclouser_status = "Not Ready";
					}	
				}
				else
				{
					$value->ndisclouser_status = "Not Ready";
				}


				$fetchAssdatass[] = array(
											"lender_name" => $value->lender_name,
											"investment" => $value->investment,
											"nfunds_received" => $value->nfunds_received,
											"recording_status" => $value->recording_status,
											"nservicer_status" => $value->nservicer_status,
											"ndisclouser_status" => $value->ndisclouser_status,
											"fulladd" => $fulladd,
											"loan_id" => $loan_id,

										);
			}

			$data['AssignmentsProData'] = $fetchAssdatass;
		}else{
			$data['AssignmentsProData'] = '';
		}

		$data['investorsName'] = $this->All_investors();

		$content1 = $this->load->view('dashboard/AssignmentProcess', $data, true);
		echo $content1;
	}



	public function ajax_assignment_process() {

		// Assignments in Process

		/* fetch data that are Active.pipeliane loans and don’t have File Closed from loan assignments... */

		$fetch_assignment = $this->User_model->query("SELECT s.loan_status, l.id, l.talimar_loan, a.lender_name, a.file_close , v.name, ph.property_address, ph.primary_property FROM `loan_assigment` as a JOIN loan_servicing as s JOIN loan as l JOIN investor as v JOIN property_home as ph on a.talimar_loan = s.talimar_loan AND a.talimar_loan = l.talimar_loan AND a.talimar_loan = ph.talimar_loan AND v.id = a.lender_name WHERE s.loan_status IN(1,2)  AND a.file_close != '1' AND ph.primary_property = '1' ORDER BY l.talimar_loan");

		$fetch_assignment = $fetch_assignment->result();

		$data['assignment_in_process'] = $fetch_assignment;

		$content1 = $this->load->view('dashboard/ajax_assignment_process', $data, true);
		echo $content1;
	}

	public function ajax_maturity_schedule() {

		// Maturity 45 days schedules
		error_reporting(0);
		$current_date = date('d-m-Y');
		$maturity_date_45 = strtotime($current_date . ' +45 days');
		$maturity_date_45 = date("Y-m-d", $maturity_date_45);
		$current_date1 = date('Y-m-d');

		$loginuser_dashboard = $this->loan_organization_setting();
		if ($loginuser_dashboard == 1) {
			$sql = "SELECT * FROM loan WHERE new_maturity_date <='" . $maturity_date_45 . "' order by maturity_date";
		} else {
			$sql = "SELECT * FROM loan WHERE new_maturity_date <='" . $maturity_date_45 . "' AND t_user_id = '" . $this->session->userdata('t_user_id') . "' order by maturity_date";
		}

		$fetch_loan_data = $this->User_model->query($sql);
		if ($fetch_loan_data->num_rows() > 0) {
			$fetch_loan_data = $fetch_loan_data->result();

			foreach ($fetch_loan_data as $row) {

				$talimar_loan = $row->talimar_loan;
				$loan_id = $row->id;
				$fci = $row->fci;
				$loan_amount = $row->loan_amount;
				$maturity_date = $row->new_maturity_date;
				$loan_document_date = $row->loan_document_date;
				$borrower_id['id'] = $row->borrower;

				$myDateTime = DateTime::createFromFormat('Y-m-d', $maturity_date);
				$newDateString = $myDateTime->format('d-m-Y');
				$now = time(); // or your date as well
				$your_date = strtotime($newDateString);
				$datediff = $your_date - $now;
				$duration = floor($datediff / (60 * 60 * 24));
				$talimar_maturity['talimar_loan'] = $row->talimar_loan;

				// FETCH Servicing data
				$fetch_servicing_data = $this->User_model->select_where('loan_servicing', $talimar_maturity);

				$fetch_servicing_data = $fetch_servicing_data->result();

				$loan_status = $fetch_servicing_data[0]->loan_status;

				$ex_borrower = $fetch_servicing_data[0]->extension_borrower;
				$letter_sent = $fetch_servicing_data[0]->extension_letter_sent;
				$sign_letter = $fetch_servicing_data[0]->signed_extension_letter;
				$fee_received = $fetch_servicing_data[0]->extension_fee_received;
				$sent_servicer = $fetch_servicing_data[0]->extension_request_sent_servicer;

				$property = $this->User_model->query("Select property_address from loan_property where talimar_loan = '" . $row->talimar_loan . "'");
				$property = $property->result();
				$s_property = $property[0]->property_address;

				// echo $duration.' - '.$maturity_date.' - '.$loan_status.'<br>';

				if ($duration < 46 && $loan_status == 2) {

					$maturity_report_data[] = array(
						'talimar_loan' => $talimar_loan,
						'loan_id' => $loan_id,
						'fci' => $fci,
						'loan_amount' => $loan_amount,
						'maturity_date' => date('m-d-Y', strtotime($maturity_date)),
						'duration' => $duration+1,
						's_property' => $s_property,

					);
				}
			}
			uasort($maturity_report_data, function ($item, $compare) {
			    return $item['duration'] >= $compare['duration']; 
			});
			$data['maturity_report_data'] = $maturity_report_data;
		}
		$content1 = $this->load->view('dashboard/ajax_maturity_schedule', $data, true);
		echo $content1;

	}

	public function ajax_portfolio_statistics() {

		//Number of lenders in active loans
		error_reporting(0);
		$lender_count_sql = 'SELECT COUNT(*) as lender_count FROM loan_assigment JOIN loan_servicing ON loan_servicing.talimar_loan = loan_assigment.talimar_loan WHERE loan_servicing.loan_status = "2" ';
		$lender_active_count_result = $this->User_model->query($lender_count_sql);
		$lender_active_count_result = $lender_active_count_result->result();

		$lender_count_active = $lender_active_count_result[0]->lender_count;
		$data['lender_count_active'] = $lender_count_active;

		// count number of paid off loans....

		$paid_off_sql = $this->User_model->query("SELECT l.id, l.talimar_loan, ls.loan_status, l.loan_amount FROM loan as l JOIN loan_servicing as ls on l.talimar_loan = ls.talimar_loan WHERE ls.loan_status = '3' ");
		$paid_off_sql_result = $paid_off_sql->result();

		$paid_off_sum = 0;
		foreach ($paid_off_sql_result as $result) {
			$paid_off_sum += $result->loan_amount;

		}

		$data['paid_off_loans'] = count($paid_off_sql_result);
		$data['paid_off_loans_amt'] = number_format($paid_off_sum);

		// count/Sum number of active, paid off AND Brokered loans....
		$loans_funded_sql = $this->User_model->query("SELECT l.id, l.talimar_loan, ls.loan_status, l.loan_amount FROM loan as l JOIN loan_servicing as ls on l.talimar_loan = ls.talimar_loan WHERE ls.loan_status IN ('2','3','5') ");
		$loans_funded_sql_result = $loans_funded_sql->result();

		$loans_funded_sum = 0;
		foreach ($loans_funded_sql_result as $result) {
			$loans_funded_sum += $result->loan_amount;

		}

		$data['loans_funded_loans'] = count($loans_funded_sql_result);
		$data['loans_funded_loans_amt'] = number_format($loans_funded_sum);

		//Fetch Spread loan

		$spread_sql = "SELECT SUM(loan_servicing.servicing_fee) as sum_servicing, count(*) as count_loans FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '2' ";
		$fetch_spread_data = $this->User_model->query($spread_sql);
		if ($fetch_spread_data->num_rows() > 0) {
			$fetch_spread_data = $fetch_spread_data->result();
			$data['fetch_spread_data'] = array(
				'count_loans' => $fetch_spread_data[0]->count_loans,
				'sum_servicing' => $fetch_spread_data[0]->sum_servicing,
			);
		}

		//Fetch all active data
		$loan_status_active['loan_status'] = '2';
		$fetch_all_active = $this->User_model->select_where('loan_servicing', $loan_status_active);
		$fetch_all_active = $fetch_all_active->result();

		$count = 0;
		$loan_amount = 0;

		foreach ($fetch_all_active as $active_loans) {
			$active_talimar_loan['talimar_loan'] = $active_loans->talimar_loan;

			$loan_data = $this->User_model->select_where('loan', $active_talimar_loan);
			$loan_data = $loan_data->result();
			foreach ($loan_data as $loan_row) {
				$loan_amount = $loan_amount + $loan_row->loan_amount;

			}

			$count++;

		}

		$active_loan_result = array(
			"active_count" => $count,
			"total_loan_amount" => $loan_amount,

		);

		$data['active_loan_result'] = $active_loan_result;

		// count number of active AND paid off loans....

		$first_day_this_year = date("Y-m-d", strtotime('first day of January ' . date('Y')));

		$last_day_this_year = date('Y-m-d', strtotime('12/31'));

		$loans_funded_sqll = $this->User_model->query("SELECT *,SUM(loan_amount) as total_amount FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where l.loan_funding_date BETWEEN '" . $first_day_this_year . "' AND '" . $last_day_this_year . "' AND ls.loan_status IN(2,3,5) AND ls.closing_status='6'");
		$loan_funded_sqll_result = $loans_funded_sqll->result();
		$loan_funded_amount = $loan_funded_sqll_result[0]->total_amount;

		$data['loans_funded_loans_amt_in_current_year'] = number_format($loan_funded_amount);
		$query2 = "SELECT * FROM loan_assigment JOIN loan on loan.talimar_loan = loan_assigment.talimar_loan JOIN  loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan  WHERE loan_servicing.loan_status='2' group by loan_assigment.lender_name";

		$fetch_active_lender_loan_data = $this->User_model->query($query2);
		$fetch_active_lender_loan_data = $fetch_active_lender_loan_data->result();
		$lender_count = 0;
		foreach ($fetch_active_lender_loan_data as $roww) {

			$lender_count++;
		}

		$data['total_lender_count'] = $lender_count;

		//count single lender loans...

		$sql = "SELECT COUNT(*) as count_lenders, loan.loan_amount as loan_amount FROM loan_assigment JOIN loan on loan.talimar_loan = loan_assigment.talimar_loan JOIN loan_servicing on loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '2' GROUP BY loan_assigment.talimar_loan";
		$count_single_lender = 0;
		$amount_single_lender = 0;
		$fetch_single_lender = $this->User_model->query($sql);
		if ($fetch_single_lender->num_rows() > 0) {
			$fetch_single_lender = $fetch_single_lender->result();
			foreach ($fetch_single_lender as $row) {
				if ($row->count_lenders == 1) {
					$count_single_lender++;
					$amount_single_lender += $row->loan_amount;
				}
			}
		}

		$data['count_single_lender'] = $count_single_lender;
		$data['amount_single_lender'] = $amount_single_lender;

		$content1 = $this->load->view('dashboard/ajax_portfolio_statistics', $data, true);
		echo $content1;

	}

//..............................ajax year to date statistics start ................................................//

	public function ajax_year_to_date_statistics() {
		error_reporting(0);
		$first_day_this_month = date('Y-m-01');
		$last_day_this_month = date('Y-m-t');

		$first_day_this_year = date("Y-m-d", strtotime('first day of January ' . date('Y')));

		$last_day_this_year = date('Y-m-d', strtotime('12/31'));

		$loans_funded_sqll = $this->User_model->query("SELECT count(*) as countt_year,SUM(loan_amount) as total_amount FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where l.loan_funding_date BETWEEN '" . $first_day_this_year . "' AND '" . $last_day_this_year . "' AND ls.loan_status IN(2,3,5) AND ls.closing_status='6'");
		$loan_funded_sqll_result = $loans_funded_sqll->result();

		$loan_funded_amount = $loan_funded_sqll_result[0]->total_amount;
		$countt_year = $loan_funded_sqll_result[0]->countt_year;

		$loans_funded_sqll2 = $this->User_model->query("SELECT count(*) as countt_month,SUM(loan_amount) as total_amount FROM loan as l JOIN loan_servicing  as ls ON l.talimar_loan = ls.talimar_loan where l.loan_funding_date BETWEEN '" . $first_day_this_month . "' AND '" . $last_day_this_month . "' AND ls.closing_status='6'");

		$loan_funded_sqll_result2 = $loans_funded_sqll2->result();
		$loan_funded_amount2 = $loan_funded_sqll_result2[0]->total_amount;

		$countt_month = $loan_funded_sqll_result2[0]->countt_month;

		$loans_funded_sqll3 = $this->User_model->query("SELECT count(*) as hash_counttt,SUM(l.loan_amount) as total_amount FROM loan as l JOIN loan_servicing  as ls ON l.talimar_loan = ls.talimar_loan where  ls.loan_status IN(3) ");

		$loan_funded_sqll_result3 = $loans_funded_sqll3->result();
		$hash_off_paid_off = $loan_funded_sqll_result3[0]->hash_counttt;
		$total_amount_off_paid_off = $loan_funded_sqll_result3[0]->total_amount;

		$data['loans_funded_loans_amt_in_current_year'] = number_format($loan_funded_amount);
		$data['hash_loans_funded_loans_in_current_year'] = $countt_year;
		$data['loans_funded_loans_amt_in_current_month'] = number_format($loan_funded_amount2);
		$data['hash_loans_funded_loan_current_month'] = $countt_month;
		$data['hash_off_paid_off'] = $hash_off_paid_off;
		$data['total_amount_off_paid_off'] = $total_amount_off_paid_off;

		$loans_funded_sql = $this->User_model->query("SELECT l.id, l.talimar_loan, ls.loan_status, l.loan_amount FROM loan as l JOIN loan_servicing as ls on l.talimar_loan = ls.talimar_loan WHERE ls.loan_status IN ('2','3','5') ");
		$loans_funded_sql_result = $loans_funded_sql->result();

		$loans_funded_sum = 0;
		foreach ($loans_funded_sql_result as $result) {
			$loans_funded_sum += $result->loan_amount;

		}

		$data['loans_funded_loans'] = count($loans_funded_sql_result);
		$data['loans_funded_loans_amt'] = number_format($loans_funded_sum);

		//$pipe_lon_data= $this->User_model->query("SELECT count(*) as plc,SUM(l.loan_amount) as p_total_amount FROM loan as l JOIN loan_servicing  as ls ON l.talimar_loan = ls.talimar_loan where  ls.loan_status ='1' AND ls.closing_status IN('2','3','9','5','8','6')");

		// $pipe_lon_data= $this->User_model->query("SELECT *,count(*) as c,SUM(la.investment) as inves FROM loan as l JOIN loan_servicing  as ls ON l.talimar_loan = ls.talimar_loan JOIN loan_assigment as la on la.talimar_loan=ls.talimar_loan  where l.loan_funding_date BETWEEN '".$first_day_this_month."' AND '".$last_day_this_month."' AND ls.loan_status='1' group by ls.talimar_loan");

		$pipe_lon_data = $this->User_model->query("SELECT count(*) as c,SUM(l.loan_amount) as ptotal_amount FROM loan as l JOIN loan_servicing  as ls ON l.talimar_loan = ls.talimar_loan where l.loan_funding_date BETWEEN '" . $first_day_this_month . "' AND '" . $last_day_this_month . "' AND ls.loan_status='1'");

		//$pipe_lon_data= $this->User_model->query("SELECT count(*) as c,SUM(l.loan_amount) as ptotal_amount FROM loan as l JOIN loan_servicing  as ls ON l.talimar_loan = ls.talimar_loan where  ls.loan_status ='1' AND ls.term_sheet ='2'");

		if ($pipe_lon_data->num_rows() > 0) {

			$pipe_loanfetch = $pipe_lon_data->result();
			// echo '<pre>';
			// print_r($pipe_loanfetch);
			// echo '</pre>';

			//$l_total = $pipe_loanfetch[0]->loan_amount ? $pipe_loanfetch[0]->loan_amount : 0;
			//            	$ll_talimar_loan=$pipe_loanfetch[0]->talimar_loan;
			// $sum_investmengment_sql = $this->User_model->query("SELECT *,count(*) as c, SUM(investment) as total_investment FROM loan_assigment WHERE talimar_loan = '".$ll_talimar_loan."'");

			//  $fetch_invest = $sum_investmengment_sql->result();
			$ptotal_amount = $pipe_loanfetch[0]->ptotal_amount;
			//$avalible=$l_total-$investment_total;
			$c = $pipe_loanfetch[0]->c ? $pipe_loanfetch[0]->c : 0;

		}
		$data['t_pipe_loan_count'] = $c;
		$data['t_pipe_loan_amount'] = $ptotal_amount;

		$content1 = $this->load->view('dashboard/ajax_year_to_date_statistics', $data, true);
		echo $content1;

	}

	public function ajax_action_item() {
		error_reporting(0);

		$loan_status_active['loan_status'] = '2';
		$fetch_all_active = $this->User_model->select_where('loan_servicing', $loan_status_active);
		$fetch_all_active = $fetch_all_active->result();

		$active_senior_balance_sql = "SELECT SUM(`supirior_original`) as sum_original, SUM(`supirior_current`) as sum_current FROM extra_details JOIN loan_servicing ON extra_details.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '2'";
		$active_senior_balance_result = $this->User_model->query($active_senior_balance_sql);
		$active_senior_balance_result = $active_senior_balance_result->result();

		$count = 0;
		$loan_amount = 0;
		$intrest_rate = 0;
		$arv = 0;
		$term_month = 0;
		$selling_price = 0;
		$future_value = 0;
		foreach ($fetch_all_active as $active_loans) {
			$active_talimar_loan['talimar_loan'] = $active_loans->talimar_loan;

			// Fetch data from Loan
			$loan_data = $this->User_model->select_where('loan', $active_talimar_loan);
			$loan_data = $loan_data->result();
			foreach ($loan_data as $loan_row) {
				$loan_amount = $loan_amount + $loan_row->loan_amount;
				$term_month = $term_month + $loan_row->term_month;
				$intrest_rate = $intrest_rate + $loan_row->intrest_rate;

			}

			// fetch property_data

			$property_data = $this->User_model->select_where('loan_property', $active_talimar_loan);
			$property_data = $property_data->result();
			foreach ($property_data as $property_row) {

				$arv = $arv + $property_row->arv;
				$future_value = $future_value + $property_row->underwriting_value;
			}

			$where_11['talimar_loan'] = $active_loans->talimar_loan;
			$fetch_loan_servicing = $this->User_model->select_where('loan_servicing', $where_11);
			$fetch_loan_servicing = $fetch_loan_servicing->result();
			foreach ($fetch_loan_servicing as $row) {
				$selling_price = $selling_price + $row->selling_price;
			}

			$count++;

		}

		$fetch_sum_extra = '';

		$active_loan_result = array(
			"active_count" => $count,
			"total_loan_amount" => $loan_amount,
			"selling_price" => $selling_price,
			"total_intrest_rate" => number_format(($intrest_rate / $count),3),
			"total_arv" => $arv,
			"total_future_value" => $future_value / $count,
			"total_term_month" => ($term_month) / $count,
			"senior_current" => $active_senior_balance_result[0]->sum_current,
			"senior_original" => $active_senior_balance_result[0]->sum_original,

		);

		$data['active_loan_result'] = $active_loan_result;

		//default loan...

		$fetch_servicing_data = $this->User_model->query("select  SUM(loan_amount) as total_amount from loan_servicing as ls JOIN loan as l ON ls.talimar_loan = l.talimar_loan where ls.loan_status = '2' AND ls.condition = '1'");

		$fetch_servicing_data = $fetch_servicing_data->result();
		// $data['fetch_total_default']	= $fetch_servicing_data[0]->total_default;
		$data['fetch_total_amount'] = $fetch_servicing_data[0]->total_amount;

		$fetch_l_boarding = $this->User_model->query("select count(*) as b_count from loan_servicing JOIN loan_servicing_checklist ON loan_servicing_checklist.talimar_loan=loan_servicing.talimar_loan where loan_servicing.loan_status ='2' AND loan_servicing_checklist.hud='1026' AND loan_servicing_checklist.checklist!='1'");

		$fetch_l_boarding = $fetch_l_boarding->result();

		// $data['fetch_total_default']	= $fetch_servicing_data[0]->total_default;
		$data['count_boarding_count'] = $fetch_l_boarding[0]->b_count;

		$fetch_l_holding = $this->User_model->query("select count(*) as h_count from loan_servicing as ls JOIN input_datas as ida ON ls.talimar_loan = ida.talimar_loan where ls.loan_status = '2' AND ida.input_name='checklist_complete' AND ida.page='loan_servicing_checklist' AND ida.value != '1'");

		$fetch_l_holding = $fetch_l_holding->result();

		// $data['fetch_total_default']	= $fetch_servicing_data[0]->total_default;
		$data['count_holding_count'] = $fetch_l_holding[0]->h_count;

		

		//  Fetch All pipeliane loan data
		$loan_status_pipeline['loan_status'] = '1';
		$fetch_all_pipeline = $this->User_model->query("select * from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status = '1' ORDER BY ls.closing_status DESC, l.loan_funding_date ASC");
		$fetch_all_pipeline = $fetch_all_pipeline->result();

		$pipeline_senior_balance_sql = "SELECT SUM(`supirior_original`) as sum_original, SUM(`supirior_current`) as sum_current FROM extra_details JOIN loan_servicing ON extra_details.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '1'";
		$pipeline_senior_balance_result = $this->User_model->query($pipeline_senior_balance_sql);
		$pipeline_senior_balance_result = $pipeline_senior_balance_result->result();

		$count = 0;
		$loan_amount = 0;
		$intrest_rate = 0;
		$arv = 0;
		$term_month = 0;
		$selling_price = 0;
		$underwriting_value = 0;
		$ltvvount = 0;
		$calculate_ltv_av = array();

		foreach ($fetch_all_pipeline as $pkey => $pipeline_loans) {
			$pipeline_talimar_loan['talimar_loan'] = $pipeline_loans->talimar_loan;

			$all_pipeline_loan[] = $pipeline_loans->talimar_loan;
			// Fetch data from Loan
			$loan_data = $this->User_model->select_where('loan', $pipeline_talimar_loan);
			$loan_data = $loan_data->result();

			foreach ($loan_data as $loan_row) {
				$loan_amount = $loan_amount + $loan_row->loan_amount;
				$term_month = $term_month + $loan_row->term_month;
				$lender_fee = $loan_row->lender_fee;
				$intrest_rate = $intrest_rate + $loan_row->intrest_rate;

				$calculate_ltv_av['loan_amount'][$pkey] = $loan_row->loan_amount;

			}

			// fetch property_data
			$property_data = $this->User_model->select_where('loan_property', $pipeline_talimar_loan);
			$property_data = $property_data->result();

			foreach ($property_data as $property_row) {
				$arv = $arv + $property_row->arv;
				$underwriting_value = $underwriting_value + $property_row->underwriting_value;

				$calculate_ltv_av['underwriting_value'][$pkey] = $property_row->underwriting_value;
				$ltvvount++;

			}
			$where_12['talimar_loan'] = $pipeline_loans->talimar_loan;
			$fetch_loan_servicing = $this->User_model->select_where('loan_servicing', $where_12);
			$fetch_loan_servicing = $fetch_loan_servicing->result();
			foreach ($fetch_loan_servicing as $row) {
				$selling_price = $selling_price + $row->selling_price;
			}
			$count++;

		}

		// calculate LTV Avg

		$ltv_key = 0;
		$total_ltv = 0;

		foreach ($calculate_ltv_av['loan_amount'] as $ltv_val) {
			$loan_amount1 = $calculate_ltv_av['loan_amount'][$ltv_key];
			$underwriting_value = $calculate_ltv_av['underwriting_value'][$ltv_key];
			$ltv = ($loan_amount1 / $underwriting_value) * 100;

			$total_ltv = $total_ltv + $ltv;

			$ltv_key++;

		}

		//$ltv_total_av = ($loan_amount/$underwriting_value)*100;  //$ltv = ($loan_amount/$underwriting_value) * 100;    $total_ltv = $total_ltv + $ltv;

		$ltv_av = $total_ltv / $ltvvount;

		$pipeline_loan_result = array(
			"pipeline_count" => $count,
			"total_loan_amount" => $loan_amount,
			"lender_fee" => $lender_fee,
			"selling_price" => $selling_price,
			"total_intrest_rate" => number_format($intrest_rate,3),
			'ltv_av' => $ltv_av,
			"total_arv" => $arv,
			"total_term_month" => $term_month / $count,
			"senior_current" => $pipeline_senior_balance_result[0]->sum_current,
			"senior_original" => $pipeline_senior_balance_result[0]->sum_original,
		);

		$data['pipeline_loan_result'] = $pipeline_loan_result;

		$pipeline_loan_result = '';
		//  End of Fetch all pipeline loan data

		// fetch all pipeline loan...
		$total_pipeline_lender_fees_amount = 0;
		$uv_value = 0;
		$select_ecum = 0;
		foreach ($all_pipeline_loan as $row) {

			$p_talimar_loan['talimar_loan'] = $row;

			$fetch_loan_data = $this->User_model->select_where('loan', $p_talimar_loan);

			//$fetch_loan_data = $this->User_model->query("select * from loan where talimar_loan = '".$row."' ORDER BY loan_funding_date ASC");

			$fetch_loan_data = $fetch_loan_data->result();

			// echo '<pre>';
			// print_r($fetch_loan_data);
			// echo '<pre>';

			$l_talimar_loan = $fetch_loan_data[0]->talimar_loan;
			$fetch_talimar['talimar_loan'] = $fetch_loan_data[0]->talimar_loan;
			$fetch_borrower['id'] = $fetch_loan_data[0]->borrower;
			$l_loan_amount = $fetch_loan_data[0]->loan_amount;

			$l_loan_type = $fetch_loan_data[0]->loan_type;
			$l_intrest_rate = number_format($fetch_loan_data[0]->intrest_rate,3);
			$l_funding_date = $fetch_loan_data[0]->loan_funding_date;
			$l_lender_fee = $fetch_loan_data[0]->lender_fee;
			$month = $fetch_loan_data[0]->term_month;
			$position = $fetch_loan_data[0]->position;

			$pipeline_lender_fees_amount = $l_loan_amount * ($l_lender_fee / 100);
			$total_pipeline_lender_fees_amount = $total_pipeline_lender_fees_amount + $pipeline_lender_fees_amount;

			//fetch underwriting_value and property_address...
			$property_data = "SELECT sum(`underwriting_value`) as uv_value,property_address FROM `loan_property` WHERE `talimar_loan`='" . $l_talimar_loan . "'";

			$fetch_property_data = $this->User_model->query($property_data);
			$fetch_property_data = $fetch_property_data->result();
			$property_address = $fetch_property_data[0]->property_address;
			$uv_value = $fetch_property_data[0]->uv_value;
			$uv_value = $fetch_property_data[0]->uv_value ? $fetch_property_data[0]->uv_value : 0;

			//fetch borrower name...
			$fetch_borrower_data = $this->User_model->select_where('borrower_data', $fetch_borrower);
			$fetch_borrower_data = $fetch_borrower_data->result();
			$borrower_name = "";
			if(!empty($$fetch_borrower_data)){
				$borrower_name = $fetch_borrower_data[0]->b_name;
			}
			

			//fetch closing_status...
			$fetch_loan_servicing = $this->User_model->select_where('loan_servicing', $p_talimar_loan);
			$fetch_loan_servicing = $fetch_loan_servicing->result();
			$closing_status = $fetch_loan_servicing[0]->closing_status;

			//fetch property home id...
			$select_property_home = $this->User_model->select_where('property_home', $p_talimar_loan);
			$select_property_home = $select_property_home->result();
			$select_property_home = $select_property_home[0]->id;

			//fetch sum of emcumbrances current value...
			$sql = "SELECT `current_balance` as total FROM `encumbrances` WHERE talimar_loan ='" . $l_talimar_loan . "' AND `property_home_id`='" . $select_property_home . "'";

			$select_ecum = $this->User_model->query($sql);
			$select_ecum = $select_ecum->result();
			$select_ecum = $select_ecum[0]->total ? $select_ecum[0]->total : 0;

			$sum_investment_assigment_sql = "SELECT SUM(investment) as total_investment FROM loan_assigment WHERE talimar_loan = '" . $l_talimar_loan . "' ";

			$sum_investment_assigment_result = $this->User_model->query($sum_investment_assigment_sql);
			$sum_investment_assigment_result = $sum_investment_assigment_result->result();
			$pileline_available = $sum_investment_assigment_result[0]->total_investment;

			$all_customize_pipeline_data[$closing_status][] = array(
				"loan_id" => $fetch_loan_data[0]->id,
				"talimar_loan" => $l_talimar_loan,
				"loan_amount" => $l_loan_amount,
				"l_loan_type" => $l_loan_type,
				"intrest_rate" => number_format($l_intrest_rate,3),
				"funding_date" => $l_funding_date,
				"lender_fee" => $l_lender_fee,
				"term_month" => $month,
				"position" => $position,
				"property_address" => $property_address,
				"uv_value" => $uv_value,
				"select_ecum" => $select_ecum,
				"borrower_name" => $borrower_name,
				"closing_status" => $closing_status,
				"avilable" => $l_loan_amount - $pileline_available,
			);

			if ($closing_status != 7) {
				$all_pipeline_data[] = array(
					"loan_id" => $fetch_loan_data[0]->id,
					"talimar_loan" => $l_talimar_loan,
					"loan_amount" => $l_loan_amount,
					"l_loan_type" => $l_loan_type,
					"intrest_rate" => number_format($l_intrest_rate,3),
					"funding_date" => $l_funding_date,
					"lender_fee" => $l_lender_fee,
					"term_month" => $month,
					"position" => $position,
					"property_address" => $property_address,
					"uv_value" => $uv_value,
					"select_ecum" => $select_ecum,
					"borrower_name" => $borrower_name,
					"closing_status" => $closing_status,
					"avilable" => $l_loan_amount - $pileline_available,
				);
			}
			if ($closing_status == 7) {
				$all_pipeline_data_closing_status7[] = array(
					"loan_id" => $fetch_loan_data[0]->id,
					"talimar_loan" => $l_talimar_loan,
					"loan_amount" => $l_loan_amount,
					"l_loan_type" => $l_loan_type,
					"intrest_rate" => number_format($l_intrest_rate,3),
					"funding_date" => $l_funding_date,
					"lender_fee" => $l_lender_fee,
					"term_month" => $month,
					"position" => $position,
					"property_address" => $property_address,
					"uv_value" => $uv_value,
					"select_ecum" => $select_ecum,
					"borrower_name" => $borrower_name,
					"closing_status" => $closing_status,
					"avilable" => $l_loan_amount - $pileline_available,
				);
			}

		}

		if (isset($all_pipeline_data_closing_status7)) {
			foreach ($all_pipeline_data_closing_status7 as $key => $row) {

				$all_pipeline_data[] = array(
					"loan_id" => $row['loan_id'],
					"talimar_loan" => $row['talimar_loan'],
					"loan_amount" => $row['loan_amount'],
					"l_loan_type" => $row['l_loan_type'],
					"intrest_rate" => number_format($row['intrest_rate'],3),
					"funding_date" => $row['funding_date'],
					"lender_fee" => $row['lender_fee'],
					"term_month" => $row['term_month'],
					"position" => $row['position'],
					"property_address" => $row['property_address'],
					"uv_value" => $row['uv_value'],
					"select_ecum" => $row['select_ecum'],
					"borrower_name" => $row['borrower_name'],
					"closing_status" => $row['closing_status'],
					"avilable" => $row['avilable'],
				);
			}
		}


		if (isset($all_customize_pipeline_data)) {
			$array = array(8, 6, 5, 4, 3, 9, 2, 1, 7);
			foreach ($array as $number) {
				if (isset($all_customize_pipeline_data[$number])) {

					foreach ($all_customize_pipeline_data[$number] as $row) {

						$all_pipeline_data_new[] = array(
							"loan_id" => $row['loan_id'],
							"talimar_loan" => $row['talimar_loan'],
							"loan_amount" => $row['loan_amount'],
							"l_loan_type" => $row['l_loan_type'],
							"intrest_rate" => number_format($row['intrest_rate'],3),
							"funding_date" => $row['funding_date'],
							"lender_fee" => $row['lender_fee'],
							"term_month" => $row['term_month'],
							"position" => $row['position'],
							"property_address" => $row['property_address'],
							"uv_value" => $row['uv_value'],
							"select_ecum" => $row['select_ecum'],
							"borrower_name" => $row['borrower_name'],
							"closing_status" => $row['closing_status'],
							"avilable" => $row['avilable'],
						);
					}
				}
			}
		}

		

		$data['all_pipeline_data'] = $all_pipeline_data_new;
		$data['total_pipeline_lender_fees_amount'] = $total_pipeline_lender_fees_amount;

		//loan holding schedule...

		// $holding = "SELECT * FROM `input_datas` WHERE `page`= 'loan_servicing_checklist' AND (value = '0' OR value = '')";

		if ($loginuser_dashboard == 1) {
			// $holding = $this->User_model->query("select talimar_loan from loan_servicing where loan_status='2' AND board_complete='2'");
			$holding = $this->User_model->query("select * from loan_servicing as ls JOIN input_datas as ida ON ls.talimar_loan = ida.talimar_loan where ls.loan_status = '2' AND ida.input_name='checklist_complete' AND ida.page='loan_servicing_checklist' AND ida.value !='1'");

		} else {

			// $holding = $this->User_model->query("select talimar_loan from loan_servicing where loan_status='2' AND board_complete='2' AND t_user_id = '".$this->session->userdata('t_user_id')."'");
			$holding = $this->User_model->query("select * from loan_servicing as ls JOIN input_datas as ida ON ls.talimar_loan = ida.talimar_loan where ls.loan_status = '2' AND ida.input_name='checklist_complete' AND ida.page='loan_servicing_checklist' AND ida.value !='1' AND ls.t_user_id = '" . $this->session->userdata('t_user_id') . "'");

		}

		$input_data = $this->User_model->query($holding);
		$input_data = $input_data->result();

		foreach ($input_data as $row) {

			$talimar_loan = $row->talimar_loan;

			$address = $this->User_model->query("select property_address from loan_property where talimar_loan = '" . $talimar_loan . "'");
			$address = $address->result();
			$p_address = $address[0]->property_address;

			$loan_status = $this->User_model->query("select loan_status from loan_servicing where talimar_loan = '" . $talimar_loan . "'");
			$loan_status = $loan_status->result();
			$status = $loan_status[0]->loan_status;

			$loan = $this->User_model->query("select * from loan where talimar_loan = '" . $talimar_loan . "'");
			$loan = $loan->result();
			$loan_id = $loan[0]->id;
			$amount = $loan[0]->loan_amount;
			$servicer = $loan[0]->fci;
			$closing_date = $loan[0]->loan_funding_date;
			$current_date = date('Y-m-d');

			$diff = $this->dateDiff($closing_date, $current_date);

			$sql = "SELECT * FROM `loan_servicing_checklist` WHERE `talimar_loan`='" . $talimar_loan . "' AND type='post wire' AND hud='1013'";
			$servicing_checklist = $this->User_model->query($sql);

			if ($servicing_checklist->num_rows() > 0) {
				$servicing_checklist = $servicing_checklist->result();

			}

			$holding_again[] = array(

				"talimar_loan" => $talimar_loan,
				"loan_id" => $loan_id,
				"p_address" => $p_address,
				"status" => $status,
				"loan_amount" => $amount,
				"servicer" => $servicer,
				"closing_date" => $closing_date,
				"diff" => $diff,
				//"hud"			=> $hud,
				"check" => $servicing_checklist[0]->checklist,
				//"type"			=> $post,

			);
		}
		$data['holding_loan'] = $holding_again;

		

		//$disb_verified = $this->User_model->select_where('loan_servicing',$where_disb);
		$disb_verified = $this->User_model->query("select * from loan_servicing where servicing_disb='2' AND loan_status='2'");

		$disb_verified = $disb_verified->result();
		$count_disb = count($disb_verified);
		$data['all_count_disb'] = $count_disb;

		//count Lender Approvals
		$lenderAppSql = $this->User_model->query("SELECT l.id as loan_id, l.borrower, l.loan_amount, lp.property_address, lp.unit, lp.city, lp.state, lp.zip, la.LP_status, la.LP_request FROM `lender_approval` as la JOIN loan as l ON l.talimar_loan = la.talimar_loan JOIN loan_property as lp On l.talimar_loan = lp.talimar_loan JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.loan_status = '2' AND la.LP_status = '1' group by l.talimar_loan");
		$lenderAppSql = $lenderAppSql->result();
		$data['countlenderappreq'] = count($lenderAppSql);
		

		//  Monthly income servicing loan

		$join_sql = "SELECT *, loan.id as loan_id FROM loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan_servicing.loan_status = '2' ";
		$fetch_all_loan = $this->User_model->query($join_sql);
		$fetch_all_loan = $fetch_all_loan->result();
		foreach ($fetch_all_loan as $row) {
			$talimar['talimar_loan'] = $row->talimar_loan;
			$talimar_loan = $row->talimar_loan;
			$loan_id = $row->loan_id;
			$fci = $row->fci;
			$loan_amount = $row->loan_amount;

			//  fetch data from loan servicing

			$fetch_servicing = $this->User_model->select_where('loan_servicing', $talimar);
			$fetch_servicing = $fetch_servicing->result();

			$servicing_fee = $fetch_servicing[0]->servicing_fee;
			$who_pay_servicing = $fetch_servicing[0]->who_pay_servicing;
			$sub_servicing_agent = $fetch_servicing[0]->sub_servicing_agent;
			$minimum_servicing_fee = $fetch_servicing[0]->minimum_servicing_fee;
			$sql = "SELECT count(*) as countrow from loan_assigment WHERE talimar_loan = '" . $talimar_loan . "' ";
			$count_lender = $this->User_model->query($sql);
			if ($count_lender->num_rows() > 0) {
				$count_lender = $count_lender->result();
				$count_lender = $count_lender[0]->countrow;
			} else {
				$count_lender = 0;
			}

			$all_monthly_income[] = array(
				'talimar_loan' => $talimar_loan,
				'loan_id' => $loan_id,
				'fci' => $fci,
				'loan_amount' => $loan_amount,
				'sub_servicing_agent' => $sub_servicing_agent,
				'who_pay_servicing' => $who_pay_servicing,
				'servicing_fee' => $servicing_fee,
				'minimum_servicing_fee' => $minimum_servicing_fee,
				'count_lender' => $count_lender,
			);
		}
		$total_servicing_income = 0;
		$total_servicing_cost = 0;
		foreach ($all_monthly_income as $key => $row) {
			$loan_amount = $all_monthly_income[$key]['loan_amount'];
			$servicing_fee = $all_monthly_income[$key]['servicing_fee'];
			$servicing_cost = $all_monthly_income[$key]['count_lender'] * $minimum_servicing_fee;
			$gross_servicing = ($loan_amount * ($servicing_fee / 100)) / 12;
			$servicing_income = $gross_servicing - $servicing_cost;

			if ($all_monthly_income[$key]['who_pay_servicing'] == 2) {
				$gross_servicing = 0;
				$servicing_cost = 0;
				$servicing_income = 0;
				$all_monthly_income[$key]['servicing_fee'] = 0;
				$all_monthly_income[$key]['minimum_servicing_fee'] = 0;
			}
			$total_servicing_income = $total_servicing_income + $servicing_income;
			//$total_servicing_cost = $total_servicing_cost + $servicing_cost;
		}
		$data['total_servicing_income'] = $total_servicing_income;
		//$data['total_servicing_cost'] 		= $total_servicing_cost;
		//$data['all_monthly_income'] 		= $all_monthly_income;

		//property loan count...

		$current_date = date('d-m-Y');
		$maturity_date_30 = strtotime($current_date . ' +30 days');
		$maturity_date_30 = date("Y-m-d", $maturity_date_30);


		$fetch_property_loan = $this->User_model->query("SELECT l.talimar_loan, l.id, l.borrower, lpi.policy_number, lpi.insurance_expiration, lpi.contact_name, lpi.request_updated_insurance, lpi.insurance_type, ls.loan_status, ph.primary_property FROM loan as l JOIN loan_property_insurance as lpi ON l.talimar_loan = lpi.talimar_loan JOIN loan_servicing as ls ON ls.talimar_loan = l.talimar_loan JOIN property_home as ph ON ph.talimar_loan = l.talimar_loan WHERE ls.loan_status = 2 AND ph.primary_property = '1' AND lpi.insurance_type = '1' AND lpi.required IN ('1','0') GROUP BY l.talimar_loan");

		$fetch_property_loan = $fetch_property_loan->result_array();
		// echo '<pre>';
		// print_r($fetch_property_loan);
		// echo '</pre>';

		$data['fetch_property_loan'] = $fetch_property_loan;
		
		//Advertising complete...
		$Advertisingcomplete = $this->User_model->query("SELECT COUNT(*) as totalSigned FROM loan_servicing as ls JOIN loan_servicing_checklist as lsc ON lsc.talimar_loan = ls.talimar_loan WHERE ls.loan_status = '2' AND lsc.hud ='1033' AND lsc.checklist !='1'");
		$Advertisingcomplete = $Advertisingcomplete->result();
		$data['Advertisingcomplete'] = $Advertisingcomplete[0]->totalSigned;


		//Upcoming Maturity Schedule...
		error_reporting(0);
		$current_date = date('d-m-Y');
		$maturity_date_45 = strtotime($current_date . ' +45 days');
		$maturity_date_45 = date("Y-m-d", $maturity_date_45);
		$current_date1 = date('Y-m-d');

		$upcoming = "SELECT *,Count(*) as total_upcoming FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE l.new_maturity_date <='" . $maturity_date_45 . "' AND ls.loan_status = '2' order by l.new_maturity_date";
		$fetch_upcoming = $this->User_model->query($upcoming);
		$fetch_upcoming = $fetch_upcoming->result();
		$fetch_upcoming_count = $fetch_upcoming[0]->total_upcoming;
		$data['fetch_upcoming_count'] = $fetch_upcoming_count;

		//Deliquent_property_taxes
		$sel = $this->User_model->query("SELECT COUNT(*) as total_tax FROM `loan_property_taxes` as lpt JOIN loan_servicing as ls ON lpt.talimar_loan = ls.talimar_loan WHERE lpt.`post_delinquent` = '1' AND ls.loan_status = '2'");
		$property_tax = $sel->result();
		$property_tax = $property_tax[0]->total_tax;
		$data['tax'] = $property_tax;

		//loan_assigment
		$assigment_in_process_sql = "SELECT COUNT(*) as count_assigment FROM loan_assigment JOIN loan ON loan.talimar_loan = loan_assigment.talimar_loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE  loan_servicing.loan_status IN('1','2') AND loan_assigment.secured_dot != '1' AND loan_assigment.file_close != '1' ";

		$assigment_in_process_result = $this->User_model->query($assigment_in_process_sql);
		$assigment_in_process_result = $assigment_in_process_result->result();

		$data['count_assigment_in_process'] = $assigment_in_process_result[0]->count_assigment;
		//$data['investment_assigment_in_process'] = $assigment_in_process_result[0]->investment_assigment_in_process;

		// ----------------------- Outstatnding draws table  ----------------------

		// $sql = "SELECT *,loan.id as loan_id ,loan_reserve_draws.talimar_loan as talimar_loan, SUM(loan_reserve_draws.date1_amount+date2_amount+date3_amount+date4_amount+date5_amount+date6_amount+date7_amount+date8_amount+date9_amount+date10_amount+date11_amount+date12_amount+date13_amount+date14_amount+date15_amount) as draws_release_amount	FROM loan_reserve_draws JOIN loan ON loan.talimar_loan = loan_reserve_draws.talimar_loan JOIN loan_property as lp ON lp.talimar_loan = loan.talimar_loan";

		// $fetch_outstanding_draws = $this->User_model->query($sql);
		// if($fetch_outstanding_draws->num_rows() > 0)
		// {
		// 	$data['outstanding_draws_count'] = $fetch_outstanding_draws->result();
		// }



			$loan_data = $this->User_model->query("select *,l.id as loan_id from loan as l JOIN loan_property as ls ON l.talimar_loan = ls.talimar_loan join loan_servicing as lsv on lsv.talimar_loan=l.talimar_loan where lsv.loan_status = '2' GROUP BY ls.talimar_loan");

			$loan_data = $loan_data->result();

			foreach ($loan_data as $key) {

				$brorrower_id = $key->borrower;
				$talimar = $key->talimar_loan;

				$select_borrower = $this->User_model->query("Select * from borrower_data join borrower_contact on borrower_data.id=borrower_contact.borrower_id join contact on contact.contact_id=borrower_contact.contact_name where borrower_data.id='" . $brorrower_id . "' ");
				$select_borrower = $select_borrower->result();

				$b_name = $select_borrower[0]->b_name;

				$select_draw = $this->User_model->query("select * from loan_reserve_checkbox join loan_reserve_draws  on loan_reserve_checkbox.loan_id=loan_reserve_draws.loan_id where loan_reserve_draws.talimar_loan='" . $talimar . "' ");

				$select_draw = $select_draw->result();
				$draws_amount_1 = 0;
				$draws_amount_2 = 0;
				$draws_amount_3 = 0;
				$draws_amount_4 = 0;
				$draws_amount_5 = 0;
				$draws_amount_6 = 0;
				$draws_amount_7 = 0;
				$draws_amount_8 = 0;
				$draws_amount_9 = 0;
				$draws_amount_10 = 0;
				$draws_amount_11 = 0;
				$draws_amount_12 = 0;
				$draws_amount_13 = 0;
				$draws_amount_14 = 0;
				$draws_amount_15 = 0;

				foreach ($select_draw as $datas) {
					$loan_id = $datas->loan_id;

					$draws_amount_1 += $datas->date1_amount;
					$draws_amount_2 += $datas->date2_amount;
					$draws_amount_3 += $datas->date3_amount;
					$draws_amount_4 += $datas->date4_amount;
					$draws_amount_5 += $datas->date5_amount;
					$draws_amount_6 += $datas->date6_amount;
					$draws_amount_7 += $datas->date7_amount;
					$draws_amount_8 += $datas->date8_amount;
					$draws_amount_9 += $datas->date9_amount;
					$draws_amount_10 += $datas->date10_amount;
					$draws_amount_11 += $datas->date11_amount;
					$draws_amount_12 += $datas->date12_amount;
					$draws_amount_13 += $datas->date13_amount;
					$draws_amount_14 += $datas->date14_amount;
					$draws_amount_15 += $datas->date15_amount;

					$date1_draw_request = $datas->date1_draw_request;
					$date2_draw_request = $datas->date2_draw_request;
					$date3_draw_request = $datas->date3_draw_request;
					$date4_draw_request = $datas->date4_draw_request;
					$date5_draw_request = $datas->date5_draw_request;
					$date6_draw_request = $datas->date6_draw_request;
					$date7_draw_request = $datas->date7_draw_request;
					$date8_draw_request = $datas->date8_draw_request;
					$date9_draw_request = $datas->date9_draw_request;
					$date10_draw_request = $datas->date10_draw_request;
					$date11_draw_request = $datas->date11_draw_request;
					$date12_draw_request = $datas->date12_draw_request;
					$date13_draw_request = $datas->date13_draw_request;
					$date14_draw_request = $datas->date14_draw_request;
					$date15_draw_request = $datas->date15_draw_request;

					$date1_draw_released = $datas->date1_draw_released;
					$date2_draw_released = $datas->date2_draw_released;
					$date3_draw_released = $datas->date3_draw_released;
					$date4_draw_released = $datas->date4_draw_released;
					$date5_draw_released = $datas->date5_draw_released;
					$date6_draw_released = $datas->date6_draw_released;
					$date7_draw_released = $datas->date7_draw_released;
					$date8_draw_released = $datas->date8_draw_released;
					$date9_draw_released = $datas->date9_draw_released;
					$date10_draw_released = $datas->date10_draw_released;
					$date11_draw_released = $datas->date11_draw_released;
					$date12_draw_released = $datas->date12_draw_released;
					$date13_draw_released = $datas->date13_draw_released;
					$date14_draw_released = $datas->date14_draw_released;
					$date15_draw_released = $datas->date15_draw_released;

					$date1_draw_submit = $datas->date1_draw_submit;
					$date2_draw_submit = $datas->date2_draw_submit;
					$date3_draw_submit = $datas->date3_draw_submit;
					$date4_draw_submit = $datas->date4_draw_submit;

					$date5_draw_submit = $datas->date5_draw_submit;
					$date6_draw_submit = $datas->date6_draw_submit;
					$date7_draw_submit = $datas->date7_draw_submit;
					$date8_draw_submit = $datas->date8_draw_submit;
					$date9_draw_submit = $datas->date9_draw_submit;
					$date10_draw_submit = $datas->date10_draw_submit;
					$date11_draw_submit = $datas->date11_draw_submit;
					$date12_draw_submit = $datas->date12_draw_submit;
					$date13_draw_submit = $datas->date13_draw_submit;
					$date14_draw_submit = $datas->date14_draw_submit;
					$date15_draw_submit = $datas->date15_draw_submit;

					$date1_draw_approved = $datas->date1_draw_approved;
					$date2_draw_approved = $datas->date2_draw_approved;
					$date3_draw_approved = $datas->date3_draw_approved;
					$date4_draw_approved = $datas->date4_draw_approved;
					$date5_draw_approved = $datas->date5_draw_approved;
					$date6_draw_approved = $datas->date6_draw_approved;
					$date7_draw_approved = $datas->date7_draw_approved;
					$date8_draw_approved = $datas->date8_draw_approved;
					$date9_draw_approved = $datas->date9_draw_approved;
					$date10_draw_approved = $datas->date10_draw_approved;
					$date11_draw_approved = $datas->date11_draw_approved;
					$date12_draw_approved = $datas->date12_draw_approved;
					$date13_draw_approved = $datas->date13_draw_approved;
					$date14_draw_approved = $datas->date14_draw_approved;
					$date15_draw_approved = $datas->date15_draw_approved;

				}

				if ($date1_draw_request == '1' && $date1_draw_request != '' && $date1_draw_released != '1' || $date2_draw_request == '1' && $date2_draw_request != '' && $date2_draw_released != '1' || $date3_draw_request == '1' && $date3_draw_request != '' && $date3_draw_released != '1' || $date4_draw_request == '1' && $date4_draw_request != '' && $date4_draw_released != '1' || $date5_draw_request == '1' && $date5_draw_request != '' && $date5_draw_released != '1' || $date6_draw_request == '1' && $date6_draw_request != '' && $date6_draw_released != '1' || $date7_draw_request == '1' && $date7_draw_request != '' && $date7_draw_released != '1' || $date8_draw_request == '1' && $date8_draw_request != '' && $date8_draw_released != '1' || $date9_draw_request == '1' && $date9_draw_request != '' && $date9_draw_released != '1' || $date10_draw_request == '1' && $date10_draw_request != '' && $date10_draw_released != '1' || $date11_draw_request == '1' && $date11_draw_request != '' && $date11_draw_released != '1' || $date12_draw_request == '1' && $date12_draw_request != '' && $date13_draw_released != '1' || $date14_draw_request == '1' && $date14_draw_request != '' && $date14_draw_released != '1' || $date15_draw_request == '1' && $date15_draw_request != '' && $date15_draw_released != '1') {

					if ($draws_amount_1 != '0' || $draws_amount_2 != '0' || $draws_amount_3 != '0' || $draws_amount_4 != '0' || $draws_amount_5 != '0' || $draws_amount_6 != '0' || $draws_amount_7 != '0' || $draws_amount_8 != '0' || $draws_amount_9 != '0' || $draws_amount_10 != '0' || $draws_amount_11 != '0' || $draws_amount_12 != '0' || $draws_amount_13 != '0' || $draws_amount_14 != '0' || $draws_amount_15 != '0') {

						$outstanding_draws_datass[] = array(
							"b_name" => $b_name,
							"loan_id" => $loan_id,
							"draws_amount_1" => $draws_amount_1,
							"draws_amount_2" => $draws_amount_2,
							"draws_amount_3" => $draws_amount_3,
							"draws_amount_4" => $draws_amount_4,
							"draws_amount_5" => $draws_amount_5,
							"draws_amount_6" => $draws_amount_6,
							"draws_amount_7" => $draws_amount_7,
							"draws_amount_8" => $draws_amount_8,
							"draws_amount_9" => $draws_amount_9,
							"draws_amount_10" => $draws_amount_10,
							"draws_amount_11" => $draws_amount_11,
							"draws_amount_12" => $draws_amount_12,
							"draws_amount_13" => $draws_amount_13,
							"draws_amount_14" => $draws_amount_14,
							"draws_amount_15" => $draws_amount_15,

						);
					}
				}
			}

			// echo '<pre>';
			// print_r($outstanding_draws_datass);
			// echo '</pre>';

			$checkCOUNTfrist = count($outstanding_draws_datass);
			if($checkCOUNTfrist > 1){
				$data['outstanding_draws_count'] = count($outstanding_draws_datass);
			}else{
				$data['outstanding_draws_count'] = count($outstanding_draws_datass);
			}
		

		
		// fetch Upcoming payoff schedule amount and count
		
		$payoff_schdule = "SELECT COUNT(*) as totalCount, SUM(l.loan_amount) as sum_loan_amount FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan JOIN pay_off_processing as pop ON l.talimar_loan = pop.talimar_loan WHERE ls.loan_status = '3' AND pop.payoff_processing != '1' ORDER BY pop.de_Good_Thru_Date DESC";

		$fetch_payoff_data = $this->User_model->query($payoff_schdule);
		$fetch_payoff_dataaa = $fetch_payoff_data->result();

		$data['payoff_report_amount'] = $fetch_payoff_dataaa[0]->sum_loan_amount ? $fetch_payoff_dataaa[0]->sum_loan_amount : 0;
		$data['payoff_report_count_payoff'] = $fetch_payoff_dataaa[0]->totalCount ? $fetch_payoff_dataaa[0]->totalCount : 0;

		

		//$data['closing_count'] = $closing_dataa[0]->count_posted ? $closing_dataa[0]->count_posted : 0;
		/*
			Update code : yes
			Date : 08-06-2021
			lsc.hud ='1019' = lsc.hud ='147'
		*/
		$borrower_call = $this->User_model->query("SELECT COUNT(*) as active_borrower_loans FROM `loan_servicing_checklist` as lsc JOIN loan_servicing as ls ON lsc.talimar_loan = ls.talimar_loan WHERE ls.loan_status ='2' AND lsc.hud ='147' AND (lsc.checklist !='1' AND lsc.not_applicable !='1')");
		$borrower_calls = $borrower_call->result();
		$data['borrower_callx'] = $borrower_calls[0]->active_borrower_loans;

		$borrower_email = $this->User_model->query("SELECT COUNT(*) as email_active_borrower_loans FROM `loan_servicing_checklist` as lsc JOIN loan_servicing as ls ON lsc.talimar_loan = ls.talimar_loan WHERE ls.loan_status ='2' AND lsc.hud ='1013' AND (lsc.checklist !='1' AND lsc.not_applicable !='1')");
		$borrower_emails = $borrower_email->result();
		$data['borrower_emailx'] = $borrower_emails[0]->email_active_borrower_loans;

		/*
		add code : yes
		Date : 08-06-2021
		add new code for Add "Servicer Deposit Received" under Loans in Holding on the Active Items Correction. Count Active Loans with Loan Closing Checklist → Confirm Servicer Received Funds is NOT CHECKED
		*/
		$servicer_deposit_received = $this->User_model->query("SELECT COUNT(*) as servicer_deposit FROM `loan_servicing_checklist` as lsc JOIN loan_servicing as ls ON lsc.talimar_loan = ls.talimar_loan WHERE ls.loan_status ='2' AND lsc.hud ='1042' AND (lsc.checklist !='1' AND lsc.not_applicable !='1')");
		$servicer_deposit_received = $servicer_deposit_received->row();
		$data['servicer_deposit_received'] = $servicer_deposit_received->servicer_deposit;


		$sql_join = "SELECT *,loan.id as loan_id FROM loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan_servicing.loan_status IN('1','2') ORDER BY loan.id DESC";
		$fetch_all_loan = $this->User_model->query($sql_join);
		$fetch_all_loan = $fetch_all_loan->result();
		foreach ($fetch_all_loan as $row) {
			$talimar_loan['talimar_loan'] = $row->talimar_loan;

			$loan_id = $row->loan_id;

			//$all_assigment_data 			= $this->User_model->select_where('loan_assigment',$talimar_loan);

			$all_assigment_data = $this->User_model->query("select * from loan_assigment where talimar_loan = '" . $row->talimar_loan . "' ");
			$all_assigment_data = $all_assigment_data->result();

			

		}

		$assigment_sql_6 = "SELECT * FROM loan_assigment JOIN loan ON loan.talimar_loan = loan_assigment.talimar_loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan_assigment.nfiles_status = '0' AND loan_assigment.nsecurity_type = '2' AND loan_servicing.loan_status IN('1','2') group by loan_assigment.lender_name";

		$assigment_is_result_6 = $this->User_model->query($assigment_sql_6);
		$assigment_result_6 = $assigment_is_result_6->result();

		$data['count_lenvv'] = $assigment_is_result_6->num_rows();

		$sql_join_1 = "SELECT *,COUNT(*) as total_term_sheet,loan.id as loan_id, SUM(loan.loan_amount) as l_sum FROM loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan_servicing.loan_status ='6' ORDER BY loan.id DESC";
		$fetch_all_loan_1 = $this->User_model->query($sql_join_1);
		$fetch_all_loa = $fetch_all_loan_1->result();
		$l_sum = $fetch_all_loa[0]->l_sum;
		$total_term_sheet = $fetch_all_loa[0]->total_term_sheet;

		$data['l_sum'] = $l_sum;
		$data['total_term_sheet'] = $total_term_sheet;

		$laon_default_forculare = $this->User_model->query("SELECT * FROM loan_servicing_default INNER JOIN add_foreclosure_request ON loan_servicing_default.talimar_loan=add_foreclosure_request.talimar_loan INNER JOIN loan_servicing ON loan_servicing.talimar_loan=add_foreclosure_request.talimar_loan WHERE loan_servicing.loan_status='2' AND loan_servicing.condition='1' group by loan_servicing_default.talimar_loan");

		$laon_default_forculares = $laon_default_forculare->result();
		$total_default = 0;
		foreach ($laon_default_forculares as $value) {
			if($value->foreclosuer_status == '1'){
				$total_default++;
			}
		}

		$data['fetch_total_default'] = $total_default;
		

		$sql_quy = "SELECT count(*) as count_com from pay_off_processing where  payoff_processing='2'";
		$coun = $this->User_model->query($sql_quy);
		if ($coun->num_rows() > 0) {
			$coun = $coun->result();
			$count_complete = $coun[0]->count_com;

		}

		$data['count_paid_off_complete'] = $count_complete;

		$upcoming_action_item = $this->User_model->query("SELECT COUNT(*) as total_upcoming FROM `loan_servicing_checklist` as lsc JOIN loan_servicing as ls ON lsc.talimar_loan = ls.talimar_loan WHERE ls.loan_status = '2' AND lsc.hud = '1014' AND lsc.checklist = '0'");
		$upcoming_action_item = $upcoming_action_item->result();
		$data['upcoming_action_item'] = $upcoming_action_item[0]->total_upcoming;

		//Real estate owned count...
		$realstateowned = $this->User_model->query("SELECT COUNT(*) as total_realstate FROM loan_servicing WHERE loan_status = '7'");
		$realstateowned = $realstateowned->result();
		$data['realstateowned'] = $realstateowned[0]->total_realstate;

		$where_condition['loan_payment_status'] = '2';
		// $fetch_condition = $this->User_model->select_where('loan_servicing', $where_condition);
		// $fetch_condition = $fetch_condition->result();

		
		// $data['count_10_days'] = count($fetch_condition);
	/*05-07-2021 BY @AJ*/
		$the_counts = "SELECT l.id, l.id AS loan_id, l.fci, l.current_balance, lh.talimar_loan, lh.currentBalance, lh.loanStatus, lh.loanAccount, lh.daysLate, lh.totalPayment, lh.loanAccount, lh.nextDueDate, bd.id AS b_id, 
			l.talimar_loan, l.fci, l.loan_amount, l.payment_amount, l.totalPaymentFCI, l.loan_funding_date, l.dayVariance, bd.b_name, bd.b_address, bd.b_unit, bd.b_city, bd.b_state, bd.b_zip, lp.property_address, lp.unit, lp.city, lp.state, lp.zip 
			FROM loan AS l
			LEFT JOIN lender_payment_history AS lh ON l.talimar_loan=lh.talimar_loan 
			LEFT JOIN loan_servicing AS ls ON lh.talimar_loan=ls.talimar_loan 
			LEFT JOIN loan_servicing_checklist AS lc ON ls.talimar_loan=lc.talimar_loan 
			LEFT JOIN loan_property AS lp ON lc.talimar_loan=lp.talimar_loan 
			LEFT JOIN borrower_data AS bd ON l.borrower=bd.id WHERE lh.daysLate > 0  GROUP BY l.talimar_loan";
		$thecnt  = $this->User_model->query($the_counts);
		 $fetch_condition = $thecnt->result();
		$data['count_10_days'] = count($fetch_condition);
	/*-----End----*/	

		$assigment_confirm_wire = "SELECT COUNT(*) as pre_count_assigment FROM loan_assigment JOIN loan ON loan.talimar_loan = loan_assigment.talimar_loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan_assigment.nfunds_received = '2' AND loan_servicing.loan_status IN ('1','2')";

		$assigment_confirm_wire = $this->User_model->query($assigment_confirm_wire);
		$assigment_confirm_wire = $assigment_confirm_wire->result();

		$data['confirm_wire'] = $assigment_confirm_wire[0]->pre_count_assigment;
		
		
		//Extension count...
		//$fetch_ext_data = $this->User_model->query("SELECT COUNT(*) as totalext FROM `extension_section` as es JOIN loan_servicing as ls ON es.`talimar_loan` = ls.talimar_loan JOIN loan_property as lp ON es.talimar_loan = lp.talimar_loan WHERE ls.loan_status = '2' AND es.`extension_complete` != '1'");
		$fetch_ext_data = $this->User_model->query("SELECT COUNT(*) as totalext FROM `extension_section` as es JOIN loan_servicing as ls ON es.`talimar_loan` = ls.talimar_loan JOIN loan_property as lp ON es.talimar_loan = lp.talimar_loan JOIN loan as l ON ls.talimar_loan = l.talimar_loan JOIN property_home as ph ON lp.talimar_loan = ph.talimar_loan AND lp.property_home_id = ph.id WHERE ls.loan_status = '2' AND es.`ext_process_option` = '1' AND ph.primary_property = '1' ORDER BY l.maturity_date ASC");
		
		$fetch_ext_data = $fetch_ext_data->result();
		$data['ext_count'] = $fetch_ext_data[0]->totalext;
		
		//Before & after videos...
		$fetchbeforeafterss = $this->User_model->query("SELECT l.loan_amount, l.id as loan_id, l.talimar_loan, ls.available_release, lp.property_address, ls.blasted_bai, ls.process_bai, ls.posted_bai FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan JOIN loan_property as lp ON l.talimar_loan = lp.talimar_loan WHERE ls.blasted_bai != '1' AND ls.construction_status = '2'");
		if($fetchbeforeafterss->num_rows() > 0){

			$fetchbeforeafterss = $fetchbeforeafterss->result();
			$data['Count_BA_video'] = count($fetchbeforeafterss);
		}else{
			$data['Count_BA_video'] = 0;
		}

		//Assignments in Process
		$fetchAssdata = $this->User_model->query("SELECT la.talimar_loan, la.lender_name, la.investment, la.nfunds_received, la.recording_status, la.nservicer_status, la.ndisclouser_status FROM loan_assigment as la JOIN loan_servicing as ls ON la.talimar_loan = ls.talimar_loan WHERE ls.loan_status = '2' AND la.nsecurity_type = '2' AND nfiles_status = '0' ORDER BY ls.talimar_loan ASC");
		if($fetchAssdata->num_rows() > 0){
			$fetchAssdata = $fetchAssdata->result();
			$data['totalAssognCount'] = count($fetchAssdata);
		}else{
			$data['totalAssognCount'] = 0;
		}

		$content1 = $this->load->view('dashboard/action_item', $data, true);
		echo $content1;

	}

//..............................ajax year to date statistics end ................................................//

	public function ajax_trust_deed() {
		error_reporting(0);
		// Fetch all loans

		//$join_sql = "SELECT *,l.id as loan_id FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.market_trust_deed = '1' AND ls.loan_status IN('1','2') ORDER BY ls.loan_status DESC";

		$join_sql = "SELECT *,l.id as loan_id FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.`closing_status` IN('2','3','4','5','6','8') AND ls.loan_status IN('1','2')";

		$fetch_all_loans = $this->User_model->query($join_sql);
		$fetch_all_loans = $fetch_all_loans->result();

		//echo $count = count($fetch_all_loans);

		foreach ($fetch_all_loans as $row) {

			$talimar_rows['talimar_loan'] = $row->talimar_loan;

			//fetch lender details...
			$fetch_lender = $this->User_model->query("SELECT * FROM loan_servicing as ls JOIN loan_assigment as la JOIN investor as i on ls.talimar_loan = la.talimar_loan AND la.lender_name = i.id WHERE ls.talimar_loan = '" . $row->talimar_loan . "' ");
			$results = $fetch_lender->row();

			// fetch assigment data sum of total investment
			$sql = "SELECT SUM(investment) as investment_total FROM loan_assigment WHERE talimar_loan = '" . $row->talimar_loan . "' ";
			$assigment_total_investment = $this->User_model->query($sql);
			$assigment_total_investment = $assigment_total_investment->result();
			$assigment_total_investment = $assigment_total_investment[0]->investment_total;

			// Count lender from assigment
			$sql = "SELECT COUNT(*) as count_lender FROM loan_assigment WHERE talimar_loan = '" . $row->talimar_loan . "' ";
			$count_assigment_lender = $this->User_model->query($sql);
			$count_assigment_lender = $count_assigment_lender->result();
			$count_assigment_lender = $count_assigment_lender[0]->count_lender;

			// fetch property_data

			$fetch_property_data = $this->User_model->query("select property_address from loan_property where talimar_loan = '" . $row->talimar_loan . "'");
			$fetch_property_data = $fetch_property_data->result();

			$avilable_deed_trust = $row->loan_amount - $assigment_total_investment;

			//echo $avilable_deed_trust.'<br>';
			//if($avilable_deed_trust > 0 || $row->loan_status == 1)
			if ($avilable_deed_trust > 0 && ($row->closing_status == '2' || $row->closing_status == '9' || $row->closing_status == '3' || $row->closing_status == '4' || $row->closing_status == '5' || $row->closing_status == '8' || $row->closing_status == '6')) {

				// $trust_deed_customize[$row->loan_status][] = array(
				// 'talimar_loan' 				=> $row->talimar_loan,
				// 'loan_id' 					=> $row->loan_id,
				// 'loan_amount' 				=> $row->loan_amount,
				// 'intrest_rate' 				=> $row->intrest_rate,
				// 'market_trust_deed' 		=> $row->market_trust_deed,
				// 'market_trust_deed' 		=> $row->market_trust_deed,
				// 'trust_deed_posted_on_website' 		=> $row->trust_deed_posted_on_website,
				// 'total_investment' 			=> $assigment_total_investment,
				// 'count_assigment_lender' 	=> $count_assigment_lender,
				// 'servicing_fee' 			=> $row->servicing_fee,
				// 'loan_status' 				=> $row->loan_status,
				// 'closing_status' 			=> $row->closing_status,
				// 'lender_minimum' 			=> $row->lender_minimum,
				// 'posted_on_website' 		=> $row->trust_deed_posted_on_website,
				// 'comming_soon' 				=> $row->comming_soon,
				// 'property_address' 			=> $fetch_property_data[0]->property_address,
				// 'avilable_deed_trust' 		=> $avilable_deed_trust,

				//);
				$trust_deed[] = array(
					'talimar_loan' => $row->talimar_loan,
					'loan_id' => $row->loan_id,
					'loan_amount' => $row->loan_amount,
					'intrest_rate' => number_format($row->intrest_rate,3),
					'market_trust_deed' => $row->market_trust_deed,
					'market_trust_deed' => $row->market_trust_deed,
					'trust_deed_posted_on_website' => $row->trust_deed_posted_on_website,
					'total_investment' => $assigment_total_investment,
					'count_assigment_lender' => $count_assigment_lender,
					'servicing_fee' => $row->servicing_fee,
					'loan_status' => $row->loan_status,
					'closing_status' => $row->closing_status,
					'lender_minimum' => $row->lender_minimum,
					'posted_on_website' => $row->trust_deed_posted_on_website,
					'comming_soon' => $row->comming_soon,
					'property_address' => $fetch_property_data[0]->property_address,
					'avilable_deed_trust' => $avilable_deed_trust,

				);
			}

		}

		//sort($trust_deed);

		$data['trust_deed'] = $trust_deed;
		$content1 = $this->load->view('dashboard/ajax_trust_deed', $data, true);
		echo $content1;

	}

	public function capital_requirement() {
		error_reporting(0);
		//for 5 days...

		$current_date = date('Y-m-d');
		$after_five_day = date('Y-m-d', strtotime('+5 days'));

		//$five_days_loans = $this->User_model->query("Select id,talimar_loan,loan_amount from loan where loan_funding_date BETWEEN '".$current_date."' AND '".$after_five_day."'");

		//$five_days_loans = $this->User_model->query("Select l.id, l.talimar_loan, l.loan_amount, ls.loan_status, ls.closing_status from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status = '1' AND ls.`closing_status` IN('2','3','4','5','6','8') AND l.loan_funding_date BETWEEN '".$current_date."' AND '".$after_five_day."'");

		$five_days_loans = $this->User_model->query("Select l.id, l.talimar_loan, l.loan_amount, ls.loan_status, ls.closing_status from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status = '1' AND ls.`closing_status` IN('2','3','4','5','6','8') AND l.loan_funding_date <= '" . $after_five_day . "'");

		if ($five_days_loans->num_rows() > 0) {

			$five_days_loans = $five_days_loans->result();

			foreach ($five_days_loans as $row) {

				$talimar_loan = $row->talimar_loan;

				$avilable = $this->User_model->query("SELECT SUM(`investment`) as total_investment FROM `loan_assigment` WHERE `talimar_loan` = '" . $talimar_loan . "'");
				$avilable = $avilable->result();
				$total_investment = $avilable[0]->total_investment;

				$avilable_grater_then_zero = $row->loan_amount - $total_investment;

				if ($avilable_grater_then_zero > 0) {

					$capital_requirements[] = array(

						"talimar_loan" => $row->talimar_loan,
						"loan_amount" => $row->loan_amount,
						"total_investment" => $total_investment,

					);
				}

			}
		}

		// echo '<pre>';
		// print_r($capital_requirements);
		// echo '</pre>';
		$data['capital_requirement_for_fiveday'] = $capital_requirements;

		//for 10 days AND only Pipeline Loans...

		$current_dates = date('Y-m-d');
		$after_ten_day = date('Y-m-d', strtotime('+30 days'));

		//$ten_days_loans = $this->User_model->query("Select l.id, l.talimar_loan, l.loan_amount, ls.loan_status, ls.closing_status from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status = '1' AND ls.`closing_status` IN('2','3','4','5','6','8') AND l.loan_funding_date BETWEEN '".$current_dates."' AND '".$after_ten_day."'");

		$ten_days_loans = $this->User_model->query("Select l.id, l.talimar_loan, l.loan_amount, ls.loan_status, ls.closing_status from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status = '1' AND ls.`closing_status` IN('2','3','4','5','6','8') AND l.loan_funding_date <= '" . $after_ten_day . "'");

		if ($ten_days_loans->num_rows() > 0) {

			$ten_days_loans = $ten_days_loans->result();

			foreach ($ten_days_loans as $row) {

				$talimar_loan = $row->talimar_loan;

				$avilable = $this->User_model->query("SELECT SUM(`investment`) as total_investment FROM `loan_assigment` WHERE `talimar_loan` = '" . $talimar_loan . "'");
				$avilable = $avilable->result();
				$total_investment = $avilable[0]->total_investment;

				$avilable_grater_then_zero = $row->loan_amount - $total_investment;

				if ($avilable_grater_then_zero > 0) {

					$capital_requirements_tenday[] = array(

						"talimar_loan" => $row->talimar_loan,
						"loan_amount" => $row->loan_amount,
						"total_investment" => $total_investment,

					);
				}

			}
		}

		$data['capital_requirement_for_tenday'] = $capital_requirements_tenday;

		//for 15 days AND only Pipeline Loans...

		$current_datess = date('Y-m-d');
		$after_fifteen_day = date('Y-m-d', strtotime('+15 days'));

		//$fifteen_days_loans = $this->User_model->query("Select l.id, l.talimar_loan, l.loan_amount, ls.loan_status, ls.closing_status from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status = '1' AND ls.`closing_status` IN('2','3','4','5','6','8') AND l.loan_funding_date BETWEEN '".$current_datess."' AND '".$after_fifteen_day."'");

		$fifteen_days_loans = $this->User_model->query("Select l.id, l.talimar_loan, l.loan_amount, ls.loan_status, ls.closing_status from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status = '1' AND ls.`closing_status` IN('2','3','4','5','6','8') AND l.loan_funding_date <= '" . $after_fifteen_day . "'");

		if ($fifteen_days_loans->num_rows() > 0) {

			$fifteen_days_loans = $fifteen_days_loans->result();

			foreach ($fifteen_days_loans as $row) {

				$talimar_loan = $row->talimar_loan;

				$avilable = $this->User_model->query("SELECT SUM(`investment`) as total_investment FROM `loan_assigment` WHERE `talimar_loan` = '" . $talimar_loan . "'");
				$avilable = $avilable->result();
				$total_investment = $avilable[0]->total_investment;

				$avilable_grater_then_zero = $row->loan_amount - $total_investment;

				if ($avilable_grater_then_zero > 0) {

					$capital_requirements_fifteen[] = array(

						"talimar_loan" => $row->talimar_loan,
						"loan_amount" => $row->loan_amount,
						"total_investment" => $total_investment,

					);
				}

			}
		}

		$data['capital_requirement_for_fifteen'] = $capital_requirements_fifteen;

		//for 30 days...

		$current_datesss = date('Y-m-d');
		$after_thirty_day = date('Y-m-d', strtotime('+30 days'));

		//$thirty_days_loans = $this->User_model->query("Select l.id, l.talimar_loan, l.loan_amount, ls.loan_status, ls.closing_status from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status = '1' AND ls.`closing_status` IN('2','3','4','5','6','8') AND l.loan_funding_date BETWEEN '".$current_datesss."' AND '".$after_thirty_day."'");

		$thirty_days_loans = $this->User_model->query("Select l.id, l.talimar_loan, l.loan_amount, ls.loan_status, ls.closing_status from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status = '1' AND ls.`closing_status` IN('2','3','4','5','6','8') AND l.loan_funding_date <= '" . $after_thirty_day . "'");

		if ($thirty_days_loans->num_rows() > 0) {

			$thirty_days_loans = $thirty_days_loans->result();

			foreach ($thirty_days_loans as $row) {

				$talimar_loan = $row->talimar_loan;

				$avilable = $this->User_model->query("SELECT SUM(`investment`) as total_investment FROM `loan_assigment` WHERE `talimar_loan` = '" . $talimar_loan . "'");
				$avilable = $avilable->result();
				$total_investment = $avilable[0]->total_investment;

				$avilable_grater_then_zero = $row->loan_amount - $total_investment;

				if ($avilable_grater_then_zero > 0) {

					$capital_requirements_thirty[] = array(

						"talimar_loan" => $row->talimar_loan,
						"loan_amount" => $row->loan_amount,
						"total_investment" => $total_investment,

					);
				}

			}
		}

		$data['capital_requirement_for_thirty'] = $capital_requirements_thirty;

		$content1 = $this->load->view('dashboard/capital_requirement', $data, true);
		echo $content1;

	}

	function ajax_beforeandafterdata(){

		$fetchbeforeafter = $this->User_model->query("SELECT l.loan_amount, l.id as loan_id, l.talimar_loan, ls.available_release, lp.property_address, ls.blasted_bai, ls.process_bai,ls.compleate_bai,ls.posted_bai FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan JOIN loan_property as lp ON l.talimar_loan = lp.talimar_loan WHERE l.loan_type = '2' AND ls.construction_status = '2' AND ls.image_ab_activate = 'yes' AND ls.image_ab_Posted='No' ");
		if($fetchbeforeafter->num_rows() > 0){

			$fetchbeforeafter = $fetchbeforeafter->result();
			foreach ($fetchbeforeafter as $value) {

				$explodeArray = explode(",",$value->available_release);

				//if(!in_array(5, $explodeArray) && !in_array(4, $explodeArray)){
				
					$beforeandafterdataArray[] = array(
														"loan_id" => $value->loan_id,
														"loan_amount" => $value->loan_amount,
														"talimar_loan" => $value->talimar_loan,
														"property_address" => $value->property_address,
														"available_release" => $value->available_release,
														"blasted_bai" => $value->blasted_bai,
														"process_bai" => $value->process_bai,
														"posted_bai" => $value->posted_bai,
														"compleate_bai" => $value->compleate_bai
												);
				//}
			}
		}else{
			$beforeandafterdataArray = '';
		}

		$data['beforeandafterdataArray'] = $beforeandafterdataArray;

		$content1 = $this->load->view('dashboard/beforeandafter', $data, true);
		echo $content1;
	}

	function balance_sheet_loan() {
		error_reporting(0);

		$sql = $this->User_model->query("SELECT *,l.id FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.loan_status ='2'");

		if ($sql->num_rows() > 0) {

			$balance_sheet_data = $sql->result();

			foreach ($balance_sheet_data as $row) {

				$talimar_loan = $row->talimar_loan;
				$loan_id = $row->id;

				$loan_amount = $row->loan_amount;

				$avilable_balance = $this->User_model->query("SELECT *,SUM(`investment`) as total_investment FROM `loan_assigment` WHERE `talimar_loan` = '" . $talimar_loan . "'");

				$avilable_bal = $avilable_balance->result();

				// echo '</pre>';
				// print_r($avilable_bal);
				// echo '</pre>';
				$total_investment = $avilable_bal[0]->total_investment;

				$avilable_bal_grater = $loan_amount - $total_investment;

				$sql_second = $this->User_model->query("select * from loan_property where talimar_loan = '" . $talimar_loan . "'");

				$active_loan_address = $sql_second->result();

				$active_address = $active_loan_address[0]->property_address;

				if ($avilable_bal_grater > 0) {

					$avilable_balance_array[] = array(

						"avilable_balence" => $avilable_bal_grater,
						"address" => $active_address,
						"loan_id" => $loan_id,
						"loan_amount" => $loan_amount,

					);

				}

			}

		}

		$data['balance_sheet'] = $avilable_balance_array;

		$content1 = $this->load->view('dashboard/balance_sheet_loan', $data, true);
		echo $content1;

	}

	function OutstandingFundsData(){

		$sqlquery = $this->User_model->query("SELECT l.id as loan_id, lp.property_address, SUM(la.investment) as loan_amount FROM loan as l join loan_servicing as ls ON l.talimar_loan = ls.talimar_loan JOIN loan_property as lp ON l.talimar_loan = lp.talimar_loan JOIN loan_assigment as la ON l.talimar_loan = la.talimar_loan where ls.loan_status = '2' AND la.paid_to = '2' AND la.nfunds_received IN('0','1','2') GROUP BY la.talimar_loan");
		if($sqlquery->num_rows() > 0){

			$sqlquery = $sqlquery->result();
			$sqlqueryData = $sqlquery;
		}else{
			$sqlqueryData = '';
		}

		$data['fundsData'] = $sqlqueryData;

		$data['abc'] = '';
		$content1 = $this->load->view('dashboard/outstanding-funds', $data, true);
		echo $content1;
	}

	function available_capital() {
		error_reporting(0);

		//       $sql=$this->User_model->query("SELECT * FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.loan_status IN ('2')");

		// if($sql->num_rows() > 0){

		// 	$balance_sheet_data = $sql->result();

		// 	foreach($balance_sheet_data as $row){

		// 		$talimar_loan = $row->talimar_loan;

		// 	 	$loan_amount =  $row->loan_amount;

		// 		$avilable_balance = $this->User_model->query("SELECT *,SUM(`investment`) as total_investment FROM `loan_assigment` WHERE `talimar_loan` = '".$talimar_loan."'");

		// 			$avilable_bal = $avilable_balance->result();

		// 			// echo '</pre>';
		// 			// print_r($avilable_bal);
		// 			// echo '</pre>';
		// 	     $total_investment = $avilable_bal[0]->total_investment;

		// 			$avilable_bal_grater = $loan_amount - $total_investment;

		// 			  if($avilable_bal_grater > 0){

		// 				$available_cap_data[] = array(

		// 												"avilable_balence" 	=> $avilable_bal_grater,
		// 												"loan_amount"       => $loan_amount

		// 											);

		// 		   }

		// 		}

		// }

		$query = $this->User_model->query("select * from fund_data");
		if ($query->num_rows() > 0) {

			$fetch_data = $query->result();
			$data['fund_data_result'] = $fetch_data;

		} else {

			$data['fund_data_result'] = '';
		}

		//$data['available_cap_data']	= $available_cap_data;

		$content1 = $this->load->view('dashboard/avaliable_capital', $data, true);
		echo $content1;

	}

	function upcoming_paid_off() {
		error_reporting(0);
		
		$sql = "SELECT l.id, l.borrower, l.talimar_loan,l.current_balance,l.loan_amount, l.intrest_rate, l.term_month, ls.demand_requested_date, pop.de_Good_Thru_Date, pop.de_Admin_Approved FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan JOIN pay_off_processing as pop ON l.talimar_loan = pop.talimar_loan WHERE ls.loan_status = '2' AND pop.de_Payoff_Request = '1' ORDER BY pop.de_Good_Thru_Date ASC";

		$fetch_loan = $this->User_model->query($sql);
		if ($fetch_loan->num_rows() > 0) {
			$fetch_loan = $fetch_loan->result();
			foreach ($fetch_loan as $loan) {

				$borrower_id = $loan->borrower;
				$talimar_loan = $loan->talimar_loan;

				$sql = "SELECT p.property_address, p.city, p.state, p.zip, p.unit FROM loan_property as p JOIN property_home as ph ON p.property_home_id = ph.id WHERE p.talimar_loan = '" . $talimar_loan . "' AND ph.primary_property = '1'";
				$fetch_property = $this->User_model->query($sql);
				if ($fetch_property->num_rows() > 0) {
					$loan_property = $fetch_property->row();
					$property_address = $loan_property->property_address;

				}

				$data['upcoming_payoff_schedule'][] = array(
					'loan_id' => $loan->id,
					'talimar_loan' => $loan->talimar_loan,
					'loan_amount' => $loan->loan_amount,
					'current_balance' => $loan->current_balance,
					'demand_requested_date' => $loan->de_Good_Thru_Date,
					'property_address' => $property_address,
					'de_Admin_Approved' => $loan->de_Admin_Approved,

				);
			}
		}

		$content1 = $this->load->view('dashboard/ajax_upcoming', $data, true);
		echo $content1;

	}

	public function OutstandingClosingFeesData() {

		$fetchfees = $this->User_model->query("SELECT ls.talimar_loan, lp.property_address, lp.loan_id FROM `loan_servicing` as ls JOIN loan_property as lp ON ls.talimar_loan = lp.talimar_loan JOIN loan_servicing_checklist as lsc ON ls.talimar_loan = lsc.talimar_loan WHERE ls.loan_status in (2,5) AND lsc.hud = '1014' and lsc.checklist !='1'");
		if($fetchfees->num_rows() > 0){

			$fetchfees = $fetchfees->result();
			foreach ($fetchfees as $value) {
				
				$fetchamt = $this->User_model->query("SELECT SUM(`paid_to_broker`) as talimarAmount FROM `closing_statement_items` WHERE `talimar_loan`='".$value->talimar_loan."' AND `paid_to`='TaliMar Financial'");
				$fetchamt = $fetchamt->result();
				$feeAmount = $fetchamt[0]->talimarAmount;

				$outFeesdata[] = array(
										"property_address" => $value->property_address,
										"loan_id" => $value->loan_id,
										"feeAmount" => $feeAmount,

										);

			}

			$data['feesData'] = $outFeesdata;

		}else{

			$data['feesData'] = '';
		}

		$content1 = $this->load->view('dashboard/ajax_outstanding_fee', $data, true);
		echo $content1;

	}

	public function ajax_loan_servicing() {
		error_reporting(0);

		$loginuser_dashboard = $this->loan_organization_setting();
		if ($loginuser_dashboard == 1) {
			$condition = "";
		} else {
			$condition = " AND loan.t_user_id = '" . $this->session->userdata('t_user_id') . "'";
		}

		//Pre-Submitted...
		$loan_Pre_submited = 'SELECT COUNT(*) as count_not_sub, SUM(loan.loan_amount) as not_sub_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan  JOIN loan_servicing_checklist ON loan_servicing_checklist.talimar_loan=loan_servicing.talimar_loan WHERE loan_servicing.loan_status = "2"  AND loan_servicing_checklist.hud="1008" AND (loan_servicing_checklist.checklist !="1"  AND loan_servicing_checklist.not_applicable !="1" ) ' . $condition . '';

		$loan_pre_submited_fetch = $this->User_model->query($loan_Pre_submited);
		$loan_pre_submited_fetch = $loan_pre_submited_fetch->result();
		$data['count_loan_not_submited_fetch'] = $loan_pre_submited_fetch[0]->count_not_sub;
		$data['sum_loan_not_submited_fetch'] = $loan_pre_submited_fetch[0]->not_sub_amount;


		//Pre-Boarding...
		$loan_Pre_Boarding = 'SELECT loan.talimar_loan,loan_servicing_checklist.checklist FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan  JOIN loan_servicing_checklist ON loan_servicing_checklist.talimar_loan=loan_servicing.talimar_loan WHERE loan_servicing.loan_status = "2"  AND loan_servicing_checklist.hud="1008" AND (loan_servicing_checklist.checklist = "1" OR loan_servicing_checklist.not_applicable="1" )' . $condition . '';
		$loan_Pre_Boardings = $this->User_model->query($loan_Pre_Boarding);
		$loan_Pre_Boardings = $loan_Pre_Boardings->result();
		$precount_loan_Pre_Boardings = 0;
		$presum_loan_Pre_Boardings = 0;
		foreach($loan_Pre_Boardings as $value){
			$loan_pre_boardingsssnews = 'SELECT loan_servicing_checklist.loan_id,loan.loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan  JOIN loan_servicing_checklist ON loan_servicing_checklist.talimar_loan=loan_servicing.talimar_loan WHERE loan.talimar_loan = "'.$value->talimar_loan.'" AND loan_servicing.loan_status = "2"  AND loan_servicing_checklist.hud="1035" AND ( loan_servicing_checklist.checklist != "1" AND  loan_servicing_checklist.not_applicable != "1" ) ' . $condition . '';
			
			$loan_pre_boardingsssnew = $this->User_model->query($loan_pre_boardingsssnews);
			if($loan_pre_boardingsssnew->num_rows() > 0){
				
				$loan_pre_boardingsssnew = $loan_pre_boardingsssnew->result();
				
				$precount_loan_Pre_Boardings++;
				$presum_loan_Pre_Boardings += $loan_pre_boardingsssnew[0]->loan_amount;

			}
		}
		$data['count_Boarding'] = $precount_loan_Pre_Boardings;
		$data['sum_loan_Boarding'] = $presum_loan_Pre_Boardings;

		//Boarding...
		$loan_pre_boardingsss = 'SELECT loan.talimar_loan,loan_servicing_checklist.checklist FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan  JOIN loan_servicing_checklist ON loan_servicing_checklist.talimar_loan=loan_servicing.talimar_loan WHERE loan_servicing.loan_status = "2"  AND loan_servicing_checklist.hud="1035" AND ( loan_servicing_checklist.checklist = "1" OR loan_servicing_checklist.not_applicable = "1" ) ' . $condition . '';

		$loan_pre_boardingsss = $this->User_model->query($loan_pre_boardingsss);
		$loan_pre_boardingsss = $loan_pre_boardingsss->result();
		
		$precount_loan_not_submited_fetch = 0;
		$presum_loan_not_submited_fetch = 0;
		foreach($loan_pre_boardingsss as $value){
			$loan_pre_boardingsssnew = 'SELECT loan.loan_amount,loan_servicing_checklist.loan_id FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan  JOIN loan_servicing_checklist ON loan_servicing_checklist.talimar_loan=loan_servicing.talimar_loan WHERE loan.talimar_loan = "'.$value->talimar_loan.'" AND loan_servicing.loan_status = "2"  AND loan_servicing_checklist.hud="1026" AND ( loan_servicing_checklist.checklist != "1" AND  loan_servicing_checklist.not_applicable != "1" ) ' . $condition . '';
			
			$loan_pre_boardingsssnew = $this->User_model->query($loan_pre_boardingsssnew);
			if($loan_pre_boardingsssnew->num_rows() > 0){
				$loan_pre_boardingsssnew = $loan_pre_boardingsssnew->result();
				$precount_loan_not_submited_fetch++;
				$presum_loan_not_submited_fetch += $loan_pre_boardingsssnew[0]->loan_amount;

			}
		}

		/*foreach($final_arrayBoarding as $values){
			
			$final_boardingsql = 'SELECT loan.loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan  JOIN loan_servicing_checklist ON loan_servicing_checklist.talimar_loan=loan_servicing.talimar_loan WHERE loan.talimar_loan = "'.$values['talimar_loan'].'" AND loan_servicing.loan_status = "2"  AND loan_servicing_checklist.hud="1035" AND loan_servicing_checklist.checklist != "1" ' . $condition . '';
			
			$final_boardingsql = $this->User_model->query($final_boardingsql);
			if($final_boardingsql->num_rows() > 0){
				
				$final_boardingsql = $final_boardingsql->result();

				$precount_loan_not_submited_fetch++;
				$presum_loan_not_submited_fetch += $final_boardingsql[0]->loan_amount;
			}
		}*/

		$data['Boarding_count'] = $precount_loan_not_submited_fetch;
		$data['Boarding_total'] = $presum_loan_not_submited_fetch;
				
		
		//Final Boarding...

		//Pre-Boarding...
		//$final_boardingsssnewss = 'SELECT loan.talimar_loan,loan_servicing_checklist.checklist FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan  JOIN loan_servicing_checklist ON loan_servicing_checklist.talimar_loan=loan_servicing.talimar_loan WHERE loan_servicing.loan_status = "2"  AND loan_servicing_checklist.hud="1026" AND (loan_servicing_checklist.checklist = "1" OR loan_servicing_checklist.not_applicable="1" )' . $condition . '';

		$fetchboardingdata = $this->User_model->query("SELECT l.id as loan_id, l.talimar_loan, l.loan_amount, l.borrower, lp.property_address, lp.unit, lp.city, lp.state, lp.zip FROM loan as l 
			JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan 
			JOIN loan_property as lp ON l.talimar_loan = lp.talimar_loan 
			JOIN loan_servicing_checklist as lsc ON l.talimar_loan = lsc.talimar_loan 
			JOIN input_datas as ida ON lsc.talimar_loan = ida.talimar_loan 
			WHERE ls.loan_status = '2' AND (lsc.hud = '1026' AND lsc.checklist ='1') AND (ida.input_name='checklist_complete' AND ida.page='loan_servicing_checklist' AND ida.value !='1')  GROUP BY l.id");
		

		$TaliMarFinalReviewCount = 0;
		$TaliMarFinalReviewATM = 0;
		if($fetchboardingdata->num_rows() > 0){
			$fetchboardingdata = $fetchboardingdata->result();
			$TaliMarFinalReviewCount = count($fetchboardingdata);
			foreach ($fetchboardingdata as $value) {
				if(!empty($value->loan_amount)){
					$TaliMarFinalReviewATM = $TaliMarFinalReviewATM + $value->loan_amount;
				}				
			}
		}

		
		$data['final_boardingsss_count'] = $TaliMarFinalReviewCount;
		$data['final_boardingsss_total'] = $TaliMarFinalReviewATM;







		/*foreach($loan_pre_boardingsss as $value){
			
			$final_boardingsssnew = 'SELECT loan.talimar_loan FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan  JOIN loan_servicing_checklist ON loan_servicing_checklist.talimar_loan=loan_servicing.talimar_loan WHERE loan.talimar_loan = "'.$value->talimar_loan.'" AND loan_servicing.loan_status = "2"  AND loan_servicing_checklist.hud="1029" AND loan_servicing_checklist.checklist = "1" ' . $condition . '';
			
			$final_boardingsssnew = $this->User_model->query($final_boardingsssnew);
			if($final_boardingsssnew->num_rows() > 0){
				
				$final_boardingsssnew = $final_boardingsssnew->result();
				
				$final_array[] = array('talimar_loan'=> $final_boardingsssnew[0]->talimar_loan);
			}
		}
		
		//$precount_final_boardingsssnewss = 0;
		//$presum_final_boardingsssnewss = 0;

		foreach($final_array as $values){
			
			$final_boardingsssnewss = 'SELECT loan.talimar_loan,loan.loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan  JOIN loan_servicing_checklist ON loan_servicing_checklist.talimar_loan=loan_servicing.talimar_loan WHERE loan.talimar_loan = "'.$values['talimar_loan'].'" AND loan_servicing.loan_status = "2"  AND loan_servicing_checklist.hud="1035" AND loan_servicing_checklist.checklist = "1" ' . $condition . '';
			
		
			
			$final_boardingsssnewss = $this->User_model->query($final_boardingsssnewss);
			if($final_boardingsssnewss->num_rows() > 0){
				
				$final_boardingsssnewss = $final_boardingsssnewss->result();

				$final_array11[] = array('talimar_loan'=> $final_boardingsssnewss[0]->talimar_loan);
				
				//$precount_final_boardingsssnewss++;
				//$presum_final_boardingsssnewss += $final_boardingsssnewss[0]->loan_amount;
			}
		}

		$precount_final_boardingsssnewss = 0;
		$presum_final_boardingsssnewss = 0;
		foreach($final_array11 as $values){*/
			/*
			  Date:28-06-2021
			  Title: Bitcot new changes
			  Details:New changes according to task
			  Update date-28-06-2021
			*/
			/*$final_boardingsssnewss = 'SELECT loan.loan_amount, loan.talimar_loan FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan  JOIN loan_servicing_checklist ON loan_servicing_checklist.talimar_loan=loan_servicing.talimar_loan WHERE loan.talimar_loan = "'.$values['talimar_loan'].'" AND loan_servicing.loan_status = "2"  AND loan_servicing_checklist.hud="1026" AND loan_servicing_checklist.checklist != "1" ' . $condition . '';*/
			/*$final_boardingsssnewss = 'SELECT loan.loan_amount, loan.talimar_loan FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan   join input_datas as ida ON loan_servicing.talimar_loan = ida.talimar_loan JOIN loan_servicing_checklist as lsc ON lsc.talimar_loan=loan_servicing.talimar_loan WHERE loan.talimar_loan = "'.$values['talimar_loan'].'" AND loan_servicing.loan_status = "2"  AND ida.page="loan_servicing_checklist" AND ida.value !="1" AND ((lsc.items = "Complete Loan Servicer Review Checklist" AND lsc.checklist = 0) OR (lsc.items = "Loan Final Boarded with Servicer" AND lsc.checklist = 1)) ' . $condition . '';
			
			$final_boardingsssnewss = $this->User_model->query($final_boardingsssnewss);
			if($final_boardingsssnewss->num_rows() > 0){
				
				$final_boardingsssnewss = $final_boardingsssnewss->result();

				$final_array1111[] = array('talimar_loan'=> $final_boardingsssnewss[0]->talimar_loan);
				
				$precount_final_boardingsssnewss++;
				$presum_final_boardingsssnewss += $final_boardingsssnewss[0]->loan_amount;
			}
		}

		
		foreach($final_array1111 as $values){

			$final_boardingsssnewss11 = 'SELECT loan.id as loan_id,loan.loan_amount, loan.talimar_loan FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan  JOIN loan_servicing_checklist ON loan_servicing_checklist.talimar_loan=loan_servicing.talimar_loan WHERE loan.talimar_loan = "'.$values['talimar_loan'].'" AND loan_servicing.loan_status = "2"  AND loan_servicing_checklist.hud="1015" AND loan_servicing_checklist.checklist != "1" ' . $condition . '';
			
		
			
			$final_boardingsssnewss11 = $this->User_model->query($final_boardingsssnewss11);
			if($final_boardingsssnewss11->num_rows() > 0){
				
				$final_boardingsssnewss11 = $final_boardingsssnewss11->result();

				$final_arraycheck22[] = $final_boardingsssnewss11;
				
				//$precount_final_boardingsssnewss++;
				//$presum_final_boardingsssnewss += $final_boardingsssnewss11[0]->loan_amount;
			}
		}

		/*echo '<pre>';
		print_r($final_arraycheck22);
		echo '</pre>';*/
		
		//$data['final_boardingsss_count'] = $precount_final_boardingsssnewss;
		//$data['final_boardingsss_total'] = $presum_final_boardingsssnewss;

		

		$content1 = $this->load->view('dashboard/ajax_loan_servicing', $data, true);
		echo $content1;

	}

	

	function AccruedChargesQuery() {
		error_reporting(0);
		$fetch_accrued_charge = $this->User_model->query("SELECT `talimar_loan`, checkPaid, SUM(`accured_amount`) as amount, SUM(`paid_amount`) as paid_amt FROM `accured_charges` GROUP BY `talimar_loan`");
		$fetch_accrued_charge = $fetch_accrued_charge->result();
		foreach ($fetch_accrued_charge as $value) {
			
			$fetch_property = $this->User_model->query("SELECT lp.loan_id, lp.property_address, ls.loan_status FROM loan_property as lp JOIN loan_servicing as ls ON ls.talimar_loan = lp.talimar_loan WHERE ls.talimar_loan ='".$value->talimar_loan."' AND ls.loan_status IN('1','2','3') GROUP BY ls.talimar_loan");
			$fetch_property = $fetch_property->result();

			if($fetch_property[0]->loan_status == '1' || $fetch_property[0]->loan_status == '2' || $fetch_property[0]->loan_status == '3'){

					$remaningamount = $value->amount - $value->paid_amt;

					if($remaningamount > 0){

						$accruedArray[] = array(
								"loan_id" 			=> $fetch_property[0]->loan_id,
								"property_address" 	=> $fetch_property[0]->property_address,
								"remaningamount" 	=> $remaningamount,
								
							);
					}
				}
		}

		$data['accruedchargedata'] = $accruedArray;
		$data['kk'] = '';
		$content1 = $this->load->view('dashboard/ajax_accrued_charges', $data, true);
		echo $content1;
	}

	function ajax_insurance() {
		error_reporting(0);

		$fetch_property_loan = $this->User_model->query("SELECT l.talimar_loan, l.id, l.borrower, lpi.policy_number, lpi.insurance_expiration, lpi.contact_name, lpi.request_updated_insurance, lpi.insurance_type, ls.loan_status, ph.primary_property,ph.property_address FROM loan as l JOIN loan_property_insurance as lpi ON l.talimar_loan = lpi.talimar_loan JOIN loan_servicing as ls ON ls.talimar_loan = l.talimar_loan JOIN property_home as ph ON ph.talimar_loan = l.talimar_loan WHERE ls.loan_status = 2 AND ph.primary_property = '1' AND lpi.insurance_type = '1'  AND lpi.required IN ('1','0') GROUP BY l.talimar_loan order by lpi.insurance_expiration");

		$fetch_property_loan = $fetch_property_loan->result();

		foreach ($fetch_property_loan as $row) {
			
			$data['fetch_property_insurance_loan'][] = array(

				'loan_id' => $row->id,
				'talimar_loan' => $row->talimar_loan,
				'insurance_expiration' => $row->insurance_expiration,
				'request_updated_insurance' => $row->request_updated_insurance,
				'property_address' => $row->property_address,

			);

		}

		$content1 = $this->load->view('dashboard/ajax_insurance', $data, true);
		echo $content1;

	}

	public function fci_servicing_schedule_data(){
		error_reporting(0);

		//FCI balance
		$fci_loan_total = $this->User_model->query("SELECT talimar_loan FROM lender_payment_history group by talimar_loan order by talimar_loan ");
		$fci_loan_total = $fci_loan_total->result();



		$fetch_fci_loan_total = $this->User_model->query("SELECT COUNT(*) as totalCount, SUM(currentBalance) as totalAmount FROM lender_payment_history  ");
		$fetch_fci_loan_total = $fetch_fci_loan_total->row();


		$FcitotalCount = $fetch_fci_loan_total->totalCount;
		$FcitotalAmount = $fetch_fci_loan_total->totalAmount;

		$data['FcitotalCount'] 		= COUNT($fci_loan_total);
		$data['FcitotalAmount'] 	= $FcitotalAmount;



		//Del Toro balance
		// $deltoro_loan_total = $this->User_model->query("SELECT COUNT(*) as totalCount, SUM(l.current_balance) as totalAmount FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan JOIN loan_servicing_checklist as lsc ON l.talimar_loan = lsc.talimar_loan WHERE ls.loan_status = '2' AND ls.sub_servicing_agent = '19' AND lsc.hud = '1035' AND (lsc.checklist = '1' OR lsc.not_applicable = '1')");
		// $deltoro_loan_total = $deltoro_loan_total->result();
		// $deltorototalCount = $deltoro_loan_total[0]->totalCount;
		// $deltorototalAmount = $deltoro_loan_total[0]->totalAmount;

		// $data['DeltotalCount'] 		= $deltorototalCount;
		// $data['DeltotalAmount'] 	= $deltorototalAmount;



		//TaliMar Financial
		$TaliMar_loan_total = $this->User_model->query("SELECT COUNT(*) as totalCount, SUM(l.current_balance) as totalAmount FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.loan_status = '2' ");
		$TaliMar_loan_total = $TaliMar_loan_total->row();
		$TaliMarCount = $TaliMar_loan_total->totalCount;
		$TaliMarAmount = $TaliMar_loan_total->totalAmount;

		$data['TaliMarCount'] 		= $TaliMarCount;
		$data['TaliMarAmount'] 		= $TaliMarAmount;


		//Total
		$fetch_fci_loan_borded = $this->User_model->query("SELECT COUNT(*) as totalBCount, SUM(l.current_balance) as totalBAmount FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan JOIN loan_servicing_checklist as lsc ON l.talimar_loan = lsc.talimar_loan WHERE ls.loan_status = '2' AND lsc.hud = '1035' AND (lsc.checklist = '0' OR lsc.not_applicable = '0')");
		$fetch_fci_loan_borded = $fetch_fci_loan_borded->row();
		$FcitotalBCount = $fetch_fci_loan_borded->totalBCount;
		$FcitotalBAmount = $fetch_fci_loan_borded->totalBAmount;

		$data['FcitotalBCount'] 		= $FcitotalBCount;
		$data['FcitotalBAmount'] 		= $FcitotalBAmount;

		$content1 = $this->load->view('dashboard/ajax_fci_servicing_schedule', $data, true);
		echo $content1;
	}

	public function get_ltv_for_loan($talimar_loan, $loan_amount) {
		error_reporting(0);
		$property_datasql = "SELECT sum(`underwriting_value`) as uv_value FROM `loan_property` WHERE `talimar_loan`='" . $talimar_loan . "'";
		$property_data = $this->User_model->query($property_datasql);
		if($property_data->num_rows() > 0){
			$property_data = $property_data->result();
			$uv_value = $property_data[0]->uv_value;
		}else{
			$uv_value = 1;
		}

		$where_talimar['talimar_loan'] = $talimar_loan;

		$fetch_property_homez = $this->User_model->select_where_asc('property_home', $where_talimar, 'primary_property');
		$property_homesssss = $fetch_property_homez->result();
		$property_home_idsss=array();

		foreach($property_homesssss as $key=>$vv){

			$property_home_idsss[]=$vv->id;
		}
		
		$implode=implode("','",$property_home_idsss);

		$sql = "SELECT  SUM(current_balance) as total FROM encumbrances WHERE property_home_id IN ('" . $implode . "') AND talimar_loan = '" . $talimar_loan . "' AND will_it_remain = 0 ";

		$select_ecums = $this->User_model->query($sql);
		if($select_ecums->num_rows() > 0){
			$select_ecums = $select_ecums->result();
			$select_ecum = $select_ecums[0]->total ? $select_ecums[0]->total : 0;
		}else{
			$select_ecum = 0;
		}

		$ltv = (($select_ecum + $loan_amount)/ $uv_value) * 100;

		return $ltv;

	}


	public function all_borrower_name() {

		$fetch_borrower_data = $this->User_model->select_star('borrower_data');
		$fetch_borrower_data = $fetch_borrower_data->result();
		foreach ($fetch_borrower_data as $value) {
			
			$all_borrower_name[$value->id] = $value->b_name;
		}

		return $all_borrower_name;
	}

	public function active_trust_deedData() {
		// error_reporting(0);
		$fetch_activetrustdeed_loan = $this->User_model->query("SELECT l.id as loan_id, l.talimar_loan, l.borrower, l.loan_amount, l.loan_funding_date, ls.closing_status, ls.new_market_trust_deed, ls.servicing_lender_rate, l.intrest_rate, l.term_month, lp.property_address FROM loan as l JOIN `loan_servicing` as ls ON l.talimar_loan = ls.talimar_loan JOIN loan_property as lp ON l.talimar_loan = lp.talimar_loan GROUP BY l.id HAVING ls.new_market_trust_deed IN('2','3','4') ORDER BY ls.new_market_trust_deed DESC, l.loan_funding_date ASC");

		if($fetch_activetrustdeed_loan->num_rows() > 0) {
			$fetch_active_loan = $fetch_activetrustdeed_loan->result();
			foreach ($fetch_active_loan as $row) {

				$sum_assigment = "SELECT SUM(investment) as total_investment, invester_yield_percent FROM loan_assigment WHERE talimar_loan = '" . $row->talimar_loan . "'";
				$assigment_sum1 = $this->User_model->query($sum_assigment);
				$assigment_sum1 = $assigment_sum1->result();
				$assigment_investment_amount = $assigment_sum1[0]->total_investment;
				

				if ($row->servicing_lender_rate == 'NaN' || $row->servicing_lender_rate == '') {

					$invester_yield_percent = $assigment_sum1[0]->invester_yield_percent;
				} else {

					$invester_yield_percent = $row->servicing_lender_rate;
				}

				$ltv = $this->get_ltv_for_loan($row->talimar_loan, $row->loan_amount);

				$active_trust_deeds[] = array(
									'talimar' => $row->talimar_loan,
									'loan_id' => $row->loan_id,
									'loan_amount' => $row->loan_amount,
									'borrower' => $row->borrower,
									'loan_funding_date' => $row->loan_funding_date,
									'closing_status' => $row->new_market_trust_deed,
									'interest_rate' => number_format($row->intrest_rate,3),
									'ltv' => $ltv,
									'invester_yield_percent' => $invester_yield_percent,
									'term_month' => $row->term_month,
									'property_address' => $row->property_address,
									'unfunded' => $row->loan_amount - $assigment_investment_amount,
								);
			}
		}else{

			$active_trust_deeds = '';
		}


		$data['active_deed'] = $active_trust_deeds;
		$data['all_borrower_name'] = $this->all_borrower_name();

		$content1 = $this->load->view('dashboard/active_trust_deed', $data, true);
		echo $content1;

	}

	public function posted_trust_deed_function() {

		error_reporting(0);
		$loginuser_dashboard = $this->loan_organization_setting();
		if ($loginuser_dashboard == 1) {
			$fetch_active_loan = $this->User_model->query("SELECT * FROM `loan_servicing` WHERE new_market_trust_deed IN('2','3','4')");
		} else {
			$fetch_active_loan = $this->User_model->query("SELECT * FROM `loan_servicing` WHERE t_user_id = '" . $this->session->userdata('t_user_id') . "' AND new_market_trust_deed IN('2','3','4')");
		}

		if ($fetch_active_loan->num_rows() > 0) {
			$fetch_active_loan = $fetch_active_loan->result();
			foreach ($fetch_active_loan as $row) {
				$loan_number['talimar_loan'] = $row->talimar_loan;
				$fetch_loan_data = $this->User_model->select_where('loan', $loan_number);
				$fetch_loan_data = $fetch_loan_data->row();

				$fetch_property_data = $this->User_model->select_where('loan_property', $loan_number);
				$fetch_property_data = $fetch_property_data->row();

				$sum_assigment = "SELECT SUM(investment) as total_investment FROM loan_assigment WHERE talimar_loan = '" . $row->talimar_loan . "'";

				$assigment_sum1 = $this->User_model->query($sum_assigment);
				$assigment_sum1 = $assigment_sum1->result();
				$assigment_investment_amount = $assigment_sum1[0]->total_investment;

				$data['fetch_post_trust_deed'][] = array(
					'talimar' => $row->talimar_loan,
					'loan_id' => $fetch_loan_data->id,
					'loan_amount' => $fetch_loan_data->loan_amount,
					'avaliable_balance' => $fetch_loan_data->loan_amount - $assigment_investment_amount,
					'property_address' => $fetch_property_data->property_address,

				);

				// }
			}

		}
		$content1 = $this->load->view('dashboard/ajax_posted_trust_deed', $data, true);
		echo $content1;

	}

	public function paid_off_process() {

		error_reporting(0);

		$loginuser_dashboard = $this->loan_organization_setting();
		if ($loginuser_dashboard == 1) {

			$fetch_paidoff = $this->User_model->query("SELECT l.borrower,l.id,l.talimar_loan,ls.loan_status,ls.talimar_loan FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.loan_status = '3' GROUP BY ls.talimar_loan");
		} else {

			$fetch_paidoff = $this->User_model->query("SELECT l.borrower,l.id,l.talimar_loan,ls.loan_status,ls.talimar_loan FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.loan_status = '3' AND l.t_user_id = '" . $this->session->userdata('t_user_id') . "' GROUP BY ls.talimar_loan");
		}

		if ($fetch_paidoff->num_rows() > 0) {

			$fetch_paidoff = $fetch_paidoff->result();

			foreach ($fetch_paidoff as $row) {

				$talimar_loan = $row->talimar_loan;
				$borrower = $row->borrower;

				$select_payoff = $this->User_model->select_where('pay_off_processing', array('talimar_loan' => $talimar_loan));
				if ($select_payoff->num_rows() > 0) {

					$select_payoff = $select_payoff->result();

					if ($select_payoff[0]->payoff_processing == 2) {

						$array_1[] = array(
							"loan_id" => $row->id,
							"talimar_loan" => $select_payoff[0]->talimar_loan,
							"borrower" => $borrower,

						);

					} else {

						$array_1[] = array(
							"loan_id" => '',
							"talimar_loan" => '',
							"borrower" => '',
						);

					}

				} else {

					$array_2[] = array(
						"loan_id" => $row->id,
						"talimar_loan" => $talimar_loan,
						"borrower" => $borrower,

					);

				}

			}

			// $combine_array = array_merge($array_2,$array_1);

			foreach ($array_1 as $row) {

				if ($row['talimar_loan'] != '') {

					$talimar_loan = $row['talimar_loan'];
					$loan_id = $row['loan_id'];
					$fetch_b['id'] = $row['borrower'];
					$street_address = $this->User_model->query("SELECT property_address,talimar_loan FROM loan_property WHERE talimar_loan = '" . $talimar_loan . "' GROUP BY talimar_loan");
					$street_address = $street_address->result();
					$property_address = $street_address[0]->property_address;

					$fetch_borrower_data = $this->User_model->select_where('borrower_data', $fetch_b);
					$fetch_borrower_data = $fetch_borrower_data->result();

					$borrower_name = $fetch_borrower_data[0]->b_name;
					$bb_id = $fetch_borrower_data[0]->id;

					$array_send_payoff[] = array(
						"loan_id" => $loan_id,
						"address" => $property_address,
						"talimar" => $talimar_loan,
						"borrower_name" => $borrower_name,
						"b_id" => $bb_id,
					);

				}
			}

			$data['fetch_paid_offf'] = $array_send_payoff;
		}

		$content1 = $this->load->view('dashboard/ajax_paid_off_process', $data, true);
		echo $content1;

	}

	/*public function ajax_new_loan_servicing(){

		    	$upcoming_action_item = $this->User_model->query("SELECT COUNT(*) as total_upcoming FROM `loan_servicing_checklist` as lsc JOIN loan_servicing as ls ON lsc.talimar_loan = ls.talimar_loan WHERE ls.loan_status = '2' AND lsc.hud = '1014' AND lsc.checklist = '0'");
				$upcoming_action_item = $upcoming_action_item->result();
				$data['upcoming_action_item'] = $upcoming_action_item[0]->total_upcoming;

		    	$data['ss'] = '1';
		    	$content1 = $this->load->view('dashboard/ajax_new_loan_servicing',$data,true);
				echo $content1;

	*/

	public function ajax_Extension_Processing() {
		
		$fetch_ext_data = $this->User_model->query("SELECT es.`talimar_loan`, es.`extension_to`, lp.property_address, lp.loan_id, l.maturity_date,l.new_maturity_date FROM `extension_section` as es JOIN loan_servicing as ls ON es.`talimar_loan` = ls.talimar_loan JOIN loan_property as lp ON es.talimar_loan = lp.talimar_loan JOIN loan as l ON ls.talimar_loan = l.talimar_loan  JOIN property_home as ph ON lp.talimar_loan = ph.talimar_loan AND lp.property_home_id = ph.id WHERE ls.loan_status = '2' AND es.`ext_process_option` = '1' AND ph.primary_property = '1' ORDER BY l.maturity_date ASC");
		
		if($fetch_ext_data->num_rows() > 0){
			$fetch_ext_data = $fetch_ext_data->result();
			
			$data['fetch_ext_data'] = $fetch_ext_data;
		}else{
			$data['fetch_ext_data'] = '';
		}
		array_multisort(array_column($data['fetch_ext_data'], 'new_maturity_date'), SORT_ASC, $data['fetch_ext_data']);
		$data['ss'] = '1';
		$content1 = $this->load->view('dashboard/ajax_extension_processing',$data,true);
		echo $content1;
	}

	/*
		Description : This function  get data for swag Request
					  
		Author      : Bitcot
		Created     : 
		Modified    :01-04-2021
	*/
	
	
	public function ajax_SwagRequestData() {
		$contact_marketing_swag = array();
		$data = array();
		$data['contact_marketing_swag'] = '';
		$marketingSQL = $this->User_model->query("SELECT * FROM contact_marketing WHERE title_status = '1'");
		if($marketingSQL->num_rows() > 0){
			$marketingSQL = $marketingSQL->result();
			foreach ($marketingSQL as $value) {
				
				$getcontactdata = $this->User_model->query("SELECT contact_firstname, contact_lastname from contact where contact_id = '".$value->contact_id."'");
				if($getcontactdata->num_rows() > 0){
					$getcontactdata = $getcontactdata->row();
					$fullname = $getcontactdata->contact_firstname.' '.$getcontactdata->contact_lastname;
				}else{
					$fullname = '';
				}

				if($fullname !='' )
				{
					$contact_marketing_swag[] = array(

							"name" => $fullname,
							"contact_id" => $value->contact_id,
							"swag" => $value->swag,
							"note" => $value->note,
							"date" => $value->date,
							"status" => $value->title_status
						);
				}
			}

			$data['contact_marketing_swag'] = $contact_marketing_swag;

		}

		$data['ss'] = '1';
		$content1 = $this->load->view('dashboard/ajax_SwagRequest',$data,true);
		echo $content1;
	}

	public function ajax_assigment_signtur() {

		error_reporting(0);
		//loan_assigment
		$assigment_in_process_sql = "SELECT COUNT(*) as count_assigment FROM loan_assigment JOIN loan ON loan.talimar_loan = loan_assigment.talimar_loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE  loan_servicing.loan_status='2' AND loan_assigment.nfiles_status != '1' ";

		$assigment_in_process_result = $this->User_model->query($assigment_in_process_sql);
		$assigment_in_process_result = $assigment_in_process_result->result();

		$data['count_post_assigment_in_process'] = $assigment_in_process_result[0]->count_assigment;

		$assigment_sql = "SELECT COUNT(*) as pre_count_assigment FROM loan_assigment JOIN loan ON loan.talimar_loan = loan_assigment.talimar_loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan_assigment.nfiles_status = '0' AND loan_assigment.nsecurity_type IN ('1','2','3') AND loan_servicing.loan_status = '2'";
		$assigment_is_result = $this->User_model->query($assigment_sql);
		$assigment_result = $assigment_is_result->result();

		$data['open'] = $assigment_result[0]->pre_count_assigment;
		// echo '<pre>';
		// print_r($assigment_result);
		// echo '</pre>';

		$assigment_sql_2 = "SELECT COUNT(*) as pre_count_assigment FROM loan_assigment JOIN loan ON loan.talimar_loan = loan_assigment.talimar_loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan_assigment.nfiles_status = '0' AND loan_assigment.nsecurity_type='1' AND loan_servicing.loan_status IN('1','2')";

		$assigment_is_result_2 = $this->User_model->query($assigment_sql_2);
		$assigment_result_2 = $assigment_is_result_2->result();

		$data['deed'] = $assigment_result_2[0]->pre_count_assigment;

		$assigment_sql_3 = "SELECT COUNT(*) as pre_count_assigment FROM loan_assigment JOIN loan ON loan.talimar_loan = loan_assigment.talimar_loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan_assigment.nfiles_status = '0' AND loan_assigment.nsecurity_type='2' AND loan_servicing.loan_status = '2'";

		$assigment_is_result_3 = $this->User_model->query($assigment_sql_3);
		$assigment_result_3 = $assigment_is_result_3->result();

		$data['assigment'] = $assigment_result_3[0]->pre_count_assigment;

		$assigment_sql_4 = "SELECT COUNT(*) as pre_count_assigment FROM loan_assigment JOIN loan ON loan.talimar_loan = loan_assigment.talimar_loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan_assigment.nfiles_status = '0' AND loan_assigment.nsecurity_type='3' ";

		$assigment_is_result_4 = $this->User_model->query($assigment_sql_4);
		$assigment_result_4 = $assigment_is_result_4->result();

		$data['other'] = $assigment_result_4[0]->pre_count_assigment;

		//$assigment_sql_5 = "SELECT COUNT(*) as pre_count_assigment FROM loan_assigment JOIN loan ON loan.talimar_loan = loan_assigment.talimar_loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan_assigment.nfiles_status = '0' AND loan_assigment.ndisclouser_status='2' ";
		$assigment_sql_5 = "SELECT COUNT(*) as pre_count_assigment FROM loan_assigment WHERE nfiles_status = '0' AND ndisclouser_status='2' ";

		$assigment_is_result_5 = $this->User_model->query($assigment_sql_5);
		$assigment_result_5 = $assigment_is_result_5->result();

		$data['submiited'] = $assigment_result_5[0]->pre_count_assigment;

		//$assigment_sql_6 = "SELECT COUNT(*) as pre_count_assigment FROM loan_assigment JOIN loan ON loan.talimar_loan = loan_assigment.talimar_loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan_assigment.nfiles_status = '0' AND loan_assigment.ndisclouser_status='3' ";
		$assigment_sql_6 = "SELECT COUNT(*) as pre_count_assigment FROM loan_assigment WHERE nfiles_status = '0' AND ndisclouser_status='3' ";

		$assigment_is_result_6 = $this->User_model->query($assigment_sql_6);
		$assigment_result_6 = $assigment_is_result_6->result();

		$data['signed'] = $assigment_result_6[0]->pre_count_assigment;

		//$ready_for_sig ="SELECT COUNT(*) as pre_count_assigment FROM loan_assigment JOIN loan_servicing ON loan_servicing.talimar_loan = loan_assigment.talimar_loan WHERE loan_assigment.nfiles_status = '0' AND loan_assigment.ndisclouser_status= '1'";
		$ready_for_sig = "SELECT *,COUNT(*) as pre_count_assigment FROM loan_assigment WHERE nfiles_status = '0' AND ndisclouser_status= '1'";

		$ready_for_sig = $this->User_model->query($ready_for_sig);
		$ready_for_sig = $ready_for_sig->result();

		// echo '<pre>';
		// print_r($ready_for_sig);
		// echo '</pre>';

		$data['ready_count'] = $ready_for_sig[0]->pre_count_assigment;

		//deed_of_trust...
		//$deed_of_trustsql = "SELECT *, COUNT(*) as deed_of_trustcount FROM loan_assigment JOIN loan_servicing ON loan_assigment.talimar_loan = loan_servicing.talimar_loan WHERE loan_assignment.nfiles_status = '0' AND loan_assignment.nsecurity_type = '1' AND loan_servicing.loan_status = '2'";
		$deed_of_trustsql = "SELECT COUNT(*) as pre_count_assigmentss FROM loan_assigment JOIN loan ON loan.talimar_loan = loan_assigment.talimar_loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan_assigment.nfiles_status = '0' AND loan_assigment.nsecurity_type='1' AND loan_servicing.loan_status = '2'";
		$deed_of_trust = $this->User_model->query($deed_of_trustsql);
		$deed_of_trust = $deed_of_trust->result();
		$data['deed_of_trust'] = $deed_of_trust[0]->pre_count_assigmentss;



		$assigment_sql_7 = "SELECT COUNT(*) as pre_count_assigment FROM loan_assigment JOIN loan ON loan.talimar_loan = loan_assigment.talimar_loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan_assigment.nfiles_status = '0' AND loan_assigment.nservicer_status='1' ";

		$assigment_is_result_7 = $this->User_model->query($assigment_sql_7);
		$assigment_result_7 = $assigment_is_result_7->result();

		$data['sub_ass'] = $assigment_result_7[0]->pre_count_assigment;

		//re870_audit_report...
		$current_date = date('Y-m-d');
		$last_12_months = date('Y-m-d', strtotime('-1 year', strtotime($current_date)));

		$audit_report = $this->User_model->query("Select * from lender_contact_type where (lender_RE_date <= '" . $last_12_months . "' OR lender_RE_date = '' OR lender_RE_date IS NULL) group by contact_id");
		$audit_report = $audit_report->result();
		$count = 0;
		foreach ($audit_report as $row) {

			$contact_idd = $row->contact_id;

			$lender_active_balance = $this->User_model->query("SELECT COUNT(*) as total_re870 FROM loan_assigment as la JOIN lender_contact as lc on lc.lender_id = la.lender_name JOIN loan_servicing as ls on la.talimar_loan = ls.talimar_loan WHERE lc.contact_id = '" . $contact_idd . "' AND ls.loan_status = '2' AND lc.required_sign = '1' group by lc.contact_id");
			$lender_active_balance = $lender_active_balance->result();

			foreach ($lender_active_balance as $rows) {
				$count++;
			}

		}
		$data['re870_audit_report'] = $count;

		$content1 = $this->load->view('dashboard/ajax_ready_signture', $data, true);
		echo $content1;

	}

	public function ajaxLenderStatusProccig()
	{
		$fetch_id_data['account_status'] 	= '1';
		$fetch_investor_data 	= $this->User_model->select_where('investor',$fetch_id_data);
		$data['fetch_investor_data'] 	= $fetch_investor_data->result();

		$content1 = $this->load->view('dashboard/ajaxLenderProcessing', $data, true);
		echo $content1;
	}

	

	public function ajax_loan_service_fun() {
		error_reporting(0);
		//fetch vendor names...

		$fetch_fci_vendors = $this->User_model->query("SELECT * FROM vendors ORDER by vendor_id");
		$fetch_fci_vendors = $fetch_fci_vendors->result();
		foreach ($fetch_fci_vendors as $key => $row) {

			if (($row->vendor_name == 'FCI Lender Services')) {

				$fci_vendor_id = $row->vendor_id;
			}
			if ($row->vendor_name == 'Del Toro Loan Servicing') {

				$del_toro_vendor_id = $row->vendor_id;
			}
			if ($row->vendor_name == 'TaliMar Financial Inc.') {

				$talimar_vendor_id = $row->vendor_id;
			}
			// if(($row->vendor_name == 'FCI Lender Services') ||( $row->vendor_name == 'Del Toro Loan Servicing')){

			// 	$vendor_id[] = $row->vendor_id;
			// }
		}

		$vendor_ids = implode(',', $vendor_id);

		// //FCI Lender Services & Del Toro...
		// $both_loan_services_sql = 'SELECT COUNT(*) as count_fci , SUM(loan_amount) as loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.sub_servicing_agent IN ('.$vendor_ids.') AND loan_servicing.loan_status = "2" AND loan_servicing.boarding_status  IN(2,3)  ';
		// $both_loan_services_result = $this->User_model->query($both_loan_services_sql);
		// $both_loan_services_result = $both_loan_services_result->result();

		// $data['count_both_loan_services'] 			= $both_loan_services_result[0]->count_fci;
		// $data['both_loan_services_total_amount'] 	= $both_loan_services_result[0]->loan_amount;

		//FCI Lender Services...
		$fci_loan_sqls = 'SELECT COUNT(*) as count_fci , SUM(current_balance) as fci_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.sub_servicing_agent ="14" AND loan_servicing.loan_status = "2"';

		// $fci_loan_sqls = 'SELECT COUNT(*) as count_fci , SUM(loan_amount) as fci_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = "2" AND loan_servicing.boarding_status ="2"';

		$fci_loan_result = $this->User_model->query($fci_loan_sqls);
		$fci_loan_result = $fci_loan_result->result();

		$data['count_fci_loan'] = $fci_loan_result[0]->count_fci;
		$data['fci_loans_total_amount'] = $fci_loan_result[0]->fci_loan_amount;

		//del toro loans
		//$deltoto_loan_sql = 'SELECT COUNT(*) as count_del_toro , SUM(loan_amount) as del_toro_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.sub_servicing_agent = "'.$del_toro_vendor_id.'" AND loan_servicing.loan_status = "2" AND loan_servicing.boarding_status IN(2,3) ';

		$deltoto_loan_sql = 'SELECT COUNT(*) as count_del_toro , SUM(current_balance) as del_toro_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = "2" AND loan_servicing.sub_servicing_agent = "' . $del_toro_vendor_id . '"';

		$deltoro_loan_sql_loan_result = $this->User_model->query($deltoto_loan_sql);
		$deltoro_loan_sql_loan_result = $deltoro_loan_sql_loan_result->result();

		$data['count_del_toro_loan'] = $deltoro_loan_sql_loan_result[0]->count_del_toro;
		$data['del_toro_loans_total_amount'] = $deltoro_loan_sql_loan_result[0]->del_toro_loan_amount;

		//talimar financial  loans
		$talimar_loan_sql = 'SELECT COUNT(*) as count_del_toro , SUM(current_balance) as del_toro_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.sub_servicing_agent = "' . $talimar_vendor_id . '" AND loan_servicing.loan_status = "2"';

		$talimar_loan_sql_loan_result = $this->User_model->query($talimar_loan_sql);
		$talimar_loan_sql_loan_result = $talimar_loan_sql_loan_result->result();

		$data['count_talimar_loan'] = $talimar_loan_sql_loan_result[0]->count_del_toro;
		$data['talimar_loans_total_amount'] = $talimar_loan_sql_loan_result[0]->del_toro_loan_amount;

		// // Loan_serviced
		// $serviced_loan_sql = 'SELECT COUNT(*) as count_serviced , SUM(loan.loan_amount) as serviced_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = "2" AND loan_servicing.boarding_status = "2" AND loan_servicing.sub_servicing_agent IN (1,3)';
		// $serviced_sql_loan_result = $this->User_model->query($serviced_loan_sql);
		// $serviced_sql_loan_result = $serviced_sql_loan_result->result();

		// $data['count_serviced_loan'] 			= $serviced_sql_loan_result[0]->count_serviced;
		// $data['serviced_loans_total_amount'] 	= $serviced_sql_loan_result[0]->serviced_loan_amount;

		$content1 = $this->load->view('dashboard/ajax_loan_service', $data, true);
		echo $content1;

	}

	public function ajax_loan_late_date() {

		error_reporting(0);

		$fetch_active_loan = $this->User_model->query("SELECT *,lp.id FROM  `loan_servicing` AS ls JOIN loan AS l JOIN loan_property as lp  ON ls.`talimar_loan` = l.`talimar_loan` AND ls.talimar_loan = lp.talimar_loan WHERE  ls.closing_status > 0 AND ls.condition ='3'");

		if ($fetch_active_loan->num_rows() > 0) {
			$fetch_active_loan = $fetch_active_loan->result();

			foreach ($fetch_active_loan as $row) {

				$talimar_loan['talimar_loan'] = $row->talimar_loan;
				$loan_fetch = $this->User_model->select_where('loan', $talimar_loan);
				$loan_fetch = $loan_fetch->result();
				$borrower_id['id'] = $loan_fetch[0]->borrower;

				$loan_id = $loan_fetch[0]->id;

				$fetch_table_loan_property_insurance = $this->User_model->select_where('loan_property_insurance', $talimar_loan);

				$loan_property_insurance_result = $fetch_table_loan_property_insurance->row();
				$loan_property_insurance_result = $loan_property_insurance_result;

				$fetch_table_loan_property = $this->User_model->select_where('loan_property', array('property_home_id' => $row->id));
				$loan_property_result = $fetch_table_loan_property->result();
				$aa = 0;
				$fetch_impound_account = $this->User_model->select_where_asc('impound_account', $talimar_loan, 'position');
				if ($fetch_impound_account->num_rows() > 0) {
					$fetch_impound_account = $fetch_impound_account->result();

					$impound_amount = $fetch_impound_account[0]->amount;

					if ($fetch_impound_account[0]->items == 'Mortgage Payment') {

						$aa += ($loan_fetch[0]->intrest_rate / 100) * $loan_fetch[0]->loan_amount / 12;
					}
					if ($fetch_impound_account[0]->items == 'Property / Hazard Insurance Payment') {
						if ($fetch_impound_account[0]->impounded == 1) {
							$aa += isset($loan_property_result) ? ($loan_property_insurance_result->annual_insurance_payment / 12) : 0;
						}
					}

					if ($fetch_impound_account[0]->items == 'County Property Taxes') {
						if ($fetch_impound_account[0]->impounded == 1) {
							$aa += isset($loan_property_result) ? ($loan_property_result[0]->annual_property_taxes / 12) : 0;
						}
					}
					if ($fetch_impound_account[0]->items == 'Flood Insurance') {
						if ($fetch_impound_account[0]->impounded == 1) {
							$aa += isset($fetch_impound_account[0]->amount) ? ($fetch_impound_account[0]->amount) : 0;
						}
					}

					if ($fetch_impound_account[0]->items == 'Mortgage Insurance') {
						if ($fetch_impound_account[0]->impounded == 1) {
							$aa += isset($fetch_impound_account[0]->amount) ? ($fetch_impound_account[0]->amount) : 0;
						}
					}

				}


				$fetch_borrower_name = $this->User_model->select_where('borrower_data', $borrower_id);
				$fetch_borrower_name = $fetch_borrower_name->result();

				$borrower_name = $fetch_borrower_name[0]->b_name;
				

				$property_sql = "SELECT property_address FROM loan_property WHERE talimar_loan = '" . $row->talimar_loan . "'";

				$fetch_property_data = $this->User_model->query($property_sql);
				$fetch_property_data = $fetch_property_data->result();

				$property_address = $fetch_property_data[0]->property_address;
				$fetch_late_date[] = array(

					'loan_id' => $loan_id,
					'borrower' => $borrower_name,
					'property_address' => $property_address,
					'monthly' => $aa,

				);

			}
			$data['fetch_late_date'] = $fetch_late_date;

			$content1 = $this->load->view('dashboard/ajax_loan_late_date', $data, true);
			echo $content1;

		}

	}

	

	public function upload_by_url() {
		$fileURL = 'https://myhappyclient.com/cls/wp-content/uploads/bb-plugin/cache/Slide_3-landscape.jpg';
		//$fileURL = 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf';

		$fileName = basename($fileURL);
		$destinationPath = 'contactdocuments/'.$fileName;
		$result = $this->aws3->uploadImageByUrl($fileURL, $destinationPath);
		
	}

	/*
	Date : 07-05-2021
	*/

	public function dashboard_reports(){
		error_reporting(0);
		$data = array();
		$data['d'] = '';
		$data['a'] = 'aaaaaaaaaaaa';

		//fetch data according to user setting...
		$loginuser_dashboard = $this->loan_organization_setting();

		$loan_status_active['loan_status'] = '2';

		if ($loginuser_dashboard == 1) {

			$fetch_all_active = $this->User_model->select_where('loan_servicing', $loan_status_active);

		} else {

			$fetch_all_active = $this->User_model->query("select * from loan_servicing as ls JOIN loan as l ON ls.talimar_loan=l.talimar_loan JOIN loan_servicing_contacts as lsc ON lsc.talimar_loan=ls.talimar_loan where ls.loan_status='2' AND (l.loan_originator= '" . $this->session->userdata('t_user_id') . "' OR lsc.loan_originator= '" . $this->session->userdata('t_user_id') . "')");

		}
		$fetch_all_active = $fetch_all_active->result();

		$active_senior_balance_sql = "SELECT SUM(`supirior_original`) as sum_original, SUM(`supirior_current`) as sum_current FROM extra_details JOIN loan_servicing ON extra_details.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '2'";
		$active_senior_balance_result = $this->User_model->query($active_senior_balance_sql);
		$active_senior_balance_result = $active_senior_balance_result->result();

		$count = 0;
		$loan_amount = 0;
		$intrest_rate = 0;
		$arv = 0;
		$term_month = 0;
		$selling_price = 0;
		$future_value = 0;
		$current_balance = 0;
		foreach ($fetch_all_active as $active_loans) {
			$active_talimar_loan['talimar_loan'] = $active_loans->talimar_loan;

			// Fetch data from Loan
			$loan_data = $this->User_model->select_where('loan', $active_talimar_loan);
			$loan_data = $loan_data->result();
			foreach ($loan_data as $loan_row) {
				$loan_amount = $loan_amount + $loan_row->loan_amount;
				$current_balance += $loan_row->current_balance;
				$term_month = $term_month + $loan_row->term_month;
				$intrest_rate = $intrest_rate + $loan_row->intrest_rate;

			}

			// fetch property_data

			$property_data = $this->User_model->select_where('loan_property', $active_talimar_loan);
			$property_data = $property_data->result();
			foreach ($property_data as $property_row) {

				$arv = $arv + $property_row->arv;

				$future_value = $future_value + $property_row->underwriting_value;

			}

			$where_11['talimar_loan'] = $active_loans->talimar_loan;
			$fetch_loan_servicing = $this->User_model->select_where('loan_servicing', $where_11);
			$fetch_loan_servicing = $fetch_loan_servicing->result();
			
			if($fetch_loan_servicing){
				foreach ($fetch_loan_servicing as $row) {
					if($row->selling_price){
						$selling_price = $selling_price + $row->selling_price;	
					}
					
				}
			}

			$count++;
			$select_ecum_total = $this->User_model->query("SELECT SUM(`current_balance`) as total_ecum FROM `encumbrances` as e JOIN property_home as ph ON e.`property_home_id` = ph.id where ph.primary_property = '1' AND e.talimar_loan = '" . $active_loans->talimar_loan . "'");
			$select_ecum_total = $select_ecum_total->result();
			$ecum_total = $select_ecum_total[0]->total_ecum;
		}

		$fetch_sum_extra = '';

		$active_loan_result = array(
			"active_count" => $count,
			"total_loan_amount" => $loan_amount,
			"total_current_balance" => $current_balance,

			"selling_price" => $selling_price,
			"total_intrest_rate" => number_format(($intrest_rate / $count),3),
			"total_arv" => $arv,
			"total_future_value" => $future_value,
			"total_term_month" => ($term_month) / $count,
			"senior_current" => $active_senior_balance_result[0]->sum_current,
			"senior_original" => $active_senior_balance_result[0]->sum_original,

		);

		$data['active_loan_result'] = $active_loan_result;

		//default loan...
		$laon_default_forculare = $this->User_model->query("SELECT * FROM loan_servicing_default INNER JOIN add_foreclosure_request ON loan_servicing_default.talimar_loan=add_foreclosure_request.talimar_loan INNER JOIN loan_servicing ON loan_servicing.talimar_loan=add_foreclosure_request.talimar_loan JOIN loan as l ON l.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status='2' AND loan_servicing.condition='1' group by loan_servicing_default.talimar_loan");

		$laon_default_forculares = $laon_default_forculare->result();
		$total_default = 0;
		$total_amount = 0;
		foreach ($laon_default_forculares as $value) {
			if($value->foreclosuer_status == '1'){
				$total_default++;
				$total_amount += $value->loan_amount;
			}
		}

		$data['fetch_total_default'] = $total_default;
		$data['fetch_total_amount'] = $total_amount;

		//  Fetch All pipeliane loan data
		$loan_status_pipeline['loan_status'] = '1';

		if ($loginuser_dashboard == 1) {
			$fetch_all_pipeline = $this->User_model->query("select * from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status IN('1','6') GROUP BY ls.talimar_loan ORDER BY ls.closing_status DESC, l.loan_funding_date ASC");
		} else {

			$fetch_all_pipeline = $this->User_model->query("select *, l.loan_originator from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan JOIN loan_servicing_contacts as lsc ON lsc.talimar_loan=ls.talimar_loan where ls.loan_status IN('1','6') AND (l.loan_originator= '" . $this->session->userdata('t_user_id') . "' OR lsc.loan_originator= '" . $this->session->userdata('t_user_id') . "') GROUP BY ls.talimar_loan ORDER BY ls.closing_status DESC, l.loan_funding_date ASC");
		}



		$fetch_all_pipeline = $fetch_all_pipeline->result();



		$pipeline_senior_balance_sql = "SELECT SUM(`supirior_original`) as sum_original, SUM(`supirior_current`) as sum_current FROM extra_details JOIN loan_servicing ON extra_details.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '1'";
		$pipeline_senior_balance_result = $this->User_model->query($pipeline_senior_balance_sql);
		$pipeline_senior_balance_result = $pipeline_senior_balance_result->result();

		$count = 0;
		$loan_amount = 0;
		$intrest_rate = 0;
		$arv = 0;
		$term_month = 0;
		$selling_price = 0;
		$underwriting_value = 0;
		$ltvvount = 0;
		$calculate_ltv_av = array();

		foreach ($fetch_all_pipeline as $pkey => $pipeline_loans) {
			$pipeline_talimar_loan['talimar_loan'] = $pipeline_loans->talimar_loan;


			$all_pipeline_loan[] = $pipeline_loans->talimar_loan;
			// Fetch data from Loan
			$loan_data = $this->User_model->query("SELECT * FROM loan WHERE talimar_loan = '".$pipeline_loans->talimar_loan."'");
			$loan_data = $loan_data->result();



			foreach ($loan_data as $loan_row) {
				$loan_amount = $loan_amount + $loan_row->loan_amount;
				$term_month = $term_month + $loan_row->term_month;
				$lender_fee = $loan_row->lender_fee;
				$intrest_rate = $intrest_rate + $loan_row->intrest_rate;

				$calculate_ltv_av['loan_amount'][$pkey] = $loan_row->loan_amount;

			}

			// fetch property_data
			$property_data = $this->User_model->select_where('loan_property', $pipeline_talimar_loan);
			$property_data = $property_data->result();

			foreach ($property_data as $property_row) {
				$arv = $arv + $property_row->arv;
				$underwriting_value = $underwriting_value + $property_row->underwriting_value;

				$calculate_ltv_av['underwriting_value'][$pkey] = $property_row->underwriting_value;
				$ltvvount++;

			}
			$where_12['talimar_loan'] = $pipeline_loans->talimar_loan;
			$fetch_loan_servicing = $this->User_model->select_where('loan_servicing', $where_12);
			$fetch_loan_servicing = $fetch_loan_servicing->result();
			foreach ($fetch_loan_servicing as $row) {
				if($row->selling_price){
					$selling_price = $selling_price + $row->selling_price;	
				}				
			}
			$count++;

		}

		// calculate LTV Avg

		$ltv_key = 0;
		$total_ltv = 0;

		foreach ($calculate_ltv_av['loan_amount'] as $ltv_val) {
			$loan_amount1 = $calculate_ltv_av['loan_amount'][$ltv_key];
			$underwriting_value = $calculate_ltv_av['underwriting_value'][$ltv_key];
			$ltv = ($loan_amount1 / $underwriting_value) * 100;

			$total_ltv = $total_ltv + $ltv;

			$ltv_key++;

		}

		$ltv_av = $total_ltv / $ltvvount;

		$pipeline_loan_result = array(
			"pipeline_count" => $count,
			"total_loan_amount" => $loan_amount,
			"lender_fee" => $lender_fee,
			"selling_price" => $selling_price,
			"total_intrest_rate" => number_format($intrest_rate,3),
			'ltv_av' => $ltv_av,
			"total_arv" => $arv,
			"total_term_month" => $term_month / $count,
			"senior_current" => $pipeline_senior_balance_result[0]->sum_current,
			"senior_original" => $pipeline_senior_balance_result[0]->sum_original,
		);

		$data['pipeline_loan_result'] = $pipeline_loan_result;

		$pipeline_loan_result = '';
		//  End of Fetch all pipeline loan data

		// fetch all pipeline loan...
		$total_pipeline_lender_fees_amount = 0;
		$uv_value = 0;
		$select_ecum = 0;
		foreach ($all_pipeline_loan as $row) {

			$p_talimar_loan['talimar_loan'] = $row;

			$fetch_loan_data = $fetch_loan_data = $this->User_model->query("SELECT * FROM loan WHERE talimar_loan = '".$row."'");

			$fetch_loan_data = $fetch_loan_data->result();


			$l_talimar_loan = $fetch_loan_data[0]->talimar_loan;
			$fetch_talimar['talimar_loan'] = $fetch_loan_data[0]->talimar_loan;
			$fetch_borrower['id'] = $fetch_loan_data[0]->borrower;
			$l_loan_amount = $fetch_loan_data[0]->loan_amount;

			$l_loan_type = $fetch_loan_data[0]->loan_type;
			$l_intrest_rate = $fetch_loan_data[0]->intrest_rate;
			$l_funding_date = $fetch_loan_data[0]->loan_funding_date;
			$l_lender_fee = $fetch_loan_data[0]->lender_fee;
			$month = $fetch_loan_data[0]->term_month;
			$position = $fetch_loan_data[0]->position;

			$pipeline_lender_fees_amount = $l_loan_amount * ($l_lender_fee / 100);
			$total_pipeline_lender_fees_amount = $total_pipeline_lender_fees_amount + $pipeline_lender_fees_amount;

			//fetch underwriting_value and property_address...
			$property_data = "SELECT sum(`underwriting_value`) as uv_value,property_address FROM `loan_property` WHERE `talimar_loan`='" . $l_talimar_loan . "'";

			$fetch_property_data = $this->User_model->query($property_data);
			$fetch_property_data = $fetch_property_data->result();
			$property_address = $fetch_property_data[0]->property_address;
			$uv_value = $fetch_property_data[0]->uv_value;
			$uv_value = $fetch_property_data[0]->uv_value ? $fetch_property_data[0]->uv_value : 0;

			//fetch borrower name...
			$fetch_borrower_data = $this->User_model->select_where('borrower_data', $fetch_borrower);
			if($fetch_borrower_data->num_rows() > 0){
				$fetch_borrower_data = $fetch_borrower_data->result();

				$borrower_name = $fetch_borrower_data[0]->b_name;
				$borrower_id = $fetch_borrower_data[0]->id;
			}

			//fetch closing_status...
			$fetch_loan_servicing = $this->User_model->select_where('loan_servicing', $p_talimar_loan);
			$fetch_loan_servicing = $fetch_loan_servicing->result();
			$closing_status = $fetch_loan_servicing[0]->closing_status;
			$term_sheet = $fetch_loan_servicing[0]->term_sheet;


			//fetch property home id...
			$select_property_home = $this->User_model->select_where('loan_property', $p_talimar_loan);
			$select_property_home = $select_property_home->result();
			$property_home_id = $select_property_home[0]->property_home_id;
			$lssss_talimar_loan['talimar_loan']=$l_talimar_loan;
		

		$fetch_property_homez = $this->User_model->select_where_asc('property_home', $lssss_talimar_loan, 'primary_property');

		$property_homesssss = $fetch_property_homez->result();
			
			
		$property_home_idsss=array();
			foreach($property_homesssss as $key=>$vv){

				 $property_home_idsss[]=$vv->id;

			}
	
			$implode=implode("','",$property_home_idsss);

					

			 $sql = "SELECT  SUM(current_balance) as total FROM encumbrances WHERE property_home_id IN ('" . $implode . "') AND talimar_loan = '" . $l_talimar_loan . "' AND will_it_remain = 0 ";

			$select_ecums = $this->User_model->query($sql);
			$select_ecums = $select_ecums->result();
			$select_ecum = $select_ecums[0]->total ? $select_ecums[0]->total : 0;

			$sum_investment_assigment_sql = "SELECT SUM(investment) as total_investment FROM loan_assigment WHERE talimar_loan = '" . $l_talimar_loan . "' ";

			$sum_investment_assigment_result = $this->User_model->query($sum_investment_assigment_sql);
			$sum_investment_assigment_result = $sum_investment_assigment_result->result();
			$pileline_available = $sum_investment_assigment_result[0]->total_investment;

			$all_customize_pipeline_data[$closing_status][] = array(
				"loan_id" => $fetch_loan_data[0]->id,
				"talimar_loan" => $l_talimar_loan,
				"loan_amount" => $l_loan_amount,
				"l_loan_type" => $l_loan_type,
				"intrest_rate" => number_format($l_intrest_rate,3),
				"funding_date" => $l_funding_date,
				"lender_fee" => $l_lender_fee,
				"term_month" => $month,
				"position" => $position,
				"property_address" => $property_address,
				"uv_value" => $uv_value,
				"select_ecum" => $select_ecum,
				"borrower_name" => $borrower_name,
				"closing_status" => $closing_status,
				"avilable" => $l_loan_amount - $pileline_available,
				"borrower_id" => $borrower_id,
				"term_sheet" => $term_sheet,
			);

			if ($closing_status != 7) {
				$all_pipeline_data[] = array(
					"loan_id" => $fetch_loan_data[0]->id,
					"talimar_loan" => $l_talimar_loan,
					"loan_amount" => $l_loan_amount,
					"l_loan_type" => $l_loan_type,
					"intrest_rate" => number_format($l_intrest_rate,3),
					"funding_date" => $l_funding_date,
					"lender_fee" => $l_lender_fee,
					"term_month" => $month,
					"position" => $position,
					"property_address" => $property_address,
					"uv_value" => $uv_value,
					"select_ecum" => $select_ecum,
					"borrower_name" => $borrower_name,
					"closing_status" => $closing_status,
					"avilable" => $l_loan_amount - $pileline_available,
					"borrower_id" => $borrower_id,
					"term_sheet" => $term_sheet,
				);
			}
			if ($closing_status == 7) {
				$all_pipeline_data_closing_status7[] = array(
					"loan_id" => $fetch_loan_data[0]->id,
					"talimar_loan" => $l_talimar_loan,
					"loan_amount" => $l_loan_amount,
					"l_loan_type" => $l_loan_type,
					"intrest_rate" => number_format($l_intrest_rate,3),
					"funding_date" => $l_funding_date,
					"lender_fee" => $l_lender_fee,
					"term_month" => $month,
					"position" => $position,
					"property_address" => $property_address,
					"uv_value" => $uv_value,
					"select_ecum" => $select_ecum,
					"borrower_name" => $borrower_name,
					"closing_status" => $closing_status,
					"avilable" => $l_loan_amount - $pileline_available,
					"borrower_id" => $borrower_id,
					"term_sheet" => $term_sheet,
				);
			}

		}

		if (isset($all_pipeline_data_closing_status7)) {
			foreach ($all_pipeline_data_closing_status7 as $key => $row) {

				$all_pipeline_data[] = array(
					"loan_id" => $row['loan_id'],
					"talimar_loan" => $row['talimar_loan'],
					"loan_amount" => $row['loan_amount'],
					"l_loan_type" => $row['l_loan_type'],
					"intrest_rate" => number_format($row['intrest_rate'],3),
					"funding_date" => $row['funding_date'],
					"lender_fee" => $row['lender_fee'],
					"term_month" => $row['term_month'],
					"position" => $row['position'],
					"property_address" => $row['property_address'],
					"uv_value" => $row['uv_value'],
					"select_ecum" => $row['select_ecum'],
					"borrower_name" => $row['borrower_name'],
					"closing_status" => $row['closing_status'],
					"avilable" => $row['avilable'],
					"borrower_id" => $row['borrower_id'],
					"term_sheet" => $row['term_sheet'],
				);
			}
		}

		if (isset($all_customize_pipeline_data)) {
			$array = array(8, 6, 5, 4, 3, 10, 9, 2, 1, 7, 11, 0);
			foreach ($array as $number) {
				if (isset($all_customize_pipeline_data[$number])) {

					foreach ($all_customize_pipeline_data[$number] as $row) {

						$all_pipeline_data_new[] = array(
							"loan_id" => $row['loan_id'],
							"talimar_loan" => $row['talimar_loan'],
							"loan_amount" => $row['loan_amount'],
							"l_loan_type" => $row['l_loan_type'],
							"intrest_rate" => number_format($row['intrest_rate'],3),
							"funding_date" => $row['funding_date'],
							"lender_fee" => $row['lender_fee'],
							"term_month" => $row['term_month'],
							"position" => $row['position'],
							"property_address" => $row['property_address'],
							"uv_value" => $row['uv_value'],
							"select_ecum" => $row['select_ecum'],
							"borrower_name" => $row['borrower_name'],
							"closing_status" => $row['closing_status'],
							"avilable" => $row['avilable'],
							"borrower_id" => $row['borrower_id'],
							"term_sheet" => $row['term_sheet'],
						);
					}
				}
			}
		}


		$data['all_pipeline_data'] = $all_pipeline_data_new;
		$data['total_pipeline_lender_fees_amount'] = $total_pipeline_lender_fees_amount;

		/*
			Description : This function use for dashboar data get - Update loan status condition 
			Author      : Bitcot
			Created     : 
			Modified    : 03-05-2021
		*/ 
		//loan holding schedule...
		if ($loginuser_dashboard == 1) {
			
			$holding = $this->User_model->query("
					SELECT * FROM loan_servicing AS ls 
					JOIN input_datas as ida ON ls.talimar_loan = ida.talimar_loan 
					WHERE ls.loan_status IN ('2') AND ida.input_name='checklist_complete' AND ida.page='loan_servicing_checklist' AND ida.value !='1' ");

		} else {
			
			$holding = $this->User_model->query("
				SELECT * FROM loan_servicing AS ls 
				JOIN input_datas as ida ON ls.talimar_loan = ida.talimar_loan 
				WHERE ls.loan_status IN ('2') AND ida.input_name='checklist_complete' AND ida.page='loan_servicing_checklist' AND ida.value !='1' AND ls.t_user_id = '" . $this->session->userdata('t_user_id') . "'");

		}

		if ($holding->num_rows() > 0) {

			$input_data = $holding->result();


			foreach ($input_data as $row) {

				$talimar_loan = $row->talimar_loan;

				$loan_recording = $this->User_model->query("SELECT `recorded_date` FROM `loan_recording_information` WHERE `talimar_loan` = '" . $talimar_loan . "'");
				if($loan_recording->num_rows() > 0){
					$date = $loan_recording->result();
					$rdate = $date[0]->recorded_date;

					if($rdate != ''){

						$recordingdate = date('m-d-Y', strtotime($rdate));
					}else{
						$recordingdate = 'Error';
					}
				}else{

					$recordingdate = 'Error';
				}


				$address = $this->User_model->query("select property_address from loan_property where talimar_loan = '" . $talimar_loan . "'");
				$address = $address->result();
				$p_address = $address[0]->property_address;

				$loan_status = $this->User_model->query("SELECT loan_status FROM loan_servicing WHERE talimar_loan = '" . $talimar_loan . "' ");
				$loan_status = $loan_status->result();
				$status = $loan_status[0]->loan_status;

				$current_date = date('Y-m-d');
				$recordingData = $this->User_model->query("SELECT recorded_date FROM `loan_recording_information` WHERE talimar_loan = '".$talimar_loan."'");
				if($recordingData->num_rows() > 0){

					$recordingData = $recordingData->result();
					$recorded_date = $recordingData[0]->recorded_date;

					$diff = '-'.$this->dateDiff($recorded_date, $current_date);
				}else{
					$diff = '';
				}

				$loan = $this->User_model->query("select * from loan where talimar_loan = '" . $talimar_loan . "'");
				$loan = $loan->result();
				$loan_id = $loan[0]->id;
				$amount = $loan[0]->loan_amount;
				$servicer = $loan[0]->fci;
				$closing_date = $loan[0]->loan_funding_date;
				

				$sql = "SELECT * FROM `loan_servicing_checklist` WHERE `talimar_loan`='" . $talimar_loan . "' AND type='post wire' AND hud='1008'";
				$servicing_checklist = $this->User_model->query($sql);

				if ($servicing_checklist->num_rows() > 0) {
					$servicing_checklist = $servicing_checklist->result();

				}

				$holding_again[] = array(

					"talimar_loan" => $talimar_loan,
					"loan_id" => $loan_id,
					"p_address" => $p_address,
					"status" => $status,
					"loan_amount" => $amount,
					"servicer" => $servicer,
					"closing_date" => $closing_date,
					"diff" => $diff,
					"check" => $servicing_checklist[0]->checklist,
					"recordingdate" => $recordingdate
				);
			}
			array_multisort(array_column($holding_again, 'diff'), SORT_ASC, $holding_again);
			$data['holding_loan'] = $holding_again;
		} else {
			$data['holding_loan'] = '';
		}

		//$disb_verified = $this->User_model->select_where('loan_servicing',$where_disb);
		$disb_verified = $this->User_model->query("select * from loan_servicing where servicing_disb='2' AND loan_status='2'");

		$disb_verified = $disb_verified->result();
		$count_disb = count($disb_verified);
		$data['all_count_disb'] = $count_disb;

		//FETCH MARKETING COMPLETE from loan servicing...

		$sql = "SELECT COUNT(*) as total FROM `loan_servicing` WHERE `loan_status`='2' AND `marketing_complete` !='1'";
		$fetch_marketing = $this->User_model->query($sql);
		$fetch_marketing = $fetch_marketing->result();
		$fetch_marketing = $fetch_marketing[0]->total;
		$data['fetch_marketing'] = $fetch_marketing;

		//  Monthly income servicing loan

		$join_sql = "SELECT *, loan.id as loan_id FROM loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan_servicing.loan_status = '2' ";
		$fetch_all_loan = $this->User_model->query($join_sql);
		$fetch_all_loan = $fetch_all_loan->result();
		foreach ($fetch_all_loan as $row) {
			$talimar['talimar_loan'] = $row->talimar_loan;
			$talimar_loan = $row->talimar_loan;
			$loan_id = $row->loan_id;
			$fci = $row->fci;
			$loan_amount = $row->loan_amount;
			$current_balancee = $row->current_balance;

			//  fetch data from loan servicing

			$fetch_servicing = $this->User_model->select_where('loan_servicing', $talimar);
			$fetch_servicing = $fetch_servicing->result();
			
			$fetch_loan_assigment = $this->User_model->select_where('loan_assigment', $talimar);
		    $fetch_loan_assigment = $fetch_loan_assigment->result();

			$servicing_fee = $fetch_servicing[0]->servicing_fee;
			$who_pay_servicing = $fetch_servicing[0]->who_pay_servicing;
			$sub_servicing_agent = $fetch_servicing[0]->sub_servicing_agent;
			$minimum_servicing_fee = $fetch_servicing[0]->minimum_servicing_fee;
			$broker_servicing_fees = $fetch_servicing[0]->broker_servicing_fees;
			$servicing_lender_rate = number_format($fetch_servicing[0]->servicing_lender_rate,3);
			$sql = "SELECT count(*) as countrow from loan_assigment WHERE talimar_loan = '" . $talimar_loan . "' ";
			$count_lender = $this->User_model->query($sql);
			if ($count_lender->num_rows() > 0) {
				$count_lender = $count_lender->result();
				$count_lender = $count_lender[0]->countrow;
			} else {
				$count_lender = 0;
			}

			$all_monthly_income[] = array(
				'talimar_loan' => $talimar_loan,
				'loan_id' => $loan_id,
				'fci' => $fci,
				'loan_amount' => $loan_amount,
				'current_balancee' => $current_balancee,
				'sub_servicing_agent' => $sub_servicing_agent,
				'who_pay_servicing' => $who_pay_servicing,
				'servicing_fee' => $servicing_fee,
				'minimum_servicing_fee' => $minimum_servicing_fee,
				'broker_servicing_fees' => $broker_servicing_fees,
				'count_lender' => $count_lender,
				'interest_rate' => number_format($row->intrest_rate,3),
				'servicing_lender_rate' => number_format($servicing_lender_rate,3),
				'invester_yield_percent' => $fetch_loan_assigment[0]->invester_yield_percent,

			);
		}
		
			
		$total_servicing_income = 0;
		$total_servicing_cost = 0;
		foreach ($all_monthly_income as $key => $row) {
			
			$payment_pro_fees = $all_monthly_income[$key]['loan_amount'];

			$pro_fees = '0';
			if ($payment_pro_fees > 0 && $payment_pro_fees <= 400000) {
				$pro_fees = '0';
			} elseif ($payment_pro_fees >= 400001 && $payment_pro_fees <= 500000) {
				$pro_fees = '25.00';
			} elseif ($payment_pro_fees >= 500001 && $payment_pro_fees <= 600000) {
				$pro_fees = '35.00';
			} elseif ($payment_pro_fees >= 600001 && $payment_pro_fees <= 700000) {
				$pro_fees = '45.00';
			} elseif ($payment_pro_fees >= 700001 && $payment_pro_fees <= 800000) {
				$pro_fees = '55.00';
			} elseif ($payment_pro_fees >= 800001 && $payment_pro_fees <= 900000) {
				$pro_fees = '65.00';
			} elseif ($payment_pro_fees >= 900001 && $payment_pro_fees <= 1000000) {
				$pro_fees = '75.00';
			} elseif ($payment_pro_fees >= 1000001 && $payment_pro_fees <= 2000000) {

				$pro_fees = '95.00';
			} elseif ($payment_pro_fees >= 2000001 && $payment_pro_fees <= 3000000) {

				$pro_fees = '115.00';
			} elseif ($payment_pro_fees >= 3000001 && $payment_pro_fees <= 4000000) {

				$pro_fees = '135.00';
			} elseif ($payment_pro_fees >= 4000001 && $payment_pro_fees <= 5000000) {

				$pro_fees = '155.00';
			} elseif ($payment_pro_fees >= 5000001 && $payment_pro_fees <= 6000000) {

				$pro_fees = '175.00';
			} elseif ($payment_pro_fees >= 6000001 && $payment_pro_fees <= 7000000) {

				$pro_fees = '195.00';
			} elseif ($payment_pro_fees >= 7000001 && $payment_pro_fees <= 8000000) {

				$pro_fees = '215.00';
			} elseif ($payment_pro_fees >= 8000001 && $payment_pro_fees <= 9000000) {

				$pro_fees = '235.00';
			} elseif ($payment_pro_fees >= 9000001 && $payment_pro_fees <= 10000000) {

				$pro_fees = '255.00';
			} elseif ($payment_pro_fees >= 10000001 && $payment_pro_fees <= 11000000) {

				$pro_fees = '275.00';
			} elseif ($payment_pro_fees >= 11000001 && $payment_pro_fees <= 12000000) {

				$pro_fees = '295.00';
				
			}else {

				$pro_fees = '0';
			}
			
			
			$loan_amount = $all_monthly_income[$key]['loan_amount'];
			$servicing_fee = $all_monthly_income[$key]['servicing_fee'];
			//$minimum_servicing_fee = $all_monthly_income[$key]['minimum_servicing_fee'];
			//$servicing_cost = $all_monthly_income[$key]['count_lender'] * $minimum_servicing_fee;
			
			if ($all_monthly_income[$key]['servicing_lender_rate'] == 'NaN' || $all_monthly_income[$key]['servicing_lender_rate'] == '') {

				$lender_ratessss = $all_monthly_income[$key]['invester_yield_percent'];
			} else {

				$lender_ratessss = $all_monthly_income[$key]['servicing_lender_rate'];
			}
			
			$nnew_val = $all_monthly_income[$key]['interest_rate'] - $lender_ratessss;
			$nnew_valss = $nnew_val / 100;

			$gross_servicing = ($all_monthly_income[$key]['current_balancee'] * $nnew_valss) / 12;

			$minimum_servicing_fee = $all_monthly_income[$key]['minimum_servicing_fee'];

			// $servicing_income=$gross_servicing - $servicing_cost;
			$lender_servicing_fee = $all_monthly_income[$key]['count_lender'] * $all_monthly_income[$key]['minimum_servicing_fee'];

			if ($lender_servicing_fee > $pro_fees) {

				$pro_fees = 0;

			} else {

				$lender_servicing_fee = 0;

			}
			
			$current_balance = $all_monthly_income[$key]['current_balancee'];

			$servicing_cost = $lender_servicing_fee + $pro_fees + $all_monthly_income[$key]['broker_servicing_fees'];
			$servicing_income = $gross_servicing - $servicing_cost;

			if ($all_monthly_income[$key]['who_pay_servicing'] == 2) {

				$gross_servicing = $gross_servicing;
				$servicing_cost = 0;
				$servicing_income = $servicing_income;
				$lender_servicing_fee = 0;
				$all_monthly_income[$key]['servicing_fee'] = $servicing_fee;
				$all_monthly_income[$key]['minimum_servicing_fee'] = 0;
			}
			
			$all_incomee[] = array(
			
								'gross_servicingg' => $gross_servicing,
								'current_balancee' => $current_balance,
							 );
			
			
			
			
			//extra...
			$gross_servicingxx = ($loan_amount * ($servicing_fee / 100)) / 12;
			$servicing_income = $gross_servicingxx - $servicing_cost;
		}
			
		$total_gross_servicing = 0;
		$total_amount = 0;
		foreach ($all_incomee as $key => $all_income) {
			
			$total_gross_servicing = $total_gross_servicing + $all_income['gross_servicingg'];
			$total_amount = $total_amount + $all_income['current_balancee'];
		}
		//echo $total_gross_servicing;
		//echo $total_amount;
		$data['spred_val_avg'] = number_format((($total_gross_servicing*12)/$total_amount)*100,2);

		$spread_sqll = "SELECT SUM(loan_servicing.servicing_fee) as sum_servicingg, count(*) as count_loanss FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '2' ";
		$fetch_spread_dataa = $this->User_model->query($spread_sqll);
		if ($fetch_spread_dataa->num_rows() > 0) {
			$fetch_spread_dataa = $fetch_spread_dataa->result();
			$data['fetch_spread_dataa'] = array(
				'count_loanss' => $fetch_spread_dataa[0]->count_loanss,
				'sum_servicingg' => $fetch_spread_dataa[0]->sum_servicingg,
			);
		}


		$current_date = date('d-m-Y');
		$maturity_date_30 = strtotime($current_date . ' +30 days');
		$maturity_date_30 = date("Y-m-d", $maturity_date_30);

		$fetch_property_loan = $this->User_model->query("SELECT l.talimar_loan, l.id, l.borrower, lpi.policy_number, lpi.insurance_expiration, lpi.contact_name, lpi.request_updated_insurance, lpi.insurance_type, ls.loan_status, ph.primary_property FROM loan as l JOIN loan_property_insurance as lpi ON l.talimar_loan = lpi.talimar_loan JOIN loan_servicing as ls ON ls.talimar_loan = l.talimar_loan JOIN property_home as ph ON ph.talimar_loan = l.talimar_loan WHERE ls.loan_status = 2 AND ph.primary_property = '1' AND lpi.insurance_type = '1' AND lpi.required IN ('1','0') GROUP BY l.talimar_loan");

		$fetch_property_loan = $fetch_property_loan->result_array();
		

		$data['fetch_property_loan'] = $fetch_property_loan;

		//re870_audit_report...
		$current_date = date('Y-m-d');
		$last_12_months = date('Y-m-d', strtotime('-1 year', strtotime($current_date)));

		$audit_report = $this->User_model->query("Select * from lender_contact_type where (lender_RE_date <= '" . $last_12_months . "' OR lender_RE_date = '' OR lender_RE_date IS NULL) group by contact_id");
		$audit_report = $audit_report->result();
		$count = 0;
		foreach ($audit_report as $row) {

			$contact_idd = $row->contact_id;

			$lender_active_balance = $this->User_model->query("SELECT COUNT(*) as total_re870 FROM loan_assigment as la JOIN lender_contact as lc on lc.lender_id = la.lender_name JOIN loan_servicing as ls on la.talimar_loan = ls.talimar_loan WHERE lc.contact_id = '" . $contact_idd . "' AND ls.loan_status = '2' AND lc.required_sign = '1' group by lc.contact_id");
			$lender_active_balance = $lender_active_balance->result();

			foreach ($lender_active_balance as $rows) {
				$count++;
			}

		}
		$data['re870_audit_report'] = $count;

		//Upcoming Maturity Schedule...
		//error_reporting(0);
		$current_date = date('d-m-Y');
		$maturity_date_45 = strtotime($current_date . ' +45 days');
		$maturity_date_45 = date("Y-m-d", $maturity_date_45);
		$current_date1 = date('Y-m-d');

		$upcoming = "SELECT *,Count(*) as total_upcoming FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE l.maturity_date <='" . $maturity_date_45 . "' AND ls.loan_status = '2' order by l.maturity_date";
		$fetch_upcoming = $this->User_model->query($upcoming);
		$fetch_upcoming = $fetch_upcoming->result();
		$fetch_upcoming_count = $fetch_upcoming[0]->total_upcoming;
		$data['fetch_upcoming_count'] = $fetch_upcoming_count;

		//Deliquent_property_taxes
		$sel = $this->User_model->query("SELECT COUNT(*) as total_tax FROM `loan_property_taxes` as lpt JOIN loan_servicing as ls ON lpt.talimar_loan = ls.talimar_loan WHERE lpt.`post_delinquent` = '1' AND ls.loan_status = '2'");
		$property_tax = $sel->result();
		$property_tax = $property_tax[0]->total_tax;
		$data['tax'] = $property_tax;

		//loan_assigment
		$assigment_in_process_sql = "SELECT COUNT(*) as count_assigment FROM loan_assigment JOIN loan ON loan.talimar_loan = loan_assigment.talimar_loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE  loan_servicing.loan_status IN('1','2') AND loan_assigment.secured_dot != '1' AND loan_assigment.file_close != '1' ";

		$assigment_in_process_result = $this->User_model->query($assigment_in_process_sql);
		$assigment_in_process_result = $assigment_in_process_result->result();

		$data['count_assigment_in_process'] = $assigment_in_process_result[0]->count_assigment;
		//$data['investment_assigment_in_process'] = $assigment_in_process_result[0]->investment_assigment_in_process;

		// ----------------------- Outstatnding draws table  ----------------------
		$data['outstanding_draws_count'] = 0;

		// fetch Upcoming payoff schedule amount and count
		$payoff_schdule = "SELECT COUNT(*) as count_payoff, SUM(l.loan_amount) as sum_loan_amount  FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.demand_requested = '1' AND ls.loan_status = '2'";
		$fetch_payoff_data = $this->User_model->query($payoff_schdule);
		$fetch_payoff_data = $fetch_payoff_data->row();

		$data['payoff_report_amount'] = $fetch_payoff_data->sum_loan_amount ? $fetch_payoff_data->sum_loan_amount : 0;
		$data['payoff_report_count_payoff'] = $fetch_payoff_data->count_payoff ? $fetch_payoff_data->count_payoff : 0;

		$join_sql = "SELECT *,l.id as loan_id FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan WHERE ls.`closing_status` IN('2','3','4','5','6','8') AND ls.loan_status IN('1','2')";

		$fetch_all_loans = $this->User_model->query($join_sql);
		$fetch_all_loans = $fetch_all_loans->result();

		//echo $count = count($fetch_all_loans);

		foreach ($fetch_all_loans as $row) {

			$talimar_rows['talimar_loan'] = $row->talimar_loan;

			//fetch lender details...
			$fetch_lender = $this->User_model->query("SELECT * FROM loan_servicing as ls JOIN loan_assigment as la JOIN investor as i on ls.talimar_loan = la.talimar_loan AND la.lender_name = i.id WHERE ls.talimar_loan = '" . $row->talimar_loan . "' ");
			$results = $fetch_lender->row();

			// fetch assigment data sum of total investment
			$sql = "SELECT SUM(investment) as investment_total FROM loan_assigment WHERE talimar_loan = '" . $row->talimar_loan . "' ";
			$assigment_total_investment = $this->User_model->query($sql);
			$assigment_total_investment = $assigment_total_investment->result();
			$assigment_total_investment = $assigment_total_investment[0]->investment_total;

			// Count lender from assigment
			$sql = "SELECT COUNT(*) as count_lender FROM loan_assigment WHERE talimar_loan = '" . $row->talimar_loan . "' ";
			$count_assigment_lender = $this->User_model->query($sql);
			$count_assigment_lender = $count_assigment_lender->result();
			$count_assigment_lender = $count_assigment_lender[0]->count_lender;

			// fetch property_data

			$fetch_property_data = $this->User_model->query("select property_address from loan_property where talimar_loan = '" . $row->talimar_loan . "'");
			$fetch_property_data = $fetch_property_data->result();

			$avilable_deed_trust = $row->loan_amount - $assigment_total_investment;

			//echo $avilable_deed_trust.'<br>';
			//if($avilable_deed_trust > 0 || $row->loan_status == 1)
			if ($avilable_deed_trust > 0 && ($row->closing_status == '2' || $row->closing_status == '9' || $row->closing_status == '3' || $row->closing_status == '4' || $row->closing_status == '5' || $row->closing_status == '8' || $row->closing_status == '6')) {

				$trust_deed[] = array(
					'talimar_loan' => $row->talimar_loan,
					'loan_id' => $row->loan_id,
					'loan_amount' => $row->loan_amount,
					'intrest_rate' => number_format($row->intrest_rate,3),
					'market_trust_deed' => $row->market_trust_deed,
					'market_trust_deed' => $row->market_trust_deed,
					'trust_deed_posted_on_website' => $row->trust_deed_posted_on_website,
					'total_investment' => $assigment_total_investment,

					'comming_soon' => $row->comming_soon,
					'property_address' => $fetch_property_data[0]->property_address,
					'avilable_deed_trust' => $avilable_deed_trust,

				);
			}

		}

		$data['loginuser_dashboard'] = $this->loan_organization_setting();
		$data['trust_deed'] = $trust_deed;

		return $data;

		
	}

	public function dashboard(){
		$data = array();
		$dataResult = $this->dashboard_reports();
		$data['content'] = $this->load->view('dashboard_view', $dataResult, true);
		$this->load->view('template_files/template', $data);
	}

	public function action_item()
	{
		
		error_reporting(0);


		$data['loginuser_dashboard'] = $this->loan_organization_setting();
		$data['trust_deed'] = $trust_deed;


		$data['content'] = $this->load->view('dashboard/pages/action_item', $data, true);
		$this->load->view('template_files/template', $data);
	}
	
	public function loan_servicing(){
		$loginuser_dashboard = $this->loan_organization_setting();

		$data = array();
		$dataResult = array();
		if ($loginuser_dashboard == 1) {
			
			$holding = $this->User_model->query("
					SELECT * FROM loan_servicing AS ls 
					JOIN input_datas as ida ON ls.talimar_loan = ida.talimar_loan 
					WHERE ls.loan_status IN ('2') AND ida.input_name='checklist_complete' AND ida.page='loan_servicing_checklist' AND ida.value !='1' ");

		} else {
			
			$holding = $this->User_model->query("
				SELECT * FROM loan_servicing AS ls 
				JOIN input_datas as ida ON ls.talimar_loan = ida.talimar_loan 
				WHERE ls.loan_status IN ('2') AND ida.input_name='checklist_complete' AND ida.page='loan_servicing_checklist' AND ida.value !='1' AND ls.t_user_id = '" . $this->session->userdata('t_user_id') . "'");

		}

		if ($holding->num_rows() > 0) {

			$input_data = $holding->result();


			foreach ($input_data as $row) {

				$talimar_loan = $row->talimar_loan;

				$loan_recording = $this->User_model->query("SELECT `recorded_date` FROM `loan_recording_information` WHERE `talimar_loan` = '" . $talimar_loan . "'");
				if($loan_recording->num_rows() > 0){
					$date = $loan_recording->result();
					$rdate = $date[0]->recorded_date;

					if($rdate != ''){

						$recordingdate = date('m-d-Y', strtotime($rdate));
					}else{
						$recordingdate = 'Error';
					}
				}else{

					$recordingdate = 'Error';
				}


				$address = $this->User_model->query("select property_address from loan_property where talimar_loan = '" . $talimar_loan . "'");
				$address = $address->result();
				$p_address = $address[0]->property_address;

				$loan_status = $this->User_model->query("SELECT loan_status FROM loan_servicing WHERE talimar_loan = '" . $talimar_loan . "' ");
				$loan_status = $loan_status->result();
				$status = $loan_status[0]->loan_status;

				$current_date = date('Y-m-d');
				$recordingData = $this->User_model->query("SELECT recorded_date FROM `loan_recording_information` WHERE talimar_loan = '".$talimar_loan."'");
				if($recordingData->num_rows() > 0){

					$recordingData = $recordingData->result();
					$recorded_date = $recordingData[0]->recorded_date;

					$diff = '-'.$this->dateDiff($recorded_date, $current_date);
				}else{
					$diff = '';
				}

				$loan = $this->User_model->query("select * from loan where talimar_loan = '" . $talimar_loan . "'");
				$loan = $loan->result();
				$loan_id = $loan[0]->id;
				$amount = $loan[0]->loan_amount;
				$servicer = $loan[0]->fci;
				$closing_date = $loan[0]->loan_funding_date;
				

				$sql = "SELECT * FROM `loan_servicing_checklist` WHERE `talimar_loan`='" . $talimar_loan . "' AND type='post wire' AND hud='1008'";
				$servicing_checklist = $this->User_model->query($sql);

				if ($servicing_checklist->num_rows() > 0) {
					$servicing_checklist = $servicing_checklist->result();

				}

				$holding_again[] = array(

					"talimar_loan" => $talimar_loan,
					"loan_id" => $loan_id,
					"p_address" => $p_address,
					"status" => $status,
					"loan_amount" => $amount,
					"servicer" => $servicer,
					"closing_date" => $closing_date,
					"diff" => $diff,
					"check" => $servicing_checklist[0]->checklist,
					"recordingdate" => $recordingdate
				);
			}
			array_multisort(array_column($holding_again, 'diff'), SORT_ASC, $holding_again);
			$dataResult['holding_loan'] = $holding_again;
		} else {
			$dataResult['holding_loan'] = '';
		}
		$data['content'] = $this->load->view('dashboard/pages/loan_servicing', $dataResult, true);
		$this->load->view('template_files/template', $data);
	}

	public function lender_servicing(){

		$data = array();
		$dataResult = '';
		$data['content'] = $this->load->view('dashboard/pages/lender_servicing', $dataResult, true);
		$this->load->view('template_files/template', $data);
	}

	public function portfolio_servicing(){

		error_reporting(0);
		$data = array();
		$loginuser_dashboard = $this->loan_organization_setting();

			$loan_status_active['loan_status'] = '2';

			if ($loginuser_dashboard == 1) {

				$fetch_all_active = $this->User_model->select_where('loan_servicing', $loan_status_active);

			} else {

				$fetch_all_active = $this->User_model->query("select * from loan_servicing as ls JOIN loan as l ON ls.talimar_loan=l.talimar_loan JOIN loan_servicing_contacts as lsc ON lsc.talimar_loan=ls.talimar_loan where ls.loan_status='2' AND (l.loan_originator= '" . $this->session->userdata('t_user_id') . "' OR lsc.loan_originator= '" . $this->session->userdata('t_user_id') . "')");

			}
			$fetch_all_active = $fetch_all_active->result();

			$active_senior_balance_sql = "SELECT SUM(`supirior_original`) as sum_original, SUM(`supirior_current`) as sum_current FROM extra_details JOIN loan_servicing ON extra_details.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '2'";
			$active_senior_balance_result = $this->User_model->query($active_senior_balance_sql);
			$active_senior_balance_result = $active_senior_balance_result->result();

			$count = 0;
			$loan_amount = 0;
			$intrest_rate = 0;
			$arv = 0;
			$term_month = 0;
			$selling_price = 0;
			$future_value = 0;
			$current_balance = 0;
			foreach ($fetch_all_active as $active_loans) {
				$active_talimar_loan['talimar_loan'] = $active_loans->talimar_loan;

				// Fetch data from Loan
				$loan_data = $this->User_model->select_where('loan', $active_talimar_loan);
				$loan_data = $loan_data->result();
				foreach ($loan_data as $loan_row) {
					$loan_amount = $loan_amount + $loan_row->loan_amount;
					$current_balance += $loan_row->current_balance;
					$term_month = $term_month + $loan_row->term_month;
					$intrest_rate = $intrest_rate + $loan_row->intrest_rate;

				}

				// fetch property_data

				$property_data = $this->User_model->select_where('loan_property', $active_talimar_loan);
				$property_data = $property_data->result();
				foreach ($property_data as $property_row) {

					$arv = $arv + $property_row->arv;

					$future_value = $future_value + $property_row->underwriting_value;

				}

				$where_11['talimar_loan'] = $active_loans->talimar_loan;
				$fetch_loan_servicing = $this->User_model->select_where('loan_servicing', $where_11);
				$fetch_loan_servicing = $fetch_loan_servicing->result();
				foreach ($fetch_loan_servicing as $row) {
					$selling_price = $selling_price + $row->selling_price;
				}

				$count++;
				$select_ecum_total = $this->User_model->query("SELECT SUM(`current_balance`) as total_ecum FROM `encumbrances` as e JOIN property_home as ph ON e.`property_home_id` = ph.id where ph.primary_property = '1' AND e.talimar_loan = '" . $active_loans->talimar_loan . "'");
				$select_ecum_total = $select_ecum_total->result();
				$ecum_total = $select_ecum_total[0]->total_ecum;
			}

			$fetch_sum_extra = '';

			$active_loan_result = array(
				"active_count" => $count,
				"total_loan_amount" => $loan_amount,
				"total_current_balance" => $current_balance,

				"selling_price" => $selling_price,
				"total_intrest_rate" => number_format(($intrest_rate / $count),3),
				"total_arv" => $arv,
				"total_future_value" => $future_value,
				"total_term_month" => ($term_month) / $count,
				"senior_current" => $active_senior_balance_result[0]->sum_current,
				"senior_original" => $active_senior_balance_result[0]->sum_original,

			);

			$data['active_loan_result'] = $active_loan_result;

			$laon_default_forculare = $this->User_model->query("SELECT * FROM loan_servicing_default INNER JOIN add_foreclosure_request ON loan_servicing_default.talimar_loan=add_foreclosure_request.talimar_loan INNER JOIN loan_servicing ON loan_servicing.talimar_loan=add_foreclosure_request.talimar_loan JOIN loan as l ON l.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status='2' AND loan_servicing.condition='1' group by loan_servicing_default.talimar_loan");

			$laon_default_forculares = $laon_default_forculare->result();
			$total_default = 0;
			$total_amount = 0;
			foreach ($laon_default_forculares as $value) {
				if($value->foreclosuer_status == '1'){
					$total_default++;
					$total_amount += $value->loan_amount;
				}
			}

			$data['fetch_total_default'] = $total_default;
			$data['fetch_total_amount'] = $total_amount;
			if ($loginuser_dashboard == 1) {
				
				$holding = $this->User_model->query("
						SELECT * FROM loan_servicing AS ls 
						JOIN input_datas as ida ON ls.talimar_loan = ida.talimar_loan 
						WHERE ls.loan_status IN ('2') AND ida.input_name='checklist_complete' AND ida.page='loan_servicing_checklist' AND ida.value !='1' ");

			} else {
				
				$holding = $this->User_model->query("
					SELECT * FROM loan_servicing AS ls 
					JOIN input_datas as ida ON ls.talimar_loan = ida.talimar_loan 
					WHERE ls.loan_status IN ('2') AND ida.input_name='checklist_complete' AND ida.page='loan_servicing_checklist' AND ida.value !='1' AND ls.t_user_id = '" . $this->session->userdata('t_user_id') . "'");

			}

			if ($holding->num_rows() > 0) {

				$input_data = $holding->result();


				foreach ($input_data as $row) {

					$talimar_loan = $row->talimar_loan;

					$loan_recording = $this->User_model->query("SELECT `recorded_date` FROM `loan_recording_information` WHERE `talimar_loan` = '" . $talimar_loan . "'");
					if($loan_recording->num_rows() > 0){
						$date = $loan_recording->result();
						$rdate = $date[0]->recorded_date;

						if($rdate != ''){

							$recordingdate = date('m-d-Y', strtotime($rdate));
						}else{
							$recordingdate = 'Error';
						}
					}else{

						$recordingdate = 'Error';
					}


					$address = $this->User_model->query("select property_address from loan_property where talimar_loan = '" . $talimar_loan . "'");
					$address = $address->result();
					$p_address = $address[0]->property_address;

					$loan_status = $this->User_model->query("SELECT loan_status FROM loan_servicing WHERE talimar_loan = '" . $talimar_loan . "' ");
					$loan_status = $loan_status->result();
					$status = $loan_status[0]->loan_status;

					$current_date = date('Y-m-d');
					$recordingData = $this->User_model->query("SELECT recorded_date FROM `loan_recording_information` WHERE talimar_loan = '".$talimar_loan."'");
					if($recordingData->num_rows() > 0){

						$recordingData = $recordingData->result();
						$recorded_date = $recordingData[0]->recorded_date;

						$diff = '-'.$this->dateDiff($recorded_date, $current_date);
					}else{
						$diff = '';
					}

					$loan = $this->User_model->query("select * from loan where talimar_loan = '" . $talimar_loan . "'");
					$loan = $loan->result();
					$loan_id = $loan[0]->id;
					$amount = $loan[0]->loan_amount;
					$servicer = $loan[0]->fci;
					$closing_date = $loan[0]->loan_funding_date;
					

					$sql = "SELECT * FROM `loan_servicing_checklist` WHERE `talimar_loan`='" . $talimar_loan . "' AND type='post wire' AND hud='1008'";
					$servicing_checklist = $this->User_model->query($sql);

					if ($servicing_checklist->num_rows() > 0) {
						$servicing_checklist = $servicing_checklist->result();

					}

					$holding_again[] = array(

						"talimar_loan" => $talimar_loan,
						"loan_id" => $loan_id,
						"p_address" => $p_address,
						"status" => $status,
						"loan_amount" => $amount,
						"servicer" => $servicer,
						"closing_date" => $closing_date,
						"diff" => $diff,
						"check" => $servicing_checklist[0]->checklist,
						"recordingdate" => $recordingdate
					);
				}
				array_multisort(array_column($holding_again, 'diff'), SORT_ASC, $holding_again);
				$data['holding_loan'] = $holding_again;
			} else {
				$data['holding_loan'] = '';
			}

			//$disb_verified = $this->User_model->select_where('loan_servicing',$where_disb);
			$disb_verified = $this->User_model->query("select * from loan_servicing where servicing_disb='2' AND loan_status='2'");

			$disb_verified = $disb_verified->result();
			$count_disb = count($disb_verified);
			$data['all_count_disb'] = $count_disb;

			//FETCH MARKETING COMPLETE from loan servicing...

			$sql = "SELECT COUNT(*) as total FROM `loan_servicing` WHERE `loan_status`='2' AND `marketing_complete` !='1'";
			$fetch_marketing = $this->User_model->query($sql);
			$fetch_marketing = $fetch_marketing->result();
			$fetch_marketing = $fetch_marketing[0]->total;
			$data['fetch_marketing'] = $fetch_marketing;

			//  Monthly income servicing loan

			$join_sql = "SELECT *, loan.id as loan_id FROM loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan_servicing.loan_status = '2' ";
			$fetch_all_loan = $this->User_model->query($join_sql);
			$fetch_all_loan = $fetch_all_loan->result();
			foreach ($fetch_all_loan as $row) {
				$talimar['talimar_loan'] = $row->talimar_loan;
				$talimar_loan = $row->talimar_loan;
				$loan_id = $row->loan_id;
				$fci = $row->fci;
				$loan_amount = $row->loan_amount;
				$current_balancee = $row->current_balance;

				//  fetch data from loan servicing

				$fetch_servicing = $this->User_model->select_where('loan_servicing', $talimar);
				$fetch_servicing = $fetch_servicing->result();
				
				$fetch_loan_assigment = $this->User_model->select_where('loan_assigment', $talimar);
			    $fetch_loan_assigment = $fetch_loan_assigment->result();

				$servicing_fee = $fetch_servicing[0]->servicing_fee;
				$who_pay_servicing = $fetch_servicing[0]->who_pay_servicing;
				$sub_servicing_agent = $fetch_servicing[0]->sub_servicing_agent;
				$minimum_servicing_fee = $fetch_servicing[0]->minimum_servicing_fee;
				$broker_servicing_fees = $fetch_servicing[0]->broker_servicing_fees;
				$servicing_lender_rate = number_format($fetch_servicing[0]->servicing_lender_rate,3);
				$sql = "SELECT count(*) as countrow from loan_assigment WHERE talimar_loan = '" . $talimar_loan . "' ";
				$count_lender = $this->User_model->query($sql);
				if ($count_lender->num_rows() > 0) {
					$count_lender = $count_lender->result();
					$count_lender = $count_lender[0]->countrow;
				} else {
					$count_lender = 0;
				}

				$all_monthly_income[] = array(
					'talimar_loan' => $talimar_loan,
					'loan_id' => $loan_id,
					'fci' => $fci,
					'loan_amount' => $loan_amount,
					'current_balancee' => $current_balancee,
					'sub_servicing_agent' => $sub_servicing_agent,
					'who_pay_servicing' => $who_pay_servicing,
					'servicing_fee' => $servicing_fee,
					'minimum_servicing_fee' => $minimum_servicing_fee,
					'broker_servicing_fees' => $broker_servicing_fees,
					'count_lender' => $count_lender,
					'interest_rate' => number_format($row->intrest_rate,3),
					'servicing_lender_rate' => number_format($servicing_lender_rate,3),
					'invester_yield_percent' => $fetch_loan_assigment[0]->invester_yield_percent,

				);
			}
			
				
			$total_servicing_income = 0;
			$total_servicing_cost = 0;
			foreach ($all_monthly_income as $key => $row) {
				
				$payment_pro_fees = $all_monthly_income[$key]['loan_amount'];

				$pro_fees = '0';
				if ($payment_pro_fees > 0 && $payment_pro_fees <= 400000) {
					$pro_fees = '0';
				} elseif ($payment_pro_fees >= 400001 && $payment_pro_fees <= 500000) {
					$pro_fees = '25.00';
				} elseif ($payment_pro_fees >= 500001 && $payment_pro_fees <= 600000) {
					$pro_fees = '35.00';
				} elseif ($payment_pro_fees >= 600001 && $payment_pro_fees <= 700000) {
					$pro_fees = '45.00';
				} elseif ($payment_pro_fees >= 700001 && $payment_pro_fees <= 800000) {
					$pro_fees = '55.00';
				} elseif ($payment_pro_fees >= 800001 && $payment_pro_fees <= 900000) {
					$pro_fees = '65.00';
				} elseif ($payment_pro_fees >= 900001 && $payment_pro_fees <= 1000000) {
					$pro_fees = '75.00';
				} elseif ($payment_pro_fees >= 1000001 && $payment_pro_fees <= 2000000) {

					$pro_fees = '95.00';
				} elseif ($payment_pro_fees >= 2000001 && $payment_pro_fees <= 3000000) {

					$pro_fees = '115.00';
				} elseif ($payment_pro_fees >= 3000001 && $payment_pro_fees <= 4000000) {

					$pro_fees = '135.00';
				} elseif ($payment_pro_fees >= 4000001 && $payment_pro_fees <= 5000000) {

					$pro_fees = '155.00';
				} elseif ($payment_pro_fees >= 5000001 && $payment_pro_fees <= 6000000) {

					$pro_fees = '175.00';
				} elseif ($payment_pro_fees >= 6000001 && $payment_pro_fees <= 7000000) {

					$pro_fees = '195.00';
				} elseif ($payment_pro_fees >= 7000001 && $payment_pro_fees <= 8000000) {

					$pro_fees = '215.00';
				} elseif ($payment_pro_fees >= 8000001 && $payment_pro_fees <= 9000000) {

					$pro_fees = '235.00';
				} elseif ($payment_pro_fees >= 9000001 && $payment_pro_fees <= 10000000) {

					$pro_fees = '255.00';
				} elseif ($payment_pro_fees >= 10000001 && $payment_pro_fees <= 11000000) {

					$pro_fees = '275.00';
				} elseif ($payment_pro_fees >= 11000001 && $payment_pro_fees <= 12000000) {

					$pro_fees = '295.00';
					
				}else {

					$pro_fees = '0';
				}
				
				
				$loan_amount = $all_monthly_income[$key]['loan_amount'];
				$servicing_fee = $all_monthly_income[$key]['servicing_fee'];
				//$minimum_servicing_fee = $all_monthly_income[$key]['minimum_servicing_fee'];
				//$servicing_cost = $all_monthly_income[$key]['count_lender'] * $minimum_servicing_fee;
				
				if ($all_monthly_income[$key]['servicing_lender_rate'] == 'NaN' || $all_monthly_income[$key]['servicing_lender_rate'] == '') {

					$lender_ratessss = $all_monthly_income[$key]['invester_yield_percent'];
				} else {

					$lender_ratessss = $all_monthly_income[$key]['servicing_lender_rate'];
				}
				
				$nnew_val = $all_monthly_income[$key]['interest_rate'] - $lender_ratessss;
				$nnew_valss = $nnew_val / 100;

				$gross_servicing = ($all_monthly_income[$key]['current_balancee'] * $nnew_valss) / 12;

				$minimum_servicing_fee = $all_monthly_income[$key]['minimum_servicing_fee'];

				// $servicing_income=$gross_servicing - $servicing_cost;
				$lender_servicing_fee = $all_monthly_income[$key]['count_lender'] * $all_monthly_income[$key]['minimum_servicing_fee'];

				if ($lender_servicing_fee > $pro_fees) {

					$pro_fees = 0;

				} else {

					$lender_servicing_fee = 0;

				}
				
				$current_balance = $all_monthly_income[$key]['current_balancee'];

				$servicing_cost = $lender_servicing_fee + $pro_fees + $all_monthly_income[$key]['broker_servicing_fees'];
				$servicing_income = $gross_servicing - $servicing_cost;

				if ($all_monthly_income[$key]['who_pay_servicing'] == 2) {

					$gross_servicing = $gross_servicing;
					$servicing_cost = 0;
					$servicing_income = $servicing_income;
					$lender_servicing_fee = 0;
					$all_monthly_income[$key]['servicing_fee'] = $servicing_fee;
					$all_monthly_income[$key]['minimum_servicing_fee'] = 0;
				}
				
				$all_incomee[] = array(
				
									'gross_servicingg' => $gross_servicing,
									'current_balancee' => $current_balance,
								 );
				
				
				
				
				//extra...
				$gross_servicingxx = ($loan_amount * ($servicing_fee / 100)) / 12;
				$servicing_income = $gross_servicingxx - $servicing_cost;
			}
				
			$total_gross_servicing = 0;
			$total_amount = 0;
			foreach ($all_incomee as $key => $all_income) {
				
				$total_gross_servicing = $total_gross_servicing + $all_income['gross_servicingg'];
				$total_amount = $total_amount + $all_income['current_balancee'];
			}
			//echo $total_gross_servicing;
			//echo $total_amount;
			$data['spred_val_avg'] = number_format((($total_gross_servicing*12)/$total_amount)*100,2);
		
		$data['content'] = $this->load->view('dashboard/pages/portfolio_servicing', $data, true);
		$this->load->view('template_files/template', $data);
	}

	public function marketing(){

		$data = array();
		$dataResult = '';
		$data['content'] = $this->load->view('dashboard/pages/marketing', $dataResult, true);
		$this->load->view('template_files/template', $data);
	}

	public function talimar_balance_sheet(){

		$data = array();
		$dataResult = '';
		$data['content'] = $this->load->view('dashboard/pages/talimar_balance_sheet', $dataResult, true);
		$this->load->view('template_files/template', $data);
	}

	public function contact_swag_request(){

		$data = array();
		$dataResult = '';
		$data['content'] = $this->load->view('dashboard/pages/contact_swag_request', $dataResult, true);
		$this->load->view('template_files/template', $data);
	}

	public function lateDaysInloans()
	{
		$data = array();
		error_reporting(0);

		$allLoan = $this->User_model->query("SELECT l.id, l.id AS loan_id, lh.daysLate, lh.loanAccount, lh.nextDueDate, l.talimar_loan, l.dayVariance, lp.property_address, lp.unit, lp.city, lp.state, lp.zip 
			FROM loan AS l
			INNER JOIN lender_payment_history AS lh ON l.talimar_loan=lh.talimar_loan 
			LEFT JOIN loan_servicing_checklist AS lc ON l.talimar_loan=lc.talimar_loan 
			LEFT JOIN loan_property AS lp ON lc.talimar_loan=lp.talimar_loan WHERE lh.daysLate  > 0 GROUP BY l.talimar_loan ORDER BY lh.daysLate Desc, l.loan_amount Desc
			");
		$data['loanData'] = $allLoan->result();

		$content1 = $this->load->view('dashboard/lateDayLoans', $data, true);
		echo $content1;
	}
	/*
		Description : This function use for get  list of Outstanding Servicer Deposits in loan servicing .
		Author      : Bitcot
		Created     : 23-08-2021
		Modified    :
	*/
	function outstandingServicerDeposits() {
		error_reporting(0);
		//Boarding...
		$outstandingServicerQue = "SELECT l.id,l.talimar_loan FROM loan AS l JOIN loan_servicing AS ls ON  l.talimar_loan=ls.talimar_loan JOIN loan_servicing_checklist AS lsc ON ls.talimar_loan=lsc.talimar_loan WHERE ls.loan_status='2'  AND (  (lsc.hud='1042' AND ((lsc.checklist='0' OR lsc.checklist='') AND (lsc.not_applicable='0' OR lsc.not_applicable=''))) OR (lsc.hud='1016' AND ((lsc.checklist='0' OR lsc.checklist='') AND (lsc.not_applicable='0' OR lsc.not_applicable=''))) ) group by l.id";
		$outstandingServicerResponed = $this->User_model->query($outstandingServicerQue);
		$outstandingServicerResult = $outstandingServicerResponed->result();		
		$resultArray=array();
		foreach($outstandingServicerResult as $value){			
			$outstandingServicerQue1016="SELECT lsc.*,lp.property_address FROM  loan_servicing_checklist AS lsc JOIN loan_property AS lp ON lsc.talimar_loan=lp.talimar_loan WHERE lsc.talimar_loan = '".$value->talimar_loan."' AND  lsc.hud IN ('1016','1042','1056','135','137','138','1037','1068','1058') ";
			$outstandingServicerResponed1016 = $this->User_model->query($outstandingServicerQue1016);
			if($outstandingServicerResponed1016->num_rows() > 0){
				$outstandingServicerResult1016 = $outstandingServicerResponed1016->result();
				$p=1;
				$successArray=[];
				$checkedData=array();
				$disabled_com33=array();
				$successStatus=0;
				foreach($outstandingServicerResult1016 as $row){
					$recivedFirstCondition[$row->hud]='';
					$appliedFirstCondition[$row->hud]='';
					if($row->hud == '1042' && ($row->checklist=='1')){
						$recivedFirstCondition[$row->hud]='1';
					}
					if($row->hud == '1016' && ($row->checklist=='1')){
						$appliedFirstCondition[$row->hud]='1';
					}
					if($row->hud == '1056' || $row->hud == '135' || $row->hud == '137' || $row->hud == '138' || $row->hud == '1037' || $row->hud == '1068' || $row->hud == '1058'){ 
             			if($row->checklist == 1 && $row->not_applicable == 1){
							$disabled_com33[] = '1';
						}else{
							$disabled_com33[] = '2';
						}
					}
					if($p==count($outstandingServicerResult1016)){
						if(in_array(1, $disabled_com33)){
						}else{
							if($recivedFirstCondition['1042']=="1"){
								$recivedCondition="Yes";
							}else{
								$recivedCondition="No";
							}
							if($appliedFirstCondition['1016']=="1"){
								$appliedCondition="Yes";
							}else{
								$appliedCondition="No";
							}
							$resultArray[]=array(
													"loan_id"=>$row->loan_id,
													"talimar_loan"=>$row->talimar_loan,
													"property_address"=>$row->property_address,
													"recived"=>$recivedCondition,
													"apply"=>$appliedCondition
												);

						}
					}
					$p++;					
				}
			}
		}
		$data['outstandingServicerData'] = $resultArray;
		$data['kk'] = '';
		$content1 = $this->load->view('dashboard/ajax_outstanding_servicer_deposits', $data, true);
		echo $content1;
	}
	/*
		Description : This function use for get  list of Loan Servicing Overview in loan servicing .
		Author      : Bitcot
		Created     : 23-08-2021
		Modified    :
	*/
	function loanServicingOverview(){
		$loanServicingOverview=array();
		/*************************************Get Total Record of 'Process Lender Fees' ****************/
		$processLenderQuery     = "SELECT count(l.talimar_loan) as total_processLender FROM loan AS l JOIN loan_servicing AS ls ON  l.talimar_loan=ls.talimar_loan JOIN loan_servicing_checklist AS lsc ON ls.talimar_loan=lsc.talimar_loan WHERE ls.loan_status='2' AND  lsc.hud='1014' AND ((lsc.checklist='0' OR lsc.checklist='') AND (lsc.not_applicable='0' OR lsc.not_applicable=''))";
		$queryResponed = $this->User_model->query($processLenderQuery);
		$resultArray = $queryResponed->result();
		$loanServicingOverview['processLender']=$resultArray[0]->total_processLender;

		/*************************************Get Total Record of '“Loan Servicing” Introduction call with Borrower' ****************/
		$introductionBorrowerQuery     = "SELECT count(l.talimar_loan) as total_introductionBorrower FROM loan AS l JOIN loan_servicing AS ls ON  l.talimar_loan=ls.talimar_loan JOIN loan_servicing_checklist AS lsc ON ls.talimar_loan=lsc.talimar_loan WHERE ls.loan_status='2' AND  lsc.hud='147' AND ((lsc.checklist='0' OR lsc.checklist='') AND (lsc.not_applicable='0' OR lsc.not_applicable=''))";
		$queryResponed = $this->User_model->query($introductionBorrowerQuery);
		$resultArray = $queryResponed->result();
		$loanServicingOverview['introductionBorrower']=$resultArray[0]->total_introductionBorrower;

		/*************************************Get Total Record of 'Submit Loan to Servicer' ****************/
		$submitLoanQuery     = "SELECT count(l.talimar_loan) as total_submitLoan FROM loan AS l JOIN loan_servicing AS ls ON  l.talimar_loan=ls.talimar_loan JOIN loan_servicing_checklist AS lsc ON ls.talimar_loan=lsc.talimar_loan WHERE ls.loan_status='2' AND  lsc.hud='1008' AND ((lsc.checklist='0' OR lsc.checklist='') AND (lsc.not_applicable='0' OR lsc.not_applicable=''))";
		$queryResponed = $this->User_model->query($submitLoanQuery);
		$resultArray = $queryResponed->result();
		$loanServicingOverview['submitLoan']=$resultArray[0]->total_submitLoan;

		/*************************************Get Total Record of 'Submit Loan to Servicer' ****************/
		$confirmServicerQuery     = "SELECT count(l.talimar_loan) as total_confirmServicer FROM loan AS l JOIN loan_servicing AS ls ON  l.talimar_loan=ls.talimar_loan JOIN loan_servicing_checklist AS lsc ON ls.talimar_loan=lsc.talimar_loan WHERE ls.loan_status='2' AND  lsc.hud='1042' AND ((lsc.checklist='0' OR lsc.checklist='') AND (lsc.not_applicable='0' OR lsc.not_applicable=''))";
		$queryResponed = $this->User_model->query($confirmServicerQuery);
		$resultArray = $queryResponed->result();
		$loanServicingOverview['confirmServicer']=$resultArray[0]->total_confirmServicer;

		/*************************************Get Total Record of 'Initial Review Complete' ****************/
		$initialCompleteQuery     = "SELECT count(l.talimar_loan) as total_initialComplete FROM loan AS l JOIN loan_servicing AS ls ON  l.talimar_loan=ls.talimar_loan JOIN loan_servicing_checklist AS lsc ON ls.talimar_loan=lsc.talimar_loan WHERE ls.loan_status='2' AND  lsc.hud='146' AND ((lsc.checklist='0' OR lsc.checklist='') AND (lsc.not_applicable='0' OR lsc.not_applicable=''))";
		$queryResponed = $this->User_model->query($initialCompleteQuery);
		$resultArray = $queryResponed->result();
		$loanServicingOverview['initialComplete']=$resultArray[0]->total_initialComplete;

		/*************************************Get Total Record of 'Confirm Servicer Applied Closing Funds Correctly' ****************/
		$confirmAppliedQuery     = "SELECT count(l.talimar_loan) as total_confirmApplied FROM loan AS l JOIN loan_servicing AS ls ON  l.talimar_loan=ls.talimar_loan JOIN loan_servicing_checklist AS lsc ON ls.talimar_loan=lsc.talimar_loan WHERE ls.loan_status='2' AND  lsc.hud='1016' AND ((lsc.checklist='0' OR lsc.checklist='') AND (lsc.not_applicable='0' OR lsc.not_applicable=''))";
		$queryResponed = $this->User_model->query($confirmAppliedQuery);
		$resultArray = $queryResponed->result();
		$loanServicingOverview['confirmApplied']=$resultArray[0]->total_confirmApplied;

		/*************************************Get Total Record of 'Obtain Original Signed Loan Documents' ****************/
		$obtainOriginalQuery     = "SELECT count(l.talimar_loan) as total_obtainOriginal FROM loan AS l JOIN loan_servicing AS ls ON  l.talimar_loan=ls.talimar_loan JOIN loan_servicing_checklist AS lsc ON ls.talimar_loan=lsc.talimar_loan WHERE ls.loan_status='2' AND  lsc.hud='1067' AND ((lsc.checklist='0' OR lsc.checklist='') AND (lsc.not_applicable='0' OR lsc.not_applicable=''))";
		$queryResponed = $this->User_model->query($obtainOriginalQuery);
		$resultArray = $queryResponed->result();
		$loanServicingOverview['obtainOriginal']=$resultArray[0]->total_obtainOriginal;


		$totalLatePayments=0;
		$totalLoanCondition=0;
		$totalLenderApproval=0;
		$totalExtensionStatus=0;
		$totalPayOffCondition=0;
		$totalRequestsReview=0; 
		$que = "SELECT l.id as glbal_loan_id,l.talimar_loan as glbal_talimar_loanas,lys.nextDueDate,lys.daysLate,ls.condition,pop.de_Payoff_Request,pop.de_Servicing_Approved,lrc.* 
				FROM loan AS l LEFT JOIN loan_servicing AS ls ON  l.talimar_loan=ls.talimar_loan 
				LEFT JOIN lender_payment_history AS lys ON l.talimar_loan=lys.talimar_loan
				LEFT JOIN pay_off_processing AS pop ON l.talimar_loan=pop.talimar_loan
				LEFT JOIN loan_reserve_checkbox AS lrc ON l.talimar_loan=lrc.talimar_loan
				WHERE ls.loan_status='2' ";
		$queryResponed = $this->User_model->query($que);
		if($queryResponed->num_rows() > 0){
			$allActiveLoans = $queryResponed->result();
			foreach ($allActiveLoans as $key => $loan) {
				if(!empty($loan->daysLate) && $loan->daysLate>10){
						$totalLatePayments++;
				}				
				if(!empty($loan->condition) && $loan->condition=="1"){
					$totalLoanCondition++;					
				}
				$statusValueData=$this->lenderApprovalStatus($loan->glbal_talimar_loanas);
				if($statusValueData=="true"){
					$totalLenderApproval++;
				}
				$extProcessOption=$this->extensionProcessingStatus($loan->glbal_talimar_loanas);
				if($extProcessOption=="true"){
					$totalExtensionStatus++;
				}
				if($loan->de_Payoff_Request=="1" && $loan->de_Servicing_Approved!="1"){
					$totalPayOffCondition++;
				}
				$m=1;
				$approvedByAdmin=0;
				$approvedByServicing=0;
				for($m=1;$m<16;$m++){
					$indexValue="date".$m."_draw_approved";
					if($loan->$indexValue!="1"){
						$approvedByAdmin++;
					}
				}
				if($approvedByAdmin=="15"){
					for($m=1;$m<16;$m++){
						$indexValue="date".$m."_request_loan";
						if($loan->$indexValue=="1"){
							$approvedByServicing=1;
							break;
						}
					}
				}
				if($approvedByServicing==1){
					$totalRequestsReview++;
				}
			}
		}
		$loanServicingOverview['totalLatePayments']=$totalLatePayments;
		$loanServicingOverview['totalLoanCondition']=$totalLoanCondition;
		$loanServicingOverview['totalLenderApproval']=$totalLenderApproval;
		$loanServicingOverview['totalExtensionStatus']=$totalExtensionStatus;
		$loanServicingOverview['totalPayOffCondition']=$totalPayOffCondition;
		$loanServicingOverview['totalRequestsReview']=$totalRequestsReview;
		$data['loanServicingOverview'] = $loanServicingOverview;
		$data['kk'] = '';
		$content1 = $this->load->view('dashboard/ajax_loan_servicing_overview', $data, true);
		echo $content1;
	}
	public function lenderApprovalStatus($talimar_loan){
		$que = "SELECT LP_status FROM lender_approval WHERE talimar_loan='$talimar_loan' AND LP_status='1' ";
		$queryResponedLP = $this->User_model->query($que);
		if($queryResponedLP->num_rows() > 0){
			$statusValue="true";
		}else{
			$statusValue="false";
		}
		return $statusValue;
	}
	public function extensionProcessingStatus($talimar_loan){
		$que = "SELECT es.ext_process_option FROM extension_section as es JOIN property_home as ph ON es.talimar_loan = ph.talimar_loan WHERE es.talimar_loan='$talimar_loan' AND es.ext_process_option='1' AND ph.primary_property = '1' ";
		$queryResponedLP = $this->User_model->query($que);
		if($queryResponedLP->num_rows() > 0){
			$statusValue="true";
		}else{
			$statusValue="false";
		}
		return $statusValue;
	}
	public function topActiveInvestors(){
		$content1 = $this->load->view('dashboard/top_active_investors', $data, true);
	}
	
}

?>
