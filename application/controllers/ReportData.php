<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReportData extends CI_Controller {

	public function __construct() {

		ini_set('max_execution_time', 72000);
		ini_set('memory_limit', '2048M');

		parent::__construct();
		$this->load->database();
		$this->load->model('User_model');
		$this->load->library('MYPDF');
		$this->load->library('Aws3','aws3');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');
		
		

		if ($this->session->userdata('t_user_id') != '') {
		} else {
			redirect(base_url() . "home", 'refresh');
		}

		/* for loan originator... */
		if ($this->session->userdata('user_role') == 1) {

			$user_settings = $this->User_model->query("SELECT * FROM `user_settings` WHERE `user_id`= '" . $this->session->userdata('t_user_id') . "' AND `loan_organization`='1'");
			if ($user_settings->num_rows() > 0) {

				redirect(base_url() . "load_data", 'refresh');
			} else {
				$loginuser_borrowers = '';
			}
		} else {
			$loginuser_borrowers = '';
		}
	}


	public function LineofCredit(){

		$fetchLineofCredit = $this->User_model->select_where('borrower_data',array('PreApproved'=>1));
		$fetchLineofCredit = $fetchLineofCredit->result();

		foreach ($fetchLineofCredit as $value) {
			$fetchcontact = $this->User_model->query("SELECT c.contact_id, c.contact_firstname, c.contact_lastname, c.contact_phone, c.contact_email FROM borrower_contact as bc JOIN contact as c ON bc.contact_id = c.contact_id WHERE bc.borrower_id = '".$value->id."'");
			if($fetchcontact->num_rows() > 0){
				$fetchcontact = $fetchcontact->result();

				$fullname = $fetchcontact[0]->contact_firstname.' '.$fetchcontact[0]->contact_lastname;
				$contact_phone = $fetchcontact[0]->contact_phone;
				$contact_email = $fetchcontact[0]->contact_email;
				$contact_id = $fetchcontact[0]->contact_id;
				$contact_firstname=$fetchcontact[0]->contact_firstname;
				$contact_lastname=$fetchcontact[0]->contact_lastname;
			}else{

				$fullname = '';
				$contact_phone = '';
				$contact_email = '';
				$contact_id = '';
				$contact_firstname="";
				$contact_lastname="";
			}

			if($value->ApprovedTerm != ''){
				$exdate = date('m-d-Y', strtotime('+'.$value->ApprovedTerm.' months'. $value->ApprovedDate));
			}else{
				$exdate = date('m-d-Y', strtotime($value->ApprovedDate));
			}
			if($value->ApprovedDate=="" || $value->ApprovedDate=='0000-00-00'){
				$value->ApprovedDate="";
				$strDate="";
			}else{
				$strDate=strtotime($value->ApprovedDate);
				$value->ApprovedDate=date('m-d-Y', strtotime($value->ApprovedDate));
			}

			$LineofCredit[] = array(
										"borrower_id" => $value->id,
										"borrower_name" => $value->b_name,
										"amount" => $value->ApprovedAmount,
										"date" =>$value->ApprovedDate,
										"strDate"=>$strDate,
										"exdate" => $exdate,
										"fullname" => $fullname,
										"contact_phone" => $contact_phone,
										"contact_email" => $contact_email,
										"contact_id" => $contact_id,
										'contact_last_name'=>$contact_lastname,
										'contact_first_name'=>$contact_firstname,
									);
		}

		$data['LineofCreditData'] = $LineofCredit;
		$LineofCreditData = $LineofCredit;

		if($this->input->post('reportDisplay') == 'excel'){

			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename="Preferred-borrower-accounts.csv";');

			$f = fopen("php://output", "w");
			fputcsv($f, array('Borrower Name', 'Contact 1 Name', 'Contact 1 Phone', 'Contact 1 Email', 'Amount', 'Date', 'Expiration Date')); // title...

			$atm_amount = 0;
			if(!empty($row['amount'])){
				$atm_amount = $row['amount'];
			}

			foreach ($LineofCreditData as $row) {

				$output['borrower_name'] = $row['borrower_name'];
				$output['fullname'] = $row['fullname'];
				$output['contact_phone'] = $row['contact_phone'];
				$output['contact_email'] = $row['contact_email'];
				$output['amount'] = number_format($atm_amount);
				$output['date'] = $row['date'];
				$output['exdate'] = $row['exdate'];
				
				fputcsv($f, $output);
			}
			
			fclose($f);
			exit();

		}elseif($this->input->post('reportDisplay') == 'pdf'){

			$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
			$pdf->SetCreator(PDF_CREATOR);
			// $pdf->SetHeaderData("include/icons/logo-light.png", 30, '', '');
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
			$pdf->SetMargins(2, 20, 2);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
			$pdf->SetFont('times', '', 7);

			$pdf->AddPage('L', 'A4');
		
			$header = '<h1 style="color:#003468;font-size:18px;"> Preferred Borrower Accounts</h1>';
			$header .= '<style>
	                        .table {
	                            width: 100%;
	                            max-width: 100%;
	                            margin-bottom: 20px;
	                            border-collapse:collapse;
	                        }
	                        .table-bordered {
	                        border: 1px solid #ddd;
	                        }
	                        table {
	                            border-spacing: 0;
	                            border-collapse: collapse;
	                        }
	                        .table td{
	                            height : 25px;
	                            font-size:12px;
	                        }
	                        tr.table_header th{
	                            height:25px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:center;
	                            background-color:#bfcfe3;
	                        }

	                        tr.table_bottom th{
	                            height:20px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:left;
	                            background-color:#bfcfe3;
	                        }

	                        tr.table_bottom td{
	                            height:20px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:left;
	                            background-color:#bfcfe3;
	                        }
	                        
	                        tr.odd td{
	                            background-color:#ededed;
	                        }
	                        </style>
	                        ';

			$header .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';
			$header .= '<tr class="table_header">';

			$header .= '<th style="width:16%;text-decoration:underline;"><strong>Borrower Name</strong></th>';
			$header .= '<th style="width:16%;text-decoration:underline;"><strong>Contact 1 Name</strong></th>';
			$header .= '<th style="width:12%;text-decoration:underline;"><strong>Contact 1 Phone</strong></th>';
			$header .= '<th style="width:18%;text-decoration:underline;"><strong>Contact 1 E-Mail</strong></th>';
			$header .= '<th style="width:12%;text-decoration:underline;"><strong>Amount</strong></th>';
			$header .= '<th style="width:12%;text-decoration:underline;"><strong>Date</strong></th>';
			$header .= '<th style="width:14%;text-decoration:underline;"><strong>Expiration Date</strong></th>';
			$header .= '</tr>';

			
			$num = 0;
			$amount = 0;
				foreach ($LineofCreditData as $key => $row) {

					if ($num % 2 == 0) {
						$class_add = "even";
					} else if ($num == 0) {
						$class_add = "even";
					} else {
						$class_add = "odd";
					}

					$amount_ap = 0;
					if(!empty($row["amount"])){
						$amount += $row["amount"];
						$amount_ap = $row["amount"];
					}

					$header .= '<tr class="' . $class_add . '">';
					$header .= '<td>' . $row["borrower_name"] . '</td>';
					$header .= '<td>' . $row["fullname"] . '</td>';
					$header .= '<td>' . $row["contact_phone"] . '</td>';
					$header .= '<td>' . $row["contact_email"] . '</td>';
					$header .= '<td>$' . number_format($amount_ap) . '</td>';
					$header .= '<td>' . $row['date']. '</td>';
					$header .= '<td>' . $row['exdate']. '</td>';
					
					$header .= '</tr>';
					$num++;
				}

			$header .= '<tr class="table_bottom">';
				$header .= "<td><strong>Total: ".$num."</strong></td>";
				$header .= "<td></td>";
				$header .= "<td></td>";
				$header .= "<td></td>";
				$header .= "<td><strong>$".number_format($amount)."</strong></td>";
				$header .= "<td></td>";
				$header .= "<td></td>";
			$header .= '</tr>';

			$header .= '</table>';

			$pdf->WriteHTML($header);
			$pdf->Output("Preferred-borrower-accounts.pdf", "D");

		}else{

			$data['content'] = $this->load->view('Reports/LineofCredit', $data, true);
			$this->load->view('template_files/template_reports', $data);
		}
	}

	

	public function renovation_reserve(){

		$loan_status = $this->input->post('loan_status');
		$cons_reserve = $this->input->post('cons_reserve');

		if($loan_status != ''){

			$condition = "ls.loan_status = '".$loan_status."'";
		}else{
			$condition = "ls.loan_status IN ('1','2','3','5')";
		}

		if($cons_reserve != ''){

			$condition1 = "AND l.draws = '".$cons_reserve."'";
		}else{
			$condition1 = "";
		}

		$data['loan_status'] = $loan_status;
		$data['cons_reserve'] = $cons_reserve;

		$fetchdraw = $this->User_model->query("SELECT lp.property_address, lp.unit, lp.city, lp.state, lp.zip, l.id as loan_id, l.talimar_loan, l.draws FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan JOIN loan_property as lp ON l.talimar_loan = lp.talimar_loan WHERE ".$condition." ".$condition1." ORDER BY l.id DESC");
		$fetchdraw = $fetchdraw->result();

		foreach ($fetchdraw as $value) {

			//for loan reserve 2...
			$fetchamountsum = $this->User_model->query("SELECT SUM(amount) as total FROM `loan_draw_schedule` WHERE `talimar_loan`='".$value->talimar_loan."'");
			if($fetchamountsum->num_rows() > 0){
				$fetchamountsum = $fetchamountsum->result();
				$totalAmount = $fetchamountsum[0]->total;
			}else{
				$totalAmount = 0;
			}

			$fetchamountreq = $this->User_model->query("SELECT SUM(`amount_Req`) as reqtotal FROM `loan_draw_released` WHERE `talimar_loan`='".$value->talimar_loan."'");
			if($fetchamountreq->num_rows() > 0){
				$fetchamountreq = $fetchamountreq->result();
				$reqAmount = $fetchamountreq[0]->reqtotal;
			}else{
				$reqAmount = 0;
			}


			//for loan reserve 1...
			$fetchbudgetsum = $this->User_model->query("SELECT SUM(`budget`) as totalbudget FROM `loan_reserve_draws` WHERE `talimar_loan`= '".$value->talimar_loan."'");
			if($fetchbudgetsum->num_rows() > 0){
				$fetchbudgetsum = $fetchbudgetsum->result();
				$totalBudget = $fetchbudgetsum[0]->totalbudget;
			}else{
				$totalBudget = 0;
			}

			$fetchdrawreq = $this->User_model->query("SELECT SUM(`date1_amount` + `date2_amount` + `date3_amount` + `date4_amount` + `date5_amount` + `date6_amount` + `date7_amount` + `date8_amount` + `date9_amount` + `date10_amount` + `date11_amount` + `date12_amount` + `date13_amount` + `date14_amount` + `date15_amount`) as drawreq FROM `loan_reserve_draws` WHERE `talimar_loan`='".$value->talimar_loan."'");
			if($fetchdrawreq->num_rows() > 0){
				$fetchdrawreq = $fetchdrawreq->result();
				$reqdrawreq = $fetchdrawreq[0]->drawreq;
			}else{
				$reqdrawreq = 0;
			}
			

			$renovation_reserve_array[] = array(
													"property_address" => $value->property_address,
													"unit" => $value->unit,
													"city" => $value->city,
													"state" => $value->state,
													"zip" => $value->zip,
													"draws" => $value->draws,
													"loan_id" => $value->loan_id,
													"draw1" => $totalBudget - $reqdrawreq,
													"draw2" => $totalAmount - $reqAmount,

												);

		}


		$data['renovation_reservedata'] = $renovation_reserve_array;
		$data['content'] = $this->load->view('Reports/RenovationReserve', $data, true);
		$this->load->view('template_files/template_reports', $data);
	}


	public function renovation_reserve_pdf(){

		$loan_status = $this->input->post('loan_status');
		$cons_reserve = $this->input->post('cons_reserve');

		if($loan_status != ''){

			$condition = "ls.loan_status = '".$loan_status."'";
		}else{
			$condition = "ls.loan_status IN ('1','2','3','5')";
		}

		if($cons_reserve != ''){

			$condition1 = "AND l.draws = '".$cons_reserve."'";
		}else{
			$condition1 = "";
		}


		$fetchdraw = $this->User_model->query("SELECT lp.property_address, lp.unit, lp.city, lp.state, lp.zip, l.id as loan_id, l.talimar_loan,l.draws FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan JOIN loan_property as lp ON l.talimar_loan = lp.talimar_loan WHERE ".$condition." ".$condition1." ORDER BY l.id DESC");
		$fetchdraw = $fetchdraw->result();

		foreach ($fetchdraw as $value) {

			//for loan reserve 2...
			$fetchamountsum = $this->User_model->query("SELECT SUM(amount) as total FROM `loan_draw_schedule` WHERE `talimar_loan`='".$value->talimar_loan."'");
			if($fetchamountsum->num_rows() > 0){
				$fetchamountsum = $fetchamountsum->result();
				$totalAmount = $fetchamountsum[0]->total;
			}else{
				$totalAmount = 0;
			}

			$fetchamountreq = $this->User_model->query("SELECT SUM(`amount_Req`) as reqtotal FROM `loan_draw_released` WHERE `talimar_loan`='".$value->talimar_loan."'");
			if($fetchamountreq->num_rows() > 0){
				$fetchamountreq = $fetchamountreq->result();
				$reqAmount = $fetchamountreq[0]->reqtotal;
			}else{
				$reqAmount = 0;
			}


			//for loan reserve 1...
			$fetchbudgetsum = $this->User_model->query("SELECT SUM(`budget`) as totalbudget FROM `loan_reserve_draws` WHERE `talimar_loan`= '".$value->talimar_loan."'");
			if($fetchbudgetsum->num_rows() > 0){
				$fetchbudgetsum = $fetchbudgetsum->result();
				$totalBudget = $fetchbudgetsum[0]->totalbudget;
			}else{
				$totalBudget = 0;
			}

			$fetchdrawreq = $this->User_model->query("SELECT SUM(`date1_amount` + `date2_amount` + `date3_amount` + `date4_amount` + `date5_amount` + `date6_amount` + `date7_amount` + `date8_amount` + `date9_amount` + `date10_amount` + `date11_amount` + `date12_amount` + `date13_amount` + `date14_amount` + `date15_amount`) as drawreq FROM `loan_reserve_draws` WHERE `talimar_loan`='".$value->talimar_loan."'");
			if($fetchdrawreq->num_rows() > 0){
				$fetchdrawreq = $fetchdrawreq->result();
				$reqdrawreq = $fetchdrawreq[0]->drawreq;
			}else{
				$reqdrawreq = 0;
			}
			

			$renovation_reserve_array[] = array(
													"property_address" => $value->property_address,
													"unit" => $value->unit,
													"city" => $value->city,
													"state" => $value->state,
													"zip" => $value->zip,
													"draws" => $value->draws,
													"loan_id" => $value->loan_id,
													"draw1" => $totalBudget - $reqdrawreq,
													"draw2" => $totalAmount - $reqAmount,

												);

		}

		$renovation_reserve_arrayval = $renovation_reserve_array;

		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);

		// set default header data
		// $pdf->SetHeaderData("include/icons/logo-light.png", 30, '', '');

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(2, 20, 2);
		// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
		$pdf->SetFont('times', '', 7);

		// add a page
		$pdf->AddPage('L', 'A4');
		// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
		$header = '<h1 style="color:#003468;font-size:18px;"> Renovation Reserve</h1>';
		$header .= '<style>
                        .table {
                            width: 100%;
                            max-width: 100%;
                            margin-bottom: 20px;
                            border-collapse:collapse;
                        }
                        .table-bordered {
                        border: 1px solid #ddd;
                        }
                        table {
                            border-spacing: 0;
                            border-collapse: collapse;
                        }
                        .table td{
                            height : 25px;
                            font-size:12px;
                        }
                        tr.table_header th{
                            height:25px;
                            font-size:12px;
                            font-family: "Times New Roman", Georgia, Serif;
                            text-align:center;
                            background-color:#bfcfe3;
                        }

                        tr.table_bottom th{
                            height:20px;
                            font-size:12px;
                            font-family: "Times New Roman", Georgia, Serif;
                            text-align:left;
                            background-color:#bfcfe3;
                        }

                        tr.table_bottom td{
                            height:20px;
                            font-size:12px;
                            font-family: "Times New Roman", Georgia, Serif;
                            text-align:left;
                            background-color:#bfcfe3;
                        }
                        
                        tr.odd td{
                            background-color:#ededed;
                        }
                        </style>
                        ';

		$header .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';
		$header .= '<tr class="table_header">';

		$header .= '<th style="width:20%;text-decoration:underline;"><strong>Street Address</strong></th>';
		$header .= '<th style="width:10%;text-decoration:underline;"><strong>Unit #</strong></th>';
		$header .= '<th style="width:10%;text-decoration:underline;"><strong>City</strong></th>';
		$header .= '<th style="width:10%;text-decoration:underline;"><strong>State</strong></th>';
		$header .= '<th style="width:10%;text-decoration:underline;"><strong>Zip</strong></th>';
		$header .= '<th style="width:10%;text-decoration:underline;"><strong>Reserve Account</strong></th>';
		$header .= '<th style="width:15%;text-decoration:underline;"><strong>Loan Reserve 1 Balance</strong></th>';
		$header .= '<th style="width:15%;text-decoration:underline;"><strong>Loan Reserve 2 Balance</strong></th>';
		$header .= '</tr>';

		$nno_yes_option = $this->config->item('nno_yes_option');
		
		$num = 0;
		$amount1 = 0;
		$amount2 = 0;
			foreach ($renovation_reserve_arrayval as $key => $row) {

				if ($num % 2 == 0) {
					$class_add = "even";
				} else if ($num == 0) {
					$class_add = "even";
				} else {
					$class_add = "odd";
				}

				$amount1 += $row['draw1'];	
				$amount2 += $row['draw2'];	

				$header .= '<tr class="' . $class_add . '">';
				$header .= '<td>' . $row["property_address"] . '</td>';
				$header .= '<td>' . $row["unit"] . '</td>';
				$header .= '<td>' . $row["city"] . '</td>';
				$header .= '<td>' . $row["state"] . '</td>';
				$header .= '<td>' . $row["zip"] . '</td>';
				$header .= '<td>' . $nno_yes_option[$row["draws"]] . '</td>';
				$header .= '<td>$' . number_format($row['draw1']) . '</td>';
				$header .= '<td>$' . number_format($row['draw2']) . '</td>';
				
				$header .= '</tr>';
				$num++;
			}

		$header .= '<tr class="table_bottom">';
			$header .= "<td><strong>Total: ".$num."</strong></td>";
			$header .= "<td></td>";
			$header .= "<td></td>";
			$header .= "<td></td>";
			$header .= "<td></td>";
			$header .= "<td></td>";
			$header .= "<td><strong>$".number_format($amount1)."</strong></td>";
			$header .= "<td><strong>$".number_format($amount2)."</strong></td>";
		$header .= '</tr>';

		$header .= '</table>';

		$pdf->WriteHTML($header);

		$pdf->Output("Renovation_reserve.pdf", "D");
		
	}

	public function TDEmail(){

		$trust_deed = $this->input->post('trust_deed');

		$fetchtdEmail = $this->User_model->query("SELECT c.contact_firstname, c.contact_lastname, c.contact_id, c.contact_phone, c.contact_email FROM `contact_tags` as ct JOIN contact as c ON ct.contact_id = c.contact_id WHERE ct.`contact_email_blast`= '2' GROUP BY ct.`contact_id`");
		$fetchtdEmail = $fetchtdEmail->result();
		foreach ($fetchtdEmail as $value) {

			$fetchTDinvestor = $this->User_model->query("SELECT `contact_specialty` FROM `contact_tags` WHERE `contact_id` = '".$value->contact_id."' AND contact_specialty = '16'");
			if($fetchTDinvestor->num_rows() > 0){
				$TDinvertor = 'Yes';
			}else{
				$TDinvertor = 'No';
			}

			if($trust_deed == '1'){

				if($TDinvertor == 'Yes'){

					$TDEmailData[] = array(	
										"name" 			=> $value->contact_firstname.' '.$value->contact_lastname,
										"phone" 		=> $value->contact_phone,
										"email" 		=> $value->contact_email,
										"id"			=> $value->contact_id,
										"TDinvertor"	=> $TDinvertor,
									);
				}
			}elseif($trust_deed == '2'){

				if($TDinvertor == 'No'){

					$TDEmailData[] = array(	
										"name" 			=> $value->contact_firstname.' '.$value->contact_lastname,
										"phone" 		=> $value->contact_phone,
										"email" 		=> $value->contact_email,
										"id"			=> $value->contact_id,
										"TDinvertor"	=> $TDinvertor,
									);
				}
			}else{

					$TDEmailData[] = array(	
										"name" 			=> $value->contact_firstname.' '.$value->contact_lastname,
										"phone" 		=> $value->contact_phone,
										"email" 		=> $value->contact_email,
										"id"			=> $value->contact_id,
										"TDinvertor"	=> $TDinvertor,
									);
			}
			
		}

		$data['trust_deed'] = $trust_deed;
		$data['TDEmailData'] = $TDEmailData;
		$data['content'] = $this->load->view('Reports/TDEmail', $data, true);
		$this->load->view('template_files/template_reports', $data);
	}


	public function TDEmail_pdf(){

		$trust_deed = $this->input->post('trust_deeds');

		$fetchtdEmail = $this->User_model->query("SELECT c.contact_firstname, c.contact_lastname, c.contact_id, c.contact_phone, c.contact_email FROM `contact_tags` as ct JOIN contact as c ON ct.contact_id = c.contact_id WHERE ct.`contact_email_blast`= '2' GROUP BY ct.`contact_id`");
		$fetchtdEmail = $fetchtdEmail->result();
		foreach ($fetchtdEmail as $value) {

			$fetchTDinvestor = $this->User_model->query("SELECT `contact_specialty` FROM `contact_tags` WHERE `contact_id` = '".$value->contact_id."' AND contact_specialty = '16'");
			if($fetchTDinvestor->num_rows() > 0){
				$TDinvertor = 'Yes';
			}else{
				$TDinvertor = 'No';
			}


			if($trust_deed == '1'){

				if($TDinvertor == 'Yes'){

					$TDEmailData[] = array(	
										"name" 			=> $value->contact_firstname.' '.$value->contact_lastname,
										"phone" 		=> $value->contact_phone,
										"email" 		=> $value->contact_email,
										"id"			=> $value->contact_id,
										"TDinvertor"	=> $TDinvertor,
									);
				}
			}elseif($trust_deed == '2'){

				if($TDinvertor == 'No'){

					$TDEmailData[] = array(	
										"name" 			=> $value->contact_firstname.' '.$value->contact_lastname,
										"phone" 		=> $value->contact_phone,
										"email" 		=> $value->contact_email,
										"id"			=> $value->contact_id,
										"TDinvertor"	=> $TDinvertor,
									);
				}
			}else{

					$TDEmailData[] = array(	
										"name" 			=> $value->contact_firstname.' '.$value->contact_lastname,
										"phone" 		=> $value->contact_phone,
										"email" 		=> $value->contact_email,
										"id"			=> $value->contact_id,
										"TDinvertor"	=> $TDinvertor,
									);
			}
		}


		$TDEmailcheckData = $TDEmailData;

		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);

		// set default header data
		// $pdf->SetHeaderData("include/icons/logo-light.png", 30, '', '');

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(2, 20, 2);
		// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
		$pdf->SetFont('times', '', 7);

		// add a page
		$pdf->AddPage('L', 'A4');
		// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
		$header = '<h1 style="color:#003468;font-size:18px;"> TD E-Mail Check</h1>';
		$header .= '<style>
                        .table {
                            width: 100%;
                            max-width: 100%;
                            margin-bottom: 20px;
                            border-collapse:collapse;
                        }
                        .table-bordered {
                        border: 1px solid #ddd;
                        }
                        table {
                            border-spacing: 0;
                            border-collapse: collapse;
                        }
                        .table td{
                            height : 25px;
                            font-size:12px;
                        }
                        tr.table_header th{
                            height:25px;
                            font-size:12px;
                            font-family: "Times New Roman", Georgia, Serif;
                            text-align:center;
                            background-color:#bfcfe3;
                        }

                        tr.table_bottom th{
                            height:20px;
                            font-size:12px;
                            font-family: "Times New Roman", Georgia, Serif;
                            text-align:left;
                            background-color:#bfcfe3;
                        }

                        tr.table_bottom td{
                            height:20px;
                            font-size:12px;
                            font-family: "Times New Roman", Georgia, Serif;
                            text-align:left;
                            background-color:#bfcfe3;
                        }
                        
                        tr.odd td{
                            background-color:#ededed;
                        }
                        </style>
                        ';

		$header .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';
		$header .= '<tr class="table_header">';

		$header .= '<th style="width:40%;text-decoration:underline;"><strong>Name</strong></th>';
		$header .= '<th style="width:20%;text-decoration:underline;"><strong>Phone</strong></th>';
		$header .= '<th style="width:20%;text-decoration:underline;"><strong>E-Mail</strong></th>';
		$header .= '<th style="width:20%;text-decoration:underline;"><strong>Trust Deed Investor</strong></th>';
		$header .= '</tr>';
		
		$num = 0;
		
		if(isset($TDEmailcheckData) && is_array($TDEmailcheckData)){
			foreach ($TDEmailcheckData as $key => $row) {

				if ($num % 2 == 0) {
					$class_add = "even";
				} else if ($num == 0) {
					$class_add = "even";
				} else {
					$class_add = "odd";
				}

				$header .= '<tr class="' . $class_add . '">';
				$header .= '<td>' . $row["name"] . '</td>';
				$header .= '<td>' . $row["phone"] . '</td>';
				$header .= '<td>' . $row["email"] . '</td>';
				$header .= '<td>' . $row["TDinvertor"] . '</td>';
				
				$header .= '</tr>';
				$num++;
			}
		}else{

			$header .= '<tr>';
			$header .= '<td>No data found!</td>';
			$header .= '</tr>';
		}

		$header .= '<tr class="table_bottom">';
			$header .= "<td><strong>Total: ".$num."</strong></td>";
			$header .= "<td></td>";
			$header .= "<td></td>";
			$header .= "<td></td>";
		$header .= '</tr>';

		$header .= '</table>';

		$pdf->WriteHTML($header);

		$pdf->Output("TDEmailcheck.pdf", "D");
		
	}


	
	public function get_contact_funds($contactID){

		$check_contact_funds = $this->User_model->query("SELECT `committed_funds` FROM `lender_contact_type` WHERE contact_id='".$contactID."'");
		if($check_contact_funds->num_rows() > 0){
			$check_contact_funds = $check_contact_funds->result();
			$check_contact_funds = $check_contact_funds[0]->committed_funds;
		}else{
			$check_contact_funds = '';
		}

		return $check_contact_funds;	
	}

	public function get_contact_addedDate($contactID){

		$check_user = $this->User_model->query("SELECT `date` FROM user_added_contact WHERE contact_id='".$contactID."'");
		if($check_user->num_rows() > 0){
			$check_user_val = $check_user->result();
			$contact_re_date = date('m-d-Y', strtotime($check_user_val[0]->date));
		}else{
			$contact_re_date = '';
		}

		return $contact_re_date;	
	}

	public function get_contact_accounts($contactID){

		$fetchaccount = $this->User_model->query("SELECT Count(*) as totallender FROM lender_contact WHERE contact_id = '".$contactID."' AND lender_id !='0'");
		if($fetchaccount->num_rows() > 0){
			$fetchaccount = $fetchaccount->result();
			$activeaccount = $fetchaccount[0]->totallender;
		}else{

			$activeaccount = 0;
		}

		return $activeaccount;
	}

	public function get_contact_totalLoans($contactID){


		/*$fetchaccount = $this->User_model->query("select COUNT(*) as total_loans, SUM(la.investment) as total_investment 
		from loan_assigment as la 
		JOIN loan_servicing as ls ON la.talimar_loan = ls.talimar_loan 
		JOIN lender_contact as lc ON la.lender_name = lc.lender_id 
		where ls.loan_status = '2' AND lc.contact_id = '$contactID'");
		$fetchaccount = $fetchaccount->row();

		$fetchaccountAll = $this->User_model->query("select COUNT(*) as total_loans, SUM(la.investment) as total_investment 
		from loan_assigment as la 
		JOIN loan_servicing as ls ON la.talimar_loan = ls.talimar_loan 
		JOIN lender_contact as lc ON la.lender_name = lc.lender_id 
		where ls.loan_status IN ('2','3') AND lc.contact_id = '$contactID'");
		$fetchaccountAll = $fetchaccountAll->row();*/



		$fetchaccount = $this->User_model->query("SELECT lender_id FROM lender_contact WHERE contact_id = '".$contactID."' AND lender_id !='0'");
		if($fetchaccount->num_rows() > 0){
			$fetchaccount = $fetchaccount->result();

			foreach ($fetchaccount as $value) {
				
				$fetchloans = $this->User_model->query("select COUNT(*) as total_loans, SUM(investment) as total_investment from loan_assigment as la JOIN loan_servicing as ls ON la.talimar_loan = ls.talimar_loan where ls.loan_status = '2' AND la.lender_name = '".$value->lender_id."'");
				$fetchloans = $fetchloans->result();

				$countActive[] = $fetchloans[0]->total_loans;
				$AmountActive[] = $fetchloans[0]->total_investment;

				//for total
				$fetchloans11 = $this->User_model->query("select COUNT(*) as total_loans1, SUM(investment) as total_investment1 from loan_assigment as la JOIN loan_servicing as ls ON la.talimar_loan = ls.talimar_loan where ls.loan_status IN ('2','3') AND la.lender_name = '".$value->lender_id."'");
				$fetchloans11 = $fetchloans11->result();
				
				$countTotal[] = $fetchloans11[0]->total_loans1;
				$AmountTotal[] = $fetchloans11[0]->total_investment1;
			}
		}

		if(!empty($countTotal) && is_array($countTotal)){
			$allvalue['countTotal'] = array_sum($countTotal);
		}else{
			$allvalue['countTotal'] = 0;
		}
		if(!empty($AmountTotal) && is_array($AmountTotal)){
			$allvalue['AmountTotal'] = array_sum($AmountTotal);
		}else{
			$allvalue['AmountTotal'] = 0;
		}

		if(!empty($countActive)){
			$allvalue['countActive'] = $countActive;
		}else{
			$allvalue['countActive'] = 0;
		}
		if(!empty($AmountActive)){
			$allvalue['AmountActive'] = $AmountActive;
		}else{
			$allvalue['AmountActive'] = 0;
		}
		

		return $allvalue;
	}
	public function active_loan_acount($contactID){
		$fetchaccount = $this->User_model->query("select COUNT(*) as total_loans, SUM(la.investment) as total_investment 
		from loan_assigment as la 
		JOIN loan_servicing as ls ON la.talimar_loan = ls.talimar_loan 
		JOIN lender_contact as lc ON la.lender_name = lc.lender_id 
		where ls.loan_status = '2' AND lc.contact_id = '$contactID'");
		$fetchloans = $fetchaccount->row();
		$countActive = $fetchloans->total_loans;
		$AmountActive = $fetchloans->total_investment;
		if(!empty($countActive)){
			$allvalue['countActive'] = $countActive;
		}else{
			$allvalue['countActive'] = 0;
		}
		if(!empty($AmountActive) ){
			$allvalue['AmountActive'] = $AmountActive;
		}else{
			$allvalue['AmountActive'] = 0;
		}
		return $allvalue;
	}
	public function total_loan_acount($contactID){
		$fetchaccount = $this->User_model->query("select COUNT(*) as total_loans, SUM(la.investment) as total_investment 
		from loan_assigment as la 
		JOIN loan_servicing as ls ON la.talimar_loan = ls.talimar_loan 
		JOIN lender_contact as lc ON la.lender_name = lc.lender_id 
		where ls.loan_status IN ('1','2','3') AND lc.contact_id = '$contactID' ");
		$fetchloans = $fetchaccount->row();
		$countTotal = $fetchloans->total_loans;
		$AmountTotal = $fetchloans->total_investment;
		if(!empty($countTotal)){
			$allvalue['countTotal'] = $countTotal;
		}else{
			$allvalue['countTotal'] = 0;
		}
		if(!empty($AmountTotal)){
			$allvalue['AmountTotal'] = $AmountTotal;
		}else{
			$allvalue['AmountTotal'] = 0;
		}
		return $allvalue;
	}
	public function pdf_excel_file(){
		$ActiveAccount="";
		$FundedTrustDeed="";
		$ActiveTrustDeed="";
		$lender_portal="";
		$WholeInvestor="";
		$reportDisplay=$this->input->post('reportDisplay');
		if(!empty($_POST)){
			$ActiveAccount="";
			$FundedTrustDeed 		= $this->input->post('FundedTrustDeed');		
			$ActiveTrustDeed 		= $this->input->post('ActiveTrustDeed');
			$WholeInvestor 			= $this->input->post('WholeInvestor');
			$lender_portal 			= $this->input->post('lender_portal');
		}
		$orderby = 'c.contact_id';
		$order = 'ASC';
		$whereCount=0;
		$havingWhere="";
		$ActiveAccountWhere="";
		if($ActiveAccount == 1)
		{
			$whereCount++;
			$ActiveAccountWhere=" Having totallender > 0 ";
		}else if($ActiveAccount == 2){
			$whereCount++;
			$ActiveAccountWhere=" Having totallender <= 0 ";
		}
		$havingWhere.=$ActiveAccountWhere;
		$FundedTrustDeedStatusWhere="";
		$FundedTrustDeedStatusHaving="";
		if($FundedTrustDeed == 1){
			$whereCount++;
			$FundedTrustDeedStatusHaving=" countTotal > 0 ";
			$FundedTrustDeedStatusWhere= " and ls.loan_status IN ('2','3') ";
		}else if($FundedTrustDeed == 2){
			$whereCount++;
			$FundedTrustDeedStatusHaving=" countTotal <= 0 ";
			// $FundedTrustDeedStatusWhere= " OR ls.loan_status IN ('2','3') ";
		}
		if(!empty($havingWhere) && !empty($FundedTrustDeedStatusHaving)){
			$havingWhere.=" OR ";
		}else if(empty($havingWhere) && !empty($FundedTrustDeedStatusHaving)){
			$havingWhere.="Having";
		}
		$havingWhere.=$FundedTrustDeedStatusHaving;
		$ActiveTrustDeedWhere="";
		$ActiveTrustDeedHaving="";
		if($ActiveTrustDeed == 1){
			$whereCount++;
			$ActiveTrustDeedHaving="  countTotal > 0 ";
			$ActiveTrustDeedWhere= " and ls.loan_status = '2' ";
		}elseif($ActiveTrustDeed == 2){
			$whereCount++;
			$ActiveTrustDeedHaving="  countTotal <= 0 ";
			// $ActiveTrustDeedWhere= " OR ls.loan_status = '2' ";
		}
		if(!empty($havingWhere) && !empty($ActiveTrustDeedHaving)){
			$havingWhere.=" OR ";
		}else if(empty($havingWhere) && !empty($ActiveTrustDeedHaving)){
			$havingWhere.=" Having ";
		}
		$havingWhere.=$ActiveTrustDeedHaving;
		$where="";
		if($WholeInvestor || $lender_portal != ''){
			if($WholeInvestor == '1'){
				$newcondition = "AND lct.WholeNoteInvestor = '1'";
			}elseif($WholeInvestor == '2'){
				$newcondition = "AND lct.WholeNoteInvestor != '1'";
			}else{
				$newcondition = '';
			}
			if($lender_portal == '1'){
				$allow_access = "AND lct.allow_access = '1'";
			}elseif($lender_portal == '2'){
				$allow_access = "AND lct.allow_access = '0'";
			}else{
				$allow_access = '';
			}
			if(!empty($request['ActiveAccount']))
			{
				$ActiveAccount = $request['ActiveAccount'];
				$where = " AND lct.account_status = '".$ActiveAccount."'";
			}
			$trust_deedCondition = "ct.contact_specialty = '16'";
			$getContact = 	$this->User_model->query("
													SELECT 	
															count(lc.contact_id) as totallender,
															count(la.lender_name) as countTotal,
															sum(la.investment) as AmountTotal,
															c.contact_id, 
															c.contact_firstname, 
															c.contact_lastname, 
															c.contact_phone, 
															c.contact_email 
													FROM contact as c JOIN contact_tags as ct ON c.contact_id = ct.contact_id 
													LEFT JOIN lender_contact_type as lct ON c.contact_id  = lct.contact_id
													LEFT JOIN lender_contact as lc ON c.contact_id  = lc.contact_id
													LEFT JOIN loan_assigment as la ON lc.lender_id  = la.lender_name
													LEFT JOIN loan_servicing as ls ON la.talimar_loan  = ls.talimar_loan
													WHERE ".$trust_deedCondition." ".$newcondition." ".$allow_access." ".$where." ".$FundedTrustDeedStatusWhere." ".$ActiveTrustDeedWhere."
													GROUP BY contact_id 
													".$havingWhere." 
													ORDER BY $orderby $order 
												");
		}else{
			$trust_deedCondition = "ct.contact_specialty = '16'";
			$getContact= $this->User_model->query("
													SELECT 	
															count(lc.contact_id) as totallender,
															count(la.lender_name) as countTotal,
															sum(la.investment) as AmountTotal,
															c.contact_id, 
															c.contact_firstname, 
															c.contact_lastname, 
															c.contact_phone, 
															c.contact_email 
													FROM contact as c JOIN contact_tags as ct ON c.contact_id = ct.contact_id 
													LEFT JOIN lender_contact as lc ON c.contact_id  = lc.contact_id
													LEFT JOIN loan_assigment as la ON lc.lender_id  = la.lender_name
													LEFT JOIN loan_servicing as ls ON la.talimar_loan  = ls.talimar_loan
													WHERE ".$trust_deedCondition." ".$FundedTrustDeedStatusWhere." ".$ActiveTrustDeedWhere."
													GROUP BY contact_id 
													".$havingWhere." 
													ORDER BY $orderby $order
												");
		}
		$getContact = $getContact->result();
		if($reportDisplay == 'excel'){
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename="lender_filter.csv";');
			$f = fopen("php://output", "w");
			fputcsv($f, array('Name', '# of Accounts', '# of Total Loans', '$ of Total Loans', '# of Active Loans', '$ of Active Loans', 'Committed Capital', 'Available Capital', 'Date Added', 'Phone', 'E-Mail')); // title...
			foreach ($getContact as $row) {
				$contact_funds = $this->get_contact_funds($row->contact_id);
				if(empty($contact_funds) || $contact_funds<0){
					$contact_funds=0;
				}
				$output['contact_firstname'] = $row->contact_firstname . ' ' . $row->contact_lastname;
				$output['contact_accounts'] = $row->totallender;
				$output['countTotal'] = number_format($row->countTotal);
				$output['AmountTotal'] = number_format($row->AmountTotal);
				$output['countActive'] = $row->countTotal;
				$output['AmountActive'] = number_format($row->AmountTotal);
				$output['contact_funds'] = number_format($contact_funds);
				$output['avaliable'] = number_format($contact_funds - $row->AmountTotal);
				$output['contact_phone'] = $row->contact_phone;
				$output['contact_email'] = $row->contact_email;
				fputcsv($f, $output);
			}
			fclose($f);
			exit();

		}
		elseif($reportDisplay == 'pdf'){
			echo "in else if";
			
			$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
			$pdf->SetMargins(2, 20, 2);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
			$pdf->SetFont('times', '', 7);
			$pdf->AddPage('L', 'A4');

			$header = '<h1 style="color:#003468;font-size:18px;"> Lender Filter</h1>';
			$header .= '<style>
	                        .table {
	                            width: 100%;
	                            max-width: 100%;
	                            margin-bottom: 20px;
	                            border-collapse:collapse;
	                        }
	                        .table-bordered {
	                        border: 1px solid #ddd;
	                        }
	                        table {
	                            border-spacing: 0;
	                            border-collapse: collapse;
	                        }
	                        .table td{
	                            height : 25px;
	                            font-size:12px;
	                        }
	                        tr.table_header th{
	                            height:25px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:center;
	                            background-color:#bfcfe3;
	                        }

	                        tr.table_bottom th{
	                            height:20px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:left;
	                            background-color:#bfcfe3;
	                        }
	                        tr.odd td{
	                            background-color:#ededed;
	                        }
	                        </style>
	                        ';

			$header .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';
			$header .= '<tr class="table_header">';

			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Name</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong># of<br>Accounts</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong># of<br>Total Loans</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong>$ of<br>Total Loans</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong># of<br>Active Loans</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong>$ of<br>Active Loans</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong>Committed<br>Capital</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong>Available<br>Capital</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong>Phone</strong></th>';
			$header .= '<th style="width:18%;text-decoration:underline;"><strong>E-Mail</strong></th>';
			$header .= '</tr>';
			$number = 0;
			$total_account = 0;
			$hash_total_loan = 0;
			$amount_total_loan = 0;
			$hash_active_loan = 0;
			$commited = 0;
			$avaliable = 0;	
			set_time_limit(0);		
			foreach ($getContact as $row) {
				$contact_funds = $this->get_contact_funds($row->contact_id);
				if(empty($contact_funds) || $contact_funds<0){
					$contact_funds=0;
				}
				$total_account += $row->totallender;
				$hash_total_loan += $row->countTotal;
				$amount_total_loan += $row->AmountTotal;
				$hash_active_loan += $row->countTotal;
				$commited += $contact_funds;
				$avaliable += ($contact_funds - $row->AmountTotal);
				if ($number % 2 == 0) {
					$class_add = "even";
				} else if ($number == 0) {
					$class_add = "even";
				} else {
					$class_add = "odd";
				}
				$header .= '<tr class="' . $class_add . '">';
				$header .= '<td>' . $row->contact_firstname . ' ' . $row->contact_lastname . '</td>';
				$header .= '<td>' . $row->totallender . '</td>';
				$header .= '<td>' . number_format($row->countTotal) . '</td>';
				$header .= '<td>$' . number_format($row->AmountTotal) . '</td>';
				$header .= '<td>' . $row->countTotal . '</td>';
				$header .= '<td>$' . number_format($row->AmountTotal) . '</td>';
				$header .= '<td>$' . number_format($contact_funds) . '</td>';
				$header .= '<td>$' . number_format($contact_funds - $row->AmountTotal) . '</td>';
				$header .= '<td>' . $row->contact_phone . '</td>';
				$header .= '<td>' . $row->contact_email . '</td>';
				$header .= '</tr>';
				

				if($number%500==0){
					sleep(1);
					continue;
				}
				if($number>1000){
					echo "row no".$number;
					die();
				}
				$number++;
			}
			die();
			$header .= '<tr class="table_bottom">';
			$header .= '<th><strong>Total: ' . $number . '</strong></th>';
			$header .= '<th><strong>' . number_format($total_account) . '</strong></th>';
			$header .= '<th><strong>' . number_format($hash_total_loan) . '</strong></th>';
			$header .= '<th><strong>' . '$' . number_format($amount_total_loan) . '</strong></th>';
			$header .= '<th><strong>' . $hash_active_loan . '</strong></th>';
			$header .= '<th></th>';
			$header .= '<th><strong>' . '$' . number_format($commited) . '</strong></th>';
			$header .= '<th><strong>' . '$' . number_format($avaliable) . '</strong></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '</tr>';
			$header .= '</table>';
			$pdf->WriteHTML($header);
			$pdf->Output("lender_filter.pdf", "D");
		}
	}
	public function ajax_lender_filters(){
		$request='';
		$search_text='';
		$startfrom=0;
		$queryLength=0;
		if(!empty($_POST)){
			$ActiveAccount="";
			$FundedTrustDeed 		= $this->input->post('FundedTrustDeed');		
			$ActiveTrustDeed 		= $this->input->post('ActiveTrustDeed');
			$WholeInvestor 			= $this->input->post('WholeInvestor');
			$lender_portal 			= $this->input->post('lender_portal');
			$reportDisplay=$this->input->post('reportDisplay');
		}
		else if(!empty($_GET)){
			$request= $_GET;
			$ActiveAccount="";
			$FundedTrustDeed 		= $request['FundedTrustDeed'];		
			$ActiveTrustDeed 		= $request['ActiveTrustDeed'];
			$WholeInvestor 			= $request['WholeInvestor'];
			$lender_portal 			= $request['lender_portal'];
			$calling_type       	= $request['calling_type'];
			$search_text        	= $request['search_text'];
		}else{
			$ActiveAccount="";
			$FundedTrustDeed="";
			$ActiveTrustDeed="";
			$calling_type="";
			$WholeInvestor="";
		}
		$columns = array(
			0  => 'c.contact_firstname',
			1  => 'totallender',	
			2  => 'countTotal',
			3  => 'AmountTotal',
			4  => 'countTotal',
			5  => 'AmountTotal',
			6  => 'c.comment',
			7  => 'c.contact_funds',
			8  => 'c.contact_phone',
			9  => 'c.contact_email'
		);
		$data['ActiveAccount'] = '';
		$where = '';
		$return_json = array();
		$rowRequests = array();
		/*Short*/
		$orderby = 'c.contact_firstname';
		$order = 'ASC';
		if(!empty($request)){
			$startfrom = $request['start'];
			$queryLength = $request['length'];
			if(isset($request['order'])){
				if($request['order'][0]['column']){
					$orderby = $columns[$request['order'][0]['column']];
				}
				if($request['order'][0]['dir']){
					$order = $request['order'][0]['dir'];
				}
			}
		}
		$total_limit=0;
		$total_offset=0;
		$limit_offset_str="";
		if(!empty($request['total_limit'])){
			$total_limit = $request['total_limit'];
			$limit_offset_str.=" LIMIT ".$total_limit;
		}
		if(!empty($request['total_offset'])){
			$total_offset = $request['total_offset'];
			$limit_offset_str.=" OFFSET ".$total_offset;
		}
		if(!empty($orderby) && $orderby=="c.contact_firstname"){
			$orderby=" TRIM(c.contact_lastname) ".$order.", TRIM(c.contact_firstname) ".$order." ";
			$order="";
		}
		/*Short End*/
		/*Search*/
		$searchValue = '';
		if(!empty($request)){			
			if( isset($request['search']) ){
				if($request['search']['value']){
					$search = trim( $request['search']['value'] );
					$searchValue = " AND (c.contact_firstname LIKE '%$search%' OR c.contact_lastname LIKE '%$search%' OR c.contact_phone LIKE '%$search%' OR c.contact_email LIKE '%$search%') ";
				}
			}
		}
		if(!empty($search_text)){
			if($searchValue == ''){
				$searchValue = " AND (c.contact_firstname LIKE '%$search_text%' OR c.contact_lastname LIKE '%$search_text%' OR c.contact_phone LIKE '%$search_text%' OR c.contact_email LIKE '%$search_text%') ";			
			}
		}
		/*Search End*/
		/*Filters*/
		$havingWhere="";
		$FilterWhere="";
		$FundedTrustDeedStatusHaving="";
		if($FundedTrustDeed == 1){
			$havingWhere=" having countTotal > 0 ";
			$FilterWhere= " and ls.loan_status IN ('1','2','3') ";
		}else if($FundedTrustDeed == 2){
			$havingWhere=" having countTotal <= 0 ";
			$FilterWhere= " and ls.loan_status IN ('1','2','3') ";
		}
		if($ActiveTrustDeed == 1){
			$havingWhere="  having countTotal > 0 ";
			$FilterWhere= " and ls.loan_status = '2' ";
		}elseif($ActiveTrustDeed == 2){
			$havingWhere="  having countTotal <= 0 ";
			 $FilterWhere= " and ls.loan_status = '2' ";
		}
		/*Filters End*/
		if($WholeInvestor || $lender_portal != '')
		{
			if($WholeInvestor == '1'){
				$newcondition = "AND lct.WholeNoteInvestor = '1'";
			}elseif($WholeInvestor == '2'){
				$newcondition = "AND lct.WholeNoteInvestor != '1'";
			}else{
				$newcondition = '';
			}
			if($lender_portal == '1'){
				$allow_access = "AND lct.allow_access = '1'";
			}elseif($lender_portal == '2'){
				$allow_access = "AND lct.allow_access = '0'";
			}else{
				$allow_access = '';
			}
			if(!empty($request['ActiveAccount']))
			{
				$ActiveAccount = $request['ActiveAccount'];
				$where = " AND lct.account_status = '".$ActiveAccount."'";
			}
			$trust_deedCondition = "ct.contact_specialty = '16'";
			$getContact 		= 	$this->User_model->query("
															SELECT 	
																SQL_CALC_FOUND_ROWS count(la.lender_name) as countTotal,
																sum(la.investment) as AmountTotal,
																c.contact_id, 
																c.contact_firstname, 
																c.contact_lastname, 
																c.contact_phone, 
																c.contact_email 
													FROM contact as c JOIN contact_tags as ct ON c.contact_id = ct.contact_id 
													LEFT JOIN lender_contact_type as lct ON c.contact_id  = lct.contact_id
													LEFT JOIN lender_contact as lc ON ct.contact_id  = lc.contact_id
													LEFT JOIN loan_assigment as la ON lc.lender_id  = la.lender_name
													LEFT JOIN loan_servicing as ls ON la.talimar_loan  = ls.talimar_loan
													WHERE ".$trust_deedCondition." ".$newcondition." ".$allow_access." ".$where."  ".$FilterWhere." ".$searchValue." 
													GROUP BY contact_id 
													".$havingWhere." 
													ORDER BY $orderby $order 
													 LIMIT $startfrom, $queryLength
															");
			$total_loan_filters ="SELECT  count(la.lender_name) as countTotal,sum(la.investment) as AmountTotal,c.contact_id,c.contact_firstname,c.contact_lastname,c.contact_phone,c.contact_email FROM contact as c JOIN contact_tags as ct ON c.contact_id = ct.contact_id  LEFT JOIN lender_contact_type as lct ON c.contact_id  = lct.contact_id LEFT JOIN lender_contact as lc ON ct.contact_id  = lc.contact_id LEFT JOIN loan_assigment as la ON lc.lender_id  = la.lender_name LEFT JOIN loan_servicing as ls ON la.talimar_loan  = ls.talimar_loan WHERE ".$trust_deedCondition." ".$newcondition." ".$allow_access." ".$where." ".$FilterWhere."  ".$searchValue."  GROUP BY contact_id  ".$havingWhere."  ORDER BY $orderby $order $limit_offset_str ";
		}else{
			$trust_deedCondition = "ct.contact_specialty = '16'  ";
			
			$query=" SELECT 	SQL_CALC_FOUND_ROWS count(lc.*) as totallender,count(la.lender_name) as countTotal,sum(la.investment) as AmountTotal,c.contact_id,c.contact_firstname,c.contact_lastname,c.contact_phone,c.contact_email FROM contact as c JOIN contact_tags as ct ON c.contact_id = ct.contact_id LEFT JOIN lender_contact_type as lct ON c.contact_id  = lct.contact_id LEFT JOIN lender_contact as lc ON c.contact_id  = lc.contact_id LEFT JOIN loan_assigment as la ON lc.lender_id  = la.lender_name LEFT JOIN loan_servicing as ls ON la.talimar_loan  = ls.talimar_loan WHERE ".$trust_deedCondition." ".$FilterWhere." ".$searchValue."  GROUP BY contact_id  ".$havingWhere."  ORDER BY $orderby $order   LIMIT $startfrom, $queryLength ";
			$getContact= $this->User_model->query(" SELECT 	SQL_CALC_FOUND_ROWS 
															count(la.lender_name) as countTotal,
															sum(la.investment) as AmountTotal,
															c.contact_id, 
															c.contact_firstname, 
															c.contact_lastname, 
															c.contact_phone, 
															c.contact_email 
													FROM contact as c JOIN contact_tags as ct ON c.contact_id = ct.contact_id 
													/*LEFT JOIN lender_contact_type as lct ON c.contact_id  = lct.contact_id*/
													LEFT JOIN lender_contact as lc ON ct.contact_id  = lc.contact_id
													LEFT JOIN loan_assigment as la ON lc.lender_id  = la.lender_name
													LEFT JOIN loan_servicing as ls ON la.talimar_loan  = ls.talimar_loan
													WHERE ".$trust_deedCondition." ".$FilterWhere." ".$searchValue." 
													GROUP BY contact_id 
													".$havingWhere." 
													ORDER BY $orderby $order 
													 LIMIT $startfrom, $queryLength
												");
			$total_loan_filters= "SELECT count(la.lender_name) as countTotal,sum(la.investment) as AmountTotal,c.contact_id,c.contact_firstname,c.contact_lastname,c.contact_phone,c.contact_email FROM contact as c JOIN contact_tags as ct ON c.contact_id = ct.contact_id  LEFT JOIN lender_contact as lc ON ct.contact_id  = lc.contact_id LEFT JOIN loan_assigment as la ON lc.lender_id  = la.lender_name LEFT JOIN loan_servicing as ls ON la.talimar_loan  = ls.talimar_loan WHERE ".$trust_deedCondition."  ".$FilterWhere." ".$searchValue."  GROUP BY contact_id  ".$havingWhere."  ORDER BY $orderby $order $limit_offset_str ";
		}
		if(!empty($_POST)){
				$total_loan_filters=$this->User_model->query($total_loan_filters);
				$getContact = $total_loan_filters->result();

				if($reportDisplay == 'excel'){
					header('Content-Type: text/csv; charset=utf-8');
					header('Content-Disposition: attachment; filename="lender_filter.csv";');
					$f = fopen("php://output", "w");
					fputcsv($f, array('Name', '# of Accounts', '# of Total Loans', '$ of Total Loans', '# of Active Loans', '$ of Active Loans', 'Committed Capital', 'Available Capital', 'Date Added', 'Phone', 'E-Mail')); // title...
					foreach ($getContact as $row) {
						
						$contact_accounts 	=  $this->get_contact_accounts($row->contact_id);
						$total_loan_array=$this->total_loan_acount($row->contact_id);
						$total_active_loan_array=$this->active_loan_acount($row->contact_id);						
						$countTotal=$total_loan_array['countTotal'];
						$AmountTotal=$total_loan_array['AmountTotal'];
						$countActive=$total_active_loan_array['countActive'];
						$AmountActive=$total_active_loan_array['AmountActive'];
						$contact_funds = $this->get_contact_investment($row->contact_id);
						if(empty($contact_funds) || $contact_funds<0){
							$contact_funds=0;
						}
						$output['contact_firstname'] =  $row->contact_firstname. ' ' .$row->contact_lastname;
						$output['contact_accounts'] = $contact_accounts;
						$output['countTotal'] = $countTotal;
						$output['AmountTotal'] = number_format($AmountTotal,2);
						$output['countActive'] = $countActive;
						$output['AmountActive'] = number_format($AmountActive,2);
						$output['contact_funds'] = number_format($contact_funds,2);
						$output['avaliable'] = number_format(($contact_funds - $AmountActive),2);
						$output['contact_phone'] = $row->contact_phone;
						$output['contact_email'] = $row->contact_email;
						fputcsv($f, $output);
					}
					fclose($f);
					exit();

				}
				elseif($reportDisplay == 'pdf'){
					$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
					$pdf->SetCreator(PDF_CREATOR);
					$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
					$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
					$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
					$pdf->SetMargins(2, 20, 2);
					$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
					$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
					$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
					$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
					$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
					$pdf->SetFont('times', '', 7);
					$pdf->AddPage('L', 'A4');
					$header = '<h1 style="color:#003468;font-size:18px;"> Lender Filter</h1>';
					$header .= '<style>
			                        .table {
			                            width: 100%;
			                            max-width: 100%;
			                            margin-bottom: 20px;
			                            border-collapse:collapse;
			                        }
			                        .table-bordered {
			                        border: 1px solid #ddd;
			                        }
			                        table {
			                            border-spacing: 0;
			                            border-collapse: collapse;
			                        }
			                        .table td{
			                            height : 25px;
			                            font-size:12px;
			                        }
			                        tr.table_header th{
			                            height:25px;
			                            font-size:12px;
			                            font-family: "Times New Roman", Georgia, Serif;
			                            text-align:center;
			                            background-color:#bfcfe3;
			                        }

			                        tr.table_bottom th{
			                            height:20px;
			                            font-size:12px;
			                            font-family: "Times New Roman", Georgia, Serif;
			                            text-align:left;
			                            background-color:#bfcfe3;
			                        }
			                        tr.odd td{
			                            background-color:#ededed;
			                        }
			                        </style>
			                        ';

					$header .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';
					$header .= '<tr class="table_header">';

					$header .= '<th style="width:10%;text-decoration:underline;"><strong>Name</strong></th>';
					$header .= '<th style="width:8%;text-decoration:underline;"><strong># of<br>Accounts</strong></th>';
					$header .= '<th style="width:8%;text-decoration:underline;"><strong># of<br>Total Loans</strong></th>';
					$header .= '<th style="width:8%;text-decoration:underline;"><strong>$ of<br>Total Loans</strong></th>';
					$header .= '<th style="width:8%;text-decoration:underline;"><strong># of<br>Active Loans</strong></th>';
					$header .= '<th style="width:8%;text-decoration:underline;"><strong>$ of<br>Active Loans</strong></th>';
					$header .= '<th style="width:8%;text-decoration:underline;"><strong>Committed<br>Capital</strong></th>';
					$header .= '<th style="width:8%;text-decoration:underline;"><strong>Available<br>Capital</strong></th>';
					$header .= '<th style="width:8%;text-decoration:underline;"><strong>Phone</strong></th>';
					$header .= '<th style="width:18%;text-decoration:underline;"><strong>E-Mail</strong></th>';
					$header .= '</tr>';
					$number = 0;
					$total_account = 0;
					$hash_total_loan = 0;
					$amount_total_loan = 0;
					$hash_active_loan = 0;
					$total_active_loan=0;
					$commited = 0;
					$avaliable = 0;			
					foreach ($getContact as $row) {
						$contact_accounts 	=  $this->get_contact_accounts($row->contact_id);
						$total_loan_array=$this->total_loan_acount($row->contact_id);
						$total_active_loan_array=$this->active_loan_acount($row->contact_id);						
						$countTotal=$total_loan_array['countTotal'];
						$AmountTotal=$total_loan_array['AmountTotal'];
						$countActive=$total_active_loan_array['countActive'];
						$AmountActive=$total_active_loan_array['AmountActive'];

						$contact_funds = $this->get_contact_investment($row->contact_id);
						if(empty($contact_funds) || $contact_funds<0){
							$contact_funds=0;
						}
						$total_account += $contact_accounts;
						$hash_total_loan += $countTotal;
						$amount_total_loan += $AmountTotal;
						$hash_active_loan += $countActive;
						$total_active_loan += $AmountActive;
						$commited += $contact_funds;
						$avaliable += ($contact_funds - $AmountActive);
						if ($number % 2 == 0) {
							$class_add = "even";
						} else if ($number == 0) {
							$class_add = "even";
						} else {
							$class_add = "odd";
						}
						$header .= '<tr class="'. $class_add .'">';
						$header .= '<td>' . $row->contact_firstname . ' ' . $row->contact_lastname . '</td>';
						$header .= '<td>' . $contact_accounts . '</td>';
						$header .= '<td>' . number_format($countTotal) . '</td>';
						$header .= '<td>$' . number_format($AmountTotal,2) . '</td>';
						$header .= '<td>' . number_format($countActive) . '</td>';
						$header .= '<td>$' . number_format($AmountActive,2) . '</td>';
						$header .= '<td>$' . number_format($contact_funds,2) . '</td>';
						$header .= '<td>$' . number_format(($contact_funds - $AmountActive),2) . '</td>';
						$header .= '<td>' . $row->contact_phone . '</td>';
						$header .= '<td>' . $row->contact_email . '</td>';
						$header .= '</tr>';
						$number++;
					}
					$header .= '<tr class="table_bottom">';
					$header .= '<th><strong>Total: ' . $number . '</strong></th>';
					$header .= '<th><strong>' . number_format($total_account) . '</strong></th>';
					$header .= '<th><strong>' . number_format($hash_total_loan) . '</strong></th>';
					$header .= '<th><strong>' . '$' . number_format($amount_total_loan,2) . '</strong></th>';
					$header .= '<th><strong>' . $hash_active_loan . '</strong></th>';
					$header .= '<th>$'.number_format($total_active_loan,2) . '</th>';
					$header .= '<th><strong>' . '$' . number_format($commited,2) . '</strong></th>';
					$header .= '<th><strong>' . '$' . number_format($avaliable,2) . '</strong></th>';
					$header .= '<th></th>';
					$header .= '<th></th>';
					$header .= '<th></th>';
					$header .= '</tr>';
					$header .= '</table>';
					$pdf->WriteHTML($header);
					$pdf->Output("lender_filter.pdf", "D");
				}
		}else{
				if($calling_type == 'table'){
					$getContact = $getContact->result();
					$totalQuery= $this->User_model->query("SELECT FOUND_ROWS()");
					$totalQueryResult= $totalQuery->result();
					$totalNumRows=0;
					foreach ($totalQueryResult[0] as $key =>$value) {
					 	$totalNumRows=$value;
					}
					foreach ($getContact as $key =>$value) {
						$contact_accounts 	=  $this->get_contact_accounts($value->contact_id);
						//$contact_accounts=$value->totallender;
						$contact_funds = $this->get_contact_investment($value->contact_id);
						if(empty($contact_funds) || $contact_funds<0){
							$contact_funds=0;
						}
						$total_loan_array=$this->total_loan_acount($value->contact_id);
						$total_active_loan_array=$this->active_loan_acount($value->contact_id);
						
						$countTotal=$total_loan_array['countTotal'];
						$AmountTotal=$total_loan_array['AmountTotal'];
						$countActive=$total_active_loan_array['countActive'];
						$AmountActive=$total_active_loan_array['AmountActive'];
						//Assign to table
						$rowRequests[$key]['name'] 						= '<a href="'.base_url().'viewcontact/'.$value->contact_id.'">'.$value->contact_firstname." ".$value->contact_lastname.'</a>';
						$rowRequests[$key]['contact_accounts']   		= $contact_accounts;
						$rowRequests[$key]['countTotal'] 		  	  	= $countTotal;
						$rowRequests[$key]['AmountTotal'] 		  		= number_format($AmountTotal,2);
						$rowRequests[$key]['countActive'] 			  	= $countActive;
						$rowRequests[$key]['AmountActive']  			= number_format($AmountActive,2);
						$rowRequests[$key]['comment']    				= number_format($contact_funds,2);
						$rowRequests[$key]['contact_funds']    			= number_format(($contact_funds - $AmountActive),2);
						$rowRequests[$key]['contact_phone']            	= $value->contact_phone;
						$rowRequests[$key]['contact_email']           	= $value->contact_email;
						//Assign to table end
					}
				}
				$total=0;
				$total_account=0;
				$hash_total_loan=0;
				$amount_total_loan=0;
				$hash_active_loan=0;
				$amount_active_loan=0;
				$commited=0;
				$avaliable=0;
				
				if($calling_type == 'total_table'){
					$totalNumRows=0;
					$total_loan_filters=$this->User_model->query($total_loan_filters);
					if($total_loan_filters->num_rows() > 0)
					{
						$getContact = $total_loan_filters->result();
						$totalNumRows=$total_loan_filters->num_rows();
						foreach ($getContact as $key => $value) 
						{
							$contact_accounts = $this->get_contact_accounts($value->contact_id);
							//$contact_accounts=$value->totallender;
							$contact_funds = $this->get_contact_investment($value->contact_id);
							if(empty($contact_funds) || $contact_funds<0){
								$contact_funds=0;
							}
							$contact_accounts 	=  $this->get_contact_accounts($value->contact_id);
							$total_loan_array=$this->total_loan_acount($value->contact_id);
							$total_active_loan_array=$this->active_loan_acount($value->contact_id);
							$countTotal=$total_loan_array['countTotal'];
							$AmountTotal=$total_loan_array['AmountTotal'];
							$countActive=$total_active_loan_array['countActive'];
							$AmountActive=$total_active_loan_array['AmountActive'];
							$total++;
							$total_account += $contact_accounts;
							$hash_total_loan += $countTotal;
							$amount_total_loan += $AmountTotal;
							$hash_active_loan += $countActive;
							$amount_active_loan += $AmountActive;
							if(!empty($contact_funds) && $contact_funds>0){
								$commited += $contact_funds;
							}
						}
					}
				}
				echo json_encode(
						array(
							'data'                        => $rowRequests, 
							'total'                       => $totalNumRows,
							'total_loan'                  => $total,
							'total_account'           	  => $total_account,
							'hash_total_loan'   		  => $hash_total_loan,
							'amount_total_loan'           => $amount_total_loan,
							'hash_active_loan'            => $hash_active_loan,
							'amount_active_loan'          => $amount_active_loan,
							'commited_capital_amount'     => $commited,
							'avilable_capital_amount'     => $commited-$amount_active_loan,
							'total_date'        		  => "",
							'total_phone'        		  => "",
							'total_email'        	  	  => ""
						)
					);
				die();
		}
	}
	public function lenderFilter(){
		$account_status_option         		= $this->config->item('account_status_option');
		$data['account_status_option']      = $account_status_option;
		$data['ActiveAccount'] = '';
		$data['content'] = $this->load->view('Reports/investorFilter', $data, true);
		$this->load->view('template_files/template_reports', $data);
	}

	public function lenderFilterNew(){
		error_reporting(0);

		$investor_type_option 				= $this->config->item('investor_type_option');
		$account_status_option         		= $this->config->item('account_status_option');
		$ActiveAccount 						= $this->input->post('ActiveAccount');
		$FundedTrustDeed 					= $this->input->post('FundedTrustDeed');
		$ActiveTrustDeed 					= $this->input->post('ActiveTrustDeed');
		$WholeInvestor 						= $this->input->post('WholeInvestor');
		$lender_portal 						= $this->input->post('lender_portal');
		$data['account_status_option']      = $account_status_option;
		$data['ActiveAccount'] 				= '';
		$where								= '';

		if($WholeInvestor || $lender_portal != ''){

			if($WholeInvestor == '1'){
				$newcondition = "AND lct.WholeNoteInvestor = '1'";
			}elseif($WholeInvestor == '2'){
				$newcondition = "AND lct.WholeNoteInvestor != '1'";
			}else{
				$newcondition = '';
			}
			if($lender_portal == '1'){
				$allow_access = "AND lct.allow_access = '1'";
			}elseif($lender_portal == '2'){
				$allow_access = "AND lct.allow_access = '0'";
			}else{
				$allow_access = '';
			}
			if($this->input->post('ActiveAccount'))
			{
				$ActiveAccount = $this->input->post('ActiveAccount');
				$where = " AND account_status = '".$ActiveAccount."'";
				$data['ActiveAccount'] = $this->input->post('ActiveAccount');
			}
			$trust_deedCondition = "ct.contact_specialty = '16'";
			$getContact = $this->User_model->query("SELECT c.contact_id, c.contact_firstname, c.contact_lastname, c.contact_phone, c.contact_email FROM lender_contact as lc JOIN investor as i ON lc.lender_id = i.id JOIN contact as c ON lc.contact_id = c.contact_id JOIN contact_tags as ct ON lc.contact_id = ct.contact_id JOIN lender_contact_type as lct ON lc.contact_id = lct.contact_id WHERE ".$trust_deedCondition." ".$newcondition." ".$allow_access." ".$where." GROUP BY contact_id ORDER BY contact_id ");
			$query="SELECT c.contact_id, c.contact_firstname, c.contact_lastname, c.contact_phone, c.contact_email FROM lender_contact as lc JOIN investor as i ON lc.lender_id = i.id JOIN contact as c ON lc.contact_id = c.contact_id JOIN contact_tags as ct ON lc.contact_id = ct.contact_id JOIN lender_contact_type as lct ON lc.contact_id = lct.contact_id WHERE ".$trust_deedCondition." ".$newcondition." ".$allow_access." ".$where." GROUP BY contact_id ORDER BY contact_id";
		}else{

			$trust_deedCondition = "ct.contact_specialty = '16'";
			$getContact = $this->User_model->query("SELECT c.contact_id, c.contact_firstname, c.contact_lastname, c.contact_phone, c.contact_email FROM contact as c JOIN contact_tags as ct ON c.contact_id = ct.contact_id WHERE ".$trust_deedCondition." GROUP BY contact_id ORDER BY contact_id ");
			$query="SELECT c.contact_id, c.contact_firstname, c.contact_lastname, c.contact_phone, c.contact_email FROM contact as c JOIN contact_tags as ct ON c.contact_id = ct.contact_id WHERE ".$trust_deedCondition." GROUP BY contact_id ORDER BY contact_id";
		}

		
		$data['ActiveAccount'] = $ActiveAccount;
		$data['FundedTrustDeed'] = $FundedTrustDeed;
		$data['ActiveTrustDeed'] = $ActiveTrustDeed;
		$data['WholeInvestor'] = $WholeInvestor;
		$data['lender_portal'] = $lender_portal;
		$data['query'] = $query;
		$getContact = $getContact->result();
		echo count($getContact)."<br>";
		$m=1;
		foreach ($getContact as $value) {

			$contact_accounts = $this->get_contact_accounts($value->contact_id);
			$contact_totalLoans = $this->get_contact_totalLoans($value->contact_id);
			$contact_addedDate = $this->get_contact_addedDate($value->contact_id);
			$contact_funds = $this->get_contact_funds($value->contact_id);


			if($ActiveTrustDeed || $FundedTrustDeed || $ActiveAccount != '')
			{

				//for Active Account filter
				if($ActiveAccount == 1){

					if($contact_accounts > 0){

						//check funded trust deed...
						if($FundedTrustDeed == 1){

							if($contact_totalLoans['countTotal'] > 0){

								$lenderFilterData[] = array(
									"contact_id" => $value->contact_id,
									"contact_firstname" => $value->contact_firstname,
									"contact_lastname" => $value->contact_lastname,
									"contact_phone" => $value->contact_phone,
									"contact_email" => $value->contact_email,
									"contact_accounts" => $contact_accounts,
									"countTotal" => $contact_totalLoans['countTotal'],
									"AmountTotal" => $contact_totalLoans['AmountTotal'],
									"countActive" => $contact_totalLoans['countActive'],
									"AmountActive" => $contact_totalLoans['AmountActive'],
									"contact_addedDate" => $contact_addedDate,
									"contact_funds" => $contact_funds,
								);
							}

						}elseif($FundedTrustDeed == 2){

							if($contact_totalLoans['countTotal'] <= 0){

								$lenderFilterData[] = array(
									"contact_id" => $value->contact_id,
									"contact_firstname" => $value->contact_firstname,
									"contact_lastname" => $value->contact_lastname,
									"contact_phone" => $value->contact_phone,
									"contact_email" => $value->contact_email,
									"contact_accounts" => $contact_accounts,
									"countTotal" => $contact_totalLoans['countTotal'],
									"AmountTotal" => $contact_totalLoans['AmountTotal'],
									"countActive" => $contact_totalLoans['countActive'],
									"AmountActive" => $contact_totalLoans['AmountActive'],
									"contact_addedDate" => $contact_addedDate,
									"contact_funds" => $contact_funds,
								);
							}
						}else{ 

							$lenderFilterData[] = array(
								"contact_id" => $value->contact_id,
								"contact_firstname" => $value->contact_firstname,
								"contact_lastname" => $value->contact_lastname,
								"contact_phone" => $value->contact_phone,
								"contact_email" => $value->contact_email,
								"contact_accounts" => $contact_accounts,
								"countTotal" => $contact_totalLoans['countTotal'],
								"AmountTotal" => $contact_totalLoans['AmountTotal'],
								"countActive" => $contact_totalLoans['countActive'],
								"AmountActive" => $contact_totalLoans['AmountActive'],
								"contact_addedDate" => $contact_addedDate,
								"contact_funds" => $contact_funds,
							);
						}
					}


				}elseif($ActiveAccount == 2){

					if($contact_accounts <= 0){

						$lenderFilterData[] = array(
							"contact_id" => $value->contact_id,
							"contact_firstname" => $value->contact_firstname,
							"contact_lastname" => $value->contact_lastname,
							"contact_phone" => $value->contact_phone,
							"contact_email" => $value->contact_email,
							"contact_accounts" => $contact_accounts,
							"countTotal" => $contact_totalLoans['countTotal'],
							"AmountTotal" => $contact_totalLoans['AmountTotal'],
							"countActive" => $contact_totalLoans['countActive'],
							"AmountActive" => $contact_totalLoans['AmountActive'],
							"contact_addedDate" => $contact_addedDate,
							"contact_funds" => $contact_funds,
						);
					}

				}else{ }

				//Funded trust deed filter
				if($FundedTrustDeed == 1){

					if($contact_totalLoans['countTotal'] > 0){
						$lenderFilterData[] = array(
							"contact_id" => $value->contact_id,
							"contact_firstname" => $value->contact_firstname,
							"contact_lastname" => $value->contact_lastname,
							"contact_phone" => $value->contact_phone,
							"contact_email" => $value->contact_email,
							"contact_accounts" => $contact_accounts,
							"countTotal" => $contact_totalLoans['countTotal'],
							"AmountTotal" => $contact_totalLoans['AmountTotal'],
							"countActive" => $contact_totalLoans['countActive'],
							"AmountActive" => $contact_totalLoans['AmountActive'],
							"contact_addedDate" => $contact_addedDate,
							"contact_funds" => $contact_funds,
						);
					}

				}elseif($FundedTrustDeed == 2){

					if($contact_totalLoans['countTotal'] <= 0){

						$lenderFilterData[] = array(
							"contact_id" => $value->contact_id,
							"contact_firstname" => $value->contact_firstname,
							"contact_lastname" => $value->contact_lastname,
							"contact_phone" => $value->contact_phone,
							"contact_email" => $value->contact_email,
							"contact_accounts" => $contact_accounts,
							"countTotal" => $contact_totalLoans['countTotal'],
							"AmountTotal" => $contact_totalLoans['AmountTotal'],
							"countActive" => $contact_totalLoans['countActive'],
							"AmountActive" => $contact_totalLoans['AmountActive'],
							"contact_addedDate" => $contact_addedDate,
							"contact_funds" => $contact_funds,
						);
						$m++;
					}
					
				}else{ }

				//Active trust deed filter
				if($ActiveTrustDeed == 1){
					if($contact_totalLoans['countActive'] > 0){
						$lenderFilterData[] = array(
							"contact_id" => $value->contact_id,
							"contact_firstname" => $value->contact_firstname,
							"contact_lastname" => $value->contact_lastname,
							"contact_phone" => $value->contact_phone,
							"contact_email" => $value->contact_email,
							"contact_accounts" => $contact_accounts,
							"countTotal" => $contact_totalLoans['countTotal'],
							"AmountTotal" => $contact_totalLoans['AmountTotal'],
							"countActive" => $contact_totalLoans['countActive'],
							"AmountActive" => $contact_totalLoans['AmountActive'],
							"contact_addedDate" => $contact_addedDate,
							"contact_funds" => $contact_funds,
						);
					}

				}elseif($ActiveTrustDeed == 2){

					if($contact_totalLoans['countActive'] <= 0){

						$lenderFilterData[] = array(
							"contact_id" => $value->contact_id,
							"contact_firstname" => $value->contact_firstname,
							"contact_lastname" => $value->contact_lastname,
							"contact_phone" => $value->contact_phone,
							"contact_email" => $value->contact_email,
							"contact_accounts" => $contact_accounts,
							"countTotal" => $contact_totalLoans['countTotal'],
							"AmountTotal" => $contact_totalLoans['AmountTotal'],
							"countActive" => $contact_totalLoans['countActive'],
							"AmountActive" => $contact_totalLoans['AmountActive'],
							"contact_addedDate" => $contact_addedDate,
							"contact_funds" => $contact_funds,
						);
					}
				}else{ }
				
				

			}else{ //Main else part

				$lenderFilterData[] = array(
						"contact_id" => $value->contact_id,
						"contact_firstname" => $value->contact_firstname,
						"contact_lastname" => $value->contact_lastname,
						"contact_phone" => $value->contact_phone,
						"contact_email" => $value->contact_email,
						"contact_accounts" => $contact_accounts,
						"countTotal" => $contact_totalLoans['countTotal'],
						"AmountTotal" => $contact_totalLoans['AmountTotal'],
						"countActive" => $contact_totalLoans['countActive'],
						"AmountActive" => $contact_totalLoans['AmountActive'],
						"contact_addedDate" => $contact_addedDate,
						"contact_funds" => $contact_funds,
					);
			}

		}
		echo "total".$m;
		//Display report data
		$reportDisplay = $this->input->post('reportDisplay');
		if($reportDisplay == 'excel'){

			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename="lender_filter.csv";');

			$f = fopen("php://output", "w");
			fputcsv($f, array('Name', '# of Accounts', '# of Total Loans', '$ of Total Loans', '# of Active Loans', '$ of Active Loans', 'Committed Capital', 'Available Capital', 'Date Added', 'Phone', 'E-Mail')); // title...

			foreach ($lenderFilterData as $row) {

				$output['contact_firstname'] = $row['contact_firstname'] . ' ' . $row['contact_lastname'];
				$output['contact_accounts'] = $row['contact_accounts'];
				$output['countTotal'] = number_format($row['countTotal']);
				$output['AmountTotal'] = number_format($row['AmountTotal']);
				$output['countActive'] = $row['countActive'];
				$output['AmountActive'] = number_format($row['AmountActive']);
				$output['contact_funds'] = number_format($row['contact_funds']);
				$output['avaliable'] = number_format($row['contact_funds'] - $row['AmountActive']);
				$output['contact_addedDate'] = $row['contact_addedDate'];
				$output['contact_phone'] = $row['contact_phone'];
				$output['contact_email'] = $row['contact_email'];
				
				fputcsv($f, $output);
			}
			
			fclose($f);
			exit();

		}elseif($reportDisplay == 'pdf'){

			$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
			$pdf->SetCreator(PDF_CREATOR);
			// $pdf->SetHeaderData("include/icons/logo-light.png", 30, '', '');
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
			$pdf->SetMargins(2, 20, 2);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
			$pdf->SetFont('times', '', 7);
			$pdf->AddPage('L', 'A4');
			$header = '<h1 style="color:#003468;font-size:18px;"> Lender Filter</h1>';
			$header .= '<style>
	                        .table {
	                            width: 100%;
	                            max-width: 100%;
	                            margin-bottom: 20px;
	                            border-collapse:collapse;
	                        }
	                        .table-bordered {
	                        border: 1px solid #ddd;
	                        }
	                        table {
	                            border-spacing: 0;
	                            border-collapse: collapse;
	                        }
	                        .table td{
	                            height : 25px;
	                            font-size:12px;
	                        }
	                        tr.table_header th{
	                            height:25px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:center;
	                            background-color:#bfcfe3;
	                        }

	                        tr.table_bottom th{
	                            height:20px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:left;
	                            background-color:#bfcfe3;
	                        }
	                        tr.odd td{
	                            background-color:#ededed;
	                        }
	                        </style>
	                        ';

			$header .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';
			$header .= '<tr class="table_header">';

			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Name</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong># of<br>Accounts</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong># of<br>Total Loans</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong>$ of<br>Total Loans</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong># of<br>Active Loans</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong>$ of<br>Active Loans</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong>Committed<br>Capital</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong>Available<br>Capital</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong>Date Added</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong>Phone</strong></th>';
			$header .= '<th style="width:18%;text-decoration:underline;"><strong>E-Mail</strong></th>';
			$header .= '</tr>';

			$number = 0;
			$total_account = 0;
			$hash_total_loan = 0;
			$amount_total_loan = 0;
			$hash_active_loan = 0;
			$commited = 0;
			$avaliable = 0;
			
			foreach ($lenderFilterData as $row) {

				$contact = $row['contact_id'];
				$total_account += $row['contact_accounts'];
				$hash_total_loan += $row['countTotal'];
				$amount_total_loan += $row['AmountTotal'];
				$hash_active_loan += $row['countActive'];
				$commited += $row['contact_funds'];
				$avaliable += ($row['contact_funds'] - $row['AmountActive']);

				if ($number % 2 == 0) {
					$class_add = "even";
				} else if ($number == 0) {
					$class_add = "even";
				} else {
					$class_add = "odd";
				}

				$header .= '<tr class="' . $class_add . '">';
				$header .= '<td>' . $row["contact_firstname"] . ' ' . $row["contact_lastname"] . '</td>';
				$header .= '<td>' . $row["contact_accounts"] . '</td>';
				$header .= '<td>' . number_format($row['countTotal']) . '</td>';
				$header .= '<td>$' . number_format($row['AmountTotal']) . '</td>';
				$header .= '<td>' . $row['countActive'] . '</td>';
				$header .= '<td>$' . number_format($row["AmountActive"]) . '</td>';
				$header .= '<td>$' . number_format($row['contact_funds']) . '</td>';
				$header .= '<td>$' . number_format($row['contact_funds'] - $row["AmountActive"]) . '</td>';
				$header .= '<td>' . $row['contact_addedDate'] . '</td>';
				$header .= '<td>' . $row["contact_phone"] . '</td>';
				$header .= '<td>' . $row["contact_email"] . '</td>';

				$header .= '</tr>';
				$number++;
			}

			$header .= '<tr class="table_bottom">';
			$header .= '<th><strong>Total: ' . $number . '</strong></th>';
			$header .= '<th><strong>' . number_format($total_account) . '</strong></th>';
			$header .= '<th><strong>' . number_format($hash_total_loan) . '</strong></th>';
			$header .= '<th><strong>' . '$' . number_format($amount_total_loan) . '</strong></th>';
			$header .= '<th><strong>' . $hash_active_loan . '</strong></th>';
			$header .= '<th></th>';
			$header .= '<th><strong>' . '$' . number_format($commited) . '</strong></th>';
			$header .= '<th><strong>' . '$' . number_format($avaliable) . '</strong></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '</tr>';
			$header .= '</table>';

			$pdf->WriteHTML($header);
			$pdf->Output("lender_filter.pdf", "D");

		}else{

			$data['lenderFilterData'] = $lenderFilterData;
			$data['content'] = $this->load->view('Reports/lenderFilterbackup-30-06-2021', $data, true);
			$this->load->view('template_files/template_reports', $data);
		}
	}

	public function lender_total_contact($lenderID){

		$total_lender_count = $this->User_model->query("SELECT COUNT(*) as totalCount FROM lender_contact WHERE lender_id = '".$lenderID."'");
		if($total_lender_count->num_rows() > 0){

			$total_lender_count = $total_lender_count->result();
			$totalCount = $total_lender_count[0]->totalCount;
		}else{
			$totalCount = 0;
		}

		return $totalCount;
	}

	public function lender_total_data($lenderID){

		$fetchloans = $this->User_model->query("select COUNT(*) as total_loans, SUM(investment) as total_investment from loan_assigment as la JOIN loan_servicing as ls ON la.talimar_loan = ls.talimar_loan where ls.loan_status = '2' AND la.lender_name = '".$lenderID."'");
		$fetchloans = $fetchloans->result();
		$countActive = $fetchloans[0]->total_loans;
		$AmountActive = $fetchloans[0]->total_investment;

		//Paid-Off loan
		$fetchloans = $this->User_model->query("select COUNT(*) as total_loans1, SUM(investment) as total_investment1 from loan_assigment as la JOIN loan_servicing as ls ON la.talimar_loan = ls.talimar_loan where ls.loan_status = '3' AND la.lender_name = '".$lenderID."'");
		$fetchloans = $fetchloans->result();
		$countPaid = $fetchloans[0]->total_loans1;
		$AmountPaid = $fetchloans[0]->total_investment1;

		$accArray['countActive'] = $countActive;
		$accArray['AmountActive'] = $AmountActive;
		$accArray['countPaid'] = $countPaid;
		$accArray['AmountPaid'] = $AmountPaid;

		return $accArray;
	}

	public function borrower_total_data($borrowerID){

		$fetchloans = $this->User_model->query("select COUNT(*) as total_loans, SUM(l.loan_amount) as total_investment from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status = '2' AND l.borrower = '".$borrowerID."'");
		$fetchloans = $fetchloans->result();
		$countActive = $fetchloans[0]->total_loans;
		$AmountActive = $fetchloans[0]->total_investment;

		//Paid-Off loan
		$fetchloans = $this->User_model->query("select COUNT(*) as total_loans1, SUM(l.loan_amount) as total_investment1 from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where ls.loan_status = '3' AND l.borrower = '".$borrowerID."'");
		$fetchloans = $fetchloans->result();
		$countPaid = $fetchloans[0]->total_loans1;
		$AmountPaid = $fetchloans[0]->total_investment1;

		$accArray['countActive'] = $countActive;
		$accArray['AmountActive'] = $AmountActive;
		$accArray['countPaid'] = $countPaid;
		$accArray['AmountPaid'] = $AmountPaid;

		return $accArray;
	}

	public function get_contact_info($contactID){

		$get_contact_info = $this->User_model->query("SELECT contact_firstname, contact_lastname, contact_phone, contact_email FROM contact WHERE contact_id = '".$contactID."'");
		$get_contact_info = $get_contact_info->result();

		return $get_contact_info; 
	}

	/*
		Description : This function use for get date borrower account by date - Add status filter 
		Author      : Bitcot
		Created     : 
		Modified    : 
		Modified    : 07-04-2021
	*/

	public function Newborroweraccounts(){
		$data = array();
		$Newborroweraccounts = array();
		$where = '';
		$date1 = '';
		$date2 = '';
		$investor_type_option 				= $this->config->item('investor_type_option');
		$account_status_option         		= $this->config->item('account_status_option');
		$data['account_status_option']      = $account_status_option;
		$data['account_status'] = '';
		$data['select_date'] = '';

		if($this->input->post('dates'))
		{

			if($this->input->post('dates')==1)
			{
				$month_start = strtotime('first day of this month', time());
				$month_end = strtotime('today', time());
				$date1 = date('Y-m-d', $month_start);
				$date2 = date('Y-m-d', $month_end);
				$data['select_date'] = $this->input->post('dates');
			}

			if($this->input->post('dates')==2)
			{
				$month_start = strtotime('first day of January', time());
				$month_end = strtotime('today', time());
				$date1 = date('Y-m-d', $month_start);
				$date2 = date('Y-m-d', $month_end);
			}

			if($this->input->post('dates')==3 && $this->input->post('start') !='' && $this->input->post('end') !='')
			{
				$start = $this->input->post('start');
				$start = trim($start);
				$start = str_replace('-', '/', $start);

				$end = $this->input->post('end');
				$end = trim($end);
				$end = str_replace('-', '/', $end);				

				$month_start = strtotime($start);
				$month_end = strtotime($end);
				

				$date1 = date('Y-m-d', $month_start);
				$date2 = date('Y-m-d', $month_end);

				$data['start'] = $this->input->post('start');;
				$data['end'] = $this->input->post('end');
			}

			if($this->input->post('dates')==4)
			{
				$month_start = strtotime("last year January 1st");
				$month_end = strtotime("last year December 31st");
				// $date1 = 

				$date1 = date('Y-m-d', $month_start);
				$date2 = date('Y-m-d', $month_end);
			}

			if($this->input->post('dates')==5)
			{
				$first_day_this_year = date("Y-m-d", strtotime('first day of January ' . date('Y')));
				$month_start = strtotime("-1 year". $first_day_this_year);
				$month_end = strtotime("-1 year". date('Y-m-d'));
				
				$date1 = date('Y-m-d', $month_start);
				$date2 = date('Y-m-d', $month_end);
			}

			if($date1 != '' && $date2 != '')
			{
				$where = "WHERE bd.bdate_created BETWEEN '" . $date1 . "' AND '" . $date2 . "'";
			}

			$data['select_date'] = $this->input->post('dates');	
		}

		if($this->input->post('account_status'))
		{
			$account_status = $this->input->post('account_status');
			if($where != '')
			{
				$where .= " AND bd.account_status = '".$account_status."'";
			}
			else{
				$where = " WHERE bd.account_status = '".$account_status."'";
			}

			$data['account_status'] = $this->input->post('account_status');
		}

		// print_r($data);
		// die();


		$getnewborrower = $this->User_model->query("SELECT bd.id as bid, bd.b_name, bd.bdate_created, bc.contact_id FROM borrower_data as bd JOIN borrower_contact as bc ON bd.id = bc.borrower_id $where GROUP BY bd.id ORDER BY bd.bdate_created DESC ");
		$getnewborrower = $getnewborrower->result();

		foreach ($getnewborrower as $value) {
			
			$lender_total_data 	= $this->borrower_total_data($value->bid);
			$contact_info 		= $this->get_contact_info($value->contact_id);

				$Newborroweraccounts[] = array(
							"lid" => $value->bid,
							"name" => $value->b_name,
							"contact_id" => $value->contact_id,
							"cname" => $contact_info[0]->contact_firstname.' '.$contact_info[0]->contact_lastname,
							"phone" => $contact_info[0]->contact_phone,
							"email" => $contact_info[0]->contact_email,
							"ldate_created" => $value->bdate_created,
							"countActive" => $lender_total_data['countActive'],
							"AmountActive" => $lender_total_data['AmountActive'],
							"countTotal" => $lender_total_data['countPaid'] + $lender_total_data['countActive'],
							"AmountTotal" => $lender_total_data['AmountPaid'] + $lender_total_data['AmountActive'],
						);
		}

		$data['Newborroweraccounts'] = $Newborroweraccounts;

		$reportDisplay = $this->input->post('reportDisplay');

		if($reportDisplay == 'excel'){

			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename="New-borrower-accounts.csv";');

			$f = fopen("php://output", "w");
			fputcsv($f, array('Borrower Name', 'Contact Name', 'Contact Phone', 'Contact E-Mail', 'Active(#)', 'Active($)', 'Total(#)', 'Total($)', 'Established')); // title...

			foreach ($Newborroweraccounts as $row) {

				if($row['ldate_created'] !=''){
					$ldate_created = date('m-d-Y', strtotime($row['ldate_created']));
				}else{
					$ldate_created = '';
				}

				$output['name'] 	= $row['name'];
				$output['cname'] 	= $row['cname'];
				$output['phone'] 	= $row['phone'];
				$output['email'] 	= $row['email'];
				$output['countActive'] = $row['countActive'];
				$output['AmountActive'] = number_format($row['AmountActive']);
				$output['countTotal'] = $row['countTotal'];
				$output['AmountTotal'] = number_format($row['AmountTotal']);
				$output['ldate_created'] = $ldate_created;
				
				fputcsv($f, $output);
			}
			
			fclose($f);
			exit();

		}elseif($reportDisplay == 'pdf'){

			$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
			$pdf->SetMargins(2, 20, 2);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
			$pdf->SetFont('times', '', 7);
			$pdf->AddPage('L', 'A4');
			$header = '<h1 style="color:#003468;font-size:18px;"> Borrower Accounts by Date</h1>';
			$header .= '<style>
	                        .table {
	                            width: 100%;
	                            max-width: 100%;
	                            margin-bottom: 20px;
	                            border-collapse:collapse;
	                        }
	                        .table-bordered {
	                        border: 1px solid #ddd;
	                        }
	                        table {
	                            border-spacing: 0;
	                            border-collapse: collapse;
	                        }
	                        .table td{
	                            height : 25px;
	                            font-size:12px;
	                        }
	                        tr.table_header th{
	                            height:25px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:center;
	                            background-color:#bfcfe3;
	                        }

	                        tr.table_bottom th{
	                            height:20px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:left;
	                            background-color:#bfcfe3;
	                        }
	                        tr.odd td{
	                            background-color:#ededed;
	                        }
	                        </style>
	                        ';

			$header .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';
			$header .= '<tr class="table_header">';

			$header .= '<th style="width:20%;text-decoration:underline;"><strong>Borrower Name</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Contact Name</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Contact Phone</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Contact Email</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Active<br>Loans</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Active ($)</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Total<br>Loans</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Total ($)</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Established</strong></th>';
			
			$header .= '</tr>';

			$number = 0;
			
			foreach ($Newborroweraccounts as $row) {

				if ($number % 2 == 0) {
					$class_add = "even";
				} else if ($number == 0) {
					$class_add = "even";
				} else {
					$class_add = "odd";
				}

				if($row['ldate_created'] !=''){
					$ldate_created = date('m-d-Y', strtotime($row['ldate_created']));
				}else{
					$ldate_created = '';
				}

				$header .= '<tr class="' . $class_add . '">';
				$header .= '<td>' . $row["name"] .'</td>';
				$header .= '<td>' . $row["cname"] . '</td>';
				$header .= '<td>' . $row["phone"] . '</td>';
				$header .= '<td>' . $row["email"] . '</td>';
				$header .= '<td>' . $row['countActive']. '</td>';
				$header .= '<td>$' . number_format($row['AmountActive']) . '</td>';
				$header .= '<td>' . $row['countTotal'] . '</td>';
				$header .= '<td>$' .number_format($row['AmountTotal']) . '</td>';
				$header .= '<td>' . $ldate_created . '</td>';

				$header .= '</tr>';
				$number++;
			}

			$header .= '<tr class="table_bottom">';
			$header .= '<th><strong>Total: ' . $number . '</strong></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			
			$header .= '</tr>';

			$header .= '</table>';

			$pdf->WriteHTML($header);
			$pdf->Output("New-borrower-accounts.pdf", "D");

		}else{

			$data['content'] = $this->load->view('Reports/Newborroweraccounts', $data, true);
			$this->load->view('template_files/template_reports', $data);
		}
	}

	/*
		Description : This function use for get date lender account by date - Add colum in Lender type, status, and remove email and hpone files and sum numeric fields
		Author      : Bitcot
		Created     : 
		Modified    : 24-03-2021
		Modified    : 27-03-2021
		Description : Add status filter
		Modified    : 07-04-2021
	*/

	public function Newlenderaccounts(){
		error_reporting(0);
		$where = '';
		$date1 = '';
		$date2 = '';
		$investor_type_option 				= $this->config->item('investor_type_option');
		$account_status_option         		= $this->config->item('account_status_option');
		$data['select_date']= '';

		if($this->input->post('dates'))
		{

			if($this->input->post('dates')==1)
			{
				$month_start = strtotime('first day of this month', time());
				$month_end = strtotime('today', time());
				$date1 = date('Y-m-d', $month_start);
				$date2 = date('Y-m-d', $month_end);
				$data['select_date'] = $this->input->post('dates');
			}

			if($this->input->post('dates')==2)
			{
				$month_start = strtotime('first day of January', time());
				$month_end = strtotime('today', time());
				$date1 = date('Y-m-d', $month_start);
				$date2 = date('Y-m-d', $month_end);
			}

			if($this->input->post('dates')==3 && $this->input->post('start') !='' && $this->input->post('end') !='')
			{
				$start = $this->input->post('start');
				$start = trim($start);
				$start = str_replace('-', '/', $start);

				$end = $this->input->post('end');
				$end = trim($end);
				$end = str_replace('-', '/', $end);				

				$month_start = strtotime($start);
				$month_end = strtotime($end);
				

				$date1 = date('Y-m-d', $month_start);
				$date2 = date('Y-m-d', $month_end);

				$data['start'] = $this->input->post('start');;
				$data['end'] = $this->input->post('end');
			}

			if($this->input->post('dates')==4)
			{
				$month_start = strtotime("last year January 1st");
				$month_end = strtotime("last year December 31st");
				// $date1 = 

				$date1 = date('Y-m-d', $month_start);
				$date2 = date('Y-m-d', $month_end);
			}

			if($this->input->post('dates')==5)
			{
				$first_day_this_year = date("Y-m-d", strtotime('first day of January ' . date('Y')));
				$month_start = strtotime("-1 year". $first_day_this_year);
				$month_end = strtotime("-1 year". date('Y-m-d'));
				
				$date1 = date('Y-m-d', $month_start);
				$date2 = date('Y-m-d', $month_end);
			}

			if($date1 != '' && $date2 != '')
			{
				$where = "HAVING i.ldate_created BETWEEN '" . $date1 . "' AND '" . $date2 . "'";
			}

			$data['select_date'] = $this->input->post('dates');	
		}

		if($this->input->post('account_status'))
		{
			$account_status = $this->input->post('account_status');
			if($where != '')
			{
				$where .= " AND account_status = '".$account_status."'";
			}
			else{
				$where = " HAVING account_status = '".$account_status."'";
			}

			$data['account_status'] = $this->input->post('account_status');
		}


		$getnewlender = $this->User_model->query("SELECT i.id as lid, i.name as name, i.investor_type as investor_type, i.account_status as account_status, i.ldate_created, lc.contact_id FROM investor as i JOIN lender_contact as lc ON i.id = lc.lender_id GROUP BY i.id $where ORDER BY i.ldate_created DESC ");


		$getnewlender = $getnewlender->result();

		foreach ($getnewlender as $value) {
			
			$lender_total_data 	= $this->lender_total_data($value->lid);
			$contact_info 		= $this->get_contact_info($value->contact_id);

			$Newlenderaccounts[] = array(
				"lid" => $value->lid,
				"name" => $value->name,
				"investor_type" => $value->investor_type,
				'account_status' => $value->account_status,
				"contact_id" => $value->contact_id,
				"cname" => $contact_info[0]->contact_firstname.' '.$contact_info[0]->contact_lastname,
				"phone" => $contact_info[0]->contact_phone,
				"email" => $contact_info[0]->contact_email,
				"ldate_created" => $value->ldate_created,
				"countActive" => $lender_total_data['countActive'],
				"AmountActive" => $lender_total_data['AmountActive'],
				"countTotal" => $lender_total_data['countPaid'] + $lender_total_data['countActive'],
				"AmountTotal" => $lender_total_data['AmountPaid'] + $lender_total_data['AmountActive'],
			);
		}

		$data['Newlenderaccounts'] = $Newlenderaccounts;

		$reportDisplay = $this->input->post('reportDisplay');
		if($reportDisplay == 'excel'){

			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename="lender-accounts-date.csv";');

			$f = fopen("php://output", "w");
			fputcsv($f, array('Lender Name', 'Lender Type', 'Status', 'Contact Name', 'Active(#)', 'Active($)', 'Total(#)', 'Total($)', 'Established')); // title...

			foreach ($Newlenderaccounts as $row) {

				$optn = isset($row['investor_type']) ? $row['investor_type'] : 0;
				$account_status = isset($row['account_status']) ? $row['account_status'] : '';	
					
				if($row['ldate_created'] !=''){
					$ldate_created = date('m-d-Y', strtotime($row['ldate_created']));
				}else{
					$ldate_created = '';
				}

				$output['name'] 	= $row['name'];
				$output['investor_type'] 	= $investor_type_option[$optn];	
				$output['account_status'] 	= $account_status_option[$account_status];
				$output['cname'] 	= $row['cname'];
				$output['countActive'] = $row['countActive'];
				$output['AmountActive'] = number_format($row['AmountActive']);
				$output['countTotal'] = $row['countTotal'];
				$output['AmountTotal'] = number_format($row['AmountTotal']);
				$output['ldate_created'] = $ldate_created;
				
				fputcsv($f, $output);
			}
			
			fclose($f);
			exit();

		}elseif($reportDisplay == 'pdf'){

			$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
			$pdf->SetCreator(PDF_CREATOR);
			// $pdf->SetHeaderData("include/icons/logo-light.png", 30, '', '');
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
			$pdf->SetMargins(2, 20, 2);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
			$pdf->SetFont('times', '', 7);
			$pdf->AddPage('L', 'A4');
			$header = '<h1 style="color:#003468;font-size:18px;"> Lender Accounts by Date</h1>';
			$header .= '<style>
	                        .table {
	                            width: 100%;
	                            max-width: 100%;
	                            margin-bottom: 20px;
	                            border-collapse:collapse;
	                        }
	                        .table-bordered {
	                        border: 1px solid #ddd;
	                        }
	                        table {
	                            border-spacing: 0;
	                            border-collapse: collapse;
	                            text-align:left;
	                        }
	                        .table td{
	                            height : 25px;
	                            font-size:12px;
	                        }
	                        tr.table_header th{
	                            height:25px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:center;
	                            background-color:#bfcfe3;
	                        }

	                        tr.table_bottom th{
	                            height:20px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:center;
	                            background-color:#bfcfe3;
	                        }
	                        tr.odd td{
	                            background-color:#ededed;
	                        }
	                        </style>
	                        ';

			$header .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';
			$header .= '<tr class="table_header">';

			$header .= '<th style="width:20%;text-decoration:underline;"><strong>Lender Name</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Lender Type</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Status</strong></th>';


			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Contact Name</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Active (#)</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Active ($)</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Total (#)</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Total ($)</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Established</strong></th>';
			
			$header .= '</tr>';

			$number = 0;
			
			foreach ($Newlenderaccounts as $row) {

				$optn = isset($row['investor_type']) ? $row['investor_type'] : 0;
				$account_status = isset($row['account_status']) ? $row['account_status'] : '';

				if ($number % 2 == 0) {
					$class_add = "even";
				} else if ($number == 0) {
					$class_add = "even";
				} else {
					$class_add = "odd";
				}

				if($row['ldate_created'] !=''){
					$ldate_created = date('m-d-Y', strtotime($row['ldate_created']));
				}else{
					$ldate_created = '';
				}

				$header .= '<tr class="' . $class_add . '">';
				$header .= '<td>' . $row["name"] .'</td>';
				$header .= '<td>' . $investor_type_option[$optn] . '</td>';
				$header .= '<td>' . $account_status_option[$account_status] . '</td>';
				$header .= '<td>' . $row["cname"] . '</td>';
				
				$header .= '<td>' . $row['countActive']. '</td>';
				$header .= '<td>$' . number_format($row['AmountActive']) . '</td>';
				$header .= '<td>' . $row['countTotal'] . '</td>';
				$header .= '<td>$' .number_format($row['AmountTotal']) . '</td>';
				$header .= '<td>' . $ldate_created . '</td>';

				$header .= '</tr>';
				$number++;
				$countActive 	= $countActive + $row['countActive'];
				$AmountActive 		= $AmountActive + $row['AmountActive'];
				$countTotal += $row['countTotal'];
				$AmountTotal += $row['AmountTotal'];

			}

			$header .= '<tr class="table_bottom">';
			$header .= '<th><strong>Total: ' . $number . '</strong></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th>'.number_format($countActive).'</th>';
			$header .= '<th>$'.number_format($AmountActive).'</th>';
			$header .= '<th>'.number_format($countTotal).'</th>';
			$header .= '<th>$'.number_format($AmountTotal).'</th>';
			$header .= '<th></th>';
			
			$header .= '</tr>';

			$header .= '</table>';

			$pdf->WriteHTML($header);
			$pdf->Output("lender-accounts-date.pdf", "D");

		}else{

			$data['content'] = $this->load->view('Reports/Newlenderaccounts', $data, true);
			$this->load->view('template_files/template_reports', $data);
		}
	}


	public function Fetch_All_borrowername() {
		
		$fetchborrowerNames = $this->User_model->select_star('borrower_data');
		$fetchborrowerNames = $fetchborrowerNames->result();
		
		foreach($fetchborrowerNames as $row){
			
			$fetchAllname[$row->id] = $row->b_name;
		}
		
		return $fetchAllname;
	}

	
	public function getBorrowerContactName(){
		
		$getdetails = $this->User_model->query("SELECT bc.borrower_id, c.contact_firstname, c.contact_lastname, c.contact_email, c.contact_phone FROM borrower_contact as bc JOIN contact as c ON c.contact_id = bc.contact_id");
		$getdetails = $getdetails->result();
		foreach($getdetails as $row){
			
			$alldetails['full_name'][$row->borrower_id] = $row->contact_firstname.' '.$row->contact_lastname;
			$alldetails['contact_email'][$row->borrower_id] = $row->contact_email;
			$alldetails['contact_phone'][$row->borrower_id] = $row->contact_phone;
		}
		
		return $alldetails;
	}
	
	
	
	public function pof_letter(){
		error_reporting(0);
		$ApprovalStatus = $this->input->post('ApprovalStatus');
		if($ApprovalStatus !=''){
			$condition = "WHERE req_status = '".$ApprovalStatus."'";
		}else{
			$condition = "";
		}
		
		$data['ApprovalStatus'] = $ApprovalStatus;
		
		$fetch_pro_app = $this->User_model->query("SELECT *, borrower_id as borrower FROM `borrower_property_approval` ".$condition."");
		$fetch_pro_app = $fetch_pro_app->result();
				
		$data['pof_letter_data'] = $fetch_pro_app;
		$data['Fetch_All_borrowername'] = $this->Fetch_All_borrowername();
		$data['getBorrowerContactName'] = $this->getBorrowerContactName();
		
		$reportDisplay = $this->input->post('reportDisplay');
		if($reportDisplay == 'pdf'){
			
			$pro_app_req_status = $this->config->item('pro_app_req_status');
			$Fetch_All_borrowername = $this->Fetch_All_borrowername();
			$getBorrowerContactName = $this->getBorrowerContactName();
			
			$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
			$pdf->SetCreator(PDF_CREATOR);
			// $pdf->SetHeaderData("include/icons/logo-light.png", 30, '', '');
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
			$pdf->SetMargins(2, 20, 2);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
			$pdf->SetFont('times', '', 7);
			$pdf->AddPage('L', 'A4');
			$header = '<h1 style="color:#003468;font-size:18px;"> POF Letter Status</h1>';
			$header .= '<style>
	                        .table {
	                            width: 100%;
	                            max-width: 100%;
	                            margin-bottom: 20px;
	                            border-collapse:collapse;
	                        }
	                        .table-bordered {
	                        border: 1px solid #ddd;
	                        }
	                        table {
	                            border-spacing: 0;
	                            border-collapse: collapse;
	                        }
	                        .table td{
	                            height : 25px;
	                            font-size:12px;
	                        }
	                        tr.table_header th{
	                            height:25px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:center;
	                            background-color:#bfcfe3;
	                        }

	                        tr.table_bottom th{
	                            height:20px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:left;
	                            background-color:#bfcfe3;
	                        }
	                        tr.odd td{
	                            background-color:#ededed;
	                        }
	                        </style>
	                        ';

			$header .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';
			$header .= '<tr class="table_header">';
			
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Borrower Name</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Contact<br>Name</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Contact<br>Email</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Contact<br>Phone</strong></th>';
			$header .= '<th style="width:15%;text-decoration:underline;"><strong>Street Address</strong></th>';
			$header .= '<th style="width:5%;text-decoration:underline;"><strong>Unit #</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>City</strong></th>';
			$header .= '<th style="width:5%;text-decoration:underline;"><strong>State</strong></th>';
			$header .= '<th style="width:5%;text-decoration:underline;"><strong>Zip</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Approved Amount</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Approval Status</strong></th>';
			
			$header .= '</tr>';

			$number = 0;
			
			foreach ($fetch_pro_app as $row) {

				if ($number % 2 == 0) {
					$class_add = "even";
				} else if ($number == 0) {
					$class_add = "even";
				} else {
					$class_add = "odd";
				}
				
				if($row->req_status !=''){
					$status = $pro_app_req_status[$row->req_status];
				}else{
					$status = '';
				}

				$header .= '<tr class="' . $class_add . '">';
				$header .= '<td>' . $Fetch_All_borrowername[$row->borrower] . '</td>';
				$header .= '<td>' . $getBorrowerContactName['full_name'][$row->borrower] . '</td>';
				$header .= '<td>' . $getBorrowerContactName['contact_email'][$row->borrower] . '</td>';
				$header .= '<td>' . $getBorrowerContactName['contact_phone'][$row->borrower] . '</td>';
				$header .= '<td>' . $row->pro_address .'</td>';
				$header .= '<td>' . $row->pro_unit . '</td>';
				$header .= '<td>' . $row->pro_city. '</td>';
				$header .= '<td>' . $row->pro_state . '</td>';
				$header .= '<td>' . $row->pro_zip. '</td>';
				$header .= '<td>$' . number_format($row->ApprovalAmount) . '</td>';
				$header .= '<td>' . $status . '</td>';

				$header .= '</tr>';
				$number++;
			}

			$header .= '<tr class="table_bottom">';
			$header .= '<th><strong>Total: ' . $number . '</strong></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			
			$header .= '</tr>';

			$header .= '</table>';

			$pdf->WriteHTML($header);
			$pdf->Output("pof_letter_status.pdf", "D");
		
		}else{

			$data['content'] = $this->load->view('Reports/pof_letter_status', $data, true);
			$this->load->view('template_files/template_reports', $data);
		}
	}		
		
	public function lender_approvals(){
		error_reporting(0);
		$loan_status = $this->input->post('loan_status');
		if($loan_status !=''){
			$condition = "WHERE ls.loan_status = '".$loan_status."'";
		}else{
			$condition = "WHERE ls.loan_status = '2' AND la.LP_status = '1'";
		}

		$data['loan_status'] = $loan_status;
		
		$lenderAppSql = $this->User_model->query("SELECT l.id as loan_id, l.borrower, l.loan_amount, lp.property_address, lp.unit, lp.city, lp.state, lp.zip, la.LP_status, la.LP_request FROM `lender_approval` as la JOIN loan as l ON l.talimar_loan = la.talimar_loan JOIN loan_property as lp On l.talimar_loan = lp.talimar_loan JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan ".$condition." group by l.talimar_loan");
		
		$lenderAppSql = $lenderAppSql->result();
		$lender_app_data = $lenderAppSql;
		
		$data['lender_app_data'] = $lender_app_data;
		$data['Fetch_All_borrowername'] = $this->Fetch_All_borrowername();
		
		$reportDisplay = $this->input->post('reportDisplay');
		if($reportDisplay == 'pdf'){
			
			$Fetch_All_borrowername = $this->Fetch_All_borrowername();
			$lender_app_status = $this->config->item('lender_app_status');
			$lender_app_request = $this->config->item('lender_app_request');
			
			$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
			$pdf->SetCreator(PDF_CREATOR);
			// $pdf->SetHeaderData("include/icons/logo-light.png", 30, '', '');
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
			$pdf->SetMargins(2, 20, 2);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
			$pdf->SetFont('times', '', 7);
			$pdf->AddPage('L', 'A4');
			$header = '<h1 style="color:#003468;font-size:18px;"> Active Lender Approvals</h1>';
			$header .= '<style>
	                        .table {
	                            width: 100%;
	                            max-width: 100%;
	                            margin-bottom: 20px;
	                            border-collapse:collapse;
	                        }
	                        .table-bordered {
	                        border: 1px solid #ddd;
	                        }
	                        table {
	                            border-spacing: 0;
	                            border-collapse: collapse;
	                        }
	                        .table td{
	                            height : 25px;
	                            font-size:12px;
	                        }
	                        tr.table_header th{
	                            height:25px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:center;
	                            background-color:#bfcfe3;
	                        }

	                        tr.table_bottom th{
	                            height:20px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:left;
	                            background-color:#bfcfe3;
	                        }
	                        tr.odd td{
	                            background-color:#ededed;
	                        }
	                        </style>
	                        ';

			$header .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';
			$header .= '<tr class="table_header">';

			$header .= '<th style="width:20%;text-decoration:underline;"><strong>Property Address</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Unit#</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>City</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>State</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Zip</strong></th>';
			$header .= '<th style="width:20%;text-decoration:underline;"><strong>Borrower Name</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Reason</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Status</strong></th>';
			
			$header .= '</tr>';

			$number = 0;
			
			foreach ($lender_app_data as $row) {

				if ($number % 2 == 0) {
					$class_add = "even";
				} else if ($number == 0) {
					$class_add = "even";
				} else {
					$class_add = "odd";
				}

				$header .= '<tr class="' . $class_add . '">';
				$header .= '<td>' . $row->property_address .'</td>';
				$header .= '<td>' . $row->unit . '</td>';
				$header .= '<td>' . $row->city. '</td>';
				$header .= '<td>' . $row->state . '</td>';
				$header .= '<td>' . $row->zip. '</td>';
				$header .= '<td>' . $Fetch_All_borrowername[$row->borrower] . '</td>';
				$header .= '<td>' . $lender_app_request[$row->LP_request] .'</td>';
				$header .= '<td>' . $lender_app_status[$row->LP_status] . '</td>';

				$header .= '</tr>';
				$number++;
			}

			$header .= '<tr class="table_bottom">';
			$header .= '<th><strong>Total: ' . $number . '</strong></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '</tr>';

			$header .= '</table>';

			$pdf->WriteHTML($header);
			$pdf->Output("lender_app_req.pdf", "D");		
		
		}else{

			$data['content'] = $this->load->view('Reports/lender_approvals_req', $data, true);
			$this->load->view('template_files/template_reports', $data);
		}
	}
	
	public function LenderMarketingReport(){
	    error_reporting(0);

	    $LenderAccount 	= $this->input->post('LenderAccount');
	    $emailnoti 		= $this->input->post('emailnoti');
	    $textnoti 		= $this->input->post('textnoti');


	    $lender_mar_data = $this->User_model->query("SELECT * FROM contact_tags WHERE contact_specialty = '16' GROUP BY contact_id ");
	    //$lender_mar_data = $this->User_model->query("SELECT * FROM contact as c JOIN contact_tags as ct ON c.contact_id = ct.contact_id JOIN lender_contact_type as lct ON c.contact_id = lct.contact_id JOIN lender_contact as lc ON c.contact_id = lc.contact_id WHERE ct.contact_specialty = '16' GROUP BY c.contact_id");
	    if($lender_mar_data->num_rows() > 0){
	        $lender_market_data = $lender_mar_data->result();
	        foreach ($lender_market_data as $value) {
	        	
	        	$fetchcontactetails = $this->User_model->query("SELECT contact_firstname, contact_lastname, contact_id, contact_phone, contact_email FROM contact WHERE contact_id = '".$value->contact_id."'");
	        	$fetchcontactetails = $fetchcontactetails->result();

	        	if($fetchcontactetails[0]->contact_email || $fetchcontactetails[0]->contact_firstname || $fetchcontactetails[0]->contact_lastname !=''){

	        		$fetchlenderdata = $this->User_model->query("SELECT link_to_lender FROM lender_contact WHERE contact_id = '".$value->contact_id."' GROUP BY contact_id");
	        		if($fetchlenderdata->num_rows() > 0){
	        			$fetchlenderdata = $fetchlenderdata->result();
	        			$lenderlink = $fetchlenderdata[0]->link_to_lender;
	        			$lenderaccount = 'Yes';
	        		}else{
	        			$lenderaccount = 'No';
	        			$lenderlink = '';
	        		}

	        		$fetchportal_login = $this->User_model->query("SELECT `date` FROM `portal_login_details` WHERE `p_user_id` = '".$value->contact_id."' ORDER BY date desc");
	        		if($fetchportal_login->num_rows() > 0){
	        			$fetchportal_login = $fetchportal_login->result();
	        			$portallogin = date('m-d-Y', strtotime($fetchportal_login[0]->date));
	        		}else{
	        			$portallogin = 'None';
	        		}
	        		
	        		$lender_mar_datadd = $this->User_model->query("SELECT contact_id,contact_email_blast FROM contact_tags WHERE contact_id = '".$value->contact_id."'");
	        		$lender_mar_datadd = $lender_mar_datadd->result();
	        		foreach ($lender_mar_datadd as $val) {
	        			$lender_mar_dataddff[$val->contact_id][] = $val->contact_email_blast;
	        		}

	        		if($LenderAccount == 1){
	        			if($lenderaccount == 'Yes'){
		        			$lender_market_datass[] = array(
		        								"contact_id" => $fetchcontactetails[0]->contact_id,
		        								"contact_firstname" => $fetchcontactetails[0]->contact_firstname,
		        								"contact_lastname" => $fetchcontactetails[0]->contact_lastname,
		        								"contact_phone" => $fetchcontactetails[0]->contact_phone,
		        								"contact_email" => $fetchcontactetails[0]->contact_email,
		        								"link_to_lender" => $lenderlink,
		        								"lenderaccount" => $lenderaccount,
		        								"portal_login" => $portallogin,
		        								
		        							);
	        			}
		        	}elseif($LenderAccount == 2){
		        		if($lenderaccount == 'No'){
		        			$lender_market_datass[] = array(
		        								"contact_id" => $fetchcontactetails[0]->contact_id,
		        								"contact_firstname" => $fetchcontactetails[0]->contact_firstname,
		        								"contact_lastname" => $fetchcontactetails[0]->contact_lastname,
		        								"contact_phone" => $fetchcontactetails[0]->contact_phone,
		        								"contact_email" => $fetchcontactetails[0]->contact_email,
		        								"link_to_lender" => $lenderlink,
		        								"lenderaccount" => $lenderaccount,
		        								"portal_login" => $portallogin,
		        								
		        							);
		        		}
		        	}else{
		        			$lender_market_datass[] = array(
		        								"contact_id" => $fetchcontactetails[0]->contact_id,
		        								"contact_firstname" => $fetchcontactetails[0]->contact_firstname,
		        								"contact_lastname" => $fetchcontactetails[0]->contact_lastname,
		        								"contact_phone" => $fetchcontactetails[0]->contact_phone,
		        								"contact_email" => $fetchcontactetails[0]->contact_email,
		        								"link_to_lender" => $lenderlink,
		        								"lenderaccount" => $lenderaccount,
		        								"portal_login" => $portallogin,
		        								
		        							);
		        	}


		        }
		    }

	        $lender_market_data = $lender_market_datass;
	        $lender_mar_datadd = $lender_mar_dataddff;
	        
	    }else{
	        $lender_market_data = '';
	        $lender_mar_datadd = '';
	    }
	    
	    $reportDisplay = $this->input->post('reportDisplay');
		if($reportDisplay == 'pdf'){

			$link_to_lender = $this->config->item('link_to_lender');

			$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
			$pdf->SetCreator(PDF_CREATOR);
			// $pdf->SetHeaderData("include/icons/logo-light.png", 30, '', '');
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
			$pdf->SetMargins(2, 20, 2);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
			$pdf->SetFont('times', '', 7);
			$pdf->AddPage('L', 'A4');
			$header = '<h1 style="color:#003468;font-size:18px;"> Lender Marketing Report</h1>';
			$header .= '<style>
	                        .table {
	                            width: 100%;
	                            max-width: 100%;
	                            margin-bottom: 20px;
	                            border-collapse:collapse;
	                        }
	                        .table-bordered {
	                        border: 1px solid #ddd;
	                        }
	                        table {
	                            border-spacing: 0;
	                            border-collapse: collapse;
	                        }
	                        .table td{
	                            height : 25px;
	                            font-size:12px;
	                        }
	                        tr.table_header th{
	                            height:25px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:center;
	                            background-color:#bfcfe3;
	                        }

	                        tr.table_bottom th{
	                            height:20px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:left;
	                            background-color:#bfcfe3;
	                        }
	                        tr.odd td{
	                            background-color:#ededed;
	                        }
	                        </style>
	                        ';

			$header .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';
			$header .= '<tr class="table_header">';

			$header .= '<th style="width:10%;text-decoration:underline;"><strong>First Name</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Last Name</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Phone</strong></th>';
			$header .= '<th style="width:20%;text-decoration:underline;"><strong>E-mail</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Lender Account</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>E-Mail<br>Notification</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Text<br>Notification</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Lender<br>Portal</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Last Login<br>Date</strong></th>';
			
			$header .= '</tr>';

			$number = 0;
			
			foreach ($lender_market_data as $row) {

				if ($number % 2 == 0) {
					$class_add = "even";
				} else if ($number == 0) {
					$class_add = "even";
				} else {
					$class_add = "odd";
				}

				if(in_array(2, $lender_mar_datadd[$row['contact_id']])){
					$email_noti = 'Yes';
				}else{
					$email_noti = 'No';
				}

				if(in_array(9, $lender_mar_datadd[$row['contact_id']])){
					$text_noti = 'Yes';
				}else{
					$text_noti = 'No';
				}

				if($emailnoti == 1){ if($email_noti == 'Yes'){

					$header .= '<tr class="' . $class_add . '">';
					$header .= '<td>' . $row['contact_firstname'] .'</td>';
					$header .= '<td>' . $row['contact_lastname'] . '</td>';
					$header .= '<td>' . $row['contact_phone']. '</td>';
					$header .= '<td>' . $row['contact_email'] . '</td>';
					$header .= '<td>' . $row['lenderaccount']. '</td>';
					$header .= '<td>'.$email_noti.'</td>';
					$header .= '<td>'.$text_noti.'</td>';
					$header .= '<td>' . $link_to_lender[$row['link_to_lender']] . '</td>';
					$header .= '<td>' . $row['portal_login'] . '</td>';

					$header .= '</tr>';
					$number++;

				} }elseif($emailnoti == 2){ if($email_noti == 'No'){

					$header .= '<tr class="' . $class_add . '">';
					$header .= '<td>' . $row['contact_firstname'] .'</td>';
					$header .= '<td>' . $row['contact_lastname'] . '</td>';
					$header .= '<td>' . $row['contact_phone']. '</td>';
					$header .= '<td>' . $row['contact_email'] . '</td>';
					$header .= '<td>' . $row['lenderaccount']. '</td>';
					$header .= '<td>'.$email_noti.'</td>';
					$header .= '<td>'.$text_noti.'</td>';
					$header .= '<td>' . $link_to_lender[$row['link_to_lender']] . '</td>';
					$header .= '<td>' . $row['portal_login'] . '</td>';

					$header .= '</tr>';
					$number++;

				} }

				//for text notification filter...
				if($textnoti == 1){ if($text_noti == 'Yes'){

					$header .= '<tr class="' . $class_add . '">';
					$header .= '<td>' . $row['contact_firstname'] .'</td>';
					$header .= '<td>' . $row['contact_lastname'] . '</td>';
					$header .= '<td>' . $row['contact_phone']. '</td>';
					$header .= '<td>' . $row['contact_email'] . '</td>';
					$header .= '<td>' . $row['lenderaccount']. '</td>';
					$header .= '<td>'.$email_noti.'</td>';
					$header .= '<td>'.$text_noti.'</td>';
					$header .= '<td>' . $link_to_lender[$row['link_to_lender']] . '</td>';
					$header .= '<td>' . $row['portal_login'] . '</td>';

					$header .= '</tr>';
					$number++;

				} }elseif($textnoti == 2){ if($text_noti == 'No'){

					$header .= '<tr class="' . $class_add . '">';
					$header .= '<td>' . $row['contact_firstname'] .'</td>';
					$header .= '<td>' . $row['contact_lastname'] . '</td>';
					$header .= '<td>' . $row['contact_phone']. '</td>';
					$header .= '<td>' . $row['contact_email'] . '</td>';
					$header .= '<td>' . $row['lenderaccount']. '</td>';
					$header .= '<td>'.$email_noti.'</td>';
					$header .= '<td>'.$text_noti.'</td>';
					$header .= '<td>' . $link_to_lender[$row['link_to_lender']] . '</td>';
					$header .= '<td>' . $row['portal_login'] . '</td>';

					$header .= '</tr>';
					$number++;

				} }else{

					$header .= '<tr class="' . $class_add . '">';
					$header .= '<td>' . $row['contact_firstname'] .'</td>';
					$header .= '<td>' . $row['contact_lastname'] . '</td>';
					$header .= '<td>' . $row['contact_phone']. '</td>';
					$header .= '<td>' . $row['contact_email'] . '</td>';
					$header .= '<td>' . $row['lenderaccount']. '</td>';
					$header .= '<td>'.$email_noti.'</td>';
					$header .= '<td>'.$text_noti.'</td>';
					$header .= '<td>' . $link_to_lender[$row['link_to_lender']] . '</td>';
					$header .= '<td>' . $row['portal_login'] . '</td>';

					$header .= '</tr>';
					$number++;
				}
			}

			$header .= '<tr class="table_bottom">';
			$header .= '<th><strong>Total: ' . $number . '</strong></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '</tr>';

			$header .= '</table>';

			$pdf->WriteHTML($header);
			$pdf->Output("lender_market_report.pdf", "D");
	    
	    }else{

	    	$data['LenderAccount']	= $LenderAccount;
	    	$data['emailnoti']	= $emailnoti;
	    	$data['textnoti']	= $textnoti;
 
	        $data['lender_market_data'] = $lender_market_data;
	        $data['lender_mar_datadd'] = $lender_mar_datadd;
	        
			$data['content'] = $this->load->view('Reports/lender_market_report', $data, true);
			$this->load->view('template_files/template_reports', $data);
		}
	}
	
	public function AutoPaymentReport(){

		
		if($this->input->post('autopayment') != ''){
			$conditionAP = "WHERE ls.ach_activated = '".$this->input->post('autopayment')."' AND ls.loan_status = '2'";
		}else{
			$conditionAP = " WHERE ls.loan_status = '2'";
		}

		$data['auto_payment'] = $this->input->post('autopayment');
		
		$autopaymentquery = $this->User_model->query("
			SELECT *, l.id as loan_id, ls.ach_activated, bc.borrower_id, bd.b_name as borrower_name,c.contact_firstname, c.contact_lastname, c.contact_email, c.contact_phone FROM loan as l 
			JOIN loan_property as lp ON l.talimar_loan = lp.talimar_loan 
			JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan 
			JOIN borrower_data as bd ON l.borrower = bd.id 
			JOIN borrower_contact as bc ON bd.id = bc.borrower_id  
			JOIN contact as c ON bc.contact_id = c.contact_id 
			".$conditionAP." group by l.talimar_loan");
		if($autopaymentquery->num_rows() > 0){

			$autopaymentquery = $autopaymentquery->result();
			$data['AutoPaymentReport'] = $autopaymentquery;

		}else{
			$data['AutoPaymentReport'] = '';
		}
		

		$reportDisplay = $this->input->post('reportDisplay');
		if($reportDisplay == 'pdf'){
			$ach_activated = $this->config->item('ach_activated');

			$yes_no_option3 = $this->config->item('yes_no_option3');
			
			
			$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
			$pdf->SetCreator(PDF_CREATOR);
			// $pdf->SetHeaderData("include/icons/logo-light.png", 30, '', '');
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
			$pdf->SetMargins(2, 20, 2);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
			$pdf->SetFont('times', '', 7);
			$pdf->AddPage('L', 'A4');
			$header = '<h1 style="color:#003468;font-size:18px;"> Auto Payment Report</h1>';
			$header .= '<style>
	                        .table {
	                            width: 100%;
	                            max-width: 100%;
	                            margin-bottom: 20px;
	                            border-collapse:collapse;
	                        }
	                        .table-bordered {
	                        border: 1px solid #ddd;
	                        }
	                        table {
	                            border-spacing: 0;
	                            border-collapse: collapse;
	                        }
	                        .table td{
	                            height : 25px;
	                            font-size:12px;
	                        }
	                        tr.table_header th{
	                            height:25px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:center;
	                            background-color:#bfcfe3;
	                        }

	                        tr.table_bottom th{
	                            height:20px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:left;
	                            background-color:#bfcfe3;
	                        }
	                        tr.odd td{
	                            background-color:#ededed;
	                        }
	                        </style>
	                        ';

			$header .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';
			$header .= '<tr class="table_header">';

			$header .= '<th style="width:15%;text-decoration:underline;"><strong>Property Address</strong></th>';
			$header .= '<th style="width:5%;text-decoration:underline;"><strong>Unit#</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>City</strong></th>';
			$header .= '<th style="width:5%;text-decoration:underline;"><strong>State</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Zip</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Borrower Name</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Contact 1<br>Name</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Contact 1<br>Email</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Contact 1<br>Phone</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Payment Amount</strong></th>';
			$header .= '<th style="width:5%;text-decoration:underline;"><strong>ACH</strong></th>';
			
			$header .= '</tr>';

			$number = 0;
			
			foreach ($autopaymentquery as $rowsss) {

				if ($number % 2 == 0) {
					$class_add = "even";
				} else if ($number == 0) {
					$class_add = "even";
				} else {
					$class_add = "odd";
				}

				$talimar_no['talimar_loan'] = $rowsss->talimar_loan;

				$fetch_loan_data = $this->User_model->select_where('loan', $talimar_no);
				$fetch_loan_data = $fetch_loan_data->result();

				$fetch_loanservicing = $this->User_model->select_where('loan_servicing', $talimar_no);
				$fetch_loanservicing = $fetch_loanservicing->result();

				$fetch_impound_account1 = $this->User_model->select_where_asc('impound_account', $talimar_no, 'position');
				if ($fetch_impound_account1->num_rows() > 0) {
					$fetch_impound_account = $fetch_impound_account1->result();
				}

				$fetch_property_sql = "SELECT * FROM loan_property JOIN property_home ON property_home.id = loan_property.property_home_id WHERE loan_property.talimar_loan = '" . $rowsss->talimar_loan . "' AND  property_home.primary_property = '1' ";
				$fetch_property_data = $this->User_model->query($fetch_property_sql);
				$fetch_property_data = $fetch_property_data->result();

				$aa = 0;				
				if(isset($fetch_impound_account)){
					
					foreach($fetch_impound_account as $key=>$row){
						$impound_amount = $row->amount;
						$readonly = '';
												
						if($row->items == 'Mortgage Payment')
						{
								 $aa +=($fetch_loan_data[0]->current_balance*($fetch_loan_data[0]->intrest_rate)/100)/12;						
								// $aa +=($outstanding_balance*$fetch_loan_result[0]->intrest_rate/100)/12;						
							// $aa += ($fetch_loan_result[0]->intrest_rate/100) * $fetch_loan_result[0]->loan_amount / 12;
						}
						if($row->items == 'Property / Hazard Insurance Payment')
						{
							if($row->impounded == 1){
								$aa += isset($loan_property_result) ? ($loan_property_insurance_result->annual_insurance_payment / 12) : 0;
							}
						}
												
						if($row->items == 'County Property Taxes')
						{
							if($row->impounded == 1){
								$aa += isset($loan_property_result) ? ($loan_property_result[0]->annual_property_taxes / 12) : 0;
							}
						}
						if($row->items == 'Flood Insurance')
						{
							if($row->impounded == 1){
								$aa += isset($row->amount) ? ($row->amount) : 0;
							}
						}
												
						if($row->items == 'Mortgage Insurance')
						{
							if($row->impounded == 1){
								$aa += isset($row->amount) ? ($row->amount) : 0;
							}
						}
					}
				}

				$header .= '<tr class="' . $class_add . '">';
				$header .= '<td>' . $rowsss->property_address .'</td>';
				$header .= '<td>' . $rowsss->unit . '</td>';
				$header .= '<td>' . $rowsss->city. '</td>';
				$header .= '<td>' . $rowsss->state . '</td>';
				$header .= '<td>' . $rowsss->zip. '</td>';
				if($rowsss->borrower_id > 0){
					$header .= '<td>' . $rowsss->borrower_name . '</td>';		
					$header .= '<td>' . $rowsss->contact_firstname ." ".$rowsss->contact_lastname. '</td>';
					$header .= '<td>' . $rowsss->contact_email . '</td>';
					$header .= '<td>' . $rowsss->contact_phone . '</td>';
				}else{
					$header .= '<td></td>';
					$header .= '<td></td>';
					$header .= '<td></td>';
					$header .= '<td></td>';
				}				
				
				$header .= '<td>$'.number_format($aa,2).'</td>';
				$header .= '<td>' . $ach_activated[$fetch_loanservicing[0]->ach_activated] . '</td>';

				$header .= '</tr>';
				$number++;
			}

			$header .= '<tr class="table_bottom">';
			$header .= '<th><strong>Total: ' . $number . '</strong></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			
			$header .= '</tr>';

			$header .= '</table>';

			$pdf->WriteHTML($header);
			$pdf->Output("auto_payment_report.pdf", "D");

		}else{

			$data['content'] = $this->load->view('Reports/autopaymentreport', $data, true);
			$this->load->view('template_files/template_reports', $data);
		}

	}

	public function fix_flip_portfolio(){
		error_reporting(0);
		$fetch_fixflip_loans = $this->User_model->query("SELECT *, l.id as loan_id FROM loan as l JOIN loan_servicing as ls on l.talimar_loan = ls.talimar_loan JOIN loan_property as lp ON l.talimar_loan = lp.talimar_loan WHERE ls.loan_status IN ('1','2','3') GROUP BY l.talimar_loan");
		$fetch_fixflip_loans = $fetch_fixflip_loans->result();
		foreach ($fetch_fixflip_loans as $key => $value) {

			$fetchAssignment = $this->User_model->query("SELECT * FROM `loan_source_and_uses` WHERE `talimar_loan` = '".$value->talimar_loan."' AND `items` IN('Purchase Price','Construction Costs')");
			$fetchAssignment = $fetchAssignment->result();
			foreach ($fetchAssignment as $value1) {

				if($value1->project_cost < 1){
					$puchasedata[$value1->items] = 0.00;
				}else{
					$puchasedata[$value1->items] = ($value1->loan_funds / $value1->project_cost) * 100;
				}
			}

			$where['talimar_loan'] = $value->talimar_loan;
			$fetchAssignment = $this->User_model->select_where('loan_assigment',$where);
			if($fetchAssignment->num_rows() > 0){
				$loan_assigment_data = $fetchAssignment->result();
				$invester_yield_percent = $loan_assigment_data[0]->invester_yield_percent;
			}else{
				$invester_yield_percent = 0;
			}

			if($value->servicing_lender_rate == 'NaN' || $value->servicing_lender_rate == '' || $value->servicing_lender_rate == '0.00'){
				if($invester_yield_percent > 0){
					$lender_ratessss = $invester_yield_percent;
				}
			}else{
			    $lender_ratessss = $value->servicing_lender_rate;
			}
			
			$sql = "SELECT COUNT(*) as count_encum, SUM(current_balance) as sum_current_balance, existing_priority_to_talimar, original_balance FROM encumbrances WHERE property_home_id = '" . $value->proprty_home_id . "' AND talimar_loan = '" . $value->talimar_loan . "' AND will_it_remain = 0 ";
			$fetch_ecum_right = $this->User_model->query($sql);
			$fetch_ecum_right = $fetch_ecum_right->result();
			

			$LOAN_TO_CV = ($value->loan_amount + $fetch_ecum_right[0]->sum_current_balance) / $value->underwriting_value * 100;

				$fix_flip_array[] = array(
											"talimar_loan" => $value->talimar_loan,
											"loan_id" => $value->loan_id,
											"loan_amount" => $value->loan_amount,
											"city" => $value->city,
											"state" => $value->state,
											"property_link" => $value->property_link,
											"lender_ratessss" => number_format($lender_ratessss,3),
											"ltv" => $LOAN_TO_CV,
											"puchase" => $puchasedata['Purchase Price'],
											"construc" => $puchasedata['Construction Costs'],
										);

		}

		$fix_flip_arraydata = $fix_flip_array;

		$reportDisplay = $this->input->post('reportDisplay');
		if($reportDisplay == 'pdf'){

			$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
			$pdf->SetCreator(PDF_CREATOR);
			// $pdf->SetHeaderData("include/icons/logo-light.png", 30, '', '');
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
			$pdf->SetMargins(2, 20, 2);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
			$pdf->SetFont('times', '', 7);
			$pdf->AddPage('L', 'A4');
			$header = '<h1 style="color:#003468;font-size:18px;"> Fix & Flip Portfolio</h1>';
			$header .= '<style>
	                        .table {
	                            width: 100%;
	                            max-width: 100%;
	                            margin-bottom: 20px;
	                            border-collapse:collapse;
	                        }
	                        .table-bordered {
	                        border: 1px solid #ddd;
	                        }
	                        table {
	                            border-spacing: 0;
	                            border-collapse: collapse;
	                        }
	                        .table td{
	                            height : 25px;
	                            font-size:12px;
	                        }
	                        tr.table_header th{
	                            height:25px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:center;
	                            background-color:#bfcfe3;
	                        }

	                        tr.table_bottom th{
	                            height:20px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:left;
	                            background-color:#bfcfe3;
	                        }
	                        tr.odd td{
	                            background-color:#ededed;
	                        }
	                        </style>
	                        ';

			$header .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';
			$header .= '<tr class="table_header">';

			$header .= '<th style="width:10%;text-decoration:underline;"><strong>City</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>State</strong></th>';
			$header .= '<th style="width:15%;text-decoration:underline;"><strong>Loan Amount</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Rate</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>LTV</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>% of Purchase</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>% of Renovation</strong></th>';
			$header .= '<th style="width:15%;text-decoration:underline;"><strong>Loan #</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Property Details</strong></th>';
			
			$header .= '</tr>';

			$number = 0;
			foreach ($fix_flip_arraydata as $rowsss) {

				if ($number % 2 == 0) {
					$class_add = "even";
				} else if ($number == 0) {
					$class_add = "even";
				} else {
					$class_add = "odd";
				}

				if($rowsss['property_link'] != ''){
					$linkurl = $rowsss['property_link'];
				}else{
					$linkurl = '#';
				}

				
				$header .= '<tr class="' . $class_add . '">';
				$header .= '<td>' . $rowsss['city']. '</td>';
				$header .= '<td>' . $rowsss['state']. '</td>';
				$header .= '<td>$' . number_format($rowsss['loan_amount'],2). '</td>';
				$header .= '<td>' . number_format($rowsss['lender_ratessss'],3). '%</td>';
				$header .= '<td>' . number_format($rowsss['ltv'],2). '%</td>';
				$header .= '<td>' . number_format($rowsss['puchase'],2). '%</td>';
				$header .= '<td>' . number_format($rowsss['construc'],2). '%</td>';
				$header .= '<td>' . $rowsss['talimar_loan']. '</td>';
				$header .= '<td><a target="_blank" href="'.$linkurl.'">Link</a></td>';
				
				$header .= '</tr>';
				$number++;
			}

			$header .= '<tr class="table_bottom">';
			$header .= '<th><strong>Total: ' . $number . '</strong></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
						
			$header .= '</tr>';

			$header .= '</table>';

			$pdf->WriteHTML($header);
			$pdf->Output("fixflip_portfolio.pdf", "D");

		}else{

			$data['fix_flip_portfolio'] = $fix_flip_arraydata;
			$data['content'] = $this->load->view('Reports/fixflip_portfolio', $data, true);
			$this->load->view('template_files/template_reports', $data);
		}
	}

	public function LenderAccountType(){
		error_reporting(0);
		$AccountType = $this->input->post('AccountType');
		$ActiveLoans = $this->input->post('ActiveLoans');
		$PaidOffLoans = $this->input->post('PaidOffLoans');


		if($AccountType != ''){
			$condition = "Where i.investor_type = '".$AccountType."'";
		}else{
			$condition = "";
		}

		$data['AccountType'] = $AccountType;
		$data['ActiveLoans'] = $ActiveLoans;
		$data['PaidOffLoans'] = $PaidOffLoans;

		$fetchlender = $this->User_model->query("SELECT *, i.id as lid FROM investor as i JOIN lender_contact as lc ON i.id = lc.lender_id ".$condition." GROUP BY i.id ORDER BY i.id DESC");
		if($fetchlender->num_rows() > 0){

			$fetchlender = $fetchlender->result();

			foreach ($fetchlender as $key => $value) {

				$lender_total_contact = $this->lender_total_contact($value->lid);
				$lender_total_data = $this->lender_total_data($value->lid);


				if($value->investor_type == 3){
					$custodian = $value->directed_IRA_NAME;
				}else{
					$custodian = '';
				}


				if($ActiveLoans || $PaidOffLoans !=''){

					//Acrive loan
					if($ActiveLoans == 1){

						if($lender_total_data['countActive'] > 0){
				
							$LenderAccountType[] = array(
									"lid" => $value->lid,
									"name" => $value->name,
									"type" => $value->investor_type,
									"custodian" => $custodian,
									"totalcontact" => $lender_total_contact,
									"countActive" => $lender_total_data['countActive'],
									"AmountActive" => $lender_total_data['AmountActive'],
									"countPaid" => $lender_total_data['countPaid'],
									"AmountPaid" => $lender_total_data['AmountPaid'],
								);
						}

					}elseif($ActiveLoans == 2){

						if($lender_total_data['countActive'] <= 0){
				
							$LenderAccountType[] = array(
									"lid" => $value->lid,
									"name" => $value->name,
									"type" => $value->investor_type,
									"custodian" => $custodian,
									"totalcontact" => $lender_total_contact,
									"countActive" => $lender_total_data['countActive'],
									"AmountActive" => $lender_total_data['AmountActive'],
									"countPaid" => $lender_total_data['countPaid'],
									"AmountPaid" => $lender_total_data['AmountPaid'],
								);
						}
					}

					//Paid Off loans
					if($PaidOffLoans == 1){

						if($lender_total_data['countPaid'] > 0){
				
							$LenderAccountType[] = array(
									"lid" => $value->lid,
									"name" => $value->name,
									"type" => $value->investor_type,
									"custodian" => $custodian,
									"totalcontact" => $lender_total_contact,
									"countActive" => $lender_total_data['countActive'],
									"AmountActive" => $lender_total_data['AmountActive'],
									"countPaid" => $lender_total_data['countPaid'],
									"AmountPaid" => $lender_total_data['AmountPaid'],
								);
						}

					}elseif($PaidOffLoans == 2){

						if($lender_total_data['countPaid'] <= 0){
				
							$LenderAccountType[] = array(
									"lid" => $value->lid,
									"name" => $value->name,
									"type" => $value->investor_type,
									"custodian" => $custodian,
									"totalcontact" => $lender_total_contact,
									"countActive" => $lender_total_data['countActive'],
									"AmountActive" => $lender_total_data['AmountActive'],
									"countPaid" => $lender_total_data['countPaid'],
									"AmountPaid" => $lender_total_data['AmountPaid'],
								);
						}
					}

				}else{  //Main else part

					$LenderAccountType[] = array(
								"lid" => $value->lid,
								"name" => $value->name,
								"type" => $value->investor_type,
								"custodian" => $custodian,
								"totalcontact" => $lender_total_contact,
								"countActive" => $lender_total_data['countActive'],
								"AmountActive" => $lender_total_data['AmountActive'],
								"countPaid" => $lender_total_data['countPaid'],
								"AmountPaid" => $lender_total_data['AmountPaid'],
							);
				}
			}

			$data['LenderAccountType'] = $LenderAccountType;
		}else{
			$data['LenderAccountType'] = '';
		}

		$reportDisplay = $this->input->post('reportDisplay');
		if($reportDisplay == 'pdf'){

			$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
			$pdf->SetCreator(PDF_CREATOR);
			// $pdf->SetHeaderData("include/icons/logo-light.png", 30, '', '');
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
			$pdf->SetMargins(2, 20, 2);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
			$pdf->SetFont('times', '', 7);
			$pdf->AddPage('L', 'A4');
			$header = '<h1 style="color:#003468;font-size:18px;"> Lender Account Type</h1>';
			$header .= '<style>
	                        .table {
	                            width: 100%;
	                            max-width: 100%;
	                            margin-bottom: 20px;
	                            border-collapse:collapse;
	                        }
	                        .table-bordered {
	                        border: 1px solid #ddd;
	                        }
	                        table {
	                            border-spacing: 0;
	                            border-collapse: collapse;
	                        }
	                        .table td{
	                            height : 25px;
	                            font-size:12px;
	                        }
	                        tr.table_header th{
	                            height:25px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:center;
	                            background-color:#bfcfe3;
	                        }

	                        tr.table_bottom th{
	                            height:20px;
	                            font-size:12px;
	                            font-family: "Times New Roman", Georgia, Serif;
	                            text-align:left;
	                            background-color:#bfcfe3;
	                        }
	                        tr.odd td{
	                            background-color:#ededed;
	                        }
	                        </style>
	                        ';

			$header .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';
			$header .= '<tr class="table_header">';

			$header .= '<th style="width:20%;text-decoration:underline;"><strong>Account Name</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong># of<br>Contact</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong>Account Type</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong>Custodian</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong>Active (#)</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong>Active ($)</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Paid Off (#)</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Paid Off ($)</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Total (#)</strong></th>';
			$header .= '<th style="width:10%;text-decoration:underline;"><strong>Total ($)</strong></th>';
			
			$header .= '</tr>';

			$number = 0;
			$hash_AmountPaid=0;
			$hash_countPaid=0;
			$hash_active_loan=0;
			$amount_active_loan=0;

			$investor_type_option = $this->config->item('investor_type_option');
			
			foreach ($LenderAccountType as $row) {

				if ($number % 2 == 0) {
					$class_add = "even";
				} else if ($number == 0) {
					$class_add = "even";
				} else {
					$class_add = "odd";
				}

				$header .= '<tr class="' . $class_add . '">';
				$header .= '<td>' . $row["name"] .'</td>';
				$header .= '<td>' . $row["totalcontact"] . '</td>';
				$header .= '<td>' . $investor_type_option[$row['type']] . '</td>';
				$header .= '<td>' . $row['custodian'] . '</td>';
				$header .= '<td>' . $row['countActive']. '</td>';
				$header .= '<td>$' . number_format($row['AmountActive']) . '</td>';
				$header .= '<td>' . $row['countPaid'] . '</td>';
				$header .= '<td>$' .number_format($row['AmountPaid']) . '</td>';
				$header .= '<td>$' . number_format($row['countActive'] + $row['countPaid']) . '</td>';
				$header .= '<td>$' . number_format($row['AmountActive'] + $row['AmountPaid']) . '</td>';

				$header .= '</tr>';
				$number++;

				$hash_active_loan += $row['countActive'];
				$amount_active_loan += $row['AmountActive'];
				$hash_countPaid += $row['countPaid'];
				$hash_AmountPaid += $row['AmountPaid'];
			}

			$header .= '<tr class="table_bottom">';
			$header .= '<th><strong>Total: ' . $number . '</strong></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th>'.number_format($hash_active_loan).'</th>';
			$header .= '<th>$'.number_format($amount_active_loan).'</th>';
			$header .= '<th>'.number_format($hash_countPaid).'</th>';
			$header .= '<th>$'.number_format($hash_AmountPaid).'</th>';
			$header .= '<th>'.number_format($hash_active_loan + $hash_countPaid).'</th>';
			$header .= '<th>$'.number_format($amount_active_loan + $hash_AmountPaid).'</th>';
			$header .= '</tr>';

			$header .= '<tr class="table_bottom">';
			$header .= '<th><strong>Average:</strong></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th></th>';
			$header .= '<th>$'.number_format($amount_active_loan/$number).'</th>';
			$header .= '<th></th>';
			$header .= '<th>$'.number_format($hash_AmountPaid/$number).'</th>';
			$header .= '<th></th>';
			$header .= '<th>$'.number_format(($amount_active_loan + $hash_AmountPaid)/$number).'</th>';
			$header .= '</tr>';

			$header .= '</table>';

			$pdf->WriteHTML($header);
			$pdf->Output("lender_account_type.pdf", "D");


		}else{
			$data['content'] = $this->load->view('Reports/LenderAccountType', $data, true);
			$this->load->view('template_files/template_reports', $data);
		}
	}
	public function get_contact_investment($contactID){

		$check_contact_funds = $this->User_model->query("SELECT `invest_per_deed` FROM `lender_contact_type` WHERE contact_id='".$contactID."'");
		if($check_contact_funds->num_rows() > 0){
			$check_contact_funds = $check_contact_funds->result();
			$check_contact_funds = $check_contact_funds[0]->invest_per_deed;
		}else{
			$check_contact_funds = '';
		}

		return $check_contact_funds;	
	}


}

?>