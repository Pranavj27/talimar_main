<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error_page extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('User_model');
		$this->load->library('Aws3','aws3');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');
		if($this->session->userdata('t_user_id') != '' )
		{
		}
		else
		{
			redirect(base_url()."home",'refresh');
		}
	}
	
	public function index()
	{
		
		$this->load->view('404_error');
		
	}
	
	
	
	 
}


