<?php 
class Student extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('Aws3','aws3');
		$this->load->helper('aws3_helper.php');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->database();
		$this->load->helper(array('form', 'url'));
		$this->load->model('User_model');
		$this->load->library('MYPDF');
		$this->load->library('email');
		$this->load->helper('common_functions.php');

	}
	public function index()
	{
		$data[] = '';
		$data['content'] = $this->load->view('student/studentlist',$data,true);
		$this->load->view('template_files/template',$data);
	}
	public function ajax_studentlist()
	{
		$columns = array(
			0 => 't1.id',
			1 => 't1.name',
			2 => 't1.age',
			3 => 't1.mobileno'			
		);
		$return_json = array();
		echo "Yes";
		// echo json_encode(array('que'=>$sql1,'data' => $rowRequests, 'total' => $contactlistrows->num_rows()));
	}
}
?>