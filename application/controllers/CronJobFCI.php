<?php
class CronJobFCI extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', '0'); 
		$this->load->database();
		$this->load->library('Aws3','aws3');
		$this->load->helper('aws3_helper.php');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');	
		$this->load->model('User_model');
		$this->load->helper('common_functions.php');
		$this->load->helper('fci_helper.php');
	}

	public function index()
	{
		$a = 1;
		$loan = $this->User_model->query("SELECT loan.fci, s.closing_status, s.talimar_loan FROM loan INNER JOIN loan_servicing AS s ON s.talimar_loan = loan.talimar_loan WHERE s.loan_status = '2'  ");
		if ($loan->num_rows() > 0) {
				$loan = $loan->result();



				foreach ($loan as $key => $value) {

				 
					$data = $this->getPaymentHistoryDetailsFCI($value->fci);
					if(!empty($data[0]))
					{
 						$all_loan = $this->User_model->query("update loan set dayVariance = '".$data[0]->dayVariance."', totalPaymentFCI = '".$data[0]->totalPayment."' where fci = '".$data[0]->loanAccount."' ");
 					}

 					$a++;
				}
		}

		echo $a;
		
		/*$serialized_data = serialize($data);
		$size = strlen($serialized_data);
		print($size * 8 / 1000); */

		
	}

	public function getPaymentHistoryDetailsFCI($fci)
	{
		$array = array();
		$dayVariance = '';
		$result = '';
			
			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => 'https://api.trustfci.com:8065/graphql',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			 CURLOPT_POSTFIELDS =>'{"query":"{\\n  getLoanActivities\\n  (\\n        loanaccount:\\"'.$fci.'\\",\\n        limit:1,\\n        offset:0,\\n        orderby: \\"dateReceived\\",\\n        order: \\"desc\\"\\n    ) \\n    {\\n        loanAccount\\n        dateReceived\\n        dateDue\\n        dayVariance\\n        totalPayment\\n    }\\n}","variables":{}}',
			  CURLOPT_HTTPHEADER => array(
			    'Authorization:  Bearer TnTaHakqp440vi0iNc7XfXLjBK9zymmdSLnfraquGb0',
			    'Content-type:  application/json'
			  ),
			));

			$response = curl_exec($curl);

			if(curl_errno($curl))
			{
			    $dayVariance = array();
			}else{
				
				$APIResult = json_decode($response);
				if(!empty($APIResult->data->getLoanActivities))
				{
					$dayVariance = $APIResult->data->getLoanActivities;
 	
				}
				else{
					$dayVariance = array();
				}	
			}

		return $dayVariance;

	}

	public function get_loan_details_by_fci_id($loanAccount){
		

		// Next payment get api code***************************************************

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://api.trustfci.com:8065/graphql',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS =>'{"query":"{ \\r\\n  getLoanInformation(\\r\\n        loanaccount:\\"'.$loanAccount.'\\",\\r\\n        offset:0,\\r\\n        orderby: \\"nextDueDate\\",\\r\\n        order: \\"desc\\"\\r\\n    )\\r\\n        {\\r\\n            loanAccount\\r\\n          nextDueDate\\r\\n        }\\r\\n}","variables":{}}',
		  CURLOPT_HTTPHEADER => array(
		    'Authorization: Bearer TnTaHakqp440vi0iNc7XfXLjBK9zymmdSLnfraquGb0',
		    'Content-type:  application/json'
		  ),
		));
		//totalPayment\\n
		$responseNextDate = curl_exec($curl);

		$NextDateArray  = json_decode($responseNextDate);

		curl_close($curl);

		$arrayNext = array();
		if(!empty($NextDateArray->data->getLoanInformation))
		{
			$NextDate = $NextDateArray->data->getLoanInformation;
			return date('Y-m-d', strtotime($NextDate[0]->nextDueDate));
		}
		else
		{
			return '';
		}
		
		return $arrayNext;
	}

	public function get_loadn_property_address_unit($talimar_loan){
		$talimarAddress = array();
		$talimarAddress['talimar_loan'] = $talimar_loan;
		$fetch_loanProperty 			= $this->User_model->select_where('loan_property',$talimarAddress);
		$fetch_loanProperty 			= $fetch_loanProperty->row();

		$fulladdress = '';
		if($fetch_loanProperty->unit != ''){
            $fulladdress = $fetch_loanProperty->property_address.' '.$fetch_loanProperty->unit.' ';
        }else{
            $fulladdress = $fetch_loanProperty->property_address.' ';
        }
        return $fulladdress;
	}

	public function get_loadn_property_address($talimar_loan){
		$talimarAddress = array();
		$talimarAddress['talimar_loan'] = $talimar_loan;
		$fetch_loanProperty 			= $this->User_model->select_where('loan_property',$talimarAddress);
		$fetch_loanProperty 			= $fetch_loanProperty->row();

		$fulladdress = '';
		if($fetch_loanProperty->unit != ''){
            $fulladdress = $fetch_loanProperty->property_address.' '.$fetch_loanProperty->unit.'; '.$fetch_loanProperty->city.', '.$fetch_loanProperty->state.' '.$fetch_loanProperty->zip;
        }else{
            $fulladdress = $fetch_loanProperty->property_address.'; '.$fetch_loanProperty->city.', '.$fetch_loanProperty->state.' '.$fetch_loanProperty->zip;
        }
        return $fulladdress;
	}

	public function get_address_format($address, $unit, $city, $state, $zip){
		$fulladdress = '';
		if($unit != ''){
            $fulladdress = $address.' '.$unit.'; '.$city.', '.$state.' '.$zip;
        }else{
            $fulladdress = $address.'; '.$city.', '.$state.' '.$zip;
        }
        return $fulladdress;
	}

	public function monthly_payment_amount($talimar_loan, $fetch_loan){
		$talimarAddress['talimar_loan'] = $talimar_loan;
		$fetch_loanProperty 			= $this->User_model->select_where('loan_property',$talimarAddress);
		$loan_property_result 			= $fetch_loanProperty->row();

		$talimar_Auto['talimar_loan'] 	= $talimar_loan;
		$fetch_impound_account 			= $this->User_model->select_where_asc('impound_account', $talimar_Auto, 'position');
		$fetch_impound_account 			= $fetch_impound_account->result();

		$aa = 0;
		if ($fetch_impound_account) {			
			if(isset($fetch_impound_account)){
				foreach($fetch_impound_account as $key=>$row){
					$impound_amount = $row->amount;
					$readonly = '';						
					if($row->items == 'Mortgage Payment')
					{						
						$aa += ($fetch_loan->intrest_rate/100) * $fetch_loan->loan_amount / 12;
					}
					if($row->items == 'Property / Hazard Insurance Payment')
					{
						if($row->impounded == 1){
							$aa += isset($loan_property_result) ? ($loan_property_insurance_result->annual_insurance_payment / 12) : 0;
						}
					}
											
					if($row->items == 'County Property Taxes')
					{
						if($row->impounded == 1){
							$aa += isset($loan_property_result) ? ($loan_property_result[0]->annual_property_taxes / 12) : 0;
						}
					}
					if($row->items == 'Flood Insurance')
					{
						if($row->impounded == 1){
							$aa += isset($row->amount) ? ($row->amount) : 0;
						}
					}
											
					if($row->items == 'Mortgage Insurance')
					{
						if($row->impounded == 1){
							$aa += isset($row->amount) ? ($row->amount) : 0;
						}
					}
				}
			}
		}
		return $aa;
	}

	public function sendRemainderBorrober()
	{
		
		$loan_search = $this->User_model->query("SELECT loan.* FROM loan INNER JOIN loan_servicing AS servicing ON loan.talimar_loan=servicing.talimar_loan WHERE loan.paymentReminder = 'on' AND servicing.loan_status = '2'"); 
		$loan_search = $loan_search->result();		
		$date_25 = date("j");
		
		if($this->uri->segment(3)){
			$date_25 = $this->uri->segment(3);
		}
					
		if($loan_search)
		{
			foreach ($loan_search as $key => $loan_details) 
			{ 

					$loan_uni_id = $loan_details->id;
					$talimar_loan = $loan_details->talimar_loan;
					$loanNext = $this->User_model->query("SELECT nextDueDate FROM lender_payment_history WHERE talimar_loan = '".$loan_details->talimar_loan."'"); 
					$loanNext = $loanNext->row();
										
					if($loanNext)
					{

						$LoanServicer = $loan_details->fci;					
						$Loan_ServicerAddress = array();
						$talimar_loan_id = $loan_details->talimar_loan;		

						$loan_amount = $loan_details->totalPaymentFCI;			

						$amountIc = $this->monthly_payment_amount($talimar_loan, $loan_details);
						if($amountIc > 0){
							$loan_amount = $amountIc;
						}
						
						$befordate = date('Y-m-d', strtotime('-6 days', strtotime($loanNext->nextDueDate)));
						$firstbefordate = date('Y-m-d', strtotime('-6 days', strtotime($loan_details->first_payment_date)));
						$loan_account_no = $loan_details->fci;
						$emiDate = $loan_search->nextDueDate;
						$todayDate = date("Y-m-d");
						$month = date('m');

						if($month == date('m', strtotime($firstbefordate)))
						{
							$befordate = $firstbefordate;
							$emiDate = $loan_details->first_payment_date;
						}
												
						if($loan_amount)
						{
							if($befordate == $todayDate && $month == date('m', strtotime($befordate))){

								$cam_payment_date=date_create($emiDate);
								$cam_before_date=date_create($befordate);
								
								
									$sql_loan_servicing = $this->User_model->query("SELECT * FROM loan_servicing WHERE talimar_loan = '$talimar_loan_id'");
									$loan_servicing_data = $sql_loan_servicing->row();
									if($loan_servicing_data){

										$sql_vendor = $this->User_model->query("SELECT * FROM vendors WHERE vendor_id = '".$loan_servicing_data->sub_servicing_agent."'");
										$Servicer_vendor = $sql_vendor->row();

										$Loan_Servicer = $Servicer_vendor->vendor_name;
										
										if($Servicer_vendor->vendor_name){
											$Loan_ServicerAddress[] = $Servicer_vendor->street_address;
										}
										$unitAdd = '';
										if($Servicer_vendor->unit){
											$unitAdd .= $Servicer_vendor->unit.'';
										}
										$unitAdd .= ';';
										$Loan_ServicerAddress[] = $unitAdd;
										if($Servicer_vendor->city){
											$Loan_ServicerAddress[] = ' '.$Servicer_vendor->city.',';
										}
										if($Servicer_vendor->state){
											$Loan_ServicerAddress[] = ''.$Servicer_vendor->state;
										}
										if($Servicer_vendor->zip){
											$Loan_ServicerAddress[] = ' '.$Servicer_vendor->zip;
										}
									}

									$phone_number = '';
									if($Servicer_vendor->phone_number){
										$phone_number = $Servicer_vendor->phone_number;
									}else{
										$phone_number = $Servicer_vendor->phone_number2;
									}

									$borrowerid = $loan_details->borrower;
									$sql_borrower = $this->User_model->query("SELECT * FROM borrower_data WHERE id = '".$borrowerid."'");
									$loan_borrower = $sql_borrower->row();

									$loan_address = $this->get_loadn_property_address($talimar_loan);
									$propertyAddressArrayData	= $this->getLoanPropertyAddress($talimar_loan);
									$propertyAddress="";
									if(!empty($propertyAddressArrayData['street_address'])){
										$propertyAddress.=$propertyAddressArrayData['street_address']." ";
									}
									if(!empty($propertyAddressArrayData['unit'])){
										$propertyAddress.=$propertyAddressArrayData['unit']."  #";
									}
									$propertyAddress.="; ";
									$propertyAddress.=$propertyAddressArrayData['city'].", ".$propertyAddressArrayData['state']." ".$propertyAddressArrayData['zip'].".";
									$b_con_id = array();
									$b_con_id['borrower_id'] = $borrowerid;						
									$fetch_b_contact = $this->User_model->select_where('contact', $b_con_id);
									$loanBorrowerData = $fetch_b_contact->row();
									$contact_firstname = '';
									if($loanBorrowerData){
										$contact_firstname = $loanBorrowerData->contact_firstname;
									}else{
										$contact_firstname = $loan_borrower->b_name;
									}
									
									$From 	 = 'servicing@talimarfinancial.com';
									$To 	 = $loan_borrower->c_email;
									$Subject = 'Upcoming Payment Reminder';//'Payment Reminder - '.$this->get_loadn_property_address_unit($talimar_loan);

									$body_content = '<p><b>Account: #####'.substr($loan_account_no, -4).'</b></p>';
									$body_content .= '<p><b>Dear '.$contact_firstname.'</b></p>';

									$body_content .= '<p>This e-mail is a reminder that your payment in the amount of <b>$'.number_format($loan_amount, 2, '.', ',').'</b> is due on <b>'.date("m-d-Y", strtotime($nextPayment)).'</b> for the loan secured on '.$propertyAddress.'</p>';

									$body_content .= '<p>Please submit your payment to <b>'.$Loan_Servicer.'</b> at <a href="https://www.google.com/maps/place/'.implode(' ', $Loan_ServicerAddress).'">'.implode(' ', $Loan_ServicerAddress).'</a> or log into www.myfci.com to process your payment. You may also pay by phone by calling <b>'.$Loan_Servicer.'</b> at <b><a href="tel:'.$phone_number.'">'.$phone_number.'</a></b>.</p>';

									$body_content .= '<p>If you are scheduled for auto-payment, the payment will be automatically deducted from your account on your designated date.</p>';

									$body_content .= '<p>If you have a payment reserve account with available funds, the funds will be automatically deducted from the account.</p>';

									$body_content .= '<p>Should you have any questions, please contact Loan Servicing at (858) 242-4900.</p>';

									$dataMailA = array();
							        $dataMailA['from_email']  	= $From;
							        $dataMailA['from_name']   	= 'TaliMar Financial';
							        $dataMailA['to']      		= 'deepesh@bitcot.com';//$To;
							        //$dataMailA['cc']      		= 'bvandenberg@talimarfinancial.com';
							        //$dataMailA['bcc']      		= 'ashishj@bitcot.com';
							        $dataMailA['name']      	= '';
							        $dataMailA['subject']     	= $Subject;
							        $dataMailA['content_body']  = $body_content;
							        $dataMailA['button_url']  	= '';
							        $dataMailA['button_text']   = '';
							        $dataMailA['attach_file']   = '';
							        $dataMailA['regards']   	= 'Loan Servicing';

							        $dataMailA['mail_tracking']   = 'yes';
							        $dataMailA['tracking_type']   = 'paymentReminder';
							        $dataMailA['loan_id']   	  = $loan_uni_id;
							        $dataMailA['borrower_id']     = $loan_details->borrower;
							        $dataMailA['lender_id']   	  = '';
							        $dataMailA['user_id']   	  = $this->session->userdata('t_user_id');

							       // send_mail_template($dataMailA);

									$new_date = date('Y-m-d');
																
									$dataNotes['loan_id'] 	 	= $loan_uni_id;
									$dataNotes['notes_date'] 	= $new_date;
									$dataNotes['contact_id'] 	= $borrowerid;
									$dataNotes['notes_text'] 	= $body_content;
									$dataNotes['user_id'] 	 	= 6;
									$dataNotes['entry_by'] 	 	= 'Brockton VandenBerg';
									$dataNotes['loan_note_type']= 4;
									$dataNotes['com_type'] 	 	= 2;
									$this->User_model->insertdata('contact_notes', $dataNotes);

									$status = 1;
									$name = 1;
									$url = base_url('CronJobFCI/sendRemainderBorrober');

									
								
							}
						}
					}
			}	
		}	
	}	

	public function borrower_requested_construction_mail(){
		
		$every_today_first = date('j');
		if($this->uri->segment(3)){
			$every_today_first = $this->uri->segment(3);
		}

		$borrowerContactrem = array();		
		$every_today_first=1;
		if($every_today_first == 1){
			$constructionUpdate = $this->User_model->query("SELECT loan.* FROM loan INNER JOIN loan_servicing AS servicing ON loan.talimar_loan=servicing.talimar_loan WHERE loan.constructionUpdate = 'on' AND servicing.loan_status = '2'");  //AND loan.fci <> ''
			$constructionSearch = $constructionUpdate->result();

			if($constructionSearch){
				foreach ($constructionSearch as $construction) {
					$loan_uni_id = $construction->id;
					$borrowerid = $construction->borrower;
					$talimar_loan = $construction->talimar_loan;
					$loan_id = $construction->id;
					$fetch_borrower_contact1 = $this->User_model->query("select * from borrower_contact where borrower_id='".$borrowerid."' order by contact_responsibility  asc");

					if ($fetch_borrower_contact1->num_rows() > 0) {
						$fetch_borrower_contact = $fetch_borrower_contact1->result();

						foreach ($fetch_borrower_contact as $value) {
							$b_con_id['contact_id'] = $value->contact_id;							
						
							$fetch_b_contact = $this->User_model->select_where('contact', $b_con_id);

							$b_contacts = $fetch_b_contact->result();
							foreach ($b_contacts as $loan_borrower) {
									$contact_id = $loan_borrower->contact_id;
									$Loan_ServicerAddress = array();

									if($loan_borrower->contact_address){
										$Loan_ServicerAddress[] = $loan_borrower->contact_address;
									}
									$unitAdd = '';
									if($loan_borrower->contact_unit){
										$unitAdd .= $loan_borrower->contact_unit.'';
									}
									//$unitAdd .= '#;';
									$Loan_ServicerAddress[] = $unitAdd;
									if($loan_borrower->contact_city){
										$Loan_ServicerAddress[] = ' '.$loan_borrower->contact_city.',';
									}
									if($loan_borrower->contact_state){
										$Loan_ServicerAddress[] = ''.$loan_borrower->contact_state;
									}
									if($loan_borrower->contact_zip){
										$Loan_ServicerAddress[] = ' '.$loan_borrower->contact_zip;
									}

								$contact_email = '';
								if($loan_borrower->contact_email){
									$contact_email = $loan_borrower->contact_email;
								}elseif($loan_borrower->contact_email2){
									$contact_email = $loan_borrower->contact_email2;
								}

								$borrowerContactrem[$contact_id]['first_name'] = $loan_borrower->contact_firstname;
								$borrowerContactrem[$contact_id]['last_name']= $loan_borrower->contact_lastname;
								$borrowerContactrem[$contact_id]['email']= $contact_email;
								$borrowerContactrem[$contact_id]['address'] = implode(' ', $Loan_ServicerAddress);

								$m_address = get_loan_property_address($talimar_loan);//$this->get_address_format($loan_borrower->contact_address, $loan_borrower->contact_unit, $loan_borrower->contact_city, $loan_borrower->contact_state, $loan_borrower->contact_zip);
								$m_name    = $loan_borrower->contact_firstname;
								$m_email    = $contact_email;

								$email_html_body = '';
								$email_html_body .= '<p><b>Hi '.$m_name.' </b></p>';
								$email_html_body .= '<p>Would you please respond with a construction status update for <b>'.$m_address.'</b>. Please include the % complete, remaining items to be completed, and estimated completion date.</p>'; 
								$email_html_body .= '<p>We appreciate your timely response. Should you have any questions regarding our request, please reach out to us. </p><br>';
								
								$regards = '
								Loan Servicing<br>
								TaliMar Financial<br>
								(858) 242-4900<br>
								servicing@talimarfinancial.com';


								$Subject = 'Construction Status Request – '.$m_address;

								$From = 'servicing@talimarfinancial.com';
								$To   = $m_email;
								

								$dataMailA = array();
						        $dataMailA['from_email']  	= $From;
						        $dataMailA['from_name']   	= 'TaliMar Financial';
						        $dataMailA['to']      		= $To;
						        $dataMailA['cc']      		= 'bvandenberg@talimarfinancial.com';
					        	$dataMailA['bcc']      		= 'ashishj@bitcot.com';
						        $dataMailA['name']      	= '';
						        $dataMailA['subject']     	= $Subject;
						        $dataMailA['content_body']  = $email_html_body;
						        $dataMailA['button_url']  	= '';
						        $dataMailA['button_text']   = '';
						        $dataMailA['attach_file']   = '';
						        $dataMailA['regards']   = $regards;

						        $dataMailA['mail_tracking']   = 'yes';
						        $dataMailA['tracking_type']   = 'constructionUpdate';
						        $dataMailA['loan_id']   	  = $loan_uni_id;
						        $dataMailA['borrower_id']     = $borrowerid;
						        $dataMailA['lender_id']   	  = '';
						        $dataMailA['user_id']   	  = $this->session->userdata('t_user_id');

						        send_mail_template($dataMailA);

						
								
								$new_date = date('Y-m-d');
								
								
								$dataNotes['loan_id'] 	 = $loan_id;
								$dataNotes['notes_date'] = $new_date;
								$dataNotes['contact_id'] = $contact_id;
								$dataNotes['notes_text'] = $email_html_body;
								$dataNotes['user_id'] 	 = 6;
								$dataNotes['entry_by'] 	 = 'Brockton VandenBerg';
								$dataNotes['loan_note_type'] = 5;
								$dataNotes['com_type'] 	 = 2;
								print_r($dataNotes);
								$this->User_model->insertdata('contact_notes', $dataNotes);

								$status = 1;
								$name = 2;
								$url = base_url('CronJobFCI/borrower_requested_construction_mail');

								

							}
						}			
					}
				}
			}
		}
	}

	public function sendRemainderBorroberchecker()
	{	
		
	}

	public function loanCompair()
	{
		$fci = getLoanPortfolio(true, '');

		if(!empty($fci))
		{
			$numRows = $this->CM->deletedatAll('lender_payment_history', array('id !=' => ''));

			if($numRows > 0)
			{
				$array = array();
				foreach($fci as $value)
				{
					$loan = $this->User_model->query("SELECT loan.id, loan.fci, loan.current_balance, s.closing_status, s.loan_status, loan.talimar_loan FROM loan INNER JOIN loan_servicing AS s ON s.talimar_loan = loan.talimar_loan WHERE fci = '".$value->loanAccount."' AND loan.talimar_loan");
					$record = $loan->row();
					if($record)
					{

						$currentBalance = (double)str_replace(',', '', $value->currentBalance);
						$difference1 = (double)$record->current_balance - $currentBalance;
						$talimar_loan = $record->talimar_loan;
					}
					else
					{
						$difference1 = (double)str_replace(',', '', $value->currentBalance);
						$talimar_loan = '';
					}

					if(!empty($value->nextDueDate))
					{
						$dateData = explode('/', $value->nextDueDate);

						$nextDueDate = $dateData[2].'-'.$dateData[0].'-'.$dateData[1];
					}
					else
					{
						$nextDueDate = '0000-00-00';
					}

					if(!empty($value->maturityDate))
					{
						$dateData = explode('/', $value->maturityDate);

						$maturityDate = $dateData[2].'-'.$dateData[0].'-'.$dateData[1];
					}
					else
					{
						$maturityDate = '0000-00-00';
					}
					
					$array[] = array(
						"talimar_loan" => $talimar_loan,
						"difference" => $difference1,
						"loanAccount" => $value->loanAccount, 
						"lenderAccount" => $value->lenderAccount, 
						"originalBalance" => $value->originalBalance, 
						"currentBalance" => $value->currentBalance, 
						"totalPayment" => $value->totalPayment,
						"daysLate" => $value->daysLate,
						"nextDueDate" => $nextDueDate,
						"maturityDate" => $maturityDate,
						"loanStatus " => $value->loanStatus
					);
				}
				
				$result = $this->CM->insertMultipleData('lender_payment_history', $array);

				$status = 1;
				$name = 3;
				$url = base_url('CronJobFCI/loanCompair');

				cronJobStatus($name, $url, $status);	
			}
		}

		echo 'Done';
	}


	public function apiConnection()
	{
		error_reporting(0);
		$data = array();

		$loan = $this->User_model->query('SELECT DISTINCT id, name, url, status, createdAt FROM cronJobHistory WHERE DATE(createdAt) = "'.date('Y-m-d').'" ORDER BY id DESC ');
		$loan 	= $loan->result();
		$data['loan'] = $loan;
		$data['content'] = $this->load->view('mail_report/CronJobStatus', $data, true);
		$this->load->view('template_files/template', $data);
	}

	public function ajaxGetCron()
	{
		$conName = $this->config->item('conName');
		$post = $this->input->post();

		$orderby = 'id';
		$order = 'DESC';

		if($post){
			if($post['order'][0]['column']){
				$columnIndex = $post['order'][0]['column']; // Column index
				$columnName = $post['columns'][$columnIndex]['data']; // Column name
				$orderby = $columnName;
			}
			if($post['order'][0]['dir']){
				$order = $post['order'][0]['dir'];
			}
		}

		$startfrom = $post['start'];
		$queryLength = $post['length'];
		$result = array();

		$where = '';

		if(!empty($post['conName']))
		{
			$where = $where . ' cronJobHistory.name = "'.$post['conName'].'" ';
		}

		if(!empty($post['dateData']))
		{
			
			if(!empty($where))
			{
				$where = $where . ' AND dateData = "'.$post['dateData'].'" ';
			}
			else
			{
				$where = $where . ' dateData = "'.$post['dateData'].'" ';
			}
		}

		if(!empty($post['timeData']))
		{
			

			if(!empty($where))
			{
				$where = $where . ' AND timeData = "'.$post['timeData'].'" ';
			}
			else
			{
				$where = $where . ' timeData = "'.$post['timeData'].'" ';
			}
		}

		if(!empty($post['createdAt']))
		{
			if(!empty($where))
			{
				$where = $where . ' AND DATE_FORMAT(Date(createdAt), "%m-%d-%Y") = "'.$post['createdAt'].'" ';
			}
			else
			{
				$where = $where . ' DATE_FORMAT(Date(createdAt), "%m-%d-%Y") = "'.$post['createdAt'].'" ';
			}	
		}

		

		if(!empty($where))
		{
			$where = 'WHERE '.$where;
		}

		$startfrom = $post['start'];
		$queryLength = $post['length'];

		$limit = ' LIMIT '.$startfrom.','.$queryLength;
		$orderby = ' ORDER BY '.$orderby.' '.$order;


		$loanTotal = $this->User_model->query('SELECT DISTINCT id, name, url, status, createdAt FROM cronJobHistory '.$where.' ORDER BY id DESC ');
		$loanTotal 	= $loanTotal->num_rows();

		$loanResult = $this->User_model->query('SELECT DISTINCT id, name, url, IF(status > 0, "Success", "Not Success") AS status, DATE_FORMAT(Date(createdAt), "%m-%d-%Y") AS dateData, TIME(createdAt) AS timeData FROM cronJobHistory '.$where.' '. $orderby.' '. $limit);
		$loanResult 	= $loanResult->result();

		foreach ($loanResult as $key => $value) {
			$value->name = !empty($conName[$value->name])?$conName[$value->name]:'';
		}

		$query = $this->db->last_query();

		echo json_encode(array('data'=>$loanResult, 'total' => $loanTotal, 'total1' => count($loanResult), 'queryData' => $query));
		
	}

	public function maturity_notification(){
		$maturityNotificationQuery = "SELECT loan.* FROM loan INNER JOIN loan_servicing AS servicing ON loan.talimar_loan=servicing.talimar_loan WHERE loan.maturityNotification = 'on' AND servicing.loan_status = '2' "; 
		$maturityNotificationQuery 			= $this->User_model->query($maturityNotificationQuery);			
		$maturityNotificationQueryResult 	= $maturityNotificationQuery->result();
		echo "<pre>";
		print_r($maturityNotificationQueryResult);
		if(!empty($maturityNotificationQueryResult)){
			foreach($maturityNotificationQueryResult as $key => $maturityNotificationRow){
				if(!empty($maturityNotificationRow->new_maturity_date) || $maturityNotificationRow->new_maturity_date!="0000-00-00"){
					$talimarLoanId				= $maturityNotificationRow->id;
					$talimarLoan				= $maturityNotificationRow->talimar_loan;
					$extentionOption			= $maturityNotificationRow->extention_option;
					$extentionMonth				= $maturityNotificationRow->extention_month;
					$maturityDate				= $maturityNotificationRow->new_maturity_date;
					$borrowerId 				= $maturityNotificationRow->borrower;
					$maturityDateMail 			= date('m-d-Y',strtotime($maturityDate));
					$currentDate				= date('Y-m-d');
					$days45Before 				= date('Y-m-d',strtotime($maturityDate.'-45 day'));
					$days30Before 				= date('Y-m-d',strtotime($maturityDate.'-30 day'));
					$days20Before 				= date('Y-m-d',strtotime($maturityDate.'-20 day'));
					$days15Before 				= date('Y-m-d',strtotime($maturityDate.'-15 day'));
					$days10Before 				= date('Y-m-d',strtotime($maturityDate.'-10 day'));
					$days5Before 				= date('Y-m-d',strtotime($maturityDate.'-5 day'));
					$days1Before 				= date('Y-m-d',strtotime($maturityDate.'-1 day'));
					echo "before date".$days45Before;
					if($currentDate==$days45Before || $currentDate==$days30Before)
					{	
						$propertyAddressArrayData	= $this->getLoanPropertyAddress($talimarLoan);
						$propertyAddress="";
						if(!empty($propertyAddressArrayData['street_address'])){
							$propertyAddress.=$propertyAddressArrayData['street_address']." ";
						}
						if(!empty($propertyAddressArrayData['unit'])){
							$propertyAddress.=$propertyAddressArrayData['unit'];
						}
						$subjectAddress=$propertyAddress;
						$propertyAddress.="; ";
						$propertyAddress.=$propertyAddressArrayData['city'].", ".$propertyAddressArrayData['state']." ".$propertyAddressArrayData['zip'];				
						$BorrowerFirstName		= "";
						$BorrowerFirstEmail		= "";
						$BorrowerContactSql 	= $this->User_model->query("SELECT c.contact_firstname,c.contact_email FROM borrower_contact as bc JOIN contact as c ON bc.contact_id = c.contact_id WHERE bc.borrower_id = '".$borrowerId."' AND bc.contact_primary = '1'");					
						$BorrowerContactResult 	= $BorrowerContactSql->result();
						if(!empty($BorrowerContactResult)){
							$BorrowerFirstName	=$BorrowerContactResult[0]->contact_firstname;
							$BorrowerFirstEmail	=$BorrowerContactResult[0]->contact_email;
						}
						$contentMail = '<p>Dear '.$BorrowerFirstName.' ,</p>';
				        $contentMail .= '<p>The loan secured on '.$propertyAddress.' is scheduled to mature on '.$maturityDateMail.'. </p>';
				        $contentMail .= '<p>';
				        if($extentionOption=="1"){
				        	$contentMail .= 'This loan includes a '.$extentionMonth.' month option to extend upon Lender approval.';
				        }
				        $contentMail .= ' All requests must be submitted to servicing@talimarfinancial.com for review and approval. We highly suggest that you submit the request at least 30 days prior to the maturity of the loan to ensure sufficient time to review the request. </p>';
				        $contentMail .= '<p>To obtain a payoff demand, please submit your request to payoff@talimarfinancial.com. Please note, demands may take 5 to 7 business days to process so please plan accordingly. </p>';
				        $contentMail .='<p>If you feel that you will not be able to pay off the loan prior to the maturity date and the loan does not include an extension option, please contact us immediately to explore some alternative options. </p>';
				        $contentMail .='<p>Should you have any questions, please contact Loan Servicing at (858) 242-4900   </p>';
				        $contentMail .'<p></p>';
				        $regards = '<p>
								Loan Servicing<br>
								TaliMar Financial</p>';
				        $dataMail = array();
				        $dataMail['from_email']  	= 'servicing@talimarfinancial.com';
				        $dataMail['from_name']   	= 'TaliMar Financial';
				        //$dataMail['to']      		= $BorrowerFirstEmail;
				        $dataMail['to']      		= 'amarjain@bitcot.com';
				        $dataMail['cc']      		= '';
				        $dataMail['bcc']      		= '';
				        $dataMail['name']      		= '';
				        $dataMail['button_url']  	= '';
						$dataMail['button_text']   = '';
						$dataMail['attach_file']   = '';
				        $dataMail['subject']     	= "Loan Maturity Notification– ".$subjectAddress;
				        $dataMail['content_body']  	= $contentMail;

				        $dataMail['regards']   = $regards;
				        $dataMail['mail_tracking']   = 'yes';
				        $dataMail['tracking_type']   = 'maturityNotification';
				        $dataMail['loan_id']   	  = $talimarLoanId;
				        $dataMail['borrower_id']     = $borrowerId;
				        $dataMail['lender_id']   	  = '';
				        $dataMail['user_id']   	  = $this->session->userdata('t_user_id');
				        send_mail_template($dataMail);
						echo "<pre>";
						print_r($dataMail);							
					}
				}
			}

			$status = 1;
			$name = 4;
			$url = base_url('CronJobFCI/maturity_notification');
		}
	}

	public function getLoanPropertyAddress($talimarLoan){
		$property_home_id 		= "";
		$property_address 		= "";
		$unit 					= "";
		$city 					= "";
		$state 					= "";
		$zip 					= "";
		$propertyAddressQuery 	= $this->User_model->query("SELECT property_home_id,property_address, unit, city, state, zip FROM loan_property WHERE talimar_loan = '".$talimarLoan."'");
		$propertyAddressQueryResult = $propertyAddressQuery->result();					
		if(!empty($propertyAddressQueryResult)){
			$property_home_id   = $propertyAddressQueryResult[0]->property_home_id;
			$property_address   = $propertyAddressQueryResult[0]->property_address;
			$unit 				= $propertyAddressQueryResult[0]->unit;
			$city 				= $propertyAddressQueryResult[0]->city;
			$state 				= $propertyAddressQueryResult[0]->state;
			$zip 				= $propertyAddressQueryResult[0]->zip;
		}
		$propertyAddressArray			= array(
										"property_home_id"  => $property_home_id,
										"street_address"    => $property_address,
										"unit"				=> $unit,
										"city" 				=> $city,
										"state" 			=> $state,
										"zip" 				=> $zip
									 );
		return $propertyAddressArray;
	}

	public function getLoanVenderAddress($sub_servicing_agent,$borrowerid){
		$street_address				= "";
		$unit 						= "";
		$city 						= "";
		$state 						= "";
		$zip 						= "";
		$Loan_Servicer 				= "";
		$phone_number 				= "";
		$contact_firstname			= "";
		$contact_email 				= "";
		if(!empty($sub_servicing_agent)){
			$sql_vendor 			= $this->User_model->query("SELECT * FROM vendors WHERE vendor_id = '".$sub_servicing_agent."'");
			$Servicer_vendor 		= $sql_vendor->row();
			if(!empty($Servicer_vendor)){
				$Loan_Servicer  	= $Servicer_vendor->vendor_name;
				$street_address 	= $Servicer_vendor->street_address;
				$unit           	= $Servicer_vendor->unit;
				$city 				= $Servicer_vendor->city;
				$state 				= $Servicer_vendor->state;
				$zip 				= $Servicer_vendor->zip;
				if($Servicer_vendor->phone_number){
					$phone_number 	= $Servicer_vendor->phone_number;
				}else{
					$phone_number 	= $Servicer_vendor->phone_number2;
				}
			}
		}
		$BorrowerContactSql 	= $this->User_model->query("SELECT c.contact_firstname,c.contact_email FROM borrower_contact as bc JOIN contact as c ON bc.contact_id = c.contact_id WHERE bc.borrower_id = '".$borrowerid."' AND bc.contact_primary = '1'");
		$BorrowerContactResult 	= $BorrowerContactSql->result();
		if(!empty($BorrowerContactResult)){
			$contact_firstname	=$BorrowerContactResult[0]->contact_firstname;
			$contact_email		=$BorrowerContactResult[0]->contact_email;
		}
		$venderAddressArray			= array(
										"loan_servicer"		=> $Loan_Servicer,
										"street_address"    => $street_address,
										"unit"				=> $unit,
										"city" 				=> $city,
										"state" 			=> $state,
										"zip" 				=> $zip,
										"phone_number"      => $phone_number,
										"contact_firstname" => $contact_firstname,
										"contact_email"		=> $contact_email
									 );
		return $venderAddressArray;
	}

	public function server_time_details(){
		$timezone = date_default_timezone_get();
		echo "The current server timezone is: " . $timezone."<br>";
		$dateTime = date('m/d/Y h:i:s a', time());
		echo $dateTime."<br>";
		$currentdate=date('Y-m-d');
		echo "current date---".$currentdate;
		$beforeDate=date('Y-m-d', strtotime('-3 days', strtotime($currentdate)));	
		echo "before date".$beforeDate;
		$cam_cuurent_date=date_create($currentdate);
		$cam_before_date=date_create($beforeDate);
		$diff=date_diff($cam_before_date,$cam_cuurent_date);
		echo "diffrence".$diff->format("%a");
	}

	public function propertyInsuranceExpiring (){
		$propertyInsuranceQuery 				= "SELECT l.id,l.talimar_loan,l.borrower FROM loan AS l INNER JOIN loan_servicing  AS ls ON l.talimar_loan=ls.talimar_loan  WHERE l.propertyInsurance = 'on' AND ls.loan_status = '2'  "; 
		$propertyInsuranceQuery 				= $this->User_model->query($propertyInsuranceQuery);
		$propertyInsuranceQueryResult 			= $propertyInsuranceQuery->result();
		if(!empty($propertyInsuranceQueryResult)){
			foreach($propertyInsuranceQueryResult as $key => $propertyInsuranceRow){				
				$talimarLoanId				= $propertyInsuranceRow->id;
				$talimarLoanNo				= $propertyInsuranceRow->talimar_loan;
				$borrowerId 				= $propertyInsuranceRow->borrower;
				$propertyAddressArrayData	= $this->getLoanPropertyAddress($talimarLoanNo);
				$propertyHomeId 			= $propertyAddressArrayData['property_home_id'];
				//Get Insurance Details
				$insoranceDetailsQuery		= $this->User_model->query("SELECT insurance_expiration,contact_email FROM loan_property_insurance  WHERE talimar_loan = '".$talimarLoanNo."' AND insurance_type = '1' AND property_home_id='$propertyHomeId' ");
				$insoranceDetailResult 		= $insoranceDetailsQuery->result();
				$insuranceContactEmail 		= "";
				$insuranceExpirationDate 	= "";
				if(!empty($insoranceDetailResult)){
					$insuranceContactEmail	= $insoranceDetailResult[0]->contact_email;
					$insuranceExpirationDate= $insoranceDetailResult[0]->insurance_expiration;
				}
				//	Insurance Details End
				if(!empty($insuranceExpirationDate) || $insuranceExpirationDate!="0000-00-00"){					
					$insuranceExpirationDateShow= date('m/d/Y',strtotime($insuranceExpirationDate));
					$currentDate				= date('Y-m-d');
					$days45Before 				= date('Y-m-d',strtotime($insuranceExpirationDate.'-45 day'));
					$days30Before 				= date('Y-m-d',strtotime($insuranceExpirationDate.'-30 day'));
					$days20Before 				= date('Y-m-d',strtotime($insuranceExpirationDate.'-20 day'));
					$days15Before 				= date('Y-m-d',strtotime($insuranceExpirationDate.'-15 day'));
					$days10Before 				= date('Y-m-d',strtotime($insuranceExpirationDate.'-10 day'));
					$days5Before 				= date('Y-m-d',strtotime($insuranceExpirationDate.'-5 day'));
					$days1Before 				= date('Y-m-d',strtotime($insuranceExpirationDate.'-1 day'));
					//$days45Before=date('Y-m-d');

					echo "insorance date-- ".$insuranceExpirationDate;
					echo "before 30 days date".$days30Before."<br>";
					echo "current date date".$currentDate."<br>";
					if($currentDate==$days45Before || $currentDate==$days30Before || $currentDate==$days20Before || $currentDate==$days15Before || $currentDate==$days10Before || $currentDate==$days5Before || $currentDate==$days1Before)
					{	
						echo "inside if condition<br>";

						$propertyAddress="";
						if(!empty($propertyAddressArrayData['street_address'])){
							$propertyAddress.=$propertyAddressArrayData['street_address']." ";
						}
						if(!empty($propertyAddressArrayData['unit'])){
							$propertyAddress.=$propertyAddressArrayData['unit'];
						}
						$subjectAddress=$propertyAddress;
						$propertyAddress.="; ";
						$propertyAddress.=$propertyAddressArrayData['city'].", ".$propertyAddressArrayData['state']." ".$propertyAddressArrayData['zip'];
						$BorrowerFirstName		= "";
						$BorrowerFirstEmail		= "";
						$BorrowerContactSql 	= $this->User_model->query("SELECT c.contact_firstname,c.contact_email FROM borrower_contact as bc JOIN contact as c ON bc.contact_id = c.contact_id WHERE bc.borrower_id = '".$borrowerId."' AND bc.contact_primary = '1'");					
						$BorrowerContactResult 	= $BorrowerContactSql->result();
						if(!empty($BorrowerContactResult)){
							$BorrowerFirstName	=$BorrowerContactResult[0]->contact_firstname;
							$BorrowerFirstEmail	=$BorrowerContactResult[0]->contact_email;
						}
						$contentMail = '<p>Dear '.$BorrowerFirstName.',</p>';
						$contentMail.= '<p>The property insurance policy insuring '.$propertyAddress.' is set to expire on '.$insuranceExpirationDateShow.'.';
				        $contentMail.= '<p>Please provide us a new insurance certificate / declaration showing an active policy on the property. You may e-mail us a copy of the evidence of insurance directly at <a href="servicing@talimarfinancial.com">servicing@talimarfinancial.com</a>.</p>';
				        $contentMail .='<p>Should you have any questions, please contact Loan Servicing at (858) 242-4900</p>';
				        $contentMail .'<p></p>';
				        $regards = '<p>
								Loan Servicing<br>
								TaliMar Financial</p>';
				        $dataMail = array();
				        $dataMail['from_email']  	= 'servicing@talimarfinancial.com';
				        $dataMail['from_name']   	= 'TaliMar Financial';
				        //$dataMail['to']      		= $BorrowerFirstEmail;
				        $dataMail['to']      		= 'amarjain@bitcot.com';
				        $dataMail['cc']      		= '';
				        $dataMail['bcc']      		= '';
				        $dataMail['name']      		= '';
				        $dataMail['button_url']  	= '';
						$dataMail['button_text']    = '';
						$dataMail['attach_file']    = '';
				        $dataMail['subject']     	= "Insurance Expiring– ".$subjectAddress;
				        $dataMail['content_body']  	= $contentMail;
				        $dataMail['regards']   		= $regards;
				        $dataMail['mail_tracking']  = 'yes';
				        $dataMail['tracking_type']  = 'insuranceNotification';
				        $dataMail['loan_id']   	    = $talimarLoanId;
				        $dataMail['borrower_id']    = $borrowerId;
				        $dataMail['lender_id']   	= '';
				        $dataMail['user_id']   	  	= $this->session->userdata('t_user_id');
				        send_mail_template($dataMail);
						echo "<pre>";
						print_r($dataMail);	
					}						
				}
			}

			$status = 1;
			$name = 5;
			$url = base_url('CronJobFCI/propertyInsuranceExpiring');


		}
	}

	public function beforePaymentReminder()
	{		
		$loan_search 			= $this->User_model->query("SELECT l.*,ls.sub_servicing_agent FROM loan AS l INNER JOIN loan_servicing AS ls ON l.talimar_loan=ls.talimar_loan WHERE l.paymentReminder = 'on' AND ls.loan_status = '2'"); 
		$loan_search 			= $loan_search->result();
		if(!empty($loan_search))
		{
			foreach ($loan_search as $key => $loan_details) 
			{ 


				$lenderPayment = $this->User_model->query("SELECT nextDueDate FROM lender_payment_history WHERE talimar_loan = '".$loan_details->talimar_loan."' ORDER BY id DESC "); 
				$lenderPayment 		= $lenderPayment->row();
				$loan_uni_id 		= $loan_details->id;
				$talimar_loan 		= $loan_details->talimar_loan;
				$borrowerid 		= $loan_details->borrower;
				$loan_account_no 	= $loan_details->fci;
				$talimar_loan_id 	= $loan_details->talimar_loan;	
				$loan_amount 		= $loan_details->totalPaymentFCI;
				$sub_servicing_agent= $loan_details->sub_servicing_agent;
				$nextPayment 		= $lenderPayment->nextDueDate;
				$BorrowerFirstName	= "";
				$BorrowerFirstEmail	= "";
				if($nextPayment)
				{			
					$amountIc 		= $this->monthly_payment_amount($talimar_loan, $loan_details);
					if($amountIc > 0){
						$loan_amount= $amountIc;
					}
					$days3Before 		= date('Y-m-d', strtotime('-3 days', strtotime($nextPayment)));	
					$days6Before 		= date('Y-m-d', strtotime('-6 days', strtotime($nextPayment)));	
					$days9Before 		= date('Y-m-d', strtotime('-9 days', strtotime($nextPayment)));									
					$todayDate 			= date("Y-m-d");
					$days3Before=$todayDate;
					if($days3Before == $todayDate){

						$BorrowerContactSql 	= $this->User_model->query("SELECT c.contact_firstname,c.contact_email FROM borrower_contact as bc JOIN contact as c ON bc.contact_id = c.contact_id WHERE bc.borrower_id = '".$borrowerid."' AND bc.contact_primary = '1'");					
						$BorrowerContactResult 	= $BorrowerContactSql->result();
						if(!empty($BorrowerContactResult)){
							$BorrowerFirstName	=$BorrowerContactResult[0]->contact_firstname;
							$BorrowerFirstEmail	=$BorrowerContactResult[0]->contact_email;
						}
						$propertyAddressArrayData	= $this->getLoanPropertyAddress($talimar_loan);
						$propertyAddress 			= $propertyAddressArrayData['street_address']." ".$propertyAddressArrayData['unit']."; ".$propertyAddressArrayData['city']." ".$propertyAddressArrayData['state']." ".$propertyAddressArrayData['zip'];
						$mailSubject				= "Payment Reminder –".$propertyAddressArrayData['street_address']." ".$propertyAddressArrayData['unit'];
						$mailSendArray				= 	array(
															"tracking_type"				=> "beforePaymentReminder",
															"loan_id"					=> $loan_uni_id,
															'borrower_id'				=> $loan_details->borrower,
															"from"						=> 'servicing@talimarfinancial.com',
															"to"						=> $BorrowerFirstEmail,
															"subject"					=> $mailSubject,
															"account_no"				=> $loan_account_no,
															"loan_amount"				=> $loan_amount,
															"next_payment_date"			=> $nextPayment,
															"first_name"				=> $BorrowerFirstName,
															"property_address" 			=> $propertyAddress
														);
						$this->paymentRemenderMailTemplate($mailSendArray);
					}
				}
			}

			$status = 1;
			$name = 6;
			$url = base_url('CronJobFCI/beforePaymentReminder');
						
		}
	}

	public function paymentRemenderMailTemplate($mailArray){
		$From 	 					= $mailArray['from'];
		$To 	 					= $mailArray['to'];
		$subject 					= $mailArray['subject'];
		$loan_account_no			= $mailArray['account_no'];
		$contact_firstname			= $mailArray['first_name'];
		$loan_amount				= $mailArray['loan_amount'];
		$nextPayment				= $mailArray['next_payment_date'];
		$loan_uni_id				= $mailArray['loan_id'];
		$borrower_id 				= $mailArray['borrower_id'];
		$tracking_type				= $mailArray['tracking_type'];
		$property_address			= $mailArray['property_address'];
		$body_content 				= '<p><b>Account:</b> #######'.substr($loan_account_no, -4).'</p>';
		$body_content.= '<p>Dear '.$contact_firstname.',</p>';
		$body_content.= '<p>This e-mail is a reminder that your payment in the amount of $'.number_format($loan_amount, 2, '.', ',').' was due on '.date("m-d-Y", strtotime($nextPayment)).' for the loan secured on '.$property_address.'.</p>';
		$body_content.= '<p>Please submit your payment to FCI Lender Services at 8180 East Kaiser Blvd; Anaheim, CA 92808 or visit  <a href="www.myfci.com">www.myfci.com</a> to process your payment. You may also pay by phone by calling  FCI Lender Services at (800) 931-2424.</p>';
		$body_content.= '<p>If you are scheduled for auto-payment, the payment will be automatically deducted from your account on the designated date.</p>';
		$body_content.= '<p>Should you have any questions, please contact Loan Servicing at (858) 242-4900.</p>';

		$dataMailA = array();
        $dataMailA['from_email']  	= $From;
        $dataMailA['from_name']   	= 'TaliMar Financial';
        $dataMailA['to']      		= 'amarjain@bitcot.com';
        $dataMailA['cc']      		= '';
		$dataMailA['bcc']      		= '';
        $dataMailA['name']      	= '';
        $dataMailA['button_url']  	= '';
        $dataMailA['button_text']   = '';
        $dataMailA['attach_file']   = '';
        $dataMailA['subject']     	= $subject;
        $dataMailA['content_body']  = $body_content;
       
        $dataMailA['regards']   	= 'Loan Servicing';
        $dataMailA['mail_tracking'] = 'yes';
        $dataMailA['tracking_type'] = $tracking_type;
        $dataMailA['loan_id']   	= $loan_uni_id;
        $dataMailA['borrower_id']   = $borrower_id;
        $dataMailA['lender_id']   	= '';
        $dataMailA['user_id']   	= $this->session->userdata('t_user_id');
        echo "<pre>";
        print_r($dataMailA);
        send_mail_template($dataMailA);
        die();
	}


}