<?php
class Property_data extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('User_model');
		$this->load->library('Aws3','aws3');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');
		// $this->load->library('session');
		if($this->session->userdata('t_user_id') != '' )
		{
		}
		else
		{
			redirect(base_url()."home",'refresh');
		}
	}
	
	public function index()
	{

	}

	public function add_more_property(){

		$p_home['talimar_loan'] 	= $this->input->post('talimar_loan');
		$p_home['loan_id'] 			= $this->input->post('loan_id');
		$p_home['primary_property'] = '2';
		$p_home['multi_properties'] = '1';

		$this->User_model->insertdata('property_home', $p_home);
		echo '1';
	}
	
	public function form_property_home_page()
	{
		/* echo "<pre>";
			print_r($_POST);
		echo "</pre>"; */
		$talimar_loan 	= $this->input->post('talimar_no');
		$loan_id 		= str_replace('/','',$this->input->post('loan_id'));
		
		$id 				= $this->input->post('id');
		$property_address 	= $this->input->post('property_address');
		$position 			= $this->input->post('position');
		$sold_refi_value 	= $this->input->post('sold_refi_value');
		$purchase_price 	= $this->input->post('purchase_price');
		$current_value 		= $this->input->post('current_value');
		$senior_liens 		= $this->input->post('senior_liens');
		$future_value 		= $this->input->post('future_value');
		// $ltv 				= $this->input->post('ltv');
		
		$fetch_property_home = $this->User_model->select_where('property_home',array('talimar_loan'=>$talimar_loan));
		if($fetch_property_home->num_rows() > 0)
		{
			$primary = 2;
		}
		else
		{
			$primary = 1;
		}
		
		foreach($id as $key => $row)
		{
			$row_id = $row;
			if($row_id == 'new')
			{
				if($primary == 1 && $key == 0)
				{
					$primary_key = '1';
				}
				else
				{
					$primary_key = '2';
				}
				// echo 'insert';
				$sql = "INSERT INTO `property_home`(`talimar_loan`, `property_address`, `purchase_price`, `position`,`current_value`,`senior_liens`, `future_value`, `sold_refi_value`, `loan_id`, `primary_property`) VALUES ('".$talimar_loan."', '".$property_address[$key]."',  '".$this->amount_format($purchase_price[$key])."', '".$position[$key]."', '".$this->amount_format($current_value[$key])."',  '".$this->amount_format($senior_liens[$key])."',  '".$this->amount_format($future_value[$key])."',   '".$this->amount_format($sold_refi_value[$key])."' , '".$loan_id."','".$primary_key."' )";
				// Only inserting
				if($property_address[$key])
				{
					$this->User_model->query($sql);
					$insert_id = $this->db->insert_id();
					if($insert_id)
					{
						$add_newv = $this->update_monthly_income_expense_cashflow($insert_id);
						
						// Also insert in Loan Property
						$sql_p = "INSERT INTO loan_property ( `property_home_id`, `talimar_loan`, `loan_id`, `property_address`, `purchase_price`, `current_value`, `arv`, `sales_refinance_value`) VALUES ('".$insert_id."', '".$talimar_loan."', '".$loan_id."', '".$property_address[$key]."', '".$this->amount_format($purchase_price[$key])."', '".$this->amount_format($current_value[$key])."', '".$this->amount_format($future_value[$key])."', '".$this->amount_format($sold_refi_value[$key])."')";
						$this->User_model->query($sql_p);
					}
				}
			}
			else
			{
				
				$sql =$this->User_model->query("UPDATE `property_home` SET  position = '".$position[$key]."', senior_liens = '".$this->amount_format($senior_liens[$key])."' WHERE id = '".$row_id."' ");
				
			}
			
			
		}
		
		$ecum_updation['purchase_price'] 		= $this->amount_format($this->input->post('ecum_purchase_price'));
		$ecum_updation['current_value'] 		= $this->amount_format($this->input->post('ecum_current_value'));
		$ecum_updation['future_value'] 			= $this->amount_format($this->input->post('ecum_future_value'));
		$ecum_updation['sold_refi_value'] 		= $this->amount_format($this->input->post('ecum_sold_refi'));
		
		$fetch_senior_ecumbrances = $this->User_model->select_where('senior_ecumbrances',array('talimar_loan'=>$talimar_loan));
		if($fetch_senior_ecumbrances->num_rows() > 0)
		{
			$this->User_model->updatedata('senior_ecumbrances',array('talimar_loan'=>$talimar_loan),$ecum_updation);
		}
		else
		{
			$ecum_updation['talimar_loan'] 	= $talimar_loan;
			$ecum_updation['loan_id'] 		= $loan_id;
			$this->User_model->insertdata('senior_ecumbrances',$ecum_updation);
		}
		

		$this->session->set_flashdata('success', 'Property data updated successfully!');
		redirect(base_url().'load_data/'.$loan_id);
		
	}
	
	public function update_monthly_income_fields(){

		if(isset($_POST['submit'])){

			$loan_id 			= $this->input->post('loanid');
			$data['unit_type'] 	= $this->input->post('unit_type');
			$data['items'] 		= $this->input->post('items');
			$data['size'] 		= $this->input->post('size');
			$data['bedroom'] 	= $this->input->post('bedroom');
			$data['bathroom'] 	= $this->input->post('bathroom');
			$data['amount'] 	= $this->input->post('amount');
			$data['notes'] 		= $this->input->post('notes');

			$where['id'] 		= $this->input->post('rowid');

			$this->User_model->updatedata('monthly_income',$where,$data);
			$this->session->set_flashdata('success', 'Property income data Updated successfully!');
			redirect(base_url().'load_data/'.$loan_id);
		}
	}

	public function update_cashflow_income_data(){

		$tableid = $this->input->post('table_id');

		$fetch_income_table = $this->User_model->select_where('monthly_income',array('id'=>$tableid));
		if($fetch_income_table->num_rows() > 0){

			$income_data = $fetch_income_table->result();
			echo json_encode($income_data);

		}else{

			echo 1;
		}
	}
	
	
	public function delete_property_home_id()
	{
		$id = $this->input->post('id');
		$loan_id = $this->input->post('loan_id');
		$this->User_model->delete('property_home',array('id'=>$id));
		$this->User_model->delete('loan_property',array('property_home_id'=>$id));
		$this->User_model->delete('encumbrances',array('property_home_id'=>$id));
		$this->User_model->delete('monthly_expense',array('property_home_id'=>$id));
		$this->User_model->delete('monthly_income',array('property_home_id'=>$id));
		$this->User_model->delete('loan_property_images',array('property_home_id'=>$id));
		$this->User_model->delete('loan_property_images',array('property_home_id'=>$id));
		$this->User_model->delete('loan_property_insurance',array('property_home_id'=>$id));
		$this->User_model->delete('loan_property_taxes',array('property_home_id'=>$id));
		$this->User_model->delete('property_folders',array('property_home_id'=>$id));
		$this->User_model->delete('comparable_sale',array('property_home_id'=>$id));
		
		$this->session->set_flashdata('popup_modal_hit','property_home');
		redirect(base_url().'load_data/'.$loan_id);
		
	}
	
	public function amount_format($n)
	{
		$n = str_replace(',','',$n);
		$a = trim($n , '$');
		$b = trim($a , ',');
		$c = trim($b , '%');
		return $c;
	}


	public function property_insurance_docs(){

		//if(isset($_POST['submit'])){

			// echo '<pre>';
			// print_r($_POST);
			// print_r($_FILES);
			// echo '</pre>';

			// die('ffff');

			$loan_id			= str_replace('/', '', $this->input->post('loan_id'));

			if($_FILES){
			
	            $property_home_id		= $this->input->post('property_home_id');
	            $talimar_no				= $this->input->post('talimar_no');

				//$folder = FCPATH.'property_insurance_doc/'.$talimar_no.'/'.$property_home_id.'/';
				$folder = 'property_insurance_doc/'.$talimar_no.'/'.$property_home_id.'/'; //FCPATH
				
				/*if(!is_dir($folder))
				{
					mkdir($folder, 0777, true);
					chmod($folder,0777);
				}*/
				
				foreach($_FILES['upload_docc'] as $key => $row_data){
					
					if($row_data)
					{
						$ext = strtolower(pathinfo($row_data, PATHINFO_EXTENSION));
						
						if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc')
						{
								
							$new_name 		= basename($row_data);
							$my_file_name 	= str_replace('.'.$ext, '_'.time().'.'.$ext, $new_name);
							$target_dir 	= $folder.$my_file_name;
								
							//move_uploaded_file($_FILES["upload_docc"]["tmp_name"], $target_dir);
							$attachmentName=$this->aws3->uploadFile($target_dir,$_FILES["upload_docc"]["tmp_name"]);
						
						}
					}
				}
				
				$this->session->set_flashdata('success', ' Document Uploaded!');
				redirect(base_url().'load_data/'.$loan_id,'refresh');
					
			}
		//}
	}
	
	/*
	public function load_property_data_updation()
	{
		$fetch_all_loans = $this->User_model->select_star('loan_property');
		$fetch_all_loans = $fetch_all_loans->result();
		foreach($fetch_all_loans as $row)
		{
			$data['talimar_loan'] 		= $row->talimar_loan;
			$data['property_address	'] 	= $row->property_address	;
			$data['priority'] 			= 1;
			$data['current_value'] 		= 0;
			$data['future_value'] 		= 0;
			$data['ltv'] 				= 0;
			
			$loan_id 					= NULL;
			$fetch_loan_data = $this->User_model->select_where('loan',array('talimar_loan'=>$row->talimar_loan));
			
			if($fetch_loan_data->num_rows() == 1){
				$fetch_loan_data 	= $fetch_loan_data->row();
				$loan_id 			= $fetch_loan_data->id;
			}
			$data['loan_id'] 			= $loan_id;
			$this->User_model->insertdata('property_home',$data);
			$insert_id = $this->db->insert_id();
			$this->User_model->updatedata('loan_property',array('id'=>$row->id),array('property_home_id'=>$insert_id));
		}
	}
	
	
	public function load_ecumbrances_updation()
	{
		$fetch_property_home = $this->User_model->select_star('property_home');
		$fetch_property_home = $fetch_property_home->result();
		foreach($fetch_property_home as $row_p_h)
		{
			$property_home_id	= $row_p_h->id;
			$talimar_loan 		= $row_p_h->talimar_loan;
			$loan_id 			= $row_p_h->loan_id;
			$fetch_encumbrances = $this->User_model->select_where('encumbrances',array('talimar_loan'=>$talimar_loan));
			if($fetch_encumbrances->num_rows() > 0)
			{
				$fetch_encumbrances = $fetch_encumbrances->result();
				foreach($fetch_encumbrances as $ecum)
				{
					$data['property_home_id'] 	= $property_home_id;
					$data['loan_id'] 			= $loan_id;
					$this->User_model->updatedata('encumbrances',array('id'=>$ecum->id),$data);
				}
				
			}
		}
	}
	
	public function update_property_data_loan_id()
	{
		$fetch_loan = $this->User_model->select_star('loan');
		$fetch_loan = $fetch_loan->result();
		foreach($fetch_loan as $loan)
		{
			$loan_id = $loan->id;
			$fetch_loan_property = $this->User_model->select_where('loan_property',array('talimar_loan'=>$loan->talimar_loan));
			if($fetch_loan_property->num_rows() > 0)
			{
				$this->User_model->updatedata('loan_property',array('talimar_loan'=>$loan->talimar_loan),array('loan_id'=>$loan_id));
			}
		}
		
	}
	*/
	
	public function form_monthly_income_expense()
	{
		$talimar_loan 			= $this->input->post('talimar_no');
		$loan_id 				= str_replace('/','',$this->input->post('loan_id'));
		$property_home_id 		= $this->input->post('property_home_id');
		
		$id_income 			= $this->input->post('id_income');
		$amount_income 		= $this->input->post('amount_income');
		$items_income 		= $this->input->post('items_income');
		
		
		foreach($id_income as $key => $row)
		{
			if($amount_income[$key] != ''){	
			
				if($row == 'new')
				{
					$sql = "INSERT INTO `monthly_income`( `talimar_loan`, `property_home_id`, `loan_id`, `items`, `amount`) VALUES ('".$talimar_loan."', '".$property_home_id."', '".$loan_id."', '".$items_income[$key]."', '".$this->amount_format($amount_income[$key])."' )";
				}
				else
				{
					$sql ="UPDATE `monthly_income` SET `items`='".$items_income[$key]."',`amount`='".$this->amount_format($amount_income[$key])."' WHERE id = '".$row."' ";
				}
				$this->User_model->query($sql);
			}
		}
		
		
		$id_expense 			= $this->input->post('id_expense');
		$amount_expense 		= $this->input->post('amount_expense');
		$items_expense 			= $this->input->post('items_expense');
		foreach($id_expense as $key => $row)
		{
			if($amount_expense[$key] != ''){
				if($row == 'new')
				{
					$sql = "INSERT INTO `monthly_expense`( `talimar_loan`, `property_home_id`, `loan_id`, `items`, `amount`) VALUES ('".$talimar_loan."', '".$property_home_id."', '".$loan_id."', '".$items_expense[$key]."', '".$this->amount_format($amount_expense[$key])."' )";
				}
				else
				{
					$sql ="UPDATE `monthly_expense` SET `items`='".$items_expense[$key]."',`amount`='".$this->amount_format($amount_expense[$key])."' WHERE id = '".$row."' ";
				}
				$this->User_model->query($sql);
			}
		}
		
		$this->session->set_flashdata('success','Cashflow updated successfully');
		$this->session->set_flashdata('popup_modal_hit', 'property_data_cash');
		$this->session->set_flashdata('property_home_id', $property_home_id);
				
		redirect(base_url().'load_data/'.$loan_id);
		// redirect(base_url().'load_data/'.$loan_id.'#property-'.$property_home_id);
	}
	
	public function update_monthly_income_expense_cashflow($id)
	{
		$fetch_property_home = $this->User_model->select_where('property_home',array('id'=>$id));
		$fetch_property_home = $fetch_property_home->result();
		foreach($fetch_property_home as $p_h)
		{
			// INCOME
			$sql = "INSERT INTO `monthly_income`(`talimar_loan`, `property_home_id`, `loan_id`, `items`, `amount`) VALUES ('".$p_h->talimar_loan."','".$p_h->id."','".$p_h->loan_id."','Gross Rental Income','0')";
			$this->User_model->query($sql);
			
			// EXPENSE
			$sql = "INSERT INTO `monthly_expense`(`talimar_loan`, `property_home_id`, `loan_id`, `items`, `amount`) VALUES ('".$p_h->talimar_loan."','".$p_h->id."','".$p_h->loan_id."','Property Tax','0')";
			$this->User_model->query($sql);
			
			// EXPENSE
			$sql = "INSERT INTO `monthly_expense`(`talimar_loan`, `property_home_id`, `loan_id`, `items`, `amount`) VALUES ('".$p_h->talimar_loan."','".$p_h->id."','".$p_h->loan_id."','Property Insurance','0')";
			$this->User_model->query($sql);
			
			// EXPENSE
			$sql = "INSERT INTO `monthly_expense`(`talimar_loan`, `property_home_id`, `loan_id`, `items`, `amount`) VALUES ('".$p_h->talimar_loan."','".$p_h->id."','".$p_h->loan_id."','Vacancy','0')";
			$this->User_model->query($sql);
			
			
		}
		
		return 1;
	}
	
	/*
	public function update_monthly_income_expense_cashflow_all()
	{
		$fetch_property_home = $this->User_model->select_star('property_home');
		$fetch_property_home = $fetch_property_home->result();
		foreach($fetch_property_home as $p_h)
		{
			// INCOME
			$sql = "INSERT INTO `monthly_income`(`talimar_loan`, `property_home_id`, `loan_id`, `items`, `amount`) VALUES ('".$p_h->talimar_loan."','".$p_h->id."','".$p_h->loan_id."','Gross Rental Income','0')";
			$this->User_model->query($sql);
			
			// EXPENSE
			$sql = "INSERT INTO `monthly_expense`(`talimar_loan`, `property_home_id`, `loan_id`, `items`, `amount`) VALUES ('".$p_h->talimar_loan."','".$p_h->id."','".$p_h->loan_id."','Property Tax','0')";
			$this->User_model->query($sql);
			
			// EXPENSE
			$sql = "INSERT INTO `monthly_expense`(`talimar_loan`, `property_home_id`, `loan_id`, `items`, `amount`) VALUES ('".$p_h->talimar_loan."','".$p_h->id."','".$p_h->loan_id."','Property Insurance','0')";
			$this->User_model->query($sql);
			
			// EXPENSE
			$sql = "INSERT INTO `monthly_expense`(`talimar_loan`, `property_home_id`, `loan_id`, `items`, `amount`) VALUES ('".$p_h->talimar_loan."','".$p_h->id."','".$p_h->loan_id."','Vacancy','0')";
			$this->User_model->query($sql);
			
			
		}
		
		return 1;
	}
	*/
	
	public function delete_monthly_income_row()
	{
		$id = $this->input->post('id');
		$this->User_model->delete('monthly_income',array('id'=>$id));
		echo 1;
	}
	public function delete_monthly_expense_row()
	{
		$id = $this->input->post('id');
		$this->User_model->delete('monthly_expense',array('id'=>$id));
		echo 1;
	}
	
	/*
	public function comparable_sale_data_updation()
	{
		$fetch_property_home = $this->User_model->select_star('property_home');
		$fetch_property_home = $fetch_property_home->result();
		foreach($fetch_property_home as $row_p_h)
		{
			$property_home_id	= $row_p_h->id;
			$talimar_loan 		= $row_p_h->talimar_loan;
			$loan_id 			= $row_p_h->loan_id;
			$fetch_comparable_sale = $this->User_model->select_where('comparable_sale',array('talimar_loan'=>$talimar_loan));
			if($fetch_comparable_sale->num_rows() > 0)
			{
				$fetch_comparable_sale = $fetch_comparable_sale->result();
				foreach($fetch_comparable_sale as $row_id)
				{
					$data['property_home_id'] 	= $property_home_id;
					$data['loan_id'] 			= $loan_id;
					$this->User_model->updatedata('comparable_sale',array('id'=>$row_id->id),$data);
				}
				
			}
		}
	}
	*/
	
	public function property_insurance_data_updation()
	{
		$fetch_property_home = $this->User_model->select_star('property_home');
		$fetch_property_home = $fetch_property_home->result();
		foreach($fetch_property_home as $row_p_h)
		{
			$property_home_id	= $row_p_h->id;
			$talimar_loan 		= $row_p_h->talimar_loan;
			$loan_id 			= $row_p_h->loan_id;
			$fetch_loan_property_insurance = $this->User_model->select_where('loan_property_insurance',array('talimar_loan'=>$talimar_loan));
			if($fetch_loan_property_insurance->num_rows() > 0)
			{
				$fetch_loan_property_insurance = $fetch_loan_property_insurance->result();
				foreach($fetch_loan_property_insurance as $row_id)
				{
					$data['property_home_id'] 	= $property_home_id;
					$data['loan_id'] 			= $loan_id;
					$this->User_model->updatedata('loan_property_insurance',array('id'=>$row_id->id),$data);
				}
				
			}
		}
	}
	
	public function add_property_insurance()
	{
		 error_reporting(0);
		$property_home_id 			= $this->input->post('property_home_id');
		$loan_id 					= str_replace('/','',$this->input->post('loan_id'));
		$talimar_loan				= $this->input->post('talimar_no');
		
		$id 						= $this->input->post('id');
		$required 					= $this->input->post('required');
		$insurance_type 			= $this->input->post('insurance_type');
		$policy_number 				= $this->input->post('policy_number');
		$request_updated_insurance 	= $this->input->post('request_updated_insurance');
		$donottrack 				= $this->input->post('donottrack');
		$annual_insurance_payment 	= $this->input->post('annual_insurance_payment');
		$monthly_payment 			= $this->input->post('monthly_payment');
		$payment_close 				= $this->input->post('payment_close');
		$insurance_carrier 			= $this->input->post('insurance_carrier');
		$insurance_expiration 		= $this->input->post('insurance_expiration');
		$contact_name 				= $this->input->post('contact_name');
		$contact_number 			= $this->input->post('contact_number');
		$contact_email 				= $this->input->post('contact_email');
		$property_single_mortage 	= $this->input->post('property_single_mortage');
		$c_notes					= $this->input->post('c_notes');
		$names_delete				= $this->input->post('names_delete');
		
		
		foreach($id as $key => $row)
		{
			$insurance_expiration_date = '';
			if($insurance_expiration[$key])
			{
				//$myDateTime = DateTime::createFromFormat('m-d-Y', $insurance_expiration[$key]);
				//$insurance_expiration_date = $myDateTime->format('Y-m-d');
				$insurance_expiration_date = input_date_format($insurance_expiration[$key]);
			}

			
			$insurance_carrier = '';
			if($row == 'new')
			{
				$insurance_carrier = input_sanitize($insurance_carrier[$key]);
				$sql = "INSERT INTO `loan_property_insurance`( `talimar_loan`, `property_home_id`, `loan_id`, `insurance_type`, `required`, `policy_number`, `request_updated_insurance`, `donottrack`, `insurance_carrier`, `insurance_expiration`, `annual_insurance_payment`, `monthly_payment`, `payment_close`, `contact_name`, `contact_number`, `contact_email`,`c_notes`,`names_delete`) VALUES ('".$talimar_loan."', '".$property_home_id."', '".$loan_id."', '".$insurance_type[$key]."', '".$required[$key]."', '".$policy_number[$key]."', '".$request_updated_insurance[$key]."', '".$donottrack[$key]."', '".$insurance_carrier."', '".$insurance_expiration_date."', '".$this->amount_format($annual_insurance_payment[$key])."', '".$this->amount_format($monthly_payment[$key])."', '".$payment_close[$key]."', '".$contact_name[$key]."', '".$contact_number[$key]."', '".$contact_email[$key]."', '".$c_notes[$key]."', '".$names_delete[$key]."')";
			}
			else
			{
				$insurance_carrier = input_sanitize($insurance_carrier[$key]);
				$sql = "UPDATE `loan_property_insurance` SET `required`='".$required[$key]."',`policy_number`='".$policy_number[$key]."',`request_updated_insurance`='".$request_updated_insurance[$key]."',`donottrack`='".$donottrack[$key]."',`insurance_carrier`='".$insurance_carrier."',`insurance_expiration`='".$insurance_expiration_date."',`annual_insurance_payment`='".$this->amount_format($annual_insurance_payment[$key])."',`monthly_payment`='".$this->amount_format($monthly_payment[$key])."',`payment_close`='".$payment_close[$key]."',`contact_name`='".$contact_name[$key]."',`contact_number`='".$contact_number[$key]."',`contact_email`='".$contact_email[$key]."', `c_notes`='".$c_notes[$key]."', `names_delete`='".$names_delete[$key]."' WHERE id = '".$row."' ";
			}
			$this->User_model->query($sql);


		}
		
		
		$fetch_input_datas = $this->User_model->select_where('input_datas',array('talimar_loan'=>$talimar_loan,'property_home_id'=>$property_home_id,'page'=>'property_insurance','input_name'=>'property_single_mortage'));
		if($fetch_input_datas->num_rows() > 0)
		{
			$this->User_model->updatedata('input_datas',array('talimar_loan'=>$talimar_loan,'property_home_id'=>$property_home_id,'page'=>'property_insurance','input_name'=>'property_single_mortage'),array('value'=>$property_single_mortage));
		}
		else
		{
			$this->User_model->insertdata('input_datas',array('talimar_loan'=>$talimar_loan,'property_home_id'=>$property_home_id,'page'=>'property_insurance','input_name'=>'property_single_mortage','value'=>$property_single_mortage));
		}
		
		$this->session->set_flashdata('success' ,' Property Insurance Updated Successfully!');
		// redirect(base_url().'load_data/'.$loan_id.'#property-'.$property_home_id);
		$this->session->set_flashdata('popup_modal_hit', 'property_insurance_modal');
		$this->session->set_flashdata('property_home_id', $property_home_id);
				
		redirect(base_url().'load_data/'.$loan_id);
	}
	
	public function add_property_home_rows(){
		$loan_id 		= str_replace('/','',$this->input->post('loan_id'));
		
		$sql_loan = $this->User_model->query("SELECT * FROM loan WHERE id = '".$loan_id."' ");
		$loan_result = $sql_loan->row();
		
		$talimar_loan 	= $loan_result->talimar_loan;
		
		// first check primary row exists or not for loan from "primary_home" table...
		
		$sql1 = $this->User_model->query("SELECT * FROM property_home WHERE talimar_loan = '".$talimar_loan."' AND primary_property = 1 ");
		if($sql1->num_rows() == 0){
			// insert row...
			$this->User_model->query("INSERT INTO property_home (talimar_loan,primary_property,loan_id) VALUES('".$talimar_loan."','1','".$loan_id."') ");
			$property_home_id = $this->db->insert_id();
			// echo $property_home_id;
			// die('dfdfdfdf');
			if($property_home_id)
			{
				$folder_names = array('Underwriting','Site Map','Site Photos');
				$creation_sequence = 0;
				foreach($folder_names as $name)
				{
					$creation_sequence++;
					$sql = "INSERT INTO `property_folders`(`talimar_loan`, `property_home_id`, `loan_id`, `creation_sequence`, `title`) VALUES ('".$talimar_loan."','".$property_home_id."','".$loan_id."','".$creation_sequence."','".$name."')";
					$this->User_model->query($sql);
					$folder_id = $this->db->insert_id();
					
					if($folder_id)
					{
						$folder_images = array('Site Map','Site Photos');
						foreach($folder_images as $name_folder)
						{
							if($name_folder == 'Site Map')
							{
								$description = array('Street Level Map','Area Map');
							}
							elseif($name_folder == 'Site Photos')
							{
								$description = array('Front of Residence','Kitchen','Living Room','Bedroom','Bathroom' ,'Rear of Residence');
							}
							
							foreach($description as $key => $dec)
							{
								$position = $key+1;
								$sql = "INSERT INTO `loan_property_images`(`talimar_loan`, `property_home_id`, `folder_id`, `loan_id`, `image_name`, `date`, `description`, `position`) VALUES ('".$talimar_loan."', '".$property_home_id."', '".$folder_id."',  '".$loan_id."', 'blank.jpg', '', '".$dec."','".$position."')";
								$this->User_model->query($sql);
							}
						}
					}
					
				}
			}
			
		}
		
		
		//check secondary rows(we need to add 3 rows) exists or not for loan from "primary_home" table...
		$sql2 = $this->User_model->query("SELECT * FROM property_home WHERE talimar_loan = '".$talimar_loan."' AND primary_property = 2 ");
		
		
		$initial_row 	= 3;
		$total_row 		= $sql2->num_rows();
		$needed_row		= $initial_row - $total_row;
		
		if($needed_row > 0){
			
			for($i = 1;$i<= $needed_row;$i++){
			
				$insert_property_home = array(
				
			
								'talimar_loan' 		=> $talimar_loan,
								'primary_property' 	=> 2,
								'loan_id' 			=> $loan_id,
								
						);
				// insert into 'property_home' table...
				$this->User_model->insertdata('property_home',$insert_property_home);
				$property_home_id = $this->db->insert_id();
				if($property_home_id)
				{
					$folder_names = array('Underwriting','Site Map','Site Photos');
					$creation_sequence = 0;
					foreach($folder_names as $name)
					{
						$creation_sequence++;
						$sql = "INSERT INTO `property_folders`(`talimar_loan`, `property_home_id`, `loan_id`, `creation_sequence`, `title`) VALUES ('".$talimar_loan."','".$property_home_id."','".$loan_id."','".$creation_sequence."','".$name."')";
						$this->User_model->query($sql);
						$folder_id = $this->db->insert_id();
						
						if($folder_id)
						{
							$folder_images = array('Site Map','Site Photos');
							foreach($folder_images as $name_folder)
							{
								if($name_folder == 'Site Map')
								{
									$description = array('Street Level Map','Area Map');
								}
								elseif($name_folder == 'Site Photos')
								{
									$description = array('Front of Residence','Kitchen','Living Room','Bedroom','Bathroom' ,'Rear of Residence');
								}
								
								foreach($description as $key => $dec)
								{
									$position = $key+1;
									$sql = "INSERT INTO `loan_property_images`(`talimar_loan`, `property_home_id`, `folder_id`, `loan_id`, `image_name`, `date`, `description`, `position`) VALUES ('".$talimar_loan."', '".$property_home_id."', '".$folder_id."',  '".$loan_id."', 'blank.jpg', '', '".$dec."','".$position."')";
									$this->User_model->query($sql);
								}
							}
						}
						
					}
				}
			}
		echo '1';
		}else{
			echo '0';
		}	
		
	}
	
	public function form_property_taxes()
	{
		$talimar_loan 					= $this->input->post('talimar_no');
		$loan_id 						= str_replace('/','',$this->input->post('loan_id'));
		$property_home_id 				= $this->input->post('property_home_id');
		
		//loan_property_taxes
		$data['annual_tax']				= $this->amount_format($this->input->post('annual_tax'));
		$data['mello_roos']				= $this->input->post('mello_roos');
		$data['delinquent']				= $this->input->post('delinquent');
		$data['post_delinquent']		= $this->input->post('post_delinquent');
		$data['delinquent_amount']		= $this->amount_format($this->input->post('delinquent_amount'));
		$data['post_delinquent_amount']	= $this->amount_format($this->input->post('post_delinquent_amount'));
		
		$data['tax_assessor_phone']		= $this->input->post('tax_assessor_phone');
		
		$fetch_loan_property_taxes = $this->User_model->select_where('loan_property_taxes',array('talimar_loan'=>$talimar_loan,'loan_id'=>$loan_id,'property_home_id'=>$property_home_id));
		if($fetch_loan_property_taxes->num_rows() > 0)
		{
			$this->User_model->updatedata('loan_property_taxes',array('talimar_loan'=>$talimar_loan,'loan_id'=>$loan_id,'property_home_id'=>$property_home_id),$data);
			$this->session->set_flashdata('success',' Property Taxes Updated Successfully');
		}
		else
		{
			$data['talimar_loan']				= $talimar_loan;
			$data['loan_id']					= $loan_id;
			$data['property_home_id']			= $property_home_id;
			$this->User_model->insertdata('loan_property_taxes',$data);
			$this->session->set_flashdata('success',' Property Taxes Records added Successfully');
		}
		
		// redirect(base_url().'load_data/'.$loan_id.'#property-'.$property_home_id);
		$this->session->set_flashdata('popup_modal_hit', 'property_data');
		$this->session->set_flashdata('property_home_id', $property_home_id);
				
		redirect(base_url().'load_data/'.$loan_id);
	}
	
	public function form_upload_property_images()
	{
		
 		$file = $_FILES['file'];
		$id						= $this->input->post('id');

  // echo '<pre>';
		// print_r($id);
		// echo '</pre>'; 
		//  die('stop'); 
		$loan_id				= str_replace('/','',$this->input->post('loan_id'));
		$property_home_id		= $this->input->post('property_home_id');
		$talimar_loan			= $this->input->post('talimar_no');
		$folder_id				= $this->input->post('folder_id');
		$date					= $this->input->post('date');
		$description			= $this->input->post('description');
		//   echo '<pre>';
		// print_r($file);
		// echo '</pre>'; 
		//  die('stop'); 
		$folder = 'all_property_images/'.$talimar_loan.'/'.$property_home_id.'/'.$folder_id.'/'; //FCPATH
		
		/*if(!is_dir($folder))
		{
			mkdir($folder, 0777, true);
		}	*/	
		
		foreach($id as $key => $row)
		{
			//echo $row.'fff<br>';
			if($row == 'new')
			{
				$ext 			= strtolower(pathinfo($file['name'][$key], PATHINFO_EXTENSION));
				$check_error 	= $file['error'][$key];
				if(($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') && $check_error == 0 )
				{
					$image_name = date('Ymdhis').'-'.rand(1,9999).'.'.$ext;
					$sql ="INSERT INTO `loan_property_images`(`talimar_loan`, `property_home_id`, `folder_id`, `loan_id`, `image_name`, `date`, `description`, `position`) VALUES ('".$talimar_loan."', '".$property_home_id."',  '".$folder_id."', '".$loan_id."', '".$image_name."', '".$date[$key]."', '".$description[$key]."', '".$key."')";
					$this->User_model->query($sql);
					$target_dir 	= $folder.$image_name;
					//move_uploaded_file($file['tmp_name'][$key], $target_dir);
					$attachmentName=$this->aws3->uploadFile($target_dir,$file['tmp_name'][$key]);
				}else{

					$sql ="INSERT INTO `loan_property_images`(`talimar_loan`, `property_home_id`, `folder_id`, `loan_id`, `date`, `description`, `position`) VALUES ('".$talimar_loan."', '".$property_home_id."',  '".$folder_id."', '".$loan_id."','".$date[$key]."', '".$description[$key]."', '".$key."')";
					$this->User_model->query($sql);

				}
			}
			else
			{
				$ext 			= strtolower(pathinfo($file['name'][$key], PATHINFO_EXTENSION));
				$check_error 	= $file['error'][$key];
				if(($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') && $check_error == 0 )
				{
					$image_name = date('Ymdhis').'-'.rand(1,9999).'.'.$ext;
					// 
					$fetch = $this->User_model->select_where('loan_property_images',array('id'=>$row));
					if($fetch->num_rows() > 0)
					{
						$fetch = $fetch->row();
						$previous_image_name = $fetch->image_name;
						/*if(file_exists($folder.$previous_image_name))
						{
							unlink($folder.$previous_image_name);
						}*/
						if(getAwsUrl($folder.$previous_image_name)){
							$this->aws3->deleteObject($folder.$previous_image_name);
						}
						
						$this->User_model->updatedata('loan_property_images',array('id'=>$row),array('image_name'=>$image_name,'description'=>$description[$key],'date'=>$date[$key]));
						
						
						
						$target_dir 	= $folder.$image_name;
						//move_uploaded_file($file['tmp_name'][$key], $target_dir);
						$attachmentName=$this->aws3->uploadFile($target_dir,$file['tmp_name'][$key]);
					}
					
					
				}
				else
				{
					$this->User_model->updatedata('loan_property_images',array('id'=>$row),array('description'=>$description[$key],'date'=>$date[$key]));
				}
			}
		}
			
		$this->session->set_flashdata('success', ' Property Images updated');
		redirect(base_url().'load_data/'.$loan_id.'#image-folder-'.$property_home_id);
		
	}
	
	public function delete_property_images()
	{
		$id = $this->input->post('id');
		$fetch_loan_property_images = $this->User_model->select_where('loan_property_images',array('id'=>$id));
		if($fetch_loan_property_images->num_rows() > 0)
		{
			$loan_property_images 	= $fetch_loan_property_images->row();
			$talimar_loan 			= $loan_property_images->talimar_loan;
			$property_home_id 		= $loan_property_images->property_home_id;
			$image_name 			= $loan_property_images->image_name;
			$folder_id 				= $loan_property_images->folder_id;
			
			//$file = FCPATH.'all_property_images/'.$talimar_loan.'/'.$property_home_id.'/'.$folder_id.'/'.$image_name;
			$file = 'all_property_images/'.$talimar_loan.'/'.$property_home_id.'/'.$folder_id.'/'.$image_name; //FCPATH
			$this->User_model->delete('loan_property_images',array('id'=>$id));
			$this->aws3->deleteObject($file);
			/*if(file_exists($file))
			{
				unlink($file);
			}*/
		}
	}
	
	public function redirect_to_property_close($loan_id)
	{
		redirect(base_url().'load_data/'.$loan_id.'#property-data-home-page');
	}
	
	/*
	public function property_folder_updation()
	{
		$fetch_property_home = $this->User_model->select_star('property_home');
		$fetch_property_home = $fetch_property_home->result();
		foreach($fetch_property_home as $row_p_h)
		{
			$property_home_id	= $row_p_h->id;
			$talimar_loan 		= $row_p_h->talimar_loan;
			$loan_id 			= $row_p_h->loan_id;
			
			$data['talimar_loan'] 		= $talimar_loan;
			$data['property_home_id'] 	= $property_home_id;
			$data['loan_id'] 			= $loan_id;
			$data['title'] 				= 'Underwriting';
			$this->User_model->insertdata('property_folders',$data);
			
		}
	}
	*/
	
	public function form_images_folder()
	{
		$talimar_loan		= $this->input->post('talimar_no');
		$loan_id			= str_replace('/','',$this->input->post('loan_id'));
		$property_home_id	= $this->input->post('property_home_id');
		
		$id 				= $this->input->post('id');
		$title 				= $this->input->post('title');
		// check folders into "property_folders" table...
		
		$sql_check  = $this->User_model->query("SELECT * FROM property_folders WHERE talimar_loan = '".$talimar_loan."' ");
		if($sql_check->num_rows() == 0){
			$sequence = 1;
		}else{
			
			$sequence = ($sql_check->num_rows())+ 1;
		}
		foreach($id as $key => $row)
		{
			if($row == 'new')
			{
				$sql = "INSERT INTO `property_folders`(`talimar_loan`, `property_home_id`, `loan_id`,`creation_sequence`, `title`) VALUES ('".$talimar_loan."', '".$property_home_id."', '".$loan_id."', '".$sequence."','".$title[$key]."')";
			}
			else
			{
				$sql = "UPDATE `property_folders` SET `title` = '".$title[$key]."' WHERE id= '".$row."' ";
			}
			
			if($title[$key])
			{
				$this->User_model->query($sql);
			}
		}
		$this->session->set_flashdata('success', 'Property Folders Updated');
		// redirect(base_url().'load_data/'.$loan_id.'#property-'.$property_home_id);
		$this->session->set_flashdata('popup_modal_hit', 'property_data_folder');
		$this->session->set_flashdata('property_home_id', $property_home_id);
				
		redirect(base_url().'load_data/'.$loan_id);
	}
	
	public function property_hidden_form()
	{
		$talimar_loan		= $this->input->post('talimar_no');
		$loan_id			= str_replace('/','',$this->input->post('loan_id'));
		$property_home_id	= $this->input->post('property_home_id');
		$table				= $this->input->post('table');
		
		$data['talimar_loan'] 		= $talimar_loan;
		$data['loan_id'] 			= $loan_id;
		
		if($table == 'property_home')
		{
			$fetch_property_home = $this->User_model->select_where('property_home',array('talimar_loan'=>$talimar_loan));
			$primary_property = '1';
			if($fetch_property_home->num_rows() > 0)
			{
				$primary_property = '2';
			}
			
			
			if($primary_property == 2)
			{
				$data['primary_property'] = 2;
				$this->User_model->insertdata('property_home',$data);
			}
			else
			{
				$data['primary_property'] = 1;
				$this->User_model->insertdata('property_home',$data);
			}
			
			$insert_id = $this->db->insert_id();
			if($insert_id)
			{
				$loan_property['property_home_id'] 	= $insert_id;
				$loan_property['talimar_loan'] 		= $talimar_loan;
				$loan_property['loan_id'] 			= $loan_id;
				// Insert Into loan_property
				$this->User_model->insertdata('loan_property',$loan_property);
				
				// Insert Into property_folders
				$property_folders['property_home_id'] 	= $insert_id;
				$property_folders['talimar_loan'] 		= $talimar_loan;
				$property_folders['loan_id'] 			= $loan_id;
				$property_folders['title'] 				= 'Underwriting';
				$this->User_model->insertdata('property_folders',$property_folders);
				
			}
			redirect(base_url().'load_data/'.$loan_id.'#property-'.$insert_id);
		}
		elseif($table == 'ecumbrances')
		{
			$data['property_home_id'] 			= $property_home_id;
			$this->User_model->insertdata('encumbrances',$data);
			$insert_id = $this->db->insert_id();
			if($insert_id)
			{
				echo $insert_id;
			}
		}
	}
	
	
	// public function dynamic_property_folders_create()
	// {
		// $fethc_loan_property = $this->User_model->select_star('loan_property');
		// $fethc_loan_property = $fethc_loan_property->result();
		
		// foreach($fethc_loan_property as $property)
		// {
			// $folder_names = array('Underwriting','Site Map','Site Photos');
			// $creation_sequence = 0;
			// foreach($folder_names as $name)
			// {
				// $creation_sequence++;
				// $sql = "INSERT INTO `property_folders`(`talimar_loan`, `property_home_id`, `loan_id`, `creation_sequence`, `title`) VALUES ('".$property->talimar_loan."','".$property->property_home_id."','".$property->loan_id."','".$creation_sequence."','".$name."')";
				// $this->User_model->query($sql);
			// }
		// }
	// }
	
	// public function dynamic_property_images_edit()
	// {
		// $folder_names = array('Site Map','Site Photos');
		// foreach($folder_names as $name)
		// {
			// $fetch_property_folders = $this->User_model->select_where('property_folders',array('title'=>$name));
			// $fetch_property_folders = $fetch_property_folders->result();
			
			// foreach($fetch_property_folders as $folder)
			// {
				// if($name == 'Site Map')
				// {
					// $description = array('Street Level Map','Area Map');
				// }
				// elseif($name == 'Site Photos')
				// {
					// $description = array('Front of Residence','Kitchen','Living Room','Bedroom','Bathroom' ,'Rear of Residence');
				// }
				
				// foreach($description as $key => $dec)
				// {
					// $position = $key+1;
					// $sql = "INSERT INTO `loan_property_images`(`talimar_loan`, `property_home_id`, `folder_id`, `loan_id`, `image_name`, `date`, `description`, `position`) VALUES ('".$folder->talimar_loan."', '".$folder->property_home_id."', '".$folder->id."',  '".$folder->loan_id."', 'blank.jpg', '', '".$dec."','".$position."')";
					// $this->User_model->query($sql);
				// }
			// }
		// }
		
	// }
	
	

	// 1
	public function update_loan_ids()
	{
		$fetch_loan = $this->User_model->select_star('loan');
		$fetch_loan = $fetch_loan->result();
		foreach($fetch_loan as $loan)
		{
			$loan_id = $loan->id;
			$talimar_loan = $loan->talimar_loan;
			$fetch_property_home = $this->User_model->select_where('property_home',array('talimar_loan'=>$talimar_loan));
			if($fetch_property_home->num_rows() > 0)
			{
				$this->User_model->updatedata('property_home',array('talimar_loan'=>$talimar_loan),array('loan_id'=>$loan_id));
			}
		}
	}
	
	// 2
	public function delete_null_loan_ids()
	{
		$sql = "DELETE FROM property_home WHERE loan_id IS NULL";
		$this->User_model->query($sql);
	}
	
	// 3
	public function property_folder_delete_null_loan_ids()
	{
		$sql = "DELETE FROM property_folders WHERE loan_id IS NULL";
		$this->User_model->query($sql);
		
		$sql = "DELETE FROM property_folders WHERE loan_id = '' ";
		$this->User_model->query($sql);
	}
	
	// 4
	public function property_images_delete_null_loan_ids()
	{
		$sql = "DELETE FROM loan_property_images WHERE loan_id IS NULL";
		$this->User_model->query($sql);
		
		$sql = "DELETE FROM loan_property_images WHERE loan_id = '' ";
		$this->User_model->query($sql);
	}
	
	//5 function to update folder 
	public function dynamic_property_folders_create()
	{
		$fetch_property_home = $this->User_model->select_star('property_home');
		$fetch_property_home = $fetch_property_home->result();
		foreach($fetch_property_home as $property_home)
		{
			$loan_id 			= $property_home->loan_id;
			$talimar_loan 		= $property_home->talimar_loan;
			$property_home_id 	= $property_home->id;
			
			$folder_names_array = array(1=>'Underwriting',2=>'Site Map',3=>'Site Photos');
			foreach($folder_names_array as $creation_sequence => $folder)
			{
				$folder_names = $folder;
				$fetch_folder = $this->User_model->select_where('property_folders',array('title'=>$folder_names,'loan_id'=>$loan_id,'talimar_loan'=>$talimar_loan,'property_home_id'=>$property_home_id));
				if($fetch_folder->num_rows() == 0)
				{
					$this->User_model->insertdata('property_folders',array('title'=>$folder_names,'loan_id'=>$loan_id,'talimar_loan'=>$talimar_loan,'property_home_id'=>$property_home_id,'creation_sequence'=>$creation_sequence));
					$folder_id = $this->db->insert_id();
					if($folder_names == 'Site Map')
					{
						$description = array('Street Level Map','Area Map');
					}
					elseif($folder_names == 'Site Photos')
					{
						$description = array('Front of Residence','Kitchen','Living Room','Bedroom','Bathroom' ,'Rear of Residence');
					}
					elseif($folder_names == 'Underwriting')
					{
						$description = array(1=>'',2=>'',3=>'',4=>'',5=>'' ,6=>'');
					}
					
					foreach($description as $key => $dec)
					{
						$position = $key+1;
						$sql = "INSERT INTO `loan_property_images`(`talimar_loan`, `property_home_id`, `folder_id`, `loan_id`, `image_name`, `date`, `description`, `position`) VALUES ('".$talimar_loan."', '".$property_home_id."', '".$folder_id."',  '".$loan_id."', 'blank.jpg', '', '".$dec."','".$position."')";
						$this->User_model->query($sql);
					}
				}
				
			}
			 
		}
		
	}
	
	public function print_insurence(){
		
		error_reporting(0);
		$loan_id = $this->uri->segment(2);
		
		
		//fetch talimar loan number...
		$sql = "SELECT talimar_loan FROM `loan` WHERE `id`='".$loan_id."'";
		$fetch_talimar_loan = $this->User_model->query($sql);
		$fetch_talimar_loan = $fetch_talimar_loan->row();
		$talimar_loan 		= $fetch_talimar_loan->talimar_loan;
		
		header("Cache-Control: ");// leave blank to avoid IE errors
			header("Pragma: ");// leave blank to avoid IE errors
			header("Content-type: application/octet-stream");
			// header("Content-type: application/vnd.ms-word");

			header("content-disposition: attachment;filename=TaliMar_$talimar_loan.doc");
		echo '<html xmlns:v="urn:schemas-microsoft-com:vml"
				xmlns:o="urn:schemas-microsoft-com:office:office"
				xmlns:w="urn:schemas-microsoft-com:office:word"
				xmlns="http://www.w3.org/TR/REC-html40">

				<head>
				<meta http-equiv=Content-Type content="text/html; charset=utf-8">
				<meta name=ProgId content=Word.Document>
				<meta name=Generator content="Microsoft Word 9">
				<meta name=Originator content="Microsoft Word 9">
				<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->
				<title>title</title>
				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				body{
					font-size:10pt;
				}
				</style>
				</head>
				<body>';

echo '<style>

		table.table{
			width:100%;
			padding : 3px 0px;
		}
		table.table th{
			text-align : left;
			font-size : 10pt;
			
		}
		table.table td{
			font-size:10pt;
			font-size : 10pt;
		
		}
		table.two_contact_signer td{
			margin : 3px;
			font-size:10pt;
		}
		h1{
			text-align:center;
		}
		h2{
			text-align:center;
		}
		p{
			font-size:10pt;
			word-spacing : 10px;
			padding : 2px 0px;
		}
		table{
			font-size:10pt;
		}
</style>';
		
		
		
		//fetch all loan table info...
		$sql1 = "SELECT * FROM `loan` WHERE `id`='".$loan_id."'";
		$fetch_loan_data = $this->User_model->query($sql1);
		$fetch_loan_data = $fetch_loan_data->result();
		// $talimar_loan=$property_loan[0]->property_address
		
		// echo'<pre>';
		// print_r($fetch_loan_data);
		// echo'</pre>';
		
		//fetch borrower name...
		$borrower_name = "SELECT * FROM `borrower_data` WHERE id='".$fetch_loan_data[0]->borrower."'";
		$fetch_borrower_name = $this->User_model->query($borrower_name);
		$fetch_borrower_name = $fetch_borrower_name->row();
		$fetch_borrower_name1 = $fetch_borrower_name->b_name;
		$fetch_borrower_title = $fetch_borrower_name->c_title;
		$company_name 			= $fetch_borrower_name->company_name;
		
		//fetch borrower contact data...
		$b_contact = "SELECT * FROM `borrower_contact` WHERE `borrower_id` = '".$fetch_loan_data[0]->borrower ."'";
		$fetch_borrower_contact = $this->User_model->query($b_contact);
		$fetch_borrower_contact = $fetch_borrower_contact->result();
		
		//fetch property loan data...
		$property_loan = $this->User_model->query("select * from loan_property where talimar_loan = '".$talimar_loan."'");
		$property_loan = $property_loan->result();
		$property_address = $property_loan[0]->property_address .''.$property_loan[0]->unit .'; '.$property_loan[0]->city .', '.$property_loan[0]->state .' '.$property_loan[0]->zip;
		
		//loan_property_insurance...
			$sql = "SELECT * FROM `loan_property_insurance` WHERE `talimar_loan`='".$talimar_loan."' AND `insurance_type` = '1'";
			$fetch_insurance = $this->User_model->query($sql);
			$fetch_insurance = $fetch_insurance->result();
			$c_notes		 = $fetch_insurance[0]->c_notes;
		
		//fetch loan_assignment data...
		$loan_assignment = "SELECT * FROM `loan_assigment` WHERE talimar_loan='".$talimar_loan ."'";
		$fetch_assignment = $this->User_model->query($loan_assignment);
		$fetch_assignment = $fetch_assignment->result();
		foreach($fetch_assignment as $row){
			
			$fetch_lender[]	 = $row->lender_name;
			
			
		}		
		
		$lender = implode("," , $fetch_lender);
		
		//fetch investor data...
		$investor = "SELECT * FROM `investor` WHERE id IN(".$lender.")";
		$investor = $this->User_model->query($investor);
		$investor = $investor->result();
		
			
		foreach($investor as $name){
			
			$inv_name[] = $name->name;
		   
		}
		
		$sql11 = "SELECT * FROM `input_datas` WHERE `page`='property_insurance' AND talimar_loan='".$talimar_loan."'";

			$fetch_insurancee = $this->User_model->query($sql11);
			$fetch_insurancee = $fetch_insurancee->result();

			$select_investor_name1 = $fetch_insurancee[0]->value;
		
		
			$select_investor_name = implode(", ", $inv_name);
			
		
			
			//$insurance_endrosement .= '';
			$insurance_endrosement .='<h2 style="font-family:Times New Roman;font-size:18px;">INSURANCE ENDORSEMENT REQUEST</h2>';
						
			$insurance_endrosement .= '<table class="table" style="width:100%;vertical-align:top;">';
		
			$insurance_endrosement .= '<tr>';
			$insurance_endrosement .= '<td style="width:40%;">Loan Document Date:</td>';
			$insurance_endrosement .= '<td style="width:60%;">'.date('m-d-Y', strtotime($fetch_loan_data[0]->loan_document_date)).'</td>';
			$insurance_endrosement .= '</tr>';
			
			$insurance_endrosement .= '<tr>';
			$insurance_endrosement .= '<td style="width:40%;">Loan Number:</td>';
			$insurance_endrosement .= '<td style="width:60%;">'.$talimar_loan.'</td>';
			$insurance_endrosement .= '</tr>';
			
			$insurance_endrosement .= '<tr>';
			$insurance_endrosement .= '<td style="width:40%;">Property Address:</td>';
			$insurance_endrosement .= '<td style="width:60%;">'.$property_address.'</td>';
			$insurance_endrosement .= '</tr>';
			
			
			$insurance_endrosement .= '<tr>';
			$insurance_endrosement .= '<td style="width:40%;">Borrower Name:</td>';
			$insurance_endrosement .= '<td style="width:60%;">'.$fetch_borrower_name1.'</td>';
			$insurance_endrosement .= '</tr>';
			
			$insurance_endrosement .= '</table>';
			
			$insurance_endrosement .= '<br>';
			
			$insurance_endrosement .= '<p>1.&nbsp; Please add to the existing fire/casualty policy the following loss payable endorsement: </p>';
			$insurance_endrosement .='<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;See Attached Lender Schedule</p>';
			if(isset($select_investor_name1) && !empty($select_investor_name1)){
				
				
			$insurance_endrosement .= '<p>2.&nbsp; Mail all notifications to '.$select_investor_name1.', 11440 West Bernardo Ct., Suite. #210 San Diego, CA 92127.<br><br></p>';	

			}else{
				
			$insurance_endrosement .= '<p >.&nbsp; Mail all notifications to '.$select_investor_name.', 11440 West Bernardo Ct., Suite. #210 San Diego, CA 92127.<br><br></p>';
			}
			
			
			//$insurance_endrosement .= '<p>2.&nbsp; Please delete the interest of all the following: <br><br></p>';
			//$insurance_endrosement .= '<p style="margin-left:25px;">**None to Delete**<br><br></p>';
			
			
			$insurance_endrosement .= '<p>3.&nbsp; The fire and casualty insurance is in an amount which provides for guaranteed replacement cost. If the insured does not have a policy which provides for guaranteed replacement, we require that the amount of insurance be for the replacement cost of the structure and/or improvements. Guaranteed replacement including current municipal code state building standards.<br><br></p>';
			$insurance_endrosement .= '<p>4.&nbsp; We require the loss payee clause be a 438BFU endorsement.<br><br></p>';
			
			
			$insurance_endrosement .= '<p>5.&nbsp; If any additional premiums are incurred for these changes, please bill the insured. Please forward a copy of the policy showing our loss payable clause as soon as possible.<br><br></p>';
			
			$insurance_endrosement .= '<p>6.&nbsp; Additional Notes:<br>'.$c_notes.'<br></p>';
			
			
			$insurance_endrosement .= '<p>&nbsp;</br></p>';
			$insurance_endrosement .= '<p>'.$company_name.'</p>';
			$insurance_endrosement .= '<p>&nbsp;</br></br></p>';
			
			$insurance_endrosement .= '<table class="table" style="width:50%;vertical-align:top;">';
			
			foreach($fetch_borrower_contact as $row){
				
				if($row->contact_responsibility == '1'){
				
				$contact = "SELECT * FROM contact WHERE contact_id = '".$row->contact_id."'";
				$fetch_contact = $this->User_model->query($contact);
				$fetch_contact = $fetch_contact->row();
				$fetch_name = $fetch_contact->contact_suffix .' '.$fetch_contact->contact_middlename .' '.$fetch_contact->contact_lastname;
				
				$insurance_endrosement .= '<tr>';
				$insurance_endrosement	.= '<td style="border-top: 1px solid black;">';
				$insurance_endrosement	.= isset($fetch_name) ? $fetch_name : '';
				$insurance_endrosement	.= '<br>';
				 
				$insurance_endrosement	.= isset($row->c_title) ? $row->c_title : '';
				 
				$insurance_endrosement	.= '</td>';
				$insurance_endrosement	.= '<td style="border-top: 1px solid black;text-align:right;vertical-align:top;">Date</td>';
			 
				$insurance_endrosement .= '</tr>';
				$insurance_endrosement .= '<tr style="margin-bottom:10px;">&nbsp;<br></tr>';
				$insurance_endrosement .= '<tr style="margin-bottom:10px;">&nbsp;<br></tr>';
				$insurance_endrosement .= '<tr style="margin-bottom:10px;">&nbsp;<br></tr>';
				$insurance_endrosement .= '<tr style="margin-bottom:10px;">&nbsp;<br></tr>';
				
				}
			
			}
			
			$insurance_endrosement .= '</table>';

			$insurance_endrosement 	.='<br style="page-break-before: always">';
                 

			$talimar_noo['talimar_loan'] = $talimar_loan;
			$fetch_assignment_dataa = $this->User_model->select_where('loan_assigment',$talimar_noo);
			$fetch_assignment_dataa = $fetch_assignment_dataa->result();

            foreach($fetch_assignment_dataa as $row)
				{
				
					$fetch_invester_idd['id']	= $row->lender_name; 
				
					$fetch_investor_data	= $this->User_model->select_where('investor',$fetch_invester_idd);
					$fetch_investor_data	= $fetch_investor_data->result();
			

				}
			
			
            $tali_draft_checked = '';
			if(isset($fetch_extra_details[0]->draft_talimar_inc))
			{
				if($fetch_extra_details[0]->draft_talimar_inc == 1)
				{
					$tali_draft_checked 				= 1;
					$tali_draft_lender_id['id'] 		= 10;
					$fetch_draft_lender_data = $this->User_model->select_where('investor',$tali_draft_lender_id);
					$fetch_draft_lender_data = $fetch_draft_lender_data->result();
					
					$draft_lender_name 				= $fetch_draft_lender_data[0]->name;
					$draft_lender_vesting 			= $fetch_draft_lender_data[0]->vesting;
					$draft_lender_entity_name		= $fetch_draft_lender_data[0]->entity_name;
					
					if($fetch_draft_lender_data[0]->investor_type == 3)
					{
						$draft_lender_entity_name = $fetch_draft_lender_data[0]->directed_IRA_NAME;
					}
					
					
					
					$draft_lender_yield_percent		= $fetch_loan_data[0]->intrest_rate - $fetch_servicing_data[0]->servicing_fee;
					
					$draft_lender_monthly_payment 	= (($draft_lender_yield_percent/100) * $fetch_loan_data[0]->loan_amount) / 12;
					
					$draft_lender_full_address		= $fetch_draft_lender_data[0]->address.'; '.$fetch_draft_lender_data[0]->city.', '.$fetch_draft_lender_data[0]->state.' '.$fetch_draft_lender_data[0]->zip;
					
					$lender_contact_id['lender_id']	= 10;
					$fetch_lender_contact = $this->User_model->select_where('lender_contact',$lender_contact_id);
					if($fetch_lender_contact->num_rows() > 0)
					{
						$fetch_lender_contact  			= $fetch_lender_contact->result();
						
						$draft_lender_contact1_title 	= $fetch_lender_contact[0]->contact_title ?$fetch_lender_contact[0]->contact_title : '';
						
						$draft_lender_contact2_title 	= $fetch_lender_contact[1]->contact_title ? $fetch_lender_contact[1]->contact_title : '';
						foreach($fetch_lender_contact as $row)
						{
							$lender_contact_id['contact_id'] 	= $row->contact_id;
							
							// FETCH CONTACT DATA FROM contact table
							$fetch_lender_contact_data 	= $this->User_model->select_where('contact',$lender_contact_id);
							if($fetch_lender_contact_data->num_rows() > 0){
								
								$fetch_lender_contact_data = $fetch_lender_contact_data->result();
								
								$draft_lender_contact1     = $fetch_lender_contact_data[0]->contact_firstname.' '.$fetch_lender_contact_data[0]->contact_middlename.' '.$fetch_lender_contact_data[0]->contact_lastname;
								
								$draft_lender_contact1_phone     = $fetch_lender_contact_data[0]->contact_phone ? $fetch_lender_contact_data[0]->contact_phone : '';
								
								$draft_lender_contact1_email     = $fetch_lender_contact_data[1]->contact_email ?$fetch_lender_contact_data[0]->contact_email : '';
							}
						}
					}
				}
			}

			$select_investor_vesting 			= $tali_draft_checked ? $draft_lender_vesting : $fetch_investor_data[0]->vesting;

              $talimar_no['talimar_loan'] = $talimar_loan;
			$fetch_assignment_data = $this->User_model->select_where('loan_assigment',$talimar_no);
			$fetch_assignment_data = $fetch_assignment_data->result();
			
			
			$insurance_endrosement 	.='<h2 style="font-family:Times New Roman;font-size:13.5px;">Lender Schedule</h2>';
			if($tali_draft_checked)
			{
				$insurance_endrosement	.= '<p> '.$select_investor_vesting.', as to an undivided 100% interest</p>';
			}
			else
			{
				foreach($fetch_assignment_data as $row)
				{
					$promissory_note_len_investment_percent = (float)($row->investment * 100)/$fetch_loan_data[0]->loan_amount;
					$fetch_invester_id['id']	= $row->lender_name; 
				
					$fetch_invester_data_1	= $this->User_model->select_where('investor',$fetch_invester_id);
					$fetch_invester_data_1	= $fetch_invester_data_1->result();
					$insurance_endrosement	.= '<p> '.$fetch_invester_data_1[0]->vesting.', as to an undivided '.number_format($promissory_note_len_investment_percent,8).'% interest</p>';
				}
			}
			echo $insurance_endrosement;
	}

	public function edit_property(){


        $talimar_no=$this->input->post('talimar_no');
        $property_home_id=$this->input->post('property_home_id');
        $loan_id=str_replace('/','',$this->input->post('loan_id'));
     	$street=trim(str_replace('-', '—', $this->input->post('prop_streette')));
        $state=trim($this->input->post('prop_statee'));
        // $unit=$this->input->post('prop_unitt');
        $city=trim($this->input->post('prop_cityy'));
        $zip=trim($this->input->post('prop_zipp'));
      

		$data['property_address']=$street.'-'.$city.',- ' .$state.'- ' .$zip;
		$data['prop_type']=$this->input->post('prop_typee');
		$data['prop_unit']=$this->input->post('prop_unit');
		$data['unit']=$this->input->post('uni');
		$data['prop_building']=str_replace(",","",$this->input->post('prop_buildingg'));
		$lot_size=str_replace(",","",$this->input->post('prop_lott'));
		$data['lot_size']=str_replace(",","",$lot_size);
		$data['bedrooms']=$this->input->post('prop_bedss');
		$data['bathroom']=$this->input->post('prop_bathss');
		$data['year_built']=$this->input->post('prop_yearr');

		$dollar_replace_salePrice=str_replace(",","",$this->input->post('prop_sale_pricee'));
		$data['sale_price']=str_replace("$","",$dollar_replace_salePrice);
		$data['unit']=trim($this->input->post('uni'));

		if($this->input->post('prop_sale_datee')){

				//$myDateTime 					= DateTime::createFromFormat('m-d-Y', $this->input->post('prop_sale_datee'));
				$newDateString 					= input_date_format($this->input->post('prop_sale_datee'));//$myDateTime->format('m-d-Y');

				$prop_new_sale_date		 	    = $newDateString ? $newDateString : date('m-d-Y');
		 } 



		$data['date_sold']=$prop_new_sale_date;

        $p_bulid=str_replace(",","",$this->input->post('prop_buildingg'));
        $new_p_bulid=str_replace(",","",$p_bulid);
        $prop_s_price_dollar=str_replace(",","",$this->input->post('prop_sale_pricee'));
        $prop_s_price=str_replace("$","",$prop_s_price_dollar);
        $new_prop_s_price=str_replace(",","",$prop_s_price);

        $data['per_sq_ft']=$new_prop_s_price/$new_p_bulid;

        $p_unit=$this->input->post('prop_unitt');
		$data['prop_per_unit']=$prop_s_price/$p_unit;
		$data['prop_link']=$this->input->post('prop_linkk');

		$data['t_user_id']=$this->session->userdata('t_user_id');
		$data['loan_id']=$loan_id;
		$data['talimar_loan']=$this->input->post('talimar_no');
		$data['property_home_id']=$property_home_id;
		$id=$this->input->post('p_idd');
	
         $this->User_model->updatedata('comparable_sale',array('property_home_id'=>$property_home_id,'id'=>$id,'talimar_loan'=>$talimar_no),$data);
   
	    $this->session->set_flashdata('success','Property comparable sale updated successfully!');
	    $this->session->set_flashdata('popup_modal_hit', 'comparable_sales');
		$this->session->set_flashdata('property_home_id', $property_home_id);
		redirect(base_url().'load_data/'.$loan_id);


}

	public function edit_com_model(){
		error_reporting(0);
		// FEtch Comparable_sale data
		$id=$this->input->post('id');
		$talimar_loan_number=$this->input->post('talimar_loan_number');

			$fetch_comparable_sale = $this->User_model->select_where_asc('comparable_sale',array('talimar_loan'=>$talimar_loan_number,'id'=>$id),'id');
			if($fetch_comparable_sale->num_rows() > 0)
			{
					
				$fetch_comparable_sale_result= $fetch_comparable_sale->result();
				foreach($fetch_comparable_sale_result as $row){
							
 					$add=explode("-",$row->property_address);

						$data['id'] 										=  $row->id;
						$data['talimar_loan'] 								=  $row->talimar_loan;
						// $data['property_address']							=  $row->property_address;
						
						$data['street']										=  $add[0];
						$data['unit']										=  $row->unit;
						$data['city']										=  str_replace(",", "", $add[1]);
						$data['state']										=  $add[2];
						$data['zip']										=  $add[3];
						$data['date_sold']									=  empty($row->date_sold) ? '':$row->date_sold;
						$data['sq_ft']										=  empty($row->sq_ft) ? '':$row->sq_ft;
						$data['bedrooms']									=  empty($row->bedrooms) ? '':$row->bedrooms;
						$data['bathroom']									=  empty($row->bathroom) ? '':$row->bathroom;
						$data['lot_size']									=  empty($row->lot_size) ? '':$row->lot_size;
						$data['year_built']									=  empty($row->year_built) ? '':$row->year_built;
						$data['sale_price']									=  empty($row->sale_price) ? '':$row->sale_price;
						$data['per_sq_ft']									=  empty($row->per_sq_ft) ? '':number_format($row->per_sq_ft);
						$data['prop_type']									=  empty($row->prop_type) ? '':$row->prop_type;
						$data['prop_unit']									=  empty($row->prop_unit) ? '':$row->prop_unit;
						$data['prop_building']								=  empty($row->prop_building) ? $row->sq_ft:$row->prop_building;
						$data['prop_per_unit']								=  empty($row->prop_per_unit) ? '':number_format($row->prop_per_unit);
						$data['prop_link']									=  empty($row->prop_link) ? '':$row->prop_link;
						$data['property_home_id']							=  $row->property_home_id;
		
				}

			   	echo json_encode($data);

			}

	
	}


	public function form_upload_reserve_images()
	{
		error_reporting(0);
 		$file = $_FILES['file'];
 		
		$id						= $this->input->post('id');

		$loan_id				= str_replace('/','',$this->input->post('loan_id'));
		$talimar_loan			= $this->input->post('talimar_no');
		$folder_id				= $this->input->post('folder_id');


	
		//   echo '<pre>';
		// print_r($file);
		// print_r($_POST);
		// echo '</pre>';

		 // die('stop'); 
		 $folder = 'all_reserve_images/'.$talimar_loan.'/'.$folder_id.'/'; //FCPATH
		
			/*if(!is_dir($folder)){


				mkdir($folder, 0777, true);
				//chmod($folder, 0777);				
			}*/

			
				
		foreach($id as $key => $row)
		{
			// echo $row.'fff<br>';
			if($row =='new' && $file['name'][$key] !='no_image.png')
			{
				$ext 			= strtolower(pathinfo($file['name'][$key], PATHINFO_EXTENSION));
				$check_error 	= $file['error'][$key];
				if(($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') && $check_error == 0 )
				{
					$image_name = date('Ymdhis').'-'.rand(1,9999).'.'.$ext;
					$sql ="INSERT INTO `loan_reserve_images`(`talimar_loan`,`title`, `loan_id`, `image_name`,`t_user_id`) VALUES ('".$talimar_loan."','".$folder_id."','".$loan_id."','".$image_name."','".$this->session->userdata('t_user_id')."')";
					$this->User_model->query($sql);
					$target_dir 	= $folder.$image_name;
					//move_uploaded_file($file['tmp_name'][$key], $target_dir);
					$attachmentName=$this->aws3->uploadFile($target_dir,$file['tmp_name'][$key]);
				}
			}
			else
			{
				$ext 			= strtolower(pathinfo($file['name'][$key], PATHINFO_EXTENSION));
				$check_error 	= $file['error'][$key];
				if(($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') && $check_error == 0 )
				{
					$image_name = date('Ymdhis').'-'.rand(1,9999).'.'.$ext;
					// 
					$fetch = $this->User_model->select_where('loan_reserve_images',array('id'=>$row));
					if($fetch->num_rows() > 0)
					{
						$fetch = $fetch->row();
						$previous_image_name = $fetch->image_name;
						/*if(file_exists($folder.$previous_image_name)){

							unlink($folder.$previous_image_name);
						}*/
						if(getAwsUrl($folder.$previous_image_name)){
							$this->aws3->deleteObject($folder.$previous_image_name);
						}
						
						$this->User_model->updatedata('loan_reserve_images',array('id'=>$row),array('image_name'=>$image_name));
						
						
						
						$target_dir 	= $folder.$image_name;
						//move_uploaded_file($file['tmp_name'][$key], $target_dir);
						$attachmentName=$this->aws3->uploadFile($target_dir,$file['tmp_name'][$key]);
					}
					
					
				}
				
			}
		}

		$this->session->set_flashdata('popup_modal_cv', array('test', 'add_draw_images', 'property-folders-'.$folder_id.''));
		$this->session->set_flashdata('success', ' Reserve Images updated');
		redirect(base_url().'load_data/'.$loan_id.'#add_draw_images');
		
	}
	

	public function form_loan_folder()
	{
		error_reporting(0);
		// print_r($_POST);
		// die();
		$talimar_loan		= $this->input->post('talimar_no');
		$loan_id			= str_replace('/','',$this->input->post('loan_id'));
		
		$id 				= $this->input->post('id');
		$title 				= $this->input->post('title');
	
		foreach($id as $key => $row)
		{
			
			if($row == 'new')
			{
				 $sql = "INSERT INTO `loan_reserve_folder`(`talimar_loan`,`loan_id`,`folder_title`) VALUES ('".$talimar_loan."','".$loan_id."','".$title[$key]."')";
			}
			else
			{
				$sql = "UPDATE `loan_reserve_folder` SET `folder_title` = '".$title[$key]."' WHERE id= '".$row."' ";
			}
			
		if($title[$key])
			{
				$this->User_model->query($sql);
			}
		}
	
		$this->session->set_flashdata('success', 'Loan Reserve Folders Updated');

	
		// redirect(base_url().'load_data/'.$loan_id.'#property-'.$property_home_id);
		//$this->session->set_flashdata('popup_modal_hit', 'property_data');
	
		$this->session->set_flashdata('popup_modal_cv', array('test', 'add_draw_images'));
		redirect(base_url().'load_data/'.$loan_id);
	}

	public function delete_reserve_images()
	{
		$id = $this->input->post('id');

		$fetch_loan_reserve_images = $this->User_model->select_where('loan_reserve_images',array('id'=>$id));
		if($fetch_loan_reserve_images->num_rows() > 0)
		{
			$loan_reserve_images 	= $fetch_loan_reserve_images->row();
			$talimar_loan 			= $loan_reserve_images->talimar_loan;
			$image_name 			= $loan_reserve_images->image_name;
			$folder_id 				= $loan_reserve_images->title;


			$folder = 'all_reserve_images/'.$talimar_loan.'/'.$folder_id.'/'.$image_name; //FCPATH
			
		
		
			$this->User_model->delete('loan_reserve_images',array('id'=>$id));
			if(getAwsUrl($folder)){
				$this->aws3->deleteObject($folder);
			}
			/*if(file_exists($folder))
			{
				unlink($folder);
			}*/
		}
	}

	
}