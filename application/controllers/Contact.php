<?php
class Contact extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('Aws3','aws3');
		$this->load->helper('aws3_helper.php');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->database();
		$this->load->helper(array('form', 'url'));
		$this->load->model('User_model');
		$this->load->library('MYPDF');
		$this->load->library('email');
		$this->load->helper('common_functions.php');

		if($this->session->userdata('t_user_id') != '' )
		{
		}
		else
		{
			redirect(base_url()."home",'refresh');
		}
		
		ini_set('memory_limit', '-1');

	}

	//03-02-2021

	public function send_user_email_verification($name, $email, $lid, $sendstring){
				
        $contentMail = '<p>Hi '.$name.',</p>';
        $contentMail .= '<p>You may create a new Lender Account by following the link below and entering your contact information. Should you have any questions, please reach out to Investor Relations.</p>';


        $Subject = 'New Lender Registration';
        $dataMailA = array();
        $dataMailA['from_email']  	= 'invest@talimarfinancial.com';
        $dataMailA['from_name']   	= 'TaliMar Financial';
        $dataMailA['to']      		= $email;
        $dataMailA['cc']      		= '';
        $dataMailA['bcc']      		= '';
        $dataMailA['name']      	= '';
        $dataMailA['subject']     	= $Subject;
        $dataMailA['content_body']  = $contentMail;
        $dataMailA['button_url']  	= Lender_url.'Home/register/'.$sendstring;
        $dataMailA['button_text']   = 'Create Account';
        $dataMailA['attach_file']   = '';
        send_mail_template($dataMailA);

	    ob_end_clean();
	    return "success";
	}

	public function create_lender_user_mail(){

		$av_account_data   = $this->input->post('av_account_data');
		$dataAv 		   = explode('@@@@', base64_decode($av_account_data));

		$lender_contact_id = $dataAv[0];
		$contact_email     = $dataAv[1];
		$contact_firstname = $dataAv[2];
		$contact_lastname  = $dataAv[3];
		$contact_phone     = $dataAv[4];

		$this->send_user_email_verification($contact_firstname, trim($contact_email), $lender_contact_id, $av_account_data);
		echo "success";
		die();
	}

	//03-02-2021

	public function filter_contacts()
	{
		error_reporting(0);
		$fetch_contact_lender 	= $this->User_model->query("SELECT c.contact_id, c.contact_firstname, c.contact_lastname FROM contact as c JOIN lender_contact as lc ON c.contact_id = lc.contact_id GROUP BY lc.contact_id");
		$fetch_contact_lender 	= $fetch_contact_lender->result();

		foreach($fetch_contact_lender as $row)
		{
			$fetch_lender_option[] = array(
											'id' => $row->contact_id,
											'text' => $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname
											);
		}
		$data['fetch_lender_option'] = $fetch_lender_option;

		//borrower
		$fetch_contact_borrower 	= $this->User_model->query("SELECT c.contact_id, c.contact_firstname, c.contact_lastname FROM contact as c JOIN borrower_contact as bc ON c.contact_id = bc.contact_id GROUP BY bc.contact_id");
		$fetch_contact_borrower 	= $fetch_contact_borrower->result();

		foreach($fetch_contact_borrower as $row)
		{
			$fetch_borrower_option[] = array(
											'id' => $row->contact_id,
											'text' => $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname
											);
		}

		$data['fetch_borrower_option'] = $fetch_borrower_option;

		$data['sdsa'] = '1234';
		$data['content'] = $this->load->view('contact/filter', $data, true);
		$this->load->view('template_files/template', $data);
	}

	/*
		Description : This function use for get contact list user data - and filter option 
					  (Tags + Internal Contact + Lender Id + Borrower Id + Specialty + Marketing Blast).
					   sorting / filter / pagination implement
		Author      : Bitcot
		Created     : 18-03-2021
		Modified    :
	*/
	public function ajax_contactlist(){
		$request= '';
		if(isset($_GET)){
			$request = $_GET;
		}


		$columns = array(
			0 => 't1.contact_lastname',
			1 => 't1.contact_firstname',
			2 => 't1.company_name_1',
			3 => 't1.contact_phone',
			4 => 't1.contact_email',			
		);
		$return_json = array();
		
		$startfrom = $request['start'];
		$queryLength = $request['length'];

		

		/*Short*/
		$orderby = 't1.contact_lastname';
		$order = 'ASC';
		if($request){
			if($request['order'][0]['column']){
				$orderby = $columns[$request['order'][0]['column']];
			}
			if($request['order'][0]['dir']){
				$order = $request['order'][0]['dir'];
			}
		}
		/*Short*/

		/*Search Variable*/
		$searchValue = '';
		if($request){			
			if( $request['search'] ){
				if($request['search']['value']){
					$searchValue = trim( $request['search']['value'] );
				}
			}
		}
		$queryWhere = '';
		$queryWhereArray = array();
		$queryWhereArray[] = " t1.contact_email <> '' ";
		if( $searchValue ){

			$queryWhereArray[] = " (concat(t1.contact_firstname, ' ' , t1.contact_lastname) LIKE '$searchValue%'
  			OR  t1.contact_firstname LIKE '$searchValue%' OR  t1.contact_firstname LIKE '%$searchValue'
  			OR  t1.contact_lastname LIKE '$searchValue%'  OR t1.contact_lastname LIKE '%$searchValue' )  OR t1.contact_email LIKE '%$searchValue%' ";

		}

		$join_sql = '';

		if($request){			
			if( isset($request['internal_contact']) ){
				if($request['internal_contact']){
					$queryWhereArray[] = " t1.internal_contact = '".$request['internal_contact']."' ";
				}
			}
			if( isset($request['company_name']) ){
				if($request['company_name']){
					$queryWhereArray[] = " (t1.company_name_1 LIKE '%".$request['company_name']."%' OR t1.company_name_2 LIKE '%".$request['company_name']."%') ";
				}
			}			
			$arrayContactId = array();
			if( isset($request['lenderwithacc']) ){
				if($request['lenderwithacc']){
					$arrayContactId[] = $request['lenderwithacc'];/* t1.internal_contact = '".$request['lenderwithacc']."' ";*/
				}
			}
			if( isset($request['borrowerwithacc']) ){
				if($request['borrowerwithacc']){
					$arrayContactId[] = $request['borrowerwithacc'];//" t1.internal_contact = '".$request['lenderwithacc']."' ";
				}
			}
			if(count($arrayContactId) > 0){
				$queryWhereArray[] = " t1.contact_id IN ('".implode(',', $arrayContactId)."') ";
			}

			if( isset($request['contact_Marketing']) ){
				if($request['contact_Marketing']){
					// $strWhere="";
					// $contact_Marketing_Array=explode(",", $request['contact_Marketing']);
					// if(count($contact_Marketing_Array)>0){
					// 	foreach ($contact_Marketing_Array as $key => $value) {
					// 		if($strWhere!=""){
					// 			$strWhere.=" AND ";
					// 		}
					// 		$strWhere.=" t1.contact_marketing_data IN ('".$value."') ";
					// 	}
					// }else{
						$queryWhereArray[]=" t1.contact_marketing_data LIKE '%".$request['contact_Marketing']."%' ";
					// }
					// $queryWhereArray[] = $strWhere;
				}
			}



			if( isset($request['contact_specialty']) ){
				if($request['contact_specialty']){
					$join_sql = 'yes';
					$queryWhereArray[] = " t2.contact_specialty IN ('".$request['contact_specialty']."') ";
				}
			}
			if( isset($request['contact_email_blast']) ){
				if($request['contact_email_blast']){
					$join_sql = 'yes';
					$queryWhereArray[] = " t2.contact_email_blast IN ('".$request['contact_email_blast']."') ";
				}
			}
		}

		if(count($queryWhereArray) > 0){
			$queryWhere = ' WHERE '.implode(' AND ', $queryWhereArray);
		}
		
		/*Search Variable*/

		$sql1 = "SELECT * FROM contact AS t1 $queryWhere GROUP BY contact_id ORDER BY $orderby $order LIMIT $startfrom, $queryLength";
		$sql2 = "SELECT * FROM contact AS t1 $queryWhere GROUP BY contact_id ";

		if($join_sql == 'yes'){
			$sql1 = "SELECT t1.* FROM contact t1 INNER JOIN contact_tags t2 ON t1.contact_id = t2.contact_id $queryWhere GROUP BY contact_id  ORDER BY $orderby $order LIMIT $startfrom, $queryLength";

			$sql2 = "SELECT t1.* FROM contact t1 INNER JOIN contact_tags t2 ON t1.contact_id = t2.contact_id $queryWhere GROUP BY contact_id ";
		}
		//echo $sql1;
		//die();

		$contactlist 	= $this->User_model->query($sql1);
		$contactlistrows= $this->User_model->query($sql2);

		$rowRequests = array();
		if($contactlist->num_rows() > 0){
			$list_result 	= $contactlist->result();
			
			foreach ($list_result as $key => $listValue) {
				$contact_date = '';
				$sql = "SELECT `date` FROM `user_added_contact` WHERE contact_id ='".$listValue->contact_id."'";
				$fetch	= $this->User_model->query($sql);
				if($fetch->num_rows() > 0){
					$fetch_result = $fetch->row();
					$contact_date =  $fetch_result->date;
				}
				$modified_date = '';

				$sql_modify = "SELECT MAX(ct.modified_date) as modified_da FROM `contact_tags` as ct JOIN user as u ON ct.user_id = u.id WHERE ct.contact_id ='".$listValue->contact_id."'";
		
				$fetch_date_modify = $this->User_model->query($sql_modify);
				if($fetch_date_modify->num_rows() > 0){
					$fetch_date_modify = $fetch_date_modify->result();
					$modified_date = $fetch_date_modify[0]->modified_da;
				}

				$rowRequests[$key]['last_name'] 		= $listValue->contact_lastname;
				$rowRequests[$key]['first_name'] 		= $listValue->contact_firstname;
				$rowRequests[$key]['company_name'] 		= $listValue->company_name_1;
				$rowRequests[$key]['phone'] 			= $listValue->contact_phone ? $listValue->contact_phone : $listValue->contact_secondary_phone;
				$rowRequests[$key]['email'] 			= $listValue->contact_email;
				$rowRequests[$key]['added'] 			= $contact_date ? date('m-d-Y',strtotime($contact_date)) :'';
				$rowRequests[$key]['modified'] 			= $modified_date ? date('m-d-Y',strtotime($modified_date)) :'';

				$action = '<a href="/Pranav/Talimar/viewcontact/'.$listValue->contact_id.'" class="btn btn-primary bu_Block"><i class="fa fa-eye" aria-hidden="true"></i></a>';
				if ($this->session->userdata('user_role') == 2) {
					$action .= ' <a href="javascript:deleteContactUserInList('.$listValue->contact_id.')" class="btn btn-primary bu_Block"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
				}
				$rowRequests[$key]['action'] 			= $action;
			}
		}
		echo json_encode(array('que'=>$sql1,'data' => $rowRequests, 'total' => $contactlistrows->num_rows()));
		die();
	}

	/*
		Description : View contactlist Data, show all users Leander's/Borrower's 
		Author      : Bitcot
		Created     : 
		Modified    : 18-03-2021
		Modified Description : Unless use code remove and manage code 
	*/
	public function contactlist(){
		error_reporting(0);
	  	ini_set('memory_limit','640M');	  	
	  	ini_set('max_execution_time', '0');

		$contact_originator_setting = $this->contact_originator_setting();
		$val=$this->input->post('search');

		$contact_specialty_arr 		= '';
		$contact_specialty_val 		= '';
		$contact_email_blast_val 	= '';
		$contact_email_blast_arr  	= '';
		$contact_market_arr  	    = '';
		$$contact_market_val 		= '';  	  

		$contact_specialty			= $this->input->post('contact_specialty') ? (array_filter($this->input->post('contact_specialty'))) : '';	
		$contact_market	= $this->input->post('contact_market') ? (array_filter($this->input->post('contact_market'))) : '';	
		$contact_email_blast		= $this->input->post('contact_email_blast') ? array_filter($this->input->post('contact_email_blast')) : '';	
		
		$contact_specialty_vals   	= $this->input->post('contact_specialty_val');
		$contact_market_vals   	= $this->input->post('contact_market_val');
		
		$contact_email_blast_vals   = $this->input->post('contact_email_blast_val');


		$borrowerwithacc   = $this->input->post('borrowerwithacc');
		$lenderwithacc   = $this->input->post('lenderwithacc');
		
		if($this->input->post('contact_specialty')){
			$contact_specialty_val=array();
			foreach($contact_specialty as $key => $cs){
				$contact_specialty_val[] = $contact_specialty_vals[$key];
			}	
		}
	
		if($this->input->post('contact_market')){
			$contact_market_val=array();
			foreach($contact_market as $key => $css){
				$contact_market_val[] = $contact_market_vals[$key];
			}	
		}


		if($this->input->post('contact_email_blast')){
			$contact_email_blast_val=array();
			foreach($contact_email_blast as $key => $ce){
				$contact_email_blast_val[] = $contact_email_blast_vals[$key];
			}	
		}
		
		$data['contact_specialty_array'] = $contact_specialty_val;
		$data['contact_market_array'] = $contact_email_blast_val;


		$specilityimplode = implode("','", $contact_specialty_val);
		$emailblastimplode = implode("','", $contact_email_blast_val);

		if($specilityimplode && $emailblastimplode != ''){

			$conditionfilter = "ct.`contact_specialty` IN ('".$specilityimplode."') AND ct.contact_email_blast IN ('".$emailblastimplode."')";

		}elseif($specilityimplode != ''){
			$conditionfilter = "ct.`contact_specialty` IN ('".$specilityimplode."')";

		}elseif($emailblastimplode != ''){
			$conditionfilter = "ct.contact_email_blast IN ('".$emailblastimplode."')";

		}else{
			$conditionfilter = "";
		}

		$contact_specialty_arr 		= $contact_specialty_val ? array_values(array_filter($contact_specialty_val)) : '';		
		$specialty_optn_arr 	= '';
		$email_blast_optn_arr 	= '';	
		if($this->input->post('contact_specialty_val')){
			$rr = $contact_specialty_val ? array_values(array_filter($contact_specialty_val)) :'';
			if($rr){				
				foreach($rr as $val){
					$specialty_optn_arr[$val] = $val;
				}
			}		
			$data['specialty_optn_arr']  =  $specialty_optn_arr;
		}
		
		$contact_market_arr 		= $contact_market_val ? array_values(array_filter($contact_market_val)) : '';		
		$market_optn_arr 	= '';		
		if($this->input->post('contact_market_val')){
			$rra = $contact_market_val ? array_values(array_filter($contact_market_val)) :'';
			if($rra){				
				foreach($rra as $vall){
					$market_optn_arr[$vall] = $vall;
				}
			}		
			$data['market_optn_arr']  =  $market_optn_arr;
		}
	
		$contact_email_blast_arr 	= $contact_email_blast_val ? array_values(array_filter($contact_email_blast_val)) :'';		
		$data['contact_specialty_arr'] 	 	= $contact_specialty_arr;
		$data['contact_market_arr'] 	 	= $contact_market_arr;
		$data['contact_email_blast_arr'] 	= $contact_email_blast_arr;
		
		$data['user_role'] = $this->session->userdata('user_role');	
		
		$contact_idd = $this->input->post('search_contact') ?  $this->input->post('search_contact')  :  $this->uri->segment(2);
			
		if($this->input->post('search_by')){				
			$option_select = $this->input->post('search_by');			
			if($option_select == 1){				
				$fetch_contact 				= $this->User_model->select_star('contact');
				$fetch_contact 				= $fetch_contact->result();
		
				foreach($fetch_contact as $row){

					$text = $row->contact_firstname;
					if($row->contact_middlename != '')
					{
						$text .= ' '.$row->contact_middlename;
					}

					if($row->contact_lastname != '')
					{
						$text .= ' '.$row->contact_lastname;
					}


					$fetch_search_option[] = array(
										'id' => $row->contact_id,
										'text' => $text
									);
				}
				$data['fetch_search_option'] = $fetch_search_option;
				$data['selected_byy'] = 1;
			}else if($option_select == 2){
				$fetch_option_data = $this->User_model->query("SELECT * from contact ");
				$fetch_option_data = $fetch_option_data->result();
				foreach($fetch_option_data as $row){
					$fetch_search_optionn[] = array(
										'id' => $row->contact_id,
										'text' => $row->contact_email,
										'text2' => $row->contact_email2
									);
				}
				$data['fetch_search_option'] = $fetch_search_optionn;
				$data['selected_byy'] = 2;
			}else if($option_select == 3){
				$fetch_option_data1 = $this->User_model->query("SELECT * from contact group by company_name_1");
				$fetch_option_data1 = $fetch_option_data1->result();
				foreach($fetch_option_data1 as $row){
					$fetch_search_optionnn[] = array(
											'id' =>$row->contact_id,
											'text' => $row->company_name_1,
											'text2' => $row->company_name_2
										);
					}
					$data['fetch_search_option'] = $fetch_search_optionnn;
					$data['selected_byy'] = 3;
				}
			}else{
				$fetch_contact 	= $this->User_model->select_star('contact');
				$fetch_contact 	= $fetch_contact->result();
				
				foreach($fetch_contact as $row){
					$text = $row->contact_firstname;
					if($row->contact_middlename != '')
					{
						$text .= ' '.$row->contact_middlename;
					}

					if($row->contact_lastname != '')
					{
						$text .= ' '.$row->contact_lastname;
					}

					$fetch_search_option[] = array(
									'id' => $row->contact_id,
									'text' => $text
								);
			}

			$fetch_OpCompany = $this->User_model->query("SELECT * from contact group by company_name_1");
			$fetch_OpCompanyData = $fetch_OpCompany->result();
			$company_name_optionnn = array();
			foreach($fetch_OpCompanyData as $row){
				$company_name_optionnn[] = array(
					'id' =>$row->contact_id,
					'text' => $row->company_name_1,
					'text2' => $row->company_name_2
				);
			}
			$data['company_name_list'] = $company_name_optionnn;
			$data['fetch_search_option'] = $fetch_search_option;
			$data['selected_byy'] = 1;
		}
		
		/*Filter Data Set 03-17-2021*/
		error_reporting(0);

		$sql_all_user1 = $this->User_model->query("SELECT * FROM user where account='1'");
		$data['all_users_data'] = $sql_all_user1->result();

		$fetch_contact_lender 	= $this->User_model->query("SELECT c.contact_id, c.contact_firstname, c.contact_lastname FROM contact as c JOIN lender_contact as lc ON c.contact_id = lc.contact_id GROUP BY lc.contact_id");
		$fetch_contact_lender 	= $fetch_contact_lender->result();
		foreach($fetch_contact_lender as $row){
			$fetch_lender_option[] = array(
				'id' => $row->contact_id,
				'text' => $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname
			);
		}
		$data['fetch_lender_option'] = $fetch_lender_option;
		//borrower
		$fetch_contact_borrower 	= $this->User_model->query("SELECT c.contact_id, c.contact_firstname, c.contact_lastname FROM contact as c JOIN borrower_contact as bc ON c.contact_id = bc.contact_id GROUP BY bc.contact_id");
		$fetch_contact_borrower 	= $fetch_contact_borrower->result();

		foreach($fetch_contact_borrower as $row){
			$fetch_borrower_option[] = array(
				'id' => $row->contact_id,
				'text' => $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname
			);
		}
		$data['fetch_borrower_option'] = $fetch_borrower_option;
		$data['sdsa'] = '1234';
		$data['content_filter'] = $this->load->view('contact/ajax_filter', $data, true);
		/*Filter Data Set 03-17-2021*/

		$data['user_id'] = $this->session->userdata('t_user_id');
		$data['content'] = $this->load->view('contact/contactlist',$data,true);
		$this->load->view('template_files/template',$data);
	}

	public function contact_originator_setting(){

		if($this->session->userdata('user_role') == 1){

			$check_setting = $this->User_model->query("SELECT * FROM `user_settings` WHERE `user_id`='".$this->session->userdata('t_user_id')."' AND `loan_organization`='1'");
			if($check_setting->num_rows() > 0){
				$loginuser_contacts = 2;
			}else{
				$loginuser_contacts = 1;
			}

		}else{

			$loginuser_contacts = 1;
		}

		return $loginuser_contacts;
	}

	
	

	public function task_view()
	{
		$fetch_all_contact 	= $this->User_model->select_star('contact','id');
		$fetch_all_contact 	= $fetch_all_contact->result();
		$data['fetch_all_contact'] 	= $fetch_all_contact;

		$t_user_id = $this->session->userdata('t_user_id');
		$current_date = date('Y-m-d');

		$task_type=$this->input->post('selct_task');

		if($task_type){

			$con_1 =" AND ct.contact_task='".$task_type."'";
			$data['task_type']=$task_type;
		}else{

			$con_1='';
			$data['task_type']='';
		}

/*07-07-2021 by @AJ*/
		$task_status=$this->input->post('selct_status');

		// if(isset($task_status) && $task_status != '0'){
		if(isset($task_status) && !empty($task_status)){

			$con_12 =" AND ct.contact_status='".$task_status."'";
			$data['task_status']=$task_status;
			
		}else{

			$con_12='';
			$data['task_status']='';

		}
/*End */

		if($this->uri->segment(3) && $this->uri->segment(4)){
		
		$sql = $this->User_model->query("SELECT * FROM `contact_tasks` as ct JOIN contact as c ON ct.contact_id = c.contact_id WHERE ct.`t_user_id`='".$t_user_id."' AND ct.contact_status = '3' AND ct.contact_date <= '".$current_date."' AND ct.contact_id='".$this->uri->segment(3)."' AND ct.id='".$this->uri->segment(4)."' ORDER BY ct.contact_date ASC");	

		}
		else{

		// $sql = $this->User_model->query("SELECT * FROM `contact_tasks` as ct JOIN contact as c ON ct.contact_id = c.contact_id WHERE ct.`t_user_id`='".$t_user_id."' AND ct.contact_status = '3' AND ct.contact_date <= '".$current_date."' ".$con_1." ORDER BY ct.contact_date ASC");
		// $sql = $this->User_model->query("SELECT * FROM `contact_tasks` as ct JOIN contact as c ON ct.contact_id = c.contact_id WHERE ct.`t_user_id`='".$t_user_id."' ".$con_12." AND ct.contact_date <= '".$current_date."' ".$con_1." ORDER BY ct.contact_date ASC");	
			$sql = $this->User_model->query("SELECT * FROM `contact_tasks` as ct  WHERE ct.`t_user_id`='".$t_user_id."' ".$con_12."".$con_1." ORDER BY ct.due_date DESC");
			
			}

		$check_sql =$sql->result();
		$data['fetch_check_sql'] = $check_sql;
		
		//fetch logged in user contact notes...
		$sql_notes = $this->User_model->query("SELECT * FROM `contact_notes` WHERE `user_id`='".$t_user_id."'");
		$notes_res = $sql_notes->result();
		$data['fetch_notes_val'] = $notes_res;
		$fetch_user_notes = $this->User_model->select_where('user_notes',array('id'=>'1'));
		$fetch_user_notes = $fetch_user_notes->row();
		$data['user_notes'] = $fetch_user_notes->notes;
		
		$data['user_id'] = $this->session->userdata('t_user_id');
		$data['content'] = $this->load->view('tasks/task_view',$data,true);
		$this->load->view('template_files/template',$data);
	}
	public function add_note_by_contact(){
		
		$notes = $this->input->post('notes');
		$this->User_model->updatedata('user_notes',array('id'=>'1'),array('notes'=>$notes));
		$this->session->set_flashdata('success',' Notes Updated!');
		redirect(base_url().'task_view');
	}


	public function download_pdf(){

		error_reporting(0);		
		ini_set('memory_limit','940M');

		$request= '';
		if(isset($_GET)){
			$request= $_GET;
		}

		$columns = array(
			0 => 't1.contact_lastname',
			1 => 't1.contact_firstname',
			2 => 't1.company_name_1',
			3 => 't1.contact_phone',
			4 => 't1.contact_email',			
		);
		$return_json = array();
		

		/*Short*/
		$orderby = 't1.contact_lastname';
		$order = 'ASC';
		if($request){
			if($request['orderby']){
				$orderby = $columns[$request['orderby']];
			}
			if($request['order']){
				$order = $request['order'];
			}
		}		
		/*Short*/

		/*Search Variable*/
		$searchValue = '';
		if($request){
			if( $request['search_ext'] ){
				$searchValue = trim( $request['search_ext'] );				
			}
		}

		$queryWhere = '';
		$queryWhereArray = array();
		$queryWhereArray[] = " t1.contact_email <> '' ";
		if( $searchValue ){

			$queryWhereArray[] = " (concat(t1.contact_firstname , ' ' , t1.contact_lastname) LIKE '$searchValue%'
  			OR  t1.contact_firstname LIKE '$searchValue%' OR  t1.contact_firstname LIKE '%$searchValue'
  			OR  t1.contact_lastname LIKE '$searchValue%'  OR t1.contact_lastname LIKE '%$searchValue' )  OR t1.contact_email LIKE '%$searchValue%' ";

		}

		$join_sql = '';

		if($request){			
			if( isset($request['internal_contact']) ){
				if($request['internal_contact']){
					$queryWhereArray[] = " t1.internal_contact = '".$request['internal_contact']."' ";
				}
			}
			if( isset($request['company_name']) ){
				if($request['company_name']){
					$queryWhereArray[] = " (t1.company_name_1 LIKE '%".$request['company_name']."%' OR t1.company_name_2 LIKE '%".$request['company_name']."%') ";
				}
			}			
			$arrayContactId = array();
			if( isset($request['lenderwithacc']) ){
				if($request['lenderwithacc']){
					$arrayContactId[] = $request['lenderwithacc'];/* t1.internal_contact = '".$request['lenderwithacc']."' ";*/
				}
			}
			if( isset($request['borrowerwithacc']) ){
				if($request['borrowerwithacc']){
					$arrayContactId[] = $request['borrowerwithacc'];//" t1.internal_contact = '".$request['lenderwithacc']."' ";
				}
			}
			if(count($arrayContactId) > 0){
				$queryWhereArray[] = " t1.contact_id IN ('".implode(',', $arrayContactId)."') ";
			}

			if( isset($request['contact_specialty']) ){
				if($request['contact_specialty']){
					$join_sql = 'yes';
					$queryWhereArray[] = " t2.contact_specialty IN ('".$request['contact_specialty']."') ";
				}
			}
			if( isset($request['contact_email_blast']) ){
				if($request['contact_email_blast']){
					$join_sql = 'yes';
					$queryWhereArray[] = " t2.contact_email_blast IN ('".$request['contact_email_blast']."') ";
				}
			}
		}

		if(count($queryWhereArray) > 0){
			$queryWhere = ' WHERE '.implode(' AND ', $queryWhereArray);
		}
		
		/*Search Variable*/

		$sql1 = "SELECT * FROM contact AS t1 $queryWhere GROUP BY contact_id ORDER BY $orderby $order";
		$sql2 = "SELECT * FROM contact AS t1 $queryWhere GROUP BY contact_id ";

		if($join_sql == 'yes'){
			$sql1 = "SELECT t1.* FROM contact t1 INNER JOIN contact_tags t2 ON t1.contact_id = t2.contact_id $queryWhere GROUP BY contact_id  ORDER BY $orderby $order";

			$sql2 = "SELECT t1.* FROM contact t1 INNER JOIN contact_tags t2 ON t1.contact_id = t2.contact_id $queryWhere GROUP BY contact_id ";
		}


		$contactlist 	= $this->User_model->query($sql1);
		$contactlistrows= $this->User_model->query($sql2);

		//echo $searchValue;
		//die();

		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);

			// set document information
			$pdf->SetCreator(PDF_CREATOR);
			// $pdf->SetAuthor('Nicola Asuni');
			// $pdf->SetTitle('TCPDF Example 004');
			// $pdf->SetSubject('TCPDF Tutorial');
			// $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

			// set default header data
			 // $pdf->SetHeaderData("include/icons/logo-light.png", 30, '', '');

			// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			// set margins
			$pdf->SetMargins(2, 20, 2);
			// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
			$pdf->SetFont('times', '', 7);

			// add a page
			$pdf->AddPage('L', 'A4');
			// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
			$header ='<h1 style="color:#003468;font-size:18px;"> Contact List</h1>';
			$header .= '<style>
			.table {
				width: 100%;
				max-width: 100%;
				margin-bottom: 20px;
				border-collapse:collapse;
			}
			.table-bordered {
			border: 1px solid #ddd;
			}
			table {
				border-spacing: 0;
				border-collapse: collapse;
			}
			.table td{
				height : 25px;
				font-size:12px;
			}
			tr.table_header th{
				height:25px;
				font-size:12px;
				font-family: "Times New Roman", Georgia, Serif;
				text-align:center;
				background-color:#bfcfe3;
			}
			
			tr.table_bottom th{
				height:20px;
				font-size:12px;
				font-family: "Times New Roman", Georgia, Serif;
				text-align:left;
				background-color:#bfcfe3;
			}
			tr.odd td{
				background-color:#ededed;
			}
			</style>
			';
			
			$header .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';
			$header .= '<tr class="table_header">';
			$header .= '<th style="width:6%;text-decoration:underline;"><strong>Last Name</strong></th>';
			$header .= '<th style="width:6%;text-decoration:underline;"><strong>First Name</strong></th>';
			

			$header .= '<th style="width:7%;text-decoration:underline;"><strong>Phone 1</strong></th>';
			$header .= '<th style="width:7%;text-decoration:underline;"><strong>Phone 2 </strong></th>';
			$header .= '<th style="width:15%;text-decoration:underline;"><strong>E-mail Address</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong>E-Mail 2</strong></th>';
			$header .= '<th style="width:7%;text-decoration:underline;"><strong>Company<br>Name</strong></th>';
			$header .= '<th style="width:7%;text-decoration:underline;"><strong>Mailing Street<br> Address</strong></th>';
			$header .= '<th style="width:6%;text-decoration:underline;"><strong>Mailing<br>Unit#</strong></th>';
			$header .= '<th style="width:6%;text-decoration:underline;"><strong>Mailing<br>City</strong></th>';
			$header .= '<th style="width:6%;text-decoration:underline;"><strong>Mailing<br>State</strong></th>';
			$header .= '<th style="width:5%;text-decoration:underline;"><strong>Mailing<br>Zip</strong></th>';
			$header .= '<th style="width:8%;text-decoration:underline;"><strong>Internal Contact</strong></th>';
			$header .= '<th style="width:6%;text-decoration:underline;"><strong>Contact #</strong></th>';
			
			$header .= '</tr>';
			
			
				$number = 0;
	

		$rowRequests = array();
		if($contactlist->num_rows() > 0){
			$list_result 	= $contactlist->result();
			
			foreach ($list_result as $key => $row) {

				$rand = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTVWXYZ0123456789"), 1, 7);

				$fetch_iuser_typedd = $this->User_model->query("Select fname,middle_name,lname from user where id='".$row->internal_contact ."'");
				if($fetch_iuser_typedd->num_rows() > 0)
				{
					$fetch_iuser_typedd = $fetch_iuser_typedd->result();
					$fetch_internalss = $fetch_iuser_typedd[0]->fname.' '.$fetch_iuser_typedd[0]->middle_name.' '.$fetch_iuser_typedd[0]->lname;
		        }	
						
				if ($number % 2 == 0) {
					$class_add = "even";
					}
					else if($number == 0)
					{
						 $class_add = "even";
					}
					else
					{
						$class_add = "odd";
					}
					
				$header .= '<tr class="'.$class_add.'">';
				$header .= '<td>'.$row->contact_lastname.'</td>';
				$header .= '<td>'.$row->contact_firstname.'</td>';
			
				$header .= '<td>'.$row->contact_phone.'</td>';
				$header .= '<td>'.$row->contact_secondary_phone.'</td>';
				$header .= '<td>'.$row->contact_email.'</td>';
				$header .= '<td>'.$row->contact_email2.'</td>';
				$header .= '<td>'.$row->company_name_1.'</td>';
				$header .= '<td>'.$row->contact_mailing_address.'</td>';
				$header .= '<td>'.$row->contact_mailing_unit.'</td>';
				$header .= '<td>'.$row->contact_mailing_city.'</td>';
				$header .= '<td>'.$row->contact_mailing_state.'</td>';
				$header .= '<td>'.$row->contact_mailing_zip.'</td>';
				$header .= '<td>'.$fetch_internalss.'</td>';
				$header .= '<td>'.$rand.'</td>';
		
				$header .= '</tr>';
				
				$number++;
			}
		}
		
		
		$header .= '</table>';
		
		$pdf->WriteHTML($header);
		
		$pdf->Output("contact_download_list.pdf","D");
		
		exit();
	}
	
	public function download_contactlist(){
		
		error_reporting(0);		
		ini_set('memory_limit','940M');

		$request= '';
		if(isset($_GET)){
			$request= $_GET;
		}

		$columns = array(
			0 => 't1.contact_lastname',
			1 => 't1.contact_firstname',
			2 => 't1.company_name_1',
			3 => 't1.contact_phone',
			4 => 't1.contact_email',			
		);
		$return_json = array();
		

		/*Short*/
		$orderby = 't1.contact_lastname';
		$order = 'ASC';
		if($request){
			if($request['orderby']){
				$orderby = $columns[$request['orderby']];
			}
			if($request['order']){
				$order = $request['order'];
			}
		}		
		/*Short*/

		/*Search Variable*/
		$searchValue = '';
		if($request){
			if( $request['search_ext'] ){
				$searchValue = trim( $request['search_ext'] );				
			}
		}

		$queryWhere = '';
		$queryWhereArray = array();
		$queryWhereArray[] = " t1.contact_email <> '' ";
		if( $searchValue ){

			$queryWhereArray[] = " (concat(t1.contact_firstname , ' ' , t1.contact_lastname) LIKE '$searchValue%'
  			OR  t1.contact_firstname LIKE '$searchValue%' OR  t1.contact_firstname LIKE '%$searchValue'
  			OR  t1.contact_lastname LIKE '$searchValue%'  OR t1.contact_lastname LIKE '%$searchValue' )  OR t1.contact_email LIKE '%$searchValue%' ";

		}

		$join_sql = '';

		if($request){			
			if( isset($request['internal_contact']) ){
				if($request['internal_contact']){
					$queryWhereArray[] = " t1.internal_contact = '".$request['internal_contact']."' ";
				}
			}
			if( isset($request['company_name']) ){
				if($request['company_name']){
					$queryWhereArray[] = " (t1.company_name_1 LIKE '%".$request['company_name']."%' OR t1.company_name_2 LIKE '%".$request['company_name']."%') ";
				}
			}			
			$arrayContactId = array();
			if( isset($request['lenderwithacc']) ){
				if($request['lenderwithacc']){
					$arrayContactId[] = $request['lenderwithacc'];/* t1.internal_contact = '".$request['lenderwithacc']."' ";*/
				}
			}
			if( isset($request['borrowerwithacc']) ){
				if($request['borrowerwithacc']){
					$arrayContactId[] = $request['borrowerwithacc'];//" t1.internal_contact = '".$request['lenderwithacc']."' ";
				}
			}
			if(count($arrayContactId) > 0){
				$queryWhereArray[] = " t1.contact_id IN ('".implode(',', $arrayContactId)."') ";
			}

			if( isset($request['contact_specialty']) ){
				if($request['contact_specialty']){
					$join_sql = 'yes';
					$queryWhereArray[] = " t2.contact_specialty IN ('".$request['contact_specialty']."') ";
				}
			}
			if( isset($request['contact_email_blast']) ){
				if($request['contact_email_blast']){
					$join_sql = 'yes';
					$queryWhereArray[] = " t2.contact_email_blast IN ('".$request['contact_email_blast']."') ";
				}
			}
		}

		if(count($queryWhereArray) > 0){
			$queryWhere = ' WHERE '.implode(' AND ', $queryWhereArray);
		}
		
		/*Search Variable*/

		$sql1 = "SELECT * FROM contact AS t1 $queryWhere GROUP BY contact_id ORDER BY $orderby $order";
		$sql2 = "SELECT * FROM contact AS t1 $queryWhere GROUP BY contact_id ";

		if($join_sql == 'yes'){
			$sql1 = "SELECT t1.* FROM contact t1 INNER JOIN contact_tags t2 ON t1.contact_id = t2.contact_id $queryWhere GROUP BY contact_id  ORDER BY $orderby $order";

			$sql2 = "SELECT t1.* FROM contact t1 INNER JOIN contact_tags t2 ON t1.contact_id = t2.contact_id $queryWhere GROUP BY contact_id ";
		}


		$contactlist 	= $this->User_model->query($sql1);
		$contactlistrows= $this->User_model->query($sql2);

		//echo $searchValue;
		//die();

		$f = fopen("php://output", "w");  
		
		fputcsv($f, array('Last Name','First Name','Phone 1','Phone 2','E-mail Address','E-Mail 2','Company Name','Mailing Street Address','Mailing Unit#','Mailing City','Mailing State','Mailing Zip','Internal Contact','Contact #'));  // title...
	

		$rowRequests = array();
		if($contactlist->num_rows() > 0){
			$list_result 	= $contactlist->result();
			
			foreach ($list_result as $key => $row) {

				$rand = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTVWXYZ0123456789"), 1, 7);
				$fetch_iuser_typedd = $this->User_model->query("Select fname,middle_name,lname from user where id='".$row->internal_contact ."'");
				if($fetch_iuser_typedd->num_rows() > 0)
				{
					$fetch_iuser_typedd = $fetch_iuser_typedd->result();
					$fetch_internals = $fetch_iuser_typedd[0]->fname.' '.$fetch_iuser_typedd[0]->middle_name.' '.$fetch_iuser_typedd[0]->lname;
		        }


					$output['last_name'] 		= $row->contact_lastname;
					$output['first_name'] 		= $row->contact_firstname;
					
					$output['phone'] 			= $row->contact_phone;
					$output['secondary_phone'] 	= $row->contact_secondary_phone;
					$output['email'] 			= $row->contact_email;
					$output['email2'] 			= $row->contact_email2;
					$output['company_name'] 	= $row->company_name_1;
					$output['mailing_address'] 	= $row->contact_mailing_address;
					$output['mailing_unit'] 	= $row->contact_mailing_unit;
					$output['mailing_city'] 	= $row->contact_mailing_city;
					$output['mailing_state'] 	= $row->contact_mailing_state;
					$output['mailing_zip'] 		= $row->contact_mailing_zip;
					$output['internal_contact'] = $fetch_internals;
					$output['rand'] 			= $rand;

					fputcsv($f, $output);
				/*************/
			}
		}
		
		
		fclose($f);
		//set headers to download file rather than displayed
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="ContactLists.csv";');
		
		exit();
		
	}

	public function listAllProperty(){

		$fetch_property = $this->User_model->query("SELECT lp.property_address, lp.loan_id from loan_property as lp JOIN property_home as ph ON lp.talimar_loan = ph.talimar_loan AND lp.property_home_id = ph.id WHERE ph.primary_property = '1'");
		$fetch_property = $fetch_property->result();
		foreach ($fetch_property as $value) {
			
			$allproperty[$value->loan_id] = $value->property_address;
		}
		return $allproperty;
	}


	public function contactData($contact_id_for_affilated){

		error_reporting(0);
		$data['user_role'] = $this->session->userdata('user_role');
		$fetch_affi_contact = $this->User_model->query("SELECT COUNT(*) as total_affi FROM `loan_affiliated_parties` WHERE `name`= '".$contact_id_for_affilated."'");
		$fetch_affi_contact = $fetch_affi_contact->result();
		$data['affi_total_count'] = $fetch_affi_contact[0]->total_affi;

		$data['listAllProperty'] = $this->listAllProperty();
		
		
		$fetch_option_data = $this->User_model->select_star('loan');
		$fetch_option_data = $fetch_option_data->result();
		foreach($fetch_option_data as $row)
		{
			$fetch_loans[] = array(
									'id' => $row->id,
									'text' => $row->talimar_loan
								);
		}
		$data['fetch_loans'] = $fetch_loans;
		 
		$fetch_property = $this->User_model->select_star('loan_property');
		$fetch_property = $fetch_property->result();
		foreach($fetch_property as $row){
			
			$property[] = array(
								'id' => $row->loan_id,	
								'text' => $row->property_address,	
								
								);
		}
		$data['property'] = $property;
		
		
		$t_user_id = $this->session->userdata('t_user_id');
		$fetch_contact 	= $this->User_model->query("SELECT contact_id, contact_firstname, contact_lastname FROM contact WHERE contact_id <> '$t_user_id'"); // LIMIT 300
		$fetch_contact 				= $fetch_contact->result();
		$data['fetch_all_contact'] 	= $fetch_contact;
		
		$contact_id = $contact_idd = $this->input->post('search_contact') ?  $this->input->post('search_contact')  :  $contact_id_for_affilated;

		if($contact_idd)
		{
			
			$contact_id['contact_id'] 	= $contact_idd;
			
			$lender_contact_id          = $contact_idd;
			$come_contact_id          	= $contact_idd;

			/* Fetch lender name */
			$sql_join = 'SELECT lender_contact.lender_id , investor.name FROM lender_contact JOIN investor ON lender_contact.lender_id = investor.id WHERE lender_contact.contact_id = "'.$come_contact_id.'" ';
			$fetch_lenders_name = $this->User_model->query($sql_join);
			
			if($fetch_lenders_name->num_rows() > 0)
			{
				$fetch_lenders_name = $fetch_lenders_name->result();
				foreach($fetch_lenders_name as $rows)
				{
					$con_id_lender_name[] = $rows->name;
				}
				$data['con_id_lender_name'] = $con_id_lender_name;
			}
			

			/*lastlogin*/
			$lastlogindetails = $this->User_model->query("SELECT * FROM `portal_login_details` WHERE `p_user_id`= '".$contact_idd."' AND status = 'Logged In' AND timeout = '' ORDER BY id DESC");
			if($lastlogindetails->num_rows() > 0){
				$lastlogindetails = $lastlogindetails->result();
				$data['lastlogindetails'] = $lastlogindetails;
			}else{
				$data['lastlogindetails'] = '';
			}
			
			// die();
			/*fetch Notes from "contact_notes" table...*/
			$sql_notes = $this->User_model->query("SELECT * FROM contact_notes WHERE contact_id = '".$contact_idd."' ORDER by id DESC");
			
			$fetch_contact_notes = $sql_notes->result();
				
			$data['fetch_contact_notes'] = $fetch_contact_notes;
			foreach($fetch_contact_notes as $key => $notes){
					
				
				
				$sql_loan_id = $this->User_model->query("SELECT * FROM loan_property WHERE loan_id = '".$notes->loan_id."' ");
			
				$fetch_talimar_loan = $sql_loan_id->row();
					
				$data['property_address'][$notes->loan_id] = $fetch_talimar_loan->property_address;
			}
			
			/*fetch Tags from "contact_tags" table...*/
			$sql_tags = $this->User_model->query("SELECT * FROM contact_tags WHERE contact_id = '".$contact_idd."' ");
			
			$fetch_sql_tags = $sql_tags->result();
				
			foreach($fetch_sql_tags as $tags){
				
				$fetch_contact_specialty[] 		= $tags->contact_specialty;
				$fetch_contact_email_blast[] 	= $tags->contact_email_blast;
				if($tags->contact_specialty){
					
					$fetch_contact_specialty_tag_id[$tags->contact_specialty] 		= $tags->id;
				}
				if($tags->contact_email_blast){
					
					$fetch_contact_email_blast_tag_id[$tags->contact_email_blast]		= $tags->id;
				}
			}	
			$data['fetch_contact_specialty'] 			= $fetch_contact_specialty;
			$data['fetch_contact_email_blast'] 			= $fetch_contact_email_blast;
			$data['fetch_contact_specialty_tag_id']   	= $fetch_contact_specialty_tag_id;
			$data['fetch_contact_email_blast_tag_id'] 	= $fetch_contact_email_blast_tag_id;
			$data['fetch_sql_tags'] 			= $fetch_sql_tags;

			$sql_modify = "SELECT *,MAX(ct.modified_date) as modified_da FROM `contact_tags` as ct JOIN user as u ON ct.user_id = u.id WHERE ct.contact_id ='".$contact_idd."' group by ct.user_id order by ct.modified_date DESC";
			
			$fetch_date_modify = $this->User_model->query($sql_modify);
			$fetch_date_modify = $fetch_date_modify->result();

			foreach($fetch_date_modify as $row){

				$user_id = $row->user_id;

				$where['id'] = $user_id;
				$select_user_name = $this->User_model->select_where('user',$where);
				$select_user_name = $select_user_name->result();

				$full_name = $select_user_name[0]->fname.' '.$select_user_name[0]->middle_name.' '.$select_user_name[0]->lname;
	

				$all_info_data[] = array(
											"email" => $full_name,
											"modified_date" => $row->modified_da,

											);
			}

			$data['data_fetch_tags']= $all_info_data;
			

			/*fetch contatct marketing;*/
			$contatctmarketing = $this->User_model->query("SELECT * FROM `contact_marketing` WHERE `contact_id`='".$contact_idd."'");
			if($contatctmarketing->num_rows() > 0){

				$data['contatctmarketing'] = $contatctmarketing->result();
			}else{
				$data['contatctmarketing'] = '';
			}

			/*fetch hard money program;*/
			$contathard_money_program = $this->User_model->query("SELECT * FROM `hard_money_program` WHERE `contact_id`='".$contact_idd."'");
			if($contathard_money_program->num_rows() > 0){
				$data['contathard_money_program'] = $contathard_money_program->result();
			}else{
				$data['contathard_money_program'] = '';
			}
			
          
			
			
         
			/*fetch permissions from "contact_assigned" table...*/
			$sql_assigned 			= $this->User_model->query("SELECT * FROM contact_assigned WHERE contact_id = '".$contact_idd."' ");
			
			$fetch_sql_assigned 	= $sql_assigned->result();
				
			$data['fetch_sql_assigned'] = $fetch_sql_assigned;
			
			if($fetch_sql_assigned[0]->particular_user_id){
				
				/*fetch user name...*/
				$sql_user_name = $this->User_model->query("SELECT * FROM user WHERE id = '".$fetch_sql_assigned[0]->particular_user_id ."' ");
				
				$fetch_sql_user_name = $sql_user_name->row();
					
				$data['fetch_sql_user_name'] = $fetch_sql_user_name->fname .' '.$fetch_sql_user_name->lname;
			
			}
			
			/*fetch user role with 1 i.e. Employees from "user" table...*/
			$sql_user = $this->User_model->query("SELECT * FROM user WHERE role = '1' ");
			
			$fetch_sql_user = $sql_user->result();
				
			$data['fetch_sql_user'] = $fetch_sql_user;
			
			/*fetch contact tasks data...*/
			$contact_task_sql	 = $this->User_model->query("SELECT * FROM contact_tasks WHERE contact_id = '".$contact_idd."'");
			$contact_task		 = $contact_task_sql->result();
			$data['fetch_contact_task'] = $contact_task; 
			
			
			/*select (Borrower) contact data for contact stats...*/
			$select_contact = "select * from contact as c JOIN borrower_contact as bc ON c.contact_id = bc.contact_id where bc.contact_id = '".$contact_idd."'";
			$fetch_all_contact_type = $this->User_model->query($select_contact);
			$fetch_all_contact_type = $fetch_all_contact_type->result();
			foreach($fetch_all_contact_type as $row){
				
				$borrower_id = $row->borrower_id;
				
				$select_pipeline = $this->User_model->query("select *,COUNT(*) as total_count, SUM(loan_amount) as total_amount from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where l.borrower = '".$borrower_id."' AND ls.loan_status = '1'");
				$select_pipeline = $select_pipeline->result();
				$data['pipeline_count']  = $select_pipeline[0]->total_count;
				$data['pipeline_amount'] = $select_pipeline[0]->total_amount;
				
				$select_active = $this->User_model->query("select *,COUNT(*) as total_count, SUM(loan_amount) as total_amount from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where l.borrower = '".$borrower_id."' AND ls.loan_status = '2'");
				$select_active = $select_active->result();
				$data['active_count']  = $select_active[0]->total_count;
				$data['active_amount'] = $select_active[0]->total_amount;
				
				$select_paidoff = $this->User_model->query("select *,COUNT(*) as total_count, SUM(loan_amount) as total_amount from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where l.borrower = '".$borrower_id."' AND ls.loan_status = '3'");
				$select_paidoff = $select_paidoff->result();
				$data['paidoff_count']  = $select_paidoff[0]->total_count;
				$data['paidoff_amount'] = $select_paidoff[0]->total_amount;
				
				
			}


			/*select lender info for contact stats...*/
			$select_lender = $this->User_model->query("Select * from contact as c JOIN lender_contact as lc ON c.contact_id = lc.contact_id where lc.contact_id = '".$contact_idd."'");
			$select_lender = $select_lender->result();
			foreach($select_lender as $row){
				
				$investor_id = $row->lender_id;
				
				$active_lender_sql = "SELECT COUNT(*) as count_loan, SUM(loan_assigment.investment) as total_investment , SUM(loan.loan_amount) as total_loan_amount FROM loan_assigment JOIN loan ON loan_assigment.talimar_loan = loan.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_assigment.lender_name = '".$investor_id."' AND loan_servicing.loan_status = '2' ";
			
				$fetch_active_loan = $this->User_model->query($active_lender_sql);
				if($fetch_active_loan->num_rows() > 0)
				{
					$fetch_active_loan = $fetch_active_loan->result();
					$active_loan[] = array(
													'count_loan' => $fetch_active_loan[0]->count_loan,
													'total_investment' => $fetch_active_loan[0]->total_investment,
													'total_loan_amount' => $fetch_active_loan[0]->total_loan_amount,
														);
				}
				
				$paidoff_lender_sql = "SELECT COUNT(*) as count_loan, SUM(loan_assigment.investment) as total_investment , SUM(loan.loan_amount) as total_loan_amount FROM loan_assigment JOIN loan ON loan_assigment.talimar_loan = loan.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_assigment.lender_name = '".$investor_id."' AND loan_servicing.loan_status = '3' ";
				
				$fetch_paidoff_loan = $this->User_model->query($paidoff_lender_sql);
				if($fetch_paidoff_loan->num_rows() > 0)
				{
					$fetch_paidoff_loan = $fetch_paidoff_loan->result();
					$paidoff_loan[] = array(
													'count_loan' => $fetch_paidoff_loan[0]->count_loan,
													'total_investment' => $fetch_paidoff_loan[0]->total_investment,
													'total_loan_amount' => $fetch_paidoff_loan[0]->total_loan_amount,
														);
				}
			}
			$data['fetch_active_loan'] = $active_loan;
			$data['fetch_paidoff_loan'] = $paidoff_loan;
			
			
			/*fetch lender account details...*/
			$select_lenderaccdetails = $this->User_model->query("Select lc.lender_id from contact as c JOIN lender_contact as lc ON c.contact_id = lc.contact_id where lc.contact_id = '".$contact_idd."'");
			if($select_lenderaccdetails->num_rows() > 0){
				$select_lenderaccdetails = $select_lenderaccdetails->result();
				foreach($select_lenderaccdetails as $row){

					$allinvestor_id = $row->lender_id;
					if($allinvestor_id != '0'){

						/*for $ of Active TD’s...*/
						$active_lenderacc_sql = "SELECT COUNT(*) as count_loan, SUM(loan_assigment.investment) as total_investment , SUM(loan.loan_amount) as total_loan_amount FROM loan_assigment JOIN loan ON loan_assigment.talimar_loan = loan.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_assigment.lender_name = '".$allinvestor_id."' AND loan_servicing.loan_status = '2' ";

						$active_lenderacc_sql = $this->User_model->query($active_lenderacc_sql);
						if($active_lenderacc_sql->num_rows() > 0)
						{
							$active_lenderacc_sql = $active_lenderacc_sql->result();
							$activelendercount = $active_lenderacc_sql[0]->count_loan;
							$activelendersum = $active_lenderacc_sql[0]->total_investment;

						}else{

							$activelendercount = 0;
							$activelendersum = 0;
						}


						/*for $ of Total TD’s...*/
						$allacc_lender_sql = "SELECT COUNT(*) as count_loan, SUM(loan_assigment.investment) as total_investment , SUM(loan.loan_amount) as total_loan_amount FROM loan_assigment JOIN loan ON loan_assigment.talimar_loan = loan.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_assigment.lender_name = '".$allinvestor_id."' AND loan_servicing.loan_status IN('1','2','3')";
						
						$allacc_lender_sql = $this->User_model->query($allacc_lender_sql);
						if($allacc_lender_sql->num_rows() > 0)
						{
							$allacc_lender_sql = $allacc_lender_sql->result();
							$alllendercount = $allacc_lender_sql[0]->count_loan;
							$alllendersum = $allacc_lender_sql[0]->total_investment;

						}else{

							$alllendercount = 0;
							$alllendersum = 0;
						}




						$alllender_accountdetails[] = array(

															'allinvestor_id' => $allinvestor_id,
															'activelendercount' => $activelendercount,
															'activelendersum' => $activelendersum,
															'alllendercount' => $alllendercount,
															'alllendersum' => $alllendersum,
														);
					}
				}
			}else{
				$alllender_accountdetails = '';
			}
			array_multisort(array_column($alllender_accountdetails, 'alllendersum'), SORT_DESC, $alllender_accountdetails);
			$data['alllender_accountdetails'] = $alllender_accountdetails;
			$data['allinvestorsnamelist'] 		= $this->allinvestorsnamelist();


			

			/*Active truset deed...*/
			$activeTrustDeedArray=array();
			if(isset($select_lenderaccdetails) && is_array($select_lenderaccdetails)){
				foreach($select_lenderaccdetails as $row){

					$allinvestors_id = $row->lender_id;
					if($allinvestors_id != '0'){

						$active_lenderacc_sqls = "SELECT loan.id as loan_id,loan.talimar_loan, loan.loan_amount, loan_assigment.lender_name, loan_assigment.investment FROM loan_assigment JOIN loan ON loan_assigment.talimar_loan = loan.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_assigment.lender_name = '".$allinvestors_id."' AND loan_servicing.loan_status = '2' ";

						$active_lenderacc_sqls = $this->User_model->query($active_lenderacc_sqls);
						if($active_lenderacc_sqls->num_rows() > 0)
						{
							$active_lenderacc_sqls = $active_lenderacc_sqls->result();
							foreach($active_lenderacc_sqls as $value) {
								
								$selectaddress = $this->User_model->query("SELECT property_address,unit,city,state,zip FROM loan_property WHERE talimar_loan = '".$value->talimar_loan."'");
								$selectaddress = $selectaddress->result();
								$fulladdress = $selectaddress[0]->property_address.''.$selectaddress[0]->unit.'; '.$selectaddress[0]->city.', '.$selectaddress[0]->state.' '.$selectaddress[0]->zip;

								$activeTrustDeedArray[] = array(
																	"loan_id" => $value->loan_id,
																	"fulladdress" => $fulladdress,
																	"loan_amount" => $value->loan_amount,
																	"investment" => $value->investment,
																	"lender_name" => $value->lender_name,
																	"lender_id" => $allinvestors_id,
																	"property_address" => $selectaddress[0]->property_address,
																	"city" => $selectaddress[0]->city,
																	"state" => $selectaddress[0]->state,
																	"zip" => $selectaddress[0]->zip,


																);

							}

						}

					}
					
						
				}
				$data['activeTrustDeedArray'] = $activeTrustDeedArray;

			}

			

			/*fetch all borrower contact details...*/
			$fetch_borrower_accounts = $this->User_model->query("SELECT bc.borrower_id From borrower_contact as bc JOIN contact as c ON bc.contact_id = c.contact_id WHERE c.contact_id = '".$contact_idd."'");
			if($fetch_borrower_accounts->num_rows() > 0){

				$fetch_borrower_accounts = $fetch_borrower_accounts->result();
				foreach ($fetch_borrower_accounts as $value) {
					
					$borrower_id = $value->borrower_id;
					
					$select_bname = $this->User_model->query("SELECT b_name,PreApproved FROM borrower_data WHERE id = '".$borrower_id."'");
					$select_bname = $select_bname->result();
					$borrowername = $select_bname[0]->b_name;
					$PreApproved = $select_bname[0]->PreApproved;
					$fetchactive_data = $this->User_model->query("SELECT COUNT(*) as totalCount, SUM(loan_amount) as totalAmount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan.borrower = '".$borrower_id."' AND loan_servicing.loan_status = '2'");
					$fetchactive_data = $fetchactive_data->result();
					$totalCount 	= $fetchactive_data[0]->totalCount;
					$totalAmount 	= $fetchactive_data[0]->totalAmount;


					


					$borroweraccountdetails[] = array(
															"borrower_id" => $borrower_id,
															"borrowername" => $borrowername,
															"PreApproved" => $PreApproved,
															"totalCount" => $totalCount ? $totalCount : 0,
															"totalAmount" => $totalAmount ? $totalAmount : 0,
													);
				}
			}else{
				$borroweraccountdetails = '';
			}

			$data['fetch_borrower_accounts'] = $borroweraccountdetails;
			
			
			/*fetch user_added_contact...*/
			
			$check_user = $this->User_model->query("SELECT * FROM `user_added_contact` as uac JOIN user as u ON uac.`t_user_id` = u.id WHERE uac.`contact_id`='".$contact_idd."' order by uac.date DESC");
			$check_user_val		 = $check_user->result();
			$data['fetch_check_user'] = $check_user_val;
			
			//fetch contact total balance for lender data tab in contact page....
		
			$active_balance = $this->User_model->query("SELECT * FROM loan_assigment as la JOIN lender_contact as lc on lc.lender_id = la.lender_name JOIN loan_servicing as ls on la.talimar_loan = ls.talimar_loan WHERE lc.contact_id = '".$contact_idd."'");
			
			$active_balance = $active_balance->result();
			$total_balance = 0;
			foreach($active_balance as $row){
				
				if($row->loan_status == '2'){
					$total_balance += $row->investment;
				}
			}
			
			$data['total_active_balance'] = $total_balance;
			
			
			/*Fetch Borrower name... */
			$sql_join = 'SELECT borrower_contact.borrower_id , borrower_data.b_name FROM borrower_contact JOIN borrower_data ON borrower_contact.borrower_id = borrower_data.id WHERE borrower_contact.contact_id = "'.$come_contact_id.'" ';
			$fetch_borrower_name = $this->User_model->query($sql_join);
			
			if($fetch_borrower_name->num_rows() > 0)
			{
				$fetch_borrower_name = $fetch_borrower_name->result();
				foreach($fetch_borrower_name as $rows)
				{
					$con_id_borrower_name[] = $rows->b_name;
				}
				$data['con_id_borrower_name'] = $con_id_borrower_name;
			}
			
			$dataWhere['contact_id']			= $contact_idd;
			
			$fetch_contact_data 		= $this->User_model->select_where('contact',$dataWhere);
			if($fetch_contact_data->num_rows() > 0)
			{
				$data['fetch_contact_data'] 		= $fetch_contact_data->result();
				$fetch_contact_data 				= $fetch_contact_data->result();


				
				$fetch_borrower_contact_type 	= $this->User_model->select_where('borrower_contact_type',$dataWhere);
				if($fetch_borrower_contact_type->num_rows() > 0)
				{
					$data['fetch_borrower_contact_type'] = $fetch_borrower_contact_type->result();
				}
			
				$fetch_lender_contact_type = $this->User_model->select_where('lender_contact_type',$dataWhere);
				if($fetch_lender_contact_type->num_rows() > 0)
				{
					$data['fetch_lender_contact_type'] = $fetch_lender_contact_type->result();
				}
				
				
				$fetch_user_type = $this->User_model->query("Select * from user where id='".$fetch_contact_data[0]->internal_contact ."'");
				if($fetch_user_type->num_rows() > 0)
				{
					$fetch_user_types = $fetch_user_type->result();
					$data['fetch_user_name'] = $fetch_user_types[0]->fname.' '.$fetch_user_types[0]->middle_name.' '.$fetch_user_types[0]->lname;
					$data['fetch_user_id'] = $fetch_user_types[0]->id;
					$data['loginuser_contacts'] =$this->contact_originator_setting();
					

				}

				/* Fetch current_loans paid off loans detail*/
				if($fetch_contact_data[0]->lender_contact_type == 1)
				{
					$fetch_lender_contact1 = $this->User_model->select_where('lender_contact',$dataWhere);
					if($fetch_lender_contact1->num_rows() > 0)
					{
						$active_count_loan 						= 0;
						$active_total_investment 				= 0;
						$paidoff_count_loan_count_loan 			= 0;
						$paidoff_count_loan_total_investment 	= 0;
						
						$fetch_lender_contact 					= $fetch_lender_contact1->result();
					
						foreach($fetch_lender_contact as $row)
						{
							$lender_id1 = $row->lender_id;
							
							$sql = "SELECT COUNT(*) as count_active, SUM(investment) as total_active_investment FROM loan_assigment JOIN loan_servicing ON loan_servicing.talimar_loan = loan_assigment.talimar_loan WHERE loan_assigment.lender_name = '".$lender_id1."' AND loan_servicing.loan_status = '2' ";
							
							$result_total 				= $this->User_model->query($sql);
							$result_total 				= $result_total->result();
							
							$active_count_loan 			= $active_count_loan + $result_total[0]->count_active;
							$active_total_investment 	= $active_total_investment + $result_total[0]->total_active_investment;
							
							$sql1 = "SELECT COUNT(*) as count_paidoff, SUM(investment) as total_paidoff_investment FROM loan_assigment JOIN loan_servicing ON loan_servicing.talimar_loan = loan_assigment.talimar_loan WHERE loan_assigment.lender_name = '".$lender_id1."' AND loan_servicing.loan_status = '3' ";
							$result_total 				= $this->User_model->query($sql1);
							$result_total 				= $result_total->result();
							
							$paidoff_count_loan 		= $paidoff_count_loan + $result_total[0]->count_paidoff;
							$paidoff_total_investment 	= $paidoff_total_investment + $result_total[0]->total_paidoff_investment;
						}
					}
					
					$data['active_count_loan'] 			= $active_count_loan;
					$data['active_total_investment'] 	= $active_total_investment;
					$data['paidoff_count_loan'] 		= $paidoff_count_loan;
					$data['paidoff_total_investment'] 	= $paidoff_total_investment;
					$data['total_contact_investment'] 	= $paidoff_total_investment + $active_total_investment;
					$data['total_count_loan'] 			= $active_count_loan + $paidoff_count_loan;
				}
				//-------------------------------------------------
			}
			/* fetch contact relationships for 'Referred' by contact_id in "contact_relationshps" table...*/
			
			
			/*fetch contact_personal_notes...*/
			
			$contact_personal_notes = $this->User_model->query("SELECT * FROM `contact_personal_notes` WHERE contact_id ='".$contact_idd."'");
			$fetch_p_notes 			= $contact_personal_notes->result();
			$data['fetch_p_notes'] 	= $contact_personal_notes;
			
			
			$sqlll = "SELECT cr.id as cr_id,  cr.contact_id as contact_id ,c.contact_firstname as contact_firstname, c.contact_lastname as contact_lastname,  cr.relationship_contact_id as relationship_contact_id  FROM contact_relationship as cr JOIN contact as c on c.contact_id = cr.relationship_contact_id WHERE cr.contact_id = '".$contact_idd."' AND  cr.relationship_contact_id != '".$contact_idd."' ";
			$fetch_relationship_result = $this->User_model->query($sqlll);
			if($fetch_relationship_result->num_rows() > 0)
			{
				$fetch_relationship_result = $fetch_relationship_result->result();
				foreach($fetch_relationship_result as $rows_data_c){
					$data['contact_relationship_data'][] = array(
																'contact_firstname' 	=> $rows_data_c->contact_firstname.' '.$rows_data_c->contact_lastname,
																'contact_id' 				=> $rows_data_c->relationship_contact_id,
																'cr_id' 							=> $rows_data_c->cr_id,
																'typerrrr'						=> 'relationship_contact_id',
															);
				}
			}
			
			$sqlll = "SELECT cr.id as cr_id,   cr.contact_id as contact_id ,c.contact_firstname as contact_firstname, c.contact_lastname as contact_lastname,   cr.relationship_contact_id as relationship_contact_id  FROM contact_relationship as cr JOIN contact as c on c.contact_id = cr.contact_id WHERE cr.contact_id != '".$contact_idd."' AND  cr.relationship_contact_id = '".$contact_idd."' ";
			$fetch_relationship_result = $this->User_model->query($sqlll);
			if($fetch_relationship_result->num_rows() > 0)
			{
				$fetch_relationship_result = $fetch_relationship_result->result();
				foreach($fetch_relationship_result as $rows_data_c){
					$data['contact_relationship_data'][] = array(
																									'contact_firstname' => $rows_data_c->contact_firstname.' '.$rows_data_c->contact_lastname,
																									'contact_id' => $rows_data_c->contact_id,
																									'cr_id' => $rows_data_c->cr_id,
																									'typerrrr'						=> 'contact_id_typer',
																								);
				}
			}
		
			
			/* fetch contact documents by contact id from 'contact_document' table...*/
			$sql_contact_document = $this->User_model->query("SELECT * FROM contact_document WHERE contact_id = '".$contact_idd."' ORDER BY id ");
			
			$data['fetch_contact_document'] = $sql_contact_document->result();
			
			
			
		}
		
		/* fetch all users from user table...*/
		$sql_all_user = $this->User_model->query("SELECT * FROM user WHERE role = 1 ");
		$data['all_users'] = $sql_all_user->result();
		
       $sqll = $this->User_model->query("select DISTINCT(borrower_id) from borrower_contact where contact_id='".$contact_id."'");
		$sqll=$sqll->result();

             


		foreach($sqll as $rw){
          $borrower_id=$rw->borrower_id;
        

		  $sql_paidoff = "SELECT * , loan_servicing.loan_status FROM loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan.borrower = '".$borrower_id."' AND loan_servicing.loan_status = '3' ORDER BY loan.talimar_loan DESC";

			$fetch_paidoff_loan = $this->User_model->query($sql_paidoff);

	
			$fetch_paidoff_loan = $fetch_paidoff_loan->result();


             $key=0;
           
		     foreach($fetch_paidoff_loan as $r)
			{	

			  $loan_amount =$r->loan_amount;	

				$key++;
			$data['fetch_paidoff_loan_latest'][] = array(
														'count_loan' => $key++,
														'loan_amount' => $loan_amount,
														);
			
			
		}}

       $sqlll = $this->User_model->query("select DISTINCT(borrower_id) from borrower_contact where contact_id='".$contact_id."'");
		$sql_q=$sqlll->result();

             


foreach($sql_q as $rww){
       $borrower_idd=$rww->borrower_id;
        

		$sql_brokred = "SELECT * , loan_servicing.loan_status FROM loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan.borrower = '".$borrower_idd."' AND loan_servicing.loan_status = '5' ORDER BY loan.talimar_loan DESC";

			$fetch_b_loan = $this->User_model->query($sql_brokred);

	
			$fetch_brokred_loann = $fetch_b_loan->result();
			

             $keytt=0;
           
		     foreach($fetch_brokred_loann as $rs)
			{	

			  $loan_amountt =$rs->loan_amount;	

				$keytt++;
$data['fetch_brokered_loan_latest'][] = array(
														'b_count_loan' => $keytt++,
														'b_loan_amount' => $loan_amountt,
														);
			
			
}}




		foreach($sql_q as $rwww){
			$borrower_idd=$rwww->borrower_id;
        

			$sql_can = "SELECT * , loan_servicing.loan_status FROM loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan.borrower = '".$borrower_idd."' AND loan_servicing.loan_status = '4' ORDER BY loan.talimar_loan DESC";

			$fetch_bc_loan = $this->User_model->query($sql_can);

	
			$fetch_can_loann = $fetch_bc_loan->result();
			

             $can_keytt=0;
           
		     foreach($fetch_can_loann as $rss)
			{	

			  $can_loan_amountt =$rss->loan_amount;	

				$can_keytt++;

				$data['fetch_cancellled_loan_latest'][] = array(
														'can_b_count_loan' => $can_keytt++,
														'can_b_loan_amount' => $can_loan_amountt,
		

				);
			
			}
		}



		$sqll_sec = $this->User_model->query("select DISTINCT(borrower_id) from borrower_contact where contact_id='".$contact_id."'");
        $sqll_sec=$sqll_sec->result();

		foreach($sqll_sec as $r){
			
			$borrower_id_sec = $r->borrower_id;
			$sql_active = "SELECT * , loan_servicing.loan_status FROM loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan.borrower = '".$borrower_id_sec."' AND loan_servicing.loan_status = '2' ORDER BY loan.talimar_loan DESC";

			$fetch_sql_active = $this->User_model->query($sql_active);
			$fetch_sql_active = $fetch_sql_active->result();

            $k=0;
		    foreach($fetch_sql_active as $ro)
			{	

			  $activ_loan_amount =$ro->loan_amount;	

				$k++;
				$data['fetch_active_loan_latest'][] = array(
												'count_loan' => $k++,
												'activ_loan_amount' => $activ_loan_amount,
												);
	
			
			}
		}

		$sqll_sec = $this->User_model->query("select DISTINCT(borrower_id) from borrower_contact where contact_id='".$contact_id."'");

        $sqll_sec=$sqll_sec->result();

		foreach($sqll_sec as $r){
			
			$borrower_id_sec=$r->borrower_id;
			$sql_active = "SELECT * , loan_servicing.loan_status FROM loan JOIN loan_servicing ON loan_servicing.talimar_loan = loan.talimar_loan WHERE loan.borrower = '".$borrower_id_sec."' AND loan_servicing.loan_status = '1' ORDER BY loan.talimar_loan DESC";

			$fetch_pipe_active = $this->User_model->query($sql_active);
			$fetch_pipe_active = $fetch_pipe_active->result();
				
            $ki=0;
           
		    foreach($fetch_pipe_active as $ri)
			{	

			  $p_loan_amount =$ri->loan_amount;	
			  $ki++;
				$data['fetch_pipe_loan_latest'][] = array(
														'count_loan' => $ki++,
														'p_loan_amount' => $p_loan_amount,
														);
			}

		}
		

			 $sqll2 = $this->User_model->query("select * from lender_contact where contact_id='".$contact_id."'");
             $sqll2=$sqll2->result();
            

            $lender_idd=array();
        foreach ($sqll2 as $ky => $val) {
   			
   			 $lender_idd[]=$val->lender_id;
            
            }
        
       

       $l_id=implode("','",$lender_idd);

  $active_lender_sql = "SELECT COUNT(*) as count_loan, SUM(loan_assigment.investment) as total_investment , SUM(loan.loan_amount) as total_loan_amount FROM loan_assigment JOIN loan ON loan_assigment.talimar_loan = loan.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '2' AND loan_assigment.lender_name IN ('".$l_id."')";
			
				$fetch_active_loan = $this->User_model->query($active_lender_sql);
				if($fetch_active_loan->num_rows() > 0)
				{
					$fetch_active_loan = $fetch_active_loan->result();
					$active_loan[] = array(
													'count_loan' => $fetch_active_loan[0]->count_loan,
													'total_investment' => $fetch_active_loan[0]->total_investment,
													'total_loan_amount' => $fetch_active_loan[0]->total_loan_amount,
														);
				}


  $pipe_lender_sql = "SELECT COUNT(*) as count_loan, SUM(loan_assigment.investment) as total_investment , SUM(loan.loan_amount) as total_loan_amount FROM loan_assigment JOIN loan ON loan_assigment.talimar_loan = loan.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '1' AND loan_assigment.lender_name IN ('".$l_id."')";
			
				$fetch_pipe_loan = $this->User_model->query($pipe_lender_sql);
				if($fetch_pipe_loan->num_rows() > 0)
				{
					$fetch_pipe_loan = $fetch_pipe_loan->result();
					$pipe_loan[] = array(
													'count_loan' => $fetch_pipe_loan[0]->count_loan,
													'total_investment' => $fetch_pipe_loan[0]->total_investment,
													'total_loan_amount' => $fetch_pipe_loan[0]->total_loan_amount,
														);
				}
				

				
				 $paidoff_lender_sql = "SELECT COUNT(*) as count_loan, SUM(loan_assigment.investment) as total_investment , SUM(loan.loan_amount) as total_loan_amount FROM loan_assigment JOIN loan ON loan_assigment.talimar_loan = loan.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE  loan_servicing.loan_status = '3'  AND loan_assigment.lender_name IN ('".$l_id."')";
				
				$fetch_paidoff_loan = $this->User_model->query($paidoff_lender_sql);
				if($fetch_paidoff_loan->num_rows() > 0)
				{
					$fetch_paidoff_loan = $fetch_paidoff_loan->result();
					$paidoff_loan[] = array(
													'count_loan' => $fetch_paidoff_loan[0]->count_loan,
													'total_investment' => $fetch_paidoff_loan[0]->total_investment,
													'total_loan_amount' => $fetch_paidoff_loan[0]->total_loan_amount,
														);
				}



		$data['paidoff_loan'] = $paidoff_loan;
		$data['active_loan'] = $active_loan;

		$sql_users = $this->User_model->query("SELECT * FROM user");
		$sql_users = $sql_users->result();
			
			foreach($sql_users as $row){
			
			 $data['fetch_userss_data'][$row->id]= $row->fname.' '.$row->lname;

			
			}


		/*active loan section start*/
		 $quer = $this->User_model->query("select DISTINCT(borrower_id) from borrower_contact where contact_id='".$contact_id."'");
		 $quer=$quer->result();
		  foreach ($quer as $key => $value) {
		  
		  $bborrower_id=$value->borrower_id;
		  $contact_active_loan_query=$this->User_model->query("SELECT l.id AS loan_id,l.loan_amount,l.intrest_rate AS interestrate,lp.property_address,lp.unit,lp.city,lp.state,lp.zip FROM loan AS l INNER JOIN loan_servicing AS ls ON l.talimar_loan=ls.talimar_loan INNER JOIN loan_property AS lp ON lp.talimar_loan=ls.talimar_loan WHERE ls.loan_status='2' AND l.borrower='".$bborrower_id."'");

		 if($contact_active_loan_query->num_rows()>0){

		 	$contact_active_loanss[] = $contact_active_loan_query->result();

		 }
		}


		$mortgageActivation = 'SELECT mortgageActivate FROM lender_contact_type WHERE contact_id = "'.$contact_idd.'" ';
		$mortgageActivation = $this->User_model->query($mortgageActivation);

		$data['mortgage'] = $mortgageActivation->row_array();

		$data['contact_active_loan_query'] = $contact_active_loanss;
		
				
		$data['user_id'] = $this->session->userdata('t_user_id');
		$data['loginuser_contacts'] =$this->contact_originator_setting();


		$data['STATE_USA'] = $this->config->item('STATE_USA');
		$data['borrower_type_option'] = $this->config->item('borrower_type_option');
		$data['company_type_option'] = $this->config->item('company_type_option');
		$data['marital_status'] = $this->config->item('marital_status');
		$data['employment_status'] = $this->config->item('borrower_employment_status');

		$data['us_yes_no_option'] = $this->config->item('us_yes_no_option');
		$data['loan_tye'] = $this->config->item('loan_tye');
		$data['ltv_option'] = $this->config->item('ltv_option');
		$data['yeild_percent'] = $this->config->item('yeild_percent');
		$data['LoanTerm_type'] = $this->config->item('LoanTerm_type');
		$data['re885_complete_option'] = $this->config->item('re885_complete_option');

		$data['position_optionn'] = $this->config->item('position_optionn');

		$data['yes_no_option'] = $this->config->item('yes_no_option');
		$data['yes_no_option3'] = $this->config->item('yes_no_option3');
		$data['no_yes_option'] = $this->config->item('no_yes_option');
		$data['notes_type'] = $this->config->item('notes_type');
		$data['contact_type'] = $this->config->item('contact_type');
		$data['contact_specialty'] = $this->config->item('contact_specialty');
		$data['contact_email_blast'] = $this->config->item('contact_email_blast');
		$data['relationships_option'] = $this->config->item('relationships_option');
		$data['contact_status_option'] = $this->config->item('contact_status_option');
		$data['task_option'] = $this->config->item('contact_status_option');
		$data['contact_permission'] = $this->config->item('contact_permission');
		$data['lender_investment_experience'] = $this->config->item('lender_investment_experience');
		$data['lender_financial_situation'] = $this->config->item('lender_financial_situation');
		$data['lender_liquidity_needs'] = $this->config->item('lender_liquidity_needs');
		$data['referral_source'] = $this->config->item('referral_source');
		$data['contact_specialty_trust'] = $this->config->item('contact_specialty_trust');
		$data['lender_property_type'] = $this->config->item('lender_property_type');
		$data['lender_lend_value'] = $this->config->item('lender_lend_value');
		$data['lender_lend_value'] = $this->config->item('lender_lend_value');
		$data['contact_number_option'] = $this->config->item('contact_number_option');
		$data['email_types_option'] = $this->config->item('email_types_option');
		$data['selct_contact_task'] = $this->config->item('selct_contact_task');
		$data['contact_marketing_data'] = $this->config->item('contact_marketing_data');
		$data['contact_marketing_status_option'] = $this->config->item('contact_marketing_status_option');
		$data['contact_purpose_menu'] = $this->config->item('contact_purpose_menu');
		$data['portal_access_option'] = $this->config->item('portal_access_option');
		$data['lender_portal_access'] = $this->config->item('lender_portal_access');
		$data['setup_package_option'] = $this->config->item('setup_package_option');
		$data['contact_id'] = $contact_id;

		//fetch_contact_data
		
		return $data;

	}

	public function contact(){
		error_reporting(0);
		$contact_id_for_affilated = $this->uri->segment(2);
		$data = $this->contactData($contact_id_for_affilated);
		$data['user_role'] = $this->session->userdata('user_role');
		$data['contact_id_list'] = $contact_id_for_affilated;
		$data['content'] = $this->load->view('contact/contact',$data,true);
		$this->load->view('template_files/template',$data);
	}

	public function activateMortgage()
	{
		$post = $this->input->post();

		if(!empty($post['mortgageActivate']) && !empty($post['id']))
		{
			$mortgageActivate = $post['mortgageActivate'];
			$id = $post['id'];

			$sql = $this->User_model->query("UPDATE lender_contact_type SET mortgageActivate = '".$mortgageActivate."' WHERE contact_id = '".$id."'");

			if($sql)
			{
				$result['status'] = true;
				$result['message'] = '<div class="alert alert-success alert-dismissible" style="width: 100%">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Success!</strong> Mortgage activate successfully.
							</div>';
			}
			else
			{
				$result['status'] = false;
				$result['message'] = '<div class="alert alert-danger alert-dismissible" style="width: 100%">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Danger!</strong> Mortgage activate failed.
							</div>';
			}
		}
		else
		{
			$result['status'] = false;
			$result['message'] = '<div class="alert alert-danger alert-dismissible" style="width: 100%">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Danger!</strong> Mortgage activate failed.
							</div>';
		}

		echo json_encode($result);
	}
	

	public function allinvestorsnamelist(){

		$select_investorname = $this->User_model->query("SELECT id,name From investor");
		$select_investorname = $select_investorname->result();

		foreach ($select_investorname as $value) {
			$investorname[$value->id] = $value->name;
		}
		return $investorname;
	}


	public function index()
	{
	
		$data['user_role'] = $this->session->userdata('user_role');
		error_reporting(0);
	
		$contact_idd = $this->input->post('search_contact') ?  $this->input->post('search_contact')  :  $this->uri->segment(2);
		
		if($contact_idd)
		{
			$contact_id['contact_id'] 	= $contact_idd;
			
			$lender_contact_id          = $contact_idd;
			$come_contact_id          	= $contact_idd;
			
			
			//fetch Tags from "contact_tags" table...
			$sql_tags = $this->User_model->query("SELECT * FROM contact_tags WHERE contact_id = '".$come_contact_id."' ");
			
			$fetch_sql_tags = $sql_tags->result();
				
			foreach($fetch_sql_tags as $tags){
				
			
				$fetch_contact_specialty[] 		= $tags->contact_specialty;
				$fetch_contact_email_blast[] 	= $tags->contact_email_blast;
				$fetch_contact_email_blast_no[] 	= $tags->contact_email_blast_no;
				if($tags->contact_specialty){
					
					$fetch_contact_specialty_tag_id[$tags->contact_specialty] 		= $tags->id;
				}
				if($tags->contact_email_blast){
					
					$fetch_contact_email_blast_tag_id[$tags->contact_email_blast] 		= $tags->id;
				}
				if($tags->contact_email_blast_no){
					
					$fetch_contact_email_blast_tag_id[$tags->contact_email_blast_no] 		= $tags->id;
				}
			}	
			
			$data['fetch_contact_specialty'] 			= $fetch_contact_specialty;
			$data['fetch_contact_email_blast'] 			= $fetch_contact_email_blast;
			$data['fetch_contact_email_blast_no'] 		= $fetch_contact_email_blast_no;
			$data['fetch_contact_specialty_tag_id'] 	= $fetch_contact_specialty_tag_id;
			$data['fetch_contact_email_blast_tag_id'][$tags->id] 	= $fetch_contact_email_blast_tag_id;
			$data['fetch_sql_tags'] 			= $fetch_sql_tags;
			
			// fetch all contact_tag_extra_field..
			$tag_extra_field = $this->User_model->query("SELECT * FROM contact_tag_extra_field WHERE contact_id = '".$contact_idd."'");
			$data['tag_extra_field'] = $tag_extra_field->result();
			
			
			
			//fetch permissions from "contact_assigned" table...
			$sql_assigned 			= $this->User_model->query("SELECT * FROM contact_assigned WHERE contact_id = '".$come_contact_id."' ");
			
			$fetch_sql_assigned 	= $sql_assigned->result();
				
			$data['fetch_sql_assigned'] = $fetch_sql_assigned;
			
			if($fetch_sql_assigned[0]->particular_user_id){
				
				//fetch user name...
				$sql_user_name = $this->User_model->query("SELECT * FROM user WHERE id = '".$fetch_sql_assigned[0]->particular_user_id ."' ");
				
				$fetch_sql_user_name = $sql_user_name->row();
					
				$data['fetch_sql_user_name'] = $fetch_sql_user_name->fname .' '.$fetch_sql_user_name->lname;
			
			}
			
			// Fetch lender name 
			$sql_join = 'SELECT lender_contact.lender_id , investor.name FROM lender_contact JOIN investor ON lender_contact.lender_id = investor.id WHERE lender_contact.contact_id = "'.$come_contact_id.'" ';
			$fetch_lenders_name = $this->User_model->query($sql_join);
			
			if($fetch_lenders_name->num_rows() > 0)
			{
				$fetch_lenders_name = $fetch_lenders_name->result();
				foreach($fetch_lenders_name as $rows)
				{
					$con_id_lender_name[] = $rows->name;
				}
				$data['con_id_lender_name'] = $con_id_lender_name;
			}
			
			// Fetch Borrower name 
			$sql_join = 'SELECT borrower_contact.borrower_id , borrower_data.b_name FROM borrower_contact JOIN borrower_data ON borrower_contact.borrower_id = borrower_data.id WHERE borrower_contact.contact_id = "'.$come_contact_id.'" ';
			$fetch_borrower_name = $this->User_model->query($sql_join);
			
			if($fetch_borrower_name->num_rows() > 0)
			{
				$fetch_borrower_name = $fetch_borrower_name->result();
				foreach($fetch_borrower_name as $rows)
				{
					$con_id_borrower_name[] = $rows->b_name;
				}
				$data['con_id_borrower_name'] = $con_id_borrower_name;
			}

			
			
			
			$data['contact_id']			= $contact_idd;
			
			$fetch_contact_data 		= $this->User_model->select_where('contact',$contact_id);
			if($fetch_contact_data->num_rows() > 0)
			{
				$data['fetch_contact_data'] 		= $fetch_contact_data->result();
				$fetch_contact_data 				= $fetch_contact_data->result();
				
				// Fetch borrower_contact_type data...
				// if($fetch_contact_data[0]->borrower_contact_type == 1)
				// {
					$fetch_borrower_contact_type 	= $this->User_model->select_where('borrower_contact_type',$contact_id);
					if($fetch_borrower_contact_type->num_rows() > 0)
					{
						$data['fetch_borrower_contact_type'] = $fetch_borrower_contact_type->result();
					}
				// }
				
				// Fetch lender_contact_type data.....
				// if($fetch_contact_data[0]->lender_contact_type == 1)
				// {
					$fetch_lender_contact_type = $this->User_model->select_where('lender_contact_type',$contact_id);
					if($fetch_lender_contact_type->num_rows() > 0)
					{
						$data['fetch_lender_contact_type'] = $fetch_lender_contact_type->result();
					}
						
				// }
				// Fetch current_loans paid off loans detail
				if($fetch_contact_data[0]->lender_contact_type == 1)
				{
					// $fetch_lender_contact1 = $this->User_model->select_where('lender_contact',$contact_id);
					$fetch_lender_contact1 = $this->User_model->query("SELECT * FROM investor as i JOIN lender_contact as lc on i.id = lc.lender_id WHERE lc.contact_id = ".$contact_idd." ");
					
					if($fetch_lender_contact1->num_rows() > 0)
					{
						$active_count_loan 						= 0;
						$active_total_investment 				= 0;
						$paidoff_count_loan_count_loan 			= 0;
						$paidoff_count_loan_total_investment 	= 0;
						
						$fetch_lender_contact 					= $fetch_lender_contact1->result();
						$data['fetch_lender_contact']			= $fetch_lender_contact1->result();
					
						/* echo "<pre>";
							print_r($fetch_lender_contact);
						echo "</pre>"; */
						foreach($fetch_lender_contact as $row)
						{
							$lender_id1 = $row->lender_id;
							
							$sql = "SELECT COUNT(*) as count_active, SUM(investment) as total_active_investment FROM loan_assigment JOIN loan_servicing ON loan_servicing.talimar_loan = loan_assigment.talimar_loan WHERE loan_assigment.lender_name = '".$lender_id1."' AND loan_servicing.loan_status = '2' ";
							// echo "<pre>";
							// echo $sql;
							// echo "</pre>";
							$result_total 				= $this->User_model->query($sql);
							$result_total 				= $result_total->result();
							
							$active_count_loan 			= $active_count_loan + $result_total[0]->count_active;
							$active_total_investment 	= $active_total_investment + $result_total[0]->total_active_investment;
							
							$sql1 = "SELECT COUNT(*) as count_paidoff, SUM(investment) as total_paidoff_investment FROM loan_assigment JOIN loan_servicing ON loan_servicing.talimar_loan = loan_assigment.talimar_loan WHERE loan_assigment.lender_name = '".$lender_id1."' AND loan_servicing.loan_status = '3' ";
							$result_total 				= $this->User_model->query($sql1);
							$result_total 				= $result_total->result();
							
							$paidoff_count_loan 		= $paidoff_count_loan + $result_total[0]->count_paidoff;
							$paidoff_total_investment 	= $paidoff_total_investment + $result_total[0]->total_paidoff_investment;
						}
					}
					
					$data['active_count_loan'] 			= $active_count_loan;
					$data['active_total_investment'] 	= $active_total_investment;
					$data['paidoff_count_loan'] 		= $paidoff_count_loan;
					$data['paidoff_total_investment'] 	= $paidoff_total_investment;
					$data['total_contact_investment'] 	= $paidoff_total_investment + $active_total_investment;
					$data['total_count_loan'] 			= $active_count_loan + $paidoff_count_loan;
				}
				//-------------------------------------------------
			}
			
			
			// fetch contact documents by contact id from 'contact_document' table...
			$sql_contact_document = $this->User_model->query("SELECT * FROM contact_document WHERE contact_id = '".$contact_idd."' ORDER BY id ");
			
			$data['fetch_contact_document'] = $sql_contact_document->result();
		}
				
		// fetch all users from user table role = 1 ...
		$sql_all_user = $this->User_model->query("SELECT * FROM user WHERE role = 1 ");
		$data['all_users'] = $sql_all_user->result();
		
		// fetch all users...
		$sql_all_user1 = $this->User_model->query("SELECT * FROM user where account='1'");
		$data['all_users_data'] = $sql_all_user1->result();
		
		$data['user_id'] = $this->session->userdata('t_user_id');
		$data['content'] = $this->load->view('contact/index',$data,true);
		$this->load->view('template_files/template',$data);
	}
	
	
	public function contact_p_notes(){
		
		$t_user_id   		= $this->session->userdata('t_user_id');
		$contact_id 		= $this->input->post('contact_id');
		$personal_note 		= $this->input->post('p_notes');
		
		$data['t_user_id'] 	   		   		= $t_user_id;
		$data['contact_id'] 	   			= $contact_id;
		$data['personal_note'] 	   			= $personal_note;
		
		$sql = "SELECT * FROM `contact_personal_notes` WHERE `contact_id`='".$contact_id."'";
		//echo $sql;
		
		$fetch_sql = $this->User_model->query($sql);
		
		if($fetch_sql->num_rows() > 0){
			$where['contact_id'] = $contact_id;
			$this->User_model->updatedata('contact_personal_notes',$where,$data);
			$this->session->set_flashdata('success','Update Personal Notes!');
			redirect(base_url().'viewcontact/'.$contact_id,'refresh');
		}else{
			
			$this->User_model->insertdata('contact_personal_notes',$data);
			$this->session->set_flashdata('success','Personal Notes Submit!');
			redirect(base_url().'viewcontact/'.$contact_id,'refresh');
		}
	}
	
	/*
		Description : Add field ofac_searched in that function
		Author      : Bitcot
		Created     : 
		Modified    : 25-03-2021
	*/
	public function add_contact_data(){
		error_reporting(0);

		$data['user_id'] 	   		   		= $this->session->userdata('t_user_id');
		$data['contact_firstname'] 	   		= $this->input->post('first_name');
		$data['contact_middlename']    		= $this->input->post('middle_name');
		$data['contact_lastname'] 	   		= $this->input->post('last_name');
		$data['contact_suffix'] 	   		= $this->input->post('contact_suffix');
		$data['prefix'] 	   				= $this->input->post('prefix');
		$data['suffix'] 	   				= $this->input->post('suffix');
		$data['contact_address'] 	   		= $this->input->post('address');
		$data['contact_unit'] 		   		= $this->input->post('unit');
		$data['contact_city'] 		  		= $this->input->post('city');
		$data['contact_state'] 		  		= $this->input->post('state');
		$data['contact_zip'] 		   		= $this->input->post('zip');
		$data['same_mailing_address'] 		= $this->input->post('same_mailing_address');
		$data['contact_mailing_address'] 	= $this->input->post('mailing_address');
		$data['contact_mailing_unit'] 		= $this->input->post('mailing_unit');
		$data['contact_mailing_city'] 		= $this->input->post('mailing_city');
		$data['contact_mailing_state'] 		= $this->input->post('mailing_state');
		$data['contact_mailing_zip'] 		= $this->input->post('mailing_zip');
		$data['contact_phone'] 		   		= $this->input->post('phone');

		$data['primary_option'] 		   	= $this->input->post('primary_option');
		$data['alter_phone'] 		   		= $this->input->post('alter_phone');
		$data['alter_option'] 		   		= $this->input->post('alter_option');
		
		$data['pemail_type'] 		   		= $this->input->post('pemail_type');
		$data['aemail_type'] 		   		= $this->input->post('aemail_type');
		$data['contact_email2'] 		   	= $this->input->post('email2');
		$data['phone_extension'] 		   	= $this->input->post('phone_extension');
		$data['alter_phone_extension'] 		= $this->input->post('alter_phone_extension');


		//$data['Scotsman_Guide'] 		   	= $this->input->post('Scotsman_Guide');

		if($data['alter_phone'] || $data['alter_option'] != ''){
			$data['addPhoneopt'] 		   	= '2';
		}else{
			$data['addPhoneopt'] 		   	= '1';
		}

		if($data['contact_email2'] || $data['aemail_type'] != ''){
			$data['addemailopt'] 		   		= '2';
		}else{
			$data['addemailopt'] 		   		= '1';
		}

		//$data['portalaccess'] 		   		= implode(",", $this->input->post('portalaccess'));


		$data['contact_secondary_phone']	= $this->input->post('secondary_phone');
		$data['contact_email'] 		   		= $this->input->post('email');
		
		$data['contact_fax'] 		   		= $this->input->post('fax');
		$data['contact_website'] 		   	= $this->input->post('website');
		$data['company_name_1'] 		   	= $this->input->post('company_name_1');
		$data['company_name_2'] 		   	= $this->input->post('company_name_2');
		$data['company_name_3'] 		   	= $this->input->post('company_name_3');
		$data['broker_license'] 		   	= $this->input->post('broker_license');
		$data['broker_license_state'] 		= $this->input->post('broker_license_state');
		$data['contractor_license'] 		= $this->input->post('contractor_license');
		$data['contractor_license_state']   = $this->input->post('contractor_license_state');
		$data['ofac_searched']				= $this->input->post('ofac_searched');
		$data['contact_personal_notes'] 	= $this->input->post('personal_notes');
		$data['fb_profile'] 				= $this->input->post('fb_profile');
		$data['lkdin_profile'] 				= $this->input->post('lkdin_profile');
		$data['referral_source'] 			= $this->input->post('referral_source');
		$ArrayContactMarketingData 			= $this->input->post('contact_marketing_data');
		if(!empty($ArrayContactMarketingData) && is_array($ArrayContactMarketingData)){
			$ArrayContactMarketingData=implode(",", $ArrayContactMarketingData);
		}
		$data['contact_marketing_data'] 	= $ArrayContactMarketingData;
		$data['internal_contact'] 			= $this->input->post('internal_contact');
		
		$data['bigger_pocket_profile'] 		= $this->input->post('bigger_pocket_profile');
		$data['contact_about'] 				= $this->input->post('about');
		$data['contact_dob'] 		  	 	= NULL;

		if($this->check_date_format($this->input->post('dob')) == 1){
			//$myDateTime3 = DateTime::createFromFormat('m-d-Y', $this->input->post('dob'));
			//$newDateString3 			= $myDateTime3->format('Y-m-d');
			$newDateString3 			= input_date_format($this->input->post('dob'));
			$data['contact_dob'] 	   =  $newDateString3; 
		}
		
		
		$data['borrower_contact_type'] = $this->input->post('borrower_chkbox_val');
		$data['lender_contact_type']   = $this->input->post('lender_chkbox_val');
		
		$contact_id		= $this->uri->segment(3) ? $this->uri->segment(3) : $this->input->post('contact_id');
		
		// $contact_id = $this->input->post('contact_id');
		$data['contact_id'] = $this->input->post('contact_id');
		if($this->input->post('contact_id'))
		{
			$update_id['contact_id'] = $this->input->post('contact_id');
			$this->User_model->updatedata('contact',$update_id,$data);
			$this->session->set_flashdata('success','Contact Updated Successfully!');
			
		}else{

			$this->User_model->insertdata('contact',$data);
			$this->session->set_flashdata('success','Contact Submit Successfully!');
			
			
			// User add contact in table ->
			$insert_id1 = $this->db->insert_id();
			$insert_id 	= $insert_id1 ? $insert_id1 : $this->input->post('contact_id');
			
			/*--------------- start add data in user_added_contact table ----------*/
			
			$last_insert_id				= $insert_id;
			$date 						= date('m-d-Y');
			//$myDateTime 				= DateTime::createFromFormat('m-d-Y',$date);
			$newDateString 				= date("Y-m-d"); //$myDateTime->format('Y-m-d');
		
			$data_user['date']  		= $newDateString ? $newDateString : '';
			
			$data_user['type'] 			= 'Contact';
			$data_user['status'] 		= 'Insert';
			$data_user['contact_id']    = $last_insert_id;
			$data_user['t_user_id']		= $this->session->userdata('t_user_id');
			
			$check_sql = $this->User_model->query("SELECT * FROM `user_added_contact` WHERE `contact_id`='".$last_insert_id."'");
			
			if($check_sql->num_rows > 0){
				
			$this->User_model->updatedata('user_added_contact',$last_insert_id,$data_user);
			
			}else{
			
				$this->User_model->insertdata('user_added_contact',$data_user);
			}
		}
	
			
		
		$name = $this->input->post('first_name').' '.$this->input->post('middle_name').''.$this->input->post('last_name');
		$insert_id 	= $insert_id1 ? $insert_id1 : $this->input->post('contact_id');
		
			$contact_specialty 	= $this->input->post('contact_specialty') ? $this->input->post('contact_specialty') : ''; 
			$contact_specialtyy 	= $this->input->post('contact_specialtyy') ? $this->input->post('contact_specialtyy') : '';
			$contact_email_blast 	= $this->input->post('contact_email_blast') ? $this->input->post('contact_email_blast') : '' ;
			$contact_email_blast_no 	= $this->input->post('contact_email_blast_no') ? $this->input->post('contact_email_blast_no') : '' ;
				
			$user_id 	   			= $this->session->userdata('t_user_id');
			$contact_id	 	 		= $insert_id;
				
			$contact_specialty_all = $this->config->item('contact_specialty');
			$contact_specialtyy_all = $this->config->item('contact_specialty_trust');
			
				foreach($contact_specialty_all as $key => $row)
				{
					$specialty = $this->User_model->query("SELECT * FROM contact_tags WHERE contact_specialty = '".$key."' AND contact_id = '".$contact_id."'");
					
					if($specialty->num_rows() > 0){
						
						if(in_array($key,$contact_specialty))
						{
							
						}
						else
						{
							$this->User_model->delete('contact_tags',array('contact_id'=>$contact_id,'contact_specialty'=>$key));
						}
					}
					else
					{
						
						if(in_array($key,$contact_specialty))
						{
							//echo 'ddd';
							$this->User_model->insertdata('contact_tags',array('contact_id'=>$contact_id,'contact_specialty'=>$key,'user_id'=>$user_id));
						}
						
					}
				}
				
				$contact_email_blast_all = $this->config->item('contact_email_blast');
				foreach($contact_email_blast_all as $key => $row){
					
					$email_blast = $this->User_model->query("SELECT * FROM contact_tags WHERE contact_email_blast = '".$key."' AND contact_id='".$contact_id."'");
					if($email_blast->num_rows() > 0){
						
						if(in_array($key,$contact_email_blast))
						{
							
						}
						else
						{
							$this->User_model->delete('contact_tags',array('contact_id'=>$contact_id,'contact_email_blast'=>$key));
						}
						
					}else{
						
						
						if(in_array($key,$contact_email_blast))
						{
							$this->User_model->insertdata('contact_tags',array('contact_id'=>$contact_id,'contact_email_blast'=>$key,'user_id'=>$user_id));
						}
					}
					
					
					$email_blast_no = $this->User_model->query("SELECT * FROM contact_tags WHERE contact_email_blast_no = '".$key."' AND contact_id='".$contact_id."'");
						if($email_blast_no->num_rows() > 0){
							
							if(in_array($key,$contact_email_blast_no))
							{
								
							}
							else
							{
								$this->User_model->delete('contact_tags',array('contact_id'=>$contact_id,'contact_email_blast_no'=>$key));
							}
							
						}else{
							
							
							if(in_array($key,$contact_email_blast_no))
							{
								$this->User_model->insertdata('contact_tags',array('contact_id'=>$contact_id,'contact_email_blast_no'=>$key,'user_id'=>$user_id));
							}
						}
				}
				
				
				/*$assigned_id = $this->input->post('assigned_id');
				$data12['contact_id'] 	 		= $insert_id;
				$data12['particular_user_id'] 	= $this->input->post('particular_user_id');
				$data12['contact_permission'] 	= ($this->input->post('contact_permission'))   ? serialize($this->input->post('contact_permission')) : '';
				$data12['user_id'] 	   			= $this->session->userdata('t_user_id');
				
				if($assigned_id){
					
					$this->User_model->updatedata('contact_assigned',array('id'=>$assigned_id),$data12);
				
				}else{
					
					$this->User_model->insertdata('contact_assigned',$data12);
				}*/
		
				// if($this->input->post('borrower_chkbox_val') == '1'){
				// echo 'insert to "borrower_contact_type" table...';
				
				
				$data1['user_id'] 	   				 	= $this->session->userdata('t_user_id');
				$data1['borrower_driver_license'] 	 	= $this->input->post('driver_license');
				$data1['borrower_driver_license_state'] = $this->input->post('driver_license_state');
				$data1['borrower_exp_date'] 	 		= NULL;
				
				if($this->check_date_format($this->input->post('exp_date')) == 1){
					
					//$myDateTime 	= DateTime::createFromFormat('m-d-Y', $this->input->post('exp_date'));
					//$newDateString 	= $myDateTime->format('Y-m-d');
					$newDateString 	= input_date_format($this->input->post('exp_date'));
					$data1['borrower_exp_date'] 	   =  $newDateString;
				}
				
				
				if($this->check_date_format($this->input->post('credit_score_date')) == 1){
					
					//$myDateTime1 	= DateTime::createFromFormat('m-d-Y', $this->input->post('credit_score_date'));
					//$newDateString1 = $myDateTime1->format('Y-m-d');
					$newDateString1 = input_date_format($this->input->post('credit_score_date'));
					$data1['borrower_credit_score_date'] = $newDateString1;
				
				}else{
					
					$data1['borrower_credit_score_date'] = NULL;
				}
				
				if($this->check_date_format($this->input->post('date_dismissed')) == 1){
					
					//$myDateTime11 	= DateTime::createFromFormat('m-d-Y', $this->input->post('date_dismissed'));
					//$newDateString11 = $myDateTime11->format('Y-m-d');
					$newDateString11 = input_date_format($this->input->post('date_dismissed'));
					$data1['borrower_date_dismissed'] = $newDateString11;
				
				}else{
					
					$data1['borrower_date_dismissed'] = NULL;
				}
				
			
				$data1['borrower_ss_num'] 	   		 = $this->input->post('security_number');
				$data1['borrower_credit_score'] 	 = $this->input->post('credit_score');
				if($this->input->post('credit_score_desc')){
					$data1['borrower_credit_score_desc'] = $this->input->post('credit_score_desc');
				}
				$data1['borrower_bankruptcy'] 	   	 = $this->input->post('Bankruptcy');
				$data1['borrower_BK'] 	   			 = $this->input->post('BK');
				$data1['borrower_BK_desc'] 	   		 = htmlentities($this->input->post('bk_desc'));
				$data1['borrower_contact_desc'] 	 = htmlentities($this->input->post('about_contact'));
				$data1['borrower_marital_status'] 	 = $this->input->post('marital_status');
				$data1['borrower_dependents'] 	   	 = $this->input->post('dependents');
				//$data1['borrower_us_citizen'] 	   	 = $this->input->post('us_citizen');
				//$data1['borrower_us_citizen_desc'] 	 = htmlentities($this->input->post('us_citizen_desc'));
				$data1['borrower_litigation'] 	   	 = $this->input->post('litigation');
				$data1['borrower_litigation_desc'] 	 = htmlentities($this->input->post('litigation_desc'));
				$data1['borrower_felony'] 	   	 	 = $this->input->post('felony');
				$data1['borrower_felony_desc'] 	 	 = htmlentities($this->input->post('felony_desc'));
				
				// first check row exist or not in 'borrower_contact_type' table...
				
				$check_id1['contact_id'] 		= $this->input->post('contact_id');
				$check_borrower_contact_type 	= $this->User_model->select_where('borrower_contact_type',$check_id1);
				
				if($check_borrower_contact_type->num_rows() > 0)
				{
					$update_id1['contact_id'] = $this->input->post('contact_id');
					
					// update to "borrower_contact_type" table...
					$this->User_model->updatedata('borrower_contact_type',$update_id1,$data1);
					
				}
				else
				{
					$data1['contact_id'] 				 = $insert_id;
					
					// insert into "borrower_contact_type" table...
					
					$this->User_model->insertdata('borrower_contact_type',$data1);
				}
				
				
				// upload Contact documents....
				if($_FILES){
				
				
					//first check document uploaded for borrower...
					$check_fci_query 	= $this->User_model->query("SELECT * FROM contact_document WHERE contact_id = '".$insert_id."' ");
					
					if($check_fci_query->num_rows() > 0)
					{
						$cnt = $check_fci_query->num_rows();
					}else{
						$cnt = 1;
					}
					
					

					$folder = 'contact_documents/'.$insert_id.'/' ;
					/*$folder = FCPATH.'contact_documents/'.$insert_id.'/' ;
					if(!is_dir($folder))
					{
						mkdir($folder, 0777, true);
					}*/
					
					
					foreach($_FILES['contact_upload_doc']['name'] as $key => $row_data){
						
						if($row_data)
						{
							
							$ext = strtolower(pathinfo($row_data, PATHINFO_EXTENSION));
							
							// only docx files are allowed to uploaded!
							
							// if($ext == 'docx' || $ext == 'doc')
							if($ext == 'pdf' )
							{
									
								// $new_name = 'borrower-'.$insert_id.'_'.$cnt++.'.'.$ext;
								$new_name = $row_data;
								$target_dir 	= $folder.$new_name;
									
								$contact_document['contact_id']				=	$insert_id ? $insert_id :$contact_idd;
								$contact_document['name']					=	$name;
								$contact_document['contact_document']		=	$target_dir;
								$contact_document['user_id']				=	$this->session->userdata('t_user_id');
								
								$attachmentName=$this->aws3->uploadFile($target_dir,$_FILES["contact_upload_doc"]["tmp_name"][$key]);
								//if(move_uploaded_file($_FILES["contact_upload_doc"]["tmp_name"][$key], $target_dir) === TRUE )
								if($attachmentName)
								{
									$this->User_model->insertdata('contact_document',$contact_document);
								}
							}
							
						}
					}
							
				}
				
				// echo 'insert to "lender_contact_type" table...';
				
				$data2['user_id'] 	   				= $this->session->userdata('t_user_id');
				$data2['lender_RE_date'] 			= NULL;
				
				$newDateString2 = '';
				$newDateString3 = '';
				
				if($this->input->post('RE_date')){
					
					//$myDateTime2 	= DateTime::createFromFormat('m-d-Y', $this->input->post('RE_date'));
					//$newDateString2 = $myDateTime2->format('Y-m-d');
					$newDateString2 = input_date_format($this->input->post('RE_date'));
					$data2['lender_RE_date'] 			=  $newDateString2;
				}
				
				$data2['setup_package']					= $this->input->post('setup_package');
				//$data2['setup_package_date'] 			= NULL;
				if($this->input->post('setup_package_date')){
					
					//$myDateTime3 	= DateTime::createFromFormat('m-d-Y', $this->input->post('setup_package_date'));
					//$newDateString3 = $myDateTime3->format('Y-m-d');
					$newDateString3 = input_date_format($this->input->post('setup_package_date'));
					$data2['setup_package_date'] 			=  $newDateString3;
				}

				if($this->input->post('DateSigned')){
					
					//$myDateTime3 	= DateTime::createFromFormat('m-d-Y', $this->input->post('DateSigned'));
					//$newDateString3 = $myDateTime3->format('Y-m-d');
					$newDateString3 = input_date_format($this->input->post('DateSigned'));
					$data2['DateSigned'] 			=  $newDateString3;
				}
				
				
				$data2['accountword'] 						= $this->input->post('accountword');
				$data2['lender_annual_income'] 				= $this->input->post('annual_income');
				$data2['lender_annual_net_worth'] 			= $this->input->post('annual_net_worth');
				$data2['lender_estimate_liquid_assets'] 	= $this->input->post('estimate_liquid_assets');
				$data2['lender_liquidity_needs'] 			= $this->input->post('liquidity_needs');
				$data2['lender_alt_contact_fields'] 		= $this->input->post('alternate_contact_fields');
				$data2['lender_alt_firstname'] 				= $this->input->post('lender_alt_first_name');
				$data2['lender_alt_middlename'] 			= $this->input->post('lender_alt_middle_name');
				$data2['lender_alt_lastname'] 				= $this->input->post('lender_alt_last_name');
				$data2['lender_alt_address'] 				= $this->input->post('lender_alt_address');
				$data2['lender_alt_unit'] 					= $this->input->post('lender_alt_unit');
				$data2['lender_alt_city'] 					= $this->input->post('lender_alt_city');
				$data2['lender_alt_state'] 					= $this->input->post('lender_alt_state');
				$data2['lender_alt_zip'] 					= $this->input->post('lender_alt_zip');
				$data2['lender_alt_phone'] 					= $this->input->post('lender_alt_phone');
				$data2['lender_alt_email'] 					= $this->input->post('lender_alt_email'); 

				
				// insert to "lender_contact_type" table...
				$check_id2['contact_id'] 	= $this->input->post('contact_id');
				
				// Also update in lender contact
				$lender_contact_data['contact_phone']			= $this->input->post('phone');
				$lender_contact_data['contact_email']			= $this->input->post('email');
				$lender_contact_data['contact_re_date']			= $newDateString2 ? $newDateString2 :'';
				$this->User_model->updatedata('lender_contact',$check_id2,$lender_contact_data);
				
				$check_lender_contact_type 	= $this->User_model->select_where('lender_contact_type',$check_id2);
				
				
				if($check_lender_contact_type->num_rows() > 0)
				{
					$update_id2['contact_id'] 		= $this->input->post('contact_id');
					$this->User_model->updatedata('lender_contact_type',$update_id2,$data2);
				}
				else
				{
					//03-02-2021
					$data2['username'] 				= '';
					$data2['password'] 				= '';
					$data2['status'] 				= 'av_account';
					$data2['allow_access']          = 1;
					//03-02-2021

					$data2['contact_id'] 			= $insert_id;
					$this->User_model->insertdata('lender_contact_type',$data2);

					$lender_unic_id = $this->db->insert_id(); //03-02-2021

					$avArgument  = '';
					$avArgument .= $lender_unic_id.'@@@@';
					$avArgument .= $this->input->post('email').'@@@@';
					$avArgument .= $this->input->post('first_name').'@@@@';
					$avArgument .= $this->input->post('last_name').'@@@@';
					$avArgument .= $this->input->post('phone');
					$stringBase  = base64_encode($avArgument);
				}
				
				
			
		 	if($this->input->post('contact_id'))
			{	
				
				redirect(base_url().'viewcontact/'.$contact_id,'refresh');
				
			}else{
				redirect(base_url().'contactlist/');
			} 	
	}
	
	
	public function check_contact_filename(){
		
		$contact_id = $this->input->post('contact_id');
		$filename 	 = $this->input->post('filename');
		
		$folder 		= 'contact_documents/'.$contact_id.'/' ; //FCPATH
		$target_dir 	= $folder.$filename;
		
		// check filename exist in the "borrower_document" table or not...
		$sel = $this->User_model->query("SELECT * FROM contact_document WHERE contact_id  = '".$contact_id."' AND contact_document = '".$target_dir."' ");	
		
		if($sel->num_rows() > 0){
			
			echo '1';			/* duplicacy occurs, error; */
		
		}else{
			
			echo '0';			/* no duplicacy occurs, unique;	*/
		
		}
	}
	
	
	public function p_address_contact(){			
			$search = $this->input->post('property_address');
			
			$sql = "SELECT * FROM `loan_property` WHERE property_address LIKE %$search%";
			$fetch_property = $this->User_model->query($sql);
			$fetch_property = $fetch_property->result();	
		
	}

	public function new_account_setup(){

		$password 	    = $this->input->post('password');
		$username 	    = $this->input->post('username');
		$name 		    = $this->input->post('name');
		$lender_id 		= $this->input->post('lender_id');
		$contact_email  = $this->input->post('contact_email');
		
		$data2 = array();
		$update_id2 = array();

		$data2['temp_password'] 				= $password;
		$update_id2['lender_contact_id'] 		= $lender_id;
		$this->User_model->updatedata('lender_contact_type',$update_id2,$data2);

		$main_content = '<p>Dear '.$name.'</p>';
		$main_content .= '<p>Thank you for reaching out to TaliMar Financial regarding about investing in trust deeds. Below is your temporary Username and Password. Once you log in, you will be directed to change your password. You may update your account information, including your username, by selecting My Profile under your Name in the top right corner</p>';

		$main_content .= '<p>Username: '.$username.'<br>Temporary Password: '.$password.'</p>';

		$main_content .= '<p style="color:red;">Note: Once the user logs in to the website, direct them to immediately change their password.</p>';
		
				
		$dataMail = array();
		$dataMail['from_email'] 	= 'invest@talimarfinancial.com';
		$dataMail['from_name'] 		= 'TaliMar Financial';
		$dataMail['to'] 			= $contact_email;
		$dataMail['name'] 			= '';
		$dataMail['subject'] 		= 'New Account Setup';
		$dataMail['content_body'] 	= $main_content;
		$dataMail['button_url'] 	= Lender_url;
		$dataMail['button_text'] 	= 'Login Here';
		send_mail_template($dataMail);

		echo "success";
		die();
	}

	public function lender_td_details(){
		$lender_td_details = 'no';
		if($this->input->post('lender_td_details')){
			if($this->input->post('lender_td_details') == 'yes'){
				$lender_td_details = 'yes';
			}
		}
		$data2['lender_td_details'] 		= $lender_td_details;
		$update_id2['contact_id'] 		= $this->input->post('contact_id');
		$this->User_model->updatedata('lender_contact_type',$update_id2,$data2);
		echo "success";
		die();
	}

	/*
		Description : This function use for update contact data - TD Details Access update  issue resolve in Lender data popup 
		Author      : Bitcot
		Created     : 
		Modified    : 06-04-2021
	*/
	
	
	public function update_contact_data(){
		
		error_reporting(0);
		$insert_id 		=  $this->input->post('contact_id');
		$contact_id 	=  $this->input->post('contact_id');
		
		
		/*********** BORROWER DATA: SCRIPT TO INSERT/UPDATE VALUES STARTS ************/
		
		$data1['borrower_ss_num'] 	   		 = $this->input->post('security_number');
		//$data1['borrower_contact_dob'] 	   		 = $this->input->post('dob');
		$data1['borrower_contact_dob'] 		 = NULL;
		
		if($this->check_date_format($this->input->post('dob')) == 1){
			//$myDateTime3 = DateTime::createFromFormat('m-d-Y', $this->input->post('dob'));
			//$newDateString3 					= $myDateTime3->format('Y-m-d');
			$newDateString3 					= input_date_format($this->input->post('dob'));
			$data1['borrower_contact_dob'] 	   	=  $newDateString3; 
		}		
		
		
		$data1['borrower_driver_license'] 	 	= $this->input->post('driver_license');
		$data1['borrower_driver_license_state'] = $this->input->post('driver_license_state');
		//$data1['borrower_exp_date'] 			= $this->input->post('exp_date');
		$data1['borrower_exp_date'] 	 		= NULL;
		if($this->check_date_format($this->input->post('exp_date')) == 1){
			//$myDateTime31 = DateTime::createFromFormat('m-d-Y', $this->input->post('exp_date'));
			//$newDateString31 					= $myDateTime31->format('Y-m-d');
			$newDateString31 					= input_date_format($this->input->post('exp_date'));
			$data1['borrower_exp_date'] 	   	=  $newDateString31; 
		}	

			
		$data1['borrower_credit_score'] 	 = $this->input->post('credit_score');
		if($this->check_date_format($this->input->post('credit_score_date')) == 1){
					
			//$myDateTime1 	= DateTime::createFromFormat('m-d-Y', $this->input->post('credit_score_date'));
			//$newDateString1 = $myDateTime1->format('Y-m-d');
			$newDateString1 = input_date_format($this->input->post('credit_score_date'));
			$data1['borrower_credit_score_date'] = $newDateString1;
				
		}else{
			$data1['borrower_credit_score_date'] = NULL;
		}
				
		if($this->input->post('credit_score_desc')){
			$data1['borrower_credit_score_desc'] = $this->input->post('credit_score_desc');	
		}
		
		$data1['borrower_marital_status'] 	 = $this->input->post('marital_status');
		//$data1['ofac_search'] 				 = $this->input->post('ofac_search');
		$data1['ofac_srch_completed'] 		 = $this->input->post('ofac_srch_completed');
		$data1['employment_status'] 		 = $this->input->post('employment_status');

		$data1['borrower_experience'] 		 = $this->input->post('borrower_experience');
		$data1['ex_how_many'] 				 = $this->input->post('ex_how_many');
		
		$data1['current_employer'] 			 = $this->input->post('current_employer');
		
		$data1['annul_income'] 			 	 = $this->amount_format($this->input->post('annul_income'));
		
		$data1['year_at_job'] 			 	 = $this->input->post('year_at_job');
		
		$data1['estimate_income'] 			 = $this->amount_format($this->input->post('estimate_income'));
		
		$data1['estimate_income_year'] 		 = $this->input->post('estimate_income_year');
		
		$data1['prvious_yr_income'] 		 = $this->amount_format($this->input->post('prvious_yr_income'));
		
		$data1['prvious_income_year'] 		 = $this->input->post('prvious_income_year');
		
		$data1['job_title'] 		 		 = $this->input->post('job_title');
		$data1['conform_paystub'] 		 	 = $this->input->post('conform_paystub');
		$data1['conform_return'] 		 	 = $this->input->post('conform_return');
		
		
		$data1['borrower_dependents'] 	   	 = $this->input->post('dependents');
		$data1['borrower_us_citizen'] 	   	 = $this->input->post('us_citizen');
		$data1['borrower_us_citizen_desc'] 	 = htmlentities($this->input->post('us_citizen_desc'));
		$data1['borrower_litigation'] 	   	 = $this->input->post('litigation');
		$data1['borrower_litigation_desc'] 	 = htmlentities($this->input->post('litigation_desc'));
		$data1['borrower_felony'] 	   	 	 = $this->input->post('felony');
		$data1['borrower_felony_desc'] 	 	 = htmlentities($this->input->post('felony_desc'));
		$data1['borrower_bankruptcy'] 	   	 = $this->input->post('Bankruptcy');
		$data1['bankruptcy_year'] 	   	 	 = $this->input->post('Bankruptcy_year');
		
		$data1['bankruptcy_7year_text'] 	 = $this->input->post('bankruptcy_7year_text');
		
		$data1['borrower_BK'] 	   			 = $this->input->post('BK');
		$data1['borrower_BK_desc'] 	   		 = htmlentities($this->input->post('bk_desc'));
		if($this->check_date_format($this->input->post('date_dismissed')) == 1){
					
			//$myDateTime11 	= DateTime::createFromFormat('m-d-Y', $this->input->post('date_dismissed'));
			//$newDateString11 = $myDateTime11->format('Y-m-d');
			$newDateString11 = input_date_format($this->input->post('date_dismissed'));
			$data1['borrower_date_dismissed'] = $newDateString11;
				
		}else{
					
			$data1['borrower_date_dismissed'] = NULL;
		}

		//borrower portal data...
		$data1['allow_access_borrower']		= $this->input->post('allow_access_borrower');
		$data1['borrower_username']			= $this->input->post('borrower_username');
		$data1['borrower_password']			= base64_encode($this->input->post('borrower_password'));
		
		
		
		// first check row exist or not in 'borrower_contact_type' table...
				
		$check_id1['contact_id'] 		= $this->input->post('contact_id');
		$check_borrower_contact_type 	= $this->User_model->select_where('borrower_contact_type',$check_id1);


				
		if($check_borrower_contact_type->num_rows() > 0)
		{
			$update_id1['contact_id'] = $this->input->post('contact_id');
			$this->User_model->updatedata('borrower_contact_type',$update_id1,$data1);			
		}
		else {
			$data1['contact_id'] 				 = $insert_id;
			$this->User_model->insertdata('borrower_contact_type',$data1);
		}
		
		/*********** BORROWER DATA: SCRIPT TO INSERT/UPDATE VALUES ENDS ************/
		
		
		/*********** LENDER DATA : SCRIPT TO INSERT/UPDATE VALUES STARTS ************/
		
			$data2['user_id'] 	   				= $this->session->userdata('t_user_id');
			
			$newDateString2 = '';
			$data2['lender_RE_date'] 			= NULL;
			
				
			if($this->input->post('RE_date')){
					
				//$myDateTime2 	= DateTime::createFromFormat('m-d-Y', $this->input->post('RE_date'));
				//$newDateString2 = $myDateTime2->format('Y-m-d');
				$newDateString2 = input_date_format($this->input->post('RE_date'));
				$data2['lender_RE_date'] 			=  $newDateString2;
			}

			$data2['lender_dob'] 				= NULL;
			$newDateString22 = '';
			$newDateString3 = '';
			$newDateString33='';
			if($this->input->post('lender_dob')){
					
				//$myDateTime22 	= DateTime::createFromFormat('m-d-Y', $this->input->post('lender_dob'));
				//$newDateString22 = $myDateTime22->format('Y-m-d');
				$newDateString22 = input_date_format($this->input->post('lender_dob'));
				
				$data2['lender_dob'] 			=  $newDateString22;
			} 
			
			//$data2['setup_package']					= $this->input->post('setup_package');
			
	        

            // ./////////////////////my code...........//
			/*Remove as per task
			  updated Date-27-07-2021*/
	        /*$data2['nda_signed']					= $this->input->post('nda_signed');
			$data2['nda_date'] 						= NULL;
			
			if($this->input->post('nda_date')){
				$newDateString33 = input_date_format($this->input->post('nda_date'));
				$data2['nda_date'] 			=  $newDateString33;
			}*/
       
            
				
			/*if($this->input->post('setup_package_date')){
				$newDateString3 = input_date_format($this->input->post('setup_package_date'));
				$data2['setup_package_date'] 			=  $newDateString3;
			}
			if($this->input->post('setup_submitted_date')){
				$newDateString3 = input_date_format($this->input->post('setup_submitted_date'));
				$data2['setup_submitted_date'] 			=  $newDateString3;
			}*/
			$data2['re_complete']					= $this->input->post('re_complete');
			if($this->input->post('DateSigned')){
				
				//$myDateTime3 	= DateTime::createFromFormat('m-d-Y', $this->input->post('DateSigned'));
				//$newDateString3 = $myDateTime3->format('Y-m-d');
				$newDateString3 = input_date_format($this->input->post('DateSigned'));
				$data2['DateSigned'] 			=  $newDateString3;
			}
			
			$data2['accountword']				= $this->input->post('accountword');
			$data2['accountwordAnswer']			= $this->input->post('accountwordAnswer');
			$data2['allow_access']				= $this->input->post('allow_access');
			$data2['username']					= $this->input->post('username');
			$data2['password']					= $this->input->post('password');
			$data2['committed_funds']			= $this->amount_format($this->input->post('committed_funds'));

           	$data2['WholeNoteInvestor']			= $this->input->post('WholeNoteInvestor');

           	$data2['ownership_type']			= $this->input->post('ownership_type');
           	$data2['lend_value']				= $this->input->post('lend_value');
           	$data2['LoanTerm']					= $this->input->post('LoanTerm');
           	$data2['ltv']						= $this->input->post('ltv');
           	$data2['yield_requirment']			= $this->input->post('yield_requirment');
	    	$data2['position']					= $this->input->post('position');	
		    $data2['loan']						= implode(",",$this->input->post('loan'));
		    $data2['yield']						= implode(",",$this->input->post('yield'));
		    $data2['property_type']				= implode(",",$this->input->post('property_type'));
		    /* current_year_update field will use in lender servey portal*/
		    $data2['current_year_update']	=date('Y-m-d');
			$data2['marriage_status']			= $this->input->post('marriage_status');
			
			$data2['e_title']					= $this->input->post('e_title');
			$data2['e_length_of_position']		= $this->input->post('e_length_of_position');


			/*
			update date-10-08-2021
			Description-Update according to task*/
			$current_position					= $this->input->post('current_position');
			$current_position 					= implode(",", $current_position);
			$data2['current_position'] 			= trim($current_position);
			$cp_title							= $this->input->post('cp_title');
			$cp_title 							= implode(",", $cp_title);
			$data2['cp_title'] 					= trim($cp_title);
			$cp_length_of_position				= $this->input->post('cp_length_of_position');
			$cp_length_of_position 				= implode(",", $cp_length_of_position);
			$data2['cp_length_of_position'] 	= trim($cp_length_of_position);
			$employment="";
			if(!empty($this->input->post('employment_first'))){
				$employment.= $this->input->post('employment_first');
				$employment.=",";
			}else{
				$employment.=",";
			}
			if(!empty($this->input->post('employment_secound'))){
				$employment.= $this->input->post('employment_secound');
			}
			$data2['employment'] 				= trim($employment);
			/*END*/

			$data2['cp_previous_position']		= $this->input->post('cp_previous_position');
			$data2['highest_year_completed']	= $this->input->post('highest_year_completed');
			$data2['year_of_graduation']		= $this->input->post('year_of_graduation');
			$data2['degree_diploma']			= $this->input->post('degree_diploma');
			$data2['financial_situation']		= $this->input->post('financial_situation');
			
			$data2['net_worth_financial_situation']	= $this->input->post('net_worth_financial_situation');
			$data2['liquid_financial_situation']	= $this->input->post('liquid_financial_situation');
			
			$data2['textarea_value']			= $this->input->post('textarea_value');
			//$data2['liquidity_needs']			= $this->input->post('liquidity_needs');
			
			$data2['liquidity_needs_checkbox_value'] = $this->input->post('liquidity_needs_checkbox_value');
			
			//$data2['invest_ex_checkbox']        = implode(',',$this->input->post('investment_experience_checkbox'));
			$invest_ex_checkbox        			= $this->input->post('investment_experience_checkbox');
			$data2['invest_ex_checkbox']        = implode(',', $invest_ex_checkbox );
			//$data2['invest_yrs_exp']       		= $this->input->post('invest_yrs_exp');
			
			$data2['investment_experience']		= $this->input->post('investment_experience');
			$data2['previous_investment_notes']	= $this->input->post('previous_investment_notes');
			$data2['other_investment_currently']= $this->input->post('other_investment_currently');
			$data2['other_investment']			= $this->input->post('other_investment');
			$data2['investment_objective']		= $this->input->post('investment_objective');


			$data2['source_income_cash']		= $this->input->post('source_income_cash');
			$data2['maximum_income']		= str_replace('$', '', str_replace(',', '', $this->input->post('maximum_income')));
			$data2['looking_invest_per_deed']		= str_replace('$', '', str_replace(',', '', $this->input->post('looking_invest_per_deed')));
			$data2['invest_per_deed']		= str_replace('$', '', str_replace(',', '', $this->input->post('invest_per_deed')));
			$data2['funded']		= str_replace(',', '', $this->input->post('funded'));
			$data2['specific']		= $this->input->post('specific');
			$data2['lender_RE_form_type']		= $this->input->post('lender_RE_form_type');
			$data2['invest_yrs_exp'] = !empty($this->input->post('invest_yrs_exp'))?implode(',', $this->input->post('invest_yrs_exp')):'';
			/*New Changes
			  update date-28-07-2021
			  Description-As per task
			*********************************Changes Start***************************************************
			*/
			$data2['other_description']					= $this->input->post('other_description');
			$data2['investment_experience_year']		= $this->input->post('investment_experience_year');
			$data2['investment_experience_month']		= $this->input->post('investment_experience_month');
			$data2['number_previous_investments']		= $this->input->post('number_previous_investments');
			$data2['other_considerations']				= $this->input->post('other_considerations');
			$data2['investment_objective_data']			= $this->input->post('investment_objective_data');
			/*********************************Changes End***************************************************/
			// insert to "lender_contact_type" table...
			$check_id2['contact_id'] 	= $this->input->post('contact_id');
			$lender_td_details = 'no';
			if($this->input->post('lender_td_details')){
				if($this->input->post('lender_td_details') == 'yes'){
					$lender_td_details = 'yes';
				}
			}
			$data2['lender_td_details'] 		= $lender_td_details;
		
				
			// Also update in lender contact
			$lender_contact_data['contact_phone']			= $this->input->post('phone');
			$lender_contact_data['contact_email']			= $this->input->post('email');
			$lender_contact_data['contact_re_date']			= $newDateString2 ? $newDateString2 :'';
			$this->User_model->updatedata('lender_contact',$check_id2,$lender_contact_data);
			
				
			$check_lender_contact_type 	= $this->User_model->select_where('lender_contact_type',$check_id2);

			if($check_lender_contact_type->num_rows() > 0)
			{
				// echo 'update to lender contact type';
				$update_id2['contact_id'] 		= $this->input->post('contact_id');
				$this->User_model->updatedata('lender_contact_type',$update_id2,$data2);

				$this->session->set_flashdata('popup_modal_hit', $this->input->post('popup_data'));
			}
			else
			{
				// echo 'insert to lender contact type';
				$data2['contact_id'] 			= $insert_id;
				$this->User_model->insertdata('lender_contact_type',$data2);
			}

		
			
		/*********** LENDER DATA : SCRIPT TO INSERT/UPDATE VALUES ENDS ************/
		
		
		/*********** CONTACT TAGS : SCRIPT TO INSERT/UPDATE VALUES STARTS ************/
			$specialty_tag_id 	= $this->input->post('specialty_tag_id');
			$email_blast_tag_id = $this->input->post('email_blast_tag_id');
				
			// insert into "contact_tags" table...
				
			$contact_specialty 	= $this->input->post('contact_specialty') ? $this->input->post('contact_specialty') : '';
				
			$contact_email_blast 	= $this->input->post('contact_email_blast') ? $this->input->post('contact_email_blast') :'' ;
			
			$user_id 	   			= $this->session->userdata('t_user_id');
				
			if($contact_specialty){
					
				foreach($contact_specialty as $k => $specialty){
						
					if($specialty_tag_id[$k]){
					
						$this->User_model->query("UPDATE contact_tags SET contact_specialty = '".$specialty."'  WHERE id = '".$specialty_tag_id[$k]."'");
						
					}else{
							
						$this->User_model->query("INSERT into contact_tags (contact_id,contact_specialty,user_id) VALUES('".$contact_id."','".$specialty."','".$user_id."')");
					}
				
				}
			}
				
			if($contact_email_blast){
				foreach($contact_email_blast as $k => $email_blast){
						
					if($email_blast_tag_id[$k]){
						$this->User_model->query("UPDATE contact_tags SET contact_email_blast = '".$email_blast."'  WHERE id = '".$email_blast_tag_id[$k]."'");
					}else{
						$this->User_model->query("INSERT into contact_tags (contact_id,contact_email_blast,user_id) VALUES('".$contact_id."','".$email_blast."','".$user_id."')");
					}
				}
			}
				
		
				
			/*********** CONTACT TAGS : SCRIPT TO INSERT/UPDATE VALUES ENDS ************/
			
			/*********** ASSIGNED : SCRIPT TO INSERT/UPDATE VALUES STARTS ************/
				
			$assigned_id = $this->input->post('assigned_id');
				
			// insert into "contact_assigned" table...
			$data12['contact_id'] 	 		= $insert_id;
			$data12['particular_user_id'] 	= $this->input->post('particular_user_id');
			$data12['contact_permission'] 	= ($this->input->post('contact_permission'))   ? serialize($this->input->post('contact_permission')) : '';
			$data12['user_id'] 	   			= $this->session->userdata('t_user_id');
				
			if($assigned_id){
					
				$this->User_model->updatedata('contact_assigned',array('id'=>$assigned_id),$data12);
				
				}else{
					
					$this->User_model->insertdata('contact_assigned',$data12);
				}
				
		/*********** ASSIGNED : SCRIPT TO INSERT/UPDATE VALUES ENDS ************/
		
		/*********** No advertisement BORROWER/Lender ************/
	
				$borrower_advertisement = $this->input->post('No_advertisement_borrower');
				$lender_advertisement   = $this->input->post('No_advertisement_lender');
				
				//if($borrower_advertisement || $lender_advertisement){
					
					$data['borrower_advertisement'] = $borrower_advertisement;
					$data['lender_advertisement']   = $lender_advertisement;
					
					$select_advertisement = $this->User_model->query("SELECT * FROM `contact_tag_extra_field` WHERE `contact_id` = '".$insert_id."'");
					if($select_advertisement->num_rows() > 0){
						
						$where['contact_id'] 	 		= $insert_id;
						$this->User_model->updatedata('contact_tag_extra_field',$where,$data);
					}else{
						
						$data['contact_id'] 	 		= $insert_id;
						$this->User_model->insertdata('contact_tag_extra_field',$data);
					}
				//}
				
				/*********** No advertisement BORROWER/Lender ************/
		
		
		if($this->input->post('modal_btn') == 'save'){
				
			$this->session->set_flashdata('success','Contact Submit Successfully!');
					
		}
		$this->session->set_flashdata('popup_modal_hit', $this->input->post('popup_data'));
		redirect(base_url().'viewcontact/'.$contact_id,'refresh');
			 
		
	}

	public function upload_new_pdfs(){

		$name	= $this->input->post('first_name').' '.$this->input->post('middle_name').' '.$this->input->post('last_name');
		$contact_idd    =  $this->input->post('contact_idd');
		$contact_id     =  $this->input->post('contact_id');
		
		// upload Contact documents....
		if($_FILES){
			$insert_id 		=  $this->input->post('contact_id');
			//first check document uploaded for borrower...
			$check_fci_query 	= $this->User_model->query("SELECT * FROM contact_document WHERE contact_id = '".$insert_id."' ");
				
			if($check_fci_query->num_rows() > 0)
			{
				$cnt = $check_fci_query->num_rows();
			}else{
				$cnt = 1;
			}
				
			$folder = 'contact_documents/'.$insert_id.'/';
			/*$folder = FCPATH.'contact_documents/'.$insert_id.'/' ;
			if(!is_dir($folder))
			{
				mkdir($folder, 0777, true);
			}*/
				
			foreach($_FILES['contact_upload_doc']['name'] as $key => $row_data){
					
				if($row_data)
				{
					$ext = strtolower(pathinfo($row_data, PATHINFO_EXTENSION));
						//die();
						
					// only docx files are allowed to uploaded!
						
					// if($ext == 'docx' || $ext == 'doc')
					if($ext == 'pdf' && $_FILES["contact_upload_doc"]["size"][$key]>=10 && $_FILES["contact_upload_doc"]["size"][$key]<=80000000)
					{
								
						// $new_name = 'borrower-'.$insert_id.'_'.$cnt++.'.'.$ext;
						//$new_name = $row_data;
						$new_name = str_replace('.'.$ext, '_'.time().'.'.$ext, $row_data);
						$target_dir 	= $folder.time().$new_name;
								
						$contact_document['contact_id']				=	$insert_id ? $insert_id :$contact_idd;
						$contact_document['name']					=	$name;
						$contact_document['contact_document']		=	$target_dir;
						$contact_document['user_id']				=	$this->session->userdata('t_user_id');
						$contact_document['document_name']          =  input_sanitize($this->input->post('document_name'));
						
						$attachmentName=$this->aws3->uploadFile($target_dir,$_FILES["contact_upload_doc"]["tmp_name"][$key]);
						//if(move_uploaded_file($_FILES["contact_upload_doc"]["tmp_name"][$key], $target_dir) === TRUE )
						if($attachmentName)
						{
							$this->User_model->insertdata('contact_document',$contact_document);
						}
					}
						
				}
			}
				
			$this->session->set_flashdata('success', ' Document uploaded!');		
		}
		
		
		// update existing document names...
			if($this->input->post('old_filename')){
				
				$old_filename 			= array_values(array_filter($this->input->post('old_filename')));
				$new_filename 			= array_values(array_filter($this->input->post('new_filename')));
				$contact_document_id 	= array_values(array_filter($this->input->post('contact_document_id')));
				
				if($contact_document_id){
					
					// first check file exist in folder or not....
					foreach($contact_document_id as $key => $old_contact_document_id){
						if ( isset($new_filename[$key])) {
							
						
							$ext = strtolower(pathinfo($old_filename[$key], PATHINFO_EXTENSION));
							$new_filenamect = str_replace('.'.$ext, '_'.time().'.'.$ext, $folder.$new_filename[$key]);

							$folder 			= 'contact_documents/'.$contact_id.'/' ;
							$old_target_dir 	= str_replace(S3_VIEW_URL, '', $old_filename[$key]); //$folder.$old_filename[$key];

							
							/*$folder 			= FCPATH.'contact_documents/'.$contact_id.'/' ;
							$old_target_dir 	= $folder.$old_filename[$key];*/
							// $new_target_dir 	= $folder.$new_filename[$key].'.'.$ext;
							$new_target_dir 	= $new_filenamect; //$folder.$new_filename[$key];
									
							
							//if(file_exists($old_target_dir)){
								$attachmentName=$this->aws3->copyObject($old_target_dir,$new_target_dir);
								if($attachmentName){
								//if(rename($old_target_dir,$new_target_dir)){
										
									// echo 'update  old filename with new file name int the "borrower_document" table...';
										
									$this->User_model->query("UPDATE contact_document SET contact_document = '".$new_target_dir."' WHERE id = '".$old_contact_document_id."' ");
								}
							/*}else{
								// echo 'no file';
							}*/
						
						}
					}
					
										
					$this->session->set_flashdata('success', ' Document File Name Updated!');
				}
				
			}

		redirect(base_url().'viewcontact/'.$insert_id,'refresh');

	}
	
	/* public function search_perticuler_contact(){
		
		$search = $this->input->post('search');
		//$sql = "Select * from contact where contact_firstname LIKE %$search% OR contact_lastname LIKE %$search% OR contact_email LIKE %$search% OR contact_phone LIKE %$search%";
		
		$sql = "Select * from contact";
		$fetch_contact = $this->User_model->query($sql);
		
		if($fetch_contact->num_rows() > 0){
			$fetch_contact = $fetch_contact->result();
			$data['fetch_contact'] = $fetch_contact;
			
		}else{
			
			$this->session->set_flashdata('error','Result Not Found!');
			redirect(base_url().'contactlist','refresh');
		}
	} */
	
	public function amount_format($n)
	{
		$n = str_replace(',','',$n);
		$a = trim($n , '$');
		$b = trim($a , ',');
		$c = trim($b , '%');
		return $c;
	}
	
	public function lender_data_print(){
		
		error_reporting(0);
		
		header("Cache-Control: ");// leave blank to avoid IE errors
			header("Pragma: ");// leave blank to avoid IE errors
			header("Content-type: application/octet-stream");
			// header("Content-type: application/vnd.ms-word");

			header("content-disposition: attachment;filename=TaliMar_$talimar_loan.doc");
		echo '<html xmlns:v="urn:schemas-microsoft-com:vml"
				xmlns:o="urn:schemas-microsoft-com:office:office"
				xmlns:w="urn:schemas-microsoft-com:office:word"
				xmlns="http://www.w3.org/TR/REC-html40">

				<head>
				<meta http-equiv=Content-Type content="text/html; charset=utf-8">
				<meta name=ProgId content=Word.Document>
				<meta name=Generator content="Microsoft Word 9">
				<meta name=Originator content="Microsoft Word 9">
				<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->
				<title>title</title>
				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				body{
					font-size:10pt;
				}
				</style>
				</head>
				<body>';

echo '<style>

		table.table{
			width:100%;
			padding : 3px 0px;
		}
		table.table th{
			text-align : left;
			font-size : 10pt;
			
		}
		table.table td{
			font-size:10pt;
			font-size : 10pt;
		
		}
		table.two_contact_signer td{
			margin : 3px;
			font-size:10pt;
		}
		h1{
			text-align:center;
		}
		h2{
			text-align:center;
		}
		p{
			font-size:10pt;
			word-spacing : 10px;
			padding : 2px 0px;
		}
		table{
			font-size:10pt;
		}
</style>';
		
		$contact_id = $this->uri->segment(2);
		//echo $contact_id;
		
		$sql = "SELECT * FROM contact WHERE contact_id ='".$contact_id."'";
		$fetch_contact = $this->User_model->query($sql);
		$fetch_contact = $fetch_contact->row();
		
		$lender_contact = "SELECT * FROM lender_contact_type WHERE contact_id='".$contact_id."'";
		$lender_contact_type = $this->User_model->query($lender_contact);	
		$lender_contact_type = $lender_contact_type->result();
			
				// echo'<pre>';
				// print_r($lender_contact_type);
				// echo'</pre>';
				
				// echo'<pre>';
				// print_r($fetch_contact);
				// echo'</pre>';
				
			$yes_no_option 			= $this->config->item('yes_no_option');
			$lender_invest_checkbox	= $this->config->item('lender_investment_experience');
				
			$leder_data_re870 .= '';
			$leder_data_re870	= '<style>
			@page Section1{
				
				margin: 1cm 1cm 1cm 1cm; 
				mso-page-orientation: portrait;
				mso-footer:f1;
			}
			
			div.Section1 { page:Section1;}
			
			div.Section1 p{
				font-size:10.5pt;
				word-spacing:5pt;
			}
			</style>';
			
			$leder_data_re870	.= '<div class="Section1">';
			
			$leder_data_re870	.= '<p>STATE of California<br>DEPARTMENT OF REAL ESTATE</p>';
			$leder_data_re870	.= '<strong>Investor Questionnaire</strong><br><span>RE 870 (Rev. 7/18)</span><hr><hr>';
		
			$leder_data_re870	.= '<table class="table" style="width:100%;border-bottom:1px solid #000;">';
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:100%;"> ☐ Initial:<span style="text-decoration:underline;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>(Date Completed)</td>';
			$leder_data_re870	.= '</tr>';
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:100%;"> ☐ Annual:<span style="text-decoration:underline;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>(Date Completed)</td>';
			$leder_data_re870	.= '</tr>';
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:100%;"> No material changes: Check here and sign the certification on page two.</td>';
			$leder_data_re870	.= '</tr>';
			$leder_data_re870	.= '</table>';
			
			$leder_data_re870	.= '<p>This questionnaire is intended to assist brokers in determining an investor’s suitability for trust deed and real property sales contract  investments as required by Business and Professions Code Section 10232.45.</p>';
			
			$leder_data_re870	.= '<p><strong>NOTE: The California Departmentof Real Estate does not endorse or offer any opinions regarding the suitability of any proposed or existing real estate investments. A guide to trust deed investments entitled “Trust Deed Investments –What You Should Know!!” (RE35) is available on the Department’sweb site at www.dre.ca.gov under the Publications menu.</strong></p>';
			
			
			$leder_data_re870	.= '<table style="background:#c1c1c1;text-align:center;border:1px solid black;width:100%;"><tr><td style="border-left:0px;border-right:0px;"><strong style="background:white;">&nbsp;&nbsp; INVESTOR &nbsp;&nbsp;</strong></td></tr></table>';
		
			$leder_data_re870	.= '<style>
			table.loan_org_table td{
				font-size : 10pt;
				font-family: Calibri, sans-serif;
				border-bottom:1px solid black;
				border-right:1px solid black;
				vertical-align:top;
				width:50%;
			}
			table.loan_org_table small{
				word-spacing :0.5pt;
				font-size : 9.5pt;
			}
			table.loan_org_table tr td:first-child{
				border-left:0px;
			}
			table.loan_org_table tr td:last-child{
				border-right:0px;
			}
			</style>';
		
		
			$leder_data_re870	.= '<table class="loan_org_table" style="width:100%;font-size:10pt;">';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="border-left:0px;">INVESTOR NAME &nbsp; '.$fetch_contact->contact_firstname .' '.$fetch_contact->contact_middlename .' '.$fetch_contact->contact_lastname .'<BR>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '<td style="border-left:0px;border-right:0px;">CO-INVESTOR NAME<BR>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td colspan="2" style="border-left:0px;border-right:0px;">If the investor is an entity, this questionnaire should be completed and signed on the entity’s behalf by the natural person with the authority to invest the entity’s funds.<BR>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td colspan="2" style="border-left:0px;border-right:0px;">NAME OF ENTITY<BR>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td colspan="2" style="border-left:0px;border-right:0px;">TYPE OF ORGANIZATION (Limited Liability Company, Retirement Plan, Pension Plan, Partnership, etc.)<BR>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="border-left:0px;">NAME OF PERSON COMPLETING THIS QUESTIONNAIRE &nbsp; '.$fetch_contact->contact_firstname .' '.$fetch_contact->contact_middlename .' '.$fetch_contact->contact_lastname .'<BR>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '<td style="border-left:0px;border-right:0px;">TITLE<BR>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '</table>';
			
			$leder_data_re870	.= '<table style="background:#c1c1c1;text-align:center;border:1px solid black;width:100%;"><tr><td style="border-left:0px;border-right:0px;"><strong style="background:white;">&nbsp;&nbsp; GENERAL INFORMATION &nbsp;&nbsp;</strong></td></tr></table>';
			
			$leder_data_re870	.= '<table class="loan_org_table" style="width:100%;font-size:10pt;">';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="border-left:0px;">ADDRESS &nbsp; '.$fetch_contact->contact_mailing_address .''.$fetch_contact->contact_mailing_unit.','.$fetch_contact->contact_mailing_city .','.$fetch_contact->contact_mailing_state .''.$fetch_contact->contact_mailing_zip .'<BR>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '<td style="border-left:0px;border-right:0px;">ADDRESS <BR>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="border-left:0px;">TELEPHONE NUMBER &nbsp; '.$fetch_contact->contact_phone .'<BR>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '<td style="border-left:0px;border-right:0px;">TELEPHONE NUMBER <BR>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="border-left:0px;">DATE OF BIRTH &nbsp; '.date('m-d-Y', strtotime($lender_contact_type[0]->lender_dob)).'<BR>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '<td style="border-left:0px;border-right:0px;">DATE OF BIRTH<BR>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '</table>';
			
			$leder_data_re870	.= '<table style="background:#c1c1c1;text-align:center;border:1px solid black;width:100%;"><tr><td style="border-left:0px;border-right:0px;"><strong style="background:white;">&nbsp;&nbsp; EMPLOYMENT INFORMATION &nbsp;&nbsp;</strong></td></tr></table>';
			
			$leder_data_re870	.= '<table class="loan_org_table" style="width:100%;font-size:10pt;">';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:25%;border-left:0px;" rowspan="2">CURRENT POSITION:<BR>&nbsp;&nbsp; '.$lender_contact_type[0]->current_position .'</td>';
			$leder_data_re870	.= '<td style="width:25%;">TITLE &nbsp; '.$lender_contact_type[0]->e_title .'<BR>&nbsp;&nbsp;<span style="text-align:center;"><small>☐ RETIRED</small></span></td>';
			$leder_data_re870	.= '<td style="width:25%;" rowspan="2">CURRENT POSITION:<BR>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '<td style="width:25%;border-right:0px;">TITLE<BR>&nbsp;&nbsp;<span style="text-align:center;"><small>☐ RETIRED</small></span></td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:25%;">LENGTH OF TIME IN POSITION &nbsp; '.$lender_contact_type[0]->e_length_of_position.'<BR>&nbsp;<center>Years</center></td>';
			$leder_data_re870	.= '<td style="width:25%;border-right:0px;">LENGTH OF TIME IN POSITION<BR>&nbsp;&nbsp;<center>Years</center></td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td colspan="2" style="border-left:0px;">PREVIOUS  POSITIONS/PROFESSIONS &nbsp; '.$lender_contact_type[0]->cp_previous_position . '<BR>&nbsp;</td>';
			$leder_data_re870	.= '<td colspan="2" style="border-left:0px;border-right:0px;">PREVIOUS  POSITIONS/PROFESSIONS<BR>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '</table>';
			
			//$leder_data_re870	.= '<br style="page-break-before: always">';
			//$leder_data_re870	.= '<table>&nbsp;<br></table>';
			
			$leder_data_re870	.= '<table style="background:#c1c1c1;text-align:center;border:1px solid black;width:100%;"><tr><td style="border-left:0px;border-right:0px;"><strong style="background:white;">&nbsp;&nbsp; EDUCATION &nbsp;&nbsp;</strong></td></tr></table>';
			
			$leder_data_re870	.= '<table class="loan_org_table" style="width:100%;font-size:10pt;">';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="border-left:0px;">HIGHEST YEAR COMPLETED &nbsp; '.$lender_contact_type[0]->highest_year_completed .'<BR>&nbsp;</td>';
			$leder_data_re870	.= '<td style="border-left:0px;border-right:0px;">HIGHEST YEAR COMPLETED<BR>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="border-left:0px;">YEAR OF GRADUATION &nbsp; '.$lender_contact_type[0]->year_of_graduation .'<BR>&nbsp;</td>';
			$leder_data_re870	.= '<td style="border-left:0px;border-right:0px;">YEAR OF GRADUATION<BR>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="border-left:0px;">DEGREE/DIPLOMA &nbsp;'.$lender_contact_type[0]->degree_diploma .'<BR>&nbsp;</td>';
			$leder_data_re870	.= '<td style="border-right:0px;border-left:0px;">DEGREE/DIPLOMA<BR>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '</table>';
			
			$leder_data_re870	.= '<table style="background:#c1c1c1;text-align:center;border:1px solid black;width:100%;"><tr><td style="border-left:0px;border-right:0px;"><strong style="background:white;">&nbsp;&nbsp; FINANCIAL SITUATION &nbsp;&nbsp;</strong></td></tr></table>';
			
			$leder_data_re870	.= '<table class="loan_org_table" style="width:100%;font-size:10pt;">';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td colspan="4" style="border-left:0px;border-right:0px;">ESTIMATED ANNUAL INCOME</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr style="border-top:0px;border-bottom:0px;">';
			if($lender_contact_type[0]->financial_situation == 1){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ Under  $50,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ Under  $50,000</td>';
			}
			if($lender_contact_type[0]->financial_situation == 2){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $50,001  to  $100,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $50,001  to  $100,000</td>';
			}
			if($lender_contact_type[0]->financial_situation == 3){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $100,001  to  $200,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $100,001  to  $200,000</td>';
			}
			if($lender_contact_type[0]->financial_situation == 4){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $200,001  to  $300,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $200,001  to  $300,000</td>';
			}
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr style="border-top:0px;border-bottom:0px;">';
			if($lender_contact_type[0]->financial_situation == 5){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $300,001 to $500,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $300,001 to $500,000</td>';
			}
			
			if($lender_contact_type[0]->financial_situation == 6){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $500,001 to $750,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $500,001 to $750,000</td>';
			}
			
			if($lender_contact_type[0]->financial_situation == 7){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $750,001 to $1,000,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $750,001 to $1,000,000</td>';
			}
			if($lender_contact_type[0]->financial_situation == 8){
			
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $1,000,001 to $5,000,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $1,000,001 to $5,000,000</td>';
				
			}
			
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr style="border-top:0px;border-bottom:0px;">';
			if($lender_contact_type[0]->financial_situation == 9){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $5,000,001 to $10,000,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $5,000,001 to $10,000,000</td>';
			}
			
			if($lender_contact_type[0]->financial_situation == 10){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ Over $10,000,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ Over $10,000,000</td>';
			}
			$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"></td>';
			$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"></td>';
			$leder_data_re870	.= '</tr>';
			
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td colspan="4" style="border-left:0px;border-right:0px;">ESTIMATED NET WORTH (Do not include the value of a principal residence, home furnishings, or automobiles.)</td>';
			$leder_data_re870	.= '</tr>';
			
			
			$leder_data_re870	.= '<tr style="border-top:0px;border-bottom:0px;">';
			if($lender_contact_type[0]->net_worth_financial_situation == 1){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ Under  $50,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ Under  $50,000</td>';
			}
			if($lender_contact_type[0]->net_worth_financial_situation == 2){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $50,001  to  $100,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $50,001  to  $100,000</td>';
			}
			if($lender_contact_type[0]->net_worth_financial_situation == 3){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $100,001  to  $200,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $100,001  to  $200,000</td>';
			}
			if($lender_contact_type[0]->net_worth_financial_situation == 4){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $200,001  to  $300,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $200,001  to  $300,000</td>';
			}
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr style="border-top:0px;border-bottom:0px;">';
			if($lender_contact_type[0]->net_worth_financial_situation == 5){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $300,001 to $500,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $300,001 to $500,000</td>';
			}
			
			if($lender_contact_type[0]->net_worth_financial_situation == 6){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $500,001 to $750,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $500,001 to $750,000</td>';
			}
			
			if($lender_contact_type[0]->net_worth_financial_situation == 7){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $750,001 to $1,000,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $750,001 to $1,000,000</td>';
			}
			if($lender_contact_type[0]->net_worth_financial_situation == 8){
			
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $1,000,001 to $5,000,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $1,000,001 to $5,000,000</td>';
				
			}
			
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr style="border-top:0px;border-bottom:0px;">';
			if($lender_contact_type[0]->net_worth_financial_situation == 9){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $5,000,001 to $10,000,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $5,000,001 to $10,000,000</td>';
			}
			
			if($lender_contact_type[0]->net_worth_financial_situation == 10){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ Over $10,000,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ Over $10,000,000</td>';
			}
			$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"></td>';
			$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"></td>';
			$leder_data_re870	.= '</tr>';
			
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td colspan="4" style="border-left:0px;border-right:0px;">ESTIMATED LIQUID ASSETS (Cash in bank, readily marketable stocks or bonds)</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr style="border-top:0px;border-bottom:0px;">';
			if($lender_contact_type[0]->liquid_financial_situation == 1){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ Under  $50,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ Under  $50,000</td>';
			}
			if($lender_contact_type[0]->liquid_financial_situation == 2){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $50,001  to  $100,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $50,001  to  $100,000</td>';
			}
			if($lender_contact_type[0]->liquid_financial_situation == 3){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $100,001  to  $200,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $100,001  to  $200,000</td>';
			}
			if($lender_contact_type[0]->liquid_financial_situation == 4){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $200,001  to  $300,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $200,001  to  $300,000</td>';
			}
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr style="border-top:0px;border-bottom:0px;">';
			if($lender_contact_type[0]->liquid_financial_situation == 5){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $300,001 to $500,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $300,001 to $500,000</td>';
			}
			
			if($lender_contact_type[0]->liquid_financial_situation == 6){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $500,001 to $750,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $500,001 to $750,000</td>';
			}
			
			if($lender_contact_type[0]->liquid_financial_situation == 7){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $750,001 to $1,000,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $750,001 to $1,000,000</td>';
			}
			if($lender_contact_type[0]->liquid_financial_situation == 8){
			
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $1,000,001 to $5,000,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $1,000,001 to $5,000,000</td>';
				
			}
			
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr style="border-top:0px;border-bottom:0px;">';
			if($lender_contact_type[0]->liquid_financial_situation == 9){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ $5,000,001 to $10,000,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ $5,000,001 to $10,000,000</td>';
			}
			
			if($lender_contact_type[0]->liquid_financial_situation == 10){
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☑ Over $10,000,000</td>';
			}else{
				$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"> ☐ Over $10,000,000</td>';
			}
			$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"></td>';
			$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;"></td>';
			$leder_data_re870	.= '</tr>';
			
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td colspan="4" style="border-left:0px;border-right:0px;">SOURCE OF INCOME AND CASH RESOURCES<br> '.$lender_contact_type[0]->textarea_value .'<br><br>&nbsp;&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '</table>';
			
			$leder_data_re870	.= '<table style="background:#c1c1c1;text-align:center;border:1px solid black;width:100%;"><tr><td style="border-left:0px;border-right:0px;"><strong style="background:white;">&nbsp;&nbsp; FINANCIAL SITUATION (continued) &nbsp;&nbsp;</strong></td></tr></table>';
			
			$leder_data_re870	.= '<table class="loan_org_table" style="width:100%;font-size:10pt;">';
			
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:100%;border-left:0px;border-right:0px;border-bottom:0px;" colspan="2">LIQUIDITY NEEDS –Select one of the following:</td>';
			$leder_data_re870	.= '</tr>';
			
			if($lender_contact_type[0]->liquidity_needs_checkbox_value == 1){
				$leder_data_re870	.= '<tr>';
				$leder_data_re870	.= '<td style="width:100%;border-left:0px;border-right:0px;border-bottom:0px;" colspan="2"> ☑ Primary need is liquidity/cash</td>';
				$leder_data_re870	.= '</tr>';
			}else{
				$leder_data_re870	.= '<tr>';
				$leder_data_re870	.= '<td style="width:100%;border-left:0px;border-right:0px;border-bottom:0px;" colspan="2"> ☐ Primary need is liquidity/cash</td>';
				$leder_data_re870	.= '</tr>';
			}
			
			if($lender_contact_type[0]->liquidity_needs_checkbox_value == 2){
				$leder_data_re870	.= '<tr>';
				$leder_data_re870	.= '<td style="width:100%;border-left:0px;border-right:0px;border-bottom:0px;" colspan="2"> ☑ Need some liquidity for possible quick access to cash</td>';
				$leder_data_re870	.= '</tr>';
			}else{
				$leder_data_re870	.= '<tr>';
				$leder_data_re870	.= '<td style="width:100%;border-left:0px;border-right:0px;border-bottom:0px;" colspan="2"> ☐ Need some liquidity for possible quick access to cash</td>';
				$leder_data_re870	.= '</tr>';	
			}
			
			if($lender_contact_type[0]->liquidity_needs_checkbox_value == 3){
				$leder_data_re870	.= '<tr>';
				$leder_data_re870	.= '<td style="width:100%;border-left:0px;border-right:0px;" colspan="2"> ☑ No liquidity needed; have other sources of cash</td>';
				
				$leder_data_re870	.= '</tr>';
			}else{
				$leder_data_re870	.= '<tr>';
				$leder_data_re870	.= '<td style="width:100%;border-left:0px;border-right:0px;" colspan="2"> ☐ No liquidity needed; have other sources of cash</td>';
				
				$leder_data_re870	.= '</tr>';
				
			}
			
			$leder_data_re870	.= '</table>';
			
			$leder_data_re870	.= '<table style="background:#c1c1c1;text-align:center;border:1px solid black;width:100%;"><tr><td style="border-left:0px;border-right:0px;"><strong style="background:white;">&nbsp;&nbsp; INVESTMENT EXPERIENCE &nbsp;&nbsp;</strong></td></tr></table>';
			
			$leder_data_re870	.= '<table class="loan_org_table" style="width:100%;font-size:10pt;">';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td colspan="3" style="border-left:0px;border-right:0px;">Report investment experience in years:</td>';
			$leder_data_re870	.= '</tr>';
			
			if($lender_contact_type[0]->invest_ex_checkbox == 1){
				$leder_data_re870	.= '<tr>';
				$leder_data_re870	.= '<td style="width:33%;border-left:0px;border-right:0px;" colspan="3"> ☑ No investment experience</td>';
				$leder_data_re870	.= '</tr>';
			}else{
				$leder_data_re870	.= '<tr>';
				$leder_data_re870	.= '<td style="width:33%;border-left:0px;border-right:0px;" colspan="3"> ☐ No investment experience</td>';
				$leder_data_re870	.= '</tr>';
			}
			
			$leder_data_re870	.= '<tr>';
			if($lender_contact_type[0]->invest_ex_checkbox == 2){
				$leder_data_re870	.= '<td style="width:33%;border-left:0px;"> ☑ Mutual Funds</td>';
			}else{
				$leder_data_re870	.= '<td style="width:33%;border-left:0px;"> ☐ Mutual Funds</td>';
			}
			if($lender_contact_type[0]->invest_ex_checkbox == 5){
				$leder_data_re870	.= '<td style="width:33%;border-left:0px;"> ☑ Annuities</td>';
			}else{
				$leder_data_re870	.= '<td style="width:33%;border-left:0px;"> ☐ Annuities</td>';
			}
			if($lender_contact_type[0]->invest_ex_checkbox == 8){
				$leder_data_re870	.= '<td style="width:34%;border-left:0px;border-right:0px;"> ☑ Bonds</td>';
			}else{
				$leder_data_re870	.= '<td style="width:34%;border-left:0px;border-right:0px;"> ☐ Bonds</td>';
			}
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			if($lender_contact_type[0]->invest_ex_checkbox == 3){
				$leder_data_re870	.= '<td style="width:33%;border-left:0px;"> ☑ Stocks/Shares</td>';
			}else{
				$leder_data_re870	.= '<td style="width:33%;border-left:0px;"> ☐ Stocks/Shares</td>';
			}
			if($lender_contact_type[0]->invest_ex_checkbox == 6){
				$leder_data_re870	.= '<td style="width:33%;border-left:0px;"> ☑ Notes </td>';
			}else{
				$leder_data_re870	.= '<td style="width:33%;border-left:0px;"> ☐ Notes </td>';
			}
			
			if($lender_contact_type[0]->invest_ex_checkbox == 9){
				$leder_data_re870	.= '<td style="width:34%;border-left:0px;border-right:0px;"> ☑ Options </td>';
			}else{
				$leder_data_re870	.= '<td style="width:34%;border-left:0px;border-right:0px;"> ☐ Options </td>';
			}
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			if($lender_contact_type[0]->invest_ex_checkbox == 4){
				$leder_data_re870	.= '<td style="width:33%;border-left:0px;"> ☑ Real Estate</td>';
			}else{
				$leder_data_re870	.= '<td style="width:33%;border-left:0px;"> ☐ Real Estate</td>';
			}
			
			if($lender_contact_type[0]->invest_ex_checkbox == 7){
				$leder_data_re870	.= '<td style="width:33%;border-left:0px;"> ☑ Other (specify):</td>';
			}else{
				$leder_data_re870	.= '<td style="width:33%;border-left:0px;"> ☐ Other (specify):</td>';
			}
			$leder_data_re870	.= '<td style="width:34%;border-left:0px;border-right:0px;"></td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:100%;border-left:0px;border-right:0px;" colspan="3">INVESTMENT EXPERIENCE IN NOTES SECURED BY TRUST DEEDS OR REAL PROPERTY SALES CONTRACTS<br>&nbsp;&nbsp;<span style="text-decoration:underline;">&nbsp;'.$lender_contact_type[0]->investment_experience .'&nbsp;</span>Years<span style="text-decoration:underline;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Months</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:100%;border-left:0px;border-right:0px;" colspan="3">NUMBER OF PREVIOUS INVESTMENTS IN NOTES SECURED BY TRUST DEEDS OR REAL PROPERTY SALES CONTRACTS<br>&nbsp;&nbsp;&nbsp;'.$lender_contact_type[0]->previous_investment_notes .'</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:100%;border-left:0px;border-right:0px;" colspan="3">OTHER INVESTMENTS CURRENTLY HELD BY THE INVESTOR<br>&nbsp;&nbsp; '.$lender_contact_type[0]->other_investment_currently .'<br>&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '</table>';
			
			//$leder_data_re870	.= '<br style="page-break-before: always">';
			//$leder_data_re870	.= '<table>&nbsp;<br></table>';
			
			$leder_data_re870	.= '<table style="background:#c1c1c1;text-align:center;border:1px solid black;width:100%;"><tr><td style="border-left:0px;border-right:0px;"><strong style="background:white;">&nbsp;&nbsp; OTHER CONSIDERATIONS &nbsp;&nbsp;</strong></td></tr></table>';
			
			$leder_data_re870	.= '<table class="loan_org_table" style="width:100%;font-size:10pt;">';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td colspan="2" style="border-left:0px;border-right:0px;">OTHER EDUCATIONAL, BUSINESS, OR FINANCIAL EXPERIENCES, INVESTMENT CONSIDERATIONS, FINANCIAL SITUATIONS, TRAINING (INCLUDING SEMINARS, CONTINUING EDUCATION, ETC.), OR PROFESSIONAL LICENSES AND CERTIFICATIONS. <br>&nbsp;&nbsp; '.$lender_contact_type[0]->other_investment .'<br><br>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			$leder_data_re870	.= '</table>';
			
			$leder_data_re870	.= '<table style="background:#c1c1c1;text-align:center;border:1px solid black;width:100%;"><tr><td style="border-left:0px;border-right:0px;"><strong style="background:white;">&nbsp;&nbsp; INVESTMENT  OBJECTIVE&nbsp;&nbsp;</strong></td></tr></table>';
			
			$leder_data_re870	.= '<table class="loan_org_table" style="width:100%;font-size:10pt;">';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td colspan="2" style="border-left:0px;border-right:0px;">OBJECTIVE OF INVESTOR(S) FOR PURCHASING OR INVESTING IN TRUST DEEDS OR REAL PROPERTY SALES CONTRACTS<br>&nbsp; '.$lender_contact_type[0]->investment_objective .' <br><br>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			$leder_data_re870	.= '</table>';
			
		
			$leder_data_re870	.= '<table style="background:#c1c1c1;text-align:center;border:1px solid black;width:100%;"><tr><td style="border-left:0px;border-right:0px;"><strong style="background:white;">&nbsp;&nbsp; INVESTOR ACKNOWLEDGEMENT &nbsp;&nbsp;</strong></td></tr></table>';
			
			$leder_data_re870	.= '<table class="loan_org_table" style="width:100%;font-size:10pt;">';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td colspan="4" style="border-left:0px;border-right:0px;"><center>I certify(or  declare)that the information provided in this questionnaire is true and correct to the best of my knowledge. I am providingthis information for the purpose of determining whether or not trust deed or real property sales contract investments are suitable for me. I understand that the broker may request an annual update of this information and that the broker may request additional information regarding my suitability as an investor for each specific transaction. I acknowledge that investments in notes secured by trust deeds and real property sales contracts are subject to risk of lossof principal and monthly income.</center><br>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:25%;border-left:0px;">INVESTOR SIGNATUR<br>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '<td style="width:25%;border-left:0px;">DATE<br>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '<td style="width:25%;border-left:0px;">CO-INVESTOR SIGNATURE<br>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;">DATE<br>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '</table>';
			
			
			$leder_data_re870	.= '<table style="background:#c1c1c1;text-align:center;border:1px solid black;width:100%;"><tr><td style="border-left:0px;border-right:0px;"><strong style="background:white;">&nbsp;&nbsp; BROKER ACKNOWLEDGEMENT &nbsp;&nbsp;</strong></td></tr></table>';
			
			$leder_data_re870	.= '<table class="loan_org_table" style="width:100%;font-size:10pt;">';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td colspan="4" style="border-left:0px;border-right:0px;"><center>I certify (or declare) that I have reviewed this completed questionnaire and that I will use the information herein to a id in meeting my responsibility to make reasonable efforts to determine that proposed investments are suitable and appropriate for the investor(s) above based on the information provided.</center><br>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:25%;border-left:0px;">NAME OF BROKER<br>&nbsp;&nbsp; Brockton G. VandenBerg</td>';
			$leder_data_re870	.= '<td style="width:25%;border-left:0px;">LICENSE ID NUMBER<br>&nbsp;&nbsp; 01851415</td>';
			$leder_data_re870	.= '<td style="width:25%;border-left:0px;">BROKER’S REPRESENTATIVE<br>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '<td style="width:25%;border-right:0px;border-left:0px;">LICENSE ID NUMBER<br>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:75%;border-right:0px;border-left:0px;" colspan="3">BROKER OR DESIGNATED REPRESENTATIVE SIGNATURE<br>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '<td style="width:25%;border-right:0px;">DATE<br>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:100%;border-left:0px;border-right:0px;" colspan="4"><center>A broker shall maintain records of the information used to determine that an investment is suitable and appropriate for each investor/purchaser for at least four years.</center><br>&nbsp;&nbsp;</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '</table>';
			
			$leder_data_re870	.= '</div>'; 
			
			//$leder_data_re870	.= '<br style="page-break-before: always">';
			
			echo $leder_data_re870;
			
	}
	
	public function contact_document_read(){
		
		// fetch filename from "contact_document" table...
		$sql = $this->User_model->query("SELECT * FROM contact_document WHERE id = '".$this->uri->segment(3)."' ");
		$fetch_name = $sql->row();
		$filename = $fetch_name->contact_document;
												 
		// Let the browser know that a PDF file is coming.
		header("Content-type: application/pdf");

		header("Content-Length: " . filesize('contact_document-'.$filename));

												 
		// Send the file to the browser.

		readfile($filename);

		exit; 
	}
	
	
	public function contact_notes($contact_id){
		error_reporting(0);
		
		/* if($_POST['submit'] == 'Update'){				// update saved notes
			
			$notes_id 			= $this->input->post('notes_id');
			$notes_date 		= $this->input->post('fetch_notes_date');
			$notes_type 		= $this->input->post('fetch_notes_type');
			$loan_id 			= $this->input->post('fetch_loan_id');
			$personal_notes 	= $this->input->post('fetch_personal_notes');
			
		
			foreach($notes_id as $key => $id){
				
				
				$myDateTime1 	= DateTime::createFromFormat('m-d-Y', $notes_date[$key]);
				$newDateString1 = $myDateTime1->format('Y-m-d');
				
				$data['contact_id'] 		= $contact_id;
				$data['notes_date'] 		= $newDateString1;
				$data['notes_type'] 		= $notes_type[$key];
				$data['loan_id'] 			= $loan_id[$key];
				$data['	notes_text'] 		= $personal_notes[$key];
				$data['user_id'] 			= $this->session->userdata('t_user_id');
			
				$this->User_model->updatedata('contact_notes',array('id' => $id),$data);
				$this->session->set_flashdata('success','Notes Updated Successfully!');
			}

			
		}else{ */											// insert new notes
			
		
			$notes 				= $this->input->post('notes');
			$notes_date 		= $this->input->post('notes_date');
			$notes_type 		= $this->input->post('notes_type');
			$purpose_type 		= $this->input->post('purpose_type');
			$loan_id 			= $this->input->post('loan_id');
			$personal_notes 	= $this->input->post('personal_notes');
			
		     
			foreach($notes as $key => $note){
				
				if($notes_date[$key]){
					
				//$myDateTime1 	= DateTime::createFromFormat('m-d-Y', $notes_date[$key]);
				//$newDateString1 = $myDateTime1->format('Y-m-d');
				$newDateString1 = input_date_format($notes_date[$key]);
				
				}else{
					$newDateString1 = '';
				}
				$data['contact_id'] 		= $contact_id;
				$data['notes_date'] 		= $newDateString1;
				$data['notes_type'] 		= $notes_type[$key];
				$data['purpose_type'] 		= $purpose_type[$key];
				$data['loan_id'] 			= $loan_id[$key];
				$data['	notes_text'] 		= $personal_notes[$key];
				$data['user_id'] 			= $this->session->userdata('t_user_id');
			
				$this->User_model->insertdata('contact_notes',$data);
				
			}
			$this->session->set_flashdata('success','Notes Added Successfully!');
		// }
			redirect(base_url().'viewcontact/'.$contact_id,'refresh');
	}
	
	
	
	public function GetcontactNotes(){
		
		$noteId = $this->input->post('noteId');
		$sql_notes = $this->User_model->query("SELECT id, contact_id, notes_date, notes_type, purpose_type, notes_text FROM contact_notes WHERE id = '".$noteId."' ");
		$fetch_contact_notes = $sql_notes->row();
		
		echo json_encode($fetch_contact_notes);
	}
	
	public function UPdateNotes(){
		
		if(isset($_POST['upnote'])){
			
			//$date = DateTime::createFromFormat('m-d-Y', $this->input->post('notedate'));
			$dateP = input_date_format($this->input->post('notedate'));
			$start_date1 = $dateP ? $dateP : date('Y-m-d');
			
			$contact_id 		= $this->input->post('contact_id');
			$where['id'] 		= $this->input->post('updateid');
			$data['notes_date'] = $start_date1;
			$data['notes_type'] = $this->input->post('notetype');
			$data['purpose_type'] = $this->input->post('purpose_type');
			$data['notes_text'] = $this->input->post('notetext');
			
			
			$this->User_model->updatedata('contact_notes',$where,$data);
			$this->session->set_flashdata('success','Notes Updated Successfully!');
			redirect(base_url().'viewcontact/'.$contact_id,'refresh');
		}
	}
	
	public function fetch_notes(){
		
		$note_id = $this->input->post('id');
		
		//fetch notes on the basis of id from "contact_notes" table....
		$sql_notes = $this->User_model->query("SELECT * FROM contact_notes WHERE id = '".$note_id."' ");
			
		$fetch_contact_notes = $sql_notes->row();
		
		$output['id'] 			= $fetch_contact_notes->id;
		$output['contact_id'] 	= $fetch_contact_notes->contact_id;
		$output['notes_date']	= date('m-d-Y',strtotime($fetch_contact_notes->notes_date));
		
		switch($fetch_contact_notes->notes_type){
			case 1:$notes_type_val = 'Email';
				break;
			case 2:$notes_type_val = 'Phone';
				break;
			case 3:$notes_type_val = 'Letter';
				break;
			case 4:$notes_type_val = 'Meeting';
				break;
			case 5:$notes_type_val = 'Other';
				break;
			case 6:$notes_type_val = 'POF Letter';
				break;
			case 7:$notes_type_val = 'Executive Summary';
				break;
			case 8:$notes_type_val = 'Loan Request';
				break;
			case 9:$notes_type_val = 'Networking Event';
				break;
			case 10:$notes_type_val = 'Loan Input Form';
				break;
			default:
				$notes_type_val = 'None';
		}

		
		
		$output['notes_type'] 	= $fetch_contact_notes->notes_type;
		$output['notes_type_name'] 	= $notes_type_val;
		
		if($fetch_contact_notes->loan_id){
			
		//fetch 'talimar loan number' from "loan" table...
		$sql_loan_id = $this->User_model->query("SELECT * FROM loan WHERE id = '".$fetch_contact_notes->loan_id."' ");
		$fetch_talimar_loan = $sql_loan_id->row();
		
	
		$output['talimar_loan'] = $fetch_talimar_loan->talimar_loan;
		}else{
			
			$output['talimar_loan'] = '';
		}
		$output['loan_id'] 		= $fetch_contact_notes->loan_id;
		$output['notes_text'] 	= $fetch_contact_notes->notes_text;
		$output['purpose_type'] 	= $fetch_contact_notes->purpose_type;
		
		print_r(json_encode($output));
	
	}
	
	Public function history_filter(){
	
		 // echo "<pre>";
			// print_r($_POST);
		// echo "</pre>";
	 	
		$action = $this->input->post('action');
		
		if($action == 'edit-note'){
			
			
			$notes_id 			= $this->input->post('fetch_notes_id');
			$contact_id 		= $this->input->post('fetch_contact_id');
			$notes_date 		= $this->input->post('fetch_notes_date');
			// $notes_type 		= $this->input->post('fetch_notes_type');
			$notes_type 		= $this->input->post('fetch_notes_type') ? $this->input->post('fetch_notes_type') : $this->input->post('fetch_notes_type_id');
			$loan_id 			= $this->input->post('fetch_talimar_loan') ?  $this->input->post('fetch_talimar_loan')  :  $this->input->post('fetch_loan_id') ;
			$personal_notes 	= $this->input->post('fetch_personal_notes');
			
		
			//foreach($notes_id as $key => $id){
				
				
				//$myDateTime1 	= DateTime::createFromFormat('m-d-Y', $notes_date);
				//$newDateString1 = $myDateTime1->format('Y-m-d');
				$newDateString1 = input_date_format($notes_date);
				
				// $data['contact_id'] 		= $contact_id;
				$data['notes_date'] 		= $newDateString1;
				$data['notes_type'] 		= $notes_type;
				$data['loan_id'] 			= $loan_id;
				$data['notes_text'] 		= $personal_notes;
				$data['user_id'] 			= $this->session->userdata('t_user_id');
			
				$this->User_model->updatedata('contact_notes',array('id' => $notes_id),$data);
				$this->session->set_flashdata('success','Notes Updated Successfully!');
				redirect(base_url().'viewcontact/'.$contact_id,'refresh');
			 //}

			
			
		}
		
		if($action == 'delete-note'){
			$id 			= $this->input->post('selection_note_id');
			$contact_id 	= $this->input->post('selection_contact_id');
			
			
			 if($id){
			
				$this->User_model->query("DELETE  FROM contact_notes WHERE id = '".$id."' ");
				$this->session->set_flashdata('success','Note deleted Successfully!');
				redirect(base_url().'viewcontact/'.$contact_id,'refresh');
			
			}else{
				
				$this->session->set_flashdata('error','Note not  deleted !');
				redirect(base_url().'viewcontact/'.$contact_id,'refresh');
			
			}
			
		}
	}
	
/* 	public function delete_notes($note_id,$contact_id){
			$this->User_model->query("DELETE  FROM contact_notes WHERE id = '".$note_id."' ");
			$this->session->set_flashdata('success','Note deleted Successfully!');
			redirect(base_url().'viewcontact/'.$contact_id,'refresh');
			
	} */
	
	
	public function add_contact_task(){ 
		
		$contact_id 				= $this->input->post('contact_id');
		$contact_task 				= $this->input->post('contact_task');
		$contact_date 				= $this->input->post('contact_date');
		$contact_status 			= $this->input->post('contact_status');
		$contact_user	 			= $this->input->post('contact_user');
		$task_notes					= $this->input->post('add_task_note');
		
		
		if($contact_date){
		
			//$myDateTime 				= DateTime::createFromFormat('m-d-Y', $contact_date);
			//$newDateString 				= $myDateTime->format('Y-m-d');
			$newDateString 				= input_date_format($contact_date);
		
			$data['contact_date'] 		= $newDateString ? $newDateString : '';
		}
		
		$data['contact_id']				= $contact_id; 
		$data['contact_task']			= $contact_task;  
		$data['contact_status']			= $contact_status; 
		$data['contact_user']			= $contact_user; 
		$data['t_user_id'] 				= $this->session->userdata('t_user_id');
		$data['task_notes']				= $task_notes;
		
			$this->User_model->insertdata('contact_tasks',$data);
			$this->session->set_flashdata('success','Contact Task Added Successfully!');
			redirect(base_url().'viewcontact/'.$contact_id,'refresh');
		
	}
	
	public function edit_contact_task(){
		
		$row_id 					= $this->input->post('row_id');
		$contact_id 				= $this->input->post('contact_id');
		$contact_task 				= $this->input->post('contact_task');
		$contact_date 				= $this->input->post('contact_date');
		$contact_status 			= $this->input->post('contact_status');
		$edit_task_notes			= $this->input->post('edit_task_notes');
		
		if($contact_date){
		
			//$myDateTime 				= DateTime::createFromFormat('m-d-Y', $contact_date);
			//$newDateString 				= $myDateTime->format('Y-m-d');
			$newDateString 				= input_date_format($contact_date);
		
			$data['contact_date'] 		= $newDateString ? $newDateString : '';
		}
		
		$data['id']						= $row_id; 
		$data['contact_id']				= $contact_id; 
		$data['contact_task']			= $contact_task;  
		$data['contact_status']			= $contact_status; 
		$data['task_notes']				= $edit_task_notes;
		
		$this->User_model->updatedata('contact_tasks',array('id' => $row_id),$data);
		$this->session->set_flashdata('success','Contact Task Updated Successfully!');
		redirect(base_url().'viewcontact/'.$contact_id,'refresh');
		
	}
	
	public function delete_document()
	{
		$id = $this->input->post('id');
		
		$fetch_document = $this->User_model->select_where('contact_document',array('id'=>$id));
		$fetch_document = $fetch_document->result();
		if($this->aws3->deleteObject($fetch_document[0]->contact_document)){
			$delete = $this->User_model->delete('contact_document',array('id'=>$id));
		}

		echo 1;
		
	}
	
	public function delete_tasks()
	{
		$id = $this->input->post('id');

		$contactDocument = $this->User_model->query("SELECT * FROM contact_document WHERE id = '$id'");
		$contactDocumentData = $contactDocument->row();
		$fileName = $contactDocumentData->contact_document;		
		$this->aws3->deleteObject($fileName);

		$delete = $this->User_model->delete('contact_tasks',array('id'=>$id));
		echo 1;
		
	}
	
	public function contact_relation(){
		
		
		$contact_id 				= $this->input->post('contact_iddd');
		$relationship_contact_id 	= $this->input->post('relationship');
		$relation_option		 	= $this->input->post('relation_option');
		$relationship_note		 	= $this->input->post('relationship_note');
		if(!empty($this->input->post('show_lender_portal')) && $this->input->post('show_lender_portal')>0){
			$show_lender_portal=1;
		}else{
			$show_lender_portal=0;
		}
		$data['contact_id'] 				= $contact_id;
		$data['relationship_contact_id'] 	= $relationship_contact_id;
		$data['relation_option'] 			= $relation_option;
		$data['relationship_note'] 			= $relationship_note;
		$data['show_lender_portal'] 		= $show_lender_portal;
		$data['user_id'] 					= $this->session->userdata('t_user_id');
		
		
		// first check relationship exist or not...
			
		$sql = $this->User_model->query("SELECT * FROM contact_relationship WHERE relationship_contact_id = '".$relationship_contact_id."' AND contact_id  = '".$contact_id."' ");
		
			
		if($sql->num_rows() > 0)
		{
			$this->session->set_flashdata('error','Relationship already Added!');
			
		}else{
				
			// insert into contact_relation table...
			
			$this->User_model->insertdata('contact_relationship',$data);
			
			$this->session->set_flashdata('success','Relationship added Successfully!');
		}
		
		redirect(base_url().'viewcontact/'.$contact_id,'refresh');
	}
	
//..........................my function start...................//

 public function contact_relationship_delete(){
	
	
	$contact_id  		= $this->input->post('contact_id');
	$id 				= $this->input->post('id');
  
   //delete from contact_relationship table...
		$this->User_model->query("DELETE FROM contact_relationship WHERE id = '".$id."' ");
			
		$this->session->set_flashdata('success','Relationship Deleted Successfully!');
			
		redirect(base_url().'viewcontact/'.$contact_id,'refresh');

}

//..........................my function end...................//


	public function edit_contact_relationship(){
	
		/*  echo "<pre>";
			print_r($_POST);
		echo "</pre>";  */
		$action  					= $this->input->post('relationship_action');
		$relationship_id  			= $this->input->post('relationship_id');
		$relationship_contact_id  	= $this->input->post('relationship_contact_id');
		$contact_id  				= $this->input->post('contact_id');

		
		if($action == 'edit'){
			
			//update value of relationship_id in "contact_relationship" table...
			$data['relationship_contact_id']	= $relationship_contact_id;
			$data['$relationship_note'] 		= $this->input->post('relationship_note');
			$this->User_model->updatedata('contact_relationship',array('contact_id' => $contact_id,'id' => $relationship_id),$data);
			$this->session->set_flashdata('success','Relationship Updated Successfully!');
			redirect(base_url().'viewcontact/'.$contact_id,'refresh');
		}
		
		if($action == 'delete'){
			
			// $relationship_id['id'] = $relationship_id;
			
			//delete from contact_relationship table...
			$this->User_model->query("DELETE FROM contact_relationship WHERE id = '".$relationship_id."' ");
			
			$this->session->set_flashdata('success','Relationship Deleted Successfully!');
			
			redirect(base_url().'viewcontact/'.$contact_id,'refresh');
			
		}
	}
	

	
	/*
	Description : This function use for delete contact user details - loan profile tags and all of this data.
	Author      : Bitcot
	Created     : 
	Modified    : 19-03-2021
	Modified Description : This function has old working to use delete user on page load. modification of this function I have used ajax 						based.
	*/
	public function delete_contact()
	{
		
		if($this->input->post('contact_id'))
		{
			$contact_id['contact_id'] = $this->input->post('contact_id');
			
			// delete from contact
			$this->User_model->delete('contact',$contact_id);
			
			// delete from borrower_contact_type
			$this->User_model->delete('borrower_contact_type',$contact_id);
			
			// delete from borrower_contact
			$this->User_model->delete('borrower_contact',$contact_id);
			
			// delete from lender_contact_type
			$this->User_model->delete('lender_contact_type',$contact_id);
			
			// delete from lender_contact
			$this->User_model->delete('lender_contact',$contact_id);
			
			// delete from contact_tags
			$this->User_model->delete('contact_tags',$contact_id);
			
			// delete from contact_notes
			$this->User_model->delete('contact_notes',$contact_id);
			
			// delete from contact_relationship
			$this->User_model->delete('contact_relationship',$contact_id);
			
			if(!empty($this->input->post('ajax_call'))){
				echo "success";
			}else{
				$this->session->set_flashdata('success',' Deleted');
				redirect(base_url().'contactlist','refresh');
			}		
			
		}
		else
		{
			if(!empty($this->input->post('ajax_call'))){
				echo "error";
			}else{
				$this->session->set_flashdata('error',' Something went wrong');
				redirect(base_url().'contactlist','refresh');
			}
			
			
		}
		
	}
	
	
	public function updates_contacts_data()
	{
		$fetch_lender_contact = $this->User_model->select_star('lender_contact');
		$fetch_lender_contact = $fetch_lender_contact->result();
		foreach($fetch_lender_contact as $row)
		{
			$fetch_contact = $this->User_model->select_where('contact',array('contact_id' => $row->contact_id));
			if($fetch_contact->num_rows() > 0)
			{
				$fetch_contact 				= $fetch_contact->result();
				$update_d['contact_phone']  = $fetch_contact[0]->contact_phone;
				$update_d['contact_email']  = $fetch_contact[0]->contact_email;
				
				$fetch_lender_contact_type = $this->User_model->select_where('lender_contact_type',array('contact_id' => $row->contact_id));
				if($fetch_lender_contact_type->num_rows() > 0)
				{
					$fetch_lender_contact_type = $fetch_lender_contact_type->result();
					$update_d['contact_re_date'] = $fetch_lender_contact_type[0]->lender_RE_date;
				}
				$this->User_model->updatedata('lender_contact',array('contact_id' => $row->contact_id),$update_d);
			}
		}
		
	}
	
	public function check_date_format($d)
	{
		// date format should be m-d-Y
		if (preg_match("/^(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])-[0-9]{4}$/",$d))
		{
			return 1;
		}
		else
		{
		   return 0;
		}
	}
	
	
	
	public function check_duplicacy(){
		
		
		/* $value = $this->input->post('value');
		$field = $this->input->post('field');
		 */
		 
		 $first_name 	= $this->input->post('first_name');
		 $last_name 	= $this->input->post('last_name');
		 $email 		= $this->input->post('email');
		 $contact_id 	= $this->input->post('contact_id');
		
		// check duplicacy in "contact" table...
		
		// $sql = $this->User_model->query("SELECT * FROM contact WHERE ".$field."  =  '".$value."' ");
		// if($first_name && $email){
			
		if(empty($contact_id))
		{
		 	$sql = $this->User_model->query("SELECT * FROM contact WHERE contact_email = '".$email."' ");
		}
		else
		{
			$sql = $this->User_model->query("SELECT * FROM contact WHERE contact_email = '".$email."' AND contact_id != '".$contact_id."' ");
		}
		
		// echo "SELECT * FROM contact WHERE contact_firstname =  '".$first_name."' AND contact_email = '".$email."'";
		
		if($sql->num_rows() > 0){
			echo '0';
		}else{
			echo '1';
		}
	}
	
	public function print_all_task(){

		error_reporting(0);
		//echo 'anjul error';
		$selct_contact_task = $this->config->item('selct_contact_task');
		$t_user_id = $this->session->userdata('t_user_id');
		$current_date = date('Y-m-d');
		$task_type=$this->input->post('selct_task');

		// $task_status=$this->input->post('selct_task');

		// if($task_status){

		// 	$con_12 =" AND ct.contact_status='".$task_status."'";
			
		// }else{

		// 	$con_12='';

		// }


		if($task_type){

			$con_1 =" AND ct.contact_task='".$task_type."'";
			
		}else{

			$con_1='';

		}

		if($this->input->post('uri_1') && $this->input->post('uri_2')){
		
		$sql = $this->User_model->query("SELECT * FROM `contact_tasks` as ct JOIN contact as c ON ct.contact_id = c.contact_id WHERE ct.`t_user_id`='".$t_user_id."' AND ct.contact_status = '3' AND ct.contact_date <= '".$current_date."' AND ct.contact_id='".$this->input->post('uri_1')."' AND ct.id='".$this->input->post('uri_2')."' ORDER BY ct.contact_date ASC");	

		}
		else{

		$sql = $this->User_model->query("SELECT * FROM `contact_tasks` as ct JOIN contact as c ON ct.contact_id = c.contact_id WHERE ct.`t_user_id`='".$t_user_id."' AND ct.contact_status = '3' AND ct.contact_date <= '".$current_date."' ".$con_1." ORDER BY ct.contact_date ASC");	
			// $sql = $this->User_model->query("SELECT * FROM `contact_tasks` as ct JOIN contact as c ON ct.contact_id = c.contact_id WHERE ct.`t_user_id`='".$t_user_id."' ".$con_12." AND ct.contact_date <= '".$current_date."' ".$con_1." ORDER BY ct.contact_date ASC");	
			
			}
		
		$check_sql = $sql->result();
		

			$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);

			// set document information
			$pdf->SetCreator(PDF_CREATOR);
			// $pdf->SetAuthor('Nicola Asuni');
			// $pdf->SetTitle('TCPDF Example 004');
			// $pdf->SetSubject('TCPDF Tutorial');
			// $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

			// set default header data
			 // $pdf->SetHeaderData("include/icons/logo-light.png", 30, '', '');

			// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			// set margins
			$pdf->SetMargins(2, 20, 2);
			// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
			$pdf->SetFont('times', '', 7);

			// add a page
			$pdf->AddPage('L', 'A4');
			// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
			$header ='<h1 style="color:#003468;font-size:18px;"> Today’s Task: '.date('m-d-Y').'</h1></br>';
			
			$header .= '<style>
			.table {
				width: 100%;
				max-width: 100%;
				margin-bottom: 20px;
				border-collapse:collapse;
			}
			.table-bordered {
			border: 1px solid #ddd;
			}
			table {
				border-spacing: 0;
				border-collapse: collapse;
			}
			
			.th_text_align_center th {
				text-align: left;
			}
			.th_text_align_center td {
				text-align: left;
			}
			
			.td_large{
				width:17%;
			}
			.small_td{
				width:4%;
			}
			
			.td_medium{
				width:5%;
			}
			tr.dark_border th{
				border: 1px solid gray;
			}
			.table td{
				font-size:12px;
				font-family: "Times New Roman", Georgia, Serif;
				height:25px;
			}
			.table th{
				height : 20px;
				font-size : 12px;
			}
			
			tr.table_header th{
				font-size:12px;
				font-family: "Times New Roman", Georgia, Serif;
				text-align:center;
				background-color:#bfcfe3;
				
			}
			
			tr.table_bottom th{
				font-size:12px;
				font-family: "Times New Roman", Georgia, Serif;
				text-align:left;
				background-color:#bfcfe3;
			}
			tr.odd td{
				background-color:#ededed;
			}
			</style>';
			
			$header .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';
			
			$header .= '<tr class="table_header">';
			
			$header .= '<th style="width:20%;text-decoration:underline;"><strong>Contact Name</strong></th>';
			
			$header .= '<th style="width:20%;text-decoration:underline;"><strong>Date</strong></th>';
			
			$header .= '<th style="width:20%;text-decoration:underline;"><strong>Phone Number</strong></th>';
			
			$header .= '<th style="width:25%;text-decoration:underline;"><strong>E-mail </strong></th>';

			$header .= '<th style="width:15%;text-decoration:underline;"><strong>Task </strong></th>';
			
			$header .= '</tr>';
			
	
		
		foreach($check_sql as $key =>  $row)
		{
			
			if ($key % 2 == 0) {
			  $class_add = "even";
			}
			else if($key == 0)
			{
				 $class_add = "even";
			}
			else
			{
				$class_add = "odd";
			}

			if(is_numeric($row->contact_task))
			{

             $task=$selct_contact_task[$row->contact_task];
			
			}else{

             $task=$row->contact_task;
			     
			     }

			
			
			$contact_name = $row->contact_firstname .' '.$row->contact_middlename.' '.$row->contact_lastname;
			
			$header .='<tr class="'.$class_add.'">';
			
			$header .="<td>".$contact_name."</td>";
			$header .="<td>".date('m-d-Y', strtotime($row->contact_date))."</td>";
			$header .="<td>".$row->contact_phone."</td>";
			$header .="<td>".$row->contact_email."</td>";
			$header .="<td>".$task."</td>";

		
			$header	.= '</tr>';
			

		}
		
		$header .= '</table>';
			
		$pdf->WriteHTML($header);
		
		$pdf->Output('contact_task.pdf','D'); 
	}
	
	
		
	public function print_contcat_stats(){
		
		error_reporting(0);
		header("Cache-Control: ");// leave blank to avoid IE errors
			header("Pragma: ");// leave blank to avoid IE errors
			header("Content-type: application/octet-stream");
			// header("Content-type: application/vnd.ms-word");

			header("content-disposition: attachment;filename=Contact_stats.doc");
		echo '<html xmlns:v="urn:schemas-microsoft-com:vml"
				xmlns:o="urn:schemas-microsoft-com:office:office"
				xmlns:w="urn:schemas-microsoft-com:office:word"
				xmlns="http://www.w3.org/TR/REC-html40">

				<head>
				<meta http-equiv=Content-Type content="text/html; charset=utf-8">
				<meta name=ProgId content=Word.Document>
				<meta name=Generator content="Microsoft Word 9">
				<meta name=Originator content="Microsoft Word 9">
				<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->
				<title>title</title>
				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				body{
					font-size:10pt;
				}
				</style>
				</head>
				<body>';

echo '<style>

		table.table{
			width:100%;
			padding : 3px 0px;
		}
		table.table th{
			text-align : left;
			font-size : 10pt;
			
		}
		table.table td{
			font-size:10pt;
			
		
		}
		table.two_contact_signer td{
			margin : 3px;
			font-size:10pt;
		}
		h1{
			text-align:center;
		}
		h2{
			text-align:center;
		}
		p{
			font-size:10pt;
			word-spacing : 10px;
			padding : 2px 0px;
		}
		table{
			font-size:10pt;
		}
		
</style>';
		
		$contact_id = $this->uri->segment(2);
	
		$contact_name = $this->User_model->query("Select * from contact where contact_id = '".$contact_id."'");
		$contact_name = $contact_name->result();
		$name = $contact_name[0]->contact_firstname .' '.$contact_name[0]->contact_middlename .' '.$contact_name[0]->contact_lastname;
		
		
			//select borrower information...
			$borrower_contact = $this->User_model->query("select * from borrower_contact as bc JOIN contact as c ON bc.contact_id = c.contact_id where c.contact_id = '".$contact_id."'");
			if($borrower_contact->num_rows() > 0)
			{
				$borrower_contact = $borrower_contact->result();
					
					$borrower_id = $borrower_contact[0]->borrower_id;
					
					$borrower_account = $this->User_model->query("select * from borrower_contact as bc JOIN contact as c ON bc.contact_id = c.contact_id where bc.borrower_id = '".$borrower_id."'");
					$borrower_account = $borrower_account->result();
					
					$borrower_account1 = $borrower_account[0]->contact_firstname .' '.$borrower_account[0]->contact_middlename .' '.$borrower_account[0]->contact_lastname;
					
					$borrower_account2 = $borrower_account[1]->contact_firstname .' '.$borrower_account[1]->contact_middlename .' '.$borrower_account[1]->contact_lastname;
					
					
					$sql_paidoff = "SELECT COUNT(*) as count_loan , SUM(loan.loan_amount) as total_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '3' AND loan.borrower = '".$borrower_id."' ";
					$fetch_paidoff_loan = $this->User_model->query($sql_paidoff);
					if($fetch_paidoff_loan->num_rows() > 0)
					{
						$fetch_paidoff_loan = $fetch_paidoff_loan->result();
						$paidoff_count_loan = $fetch_paidoff_loan ? $fetch_paidoff_loan[0]->count_loan : 0;
						$paidoff_loan_amount = $fetch_paidoff_loan ? $fetch_paidoff_loan[0]->total_loan_amount : 0;
															
					}	
					
					$sql_active = "SELECT COUNT(*) as count_loan , SUM(loan.loan_amount) as total_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '2' AND loan.borrower = '".$borrower_id."' ";
					$fetch_active_loan = $this->User_model->query($sql_active);
					if($fetch_active_loan->num_rows() > 0)
					{
						$fetch_active_loan = $fetch_active_loan->result();
						
						$active_count = $fetch_active_loan ? $fetch_active_loan[0]->count_loan : 0;
						$active_loan_amount = $fetch_active_loan ? $fetch_active_loan[0]->total_loan_amount : 0;
												
					}
					
					$sql_pipeline = "SELECT COUNT(*) as count_loan , SUM(loan.loan_amount) as total_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '1' AND loan.borrower = '".$borrower_id."' ";
					$fetch_pipeline_loan = $this->User_model->query($sql_pipeline);
					if($fetch_pipeline_loan->num_rows() > 0)
					{
						$fetch_pipeline_loan = $fetch_pipeline_loan->result();
						
						$pipeline_count = $fetch_pipeline_loan ? $fetch_pipeline_loan[0]->count_loan : 0;
						$pipeline_loan_amount = $fetch_pipeline_loan ? $fetch_pipeline_loan[0]->total_loan_amount : 0;
													
					}
				
			
			}
			
			//select lender information...
 			$lender_info = $this->User_model->query("select * from lender_contact as lc JOIN contact as c ON lc.contact_id = c.contact_id where c.contact_id = '".$contact_id."'");
			
			/*  if($lender_info->num_rows() > 0)
			 {
				$lender_info = $lender_info->result();
				
					$investor_id = $lender_info[0]->lender_id;
					
					$active_lender_sql = "SELECT COUNT(*) as count_loan, SUM(loan_assigment.investment) as total_investment , SUM(loan.loan_amount) as total_loan_amount FROM loan_assigment JOIN loan ON loan_assigment.talimar_loan = loan.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_assigment.lender_name = '".$investor_id."' AND loan_servicing.loan_status = '2' ";
				
					$fetch_active_loan = $this->User_model->query($active_lender_sql);
					$active_count1	   = $fetch_active_loan ? $fetch_active_loan[0]->count_loan : 0;
					$active_amount1	   = $fetch_active_loan ? $fetch_active_loan[0]->total_investment : 0;
					
					$paidoff_lender_sql = "SELECT COUNT(*) as count_loan, SUM(loan_assigment.investment) as total_investment , SUM(loan.loan_amount) as total_loan_amount FROM loan_assigment JOIN loan ON loan_assigment.talimar_loan = loan.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_assigment.lender_name = '".$investor_id."' AND loan_servicing.loan_status = '3' ";
				
					$fetch_paidoff_loan = $this->User_model->query($paidoff_lender_sql);
					$paidoff_count1	   = $fetch_paidoff_loan ? $fetch_paidoff_loan[0]->count_loan : 0;
					$paidoff_amount1	   = $fetch_paidoff_loan ? $fetch_paidoff_loan[0]->total_investment : 0;
				
			}
			 */
		
			$contact_stats = '';			
			$contact_stats .= '<h2>Contact Stats</h2>';			
			$contact_stats .= '<p>Contact Stats: '.$name.'</p>';			
			$contact_stats .= '<h4>Borrowing:</h4>';			
			$contact_stats .= '<table class="table" style="width:100%;">';
			
			$contact_stats .= '<tr>';
			$contact_stats .= '<td>Pipeline Loans:</td>';
			$contact_stats .= '<td>'.$pipeline_count.'</td>';
			$contact_stats .= '<td>$'.number_format($pipeline_loan_amount).'</td>';
			$contact_stats .= '</tr>';
			
			$contact_stats .= '<tr>';
			$contact_stats .= '<td>Active Loans:</td>';
			$contact_stats .= '<td>'.$active_count.'</td>';
			$contact_stats .= '<td>$'.number_format($active_loan_amount).'</td>';
			$contact_stats .= '</tr>';
			
			$contact_stats .= '<tr>';
			$contact_stats .= '<td>PaidOff Loans:</td>';
			$contact_stats .= '<td>'.$paidoff_count_loan.'</td>';
			$contact_stats .= '<td>$'.number_format($paidoff_loan_amount).'</td>';
			$contact_stats .= '</tr>';
			
			$contact_stats .= '</table>';
			
			$contact_stats .= '<p>Borrower Account:</p>';
			
			$contact_stats .= '<table>&nbsp;<br></table>';
			
			$contact_stats .= '<h4>Lending:</h4>';
			
			$contact_stats .= '<table class="table" style="width:100%;">';
			
			$contact_stats .= '<tr>';
			$contact_stats .= '<td>Active Trust Deed:</td>';
			$contact_stats .= '<td>'.$active_count1.'</td>';
			$contact_stats .= '<td>$'.number_format($active_amount1).'</td>';
			$contact_stats .= '</tr>';
			
			$contact_stats .= '<tr>';
			$contact_stats .= '<td>PaidOff Trust Deed:</td>';
			$contact_stats .= '<td>'.$paidoff_count1.'</td>';
			$contact_stats .= '<td>$'.number_format($paidoff_amount1).'</td>';
			$contact_stats .= '</tr>';
			
			$contact_stats .= '</table>';			
			$contact_stats .= '<p>Lender Account:</p>';
			
			echo $contact_stats;	
			
	}
	
	
	public function contact_stats_borrow(){
		
		error_reporting(0);
		
		header("Cache-Control: ");// leave blank to avoid IE errors
			header("Pragma: ");// leave blank to avoid IE errors
			header("Content-type: application/octet-stream");
			// header("Content-type: application/vnd.ms-word");

			header("content-disposition: attachment;filename=contact_borrow.doc");
		echo '<html xmlns:v="urn:schemas-microsoft-com:vml"
				xmlns:o="urn:schemas-microsoft-com:office:office"
				xmlns:w="urn:schemas-microsoft-com:office:word"
				xmlns="http://www.w3.org/TR/REC-html40">

				<head>
				<meta http-equiv=Content-Type content="text/html; charset=utf-8">
				<meta name=ProgId content=Word.Document>
				<meta name=Generator content="Microsoft Word 9">
				<meta name=Originator content="Microsoft Word 9">
				<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->
				<title>title</title>
				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				body{
					font-size:10pt;
				}
				</style>
				</head>
				<body>';

echo '<style>

		table.table{
			width:100%;
			padding : 3px 0px;
		}
		table.table th{
			text-align : left;
			font-size : 10pt;
			
		}
		table.table td{
			font-size:10pt;
			font-size : 10pt;
		
		}
		table.two_contact_signer td{
			margin : 3px;
			font-size:10pt;
		}
		h1{
			text-align:center;
		}
		h2{
			text-align:center;
		}
		p{
			font-size:10pt;
			word-spacing : 10px;
			padding : 2px 0px;
		}
		table{
			font-size:10pt;
		}
</style>';
		
		$contact_id = $this->uri->segment(2);
		//echo $contact_id;
		
		$current_date = date('Y-m-d');
		
		$sql = "SELECT * FROM contact WHERE contact_id ='".$contact_id."'";
		$fetch_contact = $this->User_model->query($sql);
		$fetch_contact = $fetch_contact->row();
		
		
		//fetch borrower contact details-------------------------------------------->
		
		$borrower_contact = $this->User_model->query("select * from borrower_contact where contact_id = '".$contact_id."'");
		
			if($borrower_contact->num_rows() > 0)
			{
				$borrower_contact = $borrower_contact->result();
					
				$array_lender_idd=array();	
				foreach($borrower_contact as $row){
				
				$array_lender_idd[]=$row->borrower_id;
				
				
				}
				
				$borrower_id=implode(",",$array_lender_idd);
	
					
					
					// $borrower_id = $borrower_contact[0]->borrower_id;
						// echo '<pre>';
				// print_r($borrower_id);
				// echo '</pre>';
					$borrower_account = $this->User_model->query("select * from borrower_data where id IN (".$borrower_id.")");
					$borrower_account = $borrower_account->result();
					
					$borrower_account[0]->b_name;
					$borrower_account[0]->borrower_type;
					$borrower_account1 = $borrower_account[0]->b_address .''.$borrower_account[0]->b_unit .''.$borrower_account[0]->b_city.''.$borrower_account[0]->b_state.''.$borrower_account[0]->b_zip;
					
					// $borrower_account = $this->User_model->query("select * from borrower_contact as bc JOIN contact as c ON bc.contact_id = c.contact_id where bc.borrower_id = '".$borrower_id."'");
					// $borrower_account = $borrower_account->result();
					
					$loan_account = $this->User_model->query("select * from loan where borrower IN (".$borrower_id.")");
					//echo $loan_account;
					
					$loan_account = $loan_account->result();
					$loan_account[0]->loan_amount;
					
					$talimar_loan = $loan_account[0]->talimar_loan;
					
					$loan_account1 = $this->User_model->query("select * from loan_servicing
					where talimar_loan = '".$talimar_loan."'");
					
					$loan_account1 = $loan_account1->result();
					$loan_account1[0]->loan_status;
					
					
					// loan and loan servicing table 	
					
					$sql_paidoff = "SELECT COUNT(*) as count_loan , SUM(loan.loan_amount) as total_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '3' AND loan.borrower IN (".$borrower_id.") ";
					$fetch_paidoff_loan = $this->User_model->query($sql_paidoff);
					
					if($fetch_paidoff_loan->num_rows() > 0)
					{
						$fetch_paidoff_loan = $fetch_paidoff_loan->result();
						$fetch_paidoff_loan[0]->loan_amount;
						$paidoff_count_loan = $fetch_paidoff_loan ? $fetch_paidoff_loan[0]->count_loan : 0;
						
						$paidoff_loan_amount = $fetch_paidoff_loan ? $fetch_paidoff_loan[0]->total_loan_amount : 0;
															
					}
					
					$sql_active = "SELECT COUNT(*) as count_loan , SUM(loan.loan_amount) as total_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '2' AND loan.borrower IN (".$borrower_id.") ";
					$fetch_active_loan = $this->User_model->query($sql_active);
					if($fetch_active_loan->num_rows() > 0)
					{
						$fetch_active_loan = $fetch_active_loan->result();
						
						$active_count = $fetch_active_loan ? $fetch_active_loan[0]->count_loan : '0';
						$active_loan_amount = $fetch_active_loan ? $fetch_active_loan[0]->total_loan_amount : '0';
												
					}
					$sql_pipeline = "SELECT COUNT(*) as count_loan , SUM(loan.loan_amount) as total_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '1' AND loan.borrower IN (".$borrower_id.") ";
					$fetch_pipeline_loan = $this->User_model->query($sql_pipeline);
					if($fetch_pipeline_loan->num_rows() > 0)
					{
						$fetch_pipeline_loan = $fetch_pipeline_loan->result();
						
						$pipeline_count = $fetch_pipeline_loan ? $fetch_pipeline_loan[0]->count_loan : 0;
						$pipeline_loan_amount = $fetch_pipeline_loan ? $fetch_pipeline_loan[0]->total_loan_amount : 0;
					}
					
					
					
					//loan by contact...
					$loan_contact = $this->User_model->query("select *,l.borrower as borrower_id from loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan where l.borrower IN (".$borrower_id.") AND ls.loan_status IN ('1','2','3') order by ls.loan_status ASC");
					
					$loan_contact = $loan_contact->result();
			}	

			
			$leder_data_re870 .= '';
			
			$leder_data_re870	.= '<table class="table" style="width:100%;">';
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:70%;"><strong>Contact Name:</strong> '.$fetch_contact->contact_firstname .' '.$fetch_contact->contact_middlename .' '.$fetch_contact->contact_lastname .'</td>';
			
			$leder_data_re870	.= '<td style="width:30%;"><strong>Date:</strong> '.date('m-d-Y').'</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:70%;"><strong>Phone:</strong> '.$fetch_contact->contact_phone.'</td>';
			

			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:70%;"><strong>Email Address:</strong> '.$fetch_contact->contact_email.'</td>';
			

			$leder_data_re870	.= '</tr>';
			$leder_data_re870	.= '</table>';
			
			
			$leder_data_re870	.= '<table class="table" style="width:100%;">';
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:100%;" colspan="2"><strong>Contact Stats:</strong></td><br>';
			$leder_data_re870	.= '</tr>';
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:0%;">Active # of Loans: '.$active_count.'</td>';
			$leder_data_re870	.= '<td style="width:50%;">Active $ of Loans: $'.number_format($active_loan_amount).'</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:50%;">Paid  off  #  of  Loans: '.$paidoff_count_loan.'</td>';
			$leder_data_re870	.= '<td style="width:50%;">Paid  off  #  of  Loans: $'.number_format($paidoff_loan_amount).'</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			
			$total_count = $active_count + $paidoff_count_loan;
			$total_amount = $active_loan_amount + $paidoff_loan_amount;
												
			$leder_data_re870	.= '<td style="width:50%;">Total  #  of  Loans: '.$total_count.'</td>';
			$leder_data_re870	.= '<td style="width:50%;">Total $ of Loans: $'.number_format($total_amount).'</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '</table><br>';
			
			$leder_data_re870	.= '<table class="table" style="width:100%;">';
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td colspan="2" style="width:100%;"><strong>Borrower Information:</strong><span style="text-decoration:;"></td>';
			$leder_data_re870	.= '</tr><br>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:50%;"><strong style="text-decoration:underline;">Borrower Name:</strong></td>';
			
			$leder_data_re870	.= '<td style="width:50%;"><strong style="text-decoration:underline;">Borrower Type:</strong></td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:50%;">'.$borrower_account[0]->b_name.'</td>';
			$borrower_type_option = $this->config->item('borrower_type_option');
			$leder_data_re870	.= '<td style="width:50%;">'.$borrower_type_option[$borrower_account[0]->borrower_type].'</td>';
			
			$leder_data_re870	.= '</tr>';
			
			
			$leder_data_re870	.= '</table><br>';
			
			$leder_data_re870	.= '<table class="table" style="width:100%;">';
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td colspan="4" style="width:100%;"><strong>Loans by Contact:</strong> <span></span></td><br>';
			
			
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			
			$leder_data_re870	.= '<td style="width:25%;"><strong>Borrower Name</strong></td>';
			//$leder_data_re870	.= '<td style="width:25%;">">'.$borrower_account1.'</td>';
			$leder_data_re870	.= '<td style="width:25%;"><strong>Address</strong></td>';
			
			$leder_data_re870	.= '<td style="width:25%;"><strong>Loan Amount</strong></td>';
			
			$leder_data_re870	.= '<td style="width:25%;"><strong>Loan Status</strong></td>';
			$leder_data_re870	.= '</tr>';
			$leder_data_re870	.= '<tr><td colspan="4" style="border-top:1px solid black;"></td> </tr>';
			
			
			$amount = 0;
			foreach($loan_contact as $rows){
				
			$borrower_account1 = $this->User_model->query("select * from borrower_data where id = '".$rows->borrower_id ."'");
			$borrower_account1 = $borrower_account1->result();
			$talimar['talimar_loan'] 	= $rows->talimar_loan;
			
			$fetch_property 			= $this->User_model->select_where('loan_property',$talimar);
			$fetch_property				= $fetch_property->result();
			// $property_address			= 
		
			$borrower_account11 = $fetch_property[0]->property_address.'<br>'.$fetch_property[0]->city.', '.$fetch_property[0]->state.' '.$fetch_property[0]->zip;;
				
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:25%;vertical-align:top;">'.$borrower_account1[0]->b_name.'</td>';
			
			$leder_data_re870	.= '<td style="width:30%;">'.$borrower_account11.'</td>';
			
			$leder_data_re870	.= '<td style="width:25%;">$'.number_format($rows->loan_amount).'</td>';
			$loan_status_option = $this->config->item('loan_status_option');
			$leder_data_re870	.= '<td style="width:20%;">'.$loan_status_option[$rows->loan_status].'</td>';
			$leder_data_re870	.= '</tr>';
			
			$amount += $rows->loan_amount;
			}
			
			
			$leder_data_re870	.= '</table> </br> <br>';
			
			$leder_data_re870	.= '<table class="table" style="width:100%;">';
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:50%;text-align:right;"><strong>Total:</strong> </td>';
			$leder_data_re870	.= '<td style="width:25%;margin-left:30px;"><strong>$'.number_format($amount).'</strong></td>';
			$leder_data_re870	.= '<td style="width:25%;"></td>';
			$leder_data_re870	.= '</tr>';
			$leder_data_re870	.= '</table>';
			
			echo $leder_data_re870;
			
	}
	
	
	public function contact_stats_lender(){
		
		error_reporting(0);
		
		header("Cache-Control: ");// leave blank to avoid IE errors
			header("Pragma: ");// leave blank to avoid IE errors
			header("Content-type: application/octet-stream");
			// header("Content-type: application/vnd.ms-word");

			header("content-disposition: attachment;filename=contact_lender.doc");
		echo '<html xmlns:v="urn:schemas-microsoft-com:vml"
				xmlns:o="urn:schemas-microsoft-com:office:office"
				xmlns:w="urn:schemas-microsoft-com:office:word"
				xmlns="http://www.w3.org/TR/REC-html40">

				<head>
				<meta http-equiv=Content-Type content="text/html; charset=utf-8">
				<meta name=ProgId content=Word.Document>
				<meta name=Generator content="Microsoft Word 9">
				<meta name=Originator content="Microsoft Word 9">
				<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->
				<title>title</title>
				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				body{
					font-size:10pt;
				}
				</style>
				</head>
				<body>';

echo '<style>

		table.table{
			width:100%;
			padding : 3px 0px;
		}
		table.table th{
			text-align : left;
			font-size : 10pt;
			
		}
		table.table td{
			font-size:10pt;
			font-size : 10pt;
		
		}
		table.two_contact_signer td{
			margin : 3px;
			font-size:10pt;
		}
		h1{
			text-align:center;
		}
		h2{
			text-align:center;
		}
		p{
			font-size:10pt;
			word-spacing : 10px;
			padding : 2px 0px;
		}
		table{
			font-size:10pt;
		}
		</style>';
		
		$contact_id = $this->uri->segment(2);
		//echo $contact_id;
		
		$sql = "SELECT * FROM contact WHERE contact_id ='".$contact_id."'";
		$fetch_contact = $this->User_model->query($sql);
		$fetch_contact = $fetch_contact->row();
		
		$lender_contact = "SELECT * FROM lender_contact WHERE contact_id='".$contact_id."'";
		$lender_contact_type = $this->User_model->query($lender_contact);	
		$lender_contact_type = $lender_contact_type->result();
		$array_lender_id=array();	
		foreach($lender_contact_type as $k=>$ro){
        
		$array_lender_id[]=$ro->lender_id;
		
		
		}
		
		$lender_id=implode(",",$array_lender_id);
	
		// $lender_id = $lender_contact_type[0]->lender_id;
		//echo $lender_id;
	// print_r($array_lender_id);		

		$investor = $this->User_model->query("select * from investor where id IN (".$lender_id.")");
		$investor = $investor->result();
		
		$investor_address = $investor[0]->c_address .'<br>'.$investor[0]->c_city.', '.$investor[0]->c_state.' '.$investor[0]->c_zip;
	
		// investment data----------------------------------------------
		
		$id = $investor[0]->id;
		
		$lender_contact1 = "SELECT Count(*) as total_count, SUM(`investment`) as total_amount FROM `loan_assigment` WHERE `lender_name`='".$id."'";
		$assignment1 = $this->User_model->query($lender_contact1);
		$assignment1 = $assignment1->result();
		$assignment1[0]->total_amount;
			
		// total lender fetch data--------------------------------------------->
			
		$active_lender_sql = "SELECT COUNT(*) as count_loan, SUM(loan_assigment.investment) as total_investment , SUM(loan.loan_amount) as total_loan_amount FROM loan_assigment JOIN loan ON loan_assigment.talimar_loan = loan.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_assigment.lender_name = '".$id."' AND loan_servicing.loan_status = '2' ";
				
			$fetch_active_loan = $this->User_model->query($active_lender_sql);
			$fetch_active_loan = $fetch_active_loan->result();
			
				$active_count = $fetch_active_loan ? $fetch_active_loan[0]->count_loan : '0';
				$active_amount = $fetch_active_loan ? $fetch_active_loan[0]->total_loan_amount : '0';
		
		
		$paidoff_lender_sql = "SELECT COUNT(*) as count_loan, SUM(loan_assigment.investment) as total_investment , SUM(loan.loan_amount) as total_loan_amount FROM loan_assigment JOIN loan ON loan_assigment.talimar_loan = loan.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_assigment.lender_name = '".$id."' AND loan_servicing.loan_status = '3' ";
			
			
			$fetch_paidoff_loan = $this->User_model->query($paidoff_lender_sql);
			$fetch_paidoff_loan = $fetch_paidoff_loan->result();
			
			
				$paidoff_count1	   = $fetch_paidoff_loan ? $fetch_paidoff_loan[0]->count_loan : '0';
				$paidoff_amount1	   = $fetch_paidoff_loan ? $fetch_paidoff_loan[0]->total_investment : '0';
				
				
			//loan_by_contact...
			$total_loan = $this->User_model->query("SELECT * FROM `loan_assigment` as la JOIN loan_servicing as ls ON la.talimar_loan = ls.talimar_loan WHERE la.`lender_name` IN(".$lender_id.") AND ls.loan_status IN('1','2','3') order by ls.loan_status ASC"); 		
			$total_loan = $total_loan->result();
				
				
			$leder_data_re870 .= '';
			
			$leder_data_re870	.= '<table class="table" style="width:100%;">';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td><strong>Contact Name: </strong>'.$fetch_contact->contact_firstname .' '.$fetch_contact->contact_middlename .' '.$fetch_contact->contact_lastname .'</td>';
			$leder_data_re870	.= '<td style="width:30%;"><strong>Date:</strong> '.date('m-d-Y').'</td>';
			
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td><strong>Phone: </strong>'.$fetch_contact->contact_phone .'</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td><strong>E-mail Address: </strong>'.$fetch_contact->contact_email .'</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '</table><br>';
			
			
			$leder_data_re870	.= '<table class="table" style="width:100%;">';
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:100%;" colspan="2"><strong>Contact Stats:</strong></td><br>';
			$leder_data_re870	.= '</tr>';

			$leder_data_re870	.= '<tr>';
			
			$leder_data_re870	.= '<td style="width:50%;">Active # of Trust Deeds: '.$active_count.'</td>';
			
			
			$leder_data_re870	.= '<td style="width:50%;">Active $ of Trust Deeds: $'.number_format($active_amount).'</td>';
			$leder_data_re870	.= '</tr>';
			
			$lender_data_re870 .= '<tr>';
			$leder_data_re870	.= '<td style="width:50%;">Paid  off  #  of  Trust Deeds: '.$paidoff_count1.'</td>';
			$leder_data_re870	.= '<td style="width:50%;">Paid  off  #  of  Trust Deeds: $'.number_format($paidoff_amount1).'</td>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870   .= '<tr>';
			
			$total_counts = $active_count + $paidoff_count1;
			$total_amounts = $active_amount + $paidoff_amount1;
			
			$leder_data_re870 .= '<td style="width:50%;">Total  #  of  Trust Deeds: '.$total_counts.'</td>';
			$leder_data_re870 .= '<td style="width:50%;"> $ of  Trust Deeds: $'.number_format($total_amounts).'</td>';
			$leder_data_re870 .= '</tr>';
			
			$leder_data_re870	.= '</table><br>';
			
			$leder_data_re870	.= '<table class="table" style="width:100%;">';
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td colspan="2" style="width:100%;"><strong>Lender Information:</strong><span></span></td><br>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:50%;"><strong style="text-decoration:underline;">Lender Name:</strong></td>';
			
			$leder_data_re870	.= '<td style="width:50%;"><strong style="text-decoration:underline;">Lender Type:</strong></td>';
			$leder_data_re870	.= '</tr>';
			foreach($investor as $r){
			

		
			$leder_data_re870	.= '<tr>';
		
			$leder_data_re870	.= '<td style="width:50%;">'.$r->name.'</td>';
			$investor_type_option = $this->config->item('investor_type_option');
			$leder_data_re870	.= '<td style="width:50%;">'.$investor_type_option[$r->investor_type].'</td>';
			$leder_data_re870	.= '</tr>';
			}
			$leder_data_re870	.= '</table><br>';
			
			$leder_data_re870	.= '<table class="table" style="width:100%;">';
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td colspan="4" style="width:100%;"><strong>Loans by Contact: </strong><span></span></td><br>';
			$leder_data_re870	.= '</tr>';
			
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:25%;"><strong>Lender Name</strong></td>';
			$leder_data_re870	.= '<td style="width:25%;"><strong>Address</strong><span style="text-decoration:underline;"></span></td>';
			$leder_data_re870	.= '<td style="width:25%;"><strong>Investment</strong><span style="text-decoration:underline;"></span></td>';
			$leder_data_re870	.= '<td style="width:25%;"><strong>TD Status</strong><span style="text-decoration:underline;"></span></td>';
			$leder_data_re870	.= '<tr><td colspan="4" style="border-top:1px solid black;"></td></tr>';
			$leder_data_re870	.= '</tr>';
			
			$total_invest = 0;
	
			foreach($total_loan as $rows){
			
			
			
			$loan_status_option = $this->config->item('loan_status_option');
			$investor1 = $this->User_model->query("select * from investor where id = '".$rows->lender_name."'");
			$investor1 = $investor1->result();
			 $talimar['talimar_loan'] 	= $rows->talimar_loan;
			
			$fetch_property 			= $this->User_model->select_where('loan_property',$talimar);
			$fetch_property				= $fetch_property->result();     
            $investor_address = $fetch_property[0]->property_address .'<br>'.$fetch_property[0]->city.', '.$fetch_property[0]->state.' '.$fetch_property[0]->zip;
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:30%;vertical-align:top;">'.$investor1[0]->name.'</td>';
			$leder_data_re870	.= '<td style="width:25%;">'.$investor_address.'</td>';
			$leder_data_re870	.= '<td style="width:25%;">$'.number_format($rows->investment).'</td>';
			$leder_data_re870	.= '<td style="width:20%;">'.$loan_status_option[$rows->loan_status].'</td>';
			$leder_data_re870	.= '</tr>';
			$total_invest += $rows->investment;
			}
			
			
			$leder_data_re870	.= '</table>';
			$leder_data_re870	.= '<table class="table" style="width:100%;">';
			$leder_data_re870	.= '<tr>';
			$leder_data_re870	.= '<td style="width:50%;text-align:right;"><strong>Total:</strong></td>';
			$leder_data_re870	.= '<td style="width:25%;margin-left:30px;"><strong>$'.number_format($total_invest).'</strong></td>';
			$leder_data_re870	.= '<td style="width:25%;"></td>';
			$leder_data_re870	.= '</tr>';
			$leder_data_re870	.= '</table>';
			
			echo $leder_data_re870;
			
	}
	
	
	/*public function contact_email_get(){
		
		// connect to gmail 
		// $hostname = '{imap.gmail.com:993/imap/ssl}INBOX';
		// $username = 'pankaj.wartiz@gmail.com';
		// $password = '1234@wartiz';
		
		$hostname = '{outlook.office365.com:993/imap/ssl}INBOX';
		$username = 'adabra@talimarfinancial.com';
		$password = 'wartiz@72126';

		// try to connect 
		$inbox = @imap_open($hostname,$username,$password);

		// grab emails 

		//$emails = @imap_search($inbox,'ALL'); // search CC, BCC, ALL Types of mail.

		 $emails = @imap_headerinfo($inbox,'bccaddress'); // search CC, BCC, ALL Types of mail.
		

		// if emails are returned, cycle through each... 
		if($emails) {
			
			// begin output var 
			$output = '';
			
			// put the newest emails on top 
			rsort($emails);
			
			// for every email... 
			foreach($emails as $email_number) {
				
				// get information specific to this email 
				$overview = @imap_headerinfo($inbox,$email_number,0);
				$message = @imap_qprint(imap_fetchbody($inbox,$email_number,1));
				// $message = @imap_qprint(@imap_fetchbody($inbox,$email_number,1));
				  //  echo'<pre>';
				print_r($overview);
				// print_r($message);
				//echo'</pre>';  
				
		
			    $a = $overview->to;
				$b = $overview->from;
				$from    = $b[0]->mailbox .'@'.$b[0]->host;
				$to		 = $a[0]->mailbox.'@'.$a[0]->host;
				
				$output .= 'Subject: '.$overview->subject.'<br />';
				$output .= 'From: '.$from.'<br />';
				$output .= 'To: '.$to.'<br />';
				$output .= 'Body: '.$message.'<br /><br>'; 
		
				// Attachments
				
				$structure = @imap_fetchstructure($inbox,$email_number);
				$attachments = array();
					if(isset($structure->parts) && count($structure->parts))
					{
						for($i = 0; $i < count($structure->parts); $i++)
						{
							$attachments[$i] = array(
										'is_attachment' => false,
										'filename' => '',
										'name' => '',
										'attachment' => ''
									);

							if($structure->parts[$i]->ifdparameters)
							{
								foreach($structure->parts[$i]->dparameters as $object)
								{
									if(strtolower($object->attribute) == 'filename')
									{
										$attachments[$i]['is_attachment'] = true;
										$attachments[$i]['filename'] = $object->value;
									}
								}
							}

							if($structure->parts[$i]->ifparameters)
							{
								foreach($structure->parts[$i]->parameters as $object)
								{
									if(strtolower($object->attribute) == 'name')
									{
										$attachments[$i]['is_attachment'] = true;
										$attachments[$i]['name'] = $object->value;
									}
								}
							}

							if($attachments[$i]['is_attachment'])
							{
								$attachments[$i]['attachment'] = @imap_fetchbody($inbox, $email_number, $i+1);

								 // 3 = BASE64 encoding 
								if($structure->parts[$i]->encoding == 3)
								{
									$attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
								}
									// 4 = QUOTED-PRINTABLE encoding 
								elseif($structure->parts[$i]->encoding == 4)
								{
									$attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
								} 
							}
						}
					}

				foreach($attachments as $attachment)
				{
					
					 if($attachment['is_attachment'] == 1)
					{
						$filename = $attachment['name'];
						if(empty($filename)) $filename = $attachment['filename'];
						$file_path = 'C:\xampp\htdocs\talimar\application\views\contact\upload/'; //  Upload folder
						$fp = fopen($file_path . $filename, "w+");
						fwrite($fp, $attachment['attachment']);
						fclose($fp);
					 } 
				 }
				
				// Attachments
				$fpath = $file_path . $filename;
				$data['msgno']			 	= $overview->Msgno;
				$data['subject'] 			= $overview->subject;
				$data['email_from'] 		= $from;
				$data['email_to'] 			= $to;
				$data['body'] 				= $message;
				$data['attachment'] 		= $attachment['name'];
				//$data['path'] 				= $fpath;
			
				$fetch_sql = "SELECT * FROM `email_table` WHERE Msgno = '".$overview->Msgno."'";
				$result = $this->User_model->query($fetch_sql);
				if($result->num_rows() > 0){
					
					$where['msgno'] = $overview->Msgno;
					$this->User_model->updatedata('email_table',$where,$data);
					
				}else{
					
					$this->User_model->insertdata('email_table',$data);
					
				}

			}
	
			// echo $output;
	
	
		}

		// close the connection
		@imap_close($inbox);
		
		
		
	}*/

	// public function contact_get(){
		
		// $sql = "SELECT * FROM `contact_demo` WHERE id BETWEEN '1001' And '2000'";
		// $fetch_sql = $this->User_model->query($sql);
		// $fetch_sql = $fetch_sql->result();
		// /* 
			// echo'<pre>';
			// print_r($fetch_sql);
			// echo'</pre>';
			 // */
			
		// foreach($fetch_sql as $row){
			
			// $first_name 			= str_replace("'","",$row->first_name);
			// $last_name 				= str_replace("'","",$row->last_name);
			// $email 					= $row->email_1;
			// $email_2 				= $row->email_2;
			// $phone_1 				= $row->phone_1;
			// $phone_2_main 			= $row->phone_2_main;
			// $company_name_1 		= $row->company_name_1;
			
			//$sql = "UPDATE contact_demo SET first_name = '".$first_name."',  last_name = '".$last_name."' WHERE id = '".$row->id."'";
			//$this->User_model->query($sql);
			
			// $sql = "SELECT * FROM contact WHERE contact_firstname = '".$first_name."' AND contact_lastname='".$last_name."' AND contact_email ='".$email."'";
			
			// $result = $this->User_model->query($sql);
			// /* $result = $result->result();
			// echo'<pre>';
			// print_r($result);
			// echo'</pre>';
			// die('kk'); */
				// if($result->num_rows() == 0){
					
					// $user_id	= $this->session->userdata('t_user_id');
					
					// $sql = "INSERT INTO `contact`(`user_id`, `lender_id`, `borrower_id`, `contact_firstname`, `contact_middlename`, `contact_lastname`, `contact_suffix`, `contact_address`, `contact_unit`, `contact_city`, `contact_state`, `contact_zip`, `same_mailing_address`, `contact_mailing_address`, `contact_mailing_unit`, `contact_mailing_city`, `contact_mailing_state`, `contact_mailing_zip`, `contact_title`, `contact_phone`, `contact_secondary_phone`, `contact_fax`, `contact_email`, `contact_email2`, `contact_website`, `company_name_1`, `company_name_2`, `company_name_3`, `contact_personal_notes`, `fb_profile`, `lkdin_profile`, `contact_about`, `contact_dob`, `borrower_contact_type`, `borrower_migrate`, `borrower_contact_person`, `lender_contact_type`, `lender_migrate`, `lender_contact_person`) VALUES ('".$user_id."',null,null,'".$first_name."',null,'".$last_name."', null, null, null, null, null, null, null, null, null, null, null,null, null,'".$phone_1."','".$phone_2_main."', null,'".$email."','".$email_2."', null,'".$company_name_1."', null, null, null, null, null, null, null, null, null, null, null, null, null)";
					
					// $this->User_model->query($sql);
					
				// }
		// }
		
		
		
		
	// }
	
	/* public function fetch_contact_tag_data(){
		
		$sql = "SELECT * FROM `contact_demo` WHERE id BETWEEN '1001' AND '2000'";
		$fetch_sql = $this->User_model->query($sql);
		$fetch_sql = $fetch_sql->result();
		
		foreach($fetch_sql as $row){
					
			$fname = $row->first_name;
			$lname = $row->last_name;
			$email = $row->email_1;
				
			$contact_email_blast = 3;
			
			if($row->hard_money_main == 'Yes'){
				
				$contact_email_blast = 3;
				
			}elseif($row->wholesale_blast == 'Yes'){
				
				$contact_email_blast = 1;
				
			}elseif($row->trust_deed_mailer == 'Yes'){
				
					$contact_email_blast = 2;
				
			}elseif($row->opt_out == 'Yes'){
				
					$contact_email_blast = 4;
				
			}else{
					$contact_email_blast = '';
			}
			
			$sql = "SELECT * FROM contact WHERE contact_firstname = '".$fname."' AND contact_lastname = '".$lname."' AND contact_email = '".$email."'";
			$res = $this->User_model->query($sql);
			
			if($res->num_rows() > 0){
				
				$res = $res->result();
				
				$contact_id = $res[0]->contact_id;
				// echo'<pre>';
				// print_r($contact_id);
				// echo'</pre>';
				$user_id	= $this->session->userdata('t_user_id');
				
				$sql = "INSERT INTO `contact_tags`(`contact_id`, `contact_type`, `contact_specialty`, `contact_email_blast`, `contact_permission`, `user_id`) VALUES ('".$contact_id."',null,null,'".$contact_email_blast."',null,'".$user_id."')";
				
				$this->User_model->query($sql);
				
			}
			
		}
		
			
	} */



	public function fetch_data(){
	
	 $id=$this->input->post('id');
	
	$relationship_view_query = $this->User_model->select_where('contact_relationship',array('id' => $id));

	$relationship_view_query=$relationship_view_query->result();

	foreach($relationship_view_query as $row){
		
		$data['relationship_contact_id'] =$row->relationship_contact_id;
		$data['relationship_note'] =$row->relationship_note;
		$data['relation_option'] =$row->relation_option;
		$data['show_lender_portal'] =$row->show_lender_portal;
		$data['id'] =$row->id;
	}
	
	
	echo json_encode($data);
	
	}
	
	
	public function edit_update_data(){
	
	 $id=$this->input->post('id');
	 $contact_id=$this->uri->segment(2);
	
	 $relationship_note=$this->input->post('relationship_note');
	 $relationship=$this->input->post('relationship');
	 $relation_option=$this->input->post('relation_option');
	 $show_lender_portal=$this->input->post('show_lender_portal');
	 if(!empty($show_lender_portal) && $show_lender_portal>0){
	 	$show_lender_portal=1;
	 }else{
	 	$show_lender_portal=0;
	 }
	$update =$this->db->query("update contact_relationship set relationship_note='".$relationship_note."',relation_option='".$relation_option."',relationship_contact_id='".$relationship."',show_lender_portal='".$show_lender_portal."' WHERE id = '".$id."'");
	echo 1;
		

	
		
	}

	public function SendUsername()
	{
		$this->load->library('email');
		$this->email->initialize(SMTP_SENDGRID);
		$mail = array();
		if(!empty($this->input->post('contact_id')))
		{
			$contact_id = $this->input->post('contact_id');
			$user_type = $this->input->post('user_type');

			$contact = $this->User_model->select_where('contact', array('contact_id'=>$contact_id));

			$contact = $contact->row();


			if($user_type==0)
			{
				$lander = $this->User_model->select_where('lender_contact_type', array('contact_id'=>$contact_id));

				$lander = $lander->row();

				$lander_id = $lander->lender_contact_id;

				if(!empty($lander_id) && !empty($contact->contact_email) && $user_type==0)
				{
					$email_address = $contact->contact_email;
					$contact_id  = $contact->contact_id;
					
					$name 		 = $contact->contact_firstname;

					$getusername = $this->User_model->query("Select username from lender_contact_type where contact_id = '".$contact_id."' AND lender_id > 0");
					$getusername = $getusername->row();
					$Username = '';
					if($getusername){
						$Username = $getusername->username;
					}

					if(!$Username){
							$Username = $this->input->post('username');
					}

										
					$password_message = '	
						<p style="font-size: 14px;">Dear '.$name.',
						<br>
						Your Lender Username is : '.$Username;

					$dataMailA = array();
			        $dataMailA['from_email']  	= 'invest@talimarfinancial.com';
			        $dataMailA['from_name']   	= 'TaliMar Financial';
			        $dataMailA['to']      		= $email_address;
			        $dataMailA['cc']      		= '';
			        $dataMailA['bcc']      		= '';
			        $dataMailA['name']      	= '';
			        $dataMailA['subject']     	= 'Talimar Lender Portal Username';
			        $dataMailA['content_body']  = $password_message;
			        $dataMailA['button_url']  	= '';
			        $dataMailA['button_text']   = 'Create Password';
			        $dataMailA['attach_file']   = '';
			        send_mail_template($dataMailA);
					$mail['lender'] = 1; //$this->email->send(); 
					$status = true;
					$mail['message'] = '<div class="alert alert-success alert-dismissible">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Success!</strong> Username send successfully.
							</div>';
				}
				else
				{
					$status = false;
					$mail['message'] = '<div class="alert alert-danger alert-dismissible">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Danger!</strong> Username send failed.
							</div>';
				}
			}
			elseif($user_type==1)
			{
				$lander = $this->User_model->select_where('lender_contact_type', array('contact_id'=>$contact_id));

				$lander = $lander->row();

				$lander_id = $lander->lender_contact_id;

				if(!empty($lander_id) && !empty($contact->contact_email) && $user_type==0)
				{
					$email_address = '';
						$contact_id  = '';
						$name 		 = '';
						$borrower_username 		 = '';
						$email_address = $contact->contact_email;
						if($contact){							
							$contact_id  = $contact->contact_id;
							$name 		 = $contact->contact_firstname;
							//$borrower_username = $contact->borrower_username;
						}

						$borrowerQal = $this->User_model->query("Select * from borrower_contact_type where contact_id = '".$contact_id."'");
						$borrowerData = $borrowerQal->result();

						if($borrowerData){
							$borrower_username = $borrowerData[0]->borrower_username;
						}

						if(!$borrower_username){
							$borrower_username = $this->input->post('username');
						}

										
					$password_message = '			
						<p style="font-size: 14px;">Dear '.$name.',
						<br>
						Your Borrower Username is : '.$borrower_username;


					$dataMailA = array();
			        $dataMailA['from_email']  	= 'invest@talimarfinancial.com';
			        $dataMailA['from_name']   	= 'TaliMar Financial';
			        $dataMailA['to']      		= '';
			        $dataMailA['cc']      		= '';
			        $dataMailA['bcc']      		= '';
			        $dataMailA['name']      	= '';
			        $dataMailA['subject']     	= 'Password Reset';
			        $dataMailA['content_body']  = $password_message;
			        $dataMailA['button_url']  	= '';
			        $dataMailA['button_text']   = 'Create Password';
			        $dataMailA['attach_file']   = '';
			        send_mail_template($dataMailA);
					$mail['lender'] = 1; //$this->email->send(); 
					$status = true;
					$mail['message'] = '<div class="alert alert-success alert-dismissible">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Success!</strong> Username send successfully.
							</div>';
				}
				else
				{
					$status = false;
					$mail['message'] = '<div class="alert alert-danger alert-dismissible">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Danger!</strong> Username send failed.
							</div>';
				}	
			}
			else
			{
				$status = false;
				$mail['message'] = '<div class="alert alert-danger alert-dismissible">
						  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  <strong>Danger!</strong> Username send failed.
						</div>';
			}
		}
		else
		{
			$status = false;
			$mail['message'] = '<div class="alert alert-danger alert-dismissible">
						  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  <strong>Danger!</strong> Username send failed.
						</div>';
		}

		$mail['status'] = $status;
		
	 	echo json_encode($mail);
	}


	public function sendLinkToForgetPassword()
	{
		$this->load->library('email');
		$this->email->initialize(SMTP_SENDGRID);

		$mail = array();

		if(!empty($this->input->post('contact_id')))
		{
			$contact_id = $this->input->post('contact_id');
			$user_type = $this->input->post('user_type');

			$contact = $this->User_model->select_where('contact', array('contact_id'=>$contact_id));

			$contact = $contact->row();


			if($user_type==0)
			{
				$lander = $this->User_model->select_where('lender_contact_type', array('contact_id'=>$contact_id));

				$lander = $lander->row();

				$lander_id = $lander->lender_contact_id;

				if(!empty($lander_id) && !empty($contact->contact_email) && $user_type==0)
				{
					
					$email_address = $contact->contact_email;
					$contact_id  = $contact->contact_id;
					//$name 		 = $fetch_email[0]->contact_firstname.' '.$fetch_email[0]->contact_lastname;
					$name 		 = $contact->contact_firstname;

					$getusername = $this->User_model->query("Select username from lender_contact_type where contact_id = '".$contact_id."' AND lender_id > 0");
					$getusername = $getusername->row();
					$Username = '';
					if($getusername){
						$Username = $getusername->username;
					}


					$Passwordcode   = $this->generate_pwd(10);

					//$Updated_password = $this->User_model->query("Update lender_contact_type set password = '".$Passwordcode."', c_password = '".$Passwordcode."' where contact_id = '".$contact_id ."'");
					if(!$Username){
						$Username = $this->input->post('username');
					}

					$reset_link = Lender_url.'Home/createPassword/'.$contact_id.'/'.$Username.'/'.$this->generate_pwd(15);
					$password_message = '				
								<p style="font-size: 14px;">Dear '.$name.',
									<br>
									Username : '.$Username.'
									<br>You may reset your Username and Password by following the link below. Should you have any questions, please reach out to Investor Relations.</p>
																
							';


					$dataMailA = array();
			        $dataMailA['from_email']  	= 'invest@talimarfinancial.com';
			        $dataMailA['from_name']   	= 'TaliMar Financial';
			        $dataMailA['to']      		= $email_address;
			        $dataMailA['cc']      		= '';
			        $dataMailA['bcc']      		= '';
			        $dataMailA['name']      	= '';
			        $dataMailA['subject']     	= 'Password Reset';
			        $dataMailA['content_body']  = $password_message;
			        $dataMailA['button_url']  	= $reset_link;
			        $dataMailA['button_text']   = 'Create Password';
			        $dataMailA['attach_file']   = '';
			        send_mail_template($dataMailA);
					
					/*$this->email->from('invest@talimarfinancial.com', 'TaliMar Financial');
					$this->email->to($email_address);
					$this->email->subject('Username / Password Reset');
					$this->email->message($password_message);
					$this->email->set_mailtype('html');*/ 
					$mail['lender'] = 1; //$this->email->send(); 

					$status = true;
						$mail['message'] = '<div class="alert alert-success alert-dismissible">
						  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  <strong>Success!</strong> Change password link send successfully.
						</div>';	
				}
				else
				{
					$status = false;
					$mail['message'] = '<div class="alert alert-danger alert-dismissible">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Danger!</strong> Change password link send failed.
							</div>';
				}	
			}
			elseif($user_type==1)
			{
				$borrower = $this->User_model->select_where('borrower_contact_type', array('contact_id'=>$contact_id));

				$borrower = $borrower->row();

				$borrower_id = $borrower->borrower_contact_id;

				if(!empty($borrower_id) && !empty($contact->contact_email) && $user_type==1)
				{

						$email_address = '';
						$contact_id  = '';
						$name 		 = '';
						$borrower_username 		 = '';
						$email_address = $contact->contact_email;
						if($contact){							
							$contact_id  = $contact->contact_id;
							$name 		 = $contact->contact_firstname;
							//$borrower_username = $contact->borrower_username;
						}

						$borrowerQal = $this->User_model->query("Select * from borrower_contact_type where contact_id = '".$contact_id."'");
						$borrowerData = $borrowerQal->result();

						if($borrowerData){
							$borrower_username = $borrowerData[0]->borrower_username;
						}

						if(!$borrower_username){
							$borrower_username = $this->input->post('username');
						}
						
						$resetLINK = Borrower_portal.'forgot-password-secure/'.$this->encodeURL($contact_id);

						$Message = '<p style="font-size: 14px;">Hello '.$name.',
										<br>
										Username : '.$borrower_username.'
										<br>ou may reset your Username and Password by following the link below. Should you have any questions, please reach out to Investor Relations.
										
								</p>';
							
							/*$this->email->from('noreplay@talimarfinancial.com', 'TaliMar Financial');
							$this->email->to($email_address);
							$this->email->subject('Username / Password Reset');
							$this->email->message($Message);
							$this->email->set_mailtype('html'); */


							$dataMailA['from_email']  	= 'invest@talimarfinancial.com';
					        $dataMailA['from_name']   	= 'TaliMar Financial';
					        $dataMailA['to']      		= $email_address;
					        $dataMailA['cc']      		= '';
					        $dataMailA['bcc']      		= '';
					        $dataMailA['name']      	= '';
					        $dataMailA['subject']     	= 'Password Reset';
					        $dataMailA['content_body']  = $Message;
					        $dataMailA['button_url']  	= $resetLINK;
					        $dataMailA['button_text']   = 'Reset Your Password';
					        $dataMailA['attach_file']   = '';
					        send_mail_template($dataMailA);

							$mail['borrower'] = 1 ;//$this->email->send(); 


							$status = true;
							$mail['message'] = '<div class="alert alert-success alert-dismissible">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Success!</strong> Change password link send successfully.
							</div>';							
				}
				else
				{
					$status = false;
					$mail['message'] = '<div class="alert alert-danger alert-dismissible">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <strong>Danger!</strong> Change password link send failed.
							</div>';
				}
			}
			else
			{
				$status = false;
				$mail['message'] = '<div class="alert alert-danger alert-dismissible">
						  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  <strong>Danger!</strong> Change password link send failed.
						</div>';
			}			
		}
		else
		{

			$status = false;
			$mail['message'] = '<div class="alert alert-danger alert-dismissible">
						  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  <strong>Danger!</strong> Change password link send failed.
						</div>';
		}

		$mail['status'] = $status;
		
	 	echo json_encode($mail);
	
	} 

	public function encodeURL($string){
		$encode = base64_encode(urlencode($string*12345678)/12345);
		return $encode;
	}

	public function generate_pwd($length) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKL0123456789";
        return substr(str_shuffle($chars), 0, $length);
    }
    public function checkUnique()
    {
        $post = $this->input->post();

        if(!empty($post['email']))
        {
            $where1 = 'email = "'.$post['email'].'"';
            $emailCount = $this->CM->datacount('tbl_vendor', $where1);
               
        }
        else
        {
            $emailCount = 0;
        }

        $arr = array("emailCount"=>$emailCount);

        echo json_encode($arr, true);
    }

    function contact_modal_popups(){
    	$contact_id = $this->input->post('contact_id');
    	$modal_page = $this->input->post('modal');
    	$data = $this->contactData($contact_id);
    	echo $this->load->view('contact/models/'.$modal_page,$data,true);
    }
    function contact_note_list(){
    	$contact_id=$this->input->post('contact_id');
    	$limit = 10;
    	$page_no=$this->input->post('page_no');
    	// $sql_notes = $this->User_model->query("SELECT * FROM contact_notes WHERE contact_id = '".$contact_id."' ORDER by notes_date DESC LIMIT $limit offset $offset ");
    	/*16-07-2021 by@aj*/
		$offset = ($page_no - 1) * $limit;
		if($page_no > 2){
			//$data['chk'] = $offset;
			$sql_notes = $this->User_model->query("SELECT * FROM contact_notes WHERE contact_id = '".$contact_id."' ORDER by notes_date DESC LIMIT $limit offset $offset ");
		}else{
			//$data['chk'] = 2;
			$sql_notes = $this->User_model->query("SELECT * FROM contact_notes WHERE contact_id = '".$contact_id."' ORDER by notes_date DESC");
		}
		/*---------*/    	
    	
		$fetch_contact_notes = $sql_notes->result();
		$data['fetch_contact_notes'] = $fetch_contact_notes;
		$data['listAllProperty'] = $this->listAllProperty();
		$data['contact_purpose_menu'] = $this->config->item('contact_purpose_menu');
		$sql_users = $this->User_model->query("SELECT * FROM user");
		$sql_users = $sql_users->result();
		foreach($sql_users as $row){
		 	$fetch_userss_data[$row->id]= $row->fname.' '.$row->lname;
		}
		$fetch_property = $this->User_model->select_star('loan_property');
		$fetch_property = $fetch_property->result();
		foreach($fetch_property as $row){
			
			$property[] = array(
								'id' => $row->loan_id,	
								'text' => $row->property_address,	
								
								);
		}
		$data['property'] = $property;
		$data['fetch_userss_data']=$fetch_userss_data;
		echo $this->load->view('contact/ajax_contact_note_data',$data,true);
		
    }

    public function add_task($val){

    	$data['val'] = $val;
    	$data['user_id'] = $this->session->userdata('t_user_id');
		$data['content'] = $this->load->view('tasks/task',$data,true);
		$this->load->view('template_files/template',$data);
    }

 //    public function task_form($val=false) {
 //    	// print_r($this->input->post());
 //    	error_reporting(0);
 //    	$result = false;
 //    	if($val == false){
 //    		$this->session->set_flashdata('error', 'Something went wrong !');
 //    		redirect(base_url('task_view'));
 //    	}else{
 //    		// echo $this->uri->segment(3);
 //    		$status = $this->input->post('status');
	// 		$date_ent = $this->input->post('date_entered');
	// 		$date_entered = $date_entered[0];

	// 		$data['data_f'] = $status.'{date is }'. $date_entered;
	// 		$data['user_id'] = $this->session->userdata('t_user_id');
	// 		if($val == 1){
	// 			echo 'insert';
	// 			//$Result = $this->CM->insertdata('lender_mortgage', $array3);
	// 			$msg = "Insert Successfully";
	// 			// $this->data();
	// 		}else{
	// 			echo 'update';
	// 			//$Result = $this->CM->updateWithOutAffdata('lender_mortgage', $array3);
	// 			$msg = "update_id1 Successfully";
	// 		}
 //    	}
    	
 //    	if($result){
	// 		$this->session->set_flashdata('success',$msg);                 
	// 	}else{
	// 		$this->session->set_flashdata('error', 'Something went wrong !');   
	// 	}
    	
	// 		$data['content'] = $this->load->view('tasks/task',$data,true);
	// 		$this->load->view('template_files/template',$data);
    	
		
		
	// }
	
}