<?php
class Loan_overview extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('User_model');
		$this->load->library('Aws3','aws3');
		
		 $this->load->library('session');
		 $this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');

		 
		if($this->session->userdata('t_user_id') != '' )
		{
		}
		else
		{
			redirect(base_url()."home");
		}
	}
	
	public function index()
	{ 
		error_reporting(0);
		

		
		//  SEARCH OPTIONS
		$data['user_role'] = $this->session->userdata('user_role');
	
		$input_data_closing_statement	= $this->config->item('input_data_closing_statement');
		
		if($this->input->post('search_by')){
			$option_select = $this->input->post('search_by');
			// echo $option_select;
			if($option_select == 1)
			{
				$fetch_option_data = $this->User_model->select_star('loan');
				$fetch_option_data = $fetch_option_data->result();
				foreach($fetch_option_data as $row)
				{
					$fetch_search_option[] = array(
													'id' => $row->id,
													'text' => $row->talimar_loan
													);
				}
				$data['fetch_search_option'] = $fetch_search_option;
				$data['selected_by'] = 1;
			}
			else if($option_select == 2)
			{
				$fetch_option_data = $this->User_model->query('SELECT * from loan WHERE fci != "" ');
				$fetch_option_data = $fetch_option_data->result();
				foreach($fetch_option_data as $row)
				{
					$fetch_search_option[] = array(
													'id' => $row->id,
													'text' => $row->fci
													);
				}
				$data['fetch_search_option'] = $fetch_search_option;
				$data['selected_by'] = 2;
			}
			else if($option_select == 3)
			{
				$fetch_property_search = $this->User_model->select_star_desc('loan_property','id');
				$fetch_property_search = $fetch_property_search->result();
				foreach($fetch_property_search as $row)
				{
					$p_ta_loan['talimar_loan'] 	= $row->talimar_loan;
					$property_search 			= $this->User_model->select_where('loan',$p_ta_loan);
					$property_search 			= $property_search->result();
					
					$fetch_search_option[] = array(
													'id' => $property_search[0]->id,
													'text' => $row->property_address
													);
				}
				$data['fetch_search_option'] = $fetch_search_option;
				$data['selected_by'] = 3;
			}
			else if($option_select == 4) {
				$fetch_all_contact 			= $this->User_model->query("SELECT * FROM contact WHERE borrower_id != ''");
				$fetch_all_contact 			= $fetch_all_contact->result();
		
				foreach($fetch_all_contact as $c_row)
				{
					
					$loan_search			= $this->User_model->query("SELECT loan.id FROM loan JOIN borrower_data as b JOIN borrower_contact as bc on bc.borrower_id = b.id AND b.id = loan.borrower WHERE bc.borrower_id = '".$c_row->borrower_id."' ");
					$loan_search 			= $loan_search->row();
					
					
					$fetch_search_option[] = array(
													'id' => $loan_search->id,
													'text' => $c_row->contact_firstname.' '.$c_row->contact_middlename.' '.$c_row->contact_lastname,
													);
					
				}
			
				$data['fetch_search_option'] = $fetch_search_option;
				$data['selected_by'] = 4;
			}
			else if($option_select == 5) {
				$fetch_all_borrower 			= $this->User_model->select_star('borrower_data');
				$fetch_all_borrower 			= $fetch_all_borrower->result();
				foreach($fetch_all_borrower as $row)
				{
					$p_ta_loan['borrower'] 		= $row->id;
					$borrower_search 			= $this->User_model->select_where('loan',$p_ta_loan);
					$borrower_search 			= $borrower_search->row();
					
					$fetch_search_option[] = array(
													'id' => $borrower_search->id,
													'text' => $row->b_name
													);
				}
				/* foreach($fetch_all_borrower as $row)
				{
					$fetch_search_option[] = array(
													'id' => $row->id,
													'text' => $row->b_name,
													);
				} */
			
				$data['fetch_search_option'] = $fetch_search_option;
				$data['selected_by'] = 5;
			}
		}
		else
		{
			$fetch_option_data = $this->User_model->select_star('loan');
			$fetch_option_data = $fetch_option_data->result();
			foreach($fetch_option_data as $row)
			{
				$fetch_search_option[] = array(
												'id' => $row->id,
												'text' => $row->talimar_loan
												);
			}
			$data['fetch_search_option'] = $fetch_search_option;
			$data['selected_by'] = 1;
		}
		
		//  END SEARCH OPTION
		
		
		$loan_id = $this->uri->segment(2);
		if($loan_id)
		{
			
			$loan_id 					= $this->uri->segment(2);
			$fetch['id']				= $loan_id;
			$fetch_loan  				= $this->User_model->select_where('loan',$fetch);
			if($fetch_loan->num_rows() == 1)
			{
				
				$fetch_loan_result  		= $fetch_loan->result();
				$data['fetch_loan_result'] 	= $fetch_loan_result;
				foreach($fetch_loan_result as $row)
				{
					
					$data['modal_talimar_loan'] 		= $row->talimar_loan;
					$data['modal_priority'] 			= $row->position;
					$data['modal_intrest_rate'] 		= number_format($row->intrest_rate,3);
					$data['modal_org_amount'] 			= $row->loan_amount;
					$data['modal_current_balance'] 		= $row->current_balance;
					$data['modal_maturity_date'] 		= $row->maturity_date;
					$data['modal_baloon_payment_type'] 	= $row->baloon_payment_type;
					$data['modal_ballon_payment'] 		= $row->ballon_payment;
					
					
					$talimar_borrower_id 		= $row->borrower;
					$talimar_loan_number 		= $row->talimar_loan;
					$position_number 			= $row->position;
					
					// Check impound_account data
					// $set_impund_account_values  = $this->load_impound_account_data($talimar_loan_number);
					// Check closing_statement_load_items data
					// $closing_statement1 = $this->load_closing_statement_data($talimar_loan_number);
					
					// Check input_datas_data data
					// $input_datas_data1 	= $this->input_datas_data($talimar_loan_number,$input_data_closing_statement,'closing_statement');
					$loan_borrower_id 			= $row->borrower;
				}
				
				
			}
			else
			{
				$this->session->set_flashdata('error', 'Not Found');
				redirect(base_url().'load_data');
			}
			
		}
		else
		{
			$data['modal_talimar_loan'] 		= '';
			$loan_id 							= '';
			$talimar_loan_number 				= '';
		}
		
		$talimar_loan_id['talimar_loan'] 	= $data['modal_talimar_loan'];
		//--------Fetch data from table loan_property----------//
		// Count total property home with prroperty_address...
		
		// Count total property home with prroperty_address...
		
		$fetch_total_property_home = $this->User_model->query("SELECT COUNT(*) as total_row FROM property_home WHERE talimar_loan = '".$data['modal_talimar_loan']."' AND property_address IS NOT NULL ");
		
		$result_total_property_home = $fetch_total_property_home->row();
			
		$data['fetch_total_property_home'] = $result_total_property_home->total_row;
		
		// Fetch property home
		$fetch_property_home = $this->User_model->select_where_asc('property_home',$talimar_loan_id,'primary_property');
		
		
		if($fetch_property_home->num_rows() > 0)
		{
			$property_home = $fetch_property_home->result();
			$data['fetch_property_home'] = $fetch_property_home->result();
		}
		
		if(isset($property_home))
		{
			// $property_loan['talimar_loan'] 		= $data['modal_talimar_loan'];
			// $fetch_table_loan_property 			= $this->User_model->select_where('loan_property',$property_loan);
			// $loan_property_result 				= $fetch_table_loan_property->result();
			foreach($property_home as $row)
			{
				$fetch_table_loan_property 	= $this->User_model->select_where('loan_property',array('property_home_id'=>$row->id));
				if($fetch_table_loan_property->num_rows() > 0)
				{
					$loan_property_result[$row->id] 		= $fetch_table_loan_property->result();
				}
				
				//-------------------Fetch eucmbrances data---------------
				$fetch_ecumbrances 			= $this->User_model->query("SELECT * FROM encumbrances WHERE talimar_loan = '".$talimar_loan_number."' AND property_home_id = '".$row->id."' order by position");
				$fetch_ecumbrances 			= $fetch_ecumbrances->result();
				$ecumbrances_data[$row->id] = $fetch_ecumbrances;
				
				
				// Fetch _monthly income
				$fetch_monthly_income = $this->User_model->select_where('monthly_income',array('talimar_loan'=>$talimar_loan_number,'property_home_id'=>$row->id));
				if($fetch_monthly_income->num_rows() > 0)
				{
					$fetch_monthly_income_result[$row->id] = $fetch_monthly_income->result();
				}
				
				// Fetch Expense income
				$fetch_monthly_expense = $this->User_model->select_where('monthly_expense',array('talimar_loan'=>$talimar_loan_number,'property_home_id'=>$row->id));
				if($fetch_monthly_expense->num_rows() > 0)
				{
					$fetch_monthly_expense_result[$row->id] = $fetch_monthly_expense->result();
				}
				
				// FEtch Comparable_sale data
		
				$fetch_comparable_sale = $this->User_model->select_where_asc('comparable_sale',array('talimar_loan'=>$talimar_loan_number,'property_home_id'=>$row->id),'id');
				if($fetch_comparable_sale->num_rows() > 0)
				{
					$fetch_comparable_sale_result[$row->id] = $fetch_comparable_sale->result();
				}
				
				// fetch propoerty_input_fields
				$fetch_propoerty_input_fields = $this->User_model->select_where('propoerty_input_fields',array('talimar_loan'=>$talimar_loan_number,'property_home_id'=>$row->id));
				if($fetch_propoerty_input_fields->num_rows() > 0)
				{
					$fetch_propoerty_input_fields = $fetch_propoerty_input_fields->result();
					foreach($fetch_propoerty_input_fields as $p_i_f)
					{
						$data['propoerty_input_fields'][$row->id][$p_i_f->input_field] = $p_i_f->input_value;
					}
				}
				
				//--------Fetch data from table loan_property_insurance----------//
				$loan_property_insurance_type = $this->config->item('loan_property_insurance_type');
				foreach($loan_property_insurance_type as $insurance_key => $insurance_row)
				{
					$fetch_table_loan_property_insurance = $this->User_model->select_where('loan_property_insurance',array('talimar_loan'=>$talimar_loan_number,'property_home_id'=>$row->id,'insurance_type'=>$insurance_key));
					if($fetch_table_loan_property_insurance->num_rows() > 0)
					{
						$data['loan_property_insurance'][$row->id][$insurance_key] = $fetch_table_loan_property_insurance->row();
					}
				}
				
				// fetch loan_property_taxes
				$fetch_loan_property_taxes = $this->User_model->select_where('loan_property_taxes',array('talimar_loan'=>$talimar_loan_number,'property_home_id'=>$row->id));
				if($fetch_loan_property_taxes->num_rows() > 0)
				{
					$data['loan_property_taxes'][$row->id] = $fetch_loan_property_taxes->row();
				}
				
				
				
				// fetch property_folders
				$fetch_property_folders = $this->User_model->select_where('property_folders',array('talimar_loan'=>$talimar_loan_number,'property_home_id'=>$row->id));
				if($fetch_property_folders->num_rows() > 0)
				{
					$property_folders_result = $fetch_property_folders->result();
					$data['property_folders'][$row->id] = $property_folders_result;
					foreach($property_folders_result as $row_p_f)
					{
						// fetch loan_property_images
						$fetch_loan_property_images = $this->User_model->select_where('loan_property_images',array('talimar_loan'=>$talimar_loan_number,'property_home_id'=>$row->id,'folder_id'=>$row_p_f->id));
						if($fetch_loan_property_images->num_rows() > 0)
						{
							$data['loan_property_images'][$row->id][$row_p_f->id] = $fetch_loan_property_images->result();
						}
					}
				}
				
			}
			
			$data['loan_property_result'] 		= $loan_property_result;
			$data['ecumbrances_data'] 			= $ecumbrances_data;
			if(isset($fetch_monthly_income_result))
			{
				$data['fetch_monthly_income'] 			= $fetch_monthly_income_result;
			}
			
			if(isset($fetch_monthly_expense_result))
			{
				$data['fetch_monthly_expense'] 			= $fetch_monthly_expense_result;
			}
			
			if(isset($fetch_comparable_sale_result))
			{
				$data['fetch_comparable_sale'] 	= $fetch_comparable_sale_result;
			}
			
			
			
			
		}
		
		
		
		
		//-----------Fetch table loan_monthly_payment  Data ---
		$monthly_payment['talimar_loan'] 	= $data['modal_talimar_loan'];
		$fetch_monthy_payment				= $this->User_model->select_where('loan_monthly_payment',$monthly_payment);
		$result_monthly_payment 			= $fetch_monthy_payment->result();
		//-------------------------------------------------------------
		
		//-------------------Fetch Total amount from loan_monthly_payment table --
		$sql = "SELECT SUM(amount) as amount FROM loan_monthly_payment WHERE talimar_loan = '".$data['modal_talimar_loan']."'";
		$amount_monthly_payment 		= $this->User_model->query($sql);
		$amount_monthly_payment 		= $amount_monthly_payment->result();
		
		$data['amount_monthly_payment'] = $amount_monthly_payment;
		
		//----------------------------------------------------------------------
		
		
		
		$fetch_encumbrance_data1  = $this->User_model->select_where('encumbrances_data',$talimar_loan_id);
		if($fetch_encumbrance_data1->num_rows() > 0)
		{
			$data['fetch_encumbrance_data_table'] = $fetch_encumbrance_data1->row();
		}
		
		$sql = "SELECT SUM(  `original_balance` ) AS sum_original, SUM(  `current_balance` ) AS sum_current, SUM(  `monthly_payment` ) AS sum_month, SUM(  `ballon_payment_b` ) AS sum_ballon FROM  `encumbrances` WHERE talimar_loan =  '".$talimar_loan_number."' AND  position = '1' AND senior_to_talimar = '1' ";
		// echo "SELECT SUM(  `original_balance` ) AS sum_original, SUM(  `current_balance` ) AS sum_current, SUM(  `monthly_payment` ) AS sum_month, SUM(  `ballon_payment_b` ) AS sum_ballon FROM  `encumbrances` WHERE talimar_loan =  '".$talimar_loan_number."' AND  position = '1' AND senior_to_talimar = '1' ";
		$fetch_senior_tali = $this->User_model->query($sql);
		if($fetch_senior_tali->num_rows() > 0)
		{
			$data['fetch_senior_tali'] = $fetch_senior_tali->result();
		}
		
		$fetch_encumbrance_totals1  = $this->User_model->select_where('ecumbrances_totals',$talimar_loan_id);
		if($fetch_encumbrance_totals1->num_rows() > 0)
		{
			$data['fetch_encumbrance_totals'] = $fetch_encumbrance_totals1->row();
		}

		//---------------------------------------------------------
		
		//-------------------Fetch current eucmbrances data---------------
		$fetch_current_ecumbrances 			= $this->User_model->select_where_asc('current_encumbrances',$talimar_loan_id,'priority,id');
		if($fetch_current_ecumbrances->num_rows() > 0)
		{
			$fetch_current_ecumbrances 	= $fetch_current_ecumbrances->result();
			$data['fetch_current_ecumbrances'] 	= $fetch_current_ecumbrances;
		}
		
		//-------------------Fetch current eucmbrances data---------------
		$fetch_ecumbrances_tali_fac 			= $this->User_model->select_where('encumbrances',array('talimar_loan'=>$talimar_loan_number,'lien_holder'=>'TaliMar Financial'));
		if($fetch_ecumbrances_tali_fac->num_rows() > 0)
		{
			$fetch_ecumbrances_tali_fac 	= $fetch_ecumbrances_tali_fac->result();
			$data['fetch_ecumbrances_tali_fac'] 	= $fetch_ecumbrances_tali_fac;
		}
		//-------------------Fetch closing_statement_items data---------------
		$fetch_closing_statement_items1 	= $this->User_model->select_where_asc('closing_statement_items',$talimar_loan_id,'hud');
		
		if($fetch_closing_statement_items1->num_rows() > 0)
		{
			$fetch_closing_statement_items1 	= $fetch_closing_statement_items1->result();
			$data['fetch_closing_statement_items_data'] 	= $fetch_closing_statement_items1;
		}
		
		//--------------------Fetch SUM closing_statement_item -----------------------
		$sql = "SELECT SUM(paid_to_other) as sum_paid_to_other, SUM(paid_to_broker) as sum_paid_to_broker FROM closing_statement_items WHERE talimar_loan = '".$talimar_loan_number."' ";
		
		$fetch_sum_closing_statement_item = $this->User_model->query($sql);
		$data['fetch_sum_closing_statement_item'] = $fetch_sum_closing_statement_item->result(); 
		
		//--------------------Fetch SUM closing_statement_item for direct_to_servicer checked...  -----------------------
		
		$fetch_sum_closing_statement_item = $this->User_model->query("SELECT SUM(paid_to_other) as sum_paid_to_other, SUM(paid_to_broker) as sum_paid_to_broker FROM closing_statement_items WHERE talimar_loan = '".$talimar_loan_number."' AND direct_to_servicer = '1' ");
		
		if($fetch_sum_closing_statement_item->num_rows() > 0)
		{
				$data['fetch_suspense_balance'] = $fetch_sum_closing_statement_item->result(); 
		}else{
			
			$fetch_sum_closing_statement_item = $this->User_model->query("SELECT SUM(paid_to_other) as sum_paid_to_other, SUM(paid_to_broker) as sum_paid_to_broker FROM closing_statement_items WHERE talimar_loan = '".$talimar_loan_number."' AND hud IN(813,814)");
			
			
			$data['fetch_suspense_balance'] = $fetch_sum_closing_statement_item->result();
		
		}
		
		//--------------------Fetch SUM closing_statement_item -----------------------
		$fetch_input_datas1 = $this->User_model->select_where('input_datas',array('talimar_loan'=>$talimar_loan_number,'page'=>'closing_statement'));
		if($fetch_input_datas1->num_rows() > 0)
		{
			$fetch_input_datas	 = $fetch_input_datas1->result();
			foreach($fetch_input_datas as $key => $row)
			{
				$data['fetch_closing_input_datas'][$row->input_name] = $row->value;
			}
			
		}
		
		// -------------------- Fetch senior_ecumbrances ----------------
		$fetch_senior_ecumbrances = $this->User_model->select_where('senior_ecumbrances',array('talimar_loan'=>$talimar_loan_number));
		if($fetch_senior_ecumbrances->num_rows() > 0)
		{
			$data['senior_ecumbrances'] = $fetch_senior_ecumbrances->row();
		}
		
		//--------------------Fetch SUM closing_statement_item -----------------------
		$fetch_all_input_datas1 = $this->User_model->select_where('input_datas',array('talimar_loan'=>$talimar_loan_number));
		if($fetch_all_input_datas1->num_rows() > 0)
		{
			$fetch_all_input_datas	 = $fetch_all_input_datas1->result();
			foreach($fetch_all_input_datas as $key => $row)
			{
				$data['fetch_all_input_datas'][$row->page][$row->input_name] = $row->value;
			}
			
		}
		
		
		
		$sql = "SELECT SUM(current_balance) as sum_current_balance, SUM(original_balance) as sum_original_balance, SUM(monthly_payment) as sum_monthly_payment, SUM(ballon_payment_amount) as sum_ballon_payment_amount from current_encumbrances WHERE talimar_loan = '".$talimar_loan_number."' ";
		
		$fetch_sum_current_ecum = $this->User_model->query($sql);
		if($fetch_sum_current_ecum->num_rows() > 0)
		{
			$data['fetch_sum_current_ecum'] = $fetch_sum_current_ecum->result();
		}
		//---------------------------------------------------------
		
		
		//------------------Fetch closing_statement data -------------
		$fetch_closing_statement 		= $this->User_model->select_where_asc('closing_statement',$talimar_loan_id,'position');
		
		$fetch_closing_statement 		= $fetch_closing_statement->result();
		
		$data['closing_statement_data'] = $fetch_closing_statement;
		//------------------------------------------------------------
		
		$fetch_closing_deduction = $this->User_model->select_where('closing_statement_deductions',$talimar_loan_id);
		if($fetch_closing_deduction->num_rows() > 0)
		{
			$data['fetch_closing_deduction'] = $fetch_closing_deduction->result();
		}
		
		//------------------Fetch extra_details data -------------
		$fetch_extra_details 			= $this->User_model->select_where('extra_details',$talimar_loan_id);
		
		$fetch_extra_details 			= $fetch_extra_details->result();
		
		$data['fetch_extra_details'] 	= $fetch_extra_details;
		//------------------------------------------------------------
		
			//------------------Fetch ach_deposite data -------------
		$fetch_ach_deposite_details 		= $this->User_model->select_where('ach_deposite',$talimar_loan_id);
		
		$fetch_ach_deposite_details 		= $fetch_ach_deposite_details->result();
		
		$data['fetch_ach_deposite_details'] = $fetch_ach_deposite_details;
		//------------------------------------------------------------
		
		//------------------Fetch loan_escrow data--------------------
		
		$fetch_escrow_data 			= $this->User_model->select_where('loan_escrow',$talimar_loan_id);
		$fetch_escrow_data 			= $fetch_escrow_data->result();
		
		$data['fetch_escrow_data'] 	= $fetch_escrow_data;
		//------------------------------------------------------------
		
		//-----------------Fetch loan_title-----------------------------------------
		$fetch_title_data 			= $this->User_model->select_where('loan_title',$talimar_loan_id);
		$fetch_title_data 			= $fetch_title_data->result();
		
		$data['fetch_title_data'] 	= $fetch_title_data;
		
		
		//-----------------Fetch accured_charges data -----------------------------------------
		$fetch_accured_data 			= $this->User_model->select_where('accured_charges',$talimar_loan_id);
		$fetch_accured_data 			= $fetch_accured_data->result();
		
		$data['fetch_accured_data'] 	= $fetch_accured_data;
		
		//------------------ fetch wire instruction data ------------
		
		$fetch_wire_instruction  	= $this->User_model->select_where('wire_instruction',array('talimar_loan'=>$talimar_loan_number,'loan_id'=>$loan_id));
		
		if($fetch_wire_instruction->num_rows() > 0)
		{
			$data['fetch_wire_instruction'] = $fetch_wire_instruction->result();
		}
		//------------------ fetch draw wire instruction data ------------
		
		$fetch_draw_wire_instruction  	= $this->User_model->select_where('draw_wire_instructions',array('talimar_loan'=>$talimar_loan_number,'loan_id'=>$loan_id));
		
		if($fetch_draw_wire_instruction->num_rows() > 0)
		{
			$data['fetch_draw_wire_instruction'] = $fetch_draw_wire_instruction->result();
		}
		

		
		// FEtch borrower_data according to loan
		$loan_borrower_id_data['id'] 	= $loan_borrower_id;
		$fetch_loan_borrower 			= $this->User_model->select_where('borrower_data',$loan_borrower_id_data);
		$fetch_loan_borrower 			= $fetch_loan_borrower->result();
		$data['fetch_loan_borrower'] 	= $fetch_loan_borrower;
		//---------------------------------------------------------------------------
		
		//---------------Fetch Borower_data table detail ----------------------------
		$fetch_borrower_data 			= $this->User_model->select_star_asc('borrower_data','b_name');
		$fetch_borrower_data 			= $fetch_borrower_data->result();
		
		$data['fetch_borrower_data'] 	= $fetch_borrower_data;
		
		//---------------------------------------------------------------------------
		
		//---------------Fetch loan_record_information table detail ----------------------------
		$recording_information 	= $this->User_model->select_where('loan_recording_information',$talimar_loan_id);
		$recording_information_data				= $recording_information->result();
		$data['recording_information'] 			= $recording_information_data;
		
		//---------------Fetch loan_source_and_uses table detail ----------------------------
		$fetch_loan_source_and_uses1 	= $this->User_model->select_where_asc('loan_source_and_uses',$talimar_loan_id,'id');
		if($fetch_loan_source_and_uses1->num_rows() > 0)
		{
			$fetch_loan_source_and_uses				= $fetch_loan_source_and_uses1->result();
			$data['fetch_loan_source_and_uses'] 	= $fetch_loan_source_and_uses;
		}
		
		// fetch sum of project cost from 'total_project_cost'  table...
		$fetch_total_project_cost 	= $this->User_model->query("SELECT SUM(project_cost) as total_project_cost FROM loan_source_and_uses WHERE talimar_loan = '".$data['modal_talimar_loan']."' GROUP by talimar_loan");
		$result_total_project_cost = $fetch_total_project_cost->row();
		
	
		if($fetch_total_project_cost->num_rows() > 0)
		{
			$data['total_project_cost_of_su'] = $result_total_project_cost->total_project_cost;
		}else{
			$data['total_project_cost_of_su'] = '';
		}
		//---------------------------------------------------------------------------
		
		// --------------Fetch servicing estimate lender data --------
		
		$servicing_estimate_lender  = $this->User_model->select_where_asc('servicing_estimate_lender',$talimar_loan_id,'position');
		
		if($servicing_estimate_lender->num_rows() > 0)
		{
		$servicing_estimate_lender = $servicing_estimate_lender->result();
		}
		else
		{
			$servicing_estimate_lender = '';
		}
		$data['servicing_estimate_lender'] = $servicing_estimate_lender;
		
		//------------------------------------------------------------
		
		
		
		//---------------Fetch loan_serviing table detail ----------------------------
		$loan_servicing_data 	= $this->User_model->select_where('loan_servicing',$talimar_loan_id);
		
		
			$loan_servicing_data					= $loan_servicing_data->result();
			$data['loan_servicing_data'] 			= $loan_servicing_data;

		//---------------------------------------------------------------------------
		
		//---------- Fetch impound_account data---------------------
		
		$fetch_impound_account = $this->User_model->select_where_asc('impound_account',$talimar_loan_id,'id');
		if($fetch_impound_account->num_rows() > 0)
		{
			$data['fetch_impound_account'] = $fetch_impound_account->result();
		}
		
		$i_m_sum_sql 	= "SELECT SUM(amount) as sum_amount FROM impound_account WHERE talimar_loan = '".$talimar_loan_number."' AND ( impounded = '1' OR items = 'Mortgage Payment' ) ";
		
		$fetch_sum_imp_acc = $this->User_model->query($i_m_sum_sql);
		if($fetch_sum_imp_acc->num_rows() > 0)
		{
			$fetch_sum_imp_acc = $fetch_sum_imp_acc->result();
			$data['sum_imp_acc'] = $fetch_sum_imp_acc[0]->sum_amount;
		}
		
		$sql = "SELECT SUM(amount) as sum_impounded FROM impound_account WHERE talimar_loan = '".$talimar_loan_number."' AND items != 'Mortgage Payment' ";
		// echo "SELECT SUM(amount) as sum_impounded FROM impound_account WHERE impounded = '1' AND talimar_loan = '".$talimar_loan_number."' AND items != 'Mortgage Payment'";
		$fetch_sum_impound_account = $this->User_model->query($sql);
		$fetch_sum_impound_account = $fetch_sum_impound_account->result();
		$data['sum_impound_account'] = $fetch_sum_impound_account[0]->sum_impounded;
		//---------------Fetch loan_assigment table detail ----------------------------
		$loan_assigment_data 	= $this->User_model->select_where_asc('loan_assigment',$talimar_loan_id,'position');
		
		$data['count_row_assigment'] 			= $loan_assigment_data->num_rows();
		
		$loan_assigment_data					= $loan_assigment_data->result();
		$data['loan_assigment_data'] 			= $loan_assigment_data;
		
		// Fetch all lender contacts from contact
		$where_contact_1['lender_contact_type'] = 1;
		$fetch_all_lender_contact = $this->User_model->select_where_asc('contact',$where_contact_1,'contact_firstname');
		if($fetch_all_lender_contact->num_rows() > 0)
		{
			$data['fetch_all_lender_contact'] = $fetch_all_lender_contact->result();
		}
		
		foreach($loan_assigment_data as $row)
		{
			$lender_id = $row->lender_name;
			$sql = "SELECT * FROM lender_contact JOIN contact ON lender_contact.contact_id = contact.contact_id WHERE lender_contact.lender_id = '".$lender_id."' ";
			$fetch_lender_email_contact1 = $this->User_model->query($sql);
			if($fetch_lender_email_contact1->num_rows() > 0)
			{
				$fetch_lender_email_contact = $fetch_lender_email_contact1->result();
				foreach($fetch_lender_email_contact as $contact_data)
				{
					$data['email_contact'][$lender_id][] = array(
																"contact_name" => $contact_data->contact_firstname.' '.$contact_data->contact_middlename.' '.$contact_data->contact_lastname,
																"title"			=> $contact_data->c_title,
																"email"			=> $contact_data->contact_email,
																"contact_id"	=>$contact_data->contact_id
																	);
				}
			}
			
		}
			$data['lender_name_acc_id'] 		= $lender_name_acc_id;
			//------------Fetch draws_loan table detail------------------
			
			//---------------Fetch loan_draws table detail ----------------------------
			// $sql_draws = "SELECT * FROM  `loan_draws` WHERE talimar_loan =  '".$talimar_loan_number."' ORDER BY  CAST( SUBSTR( draws FROM 6 ) AS UNSIGNED ) ASC ";
			$sql_draws = "SELECT * FROM  `loan_draws` WHERE talimar_loan =  '".$talimar_loan_number."' ORDER BY convert(`draws`, decimal) ASC ";
			
			$loan_draws_data 	= $this->User_model->query($sql_draws);
		// $loan_draws_data 	= $this->User_model->select_where_asc('loan_draws',$talimar_loan_id,ABS('draws'));
		
		  // fetch borrower details...
			
			$loan_borrower_data 	= $this->User_model->query("SELECT bd.b_name, bc.contact_id,c.contact_firstname , c.contact_lastname, c.contact_email  FROM borrower_data as bd JOIN borrower_contact as bc JOIN contact as c 
				ON bd.id = bc.borrower_id AND bc.contact_id = c.contact_id WHERE bd.id = '".$talimar_borrower_id."'");
		// echo "SELECT bd.b_name, bc.contact_id,c.contact_firstname , c.contact_lastname, c.contact_email  FROM borrower_data as bd JOIN borrower_contact as bc JOIN contact as c 
				// ON bd.id = bc.borrower_id AND bc.contact_id = c.contact_id WHERE bd.id = '".$talimar_borrower_id."'";
			$loan_draws_data				= $loan_draws_data->result();
			$data['loan_draws_data']		= $loan_draws_data;
			$loan_borrower_result			= $loan_borrower_data->row();
			$data['draws_borrower_name'] 			= $loan_borrower_result->b_name;	
			$data['draws_contact_name'] 			= $loan_borrower_result->contact_firstname.' '.$loan_borrower_result->contact_lastname;	
			$data['draws_contact_email'] 			= $loan_borrower_result->contact_email;
			//---------------Fetch fee_disbursement table detail ----------------------------
			$fee_disbursement_data = $this->User_model->select_where('fee_disbursement',$talimar_loan_id);
			if($fee_disbursement_data->num_rows() > 0)
			{
			$fee_disbursement_data				= $fee_disbursement_data->result();
			$data['fee_disbursement_data'] 		= $fee_disbursement_data;
			}
			
			// count number of assignment...
			$loan_assigment_count = $this->User_model->select_where('loan_assigment',$talimar_loan_id);
			if($loan_assigment_count->num_rows() > 0)
			{
				$data['loan_assigment_count'] 		= $loan_assigment_count->num_rows();
			}
		// FETCH PROPERT ALBUM DATA
		
		
		
		$fetch_property_album = $this->User_model->select_where_asc('property_album',$talimar_loan_id,'id');
		if($fetch_property_album->num_rows() > 0)
		{
			$fetch_property_album			= $fetch_property_album->result();
			$data['fetch_property_album'] 	= $fetch_property_album;
			
			foreach($fetch_property_album as $row)
			{
				$sql = 'SELECT * from property_images WHERE talimar_loan = "'.$row->talimar_loan.'" AND title = "'.$row->album_name.'" order by id';
				$fetch_image_album = $this->User_model->query($sql);
				$fetch_image_album = $fetch_image_album->result();
				
				$data['fetch_image_album_id'][$row->id] = $fetch_image_album;
			}
		}
		
		
		
		// Fetch Property Images 
		$where_user1['marketing']		= '0';
		$where_user1['title']			= 'under_writing';
		$where_user1['talimar_loan']	= $talimar_loan_number;
		$fetch_property_images_data = $this->User_model->select_where('property_images',$where_user1);
		if($fetch_property_images_data->num_rows() > 0)
		{
			$data['fetch_property_images'] = $fetch_property_images_data->result();
		}	
		
		$sql = 'SELECT * FROM property_images WHERE talimar_loan = "'.$talimar_loan_number.'" AND marketing = "1"  AND title="under_writing" ';
		$fetch_marketing_property_image1 = $this->User_model->query($sql);
		if($fetch_marketing_property_image1->num_rows() > 0)
		{
			$fetch_marketing_property_image = $fetch_marketing_property_image1->result();
			$data['fetch_marketing_property_image1'] = $fetch_marketing_property_image;
		}
		
		$sql = 'SELECT * FROM property_images WHERE talimar_loan = "'.$talimar_loan_number.'" AND marketing = "2" AND title="under_writing" ';
		$fetch_marketing_property_image2 = $this->User_model->query($sql);
		if($fetch_marketing_property_image2->num_rows() > 0)
		{
			$fetch_marketing_property_image2 = $fetch_marketing_property_image2->result();
			$data['fetch_marketing_property_image2'] = $fetch_marketing_property_image2;
		}
		// Fetch Property Images 
		$where_user['title']		= 'user';
		$where_user['talimar_loan']	= $talimar_loan_number;
		
		$fetch_property_images_user = $this->User_model->select_where('property_images',$where_user);
		if($fetch_property_images_user->num_rows() > 0)
		{
			$data['fetch_user_property_images'] = $fetch_property_images_user->result();
		}
		
		$sql = "SELECT * FROM property_images WHERE talimar_loan = '".$talimar_loan_number."' AND title = 'user' AND marketing = '1' ";
		$fetch_user_street_level = $this->User_model->query($sql);
		if($fetch_user_street_level->num_rows() > 0)
		{
			$fetch_user_street_level 			= $fetch_user_street_level->result();
			$data['fetch_user_street_level'] 	= $fetch_user_street_level;
		}
		
		$sql = "SELECT * FROM property_images WHERE talimar_loan = '".$talimar_loan_number."' AND title = 'user' AND marketing = '2' ";
		$fetch_user_area_map = $this->User_model->query($sql);
		if($fetch_user_area_map->num_rows() > 0)
		{
			$fetch_user_area_map 			= $fetch_user_area_map->result();
			$data['fetch_user_area_map'] 	= $fetch_user_area_map;
		}
			
		// FEtch Loan distribution data
		
		$fetch_distribution_data = $this->User_model->select_where_asc('loan_distribution',$talimar_loan_id,'id');
		if($fetch_distribution_data->num_rows() > 0)
		{
			$fetch_distribution_data = $fetch_distribution_data->result();
			$data['fetch_distribution_data'] = $fetch_distribution_data;
		}	
		
		
		// FETCH loan_document_docs data where doc_type  equal 1
		$sql = "SELECT * FROM loan_document_docs WHERE talimar_loan = '".$talimar_loan_number."' AND doc_type ='1' ";
		$fetch_loan_document_docs1 = $this->User_model->query($sql);
		if($fetch_loan_document_docs1->num_rows() > 0)
		{
			$fetch_loan_document_1 = $fetch_loan_document_docs1->result();
			$data['fetch_loan_document_1'] = $fetch_loan_document_1;
		}
		
		// FETCH loan_document_docs data where doc_type  equal 2
		$sql = "SELECT * FROM loan_document_docs WHERE talimar_loan = '".$talimar_loan_number."' AND doc_type ='2' ";
		$fetch_loan_document_docs2 = $this->User_model->query($sql);
		if($fetch_loan_document_docs2->num_rows() > 0)
		{
			$fetch_loan_document_2 			= $fetch_loan_document_docs2->result();
			$data['fetch_loan_document_2'] 	= $fetch_loan_document_2;
		}
		
		// FETCH loan_document_docs data where doc_type  equal 3
		$sql = "SELECT * FROM loan_document_docs WHERE talimar_loan = '".$talimar_loan_number."' AND doc_type ='3' ";
		$fetch_loan_document_docs3 = $this->User_model->query($sql);
		if($fetch_loan_document_docs3->num_rows() > 0)
		{
			$fetch_loan_document_3 			= $fetch_loan_document_docs3->result();
			$data['fetch_loan_document_3'] 	= $fetch_loan_document_3;
		}
		
		// FETCH loan_document_docs data where doc_type  equal 4
		$sql = "SELECT * FROM loan_document_docs WHERE talimar_loan = '".$talimar_loan_number."' AND doc_type = '4' ";
		$fetch_loan_document_docs4 = $this->User_model->query($sql);
		if($fetch_loan_document_docs4->num_rows() > 0)
		{
			$fetch_loan_document_4 			= $fetch_loan_document_docs4->result();
			$data['fetch_loan_document_4'] 	= $fetch_loan_document_4;
		}
		
		// FETCH loan_document_docs data where doc_type  equal 5
		$sql = "SELECT * FROM loan_document_docs WHERE talimar_loan = '".$talimar_loan_number."' AND doc_type = '5' ";
		$fetch_loan_document_docs5 = $this->User_model->query($sql);
		if($fetch_loan_document_docs5->num_rows() > 0)
		{
			$fetch_loan_document_5 			= $fetch_loan_document_docs5->result();
			$data['fetch_loan_document_5'] 	= $fetch_loan_document_5;
		}
		
		// FETCH loan_document_docs data where doc_type  equal 6
		$sql = "SELECT * FROM loan_document_docs WHERE talimar_loan = '".$talimar_loan_number."' AND doc_type = '6' ";
		$fetch_loan_document_docs6 = $this->User_model->query($sql);
		if($fetch_loan_document_docs6->num_rows() > 0)
		{
			$fetch_loan_document_6 				= $fetch_loan_document_docs6->result();
			$data['fetch_loan_document_6'] 		= $fetch_loan_document_6;
		}
		
		// FETCH loan_document_docs data where doc_type equal 0
		$sql = "SELECT * FROM loan_document_docs WHERE talimar_loan = '".$talimar_loan_number."' AND doc_type = '0' ";
		$fetch_loan_document_docs_0 = $this->User_model->query($sql);
		if($fetch_loan_document_docs_0->num_rows() > 0)
		{
			$fetch_loan_document_0 			= $fetch_loan_document_docs_0->result();
			$data['fetch_loan_document_0'] 	= $fetch_loan_document_0;
		}
		
		
		// Fetch Sum comparable_sale  
		$sql = "SELECT COUNT(*) as count_comparable, SUM(sq_ft) as sum_sq_ft, SUM(year_built) as sum_year_built, SUM(bedrooms) as sum_bedrooms, SUM(bathroom) as sum_bathroom, SUM(lot_size) as sum_lot_size, SUM(sale_price) as sum_sale_price, SUM(per_sq_ft) as sum_per_sq_ft FROM comparable_sale WHERE talimar_loan = '".$talimar_loan_number."' ";
		$fetch_sum_comparable_data 			= $this->User_model->query($sql);
		$fetch_sum_comparable_data 			= $fetch_sum_comparable_data->result();
		$data['fetch_sum_comparable_data']	= $fetch_sum_comparable_data;
		
		$sql = 'SELECT SUM(disbursement) as total_disbursement FROM loan_distribution WHERE talimar_loan = "'.$talimar_loan_number.'" ';
		$sum_total_disbursement			= $this->User_model->query($sql);	
		$sum_total_disbursement			= $sum_total_disbursement->result();	
		$total_sum_disbursement			= $sum_total_disbursement[0]->total_disbursement;	
		$data['total_sum_disbursement'] = $total_sum_disbursement;
			//---------------Fetch total of draws_amount in loan_draws table
		$sql = "SELECT SUM(draws_amount) as draws_amount , SUM(draws_release_amount) as  draws_release_amount , SUM(draws_remaining) as draws_remaining FROM loan_draws WHERE talimar_loan = '".$talimar_loan_number."'";
		$total_draws_amount = $this->User_model->query($sql);
		if($total_draws_amount->num_rows() > 0)
		{
		$total_draws_amount 				= $total_draws_amount->result();
		$data['total_draws_amount'] 		= $total_draws_amount[0]->draws_amount;
		$data['total_draws_release_amount'] = $total_draws_amount[0]->draws_release_amount;
		$data['total_draws_remaining'] 		= $total_draws_amount[0]->draws_remaining;
		}
		else
		{
			$data['total_draws_amount'] = '';
		} 
		//--------------------------fetch all data for Investor---------------------------------
		
		$investor_all_data 	= $this->User_model->select_star_asc('investor','name');
		
			$investor_all_data					= $investor_all_data->result();
			$data['investor_all_data'] 			= $investor_all_data;

		//---------------------------------------------------------------------------
		
		//---------------Fetch loan_notes table detail ----------------------------
		$loan_notes_fetch 	= $this->User_model->select_where_desc('loan_notes',$talimar_loan_id,'notes_date');
		
		if($loan_notes_fetch->num_rows() > 0)
		{
		$loan_notes_fetch				= $loan_notes_fetch->result();
		$data['loan_notes_fetch']		= $loan_notes_fetch;
		}
		else
		{
			$data['loan_notes_fetch']		= '';
		}
		//---------------------------------------------------------------------------
		
		// ----------Fetch borrower Contact detail---------------------
			
			
			$where_b_id['borrower_id'] 		= $fetch_loan_result[0]->borrower;
			$fetch_borrower_contact1		= $this->User_model->select_where('borrower_contact',$where_b_id);
			
			if($fetch_borrower_contact1->num_rows() > 0)
			{
				$fetch_borrower_contact = $fetch_borrower_contact1->result();
				// ---------------------- Borrower Contact 1 Section --------------------------------------------
				if($fetch_borrower_contact[0]->contact_id)
				{
					$b_con_id['contact_id']			= $fetch_borrower_contact[0]->contact_id;
					$fetch_b_contact				= $this->User_model->select_where('contact',$b_con_id);
					if($fetch_b_contact->num_rows() > 0)
					{
						$b_contact  								= $fetch_b_contact->result();
						$data['select_borrower_contact1'] 			= $b_contact[0]->contact_firstname.' '.$b_contact[0]->contact_middlename.' '.$b_contact[0]->contact_lastname;
						
						$data['select_borrower_contact1_email'] 	= $b_contact[0]->contact_email;
						$data['select_borrower_contact1_phone'] 	= $b_contact[0]->contact_phone;
						$data['select_borrower_contact1_title'] 	= $fetch_borrower_contact[0]->c_title;
					}
					 
				}
				// ---------------------- Borrower Contact 2 Section --------------------------------------------
				if($fetch_borrower_contact[1]->contact_id)
				{
					$b_con_id['contact_id']		= $fetch_borrower_contact[1]->contact_id;
					$fetch_b_contact			= $this->User_model->select_where('contact',$b_con_id);
					if($fetch_b_contact->num_rows() > 0)
					{
						$b_contact  								= $fetch_b_contact->result();
						$data['select_borrower_contact2'] 			= $b_contact[0]->contact_firstname.' '.$b_contact[0]->contact_middlename.' '.$b_contact[0]->contact_lastname;
						
						$data['select_borrower_contact2_email'] 	= $b_contact[0]->contact_email;
						$data['select_borrower_contact2_phone'] 	= $b_contact[0]->contact_phone;
						$data['select_borrower_contact2_title'] 	= $fetch_borrower_contact[1]->c_title;
					}
					 
				}
			}
		
		// fetch vendors details...
		
		
			$fetch_vendors_data 	= $this->User_model->query("SELECT * FROM vendors WHERE talimar_loan = '".$data['modal_talimar_loan']."' ");
			
			// echo "SELECT * FROM vendors WHERE talimar_loan = '".$data['modal_talimar_loan']."'";
			$fetch_vendors_data 		= $fetch_vendors_data->row();
			
			$data['vendor'] 			= $fetch_vendors_data;
		
		
			$result 					= $this->User_model->select_star('vendors');
			$result_new 				= $result->result();
			$data['all_vendor_data'] 	= $result_new;
		
		
		
		
		
		
		$user_data['t_user_id'] 	= $this->session->userdata('t_user_id');
		$result 					= $this->User_model->select_star('loan');
		$result_new 				= $result->result();
		
		$data['fetch_loans'] 		= $result_new;
		$data['result_monthly_payment'] = $result_monthly_payment;
		
		// $data['loan_id']	= $loan_id;
		
		$data['a'] = '1';
		$data['content'] = $this->load->view('load_data/load_index',$data,true);
		$this->load->view('template_files/template',$data);
		
	}

}

?>	