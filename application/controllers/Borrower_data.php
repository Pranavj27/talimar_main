<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Borrower_data extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		// $this->load->library('session');
		$this->load->model('User_model');
		$this->load->library('Aws3','aws3');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');
		if($this->session->userdata('t_user_id') == '' )
		{
			redirect(base_url()."home",'refresh');
		}

		error_reporting(0);
 		ini_set('memory_limit','-1');
		
	}
	
	//Borrower_data/randomkey
	public function randomkey(){

		$this->load->helper('string');
		echo random_string('alnum',20);

	}

	/*
		Description : This function use for Project details get  - update image functionality			  
		Author      : Bitcot
		Created     : 
		Modified    : 25-03-2021
	*/
	
	public function get_b_projects(){

		$id = $this->input->post('rowid');
		$getproject = $this->User_model->select_where('borrower_prior_project',array('id'=>$id));
		$getproject = $getproject->row();

		$getproject->hiddenImage = $getproject->projectImage;

		if($getproject->projectImage != '')
		{
			$getproject->projectImage = explode(',', $getproject->projectImage);
		}
		else
		{
			$getproject->hiddenImage = '';
			$getproject->projectImage = array();
		}

		echo json_encode($getproject);
	}

	public function get_b_projectsDelete(){

		$id = $this->input->post('rowid');

		$getproject = $this->User_model->select_where('borrower_prior_project',array('id'=>$id));
		$getproject = $getproject->row();

		if($getproject)
		{
			$projectImage = $getproject->projectImage;

			if(!empty($projectImage))
			{
				$explode = explode(',', $projectImage);
				foreach ($explode as $key => $value) {
						$this->aws3->deleteObject($value);	
				}
			}
		}
		
		$getproject = $this->User_model->delete('borrower_prior_project',array('id'=>$id));
		$this->session->set_flashdata('success','Project portfolio delete successsfully');
		echo 1;
		
	}



	public function loan_organization_setting(){

		if($this->session->userdata('user_role') == 1){

			$check_setting = $this->User_model->query("SELECT * FROM `user_settings` WHERE `user_id`='".$this->session->userdata('t_user_id')."' AND `loan_organization`='1'");
			if($check_setting->num_rows() > 0){
				$loginuser_dashboard = 2;
			}else{
				$loginuser_dashboard = 1;
			}

		}else{

			$loginuser_dashboard = 1;
		}

		return $loginuser_dashboard;

	}
	
	public function view()
	{
		error_reporting(0);		
		$this->session->set_flashdata('borrower_document_hit', '0');	
		$data['user_role'] = $this->session->userdata('user_role');
		
		$contact_idd 				= $this->input->post('contact_id') ? $this->input->post('contact_id') : $this->input->post('contact_idd'); 
		
		//fetch borrower_id on the base of contact select...
		$sql_query = $this->User_model->query("SELECT * FROM borrower_contact WHERE contact_id  = '".$contact_idd."' ");
		
		$bid = $sql_query->row();
		$bidd = '';
		if($sql_query->num_rows() > 0){
			$bidd = $bid->borrower_id;
		}
		
		
		$data['btn_name'] 	=  'view';
		
		
		$borrower_id        = $this->input->post('borrower_ids') ? $this->input->post('borrower_ids') : ( $bidd ? $bidd :($this->uri->segment(2)? $this->uri->segment(2) :''))  ;
		
		
		$fetch_contact 			= $this->User_model->query("SELECT * FROM contact ORDER by contact_firstname");
	
		$fetch_contact 				= $fetch_contact->result();
		$data['fetch_all_contact'] 	= $fetch_contact;
		
		$data['borrower_idd'] 		= $borrower_id;
		
		$data['contact_idd'] 		= $this->input->post('contact_id') ? $this->input->post('contact_id') : $this->input->post('contact_idd')  ;
		
		
	 	if($borrower_id)
		{
			$id['id']						= $borrower_id;

			$user_borrower_setting = $this->loan_organization_setting();

			if($user_borrower_setting == 1){
				$fetch_borrower_id_data = $this->User_model->select_where('borrower_data',$id);
			}else{
				$id['t_user_id'] = $this->session->userdata('t_user_id');
				$fetch_borrower_id_data = $this->User_model->select_where('borrower_data',$id);
			}

			if($fetch_borrower_id_data->num_rows() > 0){
				$fetch_borrower_id_data 		= $fetch_borrower_id_data->result();
			}else{

				$this->session->userdata('error','You have no permission to access this borrower data.');
				redirect(base_url().'borrower_view');
			}
			
			
			$data['fetch_borrower_id_data'] = $fetch_borrower_id_data;
			$contact_id['borrower_id']		= $borrower_id;


			//fetch borrower related loans...
			$borrower_related_loans = $this->User_model->query("SELECT l.id,l.talimar_loan,l.loan_amount,ls.loan_status,lp.property_address,lp.unit,lp.city,lp.state,lp.zip FROM loan as l JOIN loan_servicing as ls ON l.talimar_loan = ls.talimar_loan JOIN loan_property as lp ON l.talimar_loan = lp.talimar_loan WHERE l.borrower = '".$borrower_id."' ORDER BY ls.loan_status ASC");
			if($borrower_related_loans->num_rows() > 0){

				$borrower_related_loans_data = $borrower_related_loans->result();
			}else{

				$borrower_related_loans_data = '';
			}

           $data['borrower_related_loans_data'] 	= $borrower_related_loans_data;

           //borrower_property_approval

            $getborrower_property_approval = $this->User_model->select_where('borrower_property_approval',array('borrower_id'=>$borrower_id));
            if($getborrower_property_approval->num_rows() > 0){
          	 	$data['get_property_approval'] = $getborrower_property_approval->result();
          	}

          	//borrower_prior_project

            $getborrower_prior_project = $this->User_model->select_where('borrower_prior_project',array('borrower_id'=>$borrower_id));
            if($getborrower_prior_project->num_rows() > 0){
          	 	$data['borrower_prior_project'] = $getborrower_prior_project->result();
          	}



			// echo "SELECT * FROM borrower_contact WHERE borrower_id = '".$borrower_id."' ";
			// $borrower_contact_data = $this->User_model->select_where('borrower_contact',$contact_id);
			$borrower_contact_data = $this->User_model->query("SELECT * FROM borrower_contact WHERE borrower_id = '".$borrower_id."' ORDER by contact_primary  DESC" );
			
			if($borrower_contact_data->num_rows() > 0)
			{
				$borrower_contact_data 			= $borrower_contact_data->result();
				$data['borrower_contact_data'] 	= $borrower_contact_data;
			} 
			
			
			$sql_paidoff = "SELECT COUNT(*) as count_loan , SUM(loan.loan_amount) as total_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '3' AND loan.borrower = '".$borrower_id."' ";
			$fetch_paidoff_loan = $this->User_model->query($sql_paidoff);
			if($fetch_paidoff_loan->num_rows() > 0)
			{
				$fetch_paidoff_loan = $fetch_paidoff_loan->result();
				$data['fetch_paidoff_loan'] = array(
														'count_loan' => $fetch_paidoff_loan[0]->count_loan,
														'total_loan_amount' => $fetch_paidoff_loan[0]->total_loan_amount,
														);
			}	
			
			$sql_active = "SELECT COUNT(*) as count_loan , SUM(loan.loan_amount) as total_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '2' AND loan.borrower = '".$borrower_id."' ";
			$fetch_active_loan = $this->User_model->query($sql_active);
			if($fetch_active_loan->num_rows() > 0)
			{
				$fetch_active_loan = $fetch_active_loan->result();
				$data['fetch_active_loan'] = array(
												'count_loan' => $fetch_active_loan[0]->count_loan,
												'total_loan_amount' => $fetch_active_loan[0]->total_loan_amount,
											);
			}
			
			$sql_pipeline = "SELECT COUNT(*) as count_loan , SUM(loan.loan_amount) as total_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '1' AND loan.borrower = '".$borrower_id."' ";
			$fetch_pipeline_loan = $this->User_model->query($sql_pipeline);
			if($fetch_pipeline_loan->num_rows() > 0)
			{
				$fetch_pipeline_loan = $fetch_pipeline_loan->result();
				$data['fetch_pipeline_loan'] = array(
												'count_loan' => $fetch_pipeline_loan[0]->count_loan,
												'total_loan_amount' => $fetch_pipeline_loan[0]->total_loan_amount,
											);
			}
			
			$sql_all = "SELECT COUNT(*) as count_loan , SUM(loan.loan_amount) as total_loan_amount FROM loan  WHERE loan.borrower = '".$borrower_id."' ";
			$fetch_all_loan = $this->User_model->query($sql_all);
			if($fetch_all_loan->num_rows() > 0)
			{
				$fetch_all_loan = $fetch_all_loan->result();
				$data['fetch_all_loan'] = array(
												'all_count_loan' => $fetch_all_loan[0]->count_loan,
												'all_total_loan_amount' => $fetch_all_loan[0]->total_loan_amount,
											);
			}
			
			// fetch borrower documents by borrower id from 'borrower_document' table...
			$sql_borrower_document = $this->User_model->query("SELECT * FROM borrower_document WHERE borrower_id = '".$borrower_id."' ORDER BY id ");
			
			$data['fetch_borrower_document'] = $sql_borrower_document->result();
			 
		}
		$user						= array();
		$user['t_user_id']			= $this->session->userdata('t_user_id');
		// $result 					= $this->User_model->select_where('borrower_contact',$user);
		// $result_new				= $result->result();
		// $data['fetch_result']	= $result_new;
		
		$where1['borrower_contact_type'] 	= 1;
		// $fetch_all_contact 					= $this->User_model->select_where_asc('contact',$where1,'contact_firstname');
		// $fetch_all_contact 					= $this->User_model->select_star('contact');
		$fetch_all_contact 					= $this->User_model->query("SELECT * FROM contact ORDER by contact_firstname");
		$data['fetch_all_contact'] 			= $fetch_all_contact->result();
		//-------------------Fetch borrower data_dable ---------------------------
		/* if(($contact_idd && $borrower_id) || $borrower_id){
	
			$all_borrower_data  				= $this->User_model->select_star('borrower_data');
			$all_borrower_data					= $all_borrower_data->result();
		
		} 
		else*/
		if($contact_idd){
				
			$all_borrower_data = $this->User_model->query("SELECT * FROM borrower_contact as bc JOIN borrower_data as bd on bd.id = bc.borrower_id WHERE bc.contact_id = '".$contact_idd."' "); 	
			
			
			if($all_borrower_data->num_rows() == 0){
				
				$all_borrower_data  		= $this->User_model->select_star('borrower_data');
				$all_borrower_data			= $all_borrower_data->result();
			
			}else{
				
				$all_borrower_data			= $all_borrower_data->result();
			}
		
		}else{
			
			$all_borrower_data  			= $this->User_model->select_star('borrower_data');
			$all_borrower_data				= $all_borrower_data->result();
		}


		//display borrower by role in view page..

		if($this->session->userdata('user_role') == 1){

			$user_settings = $this->User_model->query("SELECT * FROM `user_settings` WHERE `user_id`= '".$this->session->userdata('t_user_id')."' AND `loan_organization`='1'");
			if($user_settings->num_rows() > 0){

				$loginuser_borrowers = 2;
			}else{
				$loginuser_borrowers = 1;
			}

		}else{
			$loginuser_borrowers = 1;
		}
		//echo $loginuser_borrowers;

		if($loginuser_borrowers == 1){
			$perticularUserData=array();
			$data['userLoginStatus']=0;
			$all_borrower_data  = $this->User_model->select_star('borrower_data');
		}else{
			$data['userLoginStatus']=1;
			$where['t_user_id']  = $this->session->userdata('t_user_id');
			$perticularUserData   = $this->User_model->select_where('borrower_data',$where);
			$perticularUserData   = $perticularUserData->result();
			$all_borrower_data  = $this->User_model->select_star('borrower_data');
		}

		$all_borrower_data					= $all_borrower_data->result();
		$data['all_borrower_data']			= $all_borrower_data;
		$data['all_particular_data']		= $perticularUserData;


		// fetch contact documents by contact id from 'contact_document' table...
		$sql_contact_document = $this->User_model->query("SELECT * FROM contact_document WHERE contact_id = '".$contact_idd."' ORDER BY id ");
		
		$data['fetch_contact_documentt'] = $sql_contact_document->result();
	
			$sql_contact_document = $this->User_model->query("SELECT * FROM contact_document WHERE contact_id = '".$contact_idd."' ORDER BY id ");
		
		$data['fetch_contact_documentt'] = $sql_contact_document->result();

		
		$fetch_all_users			= $this->User_model->query("SELECT * FROM user");
		$fetch_all_userss	= $fetch_all_users->result();

         foreach ($fetch_all_userss as $key => $value) {
           $data['all_users'][$value->id]=$value->fname.' '.$value->middle_name.' '.$value->lname;
         }
		
		
		$data['a'] = '1';
		$data['content'] = $this->load->view('borrower_data/view',$data,true);
		
		$this->load->view('template_files/template',$data);
	}

		
	public function check_borrower_contact_filename(){
		
		$contact_id = $this->input->post('contact_id');
		$filename 	 = $this->input->post('filename');
		
		/*$folder 		= FCPATH.'contact_documents/'.$contact_id.'/' ;*/
		$folder 		= 'contact_documents/'.$contact_id.'/' ;
		$target_dir 	= $folder.$filename;
		
		// check filename exist in the "borrower_document" table or not...
		$sel = $this->User_model->query("SELECT * FROM contact_document WHERE contact_id  = '".$contact_id."' AND contact_document = '".$target_dir."' ");	
		
		if($sel->num_rows() > 0){
			
			echo '1';			// duplicacy occurs, error;
		
		}else{
			
			echo '0';			// no duplicacy occurs, unique;	
		
		}
	}
	
	
	public function delete_borrower_document()
	{
		die('good');
		$id = $this->input->post('id');
		$delete = $this->User_model->delete('contact_document',array('id'=>$id));
		echo 1;
		
	}
	
	public function index()
	{
		$this->session->set_flashdata('borrower_document_hit', '0');	
		$data['user_role'] = $this->session->userdata('user_role');
		
		$contact_idd 				= $this->input->post('contact_id') ? $this->input->post('contact_id') : $this->input->post('contact_idd'); 
		
		//fetch borrower_id on the base of contact select...
		$sql_query = $this->User_model->query("SELECT * FROM borrower_contact WHERE contact_id  = '".$contact_idd."' ");
		
		$bid = $sql_query->row();
		$bidd = '';
		if($sql_query->num_rows() > 0){
			$bidd = $bid->borrower_id;
		}
		
		
		$data['btn_name'] 	= $this->input->post('btn_name') ? $this->input->post('btn_name') : ($this->uri->segment(2) ? $this->uri->segment(2) : 'view' );
		$borrower_id        = $this->input->post('borrower_ids') ? $this->input->post('borrower_ids') : ( $bidd ? $bidd :($this->uri->segment(3)? $this->uri->segment(3) :''));
		$fetch_contact 			= $this->User_model->query("SELECT * FROM contact ORDER by contact_firstname");
		$fetch_contact 				= $fetch_contact->result();
		$data['fetch_all_contact'] 	= $fetch_contact;
		$data['borrower_idd'] 		= $this->input->post('borrower_ids') ? $this->input->post('borrower_ids') : $this->uri->segment(3);
		$data['borrower_url_idd'] 	= $this->uri->segment(2);
		$data['contact_idd'] 		= $this->input->post('contact_id') ? $this->input->post('contact_id') : $this->input->post('contact_idd')  ;
		
		
	 	if($borrower_id)
		{
			$id['id']						= $borrower_id;

			if($this->session->userdata('user_role') == 1){

				$user_settings = $this->User_model->query("SELECT * FROM `user_settings` WHERE `user_id`= '".$this->session->userdata('t_user_id')."' AND `loan_organization`='1'");
				if($user_settings->num_rows() > 0){

					$loginuser_borrowers = 2;
				}else{
					$loginuser_borrowers = 1;
				}

			}else{
				$loginuser_borrowers = 1;
			}

			//echo $loginuser_borrowers;

			if($loginuser_borrowers == 1){

				$fetch_borrower_id_data = $this->User_model->select_where('borrower_data',$id);
			}else{

				$id['t_user_id'] = $this->session->userdata('t_user_id');
				$fetch_borrower_id_data = $this->User_model->select_where('borrower_data',$id);
			}

			if($fetch_borrower_id_data->num_rows() > 0){

				$fetch_borrower_id_data 		= $fetch_borrower_id_data->result();
				$data['fetch_borrower_id_data'] = $fetch_borrower_id_data;

			}else{

				$this->session->set_flashdata('error','You have no permisson to access this borrower data.');
				redirect(base_url().'borrower_view');

			}
			
			$contact_id['borrower_id']		= $borrower_id;
			
			$borrower_contact_data = $this->User_model->query("SELECT * FROM borrower_contact WHERE borrower_id = '".$borrower_id."' ORDER by contact_primary  DESC" );
			
			if($borrower_contact_data->num_rows() > 0)
			{
				$borrower_contact_data 			= $borrower_contact_data->result();
				$data['borrower_contact_data'] 	= $borrower_contact_data;
			} 
			
			
			$sql_paidoff = "SELECT COUNT(*) as count_loan , SUM(loan.loan_amount) as total_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '3' AND loan.borrower = '".$borrower_id."' ";
			$fetch_paidoff_loan = $this->User_model->query($sql_paidoff);
			if($fetch_paidoff_loan->num_rows() > 0)
			{
				$fetch_paidoff_loan = $fetch_paidoff_loan->result();
				$data['fetch_paidoff_loan'] = array(
														'count_loan' => $fetch_paidoff_loan[0]->count_loan,
														'total_loan_amount' => $fetch_paidoff_loan[0]->total_loan_amount,
														);
			}	
			
			$sql_active = "SELECT COUNT(*) as count_loan , SUM(loan.loan_amount) as total_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '2' AND loan.borrower = '".$borrower_id."' ";
			$fetch_active_loan = $this->User_model->query($sql_active);
			if($fetch_active_loan->num_rows() > 0)
			{
				$fetch_active_loan = $fetch_active_loan->result();
				$data['fetch_active_loan'] = array(
												'count_loan' => $fetch_active_loan[0]->count_loan,
												'total_loan_amount' => $fetch_active_loan[0]->total_loan_amount,
											);
			}
			
			$sql_pipeline = "SELECT COUNT(*) as count_loan , SUM(loan.loan_amount) as total_loan_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '1' AND loan.borrower = '".$borrower_id."' ";
			$fetch_pipeline_loan = $this->User_model->query($sql_pipeline);
			if($fetch_pipeline_loan->num_rows() > 0)
			{
				$fetch_pipeline_loan = $fetch_pipeline_loan->result();
				$data['fetch_pipeline_loan'] = array(
												'count_loan' => $fetch_pipeline_loan[0]->count_loan,
												'total_loan_amount' => $fetch_pipeline_loan[0]->total_loan_amount,
											);
			}
			
			// fetch borrower documents by borrower id from 'borrower_document' table...
			$sql_borrower_document = $this->User_model->query("SELECT * FROM borrower_document WHERE borrower_id = '".$borrower_id."' ORDER BY id ");
			
			
			$data['fetch_borrower_document'] = $sql_borrower_document->result();
			 
		}

		$user						= array();
		$user['t_user_id']			= $this->session->userdata('t_user_id');
		
		
		$where1['borrower_contact_type'] 	= 1;
		
		$fetch_all_contact 					= $this->User_model->query("SELECT * FROM contact ORDER by contact_firstname");
		$data['fetch_all_contact'] 			= $fetch_all_contact->result();
		//-------------------Fetch borrower data_dable ---------------------------
		
		if($contact_idd){
				
			$all_borrower_data = $this->User_model->query("SELECT * FROM borrower_contact as bc JOIN borrower_data as bd on bd.id = bc.borrower_id WHERE bc.contact_id = '".$contact_idd."' "); 	
			
			
			if($all_borrower_data->num_rows() == 0){
				
				$all_borrower_data  		= $this->User_model->select_star('borrower_data');
				$all_borrower_data			= $all_borrower_data->result();
			
			}else{
				
				$all_borrower_data			= $all_borrower_data->result();
			}
		
		}else{
			
			$all_borrower_data  			= $this->User_model->select_star('borrower_data');
			$all_borrower_data				= $all_borrower_data->result();
		} 
		$all_borrower_data  				= $this->User_model->select_star('borrower_data');
		$all_borrower_data					= $all_borrower_data->result();
		$data['all_borrower_data']			= $all_borrower_data;
		
		$data['a'] = '1';

           $b_id['borrower_id']=$this->uri->segment(3);
		

		   $fetch_additional_g_data		= $this->User_model->select_where('additional_graurantor',$b_id);
		   $fetch_additional_g_dataa 	= $fetch_additional_g_data->result();

			$data['fetch_additional_g_id'] =$fetch_additional_g_dataa;
	
		$query=$this->User_model->query("select * from user")->result();
		$data['all_user']=$query;


		//$data['fetch_borrower_id_data'] = '1';
		$data['content'] = $this->load->view('borrower_data/index',$data,true);
		
		$this->load->view('template_files/template',$data);
	}
	
	public function fetch_borrower_details(){
		
		$fetch = $this->User_model->query("SELECT c_email,c_phone,c_title,c_guarantor, c2_email,c2_phone, c2_title , c2_guarantor FROM borrower_data WHERE id = '".$this->input->post('id')."'  ");
		$result = $fetch->row();
		
		if($this->input->post('contact_type') == '1'){
			
			// fetch contact details of person1....
			$output['email'] 	 = $result->c_email;
			$output['email'] 	 = $result->c_email;
			$output['title'] 	 = $result->c_title;
			$output['phone'] 	 = $result->c_phone;
			$output['guarantor'] = $result->c_guarantor;
			
			print_r(json_encode($output));
			
		}else if($this->input->post('contact_type') == '2'){
			
			// fetch contact details of person2....
			$output['email']	 = $result->c2_email;
			$output['phone']	 = $result->c2_phone;
			$output['title'] 	 = $result->c2_title;
			$output['guarantor'] = $result->c2_guarantor;
			

			print_r(json_encode($output));
		}
		
	}
	
	public function borrower_document_read(){
		
		 
		// fetch filename from "borrower_document" table...
		$sql = $this->User_model->query("SELECT * FROM borrower_document WHERE id = '".$this->uri->segment(3)."' ");
		$fetch_name = $sql->row();
		
		$filename = $fetch_name->borrower_document;

												 
		// Let the browser know that a PDF file is coming.
		
		/* header("Content-type: application/pdf");
		header('Content-Disposition: inline; filename="' . $filename . '"');
		header("Content-Length: " . filesize('borrower_document-'.$filename));
			*/
		header('Content-Description: File Transfer');
		header('Content-Type: application/pdf');
		header('Content-Disposition: attachment; filename="'.basename($filename).'"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($filename));										 
		// Send the file to the browser.

		readfile($filename);

		exit; 
	} 
	
	
	
	
	
	
	public function check_borrower_filename(){
		
		$borrower_id = $this->input->post('borrower_id');
		$filename 	 = $this->input->post('filename');
		
		$folder 		= FCPATH.'borrower_documents/'.$borrower_id.'/' ;
		$target_dir 	= $folder.$filename;
		
		// check filename exist in the "borrower_document" table or not...
		$sel = $this->User_model->query("SELECT * FROM borrower_document WHERE borrower_id  = '".$borrower_id."' AND borrower_document = '".$target_dir."' ");	
		
		if($sel->num_rows() > 0){
			
			echo '1';			// duplicacy occurs, error;
		
		}else{
			
			echo '0';			// no duplicacy occurs, unique;	
		
		}
	}

	public function amount_format($n) {
		$n = str_replace(',', '', $n);
		$a = trim($n, '$');
		$b = trim($a, ',');
		$c = trim($b, '%');
		return $c;
	}

	//Update LINE of Credit data for borrower...
	public function updateLineofcredit(){

		if(isset($_POST['submitline'])){

			if($this->input->post('borrower_idds') != ''){

				//PreApproved values...
				$where11['id']							= $this->input->post('borrower_idds');
				$data11['PreApproved']					= $this->input->post('PreApproved');
				$data11['ApprovedAmount']				= $this->amount_format($this->input->post('ApprovedAmount'));

				if ($this->input->post('ApprovedDate') != '') {
					//$ApprovedDate = DateTime::createFromFormat('m-d-Y', $this->input->post('ApprovedDate'));
					//$ApprovedDate->format('Y-m-d');
					$ApprovedDate_date = input_date_format($this->input->post('ApprovedDate'));
				} else {
					$ApprovedDate_date = NULL;
				}

				$data11['ApprovedDate']					= $ApprovedDate_date;
				$data11['ApprovedTerm']					= $this->input->post('ApprovedTerm');
				$data11['PurchasePrice']				= $this->amount_format($this->input->post('PurchasePrice'));
				$data11['RenovationCosts']				= $this->amount_format($this->input->post('RenovationCosts'));
				$data11['CompletionValue']				= $this->amount_format($this->input->post('CompletionValue'));
				$data11['loc_state']					= $this->input->post('loc_state');

				$this->User_model->updatedata('borrower_data',$where11,$data11);
				$this->session->set_flashdata('success', 'Line of credit updated successfully!');				
				redirect(base_url().'borrower_view/'.$this->input->post('borrower_idds'));

			}else{

				$this->session->set_flashdata('error', 'Please select a borrower first!');				
				redirect(base_url().'borrower_view','refresh');

			}
		}else{
			redirect(base_url().'borrower_view','refresh');
		}
	}

	public function add_borrower_data()
	{
		error_reporting(0);

		$data['contact_idd']				= $this->input->post('contact_id');
		$borrower_id						= $this->input->post('borrower_id');
		
		$button								= $this->input->post('button');	
		// $button							= $this->input->post('save');	
		$data11['b_name']					= $this->input->post('borrower_name');
		$data11['b_fci']					= $this->input->post('fci_no');
		$data11['borrower_type']			= $this->input->post('borrower_type');
		$data11['t_user_id']				= $this->input->post('primary_cont');
		//$data11['b_person']				= $this->input->post('borrower_person');

		if ($this->input->post('bdate_created') != '') {
			//$bdate_createdVal = DateTime::createFromFormat('m-d-Y', $this->input->post('bdate_created'));
			//$bdate_created = $bdate_createdVal->format('Y-m-d');
			$bdate_created = input_date_format($this->input->post('bdate_created'));
		} else {
			$bdate_created = NULL;
		}
		$data11['bdate_created']			= $bdate_created;
		$data11['borrower_hash']			= $this->input->post('borrower_hash');

		$data11['b_address']				= $this->input->post('address');
		$data11['b_city']					= $this->input->post('city');
		$data11['b_unit']					= $this->input->post('unit');
		$data11['b_state']					= $this->input->post('state');
		$data11['b_zip']						= $this->input->post('zip');
		$data11['b_tax_id']					= $this->input->post('tax_id');
		//$data11['b_vesting']				= $this->input->post('vesting');
		$data11['ofac_search']				= $this->input->post('ofac_search');
		$data11['b_pg_vesting']				= $this->input->post('pg_vesting');
		$data11['company_name']				= $this->input->post('company_name');
		$data11['ira_name']					= $this->input->post('ira_name');
		$data11['ira_account']				= $this->input->post('ira_account');


		$data11['company_type']				= $this->input->post('company_type');
		$data11['fci_acc_box']				= $this->input->post('fci_acc_box');
		$data11['fci_acc_text']				= $this->input->post('fci_acc_text');
		$data11['del_toro_box']				= $this->input->post('del_toro_box');
		$data11['del_toro_text']			= $this->input->post('del_toro_text');
		
		$data11['c_reg_state']				= $this->input->post('c_registration_state');
		$data11['state_entity_no']			= $this->input->post('state_entity_no');

		$data11['artical_received']			= $this->input->post('artical_received');
		$data11['artical_approved']			= $this->input->post('artical_approved');
		$data11['artical_uploaded']			= $this->input->post('artical_uploaded');
		$data11['operating_received']			= $this->input->post('operating_received');
		$data11['operating_approved']			= $this->input->post('operating_approved');
		$data11['operating_uploaded']			= $this->input->post('operating_uploaded');
		$data11['texid_received']			= $this->input->post('texid_received');
		$data11['texid_approved']			= $this->input->post('texid_approved');
		$data11['texid_uploaded']			= $this->input->post('texid_uploaded');
		$data11['corpartical_received']			= $this->input->post('corpartical_received');
		$data11['corpartical_approved']			= $this->input->post('corpartical_approved');
		$data11['corpartical_uploaded']			= $this->input->post('corpartical_uploaded');
		$data11['corporate_received']			= $this->input->post('corporate_received');
		$data11['corporate_approved']			= $this->input->post('corporate_approved');
		$data11['corporate_uploaded']			= $this->input->post('corporate_uploaded');
		$data11['corps_texid_received']			= $this->input->post('corps_texid_received');
		$data11['corps_texid_approved']			= $this->input->post('corps_texid_approved');
		$data11['corps_texid_uploaded']			= $this->input->post('corps_texid_uploaded');
		$data11['certi_received']			= $this->input->post('certi_received');
		$data11['certi_approved']			= $this->input->post('certi_approved');
		$data11['certi_uploaded']			= $this->input->post('certi_uploaded');
		$data11['part_received']			= $this->input->post('part_received');
		$data11['part_approved']			= $this->input->post('part_approved');
		$data11['part_uploaded']			= $this->input->post('part_uploaded');
		$data11['texidlp_received']			= $this->input->post('texidlp_received');
		$data11['texidlp_approved']			= $this->input->post('texidlp_approved');
		$data11['texidlp_uploaded']			= $this->input->post('texidlp_uploaded');
		$data11['trusta_received']			= $this->input->post('trusta_received');
		$data11['trusta_approved']			= $this->input->post('trusta_approved');
		$data11['trusta_uploaded']			= $this->input->post('trusta_uploaded');



		$data11['es_received']			= $this->input->post('es_received');
		$data11['es_approved']			= $this->input->post('es_approved');
		$data11['es_uploaded']			= $this->input->post('es_uploaded');


		$data11['corps_es_received']			= $this->input->post('corps_es_received');
		$data11['corps_es_approved']			= $this->input->post('corps_es_approved');
		$data11['corps_es_uploaded']			= $this->input->post('corps_es_uploaded');
		

		$data11['eslp_received']			= $this->input->post('eslp_received');
		$data11['eslp_approved']			= $this->input->post('eslp_approved');
		$data11['eslp_uploaded']			= $this->input->post('eslp_uploaded');

		
		
		$data11['in_balance_income_sheet']	= $this->input->post('income_statement') ? $this->input->post('income_statement') : '0';
		$data11['balancesheet_date']			= $this->input->post('balancesheet_date');
		$data11['balancesheet_to']				= $this->input->post('balancesheet_to');
		$data11['income_s_from']				= $this->input->post('income_from');
		$data11['income_s_to']				= $this->input->post('income_to');
		$data11['cpa_pa']						= $this->input->post('cpa_pa') ? $this->input->post('cpa_pa') : '0';
		$data11['attached_addedeum']			= $this->input->post('attached_adden') ? $this->input->post('attached_adden') : '0';
		
		$data11['about_borrower']					= $this->input->post('about_borrower');
		$data11['c_phone']						= $this->input->post('c_phone');
		$data11['c_email']						= $this->input->post('c_email');
		$data11['account_status']				= $this->input->post('account_status');

		
		
		$t_user_id								= $this->session->userdata('t_user_id');
		
		$borrower_contact_id 					= $this->input->post('borrower_contact_id'); 
		$contact_id 							= $this->input->post('contact_contact_id'); 
		$contact_name 							= $this->input->post('contact_contact_id'); 
		$primary_contact 						= $this->input->post('primary_contact'); 
		$contact_title 							= $this->input->post('contact_title'); 
		$contact_gurrantor 						= $this->input->post('contact_gurrantor'); 
		$contact_responsibility 				= $this->input->post('contact_responsibility'); 
		$borrower_portal_access 				= $this->input->post('borrower_portal_access'); 
		$contact_phone 							= $this->input->post('contact_phone'); 
		$contact_email 							= $this->input->post('contact_email'); 
		$con_id 							    = $this->input->post('con_id'); 
		
		
		// if($button == 'Save')
		if($button == 'save')
		{	
			if($this->input->post('borrower_name') == '')			
			{				
				$this->session->set_flashdata('error', ' Please fill all fields');				
				redirect(base_url().'borrower_data','refresh');				
				die('');			
			}
	
	
			if(empty($borrower_id)){
					
				
				$check_fci['b_fci'] = $this->input->post('fci_no');
				$check_fci_query 	= $this->User_model->select_where('borrower_data',$check_fci);
				if($check_fci_query->num_rows() > 0)
				{
					
				}
				$this->User_model->insertdata('borrower_data',$data11);
				 $insert_id = $this->db->insert_id();
				if($insert_id)
				{
					$contact_id_arr = array_values(array_filter($contact_id));
					if($contact_id_arr){
					
						foreach($contact_id_arr as $key => $row)
						{
							
							$sql = "INSERT INTO `borrower_contact` ( `borrower_id`, `contact_id`, `c_title`, `contact_name`, `contact_primary`,  `contact_gurrantor`, `contact_responsibility`, `borrower_portal_access`, `contact_phone`, `contact_email`, `t_user_id`) VALUES ( '".$insert_id."', '".$row."', '".$contact_title[$key]."', '".$contact_name[$key]."', '".$primary_contact[$key]."',  '".$contact_gurrantor[$key]."', '".$contact_responsibility[$key]."',  '".$borrower_portal_access[$key]."',  '".$contact_phone[$key]."',  '".$contact_email[$key]."',  '".$t_user_id."' )";
							$this->User_model->query($sql);
						}
					}


                   $con_id=$this->input->post('con_id');
                   $iddd=$this->input->post('iddd');
                 

		             foreach($con_id as $ke => $ro)
						{
							if($ro !=''){
						
							$sql_5 = "INSERT INTO `additional_graurantor` (`borrower_id`, `contact_id`,`user_id`) VALUES ( '".$insert_id."', '".$con_id[$ke]."','".$t_user_id."' )";
							$this->User_model->query($sql_5);
						}
					  }
		     	
					$this->session->set_flashdata('success', ' Record added successfully');
					redirect(base_url().'borrower_view/'.$insert_id,'refresh');
					
				}
				else
				{
					// echo "false insert id";
					$this->session->set_flashdata('error','Something went wrong');
					redirect(base_url().'borrower_data','refresh');
				}
			
		
			

			}else
			{

		
				// upload document...
				if($_FILES){
				
					
				
					//first check document uploaded for borrower...
					$check_fci_query 	= $this->User_model->query("SELECT * FROM borrower_document WHERE borrower_id = '".$this->input->post('borrower_id')."' ");
					if($check_fci_query->num_rows() > 0)
					{
						$cnt = $check_fci_query->num_rows();
					}else{
						$cnt = 1;
					}
					
					$borrower_id			= $this->input->post('borrower_id');
					$borrower_name			= $this->input->post('borrower_name');
				
					$folder = 'borrower_documents/'.$borrower_id.'/' ; 
										
					foreach($_FILES['upload_doc']['name'] as $key => $row_data){
						
						if($row_data)
						{
							
							$ext = strtolower(pathinfo($row_data, PATHINFO_EXTENSION));
							
							if($ext == 'pdf')
							{
								$cnt++;
								$new_name 		= basename($row_data);
								$my_file_name 	= str_replace('.'.$ext, '_'.time().'.'.$ext, $new_name);
								$target_dir 	= $folder.$my_file_name;
									
								$borrower_document['borrower_id']			=	$borrower_id;
								$borrower_document['name']					=	$borrower_name;
								$borrower_document['borrower_document']		=	$target_dir;
								$borrower_document['user_id']				=	$this->session->userdata('t_user_id');
									
								
								$attachmentName=$this->aws3->uploadFile($target_dir,$_FILES["upload_doc"]["tmp_name"][$key]);
								if($attachmentName){
									$this->User_model->insertdata('borrower_document',$borrower_document);
								}
							}
							
							$this->session->set_flashdata('borrower_document_hit', '1');
						}
					}
					
					$this->session->set_flashdata('success', ' Document uploaded !');					
				}

				// update existing document names...
				if($this->input->post('old_filename')){
					
					$old_filename 			= array_values(array_filter($this->input->post('old_filename')));
					$new_filename 			= array_values(array_filter($this->input->post('new_filename')));
					$borrower_document_id 	= array_values(array_filter($this->input->post('borrower_document_id')));
					
					if($borrower_document_id){
						
						// first check file exist in folder or not....
						foreach($borrower_document_id as $key => $old_borrower_document_id){
							if ( isset($new_filename[$key])) {
								
								$folder 			= 'borrower_documents/'.$borrower_id.'/'; //FCPATH
								$ext = strtolower(pathinfo($old_filename[$key], PATHINFO_EXTENSION));
								$new_filenamect = str_replace('.'.$ext, '_'.time().'.'.$ext, $folder.$new_filename[$key]);
								
								
								$old_target_dir 	= str_replace(S3_VIEW_URL, '', $old_filename[$key]);
								$new_target_dir 	= $new_filenamect;
										
								
								
									$attachmentName=$this->aws3->copyObject($old_target_dir,$new_target_dir);
									
									if($attachmentName){
									$this->aws3->deleteObject($old_target_dir);
											
										//update  old filename with new file name int the "borrower_document" table...
											
										$this->User_model->query("UPDATE borrower_document SET borrower_document = '".$new_target_dir."' WHERE id = '".$old_borrower_document_id."' ");
									}						
							}
						}
						
						
					}
					$this->session->set_flashdata('success', ' Document uploaded !');
					
				}

				$contact_update['borrower_id']		= $borrower_id;
				$update_id['id'] = $borrower_id;
				$this->User_model->updatedata('borrower_data',$update_id,$data11);
				$contact_id_arr = array_values(array_filter($contact_id));

				if($contact_id_arr){
				
					foreach($contact_id_arr as $key => $row)
					{
						$check_where1['contact_id'] 	= $row;
						$check_where1['borrower_id'] 	= $borrower_id;
						$fetch_check_borrower_contact 	= $this->User_model->select_where('borrower_contact',$check_where1);
						
						if($fetch_check_borrower_contact->num_rows() > 0)
						{
							// echo  'if part';
							 $sql = "UPDATE `borrower_contact` SET c_title = '".$contact_title[$key]."', contact_name = '".$row."', contact_primary = '".$primary_contact[$key]."' ,contact_gurrantor = '".$contact_gurrantor[$key]."', contact_responsibility='".$contact_responsibility[$key]."', borrower_portal_access = '".$borrower_portal_access[$key]."', contact_phone = '".$contact_phone[$key]."', contact_email = '".$contact_email[$key]."' WHERE  borrower_id = '".$borrower_id."' AND contact_id = '".$row."' ";
							
						}
						else
						{
							
						$sql = "INSERT INTO `borrower_contact` ( `borrower_id`, `contact_id`, `c_title`, `contact_name`, `contact_primary`, `contact_gurrantor`,`contact_responsibility`,  `borrower_portal_access`,  `contact_phone`, `contact_email`, `t_user_id`) VALUES ( '".$borrower_id."', '".$row."', '".$contact_title[$key]."', '".$row."','".$primary_contact[$key]."', '".$contact_gurrantor[$key]."', '".$contact_responsibility[$key]."',  '".$borrower_portal_access[$key]."',  '".$contact_phone[$key]."',  '".$contact_email[$key]."',  '".$t_user_id."' )";
						}
						
						$this->User_model->query($sql);
					}
				}

				
				$this->session->set_flashdata('success', ' Borrower Data Updated');
				
				redirect(base_url().'borrower_view/'.$borrower_id,'refresh');
			}
		
		}else{
			echo 'no save button';
		}
		
		
	}

	
	public function add_borrower_contact()
	{
		$button									= $this->input->post('button');
		$borrower_id							= $this->input->post('modal_borrower_id');
		$borrower_data_id['id']					= $this->input->post('modal_borrower_id');
		$borrower_contact_id['borrower_id']		= $borrower_id;
		$data									= array();
		$data['first_name']						= $this->input->post('first_name');
		$data['middle_initial']					= $this->input->post('middle_inital');
		$data['last_name']						= $this->input->post('last_name');
		$data['address']						= $this->input->post('address');
		$data['unit']							= $this->input->post('unit');
		$data['city']							= $this->input->post('city');
		$data['state']							= $this->input->post('state');
		$data['zip']							= $this->input->post('zip');
		$data['dob']							= $this->input->post('dob');
		$data['title']							= $this->input->post('title');
		$data['phone_1']						= $this->input->post('phone_1');
		$data['phone_2']						= $this->input->post('phone_2');
		$data['email']							= $this->input->post('email');
		$data['tax_id']							= $this->input->post('tax_id');
		$data['credit_report_date']				= $this->input->post('credit_report_date');
		$data['credit_score']					= $this->input->post('credit_score');
		$data['licence']						= $this->input->post('licence');
		$data['licence_state']					= $this->input->post('licence_state');
		$data['declare_bk_12']					= $this->input->post('bk_12') ? $this->input->post('bk_12') : '0';
		$data['bk_discharge']					= $this->input->post('bk_discharge') ? $this->input->post('bk_discharge') : '0';
		$data['feolny']							= $this->input->post('felony') ? $this->input->post('sign_d') : '0';
		$data['sign_document']					= $this->input->post('sign_d') ? $this->input->post('sign_d') : '0' ;
		$data['gurrenter']						= $this->input->post('guranter') ? $this->input->post('guranter') : '0' ;
		$data['t_user_id']						= $this->session->userdata('t_user_id');
		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
		$borrower_data['b_name']				= $this->input->post('first_name').' '.$this->input->post('last_name');
		$borrower_data['b_address']				= $this->input->post('address');
		$borrower_data['b_city']				= $this->input->post('city');
		$borrower_data['b_unit']				= $this->input->post('unit');
		$borrower_data['b_state']				= $this->input->post('state');
		$borrower_data['b_zip']					= $this->input->post('zip');
		$borrower_data['b_tax_id']				= $this->input->post('tax_id');
		
		if($button == 'save')
		{
			// echo '';
			$check_borrower_id = $this->User_model->select_where('borrower_contact', $borrower_contact_id);
			if($check_borrower_id->num_rows() > 0)
			{
				// echo "<pre>";
				// print_r($data);
				// echo "</pre>";
				$this->User_model->updatedata('borrower_contact',$borrower_contact_id,$data);
				
				// update data table also  
				$this->User_model->updatedata('borrower_data',$borrower_data_id,$borrower_data);
				 $this->session->set_flashdata('success',' Updated');
				 redirect(base_url().'borrower_data/'.$borrower_id , 'refresh');
			}
			else
			{
				
				// echo "insave portion";
				$data['borrower_id']	= $borrower_id;
				$this->User_model->insertdata('borrower_contact',$data);
				 $insert_id = $this->db->insert_id();
				if($insert_id)
				{
					// echo "true insert id";
					$this->session->set_flashdata('success', ' Record added sucesfully');
					redirect(base_url()."borrower_data",'refresh');
					
				}
				else
				{
					$this->session->set_flashdata('error', ' SQL ERROR');
					redirect(base_url()."borrower_contact",'refresh');
				}
			}
		}
		else
		{
			$this->session->set_flashdata('error', ' Something went wrong');
				redirect(base_url()."borrower_contact",'refresh');
		}
		
	}


	public function priorProjects(){
		$imgurl = array();
		if(isset($_POST['priorsubmit'])){

			$pborrower_id 	= $this->input->post('pborrower_id');
			$prow_id 		= $this->input->post('prow_id');
			$hiddenImage 	= $this->input->post('hiddenImage');
			$pstreet 		= $this->input->post('pstreet');
			$pzip 		= $this->input->post('pzip');
			$punit 		    = $this->input->post('punit');
			$pcity 			= $this->input->post('pcity');
			$pstate 		= $this->input->post('pstate');
			$baimg 			= $this->input->post('baimg');

			if(!empty($_FILES['pimage']))
			{
				$total = count($_FILES['pimage']['name']);

				// Loop through each file
				for( $i=0 ; $i < $total ; $i++ ) {

					//Get the temp file path
					$filename = $_FILES['pimage']['name'][$i];


					$ext = pathinfo($filename, PATHINFO_EXTENSION);
					if($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPEG'){

						$path = 'projectImage/'.time().$i.$filename; //FCPATH
						
						//if(move_uploaded_file($_FILES["file"]["tmp_name"],$path)){
						$attachmentName=$this->aws3->uploadFile($path,$_FILES["pimage"]["tmp_name"][$i]);
						if($attachmentName){

							$imgurl[] = aws_s3_document_url($path);
						}
					}		
				}
			}

			if(count($imgurl) > 0)
			{
				$projectImage = implode(',', $imgurl);
			}
			elseif($hiddenImage == '')
			{
				$projectImage = $hiddenImage;
			}
			else
			{
				$projectImage = '';
			}


			$data['borrower_id'] 	= $pborrower_id;
			$data['street'] 		= $pstreet;
			$data['zip'] 			= !empty($pzip)?$pzip:'';
			$data['unit'] 			= !empty($punit)?$punit:'';
			$data['city'] 			= $pcity;
			$data['state'] 			= $pstate;
			$data['balink'] 		= $baimg;
			$data['projectImage'] 	= $projectImage;

			if($prow_id == 'new'){

				$this->User_model->insertdata('borrower_prior_project',$data);
				$this->session->set_flashdata('success','Prior project added successfully!');

			}else{
				$where['id'] = $prow_id;

				$getproject = $this->User_model->select_where('borrower_prior_project',array('id'=>$prow_id));
				$getproject = $getproject->row();

				if($getproject)
				{
					$projectImage = $getproject->projectImage;

					if(!empty($projectImage))
					{
						$explode = explode(',', $projectImage);
						foreach ($explode as $key => $value) {
								$this->aws3->deleteObject($value);	
						}
					}
				}

				$this->User_model->updatedata('borrower_prior_project',$where,$data);
				$this->session->set_flashdata('success','Prior project updated successfully!');
			}

			redirect(base_url().'borrower_view/'.$pborrower_id);
		}
	}
	
	public function view_borrower_data()
	{
		$user					= array();
		$user['t_user_id']		= $this->session->userdata('t_user_id');
		$result 				= $this->User_model->select_where('borrower_contact',$user);
		$result_new				= $result->result();
		$data['fetch_result']	= $result_new;
		$data['content']		= $this->load->view('borrower_data/contact_viewall',$data,true);
		$this->load->view('template_files/template',$data);
	}
	
	public function load_conact_data()
	{
		$id['id'] = $_POST['id'];
		$result = $this->User_model->select_where('borrower_contact' , $id);
		$result_new = $result->result();
		foreach($result_new as $row)
		{
			$aa['fname'] 		= $row->first_name;
			$aa['lname'] 		= $row->last_name;
			$aa['address'] 		= $row->address;
			$aa['unit'] 		= $row->unit;
			$aa['zip'] 			= $row->zip;
			$aa['city'] 		= $row->city;
			$aa['tax_id'] 		= $row->tax_id;
		}
		print_r(json_encode($aa));
		
	}
	
	public function delete_borrower()
	{
		$id['id']					= $this->uri->segment(2);
		$contact['borrower_id']		= $this->uri->segment(2);
		$this->User_model->delete('borrower_data',$id);
		$this->User_model->delete('borrower_contact',$contact);
		$this->session->set_flashdata('success',' Deleted');
		redirect(base_url().'borrower_view','refresh');
	}
	
	public function delete_borrower_contacts()
	{
		$id['id']	= $this->uri->segment(2);
		$borrower	= $this->uri->segment(3);
		
		$this->User_model->delete('borrower_contact',$id);
		$this->session->set_flashdata('success',' Borrower contact deleted successfully!');
		redirect(base_url().'borrower_view/'.$borrower ,'refresh');
	}
	
	public function delete_borrower_f()
	{
		$id = $this->uri->segment(2);
		if($id)
		{
			$borrower_id['id'] = $id;
			
			$borrwer_c1['c_first_name'] 			= '';
			$borrwer_c1['c_middle_initial'] 		= '';
			$borrwer_c1['c_last_name'] 				= '';
			$borrwer_c1['c_street'] 				= '';
			$borrwer_c1['c_unit'] 					= '';
			$borrwer_c1['c_city'] 					= '';
			$borrwer_c1['c_state'] 					= '';
			$borrwer_c1['c_zip'] 					= '';
			$borrwer_c1['c_title'] 					= '';
			$borrwer_c1['c_phone'] 					= '';
			$borrwer_c1['c_email'] 					= '';
			$borrwer_c1['c_dob'] 					= '';
			$borrwer_c1['c_ss'] 					= '';
			$borrwer_c1['c_credit_score'] 			= '';
			$borrwer_c1['c_credit_score_date'] 		= '';
			$borrwer_c1['c_credit_explaination'] 	= '';
			$borrwer_c1['c_licence'] 				= '';
			$borrwer_c1['c_licence_state'] 			= '';
			$borrwer_c1['c_l_expiration_date'] 		= '';
			$borrwer_c1['c_guarantor'] 				= '';
			$borrwer_c1['c_bk_declare'] 			= '';
			$borrwer_c1['c_bk_dismis'] 				= '';
			
			$this->User_model->updatedata('borrower_data',$borrower_id,$borrwer_c1);
			$this->session->set_flashdata('success','Deleted');
			redirect(base_url().'borrower_data/'.$id, 'refresh');
		}
	}
	
	public function delete_borrower_s()
	{
		$id = $this->uri->segment(2);
		if($id)
		{
			$borrower_id['id'] = $id;
			
			$borrwer_c2['c2_first_name'] 				= '';
			$borrwer_c2['c2_middle_initial'] 			= '';
			$borrwer_c2['c2_last_name'] 				= '';
			$borrwer_c2['c2_address'] 					= '';
			$borrwer_c2['c2_unit'] 						= '';
			$borrwer_c2['c2_city'] 						= '';
			$borrwer_c2['c2_state'] 					= '';
			$borrwer_c2['c2_zip'] 						= '';
			$borrwer_c2['c2_title'] 					= '';
			$borrwer_c2['c2_phone'] 					= '';
			$borrwer_c2['c2_email'] 					= '';
			$borrwer_c2['c2_dob'] 						= '';
			$borrwer_c2['c2_ss'] 						= '';
			$borrwer_c2['c2_credit_score'] 				= '';
			$borrwer_c2['c2_credit_score_date'] 		= '';
			$borrwer_c2['c2_credit_explaination'] 		= '';
			$borrwer_c2['c2_licence'] 					= '';
			$borrwer_c2['c2_licence_state'] 			= '';
			$borrwer_c2['c2_l_expiration_date'] 		= '';
			$borrwer_c2['c2_guarantor'] 				= '';
			$borrwer_c2['c2_bk_declare'] 				= '';
			$borrwer_c2['c2_bk_dismis'] 				= '';
			
			$this->User_model->updatedata('borrower_data',$borrower_id,$borrwer_c2);
			$this->session->set_flashdata('success','Deleted');
			redirect(base_url().'borrower_data/'.$id, 'refresh');
		}
	}
	
	public function delete_borrower_contact()
	{
		if($this->uri->segment(2))
		{
			$id['id'] = trim($this->uri->segment(2),'D');
			$this->User_model->delete('borrower_contact',$id);
			$this->session->set_flashdata('success', 'Deleted!');
			redirect(base_url().'borrower_data','refresh');
		}
	}
	
	public function update_borrower_documents(){

		/*echo "<pre>";
		print_r($_POST);
		echo "</pre>";*/
			
		$borrower_id = $this->input->post('borrower_idd');

		if($_FILES){
						
			//first check document uploaded for borrower...
			$check_fci_query 	= $this->User_model->query("SELECT * FROM borrower_document WHERE borrower_id = '".$borrower_id."' ");
			if($check_fci_query->num_rows() > 0)
			{
				$cnt = $check_fci_query->num_rows();
			}else{
				$cnt = 1;
			}

			$idd['id']=$this->input->post('borrower_idd');
			$fetch_borrower_data 		= $this->User_model->select_where('borrower_data',$idd);
			$fetch_borrower_data 		= $fetch_borrower_data->result();
		 	$b_name					= $fetch_borrower_data[0]->b_name;

		 	$borrower_id			=$this->input->post('borrower_idd');
		
		
			$folder = 'borrower_documents/'.$borrower_id.'/' ; //FCPATH
			/*if(!is_dir($folder))
			{
				mkdir($folder, 0777, true);
				chmod($folder,0777);
			}*/
			
			
			foreach($_FILES['borrower_upload_doc']['name'] as $key => $row_data){
				
				if($row_data)
				{
					
					$ext = strtolower(pathinfo($row_data, PATHINFO_EXTENSION));
					// only docx files are allowed to uploaded!
					// if($ext == 'docx' || $ext == 'doc')
					if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc')
					{
							$cnt++;
						// $new_name 		= 'borrower-'.$borrower_id.'_'.$cnt.'.'.$ext;
						// $new_name 		= basename($row_data);
						$new_name 		= basename($row_data);
						$my_file_name 	= str_replace('.'.$ext, '_'.time().'.'.$ext, $new_name);
						$target_dir 	= $folder.time().$my_file_name;
							
						$borrower_document['borrower_id']			=	$borrower_id;
						$borrower_document['name']					=	$b_name;
						$borrower_document['borrower_document']		=	$target_dir;
						$borrower_document['user_id']				=	$this->session->userdata('t_user_id');
						$borrower_document['document_name']			=	input_sanitize($this->input->post('document_name'));
							
						//if(move_uploaded_file($_FILES["borrower_upload_doc"]["tmp_name"][$key], $target_dir) === TRUE ){
						$attachmentName=$this->aws3->uploadFile($target_dir,$_FILES["borrower_upload_doc"]["tmp_name"][$key]);
						if($attachmentName){
							$this->User_model->insertdata('borrower_document',$borrower_document);
						}
						$this->session->set_flashdata('success', ' Document uploaded!');
						redirect(base_url().'borrower_view/'.$borrower_id.'#borrower_document','refresh');	
					}else{
						$this->session->set_flashdata('error', ' Only docx/doc and PDF format are allowed to upload !');
						redirect(base_url().'borrower_data/'.$borrower_id.'#borrower_document','refresh');
					}
					//$this->session->set_flashdata('borrower_document_hit', '1');
				}else{
					$this->session->set_flashdata('error', ' Only docx/doc and PDF format are allowed to upload !');
					redirect(base_url().'borrower_data/'.$borrower_id.'#borrower_document','refresh');
				}
			}
		}else{
			$this->session->set_flashdata('error', ' Only docx/doc and PDF format are allowed to upload !');
			redirect(base_url().'borrower_data/'.$borrower_id.'#borrower_document','refresh');
		}
	}

	public function borrrower_delete_document()
	{
		$id = $this->input->post('id');

		$fetch_document = $this->User_model->select_where('borrower_document',array('id'=>$id));
		$fetch_document = $fetch_document->result();
		if($this->aws3->deleteObject($fetch_document[0]->borrower_document)){
			$delete = $this->User_model->delete('borrower_document',array('id'=>$id));
		}
		echo 1;
		
	}

	public function remove_pro_app_data(){

		$rowid = $this->input->post('rowid');
		$this->User_model->delete('borrower_property_approval', array('id'=>$rowid));
		echo '1';
	}

	public function fetch_pro_app_data(){

		$rowid = $this->input->post('rowid');
		$fetch_pro_app = $this->User_model->select_where('borrower_property_approval',array('id'=>$rowid));
		$fetch_pro_app = $fetch_pro_app->result();

		echo json_encode($fetch_pro_app);
	}

	public function PropertyApproval(){

		if(isset($_POST['submitppa'])){

			if($this->input->post('borrower_iddss') != ''){

				$data['borrower_id'] 		= $this->input->post('borrower_iddss');
				$data['req_status'] 		= $this->input->post('req_status');
				$data['pro_address'] 		= $this->input->post('pro_address');
				$data['pro_unit'] 			= $this->input->post('pro_unit');
				$data['pro_city'] 			= $this->input->post('pro_city');
				$data['pro_state'] 			= $this->input->post('pro_state');
				$data['pro_zip'] 			= $this->input->post('pro_zip');
				$data['purchase_price'] 	= $this->amount_format($this->input->post('purchase_price'));
				$data['rehab_cost'] 		= $this->amount_format($this->input->post('rehab_cost'));
				$data['completion_val'] 	= $this->amount_format($this->input->post('completion_val'));
				$data['ApprovalAmount'] 	= $this->amount_format($this->input->post('ApprovalAmount'));
				$data['LoanTerm'] 			= $this->input->post('LoanTerm');
				
				if ($this->input->post('pro_date') != '') {
					//$ApprovedDate = DateTime::createFromFormat('m-d-Y', $this->input->post('pro_date'));
					//$pro_date = $ApprovedDate->format('Y-m-d');
					$pro_date = input_date_format($this->input->post('pro_date'));
				} else {
					$pro_date = NULL;
				}

				$data['pro_date'] 			= $pro_date;

				$pro_app_ids				= $this->input->post('pro_app_ids');
				if($pro_app_ids != ''){

					$where['id'] = $pro_app_ids;
					$this->User_model->updatedata('borrower_property_approval',$where,$data);
					$this->session->set_flashdata('success','Property approval updated successfully!');
					redirect(base_url().'borrower_view/'.$this->input->post('borrower_iddss'), 'refresh');

				}else{

					$this->User_model->insertdata('borrower_property_approval',$data);
					$this->session->set_flashdata('success','Property approval added successfully!');
					redirect(base_url().'borrower_view/'.$this->input->post('borrower_iddss'), 'refresh');
				}
			}else{

				$this->session->set_flashdata('error','Please select a borrower first!');
				redirect(base_url().'borrower_view', 'refresh');
			}
		}else{
			redirect(base_url().'borrower_view', 'refresh');
		}
	}


	public function download_pre_approval_letter(){

		error_reporting(0);
		header("Cache-Control: "); // leave blank to avoid IE errors
		header("Pragma: "); // leave blank to avoid IE errors
		header("Content-type: application/octet-stream");
		// header("Content-type: application/vnd.ms-word");

		header("content-disposition: attachment;filename=Letter_of_credit.doc");
		echo '<html xmlns:v="urn:schemas-microsoft-com:vml"
				xmlns:o="urn:schemas-microsoft-com:office:office"
				xmlns:w="urn:schemas-microsoft-com:office:word"
				xmlns="http://www.w3.org/TR/REC-html40">

				<head>
				<meta http-equiv=Content-Type content="text/html; charset=utf-8">
				<meta name=ProgId content=Word.Document>
				<meta name=Generator content="Microsoft Word 9">
				<meta name=Originator content="Microsoft Word 9">
				<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->
				<title>title</title>
				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				body{
					font-size:10pt;
				}
				</style>
				</head>
				<body>';

		echo '<style>

				table.table{
					width:100%;
					padding : 3px 0px;
				}
				table.table th{
					text-align : left;
					font-size : 10pt;

				}
				table.table td{
					
					font-size : 10.5pt;
					padding-bottom: 10px;

				}
				table.two_contact_signer td{
					margin : 3px;
					font-size:10.5pt;
				}
				h1{
					text-align:center;
				}
				h2{
					text-align:center;
					font-family: Times New Roman;
					font-size: 18px;
				}
				p{
					font-size:10.5pt;
					word-spacing : 10px;
					padding : 2px 0px;

				}
				table{
					font-size:10.5pt;
				}
		</style>';


		$borrower_id = $this->uri->segment(3);
		$STATE_USA 	= $this->config->item('STATE_USA');

		$getborrowername = $this->User_model->select_where('borrower_data',array('id'=>$borrower_id));
		$getborrowername = $getborrowername->result();
		$b_name = $getborrowername[0]->b_name;
		$ApprovedAmount = $getborrowername[0]->ApprovedAmount;
		$ApprovedTerm = $getborrowername[0]->ApprovedTerm;
		$PurchasePrice = $getborrowername[0]->PurchasePrice;
		$RenovationCosts = $getborrowername[0]->RenovationCosts;
		$CompletionValue = $getborrowername[0]->CompletionValue;
		$loc_state = $getborrowername[0]->loc_state;

		$getcontactname = $this->User_model->query("SELECT c.contact_firstname, c.contact_lastname FROM borrower_contact as bc JOIN contact as c ON bc.contact_id = c.contact_id WHERE bc.borrower_id = '".$borrower_id."'");
		if($getcontactname->num_rows() > 0){
			$getcontactname = $getcontactname->result();
			$contactName = $getcontactname[0]->contact_firstname.' '.$getcontactname[0]->contact_lastname;
		}else{
			$contactName = '';
		}


		$Pre_approval_letter_doc = '';
		$Pre_approval_letter_doc .= '<table style="width:100%;font-size:8pt;">';
		$Pre_approval_letter_doc .= '<tr>';
		$Pre_approval_letter_doc .= '<td style="width:50%;"><img src="' . base_url() . 'assets/admin/layout4/img/logo-light.png" ></td>';
		$Pre_approval_letter_doc .= '<td style="width:50%;color:gray;"><p style="text-align:right;font-size:8pt;">TaliMar Financial, Inc.<br>16880 West Bernardo Drive, Suite #140<br> San Diego, CA 92127<br>Tel:858.613.0111<br>Toll Free:888.868.8467<br>www.talimarfinancial.com</p></td>';
		$Pre_approval_letter_doc .= '</tr>';
		$Pre_approval_letter_doc .= '</table>';
		
		$Pre_approval_letter_doc .= '<p>'.date('F d, Y').'</p>';

		$Pre_approval_letter_doc .= '<p>Re: Letter of Credit</p>';

		$Pre_approval_letter_doc .= '<p>Dear '.$contactName.':</p>';

		$Pre_approval_letter_doc .= '<p>'.$b_name.' has been conditionally approved for a letter of credit of $'.number_format($ApprovedAmount).' for the purchase and renovation of non-owner occupied residential single family and multi-family properties in '.$STATE_USA[$loc_state].'.</p>';

		$Pre_approval_letter_doc .= '<p>The terms of our loan approval are as follows:</p>';

		$Pre_approval_letter_doc .= '<table class="table" style="width:100%; vertical-align:top;">

								<tr>
									<td style="width:25%;"><b>Letter of Credit:</b></td>
									<td style="width:60%;">$'.number_format($ApprovedAmount).'</td>
								</tr>
								<tr>
									<td style="width:25%;"><b>Term:</b></td>
									<td style="width:60%;">'.$ApprovedTerm.' Months from first use of funds</td>
								</tr>

								<tr>
									<td style="width:25%;vertical-align:top;"><b>Limits:</b></td>
									<td style="width:60%;">Not to exceed '.number_format($PurchasePrice,2).'% of purchase price and '.number_format($RenovationCosts,2).'% of renovation costs or '.number_format($CompletionValue,2).'% of completion value, whichever is less</td>
								</tr>
								<tr>
									<td style="width:25%;"><b>Other Requirements:</b></td>
									<td style="width:60%;">Loan(s) Secured in 1st Position </td>
								</tr>
								<tr>
									<td style="width:25%;"><b></b></td>
									<td style="width:60%;">No Appraisal requirement</td>
								</tr>
								<tr>
									<td style="width:25%;"><b></b></td>
									<td style="width:60%;">No Property Inspection Requirement</td>
								</tr>
								<tr>
									<td style="width:25%;"><b></b></td>
									<td style="width:60%;">7 business day close from receipt of diligence material</td>
								</tr>
								
						</table>';

		$Pre_approval_letter_doc .= '<p>Our approval is effective for 180 days from the date of this letter.</p>';

		$Pre_approval_letter_doc .= '<p>TaliMar Financial is a private lender that specializes in funding residential and commercial real estate loans.</p>'; 

		$Pre_approval_letter_doc .= '<p>As a direct lender, we provide financing that often does not meet the lending guidelines of conventional lenders and will typically close within 5 to 7 business days. You may visit our website at www.talimarfinancial.com to learn more about our lending platform and view our most recent transactions.</p>';

		$Pre_approval_letter_doc .= '<p>I am available to discuss our funding process in further detail. I can be reached directly at (858) 613-0111</p>';

		$Pre_approval_letter_doc .= '<p>Sincerely,</p>';
		$Pre_approval_letter_doc .= '<p>Brock VandenBerg, President<br>TaliMar Financial, Inc.</p>';

		echo $Pre_approval_letter_doc;

	}

	public function pre_approval_letter_property(){

		error_reporting(0);
		header("Cache-Control: "); // leave blank to avoid IE errors
		header("Pragma: "); // leave blank to avoid IE errors
		header("Content-type: application/octet-stream");
		// header("Content-type: application/vnd.ms-word");

		header("content-disposition: attachment;filename=Property_approval.doc");
		echo '<html xmlns:v="urn:schemas-microsoft-com:vml"
				xmlns:o="urn:schemas-microsoft-com:office:office"
				xmlns:w="urn:schemas-microsoft-com:office:word"
				xmlns="http://www.w3.org/TR/REC-html40">

				<head>
				<meta http-equiv=Content-Type content="text/html; charset=utf-8">
				<meta name=ProgId content=Word.Document>
				<meta name=Generator content="Microsoft Word 9">
				<meta name=Originator content="Microsoft Word 9">
				<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->
				<title>title</title>
				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				body{
					font-size:10pt;
				}
				</style>
				</head>
				<body>';

		echo '<style>

				table.table{
					width:100%;
					padding : 3px 0px;
				}
				table.table th{
					text-align : left;
					font-size : 10pt;

				}
				table.table td{
					
					font-size : 10.5pt;
					padding-bottom: 10px;

				}
				table.two_contact_signer td{
					margin : 3px;
					font-size:10.5pt;
				}
				h1{
					text-align:center;
				}
				h2{
					text-align:center;
					font-family: Times New Roman;
					font-size: 18px;
				}
				p{
					font-size:10.5pt;
					word-spacing : 10px;
					padding : 2px 0px;

				}
				table{
					font-size:10.5pt;
				}
		</style>';

		$borrower_id = $this->uri->segment(3);
		$row_id = $this->uri->segment(4);

		$getborrowername = $this->User_model->select_where('borrower_data',array('id'=>$borrower_id));
		$getborrowername = $getborrowername->result();
		$b_name = $getborrowername[0]->b_name;

		$property_approval = $this->User_model->select_where('borrower_property_approval', array('borrower_id'=>$borrower_id, 'id'=>$row_id));
		if($property_approval->num_rows() > 0){
			$property_approval = $property_approval->result();
			$ApprovalAmount = $property_approval[0]->ApprovalAmount;
			$LoanTerm = $property_approval[0]->LoanTerm;
			$pro_Addreess = $property_approval[0]->pro_address.' '.$property_approval[0]->pro_unit.'; '.$property_approval[0]->pro_city.', '.$property_approval[0]->pro_state.' '.$property_approval[0]->pro_zip;
		}else{

			$ApprovalAmount = '';
			$LoanTerm = '';
			$pro_Addreess = '';
		}

		$getcontactname = $this->User_model->query("SELECT c.contact_firstname, c.contact_lastname FROM borrower_contact as bc JOIN contact as c ON bc.contact_id = c.contact_id WHERE bc.borrower_id = '".$borrower_id."'");
		if($getcontactname->num_rows() > 0){
			$getcontactname = $getcontactname->result();
			$contactName = $getcontactname[0]->contact_firstname.' '.$getcontactname[0]->contact_lastname;
		}else{
			$contactName = '';
		}


		$approval_letter_property = '';
		$approval_letter_property .= '<table style="width:100%;font-size:8pt;">';
		$approval_letter_property .= '<tr>';
		$approval_letter_property .= '<td style="width:50%;"><img src="' . base_url() . 'assets/admin/layout4/img/logo-light.png" ></td>';
		$approval_letter_property .= '<td style="width:50%;color:gray;"><p style="text-align:right;font-size:8pt;">TaliMar Financial, Inc.<br>16880 West Bernardo Drive, Suite #140<br> San Diego, CA 92127<br>Tel:858.613.0111<br>Toll Free:888.868.8467<br>www.talimarfinancial.com</p></td>';
		$approval_letter_property .= '</tr>';
		$approval_letter_property .= '</table>';
		
		$approval_letter_property .= '<p>'.date('F d, Y').'</p>';

		$approval_letter_property .= '<p>Re: Pre Approval Letter</p>';

		$approval_letter_property .= '<p>Dear '.$contactName.': </p>';

		$approval_letter_property .= '<p>'.$b_name.' has been conditionally approved for a purchase loan in the amount of $'.number_format($ApprovalAmount,2).' to purchase '.$pro_Addreess.'.</p>';

		$approval_letter_property .= '<p>The terms of our loan approval are as follows: </p>';

		$approval_letter_property .= '<table class="table" style="width:100%; vertical-align:top;">

								<tr>
									<td style="width:25%;"><b>Property Address:</b></td>
									<td style="width:60%;">'.$pro_Addreess.'</td>
								</tr>
								<tr>
									<td style="width:25%;"><b>Loan Amount:</b></td>
									<td style="width:60%;">$'.number_format($ApprovalAmount,2).'</td>
								</tr>
								<tr>
									<td style="width:25%;"><b>Term:</b></td>
									<td style="width:60%;">'.$LoanTerm.' Months </td>
								</tr>

								<tr>
									<td style="width:25%;"><b>Requirements:</b></td>
									<td style="width:60%;">Loan(s) Secured in 1st Position</td>
								</tr>
								<tr>
									<td style="width:25%;"><b></b></td>
									<td style="width:60%;">No Appraisal requirement</td>
								</tr>
								<tr>
									<td style="width:25%;"><b></b></td>
									<td style="width:60%;">No Property Inspection Requirement</td>
								</tr>
								<tr>
									<td style="width:25%;"><b></b></td>
									<td style="width:60%;">7 business day close from receipt of diligence material</td>
								</tr>
								<tr>
									<td style="width:25%;"><b></b></td>
									<td style="width:60%;">ALTA Lender Policy</td>
								</tr>

						</table>';

		$approval_letter_property .= '<p>Our approval is effective for 90 days from the date of this letter.</p>';

		$approval_letter_property .= '<p>TaliMar Financial is a private lender that specializes in funding residential and commercial real estate loans. As a direct lender, we provide financing that often does not meet the lending guidelines of conventional lenders and will typically close within 5 to 7 business days.</p>';

		$approval_letter_property .= '<p>You may visit our website at www.talimarfinancial.com to learn more about our lending platform and view our most recent transactions. I am also available to discuss our funding process in further detail. I can be reached directly at (858) 613-0111.</p>';

		$approval_letter_property .= '<p>Sincerely</p>';
		$approval_letter_property .= '<p>Brock VandenBerg, President<br>TaliMar Financial, Inc.</p>';
		echo $approval_letter_property;

	}

	public function change_property_approval_status(){
			$property_id = $this->input->post('property_id');
			$property_status = $this->input->post('property_status');

			$query = "UPDATE borrower_property_approval SET req_status = '$property_status' WHERE id = '$property_id'";

			if($this->User_model->query($query)){
				echo "success";
			}else{
				echo "error";
			}
	}
	 
}
?>

