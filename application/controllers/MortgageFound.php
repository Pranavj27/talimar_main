<?php
class MortgageFound extends MY_Controller {

	public function __construct() {
		ini_set('max_execution_time', 7200000);
		ini_set('memory_limit', '2048M');

		parent::__construct();
		$this->load->database();
		$this->load->model('User_model');
		$this->load->helper('fci_helper.php');
		$this->load->helper('common_functions.php');
		
		if($this->session->userdata('t_user_id') == '' )
		{
			redirect(base_url()."home",'refresh');
		}
	}

	public function index()
	{
		error_reporting(0);
		
		$data = array();
		$data['btn_name'] 	=  'Mortgage Fund';

		$data['content'] = $this->load->view('mortgage/index', $data, true);
		$this->load->view('template_files/template', $data);              
		
	}

	public function portfolioOverview()
	{
		error_reporting(0);
		$data = array();
		$top = array();
		$data['btn_name'] 	=  'Mortgage Fund';
		$condition = array();		
		$condition['select'] = 'AVG(loan.term_month) AS avgTerm, COUNT(loan.id) AS totalLoan, SUM(loan_assigment.investment) AS totalAmount, SUM(loan_assigment.payment) AS totalpayment';
		$condition['where'] = array('loan_assigment.lender_name' => '417', 'loan_servicing.loan_status' => '2');

		$condition['join_table'] = 'loan_assigment';
		$condition['join_on'] = 'loan.talimar_loan = loan_assigment.talimar_loan';
		$condition['join'] = 'inner';

		$condition['join_table2'] = 'loan_servicing';
		$condition['join_on2'] = 'loan.talimar_loan = loan_servicing.talimar_loan';
		$condition['join2'] = 'inner';

		$data['investorData'] = $this->CM->getdata('loan', $condition);

		$condition = array();
		$condition['select'] = 'COUNT(loan.id) AS totalLoan, loan.loan_type, SUM(loan_assigment.investment) AS investment, SUM(loan_assigment.payment) AS payment, loan_servicing.loan_status';

		$condition['where'] = 'loan_assigment.lender_name = "417" AND loan_servicing.loan_status = "2"'; 
		$condition['join_table'] = 'loan_assigment';
		$condition['join_on'] = 'loan.talimar_loan = loan_assigment.talimar_loan';
		$condition['join'] = 'inner';

		$condition['join_table2'] = 'loan_servicing';
		$condition['join_on2'] = 'loan.talimar_loan = loan_servicing.talimar_loan';
		$condition['join2'] = 'inner';
		$condition['group_by'] = 'loan.loan_type'; 

		$data['investorTypeData'] = $this->CM->getdataAll('loan', $condition);

		$condition = array();
		$condition['select'] = 'COUNT(loan.id) AS totalLoan, loan_property.property_type,SUM(loan_assigment.investment) AS investment, SUM(loan_assigment.payment) AS payment, SUM(loan_assigment.invester_yield_percent) AS invester_yield_percent, SUM(loan_servicing.servicing_lender_rate) AS servicing_lender_rate, loan_servicing.loan_status';

		$condition['where'] = 'loan_assigment.lender_name = "417" AND loan_servicing.loan_status = "2" AND loan_property.property_type != ""'; 
		$condition['join_table'] = 'loan_assigment';
		$condition['join_on'] = 'loan.talimar_loan = loan_assigment.talimar_loan';
		$condition['join'] = 'inner';

		$condition['join_table2'] = 'loan_servicing';
		$condition['join_on2'] = 'loan.talimar_loan = loan_servicing.talimar_loan';
		$condition['join2'] = 'inner';

		$condition['join_table3'] = 'loan_property';
		$condition['join_on3'] = 'loan.talimar_loan = loan_property.talimar_loan';
		$condition['join3'] = 'inner';

		$condition['group_by'] = 'loan_property.property_type'; 

		$data['propertyType'] = $this->CM->getdataAll('loan', $condition);

		$condition = array();
		$condition['select'] = 'COUNT(loan.id) AS totalLoan,loan_property.country,SUM(loan_assigment.investment) AS investment, SUM(loan_assigment.payment) AS payment, SUM(loan_assigment.invester_yield_percent) AS invester_yield_percent, SUM(loan_servicing.servicing_lender_rate) AS servicing_lender_rate, loan_servicing.loan_status';

		$condition['where'] = 'loan_assigment.lender_name = "417" AND loan_servicing.loan_status = "2" AND loan_property.property_type != ""'; 
		$condition['join_table'] = 'loan_assigment';
		$condition['join_on'] = 'loan.talimar_loan = loan_assigment.talimar_loan';
		$condition['join'] = 'inner';

		$condition['join_table2'] = 'loan_servicing';
		$condition['join_on2'] = 'loan.talimar_loan = loan_servicing.talimar_loan';
		$condition['join2'] = 'inner';

		$condition['join_table3'] = 'loan_property';
		$condition['join_on3'] = 'loan.talimar_loan = loan_property.talimar_loan';
		$condition['join3'] = 'inner';

		$condition['group_by'] = 'loan_property.country'; 

		$data['propertyCountry'] = $this->CM->getdataAll('loan', $condition);


		$condition = array();
		$condition['select'] = 'investor.id, investor.subscriptionStatus, name';
		$condition['where'] = array('investor.mortgageActivate' => 'yes');
		$count = 0;
		$amountTotalActive = 0.00;
		$amountTotalInProcess = 0.00;

		$investorTotal = $this->CM->getdataAll('investor', $condition);


		if($investorTotal){
			foreach ($investorTotal as $key => $value) {

				if($value->subscriptionStatus == 'Active'){
					$condition = array();
					$condition['select'] = 'SUM(lender_mortgage.amount) AS amountTotal';
					$condition['where'] = array('lender_mortgage.lenderId' => $value->id, 'lender_mortgage.fundStatus' => 'Received');
					$InvTotal = $this->CM->getdata('lender_mortgage', $condition);

					if(!empty($InvTotal) && $InvTotal->amountTotal > 0)
					{
						$amountTotalActive = $amountTotalActive + $InvTotal->amountTotal;
						
						$count++;
					}
				}

				
				$condition = array();
				$condition['select'] = 'SUM(lender_mortgage.in_process) AS amountTotal';
				$condition['where'] = array('lender_mortgage.lenderId' => $value->id);
				$InProcess = $this->CM->getdata('lender_mortgage', $condition);

				if(!empty($InProcess) && $InProcess->amountTotal > 0)
				{
					$amountTotalInProcess = $amountTotalInProcess + $InProcess->amountTotal;
				}
				
			}
		}


		// loan_servicing.payoff_date as payoff_date

		$condition = array();
		$condition['select'] = 'loan.id, loan.talimar_loan, loan_property.property_address, loan_property.unit, SUM(loan_assigment.investment) AS investment, pay_off_processing.de_Good_Thru_Date as payoff_date';

		$condition['where'] = 'loan_assigment.lender_name = "417" AND loan_servicing.loan_status = "2" AND loan_property.property_type != "" AND pay_off_processing.de_Payoff_Request = "1" '; 
		$condition['join_table'] = 'loan_assigment';
		$condition['join_on'] = 'loan.talimar_loan = loan_assigment.talimar_loan';
		$condition['join'] = 'inner';

		$condition['join_table2'] = 'loan_servicing';
		$condition['join_on2'] = 'loan.talimar_loan = loan_servicing.talimar_loan';
		$condition['join2'] = 'inner';

		$condition['join_table3'] = 'loan_property';
		$condition['join_on3'] = 'loan.talimar_loan = loan_property.talimar_loan';
		$condition['join3'] = 'inner';

		$condition['join_table4'] = 'pay_off_processing';
		$condition['join_on4'] = 'loan.talimar_loan = pay_off_processing.talimar_loan';
		$condition['join4'] = 'inner';

		$condition['group_by'] = 'loan.talimar_loan';

		$data['loanPayoff'] = $this->CM->getdataAll('loan', $condition);

		$condition = array();
		$condition['select'] = 'loan.id, loan.loan_funding_date, loan.talimar_loan, SUM(t2.investment) AS investment, t4.property_address, t4.unit';
		$condition['where'] = array('t3.loan_status' => '2', 't2.lender_name' => '417');

		$condition['join_table'] = 'loan_assigment AS t2';
		$condition['join_on'] = 'loan.talimar_loan = t2.talimar_loan';
		$condition['join'] = 'inner';

		$condition['join_table2'] = 'loan_servicing AS t3';
		$condition['join_on2'] = 'loan.talimar_loan = t3.talimar_loan';
		$condition['join2'] = 'inner';

		$condition['join_table3'] = 'loan_property AS t4';
		$condition['join_on3'] = 'loan.talimar_loan = t4.talimar_loan';
		$condition['join3'] = 'inner';
		$condition['group_by'] = 'loan.talimar_loan';

		$condition['order_by_index'] = 'investment';
		$condition['order_by'] = 'desc';
		$condition['limit'] = 5; 
		$condition['start'] = 0;


		$data['topLoan'] = $this->CM->getdataAll('loan', $condition);

		$condition = array();
		$condition['select'] = 'loan.id, loan.loan_funding_date, loan.talimar_loan, SUM(t2.investment) AS investment, t4.property_address, t4.unit';
		$condition['where'] = array('t3.loan_status' => '1', 't2.lender_name' => '417');

		$condition['join_table'] = 'loan_assigment AS t2';
		$condition['join_on'] = 'loan.talimar_loan = t2.talimar_loan';
		$condition['join'] = 'inner';

		$condition['join_table2'] = 'loan_servicing AS t3';
		$condition['join_on2'] = 'loan.talimar_loan = t3.talimar_loan';
		$condition['join2'] = 'inner';

		$condition['join_table3'] = 'loan_property AS t4';
		$condition['join_on3'] = 'loan.talimar_loan = t4.talimar_loan';
		$condition['join3'] = 'inner';
		$condition['group_by'] = 'loan.talimar_loan';

		$condition['order_by_index'] = 'investment';
		$condition['order_by'] = 'desc';
		$condition['limit'] = 5; 
		$condition['start'] = 0;


		$data['piplineLoan'] = $this->CM->getdataAll('loan', $condition);


		/* New wigdts */

		$totalWhole = 0;
		$totalWholeLoanAmount = 0;
		$countDatatotalWhole = 0;

		$totalFractional = 0;
		$totalFractionalLoanAmount = 0;
		$countDataFractional = 0;
		$LTV  = 0;

		$loan_schedule_investor = array();
				
		$fetch_investors = $this->User_model->query("SELECT * FROM investor Where id = '417' ORDER BY id DESC");

		$fetch_investors = $fetch_investors->result();

		foreach ($fetch_investors as $key => $investor_data) {

				$lender_name_new = $investor_data->id;

				$sql_assign = "SELECT la.talimar_loan, la.investment, la.payment, la.invester_yield_percent , ls.loan_status FROM loan_assigment as la JOIN loan_servicing as ls ON ls.talimar_loan = la.talimar_loan WHERE la.lender_name = " . $lender_name_new . " AND ls.loan_status = 2  GROUP BY la.talimar_loan";
				$fetch_assignment_data = $this->User_model->query($sql_assign);

				$fetch_assignment_data = $fetch_assignment_data->result();

				foreach ($fetch_assignment_data as $assigment_data) {
					$talimar['talimar_loan'] = $assigment_data->talimar_loan;
					// fetch all data from talimar loan

					$fetch_loan_data = $this->User_model->select_where('loan', $talimar);
					$fetch_loan_data = $fetch_loan_data->row();

					$loan_amount = $fetch_loan_data->loan_amount;
					$loan_id = $fetch_loan_data->id;
					$fci = $fetch_loan_data->fci;
					$term = $fetch_loan_data->term_month;
					$payment_amount = $fetch_loan_data->payment_amount;

					$LTV = $LTV + get_LTV_value($fetch_loan_data->talimar_loan, $fetch_loan_data->loan_amount);


					$sql = "SELECT SUM(investment) as total_balance FROM loan_assigment WHERE  talimar_loan ='" . $talimar_loan . "' ";

					$investment_total = $this->User_model->query($sql);

					if ($investment_total->num_rows() > 0) {
						$investment_total = $investment_total->row();
						$investment_total = $investment_total->total_balance;

					} else {
						$investment_total = 0;

					}

					$available = $loan_amount - $investment_total;
					$percent_yield_dollar="";
					$assigment_data->invester_yield_percent=(float)$assigment_data->invester_yield_percent;
					if($loan_amount>0 && $assigment_data->invester_yield_percent>0){
						$percent_yield_dollar=($loan_amount / 100) * $assigment_data->invester_yield_percent;
					}


											
					$cal_invest_perxcent = ($assigment_data->investment/$loan_amount)*100;

					if($cal_invest_perxcent == 100)
					{
						$totalWhole  = $totalWhole + $assigment_data->investment;
						$totalWholeLoanAmount = $totalWholeLoanAmount + $loan_amount;
						$countDatatotalWhole++;
					}
					else
					{
						$totalFractional = $totalFractional + $assigment_data->investment;
						$totalFractionalLoanAmount = $totalFractionalLoanAmount + $loan_amount;
						$countDataFractional++;
					}
				}
			
		}

		// echo '<pre>';

		// print_r($loan_schedule_investor);

		$data['totalAvailable'] = array('amountTotalActive'=>$amountTotalActive, 'amountTotalInProcess'=>$amountTotalInProcess, 'totalWhole' => $totalWhole, 'totalWholeLoanAmount' => $totalWholeLoanAmount, 'countDatatotalWhole' => $countDatatotalWhole, 'totalFractional' => $totalFractional, 'totalFractionalLoanAmount' => $totalFractionalLoanAmount, 'countDataFractional' => $countDataFractional, 'LTV' => $LTV);
		// print_r($data['totalAvailable']);

		$data['content'] = $this->load->view('mortgage/portfolioOverview', $data, true);
		$this->load->view('template_files/template', $data); 
	}

	public function investorOverview()
	{
		error_reporting(0);
		$data = array();
		$result = array();
		$data['btn_name'] 	=  'Mortgage Fund';

		$condition = array();
		$condition['select'] = 'investor.id, investor.subscriptionStatus';
		$condition['where'] = array('investor.mortgageActivate' => 'yes');
		$count = 0;
		$amountTotal = 0.00;

		$countProccess = 0;
		$amountTotalProccess = 0.00;

		$investorTotal = $this->CM->getdataAll('investor', $condition);
		if($investorTotal){
			foreach ($investorTotal as $key => $value) {

				if($value->subscriptionStatus === 'Active'){
					$condition['select'] = 'SUM(lender_mortgage.amount) AS amountTotal';
					$condition['where'] = array('lender_mortgage.lenderId' => $value->id, 'lender_mortgage.fundStatus' => 'Received');
					$InvTotal = $this->CM->getdata('lender_mortgage', $condition);

					if(!empty($InvTotal) && $InvTotal->amountTotal > 0)
					{
						$amountTotal = $amountTotal + $InvTotal->amountTotal;
						$count++;
					}
				}

				$condition = array();

				
				$condition['select'] = 'SUM(lender_mortgage.amount) AS amountTotal';
				$condition['where'] = array('lender_mortgage.lenderId' => $value->id, 'lender_mortgage.subscriptionStatus' => 'Processing');
				$InvTotal = $this->CM->getdata('lender_mortgage', $condition);

				if(!empty($InvTotal) && $InvTotal->amountTotal > 0)
				{
					$amountTotalProccess = $amountTotalProccess + $InvTotal->amountTotal;
					$countProccess++;
				}
				

				$condition = array();
			}
		}

		$data['investorTotal'] = array('totalInvestor' => $count, 'amountTotal' => $amountTotal, 'countProccess' => $countProccess, 'amountTotalProccess' => $amountTotalProccess, 'avgTotal' => $amountTotal/$count);
		
		$condition = array();

		$condition['select'] = 'id, talimar_lender, name, investor.investor_type, SUM(lender_mortgage.amount) AS amountTotal';
		$condition['where'] = array('investor.mortgageActivate' => 'yes', 'investor.subscriptionStatus' => 'Active','lender_mortgage.fundStatus' => 'Received');
		$condition['join_table'] = 'lender_mortgage';
		$condition['join_on'] = 'investor.id = lender_mortgage.lenderId';
		$condition['join'] = 'inner';
		$condition['group_by'] = 'investor.investor_type';
		
		$data['lenderDataType'] = $this->CM->getdataAll('investor', $condition);
		
		$condition = array();
		$condition['select'] = 'id, talimar_lender, name, investor.investor_type, SUM(lender_mortgage.amount) AS amountTotal';
		$condition['where'] = array('investor.mortgageActivate' => 'yes', 'investor.subscriptionStatus' => 'Active','lender_mortgage.fundStatus' => 'Received');
		$condition['join_table'] = 'lender_mortgage';
		$condition['join_on'] = 'investor.id = lender_mortgage.lenderId';
		$condition['join'] = 'left';
		$condition['group_by'] = 'investor.id';
		$condition['order_by_index'] = 'amountTotal';
		$condition['order_by'] = 'DESC';
		$condition['limit'] = 5;
		$condition['start'] = 0;
		$data['lenderTop'] = $this->CM->getdataAll('investor', $condition);


		$condition = array();

		$condition['select'] = 'investor.id';
		$condition['where'] = array('investor.mortgageActivate' => 'yes', 'investor.subscriptionStatus' => 'Active', 'investor.distribution' => 'Distribute');
		
		$DistributeData = $this->CM->getdataAll('investor', $condition);
		$amountTotal = 0;
		$count = 0;
		$condition = array();
		if($DistributeData){
			foreach ($DistributeData as $key => $value) {
				$condition['select'] = 'SUM(lender_mortgage.amount) AS amountTotal';
				$condition['where'] = array('lender_mortgage.lenderId' => $value->id, 'lender_mortgage.fundStatus' => 'Received');
				$InvTotal = $this->CM->getdata('lender_mortgage', $condition);

				if(!empty($InvTotal) && $InvTotal->amountTotal > 0)
				{
					$amountTotal = $amountTotal + $InvTotal->amountTotal;
					$count++;
				}
			}
		}


		$data['distributeData'] = array('totalId' => $count++, 'amountTotal' => $amountTotal);


		$condition = array();

		$condition['select'] = 'investor.id';
		$condition['where'] = array('investor.mortgageActivate' => 'yes', 'investor.subscriptionStatus' => 'Active', 'investor.distribution' => 'Reinvest');
		
		$ReinvestData = $this->CM->getdataAll('investor', $condition);
		$amountTotal = 0;
		$count = 0;
		$condition = array();
		if($ReinvestData){
			foreach ($ReinvestData as $key => $value) {
				$condition['select'] = 'SUM(lender_mortgage.amount) AS amountTotal';
				$condition['where'] = array('lender_mortgage.lenderId' => $value->id, 'lender_mortgage.fundStatus' => 'Received');
				$InvTotal = $this->CM->getdata('lender_mortgage', $condition);

				if(!empty($InvTotal) && $InvTotal->amountTotal > 0)
				{
					$amountTotal = $amountTotal + $InvTotal->amountTotal;
					$count++;
				}
			}
		}


		$data['ReinvestData'] = array('totalId' => $count++, 'amountTotal' => $amountTotal);


		$condition = array();

		$condition['select'] = 'investor.id';
		$condition['where'] = array('investor.mortgageActivate' => 'yes', 'investor.subscriptionStatus' => 'Active', 'investor.distribution' => 'Both');
		
		$ReinvestData = $this->CM->getdataAll('investor', $condition);
		$amountTotal = 0;
		$count = 0;
		$condition = array();
		if($ReinvestData){
			foreach ($ReinvestData as $key => $value) {
				$condition['select'] = 'SUM(lender_mortgage.amount) AS amountTotal';
				$condition['where'] = array('lender_mortgage.lenderId' => $value->id, 'lender_mortgage.fundStatus' => 'Received');
				$InvTotal = $this->CM->getdata('lender_mortgage', $condition);

				if(!empty($InvTotal) && $InvTotal->amountTotal > 0)
				{
					$amountTotal = $amountTotal + $InvTotal->amountTotal;
					$count++;
				}
			}
		}


		$data['BothData'] = array('totalId' => $count++, 'amountTotal' => $amountTotal);

		



		// New
		$condition = array();
		$condition['select'] = 'id, SUM(lender_mortgage.amount) AS amountTotal, lender_mortgage.transactionType AS transactionType';
		$condition['where'] = array('investor.mortgageActivate' => 'yes', 'investor.subscriptionStatus' => 'Active', 'lender_mortgage.transactionType != ' => 'Other');
		$condition['join_table'] = 'lender_mortgage';
		$condition['join_on'] = 'investor.id = lender_mortgage.lenderId';
		$condition['join'] = 'inner';
		$condition['group_by'] = 'lender_mortgage.transactionType';
		
		$data['lenderCapitalType'] = $this->CM->getdataAll('investor', $condition);

		$condition = array();
		$condition['select'] = 'id, SUM(lender_mortgage.amount) AS amountTotal, lender_mortgage.transactionType AS transactionType';
		$condition['where'] = array('investor.mortgageActivate' => 'yes', 'investor.subscriptionStatus' => 'Active', 'lender_mortgage.transactionType != ' => 'Other');
		$condition['join_table'] = 'lender_mortgage';
		$condition['join_on'] = 'investor.id = lender_mortgage.lenderId';
		$condition['join'] = 'inner';
		
		
		$data['lenderCapitalTypeTotal'] = $this->CM->getdata('investor', $condition);

		$condition = array();

		$condition['select'] = 'AVG(loan.term_month) AS avgTerm, COUNT(loan.id) AS totalLoan, SUM(loan_assigment.investment) AS totalAmount, SUM(loan_assigment.payment) AS totalpayment';
		$condition['where'] = array('loan_assigment.lender_name' => '417', 'loan_servicing.loan_status' => '2');

		$condition['join_table'] = 'loan_assigment';
		$condition['join_on'] = 'loan.talimar_loan = loan_assigment.talimar_loan';
		$condition['join'] = 'inner';

		$condition['join_table2'] = 'loan_servicing';
		$condition['join_on2'] = 'loan.talimar_loan = loan_servicing.talimar_loan';
		$condition['join2'] = 'inner';

		$data['investorData'] = $this->CM->getdata('loan', $condition);

		$condition = array();

		$condition['select'] = 'name, accredited, subscriptionStatus';
		$condition['where'] = array('mortgageActivate' => 'yes', 'subscriptionStatus' => 'In Process');
		$data['accountProcessing'] = $this->CM->getdataAll('investor', $condition);


		$condition = array();

		$arrayData = array();
		$condition['select'] = 'investor.id, investor.name, 0 AS amountTotal';
		$condition['where'] = array('investor.mortgageActivate' => 'yes');
		
		$accountProcessingAm = $this->CM->getdataAll('investor', $condition);

		if($accountProcessingAm)
		{
			foreach($accountProcessingAm as $am)
			{
				$condition['select'] = 'mortgageId, SUM(amount) AS amountTotal, date';
				$condition['where'] = 'lenderId = '.$am->id.' AND lender_mortgage.subscriptionStatus = "Processing"';
				$tran = $this->CM->getdata('lender_mortgage', $condition);

				if($tran && !empty($tran->amountTotal))
				{
					$am->amountTotal = $tran->amountTotal;
					$am->date = $tran->date;

					$arrayData[] = $am;
				}
			}
		}
		
		
		$data['accountProcessingAm'] = $arrayData;
		

		$data['content'] = $this->load->view('mortgage/investorOverview', $data, true);
		$this->load->view('template_files/template', $data); 
	}

	public function loanPortfolio()
	{
		$data  = array();
		$data['btn_name'] 	=  'Mortgage Fund';

		$condition['select'] = 'loan.id, loan.talimar_loan, loan.current_balance, loan.intrest_rate, loan.loan_amount, loan.borrower, SUM(t2.investment) AS investment, t2.invester_yield_percent, t3.servicing_lender_rate, t4.property_address, t4.unit, t4.city, t4.state, t4.zip, t5.b_name';
		$condition['where'] = array('t3.loan_status' => '2', 't2.lender_name' => '417');

		$condition['join_table'] = 'loan_assigment AS t2';
		$condition['join_on'] = 'loan.talimar_loan = t2.talimar_loan';
		$condition['join'] = 'inner';

		$condition['join_table2'] = 'loan_servicing AS t3';
		$condition['join_on2'] = 'loan.talimar_loan = t3.talimar_loan';
		$condition['join2'] = 'inner';

		$condition['join_table3'] = 'loan_property AS t4';
		$condition['join_on3'] = 'loan.talimar_loan = t4.talimar_loan';
		$condition['join3'] = 'inner';

		$condition['join_table4'] = 'borrower_data AS t5';
		$condition['join_on4'] = 'loan.borrower = t5.id';
		$condition['join4'] = 'inner';

		$condition['group_by'] = 'loan.talimar_loan';

		
		$data['investorData'] = $this->CM->getdataAll('loan', $condition);

		$condition = array();
		$condition['select'] = 'loan.id, loan.talimar_loan, SUM(t2.investment) AS totalInvestor, t2.invester_yield_percent, t3.servicing_lender_rate';
		$condition['where'] = array('t3.loan_status' => '2', 't2.lender_name' => '417');

		$condition['join_table'] = 'loan_assigment AS t2';
		$condition['join_on'] = 'loan.talimar_loan = t2.talimar_loan';
		$condition['join'] = 'inner';

		$condition['join_table2'] = 'loan_servicing AS t3';
		$condition['join_on2'] = 'loan.talimar_loan = t3.talimar_loan';
		$condition['join2'] = 'inner';
		$data['investorTotal'] = $this->CM->getdata('loan', $condition);
		
		$data['content'] = $this->load->view('mortgage/loanPortfolio', $data, true);
		$this->load->view('template_files/template', $data);
	}

	public function editDetails($lenderId = '')
	{
		error_reporting(0);
		$data = array();
		$data['lenderId'] = $lenderId;
		$condition = array();
		$condition['select'] = 'id,name';
		$condition['where'] = 'mortgageActivate = "no" OR id = "'.$lenderId.'"';


		$lenderDataAll = $this->CM->getdataAll('investor', $condition);
		$data['lenderDataAll'] = $lenderDataAll;
		if(empty($lenderId))
		{
			$data['btn_name'] = '';
			$data['lenderData'] = '';
			$data['lenderDataAll'] = $lenderDataAll;
 			$data['content'] = $this->load->view('mortgage/edit', $data, true);
			$this->load->view('template_files/template', $data);
		}
		else
		{
			$condition['select'] = '*';
			$condition['where'] = array("mortgageActivate" => "yes", "id" => $lenderId);
			$lenderData = $this->CM->getdata('investor', $condition);

			if($lenderData)
			{
				$condition['select'] = 'SUM(amount) AS amountTotal';
				$condition['where'] = array("lenderId" => $lenderId, 'fundStatus' => 'Received');
				$mortgage = $this->CM->getdata('lender_mortgage', $condition);

				$lenderData->amountTotal = $mortgage->amountTotal;

				$condition['select'] = 'SUM(in_process) AS amountIn';
				$condition['where'] = array("lenderId" => $lenderId);
				$mortgage = $this->CM->getdata('lender_mortgage', $condition);

				$lenderData->amountIn = $mortgage->amountIn;

			}

			$data['lenderData'] = $lenderData;

			$condition['select'] = 'lender_contact.lender_id,contact.contact_id,contact.contact_firstname,contact.contact_middlename,contact.contact_lastname,contact.contact_email,contact.contact_phone';
			$condition['where'] = array('lender_contact.lender_id' => $lenderId);

			$condition['join_table'] = 'lender_contact';
			$condition['join_on'] = 'contact.contact_id = lender_contact.contact_id';
			$condition['join'] = 'left';

			$data['contacts'] = $this->CM->getdataAll('contact', $condition);

			$data['btn_name'] = '';

			$data['content'] = $this->load->view('mortgage/edit', $data, true);
			$this->load->view('template_files/template', $data);
		}
		
	}

	public function ajaxFilter()
	{

		$result = false;
		$post = $this->input->post();

		$data = array('mortgageActivate' => 'yes');

		if(!empty($post['investor_id']))
		{

				$where = array('id'=>$post['investor_id']);
				$numRows = $this->CM->datacount('investor', $where);
				
				if($numRows > 0)
				{
					$result = $this->CM->updateWithOutAffdata('investor', $data, $where);
				}
			
		}
		echo $result;
	}

	public function ajaxGetLender()
	{
		$account_type_option = $this->config->item('account_type_option');

		$post = $this->input->post();

		$result = '';

		$where = 'mortgageActivate = "yes"';

		if(!empty($post['subscriptionStatus']))
		{
			$where = $where . ' AND investor.subscriptionStatus = "'.$post['subscriptionStatus'].'" ';
		}

		if(!empty($post['accredited']))
		{
			$where = $where . ' AND investor.accredited = "'.$post['accredited'].'" ';
		}

		if(!empty($post['investor_id']))
		{
			$where = $where . ' AND id = "'.$post['investor_id'].'" ';
		}

		$condition['select'] = 'id, talimar_lender, name, mortgageStatus, investor.subscriptionStatus, investor.account_type, account_status, amountInvested';
		$condition['where'] = $where;
		$condition['join_table'] = 'lender_mortgage';
		$condition['join_on'] = 'investor.id = lender_mortgage.lenderId';
		$condition['join'] = 'left';
		$condition['group_by'] = 'investor.id';
		$condition['order_by_index'] = 'name';
		$condition['order_by'] = 'asc';

		$lenderData = $this->CM->getdataAll('investor', $condition);

		$account_status_option = $this->config->item('account_status_option');
		$count = 0;
		$amountT = 0;
		$amountInPro = 0;
		if(!empty($lenderData))
		{
			foreach($lenderData as $row)
			{
				$condition = array();

				$condition['select'] = 'lender_contact.lender_id,contact.contact_id,contact.contact_firstname,contact.contact_middlename,contact.contact_lastname,contact.contact_email,contact.contact_phone';
				$condition['where'] = array('lender_contact.lender_id' => $row->id);

				$condition['join_table'] = 'lender_contact';
				$condition['join_on'] = 'contact.contact_id = lender_contact.contact_id';
				$condition['join'] = 'left';

				$contact = $this->CM->getdata('contact', $condition);

				if($contact)
				{
					$name = '';

					if($contact->contact_firstname != '')
					{
						$name = $contact->contact_firstname;
					}
					if($contact->contact_middlename != '')
					{
						$name = ($name != ''?$name.' '.$contact->contact_middlename:$contact->contact_middlename); 
					}
					if($contact->contact_lastname != ''){

						$name = ($name != ''?$name.' '.$contact->contact_lastname:$contact->contact_lastname); 
					}

					$phone = $contact->contact_phone;
					$email = $contact->contact_email;	
				}
				else
				{
					$name = '';
					$phone = '';
					$email = '';
				}

				$condition = array();

				$condition['select'] = 'SUM(amount) as amountTotal';
				$condition['where'] = array('lenderId' => $row->id, 'fundStatus' => 'Received');				
				$amount = $this->CM->getdata('lender_mortgage', $condition);

				$condition = array();
				$condition['select'] = 'SUM(in_process) as inProcess';
				$condition['where'] = array('lenderId' => $row->id);				
				$amountIn = $this->CM->getdata('lender_mortgage', $condition);

				$account_status_option = $this->config->item('account_status_option');
				$url = base_url()."mortgageFund/editDetails/".$row->id;
				$url1 = base_url()."investor_view/".$row->id;
				$accountType = !empty($row->account_type)?$account_type_option[$row->account_type]:'';

				if($this->session->userdata('user_role') == 2)
				{
					$link = '<a class="text-danger" href="javaScript:void(0)" onclick="deleteRow('.$row->id.')"><i class="fa fa-trash"></i></a>';
				}
				else
				{
					$link = '';
				}

				$result .= '<tr>
					<td><a href="'.$url1.'" >' .$row->name.'</a> </td>
					<td>'.$row->subscriptionStatus.'</td>
					<td>'.$accountType.'</td>
					<td>'.$name.'</td>
					<td>'.$phone.'</td>
					<td>'.$email.'</td>
					<td>'. '$'.number_format($amountIn->inProcess,2).'</td>
					<td>'. '$'.number_format($amount->amountTotal,2).'</td>
					<td><a class="text-info" href="'.$url.'" ><i class="fa fa-edit"></i></a> </td>

					
				</tr>';	

				$amountT = $amountT + $amount->amountTotal;
				$amountInPro = $amountInPro + $amountIn->inProcess;
				$count ++;			
			}
		}
		else{
			$result = '<tr><td colspan="9" style="text-align:center">Data not found</td></tr>';
		}
		echo json_encode(array('result'=>$result, 'amountInPro'=>'$'.number_format($amountInPro,2), 'amountT'=>'$'.number_format($amountT,2), 'count'=>$count));
	}

	public function ajaxGetLender1()
	{
		$account_type_option = $this->config->item('account_type_option');

		$post = $this->input->post();
		
		$orderby = 'name';
		$order = 'ASC';

		if($post){
			if($post['order'][0]['column']){
				$columnIndex = $post['order'][0]['column']; // Column index
				$columnName = $post['columns'][$columnIndex]['data']; // Column name
				$orderby = $columnName;
			}
			if($post['order'][0]['dir']){
				$order = $post['order'][0]['dir'];
			}
		}

		$startfrom = $post['start'];
		$queryLength = $post['length'];

		$result = array();

		$where = 'mortgageActivate = "yes"';

		if(!empty($post['subscriptionStatus']))
		{
			$where = $where . ' AND investor.subscriptionStatus = "'.$post['subscriptionStatus'].'" ';
		}

		if(!empty($post['accredited']))
		{
			$where = $where . ' AND investor.accredited = "'.$post['accredited'].'" ';
		}

		if(!empty($post['investor_id']))
		{
			$where = $where . ' AND id = "'.$post['investor_id'].'" ';
		}

		if(!empty($post['distribution']))
		{
			$where = $where . ' AND distribution = "'.$post['distribution'].'" ';
		}

		if(!empty($post['transactionStatus']))
		{
			$where = $where . ' AND lender_mortgage.subscriptionStatus = "'.$post['transactionStatus'].'" ';
		}

		

		$condition['select'] = 'id, talimar_lender, name, mortgageStatus, investor.subscriptionStatus, lender_mortgage.subscriptionStatus as transactionStatus,  investor.account_type, account_status, amountInvested, SUM(in_process) as inProcess, SUM(amount) as amountTotal';
		$condition['where'] = $where;
		$condition['join_table'] = 'lender_mortgage';
		$condition['join_on'] = 'investor.id = lender_mortgage.lenderId';
		$condition['join'] = 'left';
		$condition['group_by'] = 'investor.id';
		
		$lenderTotal = $this->CM->getdataAll('investor', $condition);
		$condition = array();
		$condition['select'] = 'id, talimar_lender, investor.investor_type, name, mortgageStatus, investor.subscriptionStatus, investor.distribution, lender_mortgage.subscriptionStatus as transactionStatus, account_status, amountInvested, SUM(in_process) as inProcess, SUM(amount) as amountTotal';
		$condition['where'] = $where;
		$condition['join_table'] = 'lender_mortgage';
		$condition['join_on'] = 'investor.id = lender_mortgage.lenderId';
		$condition['join'] = 'left';
		$condition['group_by'] = 'investor.id';
		$condition['order_by_index'] = $orderby;
		$condition['order_by'] = $order;
		$condition['limit'] = $queryLength;
		$condition['start'] = $startfrom;

		$lenderData = $this->CM->getdataAll('investor', $condition);

		$query = $this->db->last_query();
		$account_status_option = $this->config->item('account_status_option');
		$investor_type_option = $this->config->item('investor_type_option');
		$count = 0;
		$amountT = 0;
		
		$amountInPro = 0;
		if(!empty($lenderData))
		{
			foreach($lenderData as $row)
			{
				$condition = array();

				$condition['select'] = 'lender_contact.lender_id,contact.contact_id,contact.contact_firstname,contact.contact_middlename,contact.contact_lastname,contact.contact_email,contact.contact_phone';
				$condition['where'] = array('lender_contact.lender_id' => $row->id);

				$condition['join_table'] = 'lender_contact';
				$condition['join_on'] = 'contact.contact_id = lender_contact.contact_id';
				$condition['join'] = 'left';

				$contact = $this->CM->getdata('contact', $condition);

				if($contact)
				{
					$name = '';

					$contact_id = $contact->contact_id;

					if($contact->contact_firstname != '')
					{
						$name = $contact->contact_firstname;
					}
					if($contact->contact_middlename != '')
					{
						$name = ($name != ''?$name.' '.$contact->contact_middlename:$contact->contact_middlename); 
					}
					if($contact->contact_lastname != ''){

						$name = ($name != ''?$name.' '.$contact->contact_lastname:$contact->contact_lastname); 
					}

					$phone = $contact->contact_phone;

					if(!empty($contact->contact_email))
					{
						$email = '<a href="mailto:'.$contact->contact_email.'">'.$contact->contact_email.'</a>';
					}	
				}
				else
				{
					$name = '';
					$phone = '';
					$email = '';
					$contact_id = '';
				}

				$condition = array();

				$condition['select'] = 'SUM(amount) as amountTotal';
				$condition['where'] = array('lenderId' => $row->id, 'fundStatus' => 'Received');				
				$amount = $this->CM->getdata('lender_mortgage', $condition);

				$condition = array();
				$condition['select'] = 'SUM(in_process) as inProcess';
				$condition['where'] = array('lenderId' => $row->id);				
				$amountIn = $this->CM->getdata('lender_mortgage', $condition);

				$account_status_option = $this->config->item('account_status_option');
				$url = base_url()."mortgageFund/editDetails/".$row->id;
				$url1 = base_url()."investor_view/".$row->id;
				$url2 = !empty($contact_id)?base_url()."viewcontact/".$contact_id : '';
				$accountType = !empty($row->account_type)?$account_type_option[$row->account_type]:'';

				if($this->session->userdata('user_role') == 2)
				{
					$link = '<a class="text-danger" href="javaScript:void(0)" onclick="deleteRow('.$row->id.')"><i class="fa fa-trash"></i></a>';
				}
				else
				{
					$link = '';
				}

				$result[] = array("nameLender" =>'<a href="'.$url1.'" >' .$row->name.'</a>',
						   "subscriptionStatus" =>$row->subscriptionStatus === 'In Process' ? 'Processing': $row->subscriptionStatus,
						   "investorType" => !empty($row->investor_type)?$investor_type_option[$row->investor_type] : '',
						   "distribution" =>$row->distribution,
						   "name" =>!empty($url2)?'<a href="'.$url2.'" >' .$name.'</a>' : '',
						   "phone" =>$phone,
						   "email" =>$email,
						   "inProcess" =>'$'.number_format($amountIn->inProcess,2),
						   "amountTotal" =>'$'.number_format($row->amountTotal,2),
						   "edit" =>'<a class="text-info" href="'.$url.'" ><i class="fa fa-eye"></i></a>',

					);	

				$amountT = $amountT + $row->amountTotal;
				$amountInPro = $amountInPro + $amountIn->inProcess;
				$count ++;			
			}
		}
		else{
			//$result = '<tr><td colspan="9" style="text-align:center">Data not found</td></tr>';
		}

		$condition = array();

		$condition['select'] = 'SUM(amount) as amountTotal';
		$condition['where'] = $where. ' AND fundStatus = "Received"';
		$condition['join_table'] = 'lender_mortgage';
		$condition['join_on'] = 'investor.id = lender_mortgage.lenderId';
		$condition['join'] = 'left';
		
		$amountData = $this->CM->getdata('investor', $condition);

		$condition = array();
		
		$condition['select'] = 'SUM(in_process) as inProcess';
		$condition['where'] = $where;
		$condition['join_table'] = 'lender_mortgage';
		$condition['join_on'] = 'investor.id = lender_mortgage.lenderId';
		$condition['join'] = 'left';
		
		$inprogressData = $this->CM->getdata('investor', $condition);
		
		echo json_encode(array('data'=>$result, 'total' => count($lenderTotal), 'total1' => count($result), 'queryData' => $query, 'amountT' => '$'.number_format($amountT), 'amountInPro' => '$'.number_format($inprogressData->inProcess)));
	}

	public function dataDeleteEdit($id = '')
	{
		
		$data = array('mortgageActivate' => 'no', 'amountInvested' => 0, 'subscriptionStatus' => 'In Process', 'distribution' => '', 'Reinvest' => 0, 'accredited' => 'no');

		$result = false;

		if(!empty($id))
		{
			$where = array('id'=>$id);
			$numRows = $this->CM->datacount('investor', $where);
				
			if($numRows > 0)
			{
				$result = $this->CM->updateWithOutAffdata('investor', $data, $where);

				if($result)
				{
					$where = array('lenderId'=>$id);
					$result = $this->CM->deletedata('lender_mortgage', $where);
				}
			}	
		}

		if($result)
		{
			$this->session->set_flashdata('success', 'Data has been Deleted successfully !');                 
		}
		else
		{
			$this->session->set_flashdata('error', 'Data has been Deleted failed !');   
		}

		

		$url = base_url('mortgageFund');

		redirect($url);
	}

	public function ajaxDelete()
	{
		$post = $this->input->post();
		$data = array('mortgageActivate' => 'no', 'amountInvested' => 0, 'subscriptionStatus' => 'In Process', 'distribution' => '', 'Reinvest' => 0, 'accredited' => 'no');

		$result = false;

		if(!empty($post['id']))
		{
			$where = array('id'=>$post['id']);
			$numRows = $this->CM->datacount('investor', $where);
				
			if($numRows > 0)
			{
				$result = $this->CM->updateWithOutAffdata('investor', $data, $where);

				if($result)
				{
					$where = array('lenderId'=>$post['id']);
					$result = $this->CM->deletedata('lender_mortgage', $where);
				}
			}	
		}
		echo $result;
	}

	public function updateMortgage($lenderId = '')
	{
		$post = $this->input->post();

		
		$data = array('mortgageActivate' => 'no');

		$result;


		if(empty($lenderId))
		{
			$result = false;
			
		}
		else
		{

			if(empty($post['saveTransation']))
			{
				$array1 = array(
					'mortgageActivate' => 'yes',
					"accredited" => (!empty($post['accredited'])?$post['accredited']:"no"),
					"mortgageTabAccess" => (!empty($post['mortgageTabAccess'])?$post['mortgageTabAccess']:""),
					"investor_type" => !empty($post['investor_type'])?$post['investor_type'] : '',
					"distribution" => (!empty($post['distribution'])?$post['distribution']:""),
					"Reinvest" => (!empty($post['Reinvest'])?$post['Reinvest']:""),
					"accountType" => (!empty($post['accountType'])?$post['accountType']:""),
					"subscriptionStatus" => (!empty($post['subscriptionStatus1'])?$post['subscriptionStatus1']:""),
					"verivestAccount" => (!empty($post['verivestAccount'])?$post['verivestAccount']:""),
				);
					
				$where = array('id'=>$lenderId);
				$numRows = $this->CM->datacount('investor', $where);

				if($numRows > 0)
				{
					$result = $this->CM->updateWithOutAffdata('investor', $array1, $where);
				}
				
			}
			else{

					$array3 = array();			
					if(!empty($post['date']))
					{
						foreach($post['date'] as $key => $value){
							$array3 = array();
							if(!empty($value))
							{
								$dC = date('Y');
							
								$date = str_replace('/', '-', $value);
								$explode = explode('-', $date);

								if($dC != $explode[2])
								{
									$explodeDate = $value;
								}
								else{

									$explodeDate =  $explode[2].'-'.$explode[0].'-'.$explode[1];
								}
								
								$array3 = array(
								"lenderId" => $lenderId,
								"date" => $explodeDate,
								"transactionType" => $post['transactionType'][$key],
								"depositType" => $post['depositType'][$key],
								"in_process" => (double)str_replace(',', '', str_replace('$', '', $post['in_process'][$key])),
								"subscriptionStatus" => $post['subscriptionStatus'][$key],
								"fundStatus" => $post['fundStatus'][$key],
								"amount" => (double)str_replace(',', '', str_replace('$', '', $post['amount'][$key]))
								);

								$result = $this->CM->insertData('lender_mortgage', $array3);

							}
						}
					}
			}
		}

		// echo $result;

		if($result)
		{
			$this->session->set_flashdata('success', 'Data has been updated successfully !');                 
		}
		else
		{
			$this->session->set_flashdata('error', 'Data has been updated failed !');   
		}

		echo $result;

		$url = base_url()."MortgageFound/transactionData/".$lenderId;

		redirect($url);
	}
	

	public function selectOption()
	{
		$condition['select'] = 'id,name as text';
		$condition['where'] = array('mortgageActivate' => 'yes');
		$lenderData = $this->CM->getdataAll('investor', $condition);

		$condition = array();
		$condition['select'] = 'id,name as text';
		$condition['where'] = array('mortgageActivate' => 'no');
		$lenderDataAll = $this->CM->getdataAll('investor', $condition);

		$array = array('lenderDataAll' => $lenderDataAll, 'selectLender' => $lenderData);
		echo json_encode($array);
	}

	public function loanCompair()
	{
		$fci = getLoanPortfolio(true, '');

		if(!empty($fci))
		{
			$numRows = $this->CM->deletedatAll('lender_payment_history', array('id !=' => ''));

			if($numRows > 0)
			{
				$array = array();
				foreach($fci as $value)
				{
					$loan = $this->User_model->query("SELECT loan.id, loan.fci, loan.current_balance, s.closing_status, s.loan_status, loan.talimar_loan FROM loan INNER JOIN loan_servicing AS s ON s.talimar_loan = loan.talimar_loan WHERE fci = '".$value->loanAccount."'");
					$record = $loan->row();
					if($record)
					{

						$currentBalance = (double)str_replace(',', '', $value->currentBalance);
						$difference1 = (double)$record->current_balance - $currentBalance;
						$talimar_loan = $record->talimar_loan;
					}
					else
					{
						$difference1 = (double)str_replace(',', '', $value->currentBalance);
						$talimar_loan = '';
					}

					$array[] = array(
						"talimar_loan" => $talimar_loan,
						"difference" => $difference1,
						"loanAccount" => $value->loanAccount, 
						"lenderAccount" => $value->lenderAccount, 
						"originalBalance" => $value->originalBalance, 
						"currentBalance" => $value->currentBalance, 
						"totalPayment" => $value->totalPayment,
						"daysLate" => $value->daysLate,
						"nextDueDate" => $value->nextDueDate,
						"loanStatus " => $value->loanStatus
					);
				}
				
				$result = $this->CM->insertMultipleData('lender_payment_history', $array);
				
			}
		}

		echo 'Done';
	}


	public function paymentCompareDashboard()
	{
		$condition = array();

		$condition['select'] = 'lp.lenderAccount, lp.difference, i.name, i.fci_acct';
		
		$condition['join_table2'] = 'investor as i';
		$condition['join_on2'] = 'lp.lenderAccount = i.fci_acct';
		$condition['join2'] = 'inner';

		$condition['group_by'] = 'lp.lenderAccount';
		$condition['where'] = "i.fci_acct != ''";

		$data['lenderData'] = $this->CM->getdataAll('lender_payment_history as lp', $condition);
		echo '<pre>';
		print_r($data['lenderData']);
	}

	public function ajaxGetTransaction()
	{
		$post = $this->input->post();
		$data = array();

		if(!empty($post))
		{	$condition['select'] = 'id,name as text';
			$condition['where'] = array('id'=>$post['lenderId']);
			$result = $this->CM->getdata('investor', $condition);

			if($result)
			{
				$condition = array();
				$data['status'] = true;
				$condition['select'] = '*';
				$condition['where'] = $post;
				$transactionType = $this->CM->getdata('lender_mortgage', $condition);

				
				$explode = explode('-', $transactionType->date);

				$explodeDate =  $explode[1].'-'.$explode[2].'-'.$explode[0];

				$transactionType->date = $explodeDate;

				if(!empty($transactionType->dateCompleted)){
					$explodeComplete = explode('-', $transactionType->dateCompleted);

					$dateCompleted =  $explodeComplete[1].'-'.$explodeComplete[2].'-'.$explodeComplete[0];

					$transactionType->dateCompleted = $dateCompleted;
				}
				else
				{
					$transactionType->dateCompleted = '';
				}

				$result->transactionType = $transactionType;
				$data['result'] = $result;
			}
			else
			{
				$data['status'] = false;
			}
		}
		else
		{
			$data['status'] = false;
		}

		echo json_encode($data);
	}

	public function ajaxUpdateTransaction()
	{
		$post = $this->input->post();


		if(!empty($post))
		{
			$date = str_replace('/', '-', $post['date']);
			$explode = explode('-', $date);

			$explodeDate =  $explode[2].'-'.$explode[0].'-'.$explode[1];

			$post['date'] = $explodeDate;

			if(!empty($post['dateCompleted'])){
				$explodeComplete = explode('-', $post['dateCompleted']);

				$dateCompleted =  $explodeComplete[2].'-'.$explodeComplete[0].'-'.$explodeComplete[1];

				$post['dateCompleted'] = $dateCompleted;
			}
			else
			{
				$post['dateCompleted'] = '';
			}
			$post['amount'] = str_replace('$', '', $post['amount']);
			

			$where = array('mortgageId'=>$post['mortgageId']);

			if(!empty($post['mortgageId']))
			{
				$result = $this->CM->updateWithOutAffdata('lender_mortgage', $post, $where);
			}
			else{
				unset($post['mortgageId']);
				$result = $this->CM->insertdata('lender_mortgage', $post, array());
			}
			

			if($result)
			{

				echo true;
			}
			else
			{
				echo false;
			}
		}
		else
		{
			echo false;
		}
	}

	public function ajaxGetTransactionAll()
	{
		$transactionType = array();
		$in_processTotalRecieved = 0.00;
		$amountTotal = 0.00;
		$amountTotal1 = 0.00;
		$recieveCount = 0;
		$in_processTotal = 0.00;
		$data = array();
		$lenderId = $this->input->post('lenderId');
		if(!empty($lenderId))
		{
			$condition['select'] = '*';
			$condition['where'] = $this->input->post();
			$condition['order_by_index'] = 'date';
			$condition['order_by'] = 'DESC';

			$transactionType = $this->CM->getdataAll('lender_mortgage', $condition);


			if($transactionType)
			{
				foreach($transactionType as $key => $value)
				{
						$explode = explode('-', $value->date);

						$explodeDate =  $explode[1].'-'.$explode[2].'-'.$explode[0];

						$value->date = $explodeDate;

						
						if(!empty($value->dateCompleted)){
							$explodeComplete = explode('-', $value->dateCompleted);

							$dateCompleted =  $explodeComplete[1].'-'.$explodeComplete[2].'-'.$explodeComplete[0];

							$value->dateCompleted = $dateCompleted;
						}
						else
						{
							$value->dateCompleted = '';
						}

						
						if($value->fundStatus == 'Received')
						{
							$amountTotal1 = (double)$amountTotal1 + (double)$value->amount;
							$in_processTotalRecieved = (double)$in_processTotalRecieved + (double)$value->in_process;
							$recieveCount++;
						}

						if($value->fundStatus == 'Received')
						{
							$value->fundStatus == 'Confirmed';
						}

						$amountTotal = (double)$amountTotal + (double)$value->amount;

						$value->amount = '$'.number_format((double)$value->amount, 2);

						$in_processTotal = (double)$in_processTotal + (double)$value->in_process;

						$value->in_process = '$'.number_format((double)$value->in_process, 2);

						$transactionType[$key] = $value;

							
				}
			}
		}

		$data['transactionType'] = $transactionType;
		$data['total'] = COUNT($transactionType);
		$data['amountTotal'] = '$'.number_format((double)$amountTotal,2);
		$data['amountTotalRecieved'] = '$'.number_format((double)$amountTotal1,2);
		$data['in_processTotalRecieved'] = '$'.number_format((double)$in_processTotalRecieved,2);
		$data['in_processTotal'] = '$'.number_format((double)$in_processTotal,2);
		$data['recieveTotal'] = $recieveCount;

		echo json_encode($data);
	}

	public function ajaxDeleteTransaction()
	{
		$post = $this->input->post();
		$result = false;
		if(!empty($post))
		{
			$result = $this->CM->deletedata('lender_mortgage', $post);
		}

		echo $result;
	}

	public function transactionData($lenderId = '')
	{
		if(empty($lenderId))
		{
			redirect(base_url().'mortgageFund');
		}
			
		$data = array();
		$data['btn_name'] 	=  'Mortgage Fund';
		$data['lenderId'] 	=  $lenderId;

		$condition['select'] = 'id,name';
		$condition['where'] = array('id' => $lenderId);
		$data['lenderData'] = $this->CM->getdata('investor', $condition);

		$data['content'] = $this->load->view('mortgage/transaction', $data, true);
		$this->load->view('template_files/template', $data);
	}

	public function creditLine()
	{

		$query 	= $this->User_model->query("select * from fund_data_mortgage ORDER BY CAST(`available_credit` AS UNSIGNED) DESC");
		if($query->num_rows()>0){

			$fetch_data=$query->result();
			$data['fund_data_result']=$fetch_data;

		}else{

		$data['fund_data_result']='';	
		}
		$data['content'] 	= $this->load->view('mortgage/creditLine',$data,true);
		
		$this->load->view('template_files/template',$data);
		
	}

	public function add_update_data(){

	 	error_reporting(0);
	 	$account_name=$this->input->post('account_name');
	 	$available_credit=$this->input->post('available_credit');
	 	$drawn=$this->input->post('drawn');
	 	$remaining=$this->input->post('remaining');
	 	$rate=$this->input->post('rate');
	 	$t_user_id=$this->session->userdata('t_user_id');
	 	$id=$this->input->post('id');
	 	
	  
	 	foreach ($id as $key => $row) 
	 	{
	       if($account_name[$key] != '')
	       {
	       
		 		if($row !='new')
		 		{

		 			 $sql="UPDATE `fund_data_mortgage` SET `account_name` = '".$account_name[$key]."', `available_credit`='".$this->amount_format($available_credit[$key])."',  `rate`='".$this->amount_format($rate[$key])."', `drawn`='".$this->amount_format($drawn[$key])."', `remaining`='".$this->amount_format($remaining[$key])."', `user_id`='".$t_user_id."' WHERE  id ='".$row."'";
		 			
		 		}else{


					  $sql="INSERT INTO `fund_data_mortgage` ( `account_name`,`available_credit`,`rate`,`drawn`,`remaining`,`user_id`) VALUES ( '".$account_name[$key]."', '".$this->amount_format($available_credit[$key])."','".$this->amount_format($rate[$key])."','".$this->amount_format($drawn[$key])."','".$this->amount_format($remaining[$key])."','".$t_user_id."')";

	 		        }
	      
	      

	      		$this->User_model->query($sql);
	      		
	      		$this->session->set_flashdata('success', 'Credit Line Updated Successfully');
	      	}
	 	}

	 	
		redirect(base_url('MortgageFound/creditLine'),'refresh');
	}

	public function delete_fund_data(){
 		error_reporting(0);
 		$id['id']=$this->input->post('id');
		
		$this->User_model->delete('fund_data_mortgage',$id);
 	}

 	public function amount_format($n)
	{
		$n = str_replace(',','',$n);
		$a = trim($n , '$');
		$b = trim($a , ',');
		$c = trim($b , '%');
		return $c;
	}
}