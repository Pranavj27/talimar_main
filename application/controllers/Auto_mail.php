<?php
class Auto_mail extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('Aws3','aws3');
		$this->load->helper('aws3_helper.php');
		$this->load->helper('fci_helper.php');
		$this->load->helper('mailhtml_helper.php');
		$this->load->database();
		$this->load->helper(array('form', 'url'));
		$this->load->model('User_model');
		$this->load->model('Common_model', 'CM');
		$this->load->library('MYPDF');
		$this->load->library('email');
		$this->load->helper('common_functions.php');

		//$this->load->library('Sendgridtalimar','sendgrid');
	}

	public function auto_trigger_mail_status(){

		//echo "auto trigger mail status";
		$data = file_get_contents("php://input");
		$data_responce = json_decode($data);
		if(count($data_responce) > 0)
		{
			foreach ($data_responce as $key => $mailValue) 
			{
				$email = $mailValue->email;
			    $event = $mailValue->event;
			    $ip = $mailValue->ip;
			    $response = $mailValue->response;
			    $sg_event_id = $mailValue->sg_event_id;
			    $messageEvent = explode('.', $mailValue->sg_message_id);
			    $sg_message_id = $messageEvent[0];

			    $timestamp = $mailValue->timestamp;

			    $emailQuery = $fetch	= $this->User_model->query("SELECT * FROM sandgrid_mail_tracking WHERE to_email = '$email' AND message_id = '$sg_message_id'");
				

				if($emailQuery->num_rows() > 0)
				{
					$next_update_at = date("Y-m-d H:i:s");
				    $resultEmailQuery = $emailQuery->row();
				    $this->User_model->query("UPDATE sandgrid_mail_tracking SET sandgrid_status = '$event', next_update_at = '$next_update_at' WHERE to_email = '$email' AND message_id = '$sg_message_id'");						
				    
				}				
			}
		}
	}

	public function get_loan_talimar_id($loan_id){
		$talimarData = array();
		$talimarData['id'] = $loan_id;
		$fetch_loanData    = $this->User_model->select_where('loan',$talimarData);
		$fetch_loanData    = $fetch_loanData->row();
		if($fetch_loanData)
		{
			return $fetch_loanData->talimar_loan;
		}
		return '';
	}

	public function get_loadn_property_address($talimar_loan){
		$talimarAddress = array();
		$talimarAddress['talimar_loan'] = $talimar_loan;
		$fetch_loanProperty 			= $this->User_model->select_where('loan_property',$talimarAddress);
		$fetch_loanProperty 			= $fetch_loanProperty->row();

		$fulladdress = '';
		if($fetch_loanProperty->unit != ''){
            $fulladdress = $fetch_loanProperty->property_address.' '.$fetch_loanProperty->unit.'; '.$fetch_loanProperty->city.', '.$fetch_loanProperty->state.' '.$fetch_loanProperty->zip;
        }else{
            $fulladdress = $fetch_loanProperty->property_address.'; '.$fetch_loanProperty->city.', '.$fetch_loanProperty->state.' '.$fetch_loanProperty->zip;
        }
        return $fulladdress;
	}
	public function get_loadn_street_address($talimar_loan){
		$talimarAddress = array();
		$talimarAddress['talimar_loan'] = $talimar_loan;
		$fetch_loanProperty 			= $this->User_model->select_where('loan_property',$talimarAddress);
		$fetch_loanProperty 			= $fetch_loanProperty->row();

		$fulladdress = '';
		if($fetch_loanProperty->unit != ''){
            $fulladdress = $fetch_loanProperty->property_address;
        }else{
            $fulladdress = $fetch_loanProperty->property_address;
        }
        return $fulladdress;
	}

	public function loan_payment_reminder(){

		$loanPaymentReminder = array();

		$where = '';
		$data = '';
		
		$id = array();
		$borrowerid = array();
		$condition['select'] = 'loan.talimar_loan, loan.id,loan.totalPaymentFCI, loan.borrower';
		$condition['where'] = array("loan.paymentReminder" => "on", "servicing.loan_status" => "2");
		$condition['join_table'] = 'loan_servicing AS servicing';
		$condition['join_on'] = 'loan.talimar_loan = servicing.talimar_loan';
		$condition['join'] = 'inner';
		$loan_search = $this->CM->getdataAll('loan', $condition);
		
		if($loan_search)
		{
			$count = COUNT($loan_search);
			$a = 1;
			$id = '';
			$borrowerid = '';

			foreach($loan_search AS $key => $loan)
			{
				if($a == $count)
				{
					$id .= "'".$loan->talimar_loan."'";
					$borrowerid .= "'".$loan->borrower."'";
				}
				else{
					$id .= "'".$loan->talimar_loan."',";
					$borrowerid .= "'".$loan->borrower."',";	
				}

				$a++;
					
			}

			

			$m = date('Y-m-d');
			$m = date('m', strtotime('+1 month', strtotime($m)));
			$m = (string)$m;
			$zeroDate = (string)'0000-00-00';


		   $where = 'loan_servicing.talimar_loan IN ('.$id.') AND loan_property.talimar_loan IN ('.$id.') AND borrower_data.id IN ('.$borrowerid.')  AND lender_payment_history.nextDueDate != '.$zeroDate.' AND MONTH(lender_payment_history.nextDueDate) = '.$m;


			$loan_servicing_data = $this->User_model->query("SELECT `loan`.`fci`, `loan`.`first_payment_date`, if(MONTH(loan.first_payment_date) = '".$m."', loan.first_payment_date, lender_payment_history.nextDueDate) AS nextDueDate, `loan`.`id`, `loan`.`talimar_loan`, `loan`.`borrower`, `loan`.`intrest_rate`,`loan`.`current_balance`,`loan_servicing`.`sub_servicing_agent`, `borrower_data`.`b_name`, `borrower_data`.`c_email`, `lender_payment_history`.`nextDueDate`, `lender_payment_history`.`totalPayment` AS totalPaymentFCI, `loan_property`.`annual_insurance_payment`,`loan_property`.`annual_property_taxes` FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `loan_servicing` ON `loan`.`talimar_loan` = `loan_servicing`.`talimar_loan` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id` INNER JOIN `lender_payment_history` ON `loan`.`talimar_loan` = `lender_payment_history`.`talimar_loan` WHERE ".$where." GROUP BY loan_property.talimar_loan");

			$loanPaymentReminder = $loan_servicing_data->result();		
		}
		$data = array();
		$data['loan_payment_reminder'] = $loanPaymentReminder;
		$data['content'] = $this->load->view('mail_report/loan_payment_reminder', $data, true);
		$this->load->view('template_files/sidebar_automail', $data);

		
	}

		public function ajaxLoanPayment()
	{
		$where = '';
		$data = '';
		

		$ix = 0;

		$id = array();
		$borrowerid = array();
		$condition['select'] = 'loan.talimar_loan, loan.id,loan.totalPaymentFCI, loan.borrower';
		$condition['where'] = array("loan.paymentReminder" => "on", "servicing.loan_status" => "2");
		$condition['join_table'] = 'loan_servicing AS servicing';
		$condition['join_on'] = 'loan.talimar_loan = servicing.talimar_loan';
		$condition['join'] = 'inner';
		$loan_search = $this->CM->getdataAll('loan', $condition);
		
		if($loan_search)
		{
			$count = COUNT($loan_search);
			$a = 1;
			$id = '';
			$borrowerid = '';

			foreach($loan_search AS $key => $loan)
			{
				if($a == $count)
				{
					$id .= "'".$loan->talimar_loan."'";
					$borrowerid .= "'".$loan->borrower."'";
				}
				else{
					$id .= "'".$loan->talimar_loan."',";
					$borrowerid .= "'".$loan->borrower."',";	
				}

				$a++;
					
			}

			

			$m = date('Y-m-d');
			$m = date('m', strtotime('+1 month', strtotime($m)));
			$m = (string)$m;
			$zeroDate = (string)'0000-00-00';


		   $where = 'loan_servicing.talimar_loan IN ('.$id.') AND loan_property.talimar_loan IN ('.$id.') AND borrower_data.id IN ('.$borrowerid.')  AND lender_payment_history.nextDueDate != '.$zeroDate.' AND MONTH(lender_payment_history.nextDueDate) = '.$m;


			$loan_servicing_data = $this->User_model->query("SELECT `loan`.`fci`, `loan`.`first_payment_date`, if(MONTH(loan.first_payment_date) = '".$m."', loan.first_payment_date, lender_payment_history.nextDueDate) AS nextDueDate, `loan`.`id`, `loan`.`talimar_loan`, `loan`.`borrower`, `loan`.`totalPaymentFCI`, `loan_servicing`.`sub_servicing_agent`, `borrower_data`.`b_name`, `borrower_data`.`c_email`, `lender_payment_history`.`nextDueDate` FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `loan_servicing` ON `loan`.`talimar_loan` = `loan_servicing`.`talimar_loan` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id` INNER JOIN `lender_payment_history` ON `loan`.`talimar_loan` = `lender_payment_history`.`talimar_loan` WHERE ".$where." GROUP BY loan_property.talimar_loan");

			$loan_servicing_data = $loan_servicing_data->result();
			$b = 0;
			if($loan_servicing_data)
			{
				foreach($loan_servicing_data as $key => $loan_details)
				{				
						$Loan_Servicer = '';
						$Loan_ServicerAddress = array();
						$talimar_loan_id = $loan_details->talimar_loan;
						$loan_amount = $loan_details->totalPaymentFCI;
						$nextPayment = $loan_details->nextDueDate;
						$befordate = date('Y-m-d', strtotime('-7 days', strtotime($nextPayment)));
						$befordate = explode('-', $befordate);
						$befordate = $befordate[1].'-'.$befordate[2].'-'.$befordate[0];

						$firstbefordate = date('Y-m-d', strtotime('-7 days', strtotime($loan_details->first_payment_date)));

						$month = date('m');

						if($month == date('m', strtotime($firstbefordate)))
						{
							$firstbefordate = explode('-', $firstbefordate);
							$befordate = $firstbefordate[1].'-'.$firstbefordate[2].'-'.$firstbefordate[0];
						}

						$loan_account_no = $loan_details->fci;
						$todayDate = date("m-d-Y");

						$nextDate = explode('-', $nextPayment);

						if($loan_amount){

							$url = base_url("load_data/".$loan_details->id);

							$data .= '<tr>
										<td>'.$loan_details->b_name.'</td>
										<td>'.$loan_details->c_email.'</td>
										<td><a href="'.$url.'">'.$this->get_loadn_property_address($talimar_loan_id).'</a></td>
										<td>'.$talimar_loan_id.'</td>
										<td>$'.number_format($loan_amount, 2).'</td>
										<td>'.$nextDate[1].'-'.$nextDate[2].'-'.$nextDate[0].'</td>
										<td>'.$befordate.'</td>
									</tr>';

							$b++;

						}
					
				}
			}
		}
		## Response
      	$response = array(
          "status" => $b > 0?1:0,
          "result" => $data
      	);


      echo json_encode($response);
	}

	public function borrower_auto_mails(){
		
		$borrowerAutoMails = array();
		$ix = 0;
		$borrowerContactrem = array();	

		$data = array();
		$data['borrower_auto_mails'] = $borrowerAutoMails;
		$data['content'] = $this->load->view('mail_report/borrower_auto_mails', $data, true);
		$this->load->view('template_files/sidebar_automail', $data);
	}

	public function ajaxBorrower()
	{
		$where = '';
		$postData = $this->input->post();
		$fields = array();
		$result = array();
		$ix = 0;

		## Read value
		$draw = $postData['draw'];
		$start = $postData['start'];
		$rowperpage = $postData['length']; // Rows display per page
		$columnIndex = $postData['order'][0]['column']; // Column index
		$columnName1 = $postData['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $postData['order'][0]['dir']; // asc or desc
		$searchValue = $postData['search']['value']; // Search value


		$columnName = 'borrower_contact.contact_responsibility';
		$columnName2 = '';

		if($columnName1 == 'first_name')
		{
			$columnName2 = 'contact.contact_firstname';
		}

		if($columnName1 == 'last_name')
		{
			$columnName2 = 'contact.contact_lastname';
		}

		if($columnName1 == 'email')
		{
			$columnName2 = 'contact.contact_email';
		}

		if($columnName1 == 'address')
		{
			$columnName2 = 'contact.contact_address';
		}

		if($columnName1 == 'talimar_loan_no')
		{
			$columnName = 'loan.talimar_loan';
		}
		
		if($columnName1 == 'property_address')
		{
			$columnName = 'loan_property.property_address';
		}

		$condition['select'] = 'loan.borrower,loan.talimar_loan';
		$condition['where'] = array('loan.constructionUpdate'=>'on', 'loan_servicing.loan_status'=>'2');
		$condition['join_table2'] = 'loan_servicing';
		$condition['join_on2'] = 'loan.talimar_loan = loan_servicing.talimar_loan';
		$condition['join2'] = 'inner';

		$loanIn = $this->CM->getdataAll('loan', $condition);

		echo $this->db->last_query();

		if($loanIn)
		{
			$count = COUNT($loanIn);
			$a = 1;
			$id = '';
			$borrowerid = '';

			foreach($loanIn AS $key => $loan)
			{
				if($a == $count)
				{
					$id .= "'".$loan->talimar_loan."'";
					$borrowerid .= "'".$loan->borrower."'";
				}
				else{
					$id .= "'".$loan->talimar_loan."',";
					$borrowerid .= "'".$loan->borrower."',";	
				}

				$a++;
					
			}

			if($columnName2 != '')
			{
				$columnName = 'ORDER BY '.$columnName.' '.$columnSortOrder;
			}
			else
			{
				$columnName = '';
			}


			$where = 'loan_servicing.talimar_loan IN ('.$id.') AND loan_property.talimar_loan IN ('.$id.') AND borrower_contact.borrower_id IN ('.$borrowerid.')';

			$loanData =   $this->User_model->query("SELECT `loan`.`fci`, `loan`.`id`, `loan`.`talimar_loan`, `loan`.`borrower`, `loan`.`totalPaymentFCI`, `loan_servicing`.`sub_servicing_agent`, `borrower_contact`.`contact_id` FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `loan_servicing` ON `loan`.`talimar_loan` = `loan_servicing`.`talimar_loan`INNER JOIN `borrower_contact` ON `loan`.`borrower` = `borrower_contact`.`borrower_id` WHERE ".$where);

			$totalRecords = $loanData->num_rows();

			if(!empty($searchValue)){
				$where = $where.' AND (loan_property.property_address LiKE "%'.$searchValue.'%")';
			}


			$loanData =   $this->User_model->query("SELECT `loan`.`fci`, `loan`.`id`, `loan`.`talimar_loan`, `loan`.`borrower`, `loan`.`totalPaymentFCI`, `loan_servicing`.`sub_servicing_agent`, `borrower_contact`.`contact_id` FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `loan_servicing` ON `loan`.`talimar_loan` = `loan_servicing`.`talimar_loan`INNER JOIN `borrower_contact` ON `loan`.`borrower` = `borrower_contact`.`borrower_id` WHERE ".$where);

			$totalRecordwithFilter = $loanData->num_rows();

			$loanData =   $this->User_model->query("SELECT `loan`.`fci`, `loan`.`id`, `loan`.`talimar_loan`, `loan`.`borrower`, `loan`.`totalPaymentFCI`, `loan_servicing`.`sub_servicing_agent`, `borrower_contact`.`contact_id` FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `loan_servicing` ON `loan`.`talimar_loan` = `loan_servicing`.`talimar_loan`INNER JOIN `borrower_contact` ON `loan`.`borrower` = `borrower_contact`.`borrower_id` WHERE ".$where." ".$columnName." LIMIT ".$start.",".$rowperpage);

			$constructionSearch = $loanData->result();

					echo $this->db->last_query();
			foreach ($constructionSearch as $key => $value) {

				$b_con_id['contact_id'] = $value->contact_id;

				$condition = array();
				$condition['select'] = '*';
				$condition['where'] = array('contact_id'=>$value->contact_id);

				if($columnName2 != '')
				{
					$condition['order_by_index'] = $columnName2;
					$condition['order_by'] = $columnSortOrder;
				}

				$b_contacts = $this->CM->getdataAll('contact', $condition);

				foreach ($b_contacts as $loan_borrower) {
					$contact_id = $loan_borrower->contact_id;
					$Loan_ServicerAddress = array();

					if($loan_borrower->contact_address){
						$Loan_ServicerAddress[] = $loan_borrower->contact_address;
					}
					$unitAdd = '';
					if($loan_borrower->contact_unit){
						$unitAdd .= $loan_borrower->contact_unit.'';
					}
					//$unitAdd .= '#;';
					$Loan_ServicerAddress[] = $unitAdd;
					if($loan_borrower->contact_city){
						$Loan_ServicerAddress[] = ' '.$loan_borrower->contact_city.',';
					}
					if($loan_borrower->contact_state){
						$Loan_ServicerAddress[] = ''.$loan_borrower->contact_state;
					}
					if($loan_borrower->contact_zip){
						$Loan_ServicerAddress[] = ' '.$loan_borrower->contact_zip;
					}

					$contact_email = '';
					if($loan_borrower->contact_email){
						$contact_email = $loan_borrower->contact_email;
					}elseif($loan_borrower->contact_email2){
						$contact_email = $loan_borrower->contact_email2;
					}

					$borrowerContactrem[$contact_id]['first_name'] = $loan_borrower->contact_firstname;
					$borrowerContactrem[$contact_id]['last_name']= $loan_borrower->contact_lastname;
					$borrowerContactrem[$contact_id]['email']= $contact_email;
					$borrowerContactrem[$contact_id]['address'] = implode(' ', $Loan_ServicerAddress);

					$m_address = implode(' ', $Loan_ServicerAddress);
					$m_name    = $loan_borrower->contact_firstname;
					$m_email    = $contact_email;		
					$time = strtotime(date('Y-m-01'));
					$mailSendDate = date("Y-m-d", strtotime("+1 month", $time));

					$borrowerAutoMails[$ix]['first_name'] 		= $m_name;
					$borrowerAutoMails[$ix]['last_name'] 		= $loan_borrower->contact_lastname;
					$borrowerAutoMails[$ix]['first_name'] 		= $loan_borrower->contact_firstname;
					$borrowerAutoMails[$ix]['loan_id'] 			= $value->id;
					$borrowerAutoMails[$ix]['loan_ac_no'] 		= $value->fci;
					$borrowerAutoMails[$ix]['talimar_loan_no']	= $value->talimar_loan;
					$borrowerAutoMails[$ix]['address']			= $m_address;
					$borrowerAutoMails[$ix]['email']			= $m_email;
					$borrowerAutoMails[$ix]['mail_send_date']	= $mailSendDate;
					$url = base_url("load_data/".$value->id);
					$borrowerAutoMails[$ix]['property_address'] = '<a href="'.$url.'">'.$this->get_loadn_property_address($value->talimar_loan).'</a>';
					$ix++;
				}
								
			}
		}


				## Response
      	$response = array(
          "draw" => intval($draw),
          "iTotalRecords" => $totalRecords,
          "iTotalDisplayRecords" => $totalRecordwithFilter,
          "aaData" => $borrowerAutoMails
      	);


      echo json_encode($response);
	}

	public function loan_reminder_track(){
		$data = array();
		$data['reminder_track'] = array();
		$data['content'] = $this->load->view('mail_report/loan_reminder_track', $data, true);
		$this->load->view('template_files/sidebar_automail', $data);
	}

	public function ajaxLoanReminderTrack()
	{
		$where = '';
		$postData = $this->input->post();
		$fields = array();
		$result = array();


		## Read value
		$draw = $postData['draw'];
		$start = $postData['start'];
		$rowperpage = $postData['length']; // Rows display per page
		$columnIndex = $postData['order'][0]['column']; // Column index
		$columnName1 = $postData['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $postData['order'][0]['dir']; // asc or desc
		$searchValue = $postData['search']['value']; // Search value

		$columnName = '';

		if($columnName1 == 'BorrowerName')
		{
			$columnName = 'borrower_data.b_name';
		}

		if($columnName1 == 'Email')
		{
			$columnName = 'tracking.to_email';
		}

		if($columnName1 == 'sandgrid_status')
		{
			$columnName = 'tracking.sandgrid_status';
		}

		if($columnName1 == 'created_at')
		{
			$columnName = 'tracking.created_at';
		}

		if($columnName1 == 'StreetAddress')
		{
			$columnName = 'loan_property.property_address';
		}

		if($columnName)
		{
			$columnName = "ORDER BY ".$columnName." ".$columnSortOrder;
		}
		else
		{
			$columnName = '';
		}


		$condition['select'] = 'tracking.*, loan.talimar_loan';
		$condition['where'] = array('tracking.tracking_type'=>'paymentReminder', 'tracking.mail_tracking'=>'yes');

		$condition['join_table'] = 'loan';
		$condition['join_on'] = 'loan.id = tracking.loan_id';
		$condition['join'] = 'inner';

		$loan_search1 = $this->CM->getdataAll('sandgrid_mail_tracking AS tracking', $condition);
		$totalRecords=0;
		$totalRecordwithFilter=0;
		if($loan_search1){
			$count = COUNT($loan_search1);
			$a = 1;
			$id = '';
			$lid = '';
			$borrowerid = '';
			foreach ($loan_search1 as $key => $loan) {

				if($a == $count)
				{
					$id .= "'".$loan->talimar_loan."'";
					$borrowerid .= "'".$loan->borrower_id."'";
					$lid .= "'".$loan->loan_id."'";
				}
				else{
					$id .= "'".$loan->talimar_loan."',";
					$borrowerid .= "'".$loan->borrower_id."',";
					$lid .= "'".$loan->loan_id."',";	
				}

				$a++;
			}


			$condition = array();

			$where = 'tracking.loan_id IN ('.$lid.') AND loan.talimar_loan IN ('.$id.') AND loan_property.talimar_loan IN ('.$id.') AND borrower_data.id IN ('.$borrowerid.')';

			$totalRecords = $this->User_model->query("SELECT `tracking`.*, `loan`.`talimar_loan`, `borrower_data`.`b_name`FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `sandgrid_mail_tracking` AS `tracking` ON `tracking`.`loan_id` = `loan`.`id` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id` WHERE ".$where." GROUP BY loan_property.talimar_loan ");

			$totalRecords = $totalRecords->num_rows();

			if(!empty($searchValue)){
				$where = $where.' AND (borrower_data.b_name LiKE "%'.$searchValue.'%" OR loan_property.property_address LiKE "%'.$searchValue.'%")';
			}

			$totalRecordwithFilter = $this->User_model->query("SELECT `tracking`.*, `loan`.`talimar_loan`, `borrower_data`.`b_name`FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `sandgrid_mail_tracking` AS `tracking` ON `tracking`.`loan_id` = `loan`.`id` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id` WHERE ".$where." GROUP BY loan_property.talimar_loan ");

			$totalRecordwithFilter = $totalRecordwithFilter->num_rows();
			$queryData="SELECT `tracking`.*, `loan`.`talimar_loan`, `borrower_data`.`b_name`FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `sandgrid_mail_tracking` AS `tracking` ON `tracking`.`loan_id` = `loan`.`id` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id` WHERE ".$where." GROUP BY loan_property.talimar_loan ".$columnName." LIMIT ".$start.",".$rowperpage;

			$loan_search = $this->User_model->query("SELECT `tracking`.*, `loan`.`talimar_loan`, `borrower_data`.`b_name`FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `sandgrid_mail_tracking` AS `tracking` ON `tracking`.`loan_id` = `loan`.`id` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id` WHERE ".$where." GROUP BY loan_property.talimar_loan ".$columnName." LIMIT ".$start.",".$rowperpage);

			$loan_search = $loan_search->result();

			foreach ($loan_search as $key => $reminder) {
				if(strtolower($reminder->sandgrid_status)=="send"){
					$reminder->sandgrid_status="Sent";
				}
				$reminder_id     = $reminder->id;
				$loan_id 		 = $reminder->loan_id;
				$talimar_loan 	 = $reminder->talimar_loan;
				$to_email 		 = $reminder->to_email;
				$sandgrid_status = $reminder->sandgrid_status;
				$created_at 	 = $reminder->created_at;
				$next_update_at  = $reminder->next_update_at;
				$borrower_id 	 = $reminder->borrower_id;

				$fields['BorrowerName'] = $reminder->b_name;
				$fields['Email'] = $reminder->to_email;
				$fields['TalimarLoanId'] = $reminder->talimar_loan;
				$url = base_url("load_data/".$reminder->loan_id);
				$fields['StreetAddress'] = '<a href="'.$url.'">'.$this->get_loadn_street_address($talimar_loan).'</a>';
				$fields['sandgrid_status'] = $sandgrid_status;
				$fields['created_at'] = $created_at;
				$fields['action'] = '<button style="margin-right:5px;margin-bottom:5px;" type="button" class="btn btn-primary open_email_content" 
										to_email="'.$reminder->to_email.'" 
										to_cc="'.$reminder->to_cc.'" 
										to_bcc="'.$reminder->to_bcc.'" 
										from_email="'.$reminder->from_email.'" 
										from_name="'.$reminder->from_name.'" 
										subject="'.$reminder->subject.'"  
										message="'.$reminder->message.'"  
										attach_file="'.$reminder->attach_file.'"  
										><i class="fa fa-eye" aria-hidden="true"></i></button>
										<button type="button" class="btn btn-primary delete_email_content" reminder_id="'.$reminder_id.'" onclick="delete_email_content(this)">
											<i class="fa fa-trash"></i>
										</button>';
				$result[] = $fields;
			}
		}
		## Response
      	$response = array(
          "draw" => intval($draw),
          "iTotalRecords" => $totalRecords,
          "iTotalDisplayRecords" => $totalRecordwithFilter,
          "aaData" => $result
      	);


      echo json_encode($response);


	}

	public function borrower_reminder_track(){
		
		$data = array();
		$data['reminder_track'] = array();
		$data['content'] = $this->load->view('mail_report/borrower_reminder_track', $data, true);
		$this->load->view('template_files/sidebar_automail', $data);
	}

	public function ajaxBorrowerTrack()
	{
		$where = '';
		$postData = $this->input->post();
		$fields = array();
		$result = array();


		## Read value
		$draw = $postData['draw'];
		$start = $postData['start'];
		$rowperpage = $postData['length']; // Rows display per page
		$columnIndex = $postData['order'][0]['column']; // Column index
		$columnName1 = $postData['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $postData['order'][0]['dir']; // asc or desc
		$searchValue = $postData['search']['value']; // Search value


		$columnName = '';

		

		if($columnName1 == 'sandgrid_status')
		{
			$columnName = 'tracking.sandgrid_status';
		}

		if($columnName1 == 'created_at')
		{
			$columnName = 'tracking.created_at';
		}

		if($columnName1 == 'to_email')
		{
			$columnName = 'tracking.to_email';
		}

		

		if($columnName1 == 'StreetAddress')
		{
			$columnName = 'loan_property.property_address';
		}




		$condition['select'] = 'tracking.*, loan.talimar_loan';
		$condition['where'] = array('tracking.tracking_type'=>'paymentReminder', 'tracking.mail_tracking'=>'yes');

		$condition['join_table'] = 'loan';
		$condition['join_on'] = 'loan.id = tracking.loan_id';
		$condition['join'] = 'inner';

		$loan_search1 = $this->CM->getdataAll('sandgrid_mail_tracking AS tracking', $condition);

		if($loan_search1){
			$count = COUNT($loan_search1);
			$a = 1;
			$id = '';
			$lid = '';
			$borrowerid = '';
			foreach ($loan_search1 as $key => $loan) {

				if($a == $count)
				{
					$id .= "'".$loan->talimar_loan."'";
					$borrowerid .= "'".$loan->borrower_id."'";
					$lid .= "'".$loan->loan_id."'";
				}
				else{
					$id .= "'".$loan->talimar_loan."',";
					$borrowerid .= "'".$loan->borrower_id."',";
					$lid .= "'".$loan->borrower_id."',";	
				}

				$a++;
			}

			if($columnName)
			{
				$columnName = "ORDER BY ".$columnName." ".$columnSortOrder;
			}
			else
			{
				$columnName = '';
			}


			$condition = array();

			$where = 'tracking.loan_id IN ('.$lid.') AND loan.talimar_loan IN ('.$id.') AND loan_property.talimar_loan IN ('.$id.') AND borrower_contact.borrower_id IN ('.$borrowerid.')';

			$loanData = $this->User_model->query("SELECT `tracking`.*, `loan`.`talimar_loan`, `borrower_contact`.`borrower_id`, `borrower_contact`.`contact_id` FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `sandgrid_mail_tracking` AS `tracking` ON `tracking`.`loan_id` = `loan`.`id` INNER JOIN `borrower_contact` ON `loan`.`borrower` = `borrower_contact`.`borrower_id` WHERE ".$where);

			$totalRecords = $loanData->num_rows();


			if(!empty($searchValue)){
				$where = $where.' AND (loan_property.property_address LiKE "%'.$searchValue.'%")';
			}

			$loanData = $this->User_model->query("SELECT `tracking`.*, `loan`.`talimar_loan`, `borrower_contact`.`borrower_id`, `borrower_contact`.`contact_id` FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `sandgrid_mail_tracking` AS `tracking` ON `tracking`.`loan_id` = `loan`.`id` INNER JOIN `borrower_contact` ON `loan`.`borrower` = `borrower_contact`.`borrower_id` WHERE ".$where);

			$totalRecordwithFilter = $loanData->num_rows();

			$loanData = $this->User_model->query("SELECT `tracking`.*, `loan`.`talimar_loan`, `borrower_contact`.`borrower_id`, `borrower_contact`.`contact_id` FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `sandgrid_mail_tracking` AS `tracking` ON `tracking`.`loan_id` = `loan`.`id` INNER JOIN `borrower_contact` ON `loan`.`borrower` = `borrower_contact`.`borrower_id` WHERE ".$where." ".$columnName." LIMIT ".$start.",".$rowperpage);

			$loan_search = $loanData->result();

			foreach ($loan_search as $key => $reminder) {

				$reminder_id = $reminder->id;
				$loan_id 		= $reminder->loan_id;
				$talimar_loan 	= $reminder->talimar_loan;
				$to_email 		= $reminder->to_email;
				$sandgrid_status= $reminder->sandgrid_status;
				$created_at 	= $reminder->created_at;
				$next_update_at = $reminder->next_update_at;
				$borrower_id 	= $reminder->borrower_id;

				$condition = array();

				$condition['select'] = '*';
				$condition['where'] = array('contact_id'=>$reminder->contact_id);
				$contactData = $this->CM->getdata('contact', $condition);

				$c_first_name = '';
				$c_last_name = '';

				if($contactData){
					$c_first_name = $contactData->contact_firstname;
					$c_last_name = $contactData->contact_lastname;
				}

				$fields['c_last_name'] = $c_first_name;
				$fields['c_first_name'] = $c_last_name;
				$fields['to_email'] = $to_email;
				$fields['talimar_loan'] = $talimar_loan;
				$url = base_url("load_data/".$reminder->loan_id);
				$fields['StreetAddress'] = '<a href="'.$url.'">'.$this->get_loadn_property_address($talimar_loan).'</a>';
				$fields['sandgrid_status'] = $sandgrid_status;
				$fields['created_at'] = $created_at;
				$fields['action'] = '<button style="margin-right:5px;margin-bottom:5px;" type="button" class="btn btn-primary open_email_content" 
										to_email="'.$reminder->to_email.'" 
										to_cc="'.$reminder->to_cc.'" 
										to_bcc="'.$reminder->to_bcc.'" 
										from_email="'.$reminder->from_email.'" 
										from_name="'.$reminder->from_name.'" 
										subject="'.$reminder->subject.'"  
										message="'.$reminder->message.'"  
										attach_file="'.$reminder->attach_file.'"  
										><i class="fa fa-eye" aria-hidden="true"></i></button>
										<button type="button" class="btn btn-primary delete_email_content" reminder_id="'.$reminder_id.'" onclick="delete_email_content(this)">
											<i class="fa fa-trash"></i>
										</button>';
				$result[] = $fields;
			}
		}
		## Response
      	$response = array(
          "draw" => intval($draw),
          "iTotalRecords" => $totalRecords,
          "iTotalDisplayRecords" => $totalRecordwithFilter,
          "aaData" => $result
      	);


      echo json_encode($response);
	}

	public function delete_mail_contact(){
		if($this->input->post('mail_id'))
		{
			$mail_id = $this->input->post('mail_id');
			$contact_id['id'] = $mail_id;
			$this->User_model->delete('sandgrid_mail_tracking',$contact_id);
			echo "success";
		}else{
			echo "error";
		}
	}
	public function maturity_mail_remender(){
		$loanPaymentReminder = array();		
		$data = array();
		$data['loan_payment_reminder'] = $loanPaymentReminder;
		$data['content'] = $this->load->view('mail_report/maturity_mail_reminder', $data, true);
		$this->load->view('template_files/sidebar_automail', $data);
		
	}
	public function ajaxMaturityMail()
	{
		$where = '';
		$data = array();
		$postData = $this->input->post();
		$fields = array();
		$loanPaymentReminder = array();
		$b = 0;

		## Read value
		$draw = $postData['draw'];
		$start = $postData['start'];
		$rowperpage = $postData['length']; // Rows display per page
		$columnIndex = $postData['order'][0]['column']; // Column index
		$columnName1 = $postData['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $postData['order'][0]['dir']; // asc or desc
		$searchValue = $postData['search']['value']; // Search value

		$columnName = '';

		if($columnName1 == 'BorrowerName')
		{
			$columnName = 'borrower_data.b_name';
		}

		if($columnName1 == 'Email')
		{
			$columnName = 'borrower_data.c_email';
		}

		if($columnName1 == 'StreetAddress')
		{
			$columnName = 'loan_property.property_address';
		}

		if($columnName1 == 'TalimarLoanId')
		{
			$columnName = 'loan.talimar_loan';
		}

		$ix = 0;

		$id = array();
		$borrowerid = array();
		$condition['select'] = 'loan.talimar_loan, loan.id, loan.borrower';
		$condition['where'] = array("loan.maturityNotification" => "on", "servicing.loan_status" => "2");
		$condition['join_table'] = 'loan_servicing AS servicing';
		$condition['join_on'] = 'loan.talimar_loan = servicing.talimar_loan';
		$condition['join'] = 'inner';
		$loan_search = $this->CM->getdataAll('loan', $condition);
		if($loan_search)
		{
			$count = COUNT($loan_search);
			$a = 1;
			$id = '';
			$borrowerid = '';

			foreach($loan_search AS $key => $loan)
			{
				if($a == $count)
				{
					$id .= "'".$loan->talimar_loan."'";
					$borrowerid .= "'".$loan->borrower."'";
				}
				else{
					$id .= "'".$loan->talimar_loan."',";
					$borrowerid .= "'".$loan->borrower."',";	
				}

				$a++;
					
			}

			if($columnName)
			{
				$columnName = "ORDER BY ".$columnName." ".$columnSortOrder;
			}
			else
			{
				$columnName = '';
			}


			$where = 'loan_servicing.talimar_loan IN ('.$id.') AND loan_property.talimar_loan IN ('.$id.') AND borrower_data.id IN ('.$borrowerid.')  AND loan.new_maturity_date != "0000-00-00" ';

			$loan = $this->User_model->query("SELECT `loan`.`fci`, `loan`.`id`, `loan`.`talimar_loan`, `loan`.`borrower`, `loan`.`totalPaymentFCI`, `loan_servicing`.`sub_servicing_agent`, `borrower_data`.`b_name`, `borrower_data`.`c_email` FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `loan_servicing` ON `loan`.`talimar_loan` = `loan_servicing`.`talimar_loan` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id`  WHERE ".$where);

			$totalRecords = $loan->num_rows();

			if(!empty($searchValue)){
				$where = $where.' AND (borrower_data.b_name LiKE "%'.$searchValue.'%" OR loan_property.property_address LiKE "%'.$searchValue.'%")';
			}



			$loan = $this->User_model->query("SELECT `loan`.`fci`, `loan`.`id`, `loan`.`talimar_loan`, `loan`.`borrower`, `loan`.`totalPaymentFCI`, `loan_servicing`.`sub_servicing_agent`, `borrower_data`.`b_name`, `borrower_data`.`c_email` FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `loan_servicing` ON `loan`.`talimar_loan` = `loan_servicing`.`talimar_loan` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id`  WHERE ".$where);

			$totalRecordwithFilter = $loan->num_rows();
			
			$loan_servicing_data = $this->User_model->query("SELECT `loan`.`fci`, `loan`.`id`,`loan`.`new_maturity_date`,`loan`.`talimar_loan`, `loan`.`borrower`, `loan`.`totalPaymentFCI`, `loan_servicing`.`sub_servicing_agent`, `borrower_data`.`b_name`, `borrower_data`.`c_email`  FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `loan_servicing` ON `loan`.`talimar_loan` = `loan_servicing`.`talimar_loan` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id`  WHERE ".$where." ".$columnName." LIMIT ".$start.",".$rowperpage);
			$query="SELECT `loan`.`fci`, `loan`.`id`,`loan`.`new_maturity_date`,`loan`.`talimar_loan`, `loan`.`borrower`, `loan`.`totalPaymentFCI`, `loan_servicing`.`sub_servicing_agent`, `borrower_data`.`b_name`, `borrower_data`.`c_email` FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `loan_servicing` ON `loan`.`talimar_loan` = `loan_servicing`.`talimar_loan` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id`  WHERE ".$where." ".$columnName." LIMIT ".$start.",".$rowperpage;

			$loan_servicing_data = $loan_servicing_data->result();

			if($loan_servicing_data)
			{
				foreach($loan_servicing_data as $key => $loan_details)
				{				
						$Loan_Servicer = '';
						$Loan_ServicerAddress = array();
						$talimar_loan_id = $loan_details->talimar_loan;
						$loan_amount = $loan_details->totalPaymentFCI;
						$loan_account_no = $loan_details->fci;
						$maturityDate = $loan_details->new_maturity_date;
						$days45Before 				= date('Y-m-d',strtotime($maturityDate.'-45 day'));
						$days30Before 				= date('Y-m-d',strtotime($maturityDate.'-30 day'));
						$days20Before 				= date('Y-m-d',strtotime($maturityDate.'-20 day'));
						$days15Before 				= date('Y-m-d',strtotime($maturityDate.'-15 day'));
						$days10Before 				= date('Y-m-d',strtotime($maturityDate.'-10 day'));
						$days5Before 				= date('Y-m-d',strtotime($maturityDate.'-5 day'));
						$days1Before 				= date('Y-m-d',strtotime($maturityDate.'-1 day'));
						$upcoming_mail_string="";
						if(!empty($days45Before) && $days45Before!="0000-00-00"){
							$upcoming45days=date('m/d/Y',strtotime($days45Before));
							$upcoming_mail_string.=$upcoming45days;
						}
						/*if(!empty($days30Before) && $days30Before!="0000-00-00"){
							$upcoming30days=date('m/d/Y',strtotime($days30Before));
							$upcoming_mail_string.="<b>30 Days-</b>".$upcoming30days."<br>";
						}
						if(!empty($days20Before) && $days20Before!="0000-00-00"){
							$upcoming20days=date('m/d/Y',strtotime($days20Before));
							$upcoming_mail_string.="<b>20 Days-</b>".$upcoming20days."<br>";
						}
						if(!empty($days15Before) && $days15Before!="0000-00-00"){
							$upcoming15days=date('m/d/Y',strtotime($days15Before));
							$upcoming_mail_string.="<b>15 Days-</b>".$upcoming15days."<br>";
						}
						if(!empty($days10Before) && $days10Before!="0000-00-00"){
							$upcoming10days=date('m/d/Y',strtotime($days10Before));
							$upcoming_mail_string.="<b>10 Days-</b>".$upcoming10days."<br>";
						}
						if(!empty($days5Before) && $days5Before!="0000-00-00"){
							$upcoming5days=date('m/d/Y',strtotime($days5Before));
							$upcoming_mail_string.="<b>05 Days-</b>".$upcoming5days."<br>";
						}
						if(!empty($days1Before) && $days1Before!="0000-00-00"){
							$upcoming1days=date('m/d/Y',strtotime($days1Before));
							$upcoming_mail_string.="<b>01 Days-</b>".$upcoming1days."<br>";
						}*/
						$maturityDate=date('m/d/Y',strtotime($maturityDate));	
						$loanPaymentReminder['BorrowerName'] = $loan_details->b_name;
						$loanPaymentReminder['Email'] = $loan_details->c_email;
						$loanPaymentReminder['loan_account_no'] = $loan_account_no;
						$loanPaymentReminder['TalimarLoanId'] = $talimar_loan_id;
						// $loanPaymentReminder['LoanAmount'] = '$'.number_format($loan_amount, 2);
						$loanPaymentReminder['maturity_date'] = $maturityDate;
						$loanPaymentReminder['SentDate'] = $upcoming_mail_string;
						$loanPaymentReminder['type'] = "45 Days";
						$url = base_url("load_data/".$loan_details->id);
						$loanPaymentReminder['StreetAddress'] = '<a href="'.$url.'">'.$this->get_loadn_property_address($talimar_loan_id).'</a>';
						$loanPaymentReminder['loan_id'] = $loan_details->id;
						$data[] = $loanPaymentReminder;
						$b++;
				}
			}
		}
		## Response
      	$response = array(
      		'query'=>$query,
          "draw" => intval($draw),
          "iTotalRecords" => $totalRecords,
          "iTotalDisplayRecords" => $totalRecordwithFilter,
          "aaData" => $data
      	);


      echo json_encode($response);
	}
	public function maturity_mail_track(){
		$data = array();
		$data['reminder_track'] = array();
		$data['content'] = $this->load->view('mail_report/maturity_mail_track', $data, true);
		$this->load->view('template_files/sidebar_automail', $data);
	}
	public function ajaxMaturityMailTrack()
	{
		$where = '';
		$postData = $this->input->post();
		$fields = array();
		$result = array();


		## Read value
		$draw = $postData['draw'];
		$start = $postData['start'];
		$rowperpage = $postData['length']; // Rows display per page
		$columnIndex = $postData['order'][0]['column']; // Column index
		$columnName1 = $postData['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $postData['order'][0]['dir']; // asc or desc
		$searchValue = $postData['search']['value']; // Search value

		$columnName = '';
		if($columnName1 == 'BorrowerName')
		{
			$columnName = 'borrower_data.b_name';
		}
		if($columnName1 == 'Email')
		{
			$columnName = 'tracking.to_email';
		}
		if($columnName1 == 'sandgrid_status')
		{
			$columnName = 'tracking.sandgrid_status';
		}
		if($columnName1 == 'created_at')
		{
			$columnName = 'tracking.created_at';
		}
		if($columnName1 == 'StreetAddress')
		{
			$columnName = 'loan_property.property_address';
		}
		$loanSearchQuery = $this->User_model->query("SELECT tracking.*, loan.talimar_loan FROM loan INNER JOIN sandgrid_mail_tracking AS tracking  ON loan.id = tracking.loan_id WHERE tracking.tracking_type='maturityNotification' AND tracking.mail_tracking='yes' ");
		$loan_search1 = $loanSearchQuery->result();	
		$query="";	
		$totalRecords=0;
		$totalRecordwithFilter=0;
		if($loan_search1){
			$count = COUNT($loan_search1);
			$a = 1;
			$id = '';
			$lid = '';
			$borrowerid = '';
			foreach ($loan_search1 as $key => $loan) {
				if($a == $count)
				{
					$id .= "'".$loan->talimar_loan."'";
					$borrowerid .= "'".$loan->borrower_id."'";
					$lid .= "'".$loan->loan_id."'";
				}
				else{
					$id .= "'".$loan->talimar_loan."',";
					$borrowerid .= "'".$loan->borrower_id."',";
					$lid .= "'".$loan->borrower_id."',";	
				}
				$a++;
			}

			$condition = array();
			$where = 'tracking.tracking_type="maturityNotification" AND tracking.loan_id IN ('.$lid.') AND loan.talimar_loan IN ('.$id.') AND loan_property.talimar_loan IN ('.$id.') AND borrower_data.id IN ('.$borrowerid.')';
			$totalRecords = $this->User_model->query("SELECT `tracking`.*, `loan`.`talimar_loan`, `borrower_data`.`b_name`FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `sandgrid_mail_tracking` AS `tracking` ON `tracking`.`loan_id` = `loan`.`id` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id` WHERE ".$where);

			$totalRecords = $totalRecords->num_rows();

			if(!empty($searchValue)){
				$where = $where.' AND (borrower_data.b_name LiKE "%'.$searchValue.'%" OR loan_property.property_address LiKE "%'.$searchValue.'%")';
			}

			$totalRecordwithFilter = $this->User_model->query("SELECT `tracking`.*, `loan`.`talimar_loan`, `borrower_data`.`b_name`FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `sandgrid_mail_tracking` AS `tracking` ON `tracking`.`loan_id` = `loan`.`id` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id` WHERE ".$where);

			$totalRecordwithFilter = $totalRecordwithFilter->num_rows();
			$query="SELECT `tracking`.*, `loan`.`talimar_loan`, `borrower_data`.`b_name`FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `sandgrid_mail_tracking` AS `tracking` ON `tracking`.`loan_id` = `loan`.`id` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id` WHERE ".$where." ORDER BY ".$columnName." ".$columnSortOrder." LIMIT ".$start.",".$rowperpage;
			$loan_search = $this->User_model->query("SELECT `tracking`.*, `loan`.`talimar_loan`, `borrower_data`.`b_name`FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `sandgrid_mail_tracking` AS `tracking` ON `tracking`.`loan_id` = `loan`.`id` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id` WHERE ".$where." ORDER BY ".$columnName." ".$columnSortOrder." LIMIT ".$start.",".$rowperpage);

			$loan_search = $loan_search->result();
			foreach ($loan_search as $key => $reminder) {
				$reminder_id     = $reminder->id;
				$loan_id 		 = $reminder->loan_id;
				$talimar_loan 	 = $reminder->talimar_loan;
				$to_email 		 = $reminder->to_email;
				$sandgrid_status = $reminder->sandgrid_status;
				$created_at 	 = $reminder->created_at;
				$next_update_at  = $reminder->next_update_at;
				$borrower_id 	 = $reminder->borrower_id;
				$fields['BorrowerName'] = $reminder->b_name;
				$fields['Email'] = $reminder->to_email;
				$fields['TalimarLoanId'] = $reminder->talimar_loan;
				$url = base_url("load_data/".$reminder->loan_id);
				$fields['StreetAddress'] = '<a href="'.$url.'">'.$this->get_loadn_property_address($talimar_loan).'</a>';
				$fields['sandgrid_status'] = $sandgrid_status;
				$fields['created_at'] = $created_at;
				$fields['action'] = '<button style="margin-right:5px;margin-bottom:5px;" type="button" class="btn btn-primary open_email_content" 
										to_email="'.$reminder->to_email.'" 
										to_cc="'.$reminder->to_cc.'" 
										to_bcc="'.$reminder->to_bcc.'" 
										from_email="'.$reminder->from_email.'" 
										from_name="'.$reminder->from_name.'" 
										subject="'.$reminder->subject.'"  
										message="'.$reminder->message.'"  
										attach_file="'.$reminder->attach_file.'"  
										><i class="fa fa-eye" aria-hidden="true"></i></button>
										<button type="button" class="btn btn-primary delete_email_content" reminder_id="'.$reminder_id.'" onclick="delete_email_content(this)">
											<i class="fa fa-trash"></i>
										</button>';
				$result[] = $fields;
			}
		}
		## Response
      	$response = array(
      	   "$query"=>$query,
          "draw" => intval($draw),
          "iTotalRecords" => $totalRecords,
          "iTotalDisplayRecords" => $totalRecordwithFilter,
          "aaData" => $result
      	);


      echo json_encode($response);


	}
	public function insurance_expiring_upcoming(){
		$loanPaymentReminder = array();		
		$data = array();
		$data['loan_payment_reminder'] = $loanPaymentReminder;
		$data['content'] = $this->load->view('mail_report/insurance_mail_reminder', $data, true);
		$this->load->view('template_files/sidebar_automail', $data);
		
	}
	public function ajaxInsuranceMail()
	{
		$where = '';
		$data = array();
		$postData = $this->input->post();
		$fields = array();
		$loanPaymentReminder = array();
		$b = 0;

		## Read value
		$draw = $postData['draw'];
		$start = $postData['start'];
		$rowperpage = $postData['length']; // Rows display per page
		$columnIndex = $postData['order'][0]['column']; // Column index
		$columnName1 = $postData['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $postData['order'][0]['dir']; // asc or desc
		$searchValue = $postData['search']['value']; // Search value

		$columnName = '';

		if($columnName1 == 'BorrowerName')
		{
			$columnName = 'borrower_data.b_name';
		}

		if($columnName1 == 'Email')
		{
			$columnName = 'borrower_data.c_email';
		}

		if($columnName1 == 'StreetAddress')
		{
			$columnName = 'loan_property.property_address';
		}

		if($columnName1 == 'TalimarLoanId')
		{
			$columnName = 'loan.talimar_loan';
		}

		$ix = 0;

		$id = array();
		$borrowerid = array();
		$condition['select'] = 'loan.talimar_loan, loan.id, loan.borrower';
		$condition['where'] = array("loan.propertyInsurance" => "on", "servicing.loan_status" => "2");
		$condition['join_table'] = 'loan_servicing AS servicing';
		$condition['join_on'] = 'loan.talimar_loan = servicing.talimar_loan';
		$condition['join'] = 'inner';
		$loan_search = $this->CM->getdataAll('loan', $condition);
		if($loan_search)
		{
			$count = COUNT($loan_search);
			$a = 1;
			$id = '';
			$borrowerid = '';

			foreach($loan_search AS $key => $loan)
			{
				if($a == $count)
				{
					$id .= "'".$loan->talimar_loan."'";
					$borrowerid .= "'".$loan->borrower."'";
				}
				else{
					$id .= "'".$loan->talimar_loan."',";
					$borrowerid .= "'".$loan->borrower."',";	
				}

				$a++;
					
			}

			if($columnName)
			{
				$columnName = "ORDER BY ".$columnName." ".$columnSortOrder;
			}
			else
			{
				$columnName = '';
			}


			$where = 'loan_servicing.talimar_loan IN ('.$id.') AND loan_property.talimar_loan IN ('.$id.') AND borrower_data.id IN ('.$borrowerid.')  ';

			$loan = $this->User_model->query("SELECT `loan`.`fci`, `loan`.`id`, `loan`.`talimar_loan`, `loan`.`borrower`, `loan`.`totalPaymentFCI`, `loan_servicing`.`sub_servicing_agent`, `borrower_data`.`b_name`, `borrower_data`.`c_email`,`loan_property_insurance`.`insurance_expiration` FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `loan_property_insurance` ON `loan_property`.`property_home_id` = `loan_property_insurance`.`property_home_id` INNER JOIN `loan_servicing` ON `loan`.`talimar_loan` = `loan_servicing`.`talimar_loan` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id`  WHERE ".$where);

			$totalRecords = $loan->num_rows();

			if(!empty($searchValue)){
				$where = $where.' AND (borrower_data.b_name LiKE "%'.$searchValue.'%" OR loan_property.property_address LiKE "%'.$searchValue.'%")';
			}



			$loan = $this->User_model->query("SELECT `loan`.`fci`, `loan`.`id`, `loan`.`talimar_loan`, `loan`.`borrower`, `loan`.`totalPaymentFCI`, `loan_servicing`.`sub_servicing_agent`, `borrower_data`.`b_name`, `borrower_data`.`c_email`,`loan_property_insurance`.`insurance_expiration` FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `loan_property_insurance` ON `loan_property`.`property_home_id` = `loan_property_insurance`.`property_home_id` INNER JOIN `loan_servicing` ON `loan`.`talimar_loan` = `loan_servicing`.`talimar_loan` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id`  WHERE ".$where);

			$totalRecordwithFilter = $loan->num_rows();
			
			$loan_servicing_data = $this->User_model->query("SELECT `loan`.`fci`, `loan`.`id`,`loan`.`new_maturity_date`,`loan`.`talimar_loan`, `loan`.`borrower`, `loan`.`totalPaymentFCI`, `loan_servicing`.`sub_servicing_agent`, `borrower_data`.`b_name`, `borrower_data`.`c_email`,`loan_property_insurance`.`insurance_expiration`  FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `loan_property_insurance` ON `loan_property`.`property_home_id` = `loan_property_insurance`.`property_home_id` INNER JOIN `loan_servicing` ON `loan`.`talimar_loan` = `loan_servicing`.`talimar_loan` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id`  WHERE ".$where." ".$columnName." LIMIT ".$start.",".$rowperpage);
			$query="SELECT `loan`.`fci`, `loan`.`id`,`loan`.`new_maturity_date`,`loan`.`talimar_loan`, `loan`.`borrower`, `loan`.`totalPaymentFCI`, `loan_servicing`.`sub_servicing_agent`, `borrower_data`.`b_name`, `borrower_data`.`c_email`,`loan_property_insurance`.`insurance_expiration` FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `loan_property_insurance` ON `loan_property`.`property_home_id` = `loan_property_insurance`.`property_home_id` INNER JOIN `loan_servicing` ON `loan`.`talimar_loan` = `loan_servicing`.`talimar_loan` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id`  WHERE ".$where." ".$columnName." LIMIT ".$start.",".$rowperpage;

			$loan_servicing_data = $loan_servicing_data->result();

			if($loan_servicing_data)
			{
				foreach($loan_servicing_data as $key => $loan_details)
				{				
						$Loan_Servicer = '';
						$Loan_ServicerAddress = array();
						$talimar_loan_id = $loan_details->talimar_loan;
						$loan_amount = $loan_details->totalPaymentFCI;
						$loan_account_no = $loan_details->fci;
						$insuranceExpirationDate = $loan_details->insurance_expiration;
						$days45Before 				= date('Y-m-d',strtotime($insuranceExpirationDate.'-45 day'));
						$days30Before 				= date('Y-m-d',strtotime($insuranceExpirationDate.'-30 day'));
						$days20Before 				= date('Y-m-d',strtotime($insuranceExpirationDate.'-20 day'));
						$days15Before 				= date('Y-m-d',strtotime($insuranceExpirationDate.'-15 day'));
						$days10Before 				= date('Y-m-d',strtotime($insuranceExpirationDate.'-10 day'));
						$days5Before 				= date('Y-m-d',strtotime($insuranceExpirationDate.'-5 day'));
						$days1Before 				= date('Y-m-d',strtotime($insuranceExpirationDate.'-1 day'));
						$upcoming_mail_string="";
						if(!empty($days30Before) && $days30Before!="0000-00-00"){
							$days30Before=date('m/d/Y',strtotime($days30Before));
							$upcoming_mail_string.=$days30Before;
						}
						/*if(!empty($days30Before) && $days30Before!="0000-00-00"){
							$upcoming30days=date('m/d/Y',strtotime($days30Before));
							$upcoming_mail_string.="<b>30 Days-</b>".$upcoming30days."<br>";
						}
						if(!empty($days20Before) && $days20Before!="0000-00-00"){
							$upcoming20days=date('m/d/Y',strtotime($days20Before));
							$upcoming_mail_string.="<b>20 Days-</b>".$upcoming20days."<br>";
						}
						if(!empty($days15Before) && $days15Before!="0000-00-00"){
							$upcoming15days=date('m/d/Y',strtotime($days15Before));
							$upcoming_mail_string.="<b>15 Days-</b>".$upcoming15days."<br>";
						}
						if(!empty($days10Before) && $days10Before!="0000-00-00"){
							$upcoming10days=date('m/d/Y',strtotime($days10Before));
							$upcoming_mail_string.="<b>10 Days-</b>".$upcoming10days."<br>";
						}
						if(!empty($days5Before) && $days5Before!="0000-00-00"){
							$upcoming5days=date('m/d/Y',strtotime($days5Before));
							$upcoming_mail_string.="<b>05 Days-</b>".$upcoming5days."<br>";
						}
						if(!empty($days1Before) && $days1Before!="0000-00-00"){
							$upcoming1days=date('m/d/Y',strtotime($days1Before));
							$upcoming_mail_string.="<b>01 Days-</b>".$upcoming1days."<br>";
						}*/
						$currentDate=date('Y-m-d');
						if(strtotime($days30Before)>=strtotime($currentDate)){
							$insuranceExpirationDate=date('m/d/Y',strtotime($insuranceExpirationDate));	
							$loanPaymentReminder['BorrowerName'] = $loan_details->b_name;
							$loanPaymentReminder['Email'] = $loan_details->c_email;
							$loanPaymentReminder['loan_account_no'] = $loan_account_no;
							$loanPaymentReminder['TalimarLoanId'] = $talimar_loan_id;
							// $loanPaymentReminder['LoanAmount'] = '$'.number_format($loan_amount, 2);
							$loanPaymentReminder['insurance_date'] = $insuranceExpirationDate;
							$loanPaymentReminder['SentDate'] = $upcoming_mail_string;
							$loanPaymentReminder['type'] = "30 Days";
							$url = base_url("load_data/".$loan_details->id);
							$loanPaymentReminder['StreetAddress'] = '<a href="'.$url.'">'.$this->get_loadn_property_address($talimar_loan_id).'</a>';
							$loanPaymentReminder['loan_id'] = $loan_details->id;
							$data[] = $loanPaymentReminder;
							$b++;
						}
				}
			}
		}
		## Response
      	$response = array(
      		'query'=>$query,
          "draw" => intval($draw),
          "iTotalRecords" => $totalRecords,
          "iTotalDisplayRecords" => $totalRecordwithFilter,
          "aaData" => $data
      	);


      echo json_encode($response);
	}
	public function insurance_expiring_track(){
		$data = array();
		$data['reminder_track'] = array();
		$data['content'] = $this->load->view('mail_report/insurance_expiring_track', $data, true);
		$this->load->view('template_files/sidebar_automail', $data);
	}
	public function ajaxInsuranceExpiringTrack()
	{
		$where = '';
		$postData = $this->input->post();
		$fields = array();
		$result = array();
		$totalRecords=0;
		$totalRecordwithFilter=0;

		## Read value
		$draw = $postData['draw'];
		$start = $postData['start'];
		$rowperpage = $postData['length']; // Rows display per page
		$columnIndex = $postData['order'][0]['column']; // Column index
		$columnName1 = $postData['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $postData['order'][0]['dir']; // asc or desc
		$searchValue = $postData['search']['value']; // Search value

		$columnName = '';
		if($columnName1 == 'BorrowerName')
		{
			$columnName = 'borrower_data.b_name';
		}
		if($columnName1 == 'Email')
		{
			$columnName = 'tracking.to_email';
		}
		if($columnName1 == 'sandgrid_status')
		{
			$columnName = 'tracking.sandgrid_status';
		}
		if($columnName1 == 'created_at')
		{
			$columnName = 'tracking.created_at';
		}
		if($columnName1 == 'StreetAddress')
		{
			$columnName = 'loan_property.property_address';
		}
		$loanSearchQuery = $this->User_model->query("SELECT tracking.*, loan.talimar_loan FROM loan INNER JOIN sandgrid_mail_tracking AS tracking  ON loan.id = tracking.loan_id WHERE tracking.tracking_type='insuranceNotification' AND tracking.mail_tracking='yes' ");
		$loan_search1 = $loanSearchQuery->result();		
		if($loan_search1){
			$count = COUNT($loan_search1);
			$a = 1;
			$id = '';
			$lid = '';
			$borrowerid = '';
			foreach ($loan_search1 as $key => $loan) {
				if($a == $count)
				{
					$id .= "'".$loan->talimar_loan."'";
					$borrowerid .= "'".$loan->borrower_id."'";
					$lid .= "'".$loan->loan_id."'";
				}
				else{
					$id .= "'".$loan->talimar_loan."',";
					$borrowerid .= "'".$loan->borrower_id."',";
					$lid .= "'".$loan->loan_id."',";	
				}
				$a++;
			}

			$condition = array();
			$where = 'tracking.tracking_type="insuranceNotification" AND tracking.loan_id IN ('.$lid.') AND loan.talimar_loan IN ('.$id.') AND loan_property.talimar_loan IN ('.$id.') AND borrower_data.id IN ('.$borrowerid.')';
			$totalRecords = $this->User_model->query("SELECT `tracking`.*, `loan`.`talimar_loan`, `borrower_data`.`b_name`FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `sandgrid_mail_tracking` AS `tracking` ON `tracking`.`loan_id` = `loan`.`id` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id` WHERE ".$where." GROUP BY loan_property.talimar_loan ");

			$totalRecords = $totalRecords->num_rows();

			if(!empty($searchValue)){
				$where = $where.' AND (borrower_data.b_name LiKE "%'.$searchValue.'%" OR loan_property.property_address LiKE "%'.$searchValue.'%")';
			}

			$totalRecordwithFilter = $this->User_model->query("SELECT `tracking`.*, `loan`.`talimar_loan`, `borrower_data`.`b_name`FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `sandgrid_mail_tracking` AS `tracking` ON `tracking`.`loan_id` = `loan`.`id` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id` WHERE ".$where." GROUP BY loan_property.talimar_loan ");

			$totalRecordwithFilter = $totalRecordwithFilter->num_rows();

			$loan_search = $this->User_model->query("SELECT `tracking`.*, `loan`.`talimar_loan`, `borrower_data`.`b_name`FROM `loan` INNER JOIN `loan_property` ON `loan`.`talimar_loan` = `loan_property`.`talimar_loan` INNER JOIN `sandgrid_mail_tracking` AS `tracking` ON `tracking`.`loan_id` = `loan`.`id` INNER JOIN `borrower_data` ON `loan`.`borrower` = `borrower_data`.`id` WHERE ".$where." GROUP BY loan_property.talimar_loan  ORDER BY ".$columnName." ".$columnSortOrder." LIMIT ".$start.",".$rowperpage);
			$loan_search = $loan_search->result();
			foreach ($loan_search as $key => $reminder) {
				$reminder_id     = $reminder->id;
				$loan_id 		 = $reminder->loan_id;
				$talimar_loan 	 = $reminder->talimar_loan;
				$to_email 		 = $reminder->to_email;
				$sandgrid_status = $reminder->sandgrid_status;
				$created_at 	 = $reminder->created_at;
				$created_at=date('m/d/Y H:i A',strtotime($created_at));	
				$next_update_at  = $reminder->next_update_at;
				$borrower_id 	 = $reminder->borrower_id;
				$fields['BorrowerName'] = $reminder->b_name;
				$fields['Email'] = $reminder->to_email;
				$fields['TalimarLoanId'] = $reminder->talimar_loan;
				$url = base_url("load_data/".$reminder->loan_id);
				$fields['StreetAddress'] = '<a href="'.$url.'">'.$this->get_loadn_property_address($talimar_loan).'</a>';
				$fields['sandgrid_status'] = $sandgrid_status;
				$fields['created_at'] = $created_at;
				$fields['action'] = '<button style="margin-right:5px;margin-bottom:5px;" type="button" class="btn btn-primary open_email_content" 
										to_email="'.$reminder->to_email.'" 
										to_cc="'.$reminder->to_cc.'" 
										to_bcc="'.$reminder->to_bcc.'" 
										from_email="'.$reminder->from_email.'" 
										from_name="'.$reminder->from_name.'" 
										subject="'.$reminder->subject.'"  
										message="'.$reminder->message.'"  
										attach_file="'.$reminder->attach_file.'"  
										><i class="fa fa-eye" aria-hidden="true"></i></button>
										<button type="button" class="btn btn-primary delete_email_content" reminder_id="'.$reminder_id.'" onclick="delete_email_content(this)">
											<i class="fa fa-trash"></i>
										</button>';
				$result[] = $fields;
			}
		}
		## Response
      	$response = array(
          "draw" => intval($draw),
          "iTotalRecords" => $totalRecords,
          "iTotalDisplayRecords" => $totalRecordwithFilter,
          "aaData" => $result
      	);
      echo json_encode($response);
	}
	public function past_due_remender(){
		$pastDueReminder = array();		
		$data = array();
		$data['past_due_remender'] = $pastDueReminder;
		$data['content'] = $this->load->view('mail_report/past_due_remender', $data, true);
		$this->load->view('template_files/sidebar_automail', $data);
	}
	public function ajaxPastDueList(){
		$request 					= '';
		$search_text 				= '';
		$calling_type				= '';
		$startfrom 					= 0;
		$queryLength 				= 0;
		$orderby 					= 'id';
		$order 						= 'ASC';
		$searchValue 				= '';
		$columns  = array(
						0  => 'bd.b_name',
						1  => 'bd.c_email',	
						2  => 'lp.property_address',
						3  => 'l.talimar_loan',
						4  => 'l.totalPaymentFCI',
						5  => 'lph.nextDueDate',
						6  => 'lph.daysLate'
					);
		if(!empty($_GET)){
			$request 				= $_GET;
			$calling_type       	= $request['calling_type'];
			$startfrom 				= $request['start'];
			$queryLength 			= $request['length'];
			if(isset($request['order'])){
				if($request['order'][0]['column']){
					$orderby = $columns[$request['order'][0]['column']];
				}
				if($request['order'][0]['dir']){
					$order = $request['order'][0]['dir'];
				}
			}
			if(isset($request['search']) ){
				if($request['search']['value']){
					$search = trim( $request['search']['value'] );
					$searchValue = " AND (bd.b_name LIKE '%$search%' OR bd.c_email LIKE '%$search%' OR lp.property_address LIKE '%$search%' ) ";
				}
			}
			if(!empty($search_text)){
				if($searchValue == ''){
					$searchValue = " AND (bd.b_name LIKE '%$search%' OR bd.c_email LIKE '%$search%' OR lp.property_address LIKE '%$search%' ) ";			
				}
			}
		}
		$rowRequests = array();
		$totalNumRows= 0;
		$where=" ls.loan_status='2' AND l.paymentReminder='on' AND lph.daysLate>0 ";
		$sql_assign = "		SELECT SQL_CALC_FOUND_ROWS
								l.id,l.talimar_loan,l.borrower,l.totalPaymentFCI,
								bd.b_name,bd.c_email,
								lph.daysLate,lph.nextDueDate,
								lp.property_address,lp.state,lp.unit,lp.city,lp.zip 
                           	FROM loan l JOIN loan_servicing ls ON l.talimar_loan=ls.talimar_loan
                           	JOIN borrower_data bd ON l.borrower = bd.id
                           	JOIN loan_property lp ON l.talimar_loan = lp.talimar_loan
                           	JOIN lender_payment_history lph ON l.talimar_loan = lph.talimar_loan
                           	WHERE ".$where." ".$searchValue. "
                           	GROUP BY lp.talimar_loan ORDER BY $orderby $order   LIMIT $startfrom, $queryLength ";
        $fetchDueLateData = $this->User_model->query($sql_assign);
		$fetchDueLateData = $fetchDueLateData->result();
		$totalQuery= $this->User_model->query("SELECT FOUND_ROWS()");
		$totalQueryResult= $totalQuery->result();
		$totalNumRows=0;
		foreach ($totalQueryResult[0] as $key =>$value) {
		 	$totalNumRows=$value;
		}
		foreach ($fetchDueLateData as $dueLateData) {
			$sendStatus		= 0;
			if($dueLateData->daysLate=="3"){
				$sendDate 	= date('Y-m-d',strtotime($dueLateData->nextDueDate.'+3 day'));
				$sendStatus	= 1;
			}
			if($dueLateData->daysLate=="6"){
				$sendDate 	= date('Y-m-d',strtotime($dueLateData->nextDueDate.'+6 day'));
				$sendStatus	= 1;
			}
			if($dueLateData->daysLate=="9"){
				$sendDate 	= date('Y-m-d',strtotime($dueLateData->nextDueDate.'+9 day'));
				$sendStatus	= 1;
			}
			if($sendStatus=="1"){
				$sendDate 		= date('m/d/Y',strtotime($sendDate));
				$nextDueDate	= date('m/d/Y',strtotime($dueLateData->nextDueDate));
				$url 			= base_url("load_data/".$dueLateData->id);
				$rowRequests[]	= array(
									"borrower_name"		=> $dueLateData->b_name,
									"email"				=> $dueLateData->c_email,
									"street_address"	=> '<a href="'.$url.'">'.$dueLateData->property_address.'</a>',
									"talimar_loan"		=> $dueLateData->talimar_loan,									
									"payment_amount"	=> $dueLateData->totalPaymentFCI,
									"due_payment_date"	=> $nextDueDate,
									"send_date"			=> $sendDate
								);
			}
		}
		$response = array(
          "data" => $rowRequests,
          "totalNumRows" => $totalNumRows
      	);
      	echo json_encode($response);
	}
	public function past_due_mails_track(){
		$pastDueMailTrack 		= array();		
		$data 					= array();
		$data['past_due_track'] = $pastDueMailTrack;
		$data['content'] 		= $this->load->view('mail_report/past_due_tracking', $data, true);
		$this->load->view('template_files/sidebar_automail', $data);
	}
	public function ajaxPastDueTracking(){
		$request 					= '';
		$search_text 				= '';
		$calling_type				= '';
		$startfrom 					= 0;
		$queryLength 				= 0;
		$orderby 					= 'id';
		$order 						= 'ASC';
		$searchValue 				= '';
		$columns  = array(
						0  => 'bd.b_name',
						1  => 'bd.c_email',	
						2  => 'lp.property_address',
						3  => 'l.talimar_loan',
						4  => 'smt.sandgrid_status',
						5  => 'smt.created_at',
						6  => 'smt.id'
					);
		if(!empty($_GET)){
			$request 				= $_GET;
			$calling_type       	= $request['calling_type'];
			$startfrom 				= $request['start'];
			$queryLength 			= $request['length'];
			if(isset($request['order'])){
				if($request['order'][0]['column']){
					$orderby = $columns[$request['order'][0]['column']];
				}
				if($request['order'][0]['dir']){
					$order = $request['order'][0]['dir'];
				}
			}
			if(isset($request['search']) ){
				if($request['search']['value']){
					$search = trim( $request['search']['value'] );
					$searchValue = " AND (bd.b_name LIKE '%$search%' OR bd.c_email LIKE '%$search%' OR lp.property_address LIKE '%$search%' ) ";
				}
			}
			if(!empty($search_text)){
				if($searchValue == ''){
					$searchValue = " AND (bd.b_name LIKE '%$search%' OR bd.c_email LIKE '%$search%' OR lp.property_address LIKE '%$search%' ) ";			
				}
			}
		}
		$rowRequests = array();
		$totalNumRows= 0;
		$where=" smt.tracking_type='beforePaymentReminder' AND smt.mail_tracking='yes' ";
		$sql_assign = "		SELECT SQL_CALC_FOUND_ROWS
								smt.*,
								l.id as loan_id,l.talimar_loan,l.borrower,
								bd.b_name,bd.c_email,
								lp.property_address,lp.state,lp.unit,lp.city,lp.zip 
                           	FROM loan l JOIN borrower_data bd ON l.borrower = bd.id
                           	JOIN loan_property lp ON l.talimar_loan = lp.talimar_loan
                           	JOIN sandgrid_mail_tracking smt ON l.id = smt.loan_id
                           	WHERE ".$where." ".$searchValue. "
                           	GROUP BY lp.talimar_loan ORDER BY $orderby $order   LIMIT $startfrom, $queryLength ";
        $fetchDueLateData = $this->User_model->query($sql_assign);
		$fetchDueLateData = $fetchDueLateData->result();
		$totalQuery= $this->User_model->query("SELECT FOUND_ROWS()");
		$totalQueryResult= $totalQuery->result();
		$totalNumRows=0;
		foreach ($totalQueryResult[0] as $key =>$value) {
		 	$totalNumRows=$value;
		}
		foreach ($fetchDueLateData as $dueLateData) {
			$created_at		= 	date('m/d/Y H:i A',strtotime($dueLateData->created_at));	
			$action         = 	'<button style="margin-right:5px;margin-bottom:5px;" type="button" class="btn btn-primary open_email_content" 
									to_email="'.$dueLateData->to_email.'" 
									to_cc="'.$dueLateData->to_cc.'" 
									to_bcc="'.$dueLateData->to_bcc.'" 
									from_email="'.$dueLateData->from_email.'" 
									from_name="'.$dueLateData->from_name.'" 
									subject="'.$dueLateData->subject.'"  
									message="'.$dueLateData->message.'"  
									attach_file="'.$dueLateData->attach_file.'"  
									><i class="fa fa-eye" aria-hidden="true"></i></button>
									<button type="button" class="btn btn-primary delete_email_content" reminder_id="'.$dueLateData->id.'" onclick="delete_email_content(this)">
										<i class="fa fa-trash"></i>
								</button>';
				$url 			= base_url("load_data/".$dueLateData->loan_id);
				$rowRequests[]	= array(
									"borrower_name"		=> $dueLateData->b_name,
									"email"				=> $dueLateData->c_email,
									"street_address"	=> '<a href="'.$url.'">'.$dueLateData->property_address.'</a>',
									"talimar_loan"		=> $dueLateData->talimar_loan,									
									"status"			=> $dueLateData->sandgrid_status,
									"date_sent"			=> $created_at,
									"action"			=> $action
								);
			
		}		
		$response = array(
          "data" => $rowRequests,
          "totalNumRows" => $totalNumRows
      	);
      	echo json_encode($response);
	}
	public function trackingNotification(){
		$trackingNotification 			= array();		
		$data 							= array();
		$data['trackingNotification'] 	= $trackingNotification;
		$data['content'] 				= $this->load->view('mail_report/tracking_notification_list', $data, true);
		$this->load->view('template_files/sidebar_without_menu', $data);
	}
	public function ajaxTrackingNotification(){
		$request 					= '';
		$search_text 				= '';
		$calling_type				= '';
		$startfrom 					= 0;
		$queryLength 				= 0;
		$orderby 					= 'id';
		$order 						= 'ASC';
		$searchValue 				= '';
		$columns  = array(
						0  => 'bd.b_name',
						1  => 'bd.b_name',	
						2  => 'bd.c_email',
						3  => 'smt.tracking_type',
						4  => 'lp.property_address',
						5  => 'smt.created_at',
						6  => 'smt.created_at',
						7  => 'smt.sandgrid_status',
						8 => 'action'
					);
		if(!empty($_GET)){
			$request 				= $_GET;
			$calling_type       	= $request['calling_type'];
			$startfrom 				= $request['start'];
			$queryLength 			= $request['length'];

			$upcoming_payment 		= $request['upcoming_payment'];
			$late_payment 			= $request['late_payment'];
			$property_insurance 	= $request['property_insurance'];
			$last_days 				= $request['last_days'];
			if(isset($request['order'])){
				if($request['order'][0]['column']){
					$orderby = $columns[$request['order'][0]['column']];
				}
				if($request['order'][0]['dir']){
					$order = $request['order'][0]['dir'];
				}
			}
			if(isset($request['search']) ){
				if($request['search']['value']){
					$search = trim( $request['search']['value'] );
					$searchValue = " AND (bd.b_name LIKE '%$search%' OR bd.c_email LIKE '%$search%' OR lp.property_address LIKE '%$search%' ) ";
				}
			}
			if(!empty($search_text)){
				if($searchValue == ''){
					$searchValue = " AND (bd.b_name LIKE '%$search%' OR bd.c_email LIKE '%$search%' OR lp.property_address LIKE '%$search%' ) ";			
				}
			}
		}
		$rowRequests = array();
		$totalNumRows= 0;
		$where=" 1=1 AND smt.mail_tracking='test' AND smt.tracking_type!='constructionUpdate' AND smt.tracking_type!='insuranceNotification' ";
		$currentDate= date('Y-m-d');
		if(!empty($last_days) && $last_days!="all"){
			$beforeDate=date('Y-m-d',strtotime($currentDate.'-'.$last_days.' day'));
			$beforeMonth= date('m',strtotime($beforeDate));
			$beforeYear= date('Y',strtotime($beforeDate));
			$startDate=$beforeYear."-".$beforeMonth."-01";
			$endDate  =$beforeYear."-".$beforeMonth."-30";
			$where.=" AND smt.created_at between '$startDate' AND '$endDate' ";
		}
		$whereStatus=0;
		if(!empty($upcoming_payment) && $upcoming_payment!="no"){
			$where.= " AND smt.tracking_type='paymentReminder' ";
			$whereStatus=1;
		}
		if(!empty($late_payment) && $late_payment!="no"){
			$where.= " AND smt.tracking_type='beforePaymentReminder' ";
			$whereStatus=1;
		}
		if(!empty($property_insurance) && $property_insurance!="no"){
			$where.= " AND smt.tracking_type='insuranceExpiring' ";
			$whereStatus=1;
		}
		if($whereStatus==0){
			$where.= " AND smt.tracking_type!='insuranceExpiring' AND smt.tracking_type!='beforePaymentReminder' AND smt.tracking_type!='paymentReminder' ";
		}
		

		$sql        ="SELECT smt.*,l.id as loan_id,l.talimar_loan,l.borrower,bd.b_name,bd.c_email,lp.property_address,lp.state,lp.unit,lp.city,lp.zip FROM loan l JOIN borrower_data bd ON l.borrower = bd.id JOIN loan_property lp ON l.talimar_loan = lp.talimar_loan JOIN sandgrid_mail_tracking smt ON l.id = smt.loan_id WHERE ".$where." ".$searchValue. " GROUP BY lp.talimar_loan ORDER BY $orderby $order   LIMIT $startfrom, $queryLength ";
		$sql_assign = "		SELECT SQL_CALC_FOUND_ROWS
								smt.*,
								l.id as loan_id,l.talimar_loan,l.borrower,
								bd.b_name,bd.c_email,
								lp.property_address,lp.state,lp.unit,lp.city,lp.zip 
                           	FROM loan l JOIN borrower_data bd ON l.borrower = bd.id
                           	JOIN loan_property lp ON l.talimar_loan = lp.talimar_loan
                           	JOIN sandgrid_mail_tracking smt ON l.id = smt.loan_id
                           	WHERE ".$where." ".$searchValue. "
                           	GROUP BY lp.talimar_loan ORDER BY $orderby $order   LIMIT $startfrom, $queryLength ";
        $fetchDueLateData = $this->User_model->query($sql_assign);
		$fetchDueLateData = $fetchDueLateData->result();
		$totalQuery= $this->User_model->query("SELECT FOUND_ROWS()");
		$totalQueryResult= $totalQuery->result();
		$totalNumRows=0;
		foreach ($totalQueryResult[0] as $key =>$value) {
		 	$totalNumRows=$value;
		}
		foreach ($fetchDueLateData as $dueLateData) {
			$BorrowerFirstName="";
			$BorrowerLastName="";
			$BorrowerFirstEmail="";
			$BorrowerContactSql 	= $this->User_model->query("SELECT c.contact_firstname,c.contact_email,c.contact_lastname FROM borrower_contact as bc JOIN contact as c ON bc.contact_id = c.contact_id WHERE bc.borrower_id = '".$dueLateData->borrower."' AND bc.contact_primary = '1'");					
			$BorrowerContactResult 	= $BorrowerContactSql->result();
			if(!empty($BorrowerContactResult)){
				$BorrowerFirstName	=$BorrowerContactResult[0]->contact_firstname;
				$BorrowerLastName	=$BorrowerContactResult[0]->contact_lastname;
				$BorrowerFirstEmail	=$BorrowerContactResult[0]->contact_email;
			}
			$created_at		= 	date('m/d/Y',strtotime($dueLateData->created_at));
			$created_time		= 	date('H:i A',strtotime($dueLateData->created_at));	
			$action         = 	'<button style="margin-right:5px;margin-bottom:5px;" type="button" class="btn btn-primary open_email_content" 
									to_email="'.$dueLateData->to_email.'" 
									to_cc="'.$dueLateData->to_cc.'" 
									to_bcc="'.$dueLateData->to_bcc.'" 
									from_email="'.$dueLateData->from_email.'" 
									from_name="'.$dueLateData->from_name.'" 
									subject="'.$dueLateData->subject.'"  
									message="'.$dueLateData->message.'"  
									attach_file="'.$dueLateData->attach_file.'"  
									><i class="fa fa-eye" aria-hidden="true"></i></button>
									';
				//echo '<button type="button" class="btn btn-primary delete_email_content" reminder_id="'.$dueLateData->id.'" onclick="delete_email_content(this)"><i class="fa fa-trash"></i></button>';
				$url 			= base_url("load_data/".$dueLateData->loan_id);
				$notification 	="";
				if($dueLateData->tracking_type=="paymentReminder"){
					$notification= "payment remainder";
				}
				if($dueLateData->tracking_type=="beforePaymentReminder"){
					$notification= "late payment";
				}
				if($dueLateData->tracking_type=="insuranceExpiring"){
					$notification= "property insurance";
				}
				if($dueLateData->tracking_type=="constructionUpdate"){
					$notification= "constraction";
				}
				$rowRequests[]	= array(
									"firstname"			=> $BorrowerFirstName,
									"lastname"			=> $BorrowerLastName,
									"email"				=> $BorrowerFirstEmail,
									"notification"		=> $notification,
									"address"			=> '<a href="'.$url.'">'.$dueLateData->property_address.'</a>',
									"sent_date"			=> $created_at,	
									"sent_time"			=> $created_time,								
									"status"			=> $dueLateData->sandgrid_status,
									"action"			=> $action
								);
			
		}		
		$response = array(
			"sql" =>$sql,
          "data" => $rowRequests,
          "totalNumRows" => $totalNumRows
      	);
      	echo json_encode($response);
	}
}