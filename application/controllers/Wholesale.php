<?php
class Wholesale extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('Aws3','aws3');
		$this->load->helper('aws3_helper.php');
		$this->load->helper('mailhtml_helper.php');
		$this->load->database();
		$this->load->helper(array('form', 'url'));
		$this->load->model('User_model');
		$this->load->library('MYPDF');
		$this->load->library('email');
		$this->load->helper('common_functions.php');
	}
	
	function index()
	{
		$this->load->view('dashboard_view');
	}
	
	public function add_wholesale_deal(){
		$task_id = $this->uri->segment(2);
		$data['val'] = $task_id;
		$fetch_stret_add = $this->street_address();
		$data['the_street_add'] = $fetch_stret_add['all_street_address'];
		$data['user_id'] = $this->session->userdata('t_user_id');
		$data['content'] = $this->load->view('wholesale_deal/wholesale_deal',$data,true);
		$this->load->view('template_files/template',$data);
	}

	public function street_address(){
		/*fetch all street address*/
		$fetch_property_search = $this->User_model->query("select lp.talimar_loan, ph.property_address from loan_property as lp JOIN property_home as ph ON lp.property_home_id = ph.id WHERE ph.primary_property = '1' order by lp.id desc");


				if ($fetch_property_search->num_rows() > 0) {

					$fetch_property_search = $fetch_property_search->result();

					foreach ($fetch_property_search as $row) {
						//$p_ta_loan['talimar_loan'] 	= $row->talimar_loan;

						$property_search = $this->User_model->query("SELECT id from loan WHERE talimar_loan='" . $row->talimar_loan . "' ");
						$property_search = $property_search->result();

						$loanStatus = $this->User_model->query("SELECT loan_status FROM loan_servicing WHERE talimar_loan = '".$row->talimar_loan."' ");

						$loanStatuss = $loanStatus->row();
						// echo '<pre>';print_R($loanStatuss->loan_status);
						if($loanStatuss){
											

							if ($loanStatuss->loan_status == 1) {
								$loan_status = 'P';
							} elseif ($loanStatuss->loan_status == 2) {
								$loan_status = 'A';
							} elseif ($loanStatuss->loan_status == 3) {
								$loan_status = 'P';
							} elseif ($loanStatuss->loan_status == 4) {
								$loan_status = 'C';
							} elseif ($loanStatuss->loan_status == 5) {
								$loan_status = 'B';
							} elseif ($loanStatuss->loan_status == 6) {
								$loan_status = 'T';
							} elseif ($loanStatuss->loan_status == 7) {
								$loan_status = 'R';
							} else {
								$loan_status = 'N/A';
							}
						}	

						$fetch_search_option[] = array(
							'id' => $property_search[0]->id,
							'text' => $row->property_address,
							'loan_status' => $loan_status
						);
					}
					$data['all_street_address'] = $fetch_search_option;
					// $data['loanStatus']= $stats_lon;	
					// print_r($fetch_search_option);				
				}
				return 	$data;
	}
}		