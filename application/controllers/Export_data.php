<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Export_data extends MY_Controller
{

	public function __construct()
	{

		parent::__construct();

		$this->load->model('User_model');
		$this->load->library('Aws3','aws3');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');
	}

	public function index()
	{ 

		$this->load->view('export_data/index');
	

	}
}





