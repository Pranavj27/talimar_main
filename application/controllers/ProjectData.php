<?php
class ProjectData extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('email');
		$this->load->model('User_model');
		$this->load->library('Aws3','aws3');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');
		
		//$this->load->library('TCPDF');
		// $this->load->library('session');
		//$this->output->enable_profiler(TRUE);
		if ($this->session->userdata('t_user_id') != '') {
		} else {
			redirect(base_url() . "home", 'refresh');
		}
		
		//error_reporting(0);
 		//ini_set('memory_limit','640M');
 		ini_set('memory_limit', '-1');

	}
	
	public function add_editdata(){
		
		if(isset($_POST['submit'])){
			
			$project_name	= $this->input->post('project_name');
			$project_status	= $this->input->post('project_status');
			$start_date 	= !empty($this->input->post('start_date'))?$this->input->post('start_date'):'';
			$complete_date 	= !empty($this->input->post('completion_date'))?$this->input->post('completion_date'):'';
			$contact_ids 	= $this->input->post('contact_name');

			$contact_idss 	= !empty($contact_ids)?implode(",",$contact_ids):'';
			$contact_idss = input_sanitize($contact_idss);
			$role 			= $this->input->post('role');
			$roleids		= !empty($role)?implode(",", $role):'';
			$description 	= input_sanitize($this->input->post('description'));
			$user_id 		= $this->input->post('userid');
			
			
			$start_date1 = input_date_format($start_date);
			
			$complete_date1 = input_date_format($complete_date);
			
			$rowid = $this->input->post('rowid');
			$folder = 'pro_attachment/'; 
			if(!is_dir($folder))
			{
				mkdir($folder, 0777, true);
			}		
			if($rowid !=''){
				
				$update = $this->User_model->query("UPDATE `projects` SET `project_name`='".$project_name."', `project_status`='".$project_status."', `start_date`='".$start_date1."', `complete_date`='".$complete_date1."', `contact_ids`='".$contact_idss."', `role`='".$roleids."', `description`='".$description."', `user_id`='".$user_id."' WHERE id = '".$rowid."'");
				//Multiple Image uploads
				if(!empty($_FILES['project_attachment'])){
					print_r($_FILES['project_attachment']);
					for ($i=0; $i <count($_FILES['project_attachment']['name']) ; $i++) { 
						$ext = strtolower(pathinfo($_FILES['project_attachment']['name'][$i], PATHINFO_EXTENSION));
						$new_name = basename($_FILES['project_attachment']['name'][$i]);
						$my_file_name 	= str_replace('.'.$ext, '_'.time().'.'.$ext, $new_name);
						$target_dir = $folder.$my_file_name;
						if(move_uploaded_file($_FILES["project_attachment"]["tmp_name"][$i], $target_dir) === TRUE) {
							$imageArray=array(
										"project_id"	=>$rowid,
										"image_name"	=>$my_file_name,
										"impage_path"	=>$target_dir
									);
							$this->User_model->insertdata('project_attachments', $imageArray);
						}
					}
				}
				$this->session->set_flashdata('success', 'Project details updated successfully!');
				redirect(base_url(). 'projects');
				
			 }else{
				
				$sql = "INSERT INTO `projects`(`project_name`, `project_status`, `start_date`, `complete_date`, `contact_ids`, `role`, `description`, `user_id`) VALUES ('".$project_name."', '".$project_status."', '".$start_date1."', '".$complete_date1."', '".$contact_idss."', '".$roleids."', '".$description."', '".$user_id."')";
				
				$this->User_model->query($sql);
				$rowid=$this->db->insert_id();
				//Multiple Image uploads
				if(!empty($_FILES['project_attachment'])){
					print_r($_FILES['project_attachment']);
					for ($i=0; $i <count($_FILES['project_attachment']['name']) ; $i++) { 
						$ext = strtolower(pathinfo($_FILES['project_attachment']['name'][$i], PATHINFO_EXTENSION));
						$new_name = basename($_FILES['project_attachment']['name'][$i]);
						$my_file_name 	= str_replace('.'.$ext, '_'.time().'.'.$ext, $new_name);
						$target_dir = $folder.$my_file_name;
						if(move_uploaded_file($_FILES["project_attachment"]["tmp_name"][$i], $target_dir) === TRUE) {
							$imageArray=array(
										"project_id"	=>$rowid,
										"image_name"	=>$my_file_name,
										"impage_path"	=>$target_dir
									);
							$this->User_model->insertdata('project_attachments', $imageArray);
						}
					}
				}
				$this->session->set_flashdata('success', 'Project details added successfully!');
				
				//send project email...
				$this->Send_project_emails($contact_idss, $user_id, $project_name, $project_status, $start_date1, $complete_date1, $description);
				
				redirect(base_url(). 'projects');
			}
		}
	}
	
	public function getuserNames(){
		
		$fetchuser = $this->User_model->select_star('user');
		$fetchuser = $fetchuser->result();
		foreach($fetchuser as $row){
			
			$allNames[$row->id] = $row->fname.' '.$row->lname;
		}
		
		return $allNames;
	}
	
	public function getuserinfo(){
		
		$id = $this->input->post('id');
		$fetchuser = $this->User_model->select_where('user', array('id'=>$id));
		$fetchuser = $fetchuser->result();
		$allNames = $fetchuser[0]->fname.' '.$fetchuser[0]->lname;
		echo $allNames;
	}
	
	public function getuserinfo1(){
		
		$id = $this->input->post('id');
		$explode = explode(',',$id);
		
		foreach($explode as $row){
			$fetchuser = $this->User_model->query("SELECT * from user where id = '".$row."'");
			$fetchuser = $fetchuser->result();
			$allNames = $fetchuser[0]->fname.' '.$fetchuser[0]->lname.', ';
			echo $allNames;
		}
		
	}

	public function getroledata(){
		
		$id = $this->input->post('id');
		$explode = explode(',',$id);
		$project_role_option = $this->config->item('project_role_option');
		
		foreach($explode as $row){
			
			$allNames = $project_role_option[$row].', ';
			echo $allNames;
		}
		
	}
	
	public function Send_project_emails($all_partcipant, $userid, $projectname, $projectstatus, $start_date1, $complete_date, $description){
		
		$allIds = !empty($all_partcipant)?$all_partcipant.','.$userid:$userid;
		$allexplode = explode(',',$allIds);
		$array_unique = array_unique($allexplode);
		
		$project_status = $this->config->item('project_status');
		$getuserNames = $this->getuserNames();
		
		$forAllParticipant = explode(',',$all_partcipant);
		$allpartname = array();

		if(!empty($forAllParticipant))
		{
			foreach($forAllParticipant as $part){
				
				 $allpartname[] = !empty($getuserNames[$part])?$getuserNames[$part]:'';
			}
		}
		
		$allnameimpport = !empty($allpartname)?implode(', ', $allpartname):'';
		
		foreach($array_unique as $id){
			
			$select_email = $this->User_model->query("SELECT `fname`, `lname`, `email_address` FROM `user` WHERE `id`= '".$id."' AND email_address != '' ");
			$select_email = $select_email->row();
			
			if($select_email){
					$useremail = $select_email->email_address;
					$projectURl = base_url().'projects';
					
					$subject = "New Project";
					$header = "From: mail@database.talimarfinancial.com";
					$message = '';
					$message .= '<p><b>Name of Project:</b> '.$projectname.'</p>';
					$message .= '<p><b>Project Status:</b> '.$project_status[$projectstatus].'</p>';
					$message .= '<p><b>Start Date:</b> '.date('m-d-Y', strtotime($start_date1)).'</p>';
					$message .= '<p><b>Completion Date:</b> '.date('m-d-Y', strtotime($complete_date)).'</p>';
					$message .= '<p><b>Created By:</b> '.$getuserNames[$userid].'</p>';
					$message .= '<p><b>Participants:</b> '.$allnameimpport.'</p>';
					$message .= '<p><b>Project Description:</b> '.$description.'</p>';
					$message .= '<p><a target="_blank" href="' . $projectURl . '">Click Here</a> to be directed to the project information.</p>';

					

					$dataMailA = array();
					$dataMailA['from_email'] 	= 'mail@talimarfinancial.com';
					$dataMailA['from_name'] 	= 'TaliMar Financial';
					$dataMailA['to'] 			= $useremail;
					$dataMailA['name'] 			= '';
					$dataMailA['subject'] 		= $subject;
					$dataMailA['content_body'] 	= $message;
					$dataMailA['button_url'] 	= '';
					$dataMailA['button_text'] 	= '';
					send_mail_template($dataMailA);
			}	
		}
		
		return;
	}
	
	public function projects(){
		//ini_set('memory_limit','640M');
		$fetchcontact = $this->User_model->query("SELECT `id`, `fname`, `lname` FROM `user` WHERE `account`= '1'");
		$fetchcontact = $fetchcontact->result();
		$data['fetchcontact'] = $fetchcontact;

		$fetch_all_contact 	= $this->User_model->select_star('contact','id');
		$fetch_all_contact 	= $fetch_all_contact->result();
		$data['fetch_all_contact'] 	= $fetch_all_contact;
		
		$project_status_opt = $this->input->post('project_status_opt'); 
		$participants = $this->input->post('participants'); 
		
		if($project_status_opt != ''){
			
			$condition = "project_status = '".$project_status_opt."'";
		}else{
			$condition = "project_status IN('1','2','3')";
		}

		if($participants != ''){
			$condition1 = "AND contact_ids LIKE '%".$participants."%'";
		}else{
			$condition1 = "";
		}

		$fetchproject = $this->User_model->query("Select * from projects Where ".$condition." ".$condition1." ORDER BY project_status desc");
		
		$data['project_status_opt'] = $project_status_opt;
		$data['participants'] = $participants;

		if($fetchproject->num_rows() > 0){
			
			$fetchproject = $fetchproject->result();
			$data['fetchproject'] = $fetchproject;
		}else{
			$data['fetchproject'] = '';
		}
		
		$getuserNames = $this->getuserNames();
		$data['getuserNames'] = $getuserNames;
		
		$data['content'] = $this->load->view('projects/projectlist', $data, true);
		$this->load->view('template_files/template',$data);
		
	}
	
	public function editrow(){
		
		$id = $this->input->post('id');
		$where['id'] = $id;
		$getrow = $this->User_model->select_where('projects',$where);
		$getrow = $getrow->result();
		$attach['project_id'] = $id;
		$getattachment_que = $this->User_model->select_where('project_attachments',$attach);
		$getattachment_row	=$getattachment_que->result();
		$idName='update_attachment_list';
		$attachmentStr='<div class="col-md-12 " id="attachment_list_row_0">
								<div class="col-md-9">
									<input type="file" name="project_attachment[]" >
								</div>
								<div class="col-md-3">
									<a href="javascript:void(0)" onclick=add_more_attachment("'.$idName.'")>Add More</a>
								</div>
							</div>';
		if(!empty($getattachment_row)){
			$p=1;
			foreach ($getattachment_row as $key => $value) {
				$attachmentStr.='<div class="col-md-12 margin_uppair" id="attachment_list_row_'.$p.'">';
				$attachmentStr.='<div class="col-md-9"><a href ="'.base_url().$value->impage_path.'"><span>'.$value->image_name.'</span></a></div>';
				$attachmentStr.='<div class="col-md-3"><a href="javascript:void(0)" onclick="remove_dynamic_attachment('.$value->id.','.$p.')" style="color:red;">Remove</a></div>';
				$attachmentStr.='</div>';
				$p++;
			}
		}
		$getrow['html_count']=count($getattachment_row);
		$getrow['html_content']=$attachmentStr;
		echo json_encode($getrow);
	}
	
	public function viewrow(){
		
		$id = $this->input->post('id');
		$where['id'] = $id;
		
		$getrow = $this->User_model->select_where('projects',$where);
		$getrow = $getrow->result();
		$attach['project_id'] = $id;
		$getattachment_que = $this->User_model->select_where('project_attachments',$attach);
		$getattachment_row	=$getattachment_que->result();
		$attachmentStr="";
		if(!empty($getattachment_row)){
			$p=1;
			foreach ($getattachment_row as $key => $value) {
				$attachmentStr.='<div class="col-md-12 margin_uppair" id="attachment_list_row_'.$p.'">';
				$attachmentStr.='<div class="col-md-9"><a href ="'.base_url().$value->impage_path.'"><span>'.$value->image_name.'</span></a></div>';
				$attachmentStr.='</div>';
				$p++;
			}
		}
		$getrow['html_content']=$attachmentStr;
		echo json_encode($getrow);
	}
	
	
	public function removerow(){
		
		$id = $this->input->post('id');
		$where['id'] = $id;
		
		$this->User_model->delete('projects',$where);

		$attach['project_id'] = $id;
		$getattachment_que = $this->User_model->select_where('project_attachments',$attach);
		$getattachment_row	=$getattachment_que->result();
		if(!empty($getattachment_row)){
			foreach ($getattachment_row as $key => $value) {
				$imagePath=$value->impage_path;
				if(file_exists($imagePath)){
					unlink($imagePath);
					$whereDelete['id'] = $value->id;
					$this->User_model->delete('project_attachments',$whereDelete);
				}
			}
		}
		echo '1';
	}
	public function deleteAttachment(){
		$id = $this->input->post('id');
		$where['id'] = $id;
		$getrow = $this->User_model->select_where('project_attachments',$where);
		$getrow = $getrow->result();
		$imagePath=$getrow[0]->impage_path;
		$status="";
		if(file_exists($imagePath)){
			unlink($imagePath);
			$this->User_model->delete('project_attachments',$where);
		    $status="success";
		}else{
			$this->User_model->delete('project_attachments',$where);
		    $status="success";
		}
		echo $status;
	}
	
	
	
}









?>