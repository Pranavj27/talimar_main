<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// $this->load->helper(array('url','html'));
		// $this->load->library('session');
		// $this->load->database();
		$this->load->model('User_model');
		$this->load->library('Aws3','aws3');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');
		
		if($this->session->userdata('t_user_id') != '' )
		{
		}
		else
		{
			redirect(base_url()."home",'refresh');
		}
	}
	
	public function index()
	{
		$fetch_all_contact 	= $this->User_model->select_star('contact','id');
		$fetch_all_contact 	= $fetch_all_contact->result();
		$data['fetch_all_contact'] 	= $fetch_all_contact;
		
		$id = $this->session->userdata('t_user_id');
		$sql = "SELECT * FROM user WHERE id = '".$id."' ";
		
		$result = $this->User_model->query($sql);
		$fetch_result = $result->result();
		$data['fetch_user_detail'] = $fetch_result;				
		$where2['t_user_id'] = $this->session->userdata('t_user_id');				
		$fetch_profile_images = $this->User_model->select_where('profile_image',$where2);				
		if($fetch_profile_images->num_rows() > 0)		
		{			
		$data['image_data'] = 1;						
		$fetch_profile_images1 = $fetch_profile_images->result();			
		$image_name = $fetch_profile_images1[0]->image;			
		$data['image_name'] = $image_name;		
		}		
		else		
		{			
		$data['image_data'] = 0;		
		}
		$data['content'] = $this->load->view('profile_view',$data,true);
		$this->load->view('template_files/template',$data);
	}
	
	
	
	public function password_reset()
	{
		
		$old = md5($this->input->post('old_password'));
		
		$new = $this->input->post('new_password');
		if($old == '' || $new == '' )
		{
			$this->session->set_flashdata('error', ' Plese fill all fields');
					redirect(base_url()."profile" , 'refresh');
					// die('stop');
		}
		if(strlen($new) < 8)		{
			$this->session->set_flashdata('error', ' New password length must be 8 or more');
					redirect(base_url()."profile" , 'refresh');
					// die('stop');
		}
		$id = $this->session->userdata('t_user_id');
		$sql = "SELECT * FROM user WHERE id = '".$id."' ";
		$result = $this->User_model->query($sql);
		if($result->num_rows() > 0)
		{
			$result_fetch = $result->result();
			foreach($result_fetch as $row)
			{
				$fetch_pass = $row->password;
			}
			if($old == $fetch_pass )
			{
				$update = "UPDATE user SET password = '".md5($new)."' WHERE id = '".$id."' ";
				if($this->User_model->query($update) === TRUE)
				{
					$this->session->set_flashdata('success', ' Successfully changed password');
					redirect(base_url()."profile" , 'refresh');
				}
				else
				{
					$this->session->set_flashdata('error', ' SQL Error');
					redirect(base_url()."profile" , 'refresh');
				}
			}
			else
			{
				$this->session->set_flashdata('error', ' Old password not match');
					redirect(base_url()."profile" , 'refresh');
					// die('stop');
			}
		}
		else
		{
			$this->session->set_flashdata('error', ' Wrong password entered');
					redirect(base_url()."profile" , 'refresh');
		}
			
	}				
}