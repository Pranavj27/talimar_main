<?php
class Graph extends MY_Controller
{
	public function __construct()
	{
		ini_set('max_execution_time', 72000); 
		ini_set('memory_limit','2048M');
		
		parent::__construct();
		$this->load->database();
		$this->load->model('User_model');
		$this->load->library('Aws3','aws3');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');

		if($this->session->userdata('t_user_id') == '')
		{
			redirect(base_url());
		}		
	}
	
	public function index()
	{
		error_reporting(0);
		$sql_active = "SELECT COUNT(*) as active_count, SUM(loan.loan_amount) as active_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '2' ";
		
		$sql_paidoff = "SELECT COUNT(*) as paidoff_count, SUM(loan.loan_amount) as paidoff_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '3' ";
		
		$sql_pipeline = "SELECT COUNT(*) as pipeline_count, SUM(loan.loan_amount) as pipeline_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '1' ";
		
		$fetch_active_result 	= $this->User_model->query($sql_active);
		$fetch_active_result 	= $fetch_active_result->result();
		
		$fetch_paidoff_result 	= $this->User_model->query($sql_paidoff);
		$fetch_paidoff_result 	= $fetch_paidoff_result->result();
		
		$fetch_pipeline_result 	= $this->User_model->query($sql_pipeline);
		$fetch_pipeline_result 	= $fetch_pipeline_result->result();
		
		$output = array(
						array(
							'loan_status'	=>'Active',
							'count'			=>number_format($fetch_active_result[0]->active_count),
							'amount'		=>number_format($fetch_active_result[0]->active_amount),
							),
						array(
							'loan_status'	=>'Paid Off',
							'count'			=>number_format($fetch_paidoff_result[0]->paidoff_count),
							'amount'		=>number_format($fetch_paidoff_result[0]->paidoff_amount),
							),
						array(
							'loan_status'	=>'Pipeline',
							'count'			=>number_format($fetch_pipeline_result[0]->pipeline_count),
							'amount'		=>number_format($fetch_pipeline_result[0]->pipeline_amount),
							),
						);
						
		$data['loan_status_data']  = json_encode($output);
		
		$loan_status_data_circle 	= array

				(
					array('y'=>number_format($fetch_active_result[0]->active_count)),
					array('y'=>number_format($fetch_paidoff_result[0]->paidoff_count)),
					array('y'=>number_format($fetch_pipeline_result[0]->pipeline_count)),
				);
				
		$data['loan_status_data_circle'] = json_encode($loan_status_data_circle);
		
		
		
		
		
		$data['content'] = $this->load->view('Reports/graph/all_graphs',$data,true);
		$this->load->view('template_files/template_reports',$data);
	}
	
	public function graph_year()
	{
		error_reporting(0);
		for($i=2010;$i<=2020;$i++)
		{
			$start_date = $i.'-01-01';
			$end_date 	= $i.'-12-31';
			
			$sql_active = "SELECT COUNT(*) as active_count, SUM(loan.loan_amount) as active_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '2' AND loan.loan_funding_date BETWEEN '".$start_date."' AND '".$end_date."' ";
		
			$sql_paidoff = "SELECT COUNT(*) as paidoff_count, SUM(loan.loan_amount) as paidoff_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '3' AND loan.loan_funding_date BETWEEN '".$start_date."' AND '".$end_date."' ";
			
			$sql_pipeline = "SELECT COUNT(*) as pipeline_count, SUM(loan.loan_amount) as pipeline_amount FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_servicing.loan_status = '1' AND loan.loan_funding_date BETWEEN '".$start_date."' AND '".$end_date."' ";
			
			$fetch_active_result 	= $this->User_model->query($sql_active);
			$fetch_active_result 	= $fetch_active_result->result();
			
			$fetch_paidoff_result 	= $this->User_model->query($sql_paidoff);
			$fetch_paidoff_result 	= $fetch_paidoff_result->result();
			
			$fetch_pipeline_result 	= $this->User_model->query($sql_pipeline);
			$fetch_pipeline_result 	= $fetch_pipeline_result->result();
			
			 
			$active_data[] =	array(
								'x'			=>$i,
								'y'			=>$fetch_active_result[0]->active_count ?number_format($fetch_active_result[0]->active_count) : 0,
								// 'amount'		=>number_format($fetch_active_result[0]->active_amount),
								);
			$paid_data[] =	array(
								'x'			=>$i,
								'y'			=>$fetch_paidoff_result[0]->paidoff_count ? number_format($fetch_paidoff_result[0]->paidoff_count) : 0,
								// 'amount'		=>number_format($fetch_paidoff_result[0]->paidoff_amount);
								);
								
			$pipe_data[] =	array(
								'x'	=>$i,
								// 'loan_status'	=>'Pipeline',
								'y'			=>$fetch_pipeline_result[0]->pipeline_count ? number_format($fetch_pipeline_result[0]->pipeline_count) : 0,
								// 'amount'		=>number_format($fetch_pipeline_result[0]->pipeline_amount)
								);
		}
		
		$data['active_data'] 	=  str_replace('"','',json_encode($active_data));
		$data['paid_data'] 		=  str_replace('"','',json_encode($paid_data));
		$data['pipe_data'] 		=  str_replace('"','',json_encode($pipe_data));
		
		$data['a'] = 1;
		$data['content'] = $this->load->view('Reports/graph/flow-all',$data,true);
		$this->load->view('template_files/template_reports',$data);
	}
}




?>	