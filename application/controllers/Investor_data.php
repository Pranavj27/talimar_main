<?php
class Investor_data extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('User_model');
		$this->load->library('Aws3','aws3');
		$this->load->library('email');
		$this->load->helper('mailhtml_helper.php');
		$this->load->helper('common_functions.php');
		
		if($this->session->userdata('t_user_id') != '' )
		{
		}
		else
		{
			redirect(base_url()."home",'refresh');
		}
		
		ini_set('memory_limit', '-1');
		error_reporting(0);
		//for loan originator...
		if($this->session->userdata('user_role') == 1){

			$user_settings = $this->User_model->query("SELECT * FROM `user_settings` WHERE `user_id`= '".$this->session->userdata('t_user_id')."' AND `loan_organization`='1'");
			if($user_settings->num_rows() > 0){

				redirect(base_url()."load_data",'refresh');
			}else{
				$loginuser_borrowers = '';
			}
		}else{
			$loginuser_borrowers = '';
		}
    
    
	}

	/*
		Description : This function use for get Lender details -> Add lender status and Mortgage Fund section add
		Author      : Bitcot
		Created     : 
		Modified    : 07-05-2021

	*/
	
	public function view()
	{
		
		error_reporting(0);
			
		
		$data['user_role'] 		= $this->session->userdata('user_role');
		
		$search_contact_id	 			= $this->input->post('search_contact_id');
		$search_investor_id 	= $this->input->post('search_investor_id');
	
		$contact_idd 			= $this->input->post('search_contact_id') ? $this->input->post('search_contact_id') : $this->input->post('contact_idd');		
		/*fetch lender_id on the base of contact select...*/
		
		$sql_query = $this->User_model->query("SELECT lender_id FROM lender_contact WHERE contact_id  = '".$contact_idd."'");
		
		$iid = $sql_query->row();
		$iidd = '';
		if($sql_query->num_rows() > 0){
			$iidd = $iid->lender_id;
		}
		
		$data['btn_name'] 	=  'view';
		
		
		$investor_id        = $this->input->post('search_investor_id') ? $this->input->post('search_investor_id') : ( $iidd ? $iidd :($this->uri->segment(2)? $this->uri->segment(2) :''))  ;
		
		
		$fetch_contact 			= $this->User_model->query("SELECT * FROM contact ORDER by contact_firstname");
	
		$fetch_contact 				= $fetch_contact->result();
		$data['fetch_all_contact'] 	= $fetch_contact;
		
		$data['investor_idd'] 		= $investor_id;
		
		$data['contact_idd'] 		= $this->input->post('search_contact_id') ? $this->input->post('search_contact_id') : $this->input->post('contact_idd')  ;
		
		$data['search_contact_id'] 		= $this->input->post('search_contact_id') ? $this->input->post('search_contact_id') : $this->input->post('contact_idd')  ;
		
		
		if($investor_id)
		{
			
			$fetch_id_data['id'] 	= $investor_id;
			$fetch_investor_data 	= $this->User_model->select_where('investor',$fetch_id_data);
			$fetch_investor_data 	= $fetch_investor_data->result();
			$data['fetch_investor_data']['id']  				 = $fetch_investor_data[0]->id;
			$data['fetch_investor_data']['talimar_lender']  	 = $fetch_investor_data[0]->talimar_lender;
			$data['fetch_investor_data']['fci_lender']  		 = $fetch_investor_data[0]->fci_lender;
			$data['fetch_investor_data']['fci_acct_service']  	 = $fetch_investor_data[0]->fci_acct_service;
			$data['fetch_investor_data']['del_acct_service']  	 = $fetch_investor_data[0]->del_acct_service;
			$data['fetch_investor_data']['fci_acct']  		     = $fetch_investor_data[0]->fci_acct;
			$data['fetch_investor_data']['del_toro_acct']  		 = $fetch_investor_data[0]->del_toro_acct;
			$data['fetch_investor_data']['del_toro_no']  		 = $fetch_investor_data[0]->del_toro_no;
			$data['fetch_investor_data']['active_fci_account']   = $fetch_investor_data[0]->active_fci_account;
			$data['fetch_investor_data']['investor_type']  		 = $fetch_investor_data[0]->investor_type;
			$data['fetch_investor_data']['ldate_created']  		 = $fetch_investor_data[0]->ldate_created;
			$data['fetch_investor_data']['account_status']  	 = $fetch_investor_data[0]->account_status;
			$data['fetch_investor_data']['entity_name']  		 = $fetch_investor_data[0]->entity_name;
			$data['fetch_investor_data']['entity_type']  		 = $fetch_investor_data[0]->entity_type;
			$data['fetch_investor_data']['entity_res_state']  	 = $fetch_investor_data[0]->entity_res_state;
			$data['fetch_investor_data']['state_entity_no']  	 = $fetch_investor_data[0]->state_entity_no;
			$data['fetch_investor_data']['funding_entity']  	 = $fetch_investor_data[0]->funding_entity;
			$data['fetch_investor_data']['date_of_trust']        = $fetch_investor_data[0]->date_of_trust;
			
			$data['fetch_investor_data']['name']  				 = $fetch_investor_data[0]->name;
			$data['fetch_investor_data']['address']  			 = $fetch_investor_data[0]->address;
			$data['fetch_investor_data']['defined_benefit_plan'] = $fetch_investor_data[0]->defined_benefit_plan;
			$data['fetch_investor_data']['unit']  				 = $fetch_investor_data[0]->unit;
			$data['fetch_investor_data']['custodian']  			 = $fetch_investor_data[0]->custodian;
			$data['fetch_investor_data']['directed_IRA_NAME']  			 = $fetch_investor_data[0]->directed_IRA_NAME;
			$data['fetch_investor_data']['tax_id']  			 = $fetch_investor_data[0]->tax_id;
			$data['fetch_investor_data']['re_date']  			 = $fetch_investor_data[0]->re_date;
			$data['fetch_investor_data']['city']  				 = $fetch_investor_data[0]->city;
			$data['fetch_investor_data']['state']  				 = $fetch_investor_data[0]->state;
			$data['fetch_investor_data']['zip']  				 = $fetch_investor_data[0]->zip;
			$data['fetch_investor_data']['vesting']  			 = $fetch_investor_data[0]->vesting;
			$data['fetch_investor_data']['company_name']  		 = $fetch_investor_data[0]->company_name;
			$data['fetch_investor_data']['company_type']  		 = $fetch_investor_data[0]->company_type;
			$data['fetch_investor_data']['register_state']  	 = $fetch_investor_data[0]->register_state;
			$data['fetch_investor_data']['bank_name']  			 = $fetch_investor_data[0]->bank_name;
			$data['fetch_investor_data']['bank_street']  			 = $fetch_investor_data[0]->bank_street;
			$data['fetch_investor_data']['bank_unit']  			 = $fetch_investor_data[0]->bank_unit;
			$data['fetch_investor_data']['bank_city']  			 = $fetch_investor_data[0]->bank_city;
			$data['fetch_investor_data']['bank_state']  			 = $fetch_investor_data[0]->bank_state;
			$data['fetch_investor_data']['bank_zip']  			 = $fetch_investor_data[0]->bank_zip;
			
			$data['fetch_investor_data']['ach_deposit']  		 = $fetch_investor_data[0]->ach_deposit;
			$data['fetch_investor_data']['account_type']  		 = $fetch_investor_data[0]->account_type ? $fetch_investor_data[0]->account_type : '';
			
			$data['fetch_investor_data']['account_name']  	 = $fetch_investor_data[0]->account_name;
			$data['fetch_investor_data']['account_number']  	 = $fetch_investor_data[0]->account_number;
			$data['fetch_investor_data']['routing_number']  	 = $fetch_investor_data[0]->routing_number;
			$data['fetch_investor_data']['bank_street']  		 = $fetch_investor_data[0]->bank_street;
			$data['fetch_investor_data']['bank_unit']  			 = $fetch_investor_data[0]->bank_unit;
			$data['fetch_investor_data']['bank_city']  			 = $fetch_investor_data[0]->bank_city;
			$data['fetch_investor_data']['bank_state']  		 = $fetch_investor_data[0]->bank_state;
			$data['fetch_investor_data']['bank_zip']  			 = $fetch_investor_data[0]->bank_zip;
			
			$data['fetch_investor_data']['ach_disbrusement']     = $fetch_investor_data[0]->ach_disbrusement;
			$data['fetch_investor_data']['check_disbrusement']   = $fetch_investor_data[0]->check_disbrusement;
			
			
			$data['fetch_investor_data']['mail_attention']  	 = $fetch_investor_data[0]->mail_attention;
			$data['fetch_investor_data']['mail_address']  		 = $fetch_investor_data[0]->mail_address;
			$data['fetch_investor_data']['mail_unit']  			 = $fetch_investor_data[0]->mail_unit;
			$data['fetch_investor_data']['mail_city']  			 = $fetch_investor_data[0]->mail_city;
			$data['fetch_investor_data']['mail_state']  		 = $fetch_investor_data[0]->mail_state;
			$data['fetch_investor_data']['mail_zip']  			 = $fetch_investor_data[0]->mail_zip;
			$data['fetch_investor_data']['other_instruction']    = $fetch_investor_data[0]->other_instruction;
			$data['fetch_investor_data']['bro_talimar']  	 	 = $fetch_investor_data[0]->bro_talimar;
			$data['fetch_investor_data']['bro_lender']  	 	 = $fetch_investor_data[0]->bro_lender;
			$data['fetch_investor_data']['mortgageActivate']  	 = $fetch_investor_data[0]->mortgageActivate;
			$data['fetch_investor_data']['amountInvested']  	 = $fetch_investor_data[0]->amountInvested;
			$data['fetch_investor_data']['mortgageStatus']		 = $fetch_investor_data[0]->mortgageStatus;
			
			$where_1['lender_id'] 	= $investor_id;
			
			$where_2['contact_id'] = $contact_idd;
		
			$fetch_lender_contact 	= $this->User_model->select_where_desc('lender_contact',$where_1,'contact_primary');
			
			if($fetch_lender_contact->num_rows() > 0)
			{
			$fetch_lender_contact 	= $fetch_lender_contact->result();
			$data['fetch_lender_contact'] = $fetch_lender_contact;
			}
			
			$active_lender_sql = "SELECT COUNT(*) as count_loan, SUM(loan_assigment.investment) as total_investment , SUM(loan.loan_amount) as total_loan_amount FROM loan_assigment JOIN loan ON loan_assigment.talimar_loan = loan.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_assigment.lender_name = '".$investor_id."' AND loan_servicing.loan_status = '2' ";
			
			$fetch_active_loan = $this->User_model->query($active_lender_sql);
			if($fetch_active_loan->num_rows() > 0)
			{
				$fetch_active_loan = $fetch_active_loan->result();
				$data['fetch_active_loan'] = array(
												'count_loan' => $fetch_active_loan[0]->count_loan,
												'total_investment' => $fetch_active_loan[0]->total_investment,
												'total_loan_amount' => $fetch_active_loan[0]->total_loan_amount,
													);
			}
			
			$paidoff_lender_sql = "SELECT COUNT(*) as count_loan, SUM(loan_assigment.investment) as total_investment , SUM(loan.loan_amount) as total_loan_amount FROM loan_assigment JOIN loan ON loan_assigment.talimar_loan = loan.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_assigment.lender_name = '".$investor_id."' AND loan_servicing.loan_status = '3' ";
			
			$fetch_paidoff_loan = $this->User_model->query($paidoff_lender_sql);
			if($fetch_paidoff_loan->num_rows() > 0)
			{
				$fetch_paidoff_loan = $fetch_paidoff_loan->result();
				$data['fetch_paidoff_loan'] = array(
												'count_loan' => $fetch_paidoff_loan[0]->count_loan,
												'total_investment' => $fetch_paidoff_loan[0]->total_investment,
												'total_loan_amount' => $fetch_paidoff_loan[0]->total_loan_amount,
													);
			}
		}

		$user_data['t_user_id'] 	= $this->session->userdata('t_user_id');
		$result 					= $this->User_model->select_star('investor');
		$result_new 				= $result->result();
		
		
		$where1['lender_contact_type'] = 1;
		
		
		$user_data['t_user_id'] 	= $this->session->userdata('t_user_id');
	
		$result 					= $this->User_model->select_star('investor');
		$result_new 				= $result->result();
		$data['all_investor_data'] 	= $result_new;
	
		
		 $sql = "SELECT loan_amount,loan.talimar_loan,intrest_rate,lender_name,investment,payment,servicing_fee,loan_status FROM loan JOIN loan_assigment ON loan.talimar_loan = loan_assigment.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_assigment.lender_name = '".$investor_id."' AND loan_servicing.loan_status = '2' ";
		
		$fetch_total_monthly_payment = $this->User_model->query($sql); 
		$total_monthly_payment = 0;
		if($fetch_total_monthly_payment->num_rows() > 0)
			{
				$fetch_total_monthly_payment = $fetch_total_monthly_payment->result();
				
				foreach($fetch_total_monthly_payment as $row)
				{
					$fetchproadd = $this->User_model->query("SELECT property_address,loan_id from loan_property where talimar_loan = '".$row->talimar_loan."'");
					$fetchproadd = $fetchproadd->result();
					$fulladdress = $fetchproadd[0]->property_address;
					$loan_id = $fetchproadd[0]->loan_id;

					$data['fetch_active_status_assigment_data'][] = array(
				
														'loan_amount'   => $row->loan_amount,
														'intrest_rate'  => number_format($row->intrest_rate,3),
														'lender_name'   => $row->lender_name,
														'servicing_fee' => $row->servicing_fee,
														'loan_status'   => $row->loan_status,
														'loan_number'   => $row->talimar_loan,
														'investment'   	=> $row->investment,
														'payment'   	=> $row->payment,
														'fulladdress'   => $fulladdress,
														'loan_id'   	=> $loan_id,
															
														);
						
					$lender_payment = ((($row->intrest_rate - $row->servicing_fee)/100) * $row->loan_amount) ;
					$total_monthly_payment = $total_monthly_payment + $lender_payment;
				}
			
			}
		
		/*new 03-01-2021*/
		$sql = "SELECT loan_amount,loan.talimar_loan,intrest_rate,lender_name,investment,payment,servicing_fee,loan_status FROM loan JOIN loan_assigment ON loan.talimar_loan = loan_assigment.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_assigment.lender_name = '".$investor_id."' AND loan_servicing.loan_status = '1' ";
		
		$pipelineTrust_deed_portfolio = $this->User_model->query($sql); 
		$pipelinetotal_monthly_payment = 0;
		if($pipelineTrust_deed_portfolio->num_rows() > 0)
			{
				$pipelineTrust_deedPortfolio = $pipelineTrust_deed_portfolio->result();
				
				foreach($pipelineTrust_deedPortfolio as $row)
				{
					$fetchproadd = $this->User_model->query("SELECT property_address,loan_id from loan_property where talimar_loan = '".$row->talimar_loan."'");
					$fetchproadd = $fetchproadd->result();
					$fulladdress = $fetchproadd[0]->property_address;
					$loan_id = $fetchproadd[0]->loan_id;

					$data['pipeline_trust_deed_portfolio'][] = array(
				
														'loan_amount'   => $row->loan_amount,
														'intrest_rate'  => number_format($row->intrest_rate,3),
														'lender_name'   => $row->lender_name,
														'servicing_fee' => $row->servicing_fee,
														'loan_status'   => $row->loan_status,
														'loan_number'   => $row->talimar_loan,
														'investment'   	=> $row->investment,
														'payment'   	=> $row->payment,
														'fulladdress'   => $fulladdress,
														'loan_id'   	=> $loan_id,
															
														);
						
					$lender_payment = ((($row->intrest_rate - $row->servicing_fee)/100) * $row->loan_amount) ;
					$pipelinetotal_monthly_payment = $pipelinetotal_monthly_payment + $lender_payment;
				}
			
			}

		/*new 03-01-2021*/
		
		$data['total_lender_monthly_payment_active'] = $total_monthly_payment/12;
			
		// fetch borrower documents by borrower id from 'borrower_document' table...
		$sql_lender_document = $this->User_model->query("SELECT * FROM lender_document WHERE lender_id = '".$investor_id."' ORDER BY id ");
			
		$data['fetch_lender_document'] = $sql_lender_document->result();

		$data['mortgageFundAccounts'] = $this->getLenderLoanData($investor_id);
		/*
			new 27-08-2021 By Bitcot
		*/
		if($investor_id>0){
			$sql_account_checklist=$this->User_model->query("SELECT completed FROM investor_account_checklist WHERE investor_id = '".$investor_id."' and hud='1008' ");
			$fetch_account_checklist = $sql_account_checklist->result();
			if(!empty($fetch_account_checklist)){
				$data['lender_account_checklist'] = $fetch_account_checklist[0]->completed;
			}else{
				$data['lender_account_checklist'] =0;
			}
		}
		
		$data['content'] = $this->load->view('investor_data/view',$data,true);
		$this->load->view('template_files/template',$data);
		
		
	}

	/*
		Description : This function use for get Lender Loan details 
		Author      : Bitcot
		Created     : 29-04-2021
		Modified    : 
	*/

	public function getLenderLoanData($investor_id)
	{

		$fetch_sql = $this->User_model->query("SELECT lp.property_type,loan.talimar_loan as talimar_loan, lp.city as city, lp.state as state, loan_servicing.servicing_lender_rate, loan.id as loan_id, loan.fci as fci, loan.payment_amount as payment_amount, loan.loan_amount as loan_amount, loan.current_balance as current_balance, loan.intrest_rate as intrest_rate , loan.lender_fee as lender_fee, loan.position as position,loan.loan_type as loan_type,  loan.term_month as term, loan.loan_funding_date as funding_date,  loan_servicing.loan_status as loan_status, loan_servicing.condition as loan_condition, loan_servicing.payoff_date as payoff_date, loan_servicing.servicing_fee as servicing_fee, loan_assigment.investment as investment, loan_assigment.invester_yield_percent as invester_yield_percent FROM loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan JOIN loan_property as lp ON lp.talimar_loan = loan_servicing.talimar_loan JOIN loan_assigment ON loan.talimar_loan = loan_assigment.talimar_loan WHERE loan_assigment.lender_name = '".$investor_id."' GROUP BY loan.talimar_loan ORDER BY loan_servicing.loan_status,loan.loan_funding_date ASC ");

		$data = $fetch_sql->result();


		return $data;
	}

	/*
		Description : This function use for get Lender Mortgage Activate status and mortgage status update
		Author      : Bitcot
		Created     : 29-04-2021
		Modified    : 07-05-2021
	*/

	public function updateLendermortgageActivate()
	{
		$mortgageActivate = $this->input->post('mortgageActivate');
		$investor_id = $this->input->post('investor_id');

		if($this->input->post('investor_id'))
		{
			$update = $this->User_model->query("UPDATE investor SET mortgageActivate = '".$mortgageActivate."' WHERE id = '".$investor_id."'");

			if($update)
			{
				echo $this->db->last_query();
			}
			else
			{
				echo false;
			}
		}else
		{
			echo false;
		}
	}

	public function index()
	{
		error_reporting(0);
		// error_reporting(E_ALL);
		
		$search_contact_id	 			= $this->input->post('search_contact_id');
		$data['search_contact_id']	 	= $this->input->post('search_contact_id');
		
		$contact_idd 				= $this->input->post('search_contact_id') ? $this->input->post('search_contact_id') : $this->input->post('contact_idd'); 
		
		//fetch borrower_id on the base of contact select...
		$sql_query = $this->User_model->query("SELECT * FROM borrower_contact WHERE contact_id  = '".$contact_idd."' ");
		
		$iid = $sql_query->row();
		$iidd = '';
		if($sql_query->num_rows() > 0){
			$iidd = $iid->borrower_id;
		}
		
		$data['btn_name'] 	= $this->input->post('btn_name') ? $this->input->post('btn_name') : ($this->uri->segment(2) ? $this->uri->segment(2) : 'view' );
		
		$search_investor_id 	= $this->input->post('search_investor_id');
		// $data['investor_id']	= $this->input->post('search_investor_id');
		
		$data['investor_idd'] 		= $this->input->post('search_investor_id') ? $this->input->post('search_investor_id') : $this->uri->segment(3);
		
		
		$data['contact_idd'] 		= $this->input->post('search_contact_id') ? $this->input->post('search_contact_id') : $this->input->post('contact_idd')  ;
		
		
		
		
		$data['user_role'] 		= $this->session->userdata('user_role');
		
		// $investor_id			= $this->uri->segment(2) ?$this->uri->segment(2) : $search_investor_id;
		
		
		$investor_id        = $this->input->post('search_investor_id') ? $this->input->post('search_investor_id') : ( $iidd ? $iidd :($this->uri->segment(3)? $this->uri->segment(3) :''))  ;
		
		if($investor_id)
		{
			$fetch_id_data['id'] 	= $investor_id;
			$fetch_investor_data 	= $this->User_model->select_where('investor',$fetch_id_data);
			$fetch_investor_data 	= $fetch_investor_data->result();
			$data['fetch_investor_data']['id']  				 = $fetch_investor_data[0]->id;
			$data['fetch_investor_data']['talimar_lender']  	 = $fetch_investor_data[0]->talimar_lender;
			$data['fetch_investor_data']['fci_lender']  		 = $fetch_investor_data[0]->fci_lender;
			$data['fetch_investor_data']['fci_acct_service']  	= $fetch_investor_data[0]->fci_acct_service;
			$data['fetch_investor_data']['del_acct_service']  	= $fetch_investor_data[0]->del_acct_service;
			$data['fetch_investor_data']['fci_acct']  		 = $fetch_investor_data[0]->fci_acct;
			$data['fetch_investor_data']['del_toro_acct']  		 = $fetch_investor_data[0]->del_toro_acct;
			$data['fetch_investor_data']['del_toro_no']  		 = $fetch_investor_data[0]->del_toro_no;
			$data['fetch_investor_data']['active_fci_account']   = $fetch_investor_data[0]->active_fci_account;
			$data['fetch_investor_data']['investor_type']  		 = $fetch_investor_data[0]->investor_type;
			$data['fetch_investor_data']['ldate_created']  		 = $fetch_investor_data[0]->ldate_created;
			$data['fetch_investor_data']['account_status']  	 = $fetch_investor_data[0]->account_status;
			$data['fetch_investor_data']['entity_name']  		 = $fetch_investor_data[0]->entity_name;
			$data['fetch_investor_data']['entity_type']  		 = $fetch_investor_data[0]->entity_type;
			$data['fetch_investor_data']['entity_res_state']  	 = $fetch_investor_data[0]->entity_res_state;
			$data['fetch_investor_data']['state_entity_no']  	 = $fetch_investor_data[0]->state_entity_no;
			$data['fetch_investor_data']['funding_entity']  	 = $fetch_investor_data[0]->funding_entity;
			$data['fetch_investor_data']['date_of_trust']        = $fetch_investor_data[0]->date_of_trust;
			$data['fetch_investor_data']['name']  				 = $fetch_investor_data[0]->name;
			// $data['fetch_investor_data']['is_person']  		 = $fetch_investor_data[0]->is_person;
			$data['fetch_investor_data']['address']  			 = $fetch_investor_data[0]->address;
			$data['fetch_investor_data']['defined_benefit_plan'] = $fetch_investor_data[0]->defined_benefit_plan;
			$data['fetch_investor_data']['unit']  				 = $fetch_investor_data[0]->unit;
			$data['fetch_investor_data']['custodian']  			 = $fetch_investor_data[0]->custodian;
			$data['fetch_investor_data']['self_dir_account']  	 = $fetch_investor_data[0]->self_dir_account;
			$data['fetch_investor_data']['directed_IRA_NAME']  	 = $fetch_investor_data[0]->directed_IRA_NAME;
			$data['fetch_investor_data']['tax_id']  			 = $fetch_investor_data[0]->tax_id;
			$data['fetch_investor_data']['re_date']  			 = $fetch_investor_data[0]->re_date;
			$data['fetch_investor_data']['city']  				 = $fetch_investor_data[0]->city;
			$data['fetch_investor_data']['state']  				 = $fetch_investor_data[0]->state;
			$data['fetch_investor_data']['zip']  				 = $fetch_investor_data[0]->zip;
			$data['fetch_investor_data']['vesting']  			 = $fetch_investor_data[0]->vesting;
			$data['fetch_investor_data']['company_name']  		 = $fetch_investor_data[0]->company_name;
			$data['fetch_investor_data']['company_type']  		 = $fetch_investor_data[0]->company_type;
			$data['fetch_investor_data']['register_state']  	 = $fetch_investor_data[0]->register_state;
			$data['fetch_investor_data']['bank_name']  			 = $fetch_investor_data[0]->bank_name;
			$data['fetch_investor_data']['bank_street']  			 = $fetch_investor_data[0]->bank_street;
			$data['fetch_investor_data']['bank_unit']  			 = $fetch_investor_data[0]->bank_unit;
			$data['fetch_investor_data']['bank_city']  			 = $fetch_investor_data[0]->bank_city;
			$data['fetch_investor_data']['bank_state']  			 = $fetch_investor_data[0]->bank_state;
			$data['fetch_investor_data']['bank_zip']  			 = $fetch_investor_data[0]->bank_zip;
			
			$data['fetch_investor_data']['ach_disbrusement']  = $fetch_investor_data[0]->ach_disbrusement;
			$data['fetch_investor_data']['check_disbrusement']   = $fetch_investor_data[0]->check_disbrusement;
			
			$data['fetch_investor_data']['ach_deposit']  		 = $fetch_investor_data[0]->ach_deposit;
			$data['fetch_investor_data']['account_type']  		 = $fetch_investor_data[0]->account_type ? $fetch_investor_data[0]->account_type : '';
			
			$data['fetch_investor_data']['account_name']  	 = $fetch_investor_data[0]->account_name;
			$data['fetch_investor_data']['account_number']  	 = $fetch_investor_data[0]->account_number;
			$data['fetch_investor_data']['routing_number']  	 = $fetch_investor_data[0]->routing_number;
			$data['fetch_investor_data']['bank_street']  		 = $fetch_investor_data[0]->bank_street;
			$data['fetch_investor_data']['bank_unit']  			 = $fetch_investor_data[0]->bank_unit;
			$data['fetch_investor_data']['bank_city']  			 = $fetch_investor_data[0]->bank_city;
			$data['fetch_investor_data']['bank_state']  		 = $fetch_investor_data[0]->bank_state;
			$data['fetch_investor_data']['bank_zip']  			 = $fetch_investor_data[0]->bank_zip;
			$data['fetch_investor_data']['mail_attention']  	 = $fetch_investor_data[0]->mail_attention;
			$data['fetch_investor_data']['mail_address']  		 = $fetch_investor_data[0]->mail_address;
			$data['fetch_investor_data']['mail_unit']  			 = $fetch_investor_data[0]->mail_unit;
			$data['fetch_investor_data']['mail_city']  			 = $fetch_investor_data[0]->mail_city;
			$data['fetch_investor_data']['mail_state']  		 = $fetch_investor_data[0]->mail_state;
			$data['fetch_investor_data']['mail_zip']  			 = $fetch_investor_data[0]->mail_zip;
			$data['fetch_investor_data']['other_instruction']  	 = $fetch_investor_data[0]->other_instruction;
			$data['fetch_investor_data']['bro_talimar']  	 	 = $fetch_investor_data[0]->bro_talimar;
			$data['fetch_investor_data']['bro_lender']  	 	 = $fetch_investor_data[0]->bro_lender;
			$data['fetch_investor_data']['mortgageActivate']  	 = $fetch_investor_data[0]->mortgageActivate;
			$data['fetch_investor_data']['amountInvested']  	 = $fetch_investor_data[0]->amountInvested;
			$data['fetch_investor_data']['mortgageStatus']  	 = $fetch_investor_data[0]->mortgageStatus;
			
			
			$fetch_id_dat['lender_id'] 	= $investor_id;
			$fetch_invest_other_cont	= $this->User_model->select_where('other_lender_contact',$fetch_id_dat);
			$fetch_invest_other_con	= $fetch_invest_other_cont->result();
			
			$data['contact_phone_other']=$fetch_invest_other_con[0]->contact_phone_other;
			$data['contact_email_other']=$fetch_invest_other_con[0]->contact_email_other;
			$data['o_contact_id']=$fetch_invest_other_con[0]->contact_id;
			$data['o_lender_id']=$fetch_invest_other_con[0]->lender_id;
		
			
			
			
			$where_1['lender_id'] 	= $investor_id;
			$fetch_lender_contact 	= $this->User_model->select_where_desc('lender_contact',$where_1,'contact_primary');
			if($fetch_lender_contact->num_rows() > 0)
			{
			$fetch_lender_contact 	= $fetch_lender_contact->result();
			$data['fetch_lender_contact'] = $fetch_lender_contact;
			}
			
			$active_lender_sql = "SELECT COUNT(*) as count_loan, SUM(loan_assigment.investment) as total_investment , SUM(loan.loan_amount) as total_loan_amount FROM loan_assigment JOIN loan ON loan_assigment.talimar_loan = loan.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_assigment.lender_name = '".$investor_id."' AND loan_servicing.loan_status = '2' ";
			
			$fetch_active_loan = $this->User_model->query($active_lender_sql);
			if($fetch_active_loan->num_rows() > 0)
			{
				$fetch_active_loan = $fetch_active_loan->result();
				$data['fetch_active_loan'] = array(
												'count_loan' => $fetch_active_loan[0]->count_loan,
												'total_investment' => $fetch_active_loan[0]->total_investment,
												'total_loan_amount' => $fetch_active_loan[0]->total_loan_amount,
													);
			}
			
			
			
			
			$paidoff_lender_sql = "SELECT COUNT(*) as count_loan, SUM(loan_assigment.investment) as total_investment , SUM(loan.loan_amount) as total_loan_amount FROM loan_assigment JOIN loan ON loan_assigment.talimar_loan = loan.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_assigment.lender_name = '".$investor_id."' AND loan_servicing.loan_status = '3' ";
			
			$fetch_paidoff_loan = $this->User_model->query($paidoff_lender_sql);
			if($fetch_paidoff_loan->num_rows() > 0)
			{
				$fetch_paidoff_loan = $fetch_paidoff_loan->result();
				$data['fetch_paidoff_loan'] = array(
												'count_loan' => $fetch_paidoff_loan[0]->count_loan,
												'total_investment' => $fetch_paidoff_loan[0]->total_investment,
												'total_loan_amount' => $fetch_paidoff_loan[0]->total_loan_amount,
													);
			}
		}
		
		$user_data['t_user_id'] 	= $this->session->userdata('t_user_id');
		$result 					= $this->User_model->select_star('investor');
		$result_new 				= $result->result();
		
		
		$where1['lender_contact_type'] = 1;
		// $fetch_all_contact 			= $this->User_model->select_where_asc('contact',$where1,'contact_firstname');
		$fetch_all_contact 			= $this->User_model->query("SELECT * FROM contact ORDER by contact_firstname");
		$data['fetch_all_contact'] 	= $fetch_all_contact->result();
		
		$user_data['t_user_id'] 	= $this->session->userdata('t_user_id');
		$result 					= $this->User_model->select_star('investor');
		$result_new 				= $result->result();
		$data['all_investor_data'] 	= $result_new;
		// die('dd');
		
		$data['generate_random_number'] = $this->generate_random_number();
		
		
		$sql = "SELECT loan_amount,loan.talimar_loan,intrest_rate,lender_name,servicing_fee,loan_status FROM loan JOIN loan_assigment ON loan.talimar_loan = loan_assigment.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_assigment.lender_name = '".$investor_id."' AND loan_servicing.loan_status = '2' ";
		
		$fetch_total_monthly_payment = $this->User_model->query($sql); 
		$total_monthly_payment = 0;
		if($fetch_total_monthly_payment->num_rows() > 0)
			{
				$fetch_total_monthly_payment = $fetch_total_monthly_payment->result();
				
				foreach($fetch_total_monthly_payment as $row)
				{
				$data['fetch_active_status_assigment_data'][] = array(
				
						'loan_amount'   => $row->loan_amount,
						'intrest_rate'  => number_format($row->intrest_rate,3),
						'lender_name'   => $row->lender_name,
						'servicing_fee' => $row->servicing_fee,
						'loan_status'   => $row->loan_status,
						'loan_number'   => $row->talimar_loan,
							
						);
						
					$lender_payment = ((($row->intrest_rate - $row->servicing_fee)/100) * $row->loan_amount) ;
					$total_monthly_payment = $total_monthly_payment + $lender_payment;
				}
			
			}


		/*new 03-01-2021*/
		$sql = "SELECT loan_amount,loan.talimar_loan,intrest_rate,lender_name,servicing_fee,loan_status FROM loan JOIN loan_assigment ON loan.talimar_loan = loan_assigment.talimar_loan JOIN loan_servicing ON loan.talimar_loan = loan_servicing.talimar_loan WHERE loan_assigment.lender_name = '".$investor_id."' AND loan_servicing.loan_status = '1' ";
		
		$Pipelinefetch_total_monthly_payment = $this->User_model->query($sql); 

		

		$Pipelinetotal_monthly_payment = 0;
		if($Pipelinefetch_total_monthly_payment->num_rows() > 0)
			{
				$Pipelinefetch_total_monthlyPayment = $Pipelinefetch_total_monthly_payment->result();
				
				foreach($Pipelinefetch_total_monthlyPayment as $row)
				{
				$data['pipeline_trust_deed_portfolio'][] = array(
				
						'loan_amount'   => $row->loan_amount,
						'intrest_rate'  => number_format($row->intrest_rate,3),
						'lender_name'   => $row->lender_name,
						'servicing_fee' => $row->servicing_fee,
						'loan_status'   => $row->loan_status,
						'loan_number'   => $row->talimar_loan,
							
						);
						
					$lender_payment = ((($row->intrest_rate - $row->servicing_fee)/100) * $row->loan_amount) ;
					$Pipelinetotal_monthly_payment = $Pipelinetotal_monthly_payment + $lender_payment;
				}
			
			}
		/*new 03-01-2021*/
	
		
		$data['total_lender_monthly_payment_active'] = $total_monthly_payment/12;
		// fetch borrower documents by borrower id from 'borrower_document' table...
		$sql_lender_document = $this->User_model->query("SELECT * FROM lender_document WHERE lender_id = '".$investor_id."' ORDER BY id ");
			
			$data['fetch_lender_document'] = $sql_lender_document->result();

		$data['content'] = $this->load->view('investor_data/index',$data,true);
		$this->load->view('template_files/template',$data);
		
	}
	
	public function fetch_lender_details(){
		
		$fetch = $this->User_model->query("SELECT email,phone,c2_email,c2_phone FROM investor WHERE id = '".$this->input->post('id')."'  ");
		$result = $fetch->row();
		
		if($this->input->post('contact_type') == '1'){
			
			// fetch contact details of person1....
			$output['email'] = $result->email;
			$output['phone'] = $result->phone_2;
			
			print_r(json_encode($output));
			
		}else if($this->input->post('contact_type') == '2'){
			
			// fetch contact details of person2....
			$output['email'] = $result->c2_email;
			$output['phone'] = $result->c2_phone;

			print_r(json_encode($output));
		}
		
	}
	
	function add_investor_data()
	{
		
		error_reporting(0);


		$f_t_user_id						=	$this->session->userdata('t_user_id');
		
		
		$investor_id 						= $this->input->post('investor_id');
		$button 							= $this->input->post('button');
		$talimar_lender =  '';
		if($this->input->post('talimar_lender')){
			$talimar_lender = $this->input->post('talimar_lender');
		}
		$data['talimar_lender']    			= $talimar_lender;
		$data['fci_acct_service']    		= $this->input->post('fci_acct_service');
		$data['del_acct_service']    		= $this->input->post('del_acct_service');
		$data['fci_acct']    				= $this->input->post('fci_acct');
		$data['del_toro_acct']    			= $this->input->post('del_toro_acct');
		$data['fci_lender']    				= $this->input->post('fci_lender');
		$data['del_toro_no']    			= $this->input->post('del_toro_no');
		$data['name']    					= $this->input->post('investor_name');
		$data['investor_type']    			= $this->input->post('investor_type');

		if ($this->input->post('ldate_created') != '') {
			//$bdate_createdVal = DateTime::createFromFormat('m-d-Y', $this->input->post('ldate_created'));
			//$ldate_created = $bdate_createdVal->format('Y-m-d');
			$ldate_created = input_date_format($this->input->post('ldate_created'));
		} else {
			$ldate_created = NULL;
		}

		$data['ldate_created']				= $ldate_created;
		$entity_date_trust=$this->input->post('entity_date_trust');
		if(!empty($entity_date_trust)){
			$data['date_of_trust']=date('Y-m-d',strtotime($entity_date_trust));
		}

		$data['account_status']    			= $this->input->post('account_status');
		$data['active_fci_account']    		= $this->input->post('active_fci_account');
		$data['entity_name']    			= $this->input->post('entity_name');
		$data['entity_type']    			= $this->input->post('entity_type');
		$data['entity_res_state']    		= $this->input->post('entity_res_state');
		$data['state_entity_no']    		= $this->input->post('state_entity_no');
		$data['funding_entity']    			= $this->input->post('funding_entity');
		// $data['register_state']    		= $this->input->post('register_state');
		$data['address']    				= $this->input->post('address');
		$data['defined_benefit_plan']    	= $this->input->post('defined_benefit_plan');
		$data['unit']    					= $this->input->post('unit');
		$data['custodian']    				= $this->input->post('custodian');
		$data['self_dir_account']    		= $this->input->post('self_dir_account');
		$data['directed_IRA_NAME']    		= $this->input->post('directed_IRA_NAME');
		$data['tax_id']    					= $this->input->post('tax_id');
		$data['new_lender_status']    		= 1;
		if($this->input->post('re_date')){
			//$myDateTime = DateTime::createFromFormat('m-d-Y', $this->input->post('re_date'));
			//$re_date = $myDateTime->format('Y-m-d');
			$re_date = input_date_format($this->input->post('re_date'));
			$data['re_date']    				= $re_date;
		}
		
		$data['city']    						= $this->input->post('city');
		$data['state']    						= $this->input->post('state');
		$data['zip']    						= $this->input->post('zip');
		$data['vesting']    					= $this->input->post('vesting');
		$data['ach_deposit']    				= $this->input->post('ach_deposit');
		$data['bank_name']    					= $this->input->post('bank_name');
		$data['bank_street']    				= $this->input->post('bank_street');
		$data['bank_unit']    					= $this->input->post('bank_unit');
		$data['bank_city']    					= $this->input->post('bank_city');
		$data['bank_state']    					= $this->input->post('bank_state');
		$data['bank_zip']    					= $this->input->post('bank_zip');
		$data['ach_disbrusement']    			= $this->input->post('ach_disbrusement');
		$data['check_disbrusement']    			= $this->input->post('check_disbrusement');		
		$data['account_type']    				= $this->input->post('account_type');
		$data['account_name']    				= $this->input->post('account_name');
		$data['account_number']    				= $this->input->post('account_number');
		$data['routing_number']    				= $this->input->post('routing_number');
		
		$data['mail_attention']    				= $this->input->post('mail_attention');
		$data['mail_address']    				= $this->input->post('mail_address');
		$data['mail_city']    					= $this->input->post('mail_city');
		$data['mail_state']    					= $this->input->post('mail_state');
		$data['mail_zip']    					= $this->input->post('mail_zip');
		$data['mail_unit']    					= $this->input->post('mail_unit');
		$data['other_instruction']    			= $this->input->post('other_instruction');
		$data['mortgageActivate']    			= $this->input->post('mortgageActivate');
		$data['amountInvested']    			    = $this->input->post('amountInvested');
		$data['mortgageStatus']    			    = $this->input->post('mortgageStatus');
		$bro_talimar 							= $this->input->post('Btalimar');
		$implode_bro_talimar    				= implode(',', $bro_talimar);
		$bro_lender 							= $this->input->post('Blender');
		$implode_bro_lender    					= implode(',', $bro_lender);
		$data['bro_talimar']    				= $implode_bro_talimar;
		$data['bro_lender']    					= $implode_bro_lender;
		
		$data['t_user_id']    				= $this->session->userdata('t_user_id');
		$data['created_date']    			= date('m-d-Y');
		$t_user_id 							= $this->session->userdata('t_user_id');
		
		$contact_id							= array_filter($this->input->post('contact_id'));
		$primary_contact 					= $this->input->post('primary_contact'); 
		$contact_phone						= $this->input->post('contact_phone');
		$contact_email						= $this->input->post('contact_email');
		$contact_title						= $this->input->post('contact_title');
		$contact_role						= $this->input->post('contact_role');
		$required_sign						= $this->input->post('required_sign');
		$link_lender						= $this->input->post('link_lender');
		$contact_re_date					= $this->input->post('contact_re_date');
		$user_id 							= $this->session->userdata('t_user_id');
	
		
		if($button == 'save')
		{	
			if($this->input->post('investor_name') == ''  ){
				$this->session->set_flashdata('error', ' Investor name must');		
				redirect(base_url().'investor_data','refresh');		
				die('');	
			}
			
			if($investor_id == ''){
				// echo 'new insert';
				$talimar_lender = '';
				if($this->input->post('talimar_lender')){
					$talimar_lender = $this->input->post('talimar_lender');
				}
				$check_loan['talimar_lender'] = $talimar_lender;
				$check_loan_query = $this->User_model->select_where('investor',$check_loan);
				
				if($check_loan_query->num_rows() == 1)
				{

				}
				
				$check_fci['fci_lender'] = $this->input->post('fci_lender');
				$check_fci_query = $this->User_model->select_where('investor',$check_fci);
				
				if($check_fci_query->num_rows() == 1)
				{
					
				}
				
				/*  insert in loan monthly payments... */
				
				$this->User_model->insertdata('investor',$data);
				
				 $insert_id = $this->db->insert_id();
				 
				 
				if($insert_id)
				{
					
					if($contact_id){
						$contact_id_arr = array_values(array_filter($contact_id));
						if($contact_id_arr){
						
							foreach($contact_id_arr as $key => $row)
							{
								$datecontact_re_date = "NULL";
								if($contact_re_date[$key])
								{
									//$myDateTime = DateTime::createFromFormat('m-d-Y', $contact_re_date[$key]);
									//$re_date = $myDateTime->format('Y-m-d');
									$re_date = input_date_format($contact_re_date[$key]);
									$datecontact_re_date    = "'".$re_date."'";
								}
								
								
								
								
								
								$sql = "INSERT INTO `lender_contact` ( `lender_id`, `contact_id`,`link_to_lender`,`contact_primary`, `c_title`,`contact_role`, `contact_phone`, `contact_email`, `contact_re_date`, `required_sign`, `t_user_id`) VALUES ( '".$insert_id."', '".$row."', '".$link_lender[$key]."', '".$primary_contact[$key]."',  '".$contact_title[$key]."', '".$contact_role[$key]."', '".$contact_phone[$key]."',  '".$contact_email[$key]."', ".$datecontact_re_date.", '".$required_sign[$key]."', '".$t_user_id."' )";
								$this->User_model->query($sql);
								
							
								
								
							
							}
						
						}
					}
									
					 $contact_phone_other=$this->input->post('contact_phone_other');
					$contact_email_other=$this->input->post('contact_email_other');
					$other_contact_id=$this->input->post('other_contact_id');
					$user_id = $this->session->userdata('t_user_id');
					if($contact_phone_other){
					
					$sqll =("INSERT INTO other_lender_contact(lender_id,contact_id,contact_phone_other,contact_email_other,t_user_id) VALUES('".$insert_id."','".$other_contact_id."','".$contact_phone_other."','".$contact_email_other."','".$user_id."')");
				
				  		$this->User_model->query($sqll);
					}
					$this->session->set_flashdata('success', ' Recorded added successfully!');
					redirect(base_url().'investor_view','refresh');
	
				}
				else
				{
					// echo "false insert id";
					$this->session->set_flashdata('error','Something went wrong');
					redirect(base_url().'load_data','refresh');
				}
				
				
			
			} else {
				
				
					$fetch_check_borrowe = $this->User_model->query("select * from other_lender_contact where lender_id='".$investor_id."'");

					$contact_phone_other=$this->input->post('contact_phone_other');
					$contact_email_other=$this->input->post('contact_email_other');
					$other_contact_id=$this->input->post('other_contact_id');

					if($fetch_check_borrowe->num_rows() > 0)
					{
				
					$sqlll = "UPDATE other_lender_contact SET contact_phone_other = '".$contact_phone_other."', contact_email_other='".$contact_email_other."',contact_id='".$other_contact_id."' WHERE  lender_id = '".$investor_id."'";
					 $this->User_model->query($sqlll);
										
					}else{

						$sqll ="INSERT INTO other_lender_contact(lender_id,contact_id,contact_phone_other,contact_email_other,t_user_id) VALUES('".$investor_id."','".$other_contact_id."','".$contact_phone_other."','".$contact_email_other."','".$user_id."')";
				
				 		 $this->User_model->query($sqll);

					}
				
				
				// echo 'updated';
				$update_id['id'] = $investor_id;
				$this->User_model->updatedata('investor',$update_id,$data);
				
				
				if($contact_id){
					
					$contact_id_arr = array_values(array_filter($contact_id));
					if($contact_id_arr){
						
						$emailIdsArrays=array();
						foreach($contact_id_arr as $key => $row)
						{
							$datecontact_re_date = "NULL";
							if($contact_re_date[$key])
							{
								//$myDateTime = DateTime::createFromFormat('m-d-Y', $contact_re_date[$key]);
								//$re_date = $myDateTime->format('Y-m-d');
								$re_date = input_date_format($contact_re_date[$key]);
										
								$datecontact_re_date    		= "'".$re_date."'";
							}
							
							$check_where1['contact_id'] 		= $row;
							$check_where1['lender_id'] 			= $investor_id;
							$fetch_check_borrower_contact = $this->User_model->select_where('lender_contact',$check_where1);
							$fetch_contact_email= $fetch_check_borrower_contact->result();
							$emailIdsArrays[]=$fetch_contact_email[0]->contact_email;
							if($fetch_check_borrower_contact->num_rows() > 0)
							{
								$sql = "UPDATE `lender_contact` SET link_to_lender = '".$link_lender[$key]."', c_title = '".$contact_title[$key]."', contact_role='".$contact_role[$key]."', contact_primary = '".$primary_contact[$key]."' , contact_phone = '".$contact_phone[$key]."', contact_email = '".$contact_email[$key]."', contact_re_date =".$datecontact_re_date.",required_sign ='".$required_sign[$key]."' WHERE  lender_id ='".$investor_id."' AND contact_id ='".$row."' ";
										
							} else{
								
								
								$sql = "INSERT INTO `lender_contact` ( `lender_id`, `contact_id`, `link_to_lender`, `contact_primary`, `c_title`, `contact_role`, `contact_phone`, `contact_email`, `contact_re_date`,  `required_sign`, `t_user_id`) VALUES ( '".$investor_id."', '".$row."',  '".$link_lender[$key]."', '".$primary_contact[$key]."', '".$contact_title[$key]."', '".$contact_role[$key]."', '".$contact_phone[$key]."',  '".$contact_email[$key]."',  ".$datecontact_re_date.", '".$required_sign[$key]."',  '".$t_user_id."' )";
							
							}
								
							//$this->User_model->query($sql);
						}
						//Send Mail to all contact if account activate start
						$previous_account_status=$this->input->post('previous_account_status');
						$current_account_status	= $this->input->post('account_status');
						$send_mail_status=0;
						if($previous_account_status=="1" && $current_account_status=="2"){
							$send_mail_status=1;
						}
						if(is_array($contact_id_arr) && count($contact_id_arr)>0 && $send_mail_status=="1"){
							$Message = '<p>Your Lender account successfully activated . </p> 
									<p>Should you have any questions, please contact Investor Relations at (858) 242-4900. </p>';
							$dataMailA['from_email']  	= 'bvandenberg@talimarfinancial.com';
					        $dataMailA['from_name']   	= 'TaliMar Financial';
					        $dataMailA['cc']      		= '';
					        $dataMailA['bcc']      		= '';
					        $dataMailA['name']      	= '';
					        $dataMailA['subject']     	= 'Lender Account Activate';
					        $dataMailA['content_body']  = $Message;
					        $dataMailA['attach_file']   = '';
					        $emailIdsArrays[]="amarjain@bitcot.com";
							foreach($emailIdsArrays as  $row)
							{
								if(!empty($row)){
									$dataMailA['to']= $row;
				        			send_mail_template($dataMailA);
								}
							}

						}
					}
				
				}
			
				$this->session->set_flashdata('success', 'Successfully Updated! ');
				redirect(base_url().'investor_view/'.$investor_id,'refresh');
			}
		
		}
		/* else{
			echo 'no save button';
		} */
	
	}
	
	public function delete_investor()
	{
		if($this->session->userdata('user_role') == 2)
		{
			$id['id'] = $this->uri->segment(2);
			$this->User_model->delete('investor',$id);
			$this->session->set_flashdata('success',' Deleted');
			redirect(base_url().'investor_view', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('error',' You do not have permission to delete');
			redirect(base_url().'investor_view', 'refresh');
		}
	}
	
	public function delete_contact_f()
	{
		$id = $this->uri->segment(2);
		if($id)
		{
			$lender_id['id'] = $id;
			
			$lender_c1['first_name'] 		= '';
			$lender_c1['middle_initial'] 	= '';
			$lender_c1['last_name'] 		= '';
			$lender_c1['c_address'] 		= '';
			$lender_c1['c_unit'] 			= '';
			$lender_c1['c_city'] 			= '';
			$lender_c1['c_state'] 			= '';
			$lender_c1['c_zip'] 			= '';
			$lender_c1['title'] 			= '';
			$lender_c1['phone_1'] 			= '';
			$lender_c1['email'] 			= '';
			
			$this->User_model->updatedata('investor',$lender_id,$lender_c1);
			$this->session->set_flashdata('success','Deleted');
			redirect(base_url().'investor_data/'.$id, 'refresh');
		}
		
	}
	
	public function delete_contact_s()
	{
		$id = $this->uri->segment(2);
		if($id)
		{
			$lender_id['id'] = $id;
			
			$lender_c2['c2_first_name'] 		= '';
			$lender_c2['c2_middle_initial'] 	= '';
			$lender_c2['c2_last_name'] 			= '';
			$lender_c2['c2_address'] 			= '';
			$lender_c2['c2_unit'] 				= '';
			$lender_c2['c2_city'] 				= '';
			$lender_c2['c2_state'] 				= '';
			$lender_c2['c2_zip'] 				= '';
			$lender_c2['c2_title'] 				= '';
			$lender_c2['c2_phone'] 				= '';
			$lender_c2['c2_email'] 				= '';
			
			$this->User_model->updatedata('investor',$lender_id,$lender_c2);
			$this->session->set_flashdata('success','Deleted');
			redirect(base_url().'investor_data/'.$id, 'refresh');
		}
	}
	
	public function delete_lender_contact()
	{
		if($this->uri->segment(2))
		{
			$id['id'] = trim($this->uri->segment(2),'D');
			$this->User_model->delete('lender_contact',$id);
			$this->session->set_flashdata('success', 'Deleted!');
			redirect(base_url().'investor_view','refresh');
		}
	}
	
	public function updates_contacts_data()
	{
		$fetch_lender_contact = $this->User_model->select_star('lender_contact');
		$fetch_lender_contact = $fetch_lender_contact->result();
		foreach($fetch_lender_contact as $row)
		{
			$fetch_contact = $this->User_model->select_where('contact',array('contact_id' => $row->contact_id));
			if($fetch_contact->num_rows() > 0)
			{
				$fetch_contact 				= $fetch_contact->result();
				$update_d['contact_phone']  = $fetch_contact[0]->contact_phone;
				$update_d['contact_email']  = $fetch_contact[0]->contact_email;
				
				$fetch_lender_contact_type = $this->User_model->select_where('lender_contact_type',array('contact_id' => $row->contact_id));
				if($fetch_lender_contact_type->num_rows() > 0)
				{
					$fetch_lender_contact_type = $fetch_lender_contact_type->result();
					$update_d['contact_re_date'] = $fetch_lender_contact_type[0]->lender_RE_date;
				}
				$this->User_model->updatedata('lender_contact',array('contact_id' => $row->contact_id),$update_d);
			}
		}
		
	}
	
	
	public function randomPrefix($length=7) {       
        $random = "";
        srand((double) microtime() * 1000000);
        $data = "0123456789";
       // $data = "A0B1C2DE3FG4HIJ5KLM6NOP7QR8ST9UVW0XYZ";
        for ($i = 0; $i < $length; $i++) {
            $random .= substr($data, (rand() % (strlen($data))), 1);
        }

        return $random;
    }
	
	public function validateActivationToken($token)
	{
		
		$stmt = $this->User_model->query("SELECT * FROM investor WHERE talimar_lender = '".$token."'");
		
		$num_returns = $stmt->num_rows();
		if($num_returns > 0)
		{
			return true;
		}
		else
		{
			return false;	
		}
	}
	
	public function generate_random_number($gen = null)
	{
		do
		{
			$gen = $this->randomPrefix();
		}
		while($this->validateActivationToken($gen));
		return $gen;
	}
	
	
	public function lender_account_setup(){
		
		error_reporting(0);
		$investor_id = $this->uri->segment(2);
		//echo $investor_id;
		
		header("Cache-Control: ");// leave blank to avoid IE errors
			header("Pragma: ");// leave blank to avoid IE errors
			header("Content-type: application/octet-stream");
			// header("Content-type: application/vnd.ms-word");

			header("content-disposition: attachment;filename=TaliMar_investor_$investor_id.doc");
		echo '<html xmlns:v="urn:schemas-microsoft-com:vml"
				xmlns:o="urn:schemas-microsoft-com:office:office"
				xmlns:w="urn:schemas-microsoft-com:office:word"
				xmlns="http://www.w3.org/TR/REC-html40">

				<head>
				<meta http-equiv=Content-Type content="text/html; charset=utf-8">
				<meta name=ProgId content=Word.Document>
				<meta name=Generator content="Microsoft Word 9">
				<meta name=Originator content="Microsoft Word 9">
				<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->
				<title>title</title>
				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				body{
					font-size:10pt;
				}
				</style>
				</head>
				<body>';

echo '<style>

		table.table{
			width:100%;
			padding : 3px 0px;
		}
		table.table th{
			text-align : left;
			font-size : 10pt;
			
		}
		table.table td{
			font-size:10pt;
			font-size : 10pt;
		
		}
		table.two_contact_signer td{
			margin : 3px;
			font-size:10pt;
		}
		h1{
			text-align:center;
		}
		h2{
			text-align:center;
		}
		p{
			font-size:10pt;
			word-spacing : 10px;
			padding : 2px 0px;
		}
		table{
			font-size:10pt;
		}
</style>';
		
		//fetch investor data...
		$investor = "SELECT * FROM investor WHERE id='".$investor_id."'";
		$fetch_investor_data = $this->User_model->query($investor);
		$fetch_investor_data = $fetch_investor_data->result();
		
		//Fetch lender contact 
			// lender contact 1
			$c_lender_contact1_name 		= '';
			$c_lender_contact1_title 		= '';
			$c_lender_contact1_phone 		= '';
			$c_lender_contact1_email 		= '';
			// lender contact 2
			$c_lender_contact2_name 		= '';
			$c_lender_contact2_title 		= '';
			$c_lender_contact2_phone 		= '';
			$c_lender_contact2_email 		= '';
			
			$sql = 'SELECT * FROM lender_contact JOIN contact ON lender_contact.contact_id = contact.contact_id WHERE lender_contact.lender_id = "'.$investor_id.'"';
			$fetch_lender_contact_detail1	= $this->User_model->query($sql);
			if($fetch_lender_contact_detail1->num_rows() > 0)
			{
				$fetch_lender_contact_detail 	= $fetch_lender_contact_detail1->result();
				
				$required_sign0 = $fetch_lender_contact_detail[0]->required_sign ? $fetch_lender_contact_detail[0]->required_sign : '';
				
				$c_lender_contact1_phone 		= $fetch_lender_contact_detail[0]->contact_phone ? $fetch_lender_contact_detail[0]->contact_phone : '';
					
				$c_lender_contact1_email 		= $fetch_lender_contact_detail[0]->contact_email ? $fetch_lender_contact_detail[0]->contact_email : '';
				
				if($required_sign0 == '1'){
				
					$c_lender_contact1_name 		= $fetch_lender_contact_detail[0]->contact_firstname.' '.$fetch_lender_contact_detail[0]->contact_middlename.' '.$fetch_lender_contact_detail[0]->contact_lastname;
					
					$c_lender_contact1_title 		= $fetch_lender_contact_detail[0]->c_title ? $fetch_lender_contact_detail[0]->c_title : '';
					
				}
				
				$required_sign1 = $fetch_lender_contact_detail[1]->required_sign ? $fetch_lender_contact_detail[0]->required_sign : '';
				
				if($required_sign1 == '1'){
					
				$c_lender_contact2_name 		= $fetch_lender_contact_detail[1]->contact_firstname ? $fetch_lender_contact_detail[1]->contact_firstname.' '.$fetch_lender_contact_detail[1]->contact_middlename.' '.$fetch_lender_contact_detail[1]->contact_lastname : '';
				
				$c_lender_contact2_title 		= $fetch_lender_contact_detail[1]->c_title ?$fetch_lender_contact_detail[1]->c_title : '';
				
				}
				
				$c_lender_contact2_phone 		= $fetch_lender_contact_detail[1]->contact_phone ?$fetch_lender_contact_detail[1]->contact_phone : '';
				
				$c_lender_contact2_email 		= $fetch_lender_contact_detail[1]->contact_email ?$fetch_lender_contact_detail[1]->contact_email : '';
			}
		
			$lender_account_setup =  '<table><tr><td></td></tr></table>';
			$lender_account_setup .= '<table class="table" style="">';
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td>';
			$lender_account_setup .= '<img src = "'.base_url().'assets/admin/layout4/img/logo-light.png" />';
			$lender_account_setup .= '</td>';
			$lender_account_setup .= '<td style="vertical-align:top;text-align:right;font-size:8pt;color:gray;">
										TaliMar Financial, Inc.
										<br>11440 West Bernardo Court, Suite #210 
										<br>San Diego, CA 92127
										<br>Tel:  858.613.0111
										<br>Toll Free: 888.868.8467
										<br>www.talimarfinancial.com
										</td>';
										
			$lender_account_setup .= '</tr>';
			$lender_account_setup .= '</table>';
			$lender_account_setup .= '<h3><center>LENDER ACCOUNT SETUP FORM</center></h3>';
			
			$lender_account_setup .= '<p style="text-decoration:underline;"><strong>Personal Information :</strong></p>';
			
			$lender_account_setup .= '<table class="table" style="width:100%";vertical-align:top;">';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:30%;">Contact Name 1:</td>';
			$lender_account_setup .= '<td style="width:70%;">'.$c_lender_contact1_name.'</td>';
			$lender_account_setup .= '</tr>';
			
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:30%;">Contact Name 2:</td>';
			$lender_account_setup .= '<td style="width:70%;">'.$c_lender_contact2_name.'</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:30%;">Street Address:</td>';
			$lender_account_setup .= '<td style="width:70%;">'.$fetch_investor_data[0]->address .', '.$fetch_investor_data[0]->unit .'</td>';
			$lender_account_setup .= '</tr>';
			
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:30%;">City:</td>';
			$lender_account_setup .= '<td style="width:70%;">'.$fetch_investor_data[0]->city.'</td>';
			$lender_account_setup .= '</tr>';
			
			$STATE_USA = $this->config->item('STATE_USA');
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:30%;">State:</td>';
			$lender_account_setup .= '<td style="width:70%;">'.$STATE_USA[$fetch_investor_data[0]->state].'</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:30%;">Zip:</td>';
			$lender_account_setup .= '<td style="width:70%;">'.$fetch_investor_data[0]->zip .'</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:30%;">Phone:</td>';
			$lender_account_setup .= '<td style="width:70%;">'.$c_lender_contact1_phone.'</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '</table>';
			
			$lender_account_setup .= '<p style="text-decoration:underline;"><strong>Account Information :</strong></p>';
			
			$lender_account_setup .= '<table class="table" style="width:100%;vertical-align:top;">';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:30%;">Lender Name:</td>';
			$lender_account_setup .= '<td style="width:70%;">'.$fetch_investor_data[0]->name .'</td>';
			$lender_account_setup .= '</tr >';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:30%;">Tex ID #:</td>';
			$lender_account_setup .= '<td style="width:70%;">'.$fetch_investor_data[0]->tax_id.'</td>';
			$lender_account_setup .= '</tr >';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:30%;">Vesting:</td>';
			$lender_account_setup .= '<td style="width:70%;">'.$fetch_investor_data[0]->vesting.'</td>';
			$lender_account_setup .= '</tr >';
			
			$lender_account_setup .= '</table>';
			
			$lender_account_setup .= '<p style="text-decoration:underline;"><strong>Account Type:</strong></p>';
			
			$lender_account_setup .= '<table class="table" style="width:100%;vertical-align:top;">';
			
			if($fetch_investor_data[0]->entity_type == '1'){
				$check1 = '☑';
			}else{
				$check1 = '☐';
			}
			
			if($fetch_investor_data[0]->entity_type == '3'){
				$check3 = '☑';
			}else{
				$check3 = '☐';
			}
			
			if($fetch_investor_data[0]->entity_type == '5'){
				$check5 = '☑';
			}else{
				$check5 = '☐';
			}
			
			if($fetch_investor_data[0]->entity_type == '6'){
				$check6 = '☑';
			}else{
				$check6 = '☐';
			}
			
			
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td colspan="3">☐ Personal</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td colspan="3">'.$check3.' Trust</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td colspan="3">'.$check5.' 401k</td>';
			$lender_account_setup .= '</tr>';
			
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td colspan="3">'.$check1.' LLC / Corp</td>';
			$lender_account_setup .= '</tr>';
			
			if($fetch_investor_data[0]->investor_type == '3'){
				
				$self = '☑';
			}else{
				$self = '☐';
			}
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td colspan="3" style="margin-left:40px;">Registered State:&nbsp; '.$STATE_USA[$fetch_investor_data[0]->entity_res_state] .'</td>';
			$lender_account_setup .= '</tr>';
			
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td colspan="3">'.$self.' Self Directed IRA</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td colspan="3" style="margin-left:40px;">Self-Directed IRA Name:&nbsp; '.$fetch_investor_data[0]->directed_IRA_NAME .'</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td colspan="3" style="margin-left:40px;">Name of Custodian:&nbsp; '.$fetch_investor_data[0]->custodian .'</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td colspan="3" style="margin-left:40px;">Account #:&nbsp; '.$fetch_investor_data[0]->self_dir_account .'</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td colspan="3">'.$check6.' Other:</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '</table>';
			
			$lender_account_setup .= '<br style="page-break-before: always">';
			
			$lender_account_setup .= '<p style="text-decoration:none;"><strong>List Names of people required to sign:</strong></p>';
			
			$lender_account_setup .= '<table class="table" style="width:100%;vertical-align:top;">';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:25%;">Contact Name:</td>';
			$lender_account_setup .= '<td style="width:25%;">'.$c_lender_contact1_name .'</td>';
			$lender_account_setup .= '<td style="width:25%;">Title:</td>';
			$lender_account_setup .= '<td style="width:25%;">'.$c_lender_contact1_title .'</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:25%;">Contact Name:</td>';
			$lender_account_setup .= '<td style="width:25%;">'.$c_lender_contact2_name .'</td>';
			$lender_account_setup .= '<td style="width:25%;">Title:</td>';
			$lender_account_setup .= '<td style="width:25%;">'.$c_lender_contact2_title .'</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '</table>';
			
			$lender_account_setup .= '<p style="text-decoration:underline;"><strong>Servicing Information:</strong></p>';
			$lender_account_setup .= '<table class="table" style="width:100%;vertical-align:top;">';
			
			$fci_acct = $fetch_investor_data[0]->fci_acct ? $fetch_investor_data[0]->fci_acct : 'Not Applicable';
			
			$del_toro_acct = $fetch_investor_data[0]->del_toro_acct ? $fetch_investor_data[0]->del_toro_acct : 'Not Applicable';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:30%;">FCI Account Number:</td>';
			$lender_account_setup .= '<td style="width:70%;">'.$fci_acct .'</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:30%;">Del Toro Account Number:</td>';
			$lender_account_setup .= '<td style="width:70%;">'.$del_toro_acct.'</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '</table>';
			
			$lender_account_setup .= '<p style="text-decoration:underline;"><strong>Payment Option:</strong></p>';
			
			$lender_account_setup .= '<table class="table" style="width:100%;vertical-align:top;">';
			
			if($fetch_investor_data[0]->check_disbrusement == '1'){
				$Check = '☑';
				
			}else{
				$Check = '☐';
			}
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td colspan="4">'.$Check.' &nbsp; Check ($2 check fee for each disbursement)</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:25%;">Name on Check: </td>';
			$lender_account_setup .= '<td style="width:25%;">'.$fetch_investor_data[0]->name .'</td>';
			$lender_account_setup .= '<td style="width:25%;"></td>';
			$lender_account_setup .= '<td style="width:25%;"></td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:25%;">Street Address: </td>';
			$lender_account_setup .= '<td style="width:25%;">'.$fetch_investor_data[0]->mail_address.'</td>';
			$lender_account_setup .= '<td style="width:25%;">Unit#:</td>';
			$lender_account_setup .= '<td style="width:25%;">'.$fetch_investor_data[0]->mail_unit.'</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:25%;">City: </td>';
			$lender_account_setup .= '<td style="width:25%;">'.$fetch_investor_data[0]->mail_city.'</td>';
			$lender_account_setup .= '<td style="width:25%;">State:</td>';
			$lender_account_setup .= '<td style="width:25%;">'.$fetch_investor_data[0]->mail_state.'</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:25%;">Zip: </td>';
			$lender_account_setup .= '<td style="width:25%;">'.$fetch_investor_data[0]->mail_zip.'</td>';
			$lender_account_setup .= '<td style="width:25%;"></td>';
			$lender_account_setup .= '<td style="width:25%;"></td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '</table>';
				
				
			$lender_account_setup .= '<table class="table" style="width:100%;vertical-align:top;">';
				
			if($fetch_investor_data[0]->ach_disbrusement == '1'){
				$ach = '☑';
				
			}else{
				$ach = '☐';
			}	
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td colspan="2">'.$ach.' &nbsp; ACH Deposit (if ACH, please complete the information below)</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:30%;">Bank Name : </td>';
			$lender_account_setup .= '<td style="width:70%;">'.$fetch_investor_data[0]->bank_name .'</td>';
			$lender_account_setup .= '</tr>';
			
			// $lender_account_setup .= '<tr>';
			// $lender_account_setup .= '<td style="width:30%;">Bank Street Address : </td>';
			// $lender_account_setup .= '<td style="width:70%;">'.$fetch_investor_data[0]->bank_street .' '.$fetch_investor_data[0]->bank_unit.'</td>';
			// $lender_account_setup .= '</tr>';
			
			// $lender_account_setup .= '<tr>';
			// $lender_account_setup .= '<td style="width:30%;">Bank Street City : </td>';
			// $lender_account_setup .= '<td style="width:70%;">'.$fetch_investor_data[0]->bank_city .'</td>';
			// $lender_account_setup .= '</tr>';
			
			// $lender_account_setup .= '<tr>';
			// $lender_account_setup .= '<td style="width:30%;">Bank State : </td>';
			// $lender_account_setup .= '<td style="width:70%;">'.$fetch_investor_data[0]->bank_state .'</td>';
			// $lender_account_setup .= '</tr>';
			
			// $lender_account_setup .= '<tr>';
			// $lender_account_setup .= '<td style="width:30%;">Bank Zip : </td>';
			// $lender_account_setup .= '<td style="width:70%;">'.$fetch_investor_data[0]->bank_zip .'</td>';
			// $lender_account_setup .= '</tr>';
			
			$account = $this->config->item('account_type_option');
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:30%;">Account Type :</td>';
			$lender_account_setup .= '<td style="width:70%;">'.$account[$fetch_investor_data[0]->account_type] .'</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:30%;">Routing Number : </td>';
			$lender_account_setup .= '<td style="width:70%;">'.$fetch_investor_data[0]->routing_number .'</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:30%;">Account Number : </td>';
			$lender_account_setup .= '<td style="width:70%;">'.$fetch_investor_data[0]->account_number .'</td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '</table>';
			
			$lender_account_setup .= '<table>&nbsp;&nbsp;<br><br></table>';
			
			
			$lender_account_setup .= '<p>'.$fetch_investor_data[0]->entity_name.'</p>';
			
			$lender_account_setup .= '<table>&nbsp;<br></table>';
			
			$lender_account_setup .= '<table class="table" style="width:100%;vertical-align:top;">';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:50%;"><hr></td>';
			$lender_account_setup .= '<td style="width:50%;"></td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:50%;">'.$c_lender_contact1_name.' &nbsp; '.$c_lender_contact1_title.'</td>';
			$lender_account_setup .= '<td style="width:50%;"></td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '</table>';
			
			$lender_account_setup .= '<table>&nbsp;&nbsp;<br><br></table>';
			
			$lender_account_setup .= '<table class="table" style="width:100%;vertical-align:top;">';
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:50%;"><hr></td>';
			$lender_account_setup .= '<td style="width:50%;"></td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '<tr>';
			$lender_account_setup .= '<td style="width:50%;">'.$c_lender_contact2_name.' &nbsp; '.$c_lender_contact2_title.'</td>';
			$lender_account_setup .= '<td style="width:50%;"></td>';
			$lender_account_setup .= '</tr>';
			
			$lender_account_setup .= '</table>';
			
			$lender_account_setup .= '<br style="page-break-before:always">';
			
			echo $lender_account_setup;
			
		
	}
	
	// public function update_all_prefix()
	// {
		// $fetch_investor = $this->User_model->select_star('investor');
		// $fetch_investor = $fetch_investor->result();
		
		// foreach($fetch_investor as $row)
		// {
			// $rand_id = $this->randomPrefix();
			// $this->User_model->updatedata('investor',array('id'=>$row->id),array('talimar_lender'=>$rand_id));
		// }
		
		// echo 'Update All';
		
	// }

public function lender_delete_document(){
	$id = $this->input->post('id');

	$fetch_document = $this->User_model->select_where('lender_document',array('id'=>$id));
	$fetch_document = $fetch_document->result();
	if($this->aws3->deleteObject($fetch_document[0]->lender_document)){
		$delete = $this->User_model->delete('lender_document',array('id'=>$id));
	}
	echo 1;
	
}

public function update_lender_documents(){
			// upload document...
			
				$lender_id			=$this->input->post('lender_idd');
				$document_name			=input_sanitize($this->input->post('document_name'));

				if($_FILES){
				
					//first check document uploaded for borrower...
					$check_fci_query 	= $this->User_model->query("SELECT * FROM lender_document WHERE lender_id = '".$lender_id."' ");
					if($check_fci_query->num_rows() > 0)
					{
						$cnt = $check_fci_query->num_rows();
					}else{
						$cnt = 1;
					}
					$idd['id']=$this->input->post('lender_idd');
				$fetch_investor_data 		= $this->User_model->select_where('investor',$idd);
				$fetch_investor_data 		= $fetch_investor_data->result();
				 $l_name					= $fetch_investor_data[0]->name;

				 $lender_id			=$this->input->post('lender_idd');
				
				
					$folder = 'lender_documents/'.$lender_id.'/' ; //FCPATH
					/*if(!is_dir($folder))
					{
						mkdir($folder, 0777, true);
						chmod($folder,0777);
					}*/
					
					
					foreach($_FILES['lender_upload_docc']['name'] as $key => $row_data){
						/* echo "<pre>";
							print_r($row_data);
						echo "</pre>"; */
						if($row_data)
						{
							
							$ext = strtolower(pathinfo($row_data, PATHINFO_EXTENSION));
							// only docx files are allowed to uploaded!
							// if($ext == 'docx' || $ext == 'doc')
							if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc')
							{
									$cnt++;
								// $new_name 		= 'borrower-'.$borrower_id.'_'.$cnt.'.'.$ext;
								// $new_name 		= basename($row_data);
								$new_name 		= basename($row_data);
								$my_file_name 	= str_replace('.'.$ext, '_'.time().'.'.$ext, $new_name);
								$target_dir 	= $folder.time().$my_file_name;
									
								$lender_document['lender_id']			=	$lender_id;
								$lender_document['name']					=	$l_name;
								$lender_document['lender_document']		=	$target_dir;
								$lender_document['user_id']				=	$this->session->userdata('t_user_id');
								$lender_document['document_name']		=	$document_name;


									
								//if(move_uploaded_file($_FILES["lender_upload_docc"]["tmp_name"][$key], $target_dir) === TRUE )
								$attachmentName=$this->aws3->uploadFile($target_dir,$_FILES["lender_upload_docc"]["tmp_name"][$key]);
								if($attachmentName){
									$this->User_model->insertdata('lender_document',$lender_document);
								}
							}
							/* else{
								$this->session->set_flashdata('error', ' Only docx/doc format are allowed to upload !');
								redirect(base_url().'borrower_data/'.$borrower_id,'refresh');
							} */
							$this->session->set_flashdata('lender_document_hit', '1');
						}
					}
					
					$this->session->set_flashdata('success', ' Document uploaded !');
					 redirect(base_url().'investor_view/'.$lender_id.'#lender_document','refresh');
						
					
				}
				// update existing document names...
				if($this->input->post('old_filename')){
					
					$old_filename 			= array_values(array_filter($this->input->post('old_filename')));
					$new_filename 			= array_values(array_filter($this->input->post('new_filename')));
					$borrower_document_id 	= array_values(array_filter($this->input->post('borrower_document_id')));
					
					if($borrower_document_id){
						
						// first check file exist in folder or not....
						foreach($borrower_document_id as $key => $old_borrower_document_id){
							if ( isset($new_filename[$key])) {
								
								$folder 			= 'borrower_documents/'.$borrower_id.'/' ; //FCPATH

								$ext = strtolower(pathinfo($old_filename[$key], PATHINFO_EXTENSION));
								$new_filenamect = str_replace('.'.$ext, '_'.time().'.'.$ext, $folder.$new_filename[$key]);
								$old_target_dir 	= str_replace(S3_VIEW_URL, '', $old_filename[$key]);
								
								$new_target_dir 	= $new_filenamect;

								/*$folder 			= 'borrower_documents/'.$borrower_id.'/' ; //FCPATH
								$old_target_dir 	= $folder.$old_filename[$key];
								// $new_target_dir 	= $folder.$new_filename[$key].'.'.$ext;
								$new_target_dir 	= $folder.$new_filename[$key];*/
										
								
								//if(file_exists($old_target_dir)){
									$attachmentName=$this->aws3->copyObject($old_target_dir,$new_target_dir);
									//if(rename($old_target_dir,$new_target_dir)){
									if($attachmentName){
										$this->aws3->deleteObject($old_target_dir);											
										// update  old filename with new file name int the "borrower_document" table...
											
										$this->User_model->query("UPDATE borrower_document SET borrower_document = '".$new_target_dir."' WHERE id = '".$old_borrower_document_id."' ");
									}
								/*}else{
									// echo 'no file';
								}*/
							
							}
						}
						
						
					}
					$this->session->set_flashdata('success', ' Document uploaded !');
					
				}
			}


public function upload_lender_documents(){
			// upload document...
			
				$lender_id			=$this->input->post('lender_iddd');

				if($_FILES){
				
					//first check document uploaded for borrower...
					$check_fci_query 	= $this->User_model->query("SELECT * FROM lender_document WHERE lender_id = '".$lender_id."' ");
					if($check_fci_query->num_rows() > 0)
					{
						$cnt = $check_fci_query->num_rows();
					}else{
						$cnt = 1;
					}
					$idd['id']=$this->input->post('lender_iddd');
				$fetch_investor_data 		= $this->User_model->select_where('investor',$idd);
				$fetch_investor_data 		= $fetch_investor_data->result();
				 $l_name					= $fetch_investor_data[0]->name;

				 $lender_id			=$this->input->post('lender_iddd');
				
				
					$folder = 'lender_documents/'.$lender_id.'/' ; //FCPATH
					/*if(!is_dir($folder))
					{
						mkdir($folder, 0777, true);
						chmod($folder,0777);
					}*/
					
					
					foreach($_FILES['lenderr_upload']['name'] as $key => $row_data){
						/* echo "<pre>";
							print_r($row_data);
						echo "</pre>"; */
						if($row_data)
						{
							
							$ext = strtolower(pathinfo($row_data, PATHINFO_EXTENSION));
							// only docx files are allowed to uploaded!
							// if($ext == 'docx' || $ext == 'doc')
							if($ext == 'pdf')
							{
									$cnt++;
								// $new_name 		= 'borrower-'.$borrower_id.'_'.$cnt.'.'.$ext;
								// $new_name 		= basename($row_data);
								$new_name 		= basename($row_data);
								$my_file_name 	= str_replace('.'.$ext, '_'.time().'.'.$ext, $new_name);
								$target_dir 	= $folder.$my_file_name;
								//$target_dir 	= $folder.$new_name;
									
								$lender_document['lender_id']			=	$lender_id;
								$lender_document['name']					=	$l_name;
								$lender_document['lender_document']		=	$target_dir;
								$lender_document['user_id']				=	$this->session->userdata('t_user_id');
									
								//if(move_uploaded_file($_FILES["lenderr_upload"]["tmp_name"][$key], $target_dir) === TRUE )
								$attachmentName=$this->aws3->uploadFile($target_dir,$_FILES["lenderr_upload"]["tmp_name"][$key]);
								if($attachmentName){
									$this->User_model->insertdata('lender_document',$lender_document);
								}
							}
							/* else{
								$this->session->set_flashdata('error', ' Only docx/doc format are allowed to upload !');
								redirect(base_url().'borrower_data/'.$borrower_id,'refresh');
							} */
							$this->session->set_flashdata('lender_document_hit', '1');
						}
					}
					
					$this->session->set_flashdata('success', ' Document uploaded !');
		
						redirect(base_url().'investor_view/'.$lender_id,'refresh');
					
				}
				// update existing document names...
				if($this->input->post('old_filename')){
					
					$old_filename 			= array_values(array_filter($this->input->post('old_filename')));
					$new_filename 			= array_values(array_filter($this->input->post('new_filename')));
					$borrower_document_id 	= array_values(array_filter($this->input->post('borrower_document_id')));
					
					if($borrower_document_id){
						
						// first check file exist in folder or not....
						foreach($borrower_document_id as $key => $old_borrower_document_id){
							if ( isset($new_filename[$key])) {
								
								$folder 			= 'borrower_documents/'.$borrower_id.'/' ; //FCPATH

								$ext = strtolower(pathinfo($old_filename[$key], PATHINFO_EXTENSION));
								$new_filenamect = str_replace('.'.$ext, '_'.time().'.'.$ext, $folder.$new_filename[$key]);
								$old_target_dir 	= str_replace(S3_VIEW_URL, '', $old_filename[$key]);

									
								
								/*$folder 			= FCPATH.'borrower_documents/'.$borrower_id.'/' ;
								$old_target_dir 	= $folder.$old_filename[$key];
								// $new_target_dir 	= $folder.$new_filename[$key].'.'.$ext;
								$new_target_dir 	= $folder.$new_filename[$key];*/
								
								$new_target_dir 	= $new_filenamect;
								
								//if(file_exists($old_target_dir)){
									
									//if(rename($old_target_dir,$new_target_dir)){
									$attachmentName=$this->aws3->copyObject($old_target_dir,$new_target_dir);
									if($attachmentName){
									$this->aws3->deleteObject($old_target_dir);
											
										// update  old filename with new file name int the "borrower_document" table...
											
										$this->User_model->query("UPDATE borrower_document SET borrower_document = '".$new_target_dir."' WHERE id = '".$old_borrower_document_id."' ");
									}
								/*}else{
									// echo 'no file';
								}*/
							
							}
						}
						
						
					}
					$this->session->set_flashdata('success', ' Document uploaded !');
					
				}
			}


	 public function investor_document_read(){
		
		 
		// fetch filename from "borrower_document" table...
		$sql = $this->User_model->query("SELECT * FROM lender_document WHERE id = '".$this->uri->segment(3)."' ");
		$fetch_name = $sql->row();
		
		$filename = $fetch_name->lender_document;

												 
		// Let the browser know that a PDF file is coming.
		
		/* header("Content-type: application/pdf");
		header('Content-Disposition: inline; filename="' . $filename . '"');
		header("Content-Length: " . filesize('borrower_document-'.$filename));
			*/
		header('Content-Description: File Transfer');
		header('Content-Type: application/pdf');
		header('Content-Disposition: attachment; filename="'.basename($filename).'"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($filename));										 
		// Send the file to the browser.

		readfile($filename);

		exit; 
	} 
	public function accountChecklist(){
		$search_investor_id						= $_POST['search_investor_id'];
		$search_investor_name					= $_POST['search_investor_name'];
		$data['search_investor_id']				= $search_investor_id;
		$data['search_investor_name']			= $search_investor_name;
		$lender_account_checklist 				= $this->config->item('lender_account_checklist');
		$data['lender_account_checklist']		= $lender_account_checklist;
		$checkListQuery="SELECT * FROM investor_account_checklist where investor_id ='$search_investor_id' ";
		$checkListRespond=$this->User_model->query($checkListQuery);
		$checkListResult=$checkListRespond->result();
		$data['checkListResult']=$checkListResult;
		$this->load->view('investor_data/modal_account_checklist', $data);
	}
	function add_account_checklist(){
		if(!empty($_POST)){
			$lender_account_checklist 				= $this->config->item('lender_account_checklist');
			$checklist_investor_id					= $_POST['checklist_investor_id'];
			foreach ($lender_account_checklist as $key => $row) {
				$checklist_complete="";
				$checklist_not_applicable="";
				if(!empty($_POST['checklist_complete_'.$key])){
					$checklist_complete=1;
				}
				else if(!empty($_POST['checklist_not_applicable_'.$key])){
					$checklist_not_applicable=1;
				}
				if(empty($_POST['checklist_id_'.$key])){
					$accountCheckList=array(
											"investor_id"	=> $checklist_investor_id,
											"hud"			=> $key,
											"items"			=> $_POST['checklist_name_'.$key],
											"completed"		=> $checklist_complete,
											"not_applicable"=> $checklist_not_applicable
										);
					$this->User_model->insertdata('investor_account_checklist',$accountCheckList);
				}else{
					$accountCheckList=array(
											"investor_id"	=> $checklist_investor_id,
											"hud"			=> $key,
											"items"			=> $_POST['checklist_name_'.$key],
											"completed"		=> $checklist_complete,
											"not_applicable"=> $checklist_not_applicable
										);
					$update_id['id'] = $_POST['checklist_id_'.$key];
					$this->User_model->updatedata('investor_account_checklist',$update_id,$accountCheckList);
				}
			}
			$this->session->set_flashdata('success', ' Account Checklist Updated Successfully !');		
			redirect(base_url().'investor_view/'.$checklist_investor_id,'refresh');
		}
	}
		 

}


