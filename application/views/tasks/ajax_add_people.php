<div class="col-md-4" id="people_row_<?php echo $people_row_count;?>" style="width: 300px;margin-left: 60px;">
	<label>Add People</label>
	<select class="form-control chosen_<?php echo $people_row_count;?>" name="add_people[]" id="add_people_<?php echo $people_row_count;?>">
		<option value="" >Select One</option>
		<?php
		if(!empty($all_users_data)){
			foreach ($all_users_data as  $value1) {
			?>
				<option value="<?php echo $value1->id;?>" ><?php echo $value1->fname.' '.$value1->middle_name.' '.$value1->lname;?></option>									
			<?php
			}	
		}
		?>										
	</select>
	<span>
		<a title="Add People" onclick="delete_people_select('<?php echo $people_row_count;?>');" class="btn btn-sm btn-danger plus_icon_add_more" >
			<i class="fa fa-trash"></i>
		</a>
	</span>
</div>