<style>

	span img {
    width: 30px;
}

.bro,td,th {
    border: none !important;
}
#personal_notes{
	border: 1px solid white !important;
}
.main-left-div-res{
    width: 100%;
    margin-left: 0% !IMPORTANT;
    float: right;
    margin-right: 0%;
}
.main-left-div-res .main-title .col-md-3, .title .col-md-3 {
	width:100% !important;
    margin-bottom: 10px;
}
.main-left-div-res .row.borrower_data_row .col-md-3 {
    width: 18%;
}
.main-left-div-res .row.borrower_data_row .col-md-8 {
    width: 80% !important;
}
.main-left-div-res .row.borrower_data_row div
.main-left-div-res .borrower_data_row {
    padding: 0px !important;
}

.main-left-div-res .borrower_data_row {
    padding: 0px !important;
    width: 100%;
	margin-top: 5px;
}
.main-left-div-res .col-md-1 {
    width: 13%;
}
.main-left-div-res .col-md-4 {
    width: 48.6%;
}
.main-left-div-res .new-notes,.main-left-div-res .saved-notes {
    width: 98% !important;
    float: left;
    border-right: 0px solid #eee;
}
.main-left-div-res  .new-notes .title {
    padding: 5px 0px !important;
}

.main-left-div-res .notes_class {
    float: left;
    width: 25% !important;
}

.main-left-div-res .new-notes .notes_class .btn-group.bootstrap-select {
      max-width: 125px;
}
.main-left-div-res .new-notes .spc-loan-div.notes_class .btn-group.bootstrap-select {
    max-width: 175px;
}

.main-left-div-res .row.borrower_data_row label.form-control{
	font-weight: normal !important;

}
.main-left-div-res .saved-notes .lender_data_row.three_btns {
    padding: 2px 30px !important;
}
.main-left-div-res .row.inline-label2 .spc-loan-div {
    float: left;
    margin-left: 0px;
}
.main-left-div-res .spc-loan-div .history_label {

    width: 52% !important;
}

.main-left-div-res .spc-loan-div{
	width:30% !important;
}

.main-left-div-res .borrower_data_row {
    padding:0px !important;
}
.main-left-div-res .dates_class {

}


.main-left-div-res .dates_class {
   width: 115px !important;
}

.main-left-div-res .history_label {
    width: 115px;
}

.row.inline-label2 div {

    margin-left: 2%;
}
.selected_loans .btn-group.bootstrap-select {
    width: 125px !important;
}

#fetch_notes_date {
	    width: 106px;

}
div#p_loan {
    margin-left: -16px !important;
}
.row.inline-label2 div label {
    float: left;
    margin-right: 11px;
    line-height: 20px;
}

.row.inline-label2 div input {
    /* float: right; */
    width: initial;
    /* max-width: 243px; */
}

.new-notes .title{
    padding: 5px 60px !important;
}
.lender_data_row.main_actions {
	float: right;
    width: 100%;
    margin-right: 120px;
    margin-top: 8px;
}
.lender_data_row .main_actions {
	padding: 0px !important;
}
.lender_data_row.three_btns {
    padding: 2px 90px !important;
}
.lender_data_row.three_btnss {
    padding: 15px 425px !important;
}

.tags_display .form-control {
    font-size: 13px;
}

.three_btns {
	float:right !important;
}
.three_btns {
	    float: left;
    margin-left: 440px;
    margin-top: 15px;
}

.main-right-div,.main-left-div,.new-notes,.saved-notes{
	float:left
}



.row.contact_tags_row {
    padding-left: 40px;
	padding-bottom: 15px;
}
.contact_tags_row .col-md-8{
	padding:5px;
}
.contact_tags_row .col-md-3{
	padding:5px;
}

.main-left-div{
	width:70%;
	border-right: 1px solid #eee;


}
.main-right-div{
	width:30%
}
.right-div-label label{
	height:initial !important;
}
.row.borrower_data_row label {
    font-weight: 400;
}

.main_select_div{
	float: right;

}
.tag_labels label {
    float: left;
    padding: 10px;
	    border: 1px solid silver;
    border-radius: 3px;

}
.tag_labels {
    float: left;
	margin-left:10px;

}
.tags_display{
	float: left;
    width: 82% !important;


}
.left_fields{
	float:right !important;
}

#borrower_data .borrower_data_row ,#lender_data .lender_data_row {

	    padding: 11px 2px !important;
}
#table_trust_deed, #table_loan{
	margin-left: 15px;
}
.tab-pane{
	margin-bottom:20px;
}
.user_content{
	padding: 10px 25px;
}
.btn-group.bootstrap-select{
	width:162px !important;
}
.search_contact {

	float:right;
	margin-top: 15px;
	margin-right: 150px;
}

label.title_history {
    line-height: 30px !important;
	font-weight:600 !important;
}
.history_label {
    width: initial;
}
#selctfrm .btn-group.bootstrap-select{
	width:240px !important;
}
.same_check #uniform-same_mailing_address{
	width:7% !important;
	float:left;
}
.inline-label .col-md-3 {
    width: 15% !important;
}
.inline-label2 .col-md-3 {
    width: 15% !important;
}
.inline-label .col-md-3 label {
    line-height: 30px;
	font-weight:600 !important;
}
.stats1, .stats2{
	float: left;
	border: 1px solid;
	width: 45% !important;
}
.b_right {
	margin-right: 15px;
}
.notes_class .btn-group.bootstrap-select {
    max-width: 100px;
}
.stats2 {
    margin-left: 80px;
}
.stats1_div label,.stats2_div label{
   padding-left: 0px !important;
}

.bs-searchbox{
	width:100% !important;
}
.btn .caret {
    margin-left: 20px !important;
}
.relationship_div .btn.dropdown-toggle.selectpicker.btn-default{
	float: left;

	margin-top: 10px !important;
}
.relation_labels{
	margin-bottom: 5px;
	float:left;
	width:100%;
}
.show_acc_to_permission{

	display:block ;
}
.hide_acc_to_permission{

	display:none ;

}
.row.borrower_data_row.history_row div {
    width: 25%;
}
.history_label{
	font-weight:400 !important;
}
.history_action{
	float:right;
	margin-top:6px;
}
.contact_document_lists li{
	list-style: none;
	padding: 5px;
	font-size: 14px;
}

.doc-div{
	margin-bottom: 15px;
}


.main-left-div-res .col-md-4 label,.main-left-div-res .col-md-6 label{
	line-height:30px;
}
.main-left-div-res .inline-label .col-md-3 {
    width: 15% !important;
}
.main-left-div-res .inline-label .col-md-4 {
    width: 35% !important;
}
@media only screen and (min-width: 992px) and (max-width: 1199px) {
.main-left-div-res .dates_class {
    width: 99px !important;
}
.main-left-div-res .new-notes .notes_class .btn-group.bootstrap-select {
    max-width: 114px !important;
}
.main-left-div-res .new-notes .spc-loan-div.notes_class .btn-group.bootstrap-select {
    max-width: 132px !important;
}

}

@media only screen and (min-width: 768px) and (max-width: 992px) {
.main-left-div-res .new-notes .notes_class .btn-group.bootstrap-select {
    max-width: 110px !important;
}
.main-left-div-res .dates_class {
    width: 120px !important;
}
.main-left-div-res .new-notes .spc-loan-div.notes_class .btn-group.bootstrap-select {
    max-width: 175px !important;
}
}




@media only screen and (max-width: 767px){
	.row.address-fields {
		padding: 5px 60px !important;
	}
	.page-header .page-header-top .top-menu {
    display: block;
    clear: none;
    margin-top: 14px;
}
	.page-header .page-header-top .menu-toggler {
    display: block;
}
.lender_data_row.main_actions {
    float: right;
    width: 100%;
    margin-right: 0;
    padding-top: 0px;
    margin-top: 0px;
}
.margin-top-10 {
    margin-top: 0px !important;
}
.main_select_div {
    float: right;
    width: 100% !important;
}
.search_contact {
    float: right;
    margin-top: 0px !important;
    margin-right: 0px !important;
}
.page-head .page-title {
    padding: 6px 0 !important;
}
.main-left-div-res .row.borrower_data_row .col-md-8 {
    width: 100% !important;
}
.main-left-div-res .row.borrower_data_row div {
    float: left;
    width: 100% !important;
    margin-bottom: 0px !important;
}
 .main-left-div {
    width: 100%;
    border-right: 0px solid #eee !important;
}
.main-left-div-res form#selctfrm {
    margin-bottom: 0px !important;
}
.main-left-div-res .row.address-fields {
    padding: 5px 14px !important;
}
.main-left-div-res .col-md-1 {
    width: 100%;
}
.main-left-div-res .tags_display {
    float: left;
    width: 100% !important;
}
.main-left-div-res .col-md-4 {
    width: 100%;
    float: left;
}

.main-left-div-res .new-notes {
    width: 100%;
    float: left;
    border-right: 0px solid #eee;
}
.main-left-div-res .row.inline-label2 div label {
    float: left;
    margin-right: 11px;
    line-height: 20px;
    width: 100%;
}
.main-left-div-res .dates_class {
    width: 100% !important;
}
.main-left-div-res .new-notes .notes_class .btn-group.bootstrap-select {
    max-width: 100% !important;
}
.main-left-div-res .row.inline-label2 .spc-loan-div {
    float: left;
    margin-left: 2% !important;
}
.main-left-div-res .spc-loan-div .history_label {
    width: 100% !important;
}
.main-left-div-res .history_action .col-md-6{
	padding-right:0px !important;
}
.main-left-div-res .col-md-9 {
		padding-right:0px !important;
		padding-left:0px !important;
}
.main-right-div {
    width: 100%;
}
.main-left-div-res .row.borrower_data_row.inline-label .col-md-3, .main-left-div-res .row.borrower_data_row.inline-label .col-md-4, .main-left-div-res .row.borrower_data_row.inline-label .col-md-6{
    width: 42% !important;
    float: left;
}
.notes_class .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    width: 100% !important;
}
}

@media (max-width: 469px) {


}
.main-left-div-res .col-md-4 {
    width:28% !important;
}
textarea.border-none {
    border: none;
}
div#page-viewcontact .main-left-div h3 {
    background: #eeeeee;
    padding: 8px 10px;
    font-size: 17px;
    font-weight: 500;
}
form.history-filter {
    padding: 0px !important;
    margin: 0px !important;
}
.row.borrower_data_row.borrower_textarea_div input.pull-right {
    margin: 6px 14px !important;
}
label#label_notes_date, label#label_notes_type, label#label_loan_id {
    border: none;
}
.row11 {
    border: 1px solid #eeeeee;
    padding: 10px 0px;
}
form.history-filter {
    padding: 0px 0px 10px 0px !important;
    margin: 0px !important;

}
.history_action form {
    border: none !important;
}



span.a>h4{

font-family: sans-serif;
}

div#contactActivetable_filter {
    float: right;
}

.col-md-3.abc {
    margin-top: 20px;
}
strong {
    font-weight: 600;
}
div#fetch_select_talimar_loan_chosen {
    width: 181px !important;
}

div#model_left_align {
    margin-left: 7px !important;
}
.alert-success {

    border-color: #d6e9c6;
    color: #009688;
    background-color: rgba(139, 195, 74, 0.48);
}

.margin_checkbox{
	margin-bottom: 4px;
}

#hardmoney .addhmspace{
    margin-bottom: 10px;
}

#hardmoney label{
    font-weight: 700 !important;
}

.margin_checkbox{
    margin-bottom: 4px;
}

div.checker, div.checker span, div.checker input {
   /* width: 19px;*/
    height: 19px;
}
input[type="checkbox"] {
  /* width: 19px;*/
    display: inline-block;
    width: 19%;
    height: 19px;
}
input.amount_format.selectpicker {
    border: #e5e5e5 solid 1px;
    height: 36px;
    border-radius: 5px;
    padding: 15px;
}
.row > .rowh4{
        	background-color:#efefef;
        	padding: 10px; 
        	font-family: sans-serif;
        }
div.checkId > .checker{
			width: 6%!important;
		}
		div.checkId1 > .checker{
			width: 10%!important;
		}
        /*20-07-2031*/
        label#ab_follow_up_task,label#ab_follow_up_date,label#ab_follow_up_status{
            border: none;
        }
        .notes_class.row_follow_up_date,.notes_class.row_follow_up_status{
            width: 29% !important;
        }
        .notes_class.row_notes_type{
            width: 31% !important;
        }
        .row.borrower_data_row.inline-label2.min_row {
    padding-top: 13px !important;
}
form.history-filter {
    border-bottom: 1px solid gray;
}
form.history-filter {
    padding-top: 15px !important;
}
.col-md-9.note_desc, .note_enter_by {
    padding-bottom: 10px;
}
#label_dta_css label{
    font-weight: 1000 !important;
}
</style>
<div class="page-container">
	<div class="container" id="page-viewcontact">
		<div class="tab-pane">
			<!--------------------MESSEGE SHOW-------------------------->
			<?php if ($this->session->flashdata('error') != '') {?>
				<div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error'); ?></div><?php }?>
			<?php if ($this->session->flashdata('success') != '') {?>
				<div id='success'><i class='fa fa-thumbs-o-up'></i> <?php echo $this->session->flashdata('success'); ?></div><?php }?>
			<div id="chkbox_err"></div>
			<div class="page-head">
				<div class="main_select_div" >
						<div class="row margin-top-10">
							<div class="lender_data_row main_actions">
								<div class="form-group">
								  	<div class="">
										<a href="/Pranav/Talimar/add_task/<?php echo $selected_data->id;?>"><button type="button" class="btn btn-success"  >Edit</button></a>
									</div>
								</div>
							</div>
						</div>
					<!------------------End Buttons Section--------------------------->
				</div>
			</div>
			<!-- END PAGE HEAD -->
			<div class="page-container">

				<!-- BEGIN PAGE BREADCRUMB -->
				<ul class="page-breadcrumb breadcrumb hide">
					<li>
						<a href="#">Home</a><i class="fa fa-circle"></i>
					</li>
					<li class="active">
						 Dashboard
					</li>
				</ul>
				<!-- END PAGE BREADCRUMB -->
				<?php
					$ids = $selected_data->id;
				   	$contact_id	= $selected_data->contact_id; 
				   	$fetch_user_id = $this->User_model->query("SELECT fname,middle_name,lname FROM user WHERE id = '$contact_id' ");
				   	$all_users_data = $fetch_user_id->result();
				   	if(!empty($all_users_data)){
				   		$contact_name=$all_users_data[0]->fname." ".$all_users_data[0]->fname." ".$all_users_data[0]->lname;
				   	}else{
				   		$contact_name="";
				   	}
				   	
				   
				     
				   	$contact_task = $selected_data->contact_task; 
				   	$originalDate = $selected_data->contact_date;
				   	if(!empty($originalDate)){
				   			$contact_date = date("m-d-Y", strtotime($originalDate));
			   		}else{
			   			$contact_date ='';
			   		}
				    $due_date = $selected_data->due_date;
				   	if(!empty($due_date)){
				   			$due_date = date("m-d-Y", strtotime($due_date));
			   		}else{
			   			$due_date ='';
			   		}
					
				   	$task_notes = $selected_data->task_notes;
				   	$contact_status = $selected_data->contact_status;
				   	$contact_user = $selected_data->contact_user;
				   	$t_user_id = $selected_data->t_user_id;

				   	$due_date='';
				   	$primary_phone = $selected_data->primary_phone;
				   	$primary_mail = $selected_data->primary_mail;
				   	$task_type = $selected_data->task_type;
				   	$add_people = $selected_data->add_people;
				   	$add_people_array = explode(",",$selected_data->add_people);
				   	$add_people_string="";

				   	foreach ($add_people_array as  $value) {

				   		$fetch_user_id = $this->User_model->query("SELECT fname,middle_name,lname FROM user WHERE id = '$value' ");
				   		$all_users_data = $fetch_user_id->result();
				   		if(empty($add_people_string)){
				   			$add_people_string.=",";
				   		}
				   		$add_people_string.=$all_users_data[0]->fname." ".$all_users_data[0]->fname." ".$all_users_data[0]->lname;
				   	}
				   	$result = $selected_data->result;
				   	$date_completed = date("m-d-Y", strtotime($selected_data->date_completed));

					
				?>
				<div class="main-left-div">
				   	<div class="main-left-div-res" id="label_dta_css">
						<div class="main-title row borrower_data_row" >
							<div class="col-md-3">
	                            <div class=""></div>
								<h3>Task Information</h3>
							</div>
						</div>
						<div class="row ">
							<div class="col-md-2">
								<label >Status:</label>
							</div>
							<div class="col-md-4">
								<label style="font-weight:normal !important;" ><?php echo $contact_status; ?></label>
							</div>
							<div class="col-md-2">
								<label >Date Entered:</label>
							</div>
							<div class="col-md-4">
								<span ><?php echo $contact_date;?></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<label >Due Date:</label>
							</div>
							<div class="col-md-4">
								<label style="font-weight:normal !important;" ><?php echo $due_date;?></label>
							</div>
							<div class="col-md-2">
								<label >Task Type:</label>
							</div>
							<div class="col-md-4">
								<label style="font-weight:normal !important;" ><?php echo $task_type;?></label>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<label >Contact Name:</label>
							</div>
							<div class="col-md-4">
								<label style="font-weight:normal !important;" ><?php echo $contact_name;?></label>
							</div>
							<div class="col-md-2">
								<label >Primary Phone:</label>
							</div>
							<div class="col-md-4">
								<label style="font-weight:normal !important;" ><?php echo $primary_phone;?></label>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<label >Primary E-Mail:</label>
							</div>
							<div class="col-md-10">
								<label style="font-weight:normal !important;" ><?php echo $primary_mail;?></label>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<label >Add People:</label>
							</div>
							<div class="col-md-10">
								<label style="font-weight:normal !important;" ><?php echo $add_people_string;?></label>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<label >Description:</label>
							</div>
							<div class="col-md-10">
								<label style="font-weight:normal !important;" ><?php echo $task_notes;?></label>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<label >Result:</label>
							</div>
							<div class="col-md-10">
								<label style="font-weight:normal !important;" ><?php echo $result;?></label>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<label >Date Completed:</label>
							</div>
							<div class="col-md-10">
								<label style="font-weight:normal !important;" ><?php echo $date_completed;?></label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
