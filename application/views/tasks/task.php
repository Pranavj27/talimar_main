<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
<style type="text/css">
	.row.margin-top-10 {
	    margin-top: 144px !important;
	}
	section{
		padding: 11px 181px;
	    margin: 0px;
	}
  	.chosen-container-single .chosen-single, .pro_Head .chosen-container-single .chosen-single, .page-head .chosen-container-single .chosen-single{
  		height: 44px !important;
  	}
	input,select{
	  	height: 44px !important;
	}
	p.error {
	    height: 5px;
	}
	.select2-container .select2-selection--single{
		height: 44px !important;
    	text-align: inherit;
    	padding: 6px;
	}
	span.select2-selection.select2-selection--single {
	    height: 44px !important;
	    text-align: inherit;
	    padding: 6px;
	}
	.plus_icon_add_more{
		margin-top: -34px;
	    margin-left: 280px;
	    width: 37px;
	    height: 25px;
	}
	p.error{
		color: red;
		margin: 7px;
	}
	.dlt_tsk,.back{
	    margin-left: 10px;
	}
	.mn{
		text-align: end;
	}
	button.btn.blue.submit_frm {
	    margin-left: 10px;
	}
	div#success {
		margin: 19px 15px 8px 0px;
	}
	span.result_counter,span.desc_counter {
	    position: inherit;
	    float: right;
	    align-items: baseline;
	    top: -23px;
	    right: 13px;
	}
	.col-md-6.mn {
	    margin-top: 3px;
	}
</style>
<?php 
	$ids =$contact_id= 	$contact_task =	$contact_date =$task_notes = $contact_status =
   	$contact_user =  	$t_user_id = 	$due_date = 	$primary_phone =  	$primary_mail =    	$task_type =  	$add_people = 	$result = 	$date_completed =$note_title='';
	if($val > 1){
		
		// echo '<pre>'; print_r($selected_data);echo '</pre>';
		if(empty($selected_data->id)){
			echo "Task has been Deleted";
			die();
		}
		$ids = $selected_data->id;

	   	$contact_id	= $selected_data->contact_id; 
	   	if(!empty($contact_data)){
	   			$contact_name = $contact_data->contact_firstname.' '.$contact_data->contact_middlename.' '.$contact_data->contact_lastname;
	   		}else{
	   			$contact_name ="";
	   		}
	   
     
	   	$contact_task = $selected_data->contact_task; 
	   	$originalDate = $selected_data->contact_date;
	   	if(validateDate($originalDate) == true){
	   		$contact_date = date("m-d-Y", strtotime($originalDate));
	   	}else{
	   		$contact_date = '';
	   	}
	
	   	$task_notes = $selected_data->task_notes;
	   	$contact_status = $selected_data->contact_status;
	   	$contact_user = $selected_data->contact_user;
	   	$t_user_id = $selected_data->t_user_id;

	   	if(validateDate($selected_data->due_date) == true){
	   		$due_date = date("m-d-Y", strtotime($selected_data->due_date));
	   	}else{
	   		$due_date = '';
	   	}

   	
	   	$primary_phone = $selected_data->primary_phone;
	   	$primary_mail = $selected_data->primary_mail;
	   	$task_type = $selected_data->task_type;
	   	$add_people = $selected_data->add_people;
	   	$add_people_array = explode(",",$selected_data->add_people);
	   	$note_title=$selected_data->note_title;
	   	$result = $selected_data->result;
	   	if(validateDate($selected_data->date_completed) == true){
	   		$date_completed = date("m-d-Y", strtotime($selected_data->date_completed));
	   	}else{
	   		$date_completed = '';
	   	}
	}
	function validateDate($date, $format = 'Y-m-d')
	{
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) === $date;
	}
?>
<div class="page-container">
	<div class="container" >
		<!--------------------MESSEGE SHOW-------------------------->
		<?php if($this->session->flashdata('error')!=''){  ?>
			<div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error');?></div><?php } ?>
		<?php if($this->session->flashdata('success')!=''){  ?>
			<div id='success'><i class='fa fa-thumbs-o-up'></i> <?php echo $this->session->flashdata('success');?></div><?php } ?>
		<div id = "chkbox_err"></div>		 
		<!-------------------END OF MESSEGE-------------------------->			
		<section>			
			<form name="add_task_form" id="add_tsk_form" method="POST" action="<?php echo base_url();?>Task/task_form/<?php echo $val;?>" >
				<div class="row"  style="margin-top: 33px;margin-bottom: 33px; ">
					<div class="col-md-6">
						<?php 
						if($val > 1){?>
							<h3 style="margin-top: 6px"><b>Edit Task</b></h3>
						<?php	
						}else{ ?>
							<h3 style="margin-top: 6px"><b>Add New Task</b></h3>
						<?php	
						}
						?>
					</div>
					<div class="col-md-6 mn">
						<?php 
						if($val > 1){
							?>						
							<a href="/Pranav/Talimar/Task/delete_task/<?php echo $val;?>" class="dlt_tsk btn blue">Delete</a>
						<?php 
						} ?>
						<?php 
						if($view_content_page==true){
						?>						
							<a href="/Pranav/Talimar/add_task/<?php echo $val;?>" class="dlt_tsk btn blue">Edit</a>
						<?php 
						} ?>						
						<a href="<?php echo base_url('task_view_list');?>" class="back btn blue">Back</a>
						<?php 
						if($view_content_page!=true){
						?>	
							<button type="submit" name="form_button" value="save" class="btn blue submit_frm" >Save</button>
						<?php 
						} ?>						
					</div>	
				</div>
				<div class="row">
					<div class="m">						
						<div class="col-md-4">
							<label  for="textinput">Status</label>
							<select class="form-control" name="status" id="status" <?php if($view_content_page==true){ ?> disabled="disabled" <?php }?> >
								<option value="" >Select One</option>
								<option value="Active" <?php echo ($contact_status == "Active")?'selected':'';?>>Active</option>
								<option value="Complete" <?php echo ($contact_status == "Complete")?'selected':'';?>>Complete</option>					
							</select>
							<p class="error"></p>
						</div>						
					</div>
				</div>
				<div class="row">
					<div class="m">						
						<div class="col-md-4">
							<label  for="textinput">Date Entered</label>
								<input type="text"  placeholder = "MM-DD-YYYY" id="date_entered" class="date_entered  form-control"  value="<?php echo $contact_date;?>" name = "date_entered[]" autocomplete="off" <?php if($view_content_page==true){ ?> readonly="readonly" <?php }?>/> 
								<p class="error"></p>
						</div>						
					</div>
					<div class="m">						
						<div class="col-md-4">
							<label  for="textinput">Due Date</label>
								<input type="text"  placeholder = "MM-DD-YYYY" id="date_due" class="date_due  form-control"  value="<?php echo $due_date;?>" name = "date_due[]" autocomplete="off" <?php if($view_content_page==true){ ?> readonly="readonly" <?php }?> /> 
								<p class="error"></p>
						</div>						
					</div>
					<div class="m">
						<div class="col-md-4">
							<label>Date Completed</label>
							<input type="text"  placeholder = "MM-DD-YYYY" id="date_completed" class="date_completed  form-control"  value="<?php echo $date_completed;?>" name = "date_completed[]" autocomplete="off" <?php if($view_content_page==true || $contact_status == "Active" || $contact_status == ""){ ?> readonly="readonly" <?php }?> />
							<p class="error"></p>
						</div>
					</div>
				</div>	
				<div class="row">
					<div class="m">
						<div class="col-md-4">
							<label>Task Type</label>
							<select class="form-control" name="task_type" id="task_type" <?php if($view_content_page==true){ ?> disabled="disabled" <?php }?>>
								<option value="" >Select One</option>
								<option value="E-Mail"  <?php echo ($task_type == 'E-Mail')?'selected':'';?>>E-Mail</option>	
								<option value="Phone" <?php echo ($task_type == 'Phone')?'selected':'';?>>Phone</option>	
								<option value="Meeting" <?php echo ($task_type == 'Meeting')?'selected':'';?>>Meeting</option>	
								<option value="Loan" <?php echo ($task_type == 'Loan')?'selected':'';?>>Loan</option>	
								<option value="Other" <?php echo ($task_type == 'Other')?'selected':'';?>>Other</option>												
							</select>
							<p class="error"></p>
						</div>						
					</div>
				</div>
				<div class="row">
					<div class="m">						
						<div class="col-md-4">
							<label  for="textinput">Contact Name</label>
							<select class="form-control " name="contact_name" id="contact_name" <?php if($view_content_page==true){ ?> disabled="disabled" <?php }?>>
								<option value="" >Select One</option>
																		
							</select>
							<p class="error"></p>

						</div>
					</div>
					<div class="m">						
						<div class="col-md-4">
							<label>Primary Phone</label>
							<input type="" id="primary_phone" class="primary_phone  form-control"  value="<?php echo $primary_phone;?>" name = "primary_phone" placeholder="(xxx)-xxx-xxxx" maxlength="14" <?php if($view_content_page==true){ ?> readonly="readonly" <?php }?>>
							<p class="error"></p>
						</div>
					</div>
					<div class="m">						
						<div class="col-md-4">
							<label>Primary E-Mail</label>
							<input type="email" id="primary_email" class="primary_email  form-control"  value="<?php echo $primary_mail;?>" name = "primary_email" placeholder="abc@xyz.com" <?php if($view_content_page==true){ ?> readonly="readonly" <?php }?>>
							<p class="error"></p>
							
						</div>
					</div>
				</div>		
				<div class="row">
					<div class="m" id="append_select_row">
						<?php
						$rowCount=0;
						if(!empty($add_people_array)){
							foreach ($add_people_array as $key => $value) {
								if($rowCount==0){
									$style='style="width: 300px"';
								}else{
									$style='style="width: 300px;margin-left: 60px;"';
								}
								?>
								<div class="col-md-4" id="people_row_<?php echo $rowCount;?>" <?php echo $style;?>>
									<label>Add People</label>
									<select class="form-control chosen" name="add_people[]" id="add_people_<?php echo $rowCount;?>" <?php if($view_content_page==true){ ?> disabled="disabled" <?php }?> >
										<option value="" >Select One</option>
										<?php
										foreach ($all_users_data as  $value1) {
											$selected="";
											if($value==$value1->id){
												$selected="selected";
											}
											?>
											<option value="<?php echo $value1->id;?>"  <?php echo $selected;?>><?php echo $value1->fname.' '.$value1->middle_name.' '.$value1->lname;?></option>									
											<?php
										}
										?>											
									</select>
									<?php
									if($rowCount==0){
										?>
										<span>
											<a title="Add People" onclick="add_people_select();" class="btn btn-sm btn-info plus_icon_add_more" <?php if($view_content_page==true){ ?> disabled="disabled" <?php }?>>
												<i class="fa fa-plus"></i>
											</a>
										</span>
										<p class="error"></p>
										<?php
									}else{
										?>
										<span>
											<a title="Add People" onclick="delete_people_select('<?php echo $rowCount;?>');" class="btn btn-sm btn-danger plus_icon_add_more" <?php if($view_content_page==true){ ?> disabled="disabled" <?php }?>>
												<i class="fa fa-trash"></i>
											</a>
										</span>
										<?php
									}
									?>
								</div>	
								<?php
								$rowCount++;
							}
						}else{
							?>
							<div class="col-md-4" id="people_row_<?php echo $rowCount;?>" style="width: 300px;">
								<label>Add People</label>
								<select class="form-control chosen" name="add_people[]" id="add_people_<?php echo $rowCount;?>" <?php if($view_content_page==true){ ?> disabled="disabled" <?php }?>>
									<option value="" >Select One</option>
									<?php
									if(!empty($all_users_data)){
										foreach ($all_users_data as  $value1) {
										?>
											<option value="<?php echo $value1->id;?>" ><?php echo $value1->fname.' '.$value1->middle_name.' '.$value1->lname;?></option>									
										<?php
										}	
									}
									?>										
								</select>
								<span>
									<a title="Add People" onclick="add_people_select();" class="btn btn-sm btn-info plus_icon_add_more" >
										<i class="fa fa-plus"></i>
									</a>
								</span>
								<p class="error"></p>
							</div>
							<?php
						}
						?>															
					</div>
				</div>
				<div class="row" >
					<div class="m">
						<div class="col-md-12">
							<label>Title</label>
							<input type="text" id="note_title" class="form-control"  value="<?php echo $note_title;?>" name="note_title"  <?php if($view_content_page==true){ ?> readonly="readonly" <?php }?>>
							<p class="error"></p>
						</div>
					</div>
				</div>
				<div class="row" style="margin-top: 20px;">
					<div class="m">
						<div class="col-md-12">
							<label>Description</label>
							<textarea id="description" class="description  form-control"  name = "description" maxlength="5000" rows="3" <?php if($view_content_page==true){ ?> readonly="readonly" <?php }?>><?php echo $task_notes;?></textarea>
							<span class="desc_counter"><i>0</i>/5000</span>
							<p class="error"></p>
						</div>
					</div>	
				</div>
				<div class="row">
					<div class="m">
						<div class="col-md-12">
							<label>Result</label>
							<textarea id="Result" class="Result  form-control" maxlength="150"  name ="Result" rows="3" <?php if($view_content_page==true || $contact_status == "Active" || $contact_status == ""){ ?> readonly="readonly" <?php }?> ><?php echo $result;?></textarea>
							<span class="result_counter"><i>0</i>/150</span>
							<p class="error"></p>
						</div>
					</div>
				</div>		
			</form>							
		</section>
	</div>
</div>
<?php
if(empty($contact_id)){
$contact_id = '';
} 
if(empty($contact_name)){
$contact_name = 'search username..';
} 
// contact_name
?>	
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>
<script type="text/javascript">
	function goBack(){
		window.history.back();
	}
	function matche_phone_formate(str){
		var clean = ('' + str).replace(/\D/g, '');
		var match = clean.match(/^(\d{3})(\d{3})(\d{4})$/);
		if (match) {
			return true;					
		}else{
			return false;
		}
	}
	function IsEmail(email) {
	  	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  	if(!regex.test(email)) {
	    	return false;
	  	}else{
	    	return true;
	  	}
	}
	$(document).ready(function(){
		/***Email formate check***/
	 	$("#primary_email").on('keyup , keypress' ,function (e) {				
			var userinput = $(this).val();
			if(IsEmail(userinput)==false){
				$(':input[type="submit"]').prop('disabled', true);
			  jQuery('#primary_email').parent().children('p.error').html("Primary Email Formate invalid");
	        }else{
	        	$(':input[type="submit"]').prop('disabled', false);
	        	 jQuery('#primary_email').parent().children('p.error').html('');
	        }
		});
		/***phone number format***/
		 
		$("#primary_phone").keypress(function (e) {
		    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		      return false;
		    }
		    var curchr = this.value.length;
		    var curval = $(this).val();
		    if (curchr == 3 && curval.indexOf("(") <= -1) {
		      	$(this).val("(" + curval + ")" + "-");
		    } else if (curchr == 4 && curval.indexOf("(") > -1) {
		      	$(this).val(curval + ")-");
		    } else if (curchr == 5 && curval.indexOf(")") > -1) {
		      	$(this).val(curval + "-");
		    } else if (curchr == 9) {
		      	$(this).val(curval + "-");
		      	$(this).attr('maxlength', '14');
		    }
		    if(matche_phone_formate(curval) == false){
		    	jQuery('#primary_phone').parent().children('p.error').html("Primary Phone Formate invalid");
		    	$(':input[type="submit"]').prop('disabled', true);
		    	if (curchr == 13) {
					$(':input[type="submit"]').prop('disabled', false);
					jQuery('#primary_phone').parent().children('p.error').html("");
		    	}else{
						$(':input[type="submit"]').prop('disabled', true);
						jQuery('#primary_phone').parent().children('p.error').html("Primary Phone Formate invalid");
		    	}
				// 	jQuery('#primary_phone').focus();
		    }else{
				jQuery('#primary_phone').parent().children('p.error').html("");
				$(':input[type="submit"]').prop('disabled', false);
		    }
		});
		$("#primary_phone").on('keyup , keypress' ,function (e){ 
		 	var phval = $(this).val();
			if(matche_phone_formate(phval) == false){
				$(':input[type="submit"]').prop('disabled', true);
				jQuery('#primary_phone').parent().children('p.error').html("Primary Phone Formate invalid");
			}else{
				$(':input[type="submit"]').prop('disabled', false);
        		jQuery('#primary_phone').parent().children('p.error').html('');
			}
		});
		jQuery('#status').on('change',function(){
			var status_val = jQuery(this).val();
			 if(status_val === 'Complete'){
				$('#date_completed').prop('readonly',false);
				$("#Result").prop('readonly',false);
			  }else{
			  	$('#date_completed').val('');
			   	$('#date_completed').prop('readonly',true);
			   	$("#Result").html("");
			   	$('#Result').prop('readonly',true);
			  }
		});
		jQuery('.submit_frm').click(function(e){			
			var chk  = true;
			var contact_name = jQuery('#contact_name').val(); 
			if (contact_name==null || contact_name==""){  
				jQuery('#contact_name').parent().children('p.error').html("Contact Name can't be blank");
				jQuery('#contact_name').focus();
				chk = false;  
			}
			var note_title=jQuery('#note_title').val();
			if (note_title==null || note_title==""){  
				jQuery('#note_title').parent().children('p.error').html("Title can't be blank");
				jQuery('#note_title').focus();
				chk = false;  
			}
			var status = jQuery('#status').val();
			if (status==null || status==""){  
				jQuery('#status').parent().children('p.error').html("Status can't be blank");
				jQuery('#status').focus();
				chk = false;  
			}
			if(status == 'Complete'){
			 	var Date_comp = jQuery('input#date_completed').val();
			 	if (Date_comp==null || Date_comp==""){  
					jQuery('#date_completed').parent().children('p.error').html("Date Completed  can't be blank");
					jQuery('#date_completed').focus(); 
					chk = false;  
				}else{
					
				}
			}
			var date_entered = jQuery('#date_entered').val();
			if (date_entered==null || date_entered==""){  
				jQuery('#date_entered').parent().children('p.error').html("Date Entered can't be blank");
				jQuery('#date_entered').focus();
				chk = false;  
			}
			var primary_email = jQuery('#primary_email').val();
			if (primary_email==null || primary_email==""){  
				jQuery('#primary_email').parent().children('p.error').html("Primary Email can't be blank");
				jQuery('#primary_email').focus();
				chk = false;  
			}
			var primaryphone = jQuery('#primary_phone').val();
			if (primaryphone==null || primaryphone==""){  
				jQuery('#primary_phone').parent().children('p.error').html("Primary Email can't be blank");
				jQuery('#primary_phone').focus();
				chk = false;  
			}
			var date_due = jQuery('#date_due').val();
			if (date_due==null || date_due==""){  
				jQuery('#date_due').parent().children('p.error').html("Due-Date can't be blank");
				jQuery('#date_due').focus();
				chk = false;  
			}
			if(chk == false){
				e.preventDefault();
			}
		});
		$(".date_due" ).datepicker({ dateFormat: 'mm-dd-yy'});
		$(".date_entered" ).datepicker({ dateFormat: 'mm-dd-yy'});
		$(".date_completed" ).datepicker({ dateFormat: 'mm-dd-yy',minDate: 0,
		beforeShow: function(i) { 
			if ($(i).attr('readonly')) { return false; } 
			}
		});
		jQuery('.select2-container input').keyup(function(){
			console.log(jQuery(this).val());
		});
	});
	$(".chosen").chosen({
		search_contains: true
	});
	var phone1 =  jQuery('#primary_phone').val();
	let formatPhoneNumber = (str) => {
			let cleaned = ('' + str).replace(/\D/g, '');
			let match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
			if (match) {
				return '(' + match[1] + ')-' + match[2] + '-' + match[3]
			}else{
				return '('+cleaned.substr(0, 3)+')-' + cleaned.substr(3, 3) + '-' + cleaned.substr(6,4);
			}
	};
	if(phone1 === ''){			
	}else{
		var yu = formatPhoneNumber(phone1);
		jQuery('#primary_phone').val(yu);
	}
	$(document).on( 'change' , '#contact_name' , function() {
		$.ajax({
        type : 'POST',
        url  : "/Pranav/Talimar/Task/fetch_name_by_id",
        data : {'id': $(this).val()},
        success : function(response){
        	console.log(response);
        	var json = JSON.parse(response);
        	$.each(json, function(index, element) {
                var email = element.email;
				var phone = element.phone;	
				if(phone!="" && phone!="NULL" && phone!="0"){
					var tui = formatPhoneNumber(phone);
					jQuery('#primary_phone').val(tui);
				}else{
					jQuery('#primary_phone').val(phone);
				}
				jQuery('#primary_email').val(email);          
    		});  
        }
    });
		

			
	});
	function after_ajx_load(){
		$(document).on( 'change' , '#contact_name' , function() {
			var email = $('#contact_name option:selected').data('email');
			var phone = $('#contact_name option:selected').data('phone');
			var tui = formatPhoneNumber(phone);
			jQuery('#primary_phone').val(tui);
			jQuery('#primary_email').val(email);				
		});
	}
	var cnoncat_ids  = "<?php echo $contact_id;?>";
	var contact_name  = "<?php echo $contact_name;?>";
	$('#contact_name').select2({
    	placeholder: contact_name,
    	minimumInputLength: 1,
    	tags: true,
    	ajax: {
	        url: "/Pranav/Talimar/Task/contact_name_lists1",
	        dataType: 'json',
	        delay: 250,
	        cache: false,
	        data: function (params) {
		            return {
		                term: params.term,
		                page: params.page || 1,
		            };
        	},
        	processResults: function(data, params) {
	           	jQuery('ul#select2-contact_name-results li:first').prop('disabled',true);
	            var page = params.page || 1;
	            return {
	                results: $.map(data, function (item) { return {id: item.id, text: item.col , source: item.total_count,}}),
	                pagination: {
	                // THE `10` SHOULD BE SAME AS `$resultCount FROM PHP, it is the number of records to fetch from table` 
	                    more: (page * 10) <= data[0].total_count
	                }
	            };
        	},              
    	}
	});	
	$('#contact_name').append('<option value="'+cnoncat_ids+'" selected="selected">'+contact_name+'</option>');
	jQuery('input.select2-search__field').keyup(function(){
		jQuery('ul#select2-contact_name-results li:first').prop('disabled',true);
	});
  	$(document).on( 'keyup' , '.select2-container input' , function() {
	  	setTimeout(function(){
	  		jQuery('span.select2-results ul').find('li:first').hide();
	  	}, 500);
	});
</script>
<script type="text/javascript">
	var people_row_count='<?php echo $rowCount;?>'
	function add_people_select(){
		var selected = $('select[name="add_people[]"]').map(function(){
		    if ($(this).val())
		        return $(this).val();
		}).get();
		console.log(selected);
		people_row_count=parseInt(people_row_count)+1;
		$.ajax({
	        	type : 'POST',
	        	url  : "/Pranav/Talimar/Task/add_new_people_select",
	         	data : {'people_row_count': people_row_count,'selected_value':selected},
		        success : function(response){	
		        	$("#append_select_row").append(response);
		        	$(".chosen_"+people_row_count).chosen({
						search_contains: true
					});
		        }
	    });
	}
	function delete_people_select(count_row){
		$("#people_row_"+count_row).remove();

	}
</script>