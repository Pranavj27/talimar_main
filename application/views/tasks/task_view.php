<style>
	div#table_contactlist_filter {
	    float: right;
	}
	div.page-title h3 {
	    padding: 7px 11px;
	    font-weight: 600;
	}
	.table{
		margin-top: 7px;
	}
	.dataTables_paginate .paginate_button.previous, .dataTables_wrapper .dataTables_paginate .paginate_button.previous a, .dataTables_paginate .paginate_button.next, .dataTables_wrapper .dataTables_paginate .paginate_button.next a{
		font-size: 12px !important;
	}
	div#table_contactlist_length{
		margin-top: 17px;
	}
	select.form-control.input-sm{
		margin-right: 15px;
	    margin-left: 15px;
	}
	div#table_contactlist_length label{
		display: flex;
	}
	div#table_contactlist_wrapper {
	    margin-top: 18px;
	}
	/*data table search hide*/
	div#table_contactlist_filter {
	    display: none;
	}
	div#table_contactlist_length {
	    display: none;
	}
	/*10-01-2021*/
	.table thead tr th {
	    font-size: 14px;
	    font-weight: 600;
	    color: #fff;
	    padding: 15px;
	    background: #C0C0C0;
	}
	/*12-07-2021*/
	table tbody tr td {
	    padding: 13px 10px !important;
	    /* line-height: 8.5px; */
	    vertical-align: middle !important;
	}
</style>
<div class="page-container contactlist-outmost">		
	<!-- BEGIN PAGE HEAD -->		
	<div class="container">
		<div class="tab-pane">
			<!--------------------MESSEGE SHOW-------------------------->
			<?php if($this->session->flashdata('error')!=''){  ?>
				<div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error');?></div><?php } ?>
			<?php if($this->session->flashdata('success')!=''){  ?>
				<div id='success'><i class='fa fa-thumbs-o-up'></i> <?php echo $this->session->flashdata('success');?></div><?php } ?>
			<div id = "chkbox_err"></div>		 
			<!-------------------END OF MESSEGE-------------------------->
			<!-- BEGIN PAGE HEAD -->			
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h3>Task View </h3>
				</div>					
			</div>
			<div class="main_c">		
				<div class="row" style="padding: 23px">			
					<div class="col-md-4">	
						<form id="selct_form" method="POST" action="<?php echo base_url();?>task_view_list">				
							<div class="col-md-3">
									<label style="margin-top:8px;">Status:<label>
						  	</div>
							<div class="col-md-5">
								<select name="selct_status" id ="selct_status" onchange = "selct_task_filter(this.value)" class="form-control">
									<option style="padding: 10px"  value="" <?php echo ($task_status=='') ? 'selected' :'';?>>Select All</option>
									<option style="padding: 10px"  value="Active" <?php echo ($task_status=='Active') ? 'selected' :'';?>>Active</option>
									<option value="Complete" <?php echo ($task_status=='Complete') ? 'selected' :'';?>>Complete</option>
								</select>
							</div>
						</form>
					</div>
					<div class="col-md-4">
					</div>
					<div class="col-md-4" style="text-align: end;">
						<a class="new_add btn btn-primary" href="<?php echo base_url('add_task/add'); ?>" >Add New Task</a></div>
					</div>
				</div>
				<div class="page-container">
		            <div class="contactlist-res-div">
		                <div class="col-md-12">	
							<table id ="table_contactlist" class="table table-responsive  table-bordered table-striped table-condensed flip-content display">
								<thead>
									<tr>									
										<th> Contact Name</th>
										<th style = "width: 300px;"> Title</th>
										<th> Status </th>
										<th> Contact Task</th>							
										<th> Due Date</th>									
										<th> Details</th> 
									</tr>
								</thead>
								<tbody>
									<?php 
									$p=0;
									if(!empty($fetch_check_sql)){
										foreach($fetch_check_sql as $key => $row){
											$che_due_date = $row->due_date;
										 	if(isset($che_due_date) && !empty($che_due_date)){
										 		$_due_dates = date('m-d-Y', strtotime($che_due_date));
										 	}else{
												$_due_dates = '';
										 	}

											?>
											<tr  data-date="<?php echo $_due_dates; ?>" data-status="<?php echo $row->contact_status;?>">
												<td style ="text-align: left;">
													<a href="<?php echo base_url().'viewcontact/'.$row->contact_id;?>"><?php echo $row->contact_user;?></a>
												</td>										
												<td style = "text-align: left;width: 300px;" >
													<?php
													$small = substr($row->note_title, 0, 250);
													if(strlen($row->note_title)>250){
														?>
														<span id='small_content_section_<?php echo $p;?>'><?php echo $small;?><a href='javascript:void(0)' onclick='show_data("large","<?php echo $p;?>")'>.read more</a></span>
														<span id='lage_content_section_<?php echo $p;?>' style="display:none;"><?php echo $row->task_notes;?><a href='javascript:void(0)' onclick='show_data("small","<?php echo $p;?>")'>.read less</a></span>
													<?php
													}else{
														echo $row->note_title;
													}
													?>
												</td>
												<td><?php echo $row->contact_status;?></td>
												<td style = "text-align: left;">
													<?php echo $row->contact_task;?>
												</td>
												<td style = "text-align: left;" data-date="<?php echo $_due_dates; ?>">
													<?php echo $_due_dates; ?>
												</td>
												<td>
													<a href="/Pranav/Talimar/add_task/<?php echo $row->id;?>/view" class="btn btn-primary bu_Block">
														<i class="fa fa-eye" aria-hidden="true"></i>
													</a>
													<a href="/Pranav/Talimar/Task/delete_task/<?php echo $row->id;?>" class="btn btn-primary bu_Block" onclick="return confirm(' you want to delete?');">
														<i class="fa fa-trash-o" aria-hidden="true"></i>
													</a>
												</td>									
											</tr>
											<?php  
											$p++; 
										} 
									}?>
								</tbody>
								<tfoot>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div><!-- main_c End-->
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
<script>
	$( document ).ready( function(){
		if(jQuery('table#table_contactlist tr').length > 1){
			table_sort_by_month_date();
		}		
	}); 
	$('#table_contactlist').DataTable({
		"pageLength": 100,
		 "order": [[ 0, "asc" ]],
		 columnDefs: [ { type: 'date', targets: [4] } ],
		 aoColumnDefs: [
		  {
		     bSortable: false,
		     aTargets: [ 5 ]
		  },
		  {
		     bSortable: false,
		     aTargets: [ 1 ]
		  }
		]
	});	
	function deleteItem() {
	    var checkstr =  confirm('are you sure you want to delete this?');
	    if(checkstr == true){
	      // do your code
	    }else{
	    	return false;
	    }
	}		
	function selct_task_filter(){
		$('form#selct_form').submit();
	}
	jQuery('div#table_contactlist_paginate li.paginate_button').on('click',function(){
			jQuery('table tbody tr td._phone').each(function(){
				var phone1 = jQuery(this).find('a').text(); 
				if(phone1 === ''){			
				}else{
					var p_frmt1 = formatPhoneNumber(phone1);
					jQuery(this).find('a').text(p_frmt1);
				}
			});
	});
	function table_sort_by_month_date(){
		jQuery('table#table_contactlist tbody tr').each(function(){
			var data_nme = jQuery(this).data('date');
			var data_status = jQuery(this).data('status');  
		});
	}		
	function show_data(type,row_no){
		if(type=="large"){
			$("#small_content_section_"+row_no).hide();
			$("#lage_content_section_"+row_no).show();
		}else{
			$("#lage_content_section_"+row_no).hide();
			$("#small_content_section_"+row_no).show();
		}

	}
</script>	