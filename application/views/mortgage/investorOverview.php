<?php $investor_type_option = $this->config->item('investor_type_option'); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
	tbody > tr >td{
		    text-align: left!important;
	}
	tfoot > tr >th{
		    text-align: left!important;
	}
</style>
<div class="page-container" id="page-invester-view">
	<div class="container">
		<div class="page-title">
			<h1 style="margin-left:14px !important;">Investor Overview</h1>
		</div>
	</div>
	<!-------------------END OF MESSEGE-------------------------->
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
				
		<?php
		
		if($btn_name == 'mortgageFund'){
			$btn_class = 'style ="display:block;" ';
		}else{
			$btn_class = 'style ="display:none;" ';			
		}
		$page_url = $_SERVER['REQUEST_URI'];
		?>
	</div>
	
	<ul class="page-breadcrumb breadcrumb hide">
		<li>
			<a href="#">Home</a><i class="fa fa-circle"></i>
		</li>
		<li class="active">
			 Dashboard
		</li>
	</ul>
	<div class="row" style="min-height: 400px;">
		<div class="col-md-4">
			
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
				<thead>
					<tr class="thead_dark_blue">
						<th colspan="3">Investor Overview</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Active Capital</td>
						<td>$<?php echo number_format($investorTotal['totalInvestor']*$investorTotal['avgTotal']); ?></td>
					</tr>
					<tr>
						<td># of Active Accounts</td>
						<td><?php echo number_format($investorTotal['totalInvestor']); ?></td>
					</tr>
					<tr>
						<td>Avg. Balance</td>
						<td>$<?php echo number_format($investorTotal['avgTotal']); ?></td>
					</tr>
					<tr>
						<td>Capital in Processing:</td>
						<td>$<?php echo number_format($investorTotal['amountTotalProccess']); ?></td>
					</tr>
					<tr>
						<td>Accounts in Processing</td>
						<td><?php echo $investorTotal['countProccess']; ?></td>
					</tr>
					<tr>
						<td>Est. Reinvested Funds</td>
						<td>
							<?php $t = (double)$investorData->totalpayment*12; 
							$t = $t/(double)$investorData->totalAmount;
							
							echo '$'.number_format(($ReinvestData['amountTotal']*$t)/12); ?>
								
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-4">
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
				<thead>
					<tr class="thead_dark_blue">
						<th colspan="3">Investor Type</th>
					</tr>
					<tr>
						<th>Type</th>
						<th>$ Amount</th>
						<th>% of Fund</th>					
					</tr>
				</thead>
				<tbody>
					<?php $totalAmount = 0.00; if($lenderDataType){ foreach ($lenderDataType as $key => $value) { 
						$totalAmount = $totalAmount + $value->amountTotal;

					 ?>
					<tr>
						<td><?php echo $investor_type_option[$value->investor_type]; ?></td>
						<td><?php echo $value->amountTotal > 0 ? '$'.number_format($value->amountTotal) : ''; ?></td>
						<td><?php echo $value->amountTotal > 0 ? number_format(((double)$value->amountTotal / (double)$investorTotal['amountTotal'])*100, 2).'%' : '' ; ?></td>					
					</tr>	
					<?php } } else {?>
						<tr><td colspan="3">Data Not Found</td></tr>
					<?php } ?>
				</tbody>
				<tfoot class="foot_light_blue">
					<tr>
						<th>Total: <?php echo COUNT($lenderDataType);  ?></th>
						<th>$<?php echo number_format($totalAmount, 2);  ?></th>
						<th><?php echo number_format(($totalAmount / $investorTotal['amountTotal'])*100, 2).'%'; ?></th>
					</tr>
				</tfoot>
			</table>
		</div>
		<div class="col-md-4">
			
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
				<thead>
					<tr class="thead_dark_blue">
						<th colspan="3">Top 5 Investors</th>
					</tr>
					<tr>
						<th>Lender Name</th>
						<th>$ Amount</th>
						<th>% of Fund</th>					
					</tr>
				</thead>
				<tbody>
					<?php $totalAmount = 0.00; if($lenderTop){ foreach ($lenderTop as $key => $value) { 
						$totalAmount = $totalAmount + $value->amountTotal;
					?>
					<tr>
						<td><?php echo $value->name; ?></td>
						<td><?php echo $value->amountTotal > 0 ? '$'.number_format($value->amountTotal) : ''; ?></td>
						<td><?php echo $value->amountTotal > 0 ? number_format(($value->amountTotal / (double)$investorTotal['amountTotal'])*100, 2).'%' : '' ; ?></td>					
					</tr>	
					<?php } } else {?>
						<tr><td colspan="3">Data Not Found</td></tr>
					<?php } ?>
				</tbody>
				<tfoot class="foot_light_blue">
					<tr>
						<th>Total: <?php echo COUNT($lenderTop);  ?></th>
						<th>$<?php echo number_format($totalAmount, 2);  ?></th>
						<th><?php echo number_format(($totalAmount / (double)$investorTotal['amountTotal'])*100, 2).'%'; ?></th>
					</tr>
				</tfoot>
			</table>
		</div>
		<div class="clearfix"></div>		
		<div class="col-md-4">
			
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
				<thead>
					<tr class="thead_dark_blue">
						<th colspan="4">Distribution Type</th>
					</tr>
					<tr>
						<th>Type</th>
						<th>Count</th>
						<th>$</th>
						<th>%</th>					
					</tr>
				</thead>
				<tbody>
					
					<tr>
						<td>Reinvest</td>
						<td><?php echo $ReinvestData['totalId']; ?></td>
						<td>$<?php echo number_format($ReinvestData['amountTotal'],2); ?></td>
						<td><?php echo number_format(($ReinvestData['amountTotal']/($investorTotal['totalInvestor']*$investorTotal['avgTotal'])) *100, 2); ?>%</td>					
					</tr>

					<tr>
						<td>Distribute</td>
						<td><?php echo $distributeData['totalId']; ?></td>
						<td>$<?php echo number_format($distributeData['amountTotal'], 2); ?></td>
						<td><?php echo number_format(($distributeData['amountTotal']/($investorTotal['totalInvestor']*$investorTotal['avgTotal'])) *100, 2); ?>%</td>					
					</tr>

					<tr>
						<td>Both</td>
						<td><?php echo $BothData['totalId']; ?></td>
						<td>$<?php echo number_format($BothData['amountTotal'], 2); ?></td>
						<td><?php echo number_format(($BothData['amountTotal']/($investorTotal['totalInvestor']*$investorTotal['avgTotal'])) *100, 2); ?>%</td>					
					</tr>	
					
				</tbody>
				<tfoot class="foot_light_blue">
					<tr>
						<th>Total</th>
						<th><?php echo $countDD = $ReinvestData['totalId'] + $distributeData['totalId'] + $BothData['totalId'];  ?></th>
						<th>$<?php echo $amountDD = number_format($ReinvestData['amountTotal'] + $distributeData['amountTotal'] + $BothData['amountTotal'] , 2); ?></th>
						<th><?php echo number_format((($ReinvestData['amountTotal'] + $distributeData['amountTotal'] + $BothData['amountTotal'])/($investorTotal['totalInvestor']*$investorTotal['avgTotal'])) *100, 2); ?>%</th>
					</tr>
				</tfoot>
			</table>
		</div>

		<div class="col-md-4">
			
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
				<thead>
					<tr class="thead_dark_blue">
						<th colspan="3">Capital Type</th>
					</tr>
					<tr>
						<th>Type</th>
						<th>Amount</th>
						<th>% of Capital</th>					
					</tr>
				</thead>
				<tbody>
					<?php $totalCapital = 0.00; if($lenderCapitalType){ foreach ($lenderCapitalType as $CapitalType) { ?>
					<tr>
						<td><?php echo $CapitalType->transactionType === 'Direct Investment' ? 'Principal Investment' : $CapitalType->transactionType; ?></td>
						<td>$<?php echo number_format($CapitalType->amountTotal, 2); ?></td>
						<td><?php echo number_format(($CapitalType->amountTotal/$lenderCapitalTypeTotal->amountTotal) *100, 2); ?>%</td>					
					</tr>	
					<?php $totalCapital = $totalCapital + $CapitalType->amountTotal;

					} } else { ?>
						<tr><td colspan="3">Data Not Found</td></tr>
					<?php } ?>
				</tbody>
				<tfoot class="foot_light_blue">
					<tr>
						<th>Total</th>
						<th>$<?php echo number_format($totalCapital, 2);  ?></th>
						<th><?php echo number_format(($totalCapital/$lenderCapitalTypeTotal->amountTotal) *100, 2); ?>%</th>
					</tr>
				</tfoot>
			</table>
		</div>

		<div class="col-md-4">
			
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
				<thead>
					<tr class="thead_dark_blue">
						<th colspan="3">Accounts in Processing</th>
					</tr>
					<tr>
						<th>Investor Name</th>
						<th>Account Type</th>
						<th>Accredited</th>					
					</tr>
				</thead>
				<tbody>
					<?php if($accountProcessing){ foreach ($accountProcessing as $accountValue) { ?>
					<tr>
						<td><?php echo $accountValue->name; ?></td>
						<td><?php echo $accountValue->subscriptionStatus == 'In Process' ? 'Processing' : $accountValue->subscriptionStatus; ?></td>
						<td><?php echo ucfirst($accountValue->accredited); ?></td>					
					</tr>	
					<?php } } else { ?>
						<tr><td colspan="3">Data Not Found</td></tr>
					<?php } ?>
				</tbody>
				<tfoot class="foot_light_blue">
					<tr>
						<th>Total</th>
						<th></th>
						<th><?php echo count($accountProcessing); ?></th>
					</tr>
				</tfoot>
			</table>
		</div>

		<div class="clearfix"></div>

		<div class="col-md-4">
			
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
				<thead>
					<tr class="thead_dark_blue">
						<th colspan="3">Accounts in Processing</th>
					</tr>
					<tr>
						<th>Investor Name</th>
						<th>Date Requested</th>
						<th>Amount ($)</th>					
					</tr>
				</thead>
				<tbody>
					<?php $totalTra = 0.00; if($accountProcessingAm){ foreach ($accountProcessingAm as $accValue) { $explode = explode('-', $accValue->date);  ?>
					<tr>
						<td><?php echo $accValue->name; ?></td>
						<td><?php echo $explode[1].'-'.$explode[2].'-'.$explode[0] ; ?></td>
						<td>$<?php echo number_format($accValue->amountTotal); ?></td>					
					</tr>	
					<?php $totalTra = $totalTra +  $accValue->amountTotal; 
				} } else { ?>
						<tr><td colspan="3">Data Not Found</td></tr>
					<?php } ?>
				</tbody>
				<tfoot class="foot_light_blue">
					<tr>
						<th>Total: <?php echo count($accountProcessingAm); ?></th>
						<th></th>
						<th>$<?php echo number_format($totalTra); ?></th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
<div class="clearfix"></div>
