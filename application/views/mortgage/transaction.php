<?php $investor_type_option = $this->config->item('investor_type_option');
$account_type_option = $this->config->item('account_type_option');


?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
	.table>tbody>tr>td, .table>tbody>tr>th, .table>thead>tr>td, .table>thead>tr>th{
		border-top: none;
	}
	.table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th{
		border-top: none;
	}
	.table>thead>tr>th{
		border-bottom: none;
	}
	.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
		border-top: none;
	}

	.error{
		color: red;
	}
	body{
		font-size: 14!important;
	}
	.col-md-4 {
    padding-top: 10px;
    padding-bottom: 5px;
	}
	.border-top h4 {
    background-color: #428bca !important;
    height: 36px !important;
    line-height: 36px !important;
    padding-left: 15px !important;
	}
</style>
<div class="page-container" id="page-invester-view">
	<div class="container">
			<!--------------------MESSEGE SHOW-------------------------->
		
		<?php if($this->session->flashdata('error')!=''){  ?>
			<div class="alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong><?php echo $this->session->flashdata('error'); ?></strong> 
            </div>
		<?php } ?>
		<?php if($this->session->flashdata('success')!=''){  ?>
			<div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong><?php echo $this->session->flashdata('success'); ?></strong> 
            </div>
		<?php } ?>
	</div>
	<!-------------------END OF MESSEGE-------------------------->
	<!-- BEGIN PAGE HEAD -->


	<input type="hidden" name="saveTransation" value="1">	
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="row">
			<div class="col-md-6 pull-left">
				<h1>Investors Details: <a href="<?php echo base_url('investor_view/'.$lenderData->id); ?>"><?php echo $lenderData->name; ?></a><small></small></h1>
			</div>
			
			<div class="col-md-6 pull-right">
				<h1 class="pull-right"><a href="<?php echo base_url('mortgageFund/editDetails/'.$lenderId); ?>" class="btn btn-primary borrower_save_button">Back</a></h1>
				<!-- <h1 class="pull-right"><input style="border-radius: 0px; margin-top: 3px;" id="submit" type="submit" class="btn btn-primary borrower_save_button" name="demo" value="Save"></h1> -->
				<h1 class="pull-right"><a class="btn btn-primary borrower_save_button" href="javascript:void(0);" onclick="AddTrasaction()">Add</a></h1>		
			</div>	
		</div>
	</div>
	
	<ul class="page-breadcrumb breadcrumb hide">
		<li>
			<a href="#">Home</a><i class="fa fa-circle"></i>
		</li>
		<li class="active">
			 Dashboard
		</li>
	</ul>
	<div class="row" id="reloadPage">
		<div class="clearfix"></div>
		<div class="clearfix"></div>
		<div class="col-md-10 border-top">
			<div class="row">			<h4 class="btn-primary">Transaction Schedule</h4>
			<table id="table" class="table table-responsive">
				<thead>
					<tr>
						<th>Date Requested</th>
						<th>Date Completed</th>
						<th>Transaction Type</th>
						<th>Transaction Status</th>
						<th>Funds Status</th>
						<th>Deposit Type</th>
						<th>Amount ($)</th>
						<th>Action</th>	
					</tr>
				</thead>
				<tbody id="investAdd">

				</tbody>
				
				<tfoot style="background-color:#e8e8e8">
					<tr>
						<td id="totalRecordRecieved"></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="totalAmountRecieved"></td>
						<td></td>
						
					</tr>
					<!-- <tr>
						<td id="totalRecord"></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="inProcess"></td>
						<td id="totalAmount"></td>
						<td></td>
						<td></td>
					</tr> -->
				</tfoot>
			</table>
		</div>
	
	</div>
</div>
<!-- Button trigger modal -->


<!-- Modal -->

<div class="clearfix"></div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width:350px">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Mortgage Fund Lender Edit</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:-19px!important">
          <span aria-hidden="true" onclick="pageReload()">&times;</span>
        </button>
      </div>
      <div id="MessageError1"></div>
      <div class="modal-body" id="modelBody">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="pageReload()">Close</button>
        <button type="button" class="btn btn-primary" onclick="updateTran()">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Add Transaction Model -->

<div class="modal fade" id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width:350px">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Mortgage Fund Lender Edit</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:-19px!important">
          <span aria-hidden="true" onclick="pageReload()">&times;</span>
        </button>
      </div>
      <div id="MessageErrorAdd"></div>
      <form id="AddTransection" method="POST">
	      <div class="modal-body" id="modelBodyAdd">
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="pageReload()">Close</button>
	        <input style="border-radius: 0px; margin-top: 3px;" id="submit" type="button" class="btn btn-primary borrower_save_button" name="demo" value="Save" onclick="AddTran()">
	      </div>
    	</form>
    </div>
  </div>
</div>
<div class="clearfix"></div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script> 

<div id="">


<script> 


function pageReload()
{
	window.location.reload();
}

$(document).ready(function(){
	$(".from-datepicker").datepicker({          
	format: 'mm-dd-yy' //can also use format: 'dd-mm-yyyy'     
	});
});

getCalender();

function getCalender(){ 
	$(".from-datepicker1").datepicker("destroy");
    $(".from-datepicker1").datepicker({ dateFormat: "mm-dd-yy" }).val();		
};

$('.amount_format').change(function(){
	var a = this.value;
		var a = replace_dollar(a);
		if(a == '')
		{
			a = 0;
		}
		if(isNaN(a) == true)
		{
			a = 0;
		}
		a = parseFloat(a);
		this.value = '$'+ number_format(a);    
});

  
function amount_format(that){
			
		var a = that.value;
		var a = replace_dollar(a);
		if(a == '')
		{
			a = 0;
		}
		if(isNaN(a) == true)
		{
			a = 0;
		}
		a = parseFloat(a);
		that.value = '$'+ number_format(a);
}; 

 
function replace_dollar(n)
{
	var a = n.replace('$', '');
	var b = a.replace(',', '');
	var b = b.replace(',', '');
	var b = b.replace(',', '');
	var b = b.replace(',', '');
	var c = b.replace('%', '');
	return c;
}

function number_format(n) {
    return n.toFixed(2).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
} 
</script>

<script type="text/javascript">

alertDismiss();
function alertDismiss(){

		$(".alert-dismissible").fadeTo(2000, 3000).slideUp(3000, function(){
		    $(".alert-dismissible").slideUp(3000);
		});
		setTimeout(function(){ $('#page-invester-view > .container').html('')}, 30000);
};

function AddTrasaction()
{
			html = '';
			$('#modelBodyAdd').html('');
			
			html += '<h4><a href="<?php echo base_url('investor_view/'.$lenderData->id); ?>">Investor : <?php echo $lenderData->name; ?></a></h4>';
			html += '<input type="hidden" class="form-control" name="mortgageId" value="">';
			html += '<input type="hidden" class="form-control" name="lenderId" value="<?php echo $lenderData->id; ?>">';
			html += '<div class="row">';
			html += '<div class="form-group">';
			html += '<label>Date Requested</label>';
			html += '<input style="width:70%!important" type="text" id="DateAdd" class="form-control from-datepicker1" name="date" value="" required>';
			html += '</div>';
			html += '<div class="form-group">';
			html += '<label>Date Completed</label>';
			html += '<input style="width:70%!important" type="text" id="dateCompleted" class="form-control from-datepicker1" name="dateCompleted" value="">';
			html += '</div>';
			html += '<div class="form-group">';
			html += '<label>Transaction Type</label>';
			html += '<select class="form-control" name="transactionType" style="width:70%!important">';
			html += '<option value="">Select One</option>';
			html += '<option>Direct Investment</option>';
			html += '<option>Reinvestment</option>';
			html += '<option>Withdrawal</option>';
			html += '<option>Other</option>';
			html += '</select>';
			html += '</div>';
			html += '<div class="form-group">';
			html += '<label>Transaction Status</label>';
			html += '<select class="form-control" name="subscriptionStatus" style="width:70%!important">';
			html += '<option value="">Select One</option>';
			html += '<option>Processing</option>';
			html += '<option>Complete</option>';				
			html += '</select>';
			html += '</div>';
			html += '<div class="form-group">';
			html += '<label>Fund Status</label>';
			html += '<select class="form-control" name="fundStatus" style="width:70%!important">';
			html += '<option value="">Select One</option>';
			html += '<option>Not Requested</option>';
			html += '<option>Requested</option>';
			html += '<option value="Received">Confirmed</option>';
			html += '</select>';
			html += '</div>';
			html += '<div class="form-group">';
			html += '<label>Deposit Type</label>';
			html += '<select class="form-control" name="depositType" style="width:70%!important">';
			html += '<option value="">Select One</option>';
			html += '<option>Wire</option>';
			html += '<option>ACH</option>';
			html += '<option>Cash / Check</option>';
			html += '<option>Reinvestment</option>';
			html += '</select>';
			html += '</div>';
	    html += '<div class="form-group">';
	    html +=	'<label>Amount ($)</label>';
	    html +=	'<input type="text" id="amount_formatmodel"  class="amount_format number_only form-control" name="amount" value="" style="width:70%!important">';
	    html +=	'</div>';
	    html +=	'</div>';

			$('#modelBodyAdd').html(html);
			setTimeout(function(){ getCalender(); }, 500);
			// amount_format('#amount_formatmodel');
			$("#myModalAdd").modal("show");
}

function ajaxgetDatamodel(mortgageId, id)
{
	html = '';
	$('#modelBody').html('');
 	$.ajax({
		type : 'POST',
		url  : '<?php echo base_url()."MortgageFound/ajaxGetTransaction";?>',
		data : {'mortgageId' : mortgageId, 'lenderId' : id},
		beforeSend: function() {
			
		},
		success : function(response){
			var json = JSON.parse(response);


			if(json.status == true)
			{
				var result = json.result;
				html += '<h4><a href="<?php echo base_url('investor_view/'); ?>'+id+'">Investor : '+result.text +'</a></h4>';
				html += '<form id="UpdateTransection">';
				html += '<input type="hidden" class="form-control from-datepicker1" id="DateUpdate" name="mortgageId" value="'+result.transactionType.mortgageId+'" required>';
				html += '<div class="row">';
				html += '<div class="form-group">';
				html += '<label>Date Requested</label>';
				html += '<input style="width:70%!important" type="text" class="form-control from-datepicker1" name="date" value="'+result.transactionType.date+'" required>';
				html += '</div>';
				html += '<div class="form-group">';
				html += '<label>Date Completed</label>';
				html += '<input style="width:70%!important" type="text" class="form-control from-datepicker1" name="dateCompleted" value="'+result.transactionType.dateCompleted+'" required>';
				html += '</div>';
				html += '<div class="form-group">';
				html += '<label>Transaction Type</label>';
				html += '<select class="form-control" name="transactionType" style="width:70%!important">';
				html += '<option value="">Select One</option>';
				if(result.transactionType.transactionType == "Direct Investment"){
					html += '<option selected>Direct Investment</option>';
				}
				else{
					html += '<option>Direct Investment</option>';
				}

				if(result.transactionType.transactionType == "Reinvestment"){
					html += '<option selected>Reinvestment</option>';
				}
				else{
					html += '<option>Reinvestment</option>';
				}

				if(result.transactionType.transactionType == "Withdrawal"){
					html += '<option selected>Withdrawal</option>';
				}
				else{
					html += '<option>Withdrawal</option>';
				}

				if(result.transactionType.transactionType == "Other"){
					html += '<option selected>Other</option>';
				}
				else{
					html += '<option>Other</option>';
				}
				
				html += '</select>';
				html += '</div>';
				html += '<div class="form-group">';
				html += '<label>Transaction Status</label>';
				html += '<select class="form-control" name="subscriptionStatus" style="width:70%!important">';
				html += '<option value="">Select One</option>';
				

				if(result.transactionType.subscriptionStatus == "Processing"){
					html += '<option selected>Processing</option>';
				}
				else{
					html += '<option>Processing</option>';
				}

				if(result.transactionType.subscriptionStatus == "Complete"){
					html += '<option selected>Complete</option>';
				}
				else{
					html += '<option>Complete</option>';
				}
				
				html += '</select>';
				html += '</div>';
				html += '<div class="form-group">';
				html += '<label>Fund Status</label>';
				html += '<select class="form-control" name="fundStatus" style="width:70%!important">';

				html += '<option value="">Select One</option>';

				if(result.transactionType.fundStatus == "Not Requested"){
					html += '<option selected>Not Requested</option>';
				}
				else{
					html += '<option>Not Requested</option>';
				}
				
				if(result.transactionType.fundStatus == "Requested"){
					html += '<option selected>Requested</option>';
				}
				else{
					html += '<option>Requested</option>';
				}

				if(result.transactionType.fundStatus == "Received"){
					html += '<option value="Received" selected>Confirmed</option>';
				}
				else{
					html += '<option value="Received">Confirmed</option>';
				}

				html += '</select>';
				html += '</div>';
				html += '<div class="form-group">';
				html += '<label>Deposit Type</label>';
				html += '<select class="form-control" name="depositType" style="width:70%!important">';
				html += '<option value="">Select One</option>';
				if(result.transactionType.depositType == "Wire"){
					html += '<option selected>Wire</option>';
				}
				else{
					html += '<option>Wire</option>';
				}

				if(result.transactionType.depositType == "ACH"){
					html += '<option selected>ACH</option>';
				}
				else{
					html += '<option>ACH</option>';
				}

				if(result.transactionType.depositType == "Cash / Check"){
					html += '<option selected>Cash / Check</option>';
				}
				else{
					html += '<option>Cash / Check</option>';
				}

				if(result.transactionType.depositType == "Cash / Check"){
					html += '<option selected>Reinvestment</option>';
				}
				else{
					html += '<option>Reinvestment</option>';
				}

				html += '</select>';
				html += '</div>';
				
		    html += '<div class="form-group">';
		    html +=	'<label>Amount ($)</label>';
		    html +=	'<input type="text" id="amount_formatmodel"  class="amount_format number_only form-control" name="amount" value="$'+result.transactionType.amount+'" style="width:70%!important">';
		    html +=	'</div>';
		    html +=	'</div>';
				html += '<form>';
				$('#modelBody').html(html);
				setTimeout(function(){ getCalender(); }, 500);
				// amount_format('#amount_formatmodel');
				$("#myModal").modal("show");
			}
			else
			{

			}
		},
		error: function(xhr) { // if error occured
			
		},	    
	});
}

function updateTran()
{
		var date = $('#DateUpdate').val();
		if(date.length > 0)
		{
	
			$('#page-invester-view > .container').html('');
			$.ajax({
				type : 'POST',
				url  : '<?php echo base_url()."MortgageFound/ajaxUpdateTransaction";?>',
				data : $('#UpdateTransection').serialize(),
				beforeSend: function() {
					
				},
				success : function(response){
					if(response)
					{
						message = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Data has been update successfully ! </strong>      	</div>';

						$('#MessageError1').html(message);
						alertDismiss();
					}
					else
					{
						message = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please select lender !</strong></div>';

						$('#MessageError1').html(message);
						alertDismiss();
					}
					console.log(response);
				}
			});
		}
		else
		{
				message = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Date field is required !</strong></div>';

				$('#MessageError1').html(message);
		}
}

function AddTran()
{
		var date = $('#DateAdd').val();
		if(date.length > 0)
		{
			$('#page-invester-view > .container').html('');
			$.ajax({
				type : 'POST',
				url  : '<?php echo base_url()."MortgageFound/ajaxUpdateTransaction";?>',
				data : $('#AddTransection').serialize(),
				beforeSend: function() {
					
				},
				success : function(response){
					if(response)
					{
						message = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Data has been Added successfully ! </strong>      	</div>';

						$('#MessageErrorAdd').html(message);
						alertDismiss();
					}
					else
					{
						message = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please select lender !</strong></div>';

						$('#MessageErrorAdd').html(message);
						alertDismiss();
					}
					console.log(response);
				}
			});
		}
		else
		{
				message = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Date field is required !</strong></div>';

				$('#MessageErrorAdd').html(message);
		}
}

function updateInvestorInformation()
{
	 
	 $.ajax({
		type : 'POST',
		url  : '<?php echo base_url()."MortgageFound/updateMortgage/".$lenderId;?>',
		data : $('#investorInformation').serialize(),
		beforeSend: function() {
			
		},
		success : function(response){
			if(response)
			{
				message = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Data has been update successfully ! </strong>      	</div>';

				$('#MessageError2').html(message);
				alertDismiss();
			}
			else
			{
				message = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please select lender !</strong></div>';

				$('#MessageError2').html(message);
				alertDismiss();
			}
			console.log(response);

		}
	});

}

getTransactinData();
function getTransactinData()
{
	html = '';
	$.ajax({
		type : 'POST',
		url  : '<?php echo base_url()."MortgageFound/ajaxGetTransactionAll";?>',
		data : {lenderId : '<?php echo $lenderId; ?>'},
		beforeSend: function() {
			$('#investAdd').html('<tr><td colspan="8"><i class="fa fa-spinner fa-spin" style="font-size:30px;text-align:center; display:block"></i><td></tr>');
		},
		success : function(response){

			$('#investAdd').html('');
			var data = jQuery.parseJSON(response);
			$.each(data.transactionType, function(key,value) {
			if(data.transactionType)
			{
						html = '';
					  html += '<tr id="datClone1">';
						html += '<td>'+value.date+'</td>';
						html += '<td>'+value.dateCompleted+'</td>';
						html += '<td>'+value.transactionType+'</td>';
						html += '<td>'+value.subscriptionStatus+'</td>';
						html += '<td>'+value.fundStatus+'</td>';
						html += '<td>'+value.depositType+'</td>';
						html += '<td>'+value.amount+'</td>';
						html += '<td><span><a href="javascript:void(0);" class=""><i class="fa fa-edit" onclick="ajaxgetDatamodel('+value.mortgageId+', '+value.lenderId+')"></i></a></span> &nbsp;&nbsp;&nbsp; ';
						html += '<span><a href="javascript:void(0);" class="remCF text-danger" onclick="deleteTransection('+value.mortgageId+')"><i class="fa fa-trash" onclick=""></i></a></span></td>';
						html += '</tr>';

						$('#investAdd').append(html);	
			}
			else
			{
				 $('#investAdd').append('<tr><td colspan="8" align="center">Data not found<td><tr>');
			}


		});
			$('#totalRecordRecieved').html('Total Received: '+data.recieveTotal);
			$('.totalAmountRecieved').html(data.amountTotalRecieved);
			$('.inProcessRecieved').html(data.in_processTotal);
			$('.inProcess').html(data.in_processTotal);
			$('#totalRecord').html('Total Committed: '+data.total);
			$('#totalAmount').html(data.amountTotal);
		}
	});
}

function deleteTransection(mortgageId)
{
	 $.ajax({
			type : 'POST',
			url  : '<?php echo base_url()."MortgageFound/ajaxDeleteTransaction";?>',
			data : {mortgageId : mortgageId},
			beforeSend: function() {
				
			},
			success : function(response){
				console.log(response);
			}
		});
}

</script>

</div>