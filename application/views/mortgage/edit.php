<?php $investor_type_option = $this->config->item('investor_type_option');
$account_type_option = $this->config->item('account_type_option');

if($fetch_investor_data['ach_disbrusement'] == '1')
{
	$valaccounttype = $account_type_option[$fetch_investor_data['account_type']];

}elseif($fetch_investor_data['check_disbrusement'] == '1'){
	$valaccounttype = 'Not Applicable';
}else{
	$valaccounttype = '';
}

?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
	.chosen-container-single .chosen-single {
		height: 37px!important;
	}
	.error{
		color: red;
	}
	body{
		font-size: 14!important;
	}
	.col-md-4 {
    padding-top: 10px;
    padding-bottom: 5px;
	}
	.border-top h4 {
    background-color: #428bca !important;
    height: 36px !important;
    line-height: 36px !important;
    padding-left: 15px !important;
	}
</style>
<div class="page-container" id="page-invester-view">
	<div class="container">
			<!--------------------MESSEGE SHOW-------------------------->
		
		<?php if($this->session->flashdata('error')!=''){  ?>
			<div class="alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong><?php echo $this->session->flashdata('error'); ?></strong> 
            </div>
		<?php } ?>
		<?php if($this->session->flashdata('success')!=''){  ?>
			<div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong><?php echo $this->session->flashdata('success'); ?></strong> 
            </div>
		<?php } ?>
	</div>
	<!-------------------END OF MESSEGE-------------------------->
	<!-- BEGIN PAGE HEAD -->
	<form class="form" id="saveForm" autocomplete="off" method="post" action="<?php echo base_url('MortgageFound/updateMortgage/'.$lenderData->id) ?>">
		<input type="hidden" name="saveTransation" value="1">
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="row">
			<div class="col-md-6 pull-left">
				<h1>Investor Details<small></small></h1>
			</div>
			
			<div class="col-md-6 pull-right">
				<h1 class="pull-right"><a href="<?php echo base_url('mortgageFund'); ?>" class="btn btn-primary borrower_save_button">Back</a></h1>

				<!-- <h1 class="pull-right"><input style="border-radius: 0px; margin-top: 3px;" id="submit" type="submit" class="btn btn-primary borrower_save_button" name="demo" value="Save"></h1> -->

				<h1 class="pull-right">
					<button style="border-radius: 0px; margin-top: 3px;" type="button" class="btn btn-primary borrower_save_button" data-toggle="modal" data-target="#myModalEdit">
  					<?php echo !empty($lenderData)?'Edit':'Add'; ?>
					</button>
				</h1>
				
				<h1 class="pull-right"><a style="border-radius: 0px; margin-top: 3px;" class="btn btn-danger borrower_save_button" href="<?php echo base_url('MortgageFound/dataDeleteEdit/'.$lenderData->id) ?>">Delete</a></h1>
			</div>	
		</div>		
	</div>
	
	<ul class="page-breadcrumb breadcrumb hide">
		<li>
			<a href="#">Home</a><i class="fa fa-circle"></i>
		</li>
		<li class="active">
			 Dashboard
		</li>
	</ul>

	<div class="row" id="reloadPage">
		<div class="col-md-12 border-top">
			<h2 class="display-2" style="margin: 6px 0px;">Investor: <a href="<?php echo base_url('investor_view/'.$lenderData->id); ?>"><?php echo $lenderData->name; ?></a></h2>
		</div>
		<div class="col-md-10 border-top">
			<div class="row">
				<h4 class="btn-primary">Investor Status</h4>
				<div class="col-md-4"><label>Active Funds: <span class="totalAmountRecieved">$<?php echo !empty($lenderData->amountTotal)?number_format($lenderData->amountTotal, 2) : 0.00; ?></span> </label></div>
				<div class="col-md-4"><label>Funds Processing:  <span class="inProcess">$<?php echo !empty($lenderData->amountIn)? number_format($lenderData->amountIn, 2) : 0.00; ?></span></label></div>
				<div class="col-md-4"></div>
			</div>	
		</div>
		<div class="col-md-2 border-top">
			<a style="width: 90%;margin-top:10px;" class=" pull-right btn btn-primary borrower_save_button" href="<?php echo base_url('MortgageFound/transactionData/'.$lenderData->id) ?>">Transaction Schedule</a>
		</div>
		<div class="clearfix"></div>
		<div class="col-md-10 border-top">
			<h4 class="btn-primary">Contact Information</h4>
			<?php if(!empty($contacts)){ foreach ($contacts as $key => $value) { 
					$name = '';

					if($value->contact_firstname != '')
					{
						$name = $value->contact_firstname;
					}
					if($value->contact_middlename != '')
					{
						$name = ($name != ''?$name.' '.$value->contact_middlename:$value->contact_middlename); 
					}
					if($value->contact_lastname != ''){

						$name = ($name != ''?$name.' '.$value->contact_lastname:$value->contact_lastname); 
					}
			?>
				<div class="row">
					<div class="col-md-4">Contact Name: <?php echo $name; ?></div>
					<div class="col-md-4">Phone: <?php echo $value->contact_phone; ?></div>
					<div class="col-md-4">E-Mail: <?php echo $value->contact_email; ?></div>
					<div class="clearfix"></div>
				</div>
			<?php } } ?>
		</div>
		<div class="clearfix"></div>
		<div class="col-md-10 row border-top">
			<h4 class="btn-primary">Investor Information</h4>
			<div class="col-md-4">Lender Type: &nbsp;&nbsp;<?php echo $investor_type_option[$lenderData->investor_type]; ?></div>
			<div class="col-md-4">Verivest Account #: &nbsp;&nbsp;<?php echo $lenderData->verivestAccount; ?></div>
			<div class="col-md-4"></div>
			<div class="clearfix"></div>
			<div class="col-md-4">Accredited: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo ucfirst($lenderData->accredited); ?></div>
			<div class="col-md-4">Account Status: <?php echo $lenderData->subscriptionStatus === 'In Process' ? 'Processing': $lenderData->subscriptionStatus; ?></div>
			<div class="col-md-4"></div>
			<div class="clearfix"></div>
			<div class="col-md-4">Distribution: &nbsp;&nbsp;&nbsp;<?php echo !empty($lenderData->distribution)? $lenderData->distribution : 'Select One'; ?></div>
			<div class="col-md-4">Reinvest: <?php echo $lenderData->Reinvest.'%'; ?></div>
			<?php 
						if($lenderData->distribution == 'Both')
						{
								$distribut = 100 - $lenderData->Reinvest;
						}
						elseif($lenderData->distribution == 'Distribute')
						{
								$distribut = 100;
						}
						elseif($lenderData->distribution == 'Reinvest')
						{
								$distribut = '0.00';
						}
						else
						{
								$distribut = '0.00';
						}

			?>

			<div class="col-md-4">Distribute: <?php echo $distribut.'%'; ?></div>
			<div class="clearfix"></div>
			<div class="col-md-4">Account Type: <?php echo $lenderData->accountType; ?></div>
			<div class="col-md-4"></div>
			<div class="col-md-4"></div>	
		</div>
		<div class="clearfix"></div>
		
	</form>
	</div>
</div>
<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<form class="form" id="investorInformation" autocomplete="off" method="post">
	  <div class="modal-dialog" role="document" style="width:50%">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h3 class="modal-title" id="">Investor Information</h3>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:-19px!important">
	          <span aria-hidden="true" onclick="pageReload()">&times;</span>
	        </button>
	      </div>
	      <div id="MessageError2"></div>
	      <div class="modal-body" id="modelBody1">
	      	<div class="col-md-4">
				<div class="form-group">
					<label>Account Status</label>
					<select class="selectpicker form-control" name="subscriptionStatus1">
				 		<option value="">Select One</option>
						<option <?php echo $lenderData->subscriptionStatus == "Interested"?"Selected":""; ?>>Interested</option>
						<option value="In Process" <?php echo $lenderData->subscriptionStatus == "In Process"?"Selected":""; ?>>Processing</option>
						<option <?php echo $lenderData->subscriptionStatus == "Active"?"Selected":""; ?>>Active</option>
						<option <?php echo $lenderData->subscriptionStatus == "Cancelled"?"Selected":""; ?>>Cancelled</option>
						<option <?php echo $lenderData->subscriptionStatus == "Closed"?"Selected":""; ?>>Closed</option>
					</select>
				</div>
			</div>
			<div class="clearfix"></div>
	      	<div class="col-md-6" style="margin-top: 9px;">
        		<div class="form-group">
					<label>Investor Name</label>
					<select class="form-control" name="investor_id" id="investor_id" class="">
						<option value="">Select One</option>
						<?php foreach ($lenderDataAll as $key => $value) { ?>
								<option value="<?php echo $value->id; ?>" <?php echo !empty($lenderId) && $lenderId === $value->id ? 'selected' : ''; ?> ><?php echo $value->name; ?></option>
						<?php } ?>		
					</select>
				</div>
	        </div>
	        <!-- <div class="col-md-4">
        		<div class="form-group" style="display:none;">
					<label>Lender Type</label>
					<select class="form-control" name="investor_type">

						<?php foreach ($investor_type_option as $key => $value) { ?>
								<option value="<?php echo $value->id; ?>" <?php echo $key==$lenderData->investor_type ? "selected" : ""; ?>><?php echo $value; ?></option>
						<?php } ?>		
					</select>
				</div>
	        </div> -->
	        <div class="col-md-4">
        		<div class="form-group">
					<label>Verivest Account #</label>
					<input type="text" class="form-control" name="verivestAccount" id="verivestAccount" value="<?php echo $lenderData->verivestAccount ?$lenderData->verivestAccount : ''; ?>">
				</div>
	        </div>
	        <div class="col-md-2"></div>
	        <div class="clearfix"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Accredited</label>
					<select class="selectpicker form-control" name="accredited">
								<option value="no" <?php echo $lenderData->accredited== "no" ? "selected" : ""; ?>>No</option>
								<option value="yes" <?php echo $lenderData->accredited== "yes" ? "selected" : ""; ?>>Yes</option>	
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Mortgage Fund Tab Access</label>
					<select class="selectpicker form-control" name="mortgageTabAccess">
				 		<option value="">Select One</option>
						<option value="yes" <?php echo $lenderData->mortgageTabAccess == "yes"?"Selected":""; ?>>Yes</option>
						<option value="no" <?php echo $lenderData->mortgageTabAccess == "no"?"Selected":""; ?>>No</option>
					</select>
				</div>
			</div>
			<div class="col-md-4"></div>
			<div class="clearfix"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Distribution</label>
					<select class="selectpicker form-control" name="distribution" id="distribution" onchange="changeValue()">
					 		<option value="">Select One</option>
							<option <?php echo $lenderData->distribution == "Reinvest"?"Selected":""; ?>>Reinvest</option>
							<option <?php echo $lenderData->distribution == "Distribute"?"Selected":""; ?>>Distribute</option>
							<option <?php echo $lenderData->distribution == "Both"?"Selected":""; ?>>Both</option>
					</select>
				</div>	
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Reinvest</label>
					<input type="text" class="form-control" onkeyup="changeValue()" name="Reinvest" id="Reinvest" value="<?php echo $lenderData->Reinvest > 0 ?$lenderData->Reinvest.'%' : '0.00%'; ?>">
				</div>	
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Distribute</label>
					<input type="text" readonly class="form-control" name="distribute" id="distribute" value="0.00%">
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Account Type</label>
					<select class="selectpicker" name="accountType" >
					 		<option value="">Select One</option>
					 		<option <?php echo $lenderData->accountType == "Not Applicable"?"Selected":""; ?>>Not Applicable</option>
							<option <?php echo $lenderData->accountType == "Checking"?"Selected":""; ?>>Checking</option>
							<option <?php echo $lenderData->accountType == "Savings"?"Selected":""; ?>>Savings</option>
					</select>
				</div>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-4"></div>
		  </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="pageReload()">Close</button>
	        <button type="button" class="btn btn-primary" onclick="updateInvestorInformation()">Save changes</button>
	      </div>
	    </div>
	  </div>
	</form>
</div>
<div class="clearfix"></div>
<!-- Modal -->

<div class="clearfix"></div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script> 

<div id="">
<script> 
$(document).ready(function(){
	$("#investor_id").chosen();
});
changeValue();
function changeValue()
{
		var distribut = 0;
		var distribution = $('#distribution').val();
		var Reinvest = $('#Reinvest').val().replace('%', '');
		$('#Reinvest').attr('readonly', true);

		if(distribution == 'Both')
		{
				$('#Reinvest').attr('readonly', false);
				distribut = 100 - Reinvest;
		}
		else if(distribution == 'Distribute')
		{
				distribut = 100;
				$('#Reinvest').val("0.00%");
		}
		else if(distribution == 'Reinvest')
		{
				$('#Reinvest').val('100%');
				
		}

		if(distribut > 0)
		{
				$('#distribute').val(distribut + "%"); 
		}
		else{
			$('#distribute').val("0.00%");
		}
}

function pageReload()
{
	var investor_id = $('#investor_id').val();
	if(investor_id.length > 0 )
	{
			window.location.href = "<?php echo base_url('mortgageFund/editDetails/'); ?>"+investor_id;
	}
	else
	{
		window.location.reload();
	}

}

$(document).ready(function(){
	$(".from-datepicker").datepicker({          
	format: 'mm-dd-yy' //can also use format: 'dd-mm-yyyy'     
	});
});

getCalender();

function getCalender(){ 
	$(".from-datepicker1").datepicker("destroy");
    $(".from-datepicker1").datepicker({ dateFormat: "mm-dd-yy" }).val();		
};

$('.amount_format').change(function(){
	var a = this.value;
		var a = replace_dollar(a);
		if(a == '')
		{
			a = 0;
		}
		if(isNaN(a) == true)
		{
			a = 0;
		}
		a = parseFloat(a);
		this.value = '$'+ number_format(a);    
});

  
function amount_format(that){
			
		var a = that.value;
		var a = replace_dollar(a);
		if(a == '')
		{
			a = 0;
		}
		if(isNaN(a) == true)
		{
			a = 0;
		}
		a = parseFloat(a);
		that.value = '$'+ number_format(a);
}; 

 
function replace_dollar(n)
{
	var a = n.replace('$', '');
	var b = a.replace(',', '');
	var b = b.replace(',', '');
	var b = b.replace(',', '');
	var b = b.replace(',', '');
	var c = b.replace('%', '');
	return c;
}

function number_format(n) {
    return n.toFixed(2).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
} 
</script>

<script type="text/javascript">

alertDismiss();
function alertDismiss(){

		$(".alert-dismissible").fadeTo(2000, 3000).slideUp(3000, function(){
		    $(".alert-dismissible").slideUp(3000);
		});
		setTimeout(function(){ $('#page-invester-view > .container').html('')}, 30000);
};



function updateInvestorInformation()
{
	 var investor_id = $('#investor_id').val();

	 if(investor_id.length > 0){
		$.ajax({
			type : 'POST',
			url  : '<?php echo base_url()."MortgageFound/updateMortgage/"; ?>'+investor_id,
			data : $('#investorInformation').serialize(),
			beforeSend: function() {
				
			},
			success : function(response){
				if(response)
				{
					message = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Data has been update successfully ! </strong>      	</div>';

					$('#MessageError2').html(message);
					alertDismiss();
				}
				else
				{
					message = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please select lender !</strong></div>';

					$('#MessageError2').html(message);
					alertDismiss();
				}
				console.log(response);

			}
		});
	}
	else
	{
		message = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please select lender !</strong></div>';
	}

}
</script>

</div>