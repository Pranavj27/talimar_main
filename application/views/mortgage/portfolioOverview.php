<?php $loan_type_option = $this->config->item('loan_type_option');
	$property_type_option = $this->config->item('property_modal_property_type');
	$all_countary = $this->config->item('usa_city_county');
	
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
	tbody > tr >td{
		    text-align: left!important;
	}
	tfoot > tr >th{
		    text-align: left!important;
	}
</style>
<div class="page-container" id="page-invester-view">
	<div class="container">
		<div class="page-title">
			<h1 style="margin-left:14px !important;">Portfolio Overview</h1>
		</div>
	</div>
	<!-------------------END OF MESSEGE-------------------------->
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
				
		<?php
		
		if($btn_name == 'mortgageFund'){
			$btn_class = 'style ="display:block;" ';
		}else{
			$btn_class = 'style ="display:none;" ';			
		}
		$page_url = $_SERVER['REQUEST_URI'];
		?>
	</div>
	
	<ul class="page-breadcrumb breadcrumb hide">
		<li>
			<a href="#">Home</a><i class="fa fa-circle"></i>
		</li>
		<li class="active">
			 Dashboard
		</li>
	</ul>
	<div class="row" style="min-height: 400px;">
		<div class="col-md-4">
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
				<thead>
					<tr class="thead_dark_blue">
						<th colspan="3">Portfolio Overview</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td># of Loans:</td>
						<td><?php echo $investorData->totalLoan;  ?></td>
					</tr>
					<tr>
						<td>$ of Active Loans:</td>
						<td>$<?php echo number_format((double)$investorData->totalAmount, 2); ?></td>
					</tr>
					<tr>
						<td>$ Active Capital:</td>
						<td>$<?php echo number_format($totalAvailable['amountTotalActive'], 2); ?></td>
					</tr>
					<tr>
						<td>$ Capital in Processing:</td>
						<td>$<?php echo number_format($totalAvailable['amountTotalInProcess'], 2); ?></td>
					</tr>
					<tr>
						<td>Avg. Yield:</td>
						<td><?php $t = (double)$investorData->totalpayment*12; $t = $t/(double)$investorData->totalAmount; echo $t = number_format($t*100, 3); ?>%</td>
					</tr>
					<tr>
						<td>Avg. Loan to Value:</td>
						<td><?php  echo number_format($totalAvailable['LTV'] / $investorData->totalLoan, 2); ?>%</td>
					</tr>
					<tr>
						<td>Avg. Term:</td>
						<td><?php echo round($investorData->avgTerm); ?> Months</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-4">
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
				<thead>
					<tr class="thead_dark_blue">
						<th colspan="4">Ownership Type</th>
					</tr>
					<tr>
						<th>Type</th>
						<th>Count</th>
						<th>$ Amount</th>
						<th>% of Investments</th>					
					</tr>
				</thead>
				<tbody>
					
					<tr>
						<td>Whole Interest</td>
						<td><?php echo $totalAvailable['countDatatotalWhole']; ?></td>
						<td>$<?php echo number_format($totalAvailable['totalWhole'], 2); ?></td>
						<td><?php echo $totalAvailable['totalWholeLoanAmount'] > 0 ? number_format(($totalAvailable['totalWhole'] / $investorData->totalAmount) * 100, 2) : 0.00; ?>%</td>
					</tr>
					<tr>
						<td>Fractional Interest</td>
						<td><?php echo $totalAvailable['countDataFractional']; ?></td>
						<td>$<?php echo number_format($totalAvailable['totalFractional'], 2); ?></td>
						<td><?php echo $totalAvailable['totalFractionalLoanAmount'] > 0 ? number_format(($totalAvailable['totalFractional'] / $investorData->totalAmount) * 100, 2) : 0.00; ?>%</td>
					</tr>
					
					
				</tbody>
				<tfoot class="foot_light_blue">
					<tr>
						<th>Total</th>
						<th><?php echo $totalAvailable['countDatatotalWhole'] + $totalAvailable['countDataFractional']; ?></th>
						<th>$<?php echo number_format($totalAvailable['totalWhole'] + $totalAvailable['totalFractional'], 2); ?></th>
						<th><?php echo number_format((($totalAvailable['totalWhole'] + $totalAvailable['totalFractional']) / $investorData->totalAmount)* 100, 2); ?>%</th>
					</tr>
				</tfoot>
			</table>
		</div>
		<div class="col-md-4">
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
				<thead>
					<tr class="thead_dark_blue">
						<th colspan="3">Loan Type</th>
					</tr>
					<tr>
						<th>Type</th>
						<th>$ Amount</th>
						<th>% of Portfolio</th>					
					</tr>
				</thead>
				<tbody>
					<?php $totalAmount = 0.00; $lender_ratessss = 0.0; 
					if($investorTypeData){ foreach ($investorTypeData as $key => $valueType) { 
						$lender_ratessss =  (double)$valueType->investment / (double)$investorData->totalAmount;

						$totalAmount = $totalAmount + (double)$valueType->investment; 
						
					?>
					<tr>
						<td><?php echo $loan_type_option[$valueType->loan_type];  ?></td>
						<td>$<?php echo number_format((double)$valueType->investment, 2);  ?></td>
						<td><?php echo number_format($lender_ratessss*100, 2);  ?>%</td>
					</tr>
					<?php } } else { ?>
						<tr><td colspan="3">Data not Found</td></tr>
					<?php } ?>
				</tbody>
				<tfoot class="foot_light_blue">
					<tr>
						<th>Total: <?php echo COUNT($investorTypeData);  ?></th>
						<th>$<?php echo number_format($totalAmount, 2);  ?></th>
						<th><?php echo number_format(($totalAmount/(double)$investorData->totalAmount)*100, 2);  ?>%</th>
					</tr>
				</tfoot>
			</table>
		</div>
		
		<div class="clearfix"></div>

		<div class="col-md-4">
			
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
				<thead>
					<tr class="thead_dark_blue">
						<th colspan="3">Property Type</th>
					</tr>
					<tr>
						<th>Type</th>
						<th>$ Amount</th>
						<th>% of Portfolio</th>					
					</tr>
				</thead>
				<tbody>
				
					<?php $totalAmount = 0.00; $lender_ratessss = 0.0; 
						if($propertyType){ foreach ($propertyType as $key => $valueType) { 
						$lender_ratessss =  (double)$valueType->investment / (double)$investorData->totalAmount;
						$totalAmount = $totalAmount + (double)$valueType->investment;
					?>
					<tr>
						<td><?php echo $property_type_option[$valueType->property_type];  ?></td>
						<td>$<?php echo number_format((double)$valueType->investment, 2);  ?></td>
						<td><?php echo number_format($lender_ratessss*100, 2);  ?>%</td>
					</tr>
					<?php } } else { ?>
						<tr><td colspan="3">Data not Found</td></tr>
					<?php } ?>
				
				</tbody>
				<tfoot class="foot_light_blue">
					<tr>
						<th>Total: <?php echo COUNT($investorTypeData);  ?></th>
						<th>$<?php echo number_format($totalAmount, 2);  ?></th>
						<th><?php echo number_format(($totalAmount/(double)$investorData->totalAmount)*100, 2);  ?>%</th>
					</tr>
				</tfoot>
			</table>
		</div>

		<div class="col-md-4 pull-left">
			
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
				<thead>
					<tr class="thead_dark_blue">
						<th colspan="3">County</th>
					</tr>
					<tr>
						<th>County</th>
						<th>$ Amount</th>
						<th>% of Portfolio</th>					
					</tr>
				</thead>
				<tbody>
				
					<?php $totalAmount = 0.00; $lender_ratessss = 0.0; 
						if($propertyCountry){ foreach ($propertyCountry as $key => $valueType) { 
							$lender_ratessss =  (double)$valueType->investment / (double)$investorData->totalAmount;
							$totalAmount = $totalAmount + (double)$valueType->investment;
						
					?>
					<tr>
						<td><?php echo $all_countary[$valueType->country];  ?></td>
						<td>$<?php echo number_format((double)$valueType->investment, 2);  ?></td>
						<td><?php echo number_format($lender_ratessss*100, 2);  ?>%</td>
					</tr>
					<?php } } else { ?>
						<tr><td colspan="3">Data not Found</td></tr>
					<?php } ?>
				
				</tbody>
				<tfoot class="foot_light_blue">
					<tr>
						<th>Total: <?php echo COUNT($investorTypeData);  ?></th>
						<th>$<?php echo number_format($totalAmount, 2);  ?></th>
						<th><?php echo number_format(($totalAmount/(double)$investorData->totalAmount)*100, 2);  ?>%</th>
					</tr>
				</tfoot>
			</table>
		</div>

		<div class="col-md-4">
			
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
				<thead>
					<tr class="thead_dark_blue">
						<th colspan="3">Payoff Demands</th>
					</tr>
					<tr>
						<th>Loan</th>
						<th>$ Amount</th>
						<th>Payoff Date</th>					
					</tr>
				</thead>
				<tbody>
				
					<?php $totalAmount = 0.00;
						if($loanPayoff){ foreach ($loanPayoff as $key => $valuePayoff) { 

						$address = '';
				
						if(!empty($valuePayoff->property_address))
						{
							$address .= $valuePayoff->property_address;
						}

						if(!empty($valuePayoff->unit))
						{
							$address .=!empty($address)?' '.$valuePayoff->unit : ' '.$valuePayoff->unit;
						}
							
						
						$totalAmount = $totalAmount + (double)$valuePayoff->investment;
					?>
					<tr>
						<td><a href="<?php echo base_url('load_data/'.$valuePayoff->id); ?>"><?php echo $address;  ?></a></td>
						<td>$<?php echo number_format((double)$valuePayoff->investment, 2);  ?></td>
						<td><?php echo !empty($valuePayoff->payoff_date) && $valuePayoff->payoff_date !== '00-00-0000' ?date('m-d-Y', strtotime($valuePayoff->payoff_date)):'00-00-0000';  ?></td>
					</tr>
					<?php } } else { ?>
						<tr><td colspan="3">Data not Found</td></tr>
					<?php } ?>
				
				</tbody>
				<tfoot class="foot_light_blue">
					<tr>
						<th>Total: <?php echo COUNT($loanPayoff);  ?></th>
						<th>$<?php echo number_format($totalAmount, 2);  ?></th>
						<th></th>
					</tr>
				</tfoot>
			</table>
		</div>
		<div class="clearfix"></div>

		<div class="col-md-4">
			
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
				<thead>
					<tr class="thead_dark_blue">
						<th colspan="3">Upcoming Payoffs</th>
					</tr>
					<tr>
						<th>Street Address</th>
						<th>$ Amount</th>
						<th>Payoff Date</th>					
					</tr>
				</thead>
				<tbody>
				
					<?php $totalAmount = 0.00;
						if($loanPayoff){ foreach ($loanPayoff as $key => $valuePayoff) { 

						if($valuePayoff->payoff_date >= date('Y-m-d')){

						$address = '';
				
						if(!empty($valuePayoff->property_address))
						{
							$address .= $valuePayoff->property_address;
						}

						if(!empty($valuePayoff->unit))
						{
							$address .=!empty($address)?' '.$valuePayoff->unit : ' '.$valuePayoff->unit;
						}
							
						
						$totalAmount = $totalAmount + (double)$valuePayoff->investment;
					?>
					<tr>
						<td><a href="<?php echo base_url('load_data/'.$valuePayoff->id); ?>"><?php echo $address;  ?></a></td>
						<td>$<?php echo number_format((double)$valuePayoff->investment, 2);  ?></td>
						<td><?php echo !empty($valuePayoff->payoff_date) && $valuePayoff->payoff_date !== '00-00-0000' ?date('m-d-Y', strtotime($valuePayoff->payoff_date)):'00-00-0000';  ?></td>
					</tr>
					<?php } } } else { ?>
						<tr><td colspan="3">Data not Found</td></tr>
					<?php } ?>
				
				</tbody>
				<tfoot class="foot_light_blue">
					<tr>
						<th>Total: <?php echo COUNT($loanPayoff);  ?></th>
						<th>$<?php echo number_format($totalAmount, 2);  ?></th>
						<th></th>
					</tr>
				</tfoot>
			</table>
		</div>

		<div class="col-md-4">
			
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
				<thead>
					<tr class="thead_dark_blue">
						<th colspan="3">Top 5 Holdings</th>
					</tr>
					<tr>
						<th>Address</th>
						<th>$ Investment</th>
						<th>% of Portfolio</th>					
					</tr>
				</thead>
				<tbody>
				
					<?php $totalAmount = 0.00;
						if($topLoan){ foreach ($topLoan as $key => $valueLoan) { 

						

						$address = '';
				
						if(!empty($valueLoan->property_address))
						{
							$address .= $valueLoan->property_address;
						}

						if(!empty($valueLoan->unit))
						{
							$address .=!empty($address)?' '.$valueLoan->unit : ' '.$valueLoan->unit;
						}
							
						
						$totalAmount = $totalAmount + (double)$valueLoan->investment;
					?>
					<tr>
						<td><a href="<?php echo base_url('load_data/'.$valueLoan->id); ?>"><?php echo $address;  ?></a></td>
						<td>$<?php echo number_format((double)$valueLoan->investment, 2);  ?></td>
						<td><?php echo number_format(((double)$valueLoan->investment/$investorData->totalAmount)*100, 2);  ?>%</td>
					</tr>
					<?php } }  else { ?>
						<tr><td colspan="3">Data not Found</td></tr>
					<?php } ?>
				
				</tbody>
				<tfoot class="foot_light_blue">
					<tr>
						<th>Total: <?php echo COUNT($topLoan);  ?></th>
						<th>$<?php echo number_format($totalAmount, 2);  ?></th>
						<th><?php echo number_format(($totalAmount/$investorData->totalAmount)*100, 2);  ?>%</th>
					</tr>
				</tfoot>
			</table>
		</div>

		<div class="col-md-4">
			
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
				<thead>
					<tr class="thead_dark_blue">
						<th colspan="3">Upcoming Closings</th>
					</tr>
					<tr>
						<th>Address</th>
						<th>Amount</th>
						<th>Closing Date</th>					
					</tr>
				</thead>
				<tbody>
				
					<?php  $totalAmount = 0.00;
						if($piplineLoan){ foreach ($piplineLoan as $key => $valuePipeline) {
							$address = '';
					
							if(!empty($valuePipeline->property_address))
							{
								$address .= $valuePipeline->property_address;
							}

							if(!empty($valuePipeline->unit))
							{
								$address .=!empty($address)?' '.$valuePipeline->unit : ' '.$valuePipeline->unit;
							}

							if(!empty($valuePipeline->loan_funding_date) && $valuePipeline->loan_funding_date != '0000-00-00'){
								$explode = explode('-', $valuePipeline->loan_funding_date);
								$fundDate = $explode[1].'-'.$explode[2].'-'.$explode[0];
							}
							else{
								$fundDate = '';
							}
							
						
							$totalAmount = $totalAmount + (double)$valuePipeline->investment;
					?>
					<tr>
						<td><a href="<?php echo base_url('load_data/'.$valuePipeline->id); ?>"><?php echo $address;  ?></a></td>
						<td>$<?php echo number_format((double)$valuePipeline->investment, 2);  ?></td>
						<td><?php echo $fundDate; ?></td>
					</tr>
					<?php } }  else { ?>
						<tr><td colspan="3">Data not Found</td></tr>
					<?php } ?>
				
				</tbody>
				<tfoot class="foot_light_blue">
					<tr>
						<th>Total: <?php echo COUNT($piplineLoan);  ?></th>
						<th>$<?php echo number_format($totalAmount, 2);  ?></th>
						<th></th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
<div class="clearfix"></div>
