
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">

.chosen-container-single .chosen-single{
	height: 34px!important;
	border: 1px solid #e5e5e5!important;
	padding-top: 4px!important;
}
.chosen-container-single .chosen-single div{
	top: -6px!important;
}
span.selection{

}
</style>



<div class="page-container" id="page-invester-view">
	<div class="container">
		
		<!--------------------MESSEGE SHOW-------------------------->
		<div id="MessageError">

			<?php if($this->session->flashdata('error')!=''){  ?>
			<div class="alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong><?php echo $this->session->flashdata('error'); ?></strong> 
            </div>
			<?php } ?>
			<?php if($this->session->flashdata('success')!=''){  ?>
				<div class="alert alert-success alert-dismissible">
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <strong><?php echo $this->session->flashdata('success'); ?></strong> 
	            </div>
			<?php } ?>
			
		</div>
		
	</div>
	<!-------------------END OF MESSEGE-------------------------->
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">

	</div>
	
	<ul class="page-breadcrumb breadcrumb hide">
		<li>
			<a href="#">Home</a><i class="fa fa-circle"></i>
		</li>
		<li class="active">
			 Dashboard
		</li>
	</ul>
	<div class="row">
		<div class="col-md-12" style="min-height: 400px;">
			<div class="page-title">
				<h1>Investor Schedule<small></small></h1>
			</div>
			<form id = "lender_form"  method = "post" action = "<?php echo $page_url;?>">
				<div class="form-group" style="float:left;width: 100%;">
					<div class="col-md-2">
						<b>Search:</b>&nbsp;<br>
						<select name="name" id="nameLender" class="form-control" onchange="getLenderData()">
							
						</select>
					</div>
					<div class="col-md-2">
						<b>Account Status:</b>&nbsp;
						<select id="subscriptionStatus" name="subscriptionStatus" class="form-control selectpicker" onchange="getLenderData()">
							<option value="">Select All</option>
							<option>Interested</option>
							<option value="In Process">Processing</option>
							<option>Active</option>
							<option>Cancelled</option>
							<!-- <option>Closed</option> -->
						</select>
					</div>
					<div class="col-md-2">
						<b>Accredited:</b>&nbsp;
						<select class="form-control selectpicker" name="accredited" id="accredited" onchange="getLenderData()">
							<option value="">Select All</option>
							<option value="yes">Yes</option>
							<option value="no">No</option>	
						</select>
					</div>
					<div class="col-md-2">
						<b>Distribution:</b>&nbsp;
						<select class="form-control selectpicker" name="distribution" id="distribution" onchange="getLenderData()">
							<option value="">Select All</option>
							<option value="Distribute">Distribute</option>
							<option value="Reinvest">Reinvest</option>
							<option value="Both">Both</option>	
						</select>
					</div>
					<div class="col-md-2">
						<b>Transaction Schedule:</b>&nbsp;
						<select class="form-control selectpicker" name="transactionStatus" id="transactionStatus" onchange="getLenderData()">
							<option value="">Select All</option>
							<option value="Processing">Processing</option>
							<option value="Complete">Complete</option>	
						</select>
					</div>
					
				
						<div class="col-md-2 row">
							<br>
							<a href="<?php echo base_url("MortgageFound/editDetails"); ?>" style="margin-left:10px; width: 100px;" class="btn btn-md btn-primary" >Add Lender</a>
						</div>
					
				</div>
			</form>
			<div class="clearfix"></div>
			<div class="row" id="ajax_table_mortgage_div">
				<table id="ajax_table_mortgage" class="table-responsive table table-bordered table-striped table-condensed flip-content dataTable">
		
				
					<thead style="background-color:#e8e8e8">
						<tr>
							<th>Investor Name</th>
							<th>Status</th>
							<th>Investor Type</th>
							<th>Distribution</th>
							<th>Contact Name</th>
							<th>Phone</th>
							<th>E-Mail</th>
							<!-- <th>Processing ($)</th> -->
							<th>Amount ($)</th>
							<th>View</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
					<tfoot id="footerData" style="background-color:#e8e8e8">
						<tr>
							<th id="TotalRecord">Total : </th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<!-- <th id="amountIn"></th> -->
							<th id="amountActive"></th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</div>	
				
			
		</div>
		<div class="clearfix"></div>
		
	</div>
</div>
<div class="clearfix"></div>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript">

	alertDismiss();
	function alertDismiss(){

		$(".alert-dismissible").fadeTo(2000, 3000).slideUp(3000, function(){
		    $(".alert-dismissible").slideUp(3000);
		});
	};


	function addLenderDb()
	{
		
		// $( "#nameLender" ).val('').trigger('change');

		var lenderId = $('#investor_id1').val();
		if(lenderId != '')
		{
			
			var AddMortgage = $("#AddMortgage");
		 	
		 	$.ajax({
				type : 'POST',
				url  : '<?php echo base_url()."MortgageFound/ajaxFilter";?>',
				data : {'investor_id' : lenderId},
				beforeSend: function() {
				
					$('tbody#LenderPaymentDetailsHistory').html('<tr><td colspan="8"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></td></tr>');
				},
				success : function(result){
					
				},
				error: function(xhr) { // if error occured
			        getLenderData();
			    },
			    complete: function() {

			    	message = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Data has been add successfully ! </strong>      	</div>';

					$('#MessageError').html(message);
					selectLender();
					getLenderData();
					alertDismiss();
					$('#investor_id1 > option:first').attr('selected',true);
			    },	    
			});
		}
		else{
				message = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please select lender !</strong></div>';

				$('#MessageError').html(message);
				alertDismiss();
		}
				
	}

function getLenderData()
{


	var dispatching_list_all = '#ajax_table_mortgage';
	$(dispatching_list_all).DataTable();

	$('#ajax_table_mortgage').DataTable().clear().destroy();

	if ( $.fn.DataTable.isDataTable('#ajax_table_mortgage') ) {
  			$('#ajax_table_mortgage').DataTable().destroy();
	}
		
	if( $( dispatching_list_all ).length ){	

		
		var nameLender = $('#nameLender').val();
		var subscriptionStatus = $('#subscriptionStatus').val();
		var accredited = $('#accredited').val();
		var distribution = $('#distribution').val();
		var transactionStatus = $('#transactionStatus').val();
		var total = 0;
		var amountT = 0;
		var amountInPro = 0;

		$dis_list_all = $( dispatching_list_all ).DataTable({
		processing: true,
		searching : false,
		serverSide: true,
		pageLength: 10,
		ajax: {

				type : 'POST',
		    	dataSrc:"data",
				data:{'investor_id' : nameLender, 'distribution' : distribution, 'subscriptionStatus' : subscriptionStatus, 'transactionStatus': transactionStatus, 'accredited' : accredited, 'orderby' : 'Desc'},
		    	url: '<?php echo base_url()."MortgageFound/ajaxGetLender1";?>',
			    dataFilter: function(data){
			    	
					var json = JSON.parse( data );
					total = json.total;
					amountT = json.amountT;
					amountInPro = json.amountInPro;
					json.recordsTotal = json.total;
					json.recordsFiltered = json.total;
					json.data = json.data;
					return JSON.stringify( json );
				},
			},
			columns: [				        
		        { data: 'nameLender' },
		        { data: 'subscriptionStatus' },
		        { data: 'investorType' },
		        { data: 'distribution' },
		        { data: 'name' },
		        { data: 'phone' },				        
		        { data: 'email' },				        
		        { data: 'amountTotal' },
		        { data: 'edit' }
		    ],
			"aoColumnDefs": [
		        { "bSortable": false, "aTargets": [ 0,3,4,5,8 ] }, 
		        { "bSearchable": false, "aTargets": [1,2,3,4,5,6,7,8 ] }
		    ],
			"language": { loadingRecords: '&nbsp;&nbsp;', processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
			"footerCallback": function ( tfoot, data, start, end, display ) {
                                            $(tfoot).find('th').eq(0).html( "Total: "+ total);
                                            // $(tfoot).find('th').eq(6).html( amountInPro );
                                            $(tfoot).find('th').eq(7).html( amountT );
                                        },        
			drawCallback: function( settings ) {
			        
			}
		});
	}
	

}

function deleteRow(id)
{
 	$.ajax({
		type : 'POST',
		url  : '<?php echo base_url()."MortgageFound/ajaxDelete";?>',
		data : {'id':id},
		beforeSend: function() {
			$('tbody#LenderPaymentDetailsHistory').html('<tr><td colspan="8"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></td></tr>');
		},
		success : function(result){
			if(result)
			{
				message = '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Data has been delete successfully ! </strong>      	</div>';
			 
			}
			else
			{
				message = '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Data has been delete failed ! </strong>      	</div>';
			}
			$('#MessageError').html(message);
			getLenderData();
			selectLender();
			alertDismiss();
		}	    
	});
}
selectLender();
function selectLender()
{
	$('#investor_id1').html('');
	$('#nameLender').html('');


	html = '';
	html += '<option value="">Select All</option>';

	html1 = '';


 	$.ajax({
		type : 'POST',
		url  : '<?php echo base_url()."MortgageFound/selectOption";?>',
		delay : 3000,
		beforeSend: function() {

		},
		success : function(result){
			var data = jQuery.parseJSON(result);
			$.each(data.selectLender, function(key,value) {

				html += '<option value="'+value.id+'">'+value.text+'</td>';
				
			});

			$('#nameLender').append(html);
			$("#nameLender").chosen();


			$.each(data.lenderDataAll, function(key,value) {

				html1 += '<option value="'+value.id+'">'+value.text+'</td>';
				
			});
			$('#investor_id1').append(html1);
			$("#investor_id1").chosen();
			getLenderData();
			$('#investor_id1').trigger("chosen:updated");
			$('#nameLender').trigger("chosen:updated");		
		}	    
	});
}
</script>

