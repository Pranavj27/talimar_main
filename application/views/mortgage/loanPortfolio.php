 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
	tbody > tr >td{
		    text-align: left!important;
	}
	tfoot.foot_light_blue > tr >th{
		    text-align: left!important;
		    padding-left: 10px!important;
	}
</style>
<div class="page-container" id="page-invester-view">
	<div class="container">
		<div class="page-title">
			<h1 style="margin-left:14px !important;">Loan Portfolio</h1>
		</div>
	</div>
	<!-------------------END OF MESSEGE-------------------------->
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
				
		<?php
		
		if($btn_name == 'mortgageFund'){
			$btn_class = 'style ="display:block;" ';
		}else{
			$btn_class = 'style ="display:none;" ';			
		}
		$page_url = $_SERVER['REQUEST_URI'];
		?>
	</div>
	
	<ul class="page-breadcrumb breadcrumb hide">
		<li>
			<a href="#">Home</a><i class="fa fa-circle"></i>
		</li>
		<li class="active">
			 Dashboard
		</li>
	</ul>
	<div class="row" style="min-height: 300px;">
		<div class="col-md-12">
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" id="loan_payment_reminder">
				<thead style="background-color:#e8e8e8">
					<tr>
						<th>Property Address</th>
						<th>Borrower Name</th>
						<th>Loan Amount</th>
						<th>Investment</th>
						<th>% of Loan</th>
						<th>% of Portfolio</th>
						<th>% LTV</th>
						<th>% Rate</th>
						<th>$ Payment</th>
					</tr>
				</thead>
				<tbody>
					<?php $outstandinTotal = 0.00; $inveTotal = 0.00; $totalPayment = 0.00; $avgData = 0.00; $yieldTotal = 0.00; if($investorData){ foreach ($investorData as $key => $value) { 
						
						if ($value->servicing_lender_rate == 'NaN' || $value->servicing_lender_rate == '' || $value->servicing_lender_rate == '0.00') {
							$lender_ratessss = $value->invester_yield_percent;
						} else {
							$lender_ratessss = $value->servicing_lender_rate;
						}

						$yieldTotal = $yieldTotal + $lender_ratessss;

						$outstandinTotal = $outstandinTotal + $value->current_balance;
						$inveTotal = $inveTotal + $value->investment;
						
						$avgData = $avgData + get_LTV_value($value->talimar_loan, $value->loan_amount);

						$address = '';
				
						if(!empty($value->property_address))
						{
							$address .= $value->property_address;
						}

						if(!empty($value->unit))
						{
							$address .=!empty($address)?' '.$value->unit : ' '.$value->unit;
						}

						if(!empty($value->city))
						{
							$address .= !empty($address)?'; '.$value->city : $value->city;
						}

						if(!empty($value->state))
						{
							$address .= !empty($address)?', '.$value->state : $value->state;
						}

						if(!empty($value->zip))
						{
							$address .= !empty($address)?' '.$value->zip : $value->zip;
						}


						?>
						<tr>
							<td><a href="<?php echo base_url('load_data/'.$value->id); ?>"><?php echo $address; ?></a></td>
							<td><?php echo $value->b_name; ?></td>
							<td>$<?php echo number_format((double)$value->current_balance, 2); ?></td>
							<td>$<?php echo number_format((double)$value->investment, 2); ?></td>
							<td><?php echo number_format(((double)$value->investment / (double)$value->current_balance)*100, 2) ; ?>%</td>
							<td><?php echo number_format(((double)$value->investment / (double)$investorTotal->totalInvestor)*100, 3) ; ?>%</td>
							<td><?php echo number_format(get_LTV_value($value->talimar_loan, $value->loan_amount), 2).'%'; ?></td>
							<td><?php echo number_format($lender_ratessss, 3); ?>%</td>
							<td>$<?php echo number_format((((double)$value->investment * (double)$lender_ratessss)/12)/100, 2) ; ?></td>

						</tr>
			
					<?php $totalPayment = $totalPayment + ((((double)$value->investment * (double)$lender_ratessss)/12)/100); } } ?>
					
				</tbody>
				<tfoot class="foot_light_blue">
					<tr>
							<th>Total <?php echo COUNT($investorData); ?></th>
							<th></th>
							<th>$<?php echo number_format((double)$outstandinTotal, 2); ?></th>
							<th>$<?php echo number_format((double)$inveTotal, 2); ?></th>
							<th><?php echo number_format(($inveTotal / $outstandinTotal)*100, 2) ; ?>%</th>
							<th><?php echo number_format(($inveTotal / $inveTotal)*100, 2) ; ?>%</th>
							<th><?php echo number_format($avgData / COUNT($investorData), 2); ?>%</th>
							<th><?php echo number_format((($totalPayment*12) / $inveTotal)*100, 3); ?>%</th>
							<th>$<?php echo number_format($totalPayment, 2); ?></th>

						</tr>
				</tfoot>
			</table>
		</div>
		<div class="clearfix"></div>		
		
	</div>
</div>
<div class="clearfix"></div>
<script type="text/javascript">
$(document).ready(function(){
	var table = $('#loan_payment_reminder').DataTable( {
	    responsive: true,
	   	searching: false
	});
});
</script>