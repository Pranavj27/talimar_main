<?php
$loan_type_option = $this->config->item('loan_type_option');
$loan_status_option = $this->config->item('loan_status_option');
$closing_status_option = $this->config->item('closing_status_option');
$serviceing_reason_option = $this->config->item('serviceing_reason_option');


?>
<!-- BEGIN CONTENT -->
<style>
.rc_class{
	padding:0px;
}
.rc_class td{
	text-align:left;
}
#LoadingImage{
	position : absolute;
	top:15%;
	left:30%;
	display: block;
}
.red{

color:red;
}

tr.foot_light_blue {
    background: rgba(75, 141, 248, 0.76);
    color: white;
}
</style>
	<div class="page-container">
			<!-- BEGIN PAGE HEAD -->

		<div class="container">
					<!--<div id="LoadingImage" style="display:none">
					  <img src="<?php echo base_url() ?>assets/global/img/loder.gif"/>
					</div>-->
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1 style="margin-left:14px !important;">Portfolio Dashboard </h1>
				</div>
				


			<!-------------- PIPELINE TABLE STARTS ------------->

				<div class="row rc_class">
					<div class="col-md-12">
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead >
								<tr class="thead_dark_blue">
									<th colspan="9">Pipeline</th>
								</tr>
								<tr>
									<!--<th>Loan #</th>-->
									<th>Property Address</th>

									<th>Borrower</th>
									<th>Loan Amount</th>
									<!-- <th>Unfunded</th> -->
									<th>Loan Type</th>
									<th>Interest Rate</th>
									<th>LTV</th>
									<!-- <th>Term</th> -->
									<th>Closing Status</th>
									<th>Loan Officer</th>
									<th>Closing Date</th>


								</tr>
							</thead>
							<tbody>

								<?php

/************/
if (isset($all_pipeline_data) && is_array($all_pipeline_data)) {

	foreach ($all_pipeline_data as $key => $row) {
		if ($all_pipeline_data[$key]['talimar_loan']) {
			$calculate_lender_points = $all_pipeline_data[$key]['loan_amount'] * ($all_pipeline_data[$key]['lender_fee'] / 100);

			$total_pipeline_available = $total_pipeline_available + $all_pipeline_data[$key]['avilable'];

			$position = $all_pipeline_data[$key]['position'];

			if ($position == '2') {

				
				$loan_to_value = ($all_pipeline_data[$key]['select_ecum'] + $all_pipeline_data[$key]['loan_amount']) / $all_pipeline_data[$key]['uv_value'] * 100;
			} else {

				$loan_to_value = ($all_pipeline_data[$key]['select_ecum'] + $all_pipeline_data[$key]['loan_amount']) / $all_pipeline_data[$key]['uv_value'] * 100;
			}
			$loan_to_valuesd = is_infinite($loan_to_value) ? 0 : $loan_to_value;
			$total_interst_rate += $all_pipeline_data[$key]['intrest_rate'];
			$total_loan_amount += $all_pipeline_data[$key]['loan_amount'];
			$total_ltv += $loan_to_valuesd;
			$total_term += $all_pipeline_data[$key]['term_month'];
			$total_avi_balance += $all_pipeline_data[$key]['avilable'];
			?>
									<tr>
										<!--<td style = "text-align: left;"></td>-->
										<?php /*<td style = "text-align: left;"><a href="<?php echo base_url(); ?>load_data/<?php echo $all_pipeline_data[$key]['loan_id']; ?>"><?php echo $all_pipeline_data[$key]['property_address'] ? $all_pipeline_data[$key]['property_address'] : 'No Property Entered'; ?></a>*/?>
										<td style = "text-align: left;"><a href="<?php echo base_url(); ?>load_data/<?php echo $all_pipeline_data[$key]['loan_id']; ?>"><?php echo $all_pipeline_data[$key]['full_property_address'] ? $all_pipeline_data[$key]['full_property_address'] : 'No Property Entered'; ?></a>

										</td>
										<td style = "text-align: left;" >
											<a href="<?php echo base_url(); ?>borrower_view/<?php echo $all_pipeline_data[$key]['borrower_id']; ?>"><?php echo $all_pipeline_data[$key]['borrower_name']; ?></a>
										</td>
										<td style = "text-align: left;">
											$<?php echo number_format($all_pipeline_data[$key]['loan_amount']); ?>
										</td>
										<?php /*<td>$<?php echo number_format($all_pipeline_data[$key]['avilable']); ?></td>*/?>
										<td >
											<?php echo $loan_type_option[$all_pipeline_data[$key]['l_loan_type']]; ?>
										</td>

										<td style = "text-align: left;" ><?php echo number_format($all_pipeline_data[$key]['intrest_rate'], 3) ?>%</td>


										<td style = "text-align: left;" class="valuesd_ltv">
											<?php 
											$valuesd_ltv = get_LTV_value($all_pipeline_data[$key]['talimar_loan'], $all_pipeline_data[$key]['loan_amount']);
											echo number_format($valuesd_ltv, 2);
											//echo number_format($loan_to_valuesd, 2); 
											?>% </td>
										<?php /*<td style = "text-align: left;" ><?php echo $all_pipeline_data[$key]['term_month'] ?> Months</td>*/?>

										<?php
											if($all_pipeline_data[$key]['term_sheet'] == '1'){
												$closingclass = ' style="text-align: left;color:green; " ';
											}else{
												$closingclass = ' style="text-align: left;" ';
											}
										?>
										
										<td <?php echo $closingclass;?>>
											<?php echo $closing_status_option[$all_pipeline_data[$key]['closing_status']]; ?>
										</td>
										<td>
											<?php echo $all_pipeline_data[$key]['user_full_name']; ?>
										</td>
										<?php 

										$funding_date = $all_pipeline_data[$key]['funding_date'] ? date('m-d-Y', strtotime($all_pipeline_data[$key]['funding_date'])) : '';
										$currentDate = date('Y-m-d');

										if($currentDate > $all_pipeline_data[$key]['funding_date']){
											$pipeclass = 'red';
										}else{
											$pipeclass = '';
										}

										?>

										<td style = "text-align: left;" class="<?php echo $pipeclass;?>">

											<?php echo $funding_date; ?>
										</td>


									<?php $key++;?>
									</tr>
										<?php }}} else {?>
									<tr>
										<td style = "text-align: left;" colspan="9">No Data Found!</td>
									</tr>
									<?php }?>
							</tbody>
							<tfoot class="foot_light_blue">
								<tr>
									<th>Total: <?php echo $key; ?></th>
									<th></th>
									<th>$<?php echo number_format($total_loan_amount); ?></th>
									<th></th>
									<th><?php echo number_format($total_interst_rate / $key, 3); ?>%</th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>


								</tr>
								<tr>
									<th>Average:</th>
									<th></th>
									<th>$<?php echo number_format($total_loan_amount / $key); ?></th>
									<th><?php //echo number_format($total_avi_balance / $key); ?></th>
									<th></th>
									<th><?php //echo number_format($total_ltv / $key, 2); ?></th>
									 <th><?php //echo number_format($total_term / $key); ?></th>
									<th></th>
									<th></th>


								</tr>
							</tfoot>

						</table>
					</div>
					</div>

					<!-------------- PIPELINE TABLE ENDS ------------->
					<!-- <div class="row rc_class">
						<div class="col-md-12" id="ajax_term_sheet">


						</div>
					</div> -->


			<?php if ($loginuser_dashboard == 1) { ?>

					<!--------------<div class="row rc_class">
						<div class="col-md-12" id="active_trust_deed">

								
						</div>
					</div>------>

					


					

				
				

				

	<?php } else { ?>   <!--- for 3rd type -->
	
				<div class="row rc_class">


					<div class="col-md-4" id="maturity_schedule">
					<!------ append maturity_schedule table here ------->
					</div>


					<div class="col-md-4" id="default_loan">
						<!------ append a default_loan table here ------->
					</div>

				</div>
	<?php }?>
	
		</div>
	</div>
	

<!-- END CONTAINER -->

<script>

ajax_servicing_companies();
function ajax_servicing_companies(){

	$.ajax({

			type	: 'POST',
			url		: '<?php echo base_url() . "Home/ajax_servicing_companies"; ?>',
			data	: {},
			success	: function(response){
				if(response)
				{
					$('div#servicing_companies').append(response);
					//window.location.reload();
				}
			}
	});
}








ajax_assignment_process();
function ajax_assignment_process(){
	return false;

	$.ajax({
				type	:	'POST',
				url		:	'<?php echo base_url() . "Home/ajax_assignment_process"; ?>',
				data	:	{},
				success	: 	function(response){
					if(response){
						$('div#assignment_process').append(response);
						//window.location.reload();
					}
				}
	});
}

/*ajax_top_lender();
function ajax_top_lender(){

	$.ajax({
				type	:	'POST',
				url		:	'<?php echo base_url() . "Home/ajax_top_lender"; ?>',
				data	:	{},
				success	: 	function(response){
					if(response){
						$('div#top_lender').append(response);
					}
				}
	});
}

ajax_top_borrower();
function ajax_top_borrower(){

	$.ajax({
				type	:	'POST',
				url		:	'<?php echo base_url() . "Home/ajax_top_borrower"; ?>',
				data	:	{},
				success	: 	function(response){
					if(response){
						$('div#top_borrower').append(response);
					}
				}
	});
}
*/








//ajax_trust_deed();
/* function ajax_trust_deed(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php// echo base_url()."Home/ajax_trust_deed";?>',
			data	: {},
			success : function(response){

				if(response){
					//$("#LoadingImage").hide();
					$('div#trust_deed').append(response);
					//window.location.reload();
				}
			}
	});
}
 */



/*ajax_active_trust_deed();
function ajax_active_trust_deed(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php //echo base_url() . "Home/active_trust_deedData"; ?>',
			data	: {},
			success : function(response){

				if(response){
					//$("#LoadingImage").hide();
					$('div#active_trust_deed').append(response);
					//window.location.reload();
				}
			}
	});
}*/

/*ajax_term();
function ajax_term(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php //echo base_url()."Home/ajax_term_sheet";?>',
			data	: {},
			success : function(response){

				if(response){
					//$("#LoadingImage").hide();
					$('div#ajax_term_sheet').append(response);
					//window.location.reload();
				}
			}
	});
}
*/



ajax_loan_date();
function ajax_loan_date(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url() . "Home/ajax_loan_late_date"; ?>',
			data	: {},
			success : function(response){

				if(response){
					//$("#LoadingImage").hide();
					$('div#ajax_loan_date_late').append(response);
					//window.location.reload();
				}
			}
	});
}


</script>
