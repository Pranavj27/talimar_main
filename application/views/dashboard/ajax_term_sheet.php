<?php 	

		$loan_type_option			= $this->config->item('loan_type_option');
		$loan_status_option			= $this->config->item('loan_status_option');
		$closing_status_option		= $this->config->item('closing_status_option');
		$term_sheet_option			= $this->config->item('term_sheet');

		?>
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead >
								<tr class="thead_dark_blue">
									<th colspan="9">Term Sheets</th>
								</tr>
								<tr>
									<!--<th>Loan #</th>-->
									<th>Property Address</th>
									<th>Borrower</th>
									<th>Loan Amount</th>
									<th>Unfunded</th>
									<th>Interest Rate</th>
									<th>LTV</th>
									<th>Term</th>
									<th>Term Sheet</th>
									<th>Closing Date</th>
									
									
								</tr>
							</thead>
							<tbody>

								<?php
						
									$key 						= 0;
									$total_pipeline_available 	= 0;
									$total_interst_rate 		= 0;
									$total_loan_amount			= 0;
									$total_ltv					= 0;
									$total_term					= 0;
									$total_avi_balance			= 0;


									if(isset($all_pipeline_data)){
						
	
							// $i = array(1,2,3,0,4,5,6,7,8,9); 
							// $final_array= array();
							// foreach($i as $index) {
							//     $final_array[$index] = $all_pipeline_data[$index];
							// }
	   
					

										foreach($all_pipeline_data as $key => $row)
										{
											if($all_pipeline_data[$key]['talimar_loan']){
											$calculate_lender_points = $all_pipeline_data[$key]['loan_amount'] * ($all_pipeline_data[$key]['lender_fee']/100);
											
											$total_pipeline_available = $total_pipeline_available + $all_pipeline_data[$key]['avilable'];
											
											$position = $all_pipeline_data[$key]['position'];
											if($position == '2'){
												
												$loan_to_value = ($all_pipeline_data[$key]['select_ecum']+$all_pipeline_data[$key]['loan_amount']) / $all_pipeline_data[$key]['uv_value']*100;
												
											}else{
										$loan_to_value = ($all_pipeline_data[$key]['select_ecum']+$all_pipeline_data[$key]['loan_amount']) / $all_pipeline_data[$key]['uv_value']*100;
											
											}
										$loan_to_valueddd = is_infinite($loan_to_value) ? 0 : $loan_to_value;
										$total_interst_rate += 	$all_pipeline_data[$key]['intrest_rate'];
										$total_loan_amount  +=  $all_pipeline_data[$key]['loan_amount'];	
										$total_ltv  +=  $loan_to_valueddd;	
										$total_term += $all_pipeline_data[$key]['term_month'];	
										$total_avi_balance += $all_pipeline_data[$key]['avilable'];	
								?>
									<tr>
										<!--<td style = "text-align: left;"></td>-->
										<td style = "text-align: left;"><a href="<?php echo base_url();?>load_data/<?php echo $all_pipeline_data[$key]['loan_id'];?>"><?php echo $all_pipeline_data[$key]['property_address'] ? $all_pipeline_data[$key]['property_address'] : 'No Property Added'; ?></a>
											
										</td>
										<td style = "text-align: left;" >
											<?php echo $all_pipeline_data[$key]['borrower_name']; ?>
										</td>
										<td style = "text-align: left;">
											$<?php echo number_format($all_pipeline_data[$key]['loan_amount']); ?>
										</td>
										<td>$<?php echo number_format($all_pipeline_data[$key]['avilable']); ?></td>
										<td style = "text-align: left;" ><?php echo number_format($all_pipeline_data[$key]['intrest_rate'],3)?>%</td>
										
										
										<td style = "text-align: left;" ><?php echo number_format($loan_to_valueddd,2); ?>%</td>
										<td style = "text-align: left;" ><?php echo $all_pipeline_data[$key]['term_month']?> Months</td>
										
										<td style = "text-align: left;" >
										<?php echo $term_sheet_option[$all_pipeline_data[$key]['term_sheet_status']];?>
										</td>

										<?php $funding_date = $all_pipeline_data[$key]['funding_date'] ? date('m-d-Y',strtotime($all_pipeline_data[$key]['funding_date'])) : ''; ?>

										<td style = "text-align: left;" >

											<?php echo $funding_date; ?>
										</td>
									
									
									<?php $key++;?>
									</tr>
										<?php }} }else {?>
									<tr>
										<td style = "text-align: left;" colspan="9">No Data Found!</td>
									</tr>
									<?php } ?>
							</tbody>
							<tfoot class="foot_light_blue">
								<tr>
									<th>Total: <?php echo $key;?></th>
									<th></th>								
									<th>$<?php echo number_format($total_loan_amount);?></th>
									<th>$<?php echo number_format($total_avi_balance);?></th>								
									<th><?php echo number_format(is_nan($total_interst_rate/$key) ? 0:$total_interst_rate/$key ,3);?>%</th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									
									
								</tr>
								<tr>
									<th>Average:</th>
									<th></th>									
									<th>$<?php echo number_format(is_nan($total_loan_amount/$key) ? 0:$total_loan_amount/$key);?></th>
									<th>$<?php echo number_format(is_nan($total_avi_balance/$key) ? 0:$total_avi_balance/$key);?></th>
									<th></th>
									<th><?php echo number_format(is_nan($total_ltv/$key) ? 0:$total_ltv/$key,2);?>%</th>
									<th><?php echo number_format(is_nan($total_term/$key) ? 0:$total_term/$key);?> Months</th>
									<th></th>
									<th></th>
									
									
								</tr>
							</tfoot>
							
						</table>	
		