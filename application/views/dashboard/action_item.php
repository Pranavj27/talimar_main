<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="100%">Action Items</th>
		</tr>
		<!--<tr>
			<th colspan="2" style="text-align:center;">Pipeline/Loan Processing</th>	
			<th colspan="2" style="text-align:center;">Loan Servicing</th>	
			<th colspan="2" style="text-align:center;">Lender Servicing</th>	
		</tr>-->
	</thead>
	<tbody>
			<?php
			$count = 0;
			$amount = 0;
			foreach($holding_loan as $boarding_val){	
				if($boarding_val['status'] == '2'){
				
					//$count++;
					$amount += $boarding_val['loan_amount'];
			} } 

			

			?>
			
			
			
		<tr>
			
			<!-- <td style = "text-align:left;width:20%;"><a href="#">Outstanding Term Sheets</a></td>
			<td style = "text-align:left;width:16%;"><?php echo $total_term_sheet; ?></td> -->
			
			<td style = "text-align:left;width:20%;"><a href="<?php echo base_url();?>assigment_in_progress">Wire Confirmation</a></td>
			<td style = "text-align:left;width:16%;"><?php echo $confirm_wire; ?></td>

			<td style = "text-align: left;width:16%;" ><a href="<?php echo base_url();?>reports/outstadingServicerDeposit">Late Payments</a></td>
			<td style = "text-align: left;width:16%;" ><?php echo $count_10_days;?></td>

			<td style ="text-align:left;width:16%;"><a href="<?php echo base_url();?>paid_of_loan_schedule">Payoffs in Process</a></td>
			<td style ="text-align:left;width:16%;"><?php echo $payoff_report_count_payoff;?></td>
		
			<!-- <td style = "text-align:left;width:16%;"><a href="<?php base_url();?>upcoming_payoff_schedule">Requested Demands</a></td>
			<td style = "text-align:left;width:16%;"><?php echo '$'.number_format($payoff_report_amount); ?></td> -->	
				
		</tr>
		<tr>
			<td style = "text-align: left;width:20%;" ><a href="<?php echo base_url();?>Outstanding_lender_fees">Outstanding Lender Fees</a></td>
			<td style = "text-align: left;width:16%;" ><?php echo $upcoming_action_item; ?></td>
			<td style = "text-align:left;width:16%;" ><a href="<?php echo base_url();?>loan_default_schedule">Loans in Default</a></td>
			<td style = "text-align:left;width:16%;" ><?php echo $fetch_total_default; ?></td>
			<td style = "text-align: left;width:16%;" ><a href="<?php echo base_url();?>maturity_report_schedule">Upcoming Maturities</a></td>
			<td style = "text-align: left;width:16%;" ><?php echo $fetch_upcoming_count;?></td>	
					
		</tr>

		<tr>
			<td style ="text-align:left;width:20%;"><a href="<?php echo base_url();?>existing_loan_schedule">Borrower Welcome E-Mail</a></td>
			<td style ="text-align:left;width:16%;"><?php echo $borrower_emailx;?></td>
			
			<td style = "text-align:left; width:20%;"><a href="<?php echo base_url();?>monthly_servicing_income">Unverified Broker Disbursements</a></td>
			<td style = "text-align:left;width:16%;"><?php echo $all_count_disb;?></td>
	
			<td style = "text-align: left;width:16%;" ><a href="<?php echo base_url();?>reports/LoanExtensionReport">Extension Processing</a></td>
			<td style = "text-align: left;width:16%;" ><?php echo $ext_count;?></td>
			<!-- <td style = "text-align: left;width:16%;" ><a href="<?php echo base_url();?>existing_loan_schedule">Real Estate Owned</a></td>
			<td style = "text-align: left;width:16%;" ><?php echo $realstateowned;?></td> -->
		
		</tr>

		<tr>
			
			<td style ="text-align:left;width:20%;"><a href="<?php echo base_url();?>existing_loan_schedule">Borrower Welcome Calls</a></td>
			<td style ="text-align:left;width:16%;"><?php echo $borrower_callx;?></td>	
	
			<td style ="text-align:left;width:20%;"><a href="<?php echo base_url();?>social_media">Outstanding Marketing</a></td>
			<td style ="text-align:left;width:16%;"><?php echo $Advertisingcomplete;?></td>

			<!-- <td style = "text-align: left;width:16%;" ><a href="#">Before & After Images</a></td> -->
			<!-- <td style = "text-align: left;width:16%;" ><?php //echo $Count_BA_video;?></td> -->
			<td style ="text-align: left;width:16%;" ><a href="<?php echo base_url();?>reports/loanBoardingReport">Loans in Boarding</a></td>
			<td style ="text-align: left;width:16%;" ><?php echo $count_boarding_count;?></td>
			
		</tr>

		<tr>

			
	

			<td style ="text-align: left;width:16%;" ><a href="<?php echo base_url();?>Outstanding_draws_report">Outstanding Draws</a></td>
			<td style ="text-align: left;width:16%;" ><?php echo $outstanding_draws_count;?></td>
			
			
			<td style ="text-align: left;width:16%;" ><a href="<?php echo base_url();?>ReportData/lender_approvals">Lender Approvals</a></td>
			<td style ="text-align: left;width:16%;" ><?php echo $countlenderappreq;?></td>
			<td style = "text-align:left;width:16%;"><a href="<?php echo base_url();?>loan_boarding_schedule">Loans in Holding</a></td>
			<!-- <td style = "text-align:left;width:16%;"><?php echo '$'.number_format($payoff_report_amount); ?></td>	 -->
			<td style = "text-align:left;width:16%;"><?php echo $count_holding_count; ?></td>
			

		</tr>


		<tr>
			<td style = "text-align: left;width:16%;" ><a href="<?php echo base_url();?>property_insurance">Property Insurance</td>
				<?php 
				    $count=0;
				    foreach($fetch_property_loan as $row)
					{ 
						if($row['request_updated_insurance'] == '1')
					     {
					
							$updated_insurance = 'Cancelled';	
				            $count +=count($updated_insurance);
				
					     } 
					     else
					    {
							 $updated_insurance='';
							 $current_date=date('Y-m-d');
							 $current= date("Y-m-d", strtotime($current_date));

							if(date('Y-m-d', strtotime($row['insurance_expiration'])) < $current)
							{

								$updated_insurance = 'Expired';
								$count +=count($updated_insurance);
							}  
							else
							{
							 
								$updated_insurance = ''; 
								
								$currentt= date("Y-m-d", strtotime($current_date . ' +45 days'));

								if(date('Y-m-d', strtotime($row['insurance_expiration'])) <= $currentt)
								{

									$updated_insurance = 'Expiring Soon';
									$count +=count($updated_insurance);
								}
								else
									{
										$updated_insurance = ''; 
								
									}

							
							}

						}

				}
					
				?>
			<td style = "text-align: left;width:16%;" ><?php echo $count; ?></td>

			<td style ="text-align: left;width:16%;"><a href="<?php echo base_url();?>assigment_in_progress">Assignments in Process</a></td>
			<td style ="text-align: left;width:16%;"><?php echo $totalAssognCount;?></td>
			<td><a href="<?php echo base_url('reports/outstanding_servicer_dposit'); ?>" >Servicer Deposit Not Received</a></td>
			<td><?php echo $servicer_deposit_received; ?></td>
			
		</tr>
		<!-- <tr>

			
	
			<td style ="text-align: left;width:16%;" ></td>
			<td style ="text-align: left;width:16%;" ></td>
			
			<td style = "text-align: left;width:16%;" ></td>
			<td style = "text-align: left;width:16%;" ></td>

		</tr> -->
		
	</tbody>
	
</table>



