<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">	
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="3">Outstanding Servicer Deposits</th>
		</tr>
		<tr>
			<th>Property</th>
			<th>Received</th>
			<th>Applied</th>
		</tr>
	</thead>
	<tbody>	
		<?php
		if(!empty($outstandingServicerData) && is_array($outstandingServicerData)){
		 	foreach($outstandingServicerData as $row){ ?>
				<tr>
					<td>
						<a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['property_address'];?></a>
					</td>
					<td>
						<?php echo $row['recived'];?>
					</td>
					<td>
						<?php echo $row['apply'];?>
					</td>
				</tr>
			<?php 
			} 
		}else{
			echo '<tr><td colspan="3" style="text-align: center;">No Data Found!</td></tr>';
		} 
		?>
	</tbody>
</table>