
<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead>
								<tr class="thead_dark_blue">
									<th colspan="100%">Available Trust Deeds</th>
								</tr>
								<tr>
									<th>Loan Number</th>
									<th>Street Name</th>
									<th>Lender Rate</th>
									<th>Loan Amount</th>
									<th>Funded</th>
									<th>Available</th>
									<th>Minimum</th>
									<th>Remaining Spots</th>
									<th>Closing Status</th>
									<th>Marketing Status</th>
								
								</tr>
							</thead>
							<tbody>
								
								<?php
								
								$closing_status_option 	 = $this->config->item('closing_status_option');
								$loan_status_option		 = $this->config->item('loan_status_option');
							
								$number 					= 0;
								$total_loan_amount 			= 0;
								$total_available			= 0;
								$total_funded_balance 		= 0;

								// usort($trust_deed, function($a, $b) {
								    // return $a['avilable_deed_trust'] <=> $b['avilable_deed_trust'];
								// });

								// $trust_deed = array_reverse($trust_deed);

								
								if(isset($trust_deed)){
									
									foreach($trust_deed as $key => $row)
									{
										

											$available = $trust_deed[$key]['loan_amount'] - $trust_deed[$key]['total_investment'];
											$lender_yield = $trust_deed[$key]['intrest_rate'] - $trust_deed[$key]['servicing_fee'];
											
											$servicing_fee = $trust_deed[$key]['servicing_fee'] ? $trust_deed[$key]['servicing_fee'] : '1.00';
											
											$lender_rate = $trust_deed[$key]['intrest_rate'] - $servicing_fee;
											
											$marketing_status = '';
											$posteds = '';
											// if($available == 0){
												// $marketing_status = 'Fully Subscribed';
											// }
											
											
												
												if($row['market_trust_deed'] == 1){
													$marketing_status = 'Posted';
												}
												// elseif($row['market_trust_deed'] == 1 && $row['trust_deed_posted_on_website'] == 1)
												// {
													// $marketing_status = 'Coming Soon';
												// }
												else
												{
													$marketing_status = 'Not Posted';
												}
											
									?>
								
								<tr>
									<td style = "text-align: left;" >
										<a href="<?php echo base_url().'load_data/'.$trust_deed[$key]['loan_id']; ?>"><?php echo $trust_deed[$key]['talimar_loan'];?></a>
									</td>
									<td style = "text-align: left;" >
										<a href="<?php echo base_url().'load_data/'.$trust_deed[$key]['loan_id']; ?>"><?php echo $trust_deed[$key]['property_address'];?></a>
									</td>
									<td style = "text-align: left;" >
										<?php echo number_format($lender_rate,3);?>%
									</td>
									<td style = "text-align: left;" >
										$<?php echo number_format($trust_deed[$key]['loan_amount']);?>
									</td>
									
									<td>
										$<?php echo number_format($row['loan_amount'] - $available); ?>
									</td>
									
									<td style = "text-align: left;">
										$<?php echo number_format($available);?>
									</td>
							
									<td style = "text-align: left;" >
										$<?php echo number_format($trust_deed[$key]['lender_minimum']);?>
									</td>
									
									<td><?php echo 10 - $trust_deed[$key]['count_assigment_lender']; ?></td>
									<td><?php echo $closing_status_option[$trust_deed[$key]['closing_status']];?></td>
									<td><?php echo $marketing_status;?></td>
								
								</tr>
									
									<?php 
										$funded_balance = $row['loan_amount'] - $available;
										$total_funded_balance += $funded_balance;
										$total_loan_amount = $total_loan_amount + $trust_deed[$key]['loan_amount'];
										$total_available = $total_available + $available;
										$number++;
									} }else {?>
									<tr>
										<td style = "text-align: left;"  colspan="8">No Data Found!</td>
									</tr>
									
									
									<?php } ?>
									
							</tbody>
							<tfoot class="foot_light_blue">
							<tr>
								<th>Total: <?php echo $number; ?></th>
								<th></th>
								<th></th>
								<th>$<?php echo number_format($total_loan_amount); ?></th>
								<th>$<?php echo number_format($total_funded_balance);?></th>
								<th>$<?php echo number_format($total_available); ?></th>
								<th></th>						
								<th></th>
								<th></th>
								<th></th>
							
								
							</tr>
						</tfoot>
						</table>
						
						
						
