<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="3">Loan Balance Comparison</th>
		</tr>
		<tr>
			<th>Title</th>
			<th># of Loans</th>
			<th>$ of Loans</th>
		</tr>

	</thead>
	<tbody>	

		<tr>
			<td><a href="<?php echo base_url();?>reports/ServicerCheck">TaliMar Financial</a></td>
			<td><?php echo $TaliMarCount;?></td>
			<td>$<?php echo number_format($TaliMarAmount, 2);?></td>
		</tr>
		
		<tr>
			<td><a href="<?php echo base_url();?>reports/ServicerCheck">FCI Balance</a></td>
			<td><?php echo $FcitotalCount;?></td>
			<td>$<?php echo number_format($FcitotalAmount, 2);?></td>
		</tr>
		

		<tr>
			<td><a href="<?php echo base_url();?>reports/ServicerCheck">Servicer Processing</a></td>
			<td><?php echo $FcitotalBCount;?></td>
			<td>$<?php echo number_format($FcitotalBAmount, 2);?></td>
		</tr>
		<tr>
			<td><a href="<?php echo base_url();?>reports/ServicerCheck">Difference</a></td>
			<td><?php echo $TaliMarCount - ($FcitotalCount + $FcitotalBCount);?></td>
			<td>$<?php echo number_format($TaliMarAmount - ($FcitotalAmount + $FcitotalBAmount), 2);?></td>
		</tr>
	</tbody>
</table>