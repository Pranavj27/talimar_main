<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead>
								<tr class="thead_dark_blue">
									<th colspan="3">Loan Statistics</th>
								</tr>
								<tr>
								<th>Item</th>
								<th>Count</th>
								<th>Balance</th>
							</tr>
							</thead>
							<tbody>
								<tr>
									<td style = "text-align: left;" >Pipeline MTE</td>
									<td style = "text-align: left;" ><?php echo $t_pipe_loan_count;?></td>
								    <td style = "text-align: left;" >$<?php echo number_format($t_pipe_loan_amount); ?></td>
								
								</tr>
								<tr>
									<td style = "text-align: left;" >Funded MTD</td>
									<td style = "text-align: left;" ><?php echo $hash_loans_funded_loan_current_month;?></td>
								    <td style = "text-align: left;" >$<?php echo $loans_funded_loans_amt_in_current_month; ?></td>
								
								</tr>
								
								<tr>
									<td style = "text-align: left;">Funded YTD</td>
									<td style = "text-align: left;"><?php echo $hash_loans_funded_loans_in_current_year; ?></td>
								<td style = "text-align: left;">$<?php echo $loans_funded_loans_amt_in_current_year; ?></td>
								</tr>
							
								
								
								<tr>
									<td style = "text-align: left;" style = "text-align: left;">Total Funded </td>
									<td style = "text-align: left;" ><?php echo $loans_funded_loans; ?></td>
									<td style = "text-align: left;" >$<?php echo $loans_funded_loans_amt; ?></td>
								</tr>
							
								
								
								<tr>
									<td style = "text-align: left;" >Total Paid Off</td>
									<td style = "text-align: left;" ><?php echo $hash_off_paid_off; ?></td>
								    <td style = "text-align: left;" >$<?php echo number_format($total_amount_off_paid_off); ?></td>
								</tr>
							
																		


								
								
							</tbody>
								
						</table>	
