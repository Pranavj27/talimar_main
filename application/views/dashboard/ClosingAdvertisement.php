<style type="text/css">
	.green{
		color:green;
	}
</style>
<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="2">Input Advertisement Text</th>
		</tr>
		<tr>
			<th>Property Address</th>
			<th>Loan Amount</th>
		</tr>

	</thead>
	<tbody>	
		<?php if(isset($ClosingAdvertisementsql) && is_array($ClosingAdvertisementsql)){ 
				foreach($ClosingAdvertisementsql as $Row){ 

					if($Row['checkvalue'] == '1'){

						$color = '';
					}else{

						$color = 'red';
					}

					?>
					<tr>
						<td><a href="<?php echo base_url();?>load_data/<?php echo $Row['loan_id'];?>"><?php echo $Row['property_address'];?></a></td>
						<td class="<?php echo $color;?>">$<?php echo number_format($Row['loan_amount']);?></td>
						
					</tr>
				<?php } }else{ ?>
					<tr>
						<td>No data found!</td>
					</tr>
				<?php } ?>
	</tbody>
</table>