<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="2">Before & After Images</th>
		</tr>
		<tr>
			<th>Property Address</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>	
		<?php 
		$class = '';
		$statusValue='';
		if(isset($beforeandafterdataArray) && is_array($beforeandafterdataArray)){ 
				foreach($beforeandafterdataArray as $Row){ 

					if($Row['process_bai'] == 1){
						$statusValue="Requested";
					}else if($Row['compleate_bai'] == 1){
						$statusValue="Completed";
					}else if($Row['posted_bai'] == 1){
						$statusValue="Posted";
						$class = 'style="color:green" ';
					}else{
						$statusValue="";
						$class = '';
					}
					?>
					<tr>
						<td class="<?php echo $class;?>">
							<a href="<?php echo base_url();?>load_data/<?php echo $Row['loan_id'];?>"><?php echo $Row['property_address'];?></a>
						</td>
						<td> <a href="<?php echo base_url();?>reports/before_and_after"><span <?php echo $class;?>><?php echo $statusValue;?></span></a></td>
						
					</tr>
				<?php } }else{ ?>
					<tr>
						<td colspan="2">No data found!</td>
					</tr>
				<?php } ?>
	</tbody>
</table>