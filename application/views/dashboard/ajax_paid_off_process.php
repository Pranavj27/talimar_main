
<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="2">Payoffs in Process</th>
		</tr>
		<tr>
			<!-- <th>Loan #</th> -->
			<th>Borrower Name</th>
			<th>Street Address</th>
		
		</tr>
	</thead>
	<tbody>	
		<?php 

        if(isset($fetch_paid_offf) && is_array($fetch_paid_offf)){
			foreach($fetch_paid_offf as $row){ ?>
		 	<tr>
			
				<td><a href="<?php echo base_url().'borrower_view/'.$row['b_id']?>"><?php echo $row['borrower_name'];?></a></td>
				<td><a href="<?php echo base_url().'load_data/'.$row['loan_id']?>"><?php echo $row['address'];?></a></td>
			
			</tr>
		<?php } }else{ ?>

			<td colspan="2" style="text-align: center;">No data found!</td>
		<?php } ?>
		
	</tbody>
		
</table>	
