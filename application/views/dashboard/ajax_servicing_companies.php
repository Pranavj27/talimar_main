<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead class="thead_dark_blue">
								<tr>
									<th colspan="2">Servicing Companies</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Loans in Pre-Boarding #</td>
									<td><?php echo $count_pre_boarding_loan; ?></td>
								</tr>
								<tr>
									<td>Loans in Pre-Boarding $</td>
									<td><?php echo '$'. number_format($amount_pre_boarding_loan); ?></td>
								</tr>


								<tr>
									<td>Loans in Boarding #</td>
									<td><?php echo $count_both_loan_services_with_bs; ?></td>
								</tr>
								<tr>
									<td>Loans in Boarding $</td>
									<td><?php echo '$'. number_format($both_loan_services_total_amount_with_bs); ?></td>
								</tr>
								<tr>
									<td>FCI Lender Services # </td>
									<td><?php echo $count_fci_loan; ?></td>
								</tr>
								<tr>
									<td>FCI Lender Services $ </td>
									<td><?php echo '$'.number_format($fci_loans_total_amount); ?></td>
								</tr>
								<tr>
									<td>Del Toro Loan Services #</td>
									<td><?php echo $count_del_toro_loan; ?></td>
								</tr>
								<tr>
									<td>Del Toro Loan Services $</td>
									<td><?php echo '$'.number_format($del_toro_loans_total_amount); ?></td>
								</tr>
								
								<tr>
									<td>TaliMar Financial # </td>
									<td><?php echo $count_talimar_loan ; ?></td>
								</tr>
								<tr>
									<td>TaliMar Financial $</td>
									<td><?php echo '$'.number_format($talimar_loans_total_amount); ?></td>
								</tr>
								
								<!--<tr>
									<!--<td># of Loans Serviced Loan</td>-->
									<!--<td>Add Loans w/ TaliMar Financial # </td>
									<td><?php echo $count_serviced_loan; ?></td>
								</tr>
								<tr>
									<!--<td>$ of Loans Serviced Loan</td>-->
									<!--<td>Add Loans w/ TaliMar $</td>
									<td><?php echo '$'.number_format($serviced_loans_total_amount); ?></td>
								</tr>-->
								
							
								
							</tbody>
						</table>