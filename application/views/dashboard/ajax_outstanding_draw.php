<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="3">Outstanding Draws</th>
		</tr>
		<tr>
			<th>Address</th>
			<th>$ Requested</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	 // echo '<pre>';
	 // print_r($outstanding_draws);
	 // echo '</pre>';
	if(isset($outstanding_draws) && is_array($outstanding_draws))
	{
		foreach($outstanding_draws as $row)
		{
	     
				
	
			?>
			<?php if($row['date1_draw_released'] !='1' &&  $row['draws_amount_1']) { 
				

				if($row['date1_draw_request'] =='1' && $row['date1_draw_submit'] =='1'){

                         $status_1='Submitted';
				}

				elseif($row['date1_draw_request'] =='1' && $row['date1_draw_approved'] =='1'){

                         $status_1='Approved';
				}

				elseif($row['date1_draw_request'] =='1' && $row['date1_draw_approved'] =='1' && $row['date1_draw_submit'] =='1'){

                         $status_1='Submitted';
				}
					
					elseif($row['date1_draw_request'] =='1')
					{

						$status_1='Needs Approval';
				}
				
          elseif($row['date1_draw_submit'] =='1')
					{
						$status_1='Submitted';
				}
				
       elseif($row['date1_draw_approved'] =='1')
	 				{
						$status_1='Approved';
				}
				
				
				else{

				 $status_1='';	
				}

				?>
			<?php if($status_1 != ''){?>
			<tr>
				<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street']; ?></a></td>
				<td>$<?php echo number_format($row['draws_amount_1'],2); ?></td>
				<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'].'#test';?>"><?php echo $status_1; ?></a></td>
			</tr>
			<?php } ?>

		<?php }else{echo "";} ?>


		<?php if($row['date2_draw_released'] !='1' &&   $row['draws_amount_2']) {


				if($row['date2_draw_request'] =='1' && $row['date2_draw_submit'] =='1'){

                         $status_2='Submitted';
				}

				elseif($row['date2_draw_request'] =='1' && $row['date2_draw_approved'] =='1'){

                         $status_2='Approved';
				}

				elseif($row['date2_draw_request'] =='1' && $row['date2_draw_approved'] =='1' && $row['date2_draw_submit'] =='1'){

                         $status_2='Submitted';
				}
					
					elseif($row['date2_draw_request'] =='1')
					{

						$status_2='Needs Approval';
				}
				
          elseif($row['date2_draw_submit'] =='1')
					{
						$status_2='Submitted';
				}
				
       elseif($row['date2_draw_approved'] =='1')
	 				{
						$status_2='Approved';
				}
				
				
				else{

				 $status_2='';	
				}



		 ?>


		 	<?php if($status_2 != ''){?>
			<tr>
				<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street']; ?></a></td>
				<td>$<?php echo number_format($row['draws_amount_2']); ?></td>
				<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'].'#test';?>"><?php echo $status_2; ?></a></td>

			</tr>
			<?php } ?>
			<?php  }else{echo "";}?>


			<?php if($row['date3_draw_released'] !='1' &&  $row['draws_amount_3']) { 

				if($row['date3_draw_request'] =='1' && $row['date3_draw_submit'] =='1'){

                         $status_3='Submitted';
				}

				elseif($row['date3_draw_request'] =='1' && $row['date3_draw_approved'] =='1'){

                         $status_3='Approved';
				}

				elseif($row['date3_draw_request'] =='1' && $row['date3_draw_approved'] =='1' && $row['date3_draw_submit'] =='1'){

                         $status_3='Submitted';
				}
					
					elseif($row['date3_draw_request'] =='1')
					{

						$status_3='Needs Approval';
				}
				
          elseif($row['date3_draw_submit'] =='1')
					{
						$status_3='Submitted';
				}
				
       elseif($row['date3_draw_approved'] =='1')
	 				{
						$status_3='Approved';
				}
				
				
				else{

				 $status_3='';	
				}





				?>
			<?php if($status_3 != ''){?>
			<tr>
				<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street']; ?></a></td>
				<td>$<?php echo number_format($row['draws_amount_3']); ?></td>
				<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'].'#test';?>"><?php echo $status_3; ?></a></td>
			</tr>
			<?php } ?>
			<?php  }else{echo "";}?>
        <?php if($row['date4_draw_released'] !='1' &&  $row['draws_amount_4']) { 

        		if($row['date4_draw_request'] =='1' && $row['date4_draw_submit'] =='1'){

                         $status_4='Submitted';
				}

				elseif($row['date4_draw_request'] =='1' && $row['date4_draw_approved'] =='1'){

                         $status_4='Approved';
				}

				elseif($row['date4_draw_request'] =='1' && $row['date4_draw_approved'] =='1' && $row['date4_draw_submit'] =='1'){

                         $status_4='Submitted';
				}
					
					elseif($row['date4_draw_request'] =='1')
					{

						$status_4='Needs Approval';
				}
				
          elseif($row['date4_draw_submit'] =='1')
					{
						$status_4='Submitted';
				}
				
       elseif($row['date4_draw_approved'] =='1')
	 				{
						$status_4='Approved';
				}
				
				
				else{

				 $status_4='';	
				}



        	?>
        	<?php if($status_4 != ''){?>
			<tr>
				<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street']; ?></a></td>
				<td>$<?php echo number_format($row['draws_amount_4']); ?></td>
					<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'].'#test';?>"><?php echo $status_4; ?></a></td>
			</tr>
			<?php } ?>
			<?php  }else{echo "";}?>

			   <?php if($row['date5_draw_released'] !='1' &&  $row['draws_amount_5']) { 

        		if($row['date5_draw_request'] =='1' && $row['date5_draw_submit'] =='1'){

                         $status_5='Submitted';
				}

				elseif($row['date5_draw_request'] =='1' && $row['date5_draw_approved'] =='1'){

                         $status_5='Approved';
				}

				elseif($row['date5_draw_request'] =='1' && $row['date5_draw_approved'] =='1' && $row['date5_draw_submit'] =='1'){

                         $status_5='Submitted';
				}
					
					elseif($row['date5_draw_request'] =='1')
					{

						$status_5='Needs Approval';
				}
				
          elseif($row['date5_draw_submit'] =='1')
					{
						$status_5='Submitted';
				}
				
       elseif($row['date5_draw_approved'] =='1')
	 				{
						$status_5='Approved';
				}
				
				
				else{

				 $status_5='';	
				}



        	?>
        	<?php if($status_5 != ''){?>
			<tr>
				<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street']; ?></a></td>
				<td>$<?php echo number_format($row['draws_amount_5']); ?></td>
					<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'].'#test';?>"><?php echo $status_5; ?></a></td>
			</tr>
			<?php } ?>
			<?php  }else{echo "";}?>

			   <?php if($row['date6_draw_released'] !='1' &&  $row['draws_amount_6']) { 

        		if($row['date6_draw_request'] =='1' && $row['date6_draw_submit'] =='1'){

                         $status_6='Submitted';
				}

				elseif($row['date6_draw_request'] =='1' && $row['date6_draw_approved'] =='1'){

                         $status_6='Approved';
				}

				elseif($row['date6_draw_request'] =='1' && $row['date6_draw_approved'] =='1' && $row['date5_draw_submit'] =='1'){

                         $status_6='Submitted';
				}
					
					elseif($row['date6_draw_request'] =='1')
					{

						$status_6='Needs Approval';
				}
				
          elseif($row['date6_draw_submit'] =='1')
					{
						$status_6='Submitted';
				}
				
       elseif($row['date6_draw_approved'] =='1')
	 				{
						$status_6='Approved';
				}
				
				
				else{

				 $status_6='';	
				}



        	?>

       
        	<?php if($status_6 != ''){?>

			<tr>
				<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street']; ?></a></td>
				<td>$<?php echo number_format($row['draws_amount_6']); ?></td>
					<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'].'#test';?>"><?php echo $status_6; ?></a></td>
			</tr>
			<?php } ?>
		<?php  }else{echo "";}?>


             <?php if($row['date7_draw_released'] !='1' &&  $row['draws_amount_7']) { 

        		if($row['date7_draw_request'] =='1' && $row['date7_draw_submit'] =='1'){

                         $status_7='Submitted';
				}

				elseif($row['date7_draw_request'] =='1' && $row['date7_draw_approved'] =='1'){

                         $status_7='Approved';
				}

				elseif($row['date7_draw_request'] =='1' && $row['date7_draw_approved'] =='1' && $row['date7_draw_submit'] =='1'){

                         $status_7='Submitted';
				}
					
					elseif($row['date7_draw_request'] =='1')
					{

						$status_7='Needs Approval';
				}
				
          elseif($row['date7_draw_submit'] =='1')
					{
						$status_7='Submitted';
				}
				
       elseif($row['date7_draw_approved'] =='1')
	 				{
						$status_7='Approved';
				}
				
				
				else{

				 $status_7='';	
				}



        	?>

        	<?php if($status_7 != ''){?>
			<tr>
				<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street']; ?></a></td>
				<td>$<?php echo number_format($row['draws_amount_7']); ?></td>
					<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'].'#test';?>"><?php echo $status_7; ?></a></td>
			</tr>
			<?php } ?>
			<?php  }else{echo "";}?>

			   <?php if($row['date8_draw_released'] !='1' &&  $row['draws_amount_8']) { 

        		if($row['date8_draw_request'] =='1' && $row['date8_draw_submit'] =='1'){

                         $status_8='Submitted';
				}

				elseif($row['date8_draw_request'] =='1' && $row['date8_draw_approved'] =='1'){

                         $status_8='Approved';
				}

				elseif($row['date8_draw_request'] =='1' && $row['date8_draw_approved'] =='1' && $row['date8_draw_submit'] =='1'){

                         $status_8='Submitted';
				}
					
					elseif($row['date8_draw_request'] =='1')
					{

						$status_8='Needs Approval';
				}
				
          elseif($row['date8_draw_submit'] =='1')
					{
						$status_8='Submitted';
				}
				
       elseif($row['date8_draw_approved'] =='1')
	 				{
						$status_8='Approved';
				}
				
				
				else{

				 $status_8='';	
				}



        	?>
        	<?php if($status_8 != ''){?>
			<tr>
				<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street']; ?></a></td>
				<td>$<?php echo number_format($row['draws_amount_8']); ?></td>
					<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'].'#test';?>"><?php echo $status_8; ?></a></td>
			</tr>
			<?php } ?>
			<?php  }else{echo "";}?>



			   <?php if($row['date9_draw_released'] !='1' &&  $row['draws_amount_9']) { 

        		if($row['date9_draw_request'] =='1' && $row['date9_draw_submit'] =='1'){

                         $status_9='Submitted';
				}

				elseif($row['date9_draw_request'] =='1' && $row['date9_draw_approved'] =='1'){

                         $status_9='Approved';
				}

				elseif($row['date9_draw_request'] =='1' && $row['date9_draw_approved'] =='1' && $row['date9_draw_submit'] =='1'){

                         $status_9='Submitted';
				}
					
					elseif($row['date9_draw_request'] =='1')
					{

						$status_9='Needs Approval';
				}
				
          elseif($row['date9_draw_submit'] =='1')
					{
						$status_9='Submitted';
				}
				
       elseif($row['date9_draw_approved'] =='1')
	 				{
						$status_9='Approved';
				}
				
				
				else{

				 $status_9='';	
				}



        	?>

        	<?php if($status_9 != ''){?>
			<tr>
				<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street']; ?></a></td>
				<td>$<?php echo number_format($row['draws_amount_9']); ?></td>
					<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'].'#test';?>"><?php echo $status_9; ?></a></td>
			</tr>
			<?php } ?>
			<?php  }else{echo "";}?>



			  <?php if($row['date10_draw_released'] !='1' &&  $row['draws_amount_10']) { 

        		if($row['date10_draw_request'] =='1' && $row['date10_draw_submit'] =='1'){

                         $status_10='Submitted';
				}

				elseif($row['date10_draw_request'] =='1' && $row['date10_draw_approved'] =='1'){

                         $status_10='Approved';
				}

				elseif($row['date10_draw_request'] =='1' && $row['date10_draw_approved'] =='1' && $row['date10_draw_submit'] =='1'){

                         $status_10='Submitted';
				}
					
					elseif($row['date10_draw_request'] =='1')
					{

						$status_10='Needs Approval';
				}
				
          elseif($row['date10_draw_submit'] =='1')
					{
						$status_10='Submitted';
				}
				
       elseif($row['date10_draw_approved'] =='1')
	 				{
						$status_10='Approved';
				}
				
				
				else{

				 $status_10='';	
				}



        	?>
        	<?php if($status_10 != ''){?>
			<tr>
				<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street']; ?></a></td>
				<td>$<?php echo number_format($row['draws_amount_10']); ?></td>
					<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'].'#test';?>"><?php echo $status_10; ?></a></td>
			</tr>
			<?php } ?>

			<?php  }else{echo "";}?>

			 <?php if($row['date11_draw_released'] !='1' &&  $row['draws_amount_11']) { 

        		if($row['date11_draw_request'] =='1' && $row['date11_draw_submit'] =='1'){

                         $status_11='Submitted';
				}

				elseif($row['date11_draw_request'] =='1' && $row['date11_draw_approved'] =='1'){

                         $status_11='Approved';
				}

				elseif($row['date11_draw_request'] =='1' && $row['date11_draw_approved'] =='1' && $row['date11_draw_submit'] =='1'){

                         $status_11='Submitted';
				}
					
					elseif($row['date11_draw_request'] =='1')
					{

						$status_11='Needs Approval';
				}
				
          elseif($row['date11_draw_submit'] =='1')
					{
						$status_11='Submitted';
				}
				
       elseif($row['date11_draw_approved'] =='1')
	 				{
						$status_11='Approved';
				}
				
				
				else{

				 $status_11='';	
				}



        	?>
        	<?php if($status_11 != ''){?>
			<tr>
				<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street']; ?></a></td>
				<td>$<?php echo number_format($row['draws_amount_11']); ?></td>
					<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'].'#test';?>"><?php echo $status_11; ?></a></td>
			</tr>
			<?php } ?>
			<?php  }else{echo "";}?>

			 <?php if($row['date12_draw_released'] !='1' &&  $row['draws_amount_12']) { 

        		if($row['date12_draw_request'] =='1' && $row['date12_draw_submit'] =='1'){

                         $status_12='Submitted';
				}

				elseif($row['date12_draw_request'] =='1' && $row['date12_draw_approved'] =='1'){

                         $status_12='Approved';
				}

				elseif($row['date12_draw_request'] =='1' && $row['date12_draw_approved'] =='1' && $row['date12_draw_submit'] =='1'){

                         $status_12='Submitted';
				}
					
					elseif($row['date12_draw_request'] =='1')
					{

						$status_12='Needs Approval';
				}
				
          elseif($row['date12_draw_submit'] =='1')
					{
						$status_12='Submitted';
				}
				
       elseif($row['date12_draw_approved'] =='1')
	 				{
						$status_12='Approved';
				}
				
				
				else{

				 $status_12='';	
				}



        	?>
        	<?php if($status_12 != ''){?>
			<tr>
				<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street']; ?></a></td>
				<td>$<?php echo number_format($row['draws_amount_12']); ?></td>
					<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'].'#test';?>"><?php echo $status_12; ?></a></td>
			</tr>
			<?php } ?>
			<?php  }else{echo "";}?>

			 <?php if($row['date13_draw_released'] !='1' &&  $row['draws_amount_13']) { 

        		if($row['date13_draw_request'] =='1' && $row['date13_draw_submit'] =='1'){

                         $status_13='Submitted';
				}

				elseif($row['date13_draw_request'] =='1' && $row['date13_draw_approved'] =='1'){

                         $status_13='Approved';
				}

				elseif($row['date13_draw_request'] =='1' && $row['date13_draw_approved'] =='1' && $row['date13_draw_submit'] =='1'){

                         $status_13='Submitted';
				}
					
					elseif($row['date13_draw_request'] =='1')
					{

						$status_13='Needs Approval';
				}
				
          elseif($row['date13_draw_submit'] =='1')
					{
						$status_13='Submitted';
				}
				
       elseif($row['date13_draw_approved'] =='1')
	 				{
						$status_13='Approved';
				}
				
				
				else{

				 $status_13='';	
				}



        	?>
        	<?php if($status_13 != ''){?>
			<tr>
				<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street']; ?></a></td>
				<td>$<?php echo number_format($row['draws_amount_13']); ?></td>
					<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'].'#test';?>"><?php echo $status_13; ?></a></td>
			</tr>
			<?php } ?>
			<?php  }else{echo "";}?>


			 <?php if($row['date14_draw_released'] !='1' &&  $row['draws_amount_14']) { 

        		if($row['date14_draw_request'] =='1' && $row['date14_draw_submit'] =='1'){

                         $status_14='Submitted';
				}

				elseif($row['date14_draw_request'] =='1' && $row['date14_draw_approved'] =='1'){

                         $status_14='Approved';
				}

				elseif($row['date14_draw_request'] =='1' && $row['date14_draw_approved'] =='1' && $row['date14_draw_submit'] =='1'){

                         $status_14='Submitted';
				}
					
					elseif($row['date14_draw_request'] =='1')
					{

						$status_14='Needs Approval';
				}
				
          elseif($row['date14_draw_submit'] =='1')
					{
						$status_14='Submitted';
				}
				
       elseif($row['date14_draw_approved'] =='1')
	 				{
						$status_14='Approved';
				}
				
				
				else{

				 $status_14='';	
				}



        	?>
        	<?php if($status_14 != ''){?>
			<tr>
				<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street']; ?></a></td>
				<td>$<?php echo number_format($row['draws_amount_14']); ?></td>
					<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'].'#test';?>"><?php echo $status_14; ?></a></td>
			</tr>
			<?php } ?>
			<?php }else{echo "";}?>

			 <?php if($row['date15_draw_released'] !='1' &&  $row['draws_amount_15']) { 

        		if($row['date15_draw_request'] =='1' && $row['date15_draw_submit'] =='1'){

                         $status_15='Submitted';
				}

				elseif($row['date15_draw_request'] =='1' && $row['date15_draw_approved'] =='1'){

                         $status_15='Approved';
				}

				elseif($row['date15_draw_request'] =='1' && $row['date15_draw_approved'] =='1' && $row['date15_draw_submit'] =='1'){

                         $status_15='Submitted';
				}
					
					elseif($row['date15_draw_request'] =='1')
					{

						$status_15='Needs Approval';
				}
				
          elseif($row['date15_draw_submit'] =='1')
					{
						$status_15='Submitted';
				}
				
       elseif($row['date15_draw_approved'] =='1')
	 				{
						$status_15='Approved';
				}
				
				
				else{

				 $status_15='';	
				}



        	?>
        	<?php if($status_15 != ''){?>
			<tr>
				<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street']; ?></a></td>
				<td>$<?php echo number_format($row['draws_amount_15']); ?></td>
					<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'].'#test';?>"><?php echo $status_15; ?></a></td>
			</tr>

		
			<?php } ?>
			<?php  }else{echo "";}?>
			<?php
		}
	 
}
	?>
	</tbody>
</table>