<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="2">Post Loan Closing</th>
		</tr>
		<tr>
			<th>Property Address</th>
			<th>Loan Amount</th>
		</tr>

	</thead>
	<tbody>	
		<?php if(isset($LoanClosingPosted) && is_array($LoanClosingPosted)){ 
				foreach($LoanClosingPosted as $Row){ 

					?>
					<tr>
						<td><a href="<?php echo base_url();?>load_data/<?php echo $Row['loan_id'];?>"><?php echo $Row['property_address'];?></a></td>
						<td>$<?php echo number_format($Row['loan_amount']);?></td>
						
					</tr>
				<?php } }else{ ?>
					<tr>
						<td colspan="2">No data found!</td>
					</tr>
				<?php } ?>
	</tbody>
</table>