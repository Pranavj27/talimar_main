<!-- <?php error_reporting(0);?> -->

<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="10">Available Trust Deeds</th>
		</tr>
		<tr>
			<th>Property Address</th>
			<th>Borrower</th>
			<th>Loan Amount</th>
			<th>Unfunded</th>
			<th>Note Rate</th>
			<th>Lender Rate</th>
			<th>LTV</th>
			<th>Term</th>
			<th>Marketing Status</th>
			<th>Closing Date</th>
		</tr>

	</thead>
	<tbody>	
		
		<?php 

			$closing_status_option = $this->config->item('closing_status_option');
			$market_trust_deed_new_option = $this->config->item('market_trust_deed_new_option');

			$countatd = 0;
			$total_amount = 0;
			$unfunded = 0;
			$interest_rate = 0;
			$term_month = 0;
			$ltv = 0;
			$invester_yield_percent = 0;

			/*echo '<pre>';
			print_r($active_deed);
			echo '</pre>';*/

			if(isset($active_deed) && is_array($active_deed)){
				foreach($active_deed as $row){ 

				$countatd++;
				$total_amount += $row['loan_amount'];
				$unfunded += $row['unfunded'];
				$interest_rate += $row['interest_rate'];
				$term_month += $row['term_month'];
				$ltv += $row['ltv'];
				$invester_yield_percent += $row['invester_yield_percent'];


		?>
				<tr>
					<td>
						<a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['property_address'];?>
					</td>
					<td>
						<a href="<?php echo base_url();?>borrower_view/<?php echo $row['borrower'];?>"><?php echo $all_borrower_name[$row['borrower']];?>
					</td>
					<td>$<?php echo number_format($row['loan_amount'],2);?></td>
					<td>$<?php echo number_format($row['unfunded'],2);?></td>
					<td><?php echo number_format($row['interest_rate'],3);?>%</td>
					<td><?php echo number_format($row['invester_yield_percent'],3);?>%</td>
					<td><?php echo number_format($row['ltv'],2);?>%</td>
					<td><?php echo $row['term_month'];?> Months</td>
					<td><?php echo $market_trust_deed_new_option[$row['closing_status']];?></td>
					<td><?php echo date('m-d-Y', strtotime($row['loan_funding_date']));?></td>					
				</tr>

			<?php } }else{ ?>
				<tr>
					<td style="width: 100%;">Data not found!</td>
				</tr>
			<?php } ?>
	</tbody>
	<tfoot class="foot_light_blue">
		<tr>
			<th>Total: <?php echo $countatd;?></th>
			<th></th>
			<th>$<?php echo number_format($total_amount,2);?></th>
			<th>$<?php echo number_format($unfunded,2);?></th>
			<th><?php echo number_format($interest_rate/$countatd,3);?>%</th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
		<tr>
			<th>Average:</th>
			<th></th>
			<th>$<?php echo number_format($total_amount/$countatd,2);?></th>
			<th>$<?php echo number_format($unfunded/$countatd,2);?></th>
			<th></th>
			<th><?php echo number_format($invester_yield_percent/$countatd,2);?>%</th>
			<th><?php echo number_format($ltv/$countatd,2);?>%</th>
			<th><?php echo number_format($term_month/$countatd);?> Months</th>
			<th></th>
			<th></th>
		</tr>
		
	</tfoot>
</table>