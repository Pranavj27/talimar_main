<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead>
								<tr class="thead_dark_blue">
									<th colspan="3">Top Lender's</th>
								</tr>
								<tr>
									<th>Contact Name</th>
									<th>Active<br>Loans</th>
									<th>Active Balance</th>
								
								</tr>
							</thead>
							<tbody>
								<?php 
							
								$number_counting = 0;
								
									
								foreach($top_lender_datas as $key => $row)
								{
									
										$next_key = $key+1;
										$previous_key = $key-1;
										
										if($top_lender_datas[$key]['contact_id'] == $top_lender_datas[$next_key]['contact_id'])
										{
											$inv_total = $top_lender_datas[$key]['total_investment'] + $top_lender_datas[$next_key]['total_investment'];
											
											$count_row  = $top_lender_datas[$key]['count'] + $top_lender_datas[$next_key]['count'];
										}
										else
										{
											$inv_total = $row['total_investment'];
											$count_row = $row['count'];
										}
										
										if($top_lender_datas[$key]['contact_id'] != $top_lender_datas[$previous_key]['contact_id'])
										{
											$number_counting++;
											
											$top_lender_array[$inv_total] = array(
															
															'contact_id' => $row['contact_id'],
															'contact_name' => $row['contact_name'],
															'count_row' => $count_row,
															'inv_total' => $inv_total,
															);
															
											$r_sort_top_lender_array[] = $inv_total;
										
									
										}
 									if($number_counting >=5)
									{
										break;
									}  
								}
								
								rsort($r_sort_top_lender_array);
								
								foreach($r_sort_top_lender_array as $row_inv)
								{
									?>
									<tr>
										<td><a href = "<?php echo base_url().'viewcontact/'.$top_lender_array[$row_inv]['contact_id']?>"><?php echo $top_lender_array[$row_inv]['contact_name'];?></a></td>
										<td class="data-center"><?php echo $top_lender_array[$row_inv]['count_row'];?></td>
										<td align = "right">$<?php echo number_format($row_inv);?></td>
									</tr>
									<?php
								}
								
								?>
							</tbody>
							
						</table>