<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">

	<thead>
		<tr class="thead_dark_blue">
			<th colspan="3">Unassigned Active Loans</th>
		</tr>
		<tr>
			<th>Address</th>
			<th>Balance</th>
			<th>Loan Amount</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$sumb = 0;
		$bal = 0;
		if (isset($balance_sheet)):
			foreach ($balance_sheet as $row): 
				$sumb += $row['loan_amount'];
				$bal += $row['avilable_balence'];
		?>
				<tr>
					<td style = "text-align: left;"><a href="<?php echo base_url() . 'load_data/' . $row['loan_id']; ?>"><?php echo $row['address']; ?></a></td>
					<td style = "text-align: left;">$<?php echo number_format($row['avilable_balence']); ?></td>
					<td style = "text-align: left;">$<?php echo number_format($row['loan_amount']); ?></td>
				</tr>
				<?php endforeach;
		endif;
		?>

	</tbody>
	<tfoot>
		<tr>
			<th>Total:</th>
			<th>$<?php echo number_format($bal,2);?></th>
			<th>$<?php echo number_format($sumb,2);?></th>
		</tr>
	</tfoot>

</table>
