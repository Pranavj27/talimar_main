<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
						<thead>
							<tr class="thead_dark_blue">
								<th colspan="3">Loan Servicing</th>
							</tr>
							<tr>
								<th>Item</th>
								<th>Count</th>
								<th>Balance</th>
							</tr>
						</thead>
						<tbody>
						
								<tr>
									<td style = "text-align: left;"><a href="<?php echo base_url().'existing_loan_schedule' ?>">FCI Lender Servicing</a></td>
								<td style = "text-align: left;" ><?php echo $count_fci_loan; ?></td>
								<td style = "text-align: left;" ><?php echo '$'.number_format($fci_loans_total_amount); ?></td>
								</tr>
								<tr>
									<td style = "text-align: left;" ><a href="<?php echo base_url().'existing_loan_schedule' ?>">Del Toro Loan Servicing</a></td>
									<td style = "text-align: left;" ><?php echo $count_del_toro_loan; ?></td>
									<td style = "text-align: left;" ><?php echo '$'.number_format($del_toro_loans_total_amount,2); ?></td>
								</tr>
								<tr>
									<td style = "text-align: left;" ><a href="<?php echo base_url().'existing_loan_schedule' ?>">TaliMar Financial Servicing</a></td>
									<td style = "text-align: left;" ><?php echo $count_talimar_loan ; ?></td>
									<td style = "text-align: left;" ><?php echo '$'.number_format($talimar_loans_total_amount,2); ?></td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<th>Total:</th>
									<th>
										<?php echo number_format($count_talimar_loan + $count_del_toro_loan + $count_fci_loan,2);?>
									</th>
									<th>$<?php echo number_format($talimar_loans_total_amount + $del_toro_loans_total_amount + $fci_loans_total_amount,2);?>
									</th>
								</tr>
							</tfoot>
					</table>