
<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead>
								<tr class="thead_dark_blue">
									<th colspan="3">Late Payments</th>
								</tr>
								<tr>
									<th>Street Address</th>
									<th>Days Late</th>
									<th>Due Date</th>
								
								</tr>
							</thead>
							<tbody>
								
								<?php
								
								
									
									if(!empty($loanData)){
										foreach($loanData as $key => $row){	
										
										$date = explode('-', $row->nextDueDate);

										$addressName = '';
										if(!empty($row->property_address))
										{
											$addressName .= $row->property_address;
										}

										

										
									?>  
									<tr>
										<td><a href="<?php echo base_url().'load_data/'.$row->loan_id; ?>"><?php echo $addressName; ?></a></td>
										<td><?php echo $row->daysLate;?> </td>
										<td><?php echo $date[1].'-'.$date[2].'-'.$date[0];?></td>
									</tr>
									<?php } } else{ ?>
										<tr>
											<td colspan="3"> No data found!</td>
										</tr>
									<?php } ?>
								
							</tbody>

						</table>	