<?php
// echo '<pre>';
// print_r($default_loan);
// echo '</pre>';

?>
<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead>
								<tr class="thead_dark_blue">
									<th colspan="3">Loans in Default</th>
								</tr>
								<tr>
									<th>Street Address</th>
									<th>Loan Amount</th>
									<th>Status</th>
								
								</tr>
							</thead>
							<tbody>
								
								<?php
								$serviceing_reason_option	= $this->config->item('serviceing_reason_option');
								$foreclosuer_status_option	= $this->config->item('foreclosuer_status');
								$foreclosuer_step_option	= $this->config->item('foreclosuer_step');

									$total_loan_amount = 0;
									$count_loan_default = 0;
									if(isset($default_loan) && is_array($default_loan)){
										foreach($default_loan as $key => $row){	?>
								<tr>
									<td style = "text-align: left;" >
										<a href="<?php echo base_url().'load_data/'.$default_loan[$key]['loan_id'];?>#forcloser_status"><?php echo $default_loan[$key]['property_address'];?></a>
									</td>
									<td style = "text-align: left;" >
										$<?php echo number_format($default_loan[$key]['loan_amount']); ?>
									</td>
									<!--<td style = "text-align: left;" >
										<?php //echo $default_loan[$key]['servicing_reason'] ? $serviceing_reason_option[$default_loan[$key]['servicing_reason']] :'';?>
									</td>-->
									<td style = "text-align: left;">										
										<a href="<?php echo base_url().'load_data/'.$default_loan[$key]['loan_id'];?>#forcloser_status"><?php echo  $default_loan[$key]['foreclosuer_step'] ? $foreclosuer_step_option[$default_loan[$key]['foreclosuer_step']] : ''; ?></a>
									</td>
								</tr>
								<?php 
									$total_loan_amount = $total_loan_amount + $default_loan[$key]['loan_amount'];
									$count_loan_default++;
										
									} }else {
								?>
								<tr>
									<td style="text-align:center;" colspan="3">No data found!</td>
								</tr>
								<?php } ?>
							</tbody>
							<tfoot class="foot_light_blue">
								<tr>
								<th> Total: <?php echo $count_loan_default;?></th>
								<th>$<?php echo  number_format($total_loan_amount);?> </th>
								<th></th>
								</tr>
							</tfoot>
						</table>	