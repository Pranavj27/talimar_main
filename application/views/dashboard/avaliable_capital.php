<?php

?>

<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="2">Available Credit</th>
		</tr>
		<tr>
			<th>Item</th>
			<th>Amount</th>
		</tr>
	</thead>
	<tbody>	
		
       <?php 

	
       $total_available_credit=0;
       $total_rem=0;
       $total_drawn=0;
       foreach($fund_data_result as $row): 
  		 
  
  		 $total_available_credit +=$row->available_credit;
  		 $total_drawn +=$row->drawn;
 		 $total_rem  +=$row->available_credit-$row->drawn;
        
		endforeach;?>

		<tr>
			<td style = "text-align: left;">Credit Limit</td>
			<td style = "text-align: left;"><?php echo '$'.number_format($total_available_credit);?></td>
			
		</tr>

	<tr>
			<td style = "text-align: left;">Outstanding Credit</td>
			<td style = "text-align: left;"><?php echo '$'.number_format($total_drawn);?></td>
			
		</tr>

	<tr>
			<td style = "text-align: left;">Available Credit</td>
		<td style = "text-align: left;"><?php echo '$'.number_format($total_rem);?></td>
			
		</tr>

	</tbody>
		
</table>	
