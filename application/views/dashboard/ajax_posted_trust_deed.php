<?php

?>

<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="3">Posted Trust Deeds</th>
		</tr>
		<tr>
			<th>Street Address</th>
			<th>Loan Amount</th>
			<th>Balance</th>
		</tr>
	</thead>
	<tbody>	
		<?php 

        if(isset($fetch_post_trust_deed) && is_array($fetch_post_trust_deed)){
			foreach($fetch_post_trust_deed as $row){ ?>
				<tr>
					<td style = "text-align: left;width:50%;"><a href="<?php echo base_url()?>load_data/<?php echo $row['loan_id'];?>#marketing_option"><?php echo $row['property_address'];?></td>
					<td style = "text-align: left; width:25%;">$<?php echo number_format($row['loan_amount']);?></td>
					<td style = "text-align: left;width:25%;">$<?php echo number_format($row['avaliable_balance']);?></td>
				</tr>
		<?php } }else{ ?>
			<td colspan="3" style="text-align: center;">No data found!</td>
		<?php } ?>
		
	</tbody>
		
</table>	
