<?php
// echo '<pre>';
// print_r($capital_requirement_for_fiveday);
// print_r($capital_requirement_for_tenday);
// print_r($capital_requirement_for_fifteen);
// print_r($capital_requirement_for_thirty);
// echo '</pre>';
//error_reporting(0);
$amount = 0;
$investment = 0;
foreach($capital_requirement_for_fiveday as $row){

	$amount += $row['loan_amount'];
	$investment += $row['total_investment'];
}


$tenday_amount = 0;
$tenday_investment = 0;
foreach($capital_requirement_for_tenday as $row){

	$tenday_amount += $row['loan_amount'];
	$tenday_investment += $row['total_investment'];
}

$fifteen_amount = 0;
$fifteen_investment = 0;
foreach($capital_requirement_for_fifteen as $row){

	$fifteen_amount += $row['loan_amount'];
	$fifteen_investment += $row['total_investment'];
}

$thirty_amount = 0;
$thirty_investment = 0;
foreach($capital_requirement_for_thirty as $row){

	$thirty_amount += $row['loan_amount'];
	$thirty_investment += $row['total_investment'];
}


?>
<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="2">Capital Contribution Requirement</th>
		</tr>
		<tr>
			<th>Days</th>
			<th>Available Balance</th>
		</tr>
	</thead>
	<tbody>	
		<tr>
			<td style = "text-align: left;">5 Days</td>
			<td style = "text-align: left;">$<?php echo number_format($amount - $investment);?></td>
		</tr>
		<tr>
			<td style = "text-align: left;">10 Days</td>
			<td style = "text-align: left;">$<?php echo number_format($tenday_amount - $tenday_investment);?></td>
		</tr>
		<tr>
			<td style = "text-align: left;">15 Days</td>
			<td style = "text-align: left;">$<?php echo number_format($fifteen_amount - $fifteen_investment);?></td>
		</tr>
		<tr>
			<td style = "text-align: left;">30 Days</td>
			<td style = "text-align: left;">$<?php echo number_format($thirty_amount - $thirty_investment);?></td>
		</tr>
		
	</tbody>
		
</table>	
