<?php

?>

<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="3">45 Day Insurance Renewal Schedule</th>
		</tr>
		<tr>
			<th>Address</th>
			<th>Expire date</th>
			<th>Days</th>
		</tr>

	</thead>
	<tbody>	
		
       <?php 
	   
       foreach($fetch_property_insurance_loan as $row){ 
	   
			$currentDAte = date('Y-m-d');
			$addmoreday = date('Y-m-d', strtotime($currentDAte.'+45 days'));
			
			if($row['insurance_expiration'] < $addmoreday){
				
				$now = strtotime($currentDAte); // or your date as well
				$your_date = strtotime($row['insurance_expiration']);
				$datediff = $your_date - $now;
				$duration = floor($datediff / (60 * 60 * 24));
				//$duration = abs(round($datediff / (60 * 60 * 24)));
	   
	   ?>
			<tr>
				<td style="text-align: left;width:65%;"><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['property_address'];?></a></td>
				<td style = "text-align: left;"><?php echo date('m-d-Y', strtotime($row['insurance_expiration']));?></td>
				<td style = "text-align: left;"><?php echo $duration;?></td>
				
			</tr>
			
	   <?php }  } ?>

	</tbody>
		
</table>	
