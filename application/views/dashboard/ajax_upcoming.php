<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="3">Requested Demands</th>
		</tr>
		<tr>
			<th>Street Address</th>
			<th>Loan Balance</th>
			<th>Date</th>
		</tr>
	</thead>
	<tbody>	
		
   		 <?php
//    		 echo '<pre>';
// print_r($upcoming_payoff_schedule);
//  echo '</pre>';
				 $count = 0;
				 $total_loan_amount = 0;
				 if(isset($upcoming_payoff_schedule) && is_array($upcoming_payoff_schedule))
				 {
					 foreach($upcoming_payoff_schedule as $row)
					 {
						 $date = date('Y-m-d');
						 if($date > $row['demand_requested_date']){
							 $addcss = 'style = "text-align: left;color:red;"';
						 }else{
							 $addcss = 'style = "text-align: left;"';
						 }

						 if($row['de_Admin_Approved'] != '1'){
						 	$addcss1 = 'style = "text-align: left;color:red;"';
						 }else{
						 	$addcss1 = 'style = "text-align: left;"';
						 }
			?>

						<tr>
							<td style = "text-align: left;"><a href="<?php echo base_url().'load_data/'.$row['loan_id']; ?>" <?php echo $addcss1;?>><?php echo $row['property_address']; ?></a></td>
						    <td style = "text-align: left;"><?php echo '$'.number_format($row['current_balance'],2);?></td>
						    <td><a <?php echo $addcss;?> href="<?php echo base_url().'load_data/'.$row['loan_id']; ?>#payoff_pro"><?php echo $row['demand_requested_date'] ? date('m-d-Y', strtotime($row['demand_requested_date'])) : '';?></a></td>
							
						</tr>
		<?php
			$count = $count + 1;
			$total_loan_amount = $total_loan_amount+$row['loan_amount'];
					 }
				 }else{?>

				 	<td style="text-align: center;" colspan="3">No data found!</td>
				<?php }
				 
		?>
	</tbody>
		<tfoot>
					<tr>
						<th>Total: <?php echo $count; ?></th>
						<th><?php echo '$'.number_format($total_loan_amount,2); ?></th>
						<th></th>
						
					</tr>
				
				</tfoot>
</table>	
