<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="3">Blast Social Media</th>
		</tr>
		<tr>
			<th>Property Address</th>
			<th>Loan Amount</th>
			<th>Date Closed</th>
		</tr>

	</thead>
	<tbody>	
		<?php if(isset($SocialMediaBlast) && is_array($SocialMediaBlast)){ 
				foreach($SocialMediaBlast as $Row){ 

					?>
					<tr>
						<td><a href="<?php echo base_url();?>load_data/<?php echo $Row['loan_id'];?>"><?php echo $Row['property_address'];?></a></td>
						<td>$<?php echo number_format($Row['loan_amount']);?></td>
						<td><?php echo $Row['loan_funding_date'];?></td>
					</tr>
				<?php } }else{ ?>
					<tr>
						<td colspan="2">No data found!</td>
					</tr>
				<?php } ?>
	</tbody>
</table>