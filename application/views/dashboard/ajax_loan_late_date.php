<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
						<thead>
							<tr class="thead_dark_blue">
								<th colspan="3">10+ Days Late</th>
							</tr>
							<tr>
								<th>Property Address</th>
								<th>Borrower Name</th>
								<th>Payment Amount</th>
							</tr>
						</thead>
						<tbody>
									<?php foreach ($fetch_late_date as $row) {?>
								<tr>
									<td style = "text-align: left;"><a href="<?php echo base_url() . 'load_data/' . $row['loan_id']; ?>"><?php echo $row['property_address']; ?></a></td>
								<td style = "text-align: left;" ><?php echo $row['borrower']; ?></td>
								<td style = "text-align: left;" ><?php echo '$' . number_format($row['monthly'], 2); ?></td>
								</tr>
									<?php }?>

							</tbody>
					</table>