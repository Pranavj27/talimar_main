<!--
		Description : This function  get data for swag Request
					  
		Author      : Bitcot
		Created     : 
		Modified    :01-04-2021
		Modified    :06-04-2021
-->
<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="100%">Talimar Swag Request</th>
		</tr>
		<tr>
			<th>Date</th>
			<th>Contact</th>
		</tr>
	</thead>
	<tbody>
		
		<?php if(isset($contact_marketing_swag) && is_array($contact_marketing_swag)){

				foreach ($contact_marketing_swag as $value) { ?>
				
				<tr>
					<td><a href="<?php echo base_url()?>viewcontact/<?php echo $value['contact_id'];?>#Marketing"><?php echo $value['date'];?></td>
					<td><a href="<?php echo base_url()?>viewcontact/<?php echo $value['contact_id'];?>#Marketing"><?php echo $value['name'];?></td>
				</tr>

		<?php } } ?>
	</tbody>
</table>