<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
						<thead>
							<tr class="thead_dark_blue">
								<th colspan="3">Portfolio Diversification</th>
							</tr>
							<tr>
								<th>Loan Type</th>
								<th># of Loans</th>
								<th>% of Portfolio</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Bridge Loan</td>
								<td><?php echo $fetch_type_1; ?></td>
								<td><?php echo number_format($fetch_type_1 / $fetch_total * 100, 2) . '%'; ?></td>

							</tr>
							<tr>
								<td>Fix and Flip</td>
								<td><?php echo $fetch_type_2; ?></td>
								<td><?php echo number_format($fetch_type_2 / $fetch_total * 100, 2) . '%'; ?></td>

							</tr>
							<tr>
								<td>Fix and Hold</td>
								<td><?php echo $fetch_type_4; ?></td>
								<td><?php echo number_format($fetch_type_4 / $fetch_total * 100, 2) . '%'; ?></td>

							</tr>
							<tr>

								<td>New Construction</td>
								<td><?php echo $fetch_type_3; ?></td>
								<td><?php echo number_format($fetch_type_3 / $fetch_total * 100, 2) . '%'; ?></td>

							</tr>

						</tbody>
					</table>