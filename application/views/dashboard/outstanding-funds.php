<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="2">Outstanding Funds</th>
		</tr>
		<tr>
			<th>Street Address</th>
			<th>Amount</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$sum = 0;
		if(isset($fundsData) && is_array($fundsData)){ 
				foreach($fundsData as $row){ 

					$sum += $row->loan_amount;
			?>
					<tr>
						<td><a href="<?php echo base_url();?>load_data/<?php echo $row->loan_id; ?>"><?php echo $row->property_address;?></a></td>
						<td>$<?php echo number_format($row->loan_amount,2);?></td>
					</tr>
		<?php } } ?>
	</tbody>
	<tfoot>
		<tr>
			<th>Total:</th>
			<th>$<?php echo number_format($sum,2);?></th>
		</tr>
	</tfoot>
</table>
