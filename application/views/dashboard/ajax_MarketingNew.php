<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="2">Outstanding Marketing</th>
		</tr>
		<tr>
			<th>Property</th>
			<th>Loan Amount</th>
		</tr>

	</thead>
	<tbody>	
		<?php if(isset($fetchmarketing) && is_array($fetchmarketing)){ 
				foreach($fetchmarketing as $Row){ ?>
					<tr>
						<td>
							<a href="<?php echo base_url();?>load_data/<?php echo $Row->loan_id;?>"><?php echo $Row->property_address;?></a>
						</td>
						<td>$<?php echo number_format($Row->loan_amount);?></td>
						
					</tr>
				<?php } }else{ ?>
					<tr>
						<td>No data found!</td>
					</tr>
				<?php } ?>
	</tbody>
</table>