<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead>
								<tr class="thead_dark_blue">
								<th colspan="4">45 Day Maturity Schedule</th>
								</tr>
								<tr>
									
									<th>Address</th>
									<th>Loan<br>Amount</th>
									<th>Maturity</th>
									<th>Days</th>
								</tr>
							</thead>
							<tbody>
								<?php 
								
								if(isset($maturity_report_data) && is_array($maturity_report_data))
								{
									foreach($maturity_report_data as $row)
									{
										
										?>
										<tr>
											
											<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['s_property'];?></td>
											<td>$<?php echo number_format($row['loan_amount']);?></td>
											<td><?php echo $row['maturity_date'];?></td>
											<td><a title="maturity report" href="<?php echo base_url();?>maturity_report_schedule"><?php echo $row['duration'];?></a></td>
										</tr>
										<?php
									}
								}else{ ?>

									<td style="text-align:center;" colspan="4">No data found!</td>

								<?php }
								?>
							</tbody>
						</table>