<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead>
								<tr class="thead_dark_blue">
									<th colspan="3">Top Borrower's</th>
								</tr>
								<tr>
									<th>Borrower 	 </th>
									<th>Active<br>Loans</th>
									<th>Total<br>Balance</th>
								
								</tr>
							</thead>
							<tbody>
							<?php
							
							foreach($top_borrower_datas as $row)
							{
							?>
							<tr>
								<td><a href="<?php echo base_url().'borrower_data/view/'.$row['borrower_id']; ?>"><?php echo $row['borrower_name']; ?></a></td>
								<td class="data-center"><?php echo $row['count']; ?></td>
								<td>$<?php echo number_format($row['total_loan_amount']); ?></td>
							</tr>
							<?php } ?>
							</tbody>
							
						</table>