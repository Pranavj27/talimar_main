<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="2">Post Marketing Sign</th>
		</tr>
		<tr>
			<th>Property Address</th>
			<th>Loan Amount</th>
		</tr>

	</thead>
	<tbody>	
		<?php

		if(isset($SignNotPosted) && is_array($SignNotPosted)){
		 foreach($SignNotPosted as $row){ ?>
		
		<tr>
			<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['property_address'];?></a></td>
			<td>$<?php echo number_format($row['loan_amount'],2);?></td>
			
			
		</tr>
	<?php } } ?>
	</tbody>
</table>