<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="2">Outstanding Closing Fees</th>
		</tr>
		<tr>
			<th>Street Address</th>
			<th>Amount</th>
		</tr>
	</thead>
	<tbody>
		<?php 

		$count = 0;
		$total = 0;
		if(isset($feesData) && is_array($feesData)){ 
				foreach($feesData as $row) { 

					$count++;
					$total += $row['feeAmount'];
		?>
					<tr>
						<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['property_address'];?></a></td>
						<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>#closing_statement_new11"><?php echo '$'.number_format($row['feeAmount'],2);?></a></td>
					</tr>
				<?php } }else{ ?>
					<tr>
						<td colspan="2">No data found!</td>
					</tr>
				<?php } ?>
	</tbody>
	<tfoot>
		<tr>
			<th>Total: <?php echo $count;?></th>
			<th>$<?php echo number_format($total);?></th>
		</tr>
	</tfoot>
</table>