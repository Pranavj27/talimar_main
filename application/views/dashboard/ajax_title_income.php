<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="3">Outgoing Lender Wires</th>
		</tr>
		<tr>
			<th>Lender Name</th>
			<th>Amount</th>
			<th>Status</th>
		</tr>

	</thead>
	<tbody>	
		<?php  $assi_fund_received = $this->config->item('assi_fund_received'); ?>
		<?php 
		if(isset($fundsArray) && is_array($fundsArray)){
		foreach($fundsArray as $row){ ?>
		<tr>
			<td><?php echo $All_investors[$row['lender_name']];?></td>
			<td>$<?php echo number_format($row['investment'],2);?></td>
			<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>#status"><?php echo $assi_fund_received[$row['fund']];?></a></td>
			
			
		</tr>
	<?php } } ?>
	</tbody>
</table>