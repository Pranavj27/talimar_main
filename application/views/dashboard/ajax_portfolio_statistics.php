<?php

// echo '<pre>';
// print_r($count_single_lender);
// print_r($amount_single_lender);
// echo '</pre>';
?>
<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead>
								<tr class="thead_dark_blue">
									<th colspan="2">Lender Statistics</th>
								</tr>
								
							</thead>
							<tbody>
							<!--	<tr>
									<td style = "text-align: left;">Funded YTD</td>
									<td style = "text-align: left;">$<?php echo $loans_funded_loans_amt_in_current_year; ?></td>
								</tr>-->
									<tr>
									<td style = "text-align: left;">Total Active Lenders</td>
									<td style = "text-align: left;"><?php echo $total_lender_count; ?></td>
								</tr>
								<tr>
									<td style = "text-align: left;">Total # of Assigned Interests</td>
									<td style = "text-align: left;"><?php echo $lender_count_active; ?></td>
								</tr>
								<tr>
									<td style = "text-align: left;" >Avg. # of Lenders / Loan</td>
									<td style = "text-align: left;" ><?php echo number_format($lender_count_active/$active_loan_result['active_count'],2);?></td>
								</tr>
								<tr>
									<td style = "text-align: left;" >Avg. $ Assigned Interest</td>
									<td style = "text-align: left;" ><?php echo '$'.number_format($active_loan_result['total_loan_amount']/$lender_count_active); ?></td>
								</tr>
								<tr>
									<td style = "text-align: left;" ># of Single Lender Loans</td>
									<td style = "text-align: left;" ><?php echo $count_single_lender;?></td>
								</tr>
								<tr>
									<td style = "text-align: left;" >$ of Single Lender Loans</td>
									<td style = "text-align: left;" >$<?php echo number_format($amount_single_lender);?></td>
								</tr>
								<!--<tr>
									<td style = "text-align: left;">Avg. Loan Spread</td>
									<td style = "text-align: left;"><?php echo number_format($fetch_spread_data['sum_servicing']/$fetch_spread_data['count_loans'],2); ?>%</td>
								</tr>-->
								
								<!--<tr>
									<td style = "text-align: left;" style = "text-align: left;"># Paid Off Loans
									<td style = "text-align: left;" ><?php echo $paid_off_loans; ?></td>
								</tr>-->
								<!--<tr>
									<td style = "text-align: left;" style = "text-align: left;">$ Paid Off Loans
									<td style = "text-align: left;" >$<?php echo $paid_off_loans_amt; ?></td>
								</tr>-->
								<!--
								<tr>
									<td style = "text-align: left;" style = "text-align: left;"># Loans Funded
									<td style = "text-align: left;" ><?php echo $loans_funded_loans; ?></td>
								</tr>
								<tr>
									<td style = "text-align: left;" style = "text-align: left;">$ Loans Funded
									<td style = "text-align: left;" >$<?php echo $loans_funded_loans_amt; ?></td>
								</tr>
								-->
							</tbody>
								
						</table>	
