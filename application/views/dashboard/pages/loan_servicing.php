<div class="page-title">
	<h1 style="margin-left:14px !important;">Loan Servicing </h1>
</div>
<div class="row rc_class">
	<div class="col-md-12" id="">

		<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
			<thead >
				<tr class="foot_light_blue">
					<th colspan="3">Loan Servicing</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<div class="row rc_class">
	<div class="col-md-12" id="loan_servicing_overview"></div>
</div>
<div class="row rc_class">

	<div class="col-md-4" id="loan_servicing">

	</div>

	<div class="col-md-4 scrolling-dashboard">
		<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
			<thead >
				<tr class="thead_dark_blue">
					<th colspan="3">Loan Holding Schedule</th>
				</tr>
				<tr>

					<th>Address</th>
					<th>Servicer Loan #</th>
					<th>Neg Time</th>
				</tr>
			</thead>
			<tbody>
				<?php
			if (isset($holding_loan) && is_array($holding_loan)) {
				foreach ($holding_loan as $boarding_val) {

				if ($boarding_val['check'] != '1') {
					$color = 'red';
				} else {
					$color = '';
				}

				?>

				<tr>
					<td style = "text-align: left;"><a href="<?php echo base_url() . 'load_data/' . $boarding_val['loan_id']; ?>"><?php echo $boarding_val['p_address']; ?></a></td>

					<td class="<?php echo $color; ?>" style = "text-align: left;"><?php echo $boarding_val['servicer']; ?></td>
					<!--<td style = "text-align: left;">$<?php echo number_format($boarding_val['loan_amount']); ?></td>-->
					<td class="<?php echo $color; ?>" style = "text-align: left;"><?php echo $boarding_val['diff']; ?></td>

				</tr>
					<?php }} else {?>

						<td style = "text-align:center;" colspan="3">No data found!</td>
					<?php }?>

			</tbody>
		</table>
	</div>

	<div class="col-md-4" id="fci_servicing_schedule">
	</div>
</div>

<div class="row rc_class">
	<div class="col-md-4 scrolling-dashboard" id="outstanding_draw"></div>
	<div class="col-md-4 scrolling-dashboard" id="ajinsurance"></div>
	<div class="col-md-4 scrolling-dashboard" id="lateDayFCI"></div>
</div>

<div class="row rc_class">

	<div class="col-md-4" id="ajax_loan_service"></div>
	<div class="col-md-4" id="maturity_schedule"></div>
	<div class="col-md-4" id="Extension_Processing"></div>

</div>

<div class="row rc_class">
	<div class="col-md-4" id="default_loan"></div>
	<div class="col-md-4 scrolling-dashboard" id="upcoming_paidd"></div>
	<div class="col-md-4 " id="paid_offf_process"></div>

</div>

<div class="row rc_class">
	<div class="col-md-4" id="loan_closing_checklist"></div>
	<div class="col-md-4 scrolling-dashboard" id="AccruedCharges"></div>
	<div class="col-md-4" id="OutstandingServicerDeposits"></div>
</div>



<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/dashboard/dashboard-new.css"); ?>">
<script type="text/javascript" src="<?php echo base_url("assets/js/dashboard/common_dashboard.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/dashboard/loan_servicing.js"); ?>"></script>