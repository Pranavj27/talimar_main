<?php
$loan_type_option = $this->config->item('loan_type_option');
$loan_status_option = $this->config->item('loan_status_option');
$closing_status_option = $this->config->item('closing_status_option');
$serviceing_reason_option = $this->config->item('serviceing_reason_option');


?>
<!-- BEGIN CONTENT -->
<style>
.rc_class{
	padding:0px;
}
.rc_class td{
	text-align:left;
}
#LoadingImage{
	position : absolute;
	top:15%;
	left:30%;
	display: block;
}
.red{

color:red;
}

tr.foot_light_blue {
    background: rgba(75, 141, 248, 0.76);
    color: white;
}
</style>
	<div class="page-container">
			<!-- BEGIN PAGE HEAD -->

		<div class="container">
					<!--<div id="LoadingImage" style="display:none">
					  <img src="<?php echo base_url() ?>assets/global/img/loder.gif"/>
					</div>-->
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1 style="margin-left:14px !important;">Action Items</h1>
				</div>
				
				<div class="row rc_class">
						<div class="col-md-12" id="action_item" style="min-height:350px;display: block;margin-left: auto;margin-right: auto;">

								<!------ append action_item table here ------->
						</div>
					</div>

			


		</div>
	</div>
	

<!-- END CONTAINER -->

<script>


ajax_action_item();
function ajax_action_item(){
	$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url() . "Home/ajax_action_item"; ?>',
			data	: {},
			beforeSend: function() {
				
					$('div#action_item').html('<i class="fa fa-spinner fa-spin" style="font-size:40px;text-align:center; display:block"></i>');
			},
			success : function(response){

				if(response){
					
					$('div#action_item').html(response);
					//window.location.reload();
				}
			},

	});
}


</script>
