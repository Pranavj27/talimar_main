<div class="page-title">
	<h1 style="margin-left:14px !important;">Lender Servicing </h1>
</div>
<div class="row rc_class">
	<div class="col-md-12" id="">

		<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
			<thead >
				<tr class="foot_light_blue">
					<th colspan="3">Lender Servicing</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<div class="row rc_class">

	<div class="col-md-12" id="AssignmentProcess">

	</div>

</div>
<div class="row rc_class">
	<div class="col-md-12" id="active_trust_deed">

			<!---append active_trust_deed table here---->
	</div>
</div>


<div class="row rc_class">

		<div class="col-md-4" id="IncomingFunds">

		</div>

		<div class="col-md-4" id="titleIncomingFunds">

		</div>
		
		<div class="col-md-4 s" id="posted_trust">

		</div>
</div>

<div class="row rc_class">

	<div class="col-md-4" id="portfolio_statistics">
		<!------ append portfolio_statistics table here ------>
	</div>

	<div class="col-md-4" id="ready_signture">

	</div>

	<div class="col-md-4" id="LenderStatusProccig">
	</div>
	
</div>
<div class="row rc_class">
	<div class="col-md-4" id="top_active_investors">
	</div>
	<div class="col-md-4" id="top_funded_investors">
	</div>
</div>


<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/dashboard/dashboard-new.css"); ?>">
<script type="text/javascript" src="<?php echo base_url("assets/js/dashboard/common_dashboard.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/dashboard/lender_servicing.js"); ?>"></script>