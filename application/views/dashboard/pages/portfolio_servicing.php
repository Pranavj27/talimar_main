<div class="page-title">
	<h1 style="margin-left:14px !important;">Portfolio Servicing </h1>
</div>
<div class="row rc_class">
	<div class="col-md-12" id="">

		<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
			<thead >
				<tr class="foot_light_blue">
					<th colspan="3">Portfolio Servicing</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<div class="row rc_class">	

	<div class="col-md-4">
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
				<thead class="thead_dark_blue">
				<tr>
					<th colspan="2">Active Loan Portfolio</th>
				</tr>
				</thead>

				<tbody>
					<tr>
						<td style = "text-align: left;" ># of Loans</td>
						<td style = "text-align: left;"><?php echo $active_loan_result['active_count']; ?></td>
					</tr>

					<tr>
						<td style = "text-align: left;">Active Portfolio Balance</td>
						<td style = "text-align: left;" >$<?php echo number_format($active_loan_result['total_current_balance']); ?></td>
					</tr>
					<tr>
						<td style = "text-align: left;">Original Portfolio Balance</td>
						<td style = "text-align: left;" >$<?php echo number_format($active_loan_result['total_loan_amount']); ?></td>
					</tr>
					<?php
				$total_intrest_percent = number_format($active_loan_result['total_intrest_rate'], 3);
					$total_intrest_dollar = $active_loan_result['total_loan_amount'] * ($total_intrest_percent / 1200) / $active_loan_result['active_count'];

					//$active_ltv = (($active_loan_result['total_loan_amount'] + $active_loan_result['senior_current'] ) /$active_loan_result['total_arv'])*100;

					$active_ltv = ($active_loan_result['total_loan_amount'] / $active_loan_result['total_future_value']) * 100;

					$active_ltv_total = $active_ltv / $active_loan_result['active_count'];

					/* echo'<pre>';
													print_r($fetch_property_data);
													echo'</pre>'; */
					/* echo $active_ltv;
													echo'<br>';
													echo $active_ltv_total; */

					?>
					<tr>

						<td style = "text-align: left;">Avg. Loan Amount</td>
						<td style = "text-align: left;">$<?php echo number_format($active_loan_result['total_loan_amount'] / $active_loan_result['active_count']); ?></td>
					</tr>

					<tr>
						<td style = "text-align: left;">Avg. Loan Rate</td>
						<td style = "text-align: left;" ><?php echo $total_intrest_percent; ?>%</td>
					</tr>

					<tr>
						<td style = "text-align: left;" >Avg. LTV</td>
						<td style = "text-align: left;" ><?php echo number_format($active_ltv_total*100, 2); ?>%</td>
					</tr>

					<tr>
						<td style = "text-align: left;" >Avg. Term</td>
						<td style = "text-align: left;"><?php echo number_format($active_loan_result['total_term_month'], 1) . ' Months'; ?></td>
					</tr>



					<tr>
						<td style = "text-align: left;" >Avg. Loan Spread</td>
						<td style = "text-align: left;"><?php echo $spred_val_avg; ?>%</td>
					</tr>



					<!-- 		<tr>
						<td style = "text-align: left;" >Monthly Net Servicing Income</td>
						<td style = "text-align: left;" >$<?php echo number_format($total_servicing_income); ?></td>
					</tr> -->
					<tr>
						<td style = "text-align: left;">Loans in Default #</td>
						<td style = "text-align: left;" ><?php echo $fetch_total_default; ?></td>
					</tr>

					<tr>
						<td style = "text-align: left;">Loans in Default $</td>
						<td style = "text-align: left;" ><?php echo '$' . number_format($fetch_total_amount); ?></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="col-md-4" id="portfolio_divertification">
			<!------ append  portfolio_divertification table here ------->
		</div>						
		
		<div class="col-md-4" id="year_portfolio_statistics">

		</div>
</div>

<div class="row rc_class">

	<div class="col-md-4" id="construction_loan">
		<!------ append construction_loan table here ------->
	</div>

	

</div>	

<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/dashboard/dashboard-new.css"); ?>">
<script type="text/javascript" src="<?php echo base_url("assets/js/dashboard/common_dashboard.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/dashboard/portfolio_servicing.js"); ?>"></script>