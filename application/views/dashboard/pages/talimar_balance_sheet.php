<div class="page-title">
	<h1 style="margin-left:14px !important;">TaliMar Balance Sheet </h1>
</div>
<div class="row rc_class">
	<div class="col-md-12" id="">

		<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
			<thead >
				<tr class="foot_light_blue">
					<th colspan="3">TaliMar Balance Sheet</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<div class="row rc_class">
	
	<div class="col-md-4" id="avaliable_capital">

	</div>

	<div class="col-md-4" id="capital_requirement">
	
	</div>

	<div class="col-md-4" id="balance_sheet_loan">

	</div>

</div>

<div class="row rc_class">

	<div class="col-md-4" id="OutstandingClosingFees">

	</div>

	<div class="col-md-4" id="OutstandingFunds">
		<!------ append OutstandingFunds table here ------->
	</div>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/dashboard/dashboard-new.css"); ?>">
<script type="text/javascript" src="<?php echo base_url("assets/js/dashboard/common_dashboard.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/dashboard/talimar_balance_sheet.js"); ?>"></script>