<div class="page-title">
	<h1 style="margin-left:14px !important;">Marketing </h1>
</div>
<div class="row rc_class">
	<div class="col-md-12" id="">

		<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
			<thead >
				<tr class="foot_light_blue">
					<th colspan="3">Marketing</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<div class="row rc_class">
	<div class="col-md-4 scrolling-dashboard" id="MarketingNew">

	</div>
	<div class="col-md-4" id="GoogleReviewRequest">

	</div>
	<div class="col-md-4 scrolling-dashboard" id="ClosingTombstone">

	</div>
</div>
<div class="row rc_class">
	
	<div class="col-md-4 scrolling-dashboard" id="ClosingAdvertisement">

	</div>
	<div class="col-md-4" id="SignNotPosted">

	</div>

	<div class="col-md-4" id="LoanClosingPosted">

	</div>
	
</div>
<div class="row rc_class">
	
	<div class="col-md-4" id="SocialMediaBlast">

	</div>
	<div class="col-md-4 scrolling-dashboard" id="beforeandafter">

	</div>
	<div class="col-md-4" id="SwagRequest">

	</div>
</div>


<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/dashboard/dashboard-new.css"); ?>">
<script type="text/javascript" src="<?php echo base_url("assets/js/dashboard/common_dashboard.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/dashboard/marketing.js"); ?>"></script>