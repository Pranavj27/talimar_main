<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="3">Extension Processing</th>
		</tr>
		<tr>

			<th>Address</th>
			<th>Maturity Date</th>
			<th>Days</th>
		</tr>
	</thead>
	<tbody>
	<?php if(isset($fetch_ext_data) && is_array($fetch_ext_data)){ 
			foreach($fetch_ext_data as $row){ 

				$date = date('Y-m-d');
				if($date > $row->new_maturity_date){
					$addstyle = 'style="color:red"';
				}else{
					$addstyle = '';
				}

				$myDateTime = DateTime::createFromFormat('Y-m-d', $row->new_maturity_date);
				$newDateString = $myDateTime->format('d-m-Y');
				$now = time(); // or your date as well
				$your_date = strtotime($newDateString);
				$datediff = $your_date - $now;
				$duration = floor($datediff / (60 * 60 * 24));

				?>
				<tr>
					<td><a href="<?php echo base_url();?>load_data/<?php echo $row->loan_id;?>"><?php echo $row->property_address;?></a></td>
					<td <?php echo $addstyle; ?>><?php echo date('m-d-Y', strtotime($row->new_maturity_date));?></td>
					<td><a href="<?php echo base_url();?>load_data/<?php echo $row->loan_id;?>#marketing_extention"><?php echo $duration;?></a></td>
				</tr>
			<?php } }else{ ?>
				<tr>
					<td colspan="3"> No data found!</td>
				</tr>
			<?php } ?>
	</tbody>
</table>