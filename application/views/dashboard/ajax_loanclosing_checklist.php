<?php
$underwriting = 0;
$investor = 0;
$clouser = 0;
$serviving = 0;
if(isset($checklistArray) && is_array($checklistArray)){
	foreach($checklistArray as $row){
		
		if($row['underwriting'] == 'No'){
			
			$underwriting++;
		}
		
		if($row['investor'] == 'No'){
			
			$investor++;
		}
		
		if($row['clouser'] == 'No'){
			
			$clouser++;
		}
		
		if($row['serviving'] == 'No'){
			
			$serviving++;
		}
	}
}


?>

<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="2">Loan Closing Checklist</th>
		</tr>
		<tr>
			<th>Item</th>
			<th>Count</th>
		</tr>

	</thead>
	<tbody>	
		<tr>
			<td>Underwriting</td>
			<td><a href="<?php echo base_url();?>Reports/LoanClosingChecklist"><?php echo $underwriting; ?></a></td>
			
		</tr>
		<tr>
			<td>Investor Relations</td>
			<td><a href="<?php echo base_url();?>Reports/LoanClosingChecklist"><?php echo $investor; ?></a></td>
			
		</tr>
		<tr>
			<td>Post Loan Closing</td>
			<td><a href="<?php echo base_url();?>Reports/LoanClosingChecklist"><?php echo $clouser; ?></a></td>
			
		</tr>
		<tr>
			<td>Loan Servicing</td>
			<td><a href="<?php echo base_url();?>Reports/LoanClosingChecklist"><?php echo $serviving; ?></a></td>
			
		</tr>
	</tbody>
</table>