<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead>
								<tr class="thead_dark_blue">
									<th colspan="2">Construction Loans</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td># of Construction Loans:</td>
									<td><?php echo $count_construction_loans; ?></td>
								</tr>
								<tr>
									<td>$ of Construction Loans:</td>
									<td>$<?php echo number_format($construction_total_loan_amount); ?></td>
								</tr>
								<tr>
									<td>Total Reserve Amount:</td>
									<td>$<?php  echo number_format($budget);?></td>
								</tr>

								<tr>
									<td>Total Reserve Disbursed:</td>
									<td>$<?php echo number_format($total_draws_amount); ?></td>
								</tr>
								<tr>
									<td>Total Reserve Remaining:</td>
									<td>$<?php echo number_format($budget - $total_draws_amount);?></td>
								</tr>
							</tbody>
						</table>