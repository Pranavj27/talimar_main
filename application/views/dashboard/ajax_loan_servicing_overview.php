<table class="table table-bordered table-striped table-condensed flip-content td_text_align_center">	
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="6">Loan Servicing Overview </th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Outstanding Lender Fees </td><td><?php echo $loanServicingOverview['processLender'];?></td>
			<td>Unverified Reserve Application </td><td><?php echo $loanServicingOverview['confirmApplied'];?></td>
			<td>Payoff Demands in Review  </td><td><?php echo $loanServicingOverview['totalPayOffCondition'];?></td>
		</tr>
		<tr>
			<td>Borrower Welcome Calls </td><td><?php echo $loanServicingOverview['introductionBorrower'];?></td>
			<td>Late Payments >10 Days </td><td><?php echo $loanServicingOverview['totalLatePayments'];?></td>
			<td>Draw Requests in Review</td><td><?php echo $loanServicingOverview['totalRequestsReview'];?></td>
		</tr>
		<tr>
			<td>Loans Not Submitted </td><td><?php echo $loanServicingOverview['submitLoan'];?></td>
			<td>Loans in Default </td><td><?php echo $loanServicingOverview['totalLoanCondition'];?></td>
			<td>Unreceived Loan Documents </td><td><?php echo $loanServicingOverview['obtainOriginal'];?></td>
		</tr>
		<tr>
			<td>Outstanding Servicer Deposits </td><td><?php echo $loanServicingOverview['confirmServicer'];?></td>
			<td>Lender Extension Approvals  </td><td><?php echo $loanServicingOverview['totalLenderApproval'];?></td>
			<td></td><td></td>
		</tr>
		<tr>
			<td>Initial Review Incomplete </td><td><?php echo $loanServicingOverview['initialComplete'];?></td>
			<td>Loan Extension Processing </td><td><?php echo $loanServicingOverview['totalExtensionStatus'];?></td>
			<td></td><td></td>
		</tr>
	</tbody>
</table>