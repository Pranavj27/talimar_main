<?php
	$assi_servicer_status		= $this->config->item('assi_servicer_status');
    $assi_fund_received			= $this->config->item('assi_fund_received');
    $Ass_Recording_Status		= $this->config->item('Ass_Recording_Status');
    $assi_disclouser_status		= $this->config->item('assi_disclouser_status');

?>
<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="7">Assignments in Process</th>
		</tr>
		<tr>
			<th>Lender Name</th>
			<th>Street Address</th>
			<th>Investment</th>
			<th>Funds Status</th>
			<th>Disclosures</th>
			<th>Assignment</th>
			<th>Boarding Status</th>
		</tr>
	</thead>
	<tbody>
		<?php if(isset($AssignmentsProData) && is_array($AssignmentsProData)){ 
				foreach($AssignmentsProData as $row) { 

					if($row['recording_status'] == 3){
						$recording_status = 'Recorded';
					}elseif($row['recording_status'] == 1){
						$recording_status = 'Not Submitted';
					}else{
						$recording_status = $Ass_Recording_Status[$row['recording_status']];
					}
				?>
					<tr>
						<td>
							<a href="<?php echo base_url();?>investor_view/<?php echo $row['lender_name'];?>"><?php echo $investorsName[$row['lender_name']];?></a>
						</td>
						<td>
							<a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['fulladd'];?></a>
						</td>
						<td>
							<a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>#assigment"><?php echo '$'.number_format($row['investment'],2);?></a></td>
						<td><?php echo $assi_fund_received[$row['nfunds_received']];?></td>
						<td><?php echo $row['ndisclouser_status']; ?></td>
						<td><?php echo $recording_status;?></td>
						<td><?php echo $assi_servicer_status[$row['nservicer_status']];?></td>
					</tr>
				<?php } }else{ ?>
					<tr>
						<td colspan="7">No data found!</td>
					</tr>
				<?php } ?>
	</tbody>
</table>
