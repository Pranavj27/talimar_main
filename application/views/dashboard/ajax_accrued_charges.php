<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
	
	<thead>
		<tr class="thead_dark_blue">
			<th colspan="2">Accrued Charges</th>
		</tr>
		<tr>
			<th>Street Address</th>
			<th>Total Accrued Charges</th>
		</tr>

	</thead>
	<tbody>	
		<?php

		if(isset($accruedchargedata) && is_array($accruedchargedata)){
		 foreach($accruedchargedata as $row){ ?>
		
		<tr>
			<td>
				<a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['property_address'];?></a></td>
			<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>#accured_charges">$<?php echo number_format($row['remaningamount'],2);?></a>
			</td>
		</tr>
	<?php } } ?>
	</tbody>
</table>