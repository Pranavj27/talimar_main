<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
						<thead>
							<tr class="thead_dark_blue">
								<th colspan="3">Loans in Boarding</th>
							</tr>
							<tr>
								<th>Item</th>
								<th>Count</th>
								<th>Balance</th>
							</tr>
						</thead>
						<tbody>
								<?php
								$totalCount=$count_loan_not_submited_fetch+$count_Boarding+$Boarding_count+$final_boardingsss_count;
								$totalSum=$sum_loan_not_submited_fetch+$sum_loan_Boarding+$Boarding_total+$final_boardingsss_total;
								?>
								<tr>
									<td style = "text-align: left;"><a href="<?php echo base_url().'reports/loanBoardingReport';?>">Not Submitted</a></td>
									<td style = "text-align: left;"><?php echo $count_loan_not_submited_fetch; ?></td>
									<td style = "text-align: left;"><?php echo '$'. number_format($sum_loan_not_submited_fetch); ?></td>
								</tr>

								<tr>
									<td style = "text-align: left;"><a href="<?php echo base_url().'reports/loanBoardingReport';?>">Servicer Processing</a></td>
									<td style = "text-align: left;"><?php echo $count_Boarding; ?></td>
									<td style = "text-align: left;"><?php echo '$'. number_format($sum_loan_Boarding); ?></td>
								</tr>
								<tr>
									<td style = "text-align: left;"><a href="<?php echo base_url().'reports/loanBoardingReport';?>">Servicer Finalizing</a></td>
									<td style = "text-align: left;"><?php echo $Boarding_count; ?></td>
									<td style = "text-align: left;"><?php echo '$'. number_format($Boarding_total); ?></td>
								</tr>
								<tr>
									<td style = "text-align: left;"><a href="<?php echo base_url().'reports/loanBoardingReport';?>">TaliMar Final Review</a></td>
									<td style = "text-align: left;"><?php echo $final_boardingsss_count; ?></td>
									<td style = "text-align: left;"><?php echo '$'. number_format($final_boardingsss_total); ?></td>
								</tr>
								

								
								<tr>
									<td style = "text-align: left;"><b>Total</a></td>
									<td style = "text-align: left;"><b><?php echo $totalCount; ?></b></td>
									<td style = "text-align: left;"><b><?php echo '$'. number_format($totalSum); ?></b></td>
								</tr>
							</tbody>
					</table>