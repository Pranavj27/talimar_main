<style>
div.error_data h2 {
    text-align: center;
    font-size: 135px;
    padding: 142px 0px 0px 0px;
	color:#003468;
}
.error_data small {
    font-size: 30px;
}
</style>
<div class="main_error_page_div">
	<div class="error_data">
		<h2>404<br><small>Page Not Found</small></h2>
	</div>
</div>