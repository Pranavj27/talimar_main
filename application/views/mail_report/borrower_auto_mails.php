<div class="page-content-wrapper">
	<div class="page-content responsive">
		
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<div class="col-md-12"><h1>Construction Reminder: <span class="sub_title_mail_auto">Upcoming Emails</span></h1></div>	
			</div>
		</div>

		<div class="rc_class">
			<?php if ($this->session->flashdata('error') != '') {?>
				<div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error'); ?></div><?php }?>
			<?php if ($this->session->flashdata('success') != '') {?>
				<div id='success'><i class='fa fa-thumbs-o-up'></i> <?php echo $this->session->flashdata('success'); ?></div><?php }?>
			<div id="chkbox_err"></div>	

			<div class="main">

				<table id="borrower_auto_mails" class="table table-bordered table-striped table-condensed flip-content">
					<thead class="flip-content">
						<tr>
							<th>Last Name</th>
							<th>First Name</th>							
							<th>Email</th>
							<th>Street Address</th>
							<!-- <th>FCI Loan Id</th> -->
							<th>Talimar Loan Id</th>							
							<th>Address</th>
							<th>Sent Date</th>
						</tr>
					</thead>
				<tbody>
					
				</tbody>
			</table>
				
			</div>
		</div>
	</div>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/auto_mail/auto_mail.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('assets/js/auto_mail/auto_mail.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#borrower_auto_mails').DataTable();
		$('#borrower_auto_mails').DataTable().clear().destroy();
	   	$('#borrower_auto_mails').DataTable({
	      	'processing': true,
	      	'serverSide': true,
	      	'pageLength': 10,
	      	'lengthChange': false,
	      	'oLanguage': {sProcessing: '<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>'},
	      	'serverMethod': 'post',
	      	"columnDefs": [
			    { "width": "30%", "targets": 0 }
			  ],
	      	'ajax': {
	          'url':'<?php echo base_url(); ?>Auto_mail/ajaxBorrower',
	      	},
	      	'columns': [
	         	{ data: 'first_name'},
		        { data: 'last_name'},
		        { data: 'email'},
		        { data: 'property_address'},
		        { data: 'talimar_loan_no' },				        
		        { data: 'address'},
		        { data: 'mail_send_date'}
	      	]
	   	});
	});
</script>
