<style type="text/css">
	.dataTables_wrapper{
		margin-top: 32px;
}
</style>
<div class="page-content-wrapper">
	<div class="page-content responsive">		
		<div class="page-head">
			<div class="page-title">
				<div class="col-md-12"><h1>Notifications</h1></div>		
			</div>
		</div>
		<div class="rc_class">
			<div class="col-md-2">
				<label>Upcoming Payment</label>
				<select name="upcoming_payment" id="upcoming_payment" class="form-control" onchange="selectone('upcoming_payment',this.value)">
					<option value="all" selected="selected">Select All</option>
					<option value="yes">Yes</option>
					<option value="no" >No</option>
				</select>
			</div>
			<div class="col-md-2">
				<label>Late Payment</label>
				<select name="late_payment" id="late_payment" class="form-control" onchange="selectone('late_payment',this.value)">
					<option value="all" selected="selected">Select All</option>
					<option value="yes">Yes</option>
					<option value="no" >No</option>
				</select>
			</div>
			<div class="col-md-2">
				<label>Property Insurance</label>
				<select name="property_insurance" id="property_insurance" class="form-control" onchange="selectone('property_insurance',this.value)">
					<option value="all" selected="selected">Select All</option>
					<option value="yes">Yes</option>
					<option value="no" >No</option>
				</select>
			</div>
			<div class="col-md-2">
				<label>Last</label>
				<select name="last_days" id="last_days" class="form-control" onchange="init_datatable_for_contact()">
					<option value="30" selected="selected">30 Days</option>
					<option value="60">60 days</option>
					<option value="90">90 Days</option>
					<option value="180">180 Days</option>
					<option value="360">360 Days</option>
					<option value="all" >All Time</option>
				</select>
			</div>
		</div>
	</br>
		<div class="rc_class">
				<div id="chkbox_err"></div>
				<div class="main">
					<table id="tracking_notification_tbl" class="table table-bordered table-striped table-condensed flip-content">
						<thead class="flip-content">
							<tr>
								<th>First Name</th>												
								<th>Last Name</th>	
								<th>E-Mail</th>	
								<th>Notification</th>	
								<th>Property Address</th>					
								<th>Date Sent</th>
								<th>Time Sent</th>
								<th>Status</th>
								<th>View</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>				
				</div>
		</div>
	</div>
</div>
<div class="modal fade" id="mailviewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
	      	<div class="modal-header">
	        	<h3 class="modal-title" id="exampleModalLabel">Mail View</h3>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
	      	</div>
	      	<div class="modal-body">
	      		<div class="mail_popup_body"></div>
	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      	</div>
    	</div>
  	</div>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/auto_mail/auto_mail.css'); ?>">
<div id="demo">
	<script type="text/javascript" src="<?php echo base_url('assets/js/auto_mail/auto_mail.js'); ?>"></script>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".navbar-collapse").hide();
		init_datatable_for_contact();
	});
	function selectone(idName,idValue){
		$("#upcoming_payment").val("no");
		$("#property_insurance").val("no");
		$("#late_payment").val("no");
		$("#"+idName).val(idValue);
		init_datatable_for_contact();

	}
	function init_datatable_for_contact(){
		var totalRecord=0;	
		var dispatching_list_all = '#tracking_notification_tbl';
		$(dispatching_list_all).DataTable().clear().destroy();  
		if($( dispatching_list_all ).length){		
			$dis_list_all = $( dispatching_list_all ).DataTable({
				processing: true,
				serverSide: true,
				pageLength: 10,
				bFilter: true,
			    ajax: {
					dataSrc:"data",
					data:function(data){

						var search_text         	= $('#tracking_notification_tbl input[type="search"]').val();
						var upcoming_payment        = $('#upcoming_payment').val();
						var late_payment         	= $('#late_payment').val();
						var property_insurance      = $('#property_insurance').val();
						var last_days 				= $("#last_days").val();
						data.search_text 			= search_text;
						data.upcoming_payment 		= upcoming_payment;
						data.property_insurance 	= property_insurance;
						data.late_payment 			= late_payment;
						data.last_days 				= last_days;
						data.calling_type 			= 'table';
					},
					url: '/Pranav/Talimar/Auto_mail/ajaxTrackingNotification',
					dataFilter: function(data){
						var json = JSON.parse( data );	
					    json.recordsTotal = json.totalNumRows;
					    json.recordsFiltered = json.totalNumRows;
					    totalRecord=json.totalNumRows;
					    json.data = json.data;				    
					    return JSON.stringify( json );
					}
			    },
			    columns: [				        
			        { data: 'firstname' },
					{ data: 'lastname' },
					{ data: 'email' },
					{ data: 'notification' },
					{ data: 'address' },
					{ data: 'sent_date' },
					{ data: 'sent_time' },
					{ data: 'status' },
					{ data: 'action' }
			    ],
			    "aoColumnDefs": [
			        { "bSortable": false, "aTargets": [8] }, //, 6, 7
			        { "bSearchable": false, "aTargets": [0, 1, 2, 3, 4, 6 ] }
			    ],
			    order: [[ 0, "ASC" ]],
			    "language": { loadingRecords: '&nbsp;&nbsp;', processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
			    drawCallback: function( settings ) {			    		    	
			    },
			    initComplete: function() {
			        $('#past_due_tracking input').unbind();
			        $('#past_due_tracking input').bind('blur change', function(e) {
			            $dis_list_all.search(this.value).draw();
			        });
			    }
			});
		}
	}
</script>