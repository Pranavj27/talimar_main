<div class="page-content-wrapper">
	<div class="page-content responsive">
		
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<div class="col-md-12"><h1>Payment Reminder: <span class="sub_title_mail_auto">Upcoming Emails </span><small>(Before payment of 5 day's)</small></h1></div>	
			</div>
		</div>

		<div class="rc_class">
			<?php if ($this->session->flashdata('error') != '') {?>
				<div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error'); ?></div><?php }?>
			<?php if ($this->session->flashdata('success') != '') {?>
				<div id='success'><i class='fa fa-thumbs-o-up'></i> <?php echo $this->session->flashdata('success'); ?></div><?php }?>
			<div id="chkbox_err"></div>	

			<div class="main">

				<table id="loan_payment_reminder" class="table table-bordered table-striped table-condensed flip-content">
					<thead class="flip-content">
						<tr>
							<th>Borrower Name</th>
							<th>Email</th>
							<!-- <th>FCI Loan Id</th> -->
							<th>Street Address</th>
							<th>TaliMar Loan</th>
							<th>Payment Amount</th>
							<th>Next Payment Date</th>
							<th>Send Date</th>
						</tr>
					</thead>
				<tbody id="">
					<?php
						$CI =& get_instance();
						$CI->load->helper('mailhtml_helper.php'); 
						if($loan_payment_reminder)
						{
							foreach($loan_payment_reminder as $key => $loan_details)
							{				
									$Loan_Servicer = '';
									$Loan_ServicerAddress = array();
									$talimar_loan_id = $loan_details->talimar_loan;
									$loan_amount = $loan_details->totalPaymentFCI;
									$nextPayment = $loan_details->nextDueDate;
									$befordate = date('Y-m-d', strtotime('-7 days', strtotime($nextPayment)));
									$befordate = explode('-', $befordate);
									$befordate = $befordate[1].'-'.$befordate[2].'-'.$befordate[0];

									$firstbefordate = date('Y-m-d', strtotime('-7 days', strtotime($loan_details->first_payment_date)));

									$month = date('m');

									if($month == date('m', strtotime($firstbefordate)))
									{
										$firstbefordate = explode('-', $firstbefordate);
										$befordate = $firstbefordate[1].'-'.$firstbefordate[2].'-'.$firstbefordate[0];
									}

									$loan_account_no = $loan_details->fci;
									$todayDate = date("m-d-Y");

									$nextDate = explode('-', $nextPayment);

									if($loan_amount){

										$url = base_url("load_data/".$loan_details->id);

										echo '<tr>
													<td>'.$loan_details->b_name.'</td>
													<td>'.$loan_details->c_email.'</td>
													<td><a href="'.$url.'">'.$CI->get_loadn_property_address($talimar_loan_id).'</a></td>
													<td>'.$talimar_loan_id.'</td>
													<td>$'.number_format($loan_amount, 2).'</td>
													<td>'.$nextDate[1].'-'.$nextDate[2].'-'.$nextDate[0].'</td>
													<td>'.$befordate.'</td>
												</tr>';

								

									}
								
							}
						}

					?>
					
				</tbody>
			</table>
				
			</div>
		</div>
	</div>
</div>

		
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/auto_mail/auto_mail.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('assets/js/auto_mail/auto_mail.js'); ?>"></script>
<script type="text/javascript">
	
$(document).ready(function(){
	
	   	$('#loan_payment_reminder').DataTable(); 
	});

</script>