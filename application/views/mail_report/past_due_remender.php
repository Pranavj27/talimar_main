<div class="page-content-wrapper">
	<div class="page-content responsive">		
		<div class="page-head">
			<div class="page-title">
				<div class="col-md-12"><h1>Past Due Remender: <span class="sub_title_mail_auto">Upcoming Email(After payment of 3,6,9 day's)</span></h1></div>	
			</div>
		</div>
		<div class="rc_class">
			<?php 
			if ($this->session->flashdata('error') != '') {?>
				<div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error'); ?></div><?php }?>
			<?php if ($this->session->flashdata('success') != '') {?>
				<div id='success'><i class='fa fa-thumbs-o-up'></i> <?php echo $this->session->flashdata('success'); ?></div>
			<?php }?>
				<div id="chkbox_err"></div>
				<div class="main">
					<table id="past_due_notification" class="table table-bordered table-striped table-condensed flip-content">
						<thead class="flip-content">
							<tr>
								<th>Borrower Name</th>												
								<th>Email</th>	
								<th>Street Address</th>	
								<th>Talimar Loan</th>	
								<th>Payment Amount</th>					
								<th>Due Payment Date</th>
								<th>Send Date</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>				
				</div>
		</div>
	</div>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/auto_mail/auto_mail.css'); ?>">
<div id="demo">
	<script type="text/javascript" src="<?php echo base_url('assets/js/auto_mail/auto_mail.js'); ?>"></script>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		init_datatable_for_contact();
	});
	function init_datatable_for_contact(){
		var totalRecord=0;	
		var dispatching_list_all = '#past_due_notification';
		$(dispatching_list_all).DataTable().clear().destroy();  
		if($( dispatching_list_all ).length){		
			$dis_list_all = $( dispatching_list_all ).DataTable({
				processing: true,
				serverSide: true,
				pageLength: 10,
				bFilter: true,
			    ajax: {
					dataSrc:"data",
					data:function(data){
						var search_text         	= $('#past_due_notification input[type="search"]').val();
						data.search_text 			= search_text;
						data.calling_type 			= 'table';
					},
					url: '/Auto_mail/ajaxPastDueList',
					dataFilter: function(data){
						var json = JSON.parse( data );	
					    json.recordsTotal = json.totalNumRows;
					    json.recordsFiltered = json.totalNumRows;
					    totalRecord=json.totalNumRows;
					    json.data = json.data;				    
					    return JSON.stringify( json );
					}
			    },
			    columns: [				        
			        { data: 'borrower_name' },
					{ data: 'email' },
					{ data: 'street_address' },
					{ data: 'talimar_loan' },
					{ data: 'payment_amount' },
					{ data: 'due_payment_date' },
					{ data: 'send_date' }
			    ],
			    "aoColumnDefs": [
			        { "bSortable": false, "aTargets": [] }, //, 6, 7
			        { "bSearchable": false, "aTargets": [0, 1, 2, 3, 4, 6 ] }
			    ],
			    order: [[ 0, "ASC" ]],
			    "language": { loadingRecords: '&nbsp;&nbsp;', processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
			    drawCallback: function( settings ) {			    		    	
			    },
			    initComplete: function() {
			        $('#past_due_notification input').unbind();
			        $('#past_due_notification input').bind('blur change', function(e) {
			            $dis_list_all.search(this.value).draw();
			        });
			    }
			});
		}
	}
</script>