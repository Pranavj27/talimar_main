<div class="page-content-wrapper">
	<div class="page-content responsive">
		
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<div class="col-md-12"><h1>Property Insurance: <span class="sub_title_mail_auto">Sent Emails</span></h1></div>	
			</div>
		</div>

		<div class="rc_class">
			<?php if ($this->session->flashdata('error') != '') {?>
				<div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error'); ?></div><?php }?>
			<?php if ($this->session->flashdata('success') != '') {?>
				<div id='success'><i class='fa fa-thumbs-o-up'></i> <?php echo $this->session->flashdata('success'); ?></div>
			<?php }?>
			<div id="chkbox_err"></div>	

			<div class="main">

				<table id="ltrigger_auto_mails" class="table table-bordered table-striped table-condensed flip-content">
					<thead class="flip-content">
						<tr>
							<th>Borrower Name</th>												
							<th>Email</th>	
							<th>Talimar Id</th>	
							<th>Street Address</th>								
							<th>Status</th>
							<th>Sent Date</th>
							<th width="100">Action</th>
						</tr>
					</thead>
				<tbody>

				</tbody>
			</table>
				
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="mailviewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Mail View</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        	
      	<div class="mail_popup_body">
      		
      	</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/auto_mail/auto_mail.css'); ?>">
<div id="demo">
<script type="text/javascript" src="<?php echo base_url('assets/js/auto_mail/auto_mail.js'); ?>"></script>
</div>
<script type="text/javascript">

	$(document).ready(function(){
		$('#ltrigger_auto_mails').DataTable();
		$('#ltrigger_auto_mails').DataTable().clear().destroy();
	   	$('#ltrigger_auto_mails').DataTable({
	      	'processing': true,
	      	'serverSide': true,
	      	'pageLength': 10,
	      	'lengthChange': false,
	      	'oLanguage': {sProcessing: '<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>'},
	      	'serverMethod': 'post',
	      	"columnDefs": [
			    { "width": "30%", "targets": 0 }
			  ],
	      	'ajax': {
	          'url':'<?php echo base_url(); ?>Auto_mail/ajaxInsuranceExpiringTrack',
	      	},
	      	'columns': [
	         	{ data: 'BorrowerName'},
		        { data: 'Email'},
		        { data: 'TalimarLoanId'},
		        { data: 'StreetAddress'},
		        { data: 'sandgrid_status' },				        
		        { data: 'created_at'},
		        { data: 'action'}
	      	],


	   	});
	   	// setTimeout(function(){ demo() }, 3000);


	   	
	});
</script>