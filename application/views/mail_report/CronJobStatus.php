<?php $conName = $this->config->item('conName'); ?>
<div class="page-content-wrapper">
	<div class="page-content responsive" style="width: 100%!Important;">
		
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<div class="col-md-12"><h1>API Connection<span class="sub_title_mail_auto"></span></h1></div>	
			</div>

		</div>

		<div class="rc_class">
			<?php if ($this->session->flashdata('error') != '') {?>
				<div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error'); ?></div><?php }?>
			<?php if ($this->session->flashdata('success') != '') {?>
				<div id='success'><i class='fa fa-thumbs-o-up'></i> <?php echo $this->session->flashdata('success'); ?></div><?php }?>
			<div id="chkbox_err"></div>	
			<div class="row">
			<div class="talimar_no_dropdowns" style="float:left;">
					
					<div class="float-direction-left">
						<br>
						<!-- <input type="date" name="createdAt" class="form-control" id="createdAt"> -->
						<input tabindex="5" class="form-control datepicker" name="createdAt" id="createdAt" placeholder="MM-DD-YYYY" value="" autocomplete="off">
					</div>
			</div>
		</div>
			<div class="main row" id="TableData">

			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script type="text/javascript">

$(document).ready(function(){

	$('#createdAt').datepicker({
    	dateFormat: 'mm-dd-yy'
	});

	


	$('#createdAt').on('change', function(){
		getLenderData();
	});

});


getLenderData();

function getLenderData()
{
	$('#TableData').html('');
	var html = '';

	html += '<table id="borrower_auto_mails" class="table table-bordered table-striped table-condensed flip-content">';
	html += '<thead class="flip-content">';
	html += '<tr>';
	html += '<th>API Connection Name</th>';
	html += '<th>Date</th>';							
	html += '<th>Time</th>';
	html += '<th>Status</th>';
	html += '</tr>';
	html += '</thead>';
	html += '<tbody>';
	html += '</tbody>';
	html += '</table>';

	$('#TableData').html(html);

	var dispatching_list_all = '#borrower_auto_mails';
	$(dispatching_list_all).DataTable();

	$('#borrower_auto_mails').DataTable().clear().destroy();

	if ( $.fn.DataTable.isDataTable('#borrower_auto_mails') ) {
  			$('#borrower_auto_mails').DataTable().destroy();
	}
		
	if( $( dispatching_list_all ).length ){	

		
		var conName = $('#conName').val();
		var createdAt = $('#createdAt').val();
		
		$dis_list_all = $( dispatching_list_all ).DataTable({
		processing: true,
		searching : false,
		serverSide: true,
		pageLength: 10,
		order: [[ 1, "desc" ]],
		ajax: {

				type : 'POST',
		    	dataSrc:"data",
				data:{'conName' : conName, 'createdAt' : createdAt, 'orderby' : 'Desc'},
		    	url: '<?php echo base_url()."CronJobFCI/ajaxGetCron";?>',
			    dataFilter: function(data){
			    	
					var json = JSON.parse( data );
					total = json.total;
					json.recordsTotal = json.total;
					json.recordsFiltered = json.total;
					json.data = json.data;
					return JSON.stringify( json );
				},
			},
			columns: [				        
		        { data: 'name' },
		        { data: 'dateData' },
		        { data: 'timeData' },
		        { data: 'status'}
		    ],
			"aoColumnDefs": [
		        { "bSortable": false, "aTargets": [0,2,3 ] }, 
		        { "bSearchable": false, "aTargets": [0,1,2,3] }
		    ],
			"language": { loadingRecords: '&nbsp;&nbsp;', processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
			        
			drawCallback: function( settings ) {
			        
			}
		});
	}
	

}

</script>
