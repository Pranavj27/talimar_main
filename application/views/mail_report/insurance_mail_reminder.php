<style type="text/css">
	.table-scrollable > .table-bordered > thead > tr:last-child > th{
		width: 155px !important;
	}
</style>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<div class="col-md-12"><h1>Property Insurance: <span class="sub_title_mail_auto">Upcoming Emails </span><small>(Before payment of 30 day's)</small></h1></div>	
			</div>
		</div>

		<div class="rc_class">
			<?php if ($this->session->flashdata('error') != '') {?>
				<div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error'); ?></div><?php }?>
			<?php if ($this->session->flashdata('success') != '') {?>
				<div id='success'><i class='fa fa-thumbs-o-up'></i> <?php echo $this->session->flashdata('success'); ?></div><?php }?>
			<div id="chkbox_err"></div>	

			<div class="main">

				<table id="maturity_mail_reminder" class="table table-bordered table-striped table-condensed flip-content">
					<thead class="flip-content">
						<tr>
							<th>Borrower Name</th>
							<th>Email</th>
							<!-- <th>FCI Loan Id</th> -->
							<th>Street Address</th>
							<th>TaliMar Loan</th>
							<!-- <th>Payment Amount</th> -->
							<th>Insurance Date</th>
							<th>Send Date</th>
							<th>Type</th>
						</tr>
					</thead>
				<tbody>

					
				</tbody>
			</table>
				
			</div>
		</div>
	</div>
</div>

		
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/auto_mail/auto_mail.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('assets/js/auto_mail/auto_mail.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#maturity_mail_reminder').DataTable();
		$('#maturity_mail_reminder').DataTable().clear().destroy();
	   	$('#maturity_mail_reminder').DataTable({
	      	'processing': true,
	      	'serverSide': true,
	      	'pageLength': 10,
	      	'lengthChange': false,
	      	'oLanguage': {sProcessing: '<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>'},
	      	'serverMethod': 'post',
	      	"columnDefs": [
			    { "width": "30%", "targets": 0 }
			  ],
	      	'ajax': {
	          'url':'<?php echo base_url(); ?>Auto_mail/ajaxInsuranceMail',
	      	},
	      	'columns': [
	         	{ data: 'BorrowerName'},
		        { data: 'Email'},
		        { data: 'StreetAddress'},
		        { data: 'TalimarLoanId'},
		        // { data: 'LoanAmount' },				        
		        { data: 'insurance_date'},
		        { data: 'SentDate'},
		        { data: 'type'}
	      	]
	   	});
	});
</script>