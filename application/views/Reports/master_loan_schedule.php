<?php
error_reporting(0);
// echo "<pre>";
// print_r($fetch_active_data);
// echo "</pre>";
$usa_city_county 				= $this->config->item('usa_city_county'); 
		
$loan_type_option 				= $this->config->item('loan_type_option');
$serviceing_sub_agent_option 	= $this->config->item('serviceing_sub_agent_option');
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Master Loan Schedule  </h1>	
					
				</div>
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>download_master_loan">
			
				<input type="hidden" name="select_box" value="<?php echo isset($master_box) ? $master_box : ''; ?>">
			
			        <button  type="submit" class="btn blue">PDF</button>
					</form>
					
				</div>
				</div>
					<div class="row">
				
				<form id="select_master" method="POST" action="<?php echo base_url();?>master_loan_schedule">
			
					
			<div class="col-md-3">
				Loan Status :  &nbsp;
				<!--<select name="investor_id[]" id ="investor_id" class="selectpicker" data-live-search="true"  multiple>-->
				<select name="master_box" id ="master_box" class="form-control"  onchange = "select_master_report(this.value)">
					<option value=""  >Select All</option>
					<option value="1" <?php if(isset($master_box)){ if($master_box == 1){ echo 'selected'; } } ?> >Pipeline</option>
					<option value="2" <?php if(isset($master_box)){ if($master_box == 2){ echo 'selected'; } } ?> >Active</option>
					<option value="3" <?php if(isset($master_box)){ if($master_box == 3){ echo 'selected'; } } ?> >Paid Off</option>
									
				</select>
			</div>
					</form>		
		</div>
		
		<div class="rc_class">
		
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Borrower Name</th>
					
					<th>Servicer #</th>
					<th>Loan Sub Servicer</th>
					<th>Loan<br>Amount</th>
					<th>Interest<br>Rate</th>
				
					<th>LTV</th>
					<th>Term</th>
				
					<th>Address</th>
					<th>Unit #</th>
					<th>State</th>
					<th>City</th>
					<th>Zip</th>
					
				
					</tr>
				</thead>
				<tbody class="scrollable">
				<?php
				$key 				= 0;				
				$total_loan_amount 	= 0;				
				$total_ltv 			= 0;				
				$total_intrest 		= 0;				
				$total_term 		= 0;
				
					foreach($fetch_active_data['talimar_loan'] as $row )
					{
					
						$loan_amount 	= $fetch_active_data['loan_amount'][$key];
						$arv 			= $fetch_active_data['property_arv'][$key];
						$underwriting_value = $fetch_active_data['underwriting_value'][$key];
						$total_ecum			 = $fetch_active_data['ecum_total'][$key];
						
						$position			 = $fetch_active_data['position'][$key];
						if($position == 1){
							
							$ltv = ($loan_amount/$underwriting_value) * 100;
						
						}else{					
							
							$ltv = ($loan_amount + $total_ecum)/$underwriting_value * 100;	
						}
						
						$total_loan_amount = $total_loan_amount + $fetch_active_data['loan_amount'][$key];						
						
							if(is_infinite($ltv)){
								
								$ltv=0.00;
							}else{
								$ltv=$ltv;
							}
						$total_ltv = $total_ltv + $ltv;	

						
						$total_intrest = $total_intrest + $fetch_active_data['interest_rate'][$key];
						$total_term = $total_term + $fetch_active_data['term_month'][$key];
						?>
						<tr>
						
							<td><?php echo $fetch_active_data['borrower_name'][$key]; ?></td>
							
						
							<td><?php echo $fetch_active_data['fci'][$key]; ?></td>
							<td><?php echo $fetch_active_data['loan_sub_servicer'][$key]; ?></td>
							<td>$<?php echo number_format($fetch_active_data['loan_amount'][$key]); ?></td>
							<td><?php echo number_format($fetch_active_data['interest_rate'][$key], 3); ?>%</td>
						
							<td>
							
								<?php 
								if(is_infinite($ltv))
								{
								$ltv='0.00';
								}else{
									
								$ltv=$ltv;	
								}
								echo number_format($ltv,2); ?>%
								<input type="hidden" name="ltv[<?php echo $fetch_active_data['loan_id'][$key]; ?>]" value="<?php echo $ltv; ?>">
							</td>
							
							<td><?php echo $fetch_active_data['term_month'][$key];?></td>
					
							<td><?php echo $fetch_active_data['property_address'][$key];?></td>
							<td><?php echo $fetch_active_data['unit'][$key];?></td>
							<td><?php echo $fetch_active_data['state'][$key];?></td>
							<td><?php echo $fetch_active_data['city'][$key];?></td>
							<td><?php echo $fetch_active_data['zip'][$key];?></td>
					
							
						</tr>
						<?php
					$key++;										
					}
				?>
				
				</tbody>				
				<tfoot>					
				<tr>						
					<th>Total</th>						
					<th><?php echo $key; ?> <b>Loans</b></th>						
					<th></th>						
					<th><?php echo '$'.number_format($total_loan_amount); ?></th>						
					<th colspan="8"></th>					
				</tr>	
				<tr>	
					<th>Average</th>						
					<th></th>						
					<th></th>						
					<th><?php echo '$'.number_format($total_loan_amount/$key); ?></th>						
					<th><?php echo number_format($total_intrest/$key,2).'%'; ?></th>						
					<th><?php echo number_format($total_ltv/$key,2).'%'; ?></th>						
					<th><?php echo number_format($total_term/$key); ?></th>						
					<th colspan="8"></th>					
				</tr>				
				</tfoot>
			</table>
			
		
		</div>
	</div>
	
	
	
	
</div>
<script>

function select_master_report(sel)
{
	
	document.getElementById('select_master').submit();
}

$(document).ready(function() {

    $('#table').DataTable();
	
} );
</script>

<style>
		.talimar_no_dropdowns {
		float: left;
		
	}
</style>