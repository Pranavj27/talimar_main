<?php

error_reporting(0);
$borrower_type_option 	= $this->config->item('borrower_type_option');
$loan_type_option 		= $this->config->item('loan_type_option');
$loan_status_option 	= $this->config->item('loan_status_option');


?>
<div class="page-content-wrapper">

	<div class="page-content responsive">

		<div class="page-head">

				<!-- BEGIN PAGE TITLE -->

				<div class="page-title">

					<h1> &nbsp; Master Borrower Schedule  </h1>	

					

				</div>

				<div class="top_download">
					<!--
					<a href="<?php echo base_url();?>master_borrower_excel">

					<button  class="btn blue">Excel Download</button>

					</a>
					-->
					<a href="<?php echo base_url();?>master_borrower_pdf/<?php echo $select_b_id; ?>">

					<button  class="btn blue">PDF</button>

					</a>
					

				</div>

		</div>
		
		

		
		
		<div class="row">
			<form id="select_borrower" method="POST" action="<?php echo base_url();?>master_borrower_schedule">

			<div class="talimar_no_dropdowns">

				Borrower :  &nbsp;

							<select name="borrower_id" onchange="select_borrower_report(this.value)" class="selectpicker" data-live-search="true" >

								<option value='all'>All</option>

								<?php

								foreach($fetch_all_borrwer as $row)

								{

									?>

									<option value="<?php echo $row->id;?>" <?php if(isset($select_b_id)){  if($select_b_id == $row->id){ echo 'selected'; } }?> ><?php echo $row->b_name;?></option>

									<?php

								}

								?>

							</select>

			</div>

			</form>
		</div>

		<div class="rc_class">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >

				<thead>

					<tr>
						<th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Borrower Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
						<th>Contact Name</th>
						<th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Phone&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
						<th>Email</th>
						<th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;City&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
						<th>State</th>
						<th># of<br>Current<br>Loans</th>
						<th>$ of<br>Current<br>Loans</th>
						<th># of<br>Paid Off<br>Loans</th>
						<th>$ of<br>Paid Off<br>Loans</th>
						
					</tr>

				</thead>

				<tbody>
				<?php
				if(isset($master_borrower_data))
				{
					foreach($master_borrower_data as $row)
					{
						?>
						<tr>
							<td><?php echo $row['borrower_name'];?></td>
							<td><?php echo $row['contact_name'];?></td>
							<td><?php echo $row['phone'];?></td>
							<td><?php echo $row['email'];?></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['count_active'];?></td>
							<td>$<?php echo number_format($row['active_loan_amount'],2);?></td>
							<td><?php echo $row['count_paidoff'];?></td>
							<td>$<?php echo number_format($row['paidoff_loan_amount'],2);?></td>
						</tr>
						<?php
					}
				}
				?>
				</tbody>

			</table>

			

		</div>

	</div>

	<!-- END CONTENT -->

</div>
<script>
$(document).ready(function() {
    $('#table').DataTable();
} );

function select_borrower_report(id)
{
	document.getElementById('select_borrower').submit();
}
</script>
