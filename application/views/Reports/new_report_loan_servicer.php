<?php

error_reporting(0);
$yes_no_option = $this->config->item('yes_no_option');
$yes_no_option3 = $this->config->item('yes_no_option3');

 $new_filter_current_late = array(
										''=> 'Select All',
										3 => 'Yes',
										2 => 'No',
											
											);
// print_r($fci_service_loan);

// echo "</pre>";

?>

<div class="page-content-wrapper">

	<div class="page-content responsive">

		<div class="page-head">

				<!-- BEGIN PAGE TITLE -->

				<div class="page-title">

					<h1> &nbsp; Loan Schedule by Servicer</h1>	

					

				</div>

				<div class="top_download">
				
					
					<form action="<?php echo base_url();?>download_servicer" method="POST">
						<input type="hidden" name="select_box" value="<?php echo $select_box ? $select_box :'';?>">
		
						<input type="hidden" name="l_b" value="<?php echo $l_b ? $l_b :'';?>">
						<input type="hidden" name="p_b" value="<?php echo $p_b ? $p_b :'';?>">
						  <button  class="btn blue" onclick="submit_filter(this)" type="button">Filter</button>
						<button  class="btn red" type="submit">PDF</button>
					</form>
					
				</div>
				
				
							

		</div>
		<div class="row">
			
			<form id="select_borrower" method="POST" action="<?php echo base_url();?>loan_servicer">
			
					<div class="col-md-2">
						<label>Loan Status:</label>
				
								<select name="select_box" id ="select_box" class="form-control"  onchange = "select_box_report(this.value)">
									<option value="" >Select All</option>
									<option value="1" <?php if(isset($select_box)){ if($select_box == 1){ echo 'selected'; } } ?>>Pipeline</option>
									<option value="2" <?php if(isset($select_box)){ if($select_box == 2){ echo 'selected'; } } ?>>Active</option>
									<option value="3" <?php if(isset($select_box)){ if($select_box == 3){ echo 'selected'; } } ?>>Paid Off</option>
				
									
								</select>
				</div>
				<div class="col-md-2">
					<label>Loan Pre-Boarded:</label>			
						<select name="p_b" id ="p_b" class="form-control">
						<?php foreach($new_filter_current_late as $key=>$row){?>
							
                    <option value="<?php echo $key;?>" <?php if($p_b == $key){ echo 'selected'; } ?>><?php echo $row;?></option>
										
							<?php 	}?>

									
					 </select>
				</div>


				
					<div class="col-md-2">
					<label>Loan Boarded:</label>			
						<select name="l_b" id ="l_b" class="form-control">
						<?php foreach($new_filter_current_late as $key=>$row){?>
							
                    <option value="<?php echo $key;?>" <?php if($l_b == $key){ echo 'selected'; } ?>><?php echo $row;?></option>
										
							<?php 	}?>

									
					 </select>
				</div>

				

			
		              
		</form>
		</div>

		<div class="rc_class">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >

				<thead>

					<tr>
						<th>Servicer #</th>
						<th>Borrower Name</th>
						<th>Street Address</th>
						<th>Unit #</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
						<th>Maturity Date</th>
						<th>Original Balance</th>
						<th>Current Balance</th>
					
						
						
						
					</tr>

				</thead>
				
				<tbody>
					<?php
						$count=0;
						$total_loan_amount=0;
						$total_outstanding_amount=0;
						if(isset($loan_result)){
	
							foreach($loan_result as $row)
							{
								?>

								<tr>
									<td><?php echo $row->fci; ?></td>
									<td><a href="<?php echo base_url().'borrower_view/'.$borrower_id[$row->borrower];?>"><?php echo $borrower_list[$row->borrower]; ?></a></td>
									
								    
								    <td><a href="<?php echo base_url().'load_data/'.$row->id;?>"><?php echo $property_list[$row->talimar_loan]; ?></a></td>
								    <td><?php echo $unit[$row->talimar_loan]; ?></td> 
								    <td><?php echo $city[$row->talimar_loan]; ?></td> 
								    <td><?php echo $state[$row->talimar_loan]; ?></td> 
								    <td><?php echo $zip[$row->talimar_loan]; ?></td> 
									<td><?php echo date('m-d-Y',strtotime($row->maturity_date)); ?></td>
									<td>$<?php echo number_format($row->loan_amount); ?></td>
									<td>$<?php echo number_format($row->current_balance); ?></td>
									
									
								</tr>

								<?php
							$count++;
							$total_loan_amount +=$row->loan_amount;
							$total_outstanding_amount +=$row->current_balance;

							 }
						}
					?>
				</tbody>
			<tfoot>
					<tr>
						<th>Total: <?php echo $count; ?></th>
						<th></th>
					
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						
						<th></th>
						<th></th>
						<th><?php echo '$'.number_format($total_loan_amount);?></th>
						<th><?php echo '$'.number_format($total_outstanding_amount);?></th>
					</tr>
					
						<tr>
						<th>Average</th>
						<th></th>
					
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						
						
						<th></th>
						<th></th>
						<th><?php echo '$'.number_format($total_loan_amount/$count);?></th>
						<th><?php echo '$'.number_format($total_outstanding_amount/$count);?></th>
					</tr>
					
					
						
					
				</tfoot>
			</table>

			

	

	</div>

	<!-- END CONTENT -->

</div>

<script>
$(document).ready(function() {
   $('#table').DataTable({
		"aaSorting": [ ],
	});
});


function submit_filter(that){
$("#select_borrower").submit();

}
</script>
