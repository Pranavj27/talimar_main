<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
// echo "<pre>";
// print_r($header_lender_name);
// echo "</pre>";

//$select_box_name = isset($select_box) ? '('.$loan_status_option[$select_box].')' : '' ;
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Contact Balance Schedule <?php //echo $select_box_name; ?></h1>	
					
				</div>
				
				<div class="top_download">
				<form method="POST" action="<?php echo base_url();?>download_lender_schedule_by_contact">
					<input type="hidden" name="download_lender_schedule" value="<?php echo $this->input->post('contact_id');?>">
					
					<input type="hidden" name="active_lender_schedule" value="<?php echo $active_lender_amount;?>">
					
					<button  type="submit" class="btn blue">PDF</button>
				</form>
				</div>
				
		</div>
		
		<div class="row">
			<div class="talimar_no_dropdowns">
			
				<form id="select_investor" method="POST" action="<?php echo base_url();?>reports/lender_schedule_by_contact">
				
				<div class="col-md-6" style="margin-right: 15px !important;">
					<span>Lender Status:</span>
					<select class="form-control" name="active_lender" onchange="select_investor_report(this.value);">
						<option value="">Select All</option>
						<option value="active" <?php if($active_lender_amount == 'active'){echo 'Selected';}?>>Active</option>
						<option value="non-active" <?php if($active_lender_amount == 'non-active'){echo 'Selected';}?>>Not Active</option>
					</select>
				
				</div>
				
				<div class="col-md-5">
				<span>&nbsp;</span>
					<select name="contact_id" id ="contact_id" onchange = "select_investor_report(this.value)" class="chosen">
						<option value="">Select Contact</option>
						<?php 
							foreach($all_lender_contact as $row)
							{
						?>
						<option value="<?php echo $row->contact_id;?>" <?php if(isset($contact_id)){  if($contact_id == $row->contact_id){ echo 'selected'; } }?> ><?php echo $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname; ?></option>
						<?php
						}
						?>
					</select>
				</div>
				
				
				</form>
			</div>
		</div>
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Contact Name</th>
					<th># of <br>Accounts</th>
					
					<th>Active<br>Loans </th>
					<th>Active<br> Balance </th>
					<th># of Paid Off<br> Loans</th>
					<th>$ of Paid Off<br> Loans</th>
					<th>Total<br> Loans </th>
					<th>Total<br> Balance </th>
					<th>Phone</th>
					<th>E-mail</th>
					
					
					
					</tr>
				</thead>
				<tbody class="scrollable">
				<?php 
				
				foreach($loan_schedule_contact as $key => $row){
					
					//$avilable_funds = $row['committed_funds'] - $row['loan_amount'];
					
					if($active_lender_amount == 'active'){
							if($row['loan_amount'] > 0){
					?>
						<tr>
							<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['contact_name'];?></a></td>
							<td><?php echo $row['total_lender'];?></td>
							
							<td><?php echo $row['total_loan'];?></td>
							<td>$<?php echo number_format($row['loan_amount']);?></td>
							<td><?php echo $row['paidoff_loan'];?></td>
							<td>$<?php echo number_format($row['paidoff_loan_amount']);?></td>
							<td><?php echo $row['loan_active_paid'];?></td>
							<td>$<?php echo number_format($row['active_paid']);?></td>
							<td><?php echo $row['investor_phone'] ;?></td>
							<td><?php echo $row['investor_email'];?></td>
							
						</tr>
						
						<?php  } 
				
					}elseif($active_lender_amount == 'non-active'){ 
					
						if($row['loan_amount'] == 0){ ?>
						<tr>
							<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['contact_name'];?></a></td>
							<td><?php echo $row['total_lender'];?></td>
							
							<td><?php echo $row['total_loan'];?></td>
							<td>$<?php echo number_format($row['loan_amount']);?></td>
							<td><?php echo $row['paidoff_loan'];?></td>
							<td>$<?php echo number_format($row['paidoff_loan_amount']);?></td>
							<td><?php echo $row['loan_active_paid'];?></td>
							<td>$<?php echo number_format($row['active_paid']);?></td>
							<td><?php echo $row['investor_phone'] ;?></td>
							<td><?php echo $row['investor_email'];?></td>
							
						</tr>
					
				
					<?php	} 
					
					}else{ ?>
					
						<tr>
							<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['contact_name'];?></a></td>
							<td><?php echo $row['total_lender'];?></td>
							
							<td><?php echo $row['total_loan'];?></td>
							<td>$<?php echo number_format($row['loan_amount']);?></td>
							<td><?php echo $row['paidoff_loan'];?></td>
							<td>$<?php echo number_format($row['paidoff_loan_amount']);?></td>
							<td><?php echo $row['loan_active_paid'];?></td>
							<td>$<?php echo number_format($row['active_paid']);?></td>
							<td><?php echo $row['investor_phone'] ;?></td>
							<td><?php echo $row['investor_email'];?></td>
							
						</tr>
					<?php }	} ?>		
				</tbody>	
				
				
			</table>
		</div>
	</div>
	
	
	
	
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
<script>

$(".chosen").chosen();

$(document).ready(function() {
	 
    $('#table').DataTable({
		"aaSorting": [[ 3, "desc" ], [ 5, "desc" ]]
	});
	
});

function select_investor_report(){
	$('form#select_investor').submit();
}

</script>
<style>
.talimar_no_dropdowns {
		float: left;		
}
.talimar_no_dropdowns div {
    float: left;
    padding: 0px;
}div#contact_id_chosen {
    width: 206px !important;
}
.chosen-container-single .chosen-single {
    padding: 4px 0px 26px 6px !important;
    border-radius: 4px !important;
    border: 1px solid #e1e1e1 !important;
}
</style>



