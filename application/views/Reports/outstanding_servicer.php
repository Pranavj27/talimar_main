<?php
$loan_total_amount = 0;
$servicer_funds_total = 0;
$filter_Received = $this->config->item('filter_Received');
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1> &nbsp; Outstanding Servicer Deposit</h1>	
			</div>

			<div class="top_download">					
				<form action="<?php echo base_url();?>outstanding_servicer_excel" method="POST">
					<input type="hidden" name="loan_status" value="">	
					<input type="hidden" name="received_filter" id="received_filter" value="<?php echo $input_filter_Received; ?>">
					<button class="btn red" type="submit">Excel</button>
				</form>
				<button type="button" onclick="loan_schedule_form()" class="btn blue">Filter</button>
			</div>	

		</div>

		<div class="row">
			<br>		
			<form action="<?php echo base_url('reports/outstanding_servicer_dposit'); ?>" name="form_existing_loan_schedule" id="form_existing_loan_schedule" method="post">	
	            <div class="col-md-2">
	            	<label>Received: </label>
	            	<select id="filter_Received" name="filter_Received" class="form-control">
	            		<?php
	            		foreach($filter_Received as $ReceivedKey => $Received){
	            			$selected = '';
	            			if($ReceivedKey == $input_filter_Received){
	            				$selected = 'selected';
	            			}
	            			?>
	            			<option value="<?php echo $ReceivedKey; ?>" <?php echo $selected; ?>><?php echo $Received; ?></option>
	            			<?php
	            		}
	            		?>
					</select>
	            </div>
	        </form>          
		</div>

		<div class="rc_class">
			<table id="outstanding_servicer_dposit" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						<th>Borrower Name</th>
						<th>Street Address</th>
						<th>Unit #</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
						<th>Amount</th>
						<th>Servicer Funds</th>
						<th>Received</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$arrFilterCheck = array();
					if($input_filter_Received == 'Yes'){
						$arrFilterCheck[] = 'Yes';
					}else if($input_filter_Received == 'No'){
						$arrFilterCheck[] = 'No';
					}else{
						$arrFilterCheck[] = 'Yes';
						$arrFilterCheck[] = 'No';
					}
					

					if($outstanding_servicer){
						foreach ($outstanding_servicer as $key => $borrowerVal) {

							$received = '';
							/*if($borrowerVal->received_yes == '1'){
								$received = 'Yes';
							}if($borrowerVal->received_yes == '0'){
								$received = 'No';
							}
							if($received == 'No'){
								if($borrowerVal->received_no == '1'){
									$received = 'Yes';
								}elseif($borrowerVal->received_no == '0'){
									$received = 'No';
								}
							}*/

							
							if($borrowerVal->received_yes == '1' || $borrowerVal->received_yes == 1){
								$received = 'Yes';
							}if($borrowerVal->received_yes == '0' || $borrowerVal->received_yes == 0){
								$received = 'No';
							}

							if($received == 'No'){
								if($borrowerVal->received_no == '1' || $borrowerVal->received_no == 1){
									$received = 'Yes';
								}elseif($borrowerVal->received_no == '0' || $borrowerVal->received_no == 0){
									$received = 'No';
								}
							}
							

							if(in_array($received, $arrFilterCheck))
							{

								$loan_id = $borrowerVal->id;
								$borrower_id = $borrowerVal->borrower;

								$talimar_loan = $borrowerVal->talimar_loan;
								$loan_servicer_funds = $this->User_model->query("SELECT SUM(paid_to_other + paid_to_broker) AS servicer_funds FROM closing_statement_items WHERE (hud = '814' OR  hud = '1005') AND `talimar_loan` = '$talimar_loan'");
								$loan_servicer_funds = $loan_servicer_funds->row();

								$loan_property = $this->User_model->query("
								SELECT 
								property.property_address AS property_address,
								property.unit AS property_unit,
								property.city AS property_city,
								property.state AS property_state,
								property.zip AS property_zip
								 FROM loan_property AS property WHERE property.loan_id = '$loan_id'");
								$loan_property = $loan_property->row();


								$borrower_firstname = '';
								$borrower_lastname = '';
								if($borrower_id){
									$contact = $this->User_model->query("
										SELECT 
										cnt.contact_firstname AS borrower_firstname, 
										cnt.contact_middlename AS borrower_middlename, 
										cnt.contact_lastname AS borrower_lastname, 
										cnt.contact_address AS borrower_address,
										cnt.contact_unit AS borrower_unit,
										cnt.contact_city AS borrower_city,
										cnt.contact_state AS borrower_state,
										cnt.contact_zip AS borrower_zip,
										cnt.contact_email AS borrower_email,
										cnt.contact_email2 AS borrower_email2
										 FROM contact AS cnt 
										 INNER JOIN borrower_contact AS borrower ON cnt.contact_id = borrower.contact_id WHERE borrower.borrower_id = '$borrower_id'");
									$contact = $contact->row();
									if($contact){
										$borrower_firstname = $contact->borrower_firstname;
										$borrower_lastname = $contact->borrower_lastname;
									}
								}

								
								$property_address = '';
								$property_unit = '';
								$property_city = '';
								$property_state = '';
								$property_zip = '';
								if($loan_property){
									$property_address = $loan_property->property_address;
									$property_unit = $loan_property->property_unit;
									$property_city = $loan_property->property_city;
									$property_state = $loan_property->property_state;
									$property_zip = $loan_property->property_zip;
								}
								
								?>
								<tr>
									<td>
										<a href="/Pranav/Talimar/borrower_view/<?php echo $borrower_id; ?>"><?php echo $borrower_firstname.' '.$borrower_lastname; ?></a>
									</td>
									<td>
										<a href="/Pranav/Talimar/load_data/<?php echo $loan_id; ?>"><?php echo $property_address; ?></a>
									</td>
									<td>
										<?php echo $property_unit; ?>
									</td>
									<td>
										<?php echo $property_city; ?>
									</td>
									<td>
										<?php echo $property_state; ?>
									</td>
									<td>
										<?php echo $property_zip; ?>
									</td>
									<td>
										$<?php echo number_format($borrowerVal->loan_amount, 2); ?>
									</td>
									<td>
										$<?php echo number_format($loan_servicer_funds->servicer_funds, 2); ?>
									</td>
									<td>
										<?php
										echo $received;
										$loan_total_amount += $borrowerVal->loan_amount;
										$servicer_funds_total += $loan_servicer_funds->servicer_funds;
										?>
									</td>
								</tr>
								<?php
							}					
						}
					}
					?>		
				</tbody>
				<tfoot>					
					<tr>
						<th></th>						
						<th></th>
						<th></th>					
						<th></th>					
						<th></th>					
						<th>Total</th>					
						<th><?php echo '$'.number_format($loan_total_amount, 2); ?></th>	
						<th><?php echo '$'.number_format($servicer_funds_total, 2); ?></th>
						<th></th>	
						
					</tr>
				</tfoot>
			</table>
	</div>
</div>


<script type="text/javascript">
function  loan_schedule_form() {
	$('#form_existing_loan_schedule').submit();
}
$(document).ready(function() {
    var table = $('#outstanding_servicer_dposit').DataTable({
		"aaSorting": [],
	});

} );
</script>