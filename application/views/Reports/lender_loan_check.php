<?php

error_reporting(0);
$loan_status_option = $this->config->item('loan_status_option');
// echo "<pre>";

// print_r($trust_deed);

// echo "</pre>";

?>

<div class="page-content-wrapper">

	<div class="page-content responsive">

		<div class="page-head">

				<!-- BEGIN PAGE TITLE -->

				<div class="page-title">

					<h1> &nbsp; Lender- Loan Check </h1>	

					

				</div>

				<div class="top_download">
					<!--
					<a href="<?php echo base_url();?>download_fci_serviced_account">

					<button  class="btn blue">Excel Download</button>

					</a>
					
					
					<a href="<?php echo base_url();?>download_fci_serviced_pdf">

					<button  class="btn blue">Download</button>

					</a>
					-->
				</div>

		</div>

		<div class="rc_class">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >

				<thead>
					<tr>
						<th>Talimar#</th>
						<th>Loan amount</th>
						<th>Available</th>
						<th># of Lender</th>
						<th>Status</th>
						<th>Property Address</th>
					</tr>
				</thead>
				
				<tbody>
				<?php 
				$number 			= 0;
				$total_loan_amount 	= 0;
				$total_available 	= 0;
					if(isset($trust_deed))
					{
						foreach($trust_deed as $key => $row)
						{
							$total_loan_amount  = $total_loan_amount + $row['loan_amount'];
							$total_available  	= $total_available + $row['available'];
				?>				
							<tr>
								<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['talimar_loan']; ?></td>
								<td><?php echo '$'.number_format($row['loan_amount']); ?></td>
								<td><?php echo '$'.number_format($row['available']); ?></td>
								<td><?php echo $row['count_assigment_lender']; ?></td>
								<td><?php echo $loan_status_option[$row['loan_status']]; ?></td>
								<td><?php echo $row['property_address']; ?></td>
								
							</tr>
			<?php			$number++;
						}
					}
			?>
				</tbody>
				<tfoot>
					<tr>
						<th>Total:<?php $number; ?></th>
						<th><?php echo '$'.number_format($total_loan_amount); ?></th>
						<th><?php echo '$'.number_format($total_available); ?></th>
						<th colspan="3"></th>
					</tr>
				</tfoot>
			</table>

			

	</div>

	</div>

	<!-- END CONTENT -->

</div>

<script>
$(document).ready(function() {
    $('#table').DataTable({
		"order": [[ 4, "asc" ]]
	});
} );
</script>