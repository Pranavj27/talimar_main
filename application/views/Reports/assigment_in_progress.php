<?php
error_reporting(0);
$loan_status_option 						= $this->config->item('loan_status_option');
 $assii_security_type					    = $this->config->item('assii_security_type');
 $assi_security_type					    = $this->config->item('assi_security_type');
$new_assii_disclouser_status				= $this->config->item('new_assii_disclouser_status');
//$assii_servicer_status						= $this->config->item('assii_servicer_status');
//$assii_fund_received						= $this->config->item('assii_fund_received');
//$assii_file_status							= $this->config->item('assii_file_status');
$assi_disclouser_status					= $this->config->item('assi_disclouser_status');
$assi_servicer_status						= $this->config->item('assi_servicer_status');
$assi_fund_received						= $this->config->item('assi_fund_received');
$assi_file_status							= $this->config->item('assi_file_status');
// echo '<pre>';
// print_r($all_assigment_result);
// echo '</pre>';

?>
<style>
.rc_class{
	padding:10px 0px;
}
table#DataTables_Table_0 th {
    padding: 2px;
	font-size: 12px;
    font-weight: 600;
	}
	
table#DataTables_Table_0 td {
    padding: 8px 2px;
    vertical-align: top;
	}


</style>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Assignments in Process </h1>	
					
				</div>
				<div class="top_download">
				<!--
				<a href="<?php echo base_url();?>download_assigment_excel">
				<button  class="btn blue">Excel Download</button>
				</a>
				-->
				<form action="<?php echo base_url();?>download_assigment" method="post">
				
					<input type="hidden" name="security_type" value="<?php echo $security_type ? $security_type : '';?>">
					<input type="hidden" name="loan_status" value="<?php echo $loan_status ? $loan_status : '';?>">
					<input type="hidden" name="disclosure_state" value="<?php echo $disclosure_state ? $disclosure_state : '';?>">
					<input type="hidden" name="fund_received" value="<?php echo $fund_received ? $fund_received : '';?>">
					<input type="hidden" name="servicer_status" value="<?php echo $servicer_status ? $servicer_status : '';?>">
					<input type="hidden" name="file_status" value="<?php echo $file_status ? $file_status : '';?>">
                 <button type="button" onclick="form_submit()" class="btn blue">Filter</button>
                 <?php if($check_data=='1'){?>

					<button type="submit" class="btn red" disabled>PDF</button>
				<?php }else{?>
			<button type="submit" class="btn red" >PDF</button>

				<?php }?>
				
				</form>
				</div>
		</div>
		
		<form id="assignment" action="<?php echo base_url();?>assigment_in_progress" method="post">
			<div class="row">

					<div class="col-md-2">
		     File Status:
				
				<select class="form-control" name="file_status">
				
				   <option value="">Select All</option>
					<option value="2"  <?php if($file_status == '2'){echo 'Selected';}?>>Open</option>
					<option value="1" <?php if($file_status == '1'){echo 'Selected';}?>>Close</option>
				</select>
			</div>	
			<div class="col-md-2">
		Security Type:
				<select class="form-control" name="security_type">
					<?php foreach($assii_security_type as $key=>$row){?>
				
					<option value="<?php echo $key;?>" <?php if($security_type == $key){echo 'Selected';}?>><?php echo $row;?></option>
						<?php 	}?>
				</select>

			</div>
				<div class="col-md-2">
		 Loan Status:
				<select class="form-control" name="loan_status">
					<option value="">Select All</option>
					<option value="1"  <?php if($loan_status == '1'){echo 'Selected';}?>>Pipeline</option>
					<option value="2" <?php if($loan_status == '2'){echo 'Selected';}?>>Active</option>
				</select>
			</div>
				<div class="col-md-2">
		 Disclosure Status:
				<select class="form-control" name="disclosure_state">
					
					<?php foreach($new_assii_disclouser_status as $key=>$row){?>
					<?php if($row == 'Not Ready'){$val = 'Select All';}else{$val = $row;}?>	
				
					<option value="<?php echo $key;?>" <?php if($disclosure_state == $key){echo 'Selected';}?>><?php echo $val;?></option>
						<?php 	}?>
				</select>
			</div>
				<div class="col-md-2">
		     Funds Received:
				<select class="form-control" name="fund_received">

					<option value="">Select All</option>
					<option value="99" <?php if($fund_received == 99){echo 'selected';}?>>Pending</option>
					<option value="1" <?php if($fund_received == 1){echo 'selected';}?>>Requested</option>
					<option value="2" <?php if($fund_received == 2){echo 'selected';}?>>Sent</option>
					<option value="3" <?php if($fund_received == 3){echo 'selected';}?>>Received</option>
				</select>
			</div>
				<div class="col-md-2">
		    Servicer Status:
				<select class="form-control" name="servicer_status">
			         <option value="">Select All</option>
					<option value="1"  <?php if($servicer_status == '1'){echo 'Selected';}?>>Submitted</option>
					<option value="2" <?php if($servicer_status == '2'){echo 'Selected';}?>>Not Submitted</option>

				</select>
			</div>

		
</div>
<!-- <div class="row">
		<div class="col-md-1 pull-right">
		<br>
		 
			</div>


</div> -->

		</form>
		
		<div class="rc_class">
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					
					<th>Lender Name</th>
					<th>Contact 1 Name</th>
					<!--					
					
					<th>Investment<br>%</th>
					-->
					<th>Property Address</th>
					<th>$<br>Investment</th>
					
					<th>Security<br>Type</th>
					<th>Disclosures<br>Status</th>
					<th>Funds<br>Received</th>
					<th>Servicer<br>Status</th>
					<th>File<br>Status</th>
					
				
					</tr>
				</thead>
				<tbody>
				<?php
				$key = 0;
				$total_investment = 0;
				if(isset($all_assigment_result)){
				foreach($all_assigment_result as  $row)
				{

					$total_investment +=$all_assigment_result[$key]['investment'];

					
				?>
						<tr>
							
							<td><a href ="<?php echo base_url().'investor_view/'.$all_assigment_result[$key]['lender_id']; ?>"><?= $all_assigment_result[$key]['lender_name'];?></a></td>
							<td><a href ="<?php echo base_url().'viewcontact/'.$all_assigment_result[$key]['contact_id']; ?>"><?= $all_assigment_result[$key]['lender_contact_name'];?></a></td>
							<td>
								<a href ="<?php echo base_url().'load_data/'.$all_assigment_result[$key]['loan_id']; ?>">
								<?= $all_assigment_result[$key]['property'];?></a>
							</td>
							<td>$<?= number_format($all_assigment_result[$key]['investment']);?></td>

							<?php if($row['nsecurity_type']=='1'|| $row['nsecurity_type']=='2'){?>

	                        <td  style="text-align:left;"><a href ="<?php echo base_url().'load_data/'.$all_assigment_result[$key]['loan_id'].'#status' ?>"><?php echo $assi_security_type[$row['nsecurity_type']];?></a></td>

							<?php }	else{ ?>
							<td  style="text-align:left;"><?php echo $assi_security_type[$row['nsecurity_type']];?></td>
						              <?php }?>
							<td style="text-align:left;"><?php echo $new_assii_disclouser_status[$row['ndisclouser_status']];?></td>
							
							<td  style="text-align:left;"><?php echo $assi_fund_received[$row['nfunds_received']];?></td>
							
							<td  style="text-align:left;"><?php echo $assi_servicer_status[$row['nservicer_status']];?></td>
							
							<td style="text-align:left;"><?php echo $assi_file_status[$row['nfiles_status']];?></td>
						
							
						</tr>
					<?php
					$key++;
				}}
					?>
				</tbody>
				 <tfoot>
					<tr>
						<th>Total:&nbsp;<?php echo $key; ?></th>
						
						
						<th></th>
						<th ></th>
						<th><?php echo '$'.number_format($total_investment);?></th>
						<th ></th>
						<th ></th>
						<th ></th>
						<th ></th>
						<th ></th>
					</tr>
		
				</tfoot> 
			</table>
			
	</div>
	</div>
	<!-- END CONTENT -->
</div>
<script src="<?php echo base_url()?>assets/extra_css_js/datatable/jquery.dataTables.min.js"></script>

<script>
$(document).ready(function() {
    $('.table').DataTable({
  "order": [[ 3, "desc" ]],
} );
} );

function form_submit(that){
	$('form#assignment').submit();
}
</script>