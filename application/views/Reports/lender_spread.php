<?php
error_reporting(0);
$loan_status_option 						= $this->config->item('loan_status_option');
$template_yes_no_option 				= $this->config->item('template_yes_no_option');


	foreach($all_vendor_data as $vendor){
			$vendor_name[$vendor->vendor_id] = $vendor->vendor_name;
				}	
?>
<style>

table#DataTables_Table_0 th {
    padding: 10px;
	font-size: 14px;
    font-weight: 600;
	}
	
table#DataTables_Table_0 td {
    padding: 8px 2px;
    vertical-align: top;
	}


</style>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
			
				<div class="page-title">
					<h1> &nbsp; Lender Negative Spread </h1>	
					
				</div>
				<div class="top_download">
				<!--
				<a href="<?php echo base_url();?>download_assigment_excel">
				<button  class="btn blue">Excel Download</button>
				</a>
				-->
				<form action="<?php echo base_url();?>download_negative_spread" method="post">
				
					<input type="hidden" name="negative" value="<?php echo $negative ? $negative : '';?>">
					<input type="hidden" name="loan_status" value="<?php echo $loan_status ? $loan_status : '';?>">
					
                 <?php if($check_data=='1'){?>

					<button type="submit" class="btn blue" disabled>PDF</button>
				<?php }else{?>
			<button type="submit" class="btn blue" >PDF</button>
				<?php }?>
				</form>
				</div>
		</div>
		
		<form id="assignment" action="<?php echo base_url();?>lender_negative_spread" method="post">
			<div class="row">

				<div class="col-md-2">
		 Loan Status:
				<select class="form-control" name="loan_status">
					<option value="">Select One</option>
					<option value="1"  <?php if($loan_status == '1'){echo 'Selected';}?>>Pipeline</option>
					<option value="2" <?php if($loan_status == '2'){echo 'Selected';}?>>Active</option>
					<option value="3" <?php if($loan_status == '3'){echo 'Selected';}?>>Paid Off</option>
				</select>
			</div>
				<div class="col-md-2">
		Negative Spread:
				<select class="form-control" name="negative">
				
					<?php foreach($template_yes_no_option as $key=>$row){?>
				
					<option value="<?php echo $key;?>" <?php if($negative == $key){echo 'Selected';}?>><?php echo $row;?></option>
						<?php 	}?>
				</select>
			</div>

		
</div>
<div class="row">
		<div class="col-md-1 pull-right">
		<br>
		   <input type="submit" class="btn blue" value="Filter">
			</div>


</div>

		</form>
		
		<div class="rc_class">
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
				
					<th>Lender Name</th>
					<th>Loan #</th>
					<th>Servicer Name</th>
					<th>Servicer #</th>
					<th>Investment $</th>
					<th>Neg. Spread</th>
					<th>Negative Spread</th>
			
					
					
				
				</tr>
				</thead>
				<tbody>
				<?php
				$key = 0;
				$total_investment = 0;
				$total_investment_spread = 0;


				if(isset($lender_spread_data)){
				foreach($lender_spread_data as  $row)
				{
					$total_investment +=$row['investment'];
					$total_investment_spread +=$row['nega_spread'];

				?>
						<tr>
							
							<td><?php echo $row['lender_name'];?></td>
							<td><a href ="<?php echo base_url().'load_data/'.$row['loan_id']; ?>"><?php echo $row['talimar_loan'];?></a></td>
							<td><?php echo $vendor_name[$row['servicer_name']];?></td>
							<td><?php echo $row['fci'];?></td>
							<td><?php echo '$'.number_format($row['investment'],2);?></td>
							<td><?php echo '$'.number_format($row['nega_spread'],2);?></td>
							<td><?php echo $row['lender_spread'];?></td>
					
							

						

	                       
							
						
							
						</tr>
					<?php
					$key++;
				}}
					?>
				</tbody>

					<tfoot id="tfootdataTables">
				<tr>
					<th>Total</th>
					<th><?php echo $key; ?></th>
					
					<th></th>
					<th></th>
					
			
					
				
					
					<th><?php echo '$'.number_format($total_investment); ?></th>
					
					
					<th><?php echo '$'.number_format($total_investment_spread); ?></th>
					<th></th>
					
				
				</tfoot>

			</table>
			
	</div>
	</div>
	<!-- END CONTENT -->
</div>
<script src="<?php echo base_url()?>assets/extra_css_js/datatable/jquery.dataTables.min.js"></script>

<script>
$(document).ready(function() {
    $('.table').DataTable();
  //"order": [[ 3, "desc" ]],
// } );
} );

// function secure_value(that){
// 	$('form#assignment').submit();
// }
</script>