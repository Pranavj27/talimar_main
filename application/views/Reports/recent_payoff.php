<?php

// echo '<pre>';
// print_r($all_recent_payoff);
// echo '</pre>';

$payoff_previous_days = $this->config->item('payoff_previous_days');

?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Recent Lender Payoffs  </h1>
				</div>

				<div class="top_download">
					<form method="post" action="<?php echo base_url();?>download_recent_payoffs">
						<input type="hidden" name="previous_payoffs" value="<?php echo $pre_day ? $pre_day : '';?>">
						<button  type="submit" class="btn blue">PDF</button>
					</form>
				</div>
		</div>

		<div class="row">
			<form id="lender_payoff" method="post" action="<?php echo base_url();?>recent_payoffs">
				<div class="col-md-2">
					<label>Paid Off in Last:</label>
					<select name="count_day" class="form-control" onchange="payoff_value(this);">
						<?php foreach($payoff_previous_days as $key => $row){ ?>

							<option value="<?php echo $key;?>" <?php if($key == $pre_day){echo 'Selected';}?>><?php echo $row;?></option>

						<?php } ?>
					</select>
				</div>
			</form>
		</div>

		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						<!--<th>Lender Name</th>-->
						<th>Contact Name</th>
						<th>Phone #</th>
						<th>E-Mail</th>
						<th>Property Address</th>
						<th>Paid Off Date</th>
						<th>Investment</th>
						<th>$ of Active Loans</th>
						<th>Committed Capital</th>
						<th>Available Capital</th>
						
					
					</tr>
				</thead>
				<tbody>
				<?php foreach($all_recent_payoff as $row){ ?>

					<tr>
						<!--<td><?php echo $row['lender_name'];?></td>-->
						<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['contact_name'];?></a></td>
						<td><?php echo $row['l_c_phone'];?></td>
						<td><?php echo $row['l_c_email'];?></td>
						<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['only_s_address'];?></a></td>
						<td><?php echo date('m-d-Y', strtotime($row['payoff_date']));?></td>
						<td>$<?php echo number_format($row['investment']);?></td>
						<td>$<?php echo number_format($row['all_active_amount']);?></td>
						<td>$<?php echo number_format($row['committed_funds']);?></td>
						<td>$<?php echo number_format($row['available_funds']);?></td>
					</tr>

				<?php } ?>
				</tbody>
				
			</table>
	</div>
	</div>
</div>

<script>
$(document).ready(function() {
    $('#table').DataTable({
        "order": [[ 6, "desc" ]]
    });
} );


function payoff_value(that){

	$('form#lender_payoff').submit();
}
</script>