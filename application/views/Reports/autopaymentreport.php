<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');
$yes_no_option3 = $this->config->item('no_yes_option');
$ach_activated = $this->config->item('ach_activated');

$loan_status_filter = array(
								'' => 'Select All',
								1  => 'Pipeline',
								2  => 'Active',
								3  => 'Paid Off',
							);

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Auto Payment Report</h1>
				</div>
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>ReportData/AutoPaymentReport">
						<input type="hidden" name="autopayment" value="<?php echo isset($auto_payment) ? $auto_payment : '';?>">
						<input type="hidden" name="reportDisplay" value="pdf">
						<button type="submit" class="btn blue">PDF</button>
					</form>
				</div>
				
		</div>
		
		<div class="row">
			<form method="post" action="<?php echo base_url();?>ReportData/AutoPaymentReport">
				<div class="col-md-2">
					<label>ACH:</label>
					<select class="form-control" name="autopayment">
						<option value="">Select All</option>
						<?php foreach($ach_activated as $key => $row) { ?>
							<option value="<?php echo $key;?>" <?php if($key == $auto_payment){ echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-1">
					<button type="submit" name="submit" class="btn btn-primary btn-sm" style="margin-top: 25px;">Filter</button>
				</div>
			</form>
		</div>

		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Property Address</th>
					<th>Unit #</th>
					<th>City</th>
					<th>State</th>
					<th>Zip</th>
					<th>Borrower Name</th>
					<th>Contact 1 Name</th>
					<th>Contact 1 E-Mail</th>					
					<th>Contact 1 Phone</th>					
					<th>Payment Amount</th>					
					<th>ACH</th>					
				</tr>
				</thead>
				<tbody>
				
				<?php if(isset($AutoPaymentReport) && is_array($AutoPaymentReport)){ 
						foreach($AutoPaymentReport as $rowsss){ 

								$talimar_no['talimar_loan'] = $rowsss->talimar_loan;

								$fetch_loan_data = $this->User_model->select_where('loan', $talimar_no);
								$fetch_loan_data = $fetch_loan_data->result();

								$fetch_impound_account1 = $this->User_model->select_where_asc('impound_account', $talimar_no, 'position');
								if ($fetch_impound_account1->num_rows() > 0) {
									$fetch_impound_account = $fetch_impound_account1->result();
								}

								$fetch_property_sql = "SELECT * FROM loan_property JOIN property_home ON property_home.id = loan_property.property_home_id WHERE loan_property.talimar_loan = '" . $rowsss->talimar_loan . "' AND  property_home.primary_property = '1' ";
								$fetch_property_data = $this->User_model->query($fetch_property_sql);
								$fetch_property_data = $fetch_property_data->result();

								$aa = 0;				
								if(isset($fetch_impound_account)){
									
									foreach($fetch_impound_account as $key=>$row){
										$impound_amount = $row->amount;
										$readonly = '';
																
										if($row->items == 'Mortgage Payment')
										{
												 $aa +=($fetch_loan_data[0]->current_balance*($fetch_loan_data[0]->intrest_rate)/100)/12;						
												// $aa +=($outstanding_balance*$fetch_loan_result[0]->intrest_rate/100)/12;						
											// $aa += ($fetch_loan_result[0]->intrest_rate/100) * $fetch_loan_result[0]->loan_amount / 12;
										}
										if($row->items == 'Property / Hazard Insurance Payment')
										{
											if($row->impounded == 1){
												$aa += isset($loan_property_result) ? ($loan_property_insurance_result->annual_insurance_payment / 12) : 0;
											}
										}
																
										if($row->items == 'County Property Taxes')
										{
											if($row->impounded == 1){
												$aa += isset($loan_property_result) ? ($loan_property_result[0]->annual_property_taxes / 12) : 0;
											}
										}
										if($row->items == 'Flood Insurance')
										{
											if($row->impounded == 1){
												$aa += isset($row->amount) ? ($row->amount) : 0;
											}
										}
																
										if($row->items == 'Mortgage Insurance')
										{
											if($row->impounded == 1){
												$aa += isset($row->amount) ? ($row->amount) : 0;
											}
										}
									}
								}

							?>
						
									<tr>
										<td><a href="<?php echo base_url();?>load_data/<?php echo $rowsss->loan_id;?>"><?php echo $rowsss->property_address;?></a></td>
										<td><?php echo $rowsss->unit;?></td>
										<td><?php echo $rowsss->city;?></td>
										<td><?php echo $rowsss->state;?></td>
										<td><?php echo $rowsss->zip;?></td>
										<td><a href="<?php echo base_url();?>borrower_view/<?php echo $rowsss->borrower_id;?>"><?php echo $rowsss->borrower_name;?></a></td>
										<td><?php echo $rowsss->contact_firstname." ".$rowsss->contact_lastname;?></td>
										<td><?php echo $rowsss->contact_email;?></td>
										<td><?php echo $rowsss->contact_phone;?></td>
										<td>$<?php echo number_format($aa,2);?></td>
										<td><?php echo $ach_activated[$rowsss->ach_activated];?></td>
									</tr>
				
								<?php  } }else{ ?>
				
								<tr>
									<td style="width:100%;" colspan="11">No data found!</td>
								</tr>
				<?php } ?>
					
				</tbody>		
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {
	 
    $('#table').DataTable();
	
});



</script>




