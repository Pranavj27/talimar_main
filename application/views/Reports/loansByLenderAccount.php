<?php 
//$loan_tye = $this->config->item('loan_tye'); 
$loan_tye 					= $this->config->item('loan_type_option');
$serviceing_condition_option = $this->config->item('serviceing_condition_option'); ?>
<div class="page-content-wrapper">
	<style type="text/css">
		input.btn.blue.aa {
		  
		    margin-top: 14px;
		    border-radius: 0px;
		   
		}
		.talimar_no_dropdowns div {

		    padding: 0px 2px;
		    width: 24%!important;
		    font-size: 14px!important;
		}

		.talimar_no_dropdowns {
		    padding-left: 0px!important;
		}
		#table_wrapper>.row>.col-md-6.col-sm-12 {
		    
		    padding-left: 0px;
		}
		.form-control[disabled] {
		    cursor: not-allowed;
		    background-color: #dddddd;
		}
		.talimar_no_dropdowns div {
			width: 100%!important;
		}
		table.dataTable tfoot th{
			border-top: 0px solid #111 !important;
		}
		 .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th  {
		 	     border: 0px solid #ddd !important; 
		 }
	</style>
	<div class="page-content responsive">
		<!--
				Update this method for add filter functionality in filter by date in lender data last update date is 24-03-2021
		-->
		<div class="page-head" style="margin-right:0px">
				<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>&nbsp;Loans by Lender Account</h1>
			</div>		
		</div>
		<div class="row">
			<div class="talimar_no_dropdowns" style="float:left;width: 20%!important;">
				<form id="form_iad" method="POST" action="<?php echo base_url().'reports/loanByLenderaccount'; ?>">
					<div class="float-direction-left" style="    width: 100%!important;">
						Lender Name : <br>
						<select name="lender_name" id="lender_name" class="selectpicker" data-live-search="true" onchange="load_form()" style="width: 215px!important">
							<option value="" <?php if($lender_name == ''){echo 'selected';}?>>Select One</option>
							<?php foreach ($investerData as $key => $lenders) { ?>
								<option value="<?php echo $lenders->lender_id; ?>" <?php if($lenders->lender_id == $lender_name){echo 'selected';}?>><?php echo  $lenders->name;?></option>
							<?php }  ?>
						</select>
					</div>	
				</form>
			</div>
		</div>
		<br>
		<!-- Close updated section -->
		<div class="row">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content">
				<thead>
					<tr>
						<th>Street Address</th>
						<th>Unit #</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
						<th>Loan Type</th>
					    <th>Term</th>
						<th>$ Loan Balance</th>
						<th>$ Ownership</th>
						<th>% Ownership</th>
						<th>% Yield</th>
					    <th>$ Payment</th>
						<th>Status</th>	
					</tr>
				</thead>
				<tbody>

				<?php				
				
				$originalBalance 	= 0.00;
				$payment_amount 	= 0.00;
				$term_month  = 0.00;
				$sumInvestment=0.00;
				$totalInvestPercent=0.00;
				$yieldPercent=0.00;
				if(isset($Newlenderaccounts) && is_array($Newlenderaccounts)){ 

					foreach($Newlenderaccounts as $row){
						$inverstPercent=(($row->investment/$row->current_balance)*100);
					?>
					<tr>
						<td><a href="<?php echo base_url()."load_data/".$row->loan_id;?>"><?php echo $row->property_address;?></td>
						<td><?php echo $row->unit;?></td>
						<td><?php echo $row->city;?></td>
						<td><?php echo $row->state;?></td>
						<td><?php echo $row->zip;?></td>
						<td><?php echo !empty($loan_tye[$row->loan_type]) ? $loan_tye[$row->loan_type] : '';?></td>
						<td><?php echo $row->term_month; ?></td>
						<td>$<?php echo number_format($row->current_balance, 2);?></td>
						<td>$<?php echo number_format($row->investment, 2);?></td>
						<td><?php echo number_format($inverstPercent, 2);?>%</td>
						<td><?php echo $row->invester_yield_percent;?>%</td>
						<td>$<?php echo number_format($row->payment, 2);?></td>
						<td><?php echo $serviceing_condition_option[$row->loan_condition];?></td>											
					</tr>
					<?php 
						$originalBalance = $originalBalance + (double)$row->current_balance;
						$payment_amount = $payment_amount + (double)$row->payment;
						$term_month = $term_month + (double)$row->term_month;
						$sumInvestment=$sumInvestment+ (double)$row->investment;
					} 
				}else{ ?>

					<tr>
						<td colspan="12">No data found!</td>
					</tr>

				<?php 
				} 
				if($sumInvestment>0 && $originalBalance>0){
					$totalInvestPercent=(($sumInvestment/$originalBalance)*100);
				}	
				if($payment_amount>0){
					$yieldPercent=(($payment_amount*12)/$sumInvestment);
				}				
				?>
				</tbody>
				<tfoot>
					<tr>
						<th>Total: <?php echo count($Newlenderaccounts); ?></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th><?php echo '$'.number_format($originalBalance, 2);?></th>
						<th><?php echo '$'.number_format($sumInvestment, 2);?></th>
						<th></th>
						<th></th>
						<th><?php echo '$'.number_format($payment_amount, 2);?></th>
						<th></th>
					</tr>
					<tr>
						<th>Average:</th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th><?php echo number_format($term_month > 0 ? $term_month/count($Newlenderaccounts):0.00, 2);?></th>
						<th><?php echo '$'.number_format($originalBalance > 0 ? $originalBalance/count($Newlenderaccounts):0.00, 2);?></th>
						<th></th>
						<th><?php echo number_format($totalInvestPercent, 2);?>%</th>
						<th><?php echo number_format($yieldPercent, 2);?>%</th>
						<th><?php echo '$'.number_format($payment_amount > 0 ?$payment_amount/count($Newlenderaccounts):0.00, 2);?></th>
						<th></th>
					</tr>
				</tfoot>
			</table>
		</div>
			
	</div>
	<!-- END CONTENT -->
</div>

<!--
		Update this method for add filter functionality in filter by date in lender data last update date is 24-03-2021
-->
<script>

function load_form(){

 $('#form_iad').submit();	

}

$(document).ready(function() {
	 
    $('#table').DataTable();
	
});
</script>




	


