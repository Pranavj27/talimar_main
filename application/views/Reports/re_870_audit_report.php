<?php error_reporting(0); ?>

<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; RE870 Audit Report </h1>
				</div>

				<div class="top_download">
					<a href="<?php echo base_url();?>download_re840_audit_pdf">
					<button  class="btn blue">PDF</button>
					</a>
				</div>
		</div>

		<div class="rc_class re870_table">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						<th>Contact Name</th>
						<th>Contact Phone</th>
						<th>Contact Email</th>
						<th>RE870 Date</th>
						<th>Status</th>
						<th>Active Lender</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#table').DataTable();
		$('#table').DataTable().clear().destroy();
	   	$('#table').DataTable({
	      	'processing': true,
	      	'serverSide': true,
	      	'pageLength': 10,
	      	'lengthChange': false,
	      	'oLanguage': {sProcessing: '<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>'},
	      	'serverMethod': 'post',
	      	"columnDefs": [
			    { "width": "20%", "targets": 0 }
			  ],
	      	'ajax': {
	          'url':'<?php echo base_url(); ?>Reports/ajax_re870_audit_report',
	      	},
	      	'columns': [
	         	{ data: 'c_name', "render": function(data, type, row, meta){
		            
		                data = '<a target="_blank" href="<?php echo base_url('viewcontact/') ?>'+row.contact_id+'">' + data + '</a>';
		            
		            
		            return data;}
		        },
		        { data: 'contact_phone'},
		        { data: 'contact_email'},
		        { data: 'lender_RE_date'},
		        { data: 'status'},				        
		        { data: 'activeloan'}
	      	]
	   	});
	});
</script>