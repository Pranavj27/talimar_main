<?php
$filter_loan_status = $this->config->item('filter_loan_status');
 //echo $status_type;
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<div class="page-title">
					<h1> &nbsp; Accrued Charges </h1>	
				</div>

				<div class="top_download">					
					<form action="<?php echo base_url();?>download_accrued_charges" method="POST">
						<input type="hidden" name="loan_statuss" value="<?php echo $status_type ? $status_type : '';?>">
						<button class="btn blue" type="submit">PDF</button>
					</form>
				</div>				
		</div>
		<div class="row">
			<form method="post" action="<?php echo base_url();?>accrued_charges">
				<div class="col-md-2">
					<label>Loan Status:</label>
					<select class="form-control" name="loan_status">
						<?php foreach($filter_loan_status as $key => $row){?>
							<option value="<?php echo $key?>" <?php if($status_type == $key){echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>

					<button type="submit" class="btn blue" style="margin-top: 24px !important;">Filter</button>
				
			</form>
		</div>

		<div class="rc_class">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						<th>Street Address</th>
						<th>Unit #</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
						<th>Borrower Name</th>
						<th>Loan Amount</th>
						<th>Accrued</th>
						<th>Paid</th>
						<th>Remaining</th>
						<!--<th>Contact Name 1</th>
						<th>Contact Phone 1</th>-->
						
					</tr>

				</thead>
				
				<tbody>
				<?php
				
				$loanTotal = 0;
				$accuredAmount = 0;
				foreach($all_accrued_charges as $row){
					
					$accuredAmount += $row['accured_amount'];
					$loanTotal += $row['loan_amount'];
					
					?>
					<tr>
						
						<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['property_address'];?></a></td>
						<td><?php echo $row['unit'] ? $row['unit'] : '';?></td>
						<td><?php echo $row['city'];?></td>
						<td><?php echo $row['state'];?></td>
						<td><?php echo $row['zip'];?></td>
						<td><?php echo $row['b_names'];?></td>
						<td>$<?php echo number_format($row['loan_amount'],2);?></td>
						<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>#accured_charges">$<?php echo number_format($row['accured_amount'],2);?></a></td>
						<td>$<?php echo number_format($row['paid_amountsss'],2);?></td>
						<td>$<?php echo number_format($row['accured_amount'] - $row['paid_amountsss'],2);?></td>
															
					</tr>
				<?php } ?>
				</tbody>
				<tfoot>
						<tr>
							<th colspan="6">Total:</th>
							<th>$<?php echo number_format($loanTotal,2);?></th>
							<th>$<?php echo number_format($accuredAmount,2);?></th>
							<th></th>
							<th></th>
						</tr>
				</tfoot>

			</table>

			

	</div>

	</div>

</div>

<script>

$(document).ready(function() {
    $('#table').DataTable();
});
</script>
