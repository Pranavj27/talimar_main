<?php
error_reporting(0);
// $investor_type_option = $this->config->item('investor_type_option');
// $loan_type_option = $this->config->item('loan_type_option');
// $property_type_option = $this->config->item('property_modal_property_type');
// $loan_status_option = $this->config->item('loan_status_option');
// $yes_no_option 		= $this->config->item('yes_no_option');
$transaction_type_option = array(
										'' => 'Select All',
										1 	=> 'Purchase',
										2 	=> 'Refinance',
										3 	=> 'Cash Out Refinance'
										
									);

$loan_status_option=array(
						""=> 'Select All',
						1 => 'Pipeline',
						2 => 'Active',
						3 => 'Paid off',
						5 => 'Brokered'	

 						);		
// echo "<pre>";

// print_r($status_loan);

// echo "</pre>";

?>
<!--<style>
table#table th {
    text-align: left;
    font-size: 12px; 
	
}
</style>-->
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Lender Rate Confirmation</h1>	
					
				</div>
				<div class="talimar_no_dropdowns">
					
					<form method="POST" action="<?php echo base_url();?>new_report_pdf">
						<input type="hidden" name="loan_status" value="<?php echo isset($loan_status) ? $loan_status : ''; ?>">
						<input type="hidden" name="loan_type" value="<?php echo isset($loan_type) ? $loan_type : ''; ?>"> 
						<input type="hidden" name="transaction_type" value="<?php echo isset($transaction_type) ? $transaction_type : ''; ?>">
						<button  type="submit" class="btn blue">PDF</button>

					</form>
					
				</div>
		</div>
		
		<div class="rc_class">
			<form id="multi_property" method="POST" action="<?php echo base_url();?>new_report">
			<div class="row">
				<div class="col-md-2">
				<label>Loan Status:</label>
					<select name="loan_status" class="form-control">
						<?php foreach($loan_status_option as $key => $row){?>
							<option value="<?php echo $key;?>" <?php if($key == $loan_status){ echo 'selected'; }?>><?php echo $row;?></option>
							<?php } ?>
					</select>
				</div>
				<div class="col-md-2">
				<label>Loan Type:</label>
					<select name="loan_type" class="form-control">
					    <option value="" <?php if(isset($loan_type)){if($loan_type == ''){echo 'Selected';}}?>>Select All</option>
						<option value="2" <?php if(isset($loan_type)){if($loan_type == '2'){echo 'Selected';}}?>>Construction (Existing)</option>
						<option value="3" <?php if(isset($loan_type)){if($loan_type == '3'){echo 'Selected';}}?>>Construction (New)</option>
						<option value="1" <?php if(isset($loan_type)){if($loan_type == '1'){echo 'Selected';}}?>>Bridge</option>
					</select>
				</div>

			<div class="col-md-2">
				<label>Transaction Type:</label>
					<select name="transaction_type" class="form-control">
						<?php foreach($transaction_type_option as $key => $row){?>
							<option value="<?php echo $key;?>" <?php if($key == $transaction_type){ echo 'selected'; }?>><?php echo $row;?></option>
							<?php } ?>
					</select>
				</div>

		<div class="col-md-2" style="margin-top:24px;">

			<button type="submit" class="btn blue">Filter</button>
				</div>


			</div>
			</form>
		</div>
		
		
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content" >
				<thead>
					<tr>
	
					
						<th>Borrower Name</th>
						<th>Property Street</th>
						<th>Unit #</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
						<th>Loan Amount</th>
						<th>Note Rate</th>
						<th>Lender Rate</th>
						<th># of Lenders</th>
						
					
					</tr>
				</thead>
				<tbody>
				<?php 
				
					$total_loan_amount 	= 0;
					$total_intrest 		= 0;
					$count 				= 0;
					$total_note 		= 0;
					$total_lender_rate 	= 0;
			
					if(isset($new_report_data))
					{
						foreach($new_report_data as $key => $row)
						{
							
							?>
							<tr>
			
								<td><?php echo $row['borrower_name']; ?></td>
								<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_address']; ?></a></td>
					            <td><?php echo $row['unit']; ?></td>
					            <td><?php echo $row['city']; ?></td>
								<td><?php echo $row['state']; ?></td>
								<td><?php echo $row['zip']; ?></td>
								<td>$<?php echo number_format($row['loan_amount']); ?></td>
								<td><?php echo ($row['intrest_rate']) ? number_format($row['intrest_rate'],3):'0.000';?>%</td>
								<td><?php echo ($row['lender_rate']) ? number_format($row['lender_rate'],3) :'0.000';?>%</td>
								<td><?php echo $row['hash_lender_count']; ?></td>
							</tr>
							
							
							<?php
							
							$total_loan_amount 	= $total_loan_amount + $row['loan_amount'];
							$total_note 		= $total_note + $row['intrest_rate'];
							$total_lender_rate	= $total_lender_rate + $row['lender_rate'];
							$count++;
							
						}
					}
				?>
					
				</tbody>	
				<tfoot>
					<tr>
						<th>Total: <?php echo $count; ?></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th><?php echo '$'.number_format($total_loan_amount);?></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
					
						<tr>
						<th>Average</th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th><?php echo '$'.number_format($total_loan_amount/$count);?></th>
						<th><?php echo number_format($total_note/$count,2);?>%</th>
						<th><?php echo number_format($total_lender_rate/$count,3);?>%</th>
						<th></th>
					</tr>
					
					
						
					
				</tfoot>
				
			</table>

			

		</div>

	</div>
	<!-- END CONTENT -->
</div>

<script>
$(document).ready(function() {
    $('#table').DataTable();
});

// function submit_status(that){
	
// 	$('form#').submit();
// }
</script>