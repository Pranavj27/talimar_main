<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
// echo "<pre>";
// print_r($fetch_active_data);
// echo "</pre>";

$select_box_name = isset($select_box) ? '('.$loan_status_option[$select_box].')' : '' ;
$select_box_name1 = isset($loan_type) ? '('.$loan_type_option[$loan_type].')' : '' ;
$select_box_name2 = isset($property_type) ? '('.$property_type_option[$property_type].')' : '' ;
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Loan Portfolio Overview</h1>	
				
				</div>
				
				<div class="top_download">
				<form method="POST" action="<?php echo base_url();?>prior_year_comparison_pdf">
				<!--<input type="hidden" name="select_box" value="<?php echo isset($select_box) ? $select_box : '';?>">
				<input type="hidden" name="loan_type" value="<?php echo isset($loan_type) ? $loan_type : '';?>">
				<input type="hidden" name="property_type" value="<?php echo isset($property_type) ? $property_type : '';?>">
				<input type="hidden" name="start_date" value="<?php echo $this->input->post('start_date');?>">
				<input type="hidden" name="end_date" value="<?php echo $this->input->post('end_date');?>">-->
				
				<button  type="submit" class="btn blue">PDF</button>
				</form>
				</div>
				
		</div>
		
		
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th ></th>
					<th style="width:170px;">YTD</th>
					<th style="width:170px;">2019</th>
					<th style="width:170px;">2018</th>
					<th style="width:170px;">2017</th>
					<th style="width:170px;">2016</th>
					<th style="width:170px;">2015</th>
				 </tr>
				</thead>
				<tbody class="scrollable">
				<?php 
				
				
					if(isset($final_array))
					{
						foreach($final_array as  $row)
						{
							
						 ?>
							<tr>
							  
								
								<td ># of Funded Loans:</td>
								<td ></td>
								<td><?php if($row['loan_count']!=''){ echo $row['loan_count'];}else{ echo "0";} ?></td>
								<td><?php if($row['loan_count_2018']!=''){ echo $row['loan_count_2018'];}else{ echo "0";} ?></td>
								<td><?php if($row['loan_count_2017']!=''){ echo $row['loan_count_2017'];}else{ echo "0";} ?></td>
								<td><?php if($row['loan_count_2016']!=''){ echo $row['loan_count_2016'];}else{ echo "0";} ?></td>
								<td><?php if($row['loan_count_2015']!=''){ echo $row['loan_count_2015'];}else{ echo "0";} ?></td>
								
								</tr>
								<tr>
								<td>$ of Funded Loans:</td>
								<td ></td>
								<td>$<?php echo number_format($row['count_loan_amount']); ?></td>
								<td>$<?php echo number_format($row['count_loan_amount_2018']); ?></td>
								<td>$<?php echo number_format($row['count_loan_amount_2017']); ?></td>
								<td>$<?php echo number_format($row['count_loan_amount_2016']); ?></td>
								<td>$<?php echo number_format($row['count_loan_amount_2015']); ?></td>
					</tr>
					
					
							<tr>
								<td>Average Loan Size:</td>
								<td ></td>
								<td>$<?php echo number_format($row['average_loan']); ?></td>
								<td>$<?php echo number_format($row['average_loan_2018']); ?></td>
								<td>$<?php echo number_format($row['average_loan_2017']); ?></td>
								<td>$<?php echo number_format($row['average_loan_2016']); ?></td>
								<td>$<?php echo number_format($row['average_loan_2015']); ?></td>
					</tr>
							<tr>
								<td>Average Lender Interest Rate:</td>
								<td ></td>
								<td><?php echo number_format($row['average_interest'],3); ?>%</td>
								<td><?php echo number_format($row['average_interest_2018'],3); ?>%</td>
								<td><?php echo number_format($row['average_interest_2017'],3); ?>%</td>
								<td><?php echo number_format($row['average_interest_2016'],3); ?>%</td>
								<td><?php echo number_format($row['average_interest_2015'],3); ?>%</td>
					</tr>
					
					<tr>
								<td>Largest Loan:</td>
								<td ></td>
								<td>$<?php echo number_format($row['highest_2019']); ?></td>
								<td>$<?php echo number_format($row['highest_2018']); ?></td>
								<td>$<?php echo number_format($row['highest_2017']); ?></td>
								<td>$<?php echo number_format($row['highest_2016']); ?></td>
								<td>$<?php echo number_format($row['highest_2015']); ?></td>

					</tr>
					<tr>
								<td>Smallest Loan:</td>
								<td ></td>
								<td>$<?php echo number_format($row['lowest_2019']); ?></td>
								<td>$<?php echo number_format($row['lowest_2018']); ?></td>
								<td>$<?php echo number_format($row['lowest_2017']); ?></td>
								<td>$<?php echo number_format($row['lowest_2016']); ?></td>
								<td>$<?php echo number_format($row['lowest_2015']); ?></td>
					</tr>

					<tr>
								<td>Number of Defaults:</td>
								<td ></td>
								<td><?php echo $row['default_2019']; ?></td>
								<td><?php echo $row['default_2018']; ?></td>
								<td><?php echo $row['default_2017']; ?></td>
								<td><?php echo $row['default_2016']; ?></td>
								<td><?php echo $row['default_2015']; ?></td>
					</tr>
			
				<?php } } ?>
				
				</tbody>	
				
			</table>
		</div>
	</div>
	
	
	
	
</div>
<script>
function form_peer_street()
{
	$("form#form_peer_street").submit();
}

$(document).ready(function() {
   // $('#table').DataTable();
	$('#table').DataTable({
 "aaSorting": []
	});
		
	});

	
	
	// var table = $('#table').DataTable();
// table
    // .column( '10:visible')
    // .order( 'asc' )
    // .draw();
	
	// $('th#click_col').click();

</script>
<style>
/*.float-direction-left {
    float: right !important;
}*/

/*.talimar_no_dropdowns div {
    float: right;
}*/
/*	.table,th,td{
width:150px;

	}*/
</style>