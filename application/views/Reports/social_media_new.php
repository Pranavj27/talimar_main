<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
			<div class="page-title">
				<h1> &nbsp; Loan Marketing Report</h1>
			</div>
			<div class="top_download">
				<form method="POST" id="social_media_frm" class="pdf_dv_loan_schedule" action="<?php echo base_url();?>download_social_media">						
					<input type="hidden" name="rsm" value="">
					<input type="hidden" name="signed_posted" value="">
					<input type="hidden" name="loan_closing_posted" value="">
					<input type="hidden" name="loan_status" value="">
					<input type="hidden" name="adv_img" value="">
					<input type="hidden" name="GoogleRequest" value="">
					<input type="hidden" name="LoanDescriptionComplete" value="">
					<input type="hidden" name="AdvertisingImageComplete" value="">				
				</form>
				<button type="submit" class="btn red" onclick="form_pdf_load()">PDF</button>
				<button type="button"  class="btn blue" onclick="FilterClick()" style="margin-top:14px;">Filter</button>
			</div>
		</div>
		<div class="row">			
			<div class="col-md-2">
				<label>Loan Status:</label>
				<select class="form-control" name="loan_status">
					<option value="">Select All</option>
					<?php 
					unset($loan_status_option[0]);
					unset($loan_status_option[4]);
					unset($loan_status_option[7]);
					foreach($loan_status_option as $key => $row){ ?>
						<option value="<?php echo $key;?>"><?php echo $row;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-2">
				<label>Google Request:</label>
				<select class="form-control" name="GoogleRequest">
					<?php foreach($template_yes_no_option as $key => $row){ ?>
						<option value="<?php echo $key;?>"><?php echo $row;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-2">
				<label>Advertising Image:</label>
				<select class="form-control" name="AdvertisingImageComplete">
					<?php foreach($template_yes_no_option as $key => $row){ ?>
						<option value="<?php echo $key;?>"><?php echo $row;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-2">
				<label>Loan Description:</label>
				<select class="form-control" name="LoanDescriptionComplete">
					<?php foreach($template_yes_no_option as $key => $row){ ?>
						<option value="<?php echo $key;?>"><?php echo $row;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-2">
				<label>Loan Closing Posted:</label>
				<select class="form-control" name="loan_closing_posted">
					<?php foreach($template_yes_no_option as $key => $row){ ?>
						<option value="<?php echo $key;?>"><?php echo $row;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-2">
				<label>Social Media Blast:</label>
				<select class="form-control" name="rsm">
					<?php foreach($template_yes_no_option as $key => $row){ ?>
						<option value="<?php echo $key;?>"><?php echo $row;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-2">
				<label>Sign Posted:</label>
				<select class="form-control" name="signed_posted">
					<?php foreach($template_yes_no_option as $key => $row){ ?>
						<option value="<?php echo $key;?>"><?php echo $row;?></option>
					<?php } ?>
				</select>
			</div>				
			<div class="col-md-2">
				<label>Advertising Complete:</label>
				<select class="form-control" name="adv_img">
					<?php foreach($template_yes_no_option as $key => $row){ ?>
						<option value="<?php echo $key;?>"><?php echo $row;?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<br>
		<br>
		<div class="rc_class">
			<div class="table-upper-section ajax_table_talimar table_All">
				<table id="social_media_tbl" class="table-responsive table table-bordered table-striped table-condensed flip-content dataTable no-footer">
					<!-- <table id="social_media_tbl" class="table table-bordered table-striped table-condensed flip-content th_text_align_center"> -->
				<thead>
					<tr>
						<th>Borrower Name</th>
						<th>Loan Amount </th>
						<th>Position</th>
						<th>Term </th>
						<th>Property Type</th>
						<th>Loan Type</th>
						<th>Property Address </th>
						<th>Unit</th>
						<th>City </th>
						<th>State </th>
						<th>Images<br>Created </th>
						<th>Closing<br>Posted</th>
						<th>Google<br>Request</th>
						<th>Sign<br>Posted</th>
						<th>Social Media<br> Complete</th>
					</tr>  
				</thead>
				<tbody></tbody>
				<tfoot>
					<tr>				
						<th>Total <span class="total_records lp_spin_load"></span></th>						
						<th>$<span class="total_loan_amount lp_spin_load"></span></th>
						<th colspan="13"></th>
					</tr>	
					<tr>				
						<th>Average</th>					
						<th>$<span class="total_loan_avarage lp_spin_load"></span></th>
						<th colspan="13"></th>
					</tr>					
				</tfoot>
			</table>
<script type="text/javascript">
	$(document).ready(function(){
		init_datatable_for_contact();
	});
	function init_datatable_for_contact(){
		var totalRecord=0;	
		var dispatching_list_all = '#social_media_tbl';
		$(dispatching_list_all).DataTable().clear().destroy();  
		if($( dispatching_list_all ).length){		
			$dis_list_all = $( dispatching_list_all ).DataTable({
				processing: true,
				serverSide: true,
				pageLength: 10,
				bFilter: true,
			    ajax: {
					dataSrc:"data",
					data:function(data){
						var loan_status 					= $('select[name="loan_status"]').val();
						var GoogleRequest	 				= $('select[name="GoogleRequest"]').val();
						var AdvertisingImageComplete 		= $('select[name="AdvertisingImageComplete"]').val();
						var LoanDescriptionComplete 		= $('select[name="LoanDescriptionComplete"]').val();
						var loan_closing_posted	 			= $('select[name="loan_closing_posted"]').val();
						var rsm 							= $('select[name="rsm"]').val();
						var signed_posted 					= $('select[name="signed_posted"]').val();
						var adv_img	 						= $('select[name="adv_img"]').val();
						var search_text         			= $('#social_media_tbl input[type="search"]').val();
						data.loan_status 					= loan_status;
						data.GoogleRequest 					= GoogleRequest;
						data.AdvertisingImageComplete 		= AdvertisingImageComplete;
						data.LoanDescriptionComplete 		= LoanDescriptionComplete;
						data.loan_closing_posted 			= loan_closing_posted;
						data.rsm 							= rsm;
						data.signed_posted 					= signed_posted;
						data.adv_img 						= adv_img;
						data.search_text 					= search_text;
						data.calling_type 					= 'table';
						$('.pdf_dv_loan_schedule input[name="loan_status"]').val(loan_status);
						$('.pdf_dv_loan_schedule input[name="GoogleRequest"]').val(GoogleRequest);					
						$('.pdf_dv_loan_schedule input[name="AdvertisingImageComplete"]').val(AdvertisingImageComplete);
						$('.pdf_dv_loan_schedule input[name="LoanDescriptionComplete"]').val(LoanDescriptionComplete);
						$('.pdf_dv_loan_schedule input[name="loan_closing_posted"]').val(loan_closing_posted);	
						$('.pdf_dv_loan_schedule input[name="rsm"]').val(rsm);					
						$('.pdf_dv_loan_schedule input[name="signed_posted"]').val(signed_posted);
						$('.pdf_dv_loan_schedule input[name="adv_img"]').val(adv_img);
					},
					url: '/Reports/ajax_social_media_reports',
					dataFilter: function(data){
						var json = JSON.parse( data );	
					    json.recordsTotal = json.totalNumRows;
					    json.recordsFiltered = json.totalNumRows;
					    totalRecord=json.totalNumRows;					    					   
					    json.data = json.data;
					    return JSON.stringify( json );
					}
			    },

			    columns: [				        
			        { data: 'borrower_name' },
					{ data: 'loan_amount' },
					{ data: 'position' },
					{ data: 'term' },
					{ data: 'property_type' },
					{ data: 'loan_type' },
					{ data: 'property_address' },
					{ data: 'unit' },
					{ data: 'city' },
					{ data: 'state' },
					{ data: 'images_created' },
					{ data: 'closing_posted' },
					{ data: 'google_request' },
					{ data: 'sign_posted' },
					{ data: 'social_media_complete' }
			    ],
			    "aoColumnDefs": [
			        { "bSortable": false, "aTargets": [10,11,12,13,14] }, //, 6, 7
			        { "bSearchable": false, "aTargets": [0, 1, 2, 3, 4, 6, 7,8,9,10,11,12,13,14 ] }
			    ],
			    order: [[ 0, "ASC" ]],
			    "language": { loadingRecords: '&nbsp;&nbsp;', processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
			    drawCallback: function( settings ) {	
			    	$('.lp_spin_load').html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:12px"></i>');
				   	total_datatable_for_contact(totalRecord);		    		    	
			    },
			    initComplete: function() {
			        $('#social_media_tbl input').unbind();
			        $('#social_media_tbl input').bind('blur change', function(e) {
			            $dis_list_all.search(this.value).draw();
			        });
			    }
			});
		}
	}
	function total_datatable_for_contact(totalRecords){		
		var loan_status 					= $('select[name="loan_status"]').val();
		var GoogleRequest	 				= $('select[name="GoogleRequest"]').val();
		var AdvertisingImageComplete 		= $('select[name="AdvertisingImageComplete"]').val();
		var LoanDescriptionComplete 		= $('select[name="LoanDescriptionComplete"]').val();
		var loan_closing_posted	 			= $('select[name="loan_closing_posted"]').val();
		var rsm 							= $('select[name="rsm"]').val();
		var signed_posted 					= $('select[name="signed_posted"]').val();
		var adv_img	 						= $('select[name="adv_img"]').val();
		var search_text         			= $('#social_media_tbl input[type="search"]').val();
		var data = {		
			'loan_status' 					: loan_status,
			'GoogleRequest' 				: GoogleRequest,
			'AdvertisingImageComplete' 		: AdvertisingImageComplete,
			'LoanDescriptionComplete' 		: LoanDescriptionComplete,
			'loan_closing_posted' 			: loan_closing_posted,
			'rsm' 							: rsm,
			'signed_posted' 				: signed_posted,
			'adv_img' 						: adv_img,
			'search_text' 					: search_text,
			'calling_type' 					: 'total_table',
			'start'                         : '',
			'length'                        : ''
		};
		req=$.ajax({
				type: "GET",
				dataType: 'json',
				url: "/Reports/ajax_social_media_reports",
				data:data,
				success: function(respones){
					$(".total_records").html(respones.totalNumRows);
					$('.total_loan_amount').html(respones.totalAmount);
					$('.total_loan_avarage').html(respones.totalAvarage);
				}
		});
	}
	function FilterClick()
	{
		init_datatable_for_contact();
	}
	function form_pdf_load(){
		var loan_status 					= $('select[name="loan_status"]').val();
		var GoogleRequest	 				= $('select[name="GoogleRequest"]').val();
		var AdvertisingImageComplete 		= $('select[name="AdvertisingImageComplete"]').val();
		var LoanDescriptionComplete 		= $('select[name="LoanDescriptionComplete"]').val();
		var loan_closing_posted	 			= $('select[name="loan_closing_posted"]').val();
		var rsm 							= $('select[name="rsm"]').val();
		var signed_posted 					= $('select[name="signed_posted"]').val();
		var adv_img	 						= $('select[name="adv_img"]').val();
		$('.pdf_dv_loan_schedule input[name="loan_status"]').val(loan_status);
		$('.pdf_dv_loan_schedule input[name="GoogleRequest"]').val(GoogleRequest);					
		$('.pdf_dv_loan_schedule input[name="AdvertisingImageComplete"]').val(AdvertisingImageComplete);
		$('.pdf_dv_loan_schedule input[name="LoanDescriptionComplete"]').val(LoanDescriptionComplete);
		$('.pdf_dv_loan_schedule input[name="loan_closing_posted"]').val(loan_closing_posted);	
		$('.pdf_dv_loan_schedule input[name="rsm"]').val(rsm);					
		$('.pdf_dv_loan_schedule input[name="signed_posted"]').val(signed_posted);
		$('.pdf_dv_loan_schedule input[name="adv_img"]').val(adv_img);
		$('#social_media_frm').submit();
	}
</script>
