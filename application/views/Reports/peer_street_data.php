<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
// echo "<pre>";
// print_r($fetch_active_data);
// echo "</pre>";

$select_box_name = isset($select_box) ? '('.$loan_status_option[$select_box].')' : '' ;
$select_box_name1 = isset($loan_type) ? '('.$loan_type_option[$loan_type].')' : '' ;
$select_box_name2 = isset($property_type) ? '('.$property_type_option[$property_type].')' : '' ;
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Investor Loan Schedule</h1>	
					
				</div>
				
				<div class="top_download">

				<form method="POST" action="<?php echo base_url();?>peer_street_csv">
				<input type="hidden" name="select_box" value="<?php echo isset($select_box) ? $select_box : '';?>">
				<input type="hidden" name="loan_type" value="<?php echo isset($loan_type) ? $loan_type : '';?>">
				<input type="hidden" name="property_type" value="<?php echo isset($property_type) ? $property_type : '';?>">
				<input type="hidden" name="start_date" value="<?php echo $start_date;?>">
				<input type="hidden" name="end_date" value="<?php echo $end_date;?>">
				<input type="hidden" name="position_opt" value="<?php echo $position_opt;?>">
				
				<button  type="submit" class="btn red" >CSV</button>
				</form>


				<form method="POST" action="<?php echo base_url();?>peer_street_pdf">
				<input type="hidden" name="select_box" value="<?php echo isset($select_box) ? $select_box : '';?>">
				<input type="hidden" name="loan_type" value="<?php echo isset($loan_type) ? $loan_type : '';?>">
				<input type="hidden" name="property_type" value="<?php echo isset($property_type) ? $property_type : '';?>">
				<input type="hidden" name="start_date" value="<?php echo $start_date;?>">
				<input type="hidden" name="end_date" value="<?php echo $end_date;?>">
				<input type="hidden" name="position_opt" value="<?php echo $position_opt;?>">
				
				<button  type="submit" class="btn red" >PDF</button>
				</form>
				</div>
				
		</div>
		
		<div class="row">
			<div class="talimar_no_dropdowns" style="padding-left:0px!important">
			
				<form method="POST" action="<?php echo base_url().'peer_street_schedule';?>" id="form_peer_street" onsubmit="return check()">
			
				<div class="col-md-2">
					Loan Status: 
					<select name="select_box" onchange="form_peer_street()">
						<?php 
							foreach ($loan_status_option as $key => $value) {
									
								?>	
								<option value="<?php echo $key;?>" <?php echo ($select_box == $key) ? 'Selected':'';?>><?php echo $value;?></option>

						<?php }?>
					</select>
				</div>
				<div class="col-md-2">
					Loan Type: 
					<select name="loan_type" onchange="form_peer_street()">
						<?php 
							foreach ($loan_type_option as $key => $value) {
									
								?>	
								<option value="<?php echo $key;?>" <?php echo ($loan_type == $key) ? 'Selected':'';?>><?php echo $value;?></option>

						<?php }?>
					</select>
				</div>
				<div class="col-md-2">
					Property Type: 
					<select name="property_type" onchange="form_peer_street()">
						<?php 
							foreach ($property_type_option as $key => $value) {
									
								?>	
								<option value="<?php echo $key;?>" <?php echo ($property_type == $key) ? 'Selected':'';?>><?php echo $value;?></option>

						<?php }?>
						
					</select>
				</div>
				<div class="col-md-1">
					Position: 
					<select name="position_opt" onchange="form_peer_street()">
						<?php 
							foreach ($position as $key => $value) {
									
								?>	
								<option value="<?php echo $key;?>" <?php echo ($position_opt == $key) ? 'Selected':'';?>><?php echo $value;?></option>

						<?php }?>
						
					</select>
				</div>
			
				<div class="col-md-2">
			
					From: <input type="text" autocomplete="off" id="start" name="start_date" class="form-control datepicker123" value="<?php echo isset($start_date) ? $start_date : ''; ?>" placeholder="Start Date">
				</div>	
					<div class="col-md-2">
				To: <input type="text" autocomplete="off" id="end"  name="end_date" class="form-control datepicker123" value="<?php echo isset($end_date) ? $end_date :''; ?>"  placeholder="End Date">
				</div>

				<br>
			
				<div class="col-md-1">
						<button  type="submit" name="submit"  class="btn btn-sm btn-primary" id="filt">Filter</button>
					</div>
				</form>
			
			</div>
		</div>
		
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Loan #</th>
					<th>City</th>
					<th>State</th>
					<th>Loan<br>Amount</th>
					<th>Position</th>
					<th>LTV <br>Ratio</th>
					<th>Lender<br>Rate</th>
					<th>Loan Type</th>
					<th>Property Type</th>
					<th>Term</th>
					<th>Closing<br>Date</th>
					<th>Loan<br>Status</th>
					<!--<th>Paid Off<br>Date</th>-->
				
					<!--<th>Condition</th>-->
					
					</tr>
				</thead>
				<tbody class="scrollable">
				<?php 
				
				$total_loan_amount 	= 0;
				$total_term 		= 0;
				$total_ltv 			= 0;
				$total_intrest 		= 0;
				$count 				= 0;
					if(isset($peer_data))
					{
						foreach($peer_data as $key => $row)
						{
							
									if($row['servicing_lender_rate'] == 'NaN' || $row['servicing_lender_rate'] == '' || $row['servicing_lender_rate'] == '0.00'){
									 	$lender_ratessss =$row['invester_yield_percent'];
									}else{

								   		$lender_ratessss = $row['servicing_lender_rate'];
									}

							 
							 		$ltv = is_infinite($row['ltv']) ? 0.00 : $row['ltv'];
							  
							?>
							<tr>
							
								<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['talimar_loan']; ?></a></td>
								
								
								<td><?php echo $row['city']; ?></td>
								<td><?php echo $row['state']; ?></td>
								
								<td>$<?php echo number_format($row['loan_amount']); ?></td>
								<td><?php echo $position[$row['position']]; ?></td>
								<td><?php echo number_format($ltv,2); ?>%</td>
								<td><?php echo number_format($lender_ratessss,3); ?>%</td>
								<td><?php echo $loan_type_option[$row['loan_type']]; ?></td>
								<td><?php echo $property_type_option[$row['property_type']]; ?></td>
								<td><?php echo $row['term']; ?> months</td>
								<td><?php echo $row['funding_date'] ? date('m-d-Y',strtotime($row['funding_date'])) : ''; ?></td>
								<td><?php echo $loan_status_option[$row['loan_status']]; ?></td>
								<!--<td><?php echo $row['payoff_date']; ?></td>-->
				
								<!--<td><?php echo $row['condition']; ?></td>-->
							</tr>
							
							
							<?php
							
							$total_intrest 		= $total_intrest + $lender_ratessss;
							$total_loan_amount 	= $total_loan_amount + $row['loan_amount'];
							$total_term 		= $total_term + $row['term'];
							$total_ltv 			= $total_ltv + $ltv;
							$count				= $key + 1;
							
						}
					}
				?>
					
				</tbody>	
				<tfoot>
					<tr>
						<th>Total:<?php echo $count; ?></th>
						<th></th>
						<th></th>
						
						<th><?php echo '$'.number_format($total_loan_amount);?></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
					
					<tr>
						<th>Avg:</th>
						<th></th>
						<th></th>
						
						<th><?php echo '$'.number_format($total_loan_amount/$count); ?></th>
						<th></th>
						<th><?php echo number_format($total_ltv/$count,2).'%'; ?></th>
						<th><?php echo number_format($total_intrest/$count,2).'%'; ?></th>
						<th></th>
						<th></th>
						<th><?php echo number_format($total_term/$count); ?> months</th>
						<th ></th>
						<th></th> 
					
					</tr>
				</tfoot>
				
			</table>
		</div>
	</div>
	
	
	
	
</div>
<script>
function check()
{
	var start=$('#start').val();
	var end=$('#end').val();

 if(start > end){

 alert('From date must be less then To date');	
 return false;
 }else{

 return true;	
 }

}

$(document).ready(function() {
   // $('#table').DataTable();
	$('#table').DataTable({
 		"aaSorting": [6,'desc']
	});
		
	});

	
	
	// var table = $('#table').DataTable();
// table
    // .column( '10:visible')
    // .order( 'asc' )
    // .draw();
	
	// $('th#click_col').click();

</script>
<style>
.float-direction-left {
    float: left !important;
}

.talimar_no_dropdowns {
    float: right;
    padding: 20px 66px;
}
div#table_length {

    margin-left: -14px!important;
}
</style>