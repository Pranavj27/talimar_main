<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');

$loan_status_filter = array(
								'' => 'Select All',
								1  => 'Pipeline',
								2  => 'Active',
								3  => 'Paid Off',
							);

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Loan Closing Checklist</h1>	
					
				</div>
				
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>Reports/download_LoanClosingChecklist">
						<button type="submit" class="btn blue">PDF</button>
					</form>
				</div>
				
		</div>
		
		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Street Address</th>
					<th>Unit</th>
					<th>City</th>
					<th>State</th>
					<th>Zip</th>
					<th>Loan Amount</th>
					<th>Closing Date</th>
					<th>Underwriting </th>
					<th>Investor<br>Relations</th>
					<th>Loan Closing</th>
					<th>Post Loan<br>Closing</th>
					<th>Loan Servicing</th>					
				</tr>
				</thead>
				<tbody>
				
				<?php if(isset($LoanClosingChecklist) && is_array($LoanClosingChecklist)){ 
						foreach($LoanClosingChecklist as $row){ 
						
								if($row['value'] != '1'){ ?>
						
									<tr>
										<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['property_address'];?></a></td>
										<td><?php echo $row['unit'];?></td>
										<td><?php echo $row['city'];?></td>
										<td><?php echo $row['state'];?></td>
										<td><?php echo $row['zip'];?></td>
										<td>$<?php echo number_format($row['loan_amount'],2);?></td>
										<td><?php echo $row['date'];?></td>
										<td><?php echo $row['underwriting'];?></td>
										<td><?php echo $row['investor'];?></td>
										<td><?php echo $row['clouser'];?></td>
										<td><?php echo $row['post_clouser'];?></td>
										<td><?php echo $row['serviving'];?></td>
										
									</tr>
				
								<?php } } }else{ ?>
				
					<tr>
						<td style="width:100%;">No data found!</td>
					</tr>
				<?php } ?>
					
				</tbody>		
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {
	 
    $('#table').DataTable();
	
});



</script>




