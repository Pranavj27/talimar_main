<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');
$not_applicable_option = $this->config->item('not_applicable_option');

$loan_status_filter = array(
								'' => 'Select All',
								1  => 'Pipeline',
								2  => 'Active',
								3  => 'Paid Off',
							);

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Payment Reserve Report</h1>	
					
				</div>
				
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>Reports/PaymentReserveReport_pdf">
					
						<input type="hidden" name="loan_status" value="<?php echo isset($loan_status) ? $loan_status : '';?>">
						<input type="hidden" name="payment_reserve" value="<?php echo isset($payment_reserve) ? $payment_reserve : '';?>">
						<button type="submit" class="btn blue">PDF</button>
						
					</form>
				</div>
				
		</div>
		
		<div class="row">
		<form method="post" action="<?php echo base_url();?>Reports/PaymentReserveReport">
			<div class="col-md-2">
				<label><b>Loan status:</b></label>
				<select name="loan_status" class="form-control">
					<option value="">Select All</option>
					<option value="1" <?php if($loan_status == 1){echo 'selected';}?>>Pipeline</option>
					<option value="2" <?php if($loan_status == 2){echo 'selected';}?>>Active</option>
					<option value="3" <?php if($loan_status == 3){echo 'selected';}?>>Paid Off</option>
				</select>
			</div>
			
			<div class="col-md-2">
				<label><b>Payment Reserve:</b></label>
				<select name="payment_reserve" class="form-control">
					<option value="">Select All</option>
					<option value="1" <?php if($payment_reserve == 1){echo 'selected';}?>>Yes</option>
					<option value="2" <?php if($payment_reserve == 2){echo 'selected';}?>>No</option>
				</select>
			</div>
			
			<div class="col-md-2">
				<button type="submit" name="submit" class="btn btn-sm btn-primary" style="margin-top:25px;">Filter</button>
			</div>
		</form>
		</div>
		
		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Street Address</th>
					<th>Unit #</th>
					<th>Borrower Name</th>
					<th>Contact Name</th>
					<th>Loan<br>Amount</th>
					<th>Monthly <br>Payment</th>
					<th>Payment <br>Reserve </th>
					<th># of <br>Months</th>
					<th>Closing Date</th>
					<th>First<br>Payment<br>Month</th>
					
										
				</tr>
				</thead>
				<tbody>
					<?php if(isset($PaymentReserveReport) && is_array($PaymentReserveReport)){ 
						foreach($PaymentReserveReport as $row){ 
						
							$monthadd = $row['extention_month'];
							if($monthadd == 'N/A'){ $monthadd = 0;}
							$daysdefault = '30';
							$plusadddays = $daysdefault * $monthadd;
							$firstPaymentdate = date('m / Y', strtotime('+'.$plusadddays.' days'. $row['loan_funding_date']));								
							if($row['payment_reserve_opt'] == 'N/A' ){
								$payment_reserve_opt = 'No';
							}elseif($row['payment_reserve_opt'] != '' ){
								$payment_reserve_opt = $not_applicable_option[$row['payment_reserve_opt']];
							}else{
								$payment_reserve_opt = 'Yes';
							}
							if($row['extention_month'] == ''){
								
								$extention_month = 'No';
							}else{
								$extention_month = $row['extention_month'];
							}

							$explode = explode('/', $firstPaymentdate);
							
					?>
						
							<tr>
								<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['property_address'];?></a></td>
								<td><?php echo $row['unit']; ?></td>
								<td><a href="<?php echo base_url();?>borrower_view/<?php echo $row['borrower'];?>"><?php echo $row['b_name'];?></a></td>
								<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['cname'];?></a></td>
								<td>$<?php echo number_format($row['loan_amount'],2);?></td>
								<td>$<?php echo number_format($row['MonthlyPayment'],2);?></td>
								<td><?php echo $row['totalpayment'];?></td>
								<td><?php echo $extention_month;?></td>
								<td><?php echo date('m-d-Y', strtotime($row['loan_funding_date']));?></td>
								<td><?php echo $explode[1].'/'.$explode[0];?></td>														
							</tr>
				
					<?php } }else{ ?>
				
					<tr>
						<td style="width:100%;" colspan="10">No data found!</td>
					</tr>
				<?php } ?>
				</tbody>		
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {
	 
    $('#table').DataTable({
        
    });
	
});
</script>