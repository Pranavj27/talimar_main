<?php
$contact_record_limit = $this->config->item('contact_record_limit');
$yes_no_option3 = $this->config->item('yes_no_option3');

?>
<style type="text/css">
		input.btn.blue.aa {
		  
		    margin-top: 14px;
		    border-radius: 0px;
		   
		}
		.talimar_no_dropdowns div {

		    padding: 0px 2px;
		    font-size: 14px!important;
		}

		.talimar_no_dropdowns {
		    padding-left: 0px!important;
		}
		#table_wrapper>.row>.col-md-6.col-sm-12 {
		    
		    padding-left: 0px;
		}
		.form-control[disabled] {
		    cursor: not-allowed;
		    background-color: #dddddd;
		}
	</style>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1> Lender Filter</h1>
			
			</div>
			<div class="top_download">
					
			    <form method="POST" action="<?php echo base_url();?>ReportData/lenderFilterNew">

			    	<input type="hidden" name="reportDisplay" value="pdf">
			    	<!-- <input type="hidden" name="record_limit" value="<?php echo isset($record_limit) ? $record_limit : '';?>">
			    	<input type="hidden" name="trust_deed" value="<?php echo isset($trust_deed) ? $trust_deed : '';?>"> -->
			    	<input type="hidden" name="ActiveAccount" value="<?php echo isset($ActiveAccount) ? $ActiveAccount : '';?>">
			    	<input type="hidden" name="FundedTrustDeed" value="<?php echo isset($FundedTrustDeed) ? $FundedTrustDeed : '';?>">
			    	<input type="hidden" name="ActiveTrustDeed" value="<?php echo isset($ActiveTrustDeed) ? $ActiveTrustDeed : '';?>">
			    	<input type="hidden" name="WholeInvestor" value="<?php echo isset($WholeInvestor) ? $WholeInvestor : '';?>">
					<button  class="btn red aa" type="submit">PDF</button> 
				</form>
					
			    <form method="POST" action="<?php echo base_url();?>ReportData/lenderFilterNew">

			    	<input type="hidden" name="reportDisplay" value="excel">
			    	<!-- <input type="hidden" name="record_limit" value="<?php echo isset($record_limit) ? $record_limit : '';?>">
			    	<input type="hidden" name="trust_deed" value="<?php echo isset($trust_deed) ? $trust_deed : '';?>"> -->
			    	<input type="hidden" name="ActiveAccount" value="<?php echo isset($ActiveAccount) ? $ActiveAccount : '';?>">
			    	<input type="hidden" name="FundedTrustDeed" value="<?php echo isset($FundedTrustDeed) ? $FundedTrustDeed : '';?>">
			    	<input type="hidden" name="ActiveTrustDeed" value="<?php echo isset($ActiveTrustDeed) ? $ActiveTrustDeed : '';?>">
			    	<input type="hidden" name="WholeInvestor" value="<?php echo isset($WholeInvestor) ? $WholeInvestor : '';?>">
					<button  class="btn green dsds" type="submit">Excel</button> 
				</form>	
				
			</div>			
		</div>
		
		<div class="row">
			<form method="post" action="<?php echo base_url();?>ReportData/lenderFilterNew">


				<div class="col-md-2">
					<label>Account Status :</label>
					<select name="ActiveAccount" class="form-control">
						<option value="">Select All</option>
							<?php foreach($account_status_option as $key => $row){
									if($row!='Select One'){
							?>
									<option value="<?= $key;?>" <?php if($ActiveAccount == $key){echo 'selected';}?>><?= $row;?></option>
							<?php } } ?>
					</select>
				</div>

				<div class="col-md-2">
					<label>Funded Trust Deed:</label>
					<select name="FundedTrustDeed" class="form-control">
						<option value="">Select All</option>
						<?php
							foreach($yes_no_option3 as $key_option => $row_option)
							{
								if($key_option)
								{
									$selected = (isset($FundedTrustDeed) && $FundedTrustDeed == $key_option) ? 'selected' : '';
									echo '<option value="'.$key_option.'" '.$selected.' >'.$row_option.'</option>';
								}
							}
						?>
					</select>
				</div>

				<div class="col-md-2">
					<label>Active Trust Deed:</label>
					<select name="ActiveTrustDeed" class="form-control">
						<option value="">Select All</option>
						<?php
							foreach($yes_no_option3 as $key_option => $row_option)
							{
								if($key_option)
								{
									$selected = (isset($ActiveTrustDeed) && $ActiveTrustDeed == $key_option) ? 'selected' : '';
									echo '<option value="'.$key_option.'" '.$selected.' >'.$row_option.'</option>';
								}
							}
						?>
					</select>
				</div>

				<div class="col-md-2">
					<label>Whole Note Investor:</label>
					<select name="WholeInvestor" class="form-control">
						<option value="">Select All</option>
						<?php
							foreach($yes_no_option3 as $key_option => $row_option)
							{
								if($key_option)
								{
									$selected = (isset($WholeInvestor) && $WholeInvestor == $key_option) ? 'selected' : '';
									echo '<option value="'.$key_option.'" '.$selected.' >'.$row_option.'</option>';
								}
							}
						?>
					</select>
				</div>

				<div class="col-md-2">
					<label>Lender Portal:</label>
					<select name="lender_portal" class="form-control">
						<option value="">Select All</option>
						<?php
							foreach($yes_no_option3 as $key_option => $row_option)
							{
								if($key_option)
								{
									$selected = (isset($lender_portal) && $lender_portal == $key_option) ? 'selected' : '';
									echo '<option value="'.$key_option.'" '.$selected.' >'.$row_option.'</option>';
								}
							}
						?>
					</select>
				</div>

				<div class="col-md-2">
					<button type="submit" name="submit" class="btn btn-primary btn-sm" style="margin-top:25px;">Filter</button>
				</div>
			</form>
		</div>
		<div class="row">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content">
				<thead>
					<tr>
						<th style="white-space:nowrap;">Name</th>
					    <th># of<br>Accounts</th>
						<th># of<br>Total<br>Loans</th>
						<th>$ of<br>Total<br>Loans</th>
						<th># of<br>Active<br>Loans</th>
						<th>$ of<br>Active<br>Loans</th>
						<th>Committed<br>Capital</th>
						<th>Available<br>Capital</th>			
						<th>Date Added</th>		
						<th>Phone</th>			
						<th>E-Mail</th>			
						
					</tr>
				</thead>
				<tbody>

				<?php 

				$total=0;
				$total_account=0;
				$hash_total_loan=0;
				$amount_total_loan=0;
				$hash_active_loan=0;
				$amount_active_loan=0;
				$commited=0;
				$avaliable=0;

				if(isset($lenderFilterData) && is_array($lenderFilterData)){ 
					foreach($lenderFilterData as $row){ 
			
                        $contact = $row['contact_id'];
				?>
					<tr>
						<td style="white-space:nowrap;">
							<a href="<?php echo base_url("viewcontact/$contact");?>"><?php echo $row['contact_firstname'].' '.$row['contact_lastname'];?></a>
						</td>
						<td><?php echo $row['contact_accounts'];?></td>
						<td><?php echo number_format($row['countTotal']);?></td>
						<td>$<?php echo number_format($row['AmountTotal']);?></td>
						<td><?php echo number_format($row['countActive']);?></td>
						<td>$<?php echo number_format($row['AmountActive']);?></td>
						<td>$<?php echo number_format($row['contact_funds']);?></td>
						<td>$<?php echo number_format($row['contact_funds'] - $row['AmountActive']);?></td>
						<td><?php echo $row['contact_addedDate'];?></td>
						<td><?php echo $row['contact_phone'];?></td>
						<td><?php echo $row['contact_email'];?></td>
					</tr>
					<?php 

					$total++;
					$total_account += $row['contact_accounts'];
					$hash_total_loan += $row['countTotal'];
					$amount_total_loan += $row['AmountTotal'];
					$hash_active_loan += $row['countActive'];
					$amount_active_loan += $row['AmountActive'];
					$commited += $row['contact_funds'];

				} }else{ ?>

					<tr>
						<td colspan="11">No data found!</td>
					</tr>

				<?php } ?>
				</tbody>
				<tfoot>
						<tr>
							<th>Total: <?php echo $total;?></th>
							<th><?php echo number_format($total_account);?></th>
							<th><?php echo number_format($hash_total_loan);?></th>
							<th>$<?php echo number_format($amount_total_loan); ?></th>
							<th><?php echo $hash_active_loan; ?></th>
							<th>$<?php echo number_format($amount_active_loan); ?></th>
							<th>$<?php echo number_format($commited);?></th>
							<th>$<?php echo number_format($commited - $amount_active_loan);?></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
				</tfoot>
			</table>
		</div>
		<?php
		/*
		<div class="row">
			<div class="table-upper-section ajax_table_talimar table_All">
				<table id="existing_loan_schedule_tab" class="table-responsive table table-bordered table-striped table-condensed flip-content dataTable no-footer">
					<thead>
						<tr>
							<th>Name</th>
							<th># of<br>Accounts</th>
							<th># of<br>Total<br>Loans</th>
							<th>$ of<br>Total<br>Loans</th>
							<th># of<br>Active<br>Loans</th>
							<th>$ of<br>Active<br>Loans</th>
							<th>Committed<br>Capital</th>
							<th>Available<br>Capital</th>
							<th>Date Added</th>		
							<th>Phone</th>			
							<th>E-Mail</th>	
						</tr>
					</thead>
					<tbody></tbody>
					<tfoot>					
						<tr>
							<th>Total: <span class="total_contact_name"></span></th>
							<th><span class="total_account_no"></span></th>
							<th><span class="total_loan_count"></span></th>
							<th>$<span class="total_loan_amount"></span></th>
							<th><span class="active_loan_count"></span></th>
							<th>$<span class="active_loan_amount"></span></th>
							<th>$<span class="commited_capital_amount"></span></th>
							<th>$<span class="avilable_capital_amount"></span></th>
							<th></th>
							<th></th>
							<th></th>			
						</tr>	
					</tfoot>
				</table>
			</div>
		</div>
		*/
		?>	
	</div>
	<!-- END CONTENT -->
</div>
<script>

$(document).ready(function() {
  
	$('#table').DataTable();
});


</script>


	


