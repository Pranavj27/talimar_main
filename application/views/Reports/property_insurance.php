<?php
error_reporting(0);
// echo "<pre>";
// print_r($fetch_active_data);
// echo "</pre>";
$yes_no_option2 	= $this->config->item('yes_no_option2');
$loan_type_option 	= $this->config->item('loan_type_option');
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Property Insurance Report</h1>	
					
				</div>
				<div class="top_download">
				<form action="<?php echo base_url();?>download_property_insurance" method="post">
					<input type="hidden" name="policy" value="<?php echo $fetch_policy ? $fetch_policy : '';?>">
					<button type="submit" class="btn blue">PDF</button>
				</form>
				</div>
		</div>
		<div class="row page-body">
			<form id="policy_select" action="<?php echo base_url();?>property_insurance" method="post">
				<div class="col-md-3">
				DTE:
					<select class="form-control" name="policy" onchange="select_policy(this.value);">
						<option value="">Select All</option>
						
						<option value="2" <?php if($fetch_policy =='2'){ echo 'selected';}?>>Current</option>
						<option value="1"<?php if($fetch_policy =='1'){ echo 'selected';}?>>Expiring Soon</option>
						<option value="3" <?php if($fetch_policy =='3'){ echo 'selected';}?>>Expired</option>
						<option value="4" <?php if($fetch_policy =='4'){ echo 'selected';}?>>Cancelled</option>
						
					</select>
				</div>
			</form>
		</div>
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<!-- <th>Loan #</th> -->
					<th>Borrower Name</th>
					<th>Contact Name</th>
					<th>Phone</th>
					<th>E-mail</th>
					<th>Street Address</th>
					<th>Unit#</th>
					<th>City</th>
					<th>State</th>
					<th>Zip</th>
					<!--<th>Unit # </th>
					<th>City</th>
					<th>State</th>
					<th>Zip</th>
					<th width="20%">Insurance Carrier Name</th>-->
					<th>Policy #</th>
					<th>Expiration Date</th>
					<!-- <th>Contact Name</th> -->
					<!--<th>Contact Phone</th>
					<th>Contact Email</th>
					<th>Request<br>Update</th>-->
					<th>DTE</th>
					<th># of Days</th>
					
				</tr>
				</thead>
				<tbody class="scrollable">
					<?php
				
					if(isset($fetch_property_insurance_loan))
					{
						foreach($fetch_property_insurance_loan as $key => $row)
						{
							
							$now = time(); // or your date as well
							$your_date = strtotime($row['insurance_expiration']);
							$datediff = $your_date - $now;
							$duration = floor($datediff / (60 * 60 * 24));
							
							if($duration < 0){
								$addstyle = 'style="color:red;"';
							}else{
								$addstyle =  '';
							}
							
							
							if($fetch_policy == '4'){
								
								if($row['request_updated_insurance'] == '1'){
								
									$updated_insurance = 'Cancelled';
								?>
									
										<tr>
											<!-- <td><?php echo $row['talimar_loan']; ?></td> -->
											<td><?php echo $row['borrower'][0]->b_name; ?></td>
											<td><?php echo $row['borrowercontactname']; ?></td>
											<td><?php echo $row['borrowercontactphone']; ?></td>
											<td><?php echo $row['borrowercontactemail']; ?></td>
											
											<td><a href="<?php echo base_url().'load_data/'.$row['loan_id']; ?>"><?php echo $row['street_address']; ?></a></td>
											<td><?php echo $row['unit']; ?></td>
											<td><?php echo $row['city']; ?></td>
											<td><?php echo $row['state']; ?></td>
											<td><?php echo $row['zip']; ?></td>
											
											
											<!--<td><?php echo $row['unit']; ?></td>
											<td><?php echo $row['city']; ?></td>
											<td><?php echo $row['state']; ?></td>
											<td><?php echo $row['zip']; ?></td>-->
											
											<td><?php echo $row['policy_number']; ?></td>
											<td><?php echo date('m-d-Y', strtotime($row['insurance_expiration'])); ?></td>
											<!-- <td><?php echo $row['contact_name']; ?></td> -->
											
											<!--<td><?php echo $row['contact_number']; ?></td>
											<td><?php echo $row['contact_email']; ?></td>-->
											<td><?php echo $updated_insurance; ?></td>
											<td <?php echo $addstyle;?>><?php echo $duration; ?></td>
											
											
										</tr>
												
								<?php	
							}
							
					  }
						elseif($fetch_policy == '3'){
							
							if($row['request_updated_insurance'] != '1'){
								//$current_date = date('m-d-Y');
								 $current_date=date('Y-m-d');
							     //$currentt= date("Y-m-d", strtotime($current_date));

								
								
								if(date('Y-m-d', strtotime($row['insurance_expiration'])) < $current_date){
                              
									$updated_insurance = 'Expired';
							?>
									
										<tr>
											<!-- <td><a href="<?php echo base_url().'load_data/'.$row['loan_id']; ?>"><?php echo $row['talimar_loan']; ?></td> -->
											<td><?php echo $row['borrower'][0]->b_name; ?></td>
											<td><?php echo $row['borrowercontactname']; ?></td>
											<td><?php echo $row['borrowercontactphone']; ?></td>
											<td><?php echo $row['borrowercontactemail']; ?></td>
											<td><a href="<?php echo base_url().'load_data/'.$row['loan_id']; ?>"><?php echo $row['street_address']; ?></a></td>
											<td><?php echo $row['unit']; ?></td>
											<td><?php echo $row['city']; ?></td>
											<td><?php echo $row['state']; ?></td>
											<td><?php echo $row['zip']; ?></td>
											
											
											<!--<td><?php echo $row['unit']; ?></td>
											<td><?php echo $row['city']; ?></td>
											<td><?php echo $row['state']; ?></td>
											<td><?php echo $row['zip']; ?></td>-->
											
											<td><?php echo $row['policy_number']; ?></td>
											<td><?php echo date('m-d-Y', strtotime($row['insurance_expiration'])); ?></td>
											<!-- <td><?php echo $row['contact_name']; ?></td> -->
											
											<!--<td><?php echo $row['contact_number']; ?></td>
											<td><?php echo $row['contact_email']; ?></td>-->
											<td><?php echo $updated_insurance; ?></td>
											<td <?php echo $addstyle;?>><?php echo $duration; ?></td>
											
										</tr>
								
								
							<?php	}
							}
						}
						
						 		elseif($fetch_policy == '2'){
							
							if($row['request_updated_insurance'] != '1'){
								//$current_date = date('m-d-Y');
								 $current_date=date('Y-m-d');
							     $currentt= date("Y-m-d", strtotime($current_date . ' +45 days'));
                                  
								
								
								if(date('Y-m-d', strtotime($row['insurance_expiration'])) > $currentt){
                            
									$updated_insurance = 'Current';
							?>
									
										<tr>
											<!-- <td><?php echo $row['talimar_loan']; ?></td> -->
											<td><?php echo $row['borrower'][0]->b_name; ?></td>
											<td><?php echo $row['borrowercontactname']; ?></td>
											<td><?php echo $row['borrowercontactphone']; ?></td>
											<td><?php echo $row['borrowercontactemail']; ?></td>
											<td><a href="<?php echo base_url().'load_data/'.$row['loan_id']; ?>"><?php echo $row['street_address']; ?></a></td>
											<td><?php echo $row['unit']; ?></td>
											<td><?php echo $row['city']; ?></td>
											<td><?php echo $row['state']; ?></td>
											<td><?php echo $row['zip']; ?></td>
											
											
											<!--<td><?php echo $row['unit']; ?></td>
											<td><?php echo $row['city']; ?></td>
											<td><?php echo $row['state']; ?></td>
											<td><?php echo $row['zip']; ?></td>-->
											
											<td><?php echo $row['policy_number']; ?></td>
											<td><?php echo date('m-d-Y', strtotime($row['insurance_expiration'])); ?></td>
											<!-- <td><?php echo $row['contact_name']; ?></td> -->
											
											<!--<td><?php echo $row['contact_number']; ?></td>
											<td><?php echo $row['contact_email']; ?></td>-->
											<td><?php echo $updated_insurance; ?></td>
											<td <?php echo $addstyle;?>><?php echo $duration; ?></td>
											
										</tr>
								
								
							<?php	}
							}
						}
						
							elseif($fetch_policy == '1'){
								
							if($row['request_updated_insurance'] != '1'){
								
							   
							
                                 $current_date=date('Y-m-d');
							     $currentt= date("Y-m-d", strtotime($current_date . ' +45 days'));
								 
								 if(date('Y-m-d', strtotime($row['insurance_expiration'])) > $current_date){
								 
								 if(date('Y-m-d', strtotime($row['insurance_expiration'])) <= $currentt){
					
								$updated_insurance = 'Expiring Soon';
										
									 
							?>
									
										<tr>
											<!-- <td><?php echo $row['talimar_loan']; ?></td> -->
											<td><?php echo $row['borrower'][0]->b_name; ?></td>
											<td><?php echo $row['borrowercontactname']; ?></td>
											<td><?php echo $row['borrowercontactphone']; ?></td>
											<td><?php echo $row['borrowercontactemail']; ?></td>
											<td><a href="<?php echo base_url().'load_data/'.$row['loan_id']; ?>"><?php echo $row['street_address']; ?></a></td>
											<td><?php echo $row['unit']; ?></td>
											<td><?php echo $row['city']; ?></td>
											<td><?php echo $row['state']; ?></td>
											<td><?php echo $row['zip']; ?></td>
											
											
											<!--<td><?php echo $row['unit']; ?></td>
											<td><?php echo $row['city']; ?></td>
											<td><?php echo $row['state']; ?></td>
											<td><?php echo $row['zip']; ?></td>-->
											
											<td><?php echo $row['policy_number']; ?></td>
											<td><?php echo date('m-d-Y', strtotime($row['insurance_expiration'])); ?></td>
											<!-- <td><?php echo $row['contact_name']; ?></td> -->
											
											<!--<td><?php echo $row['contact_number']; ?></td>
											<td><?php echo $row['contact_email']; ?></td>-->
											<td><?php echo $updated_insurance; ?></td>
											<td <?php echo $addstyle;?>><?php echo $duration; ?></td>
											
										</tr>
								
								
								 <?php	}}
							}
						}
						
						
						else{
							
							if($fetch_policy == '')
							{
								
								if($row['request_updated_insurance'] == '1')
								{
								
									$updated_insurance = 'Cancelled';	
							
							
								} 
								 else
								 {
										 $updated_insurance='';
										 $current_date=date('Y-m-d');
										 $current= date("Y-m-d", strtotime($current_date));

									if(date('Y-m-d', strtotime($row['insurance_expiration'])) < $current)
									{

										$updated_insurance = 'Expired';
										
									 }  else{
										 
										 $updated_insurance = ''; 
												
						            
									 $currentt= date("Y-m-d", strtotime($current_date . ' +45 days'));

								 if(date('Y-m-d', strtotime($row['insurance_expiration'])) > $currentt){

										$updated_insurance = 'Current';
										
									 }else{
										$updated_insurance = ''; 
										
 
								
									
										$currentt= date("Y-m-d", strtotime($current_date . ' +45 days'));

								 if(date('Y-m-d', strtotime($row['insurance_expiration'])) <= $currentt){

										$updated_insurance = 'Expiring Soon';
										
									 }else{
										$updated_insurance = ''; 
										
										
									 }

										
								 }


										
							 }
									 
							
									
								
						
							}
							?>
							<tr>
								
								<!-- <td><?php echo $row['talimar_loan']; ?></td> -->
								<td><?php echo $row['borrower'][0]->b_name; ?></td>
								<td><?php echo $row['borrowercontactname']; ?></td>
								<td><?php echo $row['borrowercontactphone']; ?></td>
								<td><?php echo $row['borrowercontactemail']; ?></td>
								<td><a href="<?php echo base_url().'load_data/'.$row['loan_id']; ?>"><?php echo $row['street_address']; ?></a></td>
								<td><?php echo $row['unit']; ?></td>
								<td><?php echo $row['city']; ?></td>
								<td><?php echo $row['state']; ?></td>
								<td><?php echo $row['zip']; ?></td>
								
								
								<!--<td><?php echo $row['unit']; ?></td>
								<td><?php echo $row['city']; ?></td>
								<td><?php echo $row['state']; ?></td>
								<td><?php echo $row['zip']; ?></td>-->
								
								<td><?php echo $row['policy_number']; ?></td>
								<td><?php echo date('m-d-Y', strtotime($row['insurance_expiration'])); ?></td>
								<!-- <td><?php echo $row['contact_name']; ?></td> -->
								
								<!--<td><?php echo $row['contact_number']; ?></td>
								<td><?php echo $row['contact_email']; ?></td>-->
								<td><?php echo $updated_insurance; ?></td>
								<td <?php echo $addstyle;?>><?php echo $duration; ?></td>
								
							</tr>
							<?php
						 }
					   }
					}
						
					}
					?>
				</tbody>				
				<tfoot>
									
				</tfoot>
			</table>
		</div>
	</div>
	
	
	
	
</div>

<script>
$(document).ready(function() {
    $('#table').DataTable({
		 "aaSorting": [[ 2, "asc" ]]
		 //"sorting":false,
	});
    
} );

function select_policy(that){
	
	$('form#policy_select').submit();
}
</script>