<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Outstanding Draw Requests</h1>	
				</div>

				<div class="top_download">
					
					<a href="<?php echo base_url();?>outstanding_draw_report_pdf">

					<button  class="btn blue">PDF</button>

					</a>

				</div>

		</div>
		<div class="row">
			<div class="col-md-2">
				<label>Draw Status:</label>
				<select name="drwa_sttaus" class="form-control">
					<option value="0">Select All</option>
					<option value="1">Requested</option>
					<option value="2">Approved</option>
					<option value="3">Submitted</option>
				</select>
			</div>
		</div>

		<div class="rc_class re870_table">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >

				<thead>

					<tr>
						<th>Borrower Name</th>
						<th>Contact</th>
						<th>Contact Phone</th>
						<th>Contact E-Mail</th>
						<th>Property</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
						<th>Amount</th>
						<th>Date</th>
						
						<th>Requested</th>
						<th>Approved</th>
						<th>Submitted</th>
						<th>Draw Released</th>
					</tr>

				</thead>
				
				<tbody>
			
				<?php 
				 // echo '<pre>';	
				 // print_r($outstanding_draws);
				 // echo '</pre>';
					foreach($outstanding_draws as $row){ 

					?>

					<?php if($row['draws_amount_1'] !='0') { 
									

									if($row['date1_draw_request'] =='1'){

                                             $request='checked';
									}else{

 										 $request='';

									}

									if( $row['date1_draw_approved'] =='1'){

                                             $approved='checked';
									}else{

										  $approved='';
									}

									
                              if($row['date1_draw_submit'] =='1')
										{
											$submit='checked';
									}else{

									$submit='';	
									}
									
                           if($row['date1_draw_released'] =='1')
						 				{
											$released='checked';
									}
									else{

									$released='';	
									}
									
									?>

						<tr>
							<td><?php echo $row['b_name'];?></td>
							<td><?php echo $row['contact_name_1'];?></td>
							
							<td><?php echo $row['contact_phone_1'];?></td>
							<td><?php echo $row['contact_email_1'];?></td>
							<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street'];?></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
						
							<td>$<?php echo number_format($row['draws_amount_1'],2);?></td>
							<td><?php echo $row['date_1'];?></td>
							<td><input type="checkbox" <?php echo $request;?>></td>
							<td><input type="checkbox" <?php echo $approved;?>></td>
							<td><input type="checkbox" <?php echo $submit;?>></td>
					
							<td><input type="checkbox" <?php echo $released;?>></td>
							
						</tr>

							<?php }else{echo "";}?>

							<?php if($row['draws_amount_2'] !='0') { 
									

									if($row['date2_draw_request'] =='1'){

                                             $request='checked';
									}else{

 										 $request='';

									}

									if( $row['date2_draw_approved'] =='1'){

                                             $approved='checked';
									}else{

										  $approved='';
									}

									
                              if($row['date2_draw_submit'] =='1')
										{
											$submit='checked';
									}else{

									$submit='';	
									}
									
                           if($row['date2_draw_released'] =='1')
						 				{
											$released='checked';
									}
									else{

									$released='';	
									}
									
									?>


						 <tr>
							<td><?php echo $row['b_name'];?></td>
							<td><?php echo $row['contact_name_1'];?></td>
							
							<td><?php echo $row['contact_phone_1'];?></td>
							<td><?php echo $row['contact_email_1'];?></td>
							<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street'];?></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
						
							<td>$<?php echo number_format($row['draws_amount_2'],2);?></td>
								<td><?php echo $row['date_2'];?></td>
							<td><input type="checkbox" <?php echo $request;?>></td>
							<td><input type="checkbox" <?php echo $approved;?>></td>
							<td><input type="checkbox" <?php echo $submit;?>></td>
					
							<td><input type="checkbox" <?php echo $released;?>></td>
							
						</tr>
						<?php }else{echo "";}?>

						<?php if($row['draws_amount_3'] !='0') { 
									

									if($row['date3_draw_request'] =='1'){

                                             $request='checked';
									}else{

 										 $request='';

									}

									if( $row['date3_draw_approved'] =='1'){

                                             $approved='checked';
									}else{

										  $approved='';
									}

									
                              if($row['date3_draw_submit'] =='1')
										{
											$submit='checked';
									}else{

									$submit='';	
									}
									
                           if($row['date3_draw_released'] =='1')
						 				{
											$released='checked';
									}
									else{

									$released='';	
									}
									
									?>

							<tr>

							<td><?php echo $row['b_name'];?></td>
							<td><?php echo $row['contact_name_1'];?></td>
							
							<td><?php echo $row['contact_phone_1'];?></td>
							<td><?php echo $row['contact_email_1'];?></td>
							<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street'];?></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
						
							<td>$<?php echo number_format($row['draws_amount_3'],2);?></td>
								<td><?php echo $row['date_3'];?></td>
							<td><input type="checkbox" <?php echo $request;?>></td>
							<td><input type="checkbox" <?php echo $approved;?>></td>
							<td><input type="checkbox" <?php echo $submit;?>></td>
					
							<td><input type="checkbox" <?php echo $released;?>></td>
							
						</tr>
						<?php }else{echo "";}?>

						<?php if($row['draws_amount_4'] !='0') { 
									

									if($row['date4_draw_request'] =='1'){

                                             $request='checked';
									}else{

 										 $request='';

									}

									if( $row['date4_draw_approved'] =='1'){

                                             $approved='checked';
									}else{

										  $approved='';
									}

									
                              if($row['date4_draw_submit'] =='1')
										{
											$submit='checked';
									}else{

									$submit='';	
									}
									
                           if($row['date4_draw_released'] =='1')
						 				{
											$released='checked';
									}
									else{

									$released='';	
									}
									
									?>

							<tr>

							<td><?php echo $row['b_name'];?></td>
							<td><?php echo $row['contact_name_1'];?></td>
							
							<td><?php echo $row['contact_phone_1'];?></td>
							<td><?php echo $row['contact_email_1'];?></td>
							<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street'];?></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
						
							<td>$<?php echo number_format($row['draws_amount_4'],2);?></td>
							<td><?php echo $row['date_4'];?></td>
							<td><input type="checkbox" <?php echo $request;?>></td>
							<td><input type="checkbox" <?php echo $approved;?>></td>
							<td><input type="checkbox" <?php echo $submit;?>></td>
					
							<td><input type="checkbox" <?php echo $released;?>></td>
							
						</tr>
						<?php }else{echo "";}?>
						<?php if($row['draws_amount_5'] !='0') { 
									

									if($row['date5_draw_request'] =='1'){

                                             $request='checked';
									}else{

 										 $request='';

									}

									if( $row['date5_draw_approved'] =='1'){

                                             $approved='checked';
									}else{

										  $approved='';
									}

									
                              if($row['date5_draw_submit'] =='1')
										{
											$submit='checked';
									}else{

									$submit='';	
									}
									
                           if($row['date5_draw_released'] =='1')
						 				{
											$released='checked';
									}
									else{

									$released='';	
									}
									
									?>

							<tr>

							<td><?php echo $row['b_name'];?></td>
							<td><?php echo $row['contact_name_1'];?></td>
							
							<td><?php echo $row['contact_phone_1'];?></td>
							<td><?php echo $row['contact_email_1'];?></td>
							<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street'];?></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
						
							<td>$<?php echo number_format($row['draws_amount_5'],2);?></td>
							<td><?php echo $row['date_5'];?></td>
							<td><input type="checkbox" <?php echo $request;?>></td>
							<td><input type="checkbox" <?php echo $approved;?>></td>
							<td><input type="checkbox" <?php echo $submit;?>></td>
					
							<td><input type="checkbox" <?php echo $released;?>></td>
							
						</tr>
						<?php }else{echo "";}?>
						<?php if($row['draws_amount_6'] !='0') { 
									

									if($row['date6_draw_request'] =='1'){

                                             $request='checked';
									}else{

 										 $request='';

									}

									if( $row['date6_draw_approved'] =='1'){

                                             $approved='checked';
									}else{

										  $approved='';
									}

									
                              if($row['date6_draw_submit'] =='1')
										{
											$submit='checked';
									}else{

									$submit='';	
									}
									
                           if($row['date6_draw_released'] =='1')
						 				{
											$released='checked';
									}
									else{

									$released='';	
									}
									
									?>

							<tr>

							<td><?php echo $row['b_name'];?></td>
							<td><?php echo $row['contact_name_1'];?></td>
							
							<td><?php echo $row['contact_phone_1'];?></td>
							<td><?php echo $row['contact_email_1'];?></td>
							<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street'];?></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
						
							<td>$<?php echo number_format($row['draws_amount_6'],2);?></td>
							<td><?php echo $row['date_6'];?></td>
							<td><input type="checkbox" <?php echo $request;?>></td>
							<td><input type="checkbox" <?php echo $approved;?>></td>
							<td><input type="checkbox" <?php echo $submit;?>></td>
					
							<td><input type="checkbox" <?php echo $released;?>></td>
							
						</tr>
						<?php }else{echo "";}?>
						<?php if($row['draws_amount_7'] !='0') { 
									

									if($row['date7_draw_request'] =='1'){

                                             $request='checked';
									}else{

 										 $request='';

									}

									if( $row['date7_draw_approved'] =='1'){

                                             $approved='checked';
									}else{

										  $approved='';
									}

									
                              if($row['date7_draw_submit'] =='1')
										{
											$submit='checked';
									}else{

									$submit='';	
									}
									
                           if($row['date7_draw_released'] =='1')
						 				{
											$released='checked';
									}
									else{

									$released='';	
									}
									
									?>

							<tr>

							<td><?php echo $row['b_name'];?></td>
							<td><?php echo $row['contact_name_1'];?></td>
							
							<td><?php echo $row['contact_phone_1'];?></td>
							<td><?php echo $row['contact_email_1'];?></td>
							<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street'];?></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
						
							<td>$<?php echo number_format($row['draws_amount_7'],2);?></td>
							<td><?php echo $row['date_7'];?></td>
							<td><input type="checkbox" <?php echo $request;?>></td>
							<td><input type="checkbox" <?php echo $approved;?>></td>
							<td><input type="checkbox" <?php echo $submit;?>></td>
					
							<td><input type="checkbox" <?php echo $released;?>></td>
							
						</tr>
						<?php }else{echo "";}?>
						<?php if($row['draws_amount_8'] !='0') { 
									

									if($row['date8_draw_request'] =='1'){

                                             $request='checked';
									}else{

 										 $request='';

									}

									if( $row['date8_draw_approved'] =='1'){

                                             $approved='checked';
									}else{

										  $approved='';
									}

									
                              if($row['date8_draw_submit'] =='1')
										{
											$submit='checked';
									}else{

									$submit='';	
									}
									
                           if($row['date8_draw_released'] =='1')
						 				{
											$released='checked';
									}
									else{

									$released='';	
									}
									
									?>

							<tr>

							<td><?php echo $row['b_name'];?></td>
							<td><?php echo $row['contact_name_1'];?></td>
							
							<td><?php echo $row['contact_phone_1'];?></td>
							<td><?php echo $row['contact_email_1'];?></td>
							<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street'];?></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
						
							<td>$<?php echo number_format($row['draws_amount_8'],2);?></td>
							<td><?php echo $row['date_8'];?></td>
							<td><input type="checkbox" <?php echo $request;?>></td>
							<td><input type="checkbox" <?php echo $approved;?>></td>
							<td><input type="checkbox" <?php echo $submit;?>></td>
					
							<td><input type="checkbox" <?php echo $released;?>></td>
							
						</tr>
						<?php }else{echo "";}?>
						<?php if($row['draws_amount_9'] !='0') { 
									

									if($row['date9_draw_request'] =='1'){

                                             $request='checked';
									}else{

 										 $request='';

									}

									if( $row['date9_draw_approved'] =='1'){

                                             $approved='checked';
									}else{

										  $approved='';
									}

									
                              if($row['date9_draw_submit'] =='1')
										{
											$submit='checked';
									}else{

									$submit='';	
									}
									
                           if($row['date9_draw_released'] =='1')
						 				{
											$released='checked';
									}
									else{

									$released='';	
									}
									
									?>

							<tr>

							<td><?php echo $row['b_name'];?></td>
							<td><?php echo $row['contact_name_1'];?></td>
							
							<td><?php echo $row['contact_phone_1'];?></td>
							<td><?php echo $row['contact_email_1'];?></td>
							<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street'];?></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
						
							<td>$<?php echo number_format($row['draws_amount_9'],2);?></td>
							<td><?php echo $row['date_9'];?></td>
							<td><input type="checkbox" <?php echo $request;?>></td>
							<td><input type="checkbox" <?php echo $approved;?>></td>
							<td><input type="checkbox" <?php echo $submit;?>></td>
					
							<td><input type="checkbox" <?php echo $released;?>></td>
							
						</tr>
						<?php }else{echo "";}?>
						<?php if($row['draws_amount_10'] !='0') { 
									

									if($row['date10_draw_request'] =='1'){

                                             $request='checked';
									}else{

 										 $request='';

									}

									if( $row['date10_draw_approved'] =='1'){

                                             $approved='checked';
									}else{

										  $approved='';
									}

									
                              if($row['date10_draw_submit'] =='1')
										{
											$submit='checked';
									}else{

									$submit='';	
									}
									
                           if($row['date10_draw_released'] =='1')
						 				{
											$released='checked';
									}
									else{

									$released='';	
									}
									
									?>

							<tr>

							<td><?php echo $row['b_name'];?></td>
							<td><?php echo $row['contact_name_1'];?></td>
							
							<td><?php echo $row['contact_phone_1'];?></td>
							<td><?php echo $row['contact_email_1'];?></td>
							<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street'];?></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
						
							<td>$<?php echo number_format($row['draws_amount_10'],2);?></td>
							<td><?php echo $row['date_10'];?></td>
							<td><input type="checkbox" <?php echo $request;?>></td>
							<td><input type="checkbox" <?php echo $approved;?>></td>
							<td><input type="checkbox" <?php echo $submit;?>></td>
					
							<td><input type="checkbox" <?php echo $released;?>></td>
							
						</tr>
						<?php }else{echo "";}?>
						<?php if($row['draws_amount_11'] !='0') { 
									

									if($row['date11_draw_request'] =='1'){

                                             $request='checked';
									}else{

 										 $request='';

									}

									if( $row['date11_draw_approved'] =='1'){

                                             $approved='checked';
									}else{

										  $approved='';
									}

									
                              if($row['date11_draw_submit'] =='1')
										{
											$submit='checked';
									}else{

									$submit='';	
									}
									
                           if($row['date11_draw_released'] =='1')
						 				{
											$released='checked';
									}
									else{

									$released='';	
									}
									
									?>

							<tr>

							<td><?php echo $row['b_name'];?></td>
							<td><?php echo $row['contact_name_1'];?></td>
							
							<td><?php echo $row['contact_phone_1'];?></td>
							<td><?php echo $row['contact_email_1'];?></td>
							<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street'];?></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
						
							<td>$<?php echo number_format($row['draws_amount_11'],2);?></td>
							<td><?php echo $row['date_11'];?></td>
							<td><input type="checkbox" <?php echo $request;?>></td>
							<td><input type="checkbox" <?php echo $approved;?>></td>
							<td><input type="checkbox" <?php echo $submit;?>></td>
					
							<td><input type="checkbox" <?php echo $released;?>></td>
							
						</tr>
						<?php }else{echo "";}?>
						<?php if($row['draws_amount_12'] !='0') { 
									

									if($row['date12_draw_request'] =='1'){

                                             $request='checked';
									}else{

 										 $request='';

									}

									if( $row['date12_draw_approved'] =='1'){

                                             $approved='checked';
									}else{

										  $approved='';
									}

									
                              if($row['date12_draw_submit'] =='1')
										{
											$submit='checked';
									}else{

									$submit='';	
									}
									
                           if($row['date12_draw_released'] =='1')
						 				{
											$released='checked';
									}
									else{

									$released='';	
									}
									
									?>

							<tr>

							<td><?php echo $row['b_name'];?></td>
							<td><?php echo $row['contact_name_1'];?></td>
							
							<td><?php echo $row['contact_phone_1'];?></td>
							<td><?php echo $row['contact_email_1'];?></td>
							<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street'];?></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
						
							<td>$<?php echo number_format($row['draws_amount_12'],2);?></td>
							<td><?php echo $row['date_12'];?></td>
							<td><input type="checkbox" <?php echo $request;?>></td>
							<td><input type="checkbox" <?php echo $approved;?>></td>
							<td><input type="checkbox" <?php echo $submit;?>></td>
					
							<td><input type="checkbox" <?php echo $released;?>></td>
							
						</tr>
						<?php }else{echo "";}?>
						<?php if($row['draws_amount_13'] !='0') { 
									

									if($row['date13_draw_request'] =='1'){

                                             $request='checked';
									}else{

 										 $request='';

									}

									if( $row['date13_draw_approved'] =='1'){

                                             $approved='checked';
									}else{

										  $approved='';
									}

									
                              if($row['date13_draw_submit'] =='1')
										{
											$submit='checked';
									}else{

									$submit='';	
									}
									
                           if($row['date13_draw_released'] =='1')
						 				{
											$released='checked';
									}
									else{

									$released='';	
									}
									
									?>

							<tr>

							<td><?php echo $row['b_name'];?></td>
							<td><?php echo $row['contact_name_1'];?></td>
							
							<td><?php echo $row['contact_phone_1'];?></td>
							<td><?php echo $row['contact_email_1'];?></td>
							<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street'];?></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
						
							<td>$<?php echo number_format($row['draws_amount_13'],2);?></td>
							<td><?php echo $row['date_13'];?></td>
							<td><input type="checkbox" <?php echo $request;?>></td>
							<td><input type="checkbox" <?php echo $approved;?>></td>
							<td><input type="checkbox" <?php echo $submit;?>></td>
					
							<td><input type="checkbox" <?php echo $released;?>></td>
							
						</tr>
						<?php }else{echo "";}?>
						<?php if($row['draws_amount_14'] !='0') { 
									

									if($row['date14_draw_request'] =='1'){

                                             $request='checked';
									}else{

 										 $request='';

									}

									if( $row['date14_draw_approved'] =='1'){

                                             $approved='checked';
									}else{

										  $approved='';
									}

									
                              if($row['date14_draw_submit'] =='1')
										{
											$submit='checked';
									}else{

									$submit='';	
									}
									
                           if($row['date14_draw_released'] =='1')
						 				{
											$released='checked';
									}
									else{

									$released='';	
									}
									
									?>

							<tr>

							<td><?php echo $row['b_name'];?></td>
							<td><?php echo $row['contact_name_1'];?></td>
							
							<td><?php echo $row['contact_phone_1'];?></td>
							<td><?php echo $row['contact_email_1'];?></td>
							<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street'];?></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
						
							<td>$<?php echo number_format($row['draws_amount_14'],2);?></td>
							<td><?php echo $row['date_14'];?></td>
							<td><input type="checkbox" <?php echo $request;?>></td>
							<td><input type="checkbox" <?php echo $approved;?>></td>
							<td><input type="checkbox" <?php echo $submit;?>></td>
					
							<td><input type="checkbox" <?php echo $released;?>></td>
							
						</tr>
						<?php }else{echo "";}?>
						<?php if($row['draws_amount_15'] !='0') { 
									

									if($row['date15_draw_request'] =='1'){

                                             $request='checked';
									}else{

 										 $request='';

									}

									if( $row['date15_draw_approved'] =='1'){

                                             $approved='checked';
									}else{

										  $approved='';
									}

									
                              if($row['date15_draw_submit'] =='1')
										{
											$submit='checked';
									}else{

									$submit='';	
									}
									
                           if($row['date15_draw_released'] =='1')
						 				{
											$released='checked';
									}
									else{

									$released='';	
									}
									
									?>

							<tr>

							<td><?php echo $row['b_name'];?></td>
							<td><?php echo $row['contact_name_1'];?></td>
							
							<td><?php echo $row['contact_phone_1'];?></td>
							<td><?php echo $row['contact_email_1'];?></td>
							<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_street'];?></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
						
							<td>$<?php echo number_format($row['draws_amount_15'],2);?></td>
							<td><?php echo $row['date_15'];?></td>
							<td><input type="checkbox" <?php echo $request;?>></td>
							<td><input type="checkbox" <?php echo $approved;?>></td>
							<td><input type="checkbox" <?php echo $submit;?>></td>
					
							<td><input type="checkbox" <?php echo $released;?>></td>
							
						</tr>
						<?php }else{echo "";}?>
				<?php } ?>	
				</tbody>
				
			</table>

	</div>

	</div>
	
</div>

<script>
$(document).ready(function() {
    $('#table').DataTable();
} );
</script>