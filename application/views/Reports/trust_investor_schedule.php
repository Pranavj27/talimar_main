<?php

error_reporting(0);

?>

<div class="page-content-wrapper">

	<div class="page-content responsive">

		<div class="page-head">

				<!-- BEGIN PAGE TITLE -->

				<div class="page-title">

					<h1> &nbsp; Trust Investor Schedule</h1>	

					

				</div>

				<div class="top_download">
					<!--
					<a href="<?php echo base_url();?>download_fci_serviced_account">

					<button  class="btn blue">Excel Download</button>

					</a>
					
					-->
					<a href="<?php echo base_url();?>trust_investor_schedule_pdf">

					<button  class="btn blue">PDF</button>

					</a>
					
				</div>

		</div>

		<div class="rc_class">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >

				<thead>

					<tr>

						<!--<th>Investor</th>-->
						
						<th>Contact Name</th>
						
						<th>Phone</th>
						
						<th>Email</th>
						
						<th>Email<br>Blast</th>
						
						<th># of <br>Current TD's</th>
						<th>$ of <br>Current TD's</th>
						<th># of <br>Paidoff TD's</th>
						<th>$ of <br>Paidoff TD's</th>

					</tr>

				</thead>
				
				<tbody>
				<?php
					if(isset($trust_investor_schedule))
					{
						foreach($trust_investor_schedule as $key => $row)
						{
							?>
							<tr>
								<!--<td><?php echo $row['investor']; ?></td>-->
								<td><?php echo $row['contact_name1']; ?></td>
								<td><?php echo $row['phone1']; ?></td>
								<td><?php echo $row['email1']; ?></td>
								<td></td>
								<td><?php echo $row['active_count']; ?></td>
								<td>$<?php echo number_format($row['active_investment']); ?></td>
								<td><?php echo $row['paidoff_count']; ?></td>
								<td>$<?php echo number_format($row['paidoff_investment']); ?></td>
							</tr>
							<?php
						}
					}
				?>
				</tbody>
				

			</table>

			

	</div>

	</div>

	<!-- END CONTENT -->

</div>

<script>
$(document).ready(function() {
    $('#table').DataTable();
} );
</script>