<?php $account_status_option = $this->config->item('account_status_option'); ?>
<div class="page-content-wrapper">
	<style type="text/css">
		input.btn.blue.aa {
		  
		    margin-top: 14px;
		    border-radius: 0px;
		   
		}
		.talimar_no_dropdowns div {

		    padding: 0px 2px;
		    width: 24%!important;
		    font-size: 14px!important;
		}

		.talimar_no_dropdowns {
		    padding-left: 0px!important;
		}
		#table_wrapper>.row>.col-md-6.col-sm-12 {
		    
		    padding-left: 0px;
		}
		.form-control[disabled] {
		    cursor: not-allowed;
		    background-color: #dddddd;
		}
	</style>
	<div class="page-content responsive">
		<!--
				Update this method for add filter functionality in filter by date in lender data last update date is 24-03-2021
		-->
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>&nbsp;Lender Accounts by Date</h1>
			
			</div>

			<div class="top_download">
					
			    <form method="POST" action="<?php echo base_url();?>ReportData/Newlenderaccounts">

			    	<input type="hidden" name="reportDisplay" value="pdf">
			    	<input type="hidden" name="account_status" value="<?php echo isset($account_status) ? $account_status : ''; ?>">
			    	<input type="hidden" name="start" value="<?php echo isset($start) ? $start : ''; ?>" >
					<input type="hidden" name="end" value="<?php echo isset($end) ? $end : ''; ?>" >
					<input type="hidden" name="dates" value="<?php echo isset($select_date) ? $select_date : ''; ?>" >
					<button  class="btn red aa" type="submit">PDF</button>

				</form>
				<form method="POST" action="<?php echo base_url();?>ReportData/Newlenderaccounts">
					<input type="hidden" name="account_status" value="<?php echo isset($account_status) ? $account_status : ''; ?>">
			    	<input type="hidden" name="reportDisplay" value="excel">
			    	<input type="hidden" name="start" value="<?php echo isset($start) ? $start : ''; ?>" >
					<input type="hidden" name="end" value="<?php echo isset($end) ? $end : ''; ?>" >
					<input type="hidden" name="dates" value="<?php echo isset($select_date) ? $select_date : ''; ?>" >
					<button  class="btn green aa" type="submit" style="margin-right: 0px;">Excel</button> 
				</form>
				<input 	type="button" class="btn blue aa" onclick="load_form(this);" value="Filter" style="float: right;">

			</div>			
		</div>
		
		<div class="row">
			<div class="talimar_no_dropdowns" style="float:left;">
				<form id="form_iad" method="POST" action="<?php echo base_url().'ReportData/Newlenderaccounts'?>">

					<div class="float-direction-left">
						Account Status : <br>
						<select name="account_status" id="account_status" onchange="disable_dates(this,1)" tabindex="4">
							<option value="">Select All</option>
							<?php foreach($account_status_option as $key => $row){
									if($row!='Select One'){
							?>
									<option value="<?= $key;?>" <?php if($account_status == $key){echo 'selected';}?>><?= $row;?></option>
							<?php } } ?>
						</select>
					</div>
				
					<div class="float-direction-left">
						Dates: <br>
						<select name="dates" id="dates" onchange="disable_dates(this,1)" tabindex="4">
							<option value="">Select Date</option>
							<option value="1" <?php if($select_date == 1){echo 'selected';}?>>Month to Date</option>
							<option value="2" <?php if($select_date == 2){echo 'selected';}?>>Year to Date</option>
							<option value="5" <?php if($select_date == 5){echo 'selected';}?>>Prior Year to Date</option>
							<option value="4" <?php if($select_date == 4){echo 'selected';}?>>Last Year</option>
							<option value="3" <?php if($select_date == 3){echo 'selected';}?>>Custom</option>
						</select>
					</div>
					
					<div class="float-direction-left" >
						Start Date: <input  tabindex="5" class="form-control datepicker" name="start" id="start" placeholder="MM/DD/YYYY" value="<?php echo isset($start) ? $start : ''; ?>" disabled  autocomplete="off">
					</div>
					<div class="float-direction-left">
						End Date: <input tabindex="5"  class="form-control datepicker" name="end" id="end" placeholder="MM/DD/YYYY" value="<?php echo isset($end) ? $end : ''; ?>" disabled  autocomplete="off">
					</div>	
					
				</form>
			</div>
		</div>
		<!-- Close updated section -->
		<div class="row">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content">
				<thead>
					<tr>
						<th>Lender Name</th>
						<th>Lender Type</th>
						<th>Status</th>
					    <th>Contact Name</th>
						<th>Active (#)</th>
						<th>Active ($)</th>
						<th>Total (#)</th>
						<th>Total ($)</th>			
						<th>Established</th>			
					</tr>
				</thead>
				<tbody>

				<?php 
				$investor_type_option 				= $this->config->item('investor_type_option');
				
				$total=0;

				if(isset($Newlenderaccounts) && is_array($Newlenderaccounts)){ 

					foreach($Newlenderaccounts as $row){ 

						if(!empty($row['ldate_created'])){
							$ldate_created = date('Y-m-d', strtotime($row['ldate_created']));
						}else{
							$ldate_created = '';
						}
					?>

					<tr>
						<td>
							<a href="<?php echo base_url();?>investor_view/<?php echo $row['lid'];?>"><?php echo $row['name'];?></a>
						</td>
						<td><?php $optn = isset($row['investor_type']) ? $row['investor_type'] : 0;	
							echo $investor_type_option[$optn]; 		
							?>	
						</td>
						<td>

							<?php $account_status = isset($row['account_status']) ? $row['account_status'] : '';	
							echo $account_status_option[$account_status]; 		
							?>
							
						</td>
						<td>
							<a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['cname'];?></a>
						</td>
						<td><?php echo $row['countActive'];?></td>
						<td>$<?php echo number_format($row['AmountActive']);?></td>
						<td><?php echo $row['countTotal'];?></td>
						<td>$<?php echo number_format($row['AmountTotal']);?></td>
						<td><?php echo $ldate_created;?></td>
												
					</tr>
					<?php 

					$total++;
					$countActive 	= $countActive + $row['countActive'];
					$AmountActive 		= $AmountActive + $row['AmountActive'];
					
					$countTotal += $row['countTotal'];
					$AmountTotal += $row['AmountTotal'];

				} }else{ ?>

					<tr>
						<td colspan="9">No data found!</td>
					</tr>

				<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<th>Total: <?php echo $total; ?></th>
						<th></th>
						<th></th>
						<th></th>
						<th><?php echo number_format($countActive);?></th>
						<th><?php echo '$'.number_format($AmountActive);?></th>
						<th><?php echo number_format($countTotal);?></th>
						<th><?php echo '$'.number_format($AmountTotal);?></th>
						<th ></th>
					</tr>
				</tfoot>
			</table>
		</div>
			
	</div>
	<!-- END CONTENT -->
</div>

<!--
		Update this method for add filter functionality in filter by date in lender data last update date is 24-03-2021
-->
<script>
$(document).ready(function() {
    $('#table').DataTable();
    
} );
disable_dates('',0);
function loan_funded_year_form()
{
	$('#loan_funded_year_form').submit();
}

function disable_dates(that,no){

if(no > 0)
{
	
	$('#start').val('');
	$('#end').val('');
}
	
	var selectbox=$('#dates').val();

	if(selectbox=='3'){
     $(".datepicker").prop('disabled',false);
	}else{
		
	 $(".datepicker").prop('disabled',true);
	 $(".datepicker").datepicker({ dateFormat: "yy-mm-dd" }).val();
	var selectbox='';
	}
}

function load_form(){

 $('#form_iad').submit();	

}
</script>


	


