<?php
$header = '<h1 style="color:#003468;font-size:18px;"> Loan Marketing Report </h1>';
		$header .= '<style>
                        .table {
                            width: 100%;
                            max-width: 100%;
                            margin-bottom: 20px;
                            border-collapse:collapse;
                        }
                        .table-bordered {
                        border: 1px solid #ddd;
                        }
                        table {
                            border-spacing: 0;
                            border-collapse: collapse;
                        }

                        .th_text_align_center th {
                            text-align: left;
                        }
                        .th_text_align_center td {
                            text-align: left;
                        }

                        .td_large{
                            width:17%;
                        }
                        .small_td{
                            width:4%;
                        }

                        .td_medium{
                            width:5%;
                        }
                        tr.dark_border th{
                            border: 1px solid gray;
                        }
                        .table td{
                            font-size:12px;
                            font-family: "Times New Roman", Georgia, Serif;
                            height:25px;
                        }
                        .table th{
                            height : 20px;
                            font-size : 12px;
                        }

                        tr.table_header th{
                            font-size:12px;
                            font-family: "Times New Roman", Georgia, Serif;
                            text-align:center;
                            background-color:#bfcfe3;
                        }

                        tr.table_bottom th{
                            font-size:12px;
                            font-family: "Times New Roman", Georgia, Serif;
                            text-align:left;
                            background-color:#bfcfe3;
                        }
                        tr.odd td{
                            background-color:#ededed;
                        }
                        </style>
                        ';
		// $header .= '<link href="http://wartiz.com/talimar/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />';
		$header .= '<table class="table table-bordered th_text_align_center" style="width:100%;">';

		$header .= '<tr class="table_header">';
		$header .= '<th style="width:12%;text-decoration:underline;"><strong>Borrower Name</strong></th>';
		// $header .= '<th  style="width:8%;text-decoration:underline;"><strong>TaliMar#</strong></th>';
		$header .= '<th  style="width:8%;text-decoration:underline;"><strong>Loan Amount</strong></th>';
		$header .= '<th  style="width:5%;text-decoration:underline;"><strong>Position</strong></th>';
		$header .= '<th  style="width:5%;text-decoration:underline;"><strong>Term</strong></th>';
		$header .= '<th  style="width:7%;text-decoration:underline;"><strong>Property Type</strong></th>';
		$header .= '<th  style="width:5%;text-decoration:underline;"><strong>Loan Type</strong></th>';
		$header .= '<th  style="width:10%;text-decoration:underline;"><strong>Property Address</strong></th>';
		$header .= '<th  style="width:5%;text-decoration:underline;"><strong>Unit</strong></th>';
		$header .= '<th  style="width:5%;text-decoration:underline;"><strong>City</strong></th>';
		$header .= '<th  style="width:5%;text-decoration:underline;"><strong>State</strong></th>';
		$header .= '<th  style="width:6%;text-decoration:underline;"><strong>Images<br>Created</strong></th>';
		$header .= '<th  style="width:6%;text-decoration:underline;"><strong>Closing<br>Posted</strong></th>';
		$header .= '<th  style="width:6%;text-decoration:underline;"><strong>Google<br>Request</strong></th>';
		$header .= '<th  style="width:6%;text-decoration:underline;"><strong>Sign<br>Posted</strong></th>';
		$header .= '<th  style="width:9%;text-decoration:underline;"><strong>Social Media<br>Complete</strong></th>';
		
		$header .= '</tr>';

		$count = 0;
		$total_loan_amount = 0;
		foreach ($fetch_social_loan as $key => $row) {

			if ($key % 2 == 0) {
				$class_add = "even";
			} else if ($key == 0) {
				$class_add = "even";
			} else {
				$class_add = "odd";
			}

			

			$total_loan_amount = $total_loan_amount + $row['loan_amount'];
			$header .= '<tr class="' . $class_add . '">';
			$header .= "<td>" . $row['borrower'] . "</td>";
			// $header .="<td>".$row['talimar']."</td>";
			$header .= "<td>$" . number_format($row['loan_amount'], 2) . "</td>";
			$header .="<td>".$position_option[$row['loan_position']]."</td>";
			$header .="<td>".$row['term_month']."</td>";
			$header .= "<td>" . $property_typee[$row['prop_type']] . "</td>";
			$header .="<td>".$loan_typee[$row['loan_type']]."</td>";
			$header .= "<td>" . $row['property_address'] . "</td>";
			$header .= "<td>" . $row['unit'] . "</td>";
			$header .= "<td>" . $row['city'] . "</td>";
			$header .= "<td>" . $row['state'] . "</td>";
			$header .= "<td>" . $row['image_ceate_val'] . "</td>";
			$header .= "<td>" . $row['closing_posted'] . "</td>";
			$header .= "<td>" . $row['google_reguest'] . "</td>";
			$header .= "<td>" . $row['sign_posted'] . "</td>";
			$header .= "<td>" . $row['social_media_com'] . "</td>";
			//$header .="<td>".$posted_on_website."</td>";
			
			
			//$header .="<td>".$avaliable_realese_sign."</td>";
			$header .= '</tr>';

			$count++;

		}

		$header .= '<tr class="table_bottom">';
		$header .= '<th><strong>Total   ' . $count . '</strong></th>';

		$header .= '<th><strong>$' . number_format($total_loan_amount, 2) . '</strong></th>';
		$header .= '<th colspan="13"></th>';
		$header .= '</tr>';

		$header .= '<tr class="table_bottom">';
		$header .= '<th><strong>Average</strong></th>';

		$header .= '<th><strong>$' . number_format($total_loan_amount / $count, 2) . '</strong></th>';
		$header .= '<th colspan="13"></th>';
		$header .= '</tr>';

		// $header .="</tbody>";
		$header .= '</table>';
	echo $header;
?>