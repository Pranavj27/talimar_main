<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Client Schwag - Lender</h1>	
					
				</div>
				
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>reports/LenderDisbursement_pdf">
						<button type="submit" class="btn blue">PDF</button>
					</form>
				</div>
				
		</div>
		
		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Contact 1 First Name</th>
					<th>Contact 1 Last Name</th>
					<th>Mailing Street Address</th>
					<th>Mailing Unit #</th>
					<th>Mailing City</th>
					<th>Mailing State</th>
					<th>Mailing Zip</th>
					<th># of Active Loans </th>
					<th>$ of Active Loans</th>
					<th># of Total Loans</th>
					<th>$ of Total Loans</th>					
				</tr>
				</thead>
				<tbody>
					<?php if(isset($LenderDisbursement) && is_array($LenderDisbursement)){ 
					
								foreach($LenderDisbursement as $row){ ?>
									<tr>
										<td><?php echo $row['contact_firstname'];?></td>
										<td><?php echo $row['contact_lastname'];?></td>
										<td><?php echo $row['contact_mailing_address'];?></td>
										<td><?php echo $row['contact_mailing_unit'];?></td>
										<td><?php echo $row['contact_mailing_city'];?></td>
										<td><?php echo $row['contact_mailing_state'];?></td>
										<td><?php echo $row['contact_mailing_zip'];?></td>
										<td><?php echo $row['totalActive'];?></td>
										<td>$<?php echo number_format($row['activeamount'],2);?></td>
										<td><?php echo $row['totalallcount'];?></td>
										<td>$<?php echo number_format($row['totalallamount'],2);?></td>
																				
									</tr>
					<?php } }else{ ?>
							<tr>
								<td colspan="11">No data found!</td>
							</tr>
					<?php } ?>
				</tbody>		
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {
	 
    $('#table').DataTable();
	
});



</script>




