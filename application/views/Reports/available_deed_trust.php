<?php

error_reporting(0);
$investor_type_option = $this->config->item('investor_type_option');

?>


<div class="page-content-wrapper">

	<div class="page-content">

		<div class="page-head">

				<!-- BEGIN PAGE TITLE -->

				<div class="page-title">

					<h1> &nbsp; Available Trust Deed Schedule  </h1>	

					

				</div>

				<div class="top_download">
					<!--
					<a href="<?php echo base_url();?>download_trust_deed_excel">

					<button  class="btn blue">Excel Download</button>

					</a>
					-->
					<a href="<?php echo base_url();?>download_trust_deed_pdf">

					<button  class="btn blue">PDF</button>

					</a>
					

				</div>

		</div>

		<div class="rc_class">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >

				<thead>

					<tr>
						<th>Loan Number</th>
						
						<th>Borrower Name</th>
						
						<th>Closing Date</th>
						
						<th>Available<br>Amount</th>
						
						<th>Loan Amount</th>

						<th>Investor Yield</th>

						<th>Property Address </th>

						<th>Property City</th>
						
						<th>Property State</th>
						
						<th>Property Zip</th>
						
						

					</tr>

				</thead>

				<tbody>
					
					<?php
					$total_available = 0;
					$total_loan_amount = 0;
					$total_investor_yield = 0;

		
					foreach($trust_deed_schedule as $key => $row_data)
					{
						$total_available 		= $total_available + $trust_deed_schedule[$key]['available'];
						$total_loan_amount 		= $total_loan_amount + $trust_deed_schedule[$key]['loan_amount'];
						$cal_inverter_yield 	= $trust_deed_schedule[$key]['intrest_rate'] - $trust_deed_schedule[$key]['servicing_fee'];
						
						$total_investor_yield 	= $total_investor_yield + $cal_inverter_yield;
						
						?>
						<tr>
						<td>
							<a href ="<?php echo base_url().'load_data/'.$trust_deed_schedule[$key]['loan_id']; ?>">
							<?php echo $trust_deed_schedule[$key]['talimar_loan']; ?></a> 
						</td>
						<td><?php echo $trust_deed_schedule[$key]['borrower']; ?></td>
						<td><?php echo date('m-d-Y', strtotime($trust_deed_schedule[$key]['closing_date'])); ?></td>
						
						<td>$<?php echo number_format($trust_deed_schedule[$key]['available']); ?></td>
						
						<td>$<?php echo number_format($trust_deed_schedule[$key]['loan_amount']); ?></td>
						<td><?php echo number_format($trust_deed_schedule[$key]['intrest_rate'] - $trust_deed_schedule[$key]['servicing_fee'],3); ?>%</td>
						
						<td><?php echo $trust_deed_schedule[$key]['property_address']; ?></td>
						<td><?php echo $trust_deed_schedule[$key]['city']; ?></td>
						<td><?php echo $trust_deed_schedule[$key]['state']; ?></td>
						<td><?php echo $trust_deed_schedule[$key]['zip']; ?></td>
						
						
						</tr>
						<?php
					}
					
					$count_row = $key + 1;
					?>

				</tbody>
				
				<tfoot>
					<tr>
						<th>Total: <?php echo $count_row; ?></th>
						<th></th>
						<th></th>
						<th><?php echo '$'.number_format($total_available); ?></th>
						<th><?php echo '$'.number_format($total_loan_amount); ?></th>
						<th colspan="5"></th>
					</tr>
					
					<tr>
						<th>Average</th>
						<th></th>
						<th></th>
						<th><?php echo '$'.number_format($total_available/$count_row); ?></th>
						<th><?php echo '$'.number_format($total_loan_amount/$count_row); ?></th>
						<th><?php echo number_format($total_investor_yield/$count_row,2); ?>%</th>
						<th colspan="4"></th>
					</tr>
				</tfoot>

			</table>

			

	</div>

	</div>

	<!-- END CONTENT -->

</div>
<!--
<script src="//code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
-->
<script>
$(document).ready(function() {
    $('#table').DataTable();
} );
</script>