<?php

error_reporting(0);
$investor_type_option 	= $this->config->item('investor_type_option');
$escrow_account_option 	= $this->config->item('escrow_account_option');
// echo "<pre>";

// print_r($loan_funded_year);

// echo "</pre>";

?>
<div class="page-content-wrapper">

	<div class="page-content responsive">
		<form action = "<?php echo base_url().'renovation_balance_pdf';?>" method="POST">
		
		<?php
		
		//this portion for show all data
		if(isset($renovation_balance))
		{
						
			foreach($renovation_balance as $key => $row)
			{
				?>
				<input type="hidden" name="loan_id[]" value="<?php echo $row['loan_id']; ?>">
				<?php
			}
		}
		?>
		<div class="page-head">

				<!-- BEGIN PAGE TITLE -->

				<div class="page-title">
					<h1> &nbsp; Renovation Fund Control Report </h1>	
				</div>

				<div class="top_download">
					<!--
					<a href="<?php echo base_url();?>download_trust_deed_excel">

					<button  class="btn blue">Excel Download</button>

					</a>
					-->
					<div class="talimar_no_dropdowns">
					<button  class="btn blue"type="submit">PDF</button>
					</div>
					
					

				</div>

		</div>

		<div class="rc_class">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >

				<thead>

					<tr>
						<th>Loan #</th>
						<th># of Draws</th>
						<th>Reserve Balance</th>
						<th>Reserve Release</th>
						<th>Reserve Available</th>
						<th>Fund Control Servicer</th>
						<th>Fund Control Account #</th>
					</tr>

				</thead>
					
				<tbody>
					<?php
					$count							=0 ;
					$total_count_draw 				= 0;
					$total_sum_draws_amount 		= 0;
					$total_sum_draws_release_amount = 0;
					$total_sum_draws_remaining 		= 0;
					if(isset($renovation_balance))
					{
						
						foreach($renovation_balance as $key => $row)
						{
							
							$total_count_draw 				= $total_count_draw + $row['count_draw'];
							$total_sum_draws_amount 		= $total_sum_draws_amount + $row['sum_draws_amount'];
							$total_sum_draws_release_amount = $total_sum_draws_release_amount + $row['sum_draws_release_amount'];
							$total_sum_draws_remaining 		= $total_sum_draws_remaining + $row['sum_draws_remaining'];
							?>
							
							<tr>
							<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['talimar']; ?></a></td>
							<td><?php echo $row['count_draw']; ?></td>
							<td>$<?php echo number_format($row['sum_draws_amount'],2); ?></td>
							<td>$<?php echo number_format($row['sum_draws_release_amount'],2); ?></td>
							<td>$<?php echo number_format($row['sum_draws_remaining'],2); ?></td>
							<td><?php echo $row['draw_escrow_other']; ?></td>
							<td><?php echo $escrow_account_option[$row['draw_escrow_account']]; ?></td>
							</tr>
							<?php
							$count++;
						}
					}
					?>
				</tbody>
				
				<tfoot>
					<tr>
						<th>Total:<?php echo $count; ?></th>
						<th><?php echo $total_count_draw;?></th>
						<th>$<?php echo number_format($total_sum_draws_amount,2);?></th>
						<th>$<?php echo number_format($total_sum_draws_release_amount,2);?></th>
						<th>$<?php echo number_format($total_sum_draws_remaining,2);?></th>
						<th colspan="2"></th>
					</tr>
				</tfoot>

			</table>

			

	</div>
	</form>

	</div>

	<!-- END CONTENT -->

</div>
<!--
<script src="//code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
-->
<script>
$(document).ready(function() {
    $('#table').DataTable({
		"order": [[ 4, "asc" ],[ 2, "asc" ],[ 0, "asc" ]]
	});
} );
function loan_funded_year_form()
{
	$('#loan_funded_year_form').submit();
}
</script>