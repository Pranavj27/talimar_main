<?php
error_reporting(0);
// echo "<pre>";
// print_r($fetch_paid_off_data);
// echo "</pre>";
$usa_city_county 				= $this->config->item('usa_city_county'); 
//$loan_type_option 				= $this->config->item('loan_type_option');

$position_option 					= $this->config->item('position_option');										
$loan_type_option= array(
							'' => 'Select All',
							1 	=> 'Bridge',
							2 	=> 'Construction (Existing)',
							3 	=> 'Construction (New)'
							
							);

$serviceing_sub_agent_option 	= $this->config->item('serviceing_sub_agent_option');


$template_yes_no_option 		= array(
										""=>'Select All',
										"2"=>'Yes',
										"1"=>'No',

										);
$template_yes_no_option_ori 		= array(
										""=>'Select All',
										"1"=>'Yes',
										"2"=>'No',

										);
$loan_status_option	= array(
							"" => 'Select All',
							6 => 'Term Sheet',
							1 => 'Pipeline',
							2 => 'Active',
							3 => 'Paid off',
							//4 => 'Cancelled',
							5 => 'Brokered'
								);	

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Paid Off Loan Schedule</h1>	
				</div>
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>download_paidoff_loan">
						
		
						<input type="hidden" name="sub_servicing_agent" value="<?php echo isset($sub_servicing_agent) ? $sub_servicing_agent : ''; ?>" >
						<input type="hidden" name="loan_type" value="<?php echo isset($loan_type) ? $loan_type : ''; ?>" >
						<input type="hidden" name="county" value="<?php echo isset($county) ? $county : ''; ?>" >
						<input type="hidden" name="borrower_id" value="<?php echo isset($borrower_id) ? $borrower_id : ''; ?>" >
						<input type="hidden" name="payoff_proces" value="<?php echo isset($payoff_proces) ? $payoff_proces : ''; ?>" >
						<input type="hidden" name="loan_status" value="<?php echo isset($loan_status) ? $loan_status : ''; ?>" >
						
						<input type="hidden" name="start_date" value="<?php echo  isset($start_date) ? $start_date:''; ?>">
						
						<input type="hidden" name="end_date" value="<?php echo isset($end_date) ? $end_date:''; ?>">
						
						<!--<a href="<?php echo base_url();?>download_existing_loan">-->
					<button  type = "submit" class="btn red">PDF</button>
						<button  type = "button" onclick="sub_filter_form()" class="btn blue">Filter</button>
						
						<!--</a>-->
					</form>
				
				</div>
		</div>
		<div class="row">
			<div class="talimar_no_dropdowns">
				<form method="POST" action="<?php echo base_url().'paid_of_loan_schedule'?>" id="form_filter">
				
					<div class="float-direction-left">
						From:
						<input type="text" name="start_date" value="<?php echo $start_date;?>" class="form-control datepicker" placeholder="Start Date"/>
						To:
						<input type="text" value="<?php echo $end_date;?>" name="end_date" class="form-control datepicker" placeholder="End Date"/>
					
					
					</div>
					<div class="float-direction-left col-md-2">
						Payoff Processed: 
						<select   class="form-control" name = "payoff_proces"  class="form-control" >
							
								<?php foreach($template_yes_no_option_ori as $key=> $row) { ?>
									<option value="<?php echo $key;?>" <?php if($key ==$payoff_proces){ echo 'selected'; } ?>><?php echo $row; ?></option>
							<?php }?>
							</select>
					</div>
						
					<div class="float-direction-left col-md-2">
						Loan Status: 
						<select id="loan_status" name="loan_status" class="form-control load_data_inputbox">
						
							<?php foreach($loan_status_option as $key => $row) { ?>
							<option value="<?php echo $key;?>" <?php if($key ==$loan_status ){ echo "selected"; }?> ><?php echo $row;?></option>
							<?php } ?>
						</select>
					</div>
					<div class="float-direction-left">
						Subservicier: 
						<select id="subservicing_agent" name="sub_servicing_agent" class="form-control input-md">
						<option value = "">Select All</option>
							<?php foreach($fetch_vendors as $key => $row){ if(($row->vendor_name == 'FCI Lender Services') ||( $row->vendor_name == 'Del Toro Loan Servicing')){?>
							<option value="<?php echo $row->vendor_id;?>" <?php if($sub_servicing_agent == $row->vendor_id){ echo 'selected'; }$optntxt = $row->vendor_name;?> ><?php echo $optntxt;?></option>
							<?php } }?>
						</select>
					</div>
				
					<div class="float-direction-left">
						County: 
						
						<select  class="form-control" name ="county" id="county_usa">
							
							<?php 
								foreach($usa_city_county as $key => $row )
								{
									
							?>
							<option value = '<?php echo $key;?>' <?php if($key == $county) { echo 'selected'; } ?>><?php echo $row;?></option>
							<?php } ?>
						</select>
					</div>
					
					<div class="float-direction-left">
						Loan Type: 
						<select id="loan_type" name="loan_type" class="form-control load_data_inputbox">
						
							<?php foreach($loan_type_option as $key => $row) { ?>
							<option value="<?php echo $key;?>" <?php if($key ==$loan_type ){ echo "selected"; }?> ><?php echo $row;?></option>
							<?php } ?>
						</select>
					</div>

		
					
					<div class="float-direction-left col-md-2">
						Borrower Name: 
						<select   class="form-control" name = "borrower_id"  class="selectpicker" data-live-search="true" >
								<option value="">Create new</option>
								<?php foreach($fetch_all_borrowers as $row) { ?>
									<option value="<?php echo $row->id;?>" <?php if(isset($borrower_id)){ if($borrower_id == $row->id ){ echo 'selected'; } } ?> ><?php echo $row->b_name ;  } ?></option>
							
							</select>
					</div>
			
					
				
						
		

					
					
				</form>

			</div>
		</div>
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Paid Off Date</th>
					<th>Borrower Name</th>	
					<th>Contact Name</th>	
					<th>Address #1</th>
					<th>#</th>
					<th>State</th>
					<th>City</th>
					<th>Zip</th>
					<!-- <th>TaliMar #</th> -->
					<th>Loan Amount</th>
					<th>Interest Rate</th>
					<th>Paid Off Value</th>										
					<th>LTV</th>
					<th>Duration</th>
					<th>Position</th>
			
				</tr>
				</thead>
				<tbody>
				<?php
				$key = 0;
				$total_loan_amount = 0;
				$total_intrest_rate = 0;
				$total_ltv = 0;
				$total_term = 0;
					foreach($fetch_paid_off_data['talimar_loan'] as $row )
					{	
					$arv 			= $fetch_paid_off_data['arv'][$key];
					
					// $ltv = (($fetch_paid_off_data['senior_sum_current'][$key]) / $fetch_paid_off_data['arv'][$key])*100;
					//$ltv = (($fetch_paid_off_data['seniorr_sum_current'][$fetch_paid_off_data['talimar_loan'][$key]])/$arv) * 100;		
					
					//$sss = ($fetch_paid_off_data['seniorr_sum_current'][$fetch_paid_off_data['talimar_loan'][$key]]/$fetch_paid_off_data['sales_refinance_value'][$key])*100;
					//$sss=($fetch_paid_off_data['loan_amount'][$key]/($total_uw_value-$total_of_senior_lien))*100.'%'; 

                     $sss=($fetch_paid_off_data['loan_amount'][$key]/$fetch_paid_off_data['underwriting_value'][$key])*100;
                     
                     
					//$sss =(($fetch_paid_off_data['current_balanced'][$key] + $fetch_paid_off_data['loan_amount'][$key])/$fetch_paid_off_data['underwriting_value'][$key])*100; 
											
					$sxs = (is_infinite($sss) || is_nan($sss)) ? 0 : $sss;
					$total_loan_amount	= $total_loan_amount + $fetch_paid_off_data['loan_amount'][$key];
					$total_intrest_rate	= $total_intrest_rate + $fetch_paid_off_data['interest_rate'][$key];
					$total_ltv	+= $sxs;
					$total_term	= $total_term + $fetch_paid_off_data['term_month'][$key];
						?>
						<tr>
							<td><?php echo $fetch_paid_off_data['paid_off_date'][$key];?></td>
							<td><a href="<?php echo base_url().'borrower_view/'.$fetch_paid_off_data['b_id'][$key];?>" ><?php echo $fetch_paid_off_data['borrower_name'][$key]; ?></a></td>
							<td><a href="<?php echo base_url().'viewcontact/'.$fetch_paid_off_data['b_contact_id'][$key];?>" ><?php echo $fetch_paid_off_data['borrower_contact_name'][$key]; ?></a></td>
						<!-- 	<td>
								<a href="<?php echo base_url().'load_data/'.$fetch_paid_off_data['loan_id'][$key];?>" >
								<?php echo $fetch_paid_off_data['talimar_loan'][$key]; ?></a>
							</td> -->


							<td><a href="<?php echo base_url().'load_data/'.$fetch_paid_off_data['loan_id'][$key];?>"><?php echo $fetch_paid_off_data['property_address'][$key];?></a></td>
							<td><?php echo $fetch_paid_off_data['unit'][$key];?></td>
							<td><?php echo $fetch_paid_off_data['state'][$key];?></td>
							<td><?php echo $fetch_paid_off_data['city'][$key];?></td>
							<td><?php echo $fetch_paid_off_data['zip'][$key];?></td>



							<td><?php echo '$'.number_format($fetch_paid_off_data['loan_amount'][$key]); ?></td>
							<td><?php echo number_format($fetch_paid_off_data['interest_rate'][$key],3)."%"; ?></td>
							<td><?php echo '$'.number_format($fetch_paid_off_data['underwriting_value'][$key]); ?></td>
							<td><?php echo number_format($sxs,2).'%'; ?></td>   														
							
							<td><?php echo $fetch_paid_off_data['term_month'][$key];?></td>
							<td><?php echo $position_option[$fetch_paid_off_data['position'][$key]];?></td>
							
						</tr>
						<?php
					$key++;
					}
				?>
				</tbody>
				
				<tfoot>
					<tr>
						<th>Total:</th>
						<th><?php echo $key; ?></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th><?php echo '$'.number_format($total_loan_amount); ?></th>
						<th colspan="5"></th>
					</tr>
					
					<tr>
						<th>Average:</th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th><?php echo '$'.number_format($total_loan_amount/$key); ?></th>
						<th><?php echo number_format($total_intrest_rate/$key,3); ?>%</th>
						<th></th>
						<th><?php echo number_format($total_ltv/$key,2); ?>%</th>
						<th><?php echo number_format($total_term/$key); ?></th>
						<th></th>
				
					</tr>
				</tfoot>
			</table>
			
	</div>
	</div>
	<!-- END CONTENT -->
</div>
<style>
	.talimar_no_dropdowns {
		float: left;
		
	}
</style>
<script>
$(document).ready(function() {
    $('#table').DataTable({
		
  columnDefs: [ { type: 'date', 'targets': [0] } ],
   order: [[ 0, 'desc' ]], 
		
	});
} );


function sub_filter_form(){

$('form#form_filter').submit();

}
</script>
