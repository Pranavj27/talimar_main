<style>
.float-direction-left {
    float: right !important;
}

.talimar_no_dropdowns div {
    float: right;
}


.float-direction-left.g {
    /*margin-right: 878px;*/
}

.float-direction-left {
    margin-right: 10px;
</style>
<?php

error_reporting(0);
$investor_type_option = $this->config->item('investor_type_option');
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$yes_no_option 		= $this->config->item('yes_no_option');
		
$select_box_name = isset($select_box) ? '('.$loan_status_option[$select_box].')' : '' ;
$date_data = '';
if(isset($start) && isset($end))
{
	$date_data = '('.$start.' to '.$end.')';
}

// echo "<pre>";

// print_r($select_date);

// echo "</pre>";

?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <link rel="stylesheet" href="/resources/demos/style.css">
<div class="page-content-wrapper">

	<div class="page-content responsive">

		<div class="page-head">

				<!-- BEGIN PAGE TITLE -->

				<div class="page-title">

					<h1> &nbsp; Funded to Date Schedule<?php echo $date_data; ?> </h1>	

					

				</div>

					<!--
					<a href="<?php echo base_url();?>download_trust_deed_excel">

					<button  class="btn blue">Excel Download</button>

					</a>
					-->
					<div class="talimar_no_dropdowns">
					
					
					<form method="POST" action="<?php echo base_url();?>loan_funded_year_pdf">
					<input type="hidden" name="start" value="<?php echo isset($start) ? $start : ''; ?>" >
					<input type="hidden" name="end" value="<?php echo isset($end) ? $end : ''; ?>" >
					
					<input type="hidden" name="select_box" value="<?php echo isset($select_box) ? $select_box : ''; ?>" >
					<input type="hidden" name="select_multi_lender" value="<?php echo isset($select_multi_lender) ? $select_multi_lender : ''; ?>" >
					<input type="hidden" name="select_fund_broker" value="<?php echo isset($select_fund_broker) ? $select_fund_broker : ''; ?>" >
					<input type="hidden" name="dates" value="<?php echo isset($select_date) ? $select_date : ''; ?>" >
					<button  type="submit" class="btn red">PDF</button>
					<input 	type="button" class="btn blue" onclick="load_form(this);" value="Filter">
					</form>
					
					</div>
					
		</div>
		<!-------------------------------Error message show ------------------->
		<?php if($this->session->flashdata('success')!=''){  ?>
		 <div id='success'><i class='fa fa-thumbs-o-up'></i><?=$this->session->flashdata('success');?></div><?php } ?>
		 
		 <?php if($this->session->flashdata('error')!=''){  ?>
		 <div id='error'><i class='fa fa-thumbs-o-down'></i><?=$this->session->flashdata('error');?></div><?php } ?>
		<!-------------------------------End Error message show ------------------->

		<div class="row">
			<div class="talimar_no_dropdowns" style="float:left;">
				<form id="form_iad" method="POST" action="<?php echo base_url().'loan_funded_year'?>">
				<!-- 	<div class="float-direction-margin">
			
					</div>	 -->
					
					<div class="float-direction-left">
						End Date: <input tabindex="6"  class="form-control datepicker" name="end" placeholder="MM/DD/YYYY" value="<?php echo isset($end) ? $end : ''; ?>" disabled  autocomplete="off">
					</div>
					<div class="float-direction-left" >
						Start Date: <input  tabindex="5" class="form-control datepicker" name="start" placeholder="MM/DD/YYYY" value="<?php echo isset($start) ? $start : ''; ?>" disabled  autocomplete="off">
					</div>
					<div class="float-direction-left">
						Dates: <br>
						<select name="dates" id="dates" onchange="disable_dates(this)" tabindex="4">
							
							<option value="">Select Date</option>
							<option value="1" <?php if($select_date == 1){echo 'selected';}?>>Month to Date</option>
							<option value="2" <?php if($select_date == 2){echo 'selected';}?>>Year to Date</option>
							<option value="5" <?php if($select_date == 5){echo 'selected';}?>>Prior Year to Date</option>
							<option value="4" <?php if($select_date == 4){echo 'selected';}?>>Last Year</option>
							<option value="3" <?php if($select_date == 3){echo 'selected';}?>>Custom</option>
						</select>
					</div>
					<div class="float-direction-left">
						Loan Status: <br>
						<select name="select_box" tabindex="3">
							<option <?php  if($select_box == 'select'){ echo 'selected'; } ?> value="select" >Select All</option>
							<option value="2" <?php  if($select_box == 2){ echo 'selected'; } ?>>Active</option>
							<option value="3" <?php  if($select_box == 3){ echo 'selected'; } ?>>Paid Off</option>
							<option value="5" <?php  if($select_box == 5){ echo 'selected'; } ?>>Brokered</option>
						</select>
					</div>
					<div class="float-direction-left">
						Is Multi lender: <br>
						<select name="select_multi_lender" tabindex="2">
							
							<option value="3" <?php if(isset($select_multi_lender)){ if($select_multi_lender == 3){ echo 'selected'; }} ?>>Select All</option>
							<option value="2" <?php if(isset($select_multi_lender)){ if($select_multi_lender == 2){ echo 'selected'; }} ?>>Yes</option>
							<option value="1" <?php if(isset($select_multi_lender)){ if($select_multi_lender == 1){ echo 'selected'; }} ?>>No</option>
							
						</select>
					</div>
					<div class="float-direction-left"  >
						Funded Directly: <br>
						<select name="select_fund_broker" tabindex="1">
							
							<option value="1" <?php if(isset($select_fund_broker)){ if($select_fund_broker == 1){ echo 'selected'; }} ?>>Select All</option>
							<option value="2" <?php if(isset($select_fund_broker)){ if($select_fund_broker == 2){ echo 'selected'; }} ?>>Yes</option><option value="3" <?php if(isset($select_fund_broker)){ if($select_fund_broker == 3){ echo 'selected'; }} ?>>No</option>
						</select>
					
					</div>	
					
				</form>
			</div>
		</div>

		<div class="rc_class">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Borrower<br>Name</th>
					<th>Property<br>Address</th>
					<th>City</th>
					<th>State</th>
					<th>Zip</th>
					<th>Loan<br>Amount</th>
					<th>Note<br>Rate</th>
					<th>Lender Rate</th>
                     <th>Term</th>
					<th>Loan Status</th>
					<th>Closing Date</th>
					
					<!--<th>Condition</th>-->
					
					</tr>
				</thead>
				<tbody class="scrollable">
				<?php 
				
				$total_loan_amount 	= 0;
				$total_term 		= 0;
				$total_ltv 			= 0;
				$total_intrest 		= 0;
				$count 				= 0;
				$total_rate 		= 0;
				$total_lender_fee 		= 0;
					if(isset($loan_funded))
					{
						foreach($loan_funded as $key => $row)
						{
							?>
							<tr>
								<!--<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['talimar_loan']; ?></a></td>-->
								
						
								<td><a href="<?php echo base_url().'borrower_view/'.$row['b_id'];?>"><?php echo $row['borrower_name']; ?></a></td>
								<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_address']; ?></a></td>
								<td><?php echo $row['city']; ?></td>
								<td><?php echo $row['state']; ?></td>
								<td><?php echo $row['zip']; ?></td>
								<td>$<?php echo number_format($row['loan_amount']); ?></td>
								<td><?php echo number_format($row['intrest_rate'],3); ?>%</td>
								<td><?php echo $row['lender_fee']; ?>%</td>
								<td><?php echo number_format($row['term']); ?></td>
								<td><?php echo $loan_status_option[$row['loan_status']]; ?></td>
								<td><?php echo date('Y-m-d', strtotime($row['funding_date'])); ?></td>
							</tr>
							
							
							<?php
							
							$total_loan_amount 	= $total_loan_amount + $row['loan_amount'];
							$total_term 		= $total_term + $row['term'];
							$count				= $key + 1;
							$total_rate += $row['intrest_rate'];
							$total_lender_fee += $row['lender_fee'];
							
						}
					}
				?>
					
				</tbody>	
				<tfoot>
					<tr>
						<th>Total: <?php echo $count; ?></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th><?php echo '$'.number_format($total_loan_amount);?></th>
						<th></th>
						<th ></th>
						<th ></th>
						<th></th>
						<th ></th>
					</tr>
					
					<tr>
						<th>Average:</th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>					
						
						<th><?php echo '$'.number_format($total_loan_amount/$count); ?></th>
						<th><?php echo number_format($total_rate/$count,3); ?>%</th>
						<th><?php echo number_format($total_lender_fee/$count,2); ?>%</th>
						<th><?php echo number_format($total_term/$count); ?> months</th>
						<th></th> 
						<th></th> 
					</tr>
				</tfoot>
				
			</table>

			

	</div>

	</div>

	<!-- END CONTENT -->

</div>
<!--
<script src="//code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
-->
<script>
$(document).ready(function() {
    $('#table').DataTable();
} );

function loan_funded_year_form()
{
	$('#loan_funded_year_form').submit();
}

function disable_dates(that){
	
var selectbox=$(that).val();
	if(selectbox=='3'){
  $(".datepicker").prop('disabled',false);	
	}else{
		
	 $(".datepicker").prop('disabled',true);
	var selectbox='';
	}
}

function load_form(){

 $('#form_iad').submit();	

}


</script>