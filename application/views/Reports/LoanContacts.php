<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Loan Contacts</h1>	
					
				</div>
				
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>reports/LoanContacts_pdf">
						<button type="submit" class="btn blue">PDF</button>
					</form>
				</div>
				
		</div>
		
		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Borrower Name</th>
					<th>Street Address</th>
					<th>Unit #</th>
					<th>City</th>
					<th>State</th>
					<th>Zip</th>
					<th>Loan Originator</th>
					<th>Underwriter </th>
					<th>Loan Closer</th>
					<th>Loan Servicer</th>
					<th>Investor Relations</th>					
				</tr>
				</thead>
				<tbody>
				<?php
				// echo '<pre>';
				// print_r($Fetch_All_username);
				// echo '</pre>';
				
				?>
				<?php if(isset($LoanContacts) && is_array($LoanContacts)){ 
						foreach($LoanContacts as $row){ ?>
						
									<tr>
										<td><?php echo $Fetch_All_borrowername[$row->borrower];?></td>
										<td><a href="<?php echo base_url();?>load_data/<?php echo $row->loan_id;?>"><?php echo $row->property_address;?></a></td>
										<td><?php echo $row->unit;?></td>
										<td><?php echo $row->city;?></td>
										<td><?php echo $row->state;?></td>
										<td><?php echo $row->zip;?></td>
										<td><?php echo $Fetch_All_username[$row->loan_originator];?></td>
										<td><?php echo $Fetch_All_username[$row->underwriter];?></td>
										<td><?php echo $Fetch_All_username[$row->loan_closer];?></td>
										<td><?php echo $Fetch_All_username[$row->loan_servicer];?></td>
										<td><?php echo $Fetch_All_username[$row->investor_relations];?></td>
																				
									</tr>
				
								<?php  } }else{ ?>
				
					<tr>
						<td style="width:100%;" colspan="11">No data found!</td>
					</tr>
				<?php } ?>
					
				</tbody>		
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {
	 
    $('#table').DataTable();
	
});



</script>




