<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');


/*echo '<pre>';
print_r($loanBoardingData);
echo '</pre>';*/

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; YR Closing Comp</h1>	
					
				</div>
				
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>reports/YRClosingComp_pdf">
						<button type="submit" class="btn blue">PDF</button>
					</form>
				</div>
				
		</div>
		
		<!--<div class="row">

			<form method="post" action="<?php echo base_url();?>reports/ActiveOutBalance">
				
				<div class="col-md-2">
					<label><b>Loan Status:</b></label>
					<select class="form-control" name="loan_status">
						<option value="">Select All</option>
						<?php 
						unset($loan_status_option[0]);
						unset($loan_status_option[4]);
						unset($loan_status_option[6]);
						unset($loan_status_option[7]);

						foreach($loan_status_option as $key => $row){ ?>
							<option value="<?php echo $key;?>" <?php if($loan_status_val == $key){echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>

				<div class="col-md-2">
					<button class="btn btn-primary btn-sm" type="submit" name="submit" style="margin-top: 25px;"">Filter</button>
				</div>

			</form>

		</div>-->

		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th></th>
					<th colspan="2">2020</th>
					<th colspan="2">2019</th>
					<th colspan="2">Difference</th>				
				</tr>
				<tr>
					<th></th>
					<th># Funded</th>				
					<th>$ Funded</th>
					<th># Funded</th>				
					<th>$ Funded</th>
					<th># Funded</th>				
					<th>$ Funded</th>				
				</tr>
				</thead>
				<tbody>
					<tr>
						<th>January</th>
						<td><?php echo number_format($TotalCount_01_20);?></td>
						<td>$<?php echo number_format($TotalAmount_01_20);?></td>				
						<td><?php echo number_format($TotalCount_01_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_01_19);?></td>				
						<td><?php echo number_format($TotalCount_01_20 - $TotalCount_01_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_01_20 - $TotalAmount_01_19);?></td>										
					</tr>
					<tr>
						<th>February</th>
						<td><?php echo number_format($TotalCount_02_20);?></td>
						<td>$<?php echo number_format($TotalAmount_02_20);?></td>				
						<td><?php echo number_format($TotalCount_02_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_02_19);?></td>								
						<td><?php echo number_format($TotalCount_02_20 - $TotalCount_02_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_02_20 - $TotalAmount_02_19);?></td>							
					</tr>
					<tr>
						<th>March</th>
						<td><?php echo number_format($TotalCount_03_20);?></td>
						<td>$<?php echo number_format($TotalAmount_03_20);?></td>				
						<td><?php echo number_format($TotalCount_03_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_03_19);?></td>								
						<td><?php echo number_format($TotalCount_03_20 - $TotalCount_03_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_03_20 - $TotalAmount_03_19);?></td>							
					</tr>
					<tr>
						<th>April</th>
						<td><?php echo number_format($TotalCount_04_20);?></td>
						<td>$<?php echo number_format($TotalAmount_04_20);?></td>				
						<td><?php echo number_format($TotalCount_04_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_04_19);?></td>								
						<td><?php echo number_format($TotalCount_04_20 - $TotalCount_04_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_04_20 - $TotalAmount_04_19);?></td>								
					</tr>
					<tr>
						<th>May</th>
						<td><?php echo number_format($TotalCount_05_20);?></td>
						<td>$<?php echo number_format($TotalAmount_05_20);?></td>				
						<td><?php echo number_format($TotalCount_05_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_05_19);?></td>								
						<td><?php echo number_format($TotalCount_05_20 - $TotalCount_05_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_05_20 - $TotalAmount_05_19);?></td>								
					</tr>
					<tr>
						<th>June</th>
						<td><?php echo number_format($TotalCount_06_20);?></td>
						<td>$<?php echo number_format($TotalAmount_06_20);?></td>				
						<td><?php echo number_format($TotalCount_06_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_06_19);?></td>								
						<td><?php echo number_format($TotalCount_06_20 - $TotalCount_06_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_06_20 - $TotalAmount_06_19);?></td>												
					</tr>
					<tr>
						<th>July</th>
						<td><?php echo number_format($TotalCount_07_20);?></td>
						<td>$<?php echo number_format($TotalAmount_07_20);?></td>				
						<td><?php echo number_format($TotalCount_07_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_07_19);?></td>								
						<td><?php echo number_format($TotalCount_07_20 - $TotalCount_07_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_07_20 - $TotalAmount_07_19);?></td>													
					</tr>
					<tr>
						<th>August</th>
						<td><?php echo number_format($TotalCount_08_20);?></td>
						<td>$<?php echo number_format($TotalAmount_08_20);?></td>				
						<td><?php echo number_format($TotalCount_08_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_08_19);?></td>								
						<td><?php echo number_format($TotalCount_08_20 - $TotalCount_08_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_08_20 - $TotalAmount_08_19);?></td>													
					</tr>
					<tr>
						<th>September</th>
						<td><?php echo number_format($TotalCount_09_20);?></td>
						<td>$<?php echo number_format($TotalAmount_09_20);?></td>				
						<td><?php echo number_format($TotalCount_09_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_09_19);?></td>								
						<td><?php echo number_format($TotalCount_09_20 - $TotalCount_09_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_09_20 - $TotalAmount_09_19);?></td>													
					</tr>
					<tr>
						<th>October</th>
						<td><?php echo number_format($TotalCount_10_20);?></td>
						<td>$<?php echo number_format($TotalAmount_10_20);?></td>				
						<td><?php echo number_format($TotalCount_10_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_10_19);?></td>								
						<td><?php echo number_format($TotalCount_10_20 - $TotalCount_10_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_10_20 - $TotalAmount_10_19);?></td>													
					</tr>
					<tr>
						<th>November</th>
						<td><?php echo number_format($TotalCount_11_20);?></td>
						<td>$<?php echo number_format($TotalAmount_11_20);?></td>				
						<td><?php echo number_format($TotalCount_11_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_11_19);?></td>								
						<td><?php echo number_format($TotalCount_11_20 - $TotalCount_11_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_11_20 - $TotalAmount_11_19);?></td>							
					</tr>
					<tr>
						<th>December</th>
						<td><?php echo number_format($TotalCount_12_20);?></td>
						<td>$<?php echo number_format($TotalAmount_12_20);?></td>				
						<td><?php echo number_format($TotalCount_12_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_12_19);?></td>								
						<td><?php echo number_format($TotalCount_12_20 - $TotalCount_12_19);?></td>				
						<td>$<?php echo number_format($TotalAmount_12_20 - $TotalAmount_12_19);?></td>							
					</tr>
				</tbody>
				<?php

					$count2020 = $TotalCount_01_20 + $TotalCount_02_20 + $TotalCount_03_20 + $TotalCount_04_20 + $TotalCount_05_20 + $TotalCount_06_20 + $TotalCount_07_20 + $TotalCount_08_20 + $TotalCount_09_20 + $TotalCount_10_20 + $TotalCount_11_20 + $TotalCount_12_20;

					$amount2020 = $TotalAmount_01_20 + $TotalAmount_02_20 + $TotalAmount_03_20 + $TotalAmount_04_20 + $TotalAmount_05_20 + $TotalAmount_06_20 + $TotalAmount_07_20 + $TotalAmount_08_20 + $TotalAmount_09_20 + $TotalAmount_10_20 + $TotalAmount_11_20 + $TotalAmount_12_20;

					$count2019 = $TotalCount_01_19 + $TotalCount_02_19 + $TotalCount_03_19 + $TotalCount_04_19 + $TotalCount_05_19 + $TotalCount_06_19 + $TotalCount_07_19 + $TotalCount_08_19 + $TotalCount_09_19 + $TotalCount_10_19 + $TotalCount_11_19 + $TotalCount_12_19;

					$Amount2019 = $TotalAmount_01_19 + $TotalAmount_02_19 + $TotalAmount_03_19 + $TotalAmount_04_19 + $TotalAmount_05_19 + $TotalAmount_06_19 + $TotalAmount_07_19 + $TotalAmount_08_19 + $TotalAmount_09_19 + $TotalAmount_10_19 + $TotalAmount_11_19 + $TotalAmount_12_19;
				?>
				<tfoot>
					<tr>
						<th>Total:</th>
						<th><?php echo number_format($count2020);?></th>				
						<th>$<?php echo number_format($amount2020,2);?></th>				
						<th><?php echo number_format($count2019);?></th>				
						<th>$<?php echo number_format($Amount2019,2);?></th>				
						<th><?php echo number_format($count2020 - $count2019);?></th>				
						<th>$<?php echo number_format($amount2020 - $Amount2019,2);?></th>	
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {
	 
    $('#table').DataTable({
    	"aaSorting": false,
    	"lengthMenu": [ 25, 50, 75, 100 ],
    });
	
});



</script>




