<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
			<div class="page-title">
				<h1> &nbsp; Fix & Flip Portfolio</h1>	
			</div>

			<div class="top_download">					
				<form action="<?php echo base_url();?>ReportData/fix_flip_portfolio" method="POST">
					<input type="hidden" name="reportDisplay" value="pdf">
					<button class="btn blue" type="submit">PDF</button>
				</form>
			</div>				
		</div>
		
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						<th>City</th>
						<th>State</th>
						<th>Loan Amount</th>
						<th>Rate</th>
						<th>LTV</th>
						<th>% of<br>Purchase</th>
						<th>% of<br>Renovation</th>
						<th>Loan #</th>
						<th>Property<br>Details</th>
					</tr>

				</thead>
				
				<tbody>
				<?php if(isset($fix_flip_portfolio) && is_array($fix_flip_portfolio)){
						foreach($fix_flip_portfolio as $row){ 

							if($row['property_link'] != ''){
								$linkurl = $row['property_link'];
							}else{
								$linkurl = '#';
							}
					?>

					<tr>
						<td><?php echo $row['city'];?></td>
						<td><?php echo $row['state'];?></td>
						<td>$<?php echo number_format($row['loan_amount'],2);?></td>
						<td><?php echo number_format($row['lender_ratessss'],3);?>%</td>
						<td><?php echo number_format($row['ltv'],2);?>%</td>
						<td><?php echo number_format($row['puchase'],2);?>%</td>
						<td><?php echo number_format($row['construc'],2);?>%</td>
						<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['talimar_loan'];?></a></td>
						<td><a href="<?php echo $linkurl;?>">Link</a></td>
					</tr>
				<?php } }else{ ?>
					<tr>
						<td colspan="9">No data found!</td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>

	</div>

</div>

<script>

$(document).ready(function() {
    $('#table').DataTable();
});
</script>
