<?php
$construction_status_option = $this->config->item('construction_status_option');
// $construction_status_option = $this->config->item('loan_type_option');
error_reporting(0);
// echo "<pre>";

// print_r($fetch_construction_loan);

// echo "</pre>";

?>
<div class="page-content-wrapper">

	<div class="page-content responsive">

		<div class="page-head">

				<!-- BEGIN PAGE TITLE -->

				<div class="page-title">

					<h1> &nbsp; Upcoming Payoff Schedule</h1>	

				</div>

				<div class="talimar_no_dropdowns">
					<a href="<?php echo base_url().'upcoming_payoff_schedule_download'; ?>"><button class="btn blue">PDF</button></a>
				</div>

		</div>

		<div class="rc_class">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >

				<thead>

					<tr>

						<th>Borrower Name</th>
						<th>TaliMar #</th>
						<th>Loan Amount</th>
						<!-- <th>Payoff Date</th> -->
						<th>Demand Exp Date</th>
						
						<th>Property&nbsp;Address </th>
						<th>Unit</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
					</tr>

				</thead>

				<tbody>
				 <?php
				 $count = 0;
				 $total_loan_amount = 0;
				 if(isset($upcoming_payoff_schedule))
				 {
					 foreach($upcoming_payoff_schedule as $row)
					 {
						?>
						<tr>
							<td><?php echo $row['borrower_name']; ?></td>
							<td><a href="<?php echo base_url()."load_data/".$row['loan_id'];?>"><?php echo $row['talimar_loan']; ?></a></td>
							<td>$<?php echo number_format($row['loan_amount'],2); ?></td>
							<td><?php echo $row['demand_requested_date'] ? date('m-d-Y',strtotime($row['demand_requested_date'])) : ''; ?></td>
							<td><?php echo $row['property_address']; ?></td>
							<td><?php echo $row['unit']; ?></td>
							<td><?php echo $row['city']; ?></td>
							<td><?php echo $row['state']; ?></td>
							<td><?php echo $row['zip']; ?></td>
						</tr>
						<?php
							$count = $count + 1;
							$total_loan_amount = $total_loan_amount+$row['loan_amount'];
					 }
				 }
				 
				 ?>
				</tbody>
				
				<tfoot>
					<tr>
						<th>Total:</th>
						<th><?php echo $count; ?></th>
						<th><?php echo '$'.number_format($total_loan_amount,2); ?></th>
						<th colspan="6"></th>
					</tr>
					<tr>
						<th>Average:</th>
						<th></th>
						<th><?php echo '$'.number_format($total_loan_amount/$count,2); ?></th>
						<th colspan="6"></th>
					</tr>
				</tfoot>
			</table>
	</div>

	</div>

	<!-- END CONTENT -->

</div>

<script>
$(document).ready(function() {
    $('#table').DataTable({
		"sorting":false,
	});
});


</script>