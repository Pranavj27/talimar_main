<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');
$not_applicable_option = $this->config->item('not_applicable_option');
$yes_no_option3 = $this->config->item('no_yes_option');


?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; TD E-Mail Check</h1>	
					
				</div>
				
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>ReportData/TDEmail_pdf">
						<input type="hidden" name="trust_deeds" value="<?php echo isset($trust_deed) ? $trust_deed : '';?>">
						<button type="submit" class="btn blue">PDF</button>
					</form>
				</div>
				
		</div>
		
		<div class="row">
			<form method="post" action="<?php echo base_url();?>ReportData/TDEmail">
				<div class="col-md-2">
					<label>Trust Deed Investor:</label>
					<select class="form-control" name="trust_deed">
						<option value="">Select All</option>
						<?php foreach($yes_no_option3 as $key => $row){ ?>
							<option value="<?php echo $key;?>" <?php if($trust_deed == $key){echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-1">
					<button type="submit" name="submit" class="btn btn-sm btn-primary" style="margin-top: 25px;">Filter</button>
				</div>
			</form>
		</div>
		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Name</th>
					<th>Phone</th>
					<th>E-Mail</th>
					<th>Trust Deed Investor</th>				
				</tr>
				</thead>
				<tbody>
					<?php

					$count = 0;
					$loanTotal = 0;
					$currentTotal = 0;
					$monthlyTotal = 0;

					if(isset($TDEmailData) && is_array($TDEmailData)){ 
						foreach($TDEmailData as $row){ 	$count++; ?>
						
							<tr>
								<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['id'];?>"><?php echo $row['name'];?></a></td>								
								<td><?php echo $row['phone'];?></td>					
								<td><?php echo $row['email'];?></td>				
								<td><?php echo $row['TDinvertor'];?></td>											
							</tr>
				
						<?php } }else{ ?>
				
					<tr>
						<td style="width:100%;">No data found!</td>
					</tr>
				<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<th>Total: <?php echo $count;?></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
				</tfoot>		
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {
	 
    $('#table').DataTable();
	
});



</script>




