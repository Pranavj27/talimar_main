<?php

$loan_status_option 	= $this->config->item('loan_status_option');
$loan_type_option 		= $this->config->item('loan_type_option');
$property_type_option 	= $this->config->item('property_modal_property_type');
$borrower_type_option 	= $this->config->item('borrower_type_option');
$transaction_type_option 	= $this->config->item('transaction_type_option');
$template_yes_no_option 	= $this->config->item('template_yes_no_option');


?>

<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<div class="page-title">
					<h1> &nbsp; Investor Tape </h1>	
				</div>

				<div class="top_download">					
					<form action="<?php echo base_url();?>investor_tape_excel" method="POST">
						<input type="hidden" name="loan_status" value="<?php echo isset($loan_status_val) ? $loan_status_val : '';?>">
						<input type="hidden" name="loan_purpose" value="<?php echo isset($loan_purpose_val) ? $loan_purpose_val : '';?>">
						<input type="hidden" name="pertinent_date" value="<?php echo isset($pertinent_date) ? $pertinent_date : '';?>">
						<button class="btn blue" type="submit">Excel</button>
					</form>
				</div>				
		</div>
		<div class="row">
			<form method="POST" action="<?php echo base_url();?>investor_tape">
				<div class="col-md-2">
					<label>Loan Status:</label>
					<select class="form-control" name="loan_status">
						<option value="" <?php if($loan_status_val == ''){echo 'selected';}?>>Select All</option>
						<option value="1" <?php if($loan_status_val == 1){echo 'selected';}?>>Pipeline + Term Sheet</option>
						<option value="2" <?php if($loan_status_val == 2){echo 'selected';}?>>Active</option>
						<option value="3" <?php if($loan_status_val == 3){echo 'selected';}?>>Paid Off</option>
					</select>
				</div>

				<div class="col-md-2">
					<label>Loan Purpose:</label>
					<select class="form-control" name="loan_purpose">
						<?php foreach($loan_type_option as $key => $row){?>
							<option value="<?php echo $key;?>" <?php if($loan_purpose_val == $key){echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-2">
					<label>Pertinent Date:</label>
					<select class="form-control" name="pertinent_date">
						<?php foreach($template_yes_no_option as $key => $row){?>
							<option value="<?php echo $key;?>" <?php if($pertinent_date == $key){echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>
				<button type="submit" name="submit" class="btn blue" style="margin-top:25px;">Filter</button>
			</form>
		</div>
		

		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						<th>Loan #</th>
						<th>Loan Status</th>
						<th>Originator</th>
						<th>Street Address</th>
						<th>Unit #</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
						<th>Purpose</th>
						<th>Transaction<br>Type</th>
						<th>Property Type</th>
						<th>Term</th>
						<th>Interest<br>Rate</th>
						<th>Seller Points</th>
						<th>Seller Spread</th>
						<th>Purchase Rate</th>
						<th>Origination<br>Date</th>
						<th>Maturity Date</th>
						<th>Property<br>Acquisition Cost</th>
						<th>Total<br>Loan Amount</th>
						<th>Initial<br>Draw Amount</th>
						<th>Rehab<br>Holdback Amount</th>
						<th>Current Unpaid<br>Principal Balance</th>
						<th>After<br>Repair Value</th>
						<th>As-Is<br>Appraisal</th>
						<th>As Repaired</th>
						<th>Loan<br>to Cost</th>
						<th>Mortgager<br>Experience</th>
						<th>FICO</th>
						<th>Mortgage<br>Type</th>
						
						
					</tr>

				</thead>
				
				<tbody>
				<?php foreach($array_all_investor_tape as $row){
					$total_cost = '';
					if($row['loan_type'] == '1'){
						$total_costs = 'N/A';
					}else{

						$project_cost_total = $row['project_cost_total'] ? $row['project_cost_total'] : '1';
						$total_cost	= $row['loan_amount']/$project_cost_total;
						$total_costs = number_format($total_cost/100,2).'%';
					}

					
					
				 ?>
					<tr>
						<td><a><?php echo $row['talimar_loan'];?><a/></td>
						<td><?php echo $loan_status_option[$row['loan_status']];?></td>
						<td>TaliMar</td>

						<?php if($pertinent_date == 2){ echo '<td></td>';}else{?>
							<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['address'];?></a></td>
						<?php } ?>
						<td><?php echo $row['unit'];?></td>
						<td><?php echo $row['city'];?></td>
						<td><?php echo $row['state'];?></td>
						<td><?php echo $row['zip'];?></td>
						<td><?php echo $loan_type_option[$row['loan_type']];?></td>
						<td><?php echo $row['transaction_type'] ? $transaction_type_option[$row['transaction_type']] : '';?></td>
						<td><?php echo $row['property_type'] ? $property_type_option[$row['property_type']] : '';?></td>
						<td><?php echo $row['term_month'];?> Month</td>
						<td><?php echo number_format($row['intrest_rate'],3);?>%</td>
						<td><?php echo $row['lender_fee'];?>%</td>
						<td><?php echo number_format($row['service_spread'],2);?>%</td>
						<td><?php echo number_format($row['lender_ratesss'],3);?>%</td>
						<td><?php echo date('m-d-Y', strtotime($row['loan_funding_date']));?></td>
						<td><?php echo date('m-d-Y', strtotime($row['maturity_date']));?></td>
						<td>$<?php echo number_format($row['total_puchase'],2);?></td>
						<td>$<?php echo number_format($row['loan_amount'],2);?></td>
						<td>$<?php echo number_format($row['draw_first_amount'],2);?></td>
						<td>$<?php echo $row['total_constructione'] ? number_format($row['total_constructione'],2) : '0.00';?></td>
						<td>$<?php echo number_format($row['current_balance'],2);?></td>
						<td></td>
						<td>$<?php echo $row['as_value_amount'] ? number_format($row['as_value_amount'],2) : '0.00';?></td>
						<td></td>
						<td><?php echo $total_costs;?></td>
						<td></td>
						<td><?php echo $row['credit_score'] ? $row['credit_score'] : ''?></td>
						<td><?php echo $row['borrower_type'] ? $borrower_type_option[$row['borrower_type']] : ''?></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>

	</div>

</div>

<script>

$(document).ready(function() {
    $('#table').DataTable();
});
</script>
