<?php

error_reporting(0);
$serviceing_reason_option = $this->config->item('serviceing_reason_option');
$reason_for_default = $this->config->item('reason_for_default');
// echo "<pre>";

// print_r($trust_deed_schedule);

// echo "</pre>";

?>
<div class="page-content-wrapper">

	<div class="page-content responsive">

		<div class="page-head">

				<!-- BEGIN PAGE TITLE -->

				<div class="page-title">

					<h1> &nbsp; Loan Default Schedule  </h1>	

					

				</div>

				<div class="top_download">

					<a href="<?php echo base_url();?>download_loan_default">

					<button  class="btn blue">PDF</button>

					</a>
					

				</div>

		</div>

		<div class="rc_class">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >

				<thead>

					<tr>

						<th>Loan Number</th>

						<th>Loan Amount</th>

						<th>Borrower Name</th>

						<th>Property Address </th>

						<th>Property City</th>
						
						<th>Property State</th>
						
						<th>Property Zip</th>
						
						<th>Reason</th>

					</tr>

				</thead>

				<tbody>
					
				<?php
				$number = 0;
					$total_loan_amount = 0;
					foreach($default_schedule as $key => $row_data)
					{
						$total_loan_amount = $total_loan_amount + $default_schedule[$key]['loan_amount'];
						?>
						<tr>
						<td>
							<a href ="<?php echo base_url().'load_data/'.$default_schedule[$key]['loan_id']; ?>">
							<?php echo $default_schedule[$key]['talimar_loan']; ?></a>
						</td>
						<td>$<?php echo number_format($default_schedule[$key]['loan_amount']); ?></td>
						<td><?php echo $default_schedule[$key]['borrower']; ?></td>
						<td><?php echo $default_schedule[$key]['property_address']; ?></td>
						<td><?php echo $default_schedule[$key]['city']; ?></td>
						<td><?php echo $default_schedule[$key]['state']; ?></td>
						<td><?php echo $default_schedule[$key]['zip']; ?></td>
						
						<td><?php echo $reason_for_default[$default_schedule[$key]['reason']]; ?></td>
						</tr>
						<?php
						$number++;
					}
				
					?>

				</tbody>
				
				<tfoot>
					<tr>
						<th>Total: <?php echo $number; ?></th>
						<th><?php echo '$'.number_format($total_loan_amount); ?></th>
						<th colspan="6"></th>
					</tr>
				</tfoot>

			</table>

			

	</div>

	</div>

	<!-- END CONTENT -->

</div>
<script>
$(document).ready(function() {
    $('#table').DataTable();
} );
</script>