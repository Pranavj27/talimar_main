<?php

error_reporting(0);

// echo "<pre>";

// print_r($fci_service_loan);

// echo "</pre>";

?>

<div class="page-content-wrapper">

	<div class="page-content responsive">

		<div class="page-head">

				<!-- BEGIN PAGE TITLE -->

				<div class="page-title">

					<h1> &nbsp; FCI Serviced Accounts </h1>	

					

				</div>

				<div class="top_download">
					<!--
					<a href="<?php echo base_url();?>download_fci_serviced_account">

					<button  class="btn blue">Excel Download</button>

					</a>
					-->
					
					<a href="<?php echo base_url();?>download_fci_serviced_pdf">

					<button  class="btn blue">PDF</button>

					</a>

				</div>

		</div>

		<div class="rc_class">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >

				<thead>

					<tr>

						<th>Loan Number</th>
						
						<th>Servicer #</th>
						
						<th style="width:20%;">Borrower</th>
						
						<th>Loan Amount</th>
						
						<th>Interest Rate</th>


						<th style="width:20%;">Address</th>


						<th>City</th>
						
						<th>State</th>


						<th>Zip</th>

						<th>Loan Status</th>

						

					</tr>

				</thead>
				
				<tbody>
				<?php
				$create_loanamount = 0;
				if(isset($fci_service_loan)){
				foreach($fci_service_loan as $key => $row_data)
				{
				?>
					<tr>
						<td>
							<a href="<?php echo base_url().'load_data/'.$fci_service_loan[$key]['loan_id'];?>">
							<?php echo $fci_service_loan[$key]['talimar_loan']; ?></a>
						</td>
						<td><?php echo $fci_service_loan[$key]['fci']; ?></td>
						<td><?php echo $fci_service_loan[$key]['borrower']; ?></td>
						<td>$<?php echo number_format($fci_service_loan[$key]['loan_amount'],2); ?></td>
						<td><?php echo number_format($fci_service_loan[$key]['intrest_rate'],3); ?>%</td>
						<td><?php echo $fci_service_loan[$key]['property_address']; ?></td>
						<td><?php echo $fci_service_loan[$key]['city']; ?></td>
						<td><?php echo $fci_service_loan[$key]['state']; ?></td>
						<td><?php echo $fci_service_loan[$key]['zip']; ?></td>
						<td>Active</td>
					</tr>
					<?php
					$countnumber = $key;
					$create_loanamount = $create_loanamount + $fci_service_loan[$key]['loan_amount'];
					?>
				<?php 
					}
					}				
					?>
				</tbody>
				<tfoot>
					<tr>
						<th style="text-align:left;" ><?php echo $countnumber+1; ?></th>
						<td></td>
						<td></td>
						<th style="text-align:left;" ><?php echo '$'.number_format($create_loanamount,2); ?></th>

						<td colspan="6"></td>
					</tr>
				</tfoot>

			</table>

			

	</div>

	</div>

	<!-- END CONTENT -->

</div>

<script>
$(document).ready(function() {
    $('#table').DataTable();
} );
</script>