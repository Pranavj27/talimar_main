<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');
$no_yes_option = $this->config->item('no_yes_option');
$nno_yes_option = $this->config->item('nno_yes_option');


?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Renovation Reserve</h1>	
					
				</div>
				
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>ReportData/renovation_reserve_pdf">
						<input type="hidden" name="loan_status" value="<?php echo isset($loan_status) ? $loan_status : '';?>">
						<input type="hidden" name="cons_reserve" value="<?php echo isset($cons_reserve) ? $cons_reserve : '';?>">
						<button type="submit" class="btn blue">PDF</button>
					</form>
				</div>
				
		</div>
		
		<div class="row">
			<form method="POST" action="<?php echo base_url();?>ReportData/renovation_reserve">
				<div class="col-md-2">
					<label>Loan Status:</label>
					<select name="loan_status" class="form-control">
                        <option value="">Select All</option>
                        <?php
                        unset($loan_status_option[0]);
                        unset($loan_status_option[4]);
                        unset($loan_status_option[7]);
                         foreach($loan_status_option as $key => $row){ ?>
                        	<option value="<?php echo $key;?>" <?php if($loan_status == $key){echo 'selected';}?>><?php echo $row;?></option>
                        <?php } ?>
                    </select>
				</div>
				<div class="col-md-3">
					<label>Construction Reserve Account:</label>
					<select name="cons_reserve" class="form-control">
                        <option value="">Select All</option>
                        <?php
                         foreach($nno_yes_option as $key => $row){ ?>
                        	<option value="<?php echo $key;?>" <?php if($cons_reserve == $key){echo 'selected';}?>><?php echo $row;?></option>
                        <?php } ?>
                    </select>
				</div>
				<div class="col-md-2">
					<button type="submit" name="submit" class="btn btn-primary btn-sm" style="margin-top: 25px;">Filter</button>
				</div>
				
			</form>
		</div>
		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Street Address</th> 
					<th>Unit #</th>
					<th>City</th>
					<th>State</th>
					<th>Zip</th>
					<th>Reserve Account</th>
					<th>Loan Reserve 1 Balance</th>
					<th>Loan Reserve 2 Balance</th>					
				</tr>
				</thead>
				<tbody>
					<?php 
					$count = 0;
					$amount1 = 0;
					$amount2 = 0;

					if(isset($renovation_reservedata) && is_array($renovation_reservedata)){ 
					
							foreach($renovation_reservedata as $row){ 

								$count++;
								$amount1 += $row['draw1'];	
								$amount2 += $row['draw2'];	

					?>
									<tr>
										<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['property_address'];?></a></td>
										<td><?php echo $row['unit'];?></td>
										<td><?php echo $row['city'];?></td>
										<td><?php echo $row['state'];?></td>
										<td><?php echo $row['zip'];?></td>
										<td><?php echo $nno_yes_option[$row['draws']];?></td>
										<td>$<?php echo number_format($row['draw1']);?></td>
										<td>$<?php echo number_format($row['draw2']);?></td>
									</tr>
					<?php } }else{ ?>
							<tr>
								<td colspan="8">No data found!</td>
							</tr>
					<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<th>Total: <?php echo $count;?></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th>$<?php echo number_format($amount1);?></th>
						<th>$<?php echo number_format($amount2);?></th>
					</tr>
				</tfoot>		
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {
	 
    $('#table').DataTable({

    	//"aaSorting":false,
    });
	
});

</script>




