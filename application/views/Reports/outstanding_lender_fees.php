<?php
error_reporting(0);


$yes_no_option2 	= $this->config->item('yes_no_option2');
$loan_type_option 	= $this->config->item('loan_type_option');
$loan_status_option = $this->config->item('loan_status_option');
$yes_no_option3 = $this->config->item('no_yes_option');

$select_status_val = isset($select_status) ? '('.$loan_status_option[$select_status].')' : '' ;
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1> &nbsp; Lender Fee Schedule<?php// echo $select_status_val;?></h1>	
					
			</div>
			<div class="top_download">
				<form method="POST" action="<?php echo base_url();?>Outstanding_lender_fees_download">
					<input type="hidden" name="select_status" value="<?php echo isset($select_status) ? $select_status : '';?>">
					<input type="hidden" name="fees_received" value="<?php echo isset($fees_received) ? $fees_received : '';?>">
					<input type="hidden" name="term_sheet" value="<?php echo isset($term_sheet) ? $term_sheet : '';?>">
					<button type="submit" class="btn blue">PDF</button>
				</form>
			</div>
		</div>
		
		<div class="row" style="margin:10px 0px 10px 0px !important;">
		  <form id="lender_status" action="<?php echo base_url();?>Outstanding_lender_fees" method="post">
			<div class="col-md-3">
				<label>Loan Status:</label>
					
					<select class="form-control" name="select_status" onchange="form_lender_status();">

						<?php foreach($loan_status_option as $key => $row){ ?>
							<option value="<?php echo $key;?>" <?php if($key == $select_status){echo 'Selected';}?>><?php echo $row;?></option>
						<?php } ?>						
					</select>						
			</div>
			<div class="col-md-3">
				<label>Fees Received:</label>
					<select class="form-control" name="fees_received" onchange="select_fee_received(this.value);">
						<option value="0" <?php if($fees_received == 0){echo 'Selected';}?>>Select All</option>	
						<option value="2" <?php if($fees_received == 2){echo 'Selected';}?>>No </option>	
											
						<option value="1" <?php if($fees_received == 1){echo 'Selected';}?>>Yes </option>						
					</select>						
			</div>
			<div class="col-md-3">
				<label>Term Sheet Signed:</label>
					<select class="form-control" name="term_sheet" onchange="select_fee_received(this.value);">
					<option value="">Select All</option>
						<?php foreach($yes_no_option3 as $key => $row){ ?>
							<option value="<?php echo $key;?>" <?php if($key == $term_sheet){echo 'Selected';}?>><?php echo $row;?></option>
						<?php } ?>							
					</select>						
			</div>
		  </form>
		</div>
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					
					<th>Borrower Name</th>
					<th>Street Address</th>
					<th>Unit#</th>
					<th>City</th>
					<th>State</th>
					<th>Zip</th>
					<th>Loan Status</th>
					<th>Lender Fees</th>
					<th>Loan Amount</th>
					<th>Closing Date</th>
					
				</tr>
				</thead>
				<tbody>
				<?php
				$count = 0;
				$lender_fee = 0;
				$loan_amount = 0;
				foreach($outstanding_lender_fee as $row){ ?>
				<tr>
				
					<td><?php echo $row['borrower'];?></td>
					<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['street'];?></a></td>
					<td><?php echo $row['unit'];?></td>
					<td><?php echo $row['city'];?></td>
					<td><?php echo $row['state'];?></td>
					<td><?php echo $row['zip'];?></td>
					<td><?php echo $loan_status_option[$row['loan_status']];?></td>
					<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>#closing_statement_new11">$<?php echo number_format($row['lender_fee'],2);?></a></td>
					<td>$<?php echo number_format($row['loan_amount'],2);?></td>
					<td><?php echo $row['closing_date'] ? date('m-d-Y',strtotime($row['closing_date'])) : ''; ?></td>
					
				</tr>	
				<?php

					$count++;
					$lender_fee += $row['lender_fee'];
					$loan_amount += $row['loan_amount'];

				 } ?>
				</tbody>				
				</tfoot>
					<tr>
						<th style="text-align:left">Total:</th>
						<th style="text-align:left"><?php echo $count;?></th>
						<th colspan="5"></th>
						<th>$<?php echo number_format($lender_fee,2);?></th>
						<th>$<?php echo number_format($loan_amount,2);?></th>
						<th></th>
						
					</tr>
					<tr>
						<th style="text-align:left">Average:</th>
						<th colspan="6"></th>
						<th>$<?php echo number_format($lender_fee/$count,2);?></th>
						<th>$<?php echo number_format($loan_amount/$count,2);?></th>
						<th></th>
						
					</tr>
				</tfoot>				
				
			</table>
		</div>
	</div>
	
	
	
	
</div>
<script>
// $(document).ready(function() {
    // $('#table').DataTable({
        // "order": [[ 9, "asc" ]]
    // });
   
// } );
$(document).ready(function() {
    $('#table').DataTable({
		"aaSorting": [],
	});
	
} );
function form_lender_status(){
	
	$('form#lender_status').submit();
}

function select_fee_received(){
	
	$('form#lender_status').submit();
}
</script>