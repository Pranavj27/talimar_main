<?php

$loan_status_option 	= $this->config->item('loan_status_option');
$loan_type_option 		= $this->config->item('loan_type_option');
$property_type_option 	= $this->config->item('property_modal_property_type');
$borrower_type_option 	= $this->config->item('borrower_type_option');
$transaction_type_option 	= $this->config->item('transaction_type_option');
$template_yes_no_option 	= $this->config->item('template_yes_no_option');
$closing_status_option 	= $this->config->item('closing_status_option');
$dates_array=[
			 
			 1=>"Custom",
			 2=>"This Month",
			 3=>"Last Month",
			 4=>"This Quarter",
			 5=>"Last Quarter",
			 6=>"This Year",
			 7=>"Last Year",
			 ""=>"All Dates",
];

?>

<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<div class="page-title">
					<h1> &nbsp; Loan Income</h1>	
				</div>

				<div class="top_download">					
					<form action="<?php echo base_url();?>loan_income_report_pdf" method="POST">
						<input type="hidden" name="loan_status" value="<?php echo isset($loan_status_val) ? $loan_status_val : '';?>">
						<input type="hidden" name="closing_status" value="<?php echo isset($closing_status_val) ? $closing_status_val : '';?>">
						<input type="hidden" name="dates" value="<?php echo isset($dates) ? $dates : '';?>">
						<input type="hidden" name="ends" value="<?php echo isset($end) ? $end : '';?>">
						<input type="hidden" name="starts" value="<?php echo isset($start) ? $start : '';?>">
						<button class="btn red" type="submit">PDF</button>
					</form>
				</div>				
		</div>
		<div class="row">
			<form method="POST" action="<?php echo base_url();?>loan_income_report">
				<div class="col-md-2">
					<label>Loan Status:</label>
					<select class="form-control" name="loan_status">
						<option value="" <?php if($loan_status_val == ''){echo 'selected';}?>>Select All</option>
						<option value="1" <?php if($loan_status_val == 1){echo 'selected';}?>>Pipeline</option>
						<option value="2" <?php if($loan_status_val == 2){echo 'selected';}?>>Active</option>
						<option value="3" <?php if($loan_status_val == 3){echo 'selected';}?>>Paid Off</option>
					</select>
				</div>
				<div class="col-md-2">
					<label>Closing Status:</label>
					<select class="form-control" name="closing_status">
						<?php foreach($closing_status_option as $key => $row){
							$selected = '';
							if($closing_status_val == $key){
								$selected = 'Selected';
							}else{
								$selected = '';
							}

								echo '<option value="'.$key.'" '.$selected.'>'.$row.'</option>';
						} ?>
					</select>
				</div>

				<div class="col-md-2">
					<label>Date:</label>
					<select name="dates" class="form-control" id="dates" onchange="date_filters(this)">
					 <?php foreach($dates_array as $key => $row){
                       ?>
						<option value="<?php echo $key;?>" <?php if($key == $dates){echo 'Selected';}?>><?php echo $row;?></option>
					 <?php }?>
				    </select>
				</div>
					<div class="col-md-2 b" >
						
						<label>Start Date</label>
						 <input  class="form-control datepicker" name="start" placeholder="MM/DD/YYYY" value="<?php echo isset($start) ? $start : ''; ?>" autocomplete="off">
					</div>

					<div class="col-md-2 b">
						
					<label>End Date</label>
						<input  class="form-control datepicker" name="end" placeholder="MM/DD/YYYY" value="<?php echo isset($end) ? $end : ''; ?>" autocomplete="off">
					</div>
					
				<button type="submit" name="submit" class="btn blue" style="margin-top:25px;">Filter</button>
			</form>
		</div>
		

		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						
						<th>Street Address</th>
						<th>Unit #</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
						<th>Loan Amount</th>
						<th>Origination<br>Fee</th>
						<th>Documentation<br>Fee</th>
						<th>Monthly Servicing<br>Income</th>
						<th>Total Fees</th>
					
						
					</tr>

				</thead>
				
				<tbody>
					
				<?php 

				
				if(isset($array_all_income)){
				$count=0;
				$t_origination_fee=0;
				$t_d_fee=0;
				$total_fte=0;
				$net_service=0;
				$t_loan_amount=0;
				foreach($array_all_income as $row){
				 

				 ?>
					<tr>
						
						<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['address'];?></a></td>
						<td><?php echo $row['unit'];?></td>
						<td><?php echo $row['city'];?></td>
						<td><?php echo $row['state'];?></td>
						<td><?php echo $row['zip'];?></td>
						<td>$<?php echo number_format($row['loan_amount']);?></td>
						<td>$<?php echo number_format($row['origination_fee'],2);?></td>
						<td>$<?php echo number_format($row['documentation_fee'],2);?></td>
						<td>$<?php echo number_format($row['net_service'],2);?></td>
						<td>$<?php echo number_format($row['total_fee'],2);?></td>
						
					
					</tr>
				<?php
				 $count++;
				 $t_origination_fee +=$row['origination_fee'];
				 $t_d_fee +=$row['documentation_fee'];
				 $total_fte +=$row['total_fee'];
				 $net_service +=$row['net_service'];
				 $t_loan_amount +=$row['loan_amount'];


				  }} ?>
				</tbody>
				<tfoot>
					<tr>
						<th>Total: <?php echo $count; ?></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th><?php echo '$'.number_format($t_loan_amount);?></th>
						<th><?php echo '$'.number_format($t_origination_fee,2);?></th>
						<th><?php echo '$'.number_format($t_d_fee,2);?></th>
						<th><?php echo '$'.number_format($net_service,2);?></th>
						<th><?php echo '$'.number_format($total_fte,2);?></th>
						
					</tr>
					<tr>
						<th>Average:</th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th><?php echo '$'.number_format($t_loan_amount/$count,2);?></th>
						<th><?php echo '$'.number_format($t_origination_fee/$count,2);?></th>
						<th><?php echo '$'.number_format($t_d_fee/$count,2);?></th>
						<th><?php echo '$'.number_format($net_service/$count,2);?></th>
						<th></th>
						
					</tr>
					
					
				</tfoot>
			</table>
		</div>

	</div>

</div>

<script>

$(document).ready(function() {
    $('#table').DataTable();
});


$(document).ready(function() {
  

   date_filters('#dates');
});

function date_filters(that) {

	var select_id = $(that).val();
	if (select_id == 1) {

		$('input[name="start"]').val('');
		$('input[name="end"]').val('');
		$('div.b').show();
	} else if (select_id == 2) {

		var date = new Date();
		var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
		var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
		$('input[name="start"]').datepicker("setDate", firstDay);
		$('input[name="end"]').datepicker("setDate", lastDay);
		$('div.b').show();
	} else if (select_id == 3) {

		var date = new Date();
		var l_firstDay = new Date(date.getFullYear(), date.getMonth(), -30);
		var l_lastDay = new Date(date.getFullYear(), date.getMonth(), 1 - 1);
		$('input[name="start"]').datepicker("setDate", l_firstDay);
		$('input[name="end"]').datepicker("setDate", l_lastDay);
		$('div.b').show();
	} else if (select_id == 4) {
		var d = new Date();
		var quarter = Math.floor((d.getMonth() / 3));
		var first_current_quater = new Date(d.getFullYear(), quarter * 3, 1);
		var last_current_quater = new Date(first_current_quater.getFullYear(), first_current_quater.getMonth() + 3, 0);

		$('input[name="start"]').datepicker("setDate", first_current_quater);
		$('input[name="end"]').datepicker("setDate", last_current_quater);
		$('div.b').show();

	} else if (select_id == 5) {

		var d = new Date();
		var quarter = Math.floor((d.getMonth() / 3));
		var first_last_quater = new Date(d.getFullYear(), quarter * 3 - 3, 1);
		var last_last_quater = new Date(first_last_quater.getFullYear(), first_last_quater.getMonth() + 3, 0);


		$('input[name="start"]').datepicker("setDate", first_last_quater);
		$('input[name="end"]').datepicker("setDate", last_last_quater);
		$('div.b').show();

	} else if (select_id == 6) {

		var date = new Date();
		var current_year_first = new Date(date.getFullYear(), 0, 1);
		var current_year_last = new Date(date.getFullYear(), 11, 31);
		$('input[name="start"]').datepicker("setDate", current_year_first);
		$('input[name="end"]').datepicker("setDate", current_year_last);
		$('div.b').show();

	} else if (select_id == 7) {

		var date = new Date();
		var last_year_first = new Date(date.getFullYear(), -12, 1);
		var last_year_last = new Date(date.getFullYear(), -1, 31);
		$('input[name="start"]').datepicker("setDate", last_year_first);
		$('input[name="end"]').datepicker("setDate", last_year_last);
		$('div.b').show();

	} else if (select_id =="") {

		$('div.b').hide();

	}


}
</script>
