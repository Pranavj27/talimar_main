<style>
	table{
		border-collapse :collapse; 
		margin:20px;
	}
	table th{
		padding : 5px;
		border : 1px solid black;
	}
	table td{
		padding : 5px;
		border : 1px solid black;
	}
</style>
<div class="page-content-wrapper">
<div class="page-content responsive">
<form method="POST" action ="<?php echo base_url();?>reports/filter_servicing_status" >
	<div class="row">
		<div class="col-sm-3">
		<label>Loan Status</label>
		<select class="form-control" name="loan_status">
			<option value="1" <?php if(isset($loan_status) && $loan_status == 1){echo 'selected'; } ?> >Pipeline</option>
			<option value="2" <?php if(isset($loan_status) && $loan_status == 2){echo 'selected'; } ?>>Active</option>
			<option value="3" <?php if(isset($loan_status) && $loan_status == 3){echo 'selected'; } ?>>Paid off</option>
			<option value="4" <?php if(isset($loan_status) && $loan_status == 4){echo 'selected'; } ?>>Dead</option>
		</select>
		</div>
		
		<div class="col-sm-3">
		<label>Loan Boarded</label>
		<select class="form-control" name="loan_boarded">
			<option value=""></option>
			<option value="2" <?php if(isset($loan_boarded) && $loan_boarded == 2){echo 'selected'; } ?>>Unchecked</option>
			<option value="1" <?php if(isset($loan_boarded) && $loan_boarded == 1){echo 'selected'; } ?>>Checked</option>
		</select>
		</div>
		
		<div class="col-sm-3">
		<label>File Closed</label>
		<select class="form-control" name="file_closed">
			<option value=""></option>
			<option value="2" <?php if(isset($file_closed) && $file_closed == 2){echo 'selected'; } ?>>Unchecked</option>
			<option value="1" <?php if(isset($file_closed) && $file_closed == 1){echo 'selected'; } ?>>Checked</option>
		</select>
		</div>
		
		<div class="col-sm-3">
		<button type="submit" class="btn blue">GO</button>
		</div>
	</div>  
	<!--
	<div class="row">
		<div class="col-sm-6">
		<label>Sub servicing Agent</label><br>
		<input type="checkbox" name="sub_agent_1">FCI <br>
		<input type="checkbox" name="sub_agent_2">Talimar <br>
		<input type="checkbox" name="sub_agent_3">Del Toro <br>
		</div>
	</div>
	-->
</form>

<table>
	<thead>
		<tr>
		<th>Loan Number</th>
		<th>Loan Amount</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		if(isset($fetch_loan_data))
		{
			foreach($fetch_loan_data as $row)
			{
				?>
				<tr>
				<td><a href="<?php echo base_url().'load_data/'.$row->loan_id;?>"><?php echo $row->talimar_loan;?></a></td>
				<td><?php echo $row->loan_amount;?></td>
				</tr>
				<?php
			}
		}
		else
		{
			?>
			<tr>
			<th colspan="2">No Data Found</th>
			</tr>
			<?php	
		}
		?>
	</tbody>
</table>
</div>
</div>
