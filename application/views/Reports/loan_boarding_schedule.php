<?php
error_reporting(0);
$serviceing_sub_agent_option	= $this->config->item('serviceing_sub_agent_option');
$boarding_status_option			= $this->config->item('boarding_status_option');
$yes_no_option_verified			= $this->config->item('yes_no_option_verified');
$yes_no_option_vd				= array(1=>"Yes",2=>"No");
$yes_no_documents				= $this->config->item('documents_received');
$initial_servicer_review		= $this->config->item('initial_servicer_review');


$loan_status_option			= $this->config->item('loan_status_option');
// echo "<pre>";
// print_r($all_boarding_schdule);
// echo "</pre>";
?>
<style>
table#table th {
    text-align: center;
    /*font-size: 16px;*/
}
.talimar_no_dropdowns {
    float: left;
    padding: 20px;
}
</style>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Loan Holding Report  </h1>	
					
				</div>
	


			<!-- 	<div class="top_download">
					<a href="<?php echo base_url();?>download_boarding_schedule">
					<button  class="btn blue">PDF</button>
					</a> -->
			
	

<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>download_boarding_schedule" id="x">
						
						<input type="hidden" name="loan_sub" value="<?php echo isset($loan_sub) ? $loan_sub : ''; ?>" >
						<input type="hidden" name="loan_board" value="<?php echo isset($loan_board) ? $loan_board : ''; ?>" >
						<input type="hidden" name="loan_post" value="<?php echo isset($loan_post) ? $loan_post : ''; ?>" >
						<input type="hidden" name="loan_board_fee" value="<?php echo isset($loan_board_fee) ? $loan_board_fee : ''; ?>" >
						<input type="hidden" name="desposit_process" value="<?php echo isset($desposit_process) ? $desposit_process : ''; ?>" >
						<input type="hidden" name="closing_comple" value="<?php echo isset($closing_comple) ? $closing_comple : ''; ?>" >
						<input type="hidden" name="board_complete" value="<?php echo isset($board_complete) ? $board_complete : ''; ?>" >
						<input type="hidden" name="loan_status" value="<?php echo isset($loan_status) ? $loan_status : ''; ?>" >
						
						<input type="hidden" name="documents_received" value="<?php echo isset($DocumentsReceived) ? $DocumentsReceived : ''; ?>" >
						<input type="hidden" name="initial_servicer_review" value="<?php echo isset($initial_servicer) ? $initial_servicer : ''; ?>" >
						
						<!--<a href="<?php echo base_url();?>download_existing_loan">
						<button  type = "submit" class="btn blue">Download</button>
						</a>-->
					</form>
					<button  type="submit" onclick="load_full_table()" class="btn red">PDF</button>
						<button  type="button" onclick="load_form()" class="btn blue">Filter</button>
					
				

				</div>
		</div>
		<div class="row">
			<div class="talimar_no_dropdowns" style="padding-left:0px!important">
				<form method="POST" action="<?php echo base_url().'loan_boarding_schedule'?>" id="sub">
				
				<div class="row">
					<?php /*
					<div class="col-md-3">
						Loan Status:
						<select id="loan_status" name="loan_status" class="form-control load_data_inputbox">
							
							<?php foreach($loan_status_option as $key => $row) { ?>
							<option value="<?php echo $key;?>" <?php if($key ==$loan_status ){ echo "selected"; }?> ><?php echo $row;?></option>
							<?php } ?>
						</select>
					</div>
					*/ ?>

					  <div class="col-md-3">
						Loans Submitted:
						<select id="loan_sub" name="loan_sub" class="form-control load_data_inputbox">
							<option value="">Select All</option>
							<?php foreach($yes_no_option_verified as $key => $row) { ?>
							<option value="<?php echo $key;?>" <?php if($key ==$loan_sub ){ echo "selected"; }?> ><?php echo $row;?></option>
							<?php } ?>
						</select>
					</div>
         
					<div class="col-md-3">
						Loan Boarded:

						<select id="loan_board" name="loan_board" class="form-control load_data_inputbox">
									<option value="">Select All</option>
							<?php foreach($yes_no_option_verified as $key => $row) { ?>
							<option value="<?php echo $key;?>" <?php if($key ==$loan_board ){ echo "selected"; }?> ><?php echo $row;?></option>
							<?php } ?>
						</select>
					</div>
         
					<div class="col-md-3">
						Loan Posted:
						<select id="loan_post" name="loan_post" class="form-control load_data_inputbox">
									<option value="">Select All</option>
							<?php foreach($yes_no_option_verified as $key => $row) { ?>
							<option value="<?php echo $key;?>" <?php if($key ==$loan_post ){ echo "selected"; }?> ><?php echo $row;?></option>
							<?php } ?>
						</select>
					</div>
         
					<div class="col-md-3">
						Lender Fees Received:

						<select id="loan_board_fee" name="loan_board_fee" class="form-control load_data_inputbox">
									<option value="">Select All</option>
							<?php foreach($yes_no_option_vd as $key => $row) { ?>
							<option value="<?php echo $key;?>" <?php if($key ==$loan_board_fee ){ echo "selected"; }?> ><?php echo $row;?></option>
							<?php } ?>
						</select>
					</div>
         
					<div class="col-md-3">
						Deposits Processed:
						<select id="desposit_process" name="desposit_process" class="form-control load_data_inputbox">
									<option value="">Select All</option>
							<?php foreach($yes_no_option_verified as $key => $row) { ?>
							<option value="<?php echo $key;?>" <?php if($key ==$desposit_process ){ echo "selected"; }?> ><?php echo $row;?></option>
							<?php } ?>
						</select>
					</div>
         	
         			<div class="col-md-3">
						Loan Documents Received:
						<select id="documents_received" name="documents_received" class="form-control load_data_inputbox">
							<?php foreach($yes_no_documents as $key => $row) { ?>
							<option value="<?php echo $key;?>" <?php if($key ==$DocumentsReceived ){ echo "selected"; }?> ><?php echo $row;?></option>
							<?php } ?>
						</select>
					</div>

					<div class="col-md-3">
						Initial Servicer Review:
						<select id="initial_servicer_review" name="initial_servicer_review" class="form-control load_data_inputbox">
							<?php foreach($initial_servicer_review as $key => $row) { ?>
							<option value="<?php echo $key;?>" <?php if($key ==$initial_servicer ){ echo "selected"; }?> ><?php echo $row;?></option>
							<?php } ?>
						</select>
					</div>

           			<?php /*
           			<div class="col-md-3">
						Closing Checklist Complete:
						<select id="closing_comple" name="closing_comple" class="form-control load_data_inputbox">
									<option value="">Select All</option>
							<?php foreach($yes_no_option_verified as $key => $row) { ?>
							<option value="<?php echo $key;?>" <?php if($key ==$closing_comple ){ echo "selected"; }?> ><?php echo $row;?></option>
							<?php } ?>
						</select>
					</div>
					*/ ?>
					<?php /*
     				<div class="col-md-3">
						Loans in Holding:
						<select id="board_complete" name="board_complete" class="form-control load_data_inputbox">
									<option value="">Select All</option>
							<?php foreach($yes_no_option_verified as $key => $row) { ?>
							<option value="<?php echo $key;?>" <?php if($key == $board_complete ){ echo "selected"; }?> ><?php echo $row;?></option>
							<?php } ?>
						</select>
					</div>
					*/ ?>
					</div>

				</form>
			</div>
	</div>


		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>

						<th>Borrower Name</th>
						<th>Borrower Contact 1</th>
						<th>Loan Servicing #</th>
						<th>Property Street</th>
						<th>Property City </th>
						<th>Property State</th>
						<th>Property Zip</th>
						<th>Loan Amount</th>
						
					</tr>
				</thead>
				<tbody>
				<?php

					//echo 'board_complete: '.$board_complete;

					$key = 0;
					foreach($all_boarding_schdule as $row)
					{
						if($board_complete == 1){

							if($row['input_datas_vals'] != 1){
							
						?>
						<tr>
							<td><?php echo $row['borrower_name'];?></td>
							<td><?php echo $row['borrower_contact'];?></td>
							<td><?php echo $row['fci'];?></td>
							<td><a href ="<?php echo base_url().'load_data/'.$row['loan_id']; ?>"><?php echo $row['property_address'];?></a></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
							<td><?php echo '$'.number_format($row['loan_amount']);?></td>
						
						</tr>
						

					<?php } }elseif($board_complete == 2){ 

							if($row['input_datas_vals'] == 1){
					 ?>

						<tr>
							<td><?php echo $row['borrower_name'];?></td>
							<td><?php echo $row['borrower_contact'];?></td>
							<td><?php echo $row['fci'];?></td>
							<td><a href ="<?php echo base_url().'load_data/'.$row['loan_id']; ?>"><?php echo $row['property_address'];?></a></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
							<td><?php echo '$'.number_format($row['loan_amount']);?></td>
						
						</tr>

					<?php } }else{ ?>

						<tr>
							<td><?php echo $row['borrower_name'];?></td>
							<td><?php echo $row['borrower_contact'];?></td>
							<td><?php echo $row['fci'];?></td>
							<td><a href ="<?php echo base_url().'load_data/'.$row['loan_id']; ?>"><?php echo $row['property_address'];?></a></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
							<td><?php echo '$'.number_format($row['loan_amount']);?></td>
						
						</tr>

					<?php }  } ?>
				</tbody>
			</table>
			
	</div>
	</div>
	<!-- END CONTENT -->
</div>
<script>
$(document).ready(function() {
    $('#table').DataTable();
});

function load_full_table()
{
	$('form#x').submit();
}

function load_form()
{
	$('form#sub').submit();
}


</script>