<?php
$loan_status_option 			= $this->config->item('loan_status_option');
$serviceing_sub_agent_option 	= $this->config->item('serviceing_sub_agent_option');
$serviceing_condition_option 	= $this->config->item('serviceing_condition_option');
$no_yes_option 	= $this->config->item('no_yes_option');
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>Investor Sale List 2</h1>
				
				</div>
				<div class="top_download">
					
			      <form  method="POST" action="<?php echo base_url().'Reports/download_lender_sales_list_2';?>">
					<input type="hidden" name="select_boxxx" value="<?php echo isset($select_boxxx) ? $select_boxxx : ''; ?>">
					<input type="hidden" name="checkboxxx" value="<?php echo isset($checkboxxx) ? $checkboxxx : ''; ?>">
				
					<button  class="btn blue" type="submit">PDF</button>
				</form>
				</div>
				
			
			
				
		</div>
		<div class="row">
					
				
			<!--<div class="col-md-3">
			Primary Contact:  &nbsp;
				
					<select name="investor_id[]" id ="investor_id" class="selectpicker" data-live-search="true"  multiple>
					<select name="contact_id" id ="contact_id" class="chosen" onchange = "select_investor_report(this.value)">
						<option value="">Select One</option>
						<?php 
							foreach($all_lender_contact as $row)
							{
						?>
						<option value="<?php echo $row->contact_id;?>" <?php if(isset($contact_id)){  if($contact_id == $row->contact_id){ echo 'selected'; } }?> ><?php echo $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname; ?></option>
						<?php
						}
						?>
					</select>
			</div>-->

			<form action="<?php echo base_url().'Reports/lender_sales_list_2';?>" method="POST">	
			<!--<div class="col-md-3">
				Lender Status :  &nbsp;
			
				<select name="select_boxx" id ="select_box" class="form-control">
					<option value="">Select All</option>
					<option value="1" <?php  if($select_boxxx == 1){ echo 'selected'; } ?>>Active Loans</option>
					<option value="2" <?php  if($select_boxxx == 2){ echo 'selected'; } ?>>No Active Loans</option>
					<option value="3" <?php  if($select_boxxx == 3){ echo 'selected'; } ?>>No Loans</option>
					
									
				</select>
			</div>-->

	<div class="col-md-5" style="margin-top:26px;">
	
		Active Account:<input type="checkbox" name="active_investment" value="1"  <?php  if($checkboxxx == 1){ echo 'checked'; } ?>>
		Active Investment:<input type="checkbox" name="active_investment" value="2"  <?php  if($checkboxxx == 2){ echo 'checked'; } ?>>
		Available Capital:<input type="checkbox" name="active_investment" value="3" <?php  if($checkboxxx == 3){ echo 'checked'; } ?>>
		</div>


<div class="col-md-1">
		<button type="submit" name="filter_active" class="btn blue" style="margin-top:18px; margin-left:-62px;">Filter</button>
	</div>
</form>
			
	

				<!--
			
			<div class="col-md-3">
				Trust Deed Investor :  &nbsp;
				<!--<select name="investor_id[]" id ="investor_id" class="selectpicker" data-live-search="true"  multiple>
					<select name="investor_id" id ="investor_id" class="chosen" onchange = "select_investor_report(this.value)">
						<option value=''>Select All</option>
						<?php 
							foreach($all_investor as $row)
							{
								?>
								<option value="<?php echo $row->id; ?>" <?php if(isset($select_investor_id)){ if($select_investor_id == $row->id ){ echo 'selected'; } } ?> ><?php echo $row->name; ?></option>
								<?php
							}
						?>
					</select>
					
					
			</div>-->
		
			
			</form>
			</div>
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						<th style="white-space:nowrap;">Name</th>
					    <th># of<br>Accounts</th>
						<th># of<br>Total Loans</th>
						<th>$ of<br>Total Loans</th>
						<th># of<br>Active Loans</th>
						<th>$ of<br>Active Loans</th>
						<th>Committed<br>Capital</th>
						<th>Available<br>Capital</th>			
						<th>E-Mail<br>List</th>			
						<th>Text List</th>			
						<th>Phone</th>			
						<th>E-Mail</th>			
						
					</tr>
				</thead>
				<tbody>

				<?php 
				$total=0;
				$total_account=0;
				$hash_total_loan=0;
				$amount_total_loan=0;
				$hash_active_loan=0;
				$commited=0;
				$avaliable=0;
				foreach($lender_all_sales_list as $row){ 
			
                        $contact=$row['contact_id'];
						$total_account +=$row['count_accounts'];
						$hash_total_loan +=$row['fetch_total'];
						$amount_total_loan +=$row['total_investment'];
						$hash_active_loan +=$row['count_active_loans'];
						$commited +=$row['committed_funds'];
						$avaliable +=($row['committed_funds'] - $row['active_investment']);
					?>


					<tr>
					
					
						<td style="white-space:nowrap;"><a href="<?php echo base_url("viewcontact/$contact");?>"><?php echo $row['contact_firstname'].' '.$row['contact_lastname'];?></a></td>
						<td><?php echo $row['count_accounts'];?></td>
						<td><?php echo $row['fetch_total'];?></td>
						<td>$<?php echo number_format($row['total_investment']);?></td>
						<td><?php echo $row['count_active_loans'];?></td>
						<td>$<?php echo number_format($row['active_investment']);?></td>
						
						<?php $commited_fund = $row['committed_funds'] ? number_format($row['committed_funds']) : number_format('0');?>
						
						<td>$<?php echo $commited_fund;?></td>
						<td>$<?php echo number_format($row['committed_funds'] - $row['active_investment']);?></td>
						<td>Yes</td>
						<?php if($row['setup_package'] == '1'){
							$active = 'Yes';
						}else{
							$active = 'No';
						}?>
						<td><?php echo $row['text_list']; ?></td>
						<td ><?php echo $row['contact_phone'];?></td>
						<td><?php echo $row['contact_email'];?></td>
					</tr>
				<?php $total++;

			} ?>
				</tbody>
				<tfoot>
						<tr>
							<th>Total: <?php echo $total;?></th>
							<th><?php echo $total_account;?></th>
							<th><?php echo $hash_total_loan;?></th>
							<th>$<?php echo number_format($amount_total_loan); ?></th>
							<th><?php echo $hash_active_loan; ?></th>
							<th></th>
							<th>$<?php echo number_format($commited);?></th>
							<th>$<?php echo number_format($avaliable);?></th>
						</tr>
						
					</tfoot>
			</table>
			</div>
			
	</div>
	<!-- END CONTENT -->
</div>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
<style>

.chosen-drop {
    width: 230px !important;
    margin-left: 0px !important;
}	
.row.borrower_data_row div {
    width: 96% !important;
}
.chosen-container{
	display: block !important;
}
.chosen-container.chosen-container-single {
    width: 230px !important;
}
.chosen-container-single .chosen-single {
    padding: 4px 0px 26px 6px !important;
    border-radius: 4px !important;
    border: 1px solid #e1e1e1 !important;
}

</style>
<script>

$('.chosen').chosen();

function select_investor_report(sel)
{
	
	document.getElementById('select_investor').submit();
}

</script>
<script>

$(document).ready(function() {
  
	$('#table').DataTable({
 "order": [[ 6, "desc" ]],
 "aaSorting": []
	});
		
	});




</script>