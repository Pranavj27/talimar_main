<?php

error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
// echo "<pre>";
// print_r($header_lender_name);
// echo "</pre>";

//$select_box_name = isset($select_box) ? '('.$loan_status_option[$select_box].')' : '' ;
?>

<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Lender Account Schedule<?php //echo $select_box_name; ?></h1>	
					
				</div>
				
				<div class="top_download">
				<form method="POST" action="<?php echo base_url();?>Reports/download_lender_s_contact">
					<input type="hidden" name="contact_id" value="<?php echo $contact_id;?>">
					
					<input type="hidden" name="investor_id" value="<?php echo $investor_id;?>">
					
					<button  type="submit" class="btn blue">PDF</button>
				</form>
				</div>
				
		</div>
		
		<div class="row">
			
			
				<form id="select_investor" method="POST" action="<?php echo base_url();?>lender_schedule_contact">
				
		
				
				<div class="col-md-3">
					<label>Contact Name:</label><br>
					<select name="contact_id" id ="contact_id" onchange = "select_investor_report(this.value)" class="chosen">
						<option value="">Select All</option>
						<?php 
							foreach($all_lender_contact as $row)
							{
						?>
						<option value="<?php echo $row->contact_id;?>" <?php if(isset($contact_id)){  if($contact_id == $row->contact_id){ echo 'selected'; } }?> ><?php echo $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname; ?></option>
						<?php
						}
						?>
					</select>
				</div>
						
	
						
						
						
			<div class="col-md-3">
				<label>Lender Name:</label>
				
				<!--<select name="investor_id[]" id ="investor_id" class="selectpicker" data-live-search="true"  multiple>-->
					<select name="investor_id" id ="investor_id" class="chosen" onchange = "select_investor_report(this.value)">
						<option value=''>Select All</option>
						<?php 
							foreach($all_investor as $row)
							{
								?>
								<option value="<?php echo $row->id; ?>" <?php if(isset($investor_id)){ if($investor_id == $row->id ){ echo 'selected'; } } ?> ><?php echo $row->name; ?></option>
								<?php
							}
						?>
					</select>
					
					
			</div>
				
			</form>
			
		</div>
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Lender Name</th>
					<th>Contact Name</th>
					<th>FCI<br>Account #</th>
					<th># of<br>Active<br>Loans </th>
					<th>$ of Active<br> Loans</th>
					<th># of<br>Paid Off<br> Loans</th>
					<th>$ of Paid Off<br> Loans</th>
					<th># of<br>Total<br> Loans </th>
					<th>$ of Total<br> Loans</th>
					<th>Disbursement<br>Type</th>
					<th>Phone</th>
					<th>E-mail</th>
					
					
					
					</tr>
				</thead>
				<tbody class="scrollable">
				<?php 
				$number=0;
				$total_active_loan=0;
				$total_active_loan_amount=0;
				$total_paidoff_loan=0;
				$total_paidoff_loan_amount=0;
				$total_loan=0;
				$total_loan_amount=0;


				foreach($loan_schedule_contact as $key => $row){

					$total_active_loan +=$row['total_loan'];
					$total_active_loan_amount +=$row['loan_amount'];
					$total_paidoff_loan +=$row['paidoff_loan'];
					$total_paidoff_loan_amount +=$row['paidoff_loan_amount'];
					$total_loan +=$row['loan_active_paid'];
					$total_loan_amount +=$row['active_paid']; 


					if($row['check_disbrusement'] == '1'){
						$disType = 'Check';
					}elseif($row['ach_disbrusement'] == '1'){
						$disType = 'ACH';
					}else{
						$disType = '';
					}

				?>
				
				
					
						<tr>
							<td><a href ="<?php echo base_url().'investor_view/'.$row['lender_id']; ?>"><?php echo $loan_schedule_contact[$key]['investor_name'];?></a></td>
							<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['contact_name'];?></a></td>							
							<td><?php echo $row['fci_acct'];?></td>
							<td><?php echo $row['total_loan'];?></td>
							<td>$<?php echo number_format($row['loan_amount']);?></td>
							<td><?php echo $row['paidoff_loan'];?></td>
							<td>$<?php echo number_format($row['paidoff_loan_amount']);?></td>
							<td><?php echo $row['loan_active_paid'];?></td>
							<td>$<?php echo number_format($row['active_paid']);?></td>
							<td><?php echo $disType;?></td>
							<td><?php echo $row['investor_phone'] ;?></td>
							<td><?php echo $row['investor_email'];?></td>
							
						</tr>
					<?php 

                 $number++;
					} ?>		
				</tbody>	
				<tfoot>
				
						<tr>
							<th>Total</th>
							<th><?php echo $number; ?></th>
							<th></th>
							<th><?php echo number_format($total_active_loan); ?></th>
							
							<th>$<?php echo number_format($total_active_loan_amount); ?></th>
							<th><?php echo number_format($total_paidoff_loan); ?></th>
							<th>$<?php echo number_format($total_paidoff_loan_amount); ?></th>
							<th><?php echo number_format($total_loan); ?></th>
							<th>$<?php echo number_format($total_loan_amount);?></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					
				</tfoot>
				
			</table>
		</div>
	</div>
	
	
	
	
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
<script>

$(".chosen").chosen();

$(document).ready(function() {
	 
    $('#table').DataTable();
	
});

function select_investor_report(){
	$('form#select_investor').submit();
}

</script>
<style>
.talimar_no_dropdowns {
		float: left;		
}
.talimar_no_dropdowns div {
    float: left;
    padding: 0px;
}div#contact_id_chosen {
    width: 206px !important;
}
.chosen-container-single .chosen-single {
    padding: 4px 0px 26px 6px !important;
    border-radius: 4px !important;
    border: 1px solid #e1e1e1 !important;
}
div#investor_id_chosen {
	width:250px !important;
}
.col-md-4.set {
  
    padding-top: 10px;
}
</style>



