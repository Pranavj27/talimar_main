<?php 

$multi_status_option=array(
							""=>"Select All",
							1 =>"Yes",
							2 =>"No"
							);


$loan_status_option	= array(
							"" => 'Select All',
							6 => 'Term Sheet',
							1 => 'Pipeline',
							2 => 'Active',
							3 => 'Paid off',
							5 => 'Brokered'
								);	

?>

<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp;Multi Lender Loans</h1>	
					
				</div>
				
				<div class="top_download">
				<form method="POST" action="<?php echo base_url();?>multilender_loan_pdf">
				 <input type="hidden" name="loan_status" value="<?php echo isset($loan_status) ? $loan_status : '';?>">
				 <input type="hidden" name="multi_status" value="<?php echo isset($multi_status) ? $multi_status : '';?>">
			
				
				<button  type="submit" class="btn red">PDF</button>
				</form>
				</div>
				
		</div>
		
			<div class="row" style="margin-left:-12px!important;">
		
			
				<form method="POST" action="<?php echo base_url().'multi_lender_loan';?>" id="">
				

				<div class="col-md-2">
					Loan Status: 
					<select name="loan_status" class="form-control input-md" >
						<?php 
							foreach ($loan_status_option as $key => $value) {
									
								?>	
								<option value="<?php echo $key;?>" <?php echo ($loan_status == $key) ? 'Selected':'';?>><?php echo $value;?></option>

						<?php }?>
					</select>
				</div>


				<div class="col-md-2">
					Multi Lender Loan: 
					<select name="multi_status" class="form-control input-md"  >
						<?php 
							foreach ($multi_status_option as $key => $value) {
									
								?>	
								<option value="<?php echo $key;?>" <?php echo ($multi_status == $key) ? 'Selected':'';?>><?php echo $value;?></option>

						<?php }?>
					</select>
				</div>

				<br>
			
			<div class="col-md-2">
					<button  type="submit" name="submit"  class="btn blue" id="filt">Filter</button>
				</div>
			</form>
			
			
		</div>
		
		
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Loan #</th>
					<th>Loan Amount</th>
					<th>Multi-Lender<br>Loan</th>
					<th># of Lenders</th>
					<th>Average Interest</th>
					<th>Property Address</th>
					<th>City</th>
					<th>State</th>
					<th>Zip</th>
					
					
					</tr>
				</thead>
				<tbody class="scrollable">
				<?php 
				
					$count=0;
					$total_loan_amount=0;
					$total_avg_int=0;

					if(isset($multi_loan_array))
					{
						foreach($multi_loan_array as $key => $row)
						{
							$count++;
							if($row['is_multi_lender'] == 1){

								$multi_lender='Yes';

							}elseif($row['is_multi_lender'] == 2){

								$multi_lender='No';
							
							}else{

							$multi_lender='Select One';
							
							}

							$avg_int = ($row['loan_amount']/$row['lenders_count']);
							$new_avg=is_infinite($avg_int) ? 0 : $avg_int;
						
							?>
							<tr>

								<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['talimar_loan']; ?></a></td>
								<td>$<?php echo number_format($row['loan_amount']); ?></td>
								<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>#assigment"><?php echo $multi_lender; ?></a></td>
								<td><?php echo $row['lenders_count']; ?></td>
							     <td><?php echo number_format($new_avg,2); ?>%</td>
							     <td><?php echo $row['property_address']; ?></td>
							     <td><?php echo $row['city']; ?></td>
								<td><?php echo $row['state']; ?></td>
								<td><?php echo $row['zip']; ?></td>
					
							</tr>
							
							
							<?php
								$total_loan_amount 	= $total_loan_amount + $row['loan_amount'];
								$total_avg_int 	= $total_avg_int + $new_avg;
						}
					}
				?>
					
				</tbody>	
				 <tfoot>
					<tr>
						<th>Total:<?php echo $count; ?></th>
						<th><?php echo '$'.number_format($total_loan_amount);?></th>
						
						<th></th>
						<th ></th>
						<th ></th>
						<th ></th>
						<th ></th>
						<th ></th>
						<th ></th>
					</tr>
					
				    <tr>
						<th>Avg:</th>
						
						<th><?php echo '$'.number_format($total_loan_amount/$count); ?></th>
						<th></th>
						<th></th>
						<th><?php echo number_format($total_avg_int/$count,2).'%'; ?></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					
						
					
					</tr>
				</tfoot> 
				
			</table>
		</div>
	</div>
	
	
	
	
</div>
<script>

$(document).ready(function() {
   // $('#table').DataTable();
	$('#table').DataTable({
 "aaSorting": []
	});
		
	});

	
	
	// var table = $('#table').DataTable();
// table
    // .column( '10:visible')
    // .order( 'asc' )
    // .draw();
	
	// $('th#click_col').click();

</script>
<style>
.float-direction-left {
    float: left !important;
}


</style>