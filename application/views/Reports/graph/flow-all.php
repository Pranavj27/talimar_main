<!-- BEGIAGE CONTAINER -->
<style>
a.canvasjs-chart-credit {
    display: none !important;
}
</style>
<div class="page-content-wrapper">

	
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row">
				
				<div class="col-md-12">
					
					<!-- BEGIN ROW -->
					<div class="row">
						<div class="col-md-12">
							<!-- BEGIN BASIC CHART PORTLET-->
					<div class="portlet light" style="height:500px;">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs font-green-sharp"></i>
								<span class="caption-subject font-green-sharp bold uppercase">Bar Chart</span>
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div>
								<p><span id="paid-off-chart-color">&nbsp;</span>Paid Off <span id="active-chart-color">&nbsp;</span>Active <span id="pipe-chart-color">&nbsp;</span> Pipeline</p>
							</div>
							<div id="chartContainer">
							</div>
							<div id="chart_detail" class="chart">
								
							</div>
						</div>
					</div>
					<!-- END BASIC CHART PORTLET-->
						</div>
					</div>
					<!-- END ROW -->
					<!-- BEGIN ROW -->
					
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
	</div>
	<!-- END PAGE CONTENT -->
</div>
<script type="text/javascript">
		window.onload = function () {
			var chart = new CanvasJS.Chart("chartContainer", {
				title: {
					text: "Yearly # of Loans"
				},
				axisX: {
					 valueFormatString:"####",
					interval: 1
				},
				data: [{
					type: "stackedColumn",
					dataPoints: <?php echo $active_data;?>
				}, {
					type: "stackedColumn",
					dataPoints: <?php echo $paid_data;?>
				}, {
					type: "stackedColumn",
					dataPoints: <?php echo $pipe_data;?>
				}]
			});
			chart.render();
		}
		
		
	</script>
<script src="<?php echo base_url();?>assets/extra_css_js/canvasjs/canvasjs.min.js"></script>