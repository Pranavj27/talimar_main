<?php
error_reporting(0);
$loan_status_options 			= array(

										"" => 'Select All',
										6 => 'Term Sheet',
										1 => 'Pipeline',
										2 => 'Active',
										3 => 'Paid off',
										5 => 'Brokered'
											);		

									
// $serviceing_sub_agent_option 	= $this->config->item('serviceing_sub_agent_option');
// $serviceing_condition_option 	= $this->config->item('serviceing_condition_option');
$lender_ownership_type  = $this->config->item('lender_ownership_type');
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Lender / Loan Schedule</h1>	 
					
				</div>
				
				<div class="top_download">
				
						<form method="POST" action="<?php echo base_url();?>download_lender_loan_pdf">

						<input type="hidden" name="loan_status" value="<?php echo isset($loan_status) ? $loan_status : ''; ?>" >
						<input type="hidden" name="fractional_int" value="<?php echo isset($fractional_int) ? $fractional_int : ''; ?>" >
			
			
					<button  class="btn blue" type="submit">PDF</button>
				</form>
				</div>
				
		</div>
	<div class="row">
			<form id="select_investor" method="POST" action="<?php echo base_url();?>lender_loan_schedule">
			    <div class="col-md-2">
						Loan Status:
						<select id="loan_status" name="loan_status" class="form-control load_data_inputbox">
							
							<?php foreach($loan_status_options as $key => $row) { ?>
							<option value="<?php echo $key;?>" <?php if($key ==$loan_status ){ echo "selected"; }?> ><?php echo $row;?></option>
							<?php } ?>
						</select>
				</div>
				<div class="col-md-2">
					Fractional Interest:  &nbsp;
						<select name="fractional_int" class="form-control" >
							<option value="">Select All</option>
							<?php foreach($lender_ownership_type as $key => $row){ ?>
								<option value="<?php echo $key; ?>" <?php if($key == $fractional_int){ echo 'selected';}?>><?php echo $row; ?></option>
							<?php  } ?>
						</select>
				</div> <br>
				<div class="col-md-2">
					<!--<button type="button" name="clear" id="reset_button" class="btn blue" style="margin-top:18px; margin-right:11px; float:right;">Clear</button>-->
					<button type="submit" name="filter_active" class="btn blue" >Filter</button>
				
				</div>


			</form>
		
	</div> 
		
	
			<div class="rc_class">
				<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
					<thead>
						<tr>
							<th>Street Address</th>
							<th>Unit #</th>
							<th>City</th>
							<th>State</th>
							<th>Zip</th>
							<th>Loan<br>Amount</th>
							<th>Fractionalized</th>
							<th># of Lenders</th>
							<th>Avg. Lender $</th>
							<th>Gross Spread %</th>
							<th>Net Spread $</th>
							
				
						</tr>
					
					</thead>
					<tbody>
							<?php
							
							$number=0;
							$t_loan_amount=0;
							$t_avg_lender=0;
							$t_servicing_income=0;
							
							$t_g_spread=0;
							foreach($lender_loan_schedules as $row)
							{
						 if(is_nan($row['avg_lender'])){
								
								$avg_lender=0;
							 }else{

							 	$avg_lender=$row['avg_lender'];
							 }

								$t_loan_amount +=$row['loan_amount'];
								$t_avg_lender +=$avg_lender;
								$t_servicing_income +=$row['servicing_income'];
								
								$t_g_spread +=$row['g_spread'];
							 
							 if($row['factorial']==1){
								$factorial='Yes';
							 }elseif($row['factorial']==2){
							 	$factorial='No';

							 }else{
								$factorial='';
							 }



						?>
						<tr>	
				
							<td><a href ="<?php echo base_url().'load_data/'.$row['loan_id']; ?>"><?php echo $row['property_address'];?></a></td>

							<td><?php echo $row['unit'];?></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
							<td><?php echo '$'.number_format($row['loan_amount']);?></td>
							<td><?php echo $factorial;?></td>
							<td><?php echo $row['lender_count'];?></td>
							<td><?php echo '$'.number_format($avg_lender);?></td>
							<td><?php echo number_format($row['g_spread'] ,2);?>%</td>
				
							<td><?php echo '$'.number_format($row['servicing_income'],2); ?></td>
						
							
						</tr>
						<?php
						$number++;
						
						

						}
						?>
					</tbody>	
					</tbody>
					 <tfoot>
						<tr>
							<th>Total</th>
							<th><?php echo $number; ?></th>
							<th ></th>
							<th ></th>
							<th ></th>
							<th ><?php echo '$'.number_format($t_loan_amount); ?></th>
							<th></th>
							<th></th>
							<th><?php echo '$'.number_format($t_avg_lender); ?></th>
							<th></th>
							<th><?php echo '$'.number_format($t_servicing_income,2); ?></th>
						
						</tr>
						<tr>
							<th style="text-align:left">Average:</th>
							<th ></th>
							<th ></th>
							<th ></th>
							<th ></th>
							<th ><?php echo '$'.number_format($t_loan_amount/$number); ?></th>
							<th></th>
							<th ></th>
							<th>$<?php echo number_format($t_avg_lender/$number);?></th>
							<th><?php echo number_format($t_g_spread/$number,2);?>%</th>
							<th>$<?php echo number_format($t_servicing_income/$number,2);?></th>
							
						
						</tr>
					</tfoot> 
				</table>
		
				
			
	</div>
	</div>
	<!-- END CONTENT -->
</div>


<script>



// function select_investor_report(sel)
// {
	
// 	document.getElementById('select_investor').submit();
// }

$(document).ready(function() {
	 
    $('#table').DataTable({
		//"aaSorting": [ 7, "asc" ]
	});
	
});

</script>

