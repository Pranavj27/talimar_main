<?php
error_reporting(0);

$construction_status_option = array(""=>"Select All", "1"=>"Active", "2"=>"Complete",);

$usa_city_county 				= $this->config->item('usa_city_county'); 	
$loan_type_option 				= $this->config->item('loan_type_option');
$serviceing_sub_agent_option 	= $this->config->item('serviceing_sub_agent_option');
$position_option 				= $this->config->item('position_option');
$closing_status_option 			= $this->config->item('closing_status_option');

$loan_status_option 		    = $this->config->item('loan_status_option');


$yes_no_option_67 				= array(""=>"Select All", "1"=>"Yes", "0"=>"No");

$no_yes_option_custom 			= array(""=>"Select All", "1"=>"Yes", "2"=>"No");
$board_complete_option 			= array(""=>"Select All", "1"=>"Yes", "2"=>"No");
$mutli_pro_option 				= array(""=>"Select All","1"=>"Yes","2"=>"No");
$mutli_lender_option 			= array(""=>"Select All","1"=>"Yes","2"=>"No");
$welcome_call_option 			= array(""=>"Select One","1"=>"Yes","2"=>"No");

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">		
				<h1> &nbsp;  Loan Schedule</h1>						
			</div>

			<div class="top_download">
				<form method="POST" action="<?php echo base_url();?>download_existing_loan" id="x" class="pdf_dv_loan_schedule">
					
					<input type="hidden" name="county" value="<?php echo isset($sub_servicing_agent) ? $sub_servicing_agent : ''; ?>" >
					<input type="hidden" name="sub_servicing_agent" value="<?php echo isset($sub_servicing_agent) ? $sub_servicing_agent : ''; ?>" >
					<input type="hidden" name="loan_type" value="<?php echo isset($loan_type) ? $loan_type : ''; ?>" >
					<input type="hidden" name="county" value="<?php echo isset($county) ? $county : ''; ?>" >
					<input type="hidden" name="borrower_id" value="<?php echo isset($borrower_id) ? $borrower_id : ''; ?>" >
					<input type="hidden" name="position" value="<?php echo isset($position) ? $position : ''; ?>" >
					<input type="hidden" name="late" value="<?php echo isset($late) ? $late : ''; ?>" >
					<input type="hidden" name="late10" value="<?php echo isset($late10) ? $late10 : ''; ?>" >
					<input type="hidden" name="loan_status" value="<?php echo isset($loan_status) ? $loan_status : ''; ?>" >
					<input type="hidden" name="board_complete" value="<?php echo isset($board_complete) ? $board_complete : ''; ?>" >
					<input type="hidden" name="construction_status" value="<?php echo isset($construction_status) ? $construction_status : ''; ?>" >
					<input type="hidden" name="multipro" value="<?php echo isset($multipro) ? $multipro : ''; ?>" >
					<input type="hidden" name="multilender" value="<?php echo isset($multilender) ? $multilender : ''; ?>" >
					<input type="hidden" name="loan_boarded" value="<?php echo isset($loan_boarded) ? $loan_boarded : ''; ?>" >
					<input type="hidden" name="welcome_call" value="<?php echo isset($welcome_call) ? $welcome_call : ''; ?>" >
					<input type="hidden" name="welcome_email" value="<?php echo isset($welcome_email) ? $welcome_email : ''; ?>" >
					<input type="hidden" name="loan_preboarded" value="<?php echo isset($loan_preboarded) ? $loan_preboarded : ''; ?>" >
					<input type="hidden" name="payoff_processed" value="<?php echo isset($payoff_processed) ? $payoff_processed : ''; ?>" >
					<input type="hidden" name="closing_status" value="<?php echo isset($closing_status) ? $closing_status : ''; ?>" >
					<input type="hidden" name="muturity_loan" value="<?php echo isset($muturity_loan) ? $muturity_loan : ''; ?>" >
					<input type="hidden" name="payment_reserve" value="<?php echo isset($payment_reserve) ? $payment_reserve : ''; ?>" >
					<input type="hidden" name="defaulted_loans" value="<?php echo isset($defaulted_loans) ? $defaulted_loans : ''; ?>" >
					<input type="hidden" name="term_sheet_sign" value="<?php echo isset($term_sheet_sign) ? $term_sheet_sign : ''; ?>" >
					<input type="hidden" name="Listed_Properties" value="<?php echo isset($Listed_Properties) ? $Listed_Properties : ''; ?>" >
					
					<?php /*<a href="<?php echo base_url();?>download_existing_loan">
					<button  type = "submit" class="btn blue">Download</button>
					</a>*/ ?>
				</form>
				<button  type="submit" onclick="load_full_table()" class="btn red">PDF</button>
				<button  type="button" onclick="load_form()" class="btn blue">Filter</button>
			</div>
		</div>

		<div class="row">
			<div class="talimar_no_dropdowns" style="padding-left:0px!important">
				<form method="POST" action="<?php echo base_url().'existing_loan_schedule'?>" id="sub" class="sub_filter_f_loan_schedule">
			        <div class="row">
                
				        <div class="col-md-2">
							Loan Status:
							<select id="loan_status" name="loan_status" class="form-control load_data_inputbox">
								<option value="0">Select All</option>
								<?php
								unset($loan_status_option[0]);
								 foreach($loan_status_option as $key => $row) { 
								 	$selected = '';
								 	if($key == 2){
								 		$selected = 'selected';
								 	}
								 	?>
								<option value="<?php echo $key;?>" <?php echo $selected; ?>><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>

						<?php /*
						<div class="col-md-2">
							Loan Pre-Boarded:
							<select name="loan_preboarded" class="form-control load_data_inputbox">
								<?php
								foreach($no_yes_option_custom as $key => $row) { ?>
								<option value="<?php echo $key;?>" <?php if($key == $loan_preboarded ){ echo "selected"; }?> ><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>
						*/ ?>

				      	<div class="col-md-2">
						  Loan Boarded:
							<select id="loan_boarded" name="loan_boarded" class="form-control load_data_inputbox">
								
								<?php foreach($board_complete_option as $key => $row) { ?>
								<option value="<?php echo $key;?>" ><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>

						<?php /*
					    <div class="col-md-2">
						  Closing Status:
							<select id="closing_status" name="closing_status" class="form-control load_data_inputbox">
								<option value="">Select All</option>
								<?php foreach($closing_status_option as $key => $row) { ?>
								<option value="<?php echo $key;?>" <?php if($key ==$closing_status){ echo "selected"; }?> ><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>
						*/ ?>
		                <div class="col-md-2">
								Loans in Holding:
								<select id="loan_status" name="board_complete" class="form-control load_data_inputbox">
									
									<?php foreach($yes_no_option_67 as $key => $row) { ?>
									<option value="<?php echo $key;?>" ><?php echo $row;?></option>
									<?php } ?>
								</select>
						</div>
						<div class="col-md-2">
								Listed Properties:
								<select id="loan_status" name="Listed_Properties" class="form-control load_data_inputbox">
									
									<?php foreach($yes_no_option_67 as $key => $row) { ?>
									<option value="<?php echo $key;?>" ><?php echo $row;?></option>
									<?php } ?>
								</select>
						</div>

						<div class="col-md-2">
							Loan Type: 
							<select id="loan_type" name="loan_type" class="form-control load_data_inputbox">
								
								<?php foreach($loan_type_option as $key => $row) { ?>
								<option value="<?php echo $key;?>" ><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>
						<input type="hidden" name="county" id="county_usa" value="">
						<?php /*
						<div class="col-md-2">
							County: 
							<select  class="form-control" name = "county" id="county_usa">
								<?php 
									foreach($usa_city_county as $key => $row )
									{
										
								?>
								<option value = '<?php echo $key;?>' ><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>
						*/ ?>
						<div class="col-md-2">
							Position: 
							<select  class="form-control" name="position">
								<?php 
									foreach($position_option as $key => $row)
									{
										
								?>
								<option value="<?php echo $key;?>"><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>
						
						
						<div class="col-md-2">
							10+ Days Late:
							<select id="late10" name="late10" class="form-control load_data_inputbox">
								
								<?php foreach($no_yes_option_custom as $key => $row) { ?>
								<option value="<?php echo $key;?>"><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>
			
		         		<div class="col-md-2">
							30+ Days Late:
							<select id="late" name="late" class="form-control load_data_inputbox">
								
								<?php foreach($no_yes_option_custom as $key => $row) { ?>
								<option value="<?php echo $key;?>" ><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>	


						<div class="col-md-2">
							Construction Status: 
							<select id="construction_status" name="construction_status" class="form-control load_data_inputbox">
								
								<?php foreach($construction_status_option as $key => $row) { ?>
								<option value="<?php echo $key;?>" ><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>

				

						<div class="col-md-2">
							Multi Property Loan: 
							<select id="multipro" name="multipro" class="form-control load_data_inputbox">
								
								<?php foreach($mutli_pro_option as $key => $row) { ?>
								<option value="<?php echo $key;?>"  ><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-2">
							Multi Lender Loan: 
							<select id="multilender" name="multilender" class="form-control load_data_inputbox">
								
								<?php foreach($mutli_lender_option as $key => $row) { ?>
								<option value="<?php echo $key;?>"  ><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>
				 

						 <div class="col-md-2">
							Payoff Processed:
							<select id="payoff_processed" name="payoff_processed" class="form-control load_data_inputbox">
								
								<?php foreach($welcome_call_option as $key => $row) { ?>
								<option value="<?php echo $key;?>" ><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-2">
							Welcome Call:
							<select id="welcome_call" name="welcome_call" class="form-control load_data_inputbox">
								
								<?php foreach($welcome_call_option as $key => $row) { ?>
								<option value="<?php echo $key;?>" ><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>

						<div class="col-md-2">
							Welcome E-Mail:
							<select id="welcome_email" name="welcome_email" class="form-control load_data_inputbox">
								
								<?php foreach($welcome_call_option as $key => $row) { ?>
								<option value="<?php echo $key;?>"><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>

						<div class="col-md-2">
							Subservicer: 
							<select id="subservicing_agent" name="sub_servicing_agent" class="form-control input-md">
							<option value = "0">Select All</option>
								<?php foreach($fetch_vendors as $key => $row){ if(($row->vendor_name == 'FCI Lender Services') ||( $row->vendor_name == 'Del Toro Loan Servicing') || ( $row->vendor_name == 'TaliMar Financial Inc.')){?>
								<option value="<?php echo $row->vendor_id;?>"  ><?php echo $optntxt;?></option>
								<?php } }?>
							</select>
						</div>
						<div class="col-md-2">
							Maturing Loans: 
							<select id="muturity_loan" name="muturity_loan" class="form-control load_data_inputbox">
								
								<?php foreach($no_yes_option_custom as $key => $row) { ?>
								<option value="<?php echo $key;?>" ><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>

						<div class="col-md-2">
							Payment Reserve:
							<select name="payment_reserve" class="form-control">
								<?php
								foreach($no_yes_option_custom as $key => $row) { ?>
									<option value="<?php echo $key;?>" ><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>
				
						<div class="col-md-2">
							 Defaulted Loans:
							<select name="defaulted_loans" class="form-control">
								<?php
								foreach($no_yes_option_custom as $key => $row) { ?>
									<option value="<?php echo $key;?>" ><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>

						<div class="col-md-2">
							Term Sheet Signed:
							<select name="term_sheet_sign" class="form-control">
								<?php
								foreach($no_yes_option_custom as $key => $row) { ?>
									<option value="<?php echo $key;?>"  ><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>
					</div>

					<?php 
					/*
					<div class="row">
						<div class="col-md-2" style="width:50% !important;">
							Borrower Name: 
							<select class="form-control" name="borrower_id">
									<option value=''>Create All</option>
									<?php foreach($fetch_all_borrowers as $row) { ?>
										<option value="<?php echo $row->id;?>" <?php if(isset($borrower_id)){ if($borrower_id == $row->id ){ echo 'selected'; }}?>><?php echo $row->b_name;?></option>
									<?php } ?>
							</select>
					
						</div>
						
					</div>
					*/ 
					?>					
				</form>
			</div>
		</div>

		<div class="rc_class">


			<div class="table-upper-section ajax_table_talimar table_All">
				<table id="existing_loan_schedule_tab" class="table-responsive table table-bordered table-striped table-condensed flip-content dataTable no-footer">
					<thead>
						<tr>
							<th>Street Address</th>
							<th>Unit #</th>
							<th>City</th>
							<th>State</th>
							<th>Zip</th>
							<th>Borrower Name</th>
							<th>Loan Amount</th>
							<th>Position</th>
							<th>LTV</th>
							<th>Rate</th>
							<th>Term</th>
							<th>Payment</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody></tbody>
					<tfoot>					
						<tr>						
							<th>Total: <span class="total_loan lp_spin_load"></span></th>						
							<th></th>						
							<th></th>						
							<th></th>						
							<th></th>						
							<th></th>												
							<th>$<span class="total_loan_amount lp_spin_load"></span></th>						
							<th></th>					
							<th></th>					
							<th></th>					
							<th></th>					
							<th>$<span class="total_loan_amount_payment lp_spin_load"></span></th>					
							<th></th>					
												
											
						</tr>	
						<tr>					
							<th>Average:</th>						
							<th></th>						
							<th></th>						
							<th></th>						
							<th></th>						
							<th></th>																			
							<th>$<span class="average_loan_amount lp_spin_load"></span></th>	
							<th></th>					
							<th><span class="average_loan_ltv lp_spin_load"></span>%</th>					
							<th><span class="average_loan_rate lp_spin_load"></span>%</th>
		                    <th><span class="average_loan_term lp_spin_load"></span></th>	
							<th>$<span class="average_loan_payment lp_spin_load"></span></th>
							<th></th>					
						</tr>	

					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url("assets/js/reports/existing_loan_schedule.js"); ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/reports/existing_loan_schedule.css"); ?>">
