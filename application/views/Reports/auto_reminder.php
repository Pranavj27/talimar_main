
<div class="page-content-wrapper">

	<div class="page-content">

		<div class="page-head">

				<!-- BEGIN PAGE TITLE -->

				<div class="page-title">

					<h1> &nbsp; Auto-Reminder Report   </h1>	

					

				</div>

				<div class="top_download">
					
					<a href="<?php echo base_url();?>download_auto_reminder">

					<button  class="btn blue">PDF</button>

					</a>
					

				</div>

		</div>

		<div class="rc_class">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >

				<thead>

					<tr>
						<th>Loan Number</th>
						<th>Contact Name</th>
						<th>Contact Phone</th>
						<th>Property Address </th>
						<th>Unit#</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
						<th>Payment Reminder</th>
						<th>Renovation<br> Status Update</th>
						<th>Property Insurance</th>
						<th>Loan Maturity</th>
						
					
					</tr>

				</thead>

				<tbody>
					<?php
					$count = 0;
					 foreach($all_reminder_loan as $row){

						$count++;
						if($row['constructionUpdate'] == 'on'){
							$constructionUpdate = 'On';
						}else{
							$constructionUpdate = 'Off';
						}

						if($row['paymentReminder'] == 'on'){
							$paymentReminder = 'On';
						}else{
							$paymentReminder = 'Off';
						}

						if($row['propertyInsurance'] == 'on'){
							$propertyInsurance = 'On';
						}else{
							$propertyInsurance = 'Off';
						}

						if($row['maturityNotification'] == 'on'){
							$maturityNotification = 'On';
						}else{
							$maturityNotification = 'Off';
						}
						?>
					<tr>
						
						<td><a href="<?php echo base_url()?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['talimar_loan'];?></a></td>
						<td><?php echo $row['contact_name'];?></td>
						<td><?php echo $row['contact_phone'];?></td>
						<td><?php echo $row['property_address'];?></td>
						<td><?php echo $row['property_unit'];?></td>
						<td><?php echo $row['property_city'];?></td>
						<td><?php echo $row['property_state'];?></td>
						<td><?php echo $row['property_zip'];?></td>
						<td><?php echo $paymentReminder;?></td>
						<td><?php echo $constructionUpdate;?></td>
						<td><?php echo $propertyInsurance;?></td>
						<td><?php echo $maturityNotification;?></td>
						
					</tr>
					<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<th>Total: <?php echo $count;?></th>
						<th colspan="9"></th>
					</tr>
				</tfoot>
				
				

			</table>

			

	</div>

	</div>

	<!-- END CONTENT -->

</div>
<!--
<script src="//code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
-->
<script>
$(document).ready(function() {
    $('#table').DataTable();
} );
</script>