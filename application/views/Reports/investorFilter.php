<?php
$contact_record_limit 	= $this->config->item('contact_record_limit');
$account_status_option  = $this->config->item('account_status_option');
$yes_no_option3 		= $this->config->item('yes_no_option3');
?>
<style type="text/css">
	a {
    text-shadow: none;
}
.table_All tbody tr td:first-child {
     font-weight: 400; 
}
</style>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Lender Contact Schedule</h1>
			</div>
			<div class="top_download">
			    <form method="POST" action="<?php echo base_url();?>ReportData/ajax_lender_filters" id="x" class="pdf_dv_loan_schedule">
			    	<input type="hidden" name="reportDisplay" value="pdf">
			    	<input type="hidden" name="FundedTrustDeed" value="<?php echo isset($FundedTrustDeed) ? $FundedTrustDeed : '';?>">
			    	<input type="hidden" name="ActiveTrustDeed" value="<?php echo isset($ActiveTrustDeed) ? $ActiveTrustDeed : '';?>">
			    	<input type="hidden" name="WholeInvestor" value="<?php echo isset($WholeInvestor) ? $WholeInvestor : '';?>">
			    	<input type="hidden" name="lender_portal" value="<?php echo isset($lender_portal) ? $lender_portal : '';?>">
					<button  class="btn red aa" type="button" onclick="load_full_table('pdf')" >PDF</button> 
					<button  class="btn green dsds" type="button" onclick="load_full_table('excel')">Excel</button> 
					<button type="button" onclick="load_form()"  class="btn btn-primary btn-sm" style="width: 70px;height: 33px;margin-top: 15px;">Filter</button>
				</form>
				
			</div>			
		</div>
		<div class="row">
		</div>
		<div class="row" style="display: none;">
			<form method="post" action="<?php echo base_url();?>ReportData/lenderFilter">
				<div class="col-md-2">
					<label>Funded Trust Deed:</label>
					<select name="FundedTrustDeed" class="form-control FundedTrustDeedDisabled" onchange="disableActiveTrustDeed(this.value)">
						<option value="">Select All</option>
						<?php
						if(!empty($yes_no_option3)){
							foreach($yes_no_option3 as $key_option => $row_option)
							{
								if($key_option)
								{
									$selected = (isset($FundedTrustDeed) && $FundedTrustDeed == $key_option) ? 'selected' : '';
									echo '<option value="'.$key_option.'" '.$selected.' >'.$row_option.'</option>';
								}
							}
						}
						?>
					</select>
				</div>
				<div class="col-md-2">
					<label>Active Trust Deed:</label>
					<select name="ActiveTrustDeed"  class="form-control ActiveTrustDeedDisabled"  onchange="FundedTrustDeedDisabled(this.value)">
						<option value="">Select All</option>
						<?php
						if(!empty($yes_no_option3)){
							foreach($yes_no_option3 as $key_option => $row_option)
							{
								if($key_option)
								{
									$selected = (isset($ActiveTrustDeed) && $ActiveTrustDeed == $key_option) ? 'selected' : '';
									echo '<option value="'.$key_option.'" '.$selected.' >'.$row_option.'</option>';
								}
							}
						}?>
					</select>
				</div>
				<div class="col-md-2">
					<label>Whole Note Investor:</label>
					<select name="WholeInvestor" class="form-control">
						<option value="">Select All</option>
						<?php
						if(!empty($yes_no_option3)){
							foreach($yes_no_option3 as $key_option => $row_option)
							{
								if($key_option)
								{
									$selected = (isset($WholeInvestor) && $WholeInvestor == $key_option) ? 'selected' : '';
									echo '<option value="'.$key_option.'" '.$selected.' >'.$row_option.'</option>';
								}
							}
						}?>
					</select>
				</div>
				<div class="col-md-2">
					<label>Lender Portal:</label>
					<select name="lender_portal" class="form-control">
						<option value="">Select All</option>
						<?php
						if(!empty($yes_no_option3)){
							foreach($yes_no_option3 as $key_option => $row_option)
							{
								if($key_option)
								{
									$selected = (isset($lender_portal) && $lender_portal == $key_option) ? 'selected' : '';
									echo '<option value="'.$key_option.'" '.$selected.' >'.$row_option.'</option>';
								}
							}
						}?>
					</select>
				</div>
				<div class="col-md-2">
				</div>
			</form>
		</div>
		<br>
		<br>
		<div class="row">
			<div class="table-upper-section ajax_table_talimar table_All">
				<table id="existing_loan_schedule_tab" class="table-responsive table table-bordered table-striped table-condensed flip-content dataTable no-footer">
					<thead>
						<tr>
							<th>Name</th>
							<th># of<br>Accounts</th>
							<th># of<br>Total<br>Loans</th>
							<th>$ of<br>Total<br>Loans</th>
							<th># of<br>Active<br>Loans</th>
							<th>$ of<br>Active<br>Loans</th>
							<th>Committed<br>Capital</th>
							<th>Available<br>Capital</th>
							<!-- <th>Date Added</th>	 -->	
							<th>Phone</th>			
							<th>E-Mail</th>	
						</tr>
					</thead>
					<tbody></tbody>
					<tfoot>					
						<tr>
							<th>Total: <span class="total_contact_name lp_spin_load"></span></th>
							<th><span class="total_account_no lp_spin_load"></span></th>
							<th><span class="total_loan_count lp_spin_load"></span></th>
							<th>$<span class="total_loan_amount lp_spin_load"></span></th>
							<th><span class="active_loan_count lp_spin_load"></span></th>
							<th>$<span class="active_loan_amount lp_spin_load"></span></th>
							<th>$<span class="commited_capital_amount lp_spin_load"></span></th>
							<th>$<span class="avilable_capital_amount lp_spin_load"></span></th>
							<!-- <th></th> -->
							<th></th>
							<th></th>			
						</tr>	
					</tfoot>
				</table>
			</div>
		</div>
			
	</div>
	<!-- END CONTENT -->
</div>
<script type="text/javascript" src="<?php echo base_url("assets/js/reports/loan_filter.js"); ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/reports/lender_filter.css"); ?>">


	


