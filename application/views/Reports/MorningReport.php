<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');

?>
<style>
tr.thead_dark_blue {
    background: #003468;
    color: white;
    text-align: center;
}

tr td{
	text-align: left !important;
}
</style>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Loan Servicing Morning Report </h1>	
					
				</div>
				
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>reports/MorningReport_pdf">
						<button type="submit" class="btn blue">PDF</button>
					</form>
				</div>
				
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<h4><b>Loan Balance (#):</b> <?php echo $totalcountActive;?></h4>
			</div>
			
			<div class="col-md-12">
				<h4><b>Loan Balance ($):</b> $<?php echo number_format($totalAmountActive,2);?></h4>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
						<thead class="thead_dark_blue">
							<tr>
								<th colspan="3">Loan in Boarding</th>
							</tr>
						</thead>
						<thead>
							<tr>
								<th>Property Address</th>
								<th>Borrower Name</th>
								<th>Loan Amount</th>
							</tr>
						</thead>
						<tbody>
						<?php if(isset($boarding_data) && is_array($boarding_data)){ 
								foreach($boarding_data as $row){ ?>
									<tr>
										<td><?php echo $row['property_address'];?></td>
										<td><?php echo $Fetch_All_borrowername[$row['borrower']];?></td>
										<td>$<?php echo number_format($row['loan_amount'],2);?></td>
									</tr>
						<?php } }else{ ?>
									<tr>
										<td colspan="3">No data found!</td>
									</tr>
						<?php } ?>
						</tbody>
				</table>
			</div>
			<div class="col-md-6">
				<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
						<thead class="thead_dark_blue">
							<tr>
								<th colspan="3">Loans in Holding</th>
							</tr>
						</thead>
						<thead>
							<tr>
								<th>Property Address</th>
								<th>Borrower Name</th>
								<th>Loan Amount</th>
							</tr>
						</thead>
						<tbody>
						<?php if(isset($holding_data) && is_array($holding_data)){ 
								foreach($holding_data as $row){ ?>
									<tr>
										<td><?php echo $row['property_address'];?></td>
										<td><?php echo $Fetch_All_borrowername[$row['borrower']];?></td>
										<td>$<?php echo number_format($row['loan_amount'],2);?></td>
									</tr>
						<?php } }else{ ?>
									<tr>
										<td colspan="3">No data found!</td>
									</tr>
						<?php } ?>
						</tbody>
				</table>
			</div>
	</div>
	
	<div class="row">
			<div class="col-md-6">
				<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
						<thead class="thead_dark_blue">
							<tr>
								<th colspan="3">Outstanding Draws </th>
							</tr>
						</thead>
						<thead>
							<tr>
								<th>Street Address</th>
								<th>Amount Requested</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
				</table>
			</div>
			<div class="col-md-6">
				<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
						<thead class="thead_dark_blue">
							<tr>
								<th colspan="3">Late Payments (10+ Days)</th>
							</tr>
						</thead>
						<thead>
							<tr>
								<th>Property Address</th>
							
							</tr>
						</thead>
						<tbody>
						<?php if(isset($count_10_days) && is_array($count_10_days)){ 
								foreach($count_10_days as $row){ ?>
									<tr>
										<td><?php echo $row->property_address;?></td>
									</tr>
						<?php } }else{ ?>
									<tr>
										<td>No data found!</td>
									</tr>
						<?php } ?>
						</tbody>
				</table>
			</div>
	</div>
	<div class="row">
			<div class="col-md-6">
				<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
						<thead class="thead_dark_blue">
							<tr>
								<th colspan="3">Maturing Loans</th>
							</tr>
						</thead>
						<thead>
							<tr>
								<th>Property Address</th>
								<th>Maturity Date</th>
								<th>Remaining Days</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
				</table>
			</div>
			<div class="col-md-6">
				<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
						<thead class="thead_dark_blue">
							<tr>
								<th colspan="3">Loans in Default </th>
							</tr>
						</thead>
						<thead>
							<tr>
								<th>Property Address</th>
								<th>Borrower Name</th>
								<th>Loan Amount</th>
							</tr>
						</thead>
						<tbody>
						<?php if(isset($default_loan) && is_array($default_loan)){ 
								foreach($default_loan as $row){ ?>
									<tr>
										<td><?php echo $row['property_address'];?></td>
										<td><?php echo $Fetch_All_borrowername[$row['borrower']];?></td>
										<td>$<?php echo number_format($row['loan_amount'],2);?></td>
									</tr>
						<?php } }else{ ?>
									<tr>
										<td colspan="3">No data found!</td>
									</tr>
						<?php } ?>
						</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
						<thead class="thead_dark_blue">
							<tr>
								<th colspan="3">Property Insurance </th>
							</tr>
						</thead>
						<thead>
							<tr>
								<th>Street Address</th>
								<th>Borrower Name</th>
								<th>Contact Name</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
				</table>
			</div>
			<div class="col-md-6">
				<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
						<thead class="thead_dark_blue">
							<tr>
								<th colspan="2">Requested Demands</th>
							</tr>
						</thead>
						<thead>
							<tr>
								<th>Street Address</th>
								<th>Loan Amount</th>
								
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td></td>
								
							</tr>
						</tbody>
				</table>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-6">
				<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
						<thead class="thead_dark_blue">
							<tr>
								<th colspan="3">Loan Payoffs</th>
							</tr>
						</thead>
						<thead>
							<tr>
								<th>Street Address</th>
								<th>Loan Amount</th>
								<th>Payoff Date</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
				</table>
			</div>
		</div>
	
</div>
<script>


$(document).ready(function() {
	 
    $('#table').DataTable();
	
});



</script>




