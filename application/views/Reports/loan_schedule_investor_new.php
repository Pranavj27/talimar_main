<style>

.chosen-drop {
    width: 230px !important;
    margin-left: 0px !important;
}	
.row.borrower_data_row div {
    width: 96% !important;
}
.chosen-container{
	display: block !important;
}
.chosen-container.chosen-container-single {
    width: 230px !important;
}
.chosen-container-single .chosen-single {
    padding: 4px 0px 26px 6px !important;
    border-radius: 4px !important;
    border: 1px solid #e1e1e1 !important;
}

</style>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title"><h1>&nbsp;&nbsp;Trust Deed Schedule by Contact</h1></div>				
			<div class="top_download">
				<form   id="select_investor" method="POST" action="<?php echo base_url().'download_trust_deed_lender';?>" class="pdf_dv_loan_schedule">
					<input type="hidden" name="investor_id" value="<?php echo isset($select_investor_id) ? $select_investor_id : ''; ?>">
					<input type="hidden" name="contact_id" value="<?php echo isset($contact_id) ? $contact_id : ''; ?>">
					<input type="hidden" name="select_box" value="<?php echo isset($select_box) ? $select_box : ''; ?>">
					<input type="hidden" name="reportDisplay" value="">
					<input type="hidden" name="contact_id_uri" value="<?php echo isset($contact_id_uri) ? $contact_id_uri : ''; ?>">
					
				</form>
				<button  class="btn green" type="button" onclick="load_form()">Filter</button>
				<button  class="btn blue" type="button" onclick="load_full_table('pdf');">PDF</button>
			</div>				
		</div>
		<div class="row">
			<div class="col-md-3">
				Contact Name :  &nbsp;
				<select name="contact_id" id ="contact_id" class="chosen" onchange = "select_investor_report(this.value)">
					<option value="">Select One</option>
					<?php 
					foreach($all_lender_contact as $row)
					{
					?>
						<option value="<?php echo $row->contact_id;?>" <?php if(isset($contact_id)){  if($contact_id == $row->contact_id){ echo 'selected'; } }?> ><?php echo $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname; ?></option>
					<?php
					}
					?>
				</select>
			</div>	
			<div class="col-md-3">
				Lender Name :  &nbsp;
				<select name="investor_id" id ="investor_id" class="chosen" onchange = "select_investor_report(this.value)">
					<option value=''>Select All</option>
					<?php 
					foreach($all_investor as $row)
					{
						?>
						<option value="<?php echo $row->id; ?>" <?php if(isset($select_investor_id)){ if($select_investor_id == $row->id ){ echo 'selected'; } } ?> ><?php echo $row->name; ?></option>
						<?php
					}
					?>
				</select>
			</div>		
			<div class="col-md-3">
				Loan Status :  &nbsp;
				<select name="select_box" id ="select_box" class="form-control"  onchange = "select_investor_report(this.value)">
					<option value=""  >Select All</option>
					<option value="2" <?php if(isset($select_box)){ if($select_box == 2){ echo 'selected'; } } ?> >Active</option>
					<option value="3" <?php if(isset($select_box)){ if($select_box == 3){ echo 'selected'; } } ?> >Paid Off</option>
					<option value="1" <?php if(isset($select_box)){ if($select_box == 1){ echo 'selected'; } } ?> >Pipeline </option>
				</select>
			</div>	
		</div>
		<br>
		<br>
		<div class="row">
			<div class="table-upper-section ajax_table_talimar table_All">
				<table id="schedule_by_contact" class="table-responsive table table-bordered table-striped table-condensed flip-content dataTable no-footer">
					<thead>
						<tr>
							<th>Lender Name</th>
							<th>Property Address</th>
							<th>Unit #</th>
							<th>City</th>
							<th>State</th>
							<th>Zip</th>
							<th>Loan<br>Condition</th>
							<th>Loan<br>Status</th>
							<th>Servicer #</th>			
							<th>Loan<br>Amount</th>	

							<th>$<br>Interest</th>
							<th>%<br>Interest</th>
							<th>%<br>Interest</th>
							<th>Monthly<br>Payment</th>
							<th>Loan<br>Term</th>
							<th>Loan <br>Servicer</th>

						</tr>
					</thead>
					<tbody></tbody>
					<tfoot>					
						<tr>
							<th>Total:</th>
							<th><span class="total_records lp_spin_load"></span></th>
							<th ></th>
							<th ></th>
							<th ></th>
							<th ></th>
							<th></th>
							<th></th>
							<th></th>
							<th>$<span class="total_loan_amount lp_spin_load"></span></th>
							<th>$<span class="total_invest_amount lp_spin_load"></span></th>
							<th></th>
							<th></th>
							<th>$<span class="total_monthly_amount lp_spin_load"></span></th>
							<th></th>	
							<th></th>	
						</tr>	
						<tr>
							<th>Average</th>
							<th colspan="10"></th>
							<th><span class="total_interest_persent_ft lp_spin_load"></span></th>
							<th ><span class="total_lender_rate_ft lp_spin_load"></span></th>
							<th ></th>
							<th>$<span class="total_loan_term_ft lp_spin_load"></span></th>
							<th></th>
						</tr>	
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url("assets/js/reports/schedule_by_contact.js"); ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/reports/lender_filter.css"); ?>">