<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');
$not_applicable_option = $this->config->item('not_applicable_option');

$loan_status_filter = array(
								'' => 'Select All',
								1  => 'Pipeline',
								2  => 'Active',
								3  => 'Paid Off',
							);

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Properties Listed for Sale</h1>	
					
				</div>
				
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>Reports/PropertyList_pdf">
					
						<button type="submit" class="btn blue">PDF</button>
						
					</form>
				</div>
				
		</div>
		
		
		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Borrower Name</th>
					<th>Street Address</th>
					<th>Unit #</th>
					<th>City</th>
					<th>State</th>
					<th>Zip</th>
					<th>Loan Amount</th>
					<th>Underwriting <br>Value</th>
					<th>List Price</th>
					<th>Difference</th>
					<th>LTV Ratio</th>
										
				</tr>
				</thead>
				<tbody>
					<?php 
					$count = 0;
					if(isset($PropertiesListedArray) && is_array($PropertiesListedArray)){ 
						foreach($PropertiesListedArray as $row){ 
							$count++;
							$diff = $row['SalePrice'] - $row['underwriting_value'];
							if($diff > 0){

								$diffVals = '';
								$diffVale = '';
								$diff = $diff;
							}else{
								$diffVals = '(';
								$diffVale = ')';
								$diff = str_replace("-", "", $diff);
							}

							?>
						
							<tr>
								<td><a href="<?php echo base_url();?>borrower_view/<?php echo $row['borrower'];?>"><?php echo $Fetch_All_borrowername[$row['borrower']];?></a></td>
								<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['property_address'];?></a></td>
								<td><?php echo $row['unit'];?></td>
								<td><?php echo $row['city'];?></td>
								<td><?php echo $row['state'];?></td>
								<td><?php echo $row['zip'];?></td>
								<td>$<?php echo number_format($row['loan_amount'],2);?></td>
								<td>$<?php echo number_format($row['underwriting_value'],2);?></td>					
								<td>$<?php echo number_format($row['SalePrice'],2);?></td>			
								<td>$<?php echo $diffVals;?><?php echo number_format($diff,2);?><?php echo $diffVale;?></td>	
								<td><?php echo number_format($row['loan_amount'] / $row['SalePrice']*100,2);?>%</td>
							</tr>
				
						<?php } }else{ ?>
				
							<tr>
								<td style="width:100%;" colspan="11">No data found!</td>
							</tr>
						<?php } ?>
				</tbody>

				<tfoot>
					<tr>
						<th>Total: <?php echo $count;?></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
				</tfoot>		
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {
	 
    $('#table').DataTable();
	
});



</script>




