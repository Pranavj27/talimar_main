<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');
$not_applicable_option = $this->config->item('not_applicable_option');

$loan_status_filter = array(
								'' => 'Select All',
								//1  => 'Pipeline',
								2  => 'Active',
								3  => 'Paid Off',
							);

$loan_servicer_option = array(
								'' => 'Select All',
								//1  => 'Pipeline',
								14  => 'FCI Lender Services',
								19  => 'Del Toro Loan Servicing',
							);

$boarding_option = array(
								'' => 'Select All',
								1  => 'Yes',
								2  => 'No'
								
							);

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Servicer Report Check</h1>	
					
				</div>
				
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>reports/ServicerCheck_pdf">
					
						<input type="hidden" name="loan_status" value="<?php echo isset($loan_status) ? $loan_status : '';?>">
						<input type="hidden" name="loan_servicer" value="<?php echo isset($loan_servicer) ? $loan_servicer : '';?>">
						<input type="hidden" name="boarding_data" value="<?php echo isset($boarding_data) ? $boarding_data : '';?>">
						<button type="submit" class="btn blue">PDF</button>
						
					</form>
				</div>
				
		</div>
		
		<div class="row">
		<form method="post" action="<?php echo base_url();?>reports/ServicerCheck">
			<div class="col-md-2">
				<label><b>Loan status:</b></label>
				<select name="loan_status" class="form-control">
					<?php foreach($loan_status_filter as $key => $row){ ?>
						<option value="<?php echo $key;?>" <?php if($loan_status == $key){echo 'selected';}?>><?php echo $row;?></option>
					<?php } ?>
				</select>
			</div>
			
			<div class="col-md-2">
				<label><b>Loan Subservicer:</b></label>
				<select name="loan_servicer" class="form-control">
					<?php foreach($loan_servicer_option as $key => $row){ ?>
						<option value="<?php echo $key;?>" <?php if($loan_servicer == $key){echo 'selected';}?>><?php echo $row;?></option>
					<?php } ?>
				</select>
			</div>

			<div class="col-md-2">
				<label><b>Boarding Status:</b></label>
				<select name="boarding_data" class="form-control">
					<?php foreach($boarding_option as $key => $row){ ?>
						<option value="<?php echo $key;?>" <?php if($boarding_data == $key){echo 'selected';}?>><?php echo $row;?></option>
					<?php } ?>
				</select>
			</div>
			
			<div class="col-md-2">
				<button type="submit" name="submit" class="btn btn-sm btn-primary" style="margin-top:25px;">Filter</button>
			</div>
		</form>
		</div>
		
		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Servicer #</th>
					<th>Borrower Name</th>
					<th>City</th>
					<th>State</th>
					<th>Zip</th>
					<th>Maturity Date</th>
					<th>Original Loan <br>Balance</th>
					<th>Current Loan <br>Balance</th>
					<th>Note Rate</th>
					<th>Lender Rate</th>
					<th>Monthly Payment</th>				
				</tr>
				</thead>
				<tbody>
					<?php

					$count = 0;
					$loanTotal = 0;
					$currentTotal = 0;
					$monthlyTotal = 0;
					 if(isset($servicercheck) && is_array($servicercheck)){ 
						foreach($servicercheck as $row){ 

								$count++;
								$currentTotal += $row['current_balance'];
								$loanTotal += $row['loan_amount'];
								$monthlyTotal += $row['monthlyPayment'];

								if($row['servicing_lender_rate'] == 'NaN' || $row['servicing_lender_rate'] == '' || $row['servicing_lender_rate'] == '0.00'){


										$lender_ratessss = $row['invester_yield'];
										
									}else{

									    $lender_ratessss = $row['servicing_lender_rate'];
									}


							?>
						
							<tr>
								<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['fci'];?></a></td>								
								<td><?php echo $Fetch_All_borrowername[$row['borrower']];?></td>					
								<td><?php echo $row['city'];?></td>				
								<td><?php echo $row['state'];?></td>				
								<td><?php echo $row['zip'];?></td>				
								<td><?php echo date('m-d-Y', strtotime($row['maturity_date']));?></td>
								<td>$<?php echo number_format($row['loan_amount'],2);?></td>
								<td>$<?php echo number_format($row['current_balance'],2);?></td>		
								<td><?php echo number_format($row['intrest_rate'],3);?>%</td>				
								<td><?php echo number_format($lender_ratessss,3);?>%</td>
								<td>$<?php echo number_format($row['monthlyPayment'],2);?></td>							
							</tr>
				
						<?php } }else{ ?>
				
					<tr>
						<td style="width:100%;" colspan="11">No data found!</td>
					</tr>
				<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<th>Total: <?php echo $count;?></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th>$<?php echo number_format($loanTotal,2);?></th>
						<th>$<?php echo number_format($currentTotal,2);?></th>
						<th></th>
						<th></th>
						<th>$<?php echo number_format($monthlyTotal,2);?></th>
					</tr>
				</tfoot>		
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {
	 
    $('#table').DataTable();
	
});



</script>




