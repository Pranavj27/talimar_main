<?php
error_reporting(0);
$yes_no_option 			= $this->config->item('yes_no_option3');
$loan_type_option 		= $this->config->item('loan_tye');
$yeild_percent_option 	= $this->config->item('yeild_percent');
$position_optionn       = $this->config->item('position_optionn');
$lender_ownership_type  = $this->config->item('lender_ownership_type');
// echo '<pre>';
// print_r($fetch_lender_details);
// echo '</pre>';


?>

<style>
span.span-box {
    background: #4b8df859;
    margin: 2px 2px;
    padding: 2px !important;
    border: 1px solid #4b8df8;
    border-radius: 6px;
    font-size: 12px;
    font-weight: 600;
}
</style>
<div class="page-content-wrapper">

	<div class="page-content responsive">

		<div class="page-head">

				<!-- BEGIN PAGE TITLE -->

				<div class="page-title">

					<h1> &nbsp; Lender Filter  </h1>	

				</div>

				<div class="top_download">

					<a href="#">

					<button  class="btn blue">PDF</button>

					</a>
					

				</div>

		</div>

		<div class="row">
			<form id="lender_filter" method="POST" action="<?php echo base_url()?>lender_filter">
				
				<div class="col-md-2">
					Yield Requirements:  &nbsp;
					<select name="yeild" class="form-control">
						<option value="">Select All</option>
						<?php
							foreach($yeild_percent_option as $key_option => $row_option)
							{
								$selected = (isset($filter_yeild) && $filter_yeild == $key_option) ? 'selected' : '';
								echo '<option value="'.$key_option.'" '.$selected.' >'.$row_option.'</option>';
							}
						?>
					</select>
				</div>
				<div class="col-md-2">
					Position:  &nbsp;
					<select name="position" class="form-control">
						<option value="">Select All</option>
						<?php
							foreach($position_optionn as $key_option => $row_option)
							{
								$selected = (isset($filter_position) && $filter_position == $key_option) ? 'selected' : '';
								echo '<option value="'.$key_option.'" '.$selected.' >'.$row_option.'</option>';
							}
						?>
					</select>
				</div>
				<div class="col-md-2">
					Loan Type:  &nbsp;
					<select name="loan_type" class="form-control">
						<option value="">Select All</option>
						<?php
							foreach($loan_type_option as $key_option => $row_option)
							{
								if($key_option)
								{
									$selected = (isset($filter_loan_type) && $filter_loan_type == $key_option) ? 'selected' : '';
									echo '<option value="'.$key_option.'" '.$selected.' >'.$row_option.'</option>';
								}
							}
						?>
					</select>
				</div>
				<div class="col-md-3">
					Investor Setup Package Complete:  &nbsp;
					<select name="investor_setup" class="form-control">
						<option value="">Select All</option>
						<?php
							foreach($yes_no_option as $key_option => $row_option)
							{
								if($key_option)
								{
									$selected = (isset($filter_investor_setup) && $filter_investor_setup == $key_option) ? 'selected' : '';
									echo '<option value="'.$key_option.'" '.$selected.' >'.$row_option.'</option>';
								}
							}
						?>
					</select>
				</div>
				<div class="col-md-2">
					Fractional Interest:
					<select name="fractional_int" class="form-control">
						<option value="">Select All</option>
						<?php foreach($lender_ownership_type as $key => $row){ ?>
							<option value="<?php echo $key; ?>" <?php if($key == $fractional_int){ echo 'selected';}?>><?php echo $row; ?></option>
						<?php  } ?>
					</select>
				</div>
				
				<div class="col-md-1">
					&nbsp;&nbsp;
					<button type="submit" class="btn blue" name="submit" value="filter">Filter</button>
				</div>
			</form>
		</div>

		<div class="rc_class">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >

				<thead>

					<tr>

						<th>Contact Name</th>
						<!--<th>Active Lender</th>-->
						<th>Yield Requirements</th>
						<th>Position</th>
						<th>Loan Type</th>
						<th>Investor Setup<br> Package Complete</th>
						<th>Fractional<br> Interest</th>
						<th>Available<br>Capital</th>

					</tr>

				</thead>

				<tbody>
				<?php 


					//echo '<pre>';
					//print_r($fetch_lender_details);
					//echo '</pre>';

				foreach($fetch_lender_details as $key => $row){

					
						$string_loan = $row['loan'];
						$array_loan  = explode(',',$string_loan);

						$string_yield = $row['yield'];
						$array_yield  = explode(',',$string_yield);

						$string_ownership_type = $row['ownership_type'];
						$array_ownership  = explode(',',$string_ownership_type);

						// Yield Filter
						$filter_yeild_status = 1;
						if(isset($filter_yeild))
						{
							if(in_array($filter_yeild,$array_yield))
							{
								$filter_yeild_status = 1;
							}
							else
							{
								$filter_yeild_status = 0;
							}
						}


						$filter_loan_type_status = 1;
						if(isset($filter_loan_type))
						{
							if(in_array($filter_loan_type,$array_loan))
							{
								$filter_loan_type_status = 1;
							}
							else
							{
								$filter_loan_type_status = 0;
							}
						}

						$filter_position_status = 1;
						if(isset($filter_position))
						{
							if($filter_position == $row['position'])
							{
								$filter_position_status = 1;
							}
							else
							{
								$filter_position_status = 0;
							}
						}

						$filter_investor_setup_status = 1;
						if(isset($filter_investor_setup))
						{
							if($filter_investor_setup == $row['setup_package'])
							{
								$filter_investor_setup_status = 1;
							}
							else
							{
								$filter_investor_setup_status = 0;
							}
						}

						$fractional_int_status = 1;
						if(isset($fractional_int))
						{
							if(in_array($fractional_int, $array_ownership))
							{
								$fractional_int_status = 1;
							}
							else
							{
								$fractional_int_status = 0;
							}
						}


						if($filter_yeild_status == 1 && $filter_loan_type_status == 1 && $filter_position_status == 1 && $filter_investor_setup_status == 1 && $fractional_int_status == 1)
						{
							?>
							<tr>
								<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>">
									<?php echo $row['name'];?></a></td>
								<!--<td><?php echo $row['active'];?></td>-->
								<td><?php foreach($array_yield as $yield_row){ echo $yeild_percent_option[$yield_row] ? '<span class="span-box">'.$yeild_percent_option[$yield_row].'</span>' : ''; } ?></td>
								<td><?php echo $position_optionn[$row['position']];?></td>
								<td><?php foreach($array_loan as $loan_row){ echo $loan_type_option[$loan_row] ?'<span class="span-box">'.$loan_type_option[$loan_row].'</span>' : ''; } ?></td>
								<td><?php echo $yes_no_option[$row['setup_package']];?></td>

								<td><?php foreach($array_ownership as $rows){ echo $lender_ownership_type[$rows] ?'<span class="span-box">'.$lender_ownership_type[$rows].'</span>' : ''; } ?></td>

								<td><?php echo '$'.number_format($row['available_caps']);?></td>
												

							</tr>
							<?php 
						} 
					} 
					?>
				</tbody>

			</table>

	</div>

	</div>

	<!-- END CONTENT -->

</div>
<script>
$(document).ready(function() {
    $('#table').DataTable();
} );



function fetch_active_lender(that){

	$('form#lender_filter').submit();

}

</script>
