<?php

error_reporting(0);

// echo "<pre>";

// print_r($fci_service_loan);

// echo "</pre>";

?>

<div class="page-content-wrapper">

	<div class="page-content responsive">

		<div class="page-head">

				<!-- BEGIN PAGE TITLE -->

				<div class="page-title">

					<h1> &nbsp; RE881 Data Report  </h1>	

					

				</div>

						<div class="talimar_no_dropdowns">
					
					
					<form method="POST" action="<?php echo base_url();?>re881_disclosure_pdf">
						<input type="hidden" name="start" value="<?php echo isset($start) ? $start : ''; ?>" >
						<input type="hidden" name="end" value="<?php echo isset($end) ? $end : ''; ?>" >
						<input type="hidden" name="select_box" value="<?php echo isset($select_box) ? $select_box : ''; ?>" >
						<input type="hidden" name="select_multi_lender" value="<?php echo isset($select_multi_lender) ? $select_multi_lender : ''; ?>" >
						<input type="hidden" name="select_fund_broker" value="<?php echo isset($select_fund_broker) ? $select_fund_broker : ''; ?>" >
						<input type="hidden" name="select_pre_penalty" value="<?php echo isset($select_pre_penalty) ? $select_pre_penalty : ''; ?>" >
						<button  type="submit" class="btn blue">PDF</button>

					</form>
					
					</div>

		</div>
		<!-------------------------------Error message show ------------------->
		<?php if($this->session->flashdata('success')!=''){  ?>
		 <div id='success'><i class='fa fa-thumbs-o-up'></i><?=$this->session->flashdata('success');?></div><?php } ?>
		 
		 <?php if($this->session->flashdata('error')!=''){  ?>
		 <div id='error'><i class='fa fa-thumbs-o-down'></i><?=$this->session->flashdata('error');?></div><?php } ?>
		<!-------------------------------End Error message show ------------------->
		<div class="row">
			<div class="talimar_no_dropdowns">
				<form method="POST" action="<?php echo base_url().'re881_disclosure_data'?>">
					
					<div class="float-direction-left">
						Start Date: <input class="form-control datepicker" name="start" placeholder="MM/DD/YYYY" value="<?php echo isset($start) ? $start : ''; ?>">
					</div>	
					<div class="float-direction-left">
						End Date: <input class="form-control datepicker" name="end" placeholder="MM/DD/YYYY" value="<?php echo isset($end) ? $end : ''; ?>" >
					</div>
					<div class="float-direction-left">
						Loan Status: <br>
						<select name="select_box">
							<option <?php  if($select_box == 1){ echo 'selected'; } ?> value="1" >Select All</option>
							<option value="2" <?php  if($select_box == 2){ echo 'selected'; } ?>>Active</option>
							<option value="3" <?php  if($select_box == 3){ echo 'selected'; } ?>>Paid Off</option>
						</select>
					</div>
					<div class="float-direction-left">
						Is Multi lender: <br>
						<select name="select_multi_lender">
							
							<option value="3" <?php if(isset($select_multi_lender)){ if($select_multi_lender == 3){ echo 'selected'; }} ?>>Select All</option>
							<option value="2" <?php if(isset($select_multi_lender)){ if($select_multi_lender == 2){ echo 'selected'; }} ?>>Yes</option>
							<option value="1" <?php if(isset($select_multi_lender)){ if($select_multi_lender == 1){ echo 'selected'; }} ?>>No</option>
							
						</select>
					</div>
					<div class="float-direction-left">
						Funded Directly: <br>
						<select name="select_fund_broker">
							
							<option value="1" <?php if(isset($select_fund_broker)){ if($select_fund_broker == 1){ echo 'selected'; }} ?>>Select All</option>
							<option value="2" <?php if(isset($select_fund_broker)){ if($select_fund_broker == 2){ echo 'selected'; }} ?>>Yes</option><option value="3" <?php if(isset($select_fund_broker)){ if($select_fund_broker == 3){ echo 'selected'; }} ?>>No</option>
						</select>
					</div>
					<div class="float-direction-left">
						PrePayment Penalty: <br>
						<select name="select_pre_penalty">
							
							<option value="2" <?php if(isset($select_pre_penalty)){ if($select_pre_penalty == 2){ echo 'selected'; }} ?>>Select All</option>
							<option value="1" <?php if(isset($select_pre_penalty)){ if($select_pre_penalty == 1){ echo 'selected'; }} ?>>Yes</option><option value="3" <?php if(isset($select_pre_penalty)){ if($select_pre_penalty == 3){ echo 'selected'; }} ?>>No</option>
						</select>
					</div>	
					<div class="float-direction-margin">
						<input type="submit" class="btn blue" value="Filter">
					</div>	
				</form>
			</div>
		</div>

		<div class="rc_class re870_table">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>

					<tr>
					
						<th>Loan #</th>
						<!--<th>Borrower Name</th>-->
						<th>Loan Amount</th>
						<th>Term</th>
						<th>Interest Rate</th>
						<th>PrePaym Penalty</th>
						<th>Term of PrePay</th>
						<th>Lender Fees</th>
						<th>Other Loan Fees</th>
						<th>Refi of Existing Loan</th>
					</tr>

				</thead>
				
				<tbody>
				<?php
					$key				= 0;
					$total_loan_amount 	= 0;
					$total_lender_fee 	= 0;
					$total_other_fee 	= 0;
					$total_term 		= 0;
					$totalintrest_rate 	= 0;
					$total_intrest 		= 0;
					$count 				= 0;
					if(isset($re881_disclosure_data))
					{
						
						foreach($re881_disclosure_data as $key => $row)
						{
						
							if($row['refi_existing'] == '0'){
								$refi_existing 		= 'No';
								 
								
							}else if($row['refi_existing'] == '1' ){
								$refi_existing 		= 'Yes';
								
							}else {
								$refi_existing 		= 'N/A';
								
							}
							
							if($row['minium_intrest'] == '0'){
								$prepayment_panalty = 'No';
								$term_prepay 		= 'N/A';
							}else if($row['minium_intrest'] == '1'){
								$prepayment_panalty = 'Yes';
								$term_prepay 		= $row['min_intrest_month'].' months'; 
							
							}else {
								
								$prepayment_panalty = 'N/A';
								$term_prepay 		= 'N/A'; 
							}
							?>
							<tr>
								<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['talimar_loan']; ?></a></td>
								
								<!--<td><?php echo $row['borrower_name']; ?></td>-->
								<td>$<?php echo number_format($row['loan_amount']); ?></td>
								<td><?php echo $row['term'].' months'; ?></td>
								<td><?php echo number_format($row['intrest_rate'], 3); ?>%</td>
								<td><?php echo $prepayment_panalty; ?></td>
								<td><?php echo $term_prepay; ?></td>
								
								<td>$<?php echo number_format($row['lender_fee'],2); ?></td>
								<td>$<?php echo number_format($row['other_fee'],2); ?></td>
								
								<td><?php echo $refi_existing; ?></td>
							</tr>
							
							
							<?php
							
							$total_loan_amount 	= $total_loan_amount + $row['loan_amount'];
							$total_lender_fee 	= $total_lender_fee + $row['lender_fee'];
							$total_other_fee 	= $total_other_fee + $row['other_fee'];
							$total_term 		= $total_term + $row['term'];
							$total_intrest 		= $total_intrest + $row['intrest_rate'];
							$count				= $key + 1;
							$key++;
						}
					}
				?>
				</tbody>
				<tfoot>
					<tr>
						<th>Total: <?php echo $key; ?><b> Loans</b></th>
						
						<th><?php echo '$'.number_format($total_loan_amount);?></th>
						
						<th ></th>
						<th ></th>
						<th ></th>
						<th ></th>
						<th><?php echo '$'.number_format($total_lender_fee);?></th>
						<th><?php echo '$'.number_format($total_other_fee);?></th>
						<th ></th>
						
					</tr>
					
					<tr>
						<th>Average:</th>
						<th><?php echo '$'.number_format($total_loan_amount/$count); ?></th>
						<th><?php echo number_format($total_term/$count); ?> months</th>
						<th><?php echo number_format($total_intrest/$count,2); ?> %</th>
						<th ></th>
						<th ></th>
						
						<th><?php echo '$'.number_format($total_lender_fee/$count);?></th>
						<th><?php echo '$'.number_format($total_other_fee/$count);?></th>
						<th ></th>
					</tr>
				</tfoot>
			</table>

			

	</div>

	</div>

	<!-- END CONTENT -->

</div>

<script>
$(document).ready(function() {
    $('#table').DataTable();
} );
</script>
<style>
	

element {

}
.talimar_no_dropdowns select {

	width: 180px;
}
</style>