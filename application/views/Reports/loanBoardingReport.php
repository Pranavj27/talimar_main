<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');

$loan_status_filter = array(
								'' => 'Select All',
								1  => 'Pipeline',
								2  => 'Active',
								3  => 'Paid Off',
							);

$arrayOption = array(
						'' => 'No',
						0  => 'No',
						1  => 'Yes',
					);


/*echo '<pre>';
print_r($loanBoardingData);
echo '</pre>';*/

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Loan Boarding Report</h1>	
					
				</div>
				
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>reports/loanBoardingReport_pdf">
						<button type="submit" class="btn blue">PDF</button>
					</form>
				</div>
				
		</div>
		
		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Property Address</th>
					<th>Unit #</th>
					<th>City</th>
					<th>State</th>
					<th>Zip</th>
					<th>Borrower Name</th>
					<th>Loan Amount</th>
					<th>Pre-Submitted</th>
					<th>Pre-Boarding</th>
					<th>Boarding</th>
					<th>Final Boarding</th>					
				</tr>
				</thead>
				<tbody>
				
				<?php if(isset($loanBoardingData) && is_array($loanBoardingData)){ 
						foreach($loanBoardingData as $row){  ?>
						
									<tr>
										<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['property_address'];?></a></td>
										<td><?php echo $row['unit'];?></td>
										<td><?php echo $row['city'];?></td>
										<td><?php echo $row['state'];?></td>
										<td><?php echo $row['zip'];?></td>
										<td><?php echo $Fetch_All_borrowername[$row['borrower']];?></td>
										<td>$<?php echo number_format($row['loan_amount'],2);?></td>
										<td><?php echo $arrayOption[$row['hud'][0]->checklist];?></td>
										<td><?php echo $arrayOption[$row['hud'][2]->checklist];?></td>
										<td><?php echo $arrayOption[$row['hud'][3]->checklist];?></td>
										<td><?php echo $arrayOption[$row['hud'][1]->checklist];?></td>
									</tr>
				
								<?php  } }else{ ?>
				
					<tr>
						<td style="width:100%;" colspan="11">No data found!</td>
					</tr>
				<?php } ?>
					
				</tbody>		
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {
	 
    $('#table').DataTable();
	
});



</script>




