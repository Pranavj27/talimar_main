<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');

$loan_status_filter = array(
								'' => 'Select All',
								1  => 'Pipeline',
								2  => 'Active',
								3  => 'Paid Off',
								4  => 'Dead',
								5  => 'Brokered',
							);

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Closing Verification</h1>	
					
				</div>
				
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>pdf_closing_verification">
						<input type="hidden" name="loan_status" value="<?php echo isset($loan_status_val) ? $loan_status_val : '';?>">
						<input type="hidden" name="closing_status" value="<?php echo isset($closing_status) ? $closing_status : '';?>">
						<button type="submit" class="btn blue">PDF</button>
					</form>
				</div>
				
		</div>
		<div class="row">
			<form method="POST" action="<?php echo base_url();?>closing_verification">
				<div class="col-md-2">
					<label>Loan Status:</label>
					<select class="form-control" name="loan_status">
						<?php foreach($loan_status_filter as $key => $row){ ?>
							<option value="<?php echo $key;?>" <?php if($loan_status_val == $key){echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>

						<div class="col-md-2">
					<label>Closing Status:</label>
					<select class="form-control" name="closing_status">
						<?php foreach($closing_status_option as $key => $row){ ?>
							<option value="<?php echo $key;?>" <?php if($closing_status == $key){echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>
				<button type="submit" name="submit" class="btn blue" style="margin-top:25px;">Filter</button>
			</form>
		</div>
		
		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Loan #</th>
					<th>Sub Servicer</th>
					<th>Sub Servicer #</th>
					<th>Loan Amount</th>
					<th>Closing Status</th>
					<th>Closing Date</th>
					<th>Loan Status</th>
					<th>Payoff Date</th>
					<th>Payoff Status</th>
					
				</tr>
				</thead>
				<tbody>
					<?php foreach($closing_verification as $row){?>
						<tr>
							<td><a href="<?php echo base_url();?>load_data/<?php echo $row->loan_id;?>"><?php echo $row->talimar_loan;?></a></td>
							<td><?php echo $newsub_servicer_option[$row->sub_servicing_agent];?></td>
							<td><?php echo $row->fci ? $row->fci : '';?></td>
							<td>$<?php echo number_format($row->loan_amount,2);?></td>
							<td><?php echo $closing_status_option[$row->closing_status];?></td>
							<td><?php echo $row->loan_funding_date ? date('m-d-Y', strtotime($row->loan_funding_date)) : '';?></td>
							<td><?php echo $loan_status_option[$row->loan_status];?></td>
							<td><?php echo $row->payoff_date ? date('m-d-Y', strtotime($row->payoff_date)) : '';?></td>
							<td><?php echo $paidoff_status_option[$row->paidoff_status];?></td>
							
						</tr>
					<?php } ?>
				</tbody>	
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {
	 
    $('#table').DataTable();
	
});



</script>




