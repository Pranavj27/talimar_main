<?php
$loan_status_option = $this->config->item('loan_status_option');
$loan_type_option = $this->config->item('loan_type_option');
$property_type = $this->config->item('property_modal_property_type');

?>

<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Loan Portfolio Schedule </h1>	
					
				</div>
				<div class="top_download">
					
					<form method="POST" action="<?php echo base_url();?>download_loan_portfolio_schedule">
					<input type="hidden" name="select_box" value="<?php echo isset($select_box) ? $select_box : ''; ?>">
					<button type="submit" class="btn blue">PDF</button>
					</form>
				</div>
				
		</div>
		<div class="row">
			<form method="POST" action="<?php echo base_url().'loan_portfolio_schedule';?>" id="loan_porfolio_schdule">
			<div class="talimar_no_dropdowns">
				Select Servicer:
				<select name="select_box" onchange="form_sumbit()">
				<?php
				foreach($loan_status_option as $key => $option)
				{
					?>
					<option value="<?php echo $key; ?>"><?php echo $option; ?></option>
					<?php
					
				}
				?>
					
				</select>
			</div>
			</form>
		</div>
		<div class="rc_class">
			<table id="table" class="table th_text_align_center table-bordered table-striped table-condensed flip-content" >
				<thead>
					<tr>
						<th>Property City</th>
						<th>Property State</th>
						<th>Loan Status</th>
						<th>Loan Type</th>
						<th>Property Type</th>
						<th>Rate</th>
						<th>Ltv</th>
						<th>Term</th>
						
					</tr>
				</thead>
				<tbody>
					<?php foreach($fetch_loan_schedule_all as $row){
						
						 // echo '<pre>';
						// print_r($row);
						// echo '</pre>'; 
						$underwriting_value = isset($row['underwriting_value']) ? $row['underwriting_value'] : 1;
						
						$ltv = ($row['loan_amount'] / $underwriting_value);
						
						?>
							<tr>								
								<td><?php echo $row['city'];?></td>
								<td><?php echo $row['state'];?></td>
								<td><?php echo $loan_status_option[$row['loan_status']];?></td>
								<td><?php echo $loan_type_option[$row['loan_type']];?></td>
								<td><?php echo $property_type[$row['property_type']];?></td>
								<td><?php echo number_format($row['intrest_rate'], 3);?></td>
								<td><?php echo number_format($row['underwriting_value'],2);?></td>
								<td><?php echo $row['term_month'];?></td>
								
							</tr>
						
					<?php } ?>
				</tbody>
			</table>
		</div>
		
	</div>
</div>
<script>


function form_sumbit()
{
	$('form#loan_porfolio_schdule').submit();
} 
$(document).ready(function() {
    $('#table').DataTable();
} );</script>
<style>
		.talimar_no_dropdowns {
		float: left;
		
	}
</style>
