<?php $account_status_option = $this->config->item('account_status_option'); ?>
<div class="page-content-wrapper">
	<style type="text/css">
		input.btn.blue.aa {
		  
		    margin-top: 14px;
		    border-radius: 0px;
		   
		}
		.talimar_no_dropdowns div {

		    padding: 0px 2px;
		    width: 24%!important;
		    font-size: 14px!important;
		}

		.talimar_no_dropdowns {
		    padding-left: 0px!important;
		}
		#table_wrapper>.row>.col-md-6.col-sm-12 {
		    
		    padding-left: 0px;
		}
		.form-control[disabled] {
		    cursor: not-allowed;
		    background-color: #dddddd;
		}

		

	</style>
	<div class="page-content responsive">
		<!--
				Update this method for add filter functionality in filter by date in lender data last update date is 24-03-2021
		-->
		<div class="page-head" style="margin-right:0px">
				<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>&nbsp;Loan Balance Comparison</h1>
			
			</div>

			<div class="top_download">	
			    <form method="POST" action="<?php echo base_url();?>reports/paymentCompare">
			    	<input type="hidden" name="reportDisplay" value="pdf">
					<button  class="btn red aa" type="submit">PDF</button>
				</form>
				<form method="POST" action="<?php echo base_url();?>reports/paymentCompare">
					<input type="hidden" name="reportDisplay" value="excel">
					<button  class="btn green aa" type="submit" style="margin-right: 0px;">Excel</button> 
				</form>
			</div>			
		</div>
		<!-- <div class="row">
			<div class="talimar_no_dropdowns" style="float:left;">
				<form id="form_iad" method="POST" action="<?php echo base_url().'reports/paymentCompare'; ?>">
					<div class="float-direction-left" style="    width: 50%!important;">
						Account Status : <br>
						<select name="loan_status" id="loan_status" onchange="load_form()" style="width: 215px!important">
							<option value="" <?php if($loanStatus == ''){echo 'selected';}?>>Select One</option>
							<option value="2" <?php if($loanStatus == 2){echo 'selected';}?>>Active</option>
							<option value="3" <?php if($loanStatus == 3){echo 'selected';}?>>Paid Off</option>
						</select>
					</div>	
				</form>
			</div>
		</div> -->
		<br>
		<!-- Close updated section -->
		<div class="row table-upper-section ajax_table_talimar table_All">
			<table id="empTable" class="table table-bordered table-striped table-condensed flip-content">
				<thead>
					<tr>
						<th>Address</th>
						<th>Loan Status</th>
						<th>FCI Account #</th>
						<th>TaliMar Account #</th>
						<th>TaliMar</th>
					    <th>Servicer</th>
						<th>Difference</th>	
					</tr>
				</thead>
				<tbody>
					<?php $num = 0; if($result){ $num = count($result); $current_balance_total = 0; $originalBalance_total = 0; $Difference_total = 0; foreach ($result as $key => $value) { 
						$current_balance_total = $current_balance_total + $value['current_balance'];
						$originalBalance_total = $originalBalance_total + $value['originalBalance'];
						$Difference_total = $Difference_total + $value['Difference'];
						$url = base_url().'load_data/'.$record->id;
					?>
					<tr>
						<td><a href="<?php echo $url;?>" style="font-weight: normal;"><?php echo $value['address'];?></a></td>
						<td><?php echo $value['loan_status'];?></td>
						<td><?php echo $value['fci'];?></td>
						<td><?php echo $value['talimar_loan'];?></td>
						<td><?php echo '$'.number_format($value['current_balance'],2);?></td>
					    <td><?php echo '$'.number_format($value['originalBalance'],2);?></td>
						<td><?php echo '$'.number_format($value['Difference'],2);?></td>	
					</tr>
					<?php } } ?>
				</tbody>
				<tfoot>
					<tr><th>Total: <?php echo $num;  ?> </th><th></th><th></th><th></th><th><?php echo '$'.number_format($current_balance_total,2);  ?></th><th><?php echo '$'.number_format($originalBalance_total,2);  ?></th><th><?php echo '$'.number_format($Difference_total,2);  ?></th></tr>
				</tfoot>

				</table>
		</div>
			
	</div>
	<!-- END CONTENT -->
</div>

<!--
		Update this method for add filter functionality in filter by date in lender data last update date is 24-03-2021
-->
<script>

function load_form(){

 $('#form_iad').submit();	

}
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#empTable').DataTable();


		// $('#empTable').DataTable().clear().destroy();
	 //   	$('#empTable').DataTable({
	 //      	'processing': true,
	 //      	'serverSide': true,
	 //      	// 'pageLength': 10,
	 //      	'lengthChange': false,
	 //      	'serverMethod': 'post',
	 //      	"columnDefs": [
		// 	    { "width": "30%", "targets": 0 }
		// 	  ],
	 //      	'ajax': {
	 //          'url':'<?php echo base_url(); ?>reports/ajaxPaymentCompare1',
	 //      	},
	 //      	'columns': [
	 //         	{ data: 'address'},
		//         { data: 'loan_status' },
		//         { data: 'fci' },
		//         { data: 'talimar_loan' },
		//         { data: 'current_balance'  },				        
		//         { data: 'originalBalance' },
		//         { data: 'Difference' }
	 //      	],
	 //      	'footerCallback': function ( row, data, start, end, display )  {
  //           	var api = this.api(), data;

  //           	// converting to interger to find total

  //           	var intVal = function ( i ) {
  //               return typeof i === 'string' ?
  //                   i.replace(/[\$,]/g, '')*1 :
  //                   typeof i === 'number' ?
  //                       i : 0;
  //           	};

  //           	var CurrentTotal = api
  //               .column( 4 )
  //               .data()
  //               .reduce( function (a, b) {
  //                   return intVal(a) + intVal(b);
  //               }, 0 );

  //               var OriginalTotal1 = api
  //               .column( 5 )
  //               .data()
  //               .reduce( function (a, b) {
  //                   return intVal(a) + intVal(b);
  //               }, 0 );

  //               var defferanceTotal = api
  //               .column( 6 )
  //               .data()
  //               .reduce( function (a, b) {
  //                   return intVal(a) + intVal(b);
  //               }, 0 );

  //               $( api.column( 0 ).footer() ).html('Total');
	 //            $( api.column( 4 ).footer() ).html('$'+ CurrentTotal.toLocaleString());
	 //            $( api.column( 5 ).footer() ).html('$'+ OriginalTotal1.toLocaleString());
	 //            $( api.column( 6 ).footer() ).html('$'+defferanceTotal.toLocaleString());
  //       	}
	 //   	});
	});
</script>

	


