<?php

$no_yes_option 	= $this->config->item('no_yes_option');
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Lender Contact List </h1>	
					
				</div>
				<div class="top_download">
					<a href="<?php echo base_url();?>download_lender_contact_list">
						<button  class="btn blue">PDF</button>
					</a>
				</div>
		</div>
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						<th>Lender Name</th>
						<th>Contact 1 Name</th>
						<th>Phone Number</th>
						<th>Email</th>
						<th>Contact 2 Name</th>
						<th>Phone Number</th>
						<th>Email</th>
							
						
					</tr>
				</thead>
				<tbody>
				<?php 	
				
				   if(isset($lender_contact_list))
				   {
			     	foreach($lender_contact_list as $row){ ?>
					
					<tr>
						<td><?php echo $row['lender_name'];?></td>
						<td><?php echo $row['contact_1_name'];?></td>
						<td><?php echo $row['phone_number_1'];?></td>
						<td><?php echo $row['email_1'];?></td>
						<td><?php echo $row['contact_2_name'];?></td>
						<td><?php echo $row['phone_number_2'];?></td>
						<td><?php echo $row['email_2'];?></td>
						
					
					
					</tr>
				   <?php }} ?>
				</tbody>
				
			</table>
			</div>
			
	</div>
	<!-- END CONTENT -->
</div>

<script>


$(document).ready(function() {
	
    $('#table').DataTable();
})



</script>