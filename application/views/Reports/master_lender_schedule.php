<?php
error_reporting(0);
$payment_option_distribution = $this->config->item('payment_option_distribution');
// echo "<pre>";
// print_r($IRA_investor_data);
// echo "</pre>";
?>
<style>
table#table th {
    text-align: center;
    font-size: 12px;
}
</style>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Self Directed IRA Lenders </h1>	
					
				</div>
				<div class="top_download">
					<a href="<?php echo base_url();?>self_directed_IRA_lenders_download">
					<button  class="btn blue">PDF</button>
					</a>
				</div>
		</div>
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						<th>Lender Name</th>
						<th>Custodian</th>
						<th>Tax ID</th>
						<th>Payment Type</th>
						<th>Mail:Attention</th>
						<th>Address</th>
						<th>Unit</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
						
					</tr>
				</thead>
				<tbody>
				<?php
				if(isset($IRA_investor_data))
				{
					foreach($IRA_investor_data as $row)
					{
						?>
						
						<tr>
							<td><a href="<?php echo base_url().'investor_data/'.$row['lender_id'];?>"><?php echo $row['lender_name']; ?></a></td>
							<td><?php echo $row['custodian']; ?></td>
							<td><?php echo $row['tax_id']; ?></td>
							<td><?php echo $payment_option_distribution[$row['ach_deposit']]; ?></td>
							<td><?php echo $row['mail_attention']; ?></td>
							<td><?php echo $row['address']; ?></td>
							<td><?php echo $row['unit']; ?></td>
							<td><?php echo $row['city']; ?></td>
							<td><?php echo $row['state']; ?></td>
							<td><?php echo $row['zip']; ?></td>
						</tr>
						
						<?php
					}
				}
				?>
				</tbody>
			</table>
			
	</div>
	</div>
	<!-- END CONTENT -->
</div>
<script>
$(document).ready(function() {
    $('#table').DataTable();
} );
</script>