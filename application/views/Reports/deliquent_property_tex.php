<?php
error_reporting(0);


$yes_no_option2 	= $this->config->item('yes_no_option2');
$loan_type_option 	= $this->config->item('loan_type_option');
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Delinquent Property Taxes</h1>	
					
				</div>
				<div class="top_download">
				<a href="<?php echo base_url();?>Deliquent_property_taxes_download">
					<button  class="btn blue">PDF</button>
				</a>
				</div>
		</div>
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Loan #</th>
					<th>Borrower Name</th>
					<th>Contact Name</th>
					<th>Contact Phone</th>
					<th>Property Street</th>
					<th>Property<br> Unit</th>
					<th>Property<br> City</th>
					<th>Property<br> State</th>
					<th>Property<br> Zip</th>
					<th>Delinquent<br> Taxes</th>
				</tr>
				</thead>
				<tbody>
				<?php 
				$count = 0;
				foreach($deliquent_property_tex as $row){ 
					if($row['loan_status'] == '2'){ 
					$count++;	?>
					<tr>
						<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['loan'];?></a></td>
						<td><?php echo $row['borrower'];?></td>
						<td><a href="<?php echo base_url()?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['contact_name'];?></a><br><a href="<?php echo base_url()?>viewcontact/<?php echo $row['contact_id1'];?>"><?php echo $row['contact_name1'];?></a></td>
						<td><?php echo $row['contact_phone'];?><br><?php echo $row['contact_phone1'];?></td>
						<td><?php echo $row['fetch_street'];?></td>
						<td><?php echo $row['fetch_unit'];?></td>
						<td><?php echo $row['fetch_city'];?></td>
						<td><?php echo $row['fetch_state'];?></td>
						<td><?php echo $row['fetch_zip'];?></td>
						<td>$<?php echo number_format($row['if_so']);?></td>
					</tr>
				
				<?php  }  } ?>
				
				</tbody>				
				<tfoot>
					<tr>
						<th>Total: <?php echo $count;?></th>
						<th colspan="9"></th>
					</tr>
				</tfoot>				
				
			</table>
		</div>
	</div>
	
	
	
	
</div>
<script>
$(document).ready(function() {
    $('#table').DataTable();
   
} );
</script>