<?php $account_status_option = $this->config->item('account_status_option'); ?>
<div class="page-content-wrapper">
	<style type="text/css">
		input.btn.blue.aa {
		  
		    margin-top: 14px;
		    border-radius: 0px;
		   
		}
		.talimar_no_dropdowns div {

		    padding: 0px 2px;
		    width: 24%!important;
		    font-size: 14px!important;
		}

		.talimar_no_dropdowns {
		    padding-left: 0px!important;
		}
		#table_wrapper>.row>.col-md-6.col-sm-12 {
		    
		    padding-left: 0px;
		}
		.form-control[disabled] {
		    cursor: not-allowed;
		    background-color: #dddddd;
		}
	</style>
	<div class="page-content responsive">
		<!--
				Update this method for add filter functionality in filter by date in lender data last update date is 24-03-2021
		-->
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>&nbsp;Late Payment Schedule</h1>
			
			</div>

			<div class="top_download">
					
			    <form method="POST" action="<?php echo base_url();?>reports/outstadingServicerDeposit">
			    	<input type="hidden" name="reportDisplay" value="pdf">
					<button  class="btn red aa" type="submit">PDF</button>
				</form>
				<form method="POST" action="<?php echo base_url();?>reports/outstadingServicerDeposit">
					<input type="hidden" name="reportDisplay" value="excel">
					<button  class="btn green aa" type="submit" style="margin-right: 0px;">Excel</button> 
				</form>
				

			</div>			
		</div>
		
		<br>
		<!-- Close updated section -->
		<div class="row">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content">
				<thead>
					<tr>
						<th>Borrower Name</th>
						<th>Loan #</th>
						<th>Street Address</th>
						<th>Unit #</th>
					    <th>City</th>
						<th>State</th>
						<th>Zip</th>
						<th>Loan Amount</th>
						<th>Payment Amount</th>			
						<th>Next Due Date</th>
						<th>Days Late</th>			
					</tr>
				</thead>
				<tbody>

				<?php				
				$total=0;
				$loan_amount 	= 0;
				$payment_amount 	= 0;

				if(isset($Newlenderaccounts) && is_array($Newlenderaccounts)){ 

					foreach($Newlenderaccounts as $row){ 

						if(!empty($row['nextDueDate']) && $row['nextDueDate'] != '0000-00-00'){
							$ldate_created = date('m-d-Y', strtotime($row['nextDueDate']));
						}else{
							$ldate_created = '';
						}
					?>

					<tr>
						<td><a href="/Pranav/Talimar/borrower_view/<?php echo $row['b_id'];  ?>"><?php echo $row['b_name'];?></a></td>
						<td><?php echo $row['talimar_loan'];  ?></td>
						<td><a href="/Pranav/Talimar/load_data/<?php echo $row['loan_id'];  ?>"><?php echo $row['property_address'];?></a></td>
						<td><?php echo $row['unit'];?></td>
						<td><?php echo $row['city'];?></td>
						<td><?php echo $row['state'];?></td>
						<td><?php echo $row['zip'];?></td>
						<td>$<?php echo number_format((double)$row['loan_amount'], 2);?></td>
						<td>$<?php echo number_format((double)$row['totalPayment'], 2);?></td>
						<td><?php echo $ldate_created;?></td>
						<td><?php echo $row['daysLate'];?></td>
												
					</tr>
					<?php 

					$total++;
					$loan_amount 	= (double)$loan_amount + (double)$row['loan_amount'];
					$payment_amount = (double)$payment_amount + (double)$row['totalPaymentFCI'];
					} 
				}else{ ?>

					<tr>
						<td colspan="11">No data found!</td>
					</tr>

				<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<th>Total: <?php echo $total; ?></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th><?php echo '$'.number_format($loan_amount, 2);?></th>
						<th><?php echo '$'.number_format($payment_amount, 2);?></th>
						<th></th>
						<th ></th>
					</tr>
				</tfoot>
			</table>
		</div>
			
	</div>
	<!-- END CONTENT -->
</div>

<!--
		Update this method for add filter functionality in filter by date in lender data last update date is 24-03-2021
-->
<script>
$(document).ready(function() {
    $('#table').DataTable();
    
} );
disable_dates('',0);
function loan_funded_year_form()
{
	$('#loan_funded_year_form').submit();
}

function disable_dates(that,no){

if(no > 0)
{
	
	$('#start').val('');
	$('#end').val('');
}
	
	var selectbox=$('#dates').val();

	if(selectbox=='3'){
     $(".datepicker").prop('disabled',false);
	}else{
		
	 $(".datepicker").prop('disabled',true);
	 $(".datepicker").datepicker({ dateFormat: "yy-mm-dd" }).val();
	var selectbox='';
	}
}

function load_form(){

 $('#form_iad').submit();	

}


</script>


	


