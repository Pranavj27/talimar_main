<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
			<div class="page-title">
				<h1> &nbsp; Investor Access Report</h1>
			</div>
		</div>
		<div class="row">			
			<div class="col-md-2">
				<label>Lender Portal:</label>
				<select class="form-control" name="lender_portal" id="lender_portal">
					<option value="">Select All</option>
					<?php foreach($lender_portal_access as $key => $row){ ?>
						<option value="<?php echo $key;?>"><?php echo $row;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-2">
				<label>TD Details:</label>
				<select class="form-control" name="td_details" id="td_details">
					<option value="">Select All</option>
					<?php foreach($td_details_option as $key => $row){ ?>
						<option value="<?php echo $key;?>"><?php echo $row;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-2">
				<label>Mortgage Fund:</label>
				<select class="form-control" name="martage_found" id="martage_found">
					<option value="">Select All</option>
					<?php foreach($martage_found_option as $key => $row){ ?>
						<option value="<?php echo $key;?>"><?php echo $row;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-2" style="display: none;">
				<label>Trust Deed Investor:</label>
				<select class="form-control" name="td_invester" id="td_invester">
					<option value="">Select All</option>
					<?php foreach($td_invester_option as $key => $row){ ?>
						<option value="<?php echo $key;?>"><?php echo $row;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-2">
				<button class="btn btn-primary" onclick="report_filter_data()" style="margin-top: 20px;">Filter</button>
			</div>
		</div>
		<br>
		<br>
		<div class="rc_class">
			<div class="table-upper-section ajax_table_talimar table_All">
				<table id="tbl_investor_access_report" class="table-responsive table table-bordered table-striped table-condensed flip-content dataTable no-footer">
				<thead>
					<tr>
						<th>Contact Name</th>
						<th>TD Investor</th>
						<th>Lender Portal</th>
						<th>TD Details</th>
						<th>Mortgage Fund</th>
						<th># of Lender Account</th>
						<th>Active TD's (#)</th>
						<th>Active TDs ($)</th>
						<th>Active MF</th>
					</tr>  
				</thead>
				<tbody></tbody>
			</table>
<script type="text/javascript">
	$(document).ready(function(){
		init_datatable_for_contact();
	});
	function init_datatable_for_contact(){
		var totalRecord=0;	
		var dispatching_list_all = '#tbl_investor_access_report';
		$(dispatching_list_all).DataTable().clear().destroy();  
		if($( dispatching_list_all ).length){		
			$dis_list_all = $( dispatching_list_all ).DataTable({
				processing: true,
				serverSide: true,
				pageLength: 10,
				bFilter: true,
			    ajax: {
					dataSrc:"data",
					data:function(data){
						var lender_portal 					= $('select[name="lender_portal"]').val();
						var td_details	 					= $('select[name="td_details"]').val();
						var martage_found 					= $('select[name="martage_found"]').val();
						var td_invester 					= $('select[name="td_invester"]').val();
						var search_text         			= $('#tbl_investor_access_report input[type="search"]').val();
						data.lender_portal 					= lender_portal;
						data.td_details 					= td_details;
						data.martage_found 					= martage_found;
						data.td_invester 					= td_invester;
						data.search_text 					= search_text;
						data.calling_type 					= 'table';
					},
					url: '/Reports/ajax_investor_access_report',
					dataFilter: function(data){
						var json = JSON.parse( data );	
					    json.recordsTotal = json.totalNumRows;
					    json.recordsFiltered = json.totalNumRows;
					    totalRecord=json.totalNumRows;					    					   
					    json.data = json.data;
					    return JSON.stringify( json );
					}
			    },
			    columns: [				        
			        { data: 'contact_name' },
			        { data: 'td_investor' },
					{ data: 'lender_portal' },
					{ data: 'td_details' },
					{ data: 'mortgage_fund' },
					{ data: 'lender_account' },
					{ data: 'active_td_no' },
					{ data: 'active_td_amount' },
					{ data: 'active_mf' }
			    ],
			    "aoColumnDefs": [
			        { "bSortable": false, "aTargets": [8] }, //, 6, 7
			        { "bSearchable": false, "aTargets": [0, 1, 2, 3, 4, 6, 7,8 ] }
			    ],
			    order: [[ 0, "ASC" ]],
			    "language": { loadingRecords: '&nbsp;&nbsp;', processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
			    drawCallback: function( settings ) {	
			    	//$('.lp_spin_load').html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:12px"></i>');
				   //	total_datatable_for_contact(totalRecord);		    		    	
			    },
			    initComplete: function() {
			        $('#tbl_investor_access_report input').unbind();
			        $('#tbl_investor_access_report input').bind('blur change', function(e) {
			            $dis_list_all.search(this.value).draw();
			        });
			    }
			});
		}
	}
	function total_datatable_for_contact(totalRecords){		
		var loan_status 					= $('select[name="loan_status"]').val();
		var GoogleRequest	 				= $('select[name="GoogleRequest"]').val();
		var AdvertisingImageComplete 		= $('select[name="AdvertisingImageComplete"]').val();
		var LoanDescriptionComplete 		= $('select[name="LoanDescriptionComplete"]').val();
		var loan_closing_posted	 			= $('select[name="loan_closing_posted"]').val();
		var rsm 							= $('select[name="rsm"]').val();
		var signed_posted 					= $('select[name="signed_posted"]').val();
		var adv_img	 						= $('select[name="adv_img"]').val();
		var search_text         			= $('#social_media_tbl input[type="search"]').val();
		var data = {		
			'loan_status' 					: loan_status,
			'GoogleRequest' 				: GoogleRequest,
			'AdvertisingImageComplete' 		: AdvertisingImageComplete,
			'LoanDescriptionComplete' 		: LoanDescriptionComplete,
			'loan_closing_posted' 			: loan_closing_posted,
			'rsm' 							: rsm,
			'signed_posted' 				: signed_posted,
			'adv_img' 						: adv_img,
			'search_text' 					: search_text,
			'calling_type' 					: 'total_table',
			'start'                         : '',
			'length'                        : ''
		};
		req=$.ajax({
				type: "GET",
				dataType: 'json',
				url: "/Reports/ajax_social_media_reports",
				data:data,
				success: function(respones){
					$(".total_records").html(respones.totalNumRows);
					$('.total_loan_amount').html(respones.totalAmount);
					$('.total_loan_avarage').html(respones.totalAvarage);
				}
		});
	}
	function report_filter_data()
	{
		init_datatable_for_contact();
	}
	function form_pdf_load(){
		var loan_status 					= $('select[name="loan_status"]').val();
		var GoogleRequest	 				= $('select[name="GoogleRequest"]').val();
		var AdvertisingImageComplete 		= $('select[name="AdvertisingImageComplete"]').val();
		var LoanDescriptionComplete 		= $('select[name="LoanDescriptionComplete"]').val();
		var loan_closing_posted	 			= $('select[name="loan_closing_posted"]').val();
		var rsm 							= $('select[name="rsm"]').val();
		var signed_posted 					= $('select[name="signed_posted"]').val();
		var adv_img	 						= $('select[name="adv_img"]').val();
		$('.pdf_dv_loan_schedule input[name="loan_status"]').val(loan_status);
		$('.pdf_dv_loan_schedule input[name="GoogleRequest"]').val(GoogleRequest);					
		$('.pdf_dv_loan_schedule input[name="AdvertisingImageComplete"]').val(AdvertisingImageComplete);
		$('.pdf_dv_loan_schedule input[name="LoanDescriptionComplete"]').val(LoanDescriptionComplete);
		$('.pdf_dv_loan_schedule input[name="loan_closing_posted"]').val(loan_closing_posted);	
		$('.pdf_dv_loan_schedule input[name="rsm"]').val(rsm);					
		$('.pdf_dv_loan_schedule input[name="signed_posted"]').val(signed_posted);
		$('.pdf_dv_loan_schedule input[name="adv_img"]').val(adv_img);
		$('#social_media_frm').submit();
	}
</script>
