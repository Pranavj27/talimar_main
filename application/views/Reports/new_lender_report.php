<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
// echo "<pre>";
// print_r($final_array);
// echo "</pre>";

$select_box_name = isset($select_box) ? '('.$loan_status_option[$select_box].')' : '' ;
$select_box_name1 = isset($loan_type) ? '('.$loan_type_option[$loan_type].')' : '' ;
$select_box_name2 = isset($property_type) ? '('.$property_type_option[$property_type].')' : '' ;
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; New Lenders Stats</h1>	
				
				</div>
				
				<div class="top_download">
				<form method="POST" action="<?php echo base_url();?>download_new_lender_report">
				<!--<input type="hidden" name="select_box" value="<?php echo isset($select_box) ? $select_box : '';?>">
				<input type="hidden" name="loan_type" value="<?php echo isset($loan_type) ? $loan_type : '';?>">
				<input type="hidden" name="property_type" value="<?php echo isset($property_type) ? $property_type : '';?>">
				<input type="hidden" name="start_date" value="<?php echo $this->input->post('start_date');?>">
				<input type="hidden" name="end_date" value="<?php echo $this->input->post('end_date');?>">-->
				
				<button  type="submit" class="btn blue">PDF</button>
				</form>
				</div>
				
		</div>
		
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th></th>
					<th style="width:170px;">MTD</th>
					<!--<th style="width:170px;">QTD</th>-->
					<th style="width:170px;">YTD</th>
				
				 </tr>
				</thead>
				<tbody class="scrollable">
				<?php 
			
									
				
					if(isset($final_array))
					{
						foreach($final_array as  $row)
						{
							
						 ?>
							<tr>
							  
								
								<td >New Lenders(Total)</td>
								<td><?php if($row['contact_total_in_mtd']!=''){ echo $row['contact_total_in_mtd'];}else{ echo "0";} ?></td>
								<!--<td><?php if($row['contact_total_in_qtd']!=''){ echo $row['contact_total_in_qtd'];}else{ echo "0";} ?></td>-->
								<td><?php if($row['contact_total_in_ytd']!=''){ echo $row['contact_total_in_ytd'];}else{ echo "0";} ?></td>
								
							</tr>
							<tr>
								<td>New Lenders(w/NDA)</td>
								<td><?php echo number_format($row['lender_nda_count']); ?></td>
								<!--<td><?php echo number_format($row['lender_nda_count_qtd_4']); ?></td>-->
								<td><?php echo number_format($row['lender_nda_count_ytd']); ?></td>
							
					       </tr>
					
					
							<tr>
								<td>New Lenders(w/Full Package)</td>
								<td><?php echo number_format($row['lender_full_pack_count']); ?></td>
								<!--<td><?php echo number_format($row['lender_full_pack_count_qtd']); ?></td>-->
								<td><?php echo number_format($row['lender_full_pack_count_ytd']); ?></td>
						
						
		
					        </tr>
							
							<tr>
							<td>
							&nbsp;
						
							</td>
							<td>
							&nbsp;
						
							</td>
							<td>
							&nbsp;
						
							</td>
							
							</tr>
							
							
							<tr>
								<td>New Lenders(#Funded)</td>
								<td><?php echo number_format($row['count_lender_funcded']); ?></td>
								<!--<td><?php echo number_format($row['count_lender_funcded_qtd']); ?></td>-->
								<td><?php echo number_format($row['count_lender_funcded_ytd']); ?></td>
						
							</tr>
					
							<tr>
								<td>New Lenders($Funded)</td>
								<td>$<?php echo number_format($row['sum_investment_mtd']); ?></td>
								<!--<td>$<?php echo number_format($row['highest_2018']); ?></td>-->
								<td>$<?php echo number_format($row['sum_investment_mtd']); ?></td>
								

							</tr>
				
			
					
			
					
			
							<?php
							
				
							
						}
					}
				?>
				
				</tbody>	
				
			</table>
		</div>
	</div>
	
	
	
	
</div>
<script>
// function form_peer_street()
// {
	// $("form#form_peer_street").submit();
// }

$(document).ready(function() {
   // $('#table').DataTable();
	$('#table').DataTable({
 "aaSorting": []
	});
		
	});

	
	
	// var table = $('#table').DataTable();
// table
    // .column( '10:visible')
    // .order( 'asc' )
    // .draw();
	
	// $('th#click_col').click();

</script>
<style>
/*.float-direction-left {
    float: right !important;
}*/

/*.talimar_no_dropdowns div {
    float: right;
}*/
.table,th,td{
width:160px;

}
</style>