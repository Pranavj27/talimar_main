<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');
$no_yes_option = $this->config->item('no_yes_option');

$foremailyes = array(''=>'No', 2 => 'Yes');
$fortextyes = array(''=>'No', 9 => 'Yes');

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Lender Setup Package</h1>	
					
				</div>
				
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>ReportData/LenderSetupPackage_pdf">
						<input type="hidden" name="from" value="<?php echo isset($from) ? $from : '';?>">
						<input type="hidden" name="to" value="<?php echo isset($to) ? $to : '';?>">
						<input type="hidden" name="setup_pack" value="<?php echo isset($setup_package) ? $setup_package : '';?>">
						<input type="hidden" name="active_account" value="<?php echo isset($active_account) ? $active_account : '';?>">
						<button type="submit" class="btn blue">PDF</button>
					</form>
				</div>
				
		</div>
		
		<div class="row">
			<form method="POST" action="<?php echo base_url();?>ReportData/LenderSetupPackage">
				<!-- <div class="col-md-2">
					<label>Setup Package Signed:</label>
					<select name="setup_pack" class="form-control" onchange="hidedatefields(this.value);">
                        <option value="">Select All</option>
                        <?php foreach($no_yes_option as $key => $row){ ?>
                        	<option value="<?php echo $key;?>" <?php if($setup_package == $key){echo 'selected';}?>><?php echo $row;?></option>
                        <?php } ?>
                    </select>
				</div> -->
				<div class="col-md-2">
					<label>Active Account:</label>
					<select name="active_account" class="form-control" >
                        <option value="">Select All</option>
                        <?php foreach($no_yes_option as $key => $row){ ?>
                        	<option value="<?php echo $key;?>" <?php if($active_account == $key){echo 'selected';}?>><?php echo $row;?></option>
                        <?php } ?>
                    </select>
				</div>
				<!-- <div class="col-md-2">
					<label>From:</label>
					<input type="text" name="from" class="form-control datepicker" placeholder="Date From" value="<?php echo isset($from) ? $from : '';?>" >
				</div>

				<div class="col-md-2">
					<label>To:</label>
					<input type="text" name="to" class="form-control datepicker" placeholder="Date To" value="<?php echo isset($to) ? $to : '';?>" >
				</div> -->

				<div class="col-md-2">
					<button type="submit" name="submit" class="btn btn-primary btn-sm" style="margin-top: 25px;">Filter</button>
				</div>
			</form>
		</div>
		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Last Name</th>
					<th>First Name</th>
					<th>Phone</th>
					<th>E-Mail</th>
					<th>Setup Package</th>
					<th>Setup Package<br>Date</th>
					<th>Trust Deed<br>E-Mail</th>
					<th>Trust Deed<br>Text</th>
					<th>Lender Accounts</th>					
				</tr>
				</thead>
				<tbody>
					<?php if(isset($LenderSetupPackageData) && is_array($LenderSetupPackageData)){ 
					
								foreach($LenderSetupPackageData as $row){ 

									if($row['date'] !=''){
										$setupdate = date('m-d-Y', strtotime($row['date']));
									}else{
										$setupdate = '';
									}

									?>
							<?php if($active_account == 1){ ?>

								<?php if($row['countlender'] > 0){ ?>
									<tr>
										<td><?php echo $row['contact_lastname'];?></td>
										<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['contact_firstname'];?></a></td>
										<td><?php echo $row['contact_phone'];?></td>
										<td>
											<a href="mailto:<?php echo $row['contact_email'];?>"><?php echo $row['contact_email'];?></a>
										</td>
										<td><?php echo $no_yes_option[$row['setup_package']];?></td>
										<td><?php echo $setupdate;?></td>
										<td><?php echo $foremailyes[$row['forYes2']];?></td>
										<td><?php echo $fortextyes[$row['forYes9']];?></td>
										<td><?php echo $row['countlender'];?></td>
										
									</tr>
								<?php } }elseif($active_account == 2){  if($row['countlender'] <= 0){ ?>

									<tr>
										<td><?php echo $row['contact_lastname'];?></td>
										<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['contact_firstname'];?></a></td>
										<td><?php echo $row['contact_phone'];?></td>
										<td>
											<a href="mailto:<?php echo $row['contact_email'];?>"><?php echo $row['contact_email'];?></a>
										</td>
										<td><?php echo $no_yes_option[$row['setup_package']];?></td>
										<td><?php echo $setupdate;?></td>
										<td><?php echo $foremailyes[$row['forYes2']];?></td>
										<td><?php echo $fortextyes[$row['forYes9']];?></td>
										<td><?php echo $row['countlender'];?></td>
										
									</tr>


								<?php } }else{ ?>

									<tr>
										<td><?php echo $row['contact_lastname'];?></td>
										<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['contact_firstname'];?></a></td>
										<td><?php echo $row['contact_phone'];?></td>
										<td>
											<a href="mailto:<?php echo $row['contact_email'];?>"><?php echo $row['contact_email'];?></a>
										</td>
										<td><?php echo $no_yes_option[$row['setup_package']];?></td>
										<td><?php echo $setupdate;?></td>
										<td><?php echo $foremailyes[$row['forYes2']];?></td>
										<td><?php echo $fortextyes[$row['forYes9']];?></td>
										<td><?php echo $row['countlender'];?></td>
										
									</tr>
								<?php }  ?>
					<?php } }else{ ?>
							<tr>
								<td colspan="8">No data found!</td>
							</tr>
					<?php } ?>
				</tbody>		
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {

	$(".datepicker" ).datepicker({ dateFormat: 'mm-dd-yy' });
	 
    $('#table').DataTable({

    	//"aaSorting":false,
    });
	
});

hidedatefields('<?php echo $setup_package;?>');
function hidedatefields(that){

	if(that == 2){

		$('input[name="from"]').attr('disabled',true);
		$('input[name="to"]').attr('disabled',true);
	}else{
		$('input[name="from"]').attr('disabled',false);
		$('input[name="to"]').attr('disabled',false);

	}


}

</script>




