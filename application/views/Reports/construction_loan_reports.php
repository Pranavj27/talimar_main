<?php
$construction_status_option = $this->config->item('construction_status_option');
// $construction_status_option = $this->config->item('loan_type_option');
error_reporting(0);
// echo "<pre>";

// print_r($fetch_construction_loan);

// echo "</pre>";

?>
<div class="page-content-wrapper">

	<div class="page-content responsive">

		<div class="page-head">

				<!-- BEGIN PAGE TITLE -->

				<div class="page-title">

					<h1> &nbsp; Construction Loan Status</h1>	

				</div>
                      <div class="top_download">
							<button type="button" onclick="click_to_submit()" class="btn blue">Filter</button>
                      		<button type="button" onclick="load_full_table()" class="btn red">PDF</button>
                      		

						   
						</div>
		</div>

		<div class="row page-body">
<form method="POST" action="<?php echo base_url()."construction_loan_reports"?>" id="sn">
	<div class="col-md-3">
	Loan Status: <br>
	<select class="form-control"  name="loan_status">
					<option value="">Select All</option>
					<option value="1" <?php if(isset($loan_status)){ if($loan_status == 1){ echo 'selected'; } } ?> >Pipeline</option>
					<option value="2" <?php if(isset($loan_status)){ if($loan_status == 2){ echo 'selected'; } } ?> >Active</option>
					<option value="3" <?php if(isset($loan_status)){ if($loan_status == 3){ echo 'selected'; } } ?> >Paid Off</option>
					<option value="4" <?php if(isset($loan_status)){ if($loan_status == 4){ echo 'selected'; } } ?> >Dead</option>
			</select>
							

</div>
<div class="col-md-3">
	Construction Loan Status: <br>
	<select class="form-control"  name="select_box">
								<option value="">Select All</option>
								<option value="1" <?php if(isset($construction_status)){ if($construction_status == 1){ echo 'selected'; } } ?>>Active</option>
								<option value="2" <?php if(isset($construction_status)){ if($construction_status == 2){ echo 'selected'; } } ?>>Complete</option>
			</select>
							

</div>	
<!-- <div class="col-md-3">
		 <br>
	
	</div>	 -->
</form>
		</div>

		<div class="rc_class">
			<form id="contruction_loan_form" method="POST" action="<?php echo  base_url().'construction_loan_pdf'; ?>">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
	<input type="hidden" name="construction_status" value="<?php echo $construction_status; ?>">
		<input type="hidden" name="loan_status" value="<?php echo $loan_status; ?>">
				<thead>

					<tr>
					
						<th>Borrower<br> Name</th>
						<th>Contact<br> Name</th>
						<th>Contact<br> Phone</th>

						<th>Street<br>Address</th>
						<th>City</th>
						
						<th>State</th>
						
						<th>Zip</th>
	            		<th id="click_col">Construction <br>Loan Status</th>
				<!-- 		<th>Loan #</th>
						 -->
						<!-- <th>Servicer #</th> -->
					<th>Reserve<br> Total</th>
					<th>Reserve Drawn</th>
					<th>Reserve<br > Remaining</th>

						<th>Loan<br> Amount</th>
						

						

					</tr>

				</thead>

				<tbody>
					<?php
					$total_loan_amount 		= 0;
					$count 					= 0;
					$total_draws_amount 	= 0;
					$total_draws_remaining 	= 0;
					$reserve_drawn 			= 0;
					if(isset($fetch_construction_loan))
					{
						foreach($fetch_construction_loan as $key => $row)
						{
							?>
							<tr>
								<td style="width: 25%;"><a href="<?php echo base_url().'borrower_view/'.$row['b_id']; ?>"><?php echo $row['borrower_name'];?></a></td>
								<td style="width: 8%;"><?php echo $row['borrower_contact'];?></td>
								<td style="width: 8%;"><?php echo $row['borrower_phone'];?></td>
								<td style="width: 15%;"><a href="<?php echo base_url().'load_data/'.$row['loan_id']; ?>"><?php echo $row['address'];?></a></td>

								<td><?php echo $row['city'];?></td>
								<td><?php echo $row['state'];?></td>
								<td><?php echo $row['zip'];?></td>
								<td><?php echo $construction_status_option[$row['construction_status']];?></td>

								<!-- <td><?php echo $row['talimar_loan'];?>
									<input type="hidden" name="loan_id[]" value="<?php echo $row['loan_id']; ?>">
								</td>
 -->
							<!-- 	<td><?php echo $row['fci'];?></td> -->
								<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'].'#test'; ?>">$<?php echo number_format($row['total_draws_amount']);?></a></td>
								<td>$<?php echo number_format($row['drawn']);?></td>
								<td>$<?php echo number_format($row['total_draws_remainingg']);?></td>
								<td>$<?php echo number_format($row['loan_amount']);?></td>
						
							</tr>
							<?php
							$total_loan_amount 		= $total_loan_amount + $row['loan_amount'];
							$total_draws_amount 	= $total_draws_amount + $row['total_draws_amount'];
							$total_draws_remaining 	= $total_draws_remaining + $row['total_draws_remainingg'];
							$reserve_drawn 			= $reserve_drawn 		  + $row['drawn'];
							$count = $key + 1;
						}
					}
					?>
				
				</tbody>
				
				<tfoot>
					<tr>
						<th>Total: <?php echo $count; ?></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					
						
						
						<th>$<?php echo number_format($total_draws_amount); ?></th>
							<th>$<?php echo number_format($reserve_drawn);?></th>
						<th>$<?php echo number_format($total_draws_remaining); ?></th>
						<th>$<?php echo number_format($total_loan_amount); ?></th>
					</tr>
				</tfoot>
			</table>
			</form>
	</div>

	</div>

	<!-- END CONTENT -->

</div>
<!--
<style>
	.DataTableDownload {color: #FFFFFF!important; background-color: #4B8DF8!important; border-radius:0;border-width:0px;padding:7px 14px;font-size:14px;float:right; };
	}
</style>
-->
<script>

$(document).ready(function() {
    $('#table').DataTable({
		"paging": true,
    	order: [[ 5, 'desc' ], [ 4, 'desc' ]],
	})
})
function load_full_table()
{
	$('form#contruction_loan_form').submit();
}
function click_to_submit()
{
	$('form#sn').submit();
}

</script>
<!--
$(document).ready(function() {
    $('#table').DataTable({

    	order: [[ 5, 'desc' ], [ 4, 'desc' ]],
    	dom: 'Bfrtip',
    	//columns: [ { width: 2000 }, null, null, null, null,null,null,null,null,null,null],
    	
        buttons: [
            {
            	text: 'Download',
            	className: 'DataTableDownload',
                extend: 'pdfHtml5',
                orientation: 'landscape',
               	pageSize: 'B4',   //'A4' LEGAL
                footer: true,
                title: '',
                exportOptions: {
     				// columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  				},
  				//columnDefs: [
				//	{ width: 10000, targets: 7 }
					

		       // ],
        		//fixedColumns: true,

                customize: function (doc) {
				doc.pageMargins = [2,42,2,40];
                	//doc.defaultStyle.fontSize = 12;
                	//doc.content[1].table.widths = 30;
                	
                	var now = new Date();
                	//var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear()+'-'+now.getUTCHours()+':'+now.getUTCMinutes();
					var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear()+'-'+now.toLocaleString('en-US', { hour: 'numeric', hour12: true });
					
					//var logo = getBase64FromImageUrl('https://database.talimarfinancial.com/assets/admin/layout4/img/logo-light.png');
					var logo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKoAAAAnCAYAAACfWNOEAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6N0M5RTZDRDdCNzk1MTFFNjhEREFBRTZFOTU1QTk1MUUiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6N0M5RTZDRDhCNzk1MTFFNjhEREFBRTZFOTU1QTk1MUUiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo3QzlFNkNENUI3OTUxMUU2OEREQUFFNkU5NTVBOTUxRSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo3QzlFNkNENkI3OTUxMUU2OEREQUFFNkU5NTVBOTUxRSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pt9/ZLsAABZoSURBVHja7F0JmFTVlT6vtt53lm4aWaQB2WRHRRTRuKAgI0bRjHuiIe4zGU2ijE5Go8ZRE7OocTSjM2hi4iRxHDNCjKIxihq3GInghqCIvVZ3V1dV1/byn1fndt96/WrpqnK+1o/7fYf3qt+959177n/Pdm8VRu3Sc2YS0QbQC6AOUBgUB7lAhlz5byFKljJQqdQxhRKgmDw/APQ26H9oX7GX1aCvgNYUkWcN6AHQ+aCPR/LgXd4yigXaqeflXw67rQd0BOh0oWKVe/YB1bFUgU4ELQU9VySeF4JOAFWM+NEb0HuJuNOTWsGiKcqx3d6SteUZoO2gL4Lmg+aAZoGmg6aC9gPdqDW6Uf7Gz2aAZoMWglaBrgdFhPaVoUWB6ZYi8rxCrnUjffBmLEKushryjppkf8RW+HaRC2PpblC9APg+kJdR7AYdAurK8I4PbPcfpqn3mKyGufsw6VimyJXlvQz0bIH8ztcAOm7EAzUeIcNTQtVz11CkcxcF3nyczIjlUW4FzQNFQZtAd4k7w58XMUYZqOdkASmXyjT3TuU34qPuK0PLeO3+dtEehZTr0vAeoabfZWnVeDxGJY3T8dFNPa/8CghOkJh8dWUn9lgB6iOg0Wz6/1rk7rDG/e0+TDqWJu1+gUxGvoWDsrGfKaAqPxWuaMy/h7wNk6jqwFXqCQfyfpAP9KTcd4OeUcHUSPTj+goRBa9AUK+WqeDSKJajPw+eo2wOvkei7Y5h8mGfPiiZFM6cfA80M89x/hvoI1CP8J1Q5HkolWxOIfEGy8jrEBxZ2jUeaCffqP2pas7xFNi2+RdmPKa06uNCA7VHGlC5o6+J77UlTx5u8f+Ok8ljQcfE94GdobY8eC6XrEiJ9JFX+v2gzcOM+FmjbgS9B7pGALZW+jWccpkEGpzuulz4FNtHfUgW+xkF8GiRIPtQSqY4y2UuOPSvhmatiof8z/oapz9cVVK5heJRM7TzJYp2DQmBTNcIA+oJMrhzC+ARk4n/oZjW1RL83Z0nSLn8twB9lfTxkWGCVDf7e0A32DRjPtr0HQnG6uVvY4s4D6zAOI3296L98y0vSyTfJPPAfbxVskMYg8EpuovigY6nPJUNP/WOnkKVcAXKWw6Fwk3VoSMNqCrVsi6HoC1beQO0U+63UDJHV0h5Rrvfmkf7/bR7dj8elvv9BRC5lovEnJ4jn3u1hVBVpHk4Pc19PqVPFjlJ7PI70POUzLNzDngOuwGJSOjceG/rtznPWj71MKqatwY+7MQRCdRplEyE7xYTu74IPBOav1VoKdPuy/Nor0yzyrBs0J59dxh8vi8B6x/ls9qNqrUFa4WUS0Ctcn9VEfgpnFU7PPsL6GbxW68w49FyDrQ8NY1Us/g08o2ZavmzIwmot4qmOkE+f71IPi8VQZvaeRh5tFfqQQFgOw3u3jVLFJ+tfE3M8nna3/amySrkW1hZLAatEPeCg70lRZSdU/lfTRnUWhq2P0Dxvg5Lu9YsOmXEANUl/t8DYrI/kSh95acsoHxAn2+Z4ACsb2j3N+SoTXdL+oY0n9fJvci33CLy3ya+OZdvfcqy65RrcDCOMMiMRckwDHKV140YoF4s141y/Ylcr6bPTxlvM9Vc3tK06miJ5jNpU5/mm5JNQxcDqOzS8K7Zt+XzfXL9uyK6FU6lVq7/Sckkv2AVYIXPakbDIwaoLJg/gQLy+W65Hiq+6+ehtMi1I00AqeTgzqBNd9q0KYn2U6WxwD4qzXmPtpBelPtLiyCDUJq//4uDLAqO+jOaU954iCdMCkXiudrK+bKidEFwIvtRub/ycwBSj0T3TkDdQcltZy41acZ7YRptSpSacmsusJ/fpOR5jaiDS3IhFb5B5LSQzqLkCb6lmqIqClBdmUAai5sUicWpzOemUDROLldWuH5Hri9LVFgjJuhBbSDVn3GgjhVN2UrOu27/oN1vcMhS8A7W+6CnHdp2aCmqQpL+ywWIP6ZkarBG5M7ZhW65/1KBcjhFUlO/lniEI/77xbV4Pi/QZSjuTCDt7QrSqoUT6OovziMPQBoKRzPxKpWAiSv9TiaCaYukSLhwzvCCzzhQx2mWwqns1PzBcptvvl606Xlp2nK6q117T77u3JXadYs2Fw9pPL9RoBwekbH9ByXzyG3aOz3ZTNJwSyITSI9aMpFOO2wK1VWU0DHzxtOvtrxNpaM8ZDo7DCp4OFpWrU9cC5doiQukzmVU3DOc/98ZBLVr1J6hzhWaaeeJu0beebto0y0Z2vKETxbT35RhQWQKZo4H3SFUQYOHmPvFZD8uqapDsmm/DMXuY7Nm5ZTk4ZTcDDimUI3qTqdRB0DaGaQjF0+kC4+bST3BKH3QFqAVs8dRU1MN9YVj6fj+M+hdWbmvieP+EiW/FrNNiz45Yl6dDj2JhDmc/heSQnMELbs3/XBz/H2R5OGg9L5Zawb+DOLb5F75oyvkPtuW8h5N8eQTnV+tBVNvavPA19cpubP0ZKZUlSHzYJpmtqyCvRwnVoGV1V3FBGqKFk5AvwZh3lmTXnjcDOoORqivP2rR+IYKWrj/KIoEHc3/Ulm5GzK8t4sGtxq/6VTBy+4FQNLXH3Pyh43hAtVlJEEXiSWs+zSySnkQQeBYWeqlmeNrKRpznKjJct09xDylTuy1NHi662bRMn9Wvim/NBZPWG2M9JF/fR5A/boAsidDHbV7ttrJFzYhK9YXvaKUDGf8OOHNLyDl8tV0CyFXoHpsfuVACQCkzaMq6OwVUykIsARCUWuCmTj6N4ysA/+5NRK3YWlef0cf+QP9A9paq8fAnqUzYP48zWsPmkTj6yso3B+zj82bg4uzQsgCemdv2OJZUeqhaDxBacA5AHzWIH2QwbplU+jKtXPJ63FZQLeViTbNl2SiNDHGy2M1klGvStWMEll/VcmwC/V4QbKcY6my1YE63ON+K2VsG1Sf2EL64cb5e8Iyh9aLNtPg2YkhaaTeQJgOmT6GjpnbTAEoK9tyzTYPHEifqWUZzs8XqNU2f0bTCEQlXrclcE5J6VqNVz5rAIc8Fa/IZeJ/JQEPszltXA1ddNJcmgctzJMnFp3zq+pAyBDT0IV2Rx3YTKsXT6BgoN/uSyvf8Pg04+JDHDcZrMXwTycmp7rMR9eeuoCWz2qi3t5wOt88nuIDYMxNdWV8sSbaQaeqr6CknF8LYmGuXDCBDpo+lrra+ijMWRLD+K7myz4HeW7lev72AM2aWEc3n7WEjpwzjnoxbk0Zv6+xXefoIpmULk74ESV3hKxtTF4I1eVeWr96Nq1cNIF6Abr+SExZF5WhudzuYsQx9zXlPjr3yGlUXeGzrJyDlq+0L1RedH5/iHGyEe9Q5wru1oLpnIHaSKkHRNaLU+3lzpsAYjtWHmsfl40bd4TBS8mO8GD5i4Nng15VgMVqncVmtv+jbpo7uYG+8oXpdO26BbRo6mjq3t1lmWDUUd8tWiYCPQl/a2ZNlgCYghHRpD0ha8Hg2VRJ6aiVvFYmc7PkZ5me4haoO60nFH3Ov7eXJjbW0A1nLqajAXzuO8Hvlvfz3vf12tBuwtDZBDb2YHGYoHA0YcmAtR5rdrQpk9wpn3Y6WNrxRByEZ42sEUOtAZo0upK+86WFtGb5FMgqRp0dfSb4bOAFD212dScAGgffdZDLVWvn0wHNtXTRypl0MLSXf1dXUu6G0aL17QsSqfN1Kp7V8Ri6+/qpG/JhC4j3M8hOlBQg99GD8bBsfWG8jxXMyQdPpktXzaL1cOeCkHEn2kIiz2k6h2OLC8B/Xjxp86kNdbjfPB/9yUVeC76naIuHr+fh/Uv4HR17e2hcfTmdcPAkCkFmnW2BGwHci2Vj4Afi8jBeDnZHGmZak+tEGODpoWD04jAkCOG/Fg7HdoD84VB0bjieWBgKRXwY1dvLDxxH8yaPgn+WSFm13JkxtWUUROc5uAp2h+eCTwtoE/jcBT494UAk5PK63z8Mgdjy2U3UyVoRPGbtV0e9EMBuuAIBf6gHbZ4CPYJ2veFgtCocjOyuqSlrW7V0f5o9oQ6m2ktRn4d2QdCoPx11R4NuR/2NoN+A3gV1gbpBPaBO8HkSY/vHMfUVrauWTqJzjppuaQXmUVdZQtESL+1qs/gtAi8/6Mdo93OLT1+kPhyJ7Wppru089YhpNB3WoMTjIY/bRW/v6aa+7tAE1D8NdCDqbwE9DYqFw9FJaFsC2b6xGGZy2YyxluVd3DKGZsDHDUNmu1t7X+/rCL4XMeiR+S2jaT2AeTTqsv+/F4ve63HT5DFV1A35fNjeR33+kAfv2Qb6I97xKmg0aAzk2xLqi/TGDdp9ItyjwxHcfoDF0dEWaMbzJai/F9cfgH6LPo0NR+JvtUxuiK4FSEt9bis4nDG+jmqrS+njrhC1dfR5wXMr2v0abV7GfTXzj5nmOwvAe+XCCZb8aitKqBXz2NoWqMc8HRLuj7PM7gVtsvqFAKY/bm6fNaUBGvgAWoP4Zr9RlVQOmWOhvwTLdgfm5kXIKgoZe91ed9xoOXZ9mlwMm22zZkpjdTdPGifxVWFTv+Ojbi9WEgY1yRIwtJIFVN0nZS3aUFVKZSUe2rr9E3rjgy4I2VA+z0CUuACmfsGU0dSDifCDGKi1MB9sgl5+t51efa89eThB3Ap+Txn6sGxmI6F/tAeaz+d1UX1lKb3+fjv9CW1M0eiZShRasKrMiwlsgqAqqKOn39I45sD7ffTKu230iu39KoBqrCunI2CGa1HvY7gNLLPR1WW0/SM/vbCj1XJd2Pe2pyn6IctpTTV06IxGaOIYtF0E9Vw0BoDgPr+wo41exXtnYAEeNrPJ8hNZW3EfVEBVX1VCFVhI/J7XP+i0gkrDNl6WbRT+5ryJ9bQQgGfwvQfL8ey2jzHOGHm9rpTxNCH4ZbeCZfuxP5h0KmEmx0LZsNV86i97LJfM63UPuBOMi5kA86FYcAC6pWg4iGbL8uQbeyDTMPl87pTxs4vD4z9sVqOFgXZoX5YbbxLtbO3F2Nvpk+4QVUGu/N6t21vJuOvOO5yBajKIqAoTtgADjMcHU0Cmx2WYnYFIAkJ1g7mvvRf+ZCIxAEA7H55gXmWlGKAtyrV8H/ZTWFvwI8VC3fMKLQfQ9XaGRFrsP/UmTZklAL5US31mkC1pZYiPyek0zlLoUX6m9ydfb1hC7oJJ7U/6lgOJx2qAv7zUywJkqRmUetzQRF0DE+xijcWgU3IbkFU5ZFXilkArAqAOlS13h12tGtTlCZb+mbb0WQLvcsP9MvzoJ88hL0wmhrwuIebPLgb3iReSLgvmXQ5rVYPFa4naHJol4XYqeOb6ZVwfsuPxmA5zzgBPjj/ZRvWeg1jOoKCZF/iC8k48/+aurqhx90/uzDSRNfCBpmNC2OkydSlxdMvvFx8pW/5scDIUQMzs+fNc2gypo2Yx1yJaytTRZA4vt59TH2zvGU4/df7kcC42X36U53iLIbMc5OHBx34AdhuUT8wTCmX8UmY3DZ6eSSlxLRNkYmm73e60wkpqnwQl4nHruaHq25enbSBx1DcTiQEeLrQxtEUxhK+q43LlvBCYv/UeUaHc3sXtc5x4ex+Yh1v1QVfNuKrx5NpPQ7T0AP8M9bjPRrZ+K5cLvBIiV6ud2z2opnORGY8lFhuUGXi43O6cAavkHtPwYLW34UFpWra2nkwCkLTUlyWnl3B4XmZF0vH4ZhN83F6vY6d4UEyUPLK3GFJ6wKrv86UAb7B3EEQ0qiZnqRyYuAXCjSqh8OC4jvBtlqTxgwBDxIN+cJ10QlMA4LaJJEh5HP8K+iX69WKCwaoAmwVI2tg4V8rf2HwQk/C+1Ufhwe/gvoIX7zKdBvo9+vlRun4OAJTbDS7WBkoe0OE8aYkooT7JQT6UUItMLWY78ASkmlz5mxQh3D/pAn9r7jIoDqWFNZkxMvnnnR6FzP7AwHfnuPi4fSxqbQLtZ+WwE4mf4W9R7kM6mbssBz09LQDdBmoHfQJqdaCAenkiFksxKwMTGY0qfoeDfmjV5xxrJGJ1aqCNTZjS5mzQDaBNGJBb8YujrXofp7hA94JKrWCD26fxmZUG5HcLwEjGeAnoQXzwWgDh54N9yAhSqXMs6HrQE5w3tZ4xD6YkSK2UlfRztmM/rax/Upbcjp+LBr4ctBl0AOhN0P/J5z2gk0G8qzSH36nG5ShXltngmK6WMQ+8T6+bUWZJ3iyzy0D/hQ8VCnxKHo58ZNFyPSPJdz7o30EllpZmPKSZt2yHUnyyB/+9XHwO3TSkTORggrVXbSOqejx4D2tWWUmWMFM7WybbibyRvgN/ncKDSaT6WiHZNUko38fiq2ksXYtqAOULH84+lZLH2n4P+in+fqbS6qz5XR5Pykq3g1RKneQv+RQYg4lzmx/awM7d3mX1195PvMNyQ0Rjaf27VOR/UBo37PuycfIY6vPvN3Va4wMPD/dbXDIHue5VGwsKiAj9LSunNKtu5m0ymyvnD3jzh7+ushF/P0lpXB7HgMxEs9uVltrzEHkkDHkng5jlYbkjmmbN5fSUmZtvnByUtWLZLA+dyCH8BsAqnbMmaeiK8gmIOI823zo0wRo0h4je4stmA0JLSH9SNM3gbtetssfOR+m2W9t4hvFXQ/mHPIHgwWT5zrrmSN25YrN8r+za8O9vjUedjkzyNNTkiuxs/auURXpiulhBO4XG743Z/Ug1Fw5yNe1zYYFV5oLkc1xp9dTdnHtkRysk+/MMNv6JolcUH5O1oyyUATykgnSoPDSwulRsIu/NBlS/CP2fKPmLIy7bnv/zpB3mVRrS0o5pVLij7yNmhYaCSG1X1sj9Kjnuxtuqi7JGlTJZluAYBEMj3jNlHOoU+w6ZAP6Rrtl230xNWCL9AlTyvF5A9o6AtzebJdJcCP3p0bKP/2gOuuLPjj7loPuQW2DIboAKZpUWTQXpGtmavko7aHMT6BckX7dJ8WdZu4rJz6UPujx0Xtm2UOMi/LEO1ExOPxIhER0Zw/jSps2XStMPVY6Qz09nPcanRbnk7GteI/vWeuE95vES9Azx0ywBpu+nHnDyaa+NsnVbkm3LOs0k8h55mAoow52LAbA6LxySAMp+KOVbcm7iy05KSIFuuCk//d3ZNGqDqPUrhpsj+5TLQeI7PybRqz/TUb40ILhIADnZto/PpvoV0J3qZFcWPpnKJbKY35OF3U3DO6z+jGj4MZT5LKs+n7GC50L8Z4fCWY2pcv5Dl1mXyOxH4oLkwmtYJZvQYqINOO8UpZFVOHDgw9V82Pg6ccxzlYlKR50sJrPEpr3vpeQB4lvE7SmknCtmkQ8iLxFTnuv5WPZz+TtLm8Q/T1cmSiDHAc2n9Tv+bgnq+HtTz1Hqcc+E+K0vycmnXL86pHL9oUKBWkLF/fGtsjz41WcI6GaK1nlRhJXIkefDAoJMv+PKv7n0BzHfr+XAs0o0n1PhrMLPJFAb7qJXgdQTsnCe0LRmpTy/Tf7+yTD4NthcqmzlAVkEmf6niHWiWe+j3P6PApbFOImDAjT0cHpU/p4VqJ2U//djnAqbr+H+wNgbWTIPh2tADebAr0kAdVaWes/KpHxNIttsZWeWsZ0uk72YHE76ZyhBCeyuE3flDAnOeFIrZPFfbndTciiv0+AvlGQrvOEzhdJ/wVAVPr55v8gsF6Cy//6W+L39NqCWCz/r1w7/JsAANKLBqtNF9j0AAAAASUVORK5CYII=';
					doc['header']=(function() {
							return {
								columns: [
									{
										alignment: 'left',
										image: logo,
										width: 100,
										margin: [5,5,5,5]
									},
									{
										alignment: 'left',
										italics: true,
										text: 'Construction Loan Schedule',
										fontSize: 18,
										margin: [20,8,0,0]
									}
								],
								// margin: [5,5,5,5]
							}
						});
					doc['footer']=(function(page, pages) {
							return {
								columns: [
									{
										alignment: 'left',
										text: ['page ', { text: page.toString() },	' of ',	{ text: pages.toString() }]
									},
									{
										alignment: 'right',
										text: ['', { text: jsDate.toString() }]
										
									}
								],
								margin: 20
							}
						});

					doc.styles['table'] = { 
				      width: '100%',
     				};
                	//doc.styles['td:nth-child(7)'] = { 
				    //   width: '1000px',
				    //   'max-width': '1000px'
     				//};

     				// doc.content[1].table.widths = [ '12%', '12%', '10%', '8%', '8%', '7%', '12%','20%', '7%', '4%', '6%'];
     				//doc.content[1].table.styles = [null,null,null,null,null,null,null,{margin:'20px 10px'},null,null,null];

                }
            }

        ]
    });

} );


</script>
-->