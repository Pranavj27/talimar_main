<?php

$no_yes_option 	= $this->config->item('no_yes_option');
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp;Sarah’s Prospects</h1>	
					
				</div>
				<div class="top_download">
					<a href="<?php echo base_url();?>download_sarah_pros_con_list">
						<button  class="btn blue">PDF</button>
					</a>
				</div>
		</div>
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						<th>Contact Name</th>
					    <th>Phone Number</th>
						<th>Email</th>
						<th>Joined Date</th>
						<th># Active Loans</th>
						<th>$ Active Loans</th>
						<th>Available Capital</th>
							
						
				 </tr>
				</thead>
				<tbody>
				<?php 	
				
				   if(isset($sarah_prospects))
				   {
			     	foreach($sarah_prospects as $row){
					
			
 $date=date('m-d-Y',strtotime($row['setup_package_date']));
  
  if ($date=='01-01-1970'){
	 $date1 ="";
  }
	  else{
		$date1=$date;
		  
	  }
  
  ?>
	
					<tr>
					
					
					
						<td><?php echo $row['contact_name'];?></td>
						<td><?php echo $row['contact_no'];?></td>
						<td><?php echo $row['email'];?></td>
						<td><?php echo $date1; ?></td>
						
						<td><?php echo $row['count_loan'];?></td>
						<td>$<?php echo number_format($row['total_loan_amount'],2);?></td>
						<td>$<?php echo number_format($row['commited_fund'],2);?></td>
					
						
					
					
					</tr>
				   <?php }} ?>
				</tbody>
				
			</table>
			</div>
			
	</div>
	<!-- END CONTENT -->
</div>

<script>


$(document).ready(function() {
	
    $('#table').DataTable();
})



</script>