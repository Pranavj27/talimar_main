<?php
// echo "<pre>";
// print_r($fetch_dead_loans);
// echo "</pre>";
error_reporting(0);
$dead_reason_option 	= $this->config->item('dead_reason_option');
$loan_type_option 		= $this->config->item('loan_type_option');
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Cancelled Loan Schedule  </h1>	
					
				</div>
				<div class="top_download">
				<a href="<?php echo base_url();?>reports/download_pdf_cancelled_loan">
				<button  class="btn blue">PDF</button>
				</a>
				</div>
		</div>
		<div class="rc_class">
			<table  id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Borrower Name</th>
					<th>Contact Name</th>
					<th>Phone</th>
					<!--<th>TaliMar #</th>
					<th>Loan Type</th>
					<th>LTV</th>
					<th>Closing Date</th>-->			
					<th>Property Address</th>					
					<th>Unit#</th>					
					<th>City</th>					
					<th>State</th>					
					<th>Zip</th>
					<th>Loan<br>Amount</th>
					<th>Interest Rate</th>
					<th>Cancelled Date</th>
					<th>Reason</th>					
					</tr>
				</thead>
				<tbody>
					<?php
					if(isset($fetch_dead_loans))
					{
						// echo '<pre>';
						// print_r($fetch_dead_loans);
						// echo '</pre>';
						foreach($fetch_dead_loans as $row)
						{
							$ltv = ($row['e_total_senior_plus_talimar_current']/$row['arv'])*100;
							?>
							<tr>
								<td><?php echo $row['borrower_name'];?></td>
								<td><?php echo $row['borrower_contact_name'];?></td>
								<td><?php echo $row['borrower_c_phone'];?></td>
								<!--<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['talimar_loan'];?></td>
								<td><?php echo $loan_type_option[$row['loan_type']];?></td>
								<td><?php echo number_format($ltv,2);?>%</td>
								<td><?php echo $row['loan_funding_date'] ? date('m-d-Y',strtotime($row['loan_funding_date'])) : '';?></td>-->
								
								<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_address'];?></a></td>
								<td><?php echo $row['unit'];?></td>
								<td><?php echo $row['city'];?></td>
								<td><?php echo $row['state'];?></td>
								<td><?php echo $row['zip'];?></td>
								<td>$<?php echo number_format($row['loan_amount']);?></td>
								<td><?php echo number_format($row['intrest_rate'],3);?>%</td>
							
							<?php 
							if($row['canceled_date'] == ''){

							$date='';
							}
							else{

 							$date = date('m-d-Y',strtotime($row['canceled_date']));
							}
								?>							

							<td><?php echo $date; ?></td>
								<td><?php echo $dead_reason_option[$row['dead_reason']];?></td>
							</tr>
							<?php
						}
					}
					?>
				</tbody>
				<tfoot>
				
				</tfoot>
				
			</table>
			</div>
			
	</div>
	<!-- END CONTENT -->
</div>
<script>
$(document).ready(function() {
    $('#table').DataTable();
} );
</script>