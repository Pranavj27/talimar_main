<?php
$contact_record_limit = $this->config->item('contact_record_limit');
$yes_no_option3 = $this->config->item('yes_no_option3');
$investor_type_option = $this->config->item('investor_type_option');

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>&nbsp;Lender Account Type</h1>
			
			</div>
			<div class="top_download">
					
			    <form method="POST" action="<?php echo base_url();?>ReportData/LenderAccountType">

			    	<input type="hidden" name="reportDisplay" value="pdf">
			    	<input type="hidden" name="AccountType" value="<?php echo isset($AccountType) ? $AccountType : '';?>">
			    	<input type="hidden" name="ActiveLoans" value="<?php echo isset($ActiveLoans) ? $ActiveLoans : '';?>">
			    	<input type="hidden" name="PaidOffLoans" value="<?php echo isset($PaidOffLoans) ? $PaidOffLoans : '';?>">
			    	
					<button  class="btn red aa" type="submit">PDF</button> 
				</form>
				
			</div>			
		</div>
		
		<div class="row">
			<form method="post" action="<?php echo base_url();?>ReportData/LenderAccountType">

				<div class="col-md-2">
					<label>Active Loans:</label>
					<select name="ActiveLoans" class="form-control">
						<option value="">Select All</option>
						<?php
							foreach($yes_no_option3 as $key_option => $row_option)
							{
								if($key_option)
								{
									$selected = (isset($ActiveLoans) && $ActiveLoans == $key_option) ? 'selected' : '';
									echo '<option value="'.$key_option.'" '.$selected.' >'.$row_option.'</option>';
								}
							}
						?>
					</select>
				</div>

				<div class="col-md-2">
					<label>Paid Off Loans:</label>
					<select name="PaidOffLoans" class="form-control">
						<option value="">Select All</option>
						<?php
							foreach($yes_no_option3 as $key_option => $row_option)
							{
								if($key_option)
								{
									$selected = (isset($PaidOffLoans) && $PaidOffLoans == $key_option) ? 'selected' : '';
									echo '<option value="'.$key_option.'" '.$selected.' >'.$row_option.'</option>';
								}
							}
						?>
					</select>
				</div>

				<div class="col-md-2">
					<label>Account Type:</label>
					<select name="AccountType" class="form-control">
						<option value="">Select All</option>
						<?php
							foreach($investor_type_option as $key_option => $row_option)
							{
								if($key_option)
								{
									$selected = (isset($AccountType) && $AccountType == $key_option) ? 'selected' : '';
									echo '<option value="'.$key_option.'" '.$selected.' >'.$row_option.'</option>';
								}
							}
						?>
					</select>
				</div>

				<div class="col-md-2">
					<button type="submit" name="submit" class="btn btn-primary btn-sm" style="margin-top:25px;">Filter</button>
				</div>
			</form>
		</div>
		<div class="row">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content">
				<thead>
					<tr>
						<th>Account Name</th>
					    <th># of<br>Contacts</th>
					    <th>Account Type</th>
					    <th>Custodian</th>
						<th>Active (#)</th>
						<th>Active ($)</th>
						<th>Paid Off (#)</th>
						<th>Paid Off ($)</th>
						<th>Total (#)</th>
						<th>Total ($)</th>			
					</tr>
				</thead>
				<tbody>

				<?php 

				$total=0;
				$hash_AmountPaid=0;
				$hash_countPaid=0;
				$hash_active_loan=0;
				$amount_active_loan=0;

				if(isset($LenderAccountType) && is_array($LenderAccountType)){ 
					foreach($LenderAccountType as $row){ ?>

					<tr>
						<td>
						<a href="<?php echo base_url();?>invester_view/<?php echo $row['lid'];?>"><?php echo $row['name'];?></a>
						</td>
						<td><?php echo $row['totalcontact'];?></td>
						<td><?php echo $investor_type_option[$row['type']];?></td>
						<td><?php echo $row['custodian'];?></td>
						<td><?php echo $row['countActive'];?></td>
						<td>$<?php echo number_format($row['AmountActive']);?></td>
						<td><?php echo $row['countPaid'];?></td>
						<td>$<?php echo number_format($row['AmountPaid']);?></td>
						<td><?php echo number_format($row['countActive'] + $row['countPaid']);?></td>
						<td>$<?php echo number_format($row['AmountActive'] + $row['AmountPaid']);?></td>
						
					</tr>
					<?php 

					$total++;
					$hash_active_loan += $row['countActive'];
					$amount_active_loan += $row['AmountActive'];
					$hash_countPaid += $row['countPaid'];
					$hash_AmountPaid += $row['AmountPaid'];

				} }else{ ?>

					<tr>
						<td colspan="10">No data found!</td>
					</tr>

				<?php } ?>
				</tbody>
				<tfoot>
						<tr>
							<th>Total: <?php echo $total;?></th>
							<th></th>
							<th></th>
							<th></th>
							<th><?php echo number_format($hash_active_loan);?></th>
							<th>$<?php echo number_format($amount_active_loan);?></th>
							<th><?php echo number_format($hash_countPaid);?></th>
							<th>$<?php echo number_format($hash_AmountPaid);?></th>
							<th><?php echo number_format($hash_active_loan + $hash_countPaid);?></th>
							<th>$<?php echo number_format($amount_active_loan + $hash_AmountPaid);?></th>
							
						</tr>
						<tr>
							<th>Average:</th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th>$<?php echo number_format($amount_active_loan/$total);?></th>
							<th></th>
							<th>$<?php echo number_format($hash_AmountPaid/$total);?></th>
							<th></th>
							<th>$<?php echo number_format(($amount_active_loan + $hash_AmountPaid)/$total);?></th>
							
						</tr>
				</tfoot>
			</table>
		</div>
			
	</div>
	<!-- END CONTENT -->
</div>


<script>

$(document).ready(function() {
  
	$('#table').DataTable();
});


</script>


	


