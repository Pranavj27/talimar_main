<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$yes_no_option3 = $this->config->item('yes_no_option3');

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Prospective Investor Schedule </h1>	
					
				</div>
				
				<div class="top_download">
				<form method="POST" action="<?php echo base_url();?>download_prospective_investors">
				
					<button  type="submit" class="btn blue">PDF</button>
				</form>
				</div>
				
		</div>
		
		
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Contact Name</th>
					<th>Setup Package Complete</th>
					<th>Joined Date</th>
					<th>Available Capital</th>
				</tr>
				</thead>
				<tbody class="scrollable">
				<?php
				
				foreach($prospective_investor as $row){
					
						$current_date = date('m-d-Y');
						$last_6_months = date('m-d-Y', strtotime('-6 Months' , strtotime($current_date)));
						
						$setup_package_date = date('m-d-Y', strtotime($row['setup_package_date']));
						
						if($last_6_months <= $setup_package_date){
				?>
					<tr>
						<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['c_name'];?></a></td>
						<td><?php echo $yes_no_option3[$row['setup_package']];?></td>
						<td><?php echo date('m-d-Y', strtotime($row['setup_package_date']));?></td>
						<td>$<?php echo number_format($row['committed_funds']);?></td>
						
					</tr>
				<?php } } ?>
				</tbody>	
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {
     $('#table').DataTable();
	
} );
</script>
