<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Daily Report <?php echo date('m-d-Y');?>  </h1>
				</div>
				<div class="top_download">
					<a href="<?php echo base_url();?>download_daily_report">
					<button  class="btn blue">PDF</button>
					</a>
				</div>
		</div>
		<div class="col-md-12">&nbsp;</div>
		<div class="col-md-12">
			<div class="page-title">
				<h3><strong>Pipeline:</strong></h3>
			</div>
		</div>
		<div class="rc_class">
			<table class="tables table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						<th>Borrower Name</th>
						<th>Property Address </th>
						<th>Loan Amount</th>
						<th>Status Date </th>
						
					</tr>
				</thead>
				<tbody>
					<?php foreach($daily_pipeline_report as $row){ ?>
						<tr>
							<td><?php echo $row['b_name'];?></td>
							<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['full_address'];?></a></td>
							<td>$<?php echo number_format($row['loan_amount']);?></td>
							<td><?php echo date('m-d-Y', strtotime($row['closing_date']));?></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>

		</div>


		<div class="col-md-12">&nbsp;</div>
		<div class="col-md-12">
			<div class="page-title">
				<h3><strong>Loan Assignments:</strong></h3>
			</div>
		</div>
		<div class="rc_class">
			<table class="tables table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						<th>Borrower Name</th>
						<th>Property Address </th>
						<th>Loan Amount</th>
						<th>Available</th>
						
					</tr>
				</thead>
				<tbody>
					<?php foreach($all_assigment_result as $row){ ?>
						<tr>
							<td><?php echo $row['b_name'];?></td>
							<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['property'];?></a></td>
							<td>$<?php echo number_format($row['loan_amount']);?></td>
							<td>$<?php echo number_format($row['loan_amount'] - $row['investment']);?></td>
							
						</tr>
					<?php } ?>
				</tbody>
			</table>

		</div>



		<div class="col-md-12">&nbsp;</div>
		<div class="col-md-12">
			<div class="page-title">
				<h3><strong>Portfolio Overview:</strong></h3>
			</div>
		</div>
		<div class="rc_class">
			<table class="tables table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						<th>Portfolio Balance</th>
						<th># of Loans </th>
						<th>Avg. Loan Amount</th>
						<th>Average LTV</th>
						<th>Average Rate</th>
						<th>Avg Term</th>
						
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>$<?php echo number_format($p_portfolio_bal);?></td>
						<td><?php echo $p_count;?></td>
						<td>$<?php echo number_format($p_portfolio_bal/$p_count);?></td>
						<td><?php echo number_format($ltv/$p_count);?>%</td>
						<td><?php echo number_format($p_intrest_rate/$p_count, 2);?>%</td>
						<td><?php echo number_format($p_term_month/$p_count);?> Months</td>
						
					</tr>
				</tbody>
			</table>

		</div>

		<div class="col-md-12">&nbsp;</div>
		<div class="col-md-12">
			<div class="page-title">
				<h3><strong>Loan Servicing:</strong></h3>
			</div>
		</div>
		<div class="rc_class">
			<table class="tables table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						<th># of Upcoming Payoffs</th>
						<th>$ of Upcoming Payoffs</th>
						<th>Avg. Upcoming Payoff</th>
						<!--<th>Monthly Servicing Income</th>-->
						<th># of Loan in Default</th>
						<th>$ of Loan in Default</th>
						
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?php echo $count;?></td>
						<td>$<?php echo number_format($amount);?></td>
						<td>$<?php echo number_format($amount/$count);?></td>
						<!--<td></td>-->
						<td><?php echo $total_count;?></td>
						<td>$<?php echo number_format($total_amount);?></td>
					</tr>
				</tbody>
			</table>

		</div>


		<div class="col-md-12">&nbsp;</div>
		<div class="col-md-12">
			<div class="page-title">
				<h3><strong>Investor Figures:</strong></h3>
			</div>
		</div>
		<div class="rc_class">
			<table class="tables table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						<th># of Active Lenders</th>
						<th>Avg. Investment $</th>
						<th>Available Capital</th>
						
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?php echo $total_counts;?></td>
						<td>$<?php echo number_format($total_amounts);?></td>
						<td></td>
					</tr>
				</tbody>
			</table>

		</div>
	</div>
</div>

<script>
$(document).ready(function() {
    $('.tables').DataTable({

    	"paging": false,
   		"ordering": false,
   		"searching": false,
   		"info": false,
    });
  
} );
</script>