<?php


error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');


						
$array_new_loan_status	= array(
							'' => 'Select All',
							6 => 'Term Sheet',
							1 => 'Pipeline',
							2 => 'Active',
							3 => 'Paid off',
							4 => 'Cancelled',
							5 => 'Brokered'
								);	




?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Contact Schedule By Borrower<?php //echo $select_box_name; ?></h1>	
					
				</div>
				
				<div class="top_download">
				<form method="POST" action="<?php echo base_url();?>download_borrower_schedule">
					<!--<input type="hidden" name="contact_id" value="<?php// echo $this->input->post('contact_id');?>">
					<input type="hidden" name="borrower_id" value="<?php //echo $this->input->post('borrower_id');?>">-->
					<input type="hidden" name="loan_status" value="<?php echo $this->input->post('loan_status');?>">
					<button  type="submit" class="btn blue">PDF</button>
				</form>
				</div>
				
		</div>
		
		<div class="row">
			
			
				<form id="select_borrower" method="POST" action="<?php echo base_url();?>reports/contact_schedule_by_borrower">
				
				<!--<div class="col-md-5">
					<select name="contact_id" id ="contact_id" onchange = "select_borrower_contact(this.value)" class="chosen">
						<option value="">Select Contact</option>
						<option value="">Select All</option>
						<?php 
							
							/*foreach($all_borrower_contact as $row)
							{

						?>
						
						<option value="<?php echo $row->contact_id;?>"><?php echo $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname; ?></option>
						<?php
						}
						?>
					</select>
				</div>
				<div class="col-md-5">
					<select name="borrower_id" id ="contact_id" onchange = "select_borrower_contact(this.value)" class="chosen">
						<option value="">Select Borrower</option>
					<option value="">Select All</option>
						<?php 
							foreach($all_borrower_name as $row)
							{
								
								
						?>
						<option value="<?php echo $row->id;?>"><?php echo $row->b_name; ?></option>
						<?php
						}*/
						?>
					</select>
				</div>-->
				<div class="col-md-2">
					<label>Loan Status:</label>
					<select class="form-control" name="loan_status" onchange="select_borrower_contact(this.value);">
						<?php foreach($array_new_loan_status as $key => $row){?>
							<option value="<?php echo $key; ?>" <?php if($loan_status_val == $key){echo 'selected';}?>><?php echo $row; ?></option>
						<?php } ?>
					</select>
			
				</div>
			</form>
		
		</div>
		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Contact Name</th>
					<th>Phone</th>
					<th>E-mail</th>
					<th>Borrower Name</th>
					<th># of<br>Active Loans</th>
					<th>$ of<br>Active Loans</th>
					<th># of<br>Paid Off<br>Loans</th>
					<th>$ of<br>Paid Off<br>Loans</th>
					
				</tr>
				</thead>
				<tbody>
				<?php foreach($schedule_by_borrower as $key => $row){ ?>
					<tr>
						<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['Contact_name'];?></a></td>
						<td><?php echo $row['contact_phone'];?></td>
						<td><a href="mailto:<?php echo  isset($row['contact_email']) ? $row['contact_email']: ''; ?>"><?php echo $row['contact_email'];?></a></td>
						<td><a href="<?php echo base_url();?>borrower_view/<?php echo $row['b_id'];?>"><?php echo $row['borrower_name']?></a></td>
						<td><?php echo $row['count_active'];?></td>
						<td>$<?php echo number_format($row['amount_active']);?></td>
						<td><?php echo $row['count_paidoff'];?></td>
						<td>$<?php echo number_format($row['amount_paidoff']);?></td>
						
					</tr>
				<?php } ?>
				</tbody>	
				
			</table>
		</div>
	</div>
	
</div>

<script>

$(document).ready(function() {
	 
    $('#table').DataTable({
		"aaSorting": [[ 5, "desc" ], [ 7, "desc" ]]
	});
	
});

function select_borrower_contact(){
	$('form#select_borrower').submit();
}

</script>
<style>
.talimar_no_dropdowns {
		float: left;		
}
.talimar_no_dropdowns div {
    float: left;
    padding: 0px;
}div#contact_id_chosen {
    width: 200px !important;
}
.chosen-container-single .chosen-single {
    padding: 4px 0px 26px 6px !important;
    border-radius: 4px !important;
    border: 1px solid #e1e1e1 !important;
}
.row {
    margin-right: 0px;
   
}
</style>



