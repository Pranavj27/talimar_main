<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');


/*echo '<pre>';
print_r($loanBoardingData);
echo '</pre>';*/

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Active vs. Outstanding Balance</h1>	
					
				</div>
				
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>reports/ActiveOutBalance_pdf">
						<input type="hidden" name="loan_status_val" value="<?php echo isset($loan_status_val) ? $loan_status_val : '';?>">
						<button type="submit" class="btn blue">PDF</button>
					</form>
				</div>
				
		</div>
		
		<div class="row">

			<form method="post" action="<?php echo base_url();?>reports/ActiveOutBalance">
				
				<div class="col-md-2">
					<label><b>Loan Status:</b></label>
					<select class="form-control" name="loan_status">
						<option value="">Select All</option>
						<?php 
						unset($loan_status_option[0]);
						unset($loan_status_option[4]);
						unset($loan_status_option[6]);
						unset($loan_status_option[7]);

						foreach($loan_status_option as $key => $row){ ?>
							<option value="<?php echo $key;?>" <?php if($loan_status_val == $key){echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>

				<div class="col-md-2">
					<button class="btn btn-primary btn-sm" type="submit" name="submit" style="margin-top: 25px;">Filter</button>
				</div>

			</form>

		</div>

		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Borrower Name</th>
					<th>Street Address</th>
					<th>Unit #</th>
					<th>City</th>
					<th>State</th>
					<th>Zip</th>
					<th>Original Balance</th>
					<th>Outstanding Balance</th>
					<th>$ Difference</th>
					<th>% Difference</th>					
				</tr>
				</thead>
				<tbody>
				
				<?php 
				$countrow = 0;
				$original_balance = 0;
				$outstanding_balance = 0;
	
				if(isset($ActiveOutBalance) && is_array($ActiveOutBalance)){ 
						foreach($ActiveOutBalance as $row){ 

						$countrow++; 

						$original_balance += $row->original_balance;
						$outstanding_balance += $row->outstanding_balance;
				?>
						
						<tr>
							<td><?php echo $Fetch_All_borrowername[$row->borrower];?></td>
							<td><a href="<?php echo base_url();?>load_data/<?php echo $row->loan_id;?>"><?php echo $row->property_address;?></a></td>
							<td><?php echo $row->unit;?></td>
							<td><?php echo $row->city;?></td>
							<td><?php echo $row->state;?></td>
							<td><?php echo $row->zip;?></td>
							<td>$<?php echo number_format($row->original_balance,2);?></td>
							<td>$<?php echo number_format($row->outstanding_balance,2);?></td>
							<td>$<?php echo number_format($row->original_balance - $row->outstanding_balance,2);?></td>
							<td><?php echo number_format($row->outstanding_balance / $row->original_balance*100,2);?>%</td>
						</tr>
	
					<?php  } }else{ ?>
				
					<tr>
						<td style="width:100%;" colspan="10">No data found!</td>
					</tr>
				<?php } ?>
					
				</tbody>	
				<tfoot>
					<tr>
						<th>Total: <?php echo $countrow;?></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th>$<?php echo number_format($original_balance,2);?></th>
						<th>$<?php echo number_format($outstanding_balance,2);?></th>
						<th>$<?php echo number_format($original_balance - $outstanding_balance,2);?></th>
						<th></th>
					</tr>
				</tfoot>	
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {
	 
    $('#table').DataTable();
	
});



</script>




