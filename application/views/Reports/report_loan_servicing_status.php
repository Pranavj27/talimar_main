<?php

error_reporting(0);
$yes_no_option = $this->config->item('yes_no_option');
$yes_no_option3 = $this->config->item('yes_no_option3');

 $new_filter_current_late = array(
										''=> 'Select All',
										3 => 'Yes',
										2 => 'No',
											
											);
// print_r($fci_service_loan);

// echo "</pre>";

?>

<div class="page-content-wrapper">

	<div class="page-content responsive">

		<div class="page-head">

				<!-- BEGIN PAGE TITLE -->

				<div class="page-title">

					<h1> &nbsp;  Loan Servicing Report</h1>	

					

				</div>

				<div class="top_download">
				
					
					<form action="<?php echo base_url();?>download_report_loan_servicing_status" method="POST">
						<input type="hidden" name="select_box" value="<?php echo $select_box ? $select_box :'';?>">
						<input type="hidden" name="dis_veri" value="<?php echo $dis_veri ? $dis_veri :'';?>">
						<input type="hidden" name="boarding_fee" value="<?php echo $boarding_fee ? $boarding_fee :'';?>">
						<input type="hidden" name="current_late" value="<?php echo $current_late ? $current_late :'';?>">
						<input type="hidden" name="l_b" value="<?php echo $l_b ? $l_b :'';?>">
						  <button  class="btn blue" onclick="submit_filter(this)" type="button">Filter</button>
						<button  class="btn red" type="submit">PDF</button>
					</form>
					
				</div>
				
				
							

		</div>
		
			<form id="select_borrower" method="POST" action="<?php echo base_url();?>report_loan_servicing_status">
			
				<div class="col-md-2">
				Loan Status :  &nbsp;
								
								<select name="select_box" id ="select_box" class="form-control"  onchange = "select_box_report(this.value)">
									<option value="" >Select All</option>
									<option value="6" <?php if(isset($select_box)){ if($select_box == 6){ echo 'selected'; } } ?>>Term Sheet</option>
									<option value="1" <?php if(isset($select_box)){ if($select_box == 1){ echo 'selected'; } } ?>>Pipeline</option>
									<option value="2" <?php if(isset($select_box)){ if($select_box == 2){ echo 'selected'; } } ?>>Active</option>
									<option value="3" <?php if(isset($select_box)){ if($select_box == 3){ echo 'selected'; } } ?>>Paid Off</option>
									<option value="4" <?php if(isset($select_box)){ if($select_box == 4){ echo 'selected'; } } ?>>Dead</option>
									<option value="5" <?php if(isset($select_box)){ if($select_box == 5){ echo 'selected'; } } ?>>Brokered</option>
									
								</select>
				</div>
		<div class="col-md-2">
				Boarding Fees Paid :  &nbsp;
								
						<select name="boarding_fee" id ="boarding_fee" class="form-control">
						<?php foreach($yes_no_option3 as $key=>$row){?>
							
                    <option value="<?php echo $key;?>" <?php if($boarding_fee == $key){ echo 'selected'; } ?>><?php echo $row;?></option>
										
							<?php 	}?>

									
								</select>
				</div>


				<div class="col-md-2">
				Disbursement Verified :  &nbsp;
								
								<select name="dis_veri" id ="dis_veri" class="form-control">
									<?php foreach($yes_no_option3 as $key=>$row){?>
							
                    <option value="<?php echo $key;?>" <?php if($dis_veri == $key){ echo 'selected'; } ?>><?php echo $row;?></option>
										
							<?php }	?>

									
								</select>
				</div>

						<div class="col-md-2">
				Current - Late :  &nbsp;
								
								<select name="current_late" id ="current_late" class="form-control">
									<?php foreach($new_filter_current_late as $key=>$row){?>
							
                    <option value="<?php echo $key;?>" <?php if($current_late == $key){ echo 'selected'; } ?>><?php echo $row;?></option>
										
							<?php }	?>

									
								</select>
				</div>

				<div class="col-md-2">
				 Loan Boarded :  &nbsp;
								
						<select name="l_b" id ="l_b" class="form-control">
						<?php foreach($new_filter_current_late as $key=>$row){?>
							
                    <option value="<?php echo $key;?>" <?php if($l_b == $key){ echo 'selected'; } ?>><?php echo $row;?></option>
										
							<?php 	}?>

									
								</select>
				</div>

				

			
		              
		</form>
		
		<div class="rc_class">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >

				<thead>

					<tr>
					
						<th>Borrower Name</th>
						<th>Contact Name</th>
						<!-- <th>Property Address</th> -->
						<th>Unit #</th>
						<th>Street Address</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
						<th>Loan Amount</th>
						<th>Outstanding Balance</th>
						<th>Maturity Date</th>
						<th>Servicer #</th>
						
						
					</tr>

				</thead>
				
				<tbody>
					<?php
						$count=0;
						$total_loan_amount=0;
						$total_outstanding_amount=0;
						if(isset($loan_result)){
	
							foreach($loan_result as $row)
							{
								?>

								<tr>
									<td><a href="<?php echo base_url().'borrower_view/'.$borrower_id[$row->borrower];?>"><?php echo $borrower_list[$row->borrower]; ?></a></td>
									<td><a href="<?php echo base_url().'viewcontact/'.$contact_id[$row->borrower];?>"><?php echo $b_contact_name[$row->borrower]; ?></a></td>
								    <td><?php echo $unit[$row->talimar_loan]; ?></td> 
								    <td><a href="<?php echo base_url().'load_data/'.$row->id;?>"><?php echo $property_list[$row->talimar_loan]; ?></a></td>
								    <td><?php echo $city[$row->talimar_loan]; ?></td> 
								    <td><?php echo $state[$row->talimar_loan]; ?></td> 
								    <td><?php echo $zip[$row->talimar_loan]; ?></td> 
							
									<td>$<?php echo number_format($row->loan_amount); ?></td>
									<td>$<?php echo number_format($row->current_balance); ?></td>
									<td><?php echo date('m-d-Y',strtotime($row->maturity_date)); ?></td>
									<td><?php echo $row->fci; ?></td>
								</tr>

								<?php
							$count++;
							$total_loan_amount +=$row->loan_amount;
							$total_outstanding_amount +=$row->current_balance;

							 }
						}
					?>
				</tbody>
			<tfoot>
					<tr>
						<th>Total: <?php echo $count; ?></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th><?php echo '$'.number_format($total_loan_amount);?></th>
						<th><?php echo '$'.number_format($total_outstanding_amount);?></th>
						<th></th>
						<th></th>
					</tr>
					
						<tr>
						<th>Average</th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th><?php echo '$'.number_format($total_loan_amount/$count);?></th>
						<th><?php echo '$'.number_format($total_outstanding_amount/$count);?></th>
						
						<th></th>
						<th></th>
					</tr>
					
					
						
					
				</tfoot>
			</table>

			

	</div>

	</div>

	<!-- END CONTENT -->

</div>

<script>
$(document).ready(function() {
    $('#table').DataTable();
});


function submit_filter(that){
$("#select_borrower").submit();

}
</script>