<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');

$loan_status_filter = array(
								'' => 'Select All',
								1  => 'Pipeline',
								2  => 'Active',
								3  => 'Paid Off',
							);

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Diligence Schedule for [<?php echo date('m-d-Y');?>]</h1>	
					
				</div>
				
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>pdf_diligence_schedule">
						<button type="submit" class="btn blue">PDF</button>
					</form>
				</div>
				
		</div>
		
		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Borrower Name</th>
					<th>Address</th>
					<th>Closing Date</th>
					<th>Outstanding Items</th>
										
				</tr>
				</thead>
						<tbody>
					<?php
					if(isset($array_diligence_schedule) && is_array($array_diligence_schedule)){
					 foreach($array_diligence_schedule as $row){

							$closing_date = $row['closing_date'] ? date('m-d-Y', strtotime($row['closing_date'])) : '';

							$fetch_underwriting = $this->User_model->query("SELECT `items` FROM `underwriting` WHERE `required`= '1' AND (`received`='0' OR `received` = '') AND `talimar_loan`= '".$row['talimar_loan']."'");
							//if($fetch_underwriting->num_rows() > 0){

								$fetch_underwriting = $fetch_underwriting->result();
								
						?>
						<tr>
							<td><?php echo $row['borrower_name'];?></td>
							<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['address'];?></a></td>
							<td><?php echo $closing_date;?></td>
							<td><?php foreach($fetch_underwriting as $value)
							{ 
                               if(!empty($value->items)){    
								echo '- '.$value->items.'<br>'; 
							}else{

							echo ''; 	
							}
							}?></td>
						</tr>
					<?php  } }  ?>
				</tbody>		
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {
	 
    $('#table').DataTable();
	
});



</script>




