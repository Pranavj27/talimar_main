<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');
$yes_no_option3 = $this->config->item('yes_no_option3');
$lender_app_status = $this->config->item('lender_app_status');
$lender_app_request = $this->config->item('lender_app_request');

$loan_status_filter = array(
								'' => 'Select All',
								1  => 'Pipeline',
								2  => 'Active',
								3  => 'Paid Off',
							);

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Active Lender Approvals</h1>
				</div>
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>ReportData/lender_approvals">
						<!-- <input type="hidden" name="loan_status" value="<?php echo isset($loan_status) ? $loan_status : '';?>"> -->
						<input type="hidden" name="reportDisplay" value="pdf">
						<button type="submit" class="btn blue">PDF</button>
					</form>
				</div>
				
		</div>
		
		<!--<div class="row">
			<form method="post" action="<?php echo base_url();?>ReportData/lender_approvals">
				<div class="col-md-3">
					<label>Loan Status:</label>
					<select class="form-control" name="loan_status">
						<?php foreach($loan_status_option as $key => $row){ ?>
							<option value="<?php echo $key;?>"><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>

				<div class="col-md-1">
					<button class="btn btn-sm btn-primary" type="submit" style="margin-top: 25px;">Filter</button>
				</div>
			</form>
		</div>-->
		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Street Address</th>
					<th>Unit #</th>
					<th>City</th>
					<th>State</th>
					<th>Zip</th>					
					<th>Borrower Name</th>
					<th>Reason</th>
					<th>Status</th>									
				</tr>
				</thead>
				<tbody>
				
				<?php if(isset($lender_app_data) && is_array($lender_app_data)){ 
						foreach($lender_app_data as $row){ ?>
						
									<tr>
										<td><a href="<?php echo base_url();?>load_data/<?php echo $row->loan_id;?>#lender_approval_main"><?php echo $row->property_address;?></a></td>
										<td><?php echo $row->unit;?></td>
										<td><?php echo $row->city;?></td>
										<td><?php echo $row->state;?></td>
										<td><?php echo $row->zip;?></td>
										<td><a href="<?php echo base_url();?>borrower_view/<?php echo $row->borrower;?>"><?php echo $Fetch_All_borrowername[$row->borrower];?></a></td>
										<td><?php echo $lender_app_request[$row->LP_request];?></td>
										<td><?php echo $lender_app_status[$row->LP_status];?></td>
									</tr>
				
								<?php  } }else{ ?>
				
								<tr>
									<td style="width:100%;" colspan="8">No data found!</td>
								</tr>
				<?php } ?>
					
				</tbody>		
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {
	 
    $('#table').DataTable();
	
});



</script>




