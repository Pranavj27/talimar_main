<?php
error_reporting(0);
// echo "<pre>";
// print_r($fetch_active_data);
// echo "</pre>";
//$yes_no_option2 	= $this->config->item('yes_no_option2');

$loan_type_option 	= $this->config->item('loan_type_option');
$position_option 	= $this->config->item('position_option');
$template_yes_no_option = array(

								'' => 'Select All',
								1 => 'Yes',
								2 => 'No',
								
							);


$property_typee	= $this->config->item('property_typee');
$loan_typee	= $this->config->item('loan_typee');
$loan_status_option	= $this->config->item('loan_status_option');

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Loan Marketing Report</h1>	
					
				</div>
			<!-- 	<div class="top_download">
				<a href="<?php echo base_url();?>download_social_media">
				
				<button  class="btn blue">PDF</button>
				</a>
				</div> -->
		
	<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>download_social_media">
						
						<input type="hidden" name="rsm" value="<?php echo isset($rsm) ? $rsm : '';?>">
						<input type="hidden" name="signed_posted" value="<?php echo isset($signed_posted) ? $signed_posted : '';?>">
						<input type="hidden" name="loan_closing_posted" value="<?php echo isset($loan_closing_posted) ? $loan_closing_posted : '';?>">
						
						<input type="hidden" name="loan_status" value="<?php echo isset($loan_status) ? $loan_status : '';?>">
						<input type="hidden" name="adv_img" value="<?php echo isset($adv_img) ? $adv_img : '';?>">
						<input type="hidden" name="GoogleRequest" value="<?php echo isset($GoogleRequest) ? $GoogleRequest : '';?>">
						<input type="hidden" name="LoanDescriptionComplete" value="<?php echo isset($LoanDescriptionComplete) ? $LoanDescriptionComplete : '';?>">
						<input type="hidden" name="AdvertisingImageComplete" value="<?php echo isset($AdvertisingImageComplete) ? $AdvertisingImageComplete : '';?>">
						<button type="submit" class="btn red">PDF</button>
						<button type="button" name="submit" class="btn blue" onclick="form_submit()" style="margin-top:14px;">Filter</button>
				
					</form>
				</div>

		</div>
			<div class="row">
			<form method="POST" action="<?php echo base_url();?>social_media" id="social_media_submit">
				
				<div class="col-md-2">
					<label>Loan Status:</label>
					<select class="form-control" name="loan_status">
						<option value="">Select All</option>
						<?php 
						unset($loan_status_option[0]);
						unset($loan_status_option[4]);
						unset($loan_status_option[7]);
						foreach($loan_status_option as $key => $row){ ?>
							<option value="<?php echo $key;?>" <?php if($loan_status == $key){echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>

				<div class="col-md-2">
					<label>Google Request:</label>
					<select class="form-control" name="GoogleRequest">
						<?php foreach($template_yes_no_option as $key => $row){ ?>
							<option value="<?php echo $key;?>" <?php if($GoogleRequest == $key){echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>

				<div class="col-md-2">
					<label>Advertising Image:</label>
					<select class="form-control" name="AdvertisingImageComplete">
						<?php foreach($template_yes_no_option as $key => $row){ ?>
							<option value="<?php echo $key;?>" <?php if($AdvertisingImageComplete == $key){echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>

				<div class="col-md-2">
					<label>Loan Description:</label>
					<select class="form-control" name="LoanDescriptionComplete">
						<?php foreach($template_yes_no_option as $key => $row){ ?>
							<option value="<?php echo $key;?>" <?php if($LoanDescriptionComplete == $key){echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>

				<div class="col-md-2">
					<label>Loan Closing Posted:</label>
					<select class="form-control" name="loan_closing_posted">
						<?php foreach($template_yes_no_option as $key => $row){ ?>
							<option value="<?php echo $key;?>" <?php if($loan_closing_posted == $key){echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>

				<div class="col-md-2">
					<label>Social Media Blast:</label>
					<select class="form-control" name="rsm">
						<?php foreach($template_yes_no_option as $key => $row){ ?>
							<option value="<?php echo $key;?>" <?php if($rsm == $key){echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>

				<div class="col-md-2">
					<label>Sign Posted:</label>
					<select class="form-control" name="signed_posted">
						<?php foreach($template_yes_no_option as $key => $row){ ?>
							<option value="<?php echo $key;?>" <?php if($signed_posted == $key){echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>

				
				<div class="col-md-2">
					<label>Advertising Complete:</label>
					<select class="form-control" name="adv_img">
						<?php foreach($template_yes_no_option as $key => $row){ ?>
							<option value="<?php echo $key;?>" <?php if($adv_img == $key){echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>

			</form>
		</div>
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Borrower Name</th>
					<!-- <th>TaliMar # </th> -->
					<th>Loan Amount </th>
					<th>Position</th>
					<th>Term </th>
					<th>Property Type</th>
					<th>Loan Type</th>
					<th>Property Address </th>
					<th>Unit</th>
					<th>City </th>
					<th>State </th>
					<th>Images<br>Created </th>
					<th>Closing<br>Posted</th>
					<th>Google<br>Request</th>
					<th>Sign<br>Posted</th>
					<th>Social Media<br> Complete</th>
					<!--<th>Posted on<br>Website</th>-->
					
					
					<!--<th>After<br>Photos</th>-->
					</tr>  
				</thead>
				<tbody class="scrollable">
					<?php
					$count = 0;
					$total_loan_amount = 0;
				
					if(isset($fetch_social_loan))
					{
						foreach($fetch_social_loan as $key => $row)
						{
							$total_loan_amount = $total_loan_amount + $row['loan_amount'];							
							
							?>
							<tr>
								<td><a href="<?php echo base_url();?>borrower_view/<?php echo $row['borrower_id'];?>"><?php echo $row['borrower']; ?></td>
								<!-- <td><a href="<?php echo base_url().'load_data/'.$row['loan_id']; ?>"><?php echo $row['talimar']; ?></td> -->
								<td>$<?php echo number_format($row['loan_amount'],2); ?></td>
								<td><?php echo $position_option[$row['loan_position']]; ?></td>
								<td><?php echo $row['loan_term']; ?></td>
								<td><?php echo $property_typee[$row['prop_type']]; ?></td>
								<td><?php echo $loan_typee[$row['loan_type']]; ?></td>
								<td><a href="<?php echo base_url().'load_data/'.$row['loan_id']; ?>"><?php echo $row['property_address']; ?></a></td>
								<td><?php echo $row['unit']; ?></td>
								<td><?php echo $row['city']; ?></td>
								<td><?php echo $row['state']; ?></td>
								<td><a href="<?php echo base_url().'load_data/'.$row['loan_id']; ?>#marketing_option"><?php echo $row['image_ceate_val']; ?></a></td>
								
								<td style="text-align:center;"><a href="<?php echo base_url().'load_data/'.$row['loan_id']; ?>#marketing_option"><?php echo $row['closing_posted']; ?></a></td>
								
								<td style="text-align:center;"><a href="<?php echo base_url().'load_data/'.$row['loan_id']; ?>#marketing_option"><?php echo $row['google_reguest']; ?></a></td>
								
								<td style="text-align:center;"><a href="<?php echo base_url().'load_data/'.$row['loan_id']; ?>#marketing_option"><?php echo $row['sign_posted']; ?></a></td>
								
								<td style="text-align:center;"><a href="<?php echo base_url().'load_data/'.$row['loan_id']; ?>#marketing_option"><?php echo $row['social_media_com']; ?></a></td>
								
								<!--<td style="text-align:center;"><?php echo $posted_on_website; ?></td>-->
								<!--<td style="text-align:center;"><?php //echo $avaliable_realese_sign; ?></td>-->
							</tr>
							<?php
							$count++;
						}
					}
					?>
				</tbody>				
				<tfoot>
					<tr>				
						<th>Total &nbsp;&nbsp;  <?php echo $count; ?></th>
						
						<th>$<?php echo number_format($total_loan_amount); ?></th>
						<th colspan="13"></th>
					</tr>	
					<tr>				
						<th>Average</th>
					
						<th>$<?php echo number_format($total_loan_amount/$count); ?></th>
						<th colspan="13"></th>
					</tr>					
				</tfoot>
			</table>
		</div>
	</div>
	
	
	
	
</div>
<script>
$(document).ready(function() {
    $('#table').DataTable({
  "order": [[ 9, "desc" ],[ 1, "desc" ]],



    });

   
});


function form_submit(){

$('form#social_media_submit').submit();

}
</script>