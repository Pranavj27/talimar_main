<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$date_option = $this->config->item('date_option');
// echo "<pre>";
// print_r($fetch_active_data);
// echo "</pre>";

//$select_box_name = isset($select_box) ? '('.$select_box.')' : '' ;
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; User Stats <?php //echo $select_box_name; ?></h1>	
					
				</div>
				
				<div class="top_download">
				<form method="POST" action="<?php echo base_url();?>employment_database_goal_pdf">
					<input type="hidden" name="select_box" value="<?php echo isset($select_box) ? $select_box : '';?>">
					
					<input type ="hidden" name="start_date" value="<?php echo $this->input->post('start_date');?>" />
					<input type ="hidden" name="end_date" value="<?php echo $this->input->post('end_date');?>" />
			      <input type="hidden" name="date_change" value="<?php echo isset($date_change) ? $date_change : '';?>">
					<button  type="submit" class="btn blue">PDF</button>
				</form>
				</div>
				
		</div>
		
		<div class="row">
			<div class="col-md-3">
				<form method="POST" action="<?php echo base_url().'employment_database_goal';?>" id="form_peer_street">
				User Name: 
				<select name="select_box" onchange="form_peer_street()" class="form-control" required>
					<option value="0" >All</option>
					<?php 
					
					if(isset($fetch_user_data)){
					
					foreach($fetch_user_data as $k=>$row){?>
					<option value="<?php echo $row->id; ?>" <?php if(isset($select_box)){ if($select_box ==$row->id){ echo 'selected'; }} ?>><?php echo  $row->fname .' '.$row->middle_name .' '.$row->lname; ?></option>
	<?php }}?>
				</select>
				</div>
				<div class="col-md-3">
				
				Date: 
						<select id="date_change" name="date_change" class="form-control ">
							<option value="">Select One</option>
							<?php foreach($date_option as $key => $row) { ?>
							<option value="<?php echo $key;?>" <?php if($key ==$date_change ){ echo "selected"; }?> ><?php echo $row;?></option>
							<?php } ?>
						</select>
				</div>
				
				<div class="col-md-3">
					Date From:
					<input type="text" name="start_date" class="datepicker form-control" placeholder="Start Date">
					 To:
					<input type="text" name="end_date" class="datepicker form-control" placeholder="End Date">
				</div>
				<div class="col-md-3">
					<button type="submit" name="submit" class="btn blue" style="margin-top: 17px !important;">Filter</button>
				</div>
			</form>
		</div>
		
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Name</th>
					<th>Note Entries</th>
					<th>Name Entries</th>
					<th>New Deal Entries</th>
					
					
					
					</tr>
				</thead>
				<tbody class="scrollable">
				
				<?php 
				$count = 0;
				$note = 0;
				$name_contact = 0;
				$new_deal = 0;
				foreach($user_entries as $row){ 
				
				$count++;
				$note += $row['note'];
				$name_contact += $row['name_contact'];
				$new_deal += $row['new_deal'];
				
				?>
					<tr>
						<td><?php echo $row['name'];?></td>
						<td><?php echo $row['note'];?></td>
						<td><?php echo $row['name_contact'];?></td>
						<td><?php echo $row['new_deal'];?></td>
						
					</tr>
				<?php } ?>		
				</tbody>	
				<tfoot>
					<tr>
						<th>Total: <?php echo $count; ?></th>
						<th><?php echo $note;?></th>
						<th><?php echo $name_contact;?></th>
						<th><?php echo $new_deal;?></th>
						
					</tr>
				</tfoot>
				
			</table>
			<br>
			
			
		<div class="col-md-12">&nbsp;</div>
		<div class="col-md-12">
			<div class="page-title">
				<h3><strong>General Stats:</strong></h3>
			</div>
		</div>
		<div class="rc_class">
			<table class="tables table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				
				</thead>
				<tbody>
					<tr>
						<td>Note Entries</td>
						<td><?php echo $note;?></td>
						<td>New Contacts</td>
				         <td><?php echo $new_deal;?></td>
			
						
					</tr>
				</tbody>
			</table>

		</div>

			
				<div class="col-md-12">&nbsp;</div>
		<div class="col-md-12">
			<div class="page-title">
				<h3><strong>Closed Loans:</strong></h3>
			</div>
		</div>
		<div class="rc_class">
			<table class="tables table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				
				</thead>
				<tbody>
					<tr>
					<td>Loan Originator</td>
					<td><?php echo $user_entries[0]['loan_orignitor'];?></td>
					<td>Underwriter</td>
					<td><?php echo $user_entries[0]['underwritter'];?></td>
					<td>Loan Closer</td>
					<td><?php echo $user_entries[0]['loan_closer'];?></td>
			
						
					</tr>
				</tbody>
			</table>

		</div>	
			
						<div class="col-md-12">&nbsp;</div>
		<div class="col-md-12">
			<div class="page-title">
				<h3><strong>New Lenders:</strong></h3>
			</div>
		</div>
		<div class="rc_class">
			<table class="tables table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				
				</thead>
				<tbody>
					<tr>
				   <td>Lenders Added</td>
					<td><?php echo $user_entries[0]['added_lender_count']; ?></td>
					<td>NDA Signed</td>
					<td><?php echo $user_entries[0]['nda']; ?></td>
					<td>Setup Package</td>
					<td><?php echo $user_entries[0]['Setup_p']; ?></td>
			       
					</tr>
					
					<tr>
					
					 <td>New Lenders</td>
					<td><?php echo $user_entries[0]['new_lenders_count']; ?></td>  
					<td colspan="4"></td>  
					
					</tr>
				</tbody>
			</table>

		</div>		
			
								<div class="col-md-12">&nbsp;</div>
		<div class="col-md-12">
			<div class="page-title">
				<h3><strong>Active Loan Portfolio:</strong></h3>
			</div>
		</div>
		<div class="rc_class">
			<table class="tables table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				
				</thead>
				<tbody>
					<tr>
			       <td>Loan Originator</td>
					<td><?php echo $user_entries[0]['active_loan_orignitor'];?></td>
					<td>Underwriter</td>
					<td><?php echo $user_entries[0]['active_underwritter'];?></td>
					<td>Loan Closer</td>
					<td><?php echo $user_entries[0]['active_loan_closer'];?></td>
			       
						
					</tr>
					
					<tr>
					 <td>Investor Relations</td>
					<td><?php echo $user_entries[0]['active_invertor'];?></td>
					<td >Loan Servicer</td>
				    <td ><?php echo $user_entries[0]['active_servicer'];?></td>
				    <td colspan="2"></td>
					</tr>
				</tbody>
			</table>

		</div>		
		
		</div>
	</div>
	
	
	
	
</div>
<script>
function form_peer_street()
{
	$("form#form_peer_street").submit();
}

$(document).ready(function() {
    $('#table').DataTable({
		
		searching: false, paging: false, info: false
	
	
	
	
	});
  
});


</script>
<style>
		.talimar_no_dropdowns {
		float: left;
		
	}
</style>