<?php
//error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');
$yes_no_option3 = $this->config->item('yes_no_option3');
$lender_app_status = $this->config->item('lender_app_status');
$lender_app_request = $this->config->item('lender_app_request');
$link_to_lender = $this->config->item('link_to_lender');

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Lender Marketing Report</h1>
				</div>
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>ReportData/LenderMarketingReport">
						<!-- <input type="hidden" name="loan_status" value="<?php echo isset($loan_status) ? $loan_status : '';?>"> -->
						<input type="hidden" name="reportDisplay" value="pdf">
						<button type="submit" class="btn blue">PDF</button>
					</form>
				</div>
				
		</div>
		
		<div class="row">
			<form method="post" action="<?php echo base_url();?>ReportData/LenderMarketingReport">
				<div class="col-md-2">
					<label>Lender Account:</label>
					<select class="form-control" name="LenderAccount">
						<?php foreach($yes_no_option3 as $key => $row){ ?>
							<option value="<?php echo $key;?>" <?php if($LenderAccount == $key){echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-2">
					<label>E-Mail Notification:</label>
					<select class="form-control" name="emailnoti">
						<?php foreach($yes_no_option3 as $key => $row){ ?>
							<option value="<?php echo $key;?>" <?php if($emailnoti == $key){echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-2">
					<label>Text Notification:</label>
					<select class="form-control" name="textnoti">
						<?php foreach($yes_no_option3 as $key => $row){ ?>
							<option value="<?php echo $key;?>" <?php if($textnoti == $key){echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>

				<div class="col-md-1">
					<button class="btn btn-sm btn-primary" type="submit" style="margin-top: 25px;">Filter</button>
				</div>
			</form>
		</div>

		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Phone</th>
					<th>E-Mail</th>
					<th>Lender<br>Account</th>					
					<th>E-Mail<br>Notification</th>
					<th>Text<br>Notification</th>
					<th>Lender<br>Portal</th>									
					<th>Last Login<br>Date</th>									
				</tr>
				</thead>
				<tbody>
				
				<?php 

				// echo '<pre>';
				// print_r($lender_mar_datadd);
				// echo '</pre>';

				if(isset($lender_market_data) && is_array($lender_market_data)){ 
						foreach($lender_market_data as $row){	

							if(in_array(2, $lender_mar_datadd[$row['contact_id']])){
								$emailnoti = 'Yes';
							}else{
								$emailnoti = 'No';
							}

							if(in_array(9, $lender_mar_datadd[$row['contact_id']])){
								$textnoti = 'Yes';
							}else{
								$textnoti = 'No';
							}

							?>
							<!--------------For Email notification ---------------->
							<?php if($emailnoti == 1){ if($emailnoti == 'Yes'){ ?>
						
								<tr>
									<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['contact_firstname'];?></a></td>
									<td><?php echo $row['contact_lastname'];?></td>
									<td><?php echo $row['contact_phone'];?></td>
									<td><?php echo $row['contact_email'];?></td>
									<td><?php echo $row['lenderaccount'];?></td>
									<td><?php echo $emailnoti; ?></td>
									<td><?php echo $textnoti; ?></td>
									<td><?php echo $link_to_lender[$row['link_to_lender']];?></td>
									<td><?php echo $row['portal_login'];?></td>
								</tr>

							<?php } }elseif($emailnoti == 2){ if($emailnoti == 'No'){ ?>

								<tr>
									<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['contact_firstname'];?></a></td>
									<td><?php echo $row['contact_lastname'];?></td>
									<td><?php echo $row['contact_phone'];?></td>
									<td><?php echo $row['contact_email'];?></td>
									<td><?php echo $row['lenderaccount'];?></td>
									<td><?php echo $emailnoti; ?></td>
									<td><?php echo $textnoti; ?></td>
									<td><?php echo $link_to_lender[$row['link_to_lender']];?></td>
									<td><?php echo $row['portal_login'];?></td>
								</tr>

							<?php } } ?>

							<!--------------For Text notification ---------------->
							<?php if($textnoti == 1){ if($textnoti == 'Yes'){ ?>
						
								<tr>
									<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['contact_firstname'];?></a></td>
									<td><?php echo $row['contact_lastname'];?></td>
									<td><?php echo $row['contact_phone'];?></td>
									<td><?php echo $row['contact_email'];?></td>
									<td><?php echo $row['lenderaccount'];?></td>
									<td><?php echo $emailnoti; ?></td>
									<td><?php echo $textnoti; ?></td>
									<td><?php echo $link_to_lender[$row['link_to_lender']];?></td>
									<td><?php echo $row['portal_login'];?></td>
								</tr>

							<?php } }elseif($textnoti == 2){ if($textnoti == 'No'){ ?>

								<tr>
									<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['contact_firstname'];?></a></td>
									<td><?php echo $row['contact_lastname'];?></td>
									<td><?php echo $row['contact_phone'];?></td>
									<td><?php echo $row['contact_email'];?></td>
									<td><?php echo $row['lenderaccount'];?></td>
									<td><?php echo $emailnoti; ?></td>
									<td><?php echo $textnoti; ?></td>
									<td><?php echo $link_to_lender[$row['link_to_lender']];?></td>
									<td><?php echo $row['portal_login'];?></td>
								</tr>
								
							<?php } }else{ ?>

								<tr>
									<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['contact_firstname'];?></a></td>
									<td><?php echo $row['contact_lastname'];?></td>
									<td><?php echo $row['contact_phone'];?></td>
									<td><?php echo $row['contact_email'];?></td>
									<td><?php echo $row['lenderaccount'];?></td>
									<td><?php echo $emailnoti; ?></td>
									<td><?php echo $textnoti; ?></td>
									<td><?php echo $link_to_lender[$row['link_to_lender']];?></td>
									<td><?php echo $row['portal_login'];?></td>
								</tr>

							<?php } ?>
	
					<?php  } }else{ ?>
	
					<tr>
						<td style="width:100%;" colspan="9">No data found!</td>
					</tr>
				<?php } ?>
					
				</tbody>		
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {
	 
    $('#table').DataTable();
	
});

</script>