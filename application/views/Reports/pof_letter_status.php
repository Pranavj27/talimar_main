<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');
$yes_no_option3 = $this->config->item('yes_no_option3');
$pro_app_req_status = $this->config->item('pro_app_req_status');


?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; POF Letter Status</h1>
				</div>
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>ReportData/pof_letter">
						<input type="hidden" name="ApprovalStatus" value="<?php echo isset($ApprovalStatus) ? $ApprovalStatus : '';?>">
						<input type="hidden" name="reportDisplay" value="pdf">
						<button type="submit" class="btn blue">PDF</button>
					</form>
				</div>
				
		</div>
		
		<div class="row">
			<form method="post" action="<?php echo base_url();?>ReportData/pof_letter">
				<div class="col-md-2">
					<label>Approval Status:</label>
					<select class="form-control" name="ApprovalStatus">
					<option value="">Select All</option>
						<?php foreach($pro_app_req_status as $key => $row) { ?>
							<option value="<?php echo $key;?>" <?php if($key == $ApprovalStatus){ echo 'selected';}?>><?php echo $row;?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-1">
					<button type="submit" name="submit" class="btn btn-primary btn-sm" style="margin-top: 25px;">Filter</button>
				</div>
			</form>
		</div>

		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Borrower Name</th>
					<th>Contact Name</th>
					<th>Contact E-Mail</th>					
					<th>Contact Phone</th>					
					<th>Street Address</th>
					<th>Unit #</th>
					<th>City</th>
					<th>State</th>
					<th>Zip</th>
					<th>Approved Amount</th>					
					<th>Approval Status</th>					
				</tr>
				</thead>
				<tbody>
				
				<?php if(isset($pof_letter_data) && is_array($pof_letter_data)){ 
						foreach($pof_letter_data as $row){ ?>
						
									<tr>
										<td><a href="<?php echo base_url();?>borrower_view/<?php echo $row->borrower;?>"><?php echo $Fetch_All_borrowername[$row->borrower];?></a></td>
										<td><?php echo $getBorrowerContactName['full_name'][$row->borrower];?></td>
										<td><?php echo $getBorrowerContactName['contact_email'][$row->borrower];?></td>
										<td><?php echo $getBorrowerContactName['contact_phone'][$row->borrower];?></td>
										<td><?php echo $row->pro_address;?></td>
										<td><?php echo $row->pro_unit;?></td>
										<td><?php echo $row->pro_city;?></td>
										<td><?php echo $row->pro_state;?></td>
										<td><?php echo $row->pro_zip;?></td>
										<td>$<?php echo number_format($row->ApprovalAmount);?></td>
										<td><?php echo $pro_app_req_status[$row->req_status];?></td>
									</tr>
				
								<?php  } }else{ ?>
				
								<tr>
									<td style="width:100%;" colspan="11">No data found!</td>
								</tr>
				<?php } ?>
					
				</tbody>		
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {
	 
    $('#table').DataTable();
	
});



</script>




