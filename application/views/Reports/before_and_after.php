<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');
$not_applicable_option = $this->config->item('not_applicable_option');
$before_after_option_new = $this->config->item('before_after_option_new');
$property_typee = $this->config->item('property_typee');

$loan_status_filter = array(
								'' => 'Select All',
								1  => 'Pipeline',
								2  => 'Active',
								3  => 'Paid Off',
							);

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Before & After Videos</h1>	
					
				</div>
				
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>Reports/before_and_after_pdf">
					
						<input type="hidden" name="loan_status" value="<?php echo isset($loan_status) ? $loan_status : '';?>">
						<input type="hidden" name="video_status" value="<?php echo isset($video_status) ? $video_status : '';?>">
						<button type="submit" class="btn blue">PDF</button>
						
					</form>
				</div>
				
		</div>
		
		<div class="row">
		<form method="post" action="<?php echo base_url();?>Reports/before_and_after">
			<div class="col-md-2">
				<label><b>Loan status:</b></label>
				<select name="loan_status" class="form-control">
					<option value="">Select All</option>
					<option value="1" <?php if($loan_status == 1){echo 'selected';}?>>Pipeline</option>
					<option value="2" <?php if($loan_status == 2){echo 'selected';}?>>Active</option>
					<option value="3" <?php if($loan_status == 3){echo 'selected';}?>>Paid Off</option>
				</select>
			</div>
			
			<div class="col-md-2">
				<label><b>Video Status:</b></label>
				<select name="video_status" class="form-control">
					<option value="">Select One</option>
					<option value="Requested" <?php if("Requested" == $video_status){echo 'selected';}?>>Requested</option>
					<option value="Completed" <?php if("Completed" == $video_status){echo 'selected';}?>>Completed</option>
					<option value="Posted" <?php if("Posted" == $video_status){echo 'selected';}?>>Posted</option>
				</select>
			</div>
			<div class="col-md-2">
				<label><b>Posted:</b></label>
				<select name="posted_status" class="form-control">
					<option value="">Select One</option>
					<option value="1" <?php if("1" == $posted_status){echo 'selected';}?>>Yes</option>
					<option value="2" <?php if("2" == $posted_status){echo 'selected';}?>>No</option>
				</select>
			</div>
			
			<div class="col-md-2">
				<button type="submit" name="submit" class="btn btn-sm btn-primary" style="margin-top:25px;">Filter</button>
			</div>
		</form>
		</div>
		
		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Property Street Address</th>
					<th>Unit</th>
					<th>City</th>
					<th>State</th>
					<th>Zip</th>
					<th>Loan Amount</th>
					<th>Property Type</th>
					<th>Video Status</th>	
					<th>Shared</th>									
				</tr>
				</thead>
				<tbody>
					<?php if(isset($before_and_after) && is_array($before_and_after)){ 
								foreach($before_and_after as $row){
									if($row['available_release']!="Requested" && $row['available_release']!="Completed" && $row['available_release']!="Posted"){
										$row['available_release']="Select One";
									}
								?>
								<tr>
									<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['property_address'];?></a></td>
									<td><?php echo $row['unit'];?></td>
									<td><?php echo $row['city'];?></td>
									<td><?php echo $row['state'];?></td>
									<td><?php echo $row['zip'];?></td>
									<td>$<?php echo number_format($row['loan_amount'],2);?></td>
									<td><?php echo $property_typee[$row['marketing_property_type']];?></td>
									<td><?php echo $row['available_release'];?></td>
									<td><?php echo $row['image_ab_Posted'];?></td>							
								</tr>
				
						<?php } }else{ ?>
				
					<tr>
						<td style="width:100%;" colspan="8">No data found!</td>
					</tr>
				<?php } ?>
				</tbody>		
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {
	 
    $('#table').DataTable();
	
});



</script>




