<?php
error_reporting(0);
// echo $join_sql;
$serviceing_sub_agent_option			= $this->config->item('serviceing_sub_agent_option');
$serviceing_who_pay_servicing_option	= $this->config->item('serviceing_who_pay_servicing_option');
$yes_no_option_verified 			    = $this->config->item('yes_no_option_verified');
$loan_status_option 			    	= $this->config->item('loan_status_option');
$position_option 			    		= $this->config->item('position_option');
?>

<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Loan Servicing Income</h1>	
					
				</div>
				<div class="top_download">
					
					<form method="POST" action="<?php echo base_url();?>download_monthly_income">
					<input type="hidden" name="select_box" value="<?php echo isset($select_box) ? $select_box : ''; ?>">
					  <input type="hidden" name="verified" value="<?php echo isset($verified) ? $verified : ''; ?>">
					  <input type="hidden" name="loan_status" value="<?php echo isset($loan_status) ? $loan_status : ''; ?>">
					<button type="submit" class="btn blue">PDF</button>
					</form>
				</div>
				
		</div>
		
		<div class="row">
			<form method="POST" action="<?php echo base_url().'monthly_servicing_income';?>" onchange="form_sumbit()" id="monthly_servicing">
			<div class="talimar_no_dropdowns">
				Select Servicer:
				<select name="select_box">
				<option value="">Select All</option>
				<?php
					foreach($serviceing_sub_agent_option as $key => $row)
					{
						?>
						<option value="<?php echo $key; ?>" <?php if(isset($select_box)){ if($select_box == $key){ echo 'selected'; } } ?>><?php echo $row; ?></option>
						<?php
					}
				?>
					
				</select>
			</div>
			
			<div class="talimar_no_dropdowns">
				Verified:
				<select name="verified">
				<option value="">Select All</option>
				<?php
					foreach($yes_no_option_verified as $key => $row)
					{
						?>
						<option value="<?php echo $key; ?>" <?php if(isset($verified)){ if($verified == $key){ echo 'selected'; } } ?>><?php echo $row; ?></option>
						<?php
					}
				?>
					
				</select>
			</div>

			<div class="talimar_no_dropdowns">
				Loan Status:
				<select name="loan_status">
				<option value="">Select All</option>
				<?php
					unset($loan_status_option[0]);
					foreach($loan_status_option as $key => $row)
					{
						?>
						<option value="<?php echo $key; ?>" <?php if(isset($loan_status)){ if($loan_status == $key){ echo 'selected'; } } ?>><?php echo $row; ?></option>
						<?php
					}
				?>
					
				</select>
			</div>
			
			</form>
		</div>
		<div class="rc_class">
			<table id="table" class="table th_text_align_center table-bordered table-striped table-condensed flip-content" >
				<thead>
					<tr>
						<th>Street Address</th>
						<!-- <th>Servicer</th> -->
						<th>Servicer<br>#</th>
						<th>Closing Date</th>
						<th>Duration</th>
				     	<th>Balance</th>
						<th>Payer</th>
						<th>Spread</th>
						<th>Gross<br>Servicing<br>Income</th>
						<th>Lender<br>Servicing<br>Fee</th>
						<th>Broker Fee</th>
						<th>Payment Fee</th>
						<th>Servicer Fee</th>
						<th>Net Income</th>						
					</tr>
				</thead>
				<tbody>
				  <?php
				  $key 						= 0;
				  $total_amount 			= 0;
				  $total_servicing_income 	= 0;
				  $total_servicing_cost 	= 0;
				  $total_minimum_servicing_fee = 0;
				  $total_gross_servicing 	= 0;
				  $total_count_lender 		= 0;
				  $avg_servicing_fee 		= 0;
				  $total_pro_fees			= 0;
				  $toal_brokr_disbursment_fee=0;
				  $total_net_servicing_new = 0;
				  $monthlyPayments = 0;

					foreach($all_monthly_income as $key=>$row)
					{

							//for Payment Processing Fee...
							$payment_pro_fees = $all_monthly_income[$key]['loan_amount'];
							
							$pro_fees = '0';
							if($payment_pro_fees > 0 && $payment_pro_fees <= 400000){
								$pro_fees = '0';
							}elseif($payment_pro_fees >= 400001 && $payment_pro_fees <= 500000){
								$pro_fees = '10.00';
							}elseif($payment_pro_fees >= 500001 && $payment_pro_fees <= 600000){
								$pro_fees = '20.00';
							}elseif($payment_pro_fees >= 600001 && $payment_pro_fees <= 700000){
								$pro_fees = '30.00';
							}elseif($payment_pro_fees >= 700001 && $payment_pro_fees <= 800000){
								$pro_fees = '40.00';
							}elseif($payment_pro_fees >= 800001 && $payment_pro_fees <= 900000){
								$pro_fees = '50.00';
							}elseif($payment_pro_fees >= 900001 && $payment_pro_fees <= 1000000){
								$pro_fees = '60.00';
							}elseif($payment_pro_fees >= 1000001 && $payment_pro_fees <= 2000000){

								$pro_fees = '70.00';
							}
							elseif($payment_pro_fees >= 2000001 && $payment_pro_fees <= 3000000){

								$pro_fees = '80.00';
							}
							elseif($payment_pro_fees >= 3000001 && $payment_pro_fees <= 4000000){

								$pro_fees = '90.00';
							}
							elseif($payment_pro_fees >= 4000001 && $payment_pro_fees <= 5000000){

								$pro_fees = '100.00';
							}
							elseif($payment_pro_fees >= 5000001 && $payment_pro_fees <= 6000000){

								$pro_fees = '110.00';
							}
							elseif($payment_pro_fees >= 6000001 && $payment_pro_fees <= 7000000){

								$pro_fees = '120.00';
							}
							elseif($payment_pro_fees >= 7000001 && $payment_pro_fees <= 8000000){

								$pro_fees = '130.00';
							}
							elseif($payment_pro_fees >= 8000001 && $payment_pro_fees <= 9000000){

								$pro_fees = '140.00';
							}	
							elseif($payment_pro_fees >= 9000001 && $payment_pro_fees <= 10000000){

								$pro_fees = '150.00';
							}
							elseif($payment_pro_fees >= 10000001 && $payment_pro_fees <= 11000000){

								$pro_fees = '160.00';
							}
							elseif($payment_pro_fees >= 11000001 && $payment_pro_fees <= 12000000){

								$pro_fees = '170.00';
							}else{

								$pro_fees = '0';
							}
								


						$total_pro_fees += $pro_fees;
						
						  $loan_amount = $all_monthly_income[$key]['loan_amount'];
						  $servicing_fee = $all_monthly_income[$key]['servicing_fee'];
						  
						  //$gross_servicing = ($loan_amount * ($servicing_fee / 100 ))/12;
		                    if($all_monthly_income[$key]['servicing_lender_rate'] == 'NaN' || $all_monthly_income[$key]['servicing_lender_rate'] == '' || $all_monthly_income[$key]['servicing_lender_rate'] == '0.000'){

								$lender_ratessss = $all_monthly_income[$key]['invester_yield_percent'];
							}else{

							    $lender_ratessss = $all_monthly_income[$key]['servicing_lender_rate'];
							}

                           $nnew_val=$all_monthly_income[$key]['interest_rate']-$lender_ratessss;

                           $nnew_vals = $nnew_val/100;

                           $gross_servicing=($all_monthly_income[$key]['current_balance']*$nnew_vals)/12;
						  
						  $minimum_servicing_fee = $all_monthly_income[$key]['minimum_servicing_fee'];

          
	        			// $servicing_cost =$lender_servicing_fee + $all_monthly_income[$key]['broker_servicing_fees'] + $pro_fees;
 							$lender_servicing_fee = $all_monthly_income[$key]['count_lender'] * $all_monthly_income[$key]['minimum_servicing_fee'];

								if($lender_servicing_fee > $pro_fees){

									$pro_feesss=0;

								}else{

									$lender_servicing_fee=0;	

								}

							$servicing_cost=$lender_servicing_fee + $pro_feesss + $all_monthly_income[$key]['broker_servicing_fees'];
							$servicing_income=$gross_servicing - $servicing_cost;


	        		


							if($all_monthly_income[$key]['who_pay_servicing'] == 2)
							{
								
								$gross_servicing 				= $gross_servicing;
								$servicing_cost 				= 0;
								$servicing_income 				= $servicing_income;
								$lender_servicing_fee 				=0;
								$all_monthly_income[$key]['servicing_fee'] 			= $servicing_fee;
								$all_monthly_income[$key]['minimum_servicing_fee'] 	= 0;
							}


               	$all_incomee[] = array(
			
				'vendor_name' 			=> $row['vendor_name'],
				'fci'					=> $row['fci'],
				'talimar_loan'			=> $row['talimar_loan'],
				'loan_amount'			=> $row['loan_amount'],
				'current_balancee'		=> $row['current_balance'],
				//'property'				=> $row['property'],
				'broker_servicing_fees'	=> $row['broker_servicing_fees'],
				'who_pay_servicing'		=> $row['who_pay_servicing'],
				'servicing_fee'			=> $row['servicing_fee'],
				'gross_servicingg'		=> $gross_servicing,
			
				'minimum_servicing_fee'	=> $row['minimum_servicing_fee'],
				'servicing_cost'		=> $servicing_cost,
				'servicing_income'		=> $servicing_income,			
				'pro_fees'				=> $pro_fees,
				'loan_id'				=> $row['loan_id'],
				'sub_servicing_agent'   => $row['sub_servicing_agent'],
				'count_lender'			=> $lender_servicing_fee,
				'property'				=> $row['property'],

				'interest_rate' 		=> $row['interest_rate'],
				'servicing_lender_rate' => $row['servicing_lender_rate'],
				'invester_yield_percent' => $row['invester_yield_percent'],
				'position' 				=> $row['position'],
				'monthlyPayment' 		=> $row['monthlyPayment'],
				'loan_funding_date'		=> $row['loan_funding_date']
							
			);

}


	array_multisort(array_column($all_incomee, 'servicing_income'), SORT_DESC,$all_incomee);


        foreach($all_incomee as $key => $all_income){

        				$dateCurent = date('Y-m-d');
						$earlier = new DateTime(date('Y-m-d', strtotime($all_income['loan_funding_date'])));
						$later = new DateTime($dateCurent);

						$diff = $later->diff($earlier)->format("%a");

        			if($all_income['servicing_lender_rate'] == 'NaN' || $all_income['servicing_lender_rate'] == ''){

						$lender_ratessss = $all_income['invester_yield_percent'];
					}else{

					    $lender_ratessss = $all_income['servicing_lender_rate'];
					}

					$new_val = $all_income['interest_rate'] - $lender_ratessss;

					if($all_income['vendor_name'] == 'FCI Lender Services'){
						$servicesName = 'FCI'; 
					}elseif($all_income['vendor_name'] == 'Del Toro Loan Servicing'){
						$servicesName = 'Del Toro'; 
					}else{
						$servicesName = $all_income['vendor_name'];
					}

                                
						?>
						<tr>
							<td>
								<!--<a href ="<?php //echo base_url().'load_data/'.$all_income['loan_id']; ?>">-->
								<a href ="<?php echo base_url().'load_data/'.$all_income['loan_id']; ?>">
								<?php echo $all_income['property'];?></a>
							
							</td>
							<!-- <td><?php //echo $servicesName;?></td> -->
							<td><?php echo $all_income['fci'];?></td>
							<td><?php echo date('Y-m-d', strtotime($all_income['loan_funding_date'])); ?></td>
							<td><?php echo $diff; ?></td>
							<td><?php echo '$'.number_format($all_income['current_balancee']);?></td>
							<td><?php echo $serviceing_who_pay_servicing_option[$all_income['who_pay_servicing']];?></td>

							<td><?php echo number_format($new_val,2).'%';?></td>

							<td><?php echo '$'.number_format($all_income['gross_servicingg']);?></td>


							<td><?php echo '$'.number_format($all_income['count_lender']);?></td>
							
							<td><?php echo '$'.number_format($all_income['broker_servicing_fees']);?></td>


							<td><?php echo '$'.number_format($all_income['pro_fees']); ?></td>
							<td><?php echo '$'.number_format($all_income['servicing_cost']);?></td>
							<td><?php echo '$'.number_format($all_income['gross_servicingg'] - $all_income['servicing_cost'],2); ?></td>
							<!-- <td><?php echo number_format($all_income['interest_rate'],2);?>%</td>
							<td><?php echo '$'.number_format($all_income['monthlyPayment'],2);?></td>
							<td><?php echo $position_option[$all_income['position']];?></td> -->
							
						</tr>
						<?php

						$total_net_servicing_new = $all_income['gross_servicingg'] - $all_income['servicing_cost'];

						$monthlyPayments 				= $monthlyPayments + $all_income['monthlyPayment'];
						$total_count_lender 			= $total_count_lender + $all_income['count_lender'];
						$total_gross_servicing 			= $total_gross_servicing + $all_income['gross_servicingg'];
						$total_minimum_servicing_fee 	= $total_minimum_servicing_fee + $all_income['count_lender'];

						$total_servicing_cost 			= $total_servicing_cost + $all_income['servicing_cost'];
						$total_amount 					= $total_amount + $all_income['current_balancee'];
						//$total_servicing_income 		= $total_servicing_income + $all_income['servicing_income'];
						$total_servicing_income 		= $total_servicing_income + $total_net_servicing_new;

						$avg_servicing_fee 				= $avg_servicing_fee + $new_val;
						
						$toal_brokr_disbursment_fee     = $toal_brokr_disbursment_fee + $all_income['broker_servicing_fees'];

						$key++;
					}
					?>    
				</tbody>
				<tfoot>
					<tr>
						<th colspan="4">Total:&nbsp;&nbsp;<?php echo $key;?></th>
						<th><?php echo '$'.number_format($total_amount); ?></th>
						<th></th>
						<th></th>
						
						<th><?php echo '$'.number_format($total_gross_servicing,2);?></th>
					
						<th><?php echo '$'.number_format($total_minimum_servicing_fee,2);?></th>

						<th><?php echo '$'.number_format($toal_brokr_disbursment_fee,2);?></th>

						<th><?php echo '$'.number_format($total_pro_fees,2);?></th>
						<th><?php echo '$'.number_format($total_servicing_cost,2);?></th>
						<th><?php echo '$'.number_format($total_servicing_income,2); ?></th>
					</tr>
					
					<tr>
						<th  colspan="4">Average:</th>
						<th><?php echo '$'.number_format($total_amount/$key,2); ?></th>
						<th></th>

						<th><?php echo  number_format((($total_gross_servicing * 12) / $total_amount)*100,2);?>%</th>
						<th><?php echo '$'.number_format($total_gross_servicing/$key,2); ?></th>
						<th><?php echo '$'.number_format($total_minimum_servicing_fee/$key,2);?></th>

						<th><?php echo '$'.number_format($toal_brokr_disbursment_fee/$key,2);?></th>

						<th><?php echo '$'.number_format($total_pro_fees/$key,2);?></th>

						<th><?php echo '$'.number_format($total_servicing_cost/$key,2); ?></th>
						<th><?php echo '$'.number_format($total_servicing_income/$key,2); ?></th>
					</tr>
				</tfoot>
			</table>
			
	</div>
	</div>
	<!-- END CONTENT -->
</div>
<script>
$(document).ready(function() {
    $('#table').DataTable({
		"order": [[ 2, 'asc' ]] // "aaSorting": [[11,'DESC'] ]
	});	
});

function form_sumbit()
{
	$('form#monthly_servicing').submit();
}

</script>
<style>
.talimar_no_dropdowns {
	float: left;
}
table#table tfoot {
    background-color: #eeeeee;
}
table.dataTable tfoot th{
	border-top: 0 !important;
}
</style>