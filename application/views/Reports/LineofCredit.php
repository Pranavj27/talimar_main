<?php
error_reporting(0);
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$position = $this->config->item('position_option');
$closing_status_option = $this->config->item('closing_status_option');
$paidoff_status_option = $this->config->item('paidoff_status_option');
$newsub_servicer_option = $this->config->item('newsub_servicer_option');

$foremailyes = array(''=>'No', 2 => 'Yes');
$fortextyes = array(''=>'No', 9 => 'Yes');

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Preferred Borrower Accounts</h1>	
					
				</div>
				
				<div class="top_download">
					<form method="POST" action="<?php echo base_url();?>ReportData/LineofCredit">
						<input type="hidden" name="reportDisplay" value="pdf">
						<button type="submit" class="btn red">PDF</button>
					</form>
					<form method="POST" action="<?php echo base_url();?>ReportData/LineofCredit">
						<input type="hidden" name="reportDisplay" value="excel">
						<button type="submit" class="btn green">Excel</button>
					</form>
				</div>
				
		</div>
		
		<div class="row">
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Last Name</th>
					<th>First Name</th>
					<th>Borrower Name</th> 
					<th>Phone</th>
					<th>Email</th>
					<th>Amount</th>
					<th>Date</th>
					<th>Expiration Date</th>					
				</tr>
				</thead>
				<tbody>
					<?php 

					$count = 0;
					$amount = 0;
						if(isset($LineofCreditData) && is_array($LineofCreditData)){ 
					
								foreach($LineofCreditData as $row){ 

									$count++;
									$amount += $row['amount'];


					?>
									<tr>
										<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['contact_last_name'];?></a></td>
										<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $row['contact_first_name'];?></a></td>
										<td><a href="<?php echo base_url();?>borrower_view/<?php echo $row['borrower_id'];?>"><?php echo $row['borrower_name'];?></a></td>
										<td><?php echo $row['contact_phone'];?></td>
										<td><?php echo $row['contact_email'];?></td>
										<td>$<?php echo number_format($row['amount']);?></td>
										<td><span><?php echo $row['strDate'];?></span><?php echo $row['date'];?></td>
										<td><?php echo $row['exdate'];?></td>
										
									</tr>
					<?php } }else{ ?>
							<tr>
								<td colspan="7">No data found!</td>
							</tr>
					<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<th>Total: <?php echo $count;?></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th>$<?php echo number_format($amount);?></th>
						<th></th>
						<th></th>
					</tr>
				</tfoot>		
				
			</table>
		</div>
	</div>
	
</div>
<script>


$(document).ready(function() {

    $('#table').DataTable({
    });
	
});

</script>
<style type="text/css">
	#table span {
    display:none; 
}
</style>




