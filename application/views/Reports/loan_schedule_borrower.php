<?php
error_reporting(0);
$loan_status_option = $this->config->item('loan_status_option');
// echo "<pre>";
// print_r($loan_schedule_borrower);
// echo "</pre>";
// echo "working";
?>
<style type="text/css">
	
		.col-md-3 {
		    width: 21% !important;
		}
</style>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Borrower Loan Schedule</h1>	
					
				</div>
				
				<div class="top_download">
					<form action="<?php echo base_url();?>download_loan_schedule_borrower" method="POST">
						<input type="hidden" name = 'contact_id' value="<?php echo isset($select_c_id) ? $select_c_id : ''; ?>">
						<input type="hidden" name = 'borrower_id' value="<?php echo isset($select_b_id) ? $select_b_id : ''; ?>">
						<input type="hidden" name = 'select_box' value="<?php echo isset($select_box) ? $select_box : ''; ?>">
						<input type="hidden" name = 'contact_id_uri' value="<?php echo isset($select_c) ? $select_c : ''; ?>">


						
						<button class="btn red" type="submit">PDF</button>
					</form>
				</div>
				
		</div>
		
			<form id="select_borrower" method="POST" action="<?php echo base_url();?>loan_schedule_borrower">
			<div class="">
				<div class="col-md-3">
						Loan Status :  &nbsp;
						<select name="select_box" id ="select_box" class="chosen"  onchange = "select_box_report(this.value)">
							<option value="all" >Select All</option>
							<!-- <option value="6" <?php if(isset($select_box)){ if($select_box == 6){ echo 'selected'; } } ?>>Term sheet</option> -->
							<option value="1" <?php if(isset($select_box)){ if($select_box == 1){ echo 'selected'; } } ?>>Pipeline</option>
							<option value="2" <?php if(isset($select_box)){ if($select_box == 2){ echo 'selected'; } } ?>>Active</option>
							<!-- <option value="3" <?php if(isset($select_box)){ if($select_box == 3){ echo 'selected'; } } ?>>Paid Off</option> -->
							<option value="4" <?php if(isset($select_box)){ if($select_box == 4){ echo 'selected'; } } ?>>Cancelled</option>
							<option value="5" <?php if(isset($select_box)){ if($select_box == 5){ echo 'selected'; } } ?>>Brokered</option>
						</select>
				</div>
				<div class="col-md-3">
			
				Contact Name:  &nbsp;
				<select class="chosen" name="contact_id" onchange = "select_box_report(this.value)">
					<option value="all">Select All</option>
					<?php


					
						foreach($fetch_all_contact as $row)
						{
							if(isset($select_c_id)){
							
								if($select_c_id == $row->contact_id){
									$selected = 'selected = "selected" ';
								}



								else{$selected = '';}
							}

               					else if($select_c == $row->contact_id){
									$selected = 'selected = "selected" ';
								}


							else{$selected = '';}
							echo '<option '.$selected.' value="'.$row->contact_id.'" >'.$row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname.'</option>';
						}
					?>
				</select>
				</div>
				<div class="col-md-3">
				Borrower Name:  &nbsp;
							<select name="borrower_id" onchange="select_borrower_report(this.value)" class="chosen">
								<option value='all'>Select All</option>
								<?php
								foreach($all_investor as $row)
								{
									?>
									<option value="<?php echo $row->id;?>" <?php if(isset($select_b_id)){  if($select_b_id == $row->id){ echo 'selected'; } }?> ><?php echo $row->b_name;?></option>
									<?php
								}
								?>
							</select>
				</div>
				
			</div>
		</form>
		
		<div class="row"><br></div>
		<!---
			<div class="row">
				<div class="borrower_title"><h3>&nbsp;&nbsp;Borrower name</h3></div>
			</div>
			-->
			<div class="rc_class">
				<table id="table22" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
					<thead>
					<tr>
						<th>Contact Name</th>
						<th>Borrower Name</th>
						<th>Street Address</th>
						<th>Unit #</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
						<!--<th>TaliMar Loan #</th>
						<th>Servicer #</th>-->
						<th>Loan Amount</th>
						<th>Position</th>
						<th>Rate</th>
						<!--<th>Loan Type</th>-->
						<th>Loan Status</th>
			
						
						
						<!--
						<th>Closing Date</th>
						<th>Payoff Date</th>-->
					</tr>
					
					</thead>
					<tbody>
					<?php 
					$key				= 0;
					$total_loan_amount	= 0;
					
					
					$arr  = $loan_schedule_borrower;
					// $sort = array();
					// foreach($arr as $k=>$v) {
						// $sort['close_date'][$k] = $v['close_date'];
					// }

					 //array_multisort($sort['close_date'], SORT_S, $arr);
				//	array_multisort($sort['loan_status'], $arr);

					foreach($arr as $key => $row)
					{
						
						if($row['position'] == '1'){
							$loan_position = '1st';
						}else if($row['position'] == '2'){
							$loan_position = '2nd';
						}else if($row['position'] == '3'){
							$loan_position = '3rd';
						}else if($row['position'] == '4'){
							$loan_position = '4th';
						}

						
						if($row['loan_status'] =='4'){

							$status='N/A';
						}else{
						$status=$row['fci'];
						}



					if($row['loan_status'] =='3' || $row['loan_status'] =='5')
					{
						
						$date=date('m-d-Y',strtotime($row['payoff_date']));
						
						}
						else{

						$date='N/A';
						
						}
						if(empty($row['close_date']))
					    {
						$close_date='';
						}else{
						$close_date=date('m-d-Y',strtotime($row['close_date']));
						
						}
						

						
					?>
							<tr>
								<td><a href ="<?php echo base_url().'viewcontact/'.$row['contact_id']; ?>"><?php echo $row['b_contact_name']; ?></a></td>
								<td><a href="<?php echo base_url().'borrower_view/'.$row['b_id'];?>"><?php echo $row['borrower_name']; ?></a></td>
								<td><a href ="<?php echo base_url().'load_data/'.$row['loan_id']; ?>"><?php echo $row['property_address']; ?></a></td>
								<td><?php echo $row['unit']; ?></td>

								<td><?php echo $row['city']; ?></td>
								<td><?php echo $row['state']; ?></td>
								<td><?php echo $row['zip']; ?></td>
								
									
									<!--<?php echo $row['talimar_loan']; ?></a>
								</td>-->
								
								<!--<td><?php echo $status; ?></td>-->
								<td><a href ="<?php echo base_url().'load_data/'.$row['loan_id']; ?>#loan_terms"><?php echo '$'.number_format($row['loan_amount']); ?></a></td>
								<td><?php echo $loan_position; ?></td>
								<td><?php echo number_format($row['intrest_rate'],3); ?>%</td>
								<td><?php echo $loan_status_option[$row['loan_status']]; ?></td>
					
								
								<!--<td><?php echo $row['loan_type']; ?></td>
							
		-->
								
								<!--
								
								<td><?php echo $close_date; ?></td>
								<td><?php echo $date; ?></td>-->
							</tr>
					<?php 
					$total_loan_amount = $total_loan_amount + $loan_schedule_borrower[$key]['loan_amount'];
					$key++;
					} ?>
					</tbody>
					<tfoot>
						<tr>
							<th>Total</th>
							<th><?php echo $key; ?></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th>$<?php echo number_format($total_loan_amount,2); ?></th>
							<th colspan="10"></th>
						</tr>
					</tfoot>
				</table>
		
		
			
	</div>
	</div>
	<!-- END CONTENT -->
</div>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
<style>

.chosen-drop {
    width: 210px !important;
    margin-left: 0px !important;
}	
.row.borrower_data_row div {
    width: 95% !important;
}
.chosen-container{
	display: block !important;
}
.chosen-container.chosen-container-single {
    width: 210px !important;
}
.chosen-container-single .chosen-single {
    padding: 4px 0px 26px 6px !important;
    border-radius: 4px !important;
    border: 1px solid #e1e1e1 !important;
}
div#table22_filter {
    float: right;
}

</style>
<script>

$('.chosen').chosen();
//$('.chosen11').chosen();

function select_borrower_report(id)
{
	document.getElementById('select_borrower').submit();
}


function select_box_report(sel)
{
	document.getElementById('select_borrower').submit();
}
$(document).ready(function() {
    $('#table22').DataTable({
		"aaSorting": [ ],
	});
	
} );
</script>

