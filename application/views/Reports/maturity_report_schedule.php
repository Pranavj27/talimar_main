<?php
error_reporting(0);
// echo "<pre>";
// print_r($maturity_report_data);
// echo "</pre>";
?>
<style>
.rc_class{
	padding: 10px 0px;
}
</style>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Maturity Report Schedule  </h1>	
					
				</div>
				<div class="top_download">
					<a href="<?php echo base_url();?>download_maturity_reports">
					<button  class="btn blue">PDF</button>
					</a>
				</div>
		</div>
		<div class="rc_class">
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						<!--<th>Loan Number</th>
						<th>FCI#</th>
						<th>Unit</th>
						<th>City</th>
						<th>St </th>
						<th>Zip</th>
						<th>Origination<br>Date</th>-->
						
						<th>Property Address</th>
						<th>Servicer #</th>
						<th>Borrower Name</th>
						<th>Contact Name</th>
						<th>Contact Phone</th>
						<th>Contact E-Mail</th>
						<th>Loan Amount</th>
						<th>Maturity Date</th>
						<th>Days to Maturity</th>
						<th>Fee</th>
						
					</tr>
				</thead>
				<tbody>
				  <?php
				  $key = 0;
				  if(count($maturity_report_data)>0){
					  
					$total_balance 	= 0;  
					$total_fees		=0;
				  foreach($maturity_report_data as $row){
					  
					  $total_balance = $total_balance + $maturity_report_data[$key]['loan_amount'];
					  
				  ?>
						<tr>
							<!--<td>
								<a href="<?php echo base_url().'load_data/'.$maturity_report_data[$key]['loan_id']; ?>">
								<?php echo $maturity_report_data[$key]['talimar_loan'];?></a>
							</td>
							<td><?php echo $maturity_report_data[$key]['fci'];?></td>
							<td><?php echo $maturity_report_data[$key]['unit'];?></td>
							<td><?php echo $maturity_report_data[$key]['city'];?></td>
							<td><?php echo $maturity_report_data[$key]['square_feet'];?></td>
							<td><?php echo $maturity_report_data[$key]['zip'];?></td>
							<td><?php echo $maturity_report_data[$key]['loan_document_date'];?></td>-->
							
							<td><a href="<?php echo base_url().'load_data/'.$maturity_report_data[$key]['loan_id']; ?>"><?php echo $maturity_report_data[$key]['property_address'];?></a></td>
							<td><?php echo $maturity_report_data[$key]['fci'];?></td>
							<td><?php echo $maturity_report_data[$key]['borrower_name'];?></td>
							<td><?php echo $maturity_report_data[$key]['b_contact_name'];?></td>
							<td><?php echo $maturity_report_data[$key]['b_contact_phone'];?></td>
							<td><?php echo $maturity_report_data[$key]['b_contact_email'];?></td>
							<td><?php echo '$'.number_format($maturity_report_data[$key]['loan_amount']);?></td>
							
							<td><?php echo $maturity_report_data[$key]['maturity_date'];?></td>
							

							
							<?php
							// $maturity = $maturity_report_data[$key]['maturity_date'];
							// $myDateTime = DateTime::createFromFormat('m-d-Y', $maturity);
							// $newDateString = $myDateTime->format('d-m-Y');
							
								// $from=date_create(date('d-m-Y'));
								// $to=date_create(date('d-m-Y',strtotime($newDateString)));
								// $diff=date_diff($from,$to);
								// print_r($diff);
								// echo $diff->format('%R%a days');
							?>
							 <td><?php echo $maturity_report_data[$key]['duration']+1;?></td>

							<?php if($maturity_report_data[$key]['extention_option'] == 1){

								$fees = ($maturity_report_data[$key]['extention_percent_amount'] / 100) * $maturity_report_data[$key]['loan_amount'];
							}else{
								$fees = '0';
							}
							$total_fees 	 = $total_fees + $fees;
							?>

							 <td>$<?php echo number_format($fees,2);?></td>
							
						</tr>
					<?php
					$key++;
					
					
				  }
				  }
					?>
				</tbody>
				<tfoot>
					<tr>
						<th>Total: <?php echo $key;?></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th>$<?php echo number_format($total_balance);?></th>
						<th></th>
						<th></th>
						<th>$<?php echo number_format($total_fees);?></th>
					</tr>
				</tfoot>
			</table>
			
	</div>
	</div>
	<!-- END CONTENT -->
</div>
<script src="<?php echo base_url()?>assets/extra_css_js/datatable/jquery.dataTables.min.js"></script>

<script>
$(document).ready(function() {
    // $('.table').DataTable();
      $('.table').DataTable({
		"aaSorting": [ 7, "asc" ]
	});
	
} );
</script>