<?php

error_reporting(0);
$investor_type_option = $this->config->item('investor_type_option');
// echo "<pre>";

// print_r($investor_data);

// echo "</pre>";

/* $active_total_loan_amount 			= isset($fetch_active_loan) ? $fetch_active_loan['total_loan_amount'] : 0;
echo '<pre>';
print_r($fetch_active_loan);
echo '</pre>'; */
?>

<div class="page-content-wrapper">

	<div class="page-content responsive">

		<div class="page-head">

				<!-- BEGIN PAGE TITLE -->

				<div class="page-title">

					<h1> &nbsp; Master Lender Schedule  </h1>	

					

				</div>
				
				<div class="top_download">
					<!--
					<a href="<?php echo base_url();?>download_master_lender_excel">

					<button  class="btn blue">Excel Download</button>

					</a>
					-->
					<form method="POST" action="<?php echo base_url();?>download_master_lender_pdf">
					<input type="hidden" name="select_box" value="<?php echo isset($select_box) ? $select_box : ''; ?>" >
					<button  type="submit" class="btn blue">PDF</button>

					</form>
					<!--<a href="<?php echo base_url();?>download_master_lender_pdf">
					<button class="btn blue">Download</button>
					</a>-->
					

				</div>
				

		</div>
		<!-------------------------------Error message show ------------------->
		<?php if($this->session->flashdata('success')!=''){  ?>
			<div id='success'><i class='fa fa-thumbs-o-up'></i><?=$this->session->flashdata('success');?></div><?php } ?>
				 
		<?php if($this->session->flashdata('error')!=''){  ?>
			<div id='error'><i class='fa fa-thumbs-o-down'></i><?=$this->session->flashdata('error');?></div><?php } ?>
		<!-------------------------------End Error message show ------------------->
		
		<div class="row">
			<div class="talimar_no_dropdowns">
				<form method="POST" action="<?php echo base_url().'master_investor_schedule'?>">
					<div class="float-direction-margin">
						<input type="submit" class="btn blue" value="Filter">
					</div>
					<div class="float-direction-left">
						Lender Status: <br>
						<select name="select_box">
							<option <?php  if($select_box == 1){ echo 'selected'; } ?> value="1" >Select All</option>
							<option value="2" <?php  if($select_box == 2){ echo 'selected'; } ?>>Active Lenders</option>
							<option value="3" <?php  if($select_box == 3){ echo 'selected'; } ?>>Non Active lenders</option>
						</select>
					</div>
					
					
				</form>
			</div>
		</div>
		
		<div class="rc_class">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >

				<thead>

					<tr>

						<!--<th>Id</th>-->
						<th>Lender Name</th>
						<th>Total Funded</th>
						<th>Active Balance</th>
						<th>Avg. $ Per Loan</th>
						<th>Lender Type</th>
						<!--<th>TaliMar #</th>
						<th>FCI Account #</th>-->
						<th>Contact Name </th>
						<th>Phone</th>
						<th>E-Mail</th>
						

					</tr>

				</thead>

				<tbody>

				 <?php
				
				foreach($inv_contact_name as $lender_id => $contact){
					if($lender_id){
						
						$all[$lender_id] = array_values(array_unique(($contact)));
					}
				}
				
				 foreach($investor_dataa as $row_data)
				 {	
					/* echo'<pre>';
					print_r($row_data);
					echo'</pre>'; */
					 ?>
					 <tr>
					 <!--<td><?php echo $row_data['id'];?></td>-->
					 <td><a href="<?php echo base_url().'investor_data/'.$row_data['id'];?>"><?php echo $row_data['name'];?></a></td>
					 <!--<td><?php echo $status[$row_data['id']];?></td>-->
					 <td><?php echo '$'.number_format($total_loan_amount[$row_data['id']]);?></td>
					 <td><?php echo '$'.number_format($sum_investment[$row_data['id']]);?></td>
					
					<td><?php echo '$'.number_format($sum_investment[$row_data['id']] / $count_loan[$row_data['id']]);?></td>
					
					 <td><?php echo $investor_type_option[$row_data['investor_type']];?></td>
					<!-- <td><?php echo $row_data['talimar_lender'];?></td>
					 
					 <td><?php echo $row_data['fci_acct'];?></td>-->
					 
					 <td><?php echo $contact_name[$row_data['id']];?></td>
					 <td><?php echo $contact_phone[$row_data['id']];?></td>
					 <td><?php echo $contact_email[$row_data['id']];?></td>
					 
					 
					 </tr>
					 <?php
				 }
				 ?>

				</tbody>

			</table>

			

	</div>

	</div>

	<!-- END CONTENT -->

</div>
<script>
$(document).ready(function() {
    $('#table').DataTable({
		
		 "aaSorting": [[ 1, "desc"]]	 
		 
	});
});
</script>
<style>
	.talimar_no_dropdowns {
		float: left;
	
	}
</style>