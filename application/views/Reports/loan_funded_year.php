<?php

error_reporting(0);
$investor_type_option = $this->config->item('investor_type_option');
// echo "<pre>";

// print_r($loan_funded_year);

// echo "</pre>";

?>
<div class="page-content-wrapper">

	<div class="page-content responsive">

		<div class="page-head">

				<!-- BEGIN PAGE TITLE -->

				<div class="page-title">

					<h1> &nbsp; Loans Funded by Year  </h1>	

					

				</div>

				<div class="top_download">
					<!--
					<a href="<?php echo base_url();?>download_trust_deed_excel">

					<button  class="btn blue">Excel Download</button>

					</a>
					-->
					<div class="talimar_no_dropdowns">
					<form method="POST" action ="<?php echo base_url().'loan_funded_by_year';?>" id="loan_funded_year_form">
					Select Year:
						<select onchange="loan_funded_year_form()" name="select_year" class="selectpicker" data-live-search="true">
						<option></option>
						<?php
						
						for($i=2009;$i<=2020;$i++)
						{
							echo '<option value="'.$i.'">'.$i.'</option>';
						}
						?>
						</select>
					</form>
					
					<a href="<?php echo base_url();?>loan_funded_pdf/<?php echo $select_year;?>">

					<button  class="btn blue">PDF</button>

					</a>
					</div>
					
					

				</div>

		</div>

		<div class="rc_class">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >

				<thead>

					<tr>
						<th>Date Funded</th>
						<th>Borrower Name</th>
						<th>Talimar #</th>
						<!--<th>FCI #</th>-->
						<th>Loan<br>Amount</th>
						<th>%<br>Intrest Rate</th>
						<th>Loan to Value</th>
						<th>#<br>Lenders</th>
						<th>Multi-<br>Lenders</th>
						<th>Property Address</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
						<th>Loan<br>Status</th>
					</tr>

				</thead>
					
				<tbody>
					<?php
					$number 				= 0;
					$total_loan_amount 		= 0;
					$total_loan_to_value	= 0;
					$total_count_lender 	= 0;
					
					if(isset($loan_funded_year)){
					
						foreach($loan_funded_year as $row)
						{
							
							$number++;
							$total_loan_amount = $total_loan_amount + $row['loan_amount'];
							$total_loan_to_value = $total_loan_to_value + $row['arv'];
							$total_count_lender = $total_count_lender + $row['count_lender'];
							
							$funding_date = $row['funding_date'] ? date('m-d-Y',strtotime($row['funding_date'])) : '';
							
							?>
							
							<tr>
							<td><?php echo $funding_date; ?></td>
							<td><?php echo $row['borrower'];?></td>
							<td>
								<a href ="<?php echo base_url().'load_data/'.$row['loan_id']; ?>">
								<?php echo $row['talimar_loan'];?></a></td>
							<!--<td><?php echo $row['fci'];?></td>-->
							<td><?php echo '$'.number_format($row['loan_amount']);?></td>
							<td><?php echo number_format($row['intrest_rate'],3) .'%';?></td>
							<td><?php echo '$'.number_format($row['arv']);?></td>
							<td><?php echo $row['count_lender'];?></td>
							<td><?php  if($row['multilender'] == '1'){echo 'YES';}else if($row['multilender'] == '0'){echo 'NO';}?></td>
							<td><?php echo $row['property_address'];?></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
							<td><?php echo $row['loan_status'];?></td>
							</tr>
							<?php
						}
					}
					?>
				</tbody>
				
				<tfoot>
					
					<tr>
						<th>Total</th>
						<th></th>
						<th><?php echo $number;?></th>
						<th><?php echo '$'.number_format($total_loan_amount); ?></th>
						<th></th>
						<th><?php echo '$'.number_format($total_loan_to_value); ?></th>
						<th><?php echo $total_count_lender; ?></th>
						<th><?php echo 'SL : ' . $total_single_lender; ?></th>
						<th colspan="5"></th>
					</tr><tr>
						<th>Average</th>
						<th></th>
						<th></th>
						<th><?php echo '$'.number_format($total_loan_amount/$number); ?></th>
						<th></th>
						<th><?php echo '$'.number_format($total_loan_to_value/$number); ?></th>
						<th><?php echo round($total_count_lender/$number,2); ?></th>
						<th><?php echo 'ML : ' . $total_multi_lender; ?></th>
						<th colspan="5"></th>
					</tr>
					
				</tfoot>

			</table>

			

	</div>

	</div>

	<!-- END CONTENT -->

</div>
<!--
<script src="//code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
-->
<script>
$(document).ready(function() {
    $('#table').DataTable();
} );

function loan_funded_year_form()
{
	$('#loan_funded_year_form').submit();
}
</script>