<?php
error_reporting(0);
$loan_status_option 			= $this->config->item('loan_status_option');
$serviceing_sub_agent_option 	= $this->config->item('serviceing_sub_agent_option');
$serviceing_condition_option 	= $this->config->item('serviceing_condition_option');

?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title"><h1>&nbsp;&nbsp;Trust Deed Schedule by Contact</h1></div>
				
				<div class="top_download">
				<form  method="POST" action="<?php echo base_url().'download_trust_deed_lender';?>">
					<input type="hidden" name="investor_id" value="<?php echo isset($select_investor_id) ? $select_investor_id : ''; ?>">
					<input type="hidden" name="contact_id" value="<?php echo isset($contact_id) ? $contact_id : ''; ?>">
					<input type="hidden" name="select_box" value="<?php echo isset($select_box) ? $select_box : ''; ?>">
					<input type="hidden" name="contact_id_uri" value="<?php echo isset($contact_id_uri) ? $contact_id_uri : ''; ?>">
					<button  class="btn blue" type="submit">PDF</button>
				</form>
				</div>
				
		</div>
		<div class="row">
			<form id="select_investor" method="POST" action="<?php echo base_url();?>loan_schedule_investor">
			
			
			<div class="col-md-3">
				Contact Name :  &nbsp;
				
					<!--<select name="investor_id[]" id ="investor_id" class="selectpicker" data-live-search="true"  multiple>-->
					<select name="contact_id" id ="contact_id" class="chosen" onchange = "select_investor_report(this.value)">
						<option value="">Select One</option>
						<?php 
							foreach($all_lender_contact as $row)
							{
						?>
						<option value="<?php echo $row->contact_id;?>" <?php if(isset($contact_id)){  if($contact_id == $row->contact_id){ echo 'selected'; } }?> ><?php echo $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname; ?></option>
						<?php
						}
						?>
					</select>
					<span style="color: red" id="contact_id_error"></span>
			</div>
				
			
			<div class="col-md-3">
				Lender Name :  &nbsp;
				<!--<select name="investor_id[]" id ="investor_id" class="selectpicker" data-live-search="true"  multiple>-->
					<select name="investor_id" id ="investor_id" class="chosen" onchange = "select_investor_report(this.value)">
						<option value=''>Select All</option>
						<?php 
							foreach($all_investor as $row)
							{
								?>
								<option value="<?php echo $row->id; ?>" <?php if(isset($select_investor_id)){ if($select_investor_id == $row->id ){ echo 'selected'; } } ?> ><?php echo $row->name; ?></option>
								<?php
							}
						?>
					</select>
					
					
			</div>
		
			<div class="col-md-3">
				Loan Status :  &nbsp;
				<!--<select name="investor_id[]" id ="investor_id" class="selectpicker" data-live-search="true"  multiple>-->
				<select name="select_box" id ="select_box" class="form-control"  onchange = "select_investor_report(this.value)">
					<option value=""  >Select All</option>
					<option value="2" <?php if(isset($select_box)){ if($select_box == 2){ echo 'selected'; } } ?> >Active</option>
					<option value="3" <?php if(isset($select_box)){ if($select_box == 3){ echo 'selected'; } } ?> >Paid Off</option>
					<option value="1" <?php if(isset($select_box)){ if($select_box == 1){ echo 'selected'; } } ?> >Pipeline </option>
									
				</select>
			</div>
		
			</form>
		
		</div>
		
		
		<!---
			<div class="row">
				<div class="borrower_title"><h3>&nbsp;&nbsp;Borrower name</h3></div>
			</div>
			-->
			<div class="rc_class">
				<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
					<thead>
						<tr>
							<th>Lender Name</th>
							<th>Property Address</th>
							<th>Unit #</th>
							<th>City</th>
							<th>State</th>
							<th>Zip</th>
							<th>Loan<br>Condition</th>
							<th>Loan<br>Status</th>
							
							<th>Servicer #</th>
							<th>Loan<br>Amount</th>
							<th>$<br>Interest</th>
							
							<th>%<br>Interest</th>
							<th>Lender<br>Rate</th>
							<th>Monthly<br>Payment</th>
							<th>Loan<br>Term</th>
							<th>Loan <br>Servicer</th>
							<!--
							<th>Available</th>
							
							<th>Monthly<br>Payment</th>
							<th>Address</th>
							<th>City</th>
							<th>State</th>
							<th>Zip</th>
							-->
							
						</tr>
					
					</thead>
					<tbody>
							<?php
							$key = 0;
							$number = 0;
							$total_loan_amount 			= 0;
							$total_investment 			= 0;
							$total_available 			= 0;
						//	$total_percent_yield 		= 0;
							$total_investment_percent 	= 0;
							$total_term 				= 0;
						//	$total_lender_payment		= 0;
							$lender_rate_total			= 0;
							$total_monthly_payment		= 0;
					
			
							foreach($loan_schedule_investor as $row)
							{
							$total_investment 		= $total_investment + $loan_schedule_investor[$key]["investment"];
							$total_loan_amount 		= $total_loan_amount + $loan_schedule_investor[$key]["loan_amount"];
							//$total_percent_yield 	+= $loan_schedule_investor[$key]["percent_yield"];
							$total_term				= $total_term + $loan_schedule_investor[$key]["term"];
							
											
							$cal_invest_perxcent = ($loan_schedule_investor[$key]["investment"]/$loan_schedule_investor[$key]["loan_amount"])*100;
							
							$total_investment_percent += $cal_invest_perxcent;
							
						//	$total_lender_payment += $loan_schedule_investor[$key]['lender_payment'];


						if($loan_schedule_investor[$key]["servicing_lender_rate"] == 'NaN' || $loan_schedule_investor[$key]["servicing_lender_rate"] == '' || $loan_schedule_investor[$key]["servicing_lender_rate"] == '0.00'){

							if($loan_schedule_investor[$key]["ass_inves_yield_percent"]>0){

							$lender_ratessss = $loan_schedule_investor[$key]["ass_inves_yield_percent"];
						}}else{

						   $lender_ratessss = $loan_schedule_investor[$key]["servicing_lender_rate"];
						}

						$monthly_payment = (($lender_ratessss/100) * $loan_schedule_investor[$key]['investment']) / 12;
						
						$lender_rate_total    +=$lender_ratessss;
						$total_monthly_payment +=$monthly_payment;

						?>
						<tr>	
							<td><a href ="<?php echo base_url().'investor_view/'.$loan_schedule_investor[$key]['lender_name_new']; ?>"><?php echo $loan_schedule_investor[$key]['investor_name'];?></a></td>
						<td><a href ="<?php echo base_url().'load_data/'.$loan_schedule_investor[$key]['loan_id']; ?>"><?php echo $loan_schedule_investor[$key]['property_address'];?></a></td>

							<td><?php echo $loan_schedule_investor[$key]['unit'];?></td>
							<td><?php echo $loan_schedule_investor[$key]['city'];?></td>
							<td><?php echo $loan_schedule_investor[$key]['state'];?></td>
							<td><?php echo $loan_schedule_investor[$key]['zip'];?></td>
							
<!--
							<td><?php echo $loan_schedule_investor[$key]['borrower'];?></td>
							
							<td><a href ="<?php echo base_url().'load_data/'.$loan_schedule_investor[$key]['loan_id']; ?>"><?php echo $loan_schedule_investor[$key]['talimar_loan'];?></a></td>
							-->

							<td><?php echo ($loan_schedule_investor[$key]['loan_status'] == 2) ? $serviceing_condition_option[$loan_schedule_investor[$key]['loan_condition']] : 'N/A'; ?></td>
							
							<td><?php echo $loan_status_option[$loan_schedule_investor[$key]['loan_status']];?></td>
							
							
							<td>
								<a href ="<?php echo base_url().'load_data/'.$loan_schedule_investor[$key]['loan_id']; ?>">
								<?php echo $loan_schedule_investor[$key]['fci'];?></a>
							</td>
							<td><?php echo '$'.number_format($loan_schedule_investor[$key]['loan_amount']);?></td>
							<td><?php echo '$'.number_format($loan_schedule_investor[$key]['investment']);?></td>
						
							<td><?php echo number_format($cal_invest_perxcent ,2);?>%</td>
							
							
							
							<td><?php echo number_format($lender_ratessss,3); ?>%</td>


							
							
							<!--
							<td>$<?php echo number_format($loan_schedule_investor[$key]['percent_yield_dollar'],2); ?></td>
							-->
							<td>$<?php echo number_format($monthly_payment,2); ?></td>
							<td><?php echo $loan_schedule_investor[$key]['term']; ?></td>
							
							
							<!--<td><?php echo '$'.number_format($loan_schedule_investor[$key]['available']);?></td>
							
							<td><?php echo '$'.number_format($loan_schedule_investor[$key]['payment_amount']);?></td>
					-->
							<td><?php echo $loan_schedule_investor[$key]['vendor_name'];?></td>
						</tr>
						<?php
						$number++;
						$key++;
						

						}
						?>
					</tbody>	
					</tbody>
					<tfoot>
						<tr>
							<th>Total</th>
							<th><?php echo $number; ?></th>
							<th ></th>
							<th ></th>
							<th ></th>
							<th ></th>
							<th></th>
							<th></th>
							<th></th>
						
				
							<th>$<?php echo number_format($total_loan_amount); ?></th>
							<th>$<?php echo number_format($total_investment); ?></th>
							<th></th>
							<th></th>
							<th>$<?php echo number_format($total_monthly_payment,2);?></th>
							<th></th>
							<th></th>
						</tr>
						<tr>
							<th>Average</th>
							<th colspan="10"></th>
							<th><?php echo number_format($total_investment/$total_loan_amount*100,2); ?>%</th>
							<th><?php echo number_format(($total_monthly_payment*12)/$total_investment*100,2); ?>%</th>
							<th></th>
							<th ><?php echo number_format($total_term/$key,0); ?></th>
							<th></th>
						</tr>
					</tfoot>
				</table>
		
				<?php
				if($select_investor_id)
				{
				?>
				
				<!--<table class="half_table_section">	
					<tbody>
					<tr>		
						<th></th>
						<th># of Loans</th>
						<th>Average Yield</th>
						<!--<th>Loan Amount</th>
						<th>$ Lender</th>
						<th>% Interest</th>
					</tr>-->										
					<!--
					<tr>						
						<td>Total Portfolio</td>	
						<td><?php echo $total_avg_yield_lender['count_pipeline']; ?></td>
						<td>
							<?php
							echo number_format($total_avg_yield_lender['total_yield_pipeline'] / $total_avg_yield_lender['count_pipeline'],2).'%';
							?>
						</td>	
						<td>$<?php echo number_format($total_avg_yield_lender['pipeline_loan_amount']); ?></td>					
						<td>$<?php echo number_format($total_avg_yield_lender['investment_pipeline']); ?></td>						
						<td><?php echo number_format(($total_avg_yield_lender['investment_pipeline']/$total_avg_yield_lender['pipeline_loan_amount'])*100 ,2).'%'; ?></td>						
					</tr>										
					-->
					<!--<tr>						
						<td>Total Paid Off</td>	
						<td><?php echo $total_avg_yield_lender['count_paidoff']; ?></td>						
						<td>
							<?php
							echo number_format($total_avg_yield_lender['total_yield_paidoff'] / $total_avg_yield_lender['count_paidoff'],2).'%';
							?>
						</td>	
						<!--<td>$<?php echo number_format($total_avg_yield_lender['paidoff_loan_Amount']); ?></td>	-->					
						<!--<td>$<?php echo number_format($total_avg_yield_lender['investment_paidoff']); ?></td>						
						<td><?php echo number_format(($total_avg_yield_lender['investment_paidoff']/$total_avg_yield_lender['paidoff_loan_Amount'])*100 ,2).'%'; ?></td>						
					</tr>										
					
					<tr>						
						<td>Total Active</td>	
						<td><?php echo $total_avg_yield_lender['count_active']; ?></td>						
						<td>
							<?php
							echo number_format($total_avg_yield_lender['total_yield_active'] / $total_avg_yield_lender['count_active'],2).'%';
							?>
						</td>
						<!--<td>$<?php echo number_format($total_avg_yield_lender['active_loan_amount']); ?></td>	-->					
						<!--<td>$<?php echo number_format($total_avg_yield_lender['investment_active']); ?></td>						
						<td><?php echo number_format(($total_avg_yield_lender['investment_active']/$total_avg_yield_lender['active_loan_amount'])*100 ,2).'%'; ?></td>
					</tr>  
					
					<tr>
						<td>Total Portfolio </td>
						<td><?php echo $total_avg_yield_lender['count_active'] + $total_avg_yield_lender['count_paidoff']; ?></td>
						<td></td>

						<td><?php echo '$'.number_format($total_avg_yield_lender['investment_active'] + $total_avg_yield_lender['investment_paidoff']);?></td>
						<td></td>
					</tr>
					
					</tbody>		
				</table>-->
				
				<?php
				}
				?>
			
	</div>
	</div>
	<!-- END CONTENT -->
</div>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
<style>

.chosen-drop {
    width: 230px !important;
    margin-left: 0px !important;
}	
.row.borrower_data_row div {
    width: 96% !important;
}
.chosen-container{
	display: block !important;
}
.chosen-container.chosen-container-single {
    width: 230px !important;
}
.chosen-container-single .chosen-single {
    padding: 4px 0px 26px 6px !important;
    border-radius: 4px !important;
    border: 1px solid #e1e1e1 !important;
}

</style>
<script>

$('.chosen').chosen();

function select_investor_report(sel)
{
	var contact_id=$("#contact_id").val()
	if(contact_id==""){
		$("#contact_id_error").html("Please Select Contact First");
	}else{
		$("#contact_id_error").html("");
		document.getElementById('select_investor').submit();
	}
}

$(document).ready(function() {
	 
    $('#table').DataTable({
		"aaSorting": [ 7, "asc" ]
	});
	
});

</script>

