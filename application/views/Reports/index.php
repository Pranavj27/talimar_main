<?php
/* echo "<pre>";
print_r($fetch_pipeline_data);
echo "</pre>"; */

$closing_status = $this->config->item('closing_status_option');
$term_yes = $this->config->item('responsibility');

error_reporting(0);
?>
<style>
  #table{


  
   table-layout: fixed;
}
span.hidden_td_span {
    font-size: 0.1px !important;
}
</style>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Pipeline Loan Schedule </h1>	
					
				</div>
			<div class="top_download">
				<form id="term" action="<?php echo base_url().'reports/download_pdf_pipeline';?>" method="POST">	
				<input type="hidden" name="term_yes" value="<?php  echo isset($term_yess) ? $term_yess :'';?>">
				<button  class="btn blue">PDF</button>
				</form>
			</div>
		</div>

	<div class="row">
			
			<form id="term" action="<?php echo base_url().'reports';?>" method="POST">	

				<div class="col-md-2">
						Term Sheet Signed:  &nbsp;
						<select name="term_yes" class="form-control" onchange="term_sheet_filter(this.value);">
							
							<?php foreach($term_yes as $key => $row){ ?>
								<option value="<?php echo $key; ?>" <?php if($key == $term_yess){ echo 'selected';}?>><?php echo $row; ?></option>
							<?php  } ?>
						</select>
				 </div>
			</form> 
	</div> 


		<div class="rc_class">
			<table  id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th style="width:83px;">Borrower Name</th>
					<th style="width:80px;">Street<br>Address</th>					
					<th style="width:33px;">Unit #</th>					
					<th style="width:53px;">City</th>					
					<th style="width:38px;">State</th>					
					<th style="width:41px;">Zip</th>		
					<th style="width:53px;">Loan Amount</th>
					<th style="width:42px;">Rate</th>
					<th style="width:42px;">LTV</th>
					<th style="width:58px;">Close Date </th>
					<th style="width:60px;">Contact</th>
					<th style="width:65px;">Phone</th>

					<!--<th style="width:47px;">Loan #</th>-->
					<!--<th style="width:60px;">Status</th>-->	
					</tr>
				</thead>
				<tbody>
				<?php
					$key 						= 0; 
					$create_loan_amount 		= 0;
					$create_available 			= 0;
					$create_lender_fess 		= 0;
					$total_ltv 					= 0;
					$total_intrest 				= 0;
					$total_term 				= 0;
					$total_lender_fee_percent 	= 0;
				
					foreach($fetch_pipeline_data as  $row )
					{
						
						$closing_status_string = $row['closing_status'];
						// 6,5,4,3,2,1,7
						if($closing_status_string == 7)
						{
							$closing_status_string = 0;
						}
						
						$loan_amount 	= $row['loan_amount'];
						$arv 			= $row['property_arv'];
					

						// New by ss
						$underwriting_value = $row['underwriting_value'];
						$ltv = ($loan_amount/$underwriting_value) * 100;

							if(is_infinite($ltv)){

								$ltvv='0.00';
							//$total_ltv = $total_ltv + $ltvv;
							}else{

									$ltvv=$ltv;

							//	$total_ltv = $total_ltv + $ltvv;
							}


						
						$total_ltv = $total_ltv + $ltvv;


						
						
					
						$total_intrest = $total_intrest + $row['interest_rate'];
						$total_term = $total_term + $row['term_month'];
						$total_lender_fee_percent = $total_lender_fee_percent + $row['lender_fee'];
						?>
						<tr>
							<td><span class="hidden_td_span"><?php echo $closing_status_string;?></span><?php echo $row['borrower_name']; ?></td>
							<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['property_address'];?></a></td>
							<td><?php echo $row['unit'];?></td>
							<td><?php echo $row['city'];?></td>
							<td><?php echo $row['state'];?></td>
							<td><?php echo $row['zip'];?></td>
							<td>$<?php echo number_format($row['loan_amount']); ?></td>

							<td><?php echo number_format($row['interest_rate'], 3); ?>%</td>

							<?php $calculate_lender_amount = $row['loan_amount'] * ($row['lender_fee'] / 100 ); ?>
							<td><?php  echo number_format($ltvv,2).'%'; ?></td>
							<td><?php echo date('m-d-Y', strtotime($row['loan_funding_date'])); ?></td>
							<td><?php echo $row['borrower_contact_name'];?></td>
							<td><?php echo $row['phone'];?></td>
							<!--<td><?php echo $closing_status[$row['closing_status']];?></td>-->
							<!--<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['talimar_loan']; ?></a></td>-->
							
				
						</tr>
						<?php
						$create_loan_amount = $create_loan_amount + $row['loan_amount'];
						$create_available = $create_available + $row['available'];
						$create_lender_fess = $create_lender_fess + $calculate_lender_amount;
						$key++;
					}
				?>
				</tbody>
				<tfoot id="tfootdataTables">
				<tr>
					<th>Total</th>
					<th><?php echo $key; ?> <b>Loans</b></th>
					
					<th></th>
			
					
				
					<th><?php // echo number_format($create_available); ?></th>
					<th><?php // echo number_format($create_available); ?></th>
					<th><?php // echo number_format($create_available); ?></th>
					<th>$<?php echo number_format($create_loan_amount); ?></th>
					
					<th><?php // echo number_format($create_available); ?></th>
					<th><?php // echo number_format($create_available); ?></th>
					<th><?php // echo number_format($create_available); ?></th>
					<th><?php // echo number_format($create_available); ?></th>
					<th><?php // echo number_format($create_available); ?></th>
				</tr>
				<tr>
				<th>Average</th>
				<th></th>

				
				<th></th>
				<th ></th>
				<th ></th>
				<th ></th>
				<th>$<?php echo number_format($create_loan_amount/$key); ?></th>
				<th><?php echo number_format($total_intrest/$key,2); ?>%</th>
				<th><?php echo number_format($total_ltv/$key ,2); ?>%</th>
			
				<th ></th>
				<th ></th>
				<th ></th>
				
				</tr>
				</tfoot>
				
			</table>
			</div>
			
	</div>
	<!-- END CONTENT -->
</div>

<script>


$(document).ready(function() {
	
    $('#table').DataTable({
     
    	// order:[ [0,'asc'], [2,'asc'] ,[3,'asc']],
    	order:[ [0,'desc']],
	});
})



function term_sheet_filter(){

$('form#term').submit();

}

</script>