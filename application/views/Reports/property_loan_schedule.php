<?php
error_reporting(0);
$investor_type_option = $this->config->item('investor_type_option');
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
$loan_status_option = $this->config->item('loan_status_option');
$yes_no_option 		= $this->config->item('yes_no_option');
		
$select_box_name = isset($select_box) ? '('.$loan_status_option[$select_box].')' : '' ;
$date_data = '';
if(isset($start) && isset($end))
{
	$date_data = '('.$start.' to '.$end.')';
}

// echo "<pre>";

// print_r($status_loan);

// echo "</pre>";

?>
<!--<style>
table#table th {
    text-align: left;
    font-size: 12px; 
	
}
</style>-->
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Multi-Property Loan Schedule</h1>	
					
				</div>
				<div class="talimar_no_dropdowns">
					
					<form method="POST" action="<?php echo base_url();?>property_loan_schedule_pdf">
						<input type="hidden" name="status_loan" value="<?php echo isset($status_loan) ? $status_loan : ''; ?>" >
						<button  type="submit" class="btn blue">PDF</button>

					</form>
					
				</div>
		</div>
		
		<div class="rc_class">
			<form id="multi_property" method="POST" action="<?php echo base_url();?>reports/property_loan_schedule">
			<div class="row">
				<div class="col-md-2">
				<label>Loan Status:</label>
					<select name="select_status" class="form-control" onchange="submit_status(this.value);">
						<option value="">Select All</option>
						<option value="2" <?php if($status_loan == 2){echo 'Selected';}?>>Active</option>
						<option value="3" <?php if($status_loan == 3){echo 'Selected';}?>>Paid Off</option>
					</select>
				</div>
			</div>
			</form>
		</div>
		
		
		<div class="rc_class">
			<table id="table" class="table table-bordered table-striped table-condensed flip-content" >
				<thead>
					<tr>
	
						<th>Loan #</th>
						<th>Borrower Name</th>
						<th>Primary - Street Address</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
						<th>Loan Amount</th>
						<th>Note Rate</th>
						<th># of Properties</th>
						<th>Loan Status</th>
						
					
					</tr>
				</thead>
				<tbody>
				<?php 
				
				$total_loan_amount 	= 0;
				$total_term 		= 0;
				$total_ltv 			= 0;
				$total_intrest 		= 0;
				$count 				= 0;
				$yield 				= 0;
					if(isset($property_loan_schedulee))
					{
						foreach($property_loan_schedulee as $key => $row)
						{
							if($row['total_row'] > 1){
							?>
							<tr>
								<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['talimar_loan']; ?></a></td>
								
								<td><?php echo $row['borrower_name']; ?></td>
								<td><?php echo $row['property_address']; ?></td>
					            <td><?php echo $row['city']; ?></td>
								<td><?php echo $row['state']; ?></td>
								<td><?php echo $row['zip']; ?></td>
								<td>$<?php echo number_format($row['loan_amount']); ?></td>
								<td><?php echo number_format($row['intrest_rate'],3); ?>%</td>
								<td><?php echo $row['total_row']; ?></td>
			
								<td><?php echo $loan_status_option[$row['loan_status']]; ?></td>
								
							</tr>
							
							
							<?php
							
							$total_loan_amount 	= $total_loan_amount + $row['loan_amount'];
							$total_term 		= $total_term + $row['term'];
							
							$yield				= $yield + $row['total_row'];
							
							$count++;
							}
						}
					}
				?>
					
				</tbody>	
				<tfoot>
					<tr>
						<th>Total: <?php echo $count; ?></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th><?php echo '$'.number_format($total_loan_amount);?></th>
						<th colspan="8"></th>
					</tr>
					
					<tr>
						<th>Average:</th>
						<th  ></th>
						<th  ></th>
						<th  ></th>
						<th  ></th>
						<th  ></th>
						<th><?php echo '$'.number_format($total_loan_amount/$count); ?></th>
						
						<th colspan="7"></th> 
					</tr>
						
					
				</tfoot>
				
			</table>

			

		</div>

	</div>
	<!-- END CONTENT -->
</div>

<script>
$(document).ready(function() {
    $('#table').DataTable();
});

function submit_status(that){
	
	$('form#multi_property').submit();
}
</script>