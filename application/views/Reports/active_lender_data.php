<?php

error_reporting(0);

// echo "<pre>";

// print_r($fci_service_loan);

// echo "</pre>";

?>

<div class="page-content-wrapper">

	<div class="page-content responsive">

		<div class="page-head">

				<!-- BEGIN PAGE TITLE -->

				<div class="page-title">

					<h1> &nbsp; Lender Data </h1>	

					

				</div>

				<div class="top_download">
					<!--
					<a href="<?php echo base_url();?>download_fci_serviced_account">

					<button  class="btn blue">Excel Download</button>

					</a>
					-->
					
					
					<form action="<?php echo base_url();?>download_lender_active_data" method="POST">
						<input type="hidden" name="lender_id" value="<?php echo isset($select_l_id) ? $select_l_id : ''; ?>">
						<button  class="btn blue" type="submit">PDF</button>

					</form>
					
				</div>
				
				<div class="row">
					<form id="form_select_lender" method="POST" action="<?php echo base_url();?>active_lender_data">
					<div class="talimar_no_dropdowns">
						Lender :  &nbsp;
									<select name="lender_id" onchange="select_box_report(this.value)" class="selectpicker" data-live-search="true" >
										<option value='all'>All</option>
										<?php
										foreach($fetch_all_lender as $row)
										{
											?>
											<option value="<?php echo $row->id;?>" <?php if(isset($select_l_id)){  if($select_l_id == $row->id){ echo 'selected'; } }?> ><?php echo $row->name;?></option>
											<?php
										}
										?>
									</select>
					</div>
					</form>
				</div>
							

		</div>

		<div class="rc_class">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >

				<thead>

					<tr>
					
						<th>Lender name</th>
						
						<th>Contact Name 1</th>
						<th>Contact Phone 1</th>
						<th>Contact Email 1</th>
						<th>Contact Title 1</th>
						
						<th>Contact Name 2</th>
						<th>Contact Phone 2</th>
						<th>Contact Email 2</th>
						<th>Contact Title 2</th>
						
					</tr>

				</thead>
				
				<tbody>
				<?php
				foreach($active_loan_lenders as $key => $row)
				{
					?>
					<tr>
					
					<td><?php echo $row['lender_name'];?></td>
					<td><?php echo $row['contact_name1'];?></td>
					<td><?php echo $row['contact_phone1'];?></td>
					<td><?php echo $row['contact_email1'];?></td>
					<td><?php echo $row['contact_title1'];?></td>
					
					<td><?php echo $row['contact_name2'];?></td>
					<td><?php echo $row['contact_phone2'];?></td>
					<td><?php echo $row['contact_email2'];?></td>
					<td><?php echo $row['contact_title2'];?></td>
					
					</tr>
					<?php
				}
				?>
				</tbody>

			</table>

			

	</div>

	</div>

	<!-- END CONTENT -->

</div>

<script>
function select_box_report(sel)
{
	document.getElementById('form_select_lender').submit();
}
$(document).ready(function() {
    $('#table').DataTable({
		 "order": [[ 3, "asc" ]]
	});
} );
</script>
<style>
div#table_filter {
    display: none !important;
}
</style>