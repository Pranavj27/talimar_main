<?php

$list_months = $this->config->item('list_month');
$list_years = $this->config->item('list_year');

// echo '<pre>';
// print_r($servicing_data);
// echo '</pre>';

?>
<div class="page-content-wrapper">

	<div class="page-content">

		<div class="page-head">

				<!-- BEGIN PAGE TITLE -->

				<div class="page-title">

					<!--<h1> &nbsp; Monthly Servicing Income (<?php echo $months ? $months : date('m'); ?> | <?php echo $year ? $year : date('Y');?>)  </h1>-->	
					<h1> &nbsp; Monthly Servicing Income (Month | Year)  </h1>	

					

				</div>

				<div class="top_download">
					
					<form method="post" action="<?php echo base_url();?>download_servicing_income_monthly_yearly">

						<input type="hidden" name="month" value="<?php echo isset($months) ? $months : ''; ?>">
						<input type="hidden" name="year" value="<?php echo isset($year) ? $year : ''; ?>">

						<button  type="submit" class="btn blue">PDF</button>

					</form>
					

				</div>

		</div>

		<div class="row">
			<form  method="POST" action="<?php echo base_url()?>loan_servicing_income_monthly_yearly">
				<div class="col-md-3">
					List Month:  &nbsp;
					<select name="month" class="form-control">
						<?php foreach($list_months as $key => $row){ ?>
							<option value="<?php echo $key; ?>" <?php if($key == $months){echo 'Selected';}?>><?php echo $row; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-3">
					List Year:  &nbsp;
					<select name="year" class="form-control">
						<?php foreach($list_years as $key => $row){ ?>
							<option value="<?php echo $key; ?>"  <?php if($key == $year){echo 'Selected';}?>><?php echo $row; ?></option>
						<?php } ?>

						
					</select>
				</div>
				<div class="col-md-1">
					&nbsp;&nbsp;
					<button type="submit" class="btn blue" name="submit" style="">Filter</button>
				</div>
			</form>
		</div>

		<div class="rc_class">

			<table id="table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >

				<thead>

					<tr>
						<th>Loan #</th>
						<th>Sub Servicer </th>
						<th>Borrower Name</th>
						<th>Full Property Address</th>
						<th>Monthly Income</th>
						<th>Partial Month</th>
						<th>Actual</th>
						
					
					</tr>
				</thead>
				<tbody>
				<?php 

				$count = 0;
				$m_total = 0;
				$a_total = 0;
				foreach($servicing_data as $row){

					$count++;
					//$month = date('m', strtotime($row['payoff_date']));
					//$day = date('d', strtotime($row['payoff_date']));

					$month = date('m', strtotime($row['demand_requested_date']));
					$day = date('d', strtotime($row['demand_requested_date']));

					$loan_funding_date = date('m', strtotime($row['loan_funding_date']));
					$loan_funding_day = date('d', strtotime($row['loan_funding_date']));

					$current_month = date('m');
					$monthly_payment = number_format($row['monthly_payment'],2);

					$m_total += $monthly_payment;

						/*if($row['demand_requested_date'] !='')
						{
							
							$Received = ($monthly_payment / 30) * $day;
						}else{
							
							$Received = $monthly_payment;
						}*/

					
						$monthsss = $months ? $months : date('m');
						if($loan_funding_date == $monthsss)
						{
							
							$partial_month = 'Yes';
							$Received = (30 - $loan_funding_day) * ($monthly_payment / 30);

						}else{
							
							$partial_month = 'No';
							$Received = $monthly_payment;
						}
					$a_total += $Received;

					if($row['sub_srvicer']=='FCI Lender Services'){

								$vendor_name='FCI';
							}elseif($row['sub_srvicer']=='Del Toro Loan Servicing'){
								$vendor_name='Del Toro';
							}else{

                                  $vendor_name='';

							}

					
				 ?>
				 
					<tr>
							<td><?php echo $row['talimar_loan'];?></td>
							<td><?php echo $vendor_name;?></td>
							<td><?php echo $row['borrower_name'];?></td>
							<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['full_address'];?></a></td>
							<td>$<?php echo number_format($row['monthly_payment'],2);?></td>
							<td><?php echo $partial_month;?></td>
							<td>$<?php echo number_format($Received,2);?></td>
							
					</tr>
				<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<th><strong>Total: <?php echo $count;?></strong></th>
						<th colspan="3"></th>
						<th><strong>$<?php echo number_format($m_total,2);?></strong></th>
						<th></th>
						<th><strong>$<?php echo number_format($a_total,2);?></strong></th>
					</tr>
				</tfoot>
			</table>
	</div>
	</div>

	<!-- END CONTENT -->

</div>
<!--
<script src="//code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
-->
<script>
$(document).ready(function() {
    $('#table').DataTable({
        "order": [[ 6, "desc" ]]
    });
} );
</script>