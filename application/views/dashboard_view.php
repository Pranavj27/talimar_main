<?php
$loan_type_option = $this->config->item('loan_type_option');
$loan_status_option = $this->config->item('loan_status_option');
$closing_status_option = $this->config->item('closing_status_option');
$serviceing_reason_option = $this->config->item('serviceing_reason_option');


?>
<!-- BEGIN CONTENT -->
<style>
.rc_class{
	padding:0px;
}
.rc_class td{
	text-align:left;
}
#LoadingImage{
	position : absolute;
	top:15%;
	left:30%;
	display: block;
}
.red{

color:red;
}

tr.foot_light_blue {
    background: rgba(75, 141, 248, 0.76);
    color: white;
}
</style>
	<div class="page-container">
			<!-- BEGIN PAGE HEAD -->

		<div class="container">
					<!--<div id="LoadingImage" style="display:none">
					  <img src="<?php echo base_url() ?>assets/global/img/loder.gif"/>
					</div>-->
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1 style="margin-left:14px !important;">Portfolio Dashboard </h1>
				</div>
				


			<!-------------- PIPELINE TABLE STARTS ------------->

				<div class="row rc_class">
					<div class="col-md-12">
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead >
								<tr class="thead_dark_blue">
									<th colspan="9">Pipeline</th>
								</tr>
								<tr>
									<!--<th>Loan #</th>-->
									<th>Property Address</th>
									<th>Borrower</th>
									<th>Loan Amount</th>
									<th>Unfunded</th>
									<th>Interest Rate</th>
									<th>LTV</th>
									<th>Term</th>
									<th>Closing Status</th>
									<th>Closing Date</th>


								</tr>
							</thead>
							<tbody>

								<?php


/************/

if (isset($all_pipeline_data) && is_array($all_pipeline_data)) {

	foreach ($all_pipeline_data as $key => $row) {
		if ($all_pipeline_data[$key]['talimar_loan']) {
			$calculate_lender_points = $all_pipeline_data[$key]['loan_amount'] * ($all_pipeline_data[$key]['lender_fee'] / 100);

			$total_pipeline_available = $total_pipeline_available + $all_pipeline_data[$key]['avilable'];

			$position = $all_pipeline_data[$key]['position'];

			if ($position == '2') {

				
				$loan_to_value = ($all_pipeline_data[$key]['select_ecum'] + $all_pipeline_data[$key]['loan_amount']) / $all_pipeline_data[$key]['uv_value'] * 100;
			} else {

				$loan_to_value = ($all_pipeline_data[$key]['select_ecum'] + $all_pipeline_data[$key]['loan_amount']) / $all_pipeline_data[$key]['uv_value'] * 100;
			}
			$loan_to_valuesd = is_infinite($loan_to_value) ? 0 : $loan_to_value;
			$total_interst_rate += $all_pipeline_data[$key]['intrest_rate'];
			$total_loan_amount += $all_pipeline_data[$key]['loan_amount'];
			$total_ltv += $loan_to_valuesd;
			$total_term += $all_pipeline_data[$key]['term_month'];
			$total_avi_balance += $all_pipeline_data[$key]['avilable'];
			?>
									<tr>
										<!--<td style = "text-align: left;"></td>-->
										<td style = "text-align: left;"><a href="<?php echo base_url(); ?>load_data/<?php echo $all_pipeline_data[$key]['loan_id']; ?>"><?php echo $all_pipeline_data[$key]['property_address'] ? $all_pipeline_data[$key]['property_address'] : 'No Property Entered'; ?></a>

										</td>
										<td style = "text-align: left;" >
											<a href="<?php echo base_url(); ?>borrower_view/<?php echo $all_pipeline_data[$key]['borrower_id']; ?>"><?php echo $all_pipeline_data[$key]['borrower_name']; ?></a>
										</td>
										<td style = "text-align: left;">
											$<?php echo number_format($all_pipeline_data[$key]['loan_amount']); ?>
										</td>
										<td>$<?php echo number_format($all_pipeline_data[$key]['avilable']); ?></td>
										<td style = "text-align: left;" ><?php echo number_format($all_pipeline_data[$key]['intrest_rate'], 3) ?>%</td>


										<td style = "text-align: left;" class="valuesd_ltv">
											<?php 
											$valuesd_ltv = get_LTV_value($all_pipeline_data[$key]['talimar_loan'], $all_pipeline_data[$key]['loan_amount']);
											echo number_format($valuesd_ltv, 2);
											//echo number_format($loan_to_valuesd, 2); 
											?>% </td>
										<td style = "text-align: left;" ><?php echo $all_pipeline_data[$key]['term_month'] ?> Months</td>

										<?php
											if($all_pipeline_data[$key]['term_sheet'] == '1'){
												$closingclass = 'green';
											}else{
												$closingclass = '';
											}
										?>
										
										<td style = "text-align: left;" class="<?php echo $closingclass;?>">
											<?php echo $closing_status_option[$all_pipeline_data[$key]['closing_status']]; ?>
										</td>

										<?php 

										$funding_date = $all_pipeline_data[$key]['funding_date'] ? date('m-d-Y', strtotime($all_pipeline_data[$key]['funding_date'])) : '';
										$currentDate = date('Y-m-d');

										if($currentDate > $all_pipeline_data[$key]['funding_date']){
											$pipeclass = 'red';
										}else{
											$pipeclass = '';
										}

										?>

										<td style = "text-align: left;" class="<?php echo $pipeclass;?>">

											<?php echo $funding_date; ?>
										</td>


									<?php $key++;?>
									</tr>
										<?php }}} else {?>
									<tr>
										<td style = "text-align: left;" colspan="9">No Data Found!</td>
									</tr>
									<?php }?>
							</tbody>
							<tfoot class="foot_light_blue">
								<tr>
									<th>Total: <?php echo $key; ?></th>
									<th></th>
									<th>$<?php echo number_format($total_loan_amount); ?></th>
									<th>$<?php echo number_format($total_avi_balance); ?></th>
									<th><?php echo number_format($total_interst_rate / $key, 3); ?>%</th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>


								</tr>
								<tr>
									<th>Average:</th>
									<th></th>
									<th>$<?php echo number_format($total_loan_amount / $key); ?></th>
									<th>$<?php echo number_format($total_avi_balance / $key); ?></th>
									<th></th>
									<th><?php echo number_format($total_ltv / $key, 2); ?>%</th>
									<th><?php echo number_format($total_term / $key); ?> Months</th>
									<th></th>
									<th></th>


								</tr>
							</tfoot>

						</table>
					</div>
					</div>

					<!-------------- PIPELINE TABLE ENDS ------------->
					<!-- <div class="row rc_class">
						<div class="col-md-12" id="ajax_term_sheet">


						</div>
					</div> -->


			<?php if ($loginuser_dashboard == 1) { ?>

					<div class="row rc_class">
						<div class="col-md-12" id="active_trust_deed">

								<!------ append active_trust_deed table here ------->
						</div>
					</div>

					<div class="row rc_class">
						<div class="col-md-12" id="action_item">

								<!------ append action_item table here ------->
						</div>
					</div>


					<div class="row rc_class">
						<div class="col-md-12" id="">

							<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
								<thead >
									<tr class="foot_light_blue">
										<th colspan="3">Loan Servicing</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>

					<div class="row rc_class">

						<div class="col-md-4" id="loan_servicing">

						</div>

						<div class="col-md-4 scrolling-dashboard">
							<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
								<thead >
									<tr class="thead_dark_blue">
										<th colspan="3">Loan Holding Schedule</th>
									</tr>
									<tr>

										<th>Address</th>
										<th>Servicer Loan #</th>
										<th>Neg Time</th>
									</tr>
								</thead>
								<tbody>
									<?php
								if (isset($holding_loan) && is_array($holding_loan)) {
									foreach ($holding_loan as $boarding_val) {

									if ($boarding_val['check'] != '1' && $boarding_val['loan_closing'] == 'yes') {
										$color = 'red';
									} else {
										$color = '';
									}

									?>

									<tr>
										<td style = "text-align: left;"><a href="<?php echo base_url() . 'load_data/' . $boarding_val['loan_id']; ?>"><?php echo $boarding_val['p_address']; ?></a></td>

										<td class="<?php echo $color; ?>" style = "text-align: left;"><?php echo $boarding_val['servicer']; ?></td>
										<!--<td style = "text-align: left;">$<?php echo number_format($boarding_val['loan_amount']); ?></td>-->
										<td class="<?php echo $color; ?>" style = "text-align: left;"><?php echo $boarding_val['diff']; ?></td>

									</tr>
										<?php }} else {?>

											<td style = "text-align:center;" colspan="3">No data found!</td>
										<?php }?>

								</tbody>
							</table>
						</div>

						<div class="col-md-4" id="fci_servicing_schedule">
						</div>

						

					</div>

				<div class="row rc_class">

					<div class="col-md-4 scrolling-dashboard" id="outstanding_draw">
							<!------ append outstanding_draw table here ------->
					</div>
					
					

					
					<div class="col-md-4 scrolling-dashboard" id="ajinsurance">

					</div>

					<div class="col-md-4" id="ajax_loan_service">

					</div>
					
					
				</div>

			


				<div class="row rc_class">

					<div class="col-md-4" id="maturity_schedule">
						<!------ append maturity_schedule table here ------->
					</div>

					
					<div class="col-md-4" id="Extension_Processing">

					</div>

					<div class="col-md-4" id="default_loan">
						<!------ append a default_loan table here ------->
					</div>

					<!--<div class="col-md-4" id="ajax_loan_date_late">-->
							


				</div>

				<div class="row rc_class">

					<div class="col-md-4 scrolling-dashboard" id="upcoming_paidd">

					</div>

					<div class="col-md-4 " id="paid_offf_process">

					</div>


					<div class="col-md-4" id="loan_closing_checklist">
					
					
					</div>
					
				</div>

				<div class="row rc_class">

					<div class="col-md-4 scrolling-dashboard" id="AccruedCharges">

					</div>

				</div>

				<div class="row rc_class">
					<div class="col-md-12" id="">

						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead >
								<tr class="foot_light_blue">
									<th colspan="3">Lender Servicing</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>

				<div class="row rc_class">

						<div class="col-md-4" id="IncomingFunds">

						</div>

						<div class="col-md-4" id="titleIncomingFunds">

						</div>
						
						<div class="col-md-4 s" id="posted_trust">

						</div>
				</div>
				<div class="row rc_class">

					<div class="col-md-12" id="AssignmentProcess">

					</div>

				</div>
				<div class="row rc_class">

					<div class="col-md-4" id="portfolio_statistics">
						<!------ append portfolio_statistics table here ------->
					</div>

					<div class="col-md-4" id="ready_signture">

					</div>

					<div class="col-md-4" id="LenderStatusProccig">
					</div>
					
				</div>
				

				<div class="row rc_class">
					<div class="col-md-12" id="">

						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead >
								<tr class="foot_light_blue">
									<th colspan="3">Portfolio Servicing</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>


				<div class="row rc_class">	

					<div class="col-md-4">
							<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
								<thead class="thead_dark_blue">
								<tr>
									<th colspan="2">Active Loan Portfolio</th>
								</tr>
								</thead>

								<tbody>
									<tr>
										<td style = "text-align: left;" ># of Loans</td>
										<td style = "text-align: left;"><?php echo $active_loan_result['active_count']; ?></td>
									</tr>

									<tr>
										<td style = "text-align: left;">Active Portfolio Balance</td>
										<td style = "text-align: left;" >$<?php echo number_format($active_loan_result['total_current_balance']); ?></td>
									</tr>
									<tr>
										<td style = "text-align: left;">Original Portfolio Balance</td>
										<td style = "text-align: left;" >$<?php echo number_format($active_loan_result['total_loan_amount']); ?></td>
									</tr>
									<?php
								$total_intrest_percent = number_format($active_loan_result['total_intrest_rate'], 3);
									$total_intrest_dollar = $active_loan_result['total_loan_amount'] * ($total_intrest_percent / 1200) / $active_loan_result['active_count'];

									//$active_ltv = (($active_loan_result['total_loan_amount'] + $active_loan_result['senior_current'] ) /$active_loan_result['total_arv'])*100;

									$active_ltv = ($active_loan_result['total_loan_amount'] / $active_loan_result['total_future_value']) * 100;

									$active_ltv_total = $active_ltv / $active_loan_result['active_count'];

									/* echo'<pre>';
																	print_r($fetch_property_data);
																	echo'</pre>'; */
									/* echo $active_ltv;
																	echo'<br>';
																	echo $active_ltv_total; */

									?>
									<tr>

										<td style = "text-align: left;">Avg. Loan Amount</td>
										<td style = "text-align: left;">$<?php echo number_format($active_loan_result['total_loan_amount'] / $active_loan_result['active_count']); ?></td>
									</tr>

									<tr>
										<td style = "text-align: left;">Avg. Loan Rate</td>
										<td style = "text-align: left;" ><?php echo $total_intrest_percent; ?>%</td>
									</tr>

									<tr>
										<td style = "text-align: left;" >Avg. LTV</td>
										<td style = "text-align: left;" ><?php echo number_format($active_ltv_total*100, 2); ?>%</td>
									</tr>

									<tr>
										<td style = "text-align: left;" >Avg. Term</td>
										<td style = "text-align: left;"><?php echo number_format($active_loan_result['total_term_month'], 1) . ' Months'; ?></td>
									</tr>



									<tr>
										<td style = "text-align: left;" >Avg. Loan Spread</td>
										<td style = "text-align: left;"><?php echo $spred_val_avg; ?>%</td>
									</tr>



									<!-- 		<tr>
										<td style = "text-align: left;" >Monthly Net Servicing Income</td>
										<td style = "text-align: left;" >$<?php echo number_format($total_servicing_income); ?></td>
									</tr> -->
									<tr>
										<td style = "text-align: left;">Loans in Default #</td>
										<td style = "text-align: left;" ><?php echo $fetch_total_default; ?></td>
									</tr>

									<tr>
										<td style = "text-align: left;">Loans in Default $</td>
										<td style = "text-align: left;" ><?php echo '$' . number_format($fetch_total_amount); ?></td>
									</tr>
								</tbody>
							</table>
						</div>

						<div class="col-md-4" id="portfolio_divertification">
							<!------ append  portfolio_divertification table here ------->
						</div>						
						
						<div class="col-md-4" id="year_portfolio_statistics">

						</div>
				</div>

				<div class="row rc_class">

					<div class="col-md-4" id="construction_loan">
						<!------ append construction_loan table here ------->
					</div>

					

				</div>

				<div class="row rc_class">
					<div class="col-md-12" id="">

						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead >
								<tr class="foot_light_blue">
									<th colspan="3">Marketing</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>

				<div class="row rc_class">
		    		

					<div class="col-md-4 scrolling-dashboard" id="MarketingNew">

					</div>
					<div class="col-md-4" id="GoogleReviewRequest">

					</div>
					<div class="col-md-4 scrolling-dashboard" id="ClosingTombstone">

					</div>

					

				</div>
				<div class="row rc_class">
		    		
					<div class="col-md-4 scrolling-dashboard" id="ClosingAdvertisement">

					</div>
					<div class="col-md-4" id="SignNotPosted">

					</div>

					<div class="col-md-4" id="LoanClosingPosted">

					</div>
					
				</div>
				<div class="row rc_class">
		    		
					<div class="col-md-4" id="SocialMediaBlast">

					</div>
					<div class="col-md-4 scrolling-dashboard" id="beforeandafter">

					</div>

				</div>

				<div class="row rc_class">
					<div class="col-md-12" id="">

						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead >
								<tr class="foot_light_blue">
									<th colspan="3">TaliMar Balance Sheet</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>

				<div class="row rc_class">
					
					<div class="col-md-4" id="avaliable_capital">

					</div>

					<div class="col-md-4" id="capital_requirement">
					
					</div>

					<div class="col-md-4" id="balance_sheet_loan">

					</div>

				</div>

				<div class="row rc_class">

					<div class="col-md-4" id="OutstandingClosingFees">

					</div>

					<div class="col-md-4" id="OutstandingFunds">
						<!------ append OutstandingFunds table here ------->
					</div>
				</div>
				

				<div class="row rc_class">
					<div class="col-md-12" id="">

						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead >
								<tr class="foot_light_blue">
									<th colspan="3">Contact Swag Request</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>

				<div class="row rc_class">
					<div class="col-md-4" id="SwagRequest">

					</div>
				</div>
	

	<?php } else { ?>   <!--- for 3rd type -->
	
				<div class="row rc_class">


					<div class="col-md-4" id="maturity_schedule">
					<!------ append maturity_schedule table here ------->
					</div>


					<div class="col-md-4" id="default_loan">
						<!------ append a default_loan table here ------->
					</div>

				</div>
	<?php }?>
	
		</div>
	</div>
	

<!-- END CONTAINER -->

<script>

ajax_insurance();
function ajax_insurance()
{
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . "Home/ajax_insurance"; ?>',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#ajinsurance').append(response);
				//window.location.reload();
			}
		}
	})
}

ajax_SwagRequest();
function ajax_SwagRequest()
{
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . "Home/ajax_SwagRequestData"; ?>',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#SwagRequest').append(response);
				//window.location.reload();
			}
		}
	})
}

ajax_SignNotPosted();
function ajax_SignNotPosted()
{
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . "Home/ajax_SignNotPostedData"; ?>',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#SignNotPosted').append(response);
				//window.location.reload();
			}
		}
	})
}

ajax_GoogleReviewRequest();
function ajax_GoogleReviewRequest()
{
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . "Home/ajax_GoogleReviewRequestData"; ?>',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#GoogleReviewRequest').append(response);
				//window.location.reload();
			}
		}
	})
}

ajax_ClosingTombstone();
function ajax_ClosingTombstone()
{
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . "Home/ajax_ClosingTombstoneData"; ?>',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#ClosingTombstone').append(response);
				//window.location.reload();
			}
		}
	})
}

ajax_LoanClosingPosted();
function ajax_LoanClosingPosted()
{
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . "Home/ajax_LoanClosingPostedData"; ?>',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#LoanClosingPosted').append(response);
				//window.location.reload();
			}
		}
	})
}

ajax_SocialMediaBlast();
function ajax_SocialMediaBlast()
{
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . "Home/ajax_SocialMediaBlastData"; ?>',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#SocialMediaBlast').append(response);
				//window.location.reload();
			}
		}
	})
}


fci_servicing_schedule_ajax();
function fci_servicing_schedule_ajax()
{
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . "Home/fci_servicing_schedule_data"; ?>',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#fci_servicing_schedule').append(response);
				//window.location.reload();
			}
		}
	})
}


ajax_upcoming_paid();
function ajax_upcoming_paid()
{
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . "Home/upcoming_paid_off"; ?>',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#upcoming_paidd').append(response);
				//window.location.reload();
			}
		}
	})
}

ajax_portfolio_divertification();
function ajax_portfolio_divertification()
{
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . "Home/ajax_portfolio_divertification"; ?>',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#portfolio_divertification').append(response);
				//window.location.reload();
			}
		}
	})
}


ajax_Extension_Processing();
function ajax_Extension_Processing()
{
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . "Home/ajax_Extension_Processing"; ?>',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#Extension_Processing').append(response);
				
			}
		}
	})
}

ajax_IncomingFunds();
function ajax_IncomingFunds()
{
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . "Home/ajax_IncomingFundsdata"; ?>',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#IncomingFunds').append(response);
				
			}
		}
	})
}

ajax_titleIncomingFunds();
function ajax_titleIncomingFunds()
{
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . "Home/ajax_titleIncomingFundsdata"; ?>',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#titleIncomingFunds').append(response);
				
			}
		}
	})
}

ajax_MarketingNew();
function ajax_MarketingNew()
{
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . "Home/ajax_MarketingNewdata"; ?>',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#MarketingNew').append(response);
				
			}
		}
	})
}

ajax_ClosingAdvertisement();
function ajax_ClosingAdvertisement()
{
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . "Home/ajax_ClosingAdvertisementdata"; ?>',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#ClosingAdvertisement').append(response);
				
			}
		}
	})
}


ajax_beforeandafter();
function ajax_beforeandafter()
{
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . "Home/ajax_beforeandafterdata"; ?>',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#beforeandafter').append(response);
				
			}
		}
	})
}


ajax_default_loans();
function ajax_default_loans()
{
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . "Home/ajax_default_loans"; ?>',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#default_loan').append(response);
				//window.location.reload();
			}
		}
	})
}

ajax_loan_closing_checklist();
function ajax_loan_closing_checklist()
{
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . "Home/ajax_loanclosing_checklist"; ?>',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#loan_closing_checklist').append(response);
				//window.location.reload();
			}
		}
	})
}

ajax_AccruedCharges();
function ajax_AccruedCharges()
{
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . "Home/AccruedChargesQuery"; ?>',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#AccruedCharges').append(response);
				//window.location.reload();
			}
		}
	})
}

ajax_servicing_companies();
function ajax_servicing_companies(){

	$.ajax({

			type	: 'POST',
			url		: '<?php echo base_url() . "Home/ajax_servicing_companies"; ?>',
			data	: {},
			success	: function(response){
				if(response)
				{
					$('div#servicing_companies').append(response);
					//window.location.reload();
				}
			}
	});
}

ajax_outstanding_draw();
function ajax_outstanding_draw(){

	$.ajax({
		type 		: 'POST',
		url 		: '<?php echo base_url() . "Home/ajax_outstanding_draw" ?>',
		data 		: {},
		success 	: function(response){
			if(response)
			{
				$('div#outstanding_draw').append(response);
				//window.location.reload();
			}
		}

	});
}


ajax_construction_loan();
function ajax_construction_loan(){

	$.ajax({
				type	:	'POST',
				url		:	'<?php echo base_url() . "Home/ajax_construction_loan"; ?>',
				data	:	{},
				success	: 	function(response){
					if(response){
						$('div#construction_loan').append(response);
						//window.location.reload();
					}
				}
	});
}

ajax_OutstandingFunds();
function ajax_OutstandingFunds(){

	$.ajax({
				type	:	'POST',
				url		:	'<?php echo base_url() . "Home/OutstandingFundsData"; ?>',
				data	:	{},
				success	: 	function(response){
					if(response){
						$('div#OutstandingFunds').append(response);
						//window.location.reload();
					}
				}
	});
}

ajax_assignment_process();
function ajax_assignment_process(){
	return false;

	$.ajax({
				type	:	'POST',
				url		:	'<?php echo base_url() . "Home/ajax_assignment_process"; ?>',
				data	:	{},
				success	: 	function(response){
					if(response){
						$('div#assignment_process').append(response);
						//window.location.reload();
					}
				}
	});
}

/*ajax_top_lender();
function ajax_top_lender(){

	$.ajax({
				type	:	'POST',
				url		:	'<?php echo base_url() . "Home/ajax_top_lender"; ?>',
				data	:	{},
				success	: 	function(response){
					if(response){
						$('div#top_lender').append(response);
					}
				}
	});
}

ajax_top_borrower();
function ajax_top_borrower(){

	$.ajax({
				type	:	'POST',
				url		:	'<?php echo base_url() . "Home/ajax_top_borrower"; ?>',
				data	:	{},
				success	: 	function(response){
					if(response){
						$('div#top_borrower').append(response);
					}
				}
	});
}
*/

ajax_maturity_schedule();
function ajax_maturity_schedule(){

	$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url() . "Home/ajax_maturity_schedule"; ?>',
			data	: {},
			success : function(response){
				if(response){
					$('div#maturity_schedule').append(response);
					//window.location.reload();
				}
			}
	});

}

ajax_portfolio_statistics();
function ajax_portfolio_statistics(){

	$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url() . "Home/ajax_portfolio_statistics"; ?>',
			data	: {},
			success : function(response){
				if(response){
					$('div#portfolio_statistics').append(response);
					//window.location.reload();
				}
			}
	});
}

ajax_year_to_date_statistics();
function ajax_year_to_date_statistics(){

	$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url() . "Home/ajax_year_to_date_statistics"; ?>',
			data	: {},
			success : function(response){
				if(response){
					$('div#year_portfolio_statistics').append(response);
					//window.location.reload();
				}
			}
	});
}



//ajax_trust_deed();
/* function ajax_trust_deed(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php// echo base_url()."Home/ajax_trust_deed";?>',
			data	: {},
			success : function(response){

				if(response){
					//$("#LoadingImage").hide();
					$('div#trust_deed').append(response);
					//window.location.reload();
				}
			}
	});
}
 */

ajax_action_item();
function ajax_action_item(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url() . "Home/ajax_action_item"; ?>',
			data	: {},
			success : function(response){

				if(response){
					//$("#LoadingImage").hide();
					$('div#action_item').append(response);
					//window.location.reload();
				}
			}
	});
}

ajax_active_trust_deed();
function ajax_active_trust_deed(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url() . "Home/active_trust_deedData"; ?>',
			data	: {},
			success : function(response){

				if(response){
					//$("#LoadingImage").hide();
					$('div#active_trust_deed').append(response);
					//window.location.reload();
				}
			}
	});
}




capital_requirements();
function capital_requirements(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url() . "Home/capital_requirement"; ?>',
			data	: {},
			success : function(response){

				if(response){
					//$("#LoadingImage").hide();
					$('div#capital_requirement').append(response);
					//window.location.reload();
				}
			}
	});
}

balance_sheet_loan();
function balance_sheet_loan(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url() . "Home/balance_sheet_loan"; ?>',
			data	: {},
			success : function(response){

				if(response){
					//$("#LoadingImage").hide();
					$('div#balance_sheet_loan').append(response);
					//window.location.reload();
				}
			}
	});
}

OutstandingClosingFees();
function OutstandingClosingFees(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url() . "Home/OutstandingClosingFeesData"; ?>',
			data	: {},
			success : function(response){

				if(response){
					//$("#LoadingImage").hide();
					$('div#OutstandingClosingFees').append(response);
					//window.location.reload();
				}
			}
	});
}

 avaliable_capital();
function avaliable_capital(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url() . "Home/available_capital"; ?>',
			data	: {},
			success : function(response){

				if(response){
					//$("#LoadingImage").hide();
					$('div#avaliable_capital').append(response);
					//window.location.reload();
				}
			}
	});
}
 loan_servicing();
function loan_servicing(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url() . "Home/ajax_loan_servicing"; ?>',
			data	: {},
			success : function(response){

				if(response){
					//$("#LoadingImage").hide();
					$('div#loan_servicing').append(response);
					//window.location.reload();
				}
			}
	});
}


 posted_trust_deed();
function posted_trust_deed(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url() . "Home/posted_trust_deed_function"; ?>',
			data	: {},
			success : function(response){

				if(response){
					//$("#LoadingImage").hide();
					$('div#posted_trust').append(response);
					//window.location.reload();
				}
			}
	});
}



 paid_off_process();
function paid_off_process(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url() . "Home/paid_off_process"; ?>',
			data	: {},
			success : function(response){

				if(response){
					//$("#LoadingImage").hide();
					$('div#paid_offf_process').append(response);
					//window.location.reload();
				}
			}
	});
}


ajax_assigment_signture();
function ajax_assigment_signture(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url() . "Home/ajax_assigment_signtur"; ?>',
			data	: {},
			success : function(response){

				if(response){
					//$("#LoadingImage").hide();
					$('div#ready_signture').append(response);
					//window.location.reload();
				}
			}
	});
}

LenderStatusProccig();
function LenderStatusProccig(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url() . "Home/ajaxLenderStatusProccig"; ?>',
			data	: {},
			success : function(response){

				if(response){
					//$("#LoadingImage").hide();
					$('div#LenderStatusProccig').append(response);
					//window.location.reload();
				}
			}
	});
}

ajax_AssignmentProcess();
function ajax_AssignmentProcess(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url() . "Home/AssignmentProcessData"; ?>',
			data	: {},
			success : function(response){

				if(response){

					//$("#LoadingImage").hide();
					$('div#AssignmentProcess').append(response);
					//window.location.reload();

					$('div#AssignmentProcess table#table').DataTable({
				    	//"aaSorting": false,
				    	"lengthChange": false,
				    	"searching": false,
				    	"paging":   false,
        				"info":     false
				    });

				}
			}
	});
}

/*ajax_term();
function ajax_term(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php //echo base_url()."Home/ajax_term_sheet";?>',
			data	: {},
			success : function(response){

				if(response){
					//$("#LoadingImage").hide();
					$('div#ajax_term_sheet').append(response);
					//window.location.reload();
				}
			}
	});
}
*/

ajax_loan_service();
function ajax_loan_service(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url() . "Home/ajax_loan_service_fun"; ?>',
			data	: {},
			success : function(response){

				if(response){
					//$("#LoadingImage").hide();
					$('div#ajax_loan_service').append(response);
					//window.location.reload();
				}
			}
	});
}

ajax_loan_date();
function ajax_loan_date(){
	//$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '<?php echo base_url() . "Home/ajax_loan_late_date"; ?>',
			data	: {},
			success : function(response){

				if(response){
					//$("#LoadingImage").hide();
					$('div#ajax_loan_date_late').append(response);
					//window.location.reload();
				}
			}
	});
}


</script>
