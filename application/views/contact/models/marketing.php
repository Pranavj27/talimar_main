<div class="modal fade" id="Marketing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							  <div class="modal-dialog modal-lg" role="document">
							    <div class="modal-content">
							    <form method="POST" action="<?php echo base_url();?>Contact_marketing/Contactmarketingdata"  name="forms_Contactmarketingdata" id="forms_Contactmarketingdata">
							      <div class="modal-header">
							        <input type="hidden" name="contact_id_page" value="<?php echo $contact_id;?>">
							        <h4 class="modal-title">Contact Marketing<?php echo isset($contact_id) ? ': ' . $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname : ''; ?></h4>
							    	
							        <div class="row">
											<h3 style="background: #eeeeee;padding: 8px 10px;font-size: 17px;font-weight: 500;">Marketing Material</h3>
									</div>
							       
							      </div>
							      <div class="modal-body">
							      	<div id="marktingdata">

							      		<?php

							      		$count = 0;
							      		 if(isset($contatctmarketing) && is_array($contatctmarketing)){ 
							      			foreach($contatctmarketing as $row){ 

							      				$count++;
							      				if($count == '1'){
							      					$style = '';
							      				}else{
							      					$style = 'style="visibility: hidden"';
							      				}
							      			?>

							      				<input type="hidden" name="rowid[]" value="<?php echo $row->id;?>">
							      				<div class="row">
							      					<div class="col-md-1">
									        			<label>Action:</label>
									        			<a title="Remove" onclick="removedata('<?php echo $row->id;?>');"><i class="fa fa-trash"></i></a>
									        		</div>
									        		<div class="col-md-2">
									        			<label <?php echo $style;?>>Status:</label>
									        			<select class="form-control" name="title_status[]">
									        				<?php foreach($contact_marketing_status_option as $key => $rowss){ ?>

									        					<option value="<?php echo $key;?>" <?php if($key == $row->title_status){ echo 'Selected';}?>><?php echo $rowss;?></option>

									        				<?php } ?>
									        			</select>
									        		</div>
									        		<div class="col-md-2">
									        			<label <?php echo $style;?>>Date:</label>
									        			<input type="text" name="date[]" class="form-control datepicker" placeholder="Enter Date" value="<?php echo date('m-d-Y', strtotime($row->date));?>">
									        		</div>
									        		<div class="col-md-2">
									        			<label <?php  echo $style;?>><?php echo $rowContact->requested_by; ?>Requested By:</label>
									        			
									        			<select class="form-control" name="contact_id[]">
									        				<?php foreach($fetch_all_contact as $key => $rowContact){ ?>

									        					<option value="<?php echo $rowContact->contact_id;?>" <?php if($rowContact->contact_id == $row->requested_by){ echo 'Selected';}?>><?php echo $rowContact->contact_firstname . ' ' . $rowContact->contact_lastname ;?></option>

									        				<?php } ?>
									        			</select>
									        		</div>
									        		<div class="col-md-2">
									        			<label <?php echo $style;?>>Item:</label>
									        			<input type="text" name="swag[]" class="form-control" placeholder="Enter Swag" value="<?php echo $row->swag;?>">
									        		</div>
									        		

									        		<div class="col-md-3">
									        			<label <?php echo $style;?>>Note:</label>
									        			<textarea type="text" name="note[]" class="form-control" rows="2" placeholder="Enter Note..."><?php echo $row->note;?></textarea>
									        		</div>
									        	</div>


							      		<?php } }else{ ?>

								      		<input type="hidden" name="rowid[]" value="new">
								        	<div class="row">
								        		<div class="col-md-1">
								        			<label>Action:</label>
								        		</div>
								        		<div class="col-md-2">
								        			<label>Status:</label>
								        			<select class="form-control" name="title_status[]">
								        				<?php foreach($contact_marketing_status_option as $key => $row){ ?>
								        					<option value="<?php echo $key;?>"><?php echo $row;?></option>
								        				<?php } ?>
								        			</select>
								        		</div>
								        		<div class="col-md-2">
								        			<label>Date:</label>
								        			<input type="text" name="date[]" class="form-control datepicker" value="" placeholder="Enter Date">
								        		</div>
								        		<div class="col-md-2">
									        			<label>Requested By:</label>
									        			<select class="form-control" name="contact_id[]">
									        				<?php foreach($fetch_all_contact as $key => $rowContact){ ?>

									        					<option value="<?php echo $rowContact->contact_id;?>"><?php echo $rowContact->contact_firstname . ' ' . $rowContact->contact_lastname ;?></option>

									        				<?php } ?>
									        			</select>
									        		</div>
								        		<div class="col-md-2">
								        			<label>Item:</label>
								        			<input type="text" name="swag[]" class="form-control" value="" placeholder="Enter Swag" >
								        		</div>
								        		<div class="col-md-3">
								        			<label>Note:</label>
								        			<textarea type="text" name="note[]" class="form-control" rows="2" placeholder="Enter Note..."></textarea>
								        		</div>

								        		

								        		
								        	</div>

								        <?php } ?>
							        </div>

							        	<div class="row">&nbsp;</div>
							        	<div class="row">
							        		<div class="col-md-12">
							        			<a onclick="addmoreswag();" class="btn btn-primary">Add</a>
							        		</div>
							        	</div>

							      </div>
							      <div class="modal-footer">
							        	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							        	<button type="sybmit" name="submit" class="btn btn-primary">Save</button>
							      </div>
							  	</form>
							    </div>
							  </div>
							</div>