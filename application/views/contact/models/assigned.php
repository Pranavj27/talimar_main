<div class="modal fade bs-modal-lg" id="assigned" tabindex="-1" role="large" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Permissions</h4>
			</div>

			<div class="modal-body">
				<?php

                    $fetch_contact_permission = unserialize($fetch_sql_assigned[0]->contact_permission);

                    foreach ($fetch_contact_permission as $con_permission) {
                    	$ee[$con_permission] = $con_permission;
                    }

                    ?>
				<input type="hidden" value="<?php echo $fetch_sql_assigned[0]->id; ?>" name="assigned_id"/>
				<div class="modal-body">

					<div class="row ">
						<div class="col-md-12">
							<label style=""><b>Permissions (only allows that selected person/group to view History and Statistics]</b></label>
						</div>
					</div>
					<div class="row contact_tags_row">
						<?php

                            foreach ($contact_permission as $key => $permission) {
                            	if ($ee[$key] == $key) {
                            		$checked = 'checked="checked"';
                            	} else {
                            		$checked = '';
                            	}

                            	?>
							<div class="col-md-8">
								<input id="checkbx_<?php echo $key; ?>" onclick="check_permission_option(this.value,<?php echo $key; ?>)" type="checkbox" <?php echo $checked; ?> class="form-control" name="contact_permission[]" value="<?php echo $key; ?>"/>&nbsp;&nbsp;<?php echo $permission; ?>
							</div>



						<?php	}?>
						<div class="col-md-8 particular_user_dropdown" style="display:none;">
							<select name="particular_user_id" id="particular_user_id">
								<option value="">Select User</option>
								<?php
                                    if ($all_users) {
                                    	foreach ($all_users as $key => $username) {

                                    		if ($fetch_sql_assigned[0]->particular_user_id == $username->id) {
                                    			$selected = 'selected';
                                    		} else {

                                    			$selected = '';
                                    		}
                                    		echo '<option ' . $selected . ' value = ' . $username->id . '>' . $username->fname . ' ' . $username->lname . '</option>';
                                    	}
                                    }
                                    ?>
							</select>
						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<button type="submit"  class="btn blue">Save</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>