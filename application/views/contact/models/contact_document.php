<div class="modal fade bs-modal-lg" id="contact_document" tabindex="-1" role="large" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
        
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Contact Document<?php echo isset($contact_id) ? ': ' . $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname : ''; ?></h4>
			</div>

			<div class="modal-body">
				<div class="doc-div">
					
					<div id="ct_Documentsadd" class="row">						
						<div class="col-md-2">&nbsp;</div>
						<div class="col-md-8">
							<div class="ct_loan_saved_document" style="display: none;">	
								<form action="<?php echo base_url();?>Contact/upload_new_pdfs" method="POST" enctype="multipart/form-data" name="forms_upload_new_pdfs" id="forms_upload_new_pdfs">
									<input type="hidden" name="contact_id" value="<?php echo isset($contact_id) ? $contact_id : ''; ?>" >

				                    <input type="hidden" name="first_name" value="<?php echo $fetch_contact_data[0]->contact_firstname; ?>" >
				                    <input type="hidden" name="middle_name" value="<?php echo $fetch_contact_data[0]->contact_middlename; ?>" >
				                    <input type="hidden" name="last_name" value="<?php echo $fetch_contact_data[0]->contact_lastname; ?>" >

				                    <div class="form-group">
										<label for="document_name">Document Name:</label>
										<input type="text" class="form-control" id="document_name" name="document_name" required>
									</div>
									<div class="form-group">
										<div id="ad">
											<label>Select Document:</label>
											<input type="file" id = "borrower_upload_1" name ="contact_upload_doc[]" class="select_images_file" >
										</div>
									</div>

									<button type="submit"  class="btn blue">Save</button>
								</form>
							</div>
						</div>
						<div class="col-md-2">&nbsp;</div>
					</div>
					<br>


					<div class="col-md-12 ct_Documentsadd_table">
						
						<table class="table table-responsive" id="loan_doc_table" style="width: 100% !important;">
							<thead>
								<tr>
									<th>Action</th>
									<th>Name of Document</th>
									<th>Uploaded Date</th>
									<th>User</th>
									<th>View</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if($fetch_contact_document){
									foreach($fetch_contact_document as $contactDocuments) {

									$FileNameGet = '';
									
									$UploadedDate = date("m-d-Y", strtotime($contactDocuments->created_at));
									
									$filename = $contactDocuments->contact_document;
									$FileArr = explode('/', $filename);
									$FileNameGet = $FileArr[count($FileArr)-1];

									$file1 = explode('/', $FileNameGet);
									$file2 = explode('.', $file1[count($file1)-1]);

									$FileNameGet = $NameUploadedcalss = $file2[0];

									if($contactDocuments->document_name){
										$NameUploaded = $contactDocuments->document_name;
									}else{
										$NameUploaded = $FileNameGet;
									}


									$up_user_id = $contactDocuments->user_id;
									$UpUserDate = $this->User_model->query("SELECT * FROM user WHERE id = '$up_user_id'")->row();
									if($UpUserDate){
										$UploadedUser = $UpUserDate->fname.' '.$UpUserDate->lname;
									}
								?>
								<tr attrremove="<?php echo $FileNameGet; ?>">
									<td>
										<a><i id="<?php echo $contactDocuments->id; ?>" onclick="delete_doc(this);" title = "Delete File" class = "fa fa-trash"  aria-hidden="true"></i></a>
									</td>
									<td>
										<?php echo $NameUploaded; ?>
									</td>
									<td><?php echo $UploadedDate; ?></td>
									<td><?php echo $UploadedUser; ?></td>
									<td>
										<a href="<?php echo aws_s3_document_url($contactDocuments->contact_document); ?>" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a>
									</td>
								</tr>
								<?php
									}
								}
								?>

							</tbody>
						</table>
					</div>

				</div>
					
			</div>

			<div class="modal-footer">
				<button type="button" id="ct_loan_saved_documentadd" class="btn btn-primary">Add Document</button>
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>