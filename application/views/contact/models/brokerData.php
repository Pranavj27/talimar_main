<div class="modal fade bs-modal-lg" id="BrokerData" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST" action="#" name="forms_BrokerData" id="forms_BrokerData">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Broker Data: <?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname : ''; ?></h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" name="contact_id" value="<?php echo $contact_id; ?>"/>

                    <div class="row" >
                        <h4 style="background-color:#efefef;padding: 10px;">Broker Portal Access</h4>

                            <div class="col-md-3">
                                <label>Broker Portal Access:</label>
                                    <select  class="form-control" name="allow_access_broker">

                                        <?php
                                            foreach ($lender_portal_access as $key => $row) {
                                                ?>
                                            <option value="<?php echo $key; ?>" <?php if ($key == $fetch_lender_contact_type[0]->allow_access) {echo 'Selected';}?>><?php echo $row; ?></option>
                                            <?php
                                            }
                                        ?>
                                    </select>
                            </div>
                            
                            <div class="col-md-3">
                                <label>Username:</label>
                                <input type="text" name="broker_username" class="form-control" value="" readonly>
                            </div>
                            
                            <div class="col-md-3">
                                <label>Password:</label>
                                <input type="text" name="broker_password" class="form-control" value="" readonly>

                            </div>
                            <div class="col-md-2">

                                <a style="margin-top: 22px !important;" class="btn btn-success"> Send E-mail</a>
                                
                            </div>
                    </div>

                </div>
                <div class="modal-footer">

                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>