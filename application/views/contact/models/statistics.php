<div class="modal fade bs-modal-lg" id="statistics" tabindex="-1" role="large" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Statistics <?php echo isset($contact_id) ? ': ' . $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname : ''; ?></h4>
			</div>
				<!-----------Hidden fields----------->
				<!-----------End of hidden Fields---->
			<div class="modal-body">
				<hr style="border-color:black !important;">

				<div class="row ">
					<div class="col-md-3">
						<label style=""><b>Trust Deed Schedule</b></label>
					</div>
				</div>
				<div class="row ">
					<table style="width:50% !important;float:left;" id="table_trust_deed" class="table table-bordered table-striped table-condensed flip-content">
						<tbody>
							<tr>
								<td>Property Address</td>
								<td>Loan Status</td>
								<td>Interest</td>
								<td>Loan Amount</td>
							</tr>
						</tbody>
					</table>

					<table style="width:30% !important;float:left;margin-left: 135px;" id="table_stats1" class="table table-bordered table-striped table-condensed flip-content">
						<tbody>

							<tr>
								<td>Active Trust Deeds:</td>
								<td>##</td>
							</tr>
							<tr>
								<td>Active Trust Deeds:</td>
								<td>$#,###,####</td>
							</tr>
							<tr>
								<td>Paid Off Trust Deeds:</td>
								<td>##</td>
							</tr>
							<tr>
								<td>Paid Off Trust Deeds:</td>
								<td>$#,###,####</td>
							</tr>
							<tr>
								<td>Total Trust Deeds:</td>
								<td>##</td>
							</tr>
							<tr>
								<td>Total Trust Deeds:</td>
								<td>$#,###,####</td>
							</tr>
						</tbody>
					</table>
				</div>

				<hr style="border-color:black !important;">

				<div class="row ">
					<div class="col-md-3">
						<label style=""><b>Loan Schedule</b></label>
					</div>
				</div>
				<div class="row ">
					<table style="width:50% !important;float:left;" id="table_loan" class="table table-bordered table-striped table-condensed flip-content">
						<tbody>
							<tr>
								<td>Property Address</td>
								<td>Loan Status</td>
								<td>Loan Amount</td>
								<td>Loan Type</td>
							</tr>
						</tbody>
					</table>

					<table style="width:30% !important;float:left;margin-left: 135px;" id="table_stats2" class="table table-bordered table-striped table-condensed flip-content">
						<tbody>

							<tr>
								<td>Active Loans:</td>
								<td>##</td>
							</tr>
							<tr>
								<td>Active Loans:</td>
								<td>$#,###,####</td>
							</tr>
							<tr>
								<td>Paid Off Loans:</td>
								<td>##</td>
							</tr>
							<tr>
								<td>Paid Off Loans:</td>
								<td>$#,###,####</td>
							</tr>
							<tr>
								<td>Total Loans:</td>
								<td>##</td>
							</tr>
							<tr>
								<td>Active Loans:</td>
								<td>$#,###,####</td>
							</tr>
						</tbody>
					</table>
				</div>
				<hr style="border-color:black !important;">

			</div>

			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<!--<button type="submit"  class="btn blue">Submit</button>-->
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
<!-- /.modal-dialog -->
</div>