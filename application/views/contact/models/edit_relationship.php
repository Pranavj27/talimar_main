<div class="modal fade bs-modal-lg" id="edit-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<input type="hidden" class="relation_id" value="">


			<form id="" method="post" action="#">
				<div class="modal-header">

					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Relationship: </h4>
				</div>
				<div class="modal-body">

			<div class="msg"></div>
						<input type ="hidden" name="contact_iddd"  value="<?php echo $contact_id; ?>"/>

					<div class="row" id="v">
						<div class="col-md-6">
							<label><strong>Contact name:</strong></label>
									<select id="relationshipss"  name="relationship" class="form-control" >
										<option value='' >Select One</option>
									<?php foreach ($fetch_all_contact as $row) { ?>
										<option value="<?php echo $row->contact_id; ?>"  >
										<?php echo $row->contact_firstname . ' ' . $row->contact_middlename . ' ' . $row->contact_lastname; ?></option>
										<?php } ?>
								</select>


						</div>

						<div class="col-md-6" id="d">
							<label><strong>Relationship Type:</strong></label>
									<input type="hidden" id="u_id" value="">
								<select name="relation_option"  id="reltionn" class="form-control" >
								<?php foreach ($relationships_option as $key => $option) {?>

									<option value="<?php echo $key; ?>" ><?php echo $option; ?></option>

								<?php } ?>
								</select>

						</div>
					</div>
					<div class="row">&nbsp;</div>
					<div class="row">
						<div class="col-md-6" >
							<label><strong>Show Lender Portal:</strong></label>
							<input type="checkbox" name="edit_show_lender_portal" id='edit_show_lender_portal' value="1">
						</div>
					</div>
					<div class="row">&nbsp;</div>
					<div class="row">
						<div class="col-md-12">
						<label><strong>Relationship Note:</strong></label>
							<textarea name="relationship_note"  type="text" id="notes" rows="6" class="form-control v" placeholder="Add Relationship Note Here..." ></textarea>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
            	<button type="button" class ="btn blue"  onClick="relationship_edit_fun(this)" >Save</button>

				</div>
			</form>
		</div>
	</div>
</div>