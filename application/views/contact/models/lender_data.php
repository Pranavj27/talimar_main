<style type="text/css">
    .margin_checkbox{
    margin-bottom: 4px;
}
div.checkId > .checker{
    width: 6%!important;
}
div.checkId1 > .checker{
    width: 10%!important;
}
div.checker, div.checker span, div.checker input {
   /* width: 19px;*/
    height: 19px;
}
input[type="checkbox"] {
  /* width: 19px;*/
    display: inline-block;
    width: 19%;
    height: 19px;
}
input.amount_format.selectpicker {
    border: #e5e5e5 solid 1px;
    height: 36px;
    border-radius: 5px;
    padding: 15px;
}
</style>
<div class="modal fade bs-modal-lg" id="lender_data" tabindex="-1" role="large" aria-hidden="true">
	<div class="modal-dialog modal-lg" style="width: 67%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Lender Data <?php echo isset($contact_id) ? ': ' . $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname : ''; ?></h4>
			</div>

			<div class="modal-body">

                <div class="row">
                    <h4 class="rowh4">Security Questions</h4>
                    <div class="col-md-6">
                        <label>Security Question</label>
                        <input type="text" name="accountword" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->accountword) ? $fetch_lender_contact_type[0]->accountword : ''; ?>" placeholder="Account Word">
                    </div>
                    <div class="col-md-6">
                        <label>Security Answer</label>
                        <input type="text" name="accountwordAnswer" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->accountwordAnswer) ? $fetch_lender_contact_type[0]->accountwordAnswer : ''; ?>" placeholder="Account Word">
                    </div>
                </div>
                <?php                                            
				/*03-02-2021*/
				$css_av_account = '';
				$css_av_account_varification = '';
				if($fetch_lender_contact_type[0]->status == 'av_account'){
					$avArgument  = '';
					$avArgument .= $fetch_lender_contact_type[0]->lender_contact_id.'@@@@';
					$avArgument .= $fetch_contact_data[0]->contact_email.'@@@@';
					$avArgument .= $fetch_contact_data[0]->contact_firstname.'@@@@';
					$avArgument .= $fetch_contact_data[0]->contact_lastname.'@@@@';
					$avArgument .= $fetch_contact_data[0]->contact_phone;
					

					$css_av_account = 'display:none;';
					$css_av_account_varification = '<a style="margin-top: 0px;padding-left: 7px;" class="btn btn-success av_account_varification_account" href="javascript:void(0)" av_account_data="'.base64_encode($avArgument).'" >Create Lender User</a>';
				}

				if($fetch_lender_contact_type[0]->username != ''){
                    $userlender = $fetch_lender_contact_type[0]->username;
                }else{
                	$strflname = $fetch_contact_data[0]->contact_firstname;
                    $userlender = $strflname[0].'_'.$fetch_contact_data[0]->contact_lastname;
                }
                $rand_password = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTVWXYZ0123456789"), 1, 10);
				?>
                <div class="row">
                    <h4 class="rowh4">Security Access</h4>
                    	<div id="messagesChange1"></div>
                        <small>
                        	<mark>
                        		<b>Note:</b> Before sending Email/login details to the Lender, please select 'Yes' under Lender Portal Access drop-down menu and then save the information.
                        	</mark>
                        	<br><br>
                        </small>

                        <div class="col-md-12">
                        	<?php echo $css_av_account_varification; ?>
                        </div>

                        <div class="col-md-4" style="<?php echo $css_av_account; ?>">
                        	<div class="portalcss_av_account" >
                                <label for="allow_access">Lender Portal Access</label>
                                <select  class="form-control" name="allow_access" id="allow_access" style="    width: 100px !important;">

                                    <?php
                                        foreach ($lender_portal_access as $key => $row) {
                                            ?>
                                        <option value="<?php echo $key; ?>" <?php if ($key == $fetch_lender_contact_type[0]->allow_access) {echo 'Selected';}?>><?php echo $row; ?></option>
                                        <?php
                                        }
                                        ?>
                                </select>
                        	</div>
                        </div>
                        <div class="col-md-4" style="<?php echo $css_av_account; ?>">
                        	<?php
                            	$lender_td_details = 'no';
                            	$no_selected = 'selected';
                            	$yes_selected = '';
                            	if(isset($fetch_lender_contact_type[0]->lender_td_details)){
                            		if($fetch_lender_contact_type[0]->lender_td_details == 'yes'){
                            			$lender_td_details = 'yes';
                            			$no_selected = '';
                            			$yes_selected = 'selected';
                            		}
                            	}
                            ?>
                            <label>Trust Deed Details Access</label>
                            <br>
                            <select name="lender_td_details" id="lender_td_details" class="form-control" style="width: 100px !important;" contact_id="<?php echo $contact_id;?>" >
                            	<option value="no" <?php echo $no_selected; ?>>No</option>
                            	<option value="yes" <?php echo $yes_selected; ?>>Yes</option>
                            </select>		
                        </div>

                        <div class="col-md-4" style="<?php echo $css_av_account; ?>">
                        	
                            <label>Mortgage Fund Access</label>
                            <br>
                            <select name="mortgageActivate" onchange="mortgageF(this.value,'<?php echo $mortgage['id']; ?>')" class="selectpicker form-control" id="mortgageActivate">
								<option value='no' <?php echo $mortgage['mortgageActivate']=='no'?'selected':'' ?>>No</option>
								<option value='yes' <?php echo $mortgage['mortgageActivate']=='yes'?'selected':'' ?>>Yes</option>	
							</select>
                            		
                        </div>

                        <div class="col-md-12">&nbsp;</div>

                        <div class="col-md-6" style="<?php echo $css_av_account; ?>">
                        	<label>Username</label>
                            <input type="text" name="username" class="form-control" value="<?php echo str_replace(" ", "", $userlender);?>" readonly />
                            
                        </div>
                        <div class="col-md-6" style="<?php echo $css_av_account; ?>">
                        	<label>Password</label>
                            <input type="text" name="password" class="form-control" value="<?php echo $fetch_lender_contact_type[0]->password ? $fetch_lender_contact_type[0]->password : $rand_password; ?>" readonly />
                        </div>
                        <div class="col-md-12">&nbsp;</div>
                        <div class="col-md-4" style="<?php echo $css_av_account; ?>">
                          	<?php if($fetch_lender_contact_type[0]->allow_access == '1'){ ?>
                        	<a style="margin-top: 0px;padding-left: 7px;" class="btn btn-success" href="javascript:void(0)" onclick="SendUsername(<?php echo $contact_id;?>,0,'<?php echo str_replace(" ", "", $userlender);?>')">Send User Name</a>
                        	<?php  }else{ ?>
                        	<a style="margin-top: 0px;padding-left: 7px;" class="btn btn-success" href="javascript:void(0)" disabled>Send User Name</a>
                        	<?php  } ?>
                        	
                        	
                        </div>

                        <div class="col-md-4" style="<?php echo $css_av_account; ?>">
                          	<?php if($fetch_lender_contact_type[0]->allow_access == '1'){ ?>
                        	<a style="margin-top: 0px;padding-left: 7px;" class="btn btn-success" href="javascript:void(0)" onclick="SendLinkToForget(<?php echo $contact_id;?>,0,'<?php echo str_replace(" ", "", $userlender);?>')">Reset Password</a>
                        	<?php  }else{ ?>
                        	<a style="margin-top: 0px;padding-left: 7px;" class="btn btn-success" href="javascript:void(0)" disabled>Reset Password</a>
                        	<?php  } ?>
                        </div>
                        <div class="col-md-4" style="<?php echo $css_av_account; ?>"> 
                            <a style="margin-top: 0px;padding-left: 7px;" data-toggle="modal" class="btn btn-success" href="#LoginHistory">Login History</a>
                        </div>
                        <div class="col-md-12">&nbsp;</div>                 
                        <!-- <div class="col-md-12" style="<?php //echo $css_av_account; ?>">
                            <label><b>Login History</label> 
                            <span><?php //echo $lastlogindetails; ?></b></span>
                        </div> -->
                </div>

                <script type="text/javascript">
                    
                    function send_email_to_contact(that){

                        var contact_id = '<?php echo $contact_id;?>';

                        if(contact_id != ''){
                            $.ajax({
                                type : 'post',
                                url  : '<?php echo base_url()."Lender_Access/lender_forgot_password"?>',
                                data : {'contact_id': contact_id},
                                success : function(response){
                                    if(response == 1){
                                    	alert('Email successfully send');
                                    }else{
                                        alert('Error: Email not send');
                                    }
                                }
                            });

                        }else{

                            alert('Error: contact ID is empty!');
                            return false;
                        }
                    }

                </script>

               

                <script type="text/javascript">
                function mortgageF(that,id)
				{

						$.ajax({
						        type : 'POST',
						        url  : '<?echo base_url()."Contact/activateMortgage";?>',
						        data : {'mortgageActivate':that,'id':id},
						        success : function(response){
							        var json =  JSON.parse(response);
									var message = json.message;
									$("#messagesActivate").html(message);
						        }
						    });
				}
                </script>


                <div class="row lender_data_row" id="">
                    <h4 class="rowh4">Active Trust Deeds</h4>
                    <table class="table table-border">
                        <thead>
                            <tr>
                                <th>Street Address</th>
                                <th>Interest ($)</th>
                                <th>Loan Amount</th>
                                <th>Account</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($activeTrustDeedArray) && is_array($activeTrustDeedArray)){ 
                                    foreach($activeTrustDeedArray as $value){ 

                                    	if($value['loan_amount'] > 0){ ?>
                                <tr>
                                    <td><a href="<?php echo base_url();?>load_data/<?php echo $value['loan_id'];?>"><?php echo $value['fulladdress'];?></a></td>
                                    <td>$<?php echo number_format($value['investment']);?></td>
                                    <td>$<?php echo number_format($value['loan_amount']);?></td>
                                    <td><a href=""><?php echo $allinvestorsnamelist[$value['lender_name']];?></a></td>
                                                                            
                                </tr>
                            <?php }  } }else{ ?>
                                <tr>
                                    <td colspan="4">Active Trust Deeds details not found!</td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

				<div class="row">
					<h4 class="rowh4">Lender Statistics</h4>
					<div class="col-md-12">
						<table class="bro table table-border table-responsive table-stripe">
						<thead>
							<tr>
								<th>Trust Deeds</th>
								<th># of Trust Deeds</th>
								<th>$ of Trust Deeds</th>
							</tr>
						<thead>
						<?php foreach ($active_loan as $row) {

                        	$count_loan = $row['count_loan'];
                        	$total_investment = $row['total_investment'];
                        	$total_loan_amount = $row['total_loan_amount'];

                        	$lender_id = $row['lender_id'];
                        }

                        foreach ($paidoff_loan as $row) {

                        	$p_count_loan = $row['count_loan'];
                        	$total_investment_p = $row['total_investment'];

                        }

                        ?>
						<tbody>
							<tr>
								<td>Active Trust Deeds</td>
								<td><?php echo $count_loan ? $count_loan : 0; ?></td>
								<td>$<?php echo number_format($total_investment); ?></td>
							</tr>
							<tr>
								<td>Paid Off Trust Deeds</td>
								<td><?php echo $p_count_loan; ?></td>
								<td>$<?php echo number_format($total_investment_p); ?></td>
							</tr>

							<tr>
								<td>Pipeline Loans</td>
								<td><?php echo $pipe_loan['count_pipe_loan']; ?></td>
								<td>$<?php echo number_format($pipe_loan['total_loan_amount']); ?></td>
							</tr>


							<?php
                                $total_trust_deeds_count = $count_loan + $p_count_loan;
                                $total_trust_deeds_amount = $total_investment + $total_investment_p;

                                ?>
							<tr>
								<th>Total Trust Deeds</th>
								<th><?php echo $total_trust_deeds_count; ?></th>
								<th>$<?php echo number_format($total_trust_deeds_amount); ?></th>
							</tr>
								<tr>
								<th><a href="<?php echo base_url('reports/loan_schedule_investor/' . $contact_id); ?>">View Report</a></th>

							</tr>
						</tbody>
					</table>
					</div>
				</div>

				<div class="row lender_data_row" id="">
					<h4 class="rowh4">Lender Accounts</h4>
					<table class="table table-border">
						<thead>
							<tr>
								<th>Account Name</th>
								<th>$ of Active TD’s</th>
								<th>$ of Total TD’s</th>
							</tr>
						</thead>
						<tbody>
							<?php if(isset($alllender_accountdetails) && is_array($alllender_accountdetails)){ 
									foreach($alllender_accountdetails as $value){ ?>
								<tr>
									<td><a href="<?php echo base_url();?>investor_view/<?php echo $value['allinvestor_id'];?>"><?php echo $allinvestorsnamelist[$value['allinvestor_id']];?></a></td>
									<td>$<?php echo number_format($value['activelendersum']);?></td>
									<td>$<?php echo number_format($value['alllendersum']);?></td>
									
									
								</tr>
							<?php } }else{ ?>
								<tr>
									<td colspan="3">Lender account details not found!</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>




				<!-- <?php /*
				$committed_funds = $fetch_lender_contact_type[0]->committed_funds;

				$available_funds = $committed_funds - $total_active_balance;
				*/
				?>
				<div class="row lender_data_row" id="">
					<h4 class="rowh4">Available Capital  <span id="show_imgg" style="display: none;"> <img src="<?php echo base_url() . 'assets/images/blank_folder.png' ?>" alt="image"></span></h4>
					<div class="col-md-4">
						<label>Committed Funds</label>
						<input type="text" name="committed_funds" id="committed_funds" class="form-control amount_format" value="<?php echo '$' . number_format($committed_funds); ?>">
					</div>
					<div class="col-md-4">
						<label>Active Balance</label>
						<input type="text" value="<?php echo '$' . number_format($total_active_balance); ?>" class="form-control" readonly />
					</div>
					<div class="col-md-4">
						<label>Available Funds</label>
						<input type="text" class="form-control" value="<?php echo '$' . number_format($available_funds); ?>" readonly />
					</div>

				</div> -->
            
                


                <div class="row lender_data_row" id="">
                	<h4 class="rowh4">Contact Lender Setup Information</h4>
                    <div class=" col-md-3">
                        <label>Lender Setup Package</label>
                            <select name="setup_package" class="form-control" >
                            <?php foreach ($setup_package_option as $key => $row) {?>
                                <option value="<?php echo $key; ?>" <?php if ($key == $fetch_lender_contact_type[0]->setup_package) {echo 'Selected';}?>><?php echo ucfirst($row); ?></option>
                            <?php }?>
                            </select>
                    </div>
                    <div class=" col-md-3">
                        <label>Date Completed</label>
                            <input type="text" class="form-control datepicker" id="package_date" name="setup_package_date" value="<?php echo isset($fetch_lender_contact_type[0]->setup_package_date) ? date('m-d-Y', strtotime($fetch_lender_contact_type[0]->setup_package_date)) : ''; ?>" placeholder="MM-DD-YYYY" />
                    </div>
                </div>
                <div class="row lender_data_row" id="">

                	<div class=" col-md-3">
                        <label>NDA Signed</label>

                    	<select name="nda_signed" class="form-control">
                            <?php foreach ($no_yes_option as $key => $row) {?>
                                <option value="<?php echo $key; ?>" <?php if ($key == $fetch_lender_contact_type[0]->nda_signed) {echo 'Selected';}?>><?php echo $row; ?></option>
                            <?php }?>
                        </select>
                    </div>

                    <div class=" col-md-3">
                        <label>Date Completed</label>
                            <input type="text" class="nda_date_val form-control datepicker" name="nda_date" value="<?php echo isset($fetch_lender_contact_type[0]->nda_date) ? date('m-d-Y', strtotime($fetch_lender_contact_type[0]->nda_date)) : ''; ?>" placeholder="MM-DD-YYYY" />
                    </div>
                </div>

                <div class="row" id="">
                	<h4 class="rowh4">Lender Survey</h4>
                	<div class="col-md-6">
                		<label>Will you invest as a Fractional Interest holder</label> 
                	</div>
                	<div class="col-md-6">
                		<select class="selectpicker" name="ownership_type">
                			<option value="">Select One</option>
                			<option value="1" <?php echo (int)$fetch_lender_contact_type[0]->ownership_type==1?'selected':''; ?>>Yes</option>
                			<option value="2" <?php echo (int)$fetch_lender_contact_type[0]->ownership_type==2?'selected':''; ?>>No</option>
                		</select>
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;" ></div> 
                	<div class="col-md-6">
                		<label>Maximum Loan to Value</label> 
                	</div>
                	<div class="col-md-6">
                		<?php $ltv = explode(",", $fetch_lender_contact_type[0]->ltv); ?>
                	
                		<select class="selectpicker" name="ltv" id="ltv_id">
                			<option value="">Select One</option>
							<option value="1" <?php echo (int)$fetch_lender_contact_type[0]->ltv==1?'selected':''; ?>>50% LTV</option>
							<option value="2" <?php echo (int)$fetch_lender_contact_type[0]->ltv==2?'selected':''; ?>>60% LTV</option>
							<option value="3" <?php echo (int)$fetch_lender_contact_type[0]->ltv==3?'selected':''; ?>>65% LTV</option>
							<option value="4" <?php echo (int)$fetch_lender_contact_type[0]->ltv==4?'selected':''; ?>>70% LTV</option>
							<option value="5" <?php echo (int)$fetch_lender_contact_type[0]->ltv==5?'selected':''; ?>>75% LTV</option>
							<option value="6" <?php echo (int)$fetch_lender_contact_type[0]->ltv==6?'selected':''; ?>>80% LTV</option>
							<option value="7" <?php echo (int)$fetch_lender_contact_type[0]->ltv==7?'selected':''; ?>>85% LTV</option>
							<option value="8" <?php echo (int)$fetch_lender_contact_type[0]->ltv==8?'selected':''; ?>>90% LTV</option>
						</select>
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div>
                	<div class="col-md-6">
                		<label>What is your maximum loan position</label> 
                	</div>
                	<div class="col-md-6">
                		<?php $position = explode(",", $fetch_lender_contact_type[0]->position); ?>
                		<select class="selectpicker" name="position">

                			<option value="">Select One</option>
                			<?php foreach ($position_optionn as $key => $row) {?>
                			<option value="<?php echo $key; ?>" <?php if ((int)$fetch_lender_contact_type[0]->position == $key) {echo "selected";}?>><?php echo $row; ?></option>
							<?php } ?> 
						</select>
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div>
                	<div class="col-md-6">
                		<label>Will you lend on Completion Value:*</label> 
                	</div>
                	<div class="col-md-6">
                		<select class="selectpicker" name="lend_value">
                			<option value="">Select One</option>
                			<option value="1" <?php echo (int)$fetch_lender_contact_type[0]->lend_value == 1?'Selected':''; ?>>Yes</option>
                			<option value="2" <?php echo (int)$fetch_lender_contact_type[0]->lend_value == 2?'Selected':''; ?>>No</option>
                		</select>
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div> 
                	<div class="col-md-6">
                		<label>What is your maximum Loan Term</label> 
                	</div>
                	<div class="col-md-6">
                		<?php $LoanTerm = explode(",", $fetch_lender_contact_type[0]->LoanTerm); ?>
                		<select class="selectpicker" name="LoanTerm">
							<option value="">Select One</option>
							<?php foreach ($LoanTerm_type as $key => $row) {?>
							<option value="<?php echo $key; ?>" <?php if ($fetch_lender_contact_type[0]->LoanTerm == $key) {echo 'selected';}?>><?php echo $row; ?></option>
							<?php } ?> 
                		</select>
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div>  
                	<div class="col-md-6">
                		<label>What is your maximum investment per Trust Deed</label> 
                	</div>
                	<div class="col-md-6">
                		<input type="text" name="maximum_income" class="amount_format selectpicker" value="<?php echo '$'.number_format((double)$fetch_lender_contact_type[0]->maximum_income); ?>">
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div>
                	
                	<div class="col-md-6">
                		<label>How much do you have to invest in Trust Deeds</label> 
                	</div>
                	<div class="col-md-6">
                		<input type="text" name="invest_per_deed" class="amount_format selectpicker" value="<?php echo '$'.number_format((double)$fetch_lender_contact_type[0]->invest_per_deed); ?>">
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div>
                	<div class="col-md-6">
                		<label>What is the total # of Trust Deeds you have funded</label> 
                	</div>
                	<div class="col-md-6">
                		<input type="text" name="funded" class="amount_format selectpicker" value="<?php echo number_format((double)$fetch_lender_contact_type[0]->funded); ?>">
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div> 
                	<div class="col-md-3">
                		<label>Loan Type</label> 
                	</div>
                	<?php $loan = explode(",", $fetch_lender_contact_type[0]->loan); ?>
                	<div class="col-md-3">
                		<input type="checkbox" name="loan[]" value="1" <?php if ((int)$loan[0] == 1) {echo "checked";}?>/><span>Bridge Loan**</span> 
                	</div>
                	<div class="col-md-3" style="padding-left:4px;margin-right: -6px;">
                		<input type="checkbox" name="loan[]" value="2" <?php if((int)$loan[1] == 2) {echo "checked";}?>/><span>Fix and Flip/Hold***</span> 
                	</div>
                	<div class="col-md-3" style="padding-left:4px">
                		<input type="checkbox" name="loan[]" value="3" <?php if((int)$loan[2] == 3) {echo "checked";}?>/><span>New Construction</span> 
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div>
                	<div class="col-md-3">
                		<label>Property Type</label> 
                	</div>
                	<div class="col-md-9 row">
                		<?php $lender_property = explode(",", $fetch_lender_contact_type[0]->property_type); ?>
                		<?php foreach ($lender_property_type as $key => $row) {?>
                    	<div class="col-md-4" style="padding-left:0px;margin-bottom:10px;">
                    		<input type="checkbox" name="property_type[]" class="form-control" value="<?php echo $key; ?>" <?php if (in_array($key, $lender_property)) {echo 'checked';}?>/><span><?php echo $row; ?></span> 
                    	</div>
                    	<?php }?>
                    </div>  
                </div>

				<div class="row" id="">
					<div class="col-md-12">
                    <p>* Completion Value refers to the value of the property once the renovations are complete. Fix and Flip / Hold loans are generally unwritten to the value of the property once the renovations are complete and may included a Renovation Hold Back for future renovation costs. </p>

                    <p>** Bridge Loan generally refers to loans in which the Borrower does not intend to renovate the property and/or the value is not based upon an After Repair Value. </p>

                    <p>*** Fix and Flip / Hold generally refers to loans in which the Borrower intends to renovate the property over the course of the loan term. The loan generally includes a Renovation Hold Back for future renovation costs and the Loan to Value ratio is based upon the Completion Value. </p>
                	</div>
                </div>

				<div class="row" id="">&nbsp;&nbsp;</div>

				<div class="row lender_data_row" id="">

					<h4 style="background-color:#efefef;padding: 10px;">General Information</h4>
					<div class="col-md-3">
						<label>Date of Birth</label>
						<input type="text" class="form-control lender_enabled input-md datepicker" name="lender_dob" value="<?php echo isset($fetch_lender_contact_type[0]->lender_dob) ? date('m-d-Y', strtotime($fetch_lender_contact_type[0]->lender_dob)) : ''; ?>" placeholder="MM-DD-YYYY" />
					</div>
				</div>


				<!-- <div class="row lender_data_row" id="">

				<h4 style="background-color:#efefef;padding: 10px;">Employment Information</h4>
					<div class="col-md-3">
						<label>Employment (Yes, No, Retired)</label>
						<input type="text" name="employment" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->employment) ? $fetch_lender_contact_type[0]->employment : ''; ?>">
					</div>
					<div class="col-md-3">
						<label>Title</label>
						<input type="text" name="e_title" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->e_title) ? $fetch_lender_contact_type[0]->e_title : ''; ?>">
					</div>
					<div class="col-md-3">
						<label>Length of Position</label>
						<input type="text" name="e_length_of_position" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->e_length_of_position) ? $fetch_lender_contact_type[0]->e_length_of_position : ''; ?>">
					</div>
				</div> -->

				<!-- <div class="row lender_data_row" id="">

					<div class="col-md-3">
						<label>Current Position</label>
						<input type="text" name="current_position" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->current_position) ? $fetch_lender_contact_type[0]->current_position : ''; ?>">
					</div>
					<div class="col-md-3">
						<label>Title</label>
						<input type="text" name="cp_title" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->cp_title) ? $fetch_lender_contact_type[0]->cp_title : ''; ?>">
					</div>
					<div class="col-md-3">
						<label>Length of Position(Years)</label>
						<input type="text" name="cp_length_of_position" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->cp_length_of_position) ? $fetch_lender_contact_type[0]->cp_length_of_position : ''; ?>">
					</div>
				</div>


				<div class="row lender_data_row" id="">

					<div class="col-md-3">
						<label>Previous Position</label>
						<input type="text" name="cp_previous_position" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->cp_previous_position) ? $fetch_lender_contact_type[0]->cp_previous_position : ''; ?>">
					</div>

				</div> -->


				<!-- <div class="row lender_data_row" id="">

					<h4 style="background-color:#efefef;padding: 10px;">Education</h4>
					<div class="col-md-3">
						<label>Highest Year Completed</label>
						<input type="text" name="highest_year_completed" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->highest_year_completed) ? $fetch_lender_contact_type[0]->highest_year_completed : ''; ?>">
					</div>
					<div class="col-md-3">
						<label>Year of Graduation</label>
						<input type="text" name="year_of_graduation" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->year_of_graduation) ? $fetch_lender_contact_type[0]->year_of_graduation : ''; ?>">
					</div>
					<div class="col-md-3">
						<label>Degree / Diploma</label>
						<input type="text" name="degree_diploma" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->degree_diploma) ? $fetch_lender_contact_type[0]->degree_diploma : ''; ?>">
					</div>
				</div> -->

				<div class="row" id="">
					<h4 class="rowh4">Lender RE870 Data</h4>
					 
                	
                	<div class="col-md-4">
                		<label>RE870 complete</label> 
                	</div>
                	<div class="col-md-6">
                		<select name="re_complete" id="re_complete" class="selectpicker" onchange="fieldHideshow1();">
                            <?php foreach ($re885_complete_option as $key => $row) {?>
                                <option value="<?php echo $key; ?>" <?php if($key==$fetch_lender_contact_type[0]->re_complete){ echo "selected";}?>><?php echo $row; ?></option>
                            <?php }?>
                        </select>
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div>
                	<div class="col-md-4">
                		<label>Date of RE870</label> 
                	</div>
                	<div class="col-md-6">
                		<input type="text" id="lender_RE_date" class="RE_date_val form-control lender_enabled input-md datepicker" name="RE_date" value="<?php echo isset($fetch_lender_contact_type[0]->lender_RE_date) ? date('m-d-Y', strtotime($fetch_lender_contact_type[0]->lender_RE_date)) : ''; ?>" placeholder="MM-DD-YYYY" onchange="display_package_date_of_re(1);"/>
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div>
                	<div class="col-md-4"> 
                		<label>Status</label> 
                	</div>
                	<div class="col-md-6">
                		<?php 
                			$lender_RE_date = $fetch_lender_contact_type[0]->lender_RE_date;
                			$date = date('d-m-Y');
                			$diff = abs(strtotime($date) - strtotime($lender_RE_date));
                			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                			if($months >= 12){
                		?>
                		<input type="text" id="lender_RE_form_type" class="form-control" name="lender_RE_form_type" value="Current" placeholder="" disabled/>
                		<?php } else { ?>
                			<input style="color:red" type="text" id="lender_RE_form_type" class="form-control lender_enabled input-md" name="lender_RE_form_type" value="Expired" placeholder="" disabled />
                		<?php } ?>
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div>
                	<div class="col-md-4"> 
                		<label>Employment Information</label> 
                	</div>
                	<div class="col-md-6">
                		<select class="form-control" name="employment" id="employmentId" onchange="fieldHideshow()">
                			<option value="">Select One</option>
                			<option value="yes" <?php if ($fetch_lender_contact_type[0]->employment == 'yes') {echo 'selected';}?>>Yes</option>
                			<option value="no" <?php if ($fetch_lender_contact_type[0]->employment == 'no') {echo 'selected';}?>>No</option>
                			<option value="retired" <?php if ($fetch_lender_contact_type[0]->employment == 'retired') {echo 'selected';}?>>Retired</option>
                		</select>
                	</div>
                	 
                	<div class="clearfix" style="margin-bottom:10px;"></div>
                	<div class="col-md-4"> 
                		<label>Current Position</label>
                		<input type="text" name="current_position" value="<?php echo isset($fetch_lender_contact_type[0]->current_position) ? $fetch_lender_contact_type[0]->current_position : ''; ?>" class="form-control"> 
                	</div>
                	<div class="col-md-4">
                		<label>Title</label>
                		<input type="text" name="cp_title" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->cp_title) ? $fetch_lender_contact_type[0]->cp_title : ''; ?>">
                	</div>
                	<div class="col-md-4">
                		<label>Length of Current Position</label>
                		<input type="text" name="cp_length_of_position" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->cp_length_of_position) ? $fetch_lender_contact_type[0]->cp_length_of_position : ''; ?>">
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div>
                	<div class="col-md-4">
                		<label>Previous Position: </label>
                		<input type="text" name="cp_previous_position" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->cp_previous_position) ? $fetch_lender_contact_type[0]->cp_previous_position : ''; ?>">
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div>

                	<div class="col-md-4"> 
                		<label>Highest Year Completed</label>
                		<input type="text" name="highest_year_completed" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->highest_year_completed) ? $fetch_lender_contact_type[0]->highest_year_completed : ''; ?>"> 
                	</div>
                	<div class="col-md-4">
                		<label>Year of Graduation</label>
                		<input type="text" name="year_of_graduation" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->year_of_graduation) ? $fetch_lender_contact_type[0]->year_of_graduation : ''; ?>">
                	</div>
                	<div class="col-md-4">
                		<label>Degree / Diploma</label>
                		<input type="text" name="degree_diploma" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->degree_diploma) ? $fetch_lender_contact_type[0]->degree_diploma : ''; ?>">
                	</div>
				</div>


				<div class="row lender_data_row" id="">

					<h4 class="rowh4">Financial Situation</h4>

					<div class="col-md-6" style="margin-bottom: 5px;">
						<label>Estimated Annual Income</label>
					</div><br><br>
					<?php foreach ($lender_financial_situation as $key => $row) {?>
					<div class="col-md-6">

							<input type="checkbox" value="<?php echo $key; ?>" onchange="financial_value(this);" class="form-control" <?php if ($key == $fetch_lender_contact_type[0]->financial_situation) {echo 'Checked';}?>/><?php echo $row; ?>

							<input type="hidden" name="financial_situation" value="<?php echo isset($fetch_lender_contact_type[0]->financial_situation) ? $fetch_lender_contact_type[0]->financial_situation : ''; ?>" id="financial_situation_value" />

					</div>
					<?php if($key%2==0){ ?>
						<div class="clearfix" style="margin-bottom:10px;"></div>
					<?php } }?>

					<div class="row"><br>&nbsp;</div>

					<div class="col-md-6" style="margin-bottom: 5px;">
						<label>Estimated Net Worth</label>
					</div><br><br>
					<?php foreach ($lender_financial_situation as $key => $row) {?>
					<div class="col-md-6">

							<input type="checkbox" value="<?php echo $key; ?>" onchange="net_worth_financial_value(this);" class="form-control" <?php if ($key == $fetch_lender_contact_type[0]->net_worth_financial_situation) {echo 'Checked';}?>/><?php echo $row; ?>

							<input type="hidden" name="net_worth_financial_situation" value="<?php echo isset($fetch_lender_contact_type[0]->net_worth_financial_situation) ? $fetch_lender_contact_type[0]->net_worth_financial_situation : ''; ?>" id="net_worth_financial_situation_value" />

					</div>
					<?php if($key%2==0){ ?>
						<div class="clearfix" style="margin-bottom:10px;"></div>
					<?php } }?>

					<div class="row"><br>&nbsp;</div>

					<div class="col-md-6" style="margin-bottom: 5px;">
						<label>Estimated Liquid Assets</label>
					</div><br><br>
					<?php foreach ($lender_financial_situation as $key => $row) { ?>
					<div class="col-md-6">

							<input type="checkbox" value="<?php echo $key; ?>" onchange="liquid_financial_value(this);" class="form-control" <?php if ($key == $fetch_lender_contact_type[0]->liquid_financial_situation) {echo 'Checked';}?>/><?php echo $row; ?>

							<input type="hidden" name="liquid_financial_situation" value="<?php echo isset($fetch_lender_contact_type[0]->liquid_financial_situation) ? $fetch_lender_contact_type[0]->liquid_financial_situation : ''; ?>" id="liquid_financial_situation_value" />

					</div>

					<?php if($key%2==0){ ?>
						<div class="clearfix" style="margin-bottom:10px;"></div>
					<?php } }?>

				</div>
				
				<!-- <div class="row lender_data_row" id="">
					<div class="col-md-9" style="width:100%;">
						<textarea class="form-control" name="textarea_value" rows="3"><?php //echo isset($fetch_lender_contact_type[0]->textarea_value) ? $fetch_lender_contact_type[0]->textarea_value : ''; ?></textarea>
					</div>
				</div> -->
				<div class="row">
					<div class="col-md-6" style="margin-bottom: 5px;">
						<label>Source of Income and Cash Resources</label>
						<input type="text" name="source_income_cash" class="form-control" value="<?php echo !empty($fetch_lender_contact_type[0]->source_income_cash)?$fetch_lender_contact_type[0]->source_income_cash:''; ?>">
					</div>
					<div class="col-md-6">
						
					</div>
				</div>
				<div class="clearfix" style="margin-bottom:10px;"></div>
				<div class="row lender_data_row" id="">

					<div class="col-md-6" style="margin-bottom: 5px;">
						<label>Liquidity Needs</label>
					</div><br>
					<?php foreach ($lender_liquidity_needs as $key => $row) { ?>
					<div class="col-md-6 checkId" style="width:68%;">
						<input type="checkbox" value="<?php echo $key; ?>" onchange="liquidity_needs_value(this);" <?php if ($key == $fetch_lender_contact_type[0]->liquidity_needs_checkbox_value) {echo 'Checked';}?> /><span><?php echo $row; ?></span>

						<input type="hidden" name="liquidity_needs_checkbox_value" value="<?php echo isset($fetch_lender_contact_type[0]->liquidity_needs_checkbox_value) ? $fetch_lender_contact_type[0]->liquidity_needs_checkbox_value : '' ?>" id="lender_liquidity_needs_chkbx" />
					</div><br>
					<div class="clearfix" style="margin-bottom:10px;"></div>
					<?php } ?>

				</div>
				
				<div class="row" id="">

					<h4 class="rowh4">Investment Experience</h4>

					<?php 
					$a = 1;
                    $explobox = explode(',', $fetch_lender_contact_type[0]->invest_ex_checkbox);

                    $explobox_year = explode(',', $fetch_lender_contact_type[0]->invest_yrs_exp);
                    
                    $y = 0;
                    foreach($lender_investment_experience as $key => $row) {

                    	if(count($lender_investment_experience) != $a){
                    ?>
							<div class="col-md-5 checkId1">
								<input type="checkbox" <?php if (in_array($key, $explobox)) {echo 'Checked';}?> name="investment_experience_checkbox[]" value="<?php echo $key; ?>"><?php echo $row; ?>

							</div>
							<?php if($key == 1){ ?>
							<div class="col-md-7" style="margin-bottom: 30px;">
							

							</div>
							<?php } else { ?> 
							<div class="col-md-6 row">

								<div class="col-md-2">Years</div> 
								<div class="col-md-10"><input type="number" class="RE_date_val form-control lender_enabled input-md " name="invest_yrs_exp[]" value="<?php echo $explobox_year[$y]; ?>" placeholder="YYYY" on/></div>

							</div>
							<?php $y++; } ?>
					<?php } else{ ?>
						<div class="col-md-4 checkId1" style="padding-left:23px">
								 <input type="checkbox" <?php if (in_array($key, $explobox)) {echo 'Checked';}?> name="investment_experience_checkbox[]" value="<?php echo $key; ?>">&nbsp;&nbsp;<?php echo $row; ?>
						</div>
						<div class="col-md-4 row">
								<div class="col-md-3">Years</div> 
								<div class="col-md-9"><input type="number" class="RE_date_val form-control lender_enabled input-md " name="invest_yrs_exp[]" value="<?php echo $explobox_year[$y]; ?>" placeholder="YYYY" on/></div>
						</div>
						<div class="col-md-4">
							<div class="col-md-3">Specific</div> 
							<div class="col-md-9"><input type="text" name="specific" class="form-control" value="<?php echo (!empty($fetch_lender_contact_type[0]->specific)?$fetch_lender_contact_type[0]->specific : ''); ?>"></div>
						</div>

					<?php }?>
					<div class="clearfix" style="margin-bottom:10px;"></div>
					<?php $a++; } ?>


				</div>
			</div>


			<div class="modal-footer">
				<input type="hidden" name="popup_data" value="lender-data">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<button type="submit"  name="modal_btn" value="save" class="btn blue">Save</button>
				<a href="<?php echo base_url(); ?>lender_data_print/<?php echo $contact_id; ?>" class="btn blue">Print</a>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade" id="LoginHistory" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action ="#">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Login History </h4>
                        </div>
                        <div class="modal-body">


                            <div class="row">
                                

                                
                                    <table id="myTableData" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Time</th>
                                                                                               
                                            </tr>
                                        </thead>
                                        
                                        <tbody id="">
                                            <?php if(!empty($lastlogindetails)){ foreach($lastlogindetails as $logValue){ ?>
                                                <tr>
                                                    <td><?php echo date('m-d-Y', strtotime($logValue->date)); ?></td>
                                                    <td><?php echo $logValue->time; ?></td>
                                                </tr>
                                            <?php } } else { '<tr><td colspan="3">Not login yet</td></tr>'; } ?>
                                        </tbody>
                                    
                                        <tfoot id="">
                                            
                                            
                                        </tfoot>
                                    </table>
                                
                            </div>
                        </div>
                        <div class="modal-footer">
                            <!-- <button type="button" name="submit" value="save" class="btn blue">Save</button> -->
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
        </div>
</div>
<script type="text/javascript">
	fieldHideshow1();
	fieldHideshow();
	function fieldHideshow1()
	{
		
		var select = $("#re_complete").find(":selected").val(); //input[name='employment']; 

		if(select == '1')
		{
			
			$("input[name='RE_date']").prop("disabled", false);

			// $("input[name='lender_RE_form_type']").prop("disabled", false);
			
		}
		else
		{
			
			$("input[name='RE_date']").val('');
			// $("input[name='lender_RE_form_type']").val('');
			

			$("input[name='RE_date']").prop("disabled", true);
			// $("input[name='lender_RE_form_type']").prop("disabled", true);
		}
	}
</script>
<script type="text/javascript">
  $(document).ready( function () {
    $('#myTableData').DataTable();
} );
</script>

