<div class="modal fade bs-modal-lg" id="add_relationship" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form id="relationfrm" method="post" action="<?php echo base_url() . 'Contact/contact_relation' ?>">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Relationship: </h4>
						</div>
						<div class="modal-body">

								<input type ="hidden" name="contact_iddd"  value="<?php echo $contact_id; ?>"/>

							<div class="row">
								<div class="col-md-6">
									<label><strong>Contact name:</strong></label>
										<select id="relationship" name="relationship" class="chosen">
											<option value=''>Select One</option>
											<?php
											foreach ($fetch_all_contact as $row) {
												?>
												<option value="<?php echo $row->contact_id; ?>"  ><?php echo $row->contact_firstname . ' ' . $row->contact_middlename . ' ' . $row->contact_lastname; ?></option>
											<?php } ?>
										</select>

								</div>

								<div class="col-md-6">
									<label><strong>Relationship Type:</strong></label>
										<select name="relation_option" class="form-control">
										<?php foreach ($relationships_option as $key => $option) {?>

										<option value="<?php echo $key; ?>" <?php if ($key == $relation_option) {echo 'Selected';}?>><?php echo $option; ?></option>

										<?php } ?>
										</select>


								</div>
								
							</div>
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="col-md-6" >
									<label><strong>Show Lender Portal:</strong></label>
									<input type="checkbox" name="show_lender_portal" id='add_show_lender_portal' value="1" <?php if(!empty($show_lender_portal) && $show_lender_portal=="1"){ echo "checked";}?>>
								</div>
							</div>
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="col-md-12">
								<label><strong>Relationship Note:</strong></label>
									<textarea name="relationship_note" type="text" rows="6" class="form-control" placeholder="Add Relationship Note Here..."></textarea>
								</div>
							</div>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
							<button class ="btn blue" type="submit" id="relation_contact_id" onclick="submit_relation_frm(this);">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>