<div class="modal fade bs-modal-lg" id="hardmoney" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST" action="<?php echo base_url();?>Contact_marketing/hardmoneyprogram" name="forms_hardmoneyprogram" id="forms_hardmoneyprogram">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Hard Money Program: <?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname : ''; ?></h4>
                </div>
                <div class="modal-body">
                    
                    <input type="hidden" name="contact_id" value="<?php echo $contact_id; ?>">
                    <input type="hidden" name="hm_rowid" value="<?php echo isset($contathard_money_program[0]->id) ? $contathard_money_program[0]->id : 'new';?>">

                    <div class="row addhmspace">
                        <div class="col-md-6">
                            <label>Company Name:</label>
                            <input type="text" name="hm_company" class="form-control" placeholder="Company Name" value="<?php echo isset($contathard_money_program[0]->hm_company) ? $contathard_money_program[0]->hm_company : '';?>">
                        </div>
                    </div>
                    <div class="row addhmspace">
                        <div class="col-md-3">
                            <label>Bridge Loans:</label>
                            <select  class="form-control" name="hm_brodge_loan">
                                <?php foreach ($no_yes_option as $key => $row) { ?>
                                    <option value="<?php echo $key; ?>" <?php if($key == $contathard_money_program[0]->hm_brodge_loan){echo 'selected';}?>><?php echo $row; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="row addhmspace">
                        <div class="col-md-3">
                            <label>LTV Ratio:</label>
                            <input type="text" name="hm_b_ltv" class="form-control" placeholder="LTV Ratio" value="<?php echo isset($contathard_money_program[0]->hm_b_ltv) ? number_format($contathard_money_program[0]->hm_b_ltv,2).'%' : '';?>">
                        </div>
                        <div class="col-md-3">
                            <label>Loan Min:</label>
                            <input type="text" name="hm_b_lmin" class="form-control" placeholder="Loan Min" value="<?php echo isset($contathard_money_program[0]->hm_b_lmin) ? '$'.number_format($contathard_money_program[0]->hm_b_lmin) : '';?>">
                        </div>
                        <div class="col-md-3">
                            <label>Loan Max:</label>
                            <input type="text" name="hm_b_lmax" class="form-control" placeholder="Loan Max" value="<?php echo isset($contathard_money_program[0]->hm_b_lmax) ? '$'.number_format($contathard_money_program[0]->hm_b_lmax) : '';?>">
                        </div>
                        <div class="col-md-3">
                            <label>Max Term (months):</label>
                            <input type="text" name="hm_b_mterm" class="form-control" placeholder="Max Term (months)" value="<?php echo isset($contathard_money_program[0]->hm_b_mterm) ? $contathard_money_program[0]->hm_b_mterm : '';?>">
                        </div>
                    </div>

                    <div class="row addhmspace">
                        <div class="col-md-3">
                            <label>Construction Loan:</label>
                            <select  class="form-control" name="hm_construct_loan">
                                <?php foreach ($no_yes_option as $key => $row) { ?>
                                    <option value="<?php echo $key; ?>" <?php if($key == $contathard_money_program[0]->hm_construct_loan){echo 'selected';}?>><?php echo $row; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row addhmspace">
                        <div class="col-md-3">
                            <label>LTV Ratio:</label>
                            <input type="text" name="hm_c_ltv" class="form-control" placeholder="LTV Ratio" value="<?php echo isset($contathard_money_program[0]->hm_c_ltv) ? number_format($contathard_money_program[0]->hm_c_ltv,2).'%' : '';?>">
                        </div>
                        <div class="col-md-3">
                            <label>Loan Min:</label>
                            <input type="text" name="hm_c_lmin" class="form-control" placeholder="Loan Min" value="<?php echo isset($contathard_money_program[0]->hm_c_lmin) ? '$'.number_format($contathard_money_program[0]->hm_c_lmin) : '';?>">
                        </div>
                        <div class="col-md-3">
                            <label>Loan Max:</label>
                            <input type="text" name="hm_c_lmax" class="form-control" placeholder="Loan Max" value="<?php echo isset($contathard_money_program[0]->hm_c_lmax) ? '$'.number_format($contathard_money_program[0]->hm_c_lmax) : '';?>">
                        </div>
                        <div class="col-md-3">
                            <label>Max Term (months):</label>
                            <input type="text" name="hm_c_mterm" class="form-control" placeholder="Max Term (months)" value="<?php echo isset($contathard_money_program[0]->hm_c_mterm) ? $contathard_money_program[0]->hm_c_mterm : '';?>">
                        </div>
                    </div>

                    <div class="row addhmspace">
                        <div class="col-md-3">
                            <label>Fix and Flip Loans:</label>
                            <select  class="form-control" name="hm_fixflip">
                                <?php foreach ($no_yes_option as $key => $row) { ?>
                                    <option value="<?php echo $key; ?>" <?php if($key == $contathard_money_program[0]->hm_fixflip){echo 'selected';}?>><?php echo $row; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row addhmspace">
                        <div class="col-md-3">
                            <label>LTV Ratio:</label>
                            <input type="text" name="hm_ff_ltv" class="form-control" placeholder="LTV Ratio" value="<?php echo isset($contathard_money_program[0]->hm_ff_ltv) ? number_format($contathard_money_program[0]->hm_ff_ltv,2).'%' : '';?>">
                        </div>
                        <div class="col-md-3">
                            <label>Loan Min:</label>
                            <input type="text" name="hm_ff_lmin" class="form-control" placeholder="Loan Min" value="<?php echo isset($contathard_money_program[0]->hm_ff_lmin) ? '$'.number_format($contathard_money_program[0]->hm_ff_lmin) : '';?>">
                        </div>
                        <div class="col-md-3">
                            <label>Loan Max:</label>
                            <input type="text" name="hm_ff_lmax" class="form-control" placeholder="Loan Max" value="<?php echo isset($contathard_money_program[0]->hm_ff_lmax) ? '$'.number_format($contathard_money_program[0]->hm_ff_lmax) : '';?>">
                        </div>
                        <div class="col-md-3">
                            <label>Max Term (months):</label>
                            <input type="text" name="hm_ff_mterm" class="form-control" placeholder="Max Term (months)" value="<?php echo isset($contathard_money_program[0]->hm_ff_mterm) ? $contathard_money_program[0]->hm_ff_mterm : '';?>">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn blue">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>