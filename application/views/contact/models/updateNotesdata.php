<div class="modal fade" id="updateNotesdata" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
	<form method="post" action="<?php echo base_url();?>Contact/UPdateNotes" name="form_updateNotesdata" id="form_updateNotesdata">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>Update Contact Note</b></h5>
      </div>
      <div class="modal-body">
	  
		<input type="hidden" name="updateid" value="">
		<input type="hidden" name="contact_id" value="">
	  
			<div class="row">
				<div class="col-md-6">
					<label><b>Date:</b></label>
					<input type="text" name="notedate" class="form-control datepicker">
				</div>
				
				<div class="col-md-6">
					<label><b>Type:</b></label>
					<select type="text" name="notetype" class="form-control">
						<?php foreach ($notes_type as $key => $type) { ?>
							<option value="<?php echo $key;?>"><?php echo $type;?></option>
						<?php }?>
					</select>
				</div>
			</div>
			
			<div class="row"><br></div>
			<div class="row">
				<div class="col-md-6">
					<label><b>Purpose:</b></label>
					<select type="text" name="purpose_type" class="form-control">
						<?php foreach ($contact_purpose_menu as $key => $type) { ?>
							<option value="<?php echo $key;?>"><?php echo $type;?></option>
						<?php }?>
					</select>
				</div>
			</div>
			<div class="row"><br></div>
			
			<div class="row">
				<div class="col-md-12">
					<label><b>Notes:</b></label>
					<textarea type="text" name="notetext" class="form-control" rows="5"></textarea>
				</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="upnote" class="btn btn-primary">Save</button>
      </div>
	  </form>
    </div>
  </div>
</div>