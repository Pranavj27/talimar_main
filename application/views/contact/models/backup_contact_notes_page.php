<?php 
if ($fetch_contact_notes) 
{
	$sequence = 0;
	foreach ($fetch_contact_notes as $key => $notes) {
		$sequence++;
		switch ($notes->notes_type) {
			case 1:$notes_type_val = 'Email';
				break;
			case 2:$notes_type_val = 'Phone';
				break;
			case 3:$notes_type_val = 'Letter';
				break;
			case 4:$notes_type_val = 'Meeting';
				break;
			case 5:$notes_type_val = 'Other';
				break;
			default:
				$notes_type_val = 'None';
		}
		?>
		<form  class ="history-filter" id="history-filter_<?php echo $notes->id; ?>" method="post" action="<?php echo base_url() . 'Contact/history_filter'; ?>" >
			<input type="hidden" value='' id='hidden-type' class='hidden-type' name='action'/>
			<input type="hidden"  id='fetch_contact_id' value="<?php echo $this->uri->segment(2); ?>"  name='fetch_contact_id'/>
			<input type="hidden" id="fetch_notes_id" name="fetch_notes_id" value="<?php echo $notes->id; ?>" />
			<div class="row  borrower_data_row inline-label2 " >
				<div class="notes_class label_notes_date<?php echo $notes->id; ?>" >
					<label class="title_history">Date:</label>
					<label class="history_label form-control" id="label_notes_date" name="fetch_notes_date" ><?php echo date('m-d-Y', strtotime($notes->notes_date)); ?></label>
				</div>
				<div class="notes_class input_notes_date<?php echo $notes->id; ?>" style="display:none;">
					<label class="title_history">Date:</label>
					<input type="text" class="form-control datepicker"  name="fetch_notes_date" id="fetch_notes_date" name="fetch_notes_date" value="<?php echo date('m-d-Y', strtotime($notes->notes_date)); ?>" placeholder="MM-DD-YYYY" />
				</div>
				<div class="notes_class label_notes_type<?php echo $notes->id; ?>">
					<label class="title_history">Type:</label>
					<label  class="history_label form-control" id="label_notes_type"><?php echo $notes_type_val; ?></label>
				</div>
				<div class="notes_class select_notes_type select_notes_type<?php echo $notes->id; ?> " style="display:none;">
					<label class="title_history">Type:</label>

					<input type="hidden" class="form-control "  id="fetch_notes_type_id" name="fetch_notes_type_id" value="<?php echo $notes_type_val; ?>" placeholder="Notes type" />

					<select id="fetch_select_notes_type" name="fetch_notes_type" class=" form-control" data-live-search="true" style="width:150px !important;" >
						<?php
						foreach ($notes_type as $key => $type) {
									if ($key == $notes->notes_type) {
										$selected = 'selected';
									} else {
										$selected = '';
									}
									?>
						<option <?php echo $selected; ?> value="<?php echo $key; ?>"  ><?php echo $type; ?></option>

						<?php }?>
					</select>
				</div>
				<div class="spc-loan-div  label_loan_id<?php echo $notes->id; ?>">
					<label class="title_history">Purpose:</label>

						<!--<label class="history_label form-control" id="label_loan_id" ><a href="<?php echo base_url(); ?>load_data/<?php echo $notes->loan_id; ?>"><?php echo $property_address[$notes->loan_id]; ?></a></label>-->
						<?php if($notes->purpose_type != ''){?>
							<label class="history_label form-control" id="label_loan_id" ><?php echo $contact_purpose_menu[$notes->purpose_type]; ?></a></label>
						<?php } ?>
				</div>
				<?php
				//echo '<pre>';
				//print_r($this->session->all_userdata());
				?>
				<?php if($this->session->userdata('user_role') == '2'){ ?>
					<a onclick="editContactNotes('<?php echo $notes->id; ?>');" class="btn btn-xs btn-info" style="float: right;margin-top: 10px;">Edit</a>
				<?php } ?>
				<div class="spc-loan-div selected_loans select_loan_id<?php echo $notes->id; ?>" style="display:none;">
					<input type="hidden"  class="form-control "  id="fetch_loan_id" name="fetch_loan_id" value="<?php echo $notes->loan_id; ?>" placeholder="loan" />
					<label class="title_history">Property:</label>

					<select id ="fetch_select_talimar_loan" name="fetch_talimar_loan" class="chosen" style="width: 181px !important;" disabled>
						<option value="">Select Property</option>
						<?php
					foreach ($property as $key => $row) {
						if ($property[$key]['id'] == $notes->loan_id) {
							$selected = 'selected';

						} else {

							$selected = '';
						}
						?>
						<option <?php echo $selected; ?> value="<?php echo $property[$key]['id']; ?>"  ><?php echo $property[$key]['text']; ?></option>
						<?php }?>
					</select>
				</div>
			</div>
			<div class="row borrower_data_row borrower_textarea_div " >
				<div class="col-md-9 label_notes_text<?php echo $notes->id; ?>" >											
					<?php echo $notes->notes_text; ?>
				</div>
				<div class="col-md-9 label_notes_text<?php echo $notes->id; ?>" >										
					<i>Entered By : <?php echo $fetch_userss_data[$notes->user_id]; ?></i>
				</div>
                <div class="col-md-9">
                    <?php if($notes->loan_id != ''){
                        $propertLINK = '<a href="'.base_url().'load_data/'.$notes->loan_id.'">'.$listAllProperty[$notes->loan_id].'</a>';
                    }else{
                        $propertLINK = '';
                    } ?>		                                
                    <i>Property: <?php echo $propertLINK; ?></i>
                </div>
				<div class="col-md-9 input_notes_text<?php echo $notes->id; ?>" style="display:none;" >
					<textarea class="form-control "  id ="fetch_personal_notes" name="fetch_personal_notes" placeholder="Text:" id="text" ><?php echo $notes->notes_text; ?></textarea>
				</div>
				<input style="display:none;6px 14px;" id ="history_formsubmit_<?php echo $notes->id; ?>" value="Update" name="submit" type="submit" class ="btn btn-primary pull-right"/>
			</div>
		</form>
		<!------------------End Buttons Section--------------------------->
	<?php	
	}
}else {?>
	<div class="row borrower_data_row" >
		<h4>No Saved Notes!</h4>
	</div>
<?php 
}?>