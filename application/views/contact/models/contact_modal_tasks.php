<div class="modal fade bs-modal-lg" id="contact_modal_tasks" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url(); ?>add_contact_task"  name="forms_add_contact_task" id="forms_add_contact_task">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Contact Tasks: <?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname : ''; ?></h4>
				</div>
				<div class="modal-body">

				<input type="hidden" name="contact_id" value="<?php echo $contact_id; ?>"/>
					<div class="row">
						<div class="col-md-6 ">
							<label><strong>Task Type:</strong></label>
							<!--<input type="text" class="form-control" name="contact_task" value="">-->
							<select name="contact_task" class="form-control">
							<?php foreach ($selct_contact_task as $key => $option) {?>

							<option value="<?php echo $key; ?>" ><?php echo $option; ?></option>

							<?php }?>

							</select>
						</div>

						<div class="col-md-6 ">
							<label><strong>Date Due:</strong></label>
							<input type="text" class="form-control datepicker" name="contact_date" value="" placeholder="MM-DD-YYYY">
						</div>

					</div>
					<div class="row">&nbsp;</div>
					<div class="row">
						<div class="col-md-12">
							<label><strong>Add Note:</strong></label>
							<textarea class="form-control" type="text" name="add_task_note" rows="3" placeholder="Add Note..."></textarea>
						</div>
					</div>
					<div class="row">&nbsp;</div>
					<div class="row">
						<div class="col-md-6 ">
							<label><strong>Status:</strong></label>
							<select name="contact_status" class="form-control">
							<?php foreach ($contact_status_option as $key => $option) {?>

							<option value="<?php echo $key; ?>"><?php echo $option; ?></option>

							<?php }?>

							</select>
						</div>

						<div class="col-md-6 ">
							<label><strong>User:</strong></label><br>
							<input type="text" class="form-control" name="contact_user" value="<?php echo $this->session->userdata('user_name'); ?>">

							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">

					<button type="button" class="btn default" data-dismiss="modal">Close</button>
					<button type="submit"  class="btn blue">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>