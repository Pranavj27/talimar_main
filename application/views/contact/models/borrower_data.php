<?php 
 $ofac_srch_option		= $this->config->item('ofac_srch_option1');
?>
<div class="modal fade bs-modal-lg" id="borrower_data" tabindex="-1" role="large" aria-hidden="true">
	<div class="modal-dialog modal-lg" style="width: 67%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Contact<?php echo isset($contact_id) ? ': ' . $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname.', ' . $fetch_contact_data[0]->suffix : ''; ?></h4>
			</div>
			<!-----------Hidden fields----------->
			<!-----------End of hidden Fields---->
			<div class="modal-body">
				<!------- Borrower Contact Type DIV STARTS ---------->
				

                <div class="row" >
                    <h4 style="background-color:#efefef;padding: 10px;">Borrower Portal Access</h4>
                        <div id="messagesChange"></div>
                        <small><mark><b>Note:</b> Before sending Email/login details to the Borrower, please select 'Yes' under Borrower Portal Access drop-down menu and then save the information.</mark><br><br></small>
                        <div class="col-md-3">
                            <label>Borrower Portal Access:</label>
                                <select  class="form-control" name="allow_access_borrower">

                                    <?php
                                        foreach ($lender_portal_access as $key => $row) {
                                            ?>
                                        <option value="<?php echo $key; ?>" <?php if ($key == $fetch_borrower_contact_type[0]->allow_access_borrower) {echo 'Selected';}?>><?php echo $row; ?></option>
                                        <?php
                                        }
                                    ?>
                                </select>
                        </div>

                        <?php 
                            if($fetch_borrower_contact_type[0]->borrower_username != ''){

                                $userborrower = $fetch_borrower_contact_type[0]->borrower_username;
                            }else{
                                $userborrower = $fetch_contact_data[0]->contact_firstname.$fetch_contact_data[0]->contact_lastname;
                            }

                            $borrower_rand_password = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTVWXYZ0123456789"), 1, 12);

                        ?>
                        
                        <div class="col-md-3">
                            <label>Username:</label>
                            <input type="text" name="borrower_username" class="form-control" value="<?php echo $userborrower;?>" readonly>
                        </div>
                        
                        <div class="col-md-3">
                            <label>Password:</label>
                            <input type="text" name="borrower_password" class="form-control" value="<?php echo isset($fetch_borrower_contact_type[0]->borrower_password) ? base64_decode($fetch_borrower_contact_type[0]->borrower_password) : $borrower_rand_password;?>" readonly>

                        </div>
                        <div class="col-md-3" style="margin-top: 24px;">

                            <a style="margin-top: 0px !important;" class="btn btn-success"> Send E-mail </a> 
                            <span style="padding-left: 5px;padding-right: 5px;font-size: 15px;">OR</span> 
                            <a style="margin-top: 0px;padding-left: 7px;" class="btn btn-success" href="javascript:void(0)" onclick="SendLinkToForget(<?php echo $contact_id;?>,1,'<?php echo $userborrower;?>')">Reset Password</a>
                            
                        </div>
                       
                        <!-- <div class="col-md-1" style="padding-left: 30px;">
                        	
                        </div> -->
                </div>

				<div class="borrower_data_popup">

				<h4 style="background-color:#efefef;padding: 10px;">Active Loans</h4>

					<div class="row borrower_data_row">
				   <div class="col-md-12">
					 <table class="bro table table-border table-responsive table-stripe">
							<thead>
							<tr>
								<th style="width:41%;">Address</th>
								<th style="width:29%;">Loan Amount</th>
								<th style="width:30%;">Rate</th>
							</tr>
							</thead>
							<tbody>
							<?php
                            
                            if (isset($contact_active_loan_query)) {
                            	foreach ($contact_active_loan_query as $rowss) {
                                foreach ($rowss as $row) {?>
							<tr>
								<td><a href="<?php echo base_url() . 'load_data/' . $row->loan_id; ?>"><?php echo ($row->property_address . ' ' . $row->unit) ? $row->property_address . ' ' . $row->unit : ''; ?></a></td>
								<td><?php echo ($row->loan_amount) ? '$' . number_format($row->loan_amount) : ''; ?></td>
								<td><?php echo ($row->interestrate) ? number_format($row->interestrate, 3) . '%' : ''; ?></td>
							</tr>
						<?php } } } ?>

							</tbody>
							</table>

							</div>



							</div>


				<h4 style="background-color:#efefef;padding: 10px;">Borrower Statistics</h4>

				<div class="row borrower_data_row">
				   <div class="col-md-12">
					 <table class="bro table table-border table-responsive table-stripe">
							<thead>
							<tr>
								<th>Loans</th>
								<th># of Loans</th>
								<th>$ of Loans</th>
							</tr>
							<thead>
							<tbody>
							<?php

                                $t_amount = 0;
                                $count_paidoff = 0;

                                foreach ($fetch_paidoff_loan_latest as $row) {
                                	$count_paidoff++;
                                	$t_amount += $row['loan_amount'];
                                }

                                $t_b_amount = 0;
                                $count_brokered = 0;

                                foreach ($fetch_brokered_loan_latest as $roooow) {
                                	$count_brokered++;
                                	$t_b_amount += $roooow['b_loan_amount'];
                                }

                                $can_count_brokered = 0;
                                $can_t_b_amount = 0;
                                foreach ($fetch_cancellled_loan_latest as $w) {
                                	$can_count_brokered++;
                                	$can_t_b_amount += $w['can_b_loan_amount'];

                                }

                                $ative_t_amount = 0;
                                $count_active = 0;
                                foreach ($fetch_active_loan_latest as $r) {
                                	$count_active++;
                                	$ative_t_amount += $r['activ_loan_amount'];

                                }

                                $pipe_t_amount = 0;
                                $count_p = 0;
                                foreach ($fetch_pipe_loan_latest as $pipe) {
                                	$count_p++;
                                	$pipe_t_amount += $pipe['p_loan_amount'];
                                }

                                ?>

							<tr>
								<td>Pipeline Loans</td>
								<td><?php echo $count_p; ?></td>
								<td>$<?php echo number_format($pipe_t_amount); ?></td>
							</tr>
							<tr>
								<td>Active Loans</td>
								<td><?php echo $count_active; ?></td>
								<td>$<?php echo number_format($ative_t_amount); ?></td>
							</tr>
							<tr>
								<td>Brokered Loans </td>
								<td><?php echo $count_brokered; ?></td>
								<td>$<?php echo number_format($t_b_amount); ?></td>
							</tr>

							<tr>
								<td>Cancelled Loans </td>
								<td><?php echo $can_count_brokered; ?></td>
								<td>$<?php echo number_format($can_t_b_amount); ?></td>
							</tr>
							<tr>
								<td>Paid Off Loans</td>
								<td><?php echo $count_paidoff; ?></td>
								<td>$<?php echo number_format($t_amount); ?></td>
							</tr>


							<?php
                            $total_loan_count = $count_active + $count_paidoff + $count_p + $count_brokered + $can_count_brokered;
                            $total_loan_amount = $ative_t_amount + $t_amount + $pipe_t_amount + $t_b_amount + $can_t_b_amount;

                            ?>
							<tr>
								<th>Total Loans</th>
								<th><?php echo $total_loan_count; ?></th>
								<th>$<?php echo number_format($total_loan_amount); ?></th>
							</tr>

							<tr>
								<th><a href="<?php echo base_url('reports/loan_schedule_borrower/' . $contact_id); ?>">View Report</a></th>

							</tr>

							</tbody>
							</table>

							</div>



						</div>




				<h4 style="background-color:#efefef;padding: 10px;">Borrower Accounts</h4>
				<div class="row borrower_data_row">
				   <div class="col-md-12">
					 	<table class="bro table table-border table-responsive table-stripe">
							<thead>
								<tr>
									<th>Borrower Name</th>
									<th>Active Loans (#)</th>
									<th>Active Loans ($)</th>
								</tr>
							</thead>
							<tbody>
								<?php if(isset($fetch_borrower_accounts) && is_array($fetch_borrower_accounts)){ 
										foreach($fetch_borrower_accounts as $value){ ?>
										<tr>
											<td>
												<a href="<?php echo base_url();?>borrower_view/<?php echo $value['borrower_id'];?>"><?php echo $value['borrowername'];?></a>
											</td>
											<td><?php echo $value['totalCount'];?></td>
											<td>$<?php echo number_format($value['totalAmount']);?></td>
										</tr>
							<?php } }else{ ?>
										<tr>
											<td colspan="3">Borrower account details not found</td>
										</tr>
							<?php } ?>
							</tbody>
						</table>
					</div>
				</div>


				<h4 style="background-color:#efefef;padding: 10px;">Personal Information</h4>
					<div class="row borrower_data_row" id="">
						<div class="col-md-3">
							<label>Social Security Number:</label>
							<input  type="text" class="form-control " name="security_number" id="security_number" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_ss_num : ''; ?>" placeholder="Social Security Number" />
						</div>

						<div class="col-md-3">
							<label>DOB:</label>
							<input type="text" class="form-control datepicker" name="dob" id="dob" value="<?php if (isset($contact_id)) {echo $fetch_borrower_contact_type[0]->borrower_contact_dob ? date('m-d-Y', strtotime($fetch_borrower_contact_type[0]->borrower_contact_dob)) : '';}?>" placeholder="DOB(Format:MM-DD-YYYY)" />
						</div>

					</div>

					<div class="row borrower_data_row" id="">
						<div class="col-md-3">
							<label>Driver License #:</label>
							<input type="textbox" class="form-control " id="driver_license" name="driver_license" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_driver_license : ''; ?>" placeholder="Driver License #" />
						</div>
						<div class="col-md-3">

							<label>Driver's License State:</label>
							<!--<input type="textbox" class="form-control borrower_enabled" id="driver_license_state" name="driver_license_state" value="<?php echo $fetch_borrower_contact_type[0]->borrower_driver_license_state ? $fetch_borrower_contact_type[0]->borrower_driver_license_state : ''; ?>" placeholder="Driver's License State" />-->

							<select id="driver_license_state" name="driver_license_state" class ="form-control">
								<option value=""></option>
								<?php
$selected = '';
foreach ($STATE_USA as $key => $value) {
if ($key == $fetch_borrower_contact_type[0]->borrower_driver_license_state) {
$selected = 'selected';
} else {
$selected = '';
}
?>
									<option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
								<?php }?>



							</select>
						</div>
						<?php
//print_r($fetch_borrower_contact_type);
?>
						<div class="col-md-3">

							<label>Expiration Date:</label>
							<input  type="text" name="exp_date" value="<?php echo isset($fetch_borrower_contact_type[0]->borrower_exp_date) ? date('m-d-Y', strtotime($fetch_borrower_contact_type[0]->borrower_exp_date)) : ''; ?>" class="form-control  datepicker"  placeholder="Expiration Date(Format:DD-MM-YYYY)" />
						</div>
					</div>

					<div class="row borrower_data_row" id="">

						<div class="col-md-3">

							<label>Credit Score:</label>
							<input  type="text" class="form-control " name="credit_score" id="credit_score" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_credit_score : ''; ?>" placeholder="Credit Score" />
						</div>

						<div class="col-md-3">

							<label>Credit Score Date:</label>
							<input type="text" class="form-control  datepicker" name="credit_score_date" id="credit_score_date" value="<?php echo isset($fetch_borrower_contact_type[0]->borrower_credit_score_date) ? date('m-d-Y', strtotime($fetch_borrower_contact_type[0]->borrower_credit_score_date)) : ''; ?>" placeholder="Credit Score Date(Format:DD-MM-YYYY)" />
						</div>

					</div>
					<div class="row borrower_data_row borrower_textarea_div">
						<div class="col-md-9">
							<textarea  class="form-control " name="credit_score_desc" placeholder="Credit Score Explanation" id="credit_score_desc"><?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_credit_score_desc : ''; ?></textarea>
						</div>
					</div>
					<div class="row borrower_data_row" id="">
						<div class="col-md-3">

						<label>Marital Status:</label>
						<select class="form-control " name="marital_status">
							<?php foreach ($marital_status as $key => $row) {?>
							<option value="<?php echo $key; ?>" <?php if ($key == $fetch_borrower_contact_type[0]->borrower_marital_status) {echo 'Selected';}?>><?php echo $row; ?></option>
							<?php }?>

						</select>
						</div>

						<div class="col-md-3">

							<label># of Dependents:</label>
							<input  type="text" class="form-control " name="dependents" id="dependents" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_dependents : ''; ?>" placeholder="# of Dependents" />
						</div>


					</div>
			<!-- 	    <div class="row borrower_data_row" id="">
						 <div class="col-md-4">

							 <label>Borrower / Contact OFAC Search:</label>
							 <select class="form-control" name="ofac_search">
								<?php foreach ($no_yes_option as $key => $row) {?>
									<option value="<?php echo $key; ?>" <?php if ($key == $fetch_borrower_contact_type[0]->ofac_search) {echo 'Selected';}?>><?php echo $row; ?></option>
								<?php }?>
							 </select>
						  </div>

					</div>	 -->

					<h4 style="background-color:#efefef;padding: 10px;">Borrower Experience</h4>

					<div class="row borrower_data_row" id="borrower_experience_selectbox">
					    <div class="col-md-3">
						<label>Borrower rehabbed:</label>
						     <select class="form-control" name="borrower_experience" onchange="display_how_may_box(this.value);">
							<?php foreach ($yes_no_option3 as $key => $row) {?>
								<option value="<?php echo $key; ?>" <?php if ($key == $fetch_borrower_contact_type[0]->borrower_experience) {echo 'selected';}?>><?php echo $row; ?></option>
							<?php }?>
						     </select>
						</div>

						<div class="col-md-3" id="how_may_box" style="display:none;">
							<label>If Yes, how many?</label>
							<input type="text" name="ex_how_many" class="form-control" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->ex_how_many : ''; ?>">
						</div>

					</div>


					<h4 style="background-color:#efefef;padding: 10px;">Employment History</h4>

					<div class="row borrower_data_row">

					    <div class="col-md-3">
							 <label>Employment Status:</label>
						     <select class="form-control" name="employment_status">
							<?php foreach ($employment_status as $key => $row) {?>
							<option value="<?php echo $key; ?>" <?php if ($key == $fetch_borrower_contact_type[0]->employment_status) {echo 'Selected';}?>><?php echo $row; ?></option>
							<?php }?>

						     </select>
						</div>

					</div>

				  <div class="row borrower_data_row">


						<div class="col-md-3">
							<label>Current Employer:</label>
							<input type="text" name="current_employer" class="form-control" value="<?php echo isset($fetch_borrower_contact_type) ? ($fetch_borrower_contact_type[0]->current_employer) : ''; ?>">
						</div>

				        <div class="col-md-3">
							<label>Job Title:</label>
							<input type="text" name="job_title" class="form-control" value="<?php echo isset($fetch_borrower_contact_type) ? ($fetch_borrower_contact_type[0]->job_title) : ''; ?>">
						</div>

						<div class="col-md-3">
							<label>Years at Job:</label>
							<input type="text" name="year_at_job" class="form-control" value="<?php echo isset($fetch_borrower_contact_type) ? ($fetch_borrower_contact_type[0]->year_at_job) : ''; ?>">
						</div>
				 </div>
					<!--<div class="row borrower_data_row">

							<div class="col-md-3">
							<label>Annual Income:</label>
							<input type="text" name="annul_income" class="form-control amount_format" value="$<?php echo isset($fetch_borrower_contact_type) ? number_format($fetch_borrower_contact_type[0]->annul_income) : ''; ?>">
						</div>




					</div>-->

					<!--<div class="row borrower_data_row">
						<div class="col-md-3">
							<label>Estimated Income YTD:</label>
							<input type="text" name="estimate_income" class="form-control amount_format" value="$<?php echo isset($fetch_borrower_contact_type) ? number_format($fetch_borrower_contact_type[0]->estimate_income) : ''; ?>">
						</div>
						<div class="col-md-3">
							<label>Year:</label>
							<input type="text" name="estimate_income_year" maxlength="4" class="form-control number_only" value="<?php echo isset($fetch_borrower_contact_type) ? ($fetch_borrower_contact_type[0]->estimate_income_year) : ''; ?>">
						</div>

							<div class="col-md-3">
							<label>Confirmed w/ Paystub:</label>
							<select name="conform_paystub" class="form-control">
							<?php foreach ($yes_no_option3 as $key => $row) {?>
							<option value="<?php echo $key; ?>" <?php if ($key == $fetch_borrower_contact_type[0]->conform_paystub) {echo 'Selected';}?>><?php echo $row; ?></option>
							<?php }?>
							</select>
						</div>

					</div>-->
					<div class="row borrower_data_row">
						<div class="col-md-3">
							<label>Annual Income:</label>
							<input type="text" name="prvious_yr_income" class="form-control amount_format" value="$<?php echo isset($fetch_borrower_contact_type) ? number_format($fetch_borrower_contact_type[0]->prvious_yr_income) : ''; ?>">
						</div>
						<div class="col-md-3">
							<label>Year:</label>
							<input type="text" name="prvious_income_year" maxlength="4" class="form-control number_only" value="<?php echo isset($fetch_borrower_contact_type) ? ($fetch_borrower_contact_type[0]->prvious_income_year) : ''; ?>">
						</div>
						<div class="col-md-3">
							<label>Confirmed w/ Return:</label>
							<select name="conform_return" class="form-control">
							<?php foreach ($yes_no_option3 as $key => $row) {?>
							<option value="<?php echo $key; ?>" <?php if ($key == $fetch_borrower_contact_type[0]->conform_return) {echo 'Selected';}?>><?php echo $row; ?></option>
							<?php }?>
							</select>
						</div>
					</div>

					<h4 style="background-color:#efefef;padding: 10px;">Other Disclosures</h4>

					<div class="col-md-12 borrower_data_row" id="">
						<div class="col-md-8">
							<label>Is the Contact a US citizen? </label>
						</div>
						<div class="col-md-4">
							<select  class="form-control borrower_enabled" name="us_citizen" id="us_citizen" onchange="display_us_citizenNew(this.value)">
								<!--<option value="">SELECT</option>-->
								<?php

foreach ($us_yes_no_option as $key => $row) {
?>
								<option value="<?php echo $key; ?>" <?php if (isset($fetch_borrower_contact_type)) {if ($fetch_borrower_contact_type[0]->borrower_us_citizen == $key) {echo 'selected';}}?>><?php echo $row; ?></option>
								<?php
}
?>

							</select>
						</div>

					</div>



					<div class="row   borrower_data_row borrower_textarea_div us_citizen_optn" style=" display:none" id="">

						<div  class="col-md-3 ">
							<textarea placeholder="If No, Description?" rows="4" name="us_citizen_desc" id="us_citizen_desc"><?php if (isset($fetch_borrower_contact_type)) {echo $fetch_borrower_contact_type[0]->borrower_us_citizen_desc;}?></textarea>
						</div>

					</div>

					<div class="col-md-12 borrower_data_row" id="">
						<div class="col-md-8" >
							<label>Does the contact have Pending Litigation? </label>
						</div>
						<div class="col-md-4">
							<select class="form-control  borrower_enabled" name="litigation" id="litigation" onchange="display_litigation(this.value)">
								<!--<option value="">SELECT</option>-->
								<?php
foreach ($yes_no_option as $key => $row) {
?>
									<option value="<?php echo $key; ?>" <?php if (isset($fetch_borrower_contact_type)) {if ($fetch_borrower_contact_type[0]->borrower_litigation == $key) {echo 'selected';}}?>><?php echo $row; ?></option>
									<?php
}
?>

							</select>
						</div>

					</div>


					<div class="row  borrower_data_row borrower_textarea_div litigation_optn" style=" display:none" id="">

						<div  class="col-md-3 ">
							<label>If Yes, explain:</label>
							<textarea placeholder="If Yes, explain?" rows="5" name="litigation_desc" id="litigation_desc"><?php if (isset($fetch_borrower_contact_type)) {echo $fetch_borrower_contact_type[0]->borrower_litigation_desc;}?></textarea>
						</div>

					</div>


					<div class="col-md-12 borrower_data_row">
						<div class="col-md-8" >
							<label>Has the Contact been convicted of a felony?</label>
						</div>

						<div class="col-md-4">
							<select class="form-control  borrower_enabled" name="felony" id="felony" onchange="display_felony(this.value)">
								<!--<option value="">SELECT</option>-->
								<?php
foreach ($yes_no_option as $key => $row) {
?>
									<option value="<?php echo $key; ?>" <?php if (isset($fetch_borrower_contact_type)) {if ($fetch_borrower_contact_type[0]->borrower_felony == $key) {echo 'selected';}}?>><?php echo $row; ?></option>
									<?php
}
?>

							</select>
						</div>

					</div>



					<div class="row  borrower_data_row borrower_textarea_div felony_optn" style=" display:none" id="">

						<div class="col-md-3">
							<label>If Yes, explain:</label>

							<textarea placeholder="If Yes, explain?" rows="5" name="felony_desc" id="felony_desc"><?php if (isset($fetch_borrower_contact_type)) {echo $fetch_borrower_contact_type[0]->borrower_felony_desc;}?></textarea>
						</div>

					</div>


					<div class="col-md-12 borrower_data_row">
						<div class="col-md-8" >
							<label>Has the Contact declared bankruptcy in last 7 years?</label>
						</div>

						<div class="col-md-4" >
							<select class="form-control borrower_enabled" name="Bankruptcy_year" onchange="display_bankruptcy_7year(this.value);" id="text_7year">

								<?php
foreach ($yes_no_option as $key => $row) {
?>
									<option value="<?php echo $key; ?>" <?php if (isset($fetch_borrower_contact_type)) {if ($fetch_borrower_contact_type[0]->bankruptcy_year == $key) {echo 'selected';}}?>><?php echo $row; ?></option>
									<?php
}
?>

							</select>
						</div>
					</div>
					<div class="row borrower_data_row borrower_textarea_div bankruptcy_7year_text" style ="display:none">
						<div class="col-md-3">
							<textarea type="text" rows="2" name="bankruptcy_7year_text" ><?php if (isset($fetch_borrower_contact_type)) {echo $fetch_borrower_contact_type[0]->bankruptcy_7year_text;}?></textarea>
						</div>
					</div>

					<div class="col-md-12 borrower_data_row">
						<div class="col-md-8" >
							<label>Has the Contact declared bankruptcy in last 12 months?</label>
						</div>
						<div class="col-md-4" >
							<select class="form-control borrower_enabled" name="Bankruptcy" id="Bankruptcy" onchange="display_BK(this.value)">

								<?php
foreach ($yes_no_option as $key => $row) {
?>
									<option value="<?php echo $key; ?>" <?php if (isset($fetch_borrower_contact_type)) {if ($fetch_borrower_contact_type[0]->borrower_bankruptcy == $key) {echo 'selected';}}?>><?php echo $row; ?></option>
									<?php
}
?>

							</select>
						</div>
					</div>

					<div class="col-md-12 borrower_data_row">
						<div class="col-md-8" >
							<label>OFAC Search Completed / Saved1</label>
						</div>
						<div class="col-md-4" >

							 <select class="form-control" name="ofac_srch_completed">
								<?php foreach ($ofac_srch_option as $key => $row) {?>
									<option value="<?php echo $key; ?>" <?php if ($key == $fetch_borrower_contact_type[0]->ofac_srch_completed) {echo 'Selected';}?>><?php echo $row; ?></option>
								<?php }?>
							 </select>
						</div>
					</div>

					<div class="row  borrower_data_row" id="">
						<div style="display:none" class="col-md-3 BK_optn">
							<label>Date Dismissed: </label>
						</div>
						<div  style="display:none" class="col-md-3 BK_optn">
							<input type="text" class="form-control lender_enabled input-md datepicker" name="date_dismissed" value="<?php echo isset($fetch_borrower_contact_type) ? date('m-d-Y', strtotime($fetch_borrower_contact_type[0]->borrower_date_dismissed)) : ''; ?>" placeholder="MM-DD-YYYY" />
						</div>
					</div>
					<div class="row  borrower_data_row" id="">
						<div style="display:none" class="col-md-3 BK_optn">
							<label>If Yes, has BK been dismissed?: </label>
						</div>
						<div style="display:none" class="col-md-3 BK_optn">
							<select  class="form-control  borrower_enabled" name="BK" id="BK" onchange="display_bk_txtarea(this.value);">
								<?php foreach ($yes_no_option as $key => $row) { ?>
									<option value="<?php echo $key; ?>" <?php if (isset($fetch_borrower_contact_type)) {if ($fetch_borrower_contact_type[0]->borrower_BK == $key) {echo 'selected';}}?>><?php echo $row; ?></option>
								<?php } ?>														
							</select>
						</div>
					</div>
					<div style= "display:none;" class="row borrower_data_row borrower_textarea_div BK_textarea_div">
						<div class="col-md-3">
							<label>If Yes, BK Explanation:</label>

							<textarea  placeholder="If Yes, BK Explanation" rows="5" class="form-control borrower_enabled" name="bk_desc" id="bk_desc"><?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_BK_desc : ''; ?></textarea>
						</div>
					</div>
				</div>
				<!----- Borrower Contact Type DIV ENDS   -------->
			</div>

			<div class="modal-footer">
				<input type="hidden" name="popup_data" value="borrower_data">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<button type="submit" name="modal_btn" value="save" class="btn blue">Save</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>