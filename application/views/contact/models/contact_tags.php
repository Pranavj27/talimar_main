<div class="modal fade bs-modal-lg" id="contact_tags" tabindex="-1" role="large" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Contact Tags:</h4>
			</div>
			<?php

				foreach (array_values(array_filter($fetch_contact_type)) as $con_type) {
					$aa[$con_type] = $con_type;
				}
				foreach (array_values(array_filter($fetch_contact_specialty)) as $con_specialty) {
					$bb[$con_specialty] = $con_specialty;
				}

				foreach (array_values(array_filter($fetch_contact_email_blast)) as $con_email_blast) {
					$dd[$con_email_blast] = $con_email_blast;
				}

				foreach ($fetch_contact_permission as $con_permission) {
					$ee[$con_permission] = $con_permission;
				}

			?>

			<div class="modal-body">
				<div class="row ">
					<div class="col-md-3">
						<label style=""><b>Specialty</b></label>
					</div>
				</div>
				<div class="row contact_tags_row">
					<?php

					foreach ($contact_specialty as $key => $specialty) {

						if ($bb[$key] == $key) {
							$checked = 'checked="checked"';
						} else {
							$checked = '';
						}
						?>
						<input type="hidden" value="<?php echo $fetch_contact_specialty_tag_id[$key]; ?>" name="specialty_tag_id[]"/>
						<div class="col-md-3">
							<input type="checkbox" <?php echo $checked; ?> class="form-control" name="contact_specialty[]" value="<?php echo $key; ?>"/>&nbsp;&nbsp;<?php echo $specialty; ?>
						</div>

					<?php	}?>


				</div>

				<div class="row ">
					<div class="col-md-3">
						<label style=""><b>E-Mail Blast</b></label>
					</div>
				</div>
				<div class="row contact_tags_row">
					<?php

                        foreach ($contact_email_blast as $key => $email_blast) {
                        	if ($dd[$key] == $key) {
                        		$checked = 'checked="checked"';
                        	} else {
                        		$checked = '';
                        	}
                        	?>
						<input type="hidden" value="<?php echo $fetch_contact_email_blast_tag_id[$key]; ?>" name="email_blast_tag_id[]"/>
						<div class="col-md-8">
							<input type="checkbox" <?php echo $checked; ?> class="form-control" name="contact_email_blast[]" value="<?php echo $key; ?>"/>&nbsp;&nbsp;<?php echo $email_blast; ?>
						</div>

					<?php	}?>


				</div>

				
			</div>

			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<button type="submit"  class="btn blue">Save</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>