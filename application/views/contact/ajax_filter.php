<?php 
$contact_specialty = $this->config->item('contact_specialty');
$contact_email_blast     = $this->config->item('contact_email_blast');
$contact_record_limit    = $this->config->item('contact_record_limit');
$contact_marketing_data  = $this->config->item('contact_marketing_data');
?>
<style>
</style>
<div class="page-container contactlist-outmost">
    <div class="container">
        <div class="tab-pane">

            <div class="page-head">
                <form action="" method="post" name="form_filter_options" id="form_filter_options">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title filter_page_title">
                    <h1>Tag Filter <small></small></h1>
                </div>
                <a href="#" id="ct_apply_filter" class="btn btn-primary ct_apply_filter bu_Block">Filter</a>
                
                <div class="row contactlist_F"><div class="col-lg-12"><div class="filter_Head"><h4>Contact Specialty:  <span class="outer-chkbx"><input onclick = "check_all_filter()" type = "checkbox" id = "select_all" />Select All </span><span class="outer-chkbx"><input onclick = "uncheck_all_filter()" type = "checkbox" id = "deselect_all" />DeSelect All</span></h4></div></div></div><div class="row">
                <?php
                
                foreach ($contact_specialty as $key => $specialty) {
                    echo '
					<span class="col-lg-3">
					<div class="list_filter_SpecialtyF">
					<input type = "hidden" value = "' . $key . '" id = "contact_specialty_val_' . $key . '" name = "contact_specialty_val[]"/>
					
					<input type = "hidden" class = "tag_fields" value = "0" id = "contact_specialty_' . $key . '" name = "contact_specialty[]"/>
					
					<input id = "contact_specialty_tag_' . $key . '" class = "all_tags contactSpecialty" title="'.$specialty.'"  type = "checkbox" value="'.$key.'"  onclick = tag_filter(' . $key . ',"contact_specialty",this.id) /><label style = "cursor:pointer;" > ' . $specialty . '</label>
                    </div>
                   </span>';
                }
                ?>
                </div>
                <div class="row contactlist_F"><div class="col-lg-12"><div class="filter_Head"><h4>Marketing Data:  <span class="outer-chkbx"><input onclick = "check_all_filter_marketingData()" type = "checkbox" id = "select_all_marketingData" />Select All </span><span class="outer-chkbx"><input onclick = "uncheck_all_filter_marketingData()" type = "checkbox" id = "deselect_all_marketingData" />DeSelect All</span></h4></div></div></div><div class="row">
                <?php
                
                foreach ($contact_marketing_data as $key => $Marketing) {
                    echo '
                    <span class="col-lg-3">
                    <div class="list_filter_SpecialtyF">
                    <input type = "hidden" value = "' . $key . '" id = "contact_Marketing_val_' . $key . '" name = "contact_Marketing_val[]"/>
                    
                    <input type = "hidden" class = "tag_fields_marketingData" value = "0" id = "contact_Marketing_' . $key . '" name = "contact_Marketing[]"/>
                    
                    <input id = "contact_Marketing_tag_' . $key . '" class = "all_tags_marketingData contactMarketing" title="'.$Marketing.'"  type = "checkbox" value="'.$key.'"  onclick = tag_filter(' . $key . ',"contact_Marketing",this.id) /><label style = "cursor:pointer;" > ' . $Marketing . '</label>
                    </div>
                   </span>';
                }
                ?>
                </div><div class="row"><div class="col-lg-12"><div class="filter_Head"><h4>Marketing Blast:  <span class="outer-chkbx"><input onclick = "check_all_filter_marketing()" type = "checkbox" id = "select_all_marketing" />Select All  </span><span class="outer-chkbx"><input onclick = "uncheck_all_filter_marketing()" type = "checkbox" id = "deselect_all_marketing" />DeSelect All</span></h4></div></div></div><div class="row">
                <?php

                // hide two element from this array...
                unset($contact_email_blast[10]);
                unset($contact_email_blast[11]);
                unset($contact_email_blast[8]);

                foreach ($contact_email_blast as $key => $email_blast) {

                    echo '
                      <span class="col-lg-3">
                        <div class="list_filter_SpecialtyF">
    						<input type = "hidden" value = "' . $key . '" id = "contact_email_blast_val_' . $key . '" name = "contact_email_blast_val[]" />
    						<input type = "hidden" class = "tag_fields_marketing" value = "0" id = "contact_email_blast_' . $key . '" name = "contact_email_blast[]"/>

    						<input id = "contact_email_blast_tag_' . $key . '"   class = "all_tags_marketing contact_emailBlast" value="'.$key.'" title="'.$email_blast.'"  type = "checkbox" onclick = tag_filter(' . $key . ',"contact_email_blast",this.id) /><label style = "cursor:pointer;" > ' . $email_blast . '</label>
                            </div>
    					</span>';
                }
                ?>

                </div>


                <div class="row"><div class="col-lg-12">
                <div class="filter_Head"><h4>Lender Data:</h4></div></div></div><div class="row">

                <div class="col-md-3">
                        <label>Contacts w/ Lender Account:</label>
                        <select class="chosen" name="lenderwithacc" id="lenderwithacc">
                            <option value="">Select One</option>
                            <?php foreach($fetch_lender_option as $ldata){ ?>
                                <option value="<?php echo $ldata['id'];?>"><?php echo $ldata['text'];?></option>
                            <?php } ?>
                        </select>
                </div>

                </div>


                <div class="row"><div class="col-lg-12">
                <div class="filter_Head"><h4>Borrower Data:</h4></div></div></div><div class="row">

               <div class="col-md-3">
                        <label>Contacts w/ Borrower Account:</label>
                        <select class="chosen" name="borrowerwithacc" id="borrowerwithacc">
                            <option value="">Select One</option>
                            <?php foreach($fetch_borrower_option as $bdata){ ?>
                                <option value="<?php echo $bdata['id'];?>"><?php echo $bdata['text'];?></option>
                            <?php } ?>
                        </select>
                </div>
                <input type="hidden" name="cp_contact_specialty" id="cp_contact_specialty" value="">
                <input type="hidden" name="cp_contact_marketing_data" id="cp_contact_marketing_data" value="">
                <input type="hidden" name="cp_contact_email_blast" id="cp_contact_email_blast" value="">
                </form>
            </div>
                <a href="#" id="ct_apply_filter_b" class="btn btn-primary ct_apply_filter bu_Block">Filter</a>
                
            </div>
        </div>
    </div>
</div>
