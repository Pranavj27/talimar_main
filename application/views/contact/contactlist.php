<?php
$STATE_USA 					= $this->config->item('STATE_USA');
$search_by_optionn 			= $this->config->item('search_by_optionn');
$borrower_type_option 		= $this->config->item('borrower_type_option'); 
$company_type_option 		= $this->config->item('company_type_option');
$marital_status 			= $this->config->item('marital_status');
$yes_no_option 				= $this->config->item('yes_no_option');
$notes_type 				= $this->config->item('notes_type');
$contact_type 				= $this->config->item('contact_type');
$contact_specialty 			= $this->config->item('contact_specialty');
$contact_specialty_trust 	= $this->config->item('contact_specialty_trust');
$contact_email_blast 		= $this->config->item('contact_email_blast');
$contact_permission 		= $this->config->item('contact_permission');
$contact_marketing_data 	= $this->config->item('contact_marketing_data');
		
//for loan originator...
if($this->session->userdata('user_role') == 1){
	$check_setting = $this->User_model->query("SELECT * FROM `user_settings` WHERE `user_id`='".$this->session->userdata('t_user_id')."' AND `loan_organization`='1'");
	if($check_setting->num_rows() > 0){
		$loginuser_dashboard = 2;
	}else{
		$loginuser_dashboard = 1;
	}
}else{
	$loginuser_dashboard = 1;
}
?>

<div class="page-container contactlist-outmost outmost_ajax_table">
	<!-- BEGIN PAGE HEAD -->
	<div class="container">
		<div class="tab-pane">
			<!--------------------MESSEGE SHOW-------------------------->
			<?php if($this->session->flashdata('error')!=''){ ?>
			<div id='error'><i class='fa fa-thumbs-o-down'></i>
				<?php echo $this->session->flashdata('error');?>
			</div>
			<?php } ?>
			<?php if($this->session->flashdata('success')!=''){ ?>
			<div id='success'><i class='fa fa-thumbs-o-up'></i> 
				<?php echo $this->session->flashdata('success');?>
			</div>
			<?php } ?>
			<div id="chkbox_err"></div>
			<!-------------------END OF MESSEGE-------------------------->
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>Contacts <small></small></h1>
				</div>
				
				<div class="row">
					<div class="col-md-12" id="tagitem">
						<?php foreach($contact_specialty_array as $row){ ?>	<span id="spe_<?php echo $row;?>"><?php echo $contact_specialty[$row];?>
								<a onclick="remove_this_selecttag('<?php echo $row;?>')">
								<i class="fa fa-times" aria-hidden="true"></i>
								</a>
							</span>
						<?php } ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" id="tagitem1">
						<?php foreach($contact_market_array as $row){ ?>	<span id="ebla_<?php echo $row;?>"><?php echo $contact_email_blast[$row];?>
								<a onclick="remove_this_selecttag1('<?php echo $row;?>')">
								<i class="fa fa-times" aria-hidden="true"></i>
								</a>
							</span>
						<?php } ?>
					</div>
				</div>
				<div id="messagesChange"></div>
				<!-- END PAGE HEAD -->
				<div class="page-container">
					<div class="contactlist-res-div">
						<!-- BEGIN PAGE BREADCRUMB -->
						<ul class="page-breadcrumb breadcrumb hide">
							<li>	<a href="#">Home</a><i class="fa fa-circle"></i>
							</li>
							<li class="active">Dashboard</li>
						</ul>
						<!-- END PAGE BREADCRUMB -->
						<input type="hidden" name="borower_contact_type_id" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_contact_id : ''; ?>">
						<input type="hidden" name="lender_contact_type_id" value="<?php echo isset($fetch_lender_contact_type) ? $fetch_lender_contact_type[0]->lender_contact_id : ''; ?>">
						<input type="hidden" name="contact_id" value="<?php echo isset($contact_id) ? $contact_id : '';?>">


						<div class="pro_Head">
							<div class="row">									
								<form id="contact_search" action="<?php echo base_url();?>contactlist" method="post">
									<div class="col-sm-3">
										<label><span class="search_title">Search By:</span></label>
										<select name="search_by" onchange="click_this()" class="form-control">
										<?php foreach($search_by_optionn as $key=>$row){ 
											if($key != 3){
											?>
											<option value="<?php echo $key; ?>" <?php if($key==$selected_byy){echo 'Selected';}?>>
												<?php echo $row; ?>
											</option>
										<?php } } ?>
									</select>
									</div>

									<div class="col-sm-3">
										<label>&nbsp;</label>
										<select name="search" onchange="submit_this(this.value)" class="chosen">
									<option value=''>Search Contact</option>
									<?php if($selected_byy=='3' ){ foreach($fetch_search_option as $key=>$row ){ ?>
									<option value="<?php if($fetch_search_option[$key]['text']!=''){ echo $fetch_search_option[$key]['text'];}else{echo $fetch_search_option[$key]['text2'];}?>" <?php if($fetch_search_option[$key][ 'id']){ if($fetch_search_option[$key][ 'id']==$loan_id){ echo 'selected'; } } ?>>
										<?php if($fetch_search_option[$key][ 'text']) { echo $fetch_search_option[$key][ 'text'];} else{ echo $fetch_search_option[$key][ 'text2'];}?>
									</option>
									<?php } } else{ foreach($fetch_search_option as $key=>$row ){ ?>
									<option value="<?php echo $fetch_search_option[$key]['id'];?>" <?php if($fetch_search_option[$key][ 'id']){ if($fetch_search_option[$key][ 'id']==$loan_id){ echo 'selected'; } } ?>>
										<?php if($fetch_search_option[$key][ 'text']) { echo $fetch_search_option[$key][ 'text'];} else{ echo $fetch_search_option[$key][ 'text2'];}?>
									</option>
									<?php } }?>
								</select>
									</div>
									<input style="display:none;" type="submit" value="Go">
									</form>
								<div class="col-sm-3">
									<input type="hidden" name="internal_contact_ajax" id="internal_contact_ajax" value="">
									&nbsp;
								</div>							
								<div class="col-sm-3">
									<label class="search_titled">Company Name:</label>
									<select name="company_name_ajax" id="company_name_ajax" class="form-control chosen">
										<option value="">Select All</option>
										<?php
										foreach($company_name_list as $key=>$row ){ 
											$company_nameView = '';
											if(trim($row['text']))
											{
												$company_nameView = trim($row['text']); 
											}else if(trim($row['text2']))
											{
												$company_nameView = trim($row['text2']); 
											}

											if($company_nameView)
											{
											?>
											<option value="<?php echo $company_nameView;?>"><?php  echo $company_nameView; ?></option>
											<?php 
											}
										}
										?>
									</select>
								</div>
								
								
							</div>										
							
							
						
						</div>	

						<div class="contactlist_left filter_Upp" id="contactlist_div">
							<div class="row">							
							<div class="col-md-12">
								<div id="b_line" align="left" class="col-Fil">


									<div class="row">
										<div class="col-sm-4">
											<div class="main_select_div">					
												<div class="main_select_div-btn-1" >
													<a href="<?php echo base_url().'contact';?>" class="btn btn-primary">Add Contact</a>
												</div>
												<div class="search_contact">
													<a href="<?php echo base_url().'Contact/download_contactlist';?>" id="download_contactlist_csv" class="btn btn-success">Excel</a>
													&nbsp;&nbsp;
													<a href="<?php echo base_url().'Contact/download_pdf';?>" id="download_contactlist_pdf" class="btn btn-danger">PDF</a>

												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="row">
												<div class="col-sm-12" class="filter_tag_main_title" style="display: none;"><h4>Showing results for</h4></div>
												<div class="col-sm-3 filter_tag_contact_specialty filter_tag_contact_list" style="display: none;">
														<div class="ul_Outer">
														<label>Contact Specialty</label>
														<ul></ul>
													</div>
												</div>

												<div class="col-sm-3 filter_tag_marketing_blast filter_tag_contact_list" style="display: none;">
													<div  class="ul_Outer">
														<label>Marketing Blast</label>
														<ul></ul>
													</div>
												</div>

												<div class="col-sm-3 filter_tag_lender_data filter_tag_contact_list" style="display: none;">
													<div class="ul_Outer">
														<label>Lender Data</label>
														<ul></ul>
													</div>
												</div>

												<div class="col-sm-3 filter_tag_borrower_data filter_tag_contact_list" style="display: none;">
													<div class="ul_Outer">
														<label>Borrower Data</label>
														<ul></ul>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-2">
											<a href="#" class="ct_filter_contacts" alt="Fiter options">
												<i class="fa fa-filter icons_active_filter" aria-hidden="true"></i>
												<i class="fa fa-times icons_hide_filter" aria-hidden="true"></i>
											</a>
											<a href="#" class="ct_reset_filter_contacts" alt="Reset Fiter options">
												<i class="fa fa-refresh" aria-hidden="true"></i>
											</a>
										</div>
									</div>
								</div>
							</div>
							</div>
							<div class="col-md-12">
								<div class="content_filter_contacts" style="display: none;">
									<?php echo $content_filter; ?>
								</div>
							</div>
							<div class="table-upper-section ajax_table_talimar table_All">
								<table id="ajax_table_contactlist" class="table-responsive table table-bordered table-striped table-condensed flip-content dataTable no-footer">
									<thead>
										<tr>
											<th>Last Name</th>
											<th>First Name</th>
											<th>Company Name</th>
											<th>Phone</th>
											<th>Email</th>
											<th>Added</th>
											<th>Modified</th>
											<th width="15%">Action</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript" src="<?php echo base_url("assets/js/contact/contactlist.js"); ?>"></script>
