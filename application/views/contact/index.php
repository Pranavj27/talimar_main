<?php
$STATE_USA = $this->config->item('STATE_USA');
$borrower_type_option = $this->config->item('borrower_type_option');
$company_type_option = $this->config->item('company_type_option');
$marital_status = $this->config->item('marital_status');
$yes_no_option = $this->config->item('yes_no_option');
$yes_no_option1 = $this->config->item('yes_no_option1');
$contact_type = $this->config->item('contact_type');
$contact_specialty = $this->config->item('contact_specialty');
$contact_email_blast = $this->config->item('contact_email_blast');
$contact_permission = $this->config->item('contact_permission');
$all_country = $this->config->item('all_countary');
$referral_source = $this->config->item('referral_source');
$contact_specialty_trust = $this->config->item('contact_specialty_trust');
$contact_number_option = $this->config->item('contact_number_option');
$email_types_option = $this->config->item('email_types_option');
$contact_marketing_data = $this->config->item('contact_marketing_data');
$portal_access_option = $this->config->item('portal_access_option');

?>
<style>


.e{
border: 1px solid red;

}
.main_title h4{
	background-color: #e8e8e8;
	height: 30px;
	line-height: 30px;
}
.main_select_div{

	float: right;

}
#borrower_data .borrower_textarea_div label{
	padding-left:0px !important;
}
#borrower_data .borrower_data_row ,#lender_data .lender_data_row {

	    padding: 11px 2px !important;
}
.tab-pane{
	margin-bottom:20px;
}
.user_content{
	padding: 10px 25px;
}
.search_contact {

	float:right;
}
.same_check #uniform-same_mailing_chkbx{
	width:7% !important;
	float:left;
}
.row.contact_tags_row {
    padding-left: 40px;
	padding-bottom: 15px;
}
.contact_tags_row .col-md-8{
	padding:5px;
}
.contact_tags_row .col-md-3{
	padding:5px;
}
.err_message{
	color: red;
    font-size: 14px;
	float: left;
    padding-left: 180px;
    padding-bottom: 4px;
}
.div1 {
    width: 50%;
    float: left;
    border-right: 1px solid silver;
}
.div2 {
    width: 50%;
    float: left;

}
.col-md-3.same_check {
    float: left;
	margin-top: 13px;
}
div#margin_left {
    margin-left: 15px;
    word-spacing: 1.15pt;
}
div#margin_left1 {
    margin-left: 15px;
    word-spacing: 1.15pt;
}
div#margin_left2 {
    margin-left: 15px;
    word-spacing: 1.15pt;
}
div.checker, div.checker span, div.checker input {
    width: 16px !important;
    height: 20px !important;
}
.col-md-3.d {
    position: relative;
    top: 94px;
}
</style>
<div class="page-container">

	<!-- BEGIN PAGE HEAD -->

	<div class="container">
		<div class="tab-pane">
			<!--------------------MESSEGE SHOW-------------------------->
			<?php if ($this->session->flashdata('error') != '') {?>
				<div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error'); ?></div><?php }?>
			<?php if ($this->session->flashdata('success') != '') {?>
				<div id='success'><i class='fa fa-thumbs-o-up'></i> <?php echo $this->session->flashdata('success'); ?></div><?php }?>
			<div id = "chkbox_err"></div>
			<!-------------------END OF MESSEGE-------------------------->
			<!-- BEGIN PAGE HEAD -->
			 <?php echo validation_errors(); ?>
			<form method="POST" action="<?php echo base_url(); ?>Contact/add_contact_data" id="formID" onsubmit="return data_check()">
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>Contact <small></small></h1>
				</div>

				<div class = "main_select_div" >


					<!------------------Buttons Section--------------------------->
					<div class="row margin-top-10">
						<div class="lender_data_row">
							<div class="form-group">

							  <div class="col-md-12">
								<input id = "formsubmitShow" value = "Save" type = "submit"  class = "btn btn-primary"  />
								<input id = "formsubmitHide" value = "Save" type = "button"  class = "btn btn-primary" style="display: none;"/>

								<a class="btn btn-primary" data-toggle="modal" href="#edit_contact">Close</a>

							  </div>
							</div>
						</div>
					</div>
					<!------------------End Buttons Section--------------------------->

				</div>
			</div>
			<!-- END PAGE HEAD -->
			<div class="page-container">

				<!-- BEGIN PAGE BREADCRUMB -->
				<ul class="page-breadcrumb breadcrumb hide">
					<li>
						<a href="#">Home</a><i class="fa fa-circle"></i>
					</li>
					<li class="active">
						 Dashboard
					</li>
				</ul>
				

				<div class="row borrower_data_row main_title" id="">

				<div class="errorrr alert alert-danger" style="display:none;"></div>

			<?php if ($this->session->flashdata('email') != '') {?>
				<div class='email alert alert-danger'> <i class='fa fa-thumbs-o-down'> </i> <?php echo $this->session->flashdata('email'); ?></div><?php }?>



							<h4>Primary Contact Information :&nbsp;&nbsp;

							</h4>

				</div>
					<div class="col-md-12 err_message">

					</div>

				<!--<form method="POST" enctype="multipart/form-data" action = "<?php echo base_url(); ?>Contact/add_contact_data" id="formID">-->
					<input type="hidden" name="borower_contact_type_id" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_contact_id : ''; ?>">

					<input type="hidden" name="lender_contact_type_id" value="<?php echo isset($fetch_lender_contact_type) ? $fetch_lender_contact_type[0]->lender_contact_id : ''; ?>">

					<input type="hidden" name="contact_id" id="contact_id" value="<?php echo isset($contact_id) ? $contact_id : ''; ?>" >

					<div class="row borrower_data_row" id="">
						<div class="col-md-3">
							<label>Prefix:</label>
							<input type="text" class="form-control" name="prefix" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->prefix : ''; ?>" placeholder = "Prefix"/>
						</div>
						<div class="col-md-3">
							<label>Suffix:</label>
							<input type="text" class="form-control" name="suffix" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->suffix : ''; ?>" placeholder = "Suffix"/>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-12">&nbsp;</div>
						<div class="col-md-3">
							<label>First Name:</label>

							<input type="text"  class="form-control" name="first_name" id = "first_name" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_firstname : set_value('first_name'); ?>" placeholder = "First Name" />
						</div>


						<div class="col-md-3">
							<label>Middle Initial:</label>
							<input type="text" class="form-control" name="middle_name" id = "middle_name" value= "<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_middlename : ''; ?>" placeholder = "MiddleName" />
						</div>

						<div class="col-md-3">
							<label>Last Name:</label>
							<input type="text"    class="form-control"  name="last_name" id = "last_name" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_lastname : ''; ?>" placeholder = "Last Name"/>
						</div>
						
						<div class="col-md-3">
							<label>Legal First Name:</label>

							<input type="text" class="form-control" name="contact_suffix" id = "contact_suffix" value = "<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_suffix : ''; ?>" placeholder = "Legal First / Middle Name" />
						</div>

					</div>

					<!--
					<div class="row borrower_data_row" id="">
						<div class="col-md-3">
							<input type="text" class="form-control" name="title" id = "title" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_title : ''; ?>" placeholder = "Title" />
						</div>
					</div>
					-->

					<div class="row borrower_data_row" >


						<div class="col-md-3">
							<label>Primary Phone #:</label>
							<input type="text" class="form-control phone-format" name="phone" id = "phone" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_phone : ''; ?>" placeholder = "Primary Phone" />
						</div>

						<div class="col-md-3">
							<label>Phone Type:</label>
							<select name="primary_option" class="form-control">
							<?php foreach ($contact_number_option as $key => $row) {?>
								<option value="<?php echo $key; ?>" <?php if ($fetch_contact_data[0]->primary_option == $key) {echo 'selected';}?>><?php echo $row; ?></option>
							<?php }?>
							</select>

						</div>
						<div class="col-md-3">
							<label>Phone Extension:</label>
							<input type="text" name="phone_extension" id="phone_extension" class="form-control" value="<?php if(!empty($fetch_contact_data[0]->phone_extension)) { echo $fetch_contact_data[0]->phone_extension; }?>">

						</div>

						<div class="col-md-3">
							
							<!--<select class="form-control" name="addPhoneopt" onchange="hideandshow(this.value);">
								<?php foreach($yes_no_option1 as $key => $row){?>
									<option value="<?php echo $key;?>" <?php if ($fetch_contact_data[0]->addPhoneopt == $key) {echo 'selected';}?>><?php echo $row;?></option>
								<?php } ?>
							</select>-->
							<a onclick="hideandshow(this,'<?php echo isset($fetch_contact_data[0]->addPhoneopt) ? $fetch_contact_data[0]->addPhoneopt : '0';?>');" class="btn btn-primary btn-sm" style="float:left;"><i class="fa fa-plus"></i></a>
						</div>

						<script type="text/javascript">
							
							//hideandshow(this,'<?php echo $fetch_contact_data[0]->addPhoneopt;?>');
							function hideandshow(that,key){

								//if(key == 2){
									$('#divdisplay').css('display','block');
								//}else{
									//$('#divdisplay').css('display','none');
								//}

							} 

							function removeExtradata(that,key){

								if(key == 'phone'){

									$('#divdisplay input[name="alter_phone"]').val('');
									$('#divdisplay').remove();

								}else if(key == 'email'){

									$('#divdisplayemail input[email="email2"]').val('');
									$('#divdisplayemail').remove();
								}else if(key == 'company'){

									$('#othercompany input[email="company_name_2"]').val('');
									$('#othercompany').remove();
								}else{

								}
							}
						</script>

						<!--<div class="col-md-3">
							<label>Secondary Phone:</label>
							<input type="text" class="form-control phone-format" name="secondary_phone" id = "phone" value="<?php// echo isset($contact_id) ? $fetch_contact_data[0]->contact_secondary_phone : ''; ?>" placeholder = "Secondary Phone" />
						</div>-->

						<!--<div class="col-md-3">
							<input type="text" class="form-control datepicker" name="dob" id = "dob" value="<?php if (isset($contact_id)) {echo $fetch_contact_data[0]->contact_dob ? date('m-d-Y', strtotime($fetch_contact_data[0]->contact_dob)) : '';}?>" placeholder = "DOB(Format:MM-DD-YYYY)" />
						</div>-->


					</div>
					<?php

					//echo 'addopt:'. $fetch_contact_data[0]->addPhoneopt;
						if($fetch_contact_data[0]->addPhoneopt == '2'){
							$addcssaa = 'style="display: block;"';
						}else{
							$addcssaa = 'style="display: none;"';
						}
					?>
					<div class="row borrower_data_row" id="divdisplay" <?php echo $addcssaa;?>>

						<div class="col-md-3">
							<label>Secondary Phone #:</label>
							<input type="text" class="form-control phone-format" name="alter_phone" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->alter_phone : ''; ?>" placeholder = "Alternative Phone" />
						</div>
						<div class="col-md-3">
							<label>Secondary Phone Type:</label>
							<select name="alter_option" class="form-control">
							<?php foreach ($contact_number_option as $key => $row) {?>
								<option value="<?php echo $key; ?>" <?php if ($fetch_contact_data[0]->alter_option == $key) {echo 'selected';}?>><?php echo $row; ?></option>
							<?php }?>
							</select>

						</div>
						<div class="col-md-3">
							<label>Secondary Phone Extension:</label>
							<input type="text" name="alter_phone_extension" id="alter_phone_extension" class="form-control" value="<?php if(!empty($fetch_contact_data[0]->alter_phone_extension)) { echo $fetch_contact_data[0]->alter_phone_extension; }?>">

						</div>
						<div class="col-md-3">
							<a title="Remove" class="btn btn-danger btn-sm" onclick="removeExtradata(this,'phone');"><i class="fa fa-trash"></i></a>
						</div>

					</div>

					<div class="row borrower_data_row" >

						<div class="col-md-3">
							<label>E-Mail:</label>
							<input type="email"  onkeyup= " return check_duplicacy(this.value,'contact_email')" class="form-control" name="email" id = "email" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_email : ''; ?>" placeholder = "E-mail">

							<input type="hidden"  class="form-control" name="email_hidden" id = "email_hidden" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_email : ''; ?>" placeholder = "E-mail" />
						</div>

						<div class="col-md-3">
							<label>E-Mail Type:</label>
							<select class="form-control" name="pemail_type">
								<?php foreach ($email_types_option as $key => $row) {?>
									<option value="<?php echo $key; ?>" <?php if ($fetch_contact_data[0]->pemail_type == $key) {echo 'selected';}?>><?php echo $row; ?></option>
								<?php }?>
							</select>
						</div>

						<div class="col-md-3">
							
							<!--<select class="form-control" name="addemailopt" onchange="hideandshowemail(this.value);">
								<?php foreach($yes_no_option1 as $key => $row){?>
									<option value="<?php echo $key;?>" <?php if ($fetch_contact_data[0]->addemailopt == $key) {echo 'selected';}?>><?php echo $row;?></option>
								<?php } ?>
							</select>-->

							<a onclick="hideandshowemail(this,'<?php echo isset($fetch_contact_data[0]->addemailopt) ? $fetch_contact_data[0]->addemailopt : '0';?>');" class="btn btn-primary btn-sm" style="float:left;"><i class="fa fa-plus"></i></a>
						</div>


						<script type="text/javascript">
							
							//hideandshowemail('<?php echo $fetch_contact_data[0]->addemailopt;?>');
							function hideandshowemail(that,key){
								//alert(that);
								//if(that == '2'){
									$('#divdisplayemail').css('display','block');
								//}else{
									//$('#divdisplayemail').css('display','none');
								//}
							} 

							function hideandshowcompany(that,key){

								$('#othercompany').css('display','block');
							}

						</script>

					</div>
					<?php

						if($fetch_contact_data[0]->addemailopt == '2'){
							$addcss = 'style="display: block;"';
						}else{
							$addcss = 'style="display: none;"';
						}
					?>

					<div class="row borrower_data_row" id="divdisplayemail" <?php echo $addcss;?>>
						<div class="col-md-3">
							<label>Secondary E-Mail:</label>
							<input type="email"  onchange = "check_duplicacy(this.value,'contact_email2')" class="form-control" name="email2" id = "email2" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_email2 : ''; ?>" placeholder = "E-mail 2" />

							<input type="hidden"  class="form-control" name="email_hidden2" id = "email_hidden2" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_email2 : ''; ?>" placeholder = "E-mail 2" />
						</div>

						<div class="col-md-3">
							<label>Secondary E-Mail Type:</label>
							<select class="form-control" name="aemail_type">
								<?php foreach ($email_types_option as $key => $row) {?>
									<option value="<?php echo $key; ?>" <?php if ($fetch_contact_data[0]->aemail_type == $key) {echo 'selected';}?>><?php echo $row; ?></option>
								<?php }?>
							</select>
						</div>
						<div class="col-md-3">
							<a title="Remove" class="btn btn-danger btn-sm" onclick="removeExtradata(this,'email');"><i class="fa fa-trash"></i></a>
						</div>

					</div>

					<div class="row borrower_data_row hide" >

						<div class="col-md-3">
							<label>Fax:</label>
							<input type="text" class="form-control" name="fax" id = "fax" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_fax : ''; ?>" placeholder = "Fax" />
						</div>

					</div>

					<div class="row borrower_data_row borrower_textarea_div" >
						<label>About:</label>
						<div class="col-md-9">
							<textarea class="form-control" name = "about" placeholder = "About:" id = "about" ><?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_about : ''; ?></textarea>
						</div>
					</div>

					<div class="row borrower_data_row" >

						<div class="col-md-3">

							<label>Company Name:</label>
							<input type="text" class="form-control" name="company_name_1" id = "company_name" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->company_name_1 : ''; ?>" placeholder = "Company Name" />
						</div>

						<div class="col-md-3">
							<a onclick="hideandshowcompany(this,'<?php echo isset($fetch_contact_data[0]->addemailopt) ? $fetch_contact_data[0]->addemailopt : '0';?>');" class="btn btn-primary btn-sm" style="float:left;"><i class="fa fa-plus"></i></a>
						</div>

						<div class="col-md-6">
							<label>Website:</label>
							<input type="text" class="form-control" name="website" id = "website" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_website : ''; ?>" placeholder = "Website" />
						</div>

						<!--<div class="col-md-3">
							<label>Scotsman Guide:</label>
							<input type="text" class="form-control" name="Scotsman_Guide" id = "Scotsman_Guide" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->Scotsman_Guide : ''; ?>" placeholder = "Scotsman Guide" />
						</div>-->

					<!--<div class="col-md-3">

							<label>Company Name3:</label>
							<input type="text" class="form-control" name="company_name_3" id = "other_company_name2" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->company_name_3 : ''; ?>" placeholder = "Company Name3" />
						</div>-->
					</div>

					<?php
						if($fetch_contact_data[0]->company_name_2 != ''){
							$addcssaacom = 'style="display: block;"';
						}else{
							$addcssaacom = 'style="display: none;"';
						}
					?>

					<div class="row borrower_data_row" id="othercompany" <?php echo $addcssaacom;?>>
						<div class="col-md-3">
							<label>Secondary Company Name:</label>
							<input type="text" class="form-control" name="company_name_2" id = "other_company_name1" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->company_name_2 : ''; ?>" placeholder = "Company Name" />
						</div>
						<div class="col-md-3">
							<a title="Remove" class="btn btn-danger btn-sm" onclick="removeExtradata(this,'company');"><i class="fa fa-trash"></i></a>
						</div>
					</div>

					<div class="row borrower_data_row" >

						<div class="col-md-3">

							<label>Broker License #:</label>
							<input type="text" class="form-control" name="broker_license"  value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->broker_license : ''; ?>" placeholder = "Broker License #" />
						</div>

						<div class="col-md-3">

							<label>State:</label>
							<select name ="broker_license_state" class = "form-control">

								<?php
//$selected = '';
foreach ($STATE_USA as $key => $value) {

	?>
								<option <?php if ($key == $fetch_contact_data[0]->broker_license_state) {echo 'Selected';}?> value = "<?php echo $key; ?>"><?php echo $value; ?></option>

								<?php }?>
							</select>
						</div>
					</div>

					<div class="row borrower_data_row" >

						<div class="col-md-3">

							<label>Contractor License #:</label>
							<input type="text" class="form-control" name="contractor_license"  value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->contractor_license : ''; ?>" placeholder = "Contractor License #" />
						</div>

						<div class="col-md-3">

							<label>State:</label>
							<select name ="contractor_license_state" class = "form-control">

								<?php
//$selected = '';
foreach ($STATE_USA as $key => $value) {

	?>
								<option <?php if ($key == $fetch_contact_data[0]->contractor_license_state) {echo 'Selected';}?> value = "<?php echo $key; ?>"><?php echo $value; ?></option>

								<?php }?>
							</select>
						</div>
					</div>

					<div class="row borrower_data_row" >
						<div class="col-md-3">
							<label>OFAC Searched #:</label>
							<select name ="ofac_searched" class = "form-control">
								<option <?php echo $fetch_contact_data[0]->ofac_searched=='No' ? 'selected' : ''; ?>>No</option>
								<option <?php echo $fetch_contact_data[0]->ofac_searched=='Yes' ? 'selected' : ''; ?>>Yes</option>
							</select>
						</div>
					</div>

					<div class="row borrower_data_row">
						<div class="col-md-3">
							<h4>Home/Business Address</h4>
						</div>
					</div>

					<div class="row borrower_data_row">
						<div class="col-md-6">
							<label>Street Address:</label>
							<input type="text" class="form-control" onchange = "fill_mailing_portion(this.id,this.value)" name="address" id = "address" value = "<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_address : ''; ?>" placeholder = "Street Address" />
						</div>

						<div class="col-md-3">
							<label>Unit #:</label>
							<input type="text" class="form-control" onchange = "fill_mailing_portion(this.id,this.value)" name="unit" id = "unit" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_unit : ''; ?>" placeholder = "Unit #" />
						</div>


					</div>

					<div class="row borrower_data_row" id="">

						<div class="col-md-3">
							<label>City:</label>
							<input type="text" class="form-control" onchange = "fill_mailing_portion(this.id,this.value)" name="city" id = "city" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_city : ''; ?>" placeholder = "City" />
						</div>
						<div class="col-md-3 select_state">
							<label>State:</label>
							<!--<input type="text" class="form-control" onchange = "fill_mailing_portion(this.id,this.value)" name="state" id = "state" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_state : ''; ?>" placeholder = "State" />-->

							<select onchange = "fill_mailing_portion(this.id,this.value)" id = "state" name ="state" class =  "form-control selectpicker">
								<option value = ""></option>
								<?php
//$selected = '';
foreach ($STATE_USA as $key => $value) {

	?>
								<option <?php if ($key == $fetch_contact_data[0]->contact_state) {echo 'Selected';}?> value = "<?php echo $key; ?>"><?php echo $value; ?></option>

								<?php }?>
							</select>
						</div>

						<div class="col-md-3">
							<label>Zip:</label>
							<input type="text" class="form-control" onchange = "fill_mailing_portion(this.id,this.value)" name="zip" id = "zip" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_zip : ''; ?>" placeholder = "Zip" />
						</div>



					</div>

					<div class="row borrower_data_row">
						<div class="col-md-3">
							<h4>Mailing Address</h4>
						</div>
						<div class="col-md-3 same_check" >

							<input type = "checkbox" <?php if (isset($contact_id)) {if ($fetch_contact_data[0]->same_mailing_address == '1') {echo 'checked';}}?> id = "same_mailing_chkbx" onclick = "copy_address()" /> <span>Check, If same as Above</span>
							<input type = "hidden" id = "same_mailing_address" name = "same_mailing_address"  value =  "<?php echo isset($contact_id) ? $fetch_contact_data[0]->same_mailing_address : '0'; ?>"/>
						</div>
					</div>


					<div class="row borrower_data_row">
						<div class="col-md-6">
							<label>Street Address:</label>
							<input type="text" class="form-control" name="mailing_address"id = "mailing_address" value = "<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_mailing_address : ''; ?>" placeholder = "Street Address" />
						</div>

						<div class="col-md-3">
							<label>Unit #:</label>
							<input type="text" class="form-control" name="mailing_unit" id = "mailing_unit" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_mailing_unit : ''; ?>" placeholder = "Unit #" />
						</div>


					</div>

					<div class="row borrower_data_row" id="">

						<div class="col-md-3">
							<label>City:</label>
							<input type="text" class="form-control" name="mailing_city" id = "mailing_city" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_mailing_city : ''; ?>" placeholder = "City" />
						</div>
						<div class="col-md-3 select_mailing_state">
							<label>State:</label>
							<!--<input type="text" class="form-control" name="mailing_state" id = "mailing_state" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_mailing_state : ''; ?>" placeholder = "State" />-->

							<select id = "mailing_state" name ="mailing_state" class =  "form-control selectpicker">
								<option value = ""></option>
								<?php
foreach ($STATE_USA as $key => $value) {

	?>
								<option <?php if ($key == $fetch_contact_data[0]->contact_mailing_state) {echo 'Selected';}?> value = "<?php echo $key; ?>"><?php echo $value; ?></option>
								<?php }?>
							</select>
						</div>

						<div class="col-md-3">
							<label>Zip:</label>
							<input type="text" class="form-control" name="mailing_zip" id = "mailing_zip" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_mailing_zip : ''; ?>" placeholder = "Zip" />
						</div>

					</div>


					<div class="row">&nbsp;</div>


					<div class="row borrower_data_row main_title">
						<h4 class="ref">Referral Source</h4>
					</div>

					<div class="borrower_data_row contact_tags_row" style="margin-left:15px !important;word-spacing: 1.15pt !important;">
						<div class="col-md-12" style="width:100% !important;">
							<?php if ($this->session->flashdata('errorr') != '') {

	$er1 = 'e';
}?>
							<label class="" style="margin-left:-10px !important;"><strong>Referral Source:</strong></label>
						</div>

								<?php foreach ($referral_source as $key => $row) {?>

							<div class="col-md-3" style="width:25% !important;">

								<input type="checkbox" id="referral_source" class="form-control" value="<?php echo $key; ?>" onchange="referral_checkbox_val(this);" <?php if ($key == $fetch_contact_data[0]->referral_source) {echo 'Checked';}?>/><?php echo $row; ?>

								<input type="hidden" name="referral_source" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->referral_source : ''; ?>" id="referral" />

							</div>
								<?php }?>

					</div>




					<!--<div class="row borrower_data_row">
						<div class="col-md-3">

							<a style="width: 300px;" class="btn blue" data-toggle="modal" href="#contact_tags">Contact Tags</a>

						</div>
						<?php if ($user_role == '2') {?>

						<div class="col-md-3">

							<a style="width: 300px;" class="btn blue" data-toggle="modal" href="#assigned">Permissions</a>

						</div>
						<?php	}?>
						<div class="col-md-3">

							<a style="width: 300px;" class="btn blue" data-toggle="modal" href="#contact_document"> Contact Documents</a>


						</div>
					</div>-->
					<!--<div class="row borrower_data_row" >

						<div class="col-md-3">

							<label>Loan:</label>
							<input type="text" class="form-control" name="contact_loan" value="<?php //echo isset($contact_id) ? $fetch_contact_data[0]->contact_loan : ''; ?>" placeholder = "Loan" />
						</div>

						<div class="col-md-3">
							<label>Transaction Coordinator:</label>
							<input type="text" class="form-control" name="transection_coordinator" value="<?php //echo isset($contact_id) ? $fetch_contact_data[0]->transection_coordinator : ''; ?>" placeholder = "Transaction Coordinator" />
						</div>

					</div>-->
					<div class="row">&nbsp;</div>
					<!--<div class="row borrower_data_row main_title">

							<h4 >Search</h4>
					</div>-->
					<?php //if($this->session->userdata('user_role')==2){?>
					<div class="row borrower_data_row main_title">
						<h4>Internal Contact</h4>
					</div>
					<div class="row borrower_data_row" id="">
						<div class="col-md-2">

							<label style="padding-top:5px;">Internal Contact:</label>

						</div>

								<div class="col-md-3" style="margin-left:-40px;">
							<select name="internal_contact" id="internal_contact" class="<?php echo $er1; ?> form-control">
							<option value="">Select One</option>
							<?php foreach ($all_users_data as $row) {?>
								<option value="<?php echo $row->id; ?>" <?php if ($row->id == $fetch_contact_data[0]->internal_contact) {echo 'Selected';}?>><?php echo $row->fname . ' ' . $row->middle_name . ' ' . $row->lname; ?></option>

							<?php }?>
							</select>
						</div>
					</div>
			<?php //} ?>

					<div class="row">&nbsp;</div>
					<div class="row borrower_data_row main_title">
							<h4>Social Media</h4>
					</div>
					<div class="row borrower_data_row borrower_textarea_div" >
						<label>Facebook Profile:</label>
						<div class="col-md-9">
								<input class="form-control" name="fb_profile" placeholder = "Facebook Link:" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->fb_profile : ''; ?>">
						</div>


					</div>
					<div class="row borrower_data_row borrower_textarea_div" >
						<label>Linkedin Profile:</label>
						<div class="col-md-9">
								<input class="form-control" name = "lkdin_profile" placeholder = "Linkedin Link:" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->lkdin_profile : ''; ?>">
						</div>


					</div>

					<div class="row borrower_data_row borrower_textarea_div" >
						<label>Bigger Pockets Profile:</label>
						<div class="col-md-9">
								<input class="form-control" name ="bigger_pocket_profile" placeholder = "Bigger Pockets Link:" value="<?php echo isset($contact_id) ? $fetch_contact_data[0]->bigger_pocket_profile : ''; ?>">
						</div>


					</div>




					<!--<div class="row borrower_data_row borrower_textarea_div" >
						<label>Personal Notes:</label>
						<div class="col-md-9">
								<textarea class="form-control" name = "personal_notes" placeholder = "Personal Notes:" id = "personal_notes" ><?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_personal_notes : ''; ?></textarea>
						</div>


					</div>-->
					<?php
// echo'<pre>';
// print_r($tag_extra_field);
// echo'</pre>';
?>

				<div class="row borrower_data_row main_title">
						<h4 class="ref">Marketing Data</h4>
					</div>
					<div class="borrower_data_row" style="margin-left:15px !important;word-spacing: 1.15pt !important;">
						<!-- <div class="col-md-12" style="width:100% !important;">
							<label class="" style="margin-left:-10px !important;"><strong>Marketing Data:</strong></label>
						</div> -->
						<?php 
						$contactMarketingDataArray=array();
						if(!empty($fetch_contact_data[0]->contact_marketing_data)){
							$contactMarketingDataArray=explode(",",$fetch_contact_data[0]->contact_marketing_data);
						}
						foreach ($contact_marketing_data as $key => $row) {
							$checked="";
							if(in_array($key,$contactMarketingDataArray)) {
								$checked="checked";
							}
							?>
							<div class="col-md-3" style="width:25% !important;">
								<input type="checkbox" name="contact_marketing_data[]" class="form-control" value="<?php echo $key; ?>"  <?php echo $checked;?>><?php echo $row; ?>
							</div>
						<?php 
						}?>
					</div>



					<div class="row">&nbsp;</div>

					<div class="row borrower_data_row main_title">

							<h4 class="specialty">Specialty <small>(Select One or More)</small></h4>
					</div>

					<!---  Contact Tags modal  Starts---->

					<!-----------<div class="modal fade bs-modal-lg" id="" tabindex="-1" role="large" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Contact Tags:</h4>
								</div>
									Hidden fields----------->
									<!-----------End of hidden Fields---->
									<?php

// echo'<pre>';
// print_r($fetch_contact_specialty);
// echo'</pre>';
foreach (array_values(array_filter($fetch_contact_type)) as $con_type) {
	$aa[$con_type] = $con_type;
}
foreach (array_values(array_filter($fetch_contact_specialty)) as $con_specialty) {

	$bb[$con_specialty] = $con_specialty;
}

foreach (array_values(array_filter($fetch_contact_email_blast)) as $con_email_blast) {
	$dd[$con_email_blast] = $con_email_blast;
}

foreach (array_values(array_filter($fetch_contact_email_blast_no)) as $con_email_blast_no) {
	$dd_no[$con_email_blast_no] = $con_email_blast_no;
}

foreach ($fetch_contact_permission as $con_permission) {
	$ee[$con_permission] = $con_permission;
}

?>

									<?php foreach ($fetch_sql_tags as $key => $row) {
	// echo'<pre>';
	// print_r($fetch_sql_tags[$key]->id);
	// echo'</pre>';

	?>
									<input type = "hidden" value = "<?php echo $fetch_sql_tags[$key]->id; ?>" name = "tag_id[]">
									<?php }?>

									<!--<input type = "text" value = "<?php echo $fetch_sql_tags[0]->id; ?>" name = "tag_id" >-->
								<div class="row">

									<!--<div class="row ">
										<div class="col-md-3">
											<label style = ""><b>Contact Type</b></label>
										</div>
									</div>
									<div class="row contact_tags_row">
										<?php
foreach ($contact_type as $key => $type) {

	if ($aa[$key] == $key) {
		$checked = 'checked = "checked"';
	} else {
		$checked = '';
	}
	?>
											<div class="col-md-8">
												<input <?php echo $checked; ?> type="checkbox" class="form-control" name = "contact_type[]" value = "<?php echo $aa[$key] ? $aa[$key] : $key; ?>"/>&nbsp;&nbsp;<?php echo $type; ?>
											</div>

										<?php	}?>


									</div>-->

									<!--<div class="row borrower_data_row">
										<div class="col-md-3" >
											<label style ="margin-left:20px;"><b>Specialty <small>(Select One or More)</small></b></label>
										</div>
									</div>-->

									<div class="borrower_data_row contact_tags_row" id="margin_left">


										<?php

											foreach ($contact_specialty as $key => $specialty) {

												if ($bb[$key] == $key) {
													$checked = "checked";
													// $color='text-danger';
												} else {
													$checked = '';
													// $color='';
												}

												?>
											<input type = "hidden" value = "<?php echo $fetch_contact_specialty_tag_id[$key]; ?>" name = "specialty_tag_id[]"/>
											<div class="col-md-3">
												<input type="checkbox" <?php echo $checked; ?> class="form-control contact_specialty" name = "contact_specialty[]" value = "<?php echo $key; ?>"/>&nbsp;&nbsp;<?php echo $specialty; ?>
											</div>

										<?php	}?>
									</div>


									
									<?php
									$disabledCheckbox = '';

									$disabledCheckbox1 = '';
									$disabledCheckbox2 = '';
									$disabledCheckbox3 = '';
									
									$disabledCheckbox6 = '';
									$disabledCheckbox7 = '';
									
									if(isset($dd)){
										if(in_array('12', $dd)){
											$disabledCheckbox = 'disabled="true"';
										}
										if(in_array('4', $dd)){
											$disabledCheckbox1 = 'disabled="true"';
											$disabledCheckbox2 = 'disabled="true"';
											$disabledCheckbox3 = 'disabled="true"';
										}
										if(in_array('5', $dd)){
											$disabledCheckbox6 = 'disabled="true"';
											$disabledCheckbox7 = 'disabled="true"';
										}
									}
									?>
									<div class="row">&nbsp;</div>
									<div class="row borrower_data_row main_title">

										<h4 class="">Media Contact Options <small>(Select One or More)</small></h4>
									</div>
									<div class="borrower_data_row contact_tags_row" id="margin_left1">
											<?php if ($this->session->flashdata('errorr') != '') {

														$er1 = 'e';
													}?>

									<div class="col-md-8">
										<strong class="borrower">Borrower</strong>
										<!-- </br>Yes &nbsp;&nbsp;No -->
									</div>
									<!--
									<div class="col-md-8">
										<input type="checkbox" class="form-control ba" onclick="borrower_advertisement(this);" <?php if ($tag_extra_field[0]->borrower_advertisement == '1') {echo 'Checked';}?>> &nbsp;No Advertisement

										<input type="hidden" name="No_advertisement_borrower" id="advertisement_borrower" value="<?php echo $tag_extra_field[0]->borrower_advertisement ? $tag_extra_field[0]->borrower_advertisement : ''; ?>">
									</div>-->
								<?php foreach ($contact_email_blast as $key => $email_blast) {
									if ($dd[$key] == $key) {
										$checked = 'checked = "checked"';
										$checked_b_yes = 'disabled';
									} else {

										$checked = '';
										$checked_b_yes = '';
									}

									if ($dd_no[$key] == $key) {
										$checked_no = 'checked = "checked"';
										$checked_b_no = 'disabled';

									} else {

										$checked_no = '';
										$checked_b_no = '';
									}

	

	if ($key == '1' || $key == '3' || $key == '6' || $key == '10') {

		if ($key == '6') {
			
			?>
										<div class="col-md-8 <?php// echo $color;?>">
											<input type="checkbox" <?php echo $checked; ?>  onclick="diasbled_checkbox('<?php echo $key; ?>',this,'yes')" class="form-control Borrower_checkboxes" <?php echo $disabledCheckbox; ?>  id="hide_mail6" name = "contact_email_blast[]" value = "<?php echo $key; ?>" <?php if(in_array('4', $dd)){ echo 'disabled="true"'; } ?>>&nbsp;&nbsp;
											<!-- <input type="checkbox"  onclick="diasbled_checkbox('<?php echo $key; ?>',this,'no')" class="form-control borrower_hide12" id="hide_no_mail6" name = "contact_email_blast_no[]" <?php echo $checked_no; ?> <?php echo $checked_b_yes; ?> value = "<?php echo $key; ?>"> --> &nbsp;&nbsp;<?php echo $email_blast; ?>
										</div>

											<?php } else if ($key == '10') {?>

										<div class="col-md-8 <?php //echo $color;?>">
											<input type="checkbox" <?php echo $checked; ?> class="form-control Borrower_checkboxes" <?php echo $disabledCheckbox; ?> id="hide_mai" name = "contact_email_blast[]" value = "<?php echo $key; ?>" <?php if(in_array('4', $dd)){ echo 'disabled="true"'; } ?>>&nbsp;&nbsp;<?php echo $email_blast; ?>
										</div>
												<?php } else {?>

										<div class="col-md-8 <?php// echo $color;?>">
											<input type="checkbox" <?php echo $checked; ?>   onclick="diasbled_checkbox('<?php echo $key; ?>',this,'yes')" class="form-control Borrower_checkboxes"  <?php echo $disabledCheckbox; ?> id="hide_yes_<?php echo $key; ?>"  name = "contact_email_blast[]" value = "<?php echo $key; ?>" <?php if(in_array('4', $dd)){ echo 'disabled="true"'; } ?>>&nbsp;&nbsp;
											<!-- <input type="checkbox"  class="form-control" id="hide_no_<?php echo $key; ?>" onclick="diasbled_checkbox('<?php echo $key; ?>',this,'no')" name = "contact_email_blast_no[]" <?php echo $checked_b_yes; ?>   <?php echo $checked_no; ?> value = "<?php echo $key; ?>"> --> &nbsp;&nbsp;<?php echo $email_blast; ?>
										</div>

										<?php }}}?>
									</div>
									<div class="row">&nbsp;</div>
									<div class="borrower_data_row contact_tags_row" id="margin_left1">
											<?php if ($this->session->flashdata('errorr') != '') {

	$er1 = 'e';
}?>
									<div class="col-md-8">
										<strong class="lender">Lender</strong>
										<!-- </br>Yes &nbsp;&nbsp;No -->
									</div>
									<!--
									<div class="col-md-8">
										<input type="checkbox" class="form-control la" onclick="lender_advertisement(this);" <?php if ($tag_extra_field[0]->lender_advertisement == 1) {echo 'Checked';}?>> &nbsp;No Advertisement

										<input type="hidden" name="No_advertisement_lender" id="advertisement_lender" value="<?php echo $tag_extra_field[0]->lender_advertisement ? $tag_extra_field[0]->lender_advertisement : ''; ?>">
									</div>-->
									<?php foreach ($contact_email_blast as $key => $email_blast) {
	if ($dd[$key] == $key) {
		$checked = 'checked = "checked"';
		//$checked_ds2 = 'disabled';
		$checked_lyes = 'disabled';
	} else {

		$checked = '';
		$checked_lyes = '';
	}

	if ($dd_no[$key] == $key) {
		$checked_no = 'checked = "checked"';
		$checked_lno = 'disabled';

	} else {

		$checked_no = '';
		$checked_lno = '';
	}

	/* if($this->session->userdata('user_role') =='1' )
												{
													if($dd[$key] == $key){
														$checked ="checked";
														// $color='text-danger';
													}
													else
													{
														$checked = '';
														// $color='';
													}
												}
												else{

												if($dd[$key]== $key){
													$checked ="checked";
													if($this->session->userdata('t_user_id') != $contact_tag_user_id[$fetch_contact_specialty_tag_id[$key]] )
													{
														$color='text-danger';
													}
												}
												else
												{
													$checked = "";
													 $color='';
												}
												} */

	if ($key == '2' || $key == '7' || $key == '9' || $key == '11' || $key == '13') {

		if ($key == '7') {
			?>
										<div class="col-md-8 <?php //echo $color;?>">
											<input type="checkbox" <?php echo $checked; ?>  onclick="diasbled_checkbox('<?php echo $key; ?>',this,'yes')" class="form-control borrower_hidew Borrower_checkboxes" <?php echo $disabledCheckbox; ?>  id="l_hide_yes_<?php echo $key; ?>"  name = "contact_email_blast[]" value = "<?php echo $key; ?>" <?php if(in_array('5', $dd)){ echo 'disabled="true"'; } ?>>&nbsp;&nbsp;
											<!-- <input type="checkbox" <?php echo $checked_no; ?>  <?php echo $checked_lyes; ?> class="form-control " data-new="abc" id="l_hide_no_<?php echo $key; ?>" onclick="diasbled_checkbox('<?php echo $key; ?>',this,'no')" name = "contact_email_blast_no[]" value = "<?php echo $key; ?>"> -->&nbsp;&nbsp;<?php echo $email_blast; ?>
										</div>

									<?php } else if ($key == '9' || $key == '11') {?>

										<div class="col-md-8 <?php //echo $color;?>">
											<input type="checkbox" <?php echo $checked; ?> class="form-control Lender_checkboxes" <?php echo $disabledCheckbox; ?> id="l_hide_yes_<?php echo $key; ?>" onclick="diasbled_checkbox('<?php echo $key; ?>',this,'yes')" name = "contact_email_blast[]" value = "<?php echo $key; ?>" <?php if(in_array('5', $dd)){ if($key != 9) { echo 'disabled="true"'; }} ?>>&nbsp;&nbsp;
											<!-- <input type="checkbox" <?php echo $checked_no; ?> <?php echo $checked_lyes; ?> onclick="diasbled_checkbox('<?php echo $key; ?>',this,'no')" class="form-control" id="l_hide_no_<?php echo $key; ?>" name = "contact_email_blast_no[]" value = "<?php echo $key; ?>"> -->&nbsp;&nbsp;<?php echo $email_blast; ?>
										</div>




								<?php } else {?>

										<div class="col-md-8 <?php// echo $color;?>">
											<input type="checkbox" <?php echo $checked; ?>  onclick="diasbled_checkbox('<?php echo $key; ?>',this,'yes')" class="form-control Lender_checkboxes" <?php echo $disabledCheckbox; ?> id="l_hide_yes_<?php echo $key; ?>" name="contact_email_blast[]" value = "<?php echo $key; ?>" <?php if(in_array('5', $dd)){ echo 'disabled="true"'; } ?>>&nbsp;&nbsp;

											<!-- <input type="checkbox" <?php echo $checked_no; ?> <?php echo $checked_lyes; ?> onclick="diasbled_checkbox('<?php echo $key; ?>',this,'no')" class="form-control" id="l_hide_no_<?php echo $key; ?>" name="contact_email_blast_no[]" value = "<?php echo $key; ?>" > -->&nbsp;&nbsp;<?php echo $email_blast; ?>
										</div>

									<?php }}}?>
									</div>
									<div class="row">&nbsp;</div>
									<div class="borrower_data_row contact_tags_row" id="margin_left1">
									<div class="col-md-8">
										<strong>Opt Out</strong>
										<!-- </br>Yes &nbsp;&nbsp;No -->
								</div>
									<?php 

									foreach ($contact_email_blast as $key => $email_blast) {
	if ($dd[$key] == $key) {
		$checked = 'checked = "checked"';
		$checked_opt_yes = 'disabled';

	} else {

		$checked = '';
		$checked_opt_yes = '';
	}
	if ($dd_no[$key] == $key) {
		$checked_no = 'checked = "checked"';
		$checked_opt_no = 'disabled';
	} else {

		$checked_no = '';
		$checked_opt_no = '';
	}
	/* if($this->session->userdata('user_role') =='1' )
												{
													if($dd[$key] == $key){
														$checked ="checked";
														// $color='text-danger';
													}
													else
													{
														$checked = '';
														// $color='';
													}
												}
												else{

												if($dd[$key]== $key){
													$checked ="checked";
													if($this->session->userdata('t_user_id') != $contact_tag_user_id[$fetch_contact_specialty_tag_id[$key]] )
													{
														$color='text-danger';
													}
												}
												else
												{
													$checked = "";
													 $color='';
												}
												} */

	if ($key == '4' || $key == '5' || $key == '12') {

		if ($key == '5') {
			?>
										<div class="col-md-8 <?php// echo $color;?>">
											<input type="checkbox" <?php echo $checked; ?>  class="form-control" <?php if($key != '12'){ echo $disabledCheckbox; } ?> name = "contact_email_blast[]" id="opt_out_value_yes_<?php echo $key; ?>"  value = "<?php echo $key; ?>" onclick="hide_some_checkbox(this,'<?php echo $key; ?>','yes');diasbled_checkbox('<?php echo $key; ?>',this,'yes');">&nbsp;&nbsp;
											<!-- <input type="checkbox" <?php echo $checked_no; ?> class="form-control" name = "contact_email_blast_no[]" id="opt_out_value_no_<?php echo $key; ?>"  value = "<?php echo $key; ?>"  <?php echo $checked_opt_yes; ?> onclick="hide_some_checkbox(this,'<?php echo $key; ?>','no');diasbled_checkbox('<?php echo $key; ?>',this,'no');"> -->&nbsp;&nbsp;<?php echo $email_blast; ?>
										</div>

											<?php } else {?>

										<div class="col-md-8 <?php// echo $color;?>">
											<input type="checkbox" <?php if($key != '12'){ echo $disabledCheckbox; } ?> <?php  echo $checked; ?>  class="form-control" name = "contact_email_blast[]"  id="opt_out_value_yes_<?php echo $key; ?>" value = "<?php echo $key; ?>" onclick="hide_some_checkbox(this,'<?php echo $key; ?>','yes');diasbled_checkbox('<?php echo $key; ?>',this,'yes');">&nbsp;&nbsp;
											<!-- <input type="checkbox" <?php echo $checked_no; ?> class="form-control" name = "contact_email_blast_no[]" id="opt_out_value_no_<?php echo $key; ?>" value = "<?php echo $key; ?>" <?php echo $checked_opt_yes; ?> onclick="hide_some_checkbox(this,'<?php echo $key; ?>','no');diasbled_checkbox('<?php echo $key; ?>',this,'no')"> -->&nbsp;&nbsp;<?php echo $email_blast; ?>
										</div>


										<?php	}}}?>
									</div>


								</div>

								


					<!---  Borrower Data modal   Starts---->

					<div class="modal fade bs-modal-lg" id="borrower_data" tabindex="-1" role="large" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Contact<?php echo isset($contact_id) ? ': ' . $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname : ''; ?></h4>
								</div>
									<!-----------Hidden fields----------->
									<!-----------End of hidden Fields---->
								<div class="modal-body">
									<!--------- Borrower Contact Type DIV STARTS -------------->

									<div class="">
										<!--<div class="row borrower_data_row" >
											<div class="lender-names-contact-page" >
												<?php
if (isset($con_id_borrower_name)) {
	foreach ($con_id_borrower_name as $key => $row) {
		$sr_no = $key + 1;
		echo $sr_no . '. ' . $row . '<br>';
	}
}
?>

											</div>
										</div>-->

										<div class="row borrower_data_row" id="">
											<div class="col-md-3">
												<label>Social Security Number:</label>
												<input  type="text" class="form-control borrower_enabled" name="security_number" id = "security_number" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_ss_num : ''; ?>" placeholder = "Social Security Number" />
											</div>

											<div class="col-md-3">
												<label>DOB:</label>
												<input type="text" class="form-control datepicker" name="dob" id = "dob" value="<?php if (isset($contact_id)) {echo $fetch_contact_data[0]->contact_dob ? date('m-d-Y', strtotime($fetch_contact_data[0]->contact_dob)) : '';}?>" placeholder = "DOB(Format:MM-DD-YYYY)" />
											</div>

										</div>

										<div class="row borrower_data_row" id="">
											<div class="col-md-3">
												<label>Driver License #:</label>
												<input type="textbox" class="form-control borrower_enabled" id = "driver_license" name="driver_license" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_driver_license : ''; ?>" placeholder = "Driver License #" />
											</div>
											<div class="col-md-3">


												<label>Driver's License State:</label>
												<!--<input type="textbox" class="form-control borrower_enabled" id = "driver_license_state" name="driver_license_state" value="<?php echo $fetch_borrower_contact_type[0]->borrower_driver_license_state ? $fetch_borrower_contact_type[0]->borrower_driver_license_state : ''; ?>" placeholder = "Driver's License State" />-->

												<select id = "driver_license_state" name="driver_license_state" class =  "form-control selectpicker">
													<option value = ""></option>
													<?php
$selected = '';
foreach ($all_country as $key => $value) {
	if ($key == $fetch_borrower_contact_type[0]->borrower_driver_license_state) {
		$selected = 'selected';
	} else {
		$selected = '';
	}
	?>
														<option <?php echo $selected; ?> value = "<?php echo $key; ?>"><?php echo $value; ?></option>
													<?php }?>



												</select>
											</div>
											<div class="col-md-3">

												<label>Expiration Date:</label>
												<input  type="text" name="exp_date" value="<?php echo isset($fetch_borrower_contact_type[0]->borrower_exp_date) ? date('m-d-Y', strtotime($fetch_borrower_contact_type[0]->borrower_exp_date)) : ''; ?>" class="lic_exp_date_val form-control borrower_enabled datepicker"  placeholder = "Expiration Date(Format:DD-MM-YYYY)" />
											</div>

										</div>

										<div class="row borrower_data_row" id="">

											<div class="col-md-3">

												<label>Credit Score:</label>
												<input  type="text" class="form-control borrower_enabled" name="credit_score" id = "credit_score" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_credit_score : ''; ?>" placeholder = "Credit Score" />
											</div>

											<div class="col-md-3">

												<label>Credit Score Date:</label>
												<input type="text" class="form-control borrower_enabled datepicker" name="credit_score_date" id = "credit_score_date" value="<?php echo isset($fetch_borrower_contact_type[0]->borrower_credit_score_date) ? date('m-d-Y', strtotime($fetch_borrower_contact_type[0]->borrower_credit_score_date)) : ''; ?>" placeholder = "Credit Score Date(Format:DD-MM-YYYY)" />
											</div>

										</div>

										<div class="row borrower_data_row" id="">
											<div class="col-md-3">

												<label>Martial Status:</label>
												<select class="form-control borrower_enabled" name="marital_status" id = "marital_status">
													<?php
$key = '';
foreach ($marital_status as $key => $marital) {
	if ($key == $fetch_borrower_contact_type[0]->borrower_marital_status) {
		$selected = 'selected ';
	} else {
		$selected = '';
	}

	echo "<option " . $selected . "  value = " . $key . " >" . $marital . "</option>";
}
?>
												</select>
											</div>

											<div class="col-md-3">

												<label># of Dependents:</label>
												<input  type="text" class="form-control borrower_enabled" name="dependents" id = "dependents" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_dependents : ''; ?>" placeholder = "# of Dependents" />
											</div>


										</div>


										<div class="row borrower_data_row" id="">
											<div class="col-md-3">
												<label style="width: 55%;">US Citizen? </label>
											</div>
											<div class="col-md-3">
												<select  class = "form-control borrower_enabled" name = "us_citizen" id = "us_citizen" onchange = "display_us_citizen(this.value)">
													<!--<option value = "">SELECT</option>-->
													<?php
foreach ($yes_no_option as $key => $row) {
	?>
														<option value="<?php echo $key; ?>" <?php if (isset($fetch_borrower_contact_type)) {if ($fetch_borrower_contact_type[0]->borrower_us_citizen == $key) {echo 'selected';}}?>><?php echo $row; ?></option>
														<?php
}
?>

												</select>
											</div>

										</div>



										<div class="row   borrower_data_row borrower_textarea_div us_citizen_optn" style = " display:none" id="">

											<div  class="col-md-3 ">
												<textarea placeholder = "If No, Description?" rows = "4" name = "us_citizen_desc" id = "us_citizen_desc"><?php if (isset($fetch_borrower_contact_type)) {echo $fetch_borrower_contact_type[0]->borrower_us_citizen_desc;}?></textarea>
											</div>

										</div>

										<div class="row borrower_data_row" id="">
											<div class="col-md-3" >
												<label style="width: 55%;">Pending Litigation? </label>
											</div>
											<div class="col-md-3">
												<select class = "form-control  borrower_enabled" name = "litigation" id = "litigation" onchange = "display_litigation(this.value)">
													<!--<option value = "">SELECT</option>-->
													<?php
foreach ($yes_no_option as $key => $row) {
	?>
														<option value="<?php echo $key; ?>" <?php if (isset($fetch_borrower_contact_type)) {if ($fetch_borrower_contact_type[0]->borrower_litigation == $key) {echo 'selected';}}?>><?php echo $row; ?></option>
														<?php
}
?>

												</select>
											</div>

										</div>



										<div class="row  borrower_data_row borrower_textarea_div litigation_optn" style = " display:none" id="">

											<div  class="col-md-3 ">
												<label>If Yes, explain:</label>
												<textarea placeholder = "If Yes, explain?" rows = "5" name = "litigation_desc" id = "litigation_desc"><?php if (isset($fetch_borrower_contact_type)) {echo $fetch_borrower_contact_type[0]->borrower_litigation_desc;}?></textarea>
											</div>

										</div>


										<div class="row borrower_data_row" id="">
											<div class="col-md-3" >
												<label style="width: 55%;">Convicted of Felony?</label>
											</div>
											<div class="col-md-3">
												<select class = "form-control  borrower_enabled" name = "felony" id = "felony" onchange = "display_felony(this.value)">
													<!--<option value = "">SELECT</option>-->
													<?php
foreach ($yes_no_option as $key => $row) {
	?>
														<option value="<?php echo $key; ?>" <?php if (isset($fetch_borrower_contact_type)) {if ($fetch_borrower_contact_type[0]->borrower_felony == $key) {echo 'selected';}}?>><?php echo $row; ?></option>
														<?php
}
?>

												</select>
											</div>

										</div>



										<div class="row  borrower_data_row borrower_textarea_div felony_optn" style = " display:none" id="">

											<div  class="col-md-3 ">
												<label>If Yes, explain:</label>

												<textarea placeholder = "If Yes, explain?" rows = "5" name = "felony_desc" id = "felony_desc"><?php if (isset($fetch_borrower_contact_type)) {echo $fetch_borrower_contact_type[0]->borrower_felony_desc;}?></textarea>
											</div>

										</div>


									<div class="row  borrower_data_row" id="">
										<div class="col-md-3" >
											<label style = "width:71% !important">Bankruptcy last 12 months</label>
										</div>
										<div class="col-md-3 "">

											<select class = "form-control borrower_enabled" name = "Bankruptcy" id = "Bankruptcy" onchange = "display_BK(this.value)">
												<!--<option value = "">SELECT</option>-->
												<?php
foreach ($yes_no_option as $key => $row) {
	?>
													<option value="<?php echo $key; ?>" <?php if (isset($fetch_borrower_contact_type)) {if ($fetch_borrower_contact_type[0]->borrower_bankruptcy == $key) {echo 'selected';}}?>><?php echo $row; ?></option>
													<?php
}
?>

											</select>
										</div>
									</div>
									<div class="row  borrower_data_row" id="">
										<div style = "display:none" class="col-md-3 BK_optn">

											<label>Date Dismissed: </label>

										</div>
										<div  style = "display:none" class="col-md-3 BK_optn">

											<input type="text" class="form-control lender_enabled input-md datepicker" name="date_dismissed" value="<?php echo isset($fetch_borrower_contact_type) ? date('m-d-Y', strtotime($fetch_borrower_contact_type[0]->borrower_date_dismissed)) : ''; ?>" placeholder = "MM-DD-YYYY" />
										</div>
									</div>
									<div class="row  borrower_data_row" id="">

										<div style = "display:none" class="col-md-3 BK_optn">
											<label>If Yes, has BK been dismissed?: </label>

										</div>


										<div style = "display:none" class="col-md-3 BK_optn">
											<select  class = "form-control  borrower_enabled" name = "BK" id = "BK" onchange = "display_bk_txtarea(this.value);">
												<!--<option value = "">SELECT</option>-->
												<?php
foreach ($yes_no_option as $key => $row) {
	?>
													<option value="<?php echo $key; ?>" <?php if (isset($fetch_borrower_contact_type)) {if ($fetch_borrower_contact_type[0]->borrower_BK == $key) {echo 'selected';}}?>><?php echo $row; ?></option>
													<?php
}
?>
											</select>
										</div>

									</div>


									<div style= "display:none;" class="row borrower_data_row borrower_textarea_div BK_textarea_div">
										<div class="col-md-3">
											<label>If Yes, BK Explanation:</label>

											<textarea  placeholder = "If Yes, BK Explanation" rows = "5" class="form-control borrower_enabled" name="bk_desc" id = "bk_desc"><?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_BK_desc : ''; ?></textarea>
										</div>
									</div>
								</div>

								<!--------- Borrower Contact Type DIV ENDS   -------------->

								</div>

								<div class="modal-footer">
									<button type="submit" name = "modal_btn" value = "close" class="btn default" >Close</button>
									<button type="submit" name = "modal_btn" value = "save" class="btn blue">Save</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
					<!-- /.modal-dialog -->
					</div>

					<!---  Borrower Data modal  End ---->

					<!---  Lender Data modal   Starts---->

					<div class="modal fade bs-modal-lg" id="lender_data" tabindex="-1" role="large" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Lender Data <?php echo isset($contact_id) ? ': ' . $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname : ''; ?></h4>
								</div>
									<!-----------Hidden fields----------->
									<!-----------End of hidden Fields---->
								<div class="modal-body">

									<!--------- Lender   Contact Type DIV STARTS -------------->

									<div class="">
										<div class="row lender_data_row" id="">
											<div class="col-md-3">

												<a style="width: 300px;" class="btn blue" data-toggle="modal" href="<?php echo base_url() . 'loan_schedule_investor/' . $contact_id; ?>">Lender Report</a>
											</div>
										</div>
										<div class="row lender_data_row" id="">
											<h4>Investor Suitability Data</h4>

											<div class="col-md-3">
												<label>Annual Income:</label>
												<input type = "text" value = "<?php echo isset($fetch_lender_contact_type[0]->lender_annual_income) ? $fetch_lender_contact_type[0]->lender_annual_income : ''; ?>" class=" form-control  " name = "annual_income" id = "annual_income"/>
											</div>

											<div class="col-md-3">
												<label>Annual Net Worth:</label>
												<input type = "text" value = "<?php echo isset($fetch_lender_contact_type[0]->lender_annual_net_worth) ? $fetch_lender_contact_type[0]->lender_annual_net_worth : ''; ?>" class=" form-control  " name = "annual_net_worth" id = "annual_net_worth"/>
											</div>

											<div class="col-md-3">
												<label>Estimate Liquid Assets:</label>
												<input type = "text" value = "<?php echo isset($fetch_lender_contact_type[0]->lender_estimate_liquid_assets) ? $fetch_lender_contact_type[0]->lender_estimate_liquid_assets : ''; ?>" class=" form-control  " name = "estimate_liquid_assets" id = "estimate_liquid_assets"/>
											</div>
										</div>
										<div class="row lender_data_row" id="">
											<div class=" col-md-3">
												<label>Liquidity Needs: </label>
												<input type="text" class=" form-control  " name="liquidity_needs" value="<?php echo isset($fetch_lender_contact_type[0]->lender_liquidity_needs) ? $fetch_lender_contact_type[0]->lender_liquidity_needs : ''; ?>" />
											</div>

											<div class=" col-md-3">
												<label>RE870 DATE:</label>
												<input type="text" class="RE_date_val form-control lender_enabled input-md datepicker" name="RE_date" value="<?php echo isset($fetch_lender_contact_type[0]->lender_RE_date) ? date('m-d-Y', strtotime($fetch_lender_contact_type[0]->lender_RE_date)) : ''; ?>" placeholder = "MM-DD-YYYY" />
											</div>
										</div>
									</div>
								</div>

								<div class="modal-footer">

									<button type="submit" name = "modal_btn" value = "close" class="btn default" >Close</button>

									<button type="submit"  name = "modal_btn" value = "save" class="btn blue">Save</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
					<!-- /.modal-dialog -->
					</div>

					<!---  Lender Data modal  End ---->

				<div class="modal fade" id="edit_contact" tabindex="-1" role="basic" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">

							<div class="modal-body">
								<div class="row" style="padding-top:8px;">

									<div class="col-md-8" style="padding-top:6px;"><p>Do you want to Save Information?</p></div>
									<div class="col-md-2">
										<!--<a style="margin-left:38px;" class="btn blue borrower_save_button" href="javascript:void(0);" onclick = "save_info('edit_contact')">Yes</a>-->

										<input style="margin-left:40px;" id = "formsubmit" value = "Yes" type = "submit"  class = "btn btn-primary"/>

									</div>
									<div class="col-md-2">
										<a class="btn default borrower_save_button" href = "<?php echo base_url() . 'viewcontact/' . $contact_id; ?>" >No</a>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


					<!---  Assigned modal   Starts---->

					<div class="modal fade bs-modal-lg" id="assigned" tabindex="-1" role="large" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Permissions</h4>
								</div>
									<!-----------Hidden fields----------->
									<!-----------End of hidden Fields---->
								<div class="modal-body">
										<?php

											$fetch_contact_permission = unserialize($fetch_sql_assigned[0]->contact_permission);

											foreach ($fetch_contact_permission as $con_permission) {
												$ee[$con_permission] = $con_permission;
											}

											?>
									<input type = "hidden" value = "<?php echo $fetch_sql_assigned[0]->id; ?>" name = "assigned_id"/>
									<div class="modal-body">

										<div class="row ">
											<div class="col-md-12">
												<label style = ""><b>Permissions (only allows that selected person/group to view History and Statistics]</b></label>
											</div>
										</div>
										<div class="row contact_tags_row">
											<?php

												foreach ($contact_permission as $key => $permission) {
													if ($ee[$key] == $key) {
														$checked = 'checked = "checked"';
													} else {
														$checked = '';
													}

													?>
												<div class="col-md-8">
													<input id = "checkbx_<?php echo $key; ?>" onclick = "check_permission_option(this.value,<?php echo $key; ?>)" type="checkbox" <?php echo $checked; ?> class="form-control" name = "contact_permission[]" value = "<?php echo $key; ?>"/>&nbsp;&nbsp;<?php echo $permission; ?>
												</div>



											<?php	}?>
											<div class="col-md-8 particular_user_dropdown" style = "display:none;">
												<select name = "particular_user_id" id = "particular_user_id">
													<option value = "">Select User</option>
														<?php
														if ($all_users) {
															foreach ($all_users as $key => $username) {

																if ($fetch_sql_assigned[0]->particular_user_id == $username->id) {
																	$selected = 'selected';
																} else {

																	$selected = '';
																}
																echo '<option ' . $selected . ' value = ' . $username->id . '>' . $username->fname . ' ' . $username->lname . '</option>';
															}
														}
														?>
												</select>
											</div>

										</div>
									</div>
								</div>

								<div class="modal-footer">
									<!--<button type="button" class="btn default" data-dismiss="modal">Close</button>-->
									<button type="submit"  class="btn blue">Save</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
					<!-- /.modal-dialog -->
					</div>

					<!--- Assigned modal  End ---->

					<!---  Contact Document modal   Starts---->

					<div class="modal fade bs-modal-lg" id="contact_document" tabindex="-1" role="large" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Contact Document</h4>
								</div>
									<!-----------Hidden fields----------->
									<!-----------End of hidden Fields---->
								<div class="modal-body">
									
									<div class="row">
										<input type = "hidden" name = "document_id" id = "document_id" />
										<div class="col-md-4">
											<label>Selected Documents</label>
											<?php
												if ($fetch_contact_document) {
													echo '<ul class = "contact_document_lists">';
													foreach ($fetch_contact_document as $contact_documents) {

														echo '<li><a href = "javascript:void(0);" onclick = "read_document(' . $contact_documents->id . ')">' . basename($contact_documents->contact_document) . '</a></li>';

													}
													echo '</ul>';
												}
												?>

										</div>
									</div>
									<hr>
									<div class=" doc-div">
										<div class="row">
											<div class="col-md-4">
												<input type="file" id = "borrower_upload_1" name ="contact_upload_doc[]" class="select_images_file" onchange="change_this_image(this)" >
											</div>
										</div>
									</div><br>
									<!--<br>
									<button onclick = "borrower_add_more_row();" id = "add_row" class = "btn blue" type = "button"><i class="fa fa-plus" aria-hidden="true">Add More</i></button>-->
								</div>

								<div class="modal-footer">
									<!--<button type="button" class="btn default" data-dismiss="modal">Close</button>-->
									<button type="submit"  class="btn blue">Save</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
					<!-- /.modal-dialog -->
					</div>

					<!---  Contact Document modal  End ---->

					<!---  Lender Document modal   Starts---->

					<div class="modal fade bs-modal-lg" id="lender_document" tabindex="-1" role="large" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Lender Document</h4>
								</div>
									<!-----------Hidden fields----------->
									<!-----------End of hidden Fields---->
								<div class="modal-body">
										<div class=" doc-div1">
											<div class="row">
												<div class="col-md-4">
													<input type="file" id = "lender_upload_1" name ="lender_upload_doc[]" class="select_images_file" onchange="change_this_image(this)" >
												</div>
												<!--<div class="col-md-4">
													<a id="" onclick="delete_property_image(this.id)"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
												</div>-->
											</div>
										</div>
										<br>
										<button onclick = "lender_add_more_row();" id = "add_row1" class = "btn blue" type = "button"><i class="fa fa-plus" aria-hidden="true">Add More</i></button>

								</div>

								<div class="modal-footer">
									<!--<button type="button" class="btn default" data-dismiss="modal">Close</button>-->
									<button type="submit"  class="btn blue">Save</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
					<!-- /.modal-dialog -->
					</div>

					<!---  Lender Document modal  End ---->

				</form>
			</div>
		</div>

	</div>
</div>

<form id="delete_contact_form" method="POST" action="<?php echo base_url(); ?>delete_contact">
<input type="hidden" value="<?php echo $contact_id; ?>" name="contact_id">
</form>

<script type="text/javascript" src="<?php echo base_url("assets/js/contact/contact_edit.js"); ?>"></script>

<script>



function referral_checkbox_val(that){
	var val = that.value;
	//alert(val);
	if($(that).is(':Checked')){
		$('input#referral').val(val);
	}else{
		$('input#referral').val('');
	}

}

function marketing_data_checkbox_val(that){
	var val = that.value;
	//alert(val);
	if($(that).is(':Checked')){
		$('input#marketing_data').val(val);
	}else{
		$('input#marketing_data').val('');
	}

}

function option_select(id)
{
		// $('#selctfrm').submit();
		window.location.href = "<?php echo base_url(); ?>contact/"+id;
}
/* $("#formsubmit").click(function() {
     if(validate()){
		alert('ready to submit');
        $('#formID').submit();
    }
   });
 */
function validate() {
    /*  if blank return false else true */
  if($('#borrower_chkbox_val').val() == '' && $('#lender_chkbox_val').prop('checked',false).val() == ''){
	  $('#chkbox_err').html('Please Choose Contact type Before Submit the form!').css('color','red');
	return false;

  }   else{
	   $('#chkbox_err').html('');
		return 1;
  }
}

function display_div(type){

	if($('#borrower_chkbox').prop('checked')  && $('#lender_chkbox').prop('checked')){
           // $('.borrower_contact_type').css('display','block');
           // $('.lender_contact_type')  .css('display','block');
		   $('#borrower_chkbox_val').val('1');
		   $('#lender_chkbox_val')  .val('1');
		   // $('.borrower_enabled').prop('disabled',false);
		   // $('.lender_enabled').prop('disabled',false);

	}else if ($('#borrower_chkbox').prop('checked')) {
		   // $('.borrower_contact_type').css('display','block');
           // $('.lender_contact_type')  .css('display','none');
		   $('#borrower_chkbox_val').val('1');
		   $('#lender_chkbox_val')  .val('');

		   // $('.borrower_enabled').prop('disabled',false);

    } else if($('#lender_chkbox').prop('checked')){
		   // $('.borrower_contact_type').css('display','none');
           // $('.lender_contact_type')  .css('display','block');
		   $('#borrower_chkbox_val')  .val('');
		   $('#lender_chkbox_val')    .val('1');
		   // $('.lender_enabled').prop('disabled',false);

    }else{
		   // $('.borrower_contact_type').css('display','none');
           // $('.lender_contact_type')  .css('display','none');
		   $('#borrower_chkbox_val').val('');
		   $('#lender_chkbox_val')  .val('');
		   // $('.borrower_enabled').prop('disabled',true);
		   // $('.lender_enabled').prop('disabled',true);
	}

}

$(document).ready(function(){
	if($('#borrower_chkbox').prop('checked')  && $('#lender_chkbox').prop('checked')){
           // $('.borrower_contact_type').css('display','block');
           // $('.lender_contact_type')  .css('display','block');
		   $('#borrower_chkbox_val').val('1');
		   $('#lender_chkbox_val')  .val('1');
		   // $('.borrower_enabled').prop('disabled',false);
		   // $('.lender_enabled').prop('disabled',false);

	}else if ($('#borrower_chkbox').prop('checked')) {
		   // $('.borrower_contact_type').css('display','block');
           // $('.lender_contact_type')  .css('display','none');
		   $('#borrower_chkbox_val').val('1');
		   $('#lender_chkbox_val')  .val('');

		   // $('.borrower_enabled').prop('disabled',false);

    } else if($('#lender_chkbox').prop('checked')){
		   // $('.borrower_contact_type').css('display','none');
           // $('.lender_contact_type')  .css('display','block');
		   $('#borrower_chkbox_val')  .val('');
		   $('#lender_chkbox_val')    .val('1');
		   // $('.lender_enabled').prop('disabled',false);

    }else{
		   // $('.borrower_contact_type').css('display','none');
           // $('.lender_contact_type')  .css('display','none');
		   $('#borrower_chkbox_val').val('');
		   $('#lender_chkbox_val')  .val('');
		   // $('.borrower_enabled').prop('disabled',true);
		   // $('.lender_enabled').prop('disabled',true);
	}
	if($('#us_citizen').val() == '2'){

		$('.us_citizen_optn').css('display','block');

	}else{

		$('.us_citizen_optn').css('display','none');
	}

	if($('#litigation').val() == '1'){

		$('.litigation_optn').css('display','block');

	}else{
		$('.litigation_optn').css('display','none');

	}

	if($('#felony').val() == '1'){

		$('.felony_optn').css('display','block');

	}else{
		$('.felony_optn').css('display','none');

	}

	if($('#Bankruptcy').val() == '1'){


		$('.BK_optn').css('display','block');

		if($('#BK').val() == '1'){
			$('.BK_textarea_div').css('display','block');
		}else{

			$('.BK_textarea_div').css('display','none');
		}
	}else{
		$('.BK_optn').css('display','none');
		$('.BK_textarea_div').css('display','none');
		$("select#BK").val('0');
	}


	if($('#checkbx_3').prop('checked') == true){
		$('.particular_user_dropdown').css('display','block');
	}
});

function display_BK(value){
	if(value == '1'){
		$('.BK_optn').css('display','block');
	}else{
		$('.BK_optn').css('display','none');
		$('.BK_textarea_div').css('display','none');
		$("select#BK").val('0');
	}
}

function display_litigation(value){
	if(value == '1'){
		$('.litigation_optn').css('display','block');
	}else{
		$('.litigation_optn').css('display','none');
		// $("select#litigation").val('0');
	}
}

function display_felony(value){
	if(value == '1'){
		$('.felony_optn').css('display','block');
	}else{
		$('.felony_optn').css('display','none');
		// $("select#litigation").val('0');
	}
}
function display_us_citizen(value){
	//alert(value);
	if(value == '2'){
		$('.us_citizen_optn').css('display','block');
	}else{
		$('.us_citizen_optn').css('display','none');
		// $("select#litigation").val('0');
	}
}

function display_bk_txtarea(value){
	if(value == '1'){
		$('.BK_textarea_div').css('display','block');
	}else{

		$('.BK_textarea_div').css('display','none');
	}
}

/* function display_lender_date_input(value){
	if(value == '1'){
		$('.RE_date').css('display','block');
	}else{
		$('.RE_date').css('display','none');

	}
} */



 function diasbled_checkbox(key,that,type){

	if($(that).is(':checked') && type=='yes'){

		$('input#hide_no_mail'+key).attr('disabled',true);
		$('input#hide_no_'+key).attr('disabled',true);
		$('input#l_hide_no_'+key).attr('disabled',true);
		$('input#opt_out_value_no_'+key).attr('disabled',true);

      }else{

      $('input#hide_no_mail'+key).attr('disabled',false);
      $('input#hide_no_'+key).attr('disabled',false);
      $('input#l_hide_no_'+key).attr('disabled',false);
      $('input#opt_out_value_no_'+key).attr('disabled',false);
      }

      if($(that).is(':checked') && type=='no'){

      	$('input#hide_mail'+key).attr('disabled',true);
      	$('input#hide_yes_'+key).attr('disabled',true);
      	$('input#l_hide_yes_'+key).attr('disabled',true);
      	$('input#opt_out_value_yes_'+key).attr('disabled',true);
      }else{

      $('input#hide_mail'+key).attr('disabled',false);
      $('input#hide_yes_'+key).attr('disabled',false);
      $('input#l_hide_yes_'+key).attr('disabled',false);
      $('input#opt_out_value_yes_'+key).attr('disabled',false);

	}

 }


 function hide_some_checkbox(that,key,type){


	// if($(that).is(':checked') && type=='yes'){

	// 	if(key == 4){

	// 		// $('input#hide_mail6').attr('disabled',true);
	// 		// $('input.borrower_hidew').attr('disabled',true);
	//     }
	// 	else if(key == 5){

	// 	 // 	$('input#hide_yes_1').attr('disabled',true);
	// 	 // 	$('input#hide_yes_3').attr('disabled',true);
	// 		// $('input#l_hide_yes_2').attr('disabled',true);
	// 	}
	// 	 //else if(key == 4 && key == 5){

	// 	// 	$('input#hide_mail').attr('disabled',true);
	// 	// 	$('input#hide_mail6').attr('disabled',true);
	// 	// 	$('input#hide_email2').attr('disabled',true);
	// 	// 	$('input.borrower_hide3').attr('disabled',true);

	// 	// }

	// }else{

	// 	if(key == 4){

	// 		// $('input#hide_mail6').attr('disabled',false);
	// 		//  $('input.borrower_hidew').attr('disabled',false);

	// 	 }
	// 	 else if(key == 5){

	// 		//  $('input#hide_yes_1').attr('disabled',false);
	// 	 // 	$('input#hide_yes_3').attr('disabled',false);
	// 		// $('input#l_hide_yes_2').attr('disabled',false);
	// 	 }
	// 	 //else if(key == 4 && key == 5){

	// 	// 	$('input#hide_mail').attr('disabled',false);
	// 	// 	$('input#hide_mail6').attr('disabled',false);
	// 	// 	$('input#hide_email2').attr('disabled',false);
	// 	// 	$('input.borrower_hide3').attr('disabled',false);

	// 	// }

	// }

	if($(that).is(':checked') && type=='no'){

		if(key == 4){

			// $('input.borrower_hide12').attr('disabled',true);
			// $('input#l_hide_no_7').attr('disabled',true);
			// $('input#hide_yes_1').attr('disabled',true);
			// $('input#hide_yes_3').attr('disabled',true);
			// $('input#hide_mail6').attr('disabled',true);
			// $('input#l_hide_yes_2').attr('disabled',true);
			// $('input#l_hide_yes_7').attr('disabled',true);
			// $('input#l_hide_yes_9').attr('disabled',true);
			$('input#opt_out_value_yes_4').attr('disabled',true);
			// $('input#opt_out_value_yes_5').attr('disabled',true);


		}else if(key == 5){

			// $('input#hide_no_1').attr('disabled',true);
		 // 	$('input#hide_no_3').attr('disabled',true);
			// $('input#l_hide_no_2').attr('disabled',true);
			// $('input#hide_yes_1').attr('disabled',true);
			// $('input#hide_yes_3').attr('disabled',true);
			// $('input#hide_mail6').attr('disabled',true);
			// $('input#l_hide_yes_2').attr('disabled',true);
			// $('input#l_hide_yes_7').attr('disabled',true);
			// $('input#l_hide_yes_9').attr('disabled',true);
			// $('input#opt_out_value_yes_4').attr('disabled',true);
			$('input#opt_out_value_yes_5').attr('disabled',true);

		}

		//else if(key == 4 && key == 5){

		// 	$('input#hide_mail').attr('disabled',true);
		// 	$('input#hide_mail6').attr('disabled',true);
		// 	$('input#hide_email2').attr('disabled',true);
		// 	$('input.borrower_hide3').attr('disabled',true);

		// }

	}else{

		if(key == 4){

		// $('input.borrower_hide12').attr('disabled',false);
		// $('input#l_hide_no_7').attr('disabled',false);
		// $('input#hide_yes_1').attr('disabled',false);
		// $('input#hide_yes_3').attr('disabled',false);
		// $('input#hide_mail6').attr('disabled',false);
		// $('input#l_hide_yes_2').attr('disabled',false);
		// $('input#l_hide_yes_7').attr('disabled',false);
		// $('input#l_hide_yes_9').attr('disabled',false);
		 //$('input#opt_out_value_yes_4').attr('disabled',false);
		// $('input#opt_out_value_yes_5').attr('disabled',false);

		}else if(key == 5){

		  //   $('input#hide_no_1').attr('disabled',false);
		 	// $('input#hide_no_3').attr('disabled',false);
			// $('input#l_hide_no_2').attr('disabled',false);
			// $('input#hide_yes_1').attr('disabled',false);
			// $('input#hide_yes_3').attr('disabled',false);
			// $('input#hide_mail6').attr('disabled',false);
			// $('input#l_hide_yes_2').attr('disabled',false);
			// $('input#l_hide_yes_7').attr('disabled',false);
			// $('input#l_hide_yes_9').attr('disabled',false);
			// $('input#opt_out_value_yes_4').attr('disabled',false);
			$('input#opt_out_value_yes_5').attr('disabled',false);

	}

	 //else if(key == 4 && key == 5){

		// 	$('input#hide_mail').attr('disabled',false);
		// 	$('input#hide_mail6').attr('disabled',false);
		// 	$('input#hide_email2').attr('disabled',false);
		// 	$('input.borrower_hide3').attr('disabled',false);

		// }
	}


}






function borrower_advertisement(that){

	if($(that).is(':checked')){

		$('input#advertisement_borrower').val('1');
		$('input.borrower_hide').attr('disabled',true);
		$('input.borrower_hide').val('0');
	}else{
		$('input#advertisement_borrower').val('0');
		$('input.borrower_hide').attr('disabled',false);

	}

}


function lender_advertisement(that){

	if($(that).is(':checked')){

		$('input#advertisement_lender').val('1');
		$('input.lender_hide').attr('disabled',true);
		$('input.lender_hide').val('0');

	}else{

		$('input#advertisement_lender').val('0');
		$('input.lender_hide').attr('disabled',false);
	}

}

$(document).ready(function(){

	var borrowers = $('input#advertisement_borrower').val();
	var lender 	 = $('input#advertisement_lender').val();

	if(borrowers == 1){

		$('input.borrower_hide').attr('disabled',true);
		$('input.borrower_hide').val('0');
	}else{
		$('input.borrower_hide').attr('disabled',false);
	}


	if(lender == 1){

		$('input.lender_hide').attr('disabled',true);
		$('input.lender_hide').val('0');
	}else{
		$('input.lender_hide').attr('disabled',false);
	}
});




function display_alternate_contact_fields(value){
	if(value == '1'){
		$('.alternate_contact_fields_display').css('display','block');
		$('#lender_alt_first_name') .prop('required',true);
		// $('#lender_alt_middle_name').prop('required',true);
		$('#lender_alt_last_name')  .prop('required',true);

	}else{
		$('.alternate_contact_fields_display').css('display','none');
		$('#lender_alt_first_name') .prop('required',false);
		// $('#lender_alt_middle_name').prop('required',false);
		$('#lender_alt_last_name')  .prop('required',false);

	}
}

$(document).ready(function(){
	var value= $('#alternate_contact_fields').val();
	if(value == '1'){
		$('.alternate_contact_fields_display').css('display','block');
		$('#lender_alt_first_name') .prop('required',true);
		// $('#lender_alt_middle_name').prop('required',true);
		$('#lender_alt_last_name')  .prop('required',true);

	}else{
		$('.alternate_contact_fields_display').css('display','none');
		$('#lender_alt_first_name') .prop('required',false);
		// $('#lender_alt_middle_name').prop('required',false);
		$('#lender_alt_last_name')  .prop('required',false);

	}
})


function borrower_select(id)
{

	window.location.href = "<?php echo base_url(); ?>contact/B"+id;

}
function investor_select(id)
{

	window.location.href = "<?php echo base_url(); ?>contact/L"+id;

}
function contact_select(id)
{

	window.location.href = "<?php echo base_url(); ?>contact/C"+id;

}

function delete_contact()
{
    if (confirm("Are you sure to delete?") == true) {
        $('#delete_contact_form').submit();
    }
}

function copy_address(){
	if($('#same_mailing_chkbx').attr('checked')){

		var street_address 	= $('#address').val();
		var unit 			= $('#unit').val();
		var city 			= $('#city').val();
		var state 			= $('#state').val();
		var zip 			= $('#zip').val();

		$('#mailing_address').val(street_address);
		$('#mailing_unit').val(unit);
		$('#mailing_city').val(city);
		$('#mailing_state').val(state);
		$('#mailing_zip').val(zip);

		$('#same_mailing_address').val('1');

	}else{


		$('#mailing_address').val('');
		$('#mailing_unit').val('');
		$('#mailing_city').val('');
		$('#mailing_state').val('');
		$('#mailing_zip').val('');

		$('#same_mailing_address').val('0');
	}
}

function fill_mailing_portion(id,value){

	if($('#same_mailing_chkbx').attr('checked')){

		var idd = 'mailing_'+id;

		$('#mailing_'+id).val(value);

		if(idd == 'mailing_state'){


			// var aa = $('#'+id+'input[value=AX]').text();
			var aa = $('.select_state .bootstrap-select .filter-option').text();

			$('.select_mailing_state .bootstrap-select .filter-option').text(aa);
		}

		$('#same_mailing_address').val('1');

	}else{

		$('#mailing_'+id).val('');
		$('#same_mailing_address').val('0');

	}
}


var i= 1;
function borrower_add_more_row(){
	  i++;

		 // $('#add_row').css("display","block");
	 if(i < 6){

	  $( ".doc-div" ).append( '<br><div class="row"><div class="col-md-4"><input type="file" id = "borrower_upload_'+i+'" name ="contact_upload_doc[]" class="select_images_file" onchange="change_this_image(this)" name="borrower_files[]"></div></div>' );
	 }else{
		 // $('#add_row').css("display","none");
		 // alert('no mpre row added');
	 }

}

var j= 1;
function lender_add_more_row(){
	 j++;

		 // $('#add_row').css("display","block");
	 if(j < 6){

	  $( ".doc-div1" ).append( '<br><div class="row"><div class="col-md-4"><input type="file" id = "lender_upload_'+j+'" name ="lender_upload_doc[]" class="select_images_file" onchange="change_this_image(this)" name="lender_files[]"></div></div>' );
	 }else{
		 // $('#add_row').css("display","none");
		 // alert('no mpre row added');
	 }

  }


function read_document(id){

	 // alert('ddd');
	 $('#document_id').val(id);
	 // $('#documentfrm').submit();
	  window.location.href = "<?php echo base_url() . 'Contact/contact_document_read/' ?>"+id;
 }
 function check_duplicacy(value,field){

 	$('#formsubmitShow').hide();
 	$('#formsubmitHide').show();

 		var contact_id = $("#contact_id").val();
		var email 			= $('#email').val();
		var email_hidden 	= $('#email_hidden').val();
		var first_name 		= $('#first_name').val();
		var last_name 		= $('#last_name').val();
		var edit_id 		= '<?php echo $this->uri->segment(2); ?>';

		 if(value != ''){
			// ajax call to check duplicacy...
				// $('.err_message').html('');
			$.ajax({
				type: "post",
				url: "<?php echo base_url() ?>Contact/check_duplicacy/",
				data:{'value':value,'field':field,'contact_id':contact_id,'email':email,'first_name':first_name,'last_name':last_name},
				success: function(response){
					if(response == 0){
						$('.err_message').html('<div class = "col-md-12 err_contact_email" >The primary email id already exist, please use a different email address.</div>');
					}else{
						$('#formsubmitShow').show();
 						$('#formsubmitHide').hide();
						$('.err_'+field).html('');
					}
				}
			});
		}
}

function check_permission_option(value,id){

	if(value == '3'){
		$('.particular_user_dropdown').css('display','block');

		if($('#checkbx_2').prop('checked') == true){
			$('#checkbx_2').prop('checked',false);


		}
		if($('#checkbx_3').prop('checked') == false){
			$('.particular_user_dropdown').css('display','none');
		}
		/* else{
			$('#checkbx_2').prop('checked',true);


		} */
	}else if(value == '2'){

		if($('#checkbx_2').prop('checked') == true){

			$('#checkbx_1').attr('disabled',true);
			$('#checkbx_4').attr('disabled',true);
			$('#checkbx_5').attr('disabled',true);
			$('#checkbx_3').attr('disabled',true);

			$('#checkbx_1').prop('checked',false);
			$('#checkbx_4').prop('checked',false);
			$('#checkbx_5').prop('checked',false);
			$('#checkbx_3').prop('checked',false);
			$('#particular_user_id').val('');

		}
		 else{
			// $('#checkbx_3').prop('checked',true);
			$('#checkbx_1').attr('disabled',false);
			$('#checkbx_4').attr('disabled',false);
			$('#checkbx_5').attr('disabled',false);
			$('#checkbx_3').attr('disabled',false);

		}
		$('.particular_user_dropdown').css('display','none');
	}else if(value == '1'){

		$('#checkbx_3').prop('checked',false);
		$('.particular_user_dropdown').css('display','none');
	}else{


		$('.particular_user_dropdown').css('display','none');

	}

}

$(document).ready(function(){

  /***phone number format***/
  $(".phone-format").keypress(function (e) {

    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
    }
    var curchr = this.value.length;
    var curval = $(this).val();
    if (curchr == 3 && curval.indexOf("(") <= -1) {
      $(this).val("(" + curval + ")" + "-");
    } else if (curchr == 4 && curval.indexOf("(") > -1) {
      $(this).val(curval + ")-");
    } else if (curchr == 5 && curval.indexOf(")") > -1) {
      $(this).val(curval + "-");
    } else if (curchr == 9) {
      $(this).val(curval + "-");
      $(this).attr('maxlength', '14');
    }
  });
});
// $.noConflict();


$(document).ready(function(){

	var opt_out5 = $('input#opt_out_value5').attr('checked');
	var opt_out4 = $('input#opt_out_value4').attr('checked');

	if(opt_out5 == 'checked'){

		$('input#hide_email2').attr('disabled',true);
		$('input#hide_email13').attr('disabled',true);


	}else{

		$('input#hide_email2').attr('disabled',false);
		$('input#hide_email13').attr('disabled',false);

	}

	if(opt_out4 == 'checked'){

		$('input#hide_mail').attr('disabled',true);
		//$('input#hide_mail6').attr('disabled',true);

	}else{

		$('input#hide_mail').attr('disabled',false);
		//$('input#hide_mail6').attr('disabled',false);
	}



});

	function data_check(){

	 var first_name= $("#first_name").val();

		var contact_permission= $('input#contact_permission[type=checkbox]:checked');
		var internal_contact= $("#internal_contact").val();
		//var contact_specialty= $('input.contact_specialty[type=checkbox]:checked');
	  // alert(contact_specialty);
		var referral_source= $('input#referral_source[type=checkbox]:checked');
		//var borrower_hide= $('input.borrower_hide[type=checkbox]:checked');
		//var lender_hide= $('input.lender_hide[type=checkbox]:checked');
		//var borrower_ads= $('input.ba[type=checkbox]:checked');
		//var lender_ads= $('input.la[type=checkbox]:checked');
		var email= $('#email').val();

   // if(first_name == '' && internal_contact == '' && contact_specialty.length == 0 && referral_source.length == 0 && borrower_hide.length == 0 && lender_hide.length == 0  && contact_permission.length == 0  )
   if(first_name == '' && email == '' && internal_contact == '' && referral_source.length == 0  && contact_permission.length == 0)


   {

	$("div.errorrr").css('display','block');
	$("div.errorrr").text("Contact Data Not Complete");
	$("#first_name").focus().css('border','1px solid red');
	$("#email").focus().css('border','1px solid red');
	$("#internal_contact").focus().css('border','1px solid red');

	//$(".specialty").focus().css('border','1px solid red');

	$(".ref").focus().css('border','1px solid red');
	//$(".borrower").focus().css('border','1px solid red');
	//$(".lender").focus().css('border','1px solid red');
	$(".permission").focus().css('border','1px solid red');
		return false;

	 }

	else if(first_name == '') {

	$("div.errorrr").css('display','block');
	$("div.errorrr").text("First Name Not Complete");

		$("#first_name").focus().css('border','1px solid red');

		$("#internal_contact").focus().css('border','');

		//$(".specialty").focus().css('border','');

		$(".ref").focus().css('border','');
		//$(".borrower").focus().css('border','');
	//	$(".lender").focus().css('border','');
		$(".permission").focus().css('border','');


			return false;

	 }

	 else if(email == '') {

	$("div.errorrr").css('display','block');
	$("div.errorrr").text("Primary email address Not Complete");

		$("#email").focus().css('border','1px solid red');

		$("#internal_contact").focus().css('border','');

		//$(".specialty").focus().css('border','');

		$(".ref").focus().css('border','');
		//$(".borrower").focus().css('border','');
	//	$(".lender").focus().css('border','');
		$(".permission").focus().css('border','');


			return false;

	 }

	else if(referral_source.length == 0){

		$("div.errorrr").css('display','block');
		$("div.errorrr").text("Referral Source Not Complete");
		$(".ref").focus().css('border','1px solid red');


		$("#first_name").focus().css('border','');

		//$(".specialty").focus().css('border','');

		$("#internal_contact").focus().css('border','');

		//$(".borrower").focus().css('border','');
		//$(".lender").focus().css('border','');
		$(".permission").focus().css('border','');


		return false;

	 }
	 else if(internal_contact == ''){

	 	$("div.errorrr").css('display','block');
		$("div.errorrr").text("Internal Contact Not Complete");

		 $("#internal_contact").focus().css('border','1px solid red');

		$("#first_name").focus().css('border','');
		$(".permission").focus().css('border','');


		//$(".specialty").focus().css('border','');
		$(".ref").focus().css('border','');
		//$(".borrower").focus().css('border','');
		//$(".lender").focus().css('border','');


		 return false;

	 }

	 //  else if(contact_specialty.length == 0){

	 //  	$("div.errorrr").css('display','block');
		// $("div.errorrr").text("Contact Specialty Not Complete");

		//  //$(".specialty").focus().css('border','1px solid red');

		// $("#first_name").focus().css('border','');

		// $("#internal_contact").focus().css('border','');

		// $(".ref").focus().css('border','');
		// ///$(".borrower").focus().css('border','');
		// //$(".lender").focus().css('border','');
		// $(".permission").focus().css('border','');

		//  return false;
	 // }



	 //  else if(borrower_hide.length == 0 && borrower_ads.length == 0){

	 //  	$("div.errorrr").css('display','block');
		// $("div.errorrr").text("Contact Specialty Borrower Not Complete");

		// //$(".borrower").focus().css('border','1px solid red');

		// $("#first_name").focus().css('border','');

		// //$(".specialty").focus().css('border','');

		// $("#internal_contact").focus().css('border','');
		// $(".ref").focus().css('border','');
		// $(".permission").focus().css('border','');

		// //$(".lender").focus().css('border','');


		//  return false;

	 // }

// 	  else if(lender_hide.length == 0 && lender_ads.length == 0){

// 	  	$("div.errorrr").css('display','block');
// 		$("div.errorrr").text("Contact Specialty Lender Not Complete");

// 		//$(".lender").focus().css('border','1px solid red');


// 		$("#first_name").focus().css('border','');
// //
// 		$(".specialty").focus().css('border','');

// 		$("#internal_contact").focus().css('border','');
// 		$(".ref").focus().css('border','');
// 		//$(".borrower").focus().css('border','');
// 		$(".permission").focus().css('border','');


// 		 return false;

// 	 }

	 //   else if(contact_permission.length == 0){

	 //   	$("div.errorrr").css('display','block');
		// $("div.errorrr").text("Contact Permission Not Complete");

		// $(".permission").focus().css('border','1px solid red');


		// $("#first_name").focus().css('border','');

		// $(".specialty").focus().css('border','');

		// $("#internal_contact").focus().css('border','');
		// $(".ref").focus().css('border','');
		// $(".borrower").focus().css('border','');
		// $(".lender").focus().css('border','');


		//  return false;

	 // }



	 else{

		 return true;
	 }



	}

$(document).ready(function(){

if($('#hide_no_mail6').is(':checked')){
	//$('input#hide_mail6').attr('disabled',true);


}



	if($('#opt_out_value_yes_4').is(':checked')){


			// $('input#hide_mail6').attr('disabled',true);
			//  $('input.borrower_hidew').attr('disabled',true);

}

if($('#opt_out_value_yes_5').is(':checked')){


			// $('input#hide_yes_1').attr('disabled',true);
		 // 	$('input#hide_yes_3').attr('disabled',true);
			// $('input#l_hide_yes_2').attr('disabled',true);



}
if($('#opt_out_value_no_4').is(':checked')){

			// $('input.borrower_hide12').attr('disabled',true);
			// $('input#l_hide_no_7').attr('disabled',true);
			// $('input#hide_yes_1').attr('disabled',true);
			// $('input#hide_yes_3').attr('disabled',true);
			// $('input#hide_mail6').attr('disabled',true);
			// $('input#l_hide_yes_2').attr('disabled',true);
			// $('input#l_hide_yes_7').attr('disabled',true);
			// $('input#l_hide_yes_9').attr('disabled',true);
			$('input#opt_out_value_yes_4').attr('disabled',true);
			// $('input#opt_out_value_yes_5').attr('disabled',true);



}
if($('#opt_out_value_no_5').is(':checked')){

			// $('input#hide_no_1').attr('disabled',true);
		 // 	$('input#hide_no_3').attr('disabled',true);
			// $('input#l_hide_no_2').attr('disabled',true);

			// $('input#hide_yes_1').attr('disabled',true);
			// $('input#hide_yes_3').attr('disabled',true);
			// $('input#hide_mail6').attr('disabled',true);
			// $('input#l_hide_yes_2').attr('disabled',true);
			// $('input#l_hide_yes_7').attr('disabled',true);
			// $('input#l_hide_yes_9').attr('disabled',true);
			// $('input#opt_out_value_yes_4').attr('disabled',true);
			$('input#opt_out_value_yes_5').attr('disabled',true);


}

});


</script>

<!-- <script type="text/javascript" src="<?php //echo base_url("assets/js/contact/contactlist.js"); ?>"></script> -->