<?php
$STATE_USA = $this->config->item('STATE_USA');
$borrower_type_option = $this->config->item('borrower_type_option');
$company_type_option = $this->config->item('company_type_option');
$marital_status = $this->config->item('marital_status');
$employment_status = $this->config->item('borrower_employment_status');

$us_yes_no_option = $this->config->item('us_yes_no_option');
$loan_tye = $this->config->item('loan_tye');
$ltv_option = $this->config->item('ltv_option');
$yeild_percent = $this->config->item('yeild_percent');
$LoanTerm_type = $this->config->item('LoanTerm_type');
$re885_complete_option = $this->config->item('re885_complete_option');

$position_optionn = $this->config->item('position_optionn');

$yes_no_option = $this->config->item('yes_no_option');
$yes_no_option3 = $this->config->item('yes_no_option3');
$no_yes_option = $this->config->item('no_yes_option');
$notes_type = $this->config->item('notes_type');
$contact_type = $this->config->item('contact_type');
$contact_specialty = $this->config->item('contact_specialty');
$contact_email_blast = $this->config->item('contact_email_blast');
$relationships_option = $this->config->item('relationships_option');
$contact_status_option = $this->config->item('contact_status_option');
$task_option = $this->config->item('contact_status_option');
$contact_permission = $this->config->item('contact_permission');
$lender_investment_experience = $this->config->item('lender_investment_experience');
$lender_financial_situation = $this->config->item('lender_financial_situation');
$lender_liquidity_needs = $this->config->item('lender_liquidity_needs');
$referral_source = $this->config->item('referral_source');
$contact_specialty_trust = $this->config->item('contact_specialty_trust');
$lender_property_type = $this->config->item('lender_property_type');
$lender_lend_value = $this->config->item('lender_lend_value');
$lender_lend_value = $this->config->item('lender_lend_value');
$contact_number_option = $this->config->item('contact_number_option');
$email_types_option = $this->config->item('email_types_option');
$selct_contact_task = $this->config->item('selct_contact_task');
$contact_marketing_data = $this->config->item('contact_marketing_data');
$contact_marketing_status_option = $this->config->item('contact_marketing_status_option');
$contact_purpose_menu = $this->config->item('contact_purpose_menu');
$portal_access_option = $this->config->item('portal_access_option');
$lender_portal_access = $this->config->item('lender_portal_access');
$setup_package_option = $this->config->item('setup_package_option');

/*21-07-2021 By@aj*/
$ofac_srch_option     = $this->config->item('ofac_srch_option');
$brodge_loan_option   = $this->config->item('brodge_loan_option');
$fix_flip_loan_option = $this->config->item('fix_flip_loan_option');
$ct_hm_construct_loan     = $this->config->item('hm_construct_loan');

/*End*/
?>
<style>

	span img {
    width: 30px;
}

.bro,td,th {
    border: none !important;
}
#personal_notes{
	border: 1px solid white !important;
}
.main-left-div-res{
    width: 100%;
    margin-left: 0% !IMPORTANT;
    float: right;
    margin-right: 0%;
}
.main-left-div-res .main-title .col-md-3, .title .col-md-3 {
	width:100% !important;
    margin-bottom: 10px;
}
.main-left-div-res .row.borrower_data_row .col-md-3 {
    width: 18%;
}
.main-left-div-res .row.borrower_data_row .col-md-8 {
    width: 80% !important;
}
.main-left-div-res .row.borrower_data_row div
.main-left-div-res .borrower_data_row {
    padding: 0px !important;
}

.main-left-div-res .borrower_data_row {
    padding: 0px !important;
    width: 100%;
	margin-top: 5px;
}
.main-left-div-res .col-md-1 {
    width: 9.4%;
}
.main-left-div-res .col-md-4 {
    width: 48.6%;
}
.main-left-div-res .new-notes,.main-left-div-res .saved-notes {
    width: 98% !important;
    float: left;
    border-right: 0px solid #eee;
}
.main-left-div-res  .new-notes .title {
    padding: 5px 0px !important;
}

.main-left-div-res .notes_class {
    float: left;
    width: 25% !important;
}

.main-left-div-res .new-notes .notes_class .btn-group.bootstrap-select {
      max-width: 125px;
}
.main-left-div-res .new-notes .spc-loan-div.notes_class .btn-group.bootstrap-select {
    max-width: 175px;
}

.main-left-div-res .row.borrower_data_row label.form-control{
	font-weight: normal !important;

}
.main-left-div-res .saved-notes .lender_data_row.three_btns {
    padding: 2px 30px !important;
}
.main-left-div-res .row.inline-label2 .spc-loan-div {
    float: left;
    margin-left: 0px;
}
.main-left-div-res .spc-loan-div .history_label {

    width: 52% !important;
}

.main-left-div-res .spc-loan-div{
	width:30% !important;
}

.main-left-div-res .borrower_data_row {
    padding:0px !important;
}
.main-left-div-res .dates_class {

}


.main-left-div-res .dates_class {
   width: 115px !important;
}

.main-left-div-res .history_label {
    width: 115px;
}

.row.inline-label2 div {

    margin-left: 2%;
}
.selected_loans .btn-group.bootstrap-select {
    width: 125px !important;
}

#fetch_notes_date {
	    width: 106px;

}
div#p_loan {
    margin-left: -16px !important;
}
.row.inline-label2 div label {
    float: left;
    margin-right: 11px;
    line-height: 20px;
}

.row.inline-label2 div input {
    /* float: right; */
    width: initial;
    /* max-width: 243px; */
}

.new-notes .title{
    padding: 5px 60px !important;
}
.lender_data_row.main_actions {
	float: right;
    width: 100%;
    margin-right: 120px;
    margin-top: 8px;
}
.lender_data_row .main_actions {
	padding: 0px !important;
}
.lender_data_row.three_btns {
    padding: 2px 90px !important;
}
.lender_data_row.three_btnss {
    padding: 15px 425px !important;
}

.tags_display .form-control {
    font-size: 13px;
}

.three_btns {
	float:right !important;
}
.three_btns {
	    float: left;
    margin-left: 440px;
    margin-top: 15px;
}

.main-right-div,.main-left-div,.new-notes,.saved-notes{
	float:left
}



.row.contact_tags_row {
    padding-left: 40px;
	padding-bottom: 15px;
}
.contact_tags_row .col-md-8{
	padding:5px;
}
.contact_tags_row .col-md-3{
	padding:5px;
}

.main-left-div{
	width:70%;
	border-right: 1px solid #eee;


}
.main-right-div{
	width:30%
}
.right-div-label label{
	height:initial !important;
}
.row.borrower_data_row label {
    font-weight: 400;
}

.main_select_div{
	float: right;

}
.tag_labels label {
    float: left;
    padding: 10px;
	    border: 1px solid silver;
    border-radius: 3px;

}
.tag_labels {
    float: left;
	margin-left:10px;

}
.tags_display{
	float: left;
    width: 82% !important;


}
.left_fields{
	float:right !important;
}

#borrower_data .borrower_data_row ,#lender_data .lender_data_row {

	    padding: 11px 2px !important;
}
#table_trust_deed, #table_loan{
	margin-left: 15px;
}
.tab-pane{
	margin-bottom:20px;
}
.user_content{
	padding: 10px 25px;
}
.btn-group.bootstrap-select{
	width:162px !important;
}
.search_contact {

	float:right;
	margin-top: 15px;
	margin-right: 150px;
}

label.title_history {
    line-height: 30px !important;
	font-weight:600 !important;
}
.history_label {
    width: initial;
}
#selctfrm .btn-group.bootstrap-select{
	width:240px !important;
}
.same_check #uniform-same_mailing_address{
	width:7% !important;
	float:left;
}
.inline-label .col-md-3 {
    width: 15% !important;
}
.inline-label2 .col-md-3 {
    width: 15% !important;
}
.inline-label .col-md-3 label {
    line-height: 30px;
	font-weight:600 !important;
}
.stats1, .stats2{
	float: left;
	border: 1px solid;
	width: 45% !important;
}
.b_right {
	margin-right: 15px;
}
.notes_class .btn-group.bootstrap-select {
    max-width: 100px;
}
.stats2 {
    margin-left: 80px;
}
.stats1_div label,.stats2_div label{
   padding-left: 0px !important;
}

.bs-searchbox{
	width:100% !important;
}
.btn .caret {
    margin-left: 20px !important;
}
.relationship_div .btn.dropdown-toggle.selectpicker.btn-default{
	float: left;

	margin-top: 10px !important;
}
.relation_labels{
	margin-bottom: 5px;
	float:left;
	width:100%;
}
.show_acc_to_permission{

	display:block ;
}
.hide_acc_to_permission{

	display:none ;

}
.row.borrower_data_row.history_row div {
    width: 25%;
}
.history_label{
	font-weight:400 !important;
}
.history_action{
	float:right;
	margin-top:6px;
}
.contact_document_lists li{
	list-style: none;
	padding: 5px;
	font-size: 14px;
}

.doc-div{
	margin-bottom: 15px;
}


.main-left-div-res .col-md-4 label,.main-left-div-res .col-md-6 label{
	line-height:30px;
}
.main-left-div-res .inline-label .col-md-3 {
    width: 15% !important;
}
.main-left-div-res .inline-label .col-md-4 {
    width: 35% !important;
}
@media only screen and (min-width: 992px) and (max-width: 1199px) {
.main-left-div-res .dates_class {
    width: 99px !important;
}
.main-left-div-res .new-notes .notes_class .btn-group.bootstrap-select {
    max-width: 114px !important;
}
.main-left-div-res .new-notes .spc-loan-div.notes_class .btn-group.bootstrap-select {
    max-width: 132px !important;
}

}

@media only screen and (min-width: 768px) and (max-width: 992px) {
.main-left-div-res .new-notes .notes_class .btn-group.bootstrap-select {
    max-width: 110px !important;
}
.main-left-div-res .dates_class {
    width: 120px !important;
}
.main-left-div-res .new-notes .spc-loan-div.notes_class .btn-group.bootstrap-select {
    max-width: 175px !important;
}
}




@media only screen and (max-width: 767px){
	.row.address-fields {
		padding: 5px 60px !important;
	}
	.page-header .page-header-top .top-menu {
    display: block;
    clear: none;
    margin-top: 14px;
}
	.page-header .page-header-top .menu-toggler {
    display: block;
}
.lender_data_row.main_actions {
    float: right;
    width: 100%;
    margin-right: 0;
    padding-top: 0px;
    margin-top: 0px;
}
.margin-top-10 {
    margin-top: 0px !important;
}
.main_select_div {
    float: right;
    width: 100% !important;
}
.search_contact {
    float: right;
    margin-top: 0px !important;
    margin-right: 0px !important;
}
.page-head .page-title {
    padding: 6px 0 !important;
}
.main-left-div-res .row.borrower_data_row .col-md-8 {
    width: 100% !important;
}
.main-left-div-res .row.borrower_data_row div {
    float: left;
    width: 100% !important;
    margin-bottom: 0px !important;
}
 .main-left-div {
    width: 100%;
    border-right: 0px solid #eee !important;
}
.main-left-div-res form#selctfrm {
    margin-bottom: 0px !important;
}
.main-left-div-res .row.address-fields {
    padding: 5px 14px !important;
}
.main-left-div-res .col-md-1 {
    width: 100%;
}
.main-left-div-res .tags_display {
    float: left;
    width: 100% !important;
}
.main-left-div-res .col-md-4 {
    width: 100%;
    float: left;
}

.main-left-div-res .new-notes {
    width: 100%;
    float: left;
    border-right: 0px solid #eee;
}
.main-left-div-res .row.inline-label2 div label {
    float: left;
    margin-right: 11px;
    line-height: 20px;
    width: 100%;
}
.main-left-div-res .dates_class {
    width: 100% !important;
}
.main-left-div-res .new-notes .notes_class .btn-group.bootstrap-select {
    max-width: 100% !important;
}
.main-left-div-res .row.inline-label2 .spc-loan-div {
    float: left;
    margin-left: 2% !important;
}
.main-left-div-res .spc-loan-div .history_label {
    width: 100% !important;
}
.main-left-div-res .history_action .col-md-6{
	padding-right:0px !important;
}
.main-left-div-res .col-md-9 {
		padding-right:0px !important;
		padding-left:0px !important;
}
.main-right-div {
    width: 100%;
}
.main-left-div-res .row.borrower_data_row.inline-label .col-md-3, .main-left-div-res .row.borrower_data_row.inline-label .col-md-4, .main-left-div-res .row.borrower_data_row.inline-label .col-md-6{
    width: 42% !important;
    float: left;
}
.notes_class .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    width: 100% !important;
}
}

@media (max-width: 469px) {


}
.main-left-div-res .col-md-4 {
    width:28% !important;
}
textarea.border-none {
    border: none;
}
div#page-viewcontact .main-left-div h3 {
    background: #eeeeee;
    padding: 8px 10px;
    font-size: 17px;
    font-weight: 500;
}
form.history-filter {
    padding: 0px !important;
    margin: 0px !important;
}
.row.borrower_data_row.borrower_textarea_div input.pull-right {
    margin: 6px 14px !important;
}
label#label_notes_date, label#label_notes_type, label#label_loan_id {
    border: none;
}
.row11 {
    border: 1px solid #eeeeee;
    padding: 10px 0px;
}
form.history-filter {
    padding: 0px 0px 10px 0px !important;
    margin: 0px !important;

}
.history_action form {
    border: none !important;
}



span.a>h4{

font-family: sans-serif;
}

div#contactActivetable_filter {
    float: right;
}

.col-md-3.abc {
    margin-top: 20px;
}
strong {
    font-weight: 600;
}
div#fetch_select_talimar_loan_chosen {
    width: 181px !important;
}

div#model_left_align {
    margin-left: 7px !important;
}
.alert-success {

    border-color: #d6e9c6;
    color: #009688;
    background-color: rgba(139, 195, 74, 0.48);
}

.margin_checkbox{
	margin-bottom: 4px;
}

#hardmoney .addhmspace{
    margin-bottom: 10px;
}

#hardmoney label{
    font-weight: 700 !important;
}

.margin_checkbox{
    margin-bottom: 4px;
}

div.checker, div.checker span, div.checker input {
   /* width: 19px;*/
    height: 19px;
}
input[type="checkbox"] {
  /* width: 19px;*/
    display: inline-block;
    width: 19%;
    height: 19px;
}
input.amount_format.selectpicker {
    border: #e5e5e5 solid 1px;
    height: 36px;
    border-radius: 5px;
    padding: 15px;
}
.row > .rowh4{
        	background-color:#efefef;
        	padding: 10px; 
        	font-family: sans-serif;
        }
div.checkId > .checker{
			width: 6%!important;
		}
		div.checkId1 > .checker{
			width: 10%!important;
		}
        /*20-07-2031*/
        label#ab_follow_up_task,label#ab_follow_up_date,label#ab_follow_up_status{
            border: none;
        }
        .notes_class.row_follow_up_date,.notes_class.row_follow_up_status{
            width: 29% !important;
        }
        .notes_class.row_notes_type{
            width: 31% !important;
        }
        .row.borrower_data_row.inline-label2.min_row {
    padding-top: 13px !important;
}
form.history-filter {
    border-bottom: 1px solid gray;
}
form.history-filter {
    padding-top: 15px !important;
}
.col-md-9.note_desc, .note_enter_by {
    padding-bottom: 10px;
}
</style>
<div class="page-container">

	<!-- BEGIN PAGE HEAD -->

	<div class="container" id="page-viewcontact">
		<div class="tab-pane">
			<!--------------------MESSEGE SHOW-------------------------->
			<?php if ($this->session->flashdata('error') != '') {?>
				<div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error'); ?></div><?php }?>
			<?php if ($this->session->flashdata('success') != '') {?>
				<div id='success'><i class='fa fa-thumbs-o-up'></i> <?php echo $this->session->flashdata('success'); ?></div><?php }?>
			<div id="chkbox_err"></div>
			<!-------------------END OF MESSEGE-------------------------->
			<!-- BEGIN PAGE HEAD -->

			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				

				<div class="main_select_div" >


					<!------------------Buttons Section Starts--------------------------->
						<div class="row margin-top-10">
							<div class="lender_data_row main_actions">
								<div class="form-group">

								  <div class="">
								<?php if (is_numeric($this->uri->segment(2))) {
	?>
								<?php
if (isset($contact_id)) {
		if ($user_role == '2') {?>

										<button type="button" class="btn btn-danger" onclick="delete_contact()" <?php echo $disabled; ?>>Delete</button>
										<?php
}}
	?>

								<?php

	$fetch_user_id = $this->User_model->query("SELECT user_id FROM contact WHERE contact_id = '" . $this->uri->segment(2) . "' AND user_id = '" . $this->session->userdata('t_user_id') . "'");
	if ($fetch_user_id->num_rows() > 0) {

		$acces_contcat = 1;
	} else {
		$acces_contcat = 2;
	}

	if ($loginuser_contacts == 1) {

		$blocked = '';

	} elseif ($loginuser_contacts == 2 && $acces_contcat == 1) {

		$blocked = '';
	} else {
		$blocked = 'disabled';
	}

	?>

								<a id="singlebutton"  class="btn btn-primary" href="<?php echo base_url() . 'contact/' . $this->uri->segment(2); ?>" <?php echo $blocked; ?>>Edit</a>

								<a id="singlebutton"  class="btn btn-primary" href="<?php echo base_url() . 'contactlist'; ?>" >Close</a>

								<?php }?>
								<!--<a id="singlebutton"  class="btn btn-primary" href="<?php echo base_url(); ?>contact" >Add New</a>-->

								  </div>
								</div>
							</div>
						</div>
						<!------------------End Buttons Section--------------------------->
				</div>
			</div>
			<!-- END PAGE HEAD -->
			<div class="page-container">

				<!-- BEGIN PAGE BREADCRUMB -->
				<ul class="page-breadcrumb breadcrumb hide">
					<li>
						<a href="#">Home</a><i class="fa fa-circle"></i>
					</li>
					<li class="active">
						 Dashboard
					</li>
				</ul>
				<!-- END PAGE BREADCRUMB -->


				<input type="hidden" name="borower_contact_type_id" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_contact_id : ''; ?>">

				<input type="hidden" name="lender_contact_type_id" value="<?php echo isset($fetch_lender_contact_type) ? $fetch_lender_contact_type[0]->lender_contact_id : ''; ?>">

				<input type="hidden" name="contact_id" value="<?php echo isset($contact_id) ? $contact_id : ''; ?>" >

				<!------------------  MAIN  LEFT DIV ENDS ------------------------>
				<div class="main-left-div">
				   <div class="main-left-div-res">
					<div class="main-title row borrower_data_row" >

						<div class="col-md-3">

  <div class="<?php echo $class; ?>"><?php echo $msg; ?> </div>
							<h3>Contact Information</h3>
						</div>
					</div>
					<div class="row borrower_data_row inline-label" id="phone1_field">

						<div class="col-md-3">
							<label >Name:</label>
						</div>
						<div class="col-md-4">
                            <?php
                            $contactName="";
                            if(!empty($fetch_contact_data[0]->prefix)){
                                $contactName.=$fetch_contact_data[0]->prefix." ";
                            }
                            if(!empty($fetch_contact_data[0]->contact_firstname)){
                                $contactName.=$fetch_contact_data[0]->contact_firstname." ";
                            }
                            if(!empty($fetch_contact_data[0]->contact_middlename)){
                                $contactName.=$fetch_contact_data[0]->contact_middlename." ";
                            }
                            if(!empty($fetch_contact_data[0]->contact_lastname)){
                                $contactName.=$fetch_contact_data[0]->contact_lastname." ";
                            }
                            if(!empty($fetch_contact_data[0]->suffix)){
                                $contactName.=$fetch_contact_data[0]->suffix;
                            }
                            ?>
							<label style="font-weight:normal !important;" ><?php echo $contactName; ?></label>
						</div>

                        <div class="col-md-3">
                            <label>Legal First Name:</label>
                        </div>
						<div class="col-md-4">
							
							<label style="font-weight:normal !important;"  class=" phone-format"><?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_suffix : ''; ?></label>
						</div>
					</div>
					<div class="row borrower_data_row inline-label" id="phone1_field">
						<div class="col-md-3">
							<label >Company:</label>
						</div>
						<div class="col-md-4">
							<label style="font-weight:normal !important;"  class=" phone-format"><?php echo isset($contact_id) ? $fetch_contact_data[0]->company_name_1 : ''; ?></label>
						</div>
						<div class="col-md-3">
							<label >Website:</label>
						</div>
						<div class="col-md-4">
							<label style="font-weight:normal !important;"  class=""><a target="_blank" href="<?php echo $fetch_contact_data[0]->contact_website; ?>"><?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_website : ''; ?></a></label>

						</div>
					</div>

					<div class="row borrower_data_row inline-label" id="">
						<div class="col-md-3">
							<label >Primary Phone:</label>
						</div>
						<div class="col-md-4">
							<label style="font-weight:normal !important;"  class="phone-format"><?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_phone : ''; ?></label>

						</div>
						<div class="col-md-3">
							<label >Phone Type:</label>
						</div>
						<div class="col-md-4">
							<label style="font-weight:normal !important;"  class="phone-format"><?php echo isset($contact_id) ? $contact_number_option[$fetch_contact_data[0]->primary_option] : ''; ?></label>

						</div>
						<div class="col-md-3">
							<label >Phone Extension:</label>
						</div>
						<div class="col-md-4">
							<label style="font-weight:normal !important;"  class="phone-format"><?php echo isset($contact_id) ? $fetch_contact_data[0]->phone_extension : ''; ?></label>

						</div>
					</div>

                    <?php if($fetch_contact_data[0]->alter_phone != ''){ ?>

    					<div class="row borrower_data_row inline-label">
    						<div class="col-md-3">
    							<label >Secondary Phone:</label>
    						</div>
    						<div class="col-md-4">
    							<label style="font-weight:normal !important;"  class="phone-format"><?php echo isset($contact_id) ? $fetch_contact_data[0]->alter_phone : ''; ?></label>

    						</div>

    						<div class="col-md-3">
    							<label>Phone Type:</label>
    						</div>
    						<div class="col-md-4">
    							<label style="font-weight:normal !important;"  class="phone-format"><?php echo isset($contact_id) ? $contact_number_option[$fetch_contact_data[0]->alter_option] : ''; ?></label>

    						</div>
							<div class="col-md-3">
    							<label>Phone Extension:</label>
    						</div>
    						<div class="col-md-4">
    							<label style="font-weight:normal !important;"  class="phone-format"><?php echo isset($contact_id) ? $fetch_contact_data[0]->alter_phone_extension : ''; ?></label>

    						</div>
    					</div>
                    <?php } ?>

					<div class="row borrower_data_row inline-label" id="">
						<div class="col-md-3">
							<label>Primary E-Mail:</label>
						</div>
						<div class="col-md-4">
							<label style="font-weight:normal !important;"  class=" "><a href="mailto:<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_email : ''; ?>"><?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_email : ''; ?></a> </label>
						</div>


						<div class="col-md-3">
							<label>E-Mail Type:</label>
						</div>
						<div class="col-md-4">
							<label style="font-weight:normal !important;"  class=" "><?php echo isset($contact_id) ? $email_types_option[$fetch_contact_data[0]->pemail_type] : ''; ?></label>
						</div>

					</div>

                    <?php if($fetch_contact_data[0]->contact_email2 != ''){ ?>

    					<div class="row borrower_data_row inline-label" id="">
    						<div class="col-md-3">
    							<label>Secondary E-Mail:</label>
    						</div>
    						<div class="col-md-4">
    							<label style="font-weight:normal !important;"  class=""><a href="mailto:<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_email2 : ''; ?>"><?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_email2 : ''; ?></a></label>
    						</div>

    						<div class="col-md-3">
    							<label>E-Mail Type:</label>
    						</div>
    						<div class="col-md-4">
    							<label style="font-weight:normal !important;"  class=" "><?php echo isset($contact_id) ? $email_types_option[$fetch_contact_data[0]->aemail_type] : ''; ?></label>
    						</div>
    					</div>

                    <?php } ?>


					<div class=" borrower_data_row inline-label" id="">
						<div class="col-md-3">
							<label >Address:</label>
						</div>
						<div class="col-md-9">
							<label style="font-weight:normal !important;"  class=" "><?php echo ((isset($contact_id)) && ($fetch_contact_data[0]->contact_address)) ? $fetch_contact_data[0]->contact_address : ''; ?></label>


							<label style="font-weight:normal !important;"  class=" "><?php echo ((isset($contact_id)) && ($fetch_contact_data[0]->contact_unit)) ? $fetch_contact_data[0]->contact_unit . '; ' : ';'; ?></label>


							<label style="font-weight:normal !important;"  class=""><?php echo ((isset($contact_id)) && ($fetch_contact_data[0]->contact_city)) ? $fetch_contact_data[0]->contact_city . ', ' : ''; ?></label>



							<label style="font-weight:normal !important;"  class=""><?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_state . ' ' : ''; ?></label>

							<label style="font-weight:normal !important;"  class=""><?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_zip : ''; ?></label>

						</div>
					</div>

					<!--<div class=" borrower_data_row " style="float: left;width: 100%;margin-left:100px !important;" id="">


							<div class="col-md-2">

								<label style="font-weight:normal !important;"  class="form-control "><?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_state : ''; ?></label>

							</div>

							<div class="col-md-2">

								<label style="font-weight:normal !important;"  class="form-control "><?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_zip : ''; ?></label>

							</div>


						</div>-->
						<div class=" borrower_data_row inline-label"  style ="float: left;width: 100%;" id="">
						<?php

                        if (in_array('16', $fetch_contact_specialty)) {

                        	$code = 'Yes';
                        } else {
                        	$code = 'No';
                        }

                        ?>
						<div class="col-md-5" style="width:15%;">
							<strong>Trust Deed Investor: </strong>
						</div>
						<div class="col-md-5">
							<label><?php echo $code; ?></label>
						</div>



					</div>
						<div class=" borrower_data_row inline-label"  style ="float: left;width: 100%;" id="">
							<div class="col-md-3" >
								<label style="font-size: 14px !important;">Tags:</label>
							</div>
							<div class="tags_display">
							<?php
if ($fetch_sql_tags) {

	// foreach($fetch_sql_tags as $key => $tags){

	/* 	$fetch_contact_type 		= unserialize($fetch_sql_tags[0]->contact_type);
									$fetch_contact_specialty 	= unserialize($fetch_sql_tags[0]->contact_specialty);
									$fetch_contact_email_blast 	= unserialize($fetch_sql_tags[0]->contact_email_blast);
									$fetch_contact_permission 	= unserialize($fetch_sql_assigned[0]->contact_permission);
									 */
	/* foreach($fetch_contact_type as $con_type){
										$aa[$con_type] = $con_type;
									} */

	foreach (array_filter($fetch_contact_specialty) as $con_specialty) {
		$bb[$con_specialty] = $con_specialty;
	}

	foreach (array_filter($fetch_contact_email_blast) as $con_email_blast) {
		$dd[$con_email_blast] = $con_email_blast;
	}

	/* foreach($fetch_contact_permission as $con_permission){
										$ee[$con_permission] = $con_permission;
									} */

	// foreach($contact_type as $key => $type){

	// if($key == $aa[$key]){

	// echo '<div class="tag_labels">
	// <label>'.$type.'</label>
	// </div>	';
	// }
	// }

	foreach ($contact_specialty as $key => $specialty) {

		if ($key == $bb[$key]) {
			echo '<div  class="col-md-4">
											<label >' . $specialty . '</label>

											</div>	';
		}
	}
	/*foreach ($contact_specialty_trust as $key => $specialtyyy) {

		if ($key == $bb[$key]) {
			echo '<div  class="col-md-4">
											<label >' . $specialtyyy . '</label>

											</div>	';
		}
	}*/
	foreach ($contact_email_blast as $key => $email_blast) {

		if ($key == $dd[$key]) {
			echo '<div  class="col-md-4">
											<label>' . $email_blast . '</label>

											</div>	';
		}
	}
	// }
}
?>
							</div>

								<!--<div class="col-md-4">
									<input type="text" readonly class="form-control" name="email" id="tags" value="<?php echo $contact_specialty_val; ?>" placeholder="Tags" />
								</div>	-->


						</div>

						<div class="row borrower_data_row" id="">
							<div class="col-md-2">
								<label style="padding-left: 0px !important;" ><strong>Internal Contact:</strong></label>
							</div>
							<div class="col-md-8">
								<label style="margin-top:0px;margin-left:-15px;"><?php echo isset($contact_id) ? $fetch_user_name : ''; ?></label>
							</div>
						</div>
						<div class="row borrower_data_row inline-label" id="">
							<?php
if ($user_role == '2') {
	?>
							<div class="col-md-3">
								<label >Assigned:</label>
							</div>
								 <?php }?>

								<div class="tags_display" style="font-weight:normal !important">
									<?php
if ($fetch_sql_assigned) {
	$fetch_contact_permission = unserialize($fetch_sql_assigned[0]->contact_permission);

	foreach ($fetch_contact_permission as $con_permission) {
		$ee[$con_permission] = $con_permission;
	}

	foreach ($contact_permission as $key => $permission) {

		if ($ee[$key] == 3) {
			echo '<div  class="col-md-4">

														<label >Selected User: ' . $fetch_sql_user_name . '</label>

														</div>	';
		} else if ($key == $ee[$key]) {
			echo '<div  class="col-md-4">
														<label  >' . $permission . '</label>

														</div>	';
		}
	}
}

?>
								</div>
						</div>

						<div class="row borrower_data_row  inline-label">
							<div class="col-md-3">
								<label style="padding-left: 0px !important;" >Marketing Data:</label>
							</div>
							<div class="col-md-8">
								<label style="margin-top: 6px;"><?php echo isset($contact_id) ? $contact_marketing_data[$fetch_contact_data[0]->contact_marketing_data] : ''; ?></label>
							</div>


						</div>


						<div class="row borrower_data_row  inline-label" >
							<div class="col-md-3">
								<label style="padding-left: 0px !important;" >Referral Source:</label>
							</div>
							<div class="col-md-8">
								<label style="margin-top: 6px;"><?php echo isset($contact_id) ? $referral_source[$fetch_contact_data[0]->referral_source] : ''; ?></label>
							</div>


						</div>
							<!--<div class="row borrower_data_row  inline-label" >
							<div class="col-md-3">
								<label style="padding-left: 0px !important;" >Search:</label>
							</div>
							<div class="col-md-8">
								<label style="margin-top: 6px;"><?php echo isset($contact_id) ? $referral_source[$fetch_contact_data[0]->referral_source] : ''; ?></label>
							</div>


						</div>-->
					<div class=" borrower_data_row inline-label">
						<div class="col-md-3">
							<label><strong>Facebook:</strong></label>
						</div>
						<div class="col-md-4">
							<label style="font-weight:normal !important;"  class=" "><a target="_blank" href ="<?php echo $fetch_contact_data[0]->fb_profile; ?>"><?php echo ((isset($contact_id)) && ($fetch_contact_data[0]->fb_profile)) ? $fetch_contact_data[0]->fb_profile : ''; ?></a></label>

						</div>

						<!--<div class="col-md-3">
							<label><strong>Linkedin:</strong></label>
						</div>
						<div class="col-md-4">
							<label style="font-weight:normal !important;"  class=" "><a target="_blank" href ="<?php echo $fetch_contact_data[0]->lkdin_profile; ?>"><?php echo ((isset($contact_id)) && ($fetch_contact_data[0]->lkdin_profile)) ? $fetch_contact_data[0]->lkdin_profile : ''; ?></a></label>

						</div>-->

					</div>
								<div class="row"></div>

					<div class=" borrower_data_row inline-label">

						<div class="col-md-3">
							<label><strong>Linkedin:</strong></label>
						</div>
						<div class="col-md-9">
							<label style="font-weight:normal !important;"  class=" "><a target="_blank" href ="<?php echo $fetch_contact_data[0]->lkdin_profile; ?>"><?php echo ((isset($contact_id)) && ($fetch_contact_data[0]->lkdin_profile)) ? $fetch_contact_data[0]->lkdin_profile : ''; ?></a></label>

						</div>

				    </div>


					<div class="row"></div>
					<div class=" borrower_data_row inline-label">
						<div class="col-md-3">
							<label><strong>Bigger Pockets:</strong></label>
						</div>
						<div class="col-md-9">
							<label style="font-weight:normal !important;"  class=" "><a target="_blank" href ="<?php echo $fetch_contact_data[0]->bigger_pocket_profile; ?>"><?php echo ((isset($contact_id)) && ($fetch_contact_data[0]->bigger_pocket_profile)) ? $fetch_contact_data[0]->bigger_pocket_profile : ''; ?></a></label>

						</div>
					</div>
						<div class="row"></div>
						<div class="row borrower_data_row  inline-label" >
							<div class="col-md-3">
								<label style="padding-left: 0px !important;" >About:</label>
							</div>
							<div class="col-md-8">
								<label><?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_about : ''; ?></label>
							</div>


						</div>

						<div class="row"></div>
						<div class="borrower_data_row  inline-label" >
						<form method="post" action="<?php echo base_url() ?>contact_p_notes">
							<input type="hidden" name="contact_id" value="<?php echo isset($contact_id) ? $contact_id : ''; ?>" >

							<div class="col-md-12">
								<div class="col-md-3" id="p_loan">
									<label>Personal Notes:</label>
								</div>
								<div class="col-md-8">
								<?php $a = $fetch_p_notes->result_object;?>
									<!--<input type="text" name="p_notes" class="form-control" value="<?php echo $a[0]->personal_note; ?>">-->

									<textarea name="p_notes" type="text" class="form-control" rows="3" placeholder="Add Personal Notes Here..."><?php echo $a[0]->personal_note; ?></textarea>
								</div>
								<div class="col-md-1">
									<button style="vertical-align:top;margin-left:-25px;" class="btn blue" type="submit">Save</button>
								</div>
							</div>
						</form>
					</div>

					<?php
					
					$tCountpaidoff = 0;
					foreach ($fetch_paidoff_loan_latest as $row) {
						$tCountpaidoff++;
					}
					
					$tActiveCount = 0;
					foreach ($fetch_active_loan_latest as $r) {
						$tActiveCount++;
					}
					
					$tpipeCount = 0;
					foreach ($fetch_pipe_loan_latest as $r) {
						$tpipeCount++;
					}
					
					$BorrowerTransaction = $tCountpaidoff + $tActiveCount + $tpipeCount;
					
					
					foreach ($active_loan as $row) {
						$lenderActivecount = $row['count_loan'];
					}
					
					foreach ($paidoff_loan as $row) {
						$lenderpaidoffcount = $row['count_loan'];
					}
					
					$LEndertransaction = $lenderActivecount + $lenderpaidoffcount;
					
					
					?>

                   <div class="new-notes">

						<div class="title row borrower_data_row ">
							<div class="col-md-3">
								<h3>Transaction Count</h3>
							</div>
						</div>
						<div class="col-md-4">
							<label>&nbsp;&nbsp;Borrower: <a onclick="borrowerTransectionform(this);"><?php echo $BorrowerTransaction; ?></a></label>
						</div>
						<div class="col-md-4">
							<label>&nbsp;&nbsp;Lender: <a onclick="lenderTransectionform(this);"><?php echo $LEndertransaction; ?></a></label>
						</div>
						<div class="col-md-4">
							<label>&nbsp;&nbsp;Affiliated: <a onclick="AffiliatedTransectionform(this);"><?php echo $affi_total_count; ?></a></label>
						</div>
						
						
					</div>


					<!------------------ NEW NOTES DIV STARTS ---------------------------->

					<div class="new-notes">

					<?php if (is_numeric($this->uri->segment(2))) { ?>

						<form method="post" name="" action="<?php echo base_url() . 'contact_notes/' . $contact_id; ?>">

						<div class="title row borrower_data_row ">
							<div class="col-md-3">
								<h3>Contact Notes</h3>
							</div>

							<?php foreach ($fetch_check_user as $key => $row) {

                                if($row->msg != ''){ ?>

                                <div class="col-md-3">
                                    <h5><?php echo $row->msg.' <strong>'.date('m-d-Y', strtotime($row->date)); ?></strong>.</h5>
                                </div>

                                <?php }else{ ?>

    							<div class="col-md-3">
    								<h5><strong><?php echo $row->fname . ' ' . $row->middle_name . ' ' . $row->lname; ?></strong> added <strong><?php echo isset($row->contact_id) ? $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname : ''; ?></strong> on <strong><?php echo date('m-d-Y', strtotime($row->date)); ?></strong>.</h5>
    							</div>

							<?php } } ?>

						</div>
                        <!-- 20-07-2021 -->
                        <div class="row">
                            <div class="col-md-6">
                                <a class="btn blue" data-toggle="modal" href="<?php echo base_url().'add_contact_note/'.$contact_id.'/-1';?>"><i class="fa fa-plus" aria-hidden="true"></i> Add Notes</a>
                            </div>
                            
                        </div>
                        <div class="row">&nbsp;</div>
						<div class="row11">

							<div class="row  borrower_data_row inline-label2" id="">
								<div class="notes_class">
									<label class="title_history" >Date:</label>

									<input type="hidden" value ="1" name="notes[]"/>
									<input type="text" class="dates_class form-control datepicker" name="notes_date[]" id="notes_date_1" value="<?php echo date('m-d-Y'); ?>" placeholder="MM-DD-YYYY" />

								</div>

								<div class="notes_class">
									<label class="title_history" >Type:</label>

									<select id="notes_type_1" name="notes_type[]" class="form-control" data-live-search="true" style="width:150px !important;">
											<?php foreach ($notes_type as $key => $type) {

		?>
												<option value="<?php echo $key; ?>" <?php echo $selected; ?> ><?php echo $type; ?></option>

											<?php }?>
									</select>

								</div>
								
								<div class="spc-loan-div notes_class ">
									<label class="title_history">Purpose :</label>
									<select id="purpose_type" name ="purpose_type[]" class="form-control"  style="width:130px !important;">
											<?php foreach ($contact_purpose_menu as $key => $types) { ?>
												<option value="<?php echo $key; ?>" ><?php echo $types; ?></option>

											<?php }?>
									</select>
								</div>

								<input style="width:100px;     float: right;" id ="formsubmit" value="Save" type="submit" name="submit" class="btn btn-primary b_right"/>

							</div>

							<div class="row borrower_data_row borrower_textarea_div" >
									<div class="col-md-9">
										<textarea class="form-control" name="personal_notes[]" placeholder="Text:" id="personal_notes_1" ></textarea>
									</div>
							</div>

						</form>
						</div>
						<?php }?>

					</div>


					<!------------------ SAVED NOTES DIV STARTS ------------------------>
					<div class="saved-notes <?php echo $permission_class; ?>">
						<!--<div class="row borrower_data_row">
								<div class="col-md-3">
								<h3>History</h3>

								</div>
						</div>-->


						<?php 
                         function validateDate($_date, $_format = 'Y-m-d'){
                            $d = DateTime::createFromFormat($_format, $_date);      
                            return $d && $d->format($_format) === $_date;
                        }


                        if ($fetch_contact_notes) {
                            // echo '<pre>'; print_r($fetch_contact_notes); echo '</pre>';

                	$sequence = 0;
                	foreach ($fetch_contact_notes as $key => $notes) {
                		$sequence++;
                		// echo '<pre>';
                		// print_r($listAllProperty);
                		// echo '</pre>';
                		switch ($notes->notes_type) {
                		case 1:$notes_type_val = 'Email';
                			break;
                		case 2:$notes_type_val = 'Phone';
                			break;
                		case 3:$notes_type_val = 'Letter';
                			break;
                		case 4:$notes_type_val = 'Meeting';
                			break;
                		case 5:$notes_type_val = 'Other';
                			break;
                		//case 6:$notes_type_val = 'POF Letter';
                			//break;
                		//case 7:$notes_type_val = 'Executive Summary';
                			//break;
                		//case 8:$notes_type_val = 'Loan Request';
                		//	break;
                		//case 9:$notes_type_val = 'Networking Event';
                		//	break;
                		//case 10:$notes_type_val = 'Loan Input Form';
                			//break;
                		default:
                			$notes_type_val = 'None';
                		}
                        /*20-07-2021 by@Aj*/
                       

                        $ab_follow_up_date = $notes->ab_follow_up_date;
                         // $_follow_up_date = date("m-d-Y", strtotime($ab_follow_up_date));
                            if(validateDate($ab_follow_up_date) == true){
                                $_follow_up_date = date("m-d-Y", strtotime($ab_follow_up_date));
                            }else{

                                $_follow_up_date = '';
                            } 
                        /*End*/    

                		?>


						<form  class ="history-filter" id="history-filter_<?php echo $notes->id; ?>" method="post" action="<?php echo base_url() . 'Contact/history_filter'; ?>" >
								<input type="hidden" value='' id='hidden-type' class='hidden-type' name='action'/>
								<input type="hidden"  id='fetch_contact_id' value="<?php echo $this->uri->segment(2); ?>"  name='fetch_contact_id'/>
								<input type="hidden" id="fetch_notes_id" name="fetch_notes_id" value="<?php echo $notes->id; ?>" />

							<div class="row  borrower_data_row inline-label2 min_row" >
								<div class="notes_class label_notes_date<?php echo $notes->id; ?>" >
									<label class="title_history">Date:</label>
									<label class="history_label form-control" id="label_notes_date" name="fetch_notes_date" ><a href="<?php echo base_url().'add_contact_note/'.$notes->contact_id.'/'.$notes->id;?>"><?php echo date('m-d-Y', strtotime($notes->notes_date)); ?></a></label>

								</div>

								<div class="notes_class input_notes_date<?php echo $notes->id; ?>" style="display:none;">
									<label class="title_history">Date:</label>
									<input type="text" class="form-control datepicker"  name="fetch_notes_date" id="fetch_notes_date" name="fetch_notes_date" value="<?php echo date('m-d-Y', strtotime($notes->notes_date)); ?>" placeholder="MM-DD-YYYY" />
								</div>


								<div class="notes_class label_notes_type<?php echo $notes->id; ?> row_notes_type">
									<label class="title_history">Type:</label>
									<label  class="history_label form-control" id="label_notes_type"><a href="<?php echo base_url().'add_contact_note/'.$notes->contact_id.'/'.$notes->id;?>"><?php echo $notes_type_val; ?></a></label>

								</div>

								<div class="notes_class select_notes_type select_notes_type<?php echo $notes->id; ?> " style="display:none;">
									<label class="title_history">Type:</label>

									<input type="hidden" class="form-control "  id="fetch_notes_type_id" name="fetch_notes_type_id" value="<?php echo $notes_type_val; ?>" placeholder="Notes type" />

									<select id="fetch_select_notes_type" name="fetch_notes_type" class=" form-control" data-live-search="true" style="width:150px !important;" >
										<?php
										foreach ($notes_type as $key => $type) {
													if ($key == $notes->notes_type) {
														$selected = 'selected';
													} else {
														$selected = '';
													}
													?>
										<option <?php echo $selected; ?> value="<?php echo $key; ?>"  ><?php echo $type; ?></option>

										<?php }?>
									</select>
								</div>

								<div class="spc-loan-div  label_loan_id<?php echo $notes->id; ?>">
									<label class="title_history">Purpose:</label>

									<!--<label class="history_label form-control" id="label_loan_id" ><a href="<?php echo base_url(); ?>load_data/<?php echo $notes->loan_id; ?>"><?php echo $property_address[$notes->loan_id]; ?></a></label>-->

									<?php if($notes->purpose_type != ''){?>
										<label class="history_label form-control" id="label_loan_id" ><a href="<?php echo base_url().'add_contact_note/'.$notes->contact_id.'/'.$notes->id;?>"><?php echo $contact_purpose_menu[$notes->purpose_type]; ?></a></label>
									<?php } ?>
								</div>
								
								<?php
								
								//echo '<pre>';
								//print_r($this->session->all_userdata());
								?>
								<?php if($this->session->userdata('user_role') == '2'){ ?>
									<!-- <a onclick="editContactNotes('<?php //echo $notes->id; ?>');" class="btn btn-xs btn-info" style="float: right;margin-top: 10px;">Edit</a> -->
								<?php } ?>

								<div class="spc-loan-div selected_loans select_loan_id<?php echo $notes->id; ?>" style="display:none;">
									<input type="hidden"  class="form-control "  id="fetch_loan_id" name="fetch_loan_id" value="<?php echo $notes->loan_id; ?>" placeholder="loan" />
									<label class="title_history">Property:</label>

									<select id ="fetch_select_talimar_loan" name="fetch_talimar_loan" class="chosen" style="width: 181px !important;" disabled>
										<option value="">Select Property</option>
										<?php
									foreach ($property as $key => $row) {
										if ($property[$key]['id'] == $notes->loan_id) {
											$selected = 'selected';

										} else {

											$selected = '';
										}
										?>
										<option <?php echo $selected; ?> value="<?php echo $property[$key]['id']; ?>"  ><?php echo $property[$key]['text']; ?></option>
										<?php }?>
									</select>



									
								</div>

							</div>

							<div class="row borrower_data_row borrower_textarea_div " >
								<div class="col-md-9 note_desc label_notes_text<?php echo $notes->id; ?>" >
									
									<a href="<?php echo base_url().'add_contact_note/'.$notes->contact_id.'/'.$notes->id;?>"><?php echo $notes->notes_text; ?></a>
								</div>
								<div class="col-md-9 note_enter_by label_notes_text<?php echo $notes->id; ?>" >
								
									<i>Entered By : <a href="<?php echo base_url().'add_contact_note/'.$notes->contact_id.'/'.$notes->id;?>"><?php //echo $fetch_userss_data[$notes->user_id];
                                    if(isset($notes->entry_by)){
                                        echo $notes->entry_by;
                                    }else{
                                        echo $fetch_userss_data[$notes->user_id];
                                    } 
                                    // if(empty($fetch_userss_data[$notes->user_id])){
                                    //     echo $notes->entry_by;
                                    // }else{
                                    //     echo $fetch_userss_data[$notes->user_id];
                                    // }
                                    

                                    ?></a></i>
								</div>
                                <?php //print_R($notes);?>
                                <div class="col-md-9">

                                    <?php if($notes->loan_id != ''){
                                        $propertLINK = '<a href="'.base_url().'load_data/'.$notes->loan_id.'">'.$listAllProperty[$notes->loan_id].'</a>';
                                    }else{
                                        $propertLINK = '';
                                    } ?>
                                
                                    <i>Property: <?php echo $propertLINK; ?></i>
                                </div>

								<div class="col-md-9 input_notes_text<?php echo $notes->id; ?>" style="display:none;" >
									<textarea class="form-control "  id ="fetch_personal_notes" name="fetch_personal_notes" placeholder="Text:" id="text" ><?php echo $notes->notes_text; ?></textarea>
								</div>

								<input style="display:none;6px 14px;" id ="history_formsubmit_<?php echo $notes->id; ?>" value="Update" name="submit" type="submit" class ="btn btn-primary pull-right"/>
								
								

							</div>
                            <!-- 20-07-2021 by: @Aj -->
                            <div class="row  borrower_data_row inline-label2 " >
                                <div class="notes_class label_notes_followup<?php echo $notes->id; ?> row_follow_up_task" >
                                    <label class="title_history">Follow Up:</label>
                                    <label class="history_label form-control" id="ab_follow_up_task" name="ab_follow_up_task" ><a href="<?php echo base_url().'add_contact_note/'.$notes->contact_id.'/'.$notes->id;?>"><?php echo $notes->ab_follow_up_task; ?></a></label>

                                </div>

                                <div class="notes_class label_notes_followupDate<?php echo $notes->id; ?> row_follow_up_date" >
                                    <label class="title_history">Follow Up Date:</label>
                                    <label class="history_label form-control" id="ab_follow_up_date" name="ab_follow_up_date" ><a href="<?php echo base_url().'add_contact_note/'.$notes->contact_id.'/'.$notes->id;?>"><?php echo $_follow_up_date; ?></a></label>
                                  
                                    <!-- <input type="text" class="form-control"  name="fetch_notes_date" id="fetch_notes_date" name="fetch_notes_date" value="<?php //echo date('m-d-Y', strtotime($notes->notes_date)); ?>" placeholder="MM-DD-YYYY" /> -->
                                </div>

                                <div class="notes_class label_notes_followupstatus<?php echo $notes->id; ?> row_follow_up_status" >
                                    <label class="title_history">Follow Up Status:</label>
                                    <label class="history_label form-control" id="ab_follow_up_status" name="ab_follow_up_status" ><a href="<?php echo base_url().'add_contact_note/'.$notes->contact_id.'/'.$notes->id;?>"><?php  
                                    if($notes->ab_follow_up_status == 'outstanding'){
                                        echo 'Outstanding';
                                    }else{
                                       echo $notes->ab_follow_up_status; 
                                    }
                                     ?></a></label>
                                  
                                    <!-- <input type="text" class="form-control"  name="fetch_notes_date" id="fetch_notes_date" name="fetch_notes_date" value="<?php //echo date('m-d-Y', strtotime($notes->notes_date)); ?>" placeholder="MM-DD-YYYY" /> -->
                                </div>

                            </div>   



						</form>
							<!------------------End Buttons Section--------------------------->

			<?php	}
                } else {
                    ?>
								<div class="row borrower_data_row" >
									<h4>No Saved Notes!</h4>
								</div>
						<?php }?>


					</div>



					<!------------------ SAVED NOTES DIV ENDS -------------------->

						</div>  <!--res-->



				</div>
			
<form id="lendertra_form" action="<?php echo base_url('loan_schedule_investor'); ?>" method="post">
	<input type="hidden" name="contact_id" value="<?php echo $this->uri->segment(2);?>">
</form>
<form id="affiliated_form" action="<?php echo base_url('affiliated_trancation'); ?>" method="post">
	<input type="hidden" name="contact_id" value="<?php echo $this->uri->segment(2);?>">
</form>
<form id="borrowertra_form" action="<?php echo base_url('loan_schedule_borrower'); ?>" method="post">
	<input type="hidden" name="contact_id" value="<?php echo $this->uri->segment(2);?>">
</form>	
		
<script>

function lenderTransectionform(that){
	
	$('form#lendertra_form').submit();
}
function AffiliatedTransectionform(that){
	
	$('form#affiliated_form').submit();
}
function borrowerTransectionform(that){
	
	$('form#borrowertra_form').submit();
}

function editContactNotes(noteId){
	
	//alert(noteId);
	
	$.ajax({
			type : 'POST',
			url  : '<?php echo base_url()."Contact/GetcontactNotes";?>',
			data : {'noteId':noteId},
			success : function(response){
				
				var data = JSON.parse(response);
				var notes_date = formatDate(data.notes_date);
				$('#updateNotesdata input[name="updateid"]').val(data.id);
				$('#updateNotesdata input[name="contact_id"]').val(data.contact_id);
				$('#updateNotesdata input[name="notedate"]').val(notes_date);
				$('#updateNotesdata select[name="notetype"] option[value="'+data.notes_type+'"]').attr('selected','selected');
				$('#updateNotesdata select[name="purpose_type"] option[value="'+data.purpose_type+'"]').attr('selected','selected');
				$('#updateNotesdata textarea[name="notetext"]').val(data.notes_text);
				
				$('#updateNotesdata').modal('show');
			}
		
	});
	
}

function formatDate(date) {
 var d = new Date(date),
	 month = '' + (d.getMonth() + 1),
	 day = '' + d.getDate(),
	 year = d.getFullYear();

 if (month.length < 2) month = '0' + month;
 if (day.length < 2) day = '0' + day;

 return [month, day, year].join('-');
}

</script>

<!-- Modal -->
<div class="modal fade" id="updateNotesdata" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
	<form method="post" action="<?php echo base_url();?>Contact/UPdateNotes" name="form_updateNotesdata" id="form_updateNotesdata">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>Update Contact Note</b></h5>
      </div>
      <div class="modal-body">
	  
		<input type="hidden" name="updateid" value="">
		<input type="hidden" name="contact_id" value="">
	  
			<div class="row">
				<div class="col-md-6">
					<label><b>Date:</b></label>
					<input type="text" name="notedate" class="form-control datepicker">
				</div>
				
				<div class="col-md-6">
					<label><b>Type:</b></label>
					<select type="text" name="notetype" class="form-control">
						<?php foreach ($notes_type as $key => $type) { ?>
							<option value="<?php echo $key;?>"><?php echo $type;?></option>
						<?php }?>
					</select>
				</div>
			</div>
			
			<div class="row"><br></div>
			<div class="row">
				<div class="col-md-6">
					<label><b>Purpose:</b></label>
					<select type="text" name="purpose_type" class="form-control">
						<?php foreach ($contact_purpose_menu as $key => $type) { ?>
							<option value="<?php echo $key;?>"><?php echo $type;?></option>
						<?php }?>
					</select>
				</div>
			</div>
			<div class="row"><br></div>
			
			<div class="row">
				<div class="col-md-12">
					<label><b>Follow up:</b></label>
					<textarea type="text" name="notetext" class="form-control" rows="5"></textarea>
				</div>
			</div>

            <div class="row">
                <div class="col-md-12">
                    <label><b>Follow up Task:</b></label>                    
                    <select type="text" name="ab_follow_up" class="form-control">
                        <option value="No">No</option>
                        <option value="Yes">Yes</option>
                        
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label><b>Follow Up Date: </b></label>
                    <input type="text" name="ab_follow_up_date" class="form-control datepicker">
                </div>
                
                <div class="col-md-4">
                    <label><b>Follow Up Status:</b></label>
                    <select type="text" name="ab_follow_up_status" class="form-control">
                        <option value="No">No</option>
                        <option value="Yes">Yes</option>
                        
                    </select>
                </div>
                <div class="col-md-4">
                    <label><b>Follow Up Type:</b></label>
                    <select type="text" name="ab_follow_up_type" class="form-control">
                        <option value="call">Call</option>
                        <option value="e_Mail">E-Mail</option>
                        <option value="meeting" selected="">Meeting</option>
                        <option value="loan_request">Loan Request</option>
                        <option value="Other">Other</option>
                        
                    </select>
                </div>
            </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="upnote" class="btn btn-primary">Save</button>
      </div>
	  </form>
    </div>
  </div>
</div>


					<!------------------  MAIN  LEFT DIV STARTS ------------------------>

					<!------------------  MAIN  RiGHT DIV STARTS ------------------------>
					<div class="main-right-div">
						<?php

$fetch_contact_permission = unserialize($fetch_sql_assigned[0]->contact_permission);
$fetch_particular_user_id = $fetch_sql_assigned[0]->particular_user_id;

foreach ($fetch_contact_permission as $con_permission) {
	if ($con_permission == '3') {

		$user_idd[] = $fetch_particular_user_id;
		$user_rolee[] = '1';

	} else if ($con_permission == '2') {

		$user_rolee[] = '1';
	}

	if (($con_permission == '1') || ($con_permission == '2')) {

		// $user_idd[] = $con_permission ;
		$user_rolee[] = '2';

	}

}

if (($user_idd) && ($user_role == '1')) {

	if (in_array($user_id, $user_idd)) {

		$permission_class = 'show_acc_to_permission';

	} else {
		$permission_class = 'hide_acc_to_permission';

	}
}
if (($user_rolee) && ($user_role == '2')) {

	if ((in_array('2', $user_rolee)) && ($user_role == '2')) {

		$permission_class = 'show_acc_to_permission';
	} else {
		$permission_class = 'hide_acc_to_permission';

	}
} else {
	$permission_class = 'show_acc_to_permission';

}
if ($user_role == '1') {

	if ((in_array('1', $user_rolee)) && ($user_role == '1')) {
		$permission_class = 'show_acc_to_permission';
	}

}

?>

						<!---<div class="col-md-8 <?php echo $permission_class; ?>">
							<label>Statistics:</label>
								<a style="width:100%;" class="btn blue" data-toggle="modal" href="#statistics">Statistics </a>
						</div>	-->


						<!---  Statistics modal  Starts---->

						<div class="modal fade bs-modal-lg" id="statistics" tabindex="-1" role="large" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h4 class="modal-title">Statistics <?php echo isset($contact_id) ? ': ' . $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname : ''; ?></h4>
									</div>
										<!-----------Hidden fields----------->
										<!-----------End of hidden Fields---->
									<div class="modal-body">
										<hr style="border-color:black !important;">

										<div class="row ">
											<div class="col-md-3">
												<label style=""><b>Trust Deed Schedule</b></label>
											</div>
										</div>
										<div class="row ">
											<table style="width:50% !important;float:left;" id="table_trust_deed" class="table table-bordered table-striped table-condensed flip-content">
												<tbody>
													<tr>
														<td>Property Address</td>
														<td>Loan Status</td>
														<td>Interest</td>
														<td>Loan Amount</td>
													</tr>
												</tbody>
											</table>

											<table style="width:30% !important;float:left;margin-left: 135px;" id="table_stats1" class="table table-bordered table-striped table-condensed flip-content">
												<tbody>

													<tr>
														<td>Active Trust Deeds:</td>
														<td>##</td>
													</tr>
													<tr>
														<td>Active Trust Deeds:</td>
														<td>$#,###,####</td>
													</tr>
													<tr>
														<td>Paid Off Trust Deeds:</td>
														<td>##</td>
													</tr>
													<tr>
														<td>Paid Off Trust Deeds:</td>
														<td>$#,###,####</td>
													</tr>
													<tr>
														<td>Total Trust Deeds:</td>
														<td>##</td>
													</tr>
													<tr>
														<td>Total Trust Deeds:</td>
														<td>$#,###,####</td>
													</tr>
												</tbody>
											</table>
										</div>

										<hr style="border-color:black !important;">

										<div class="row ">
											<div class="col-md-3">
												<label style=""><b>Loan Schedule</b></label>
											</div>
										</div>
										<div class="row ">
											<table style="width:50% !important;float:left;" id="table_loan" class="table table-bordered table-striped table-condensed flip-content">
												<tbody>
													<tr>
														<td>Property Address</td>
														<td>Loan Status</td>
														<td>Loan Amount</td>
														<td>Loan Type</td>
													</tr>
												</tbody>
											</table>

											<table style="width:30% !important;float:left;margin-left: 135px;" id="table_stats2" class="table table-bordered table-striped table-condensed flip-content">
												<tbody>

													<tr>
														<td>Active Loans:</td>
														<td>##</td>
													</tr>
													<tr>
														<td>Active Loans:</td>
														<td>$#,###,####</td>
													</tr>
													<tr>
														<td>Paid Off Loans:</td>
														<td>##</td>
													</tr>
													<tr>
														<td>Paid Off Loans:</td>
														<td>$#,###,####</td>
													</tr>
													<tr>
														<td>Total Loans:</td>
														<td>##</td>
													</tr>
													<tr>
														<td>Active Loans:</td>
														<td>$#,###,####</td>
													</tr>
												</tbody>
											</table>
										</div>
										<hr style="border-color:black !important;">

									</div>

									<div class="modal-footer">
										<button type="button" class="btn default" data-dismiss="modal">Close</button>
										<!--<button type="submit"  class="btn blue">Submit</button>-->
									</div>
								</div>
								<!-- /.modal-content -->
							</div>
						<!-- /.modal-dialog -->
						</div>

						<!--- Statistics modal  End ---->

						<form method="POST" action="<?php echo base_url(); ?>Contact/update_contact_data" id="formID" name="formID_bro">

							<input type="hidden" name="borower_contact_type_id" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_contact_id : ''; ?>">

							<input type="hidden" name="lender_contact_type_id" value="<?php echo isset($fetch_lender_contact_type) ? $fetch_lender_contact_type[0]->lender_contact_id : ''; ?>">

							<input type="hidden" name="contact_id" value="<?php echo isset($contact_id) ? $contact_id : ''; ?>" >

							<input type="hidden" name="first_name" value="<?php echo $fetch_contact_data[0]->contact_firstname; ?>" >
							<input type="hidden" name="middle_name" value="<?php echo $fetch_contact_data[0]->contact_middlename; ?>" >
							<input type="hidden" name="last_name" value="<?php echo $fetch_contact_data[0]->contact_lastname; ?>" >




							

							<!---  Borrower Data modal   Starts---->
							<div class="modal fade bs-modal-lg" id="borrower_data" tabindex="-1" role="large" aria-hidden="true">
								<div class="modal-dialog modal-lg" style="width: 67%;">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Contact<?php echo isset($contact_id) ? ': ' . $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname.', ' . $fetch_contact_data[0]->suffix : ''; ?></h4>
										</div>
										<!-----------Hidden fields----------->
										<!-----------End of hidden Fields---->
										<div class="modal-body">
											<!------- Borrower Contact Type DIV STARTS ---------->

                                            <div class="row" >
                                                <h4 style="background-color:#efefef;padding: 10px;">Borrower Portal Access</h4>
                                                    <div id="messagesChange"></div>
                                                    <small><mark><b>Note:</b> Before sending Email/login details to the Borrower, please select 'Yes' under Borrower Portal Access drop-down menu and then save the information.</mark><br><br></small>
                                                    <div class="col-md-3">
                                                        <label>Borrower Portal Access:</label>
                                                            <select  class="form-control" name="allow_access_borrower">

                                                                <?php
                                                                    foreach ($lender_portal_access as $key => $row) {
                                                                        ?>
                                                                    <option value="<?php echo $key; ?>" <?php if ($key == $fetch_borrower_contact_type[0]->allow_access_borrower) {echo 'Selected';}?>><?php echo $row; ?></option>
                                                                    <?php
                                                                    }
                                                                ?>
                                                            </select>
                                                    </div>

                                                    <?php 
                                                        if($fetch_borrower_contact_type[0]->borrower_username != ''){

                                                            $userborrower = $fetch_borrower_contact_type[0]->borrower_username;
                                                        }else{
                                                            $userborrower = $fetch_contact_data[0]->contact_firstname.$fetch_contact_data[0]->contact_lastname;
                                                        }

                                                        $borrower_rand_password = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTVWXYZ0123456789"), 1, 12);

                                                    ?>
                                                    
                                                    <div class="col-md-3">
                                                        <label>Username:</label>
                                                        <input type="text" name="borrower_username" class="form-control" value="<?php echo $userborrower;?>" readonly>
                                                    </div>
                                                    
                                                    <div class="col-md-3">
                                                        <label>Password:</label>
                                                        <input type="text" name="borrower_password" class="form-control" value="<?php echo isset($fetch_borrower_contact_type[0]->borrower_password) ? base64_decode($fetch_borrower_contact_type[0]->borrower_password) : $borrower_rand_password;?>" readonly>

                                                    </div>
                                                    <div class="col-md-3" style="margin-top: 24px;">

                                                        <a style="margin-top: 0px !important;" class="btn btn-success"> Send E-mail </a> 
                                                        <span style="padding-left: 5px;padding-right: 5px;font-size: 15px;">OR</span> 
                                                        <a style="margin-top: 0px;padding-left: 7px;" class="btn btn-success" href="javascript:void(0)" onclick="SendLinkToForget(<?php echo $contact_id;?>,1,'<?php echo $userborrower;?>')">Reset Password</a>
                                                        
                                                    </div>
                                                   
                                                    <!-- <div class="col-md-1" style="padding-left: 30px;">
                                                    	
                                                    </div> -->
                                            </div>

											<div class="borrower_data_popup">

											<h4 style="background-color:#efefef;padding: 10px;">Active Loans</h4>

												<div class="row borrower_data_row">
											   <div class="col-md-12">
												 <table class="bro table table-border table-responsive table-stripe">
														<thead>
														<tr>
															<th style="width:41%;">Address</th>
															<th style="width:29%;">Loan Amount</th>
															<th style="width:30%;">Rate</th>
														</tr>
														</thead>
														<tbody>
														<?php
                                                        /*echo '<pre>';
                                                        print_r($contact_active_loan_query);
                                                        echo '</pre>'; */

                                                        if (isset($contact_active_loan_query)) {
                                                        	foreach ($contact_active_loan_query as $rowss) {
                                                            foreach ($rowss as $row) {?>
														<tr>
															<td><a href="<?php echo base_url() . 'load_data/' . $row->loan_id; ?>"><?php echo ($row->property_address . ' ' . $row->unit) ? $row->property_address . ' ' . $row->unit : ''; ?></a></td>
															<td><?php echo ($row->loan_amount) ? '$' . number_format($row->loan_amount) : ''; ?></td>
															<td><?php echo ($row->interestrate) ? number_format($row->interestrate, 3) . '%' : ''; ?></td>
														</tr>
													<?php } } } ?>

														</tbody>
														</table>

														</div>



														</div>


											<h4 style="background-color:#efefef;padding: 10px;">Borrower Statistics</h4>

											<div class="row borrower_data_row">
											   <div class="col-md-12">
												 <table class="bro table table-border table-responsive table-stripe">
														<thead>
														<tr>
															<th>Loans</th>
															<th># of Loans</th>
															<th>$ of Loans</th>
														</tr>
														<thead>
														<tbody>
														<?php

                                                            $t_amount = 0;
                                                            $count_paidoff = 0;

                                                            foreach ($fetch_paidoff_loan_latest as $row) {
                                                            	$count_paidoff++;
                                                            	$t_amount += $row['loan_amount'];
                                                            }

                                                            $t_b_amount = 0;
                                                            $count_brokered = 0;

                                                            foreach ($fetch_brokered_loan_latest as $roooow) {
                                                            	$count_brokered++;
                                                            	$t_b_amount += $roooow['b_loan_amount'];
                                                            }

                                                            $can_count_brokered = 0;
                                                            $can_t_b_amount = 0;
                                                            foreach ($fetch_cancellled_loan_latest as $w) {
                                                            	$can_count_brokered++;
                                                            	$can_t_b_amount += $w['can_b_loan_amount'];

                                                            }

                                                            $ative_t_amount = 0;
                                                            $count_active = 0;
                                                            foreach ($fetch_active_loan_latest as $r) {
                                                            	$count_active++;
                                                            	$ative_t_amount += $r['activ_loan_amount'];

                                                            }

                                                            $pipe_t_amount = 0;
                                                            $count_p = 0;
                                                            foreach ($fetch_pipe_loan_latest as $pipe) {
                                                            	$count_p++;
                                                            	$pipe_t_amount += $pipe['p_loan_amount'];
                                                            }

                                                            ?>

														<tr>
															<td>Pipeline Loans</td>
															<td><?php echo $count_p; ?></td>
															<td>$<?php echo number_format($pipe_t_amount); ?></td>
														</tr>
														<tr>
															<td>Active Loans</td>
															<td><?php echo $count_active; ?></td>
															<td>$<?php echo number_format($ative_t_amount); ?></td>
														</tr>
														<tr>
															<td>Brokered Loans </td>
															<td><?php echo $count_brokered; ?></td>
															<td>$<?php echo number_format($t_b_amount); ?></td>
														</tr>

														<tr>
															<td>Cancelled Loans </td>
															<td><?php echo $can_count_brokered; ?></td>
															<td>$<?php echo number_format($can_t_b_amount); ?></td>
														</tr>
														<tr>
															<td>Paid Off Loans</td>
															<td><?php echo $count_paidoff; ?></td>
															<td>$<?php echo number_format($t_amount); ?></td>
														</tr>


														<?php
                                                        $total_loan_count = $count_active + $count_paidoff + $count_p + $count_brokered + $can_count_brokered;
                                                        $total_loan_amount = $ative_t_amount + $t_amount + $pipe_t_amount + $t_b_amount + $can_t_b_amount;

                                                        ?>
														<tr>
															<th>Total Loans</th>
															<th><?php echo $total_loan_count; ?></th>
															<th>$<?php echo number_format($total_loan_amount); ?></th>
														</tr>

														<tr>
															<th><a href="<?php echo base_url('reports/loan_schedule_borrower/' . $contact_id); ?>">View Report</a></th>

														</tr>

														</tbody>
														</table>

														</div>



													</div>




											<h4 style="background-color:#efefef;padding: 10px;">Borrower Accounts</h4>
											<div class="row borrower_data_row">
											   <div class="col-md-12">
												 	<table class="bro table table-border table-responsive table-stripe">
														<thead>
															<tr>
																<th>Borrower Name</th>
                                                                <th>PBA Account</th>
																<th>Active Loans</th>
																<th>Active Loans</th>
															</tr>
														</thead>
														<tbody>
															<?php if(isset($fetch_borrower_accounts) && is_array($fetch_borrower_accounts)){ 
																	foreach($fetch_borrower_accounts as $value){ 
                                                                        $pbaAccount="";
                                                                        if($value['PreApproved']=="1"){
                                                                            $pbaAccount="Yes";
                                                                        }else{
                                                                            $pbaAccount="No";
                                                                        }
                                                                    ?>
																	<tr>
																		<td>
																			<a href="<?php echo base_url();?>borrower_view/<?php echo $value['borrower_id'];?>"><?php echo $value['borrowername'];?></a>
																		</td>
                                                                        <td><?php echo $pbaAccount;?></td>
																		<td><?php echo $value['totalCount'];?></td>
																		<td>$<?php echo number_format($value['totalAmount']);?></td>
																	</tr>
														<?php } }else{ ?>
																	<tr>
																		<td colspan="3">Borrower account details not found</td>
																	</tr>
														<?php } ?>
														</tbody>
													</table>
												</div>
											</div>


											<h4 style="background-color:#efefef;padding: 10px;">Personal Information</h4>
												<div class="row borrower_data_row" id="">
													<div class="col-md-3">
														<label>Social Security Number:</label>
														<input  type="text" class="form-control " name="security_number" id="security_number" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_ss_num : ''; ?>" placeholder="Social Security Number" />
													</div>

													<div class="col-md-3">
														<label>DOB:</label>
														<input type="text" class="form-control datepicker" name="dob" id="dob" value="<?php if (isset($contact_id)) {echo $fetch_borrower_contact_type[0]->borrower_contact_dob ? date('m-d-Y', strtotime($fetch_borrower_contact_type[0]->borrower_contact_dob)) : '';}?>" placeholder="DOB(Format:MM-DD-YYYY)" />
													</div>

												</div>

												<div class="row borrower_data_row" id="">
													<div class="col-md-3">
														<label>Driver License #:</label>
														<input type="textbox" class="form-control " id="driver_license" name="driver_license" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_driver_license : ''; ?>" placeholder="Driver License #" />
													</div>
													<div class="col-md-3">

														<label>Driver's License State:</label>
														<!--<input type="textbox" class="form-control borrower_enabled" id="driver_license_state" name="driver_license_state" value="<?php echo $fetch_borrower_contact_type[0]->borrower_driver_license_state ? $fetch_borrower_contact_type[0]->borrower_driver_license_state : ''; ?>" placeholder="Driver's License State" />-->

														<select id="driver_license_state" name="driver_license_state" class ="form-control">
															<option value=""></option>
															<?php
$selected = '';
foreach ($STATE_USA as $key => $value) {
	if ($key == $fetch_borrower_contact_type[0]->borrower_driver_license_state) {
		$selected = 'selected';
	} else {
		$selected = '';
	}
	?>
																<option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
															<?php }?>



														</select>
													</div>
													<?php
//print_r($fetch_borrower_contact_type);
?>
													<div class="col-md-3">

														<label>Expiration Date:</label>
														<input  type="text" name="exp_date" value="<?php echo isset($fetch_borrower_contact_type[0]->borrower_exp_date) ? date('m-d-Y', strtotime($fetch_borrower_contact_type[0]->borrower_exp_date)) : ''; ?>" class="form-control  datepicker"  placeholder="Expiration Date(Format:DD-MM-YYYY)" />
													</div>
												</div>

												<div class="row borrower_data_row" id="">

													<div class="col-md-3">

														<label>Credit Score:</label>
														<input  type="text" class="form-control " name="credit_score" id="credit_score" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_credit_score : ''; ?>" placeholder="Credit Score" />
													</div>

													<div class="col-md-3">

														<label>Credit Score Date:</label>
														<input type="text" class="form-control  datepicker" name="credit_score_date" id="credit_score_date" value="<?php echo isset($fetch_borrower_contact_type[0]->borrower_credit_score_date) ? date('m-d-Y', strtotime($fetch_borrower_contact_type[0]->borrower_credit_score_date)) : ''; ?>" placeholder="Credit Score Date(Format:DD-MM-YYYY)" />
													</div>

												</div>
												<div class="row borrower_data_row borrower_textarea_div">
													<div class="col-md-9">
														<textarea  class="form-control " name="credit_score_desc" placeholder="Credit Score Explanation" id="credit_score_desc"><?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_credit_score_desc : ''; ?></textarea>
													</div>
												</div>
												<div class="row borrower_data_row" id="">
													<div class="col-md-3">

													<label>Marital Status:</label>
													<select class="form-control " name="marital_status">
														<?php foreach ($marital_status as $key => $row) {?>
														<option value="<?php echo $key; ?>" <?php if ($key == $fetch_borrower_contact_type[0]->borrower_marital_status) {echo 'Selected';}?>><?php echo $row; ?></option>
														<?php }?>

													</select>
													</div>

													<div class="col-md-3">

														<label># of Dependents:</label>
														<input  type="text" class="form-control " name="dependents" id="dependents" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_dependents : ''; ?>" placeholder="# of Dependents" />
													</div>


												</div>
										<!-- 	    <div class="row borrower_data_row" id="">
													 <div class="col-md-4">

														 <label>Borrower / Contact OFAC Search:</label>
														 <select class="form-control" name="ofac_search">
															<?php foreach ($no_yes_option as $key => $row) {?>
																<option value="<?php echo $key; ?>" <?php if ($key == $fetch_borrower_contact_type[0]->ofac_search) {echo 'Selected';}?>><?php echo $row; ?></option>
															<?php }?>
														 </select>
													  </div>

												</div>	 -->

												<h4 style="background-color:#efefef;padding: 10px;">Borrower Experience</h4>

												<div class="row borrower_data_row" id="borrower_experience_selectbox">
												    <div class="col-md-3">
													<label>Borrower rehabbed:</label>
													     <select class="form-control" name="borrower_experience" onchange="display_how_may_box(this.value);">
														<?php foreach ($yes_no_option3 as $key => $row) {?>
															<option value="<?php echo $key; ?>" <?php if ($key == $fetch_borrower_contact_type[0]->borrower_experience) {echo 'selected';}?>><?php echo $row; ?></option>
														<?php }?>
													     </select>
													</div>

													<div class="col-md-3" id="how_may_box" style="display:none;">
														<label>If Yes, how many?</label>
														<input type="text" name="ex_how_many" class="form-control" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->ex_how_many : ''; ?>">
													</div>

												</div>


												<h4 style="background-color:#efefef;padding: 10px;">Employment History</h4>

												<div class="row borrower_data_row">

												    <div class="col-md-3">
														 <label>Employment Status:</label>
													     <select class="form-control" name="employment_status">
														<?php foreach ($employment_status as $key => $row) {?>
														<option value="<?php echo $key; ?>" <?php if ($key == $fetch_borrower_contact_type[0]->employment_status) {echo 'Selected';}?>><?php echo $row; ?></option>
														<?php }?>

													     </select>
													</div>

												</div>

											  <div class="row borrower_data_row">


													<div class="col-md-3">
														<label>Current Employer:</label>
														<input type="text" name="current_employer" class="form-control" value="<?php echo isset($fetch_borrower_contact_type) ? ($fetch_borrower_contact_type[0]->current_employer) : ''; ?>">
													</div>

											        <div class="col-md-3">
														<label>Job Title:</label>
														<input type="text" name="job_title" class="form-control" value="<?php echo isset($fetch_borrower_contact_type) ? ($fetch_borrower_contact_type[0]->job_title) : ''; ?>">
													</div>

													<div class="col-md-3">
														<label>Years at Job:</label>
														<input type="text" name="year_at_job" class="form-control" value="<?php echo isset($fetch_borrower_contact_type) ? ($fetch_borrower_contact_type[0]->year_at_job) : ''; ?>">
													</div>
											 </div>
												<!--<div class="row borrower_data_row">

														<div class="col-md-3">
														<label>Annual Income:</label>
														<input type="text" name="annul_income" class="form-control amount_format" value="$<?php echo isset($fetch_borrower_contact_type) ? number_format($fetch_borrower_contact_type[0]->annul_income) : ''; ?>">
													</div>




												</div>-->

												<!--<div class="row borrower_data_row">
													<div class="col-md-3">
														<label>Estimated Income YTD:</label>
														<input type="text" name="estimate_income" class="form-control amount_format" value="$<?php echo isset($fetch_borrower_contact_type) ? number_format($fetch_borrower_contact_type[0]->estimate_income) : ''; ?>">
													</div>
													<div class="col-md-3">
														<label>Year:</label>
														<input type="text" name="estimate_income_year" maxlength="4" class="form-control number_only" value="<?php echo isset($fetch_borrower_contact_type) ? ($fetch_borrower_contact_type[0]->estimate_income_year) : ''; ?>">
													</div>

														<div class="col-md-3">
														<label>Confirmed w/ Paystub:</label>
														<select name="conform_paystub" class="form-control">
														<?php foreach ($yes_no_option3 as $key => $row) {?>
														<option value="<?php echo $key; ?>" <?php if ($key == $fetch_borrower_contact_type[0]->conform_paystub) {echo 'Selected';}?>><?php echo $row; ?></option>
														<?php }?>
														</select>
													</div>

												</div>-->
												<div class="row borrower_data_row">
													<div class="col-md-3">
														<label>Annual Income:</label>
														<input type="text" name="prvious_yr_income" class="form-control amount_format" value="$<?php echo isset($fetch_borrower_contact_type) ? number_format($fetch_borrower_contact_type[0]->prvious_yr_income) : ''; ?>">
													</div>
													<div class="col-md-3">
														<label>Year:</label>
														<input type="text" name="prvious_income_year" maxlength="4" class="form-control number_only" value="<?php echo isset($fetch_borrower_contact_type) ? ($fetch_borrower_contact_type[0]->prvious_income_year) : ''; ?>">
													</div>
													<div class="col-md-3">
														<label>Confirmed w/ Return:</label>
														<select name="conform_return" class="form-control">
														<?php foreach ($yes_no_option3 as $key => $row) {?>
														<option value="<?php echo $key; ?>" <?php if ($key == $fetch_borrower_contact_type[0]->conform_return) {echo 'Selected';}?>><?php echo $row; ?></option>
														<?php }?>
														</select>
													</div>
												</div>

												<h4 style="background-color:#efefef;padding: 10px;">Other Disclosures</h4>

												<div class="col-md-12 borrower_data_row" id="">
													<div class="col-md-8">
														<label>Is the Contact a US citizen? </label>
													</div>
													<div class="col-md-4">
														<select  class="form-control borrower_enabled" name="us_citizen" id="us_citizen" onchange="display_us_citizenNew(this.value)">
															<!--<option value="">SELECT</option>-->
															<?php

foreach ($us_yes_no_option as $key => $row) {
	?>
															<option value="<?php echo $key; ?>" <?php if (isset($fetch_borrower_contact_type)) {if ($fetch_borrower_contact_type[0]->borrower_us_citizen == $key) {echo 'selected';}}?>><?php echo $row; ?></option>
															<?php
}
?>

														</select>
													</div>

												</div>



												<div class="row   borrower_data_row borrower_textarea_div us_citizen_optn" style=" display:none" id="">

													<div  class="col-md-3 ">
														<textarea placeholder="If No, Description?" rows="4" name="us_citizen_desc" id="us_citizen_desc"><?php if (isset($fetch_borrower_contact_type)) {echo $fetch_borrower_contact_type[0]->borrower_us_citizen_desc;}?></textarea>
													</div>

												</div>

												<div class="col-md-12 borrower_data_row" id="">
													<div class="col-md-8" >
														<label>Does the contact have Pending Litigation? </label>
													</div>
													<div class="col-md-4">
														<select class="form-control  borrower_enabled" name="litigation" id="litigation" onchange="display_litigation(this.value)">
															<!--<option value="">SELECT</option>-->
															<?php
foreach ($yes_no_option as $key => $row) {
	?>
																<option value="<?php echo $key; ?>" <?php if (isset($fetch_borrower_contact_type)) {if ($fetch_borrower_contact_type[0]->borrower_litigation == $key) {echo 'selected';}}?>><?php echo $row; ?></option>
																<?php
}
?>

														</select>
													</div>

												</div>


												<div class="row  borrower_data_row borrower_textarea_div litigation_optn" style=" display:none" id="">

													<div  class="col-md-3 ">
														<label>If Yes, explain:</label>
														<textarea placeholder="If Yes, explain?" rows="5" name="litigation_desc" id="litigation_desc"><?php if (isset($fetch_borrower_contact_type)) {echo $fetch_borrower_contact_type[0]->borrower_litigation_desc;}?></textarea>
													</div>

												</div>


												<div class="col-md-12 borrower_data_row">
													<div class="col-md-8" >
														<label>Has the Contact been convicted of a felony?</label>
													</div>

													<div class="col-md-4">
														<select class="form-control  borrower_enabled" name="felony" id="felony" onchange="display_felony(this.value)">
															<!--<option value="">SELECT</option>-->
															<?php
foreach ($yes_no_option as $key => $row) {
	?>
																<option value="<?php echo $key; ?>" <?php if (isset($fetch_borrower_contact_type)) {if ($fetch_borrower_contact_type[0]->borrower_felony == $key) {echo 'selected';}}?>><?php echo $row; ?></option>
																<?php
}
?>

														</select>
													</div>

												</div>



												<div class="row  borrower_data_row borrower_textarea_div felony_optn" style=" display:none" id="">

													<div class="col-md-3">
														<label>If Yes, explain:</label>

														<textarea placeholder="If Yes, explain?" rows="5" name="felony_desc" id="felony_desc"><?php if (isset($fetch_borrower_contact_type)) {echo $fetch_borrower_contact_type[0]->borrower_felony_desc;}?></textarea>
													</div>

												</div>


												<div class="col-md-12 borrower_data_row">
													<div class="col-md-8" >
														<label>Has the Contact declared bankruptcy in last 7 years?</label>
													</div>

													<div class="col-md-4" >
														<select class="form-control borrower_enabled" name="Bankruptcy_year" onchange="display_bankruptcy_7year(this.value);" id="text_7year">

															<?php
foreach ($yes_no_option as $key => $row) {
	?>
																<option value="<?php echo $key; ?>" <?php if (isset($fetch_borrower_contact_type)) {if ($fetch_borrower_contact_type[0]->bankruptcy_year == $key) {echo 'selected';}}?>><?php echo $row; ?></option>
																<?php
}
?>

														</select>
													</div>
												</div>
												<div class="row borrower_data_row borrower_textarea_div bankruptcy_7year_text" style ="display:none">
													<div class="col-md-3">
														<textarea type="text" rows="2" name="bankruptcy_7year_text" ><?php if (isset($fetch_borrower_contact_type)) {echo $fetch_borrower_contact_type[0]->bankruptcy_7year_text;}?></textarea>
													</div>
												</div>

												<div class="col-md-12 borrower_data_row">
													<div class="col-md-8" >
														<label>Has the Contact declared bankruptcy in last 12 months?</label>
													</div>
													<div class="col-md-4" >
														<select class="form-control borrower_enabled" name="Bankruptcy" id="Bankruptcy" onchange="display_BK(this.value)">

															<?php
foreach ($yes_no_option as $key => $row) {
	?>
																<option value="<?php echo $key; ?>" <?php if (isset($fetch_borrower_contact_type)) {if ($fetch_borrower_contact_type[0]->borrower_bankruptcy == $key) {echo 'selected';}}?>><?php echo $row; ?></option>
																<?php
}
?>

														</select>
													</div>
												</div>

												<div class="col-md-12 borrower_data_row">
													<div class="col-md-8" >
														<label>OFAC Search Completed / Saved</label>
													</div>
													<div class="col-md-4" >

														 <select class="form-control" name="ofac_srch_completed">
															<?php foreach ($ofac_srch_option as $key => $row) {?>
																<option value="<?php echo $key; ?>" <?php if ($key == $fetch_borrower_contact_type[0]->ofac_srch_completed) {echo 'Selected';}?>><?php echo $row; ?></option>
															<?php }?>
														 </select>
													</div>
												</div>

												<div class="row  borrower_data_row" id="">
													<div style="display:none" class="col-md-3 BK_optn">
														<label>Date Dismissed: </label>
													</div>
													<div  style="display:none" class="col-md-3 BK_optn">
														<input type="text" class="form-control lender_enabled input-md datepicker" name="date_dismissed" value="<?php echo isset($fetch_borrower_contact_type) ? date('m-d-Y', strtotime($fetch_borrower_contact_type[0]->borrower_date_dismissed)) : ''; ?>" placeholder="MM-DD-YYYY" />
													</div>
												</div>
												<div class="row  borrower_data_row" id="">
													<div style="display:none" class="col-md-3 BK_optn">
														<label>If Yes, has BK been dismissed?: </label>
													</div>
													<div style="display:none" class="col-md-3 BK_optn">
														<select  class="form-control  borrower_enabled" name="BK" id="BK" onchange="display_bk_txtarea(this.value);">
															<?php foreach ($yes_no_option as $key => $row) { ?>
																<option value="<?php echo $key; ?>" <?php if (isset($fetch_borrower_contact_type)) {if ($fetch_borrower_contact_type[0]->borrower_BK == $key) {echo 'selected';}}?>><?php echo $row; ?></option>
															<?php } ?>														
														</select>
													</div>
												</div>
												<div style= "display:none;" class="row borrower_data_row borrower_textarea_div BK_textarea_div">
													<div class="col-md-3">
														<label>If Yes, BK Explanation:</label>

														<textarea  placeholder="If Yes, BK Explanation" rows="5" class="form-control borrower_enabled" name="bk_desc" id="bk_desc"><?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_BK_desc : ''; ?></textarea>
													</div>
												</div>
											</div>
											<!----- Borrower Contact Type DIV ENDS   -------->
										</div>

										<div class="modal-footer">
											<input type="hidden" name="popup_data" value="borrower_data">
											<button type="button" class="btn default" data-dismiss="modal">Close</button>
											<button type="submit" name="modal_btn" value="save" class="btn blue">Save</button>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>

		<!---  Borrower Data modal  End ---->
        <!---  Lender Data modal   Starts---->
<style type="text/css">
        .row > .rowh4{
        	background-color:#efefef;
        	padding: 10px; 
        	font-family: sans-serif;
        }        
		div.checkId > .checker{
			width: 6%!important;
		}
		div.checkId1 > .checker{
			width: 10%!important;
		}					
        </style>
<div class="modal fade bs-modal-lg" id="lender_data" tabindex="-1" role="large" aria-hidden="true">
	<div class="modal-dialog modal-lg" style="width: 67%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Lender Data <?php echo isset($contact_id) ? ': ' . $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname : ''; ?></h4>
			</div>

			<div class="modal-body">

                <div class="row">
                    <h4 class="rowh4">Security Questions</h4>
                    <div class="col-md-6">
                        <label>Security Question</label>
                        <input type="text" name="accountword" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->accountword) ? $fetch_lender_contact_type[0]->accountword : ''; ?>" placeholder="Account Word">
                    </div>
                    <div class="col-md-6">
                        <label>Security Answer</label>
                        <input type="text" name="accountwordAnswer" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->accountwordAnswer) ? $fetch_lender_contact_type[0]->accountwordAnswer : ''; ?>" placeholder="Account Word">
                    </div>
                </div>
                <?php                                            
				/*03-02-2021*/
				$css_av_account = '';
				$css_av_account_varification = '';
				if($fetch_lender_contact_type[0]->status == 'av_account'){
					$avArgument  = '';
					$avArgument .= $fetch_lender_contact_type[0]->lender_contact_id.'@@@@';
					$avArgument .= $fetch_contact_data[0]->contact_email.'@@@@';
					$avArgument .= $fetch_contact_data[0]->contact_firstname.'@@@@';
					$avArgument .= $fetch_contact_data[0]->contact_lastname.'@@@@';
					$avArgument .= $fetch_contact_data[0]->contact_phone;
					

					$css_av_account = 'display:none;';
					$css_av_account_varification = '<a style="margin-top: 0px;padding-left: 7px;" class="btn btn-success av_account_varification_account" href="javascript:void(0)" av_account_data="'.base64_encode($avArgument).'" >Create Lender User</a>';
				}

				if($fetch_lender_contact_type[0]->username != ''){
                    $userlender = $fetch_lender_contact_type[0]->username;
                }else{
                	$strflname = $fetch_contact_data[0]->contact_firstname;
                    $userlender = $strflname[0].'_'.$fetch_contact_data[0]->contact_lastname;
                }
                $rand_password = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTVWXYZ0123456789"), 1, 10);
				?>
                <div class="row">
                    <h4 class="rowh4">Security Access</h4>
                    	<div id="messagesChange1"></div>
                        <small>
                        	<mark>
                        		<b>Note:</b> Before sending Email/login details to the Lender, please select 'Yes' under Lender Portal Access drop-down menu and then save the information.
                        	</mark>
                        	<br><br>
                        </small>

                        <div class="col-md-12">
                        	<?php echo $css_av_account_varification; ?>
                        </div>

                        <div class="col-md-4" style="<?php echo $css_av_account; ?>">
                        	<div class="portalcss_av_account" >
                                <label for="allow_access">Lender Portal Access</label>
                                <select  class="form-control" name="allow_access" id="allow_access" style="    width: 100px !important;">

                                    <?php
                                        foreach ($lender_portal_access as $key => $row) {
                                            ?>
                                        <option value="<?php echo $key; ?>" <?php if ($key == $fetch_lender_contact_type[0]->allow_access) {echo 'Selected';}?>><?php echo $row; ?></option>
                                        <?php
                                        }
                                        ?>
                                </select>
                        	</div>
                        </div>
                        <div class="col-md-4" style="<?php echo $css_av_account; ?>">
                        	<?php
                            	$lender_td_details = 'no';
                            	$no_selected = 'selected';
                            	$yes_selected = '';
                            	if(isset($fetch_lender_contact_type[0]->lender_td_details)){
                            		if($fetch_lender_contact_type[0]->lender_td_details == 'yes'){
                            			$lender_td_details = 'yes';
                            			$no_selected = '';
                            			$yes_selected = 'selected';
                            		}
                            	}
                            ?>
                            <label>Trust Deed Details Access</label>
                            <br>
                            <select name="lender_td_details" id="lender_td_details" class="form-control" style="width: 100px !important;" contact_id="<?php echo $contact_id;?>" >
                            	<option value="no" <?php echo $no_selected; ?>>No</option>
                            	<option value="yes" <?php echo $yes_selected; ?>>Yes</option>
                            </select>		
                        </div>

                        <div class="col-md-4" style="<?php echo $css_av_account; ?>">
                        	
                            <label>Mortgage Fund Access</label>
                            <br>
                            <select name="mortgageActivate" onchange="mortgageF(this.value,'<?php echo $contact_id;?>')" class="selectpicker form-control" id="mortgageActivate">
								<option value='no' <?php echo $mortgage['mortgageActivate']=='no'?'selected':'' ?>>No</option>
								<option value='yes' <?php echo $mortgage['mortgageActivate']=='yes'?'selected':'' ?>>Yes</option>	
							</select>
                            		
                        </div>

                        <div class="col-md-12">&nbsp;</div>

                        <div class="col-md-6" style="<?php echo $css_av_account; ?>">
                        	<label>Username</label>
                            <input type="text" name="username" class="form-control" value="<?php echo str_replace(" ", "", $userlender);?>" readonly />
                            
                        </div>
                        <div class="col-md-6" style="<?php echo $css_av_account; ?>">
                        	<label>Password</label>
                            <input type="text" name="password" class="form-control" value="<?php echo $fetch_lender_contact_type[0]->password ? $fetch_lender_contact_type[0]->password : $rand_password; ?>" readonly />
                        </div>
                        <div class="col-md-12">&nbsp;</div>
                        <div class="col-md-4" style="<?php echo $css_av_account; ?>">
                          	<?php if($fetch_lender_contact_type[0]->allow_access == '1'){ ?>
                        	<a style="margin-top: 0px;padding-left: 7px;" class="btn btn-success" href="javascript:void(0)" onclick="SendUsername(<?php echo $contact_id;?>,0,'<?php echo str_replace(" ", "", $userlender);?>')">Send User Name</a>
                        	<?php  }else{ ?>
                        	<a style="margin-top: 0px;padding-left: 7px;" class="btn btn-success" href="javascript:void(0)" disabled>Send User Name</a>
                        	<?php  } ?>
                        	
                        	
                        </div>

                        <div class="col-md-4" style="<?php echo $css_av_account; ?>">
                          	<?php if($fetch_lender_contact_type[0]->allow_access == '1'){ ?>
                        	<a style="margin-top: 0px;padding-left: 7px;" class="btn btn-success" href="javascript:void(0)" onclick="SendLinkToForget(<?php echo $contact_id;?>,0,'<?php echo str_replace(" ", "", $userlender);?>')">Reset Password</a>
                        	<?php  }else{ ?>
                        	<a style="margin-top: 0px;padding-left: 7px;" class="btn btn-success" href="javascript:void(0)" disabled>Reset Password</a>
                        	<?php  } ?>
                        </div>
                        <div class="col-md-4" style="<?php echo $css_av_account; ?>"> 
                            <a style="margin-top: 0px;padding-left: 7px;" data-toggle="modal" class="btn btn-success" href="#LoginHistory">Login History</a>
                        </div>
                        <div class="col-md-12">&nbsp;</div>                 
                        <!-- <div class="col-md-12" style="<?php //echo $css_av_account; ?>">
                            <label><b>Login History</label> 
                            <span><?php //echo $lastlogindetails; ?></b></span>
                        </div> -->
                </div>

                <script type="text/javascript">
                    
                    function send_email_to_contact(that){

                        var contact_id = '<?php echo $contact_id;?>';

                        if(contact_id != ''){
                            $.ajax({
                                type : 'post',
                                url  : '<?php echo base_url()."Lender_Access/lender_forgot_password"?>',
                                data : {'contact_id': contact_id},
                                success : function(response){
                                    if(response == 1){
                                    	alert('Email successfully send');
                                    }else{
                                        alert('Error: Email not send');
                                    }
                                }
                            });

                        }else{

                            alert('Error: contact ID is empty!');
                            return false;
                        }
                    }

                </script>

               

                <script type="text/javascript">
                function mortgageF(that,id)
				{

						$.ajax({
						        type : 'POST',
						        url  : '<?echo base_url()."Contact/activateMortgage";?>',
						        data : {'mortgageActivate':that,'id':id},
						        success : function(response){
							        var json =  JSON.parse(response);
									var message = json.message;
									$("#messagesActivate").html(message);
						        }
						    });
				}
                </script>


                <div class="row lender_data_row" id="">
                    <h4 class="rowh4">Active Trust Deeds</h4>
                    <table class="table table-border">
                        <thead>
                            <tr>
                                <th>Street Address</th>
                                <th>Interest ($)</th>
                                <th>Loan Amount</th>
                                <th>Account</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($activeTrustDeedArray) && is_array($activeTrustDeedArray)){ 
                                    foreach($activeTrustDeedArray as $value){ 

                                    	if($value['loan_amount'] > 0){ ?>
                                <tr>
                                    <td><a href="<?php echo base_url();?>load_data/<?php echo $value['loan_id'];?>"><?php echo $value['fulladdress'];?></a></td>
                                    <td>$<?php echo number_format($value['investment']);?></td>
                                    <td>$<?php echo number_format($value['loan_amount']);?></td>
                                    <td><a href=""><?php echo $allinvestorsnamelist[$value['lender_name']];?></a></td>
                                                                            
                                </tr>
                            <?php }  } }else{ ?>
                                <tr>
                                    <td colspan="4">Active Trust Deeds details not found!</td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

				<div class="row">
					<h4 class="rowh4">Lender Statistics</h4>
					<div class="col-md-12">
						<table class="bro table table-border table-responsive table-stripe">
						<thead>
							<tr>
								<th>Trust Deeds</th>
								<th># of Trust Deeds</th>
								<th>$ of Trust Deeds</th>
							</tr>
						<thead>
						<?php foreach ($active_loan as $row) {

                        	$count_loan = $row['count_loan'];
                        	$total_investment = $row['total_investment'];
                        	$total_loan_amount = $row['total_loan_amount'];

                        	$lender_id = $row['lender_id'];
                        }

                        foreach ($paidoff_loan as $row) {

                        	$p_count_loan = $row['count_loan'];
                        	$total_investment_p = $row['total_investment'];

                        }

                        ?>
						<tbody>
							<tr>
								<td>Active Trust Deeds</td>
								<td><?php echo $count_loan ? $count_loan : 0; ?></td>
								<td>$<?php echo number_format($total_investment); ?></td>
							</tr>
							<tr>
								<td>Paid Off Trust Deeds</td>
								<td><?php echo $p_count_loan; ?></td>
								<td>$<?php echo number_format($total_investment_p); ?></td>
							</tr>

							<tr>
								<td>Pipeline Loans</td>
								<td><?php echo $pipe_loan['count_pipe_loan']; ?></td>
								<td>$<?php echo number_format($pipe_loan['total_loan_amount']); ?></td>
							</tr>


							<?php
                                $total_trust_deeds_count = $count_loan + $p_count_loan;
                                $total_trust_deeds_amount = $total_investment + $total_investment_p;

                                ?>
							<tr>
								<th>Total Trust Deeds</th>
								<th><?php echo $total_trust_deeds_count; ?></th>
								<th>$<?php echo number_format($total_trust_deeds_amount); ?></th>
							</tr>
								<tr>
								<th><a href="<?php echo base_url('reports/loan_schedule_investor/' . $contact_id); ?>">View Report</a></th>

							</tr>
						</tbody>
					</table>
					</div>
				</div>

				<div class="row lender_data_row" id="">
					<h4 class="rowh4">Lender Accounts</h4>
					<table class="table table-border">
						<thead>
							<tr>
								<th>Account Name</th>
                                <th># of Active Trust Deeds</th>
								<th>$ of Active Trust Deeds</th>
                                <th># of Total Trust Deeds</th>
								<th>$ of Total Trust Deeds</th>
							</tr>
						</thead>
						<tbody>
							<?php 
                            if(isset($alllender_accountdetails) && is_array($alllender_accountdetails)){ 
									foreach($alllender_accountdetails as $value){ 
                                        $investerName=$allinvestorsnamelist[$value['allinvestor_id']];
                                        if(!empty($investerName)){
                                            ?>
        								    <tr>
            									<td><a href="<?php echo base_url();?>investor_view/<?php echo $value['allinvestor_id'];?>"><?php echo $investerName;?></a></td>
                                                <td><?php echo $value['activelendercount'];?></td>
            									<td>$<?php echo number_format($value['activelendersum']);?></td>
                                                <td><?php echo $value['alllendercount'];?></td>
            									<td>$<?php echo number_format($value['alllendersum']);?></td>								
        									
        								    </tr>
							                 <?php
                                        } 
                                    } 
                            }else{ ?>
								<tr>
									<td colspan="3">Lender account details not found!</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>





            
                

                <?php
                /*
                <div class="row lender_data_row" id="">
                	<h4 class="rowh4">Contact Lender Setup Information</h4>
                    <div class=" col-md-3">
                        <label>Lender Setup Package</label>
                            <select name="setup_package" class="form-control" >
                            <?php foreach ($setup_package_option as $key => $row) {?>
                                <option value="<?php echo $key; ?>" <?php if ($key == $fetch_lender_contact_type[0]->setup_package) {echo 'Selected';}?>><?php echo ucfirst($row); ?></option>
                            <?php }?>
                            </select>
                    </div>
                    <div class=" col-md-3">
                        <label>Date Submitted</label>
                            <input type="text" class="form-control datepicker" id="setup_submitted_date" name="setup_submitted_date" value="<?php echo isset($fetch_lender_contact_type[0]->setup_submitted_date) ? date('m-d-Y', strtotime($fetch_lender_contact_type[0]->setup_submitted_date)) : ''; ?>" placeholder="MM-DD-YYYY" />
                    </div>
                    <div class=" col-md-3">
                        <label>Date Completed</label>
                            <input type="text" class="form-control datepicker" id="package_date" name="setup_package_date" value="<?php echo isset($fetch_lender_contact_type[0]->setup_package_date) ? date('m-d-Y', strtotime($fetch_lender_contact_type[0]->setup_package_date)) : ''; ?>" placeholder="MM-DD-YYYY" />
                    </div>
                </div>
            <?php */?>
                <!-- Remove as per task
              updated Date-27-07-2021 -->
                <!-- <div class="row lender_data_row" id="">

                	<div class=" col-md-3">
                        <label>NDA Signed</label>

                    	<select name="nda_signed" class="form-control">
                            <?php foreach ($no_yes_option as $key => $row) {?>
                                <option value="<?php echo $key; ?>" <?php if ($key == $fetch_lender_contact_type[0]->nda_signed) {echo 'Selected';}?>><?php echo $row; ?></option>
                            <?php }?>
                        </select>
                    </div>

                    <div class=" col-md-3">
                        <label>Date Completed</label>
                            <input type="text" class="form-control datepicker" name="nda_date" value="<?php echo isset($fetch_lender_contact_type[0]->nda_date) ? date('m-d-Y', strtotime($fetch_lender_contact_type[0]->nda_date)) : ''; ?>" placeholder="MM-DD-YYYY" />
                    </div>
                </div>-->

                <div class="row" id="">
                	<h4 class="rowh4">Lender Survey</h4>
                	<div class="col-md-6">
                		<label>Will you invest in a Fractional Interest?</label> 
                	</div>
                	<div class="col-md-6">
                		<select class="selectpicker" name="ownership_type">
                			<option value="">Select One</option>
                			<option value="1" <?php echo (int)$fetch_lender_contact_type[0]->ownership_type==1?'selected':''; ?>>Yes</option>
                			<option value="2" <?php echo (int)$fetch_lender_contact_type[0]->ownership_type==2?'selected':''; ?>>No</option>
                		</select>
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;" ></div> 
                	<div class="col-md-6">
                		<label>What is your max LTV ratio?</label> 
                	</div>
                	<div class="col-md-6">
                		<?php $ltv = explode(",", $fetch_lender_contact_type[0]->ltv); ?>
                	
                		<select class="selectpicker" name="ltv" id="ltv_id">
                			<option value="">Select One</option>
							<option value="1" <?php echo (int)$fetch_lender_contact_type[0]->ltv==1?'selected':''; ?>>50% LTV</option>
							<option value="2" <?php echo (int)$fetch_lender_contact_type[0]->ltv==2?'selected':''; ?>>60% LTV</option>
							<option value="3" <?php echo (int)$fetch_lender_contact_type[0]->ltv==3?'selected':''; ?>>65% LTV</option>
							<option value="4" <?php echo (int)$fetch_lender_contact_type[0]->ltv==4?'selected':''; ?>>70% LTV</option>
							<option value="5" <?php echo (int)$fetch_lender_contact_type[0]->ltv==5?'selected':''; ?>>75% LTV</option>
							<option value="6" <?php echo (int)$fetch_lender_contact_type[0]->ltv==6?'selected':''; ?>>80% LTV</option>
							<option value="7" <?php echo (int)$fetch_lender_contact_type[0]->ltv==7?'selected':''; ?>>85% LTV</option>
							<option value="8" <?php echo (int)$fetch_lender_contact_type[0]->ltv==8?'selected':''; ?>>90% LTV</option>
						</select>
                	</div>
                    <div class="clearfix" style="margin-bottom:10px;" ></div> 
                    <div class="col-md-6">
                        <label>What is your Yield Requirement?</label> 
                    </div>
                    <div class="col-md-6">
                        <select class="selectpicker" name="yield_requirment" id="yield_requirment">
                            <option value="">Select One</option>
                            <option value="1" <?php echo (int)$fetch_lender_contact_type[0]->yield_requirment==1?'selected':''; ?>>Less than 6.0%</option>
                            <option value="2" <?php echo (int)$fetch_lender_contact_type[0]->yield_requirment==2?'selected':''; ?>>6.0%</option>
                            <option value="3" <?php echo (int)$fetch_lender_contact_type[0]->yield_requirment==3?'selected':''; ?>>6.5% </option>
                            <option value="4" <?php echo (int)$fetch_lender_contact_type[0]->yield_requirment==4?'selected':''; ?>>7.0%</option>
                            <option value="5" <?php echo (int)$fetch_lender_contact_type[0]->yield_requirment==5?'selected':''; ?>>7.5%</option>
                            <option value="6" <?php echo (int)$fetch_lender_contact_type[0]->yield_requirment==6?'selected':''; ?>>8.0%</option>
                            <option value="7" <?php echo (int)$fetch_lender_contact_type[0]->yield_requirment==7?'selected':''; ?>>8.5%</option>
                            <option value="8" <?php echo (int)$fetch_lender_contact_type[0]->yield_requirment==8?'selected':''; ?>>9.0%</option>
                            <option value="9" <?php echo (int)$fetch_lender_contact_type[0]->yield_requirment==9?'selected':''; ?>>9.5%</option>
                            <option value="10" <?php echo (int)$fetch_lender_contact_type[0]->yield_requirment==9?'selected':''; ?>>Greater than 9.0%</option>
                        </select>
                    </div>
                	<div class="clearfix" style="margin-bottom:10px;"></div>
                	<div class="col-md-6">
                		<label>What is your maximum loan position?</label> 
                	</div>
                	<div class="col-md-6">
                		<?php $position = explode(",", $fetch_lender_contact_type[0]->position); ?>
                		<select class="selectpicker" name="position">

                			<option value="">Select One</option>
                			<?php foreach ($position_optionn as $key => $row) {?>
                			<option value="<?php echo $key; ?>" <?php if ((int)$fetch_lender_contact_type[0]->position == $key) {echo "selected";}?>><?php echo $row; ?></option>
							<?php } ?> 
						</select>
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div>
                	<div class="col-md-6">
                		<label>Will you lend on Completion Value *?</label> 
                	</div>
                	<div class="col-md-6">
                		<select class="selectpicker" name="lend_value">
                			<option value="">Select One</option>
                			<option value="1" <?php echo (int)$fetch_lender_contact_type[0]->lend_value == 1?'Selected':''; ?>>Yes</option>
                			<option value="2" <?php echo (int)$fetch_lender_contact_type[0]->lend_value == 2?'Selected':''; ?>>No</option>
                		</select>
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div> 
                	<div class="col-md-6">
                		<label>What is your maximum Loan Term?</label> 
                	</div>
                	<div class="col-md-6">
                		<?php $LoanTerm = explode(",", $fetch_lender_contact_type[0]->LoanTerm); ?>
                		<select class="selectpicker" name="LoanTerm">
							<option value="">Select One</option>
							<?php foreach ($LoanTerm_type as $key => $row) {?>
							<option value="<?php echo $key; ?>" <?php if ($fetch_lender_contact_type[0]->LoanTerm == $key) {echo 'selected';}?>><?php echo $row; ?></option>
							<?php } ?> 
                		</select>
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div>  
                	<div class="col-md-6">
                		<label>What is your maximum investment per Trust Deed?</label> 
                	</div>
                	<div class="col-md-6">
                		<input type="text" name="maximum_income" class="amount_format selectpicker" value="<?php echo '$'.number_format((double)$fetch_lender_contact_type[0]->maximum_income); ?>">
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div>
                	
                	<div class="col-md-6">
                		<label>How much are you looking to invest in Trust Deeds?</label> 
                	</div>
                	<div class="col-md-6">
                		<input type="text" name="invest_per_deed" class="amount_format selectpicker" value="<?php echo '$'.number_format((double)$fetch_lender_contact_type[0]->invest_per_deed); ?>">
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div>
                	<div class="col-md-6">
                		<label>How many Trust Deeds have you funded?</label> 
                	</div>
                	<div class="col-md-6">
                		<input type="text" name="funded" class="amount_format selectpicker" value="<?php echo number_format((double)$fetch_lender_contact_type[0]->funded); ?>">
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div> 
                	<div class="col-md-6">
                		<label>Select all loan types you would consider:</label> 
                	</div>
                    <div class="col-md-6"></div>
                    <div class="clearfix" style="margin-bottom:10px;"></div> 
                    <div class="col-md-3"></div>
                	<?php $loan = explode(",", $fetch_lender_contact_type[0]->loan); ?>
                	<div class="col-md-3">
                		<input type="checkbox" name="loan[]" value="1" <?php if ((int)$loan[0] == 1) {echo "checked";}?>/><span>Bridge Loan**</span> 
                	</div>
                	<div class="col-md-3" style="padding-left:4px;margin-right: -6px;">
                		<input type="checkbox" name="loan[]" value="2" <?php if((int)$loan[1] == 2) {echo "checked";}?>/><span>Fix and Flip/Hold***</span> 
                	</div>
                	<div class="col-md-3" style="padding-left:4px">
                		<input type="checkbox" name="loan[]" value="3" <?php if((int)$loan[2] == 3) {echo "checked";}?>/><span>New Construction</span> 
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div>
                	<div class="col-md-6">
                		<label>Select all properties types you would consider:</label> 
                	</div>
                    <div class="col-md-6"></div>
                    <div class="clearfix" style="margin-bottom:10px;"></div> 
                    <div class="col-md-3"></div>
                	<div class="col-md-9 row">
                		<?php $lender_property = explode(",", $fetch_lender_contact_type[0]->property_type); ?>
                		<?php foreach ($lender_property_type as $key => $row) {?>
                    	<div class="col-md-4" style="padding-left:0px;margin-bottom:10px;">
                    		<input type="checkbox" name="property_type[]" class="form-control" value="<?php echo $key; ?>" <?php if (in_array($key, $lender_property)) {echo 'checked';}?>/><span><?php echo $row; ?></span> 
                    	</div>
                    	<?php }?>
                    </div>  
                </div>

				<div class="row" id="">
					<div class="col-md-12">
                    <p>* Completion Value refers to the value of the property once the renovations are complete. Fix and Flip / Hold loans are generally unwritten to the value of the property once the renovations are complete and may included a Renovation Hold Back for future renovation costs. </p>

                    <p>** Bridge Loan generally refers to loans in which the Borrower does not intend to renovate the property and/or the value is not based upon an After Repair Value. </p>

                    <p>*** Fix and Flip / Hold generally refers to loans in which the Borrower intends to renovate the property over the course of the loan term. The loan generally includes a Renovation Hold Back for future renovation costs and the Loan to Value ratio is based upon the Completion Value. </p>
                	</div>
                </div>

				<div class="row" id="">&nbsp;&nbsp;</div>

				<div class="row lender_data_row" id="">

					<h4 style="background-color:#efefef;padding: 10px;">General Information</h4>
					<div class="col-md-3">
						<label>Date of Birth</label>
						<input type="text" class="form-control lender_enabled input-md datepicker" name="lender_dob" value="<?php echo isset($fetch_lender_contact_type[0]->lender_dob) ? date('m-d-Y', strtotime($fetch_lender_contact_type[0]->lender_dob)) : ''; ?>" placeholder="MM-DD-YYYY" />
					</div>
				</div>



				<div class="row" id="">
					<h4 class="rowh4">Lender RE870 Data</h4>
					 
                	
                	<div class="col-md-2" style="margin-top: 8px;">
                		<label>RE870 complete</label> 
                	</div>
                	<div class="col-md-2">
                		<select name="re_complete" id="re_complete" class="selectpicker" onchange="fieldHideshow1();">
                            <?php foreach ($re885_complete_option as $key => $row) {?>
                                <option value="<?php echo $key; ?>" <?php if($key==$fetch_lender_contact_type[0]->re_complete){ echo "selected";}?>><?php echo $row; ?></option>
                            <?php }?>
                        </select>
                	</div>
                    <div class="col-md-2" style="margin-top: 8px;margin-left: 25px;">
                        <label>Date Completed</label> 
                    </div>
                    <div class="col-md-2">
                        <input type="text" id="lender_RE_date" class="RE_date_val form-control lender_enabled input-md datepicker" name="RE_date" value="<?php echo isset($fetch_lender_contact_type[0]->lender_RE_date) ? date('m-d-Y', strtotime($fetch_lender_contact_type[0]->lender_RE_date)) : ''; ?>" placeholder="MM-DD-YYYY" onchange="display_package_date_of_re(1);"/>
                    </div>
                    <div class="col-md-1" style="margin-top: 8px;"> 
                        <label>Status</label> 
                    </div>
                    <div class="col-md-2">
                        <?php 
                        $exipryStatus="";
                        $selectStyle="";
                        if($fetch_lender_contact_type[0]->re_complete=='2'){
                            $exipryStatus="Not Complete";
                            $selectStyle='style="width: 150px !important;color:black;"';
                        }else{
                            $lender_RE_date = $fetch_lender_contact_type[0]->lender_RE_date;
                            $date = date('d-m-Y');
                            $oneYearBefore = date('d-m-Y',strtotime(date("Y-m-d", mktime()) . " - 365 day"));
                            $diff = strtotime($oneYearBefore) - strtotime($lender_RE_date);
                            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                            
                            if($months==0){
                                $oneYearBeforeDay=date('d',strtotime($oneYearBefore));
                                $lender_RE_dateDay=date('d',strtotime($lender_RE_date));
                                if($oneYearBeforeDay<$lender_RE_dateDay){
                                    $exipryStatus="Current";
                                    $selectStyle='style="width: 150px !important;color:green;"';
                                }else{
                                    $exipryStatus="Expired";
                                    $selectStyle='style="width: 150px !important;color:red;"';
                                }
                            }else if($months>0){
                                $exipryStatus="Expired";
                                $selectStyle='style="width: 150px !important;color:red;"';
                            }else{
                                $exipryStatus="Current";
                                $selectStyle='style="width: 150px !important;color:green;"';
                            }
                        }
                        ?>
                        <select id="lender_RE_form_type" name="lender_RE_form_type" class="form-control" disabled="disabled" <?php echo $selectStyle;?>>
                            <option value="Not Complete" <?php if($exipryStatus=="Not Complete"){ echo "selected"; }?>>Not Complete</option>
                            <option value="Current"  <?php if($exipryStatus=="Current"){ echo "selected"; }?>>Current</option>
                            <option value="Expired" style="color:red" <?php if($exipryStatus=="Expired"){ echo "selected"; }?>>Expired</option>
                        </select>
                    </div>
                    <div class="clearfix" style="margin-bottom:10px;"></div>
                    


                    <?php
                    /*
                	<div class="col-md-4"> 
                		<label><b>Employment Information</b></label> 
                	</div>
                	<div class="col-md-6">
                		<select class="form-control" name="employment" id="employmentId" onchange="fieldHideshow()">
                			<option value="">Select One</option>
                			<option value="yes" <?php if ($fetch_lender_contact_type[0]->employment == 'yes') {echo 'selected';}?>>Yes</option>
                			<option value="no" <?php if ($fetch_lender_contact_type[0]->employment == 'no') {echo 'selected';}?>>No</option>
                			<option value="retired" <?php if ($fetch_lender_contact_type[0]->employment == 'retired') {echo 'selected';}?>>Retired</option>
                		</select>
                	</div>
                	 
                	<div class="clearfix" style="margin-bottom:10px;"></div>
                	<div class="col-md-4"> 
                		<label>Current Position</label>
                		<input type="text" name="current_position" value="<?php echo isset($fetch_lender_contact_type[0]->current_position) ? $fetch_lender_contact_type[0]->current_position : ''; ?>" class="form-control"> 
                	</div>
                	<div class="col-md-4">
                		<label>Title</label>
                		<input type="text" name="cp_title" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->cp_title) ? $fetch_lender_contact_type[0]->cp_title : ''; ?>">
                	</div>
                	<div class="col-md-4">
                		<label>Length of Current Position</label>
                		<input type="text" name="cp_length_of_position" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->cp_length_of_position) ? $fetch_lender_contact_type[0]->cp_length_of_position : ''; ?>">
                	</div>
                    */
                    ?>
                    <?php
                    $employment=$fetch_lender_contact_type[0]->employment;
                    $employment=explode(",", $employment);

                    $cp_title=$fetch_lender_contact_type[0]->cp_title;
                    $cp_title=explode(",", $cp_title);

                    $cp_length_of_position=$fetch_lender_contact_type[0]->cp_length_of_position;
                    $cp_length_of_position=explode(",", $cp_length_of_position);

                    $current_position=$fetch_lender_contact_type[0]->current_position;
                    $current_position=explode(",", $current_position);
                    ?>
                    <div class="col-md-12"> 
                        <label><b>Employment History</b></label> 
                    </div>
                   <!--  Employment Information Row-1 -->
                    <div class="col-md-3"> 
                        <label>Industry</label>
                        <input type="text" name="current_position[]" value="<?php if(!empty($current_position[0])){ echo $current_position[0]; }?>" class="form-control"> 
                    </div>
                    <div class="col-md-3">
                        <label>Title</label>
                        <input type="text" name="cp_title[]"   value="<?php if(!empty($cp_title[0])){ echo $cp_title[0]; }?>" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <label>Length of Position (Years)</label>
                        <input type="text" name="cp_length_of_position[]" value="<?php if(!empty($cp_length_of_position[0])){ echo $cp_length_of_position[0]; }?>" class="form-control" >
                    </div>
                    <div class="col-md-3" style="margin-top: 25px">
                        <input type="checkbox" name="employment_first" value="1" class="form-control" <?php if(!empty($employment[0]) && $employment[0]=="1"){ echo "checked"; }?>>Retired 
                    </div>
                    <!--  Employment Information Row-2 -->
                    <div class="clearfix" style="margin-bottom:10px;"></div>
                    <div class="col-md-3"> 
                        <label>Industry</label>
                        <input type="text" name="current_position[]" value="<?php if(!empty($current_position[1])){ echo $current_position[1]; }?>" class="form-control"> 
                    </div>
                    <div class="col-md-3">
                        <label>Title</label>
                        <input type="text" name="cp_title[]"   value="<?php if(!empty($cp_title[1])){ echo $cp_title[1]; }?>" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <label>Length of Position (Years)</label>
                        <input type="text" name="cp_length_of_position[]" value="<?php if(!empty($cp_length_of_position[1])){ echo $cp_length_of_position[1]; }?>" class="form-control" >
                    </div>
                    <div class="col-md-3" style="margin-top: 25px">
                        <input type="checkbox" name="employment_secound" value="1" class="form-control" <?php if(!empty($employment[1]) && $employment[1]=="1"){ echo "checked"; }?>>Retired 
                    </div>
                    <!--  Employment Information Row-2 END-->
                	<div class="clearfix" style="margin-bottom:10px;"></div>
                	<div class="col-md-4" style="display:none;">
                		<label>Previous Position: </label>
                		<input type="text" name="cp_previous_position" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->cp_previous_position) ? $fetch_lender_contact_type[0]->cp_previous_position : ''; ?>">
                	</div>
                	<div class="clearfix" style="margin-bottom:10px;"></div>

                    <div class="col-md-12"> 
                        <label><b>Education Information</b></label> 
                    </div>
                	<div class="col-md-4"> 
                		<label>Highest Year Completed</label>
                        <select name="highest_year_completed" id="highest_year_completed" class="form-control">
                            <option value="" >Select One</option>
                            <option value="Grade School" <?php if(!empty($fetch_lender_contact_type[0]->highest_year_completed) && $fetch_lender_contact_type[0]->highest_year_completed=="Grade School"){ echo "selected"; }?>>Grade School</option>
                            <option value="High School" <?php if(!empty($fetch_lender_contact_type[0]->highest_year_completed) && $fetch_lender_contact_type[0]->highest_year_completed=="High School"){ echo "selected"; }?>>High School</option>
                            <option value="Associates Degree" <?php if(!empty($fetch_lender_contact_type[0]->highest_year_completed) && $fetch_lender_contact_type[0]->highest_year_completed=="Associates Degree"){ echo "selected"; }?>>Associates Degree</option>
                            <option value="Bachelors" <?php if(!empty($fetch_lender_contact_type[0]->highest_year_completed) && $fetch_lender_contact_type[0]->highest_year_completed=="Bachelors"){ echo "selected"; }?>>Bachelors</option>
                            <option value="Masters" <?php if(!empty($fetch_lender_contact_type[0]->highest_year_completed) && $fetch_lender_contact_type[0]->highest_year_completed=="Masters"){ echo "selected"; }?>>Masters</option>
                            <option value="Doctorate" <?php if(!empty($fetch_lender_contact_type[0]->highest_year_completed) && $fetch_lender_contact_type[0]->highest_year_completed=="Doctorate"){ echo "selected"; }?>>Doctorate</option>
                        </select>
                		<!-- <input type="text" name="highest_year_completed" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->highest_year_completed) ? $fetch_lender_contact_type[0]->highest_year_completed : ''; ?>"> --> 
                	</div>
                	<div class="col-md-4">
                		<label>Year of Graduation</label>
                		<input type="text" name="year_of_graduation" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->year_of_graduation) ? $fetch_lender_contact_type[0]->year_of_graduation : ''; ?>">
                	</div>
                	<div class="col-md-4">
                		<label>Degree / Diploma</label>
                		<input type="text" name="degree_diploma" class="form-control" value="<?php echo isset($fetch_lender_contact_type[0]->degree_diploma) ? $fetch_lender_contact_type[0]->degree_diploma : ''; ?>">
                	</div>
				</div>


				<div class="row lender_data_row" id="">

					<h4 class="rowh4">Financial Situation</h4>

					<div class="col-md-6" style="margin-bottom: 5px;">
						<label>Estimated Annual Income</label>
					</div></br><br>
					<?php foreach ($lender_financial_situation as $key => $row) {?>
					<div class="col-md-6">

							<input type="checkbox" value="<?php echo $key; ?>" onchange="financial_value(this);" class="form-control" <?php if ($key == $fetch_lender_contact_type[0]->financial_situation) {echo 'Checked';}?>/><?php echo $row; ?>

							<input type="hidden" name="financial_situation" value="<?php echo isset($fetch_lender_contact_type[0]->financial_situation) ? $fetch_lender_contact_type[0]->financial_situation : ''; ?>" id="financial_situation_value" />

					</div>
					<?php if($key%2==0){ ?>
						<div class="clearfix" style="margin-bottom:10px;"></div>
					<?php } }?>

					<div class="row"><br>&nbsp;</div>

					<div class="col-md-6" style="margin-bottom: 5px;">
						<label>Estimated Net Worth</label>
					</div></br><br>
					<?php foreach ($lender_financial_situation as $key => $row) {?>
					<div class="col-md-6">

							<input type="checkbox" value="<?php echo $key; ?>" onchange="net_worth_financial_value(this);" class="form-control" <?php if ($key == $fetch_lender_contact_type[0]->net_worth_financial_situation) {echo 'Checked';}?>/><?php echo $row; ?>

							<input type="hidden" name="net_worth_financial_situation" value="<?php echo isset($fetch_lender_contact_type[0]->net_worth_financial_situation) ? $fetch_lender_contact_type[0]->net_worth_financial_situation : ''; ?>" id="net_worth_financial_situation_value" />

					</div>
					<?php if($key%2==0){ ?>
						<div class="clearfix" style="margin-bottom:10px;"></div>
					<?php } }?>

					<div class="row"><br>&nbsp;</div>

					<div class="col-md-6" style="margin-bottom: 5px;">
						<label>Estimated Liquid Assets</label>
					</div></br><br>
					<?php foreach ($lender_financial_situation as $key => $row) { ?>
					<div class="col-md-6">

							<input type="checkbox" value="<?php echo $key; ?>" onchange="liquid_financial_value(this);" class="form-control" <?php if ($key == $fetch_lender_contact_type[0]->liquid_financial_situation) {echo 'Checked';}?>/><?php echo $row; ?>

							<input type="hidden" name="liquid_financial_situation" value="<?php echo isset($fetch_lender_contact_type[0]->liquid_financial_situation) ? $fetch_lender_contact_type[0]->liquid_financial_situation : ''; ?>" id="liquid_financial_situation_value" />

					</div>

					<?php if($key%2==0){ ?>
						<div class="clearfix" style="margin-bottom:10px;"></div>
					<?php } }?>

				</div>
				
				<!-- <div class="row lender_data_row" id="">
					<div class="col-md-9" style="width:100%;">
						<textarea class="form-control" name="textarea_value" rows="3"><?php //echo isset($fetch_lender_contact_type[0]->textarea_value) ? $fetch_lender_contact_type[0]->textarea_value : ''; ?></textarea>
					</div>
				</div> -->
				<div class="row">
					<div class="col-md-6" style="margin-bottom: 5px;">
						<label>Source of Income and Cash Resources</label>

						<!-- <input type="text" name="source_income_cash" class="form-control" value=""> -->
					</div>
					<div class="col-md-12" style="margin-bottom: 5px;">
						<textarea id="source_income_cash" class="form-control"  name = "source_income_cash" maxlength="5000" rows="3" ><?php echo !empty($fetch_lender_contact_type[0]->source_income_cash)?$fetch_lender_contact_type[0]->source_income_cash:''; ?></textarea>
					</div>
				</div>
				<div class="clearfix" style="margin-bottom:10px;"></div>
				<div class="row lender_data_row" id="">

					<div class="col-md-6" style="margin-bottom: 5px;">
						<label>Liquidity Needs</label>
					</div></br>
					<?php foreach ($lender_liquidity_needs as $key => $row) { ?>
					<div class="col-md-6 checkId" style="width:68%;">
						<input type="checkbox" value="<?php echo $key; ?>" onchange="liquidity_needs_value(this);" <?php if ($key == $fetch_lender_contact_type[0]->liquidity_needs_checkbox_value) {echo 'Checked';}?> /><span><?php echo $row; ?></span>

						<input type="hidden" name="liquidity_needs_checkbox_value" value="<?php echo isset($fetch_lender_contact_type[0]->liquidity_needs_checkbox_value) ? $fetch_lender_contact_type[0]->liquidity_needs_checkbox_value : '' ?>" id="lender_liquidity_needs_chkbx" />
					</div></br>
					<div class="clearfix" style="margin-bottom:10px;"></div>
					<?php } ?>

				</div>
				
				<div class="row" id="">

					<h4 class="rowh4">Investment Experience</h4>

					<?php 
					$a = 1;
                    $other_description_status="";
                    $explobox = explode(',', $fetch_lender_contact_type[0]->invest_ex_checkbox);

                    $explobox_year = explode(',', $fetch_lender_contact_type[0]->invest_yrs_exp);
                    
                    $y = 0;
                    foreach($lender_investment_experience as $key => $row) {

                    	if(count($lender_investment_experience) != $a){
                    ?>
							<div class="col-md-5 checkId1">
								<input type="checkbox" <?php if (in_array($key, $explobox)) {echo 'Checked';}?> name="investment_experience_checkbox[]" value="<?php echo $key; ?>"><?php echo $row; ?>

							</div>
							<?php if($key == 1){ ?>
							<div class="col-md-7" style="margin-bottom: 30px;">
							

							</div>
							<?php } else { ?> 
							<div class="col-md-6 row">

								<div class="col-md-2">Years</div> 
								<div class="col-md-10"><input type="number" class="RE_date_val form-control lender_enabled input-md " name="invest_yrs_exp[]" value="<?php echo $explobox_year[$y]; ?>" placeholder="YYYY" on/></div>

							</div>
							<?php $y++; } ?>
					<?php } else{ ?>
						<div class="col-md-4 checkId1">
                                <?php
                                if(strtolower($row)=="other"){
                                    if (in_array($key, $explobox)){
                                        $other_description_status='disabled="disabled"';
                                    }
                                    ?>
                                     <input type="checkbox" id="investment_experience_checkbox_other" <?php if (in_array($key, $explobox)) {echo 'Checked';}?> name="investment_experience_checkbox[]" value="<?php echo $key; ?>" onchange="get_checked_other()"><?php echo $row; ?>
                                    <?php
                                }else{
                                    ?>
                                     <input type="checkbox" <?php if (in_array($key, $explobox)) {echo 'Checked';}?> name="investment_experience_checkbox[]" value="<?php echo $key; ?>"><?php echo $row; ?>
                                    <?php
                                }
								?>
						</div>
						<div class="col-md-4 row">
								<div class="col-md-3">Years</div> 
								<div class="col-md-9"><input type="number" class="RE_date_val form-control lender_enabled input-md " name="invest_yrs_exp[]" value="<?php echo $explobox_year[$y]; ?>" placeholder="YYYY" on/></div>
						</div>
						<div class="col-md-4">
							<div class="col-md-3">Specific</div> 
							<div class="col-md-9"><input type="text" name="specific" class="form-control" value="<?php echo (!empty($fetch_lender_contact_type[0]->specific)?$fetch_lender_contact_type[0]->specific : ''); ?>"></div>
						</div>

					<?php }?>
					<div class="clearfix" style="margin-bottom:10px;"></div>
					<?php $a++; } ?>


				</div>
			</div>
            <div class="clearfix" style="margin-bottom:10px;"></div>
            <div class="row" id="">
                <div class="col-md-6" style="margin-bottom: 5px;">
                    <label>Other Description</label>
                </div>
                <div class="col-md-12" style="margin-bottom: 5px;">
                    <textarea id="other_description" class="form-control"  name = "other_description" maxlength="5000" rows="3" <?php echo $other_description_status;?>><?php echo !empty($fetch_lender_contact_type[0]->other_description)?$fetch_lender_contact_type[0]->other_description:''; ?></textarea>
                </div>
            </div>
            <div class="clearfix" style="margin-bottom:10px;"></div>
            <div class="row" id="">

                <div class="col-md-6" style="margin-bottom: 5px;">
                    <label>Investment experience in notes secured by trust deeds</label>
                </div>
                <div class="col-md-6" style="margin-bottom: 5px;">
                    <div class="col-md-6" style="margin-bottom: 5px;">
                        <div class="col-md-3" >Years: </div>
                        <div class="col-md-9" style="margin-bottom: 5px;">
                            <input type="number" class="RE_date_val form-control lender_enabled input-md " name="investment_experience_year" id="investment_experience_year" value="<?php echo !empty($fetch_lender_contact_type[0]->investment_experience_year)?$fetch_lender_contact_type[0]->investment_experience_year:''; ?>" placeholder="YYYY" on/>
                        </div>
                    </div>
                    <div class="col-md-6" style="margin-bottom: 5px;">
                        <div class="col-md-3" >Months: </div>
                        <div class="col-md-9" style="margin-bottom: 5px;">
                            <input type="text" name="investment_experience_month" id="investment_experience_month" class="form-control" value="<?php echo !empty($fetch_lender_contact_type[0]->investment_experience_month)?$fetch_lender_contact_type[0]->investment_experience_month:''; ?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix" style="margin-bottom:10px;"></div>
            <div class="row" id="">
                <div class="col-md-12" style="margin-bottom: 5px;">
                    <label>Number of previous investments in notes secured by Trust Deeds or real property sales contracts</label>
                   
                </div>
                <div class="col-md-12" style="margin-bottom: 5px;">
                     <textarea id="number_previous_investments" class="form-control"  name = "number_previous_investments" maxlength="5000" rows="3" ><?php echo !empty($fetch_lender_contact_type[0]->number_previous_investments)?$fetch_lender_contact_type[0]->number_previous_investments:''; ?></textarea>
                </div>
            </div>
            <div class="clearfix" style="margin-bottom:10px;"></div>
            <div class="row" id="">
                <div class="col-md-6" style="margin-bottom: 5px;">
                    <label>Other Considerations</label>
                </div>
                <div class="col-md-12" style="margin-bottom: 5px;">
                    <textarea id="other_considerations" class="form-control"  name = "other_considerations" maxlength="5000" rows="3" ><?php echo !empty($fetch_lender_contact_type[0]->other_considerations)?$fetch_lender_contact_type[0]->other_considerations:''; ?></textarea>
                </div>
            </div>
            <div class="clearfix" style="margin-bottom:10px;"></div>
            <div class="row" id="">
                <div class="col-md-6" style="margin-bottom: 5px;">
                    <label>Investment Objective</label>
                </div>
                <div class="col-md-12" style="margin-bottom: 5px;">
                    <textarea id="investment_objective_data" class="form-control"  name = "investment_objective_data" maxlength="5000" rows="3" ><?php echo !empty($fetch_lender_contact_type[0]->investment_objective_data)?$fetch_lender_contact_type[0]->investment_objective_data:''; ?></textarea>
                </div>
            </div>
			<div class="modal-footer">
				<input type="hidden" name="popup_data" value="lender-data">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<button type="submit"  name="modal_btn" value="save" class="btn blue">Save</button>
				<a href="<?php echo base_url(); ?>lender_data_print/<?php echo $contact_id; ?>" class="btn blue">Print</a>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

					
							<!---  Contact Tags modal  Starts---->

							<div class="modal fade bs-modal-lg" id="contact_tags" tabindex="-1" role="large" aria-hidden="true">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Contact Tags:</h4>
										</div>
										<?php

											foreach (array_values(array_filter($fetch_contact_type)) as $con_type) {
												$aa[$con_type] = $con_type;
											}
											foreach (array_values(array_filter($fetch_contact_specialty)) as $con_specialty) {
												$bb[$con_specialty] = $con_specialty;
											}

											foreach (array_values(array_filter($fetch_contact_email_blast)) as $con_email_blast) {
												$dd[$con_email_blast] = $con_email_blast;
											}

											foreach ($fetch_contact_permission as $con_permission) {
												$ee[$con_permission] = $con_permission;
											}

										?>

										<div class="modal-body">
											<div class="row ">
												<div class="col-md-3">
													<label style=""><b>Specialty</b></label>
												</div>
											</div>
											<div class="row contact_tags_row">
												<?php

												foreach ($contact_specialty as $key => $specialty) {

													if ($bb[$key] == $key) {
														$checked = 'checked="checked"';
													} else {
														$checked = '';
													}
													?>
													<input type="hidden" value="<?php echo $fetch_contact_specialty_tag_id[$key]; ?>" name="specialty_tag_id[]"/>
													<div class="col-md-3">
														<input type="checkbox" <?php echo $checked; ?> class="form-control" name="contact_specialty[]" value="<?php echo $key; ?>"/>&nbsp;&nbsp;<?php echo $specialty; ?>
													</div>

												<?php	}?>


											</div>

											<div class="row ">
												<div class="col-md-3">
													<label style=""><b>E-Mail Blast</b></label>
												</div>
											</div>
											<div class="row contact_tags_row">
												<?php

                                                    foreach ($contact_email_blast as $key => $email_blast) {
                                                    	if ($dd[$key] == $key) {
                                                    		$checked = 'checked="checked"';
                                                    	} else {
                                                    		$checked = '';
                                                    	}
                                                    	?>
													<input type="hidden" value="<?php echo $fetch_contact_email_blast_tag_id[$key]; ?>" name="email_blast_tag_id[]"/>
													<div class="col-md-8">
														<input type="checkbox" <?php echo $checked; ?> class="form-control" name="contact_email_blast[]" value="<?php echo $key; ?>"/>&nbsp;&nbsp;<?php echo $email_blast; ?>
													</div>

												<?php	}?>


											</div>

											
										</div>

										<div class="modal-footer">
											<button type="button" class="btn default" data-dismiss="modal">Close</button>
											<button type="submit"  class="btn blue">Save</button>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>

							<!--- Contact Tags modal  End-->

							<!---  Assigned modal   Starts---->
							<div class="modal fade bs-modal-lg" id="assigned" tabindex="-1" role="large" aria-hidden="true">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Permissions</h4>
										</div>

										<div class="modal-body">
											<?php

                                                $fetch_contact_permission = unserialize($fetch_sql_assigned[0]->contact_permission);

                                                foreach ($fetch_contact_permission as $con_permission) {
                                                	$ee[$con_permission] = $con_permission;
                                                }

                                                ?>
											<input type="hidden" value="<?php echo $fetch_sql_assigned[0]->id; ?>" name="assigned_id"/>
											<div class="modal-body">

												<div class="row ">
													<div class="col-md-12">
														<label style=""><b>Permissions (only allows that selected person/group to view History and Statistics]</b></label>
													</div>
												</div>
												<div class="row contact_tags_row">
													<?php

                                                        foreach ($contact_permission as $key => $permission) {
                                                        	if ($ee[$key] == $key) {
                                                        		$checked = 'checked="checked"';
                                                        	} else {
                                                        		$checked = '';
                                                        	}

                                                        	?>
														<div class="col-md-8">
															<input id="checkbx_<?php echo $key; ?>" onclick="check_permission_option(this.value,<?php echo $key; ?>)" type="checkbox" <?php echo $checked; ?> class="form-control" name="contact_permission[]" value="<?php echo $key; ?>"/>&nbsp;&nbsp;<?php echo $permission; ?>
														</div>



													<?php	}?>
													<div class="col-md-8 particular_user_dropdown" style="display:none;">
														<select name="particular_user_id" id="particular_user_id">
															<option value="">Select User</option>
															<?php
                                                                if ($all_users) {
                                                                	foreach ($all_users as $key => $username) {

                                                                		if ($fetch_sql_assigned[0]->particular_user_id == $username->id) {
                                                                			$selected = 'selected';
                                                                		} else {

                                                                			$selected = '';
                                                                		}
                                                                		echo '<option ' . $selected . ' value = ' . $username->id . '>' . $username->fname . ' ' . $username->lname . '</option>';
                                                                	}
                                                                }
                                                                ?>
														</select>
													</div>
												</div>
											</div>
										</div>

										<div class="modal-footer">
											<button type="button" class="btn default" data-dismiss="modal">Close</button>
											<button type="submit"  class="btn blue">Save</button>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>

							<!--- Assigned modal  End ---->

                        </form>

							<!---  Contact Document modal   Starts---->

<div class="modal fade bs-modal-lg" id="contact_document" tabindex="-1" role="large" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
        
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Contact Document<?php echo isset($contact_id) ? ': ' . $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname : ''; ?></h4>
			</div>

			<div class="modal-body">
				<div class="doc-div">
					
					<div id="ct_Documentsadd" class="row">						
						<div class="col-md-2">&nbsp;</div>
						<div class="col-md-8">
							<div class="ct_loan_saved_document" style="display: none;">	
								<form action="<?php echo base_url();?>Contact/upload_new_pdfs" method="POST" enctype="multipart/form-data" name="forms_upload_new_pdfs" id="forms_upload_new_pdfs">
									<input type="hidden" name="contact_id" value="<?php echo isset($contact_id) ? $contact_id : ''; ?>" >

				                    <input type="hidden" name="first_name" value="<?php echo $fetch_contact_data[0]->contact_firstname; ?>" >
				                    <input type="hidden" name="middle_name" value="<?php echo $fetch_contact_data[0]->contact_middlename; ?>" >
				                    <input type="hidden" name="last_name" value="<?php echo $fetch_contact_data[0]->contact_lastname; ?>" >

				                    <div class="form-group">
										<label for="document_name">Document Name:</label>
										<input type="text" class="form-control" id="document_name" name="document_name" required>
									</div>
									<div class="form-group">
										<div id="ad">
											<label>Select Document:</label>
											<input type="file" id = "borrower_upload_1" name ="contact_upload_doc[]" class="select_images_file" >
										</div>
									</div>

									<button type="submit"  class="btn blue">Save</button>
								</form>
							</div>
						</div>
						<div class="col-md-2">&nbsp;</div>
					</div>
					<br>


					<div class="col-md-12 ct_Documentsadd_table">
						
						<table class="table table-responsive" id="loan_doc_table" style="width: 100% !important;">
							<thead>
								<tr>
									<th>Action</th>
									<th>Name of Document</th>
									<th>Uploaded Date</th>
									<th>User</th>
									<th>View</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if($fetch_contact_document){
									foreach($fetch_contact_document as $contactDocuments) {

									$FileNameGet = '';
									
									$UploadedDate = date("m-d-Y", strtotime($contactDocuments->created_at));
									
									$filename = $contactDocuments->contact_document;
									$FileArr = explode('/', $filename);
									$FileNameGet = $FileArr[count($FileArr)-1];

									$file1 = explode('/', $FileNameGet);
									$file2 = explode('.', $file1[count($file1)-1]);

									$FileNameGet = $NameUploadedcalss = $file2[0];

									if($contactDocuments->document_name){
										$NameUploaded = $contactDocuments->document_name;
									}else{
										$NameUploaded = $FileNameGet;
									}


									$up_user_id = $contactDocuments->user_id;
									$UpUserDate = $this->User_model->query("SELECT * FROM user WHERE id = '$up_user_id'")->row();
									if($UpUserDate){
										$UploadedUser = $UpUserDate->fname.' '.$UpUserDate->lname;
									}
								?>
								<tr attrremove="<?php echo $FileNameGet; ?>">
									<td>
										<a><i id="<?php echo $contactDocuments->id; ?>" onclick="delete_doc(this);" title = "Delete File" class = "fa fa-trash"  aria-hidden="true"></i></a>
									</td>
									<td>
										<?php echo $NameUploaded; ?>
									</td>
									<td><?php echo $UploadedDate; ?></td>
									<td><?php echo $UploadedUser; ?></td>
									<td>
										<a href="<?php echo aws_s3_document_url($contactDocuments->contact_document); ?>" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a>
									</td>
								</tr>
								<?php
									}
								}
								?>

							</tbody>
						</table>
					</div>

				</div>
					
			</div>

			<div class="modal-footer">
				<button type="button" id="ct_loan_saved_documentadd" class="btn btn-primary">Add Document</button>
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
						<!---  Contact Document modal  End ---->
						<!---
							Description : This popup use for add and update marketing material data - add one field requested_by 
							Author      : Bitcot
							Created     : 
							Modified    : 06-04-2021
						---->

						<!---  Marketing Data modal   Starts---->
							<div class="modal fade" id="Marketing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							  <div class="modal-dialog modal-lg" role="document">
							    <div class="modal-content">
							    <form method="POST" action="<?php echo base_url();?>Contact_marketing/Contactmarketingdata" name="forms_Contactmarketingdata" id="forms_Contactmarketingdata">
							      <div class="modal-header">
							        <input type="hidden" name="contact_id_page" value="<?php echo $contact_id;?>">
							        <h4 class="modal-title">Contact Marketing<?php echo isset($contact_id) ? ': ' . $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname : ''; ?></h4>
							    	
							        <div class="row">
											<h3 style="background: #eeeeee;padding: 8px 10px;font-size: 17px;font-weight: 500;">Marketing Material</h3>
									</div>
							       
							      </div>
							      <div class="modal-body">
							      	<div id="marktingdata">

							      		<?php

							      		$count = 0;
							      		 if(isset($contatctmarketing) && is_array($contatctmarketing)){ 
							      			foreach($contatctmarketing as $row){ 

							      				$count++;
							      				if($count == '1'){
							      					$style = '';
							      				}else{
							      					$style = 'style="visibility: hidden"';
							      				}
							      			?>

							      				<input type="hidden" name="rowid[]" value="<?php echo $row->id;?>">
							      				<div class="row">
							      					<div class="col-md-1">
									        			<label>Action:</label>
									        			<a title="Remove" onclick="removedata('<?php echo $row->id;?>');"><i class="fa fa-trash"></i></a>
									        		</div>
									        		<div class="col-md-2">
									        			<label <?php echo $style;?>>Status:</label>
									        			<select class="form-control" name="title_status[]">
									        				<?php foreach($contact_marketing_status_option as $key => $rowss){ ?>

									        					<option value="<?php echo $key;?>" <?php if($key == $row->title_status){ echo 'Selected';}?>><?php echo $rowss;?></option>

									        				<?php } ?>
									        			</select>
									        		</div>
									        		<div class="col-md-2">
									        			<label <?php echo $style;?>>Date:</label>
									        			<input type="text" name="date[]" class="form-control datepicker" placeholder="Enter Date" value="<?php echo date('m-d-Y', strtotime($row->date));?>">
									        		</div>
									        		<div class="col-md-2">
									        			<label <?php  echo $style;?>><?php echo $rowContact->requested_by; ?>Requested By:</label>
									        			
									        			<select class="form-control" name="contact_id[]">
									        				<?php foreach($fetch_all_contact as $key => $rowContact){ ?>

									        					<option value="<?php echo $rowContact->contact_id;?>" <?php if($rowContact->contact_id == $row->requested_by){ echo 'Selected';}?>><?php echo $rowContact->contact_firstname . ' ' . $rowContact->contact_lastname ;?></option>

									        				<?php } ?>
									        			</select>
									        		</div>
									        		<div class="col-md-2">
									        			<label <?php echo $style;?>>Item:</label>
									        			<input type="text" name="swag[]" class="form-control" placeholder="Enter Swag" value="<?php echo $row->swag;?>">
									        		</div>
									        		

									        		<div class="col-md-3">
									        			<label <?php echo $style;?>>Note:</label>
									        			<textarea type="text" name="note[]" class="form-control" rows="2" placeholder="Enter Note..."><?php echo $row->note;?></textarea>
									        		</div>
									        	</div>


							      		<?php } }else{ ?>

								      		<input type="hidden" name="rowid[]" value="new">
								        	<div class="row">
								        		<div class="col-md-1">
								        			<label>Action:</label>
								        		</div>
								        		<div class="col-md-2">
								        			<label>Status:</label>
								        			<select class="form-control" name="title_status[]">
								        				<?php foreach($contact_marketing_status_option as $key => $row){ ?>
								        					<option value="<?php echo $key;?>"><?php echo $row;?></option>
								        				<?php } ?>
								        			</select>
								        		</div>
								        		<div class="col-md-2">
								        			<label>Date:</label>
								        			<input type="text" name="date[]" class="form-control datepicker" value="" placeholder="Enter Date">
								        		</div>
								        		<div class="col-md-2">
									        			<label>Requested By:</label>
									        			<select class="form-control" name="contact_id[]">
									        				<?php foreach($fetch_all_contact as $key => $rowContact){ ?>

									        					<option value="<?php echo $rowContact->contact_id;?>"><?php echo $rowContact->contact_firstname . ' ' . $rowContact->contact_lastname ;?></option>

									        				<?php } ?>
									        			</select>
									        		</div>
								        		<div class="col-md-2">
								        			<label>Item:</label>
								        			<input type="text" name="swag[]" class="form-control" value="" placeholder="Enter Swag" >
								        		</div>
								        		<div class="col-md-3">
								        			<label>Note:</label>
								        			<textarea type="text" name="note[]" class="form-control" rows="2" placeholder="Enter Note..."></textarea>
								        		</div>

								        		

								        		
								        	</div>

								        <?php } ?>
							        </div>

							        	<div class="row">&nbsp;</div>
							        	<div class="row">
							        		<div class="col-md-12">
							        			<a onclick="addmoreswag();" class="btn btn-primary">Add</a>
							        		</div>
							        	</div>

							      </div>
							      <div class="modal-footer">
							        	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							        	<button type="sybmit" name="submit" class="btn btn-primary">Save</button>
							      </div>
							  	</form>
							    </div>
							  </div>
							</div>

							<script type="text/javascript">
								
								function addmoreswag(){

									var count = $('#Marketing #marktingdata .row').length;
									
									count++;

									$('#Marketing #marktingdata').append('<input type="hidden" name="rowid[]" value="new"><div class="row" id="row_'+count+'"><div class="col-md-1"><label style="visibility:hidden;">Action:</label><a title="Remove" onclick="remove_row('+count+');"><i class="fa fa-trash"></i></a></div><div class="col-md-2"><label style="visibility:hidden;">Status:</label><select class="form-control" name="title_status[]"><?php foreach($contact_marketing_status_option as $key => $row){ ?><option value="<?php echo $key;?>"><?php echo $row;?></option><?php } ?></select></div><div class="col-md-2"><label style="visibility:hidden;">Date:</label><input type="text" placeholder="Enter Date" name="date[]" class="form-control datepicker" value=""></div><div class="col-md-2"><label style="visibility:hidden;">Requested By:</label><select class="form-control" name="contact_id[]">  <?php $contactLenth = count($fetch_all_contact); foreach($fetch_all_contact as $key => $rowContact){ ?><option value="<?php echo $rowContact->contact_id;?>"><?php echo str_replace("'", '', $rowContact->contact_firstname) . ' ' . str_replace("'", '', $rowContact->contact_lastname) ;?></option> <?php if($contactLenth==$key){break;}} ?>  </select></div><div class="col-md-2"><label style="visibility:hidden;">Item:</label><input type="text" name="swag[]" placeholder="Enter Swag" class="form-control" value=""></div><div class="col-md-3"><label style="visibility:hidden;">Note:</label><textarea type="text" placeholder="Enter Note..." name="note[]" class="form-control" rows="2"></textarea></div></div>');

									$(".datepicker").datepicker({ dateFormat: 'mm-dd-yy' });
								}

								function remove_row(rowid){

									$('#Marketing #marktingdata #row_'+rowid).remove();
								}

								function removedata(id){

									//alert(id);
									if(confirm('Are you sure to remove this?')){

										$.ajax({
													type : 'POST',
													url  : '<?php echo base_url()."Contact_marketing/removerow"?>',
													data : {'id':id},
													success : function(response){

														alert('Row removed successfully');
														window.location.reload();
													}

										});
									}
								}

							</script>
						<!------------  Marketing Data modal End ------------------------------------>

						<hr style="width:97%;margin-left:15px;float: left;">

							<div class="row" id="model_left_align">
								
								<div class="col-md-12">&nbsp;</div>
								<div class="col-md-12">
									<a class="btn btn-primary borrower_data_pop" data-toggle="modal" href="#borrower_data" style="width: 55% !important;">Borrower Data </a>
								</div>
								
								<div class="col-md-12">&nbsp;</div>
								<div class="col-md-12">
                                <?php
                                  
                                    if (in_array('16', $fetch_contact_specialty)) {

                                        echo '<a class="btn btn-primary" onclick="load_modal(this)"  style="width: 55% !important;">Lender Data </a>';
                                    } else {
                                        
                                        echo '<a class="btn btn-primary"  style="width: 55% !important;" disabled>Lender Data </a>';
                                    }
                                ?>
									
								</div>
								<div class="col-md-12">&nbsp;</div>
								<div class="col-md-12">
									<a class="btn btn-primary" data-toggle="modal" href="#BrokerData" style="width: 55% !important;">Broker Data</a>
								</div>
                                <div class="col-md-12">&nbsp;</div>
                                <div class="col-md-12">
                                    <a class="btn btn-primary" data-toggle="modal" href="#hardmoney" style="width: 55% !important;">Hard Money Program</a>
                                </div>
                                <div class="col-md-12">&nbsp;</div>
                                <div class="col-md-12">
                                    <a class="btn btn-primary" data-toggle="modal" href="#Marketing" style="width: 55% !important;">Marketing</a>
                                </div>
								
								<div class="col-md-12">&nbsp;</div>

								<div class="col-md-12">&nbsp;</div>
								<!-- 
									Description : Rename "Add Documents" to "Saved Documents"
									Author      : Bitcot
									Created     : 
									Modified    : 25-03-2021
								 -->
								<div class="col-md-12">

									<a class="btn  btn-primary" data-toggle="modal" href="#contact_document" style="width: 55% !important;">Contact Documents</a>
								</div>
							</div>
						


							<div class="col-md-12 "><br><br></div>

                            <div class="col-md-12 ">
								<label style="font-weight:600 !important;">Contact Tasks:</label>

								<?php
                              
                                if (isset($fetch_contact_task)) {

                                	foreach ($fetch_contact_task as $key => $row) {
                                		?>

									<div class="relation_labels" id="contact_task_<?php echo $row->id; ?>">

									<div class="col-md-12">
								    <label>
                                    <strong>Task Type: </strong> <a href="<?php echo base_url() . 'Contact/task_view/' . $row->contact_id . '/' . $row->id; ?>"><?php echo is_numeric($row->contact_task) ? $selct_contact_task[$row->contact_task] : $row->contact_task; ?></a></label>
											</div>

											<div class="col-md-12">
												<label><strong>Date: </strong> <?php echo date('m-d-Y', strtotime($row->contact_date)); ?></label>
											</div>
											<div class="col-md-12">
												<label><strong>Note: </strong> <?php echo $row->task_notes; ?></label>
											</div>
										

											<div class="col-md-12">
												<a class="col-md-1" title="Edit" id="<?php echo $row->id; ?>" onclick="edit_contact_task(this.id);"><i class="fa fa-edit" aria-hidden="true"></i></a>

												
												<a class="col-md-1" title="Delete" id="<?php echo $row->id; ?>" onclick="delete_contact_task(this.id);"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
											</div>

										</div>
										<div class="row"></div>

									<form method ="post" action="<?php echo base_url(); ?>edit_contact_task"  name="forms_edit_contact_task" id="forms_edit_contact_task">

										<input type="hidden" name="contact_id" value ="<?php echo $row->contact_id; ?>">

										<input type="hidden" name="row_id" value ="<?php echo $row->id; ?>">

										<div class="col-md-12 select_task_<?php echo $row->id; ?>" style="display:none;">
											<div class="row">
												<label><strong>Task Type: </strong> </label>
												
											<select name="contact_task" style="width:50% !important;">
													<?php foreach ($selct_contact_task as $key => $option) {?>

													<option value="<?php echo $key; ?>" <?php if ($row->contact_task == $key) {echo 'selected';}?>><?php echo $option; ?></option>

													<?php }?>

													</select>
											</div>
											<div class="row">
												<label><strong>Date: </strong></label>
												<input style="width:70%;" type="text" class="datepicker" name="contact_date" value="<?php echo date('m-d-Y', strtotime($row->contact_date)); ?>">
											</div>
											<div class="row">
												<label style="vertical-align:top;"><strong>Note: </strong></label>
												
												<textarea style="width:70%;" type="text" name="edit_task_notes" rows="3"><?php echo $row->task_notes; ?></textarea>
											</div>
											<div class="row">
												<label><strong>Status: </strong></label>
												<select name="contact_status" style="width:50% !important;">
													<?php foreach ($contact_status_option as $key => $option) {?>

													<option value="<?php echo $key; ?>" <?php if ($key == $row->contact_status) {echo 'Selected';}?>><?php echo $option; ?></option>

													<?php }?>

												</select>

												<button type="submit" class="btn blue">Save</button>
											</div>
										</div>
									</form>
									<div class="row"></div>

								<?php } } ?>

								<div class="col-md-12">
									<a class="btn blue" data-toggle="modal" href="#contact_modal_tasks"><i class="fa fa-plus" aria-hidden="true"></i> Add </a>

								</div>
							</div>

                        <!----------------- Contact Hard money modal Start -------------->
                        <div class="modal fade bs-modal-lg" id="hardmoney" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <form method="POST" action="<?php echo base_url();?>Contact_marketing/hardmoneyprogram" name="forms_hardmoneyprogram" id="forms_hardmoneyprogram">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                            <h4 class="modal-title">Hard Money Program: <?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname : ''; ?></h4>
                                        </div>
                                        <div class="modal-body">
                                            
                                            <input type="hidden" name="contact_id" value="<?php echo $contact_id; ?>">
                                            <input type="hidden" name="hm_rowid" value="<?php echo isset($contathard_money_program[0]->id) ? $contathard_money_program[0]->id : 'new';?>">

                                            <div class="row addhmspace">
                                                <div class="col-md-6">
                                                    <label>Company Name:</label>
                                                    <input type="text" name="hm_company" class="form-control" placeholder="Company Name" value="<?php echo isset($contathard_money_program[0]->hm_company) ? $contathard_money_program[0]->hm_company : '';?>">
                                                </div>
                                            </div>
                                            <div class="row addhmspace">
                                                <div class="col-md-3">
                                                    <label>Bridge Loans:</label>
                                                    <select  class="form-control" name="hm_brodge_loan">
                                                        <?php foreach ($brodge_loan_option as $key => $row) { ?>
                                                            <option value="<?php echo $key; ?>" <?php if($key == $contathard_money_program[0]->hm_brodge_loan){echo 'selected';}?>><?php echo $row; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row addhmspace">
                                                <div class="col-md-3">
                                                    <label>LTV Ratio:</label>
                                                    <input type="text" name="hm_b_ltv" class="form-control" placeholder="LTV Ratio" value="<?php echo isset($contathard_money_program[0]->hm_b_ltv) ? number_format($contathard_money_program[0]->hm_b_ltv,2).'%' : '';?>">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Loan Min:</label>
                                                    <input type="text" name="hm_b_lmin" class="form-control" placeholder="Loan Min" value="<?php echo isset($contathard_money_program[0]->hm_b_lmin) ? '$'.number_format($contathard_money_program[0]->hm_b_lmin) : '';?>">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Loan Max:</label>
                                                    <input type="text" name="hm_b_lmax" class="form-control" placeholder="Loan Max" value="<?php echo isset($contathard_money_program[0]->hm_b_lmax) ? '$'.number_format($contathard_money_program[0]->hm_b_lmax) : '';?>">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Max Term (months):</label>
                                                    <input type="text" name="hm_b_mterm" class="form-control" placeholder="Max Term (months)" value="<?php echo isset($contathard_money_program[0]->hm_b_mterm) ? $contathard_money_program[0]->hm_b_mterm : '';?>">
                                                </div>
                                            </div>

                                            <div class="row addhmspace">
                                                <div class="col-md-3">
                                                    <label>Construction Loan:</label>
                                                    <select  class="form-control" name="hm_construct_loan">
                                                        <?php foreach ($ct_hm_construct_loan as $key => $row) { ?>
                                                            <option value="<?php echo $key; ?>" <?php if($key == $contathard_money_program[0]->hm_construct_loan){echo 'selected';}?>><?php echo $row; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row addhmspace">
                                                <div class="col-md-3">
                                                    <label>LTV Ratio:</label>
                                                    <input type="text" name="hm_c_ltv" class="form-control" placeholder="LTV Ratio" value="<?php echo isset($contathard_money_program[0]->hm_c_ltv) ? number_format($contathard_money_program[0]->hm_c_ltv,2).'%' : '';?>">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Loan Min:</label>
                                                    <input type="text" name="hm_c_lmin" class="form-control" placeholder="Loan Min" value="<?php echo isset($contathard_money_program[0]->hm_c_lmin) ? '$'.number_format($contathard_money_program[0]->hm_c_lmin) : '';?>">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Loan Max:</label>
                                                    <input type="text" name="hm_c_lmax" class="form-control" placeholder="Loan Max" value="<?php echo isset($contathard_money_program[0]->hm_c_lmax) ? '$'.number_format($contathard_money_program[0]->hm_c_lmax) : '';?>">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Max Term (months):</label>
                                                    <input type="text" name="hm_c_mterm" class="form-control" placeholder="Max Term (months)" value="<?php echo isset($contathard_money_program[0]->hm_c_mterm) ? $contathard_money_program[0]->hm_c_mterm : '';?>">
                                                </div>
                                            </div>

                                            <div class="row addhmspace">
                                                <div class="col-md-3">
                                                    <label>Fix and Flip Loans:</label>
                                                    <select  class="form-control" name="hm_fixflip">
                                                        <?php foreach ($fix_flip_loan_option as $key => $row) { ?>
                                                            <option value="<?php echo $key; ?>" <?php if($key == $contathard_money_program[0]->hm_fixflip){echo 'selected';}?>><?php echo $row; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row addhmspace">
                                                <div class="col-md-3">
                                                    <label>LTV Ratio:</label>
                                                    <input type="text" name="hm_ff_ltv" class="form-control" placeholder="LTV Ratio" value="<?php echo isset($contathard_money_program[0]->hm_ff_ltv) ? number_format($contathard_money_program[0]->hm_ff_ltv,2).'%' : '';?>">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Loan Min:</label>
                                                    <input type="text" name="hm_ff_lmin" class="form-control" placeholder="Loan Min" value="<?php echo isset($contathard_money_program[0]->hm_ff_lmin) ? '$'.number_format($contathard_money_program[0]->hm_ff_lmin) : '';?>">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Loan Max:</label>
                                                    <input type="text" name="hm_ff_lmax" class="form-control" placeholder="Loan Max" value="<?php echo isset($contathard_money_program[0]->hm_ff_lmax) ? '$'.number_format($contathard_money_program[0]->hm_ff_lmax) : '';?>">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Max Term (months):</label>
                                                    <input type="text" name="hm_ff_mterm" class="form-control" placeholder="Max Term (months)" value="<?php echo isset($contathard_money_program[0]->hm_ff_mterm) ? $contathard_money_program[0]->hm_ff_mterm : '';?>">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                            <button type="submit" name="submit" class="btn blue">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!----------------- Contact Hard money modal End -------------->

						<!----------------- Contact Broker data modal Start -------------->
                        <div class="modal fade bs-modal-lg" id="BrokerData" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <form method="POST" action="#" name="forms_BrokerData" id="forms_BrokerData">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                            <h4 class="modal-title">Broker Data: <?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname : ''; ?></h4>
                                        </div>
                                        <div class="modal-body">

                                            <input type="hidden" name="contact_id" value="<?php echo $contact_id; ?>"/>

                                            <div class="row" >
                                                <h4 style="background-color:#efefef;padding: 10px;">Broker Portal Access</h4>

                                                    <div class="col-md-3">
                                                        <label>Broker Portal Access:</label>
                                                            <select  class="form-control" name="allow_access_broker">

                                                                <?php
                                                                    foreach ($lender_portal_access as $key => $row) {
                                                                        ?>
                                                                    <option value="<?php echo $key; ?>" <?php if ($key == $fetch_lender_contact_type[0]->allow_access) {echo 'Selected';}?>><?php echo $row; ?></option>
                                                                    <?php
                                                                    }
                                                                ?>
                                                            </select>
                                                    </div>
                                                    
                                                    <div class="col-md-3">
                                                        <label>Username:</label>
                                                        <input type="text" name="broker_username" class="form-control" value="" readonly>
                                                    </div>
                                                    
                                                    <div class="col-md-3">
                                                        <label>Password:</label>
                                                        <input type="text" name="broker_password" class="form-control" value="" readonly>

                                                    </div>
                                                    <div class="col-md-2">

                                                        <a style="margin-top: 22px !important;" class="btn btn-success"> Send E-mail</a>
                                                        
                                                    </div>
                                            </div>

                                        </div>
                                        <div class="modal-footer">

                                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn blue">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    <!----------------- Contact Broker data modal End -------------->

                    <!----------------- Contact tags modal Start -------------->

					<div class="modal fade bs-modal-lg" id="contact_modal_tasks" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<form method="POST" action="<?php echo base_url(); ?>add_contact_task" name="forms_add_contact_task" id="forms_add_contact_task">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Contact Tasks: <?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname : ''; ?></h4>
										</div>
										<div class="modal-body">

										<input type="hidden" name="contact_id" value="<?php echo $contact_id; ?>"/>
											<div class="row">
												<div class="col-md-6 ">
													<label><strong>Task Type:</strong></label>
													<!--<input type="text" class="form-control" name="contact_task" value="">-->
													<select name="contact_task" class="form-control">
													<?php foreach ($selct_contact_task as $key => $option) {?>

													<option value="<?php echo $key; ?>" ><?php echo $option; ?></option>

													<?php }?>

													</select>
												</div>

												<div class="col-md-6 ">
													<label><strong>Date Due:</strong></label>
													<input type="text" class="form-control datepicker" name="contact_date" value="" placeholder="MM-DD-YYYY">
												</div>

											</div>
											<div class="row">&nbsp;</div>
											<div class="row">
												<div class="col-md-12">
													<label><strong>Add Note:</strong></label>
													<textarea class="form-control" type="text" name="add_task_note" rows="3" placeholder="Add Note..."></textarea>
												</div>
											</div>
											<div class="row">&nbsp;</div>
											<div class="row">
												<div class="col-md-6 ">
													<label><strong>Status:</strong></label>
													<select name="contact_status" class="form-control">
													<?php foreach ($contact_status_option as $key => $option) {?>

													<option value="<?php echo $key; ?>"><?php echo $option; ?></option>

													<?php }?>

													</select>
												</div>

												<div class="col-md-6 ">
													<label><strong>User:</strong></label><br>
													<input type="text" class="form-control" name="contact_user" value="<?php echo $this->session->userdata('user_name'); ?>">

													</select>
												</div>
											</div>
										</div>
										<div class="modal-footer">

											<button type="button" class="btn default" data-dismiss="modal">Close</button>
											<button type="submit"  class="btn blue">Save</button>
										</div>
									</form>
								</div>
							</div>
						</div>


						<!----------------- Contact tags modal end -------------->

						<hr style="float: left;width: 100%;">
							<div class="col-md-12 ">
								<label style="font-weight:600 !important;">Relationships:</label>

								<!----------------- FOR 'WAS REFERRED' --------------->
									<?php

										if (isset($contact_relationship_data)) {

										foreach ($contact_relationship_data as $relationship_name) {
									?>


											<form id="editrelationfrm<?php echo $relationship_name['cr_id']; ?>" method="post" action="<?php echo base_url() . 'Contact/edit_contact_relationship' ?>" name="forms_edit_contact_relationship" id="forms_edit_contact_relationship">
											<div class=" relation_labels">
												<!--<div class="col-md-9">

														<label><span> <a href="<?php echo base_url() . 'viewcontact/' . $relationship_name['contact_id']; ?>"><?php echo $relationship_contact_email1[$key]; ?> </a></span></label>

												</div>-->
												<div class="col-md-8 contact_relationship_name<?php echo $relationship_name['contact_id']; ?>">
												<!--
													 <label><b> Was referred by  <b></label>

													 <label><?php echo $relationship_contact_names1[$key]; ?></label>-->

													<label><span> <a href="<?php echo base_url() . 'viewcontact/' . $relationship_name['contact_id']; ?>"><?php echo $relationship_name['contact_firstname']; ?> </a></span></label>


												</div>

												<div class="col-md-4">
													<!--
													<a title="Delete" id="<?php echo $relationship_name['contact_id']; ?>" onclick="contact_relationship_action(this.id,'delete')"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
													-->
											<input type="hidden" value="<?php echo $this->uri->segment(2); ?>" id="x_id">
													<a title="Edit" id="<?php echo $relationship_name['cr_id']; ?>" onclick="edit_relationship_details(this)"><i class="fa fa-edit" aria-hidden="true"></i></a>&nbsp;&nbsp;
														<!---view button start------->
													<a title="View" id="<?php echo $relationship_name['cr_id']; ?>" onclick="view_relationship_details(this);"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;&nbsp;
													<!---view button end------->
													<a title="Delete" id="<?php echo $relationship_name['cr_id']; ?>" onclick="contact_relationship_action(this)"><i class="fa fa-trash-o" aria-hidden="true"></i></a>&nbsp;&nbsp;
												</div>




											</div>
											</form>

								<?php	} } ?>


								<!---------------------- FOR REFERRED --------------->
								<?php
									if ($relationship_contact_names) {

										foreach ($relationship_contact_names as $key => $relationship_name) {
								?>

											<form id="editrelationgfrm<?php echo $relationship_id[$key]; ?>" method="post" action="<?php echo base_url() . 'Contact/edit_contact_relationship' ?>">
												<input type="hidden" name="contact_id" value="<?php echo $this->uri->segment(2); ?>">
												<input type="hidden" name="relationship_action" id="relationship_action_<?php echo $relationship_id[$key]; ?>" value="">
												<input type="hidden" name="relationship_id" id="relationship_id_<?php echo $relationship_id[$key]; ?>" value="<?php echo $relationship_id[$key]; ?>">
												<input type="hidden" name="relationship_contact_id" id="relationship_contact_id<?php echo $relationship_id[$key]; ?>" value="<?php echo $relationship_contact_id[$key]; ?>">


												<div class=" relation_labels">

													<div class="col-md-6 contact_relationship_name<?php echo $relationship_id[$key]; ?>">
														<label><a href="<?php echo base_url() . 'viewcontact/' . $relationship_contact_id[$key]; ?>"><?php echo $relationship_name; ?></a></label>
													</div>

													<div class="col-md-9 select_relationship<?php echo $relationship_id[$key]; ?>" style="display:none;">
														<select  onchange="fill_relationship_id(<?php echo $relationship_id[$key]; ?>,this.value)" class="chosen" style="">
															<option value=''>SEARCH </option>
																<?php foreach ($fetch_all_contact as $row) {
																if ($row->contact_id == $relationship_contact_id[$key]) {
																$selected = 'selected';
																} else {
																$selected = '';
																} ?>
																<option <?php echo $selected; ?> value="<?php echo $row->contact_id; ?>"  ><?php echo $row->contact_firstname . ' ' . $row->contact_middlename . ' ' . $row->contact_lastname; ?></option>
																<?php } ?>
														</select>
													</div>
													<div class="col-md-3 contact_relationship_name">
														
													</div>

													<div class="col-md-3">
														<a title="Edit" id="<?php echo $relationship_id[$key]; ?>" onclick="edit_relationship_details(this)"><i class="fa fa-edit" aria-hidden="true"></i></a>&nbsp;&nbsp;
														<!---view button start------->


														<a title="View" id="<?php echo $relationship_id[$key]; ?>" onclick="view_relationship_details(this);"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;&nbsp;


														<!---view button end------->
														<a title="Delete" id="<?php echo $relationship_id[$key]; ?>" onclick="contact_relationship_action(this.id,'delete')"><i class="fa fa-trash-o" aria-hidden="true"></i></a>&nbsp;&nbsp;

													
													</div>
												</div>
											</form>

								<?php } } ?>

								<!--<hr style="float: left;width: 100%;">-->


		<div class="col-md-12 ">
			<a class="btn blue" data-toggle="modal" href="#add_relationship"><i class="fa fa-plus" aria-hidden="true"></i> Add a Relationship</a>
		</div>

	<!--------- Add Relationship model start ------------------>

		<div class="modal fade bs-modal-lg" id="add_relationship" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form id="relationfrm" method="post" action="<?php echo base_url() . 'Contact/contact_relation' ?>">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Relationship: </h4>
						</div>
						<div class="modal-body">

								<input type ="hidden" name="contact_iddd"  value="<?php echo $contact_id; ?>"/>

							<div class="row">
								<div class="col-md-6">
									<label><strong>Contact name:</strong></label>
										<select id="relationship" name="relationship" class="chosen">
											<option value=''>Select One</option>
											<?php
											foreach ($fetch_all_contact as $row) {
												?>
												<option value="<?php echo $row->contact_id; ?>"  ><?php echo $row->contact_firstname . ' ' . $row->contact_middlename . ' ' . $row->contact_lastname; ?></option>
											<?php } ?>
										</select>

								</div>

								<div class="col-md-6">
									<label><strong>Relationship Type:</strong></label>
										<select name="relation_option" class="form-control">
										<?php foreach ($relationships_option as $key => $option) {?>

										<option value="<?php echo $key; ?>" <?php if ($key == $relation_option) {echo 'Selected';}?>><?php echo $option; ?></option>

										<?php } ?>
										</select>


								</div>
							</div>
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="col-md-12">
								<label><strong>Relationship Note:</strong></label>
									<textarea name="relationship_note" type="text" rows="6" class="form-control" placeholder="Add Relationship Note Here..."></textarea>
								</div>
							</div>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
							<button class ="btn blue" type="submit" id="relation_contact_id" onclick="submit_relation_frm(this);">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	<!---------Add  Relationship model end ------------------>


<!--------- View Relationship model-->

		<div class="modal fade bs-modal-lg" id="view-modal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form id="relationfrm" method="post" action="">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">View Relationship: </h4>
						</div>
						<div class="modal-body">

								<input type ="hidden" name="contact_iddd"  value="<?php echo $contact_id; ?>"/>

							<div class="row">
								<div class="col-md-6">
									<label><strong>Contact name:</strong></label>
										<select id="relationships"  name="relationship" class="form-control" disabled>
												<option value='' disabled>Select One</option>
											<?php foreach ($fetch_all_contact as $row) { ?>
												<option value="<?php echo $row->contact_id; ?>"  disabled>
												<?php echo $row->contact_firstname . ' ' . $row->contact_middlename . ' ' . $row->contact_lastname; ?></option>
											<?php } ?>
										</select>

								</div>

								<div class="col-md-6">
									<label><strong>Relationship Type:</strong></label>

									<select name="relation_option"  id="reltionn" class="form-control" disabled>
										<?php foreach ($relationships_option as $key => $option) {?>

											<option value="<?php echo $key; ?>" disabled><?php echo $option; ?></option>

										<?php } ?>
									</select>

								</div>
							</div>
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="col-md-12">
								<label><strong>Relationship Note:</strong></label>
									<textarea name="relationship_note"  type="text" id="notes" rows="6" class="form-control " placeholder="Add Relationship Note Here..." readonly></textarea>
								</div>
							</div>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>

						</div>
					</form>
				</div>
			</div>
		</div>



	<!--------- View Relationship model end ------------------>



<!--------- edit Relationship model -->

		<div class="modal fade bs-modal-lg" id="edit-modal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<input type="hidden" class="relation_id" value="">


					<form id="" method="post" action="#">
						<div class="modal-header">

							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Relationship: </h4>
						</div>
						<div class="modal-body">

					<div class="msg"></div>
								<input type ="hidden" name="contact_iddd"  value="<?php echo $contact_id; ?>"/>

							<div class="row" id="v">
								<div class="col-md-6">
									<label><strong>Contact name:</strong></label>
											<select id="relationshipss"  name="relationship" class="form-control" >
												<option value='' >Select One</option>
											<?php foreach ($fetch_all_contact as $row) { ?>
												<option value="<?php echo $row->contact_id; ?>"  >
												<?php echo $row->contact_firstname . ' ' . $row->contact_middlename . ' ' . $row->contact_lastname; ?></option>
												<?php } ?>
										</select>


								</div>

								<div class="col-md-6" id="d">
									<label><strong>Relationship Type:</strong></label>
											<input type="hidden" id="u_id" value="">
										<select name="relation_option"  id="reltionn" class="form-control" >
										<?php foreach ($relationships_option as $key => $option) {?>

											<option value="<?php echo $key; ?>" ><?php echo $option; ?></option>

										<?php } ?>
										</select>

								</div>
							</div>
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="col-md-12">
								<label><strong>Relationship Note:</strong></label>
									<textarea name="relationship_note"  type="text" id="notes" rows="6" class="form-control v" placeholder="Add Relationship Note Here..." ></textarea>
								</div>
							</div>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    	<button type="button" class ="btn blue"  onClick="relationship_edit_fun(this)" >Save</button>

						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- Login History Model -->

		<div class="modal fade" id="LoginHistory" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action ="#">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Login History </h4>
                        </div>
                        <div class="modal-body">


                            <div class="row">
                                

                                
                                    <table id="myTableData" class="table table-bordered table-striped table-condensed flip-content th_text_align_center" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Time</th>
                                                                                               
                                            </tr>
                                        </thead>
                                        
                                        <tbody id="">
                                            <?php if(!empty($lastlogindetails)){ foreach($lastlogindetails as $logValue){ ?>
                                                <tr>
                                                    <td><?php echo date('m-d-Y', strtotime($logValue->date)); ?></td>
                                                    <td><?php echo $logValue->time; ?></td>
                                                </tr>
                                            <?php } } else { '<tr><td colspan="3">Not login yet</td></tr>'; } ?>
                                        </tbody>
                                    
                                        <tfoot id="">
                                            
                                            
                                        </tfoot>
                                    </table>
                                
                            </div>
                        </div>
                        <div class="modal-footer">
                            <!-- <button type="button" name="submit" value="save" class="btn blue">Save</button> -->
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
        </div>
</div>

	<!--------- edit Relationship model end ------------------>




					</div>

					<!------------------ MAIN RiGHT DIV ENDS -------------------------------->
					<hr style="float: left;width: 100%;">

						<div class="col-md-12 ">
								<label style="font-weight:600 !important;">History:</label>




							<?php foreach ($data_fetch_tags as $k => $r) { if ($r['email'] != '') { ?>

							<h5><strong><?php echo date('m/d/Y', strtotime($r['modified_date'])); ?></strong> – Modified by <strong><?php echo $r['email']; ?></strong></h5>




							<?php }}?>


							<?php

                        foreach ($fetch_check_user as $key => $row) {

                            if($row->msg != ''){ ?>

                                
                                <h5><?php echo $row->msg.' <strong>'.date('m-d-Y', strtotime($row->date)); ?></strong>.</h5>
                                

                                <?php }else{ ?>


						          <h5><strong><?php echo date('m/d/Y', strtotime($row->date)); ?></strong> – Created by <strong><?php echo $row->fname . ' ' . $row->middle_name . ' ' . $row->lname; ?></strong></h5>

							<?php  } } ?>

							</div>

					<hr style="float: left;width: 100%;">

			</div>
		</div>

	</div>
</div>



<form id="delete_contact_form" method="POST" action="<?php echo base_url(); ?>delete_contact">
<input type="hidden" value="<?php echo $contact_id; ?>" name="contact_id">
</form>

<!----------------- loan borrower report ------------------->
	<form method="POST"  action="<?php echo base_url(); ?>loan_schedule_borrower" id="loan_borrower_reportdata">
		<input type="hidden" name="contact_ids" >
	</form>
<!----------------- loan borrower report ------------------->

<!----------------- lender trust deed report ------------------->
	<form method="POST"  action="<?php echo base_url(); ?>loan_schedule_investor" id="trust_deed_reportdata">
		<input type="hidden" name="contact_ids" >
	</form>
<!----------------- lender trust deed report ------------------->

 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
<style>

#ct_Documentsadd{
	float: left;
	width: 100%;
}
div#relationship_chosen {
    width: 250px !important;
    margin-top: 0px !important;
}
.chosen-container-single .chosen-single {
    padding: 4px 0px 26px 6px !important;
    border-radius: 4px !important;
    border: 1px solid #e1e1e1 !important;
}
.chosen-container.chosen-container-single {
    width: 250px !important;
}
</style>
<script type="text/javascript">

function SendUsername(contact_id,user_type,username)
{
	$.ajax({
        type : 'POST',
        url  : '/Contact/SendUsername',
        data : {'contact_id':contact_id,'user_type':user_type,'username':username},
        success : function(response){
	        var json =  JSON.parse(response);
			var message = json.message;
			if(user_type==1)
			{
				
				$("#messagesChange").html(message);
			}
			else
			{
				$("#messagesChange1").html(message);
			}
        }
	});
}
	
 function delete_contact_task(id){
	var c_id = id;
	if(confirm('Are you Sure To Delete?')){

		$.ajax({
			type : 'POST',
			url	 : '<?php echo base_url() . "Contact/delete_tasks"; ?>',
			data : { 'id':c_id },
			success : function(response){
				if(response == 1){
					$('div#contact_task_'+id).remove();
					//$('p#message-contact-task').text('Successfully Deleted');
				}
			}

		});
	}
}

$(document).ready(function(){
	$('body').on('click', '#ct_loan_saved_documentadd', function(){
		$(".ct_loan_saved_document").toggle('slow');
	});
});

function delete_doc(that){
	var doc_id = that.id;
	if(confirm('Are You Sure To Delete?')){
		$.ajax({

			type	:'POST',
			url		:'<?php echo base_url() . "Contact/delete_document"; ?>',
			data	:{'id':doc_id},
			success : function(response){
				if(response == 1){
					$(that).parent().parent('li').remove();
					window.location.reload();
				}
			}

		});
	}
}

$(document).ready(function(){
    var popup_modal_hit = '<?php echo $this->session->flashdata('popup_modal_hit'); ?>';
    if(popup_modal_hit == 'lender-data'){
        $('#lender_data').modal('show');
    }else if(popup_modal_hit == 'borrower_data'){
    	$('#borrower_data').modal('show');
    }else{

    }
});
 


function view_relationship_details(that){
	var id = that.id;
	$('div#view-modal select#relationships option').attr('selected',false);
	$('div#view-modal select#reltionn option').attr('selected',false);
	$.ajax({
        type: "post",
		url: "<?php echo base_url() ?>Contact/fetch_data",
		data:{'id':id},
		success : function(response){
			if(response){

				var data = JSON.parse(response);

				$('div#view-modal select#relationships option[value="'+data.relationship_contact_id+'"]').attr('selected',true);
				$('div#view-modal select#reltionn option[value="'+data.relation_option+'"]').attr('selected',true);


				$('div#view-modal textarea#notes').text(data.relationship_note);
				$('div#view-modal').modal('show');
			}
		}
  });
}

function edit_relationship_details(t){

	var id = t.id;

	$('div#edit-modal select#relationshipss option').attr('selected',false);
	$('div#edit-modal select#reltionn option').attr('selected',false);
	$.ajax({
        type: "post",
		url: "<?php echo base_url() ?>Contact/fetch_data",
		data:{'id':id},
		success : function(response){
			if(response){

				var data = JSON.parse(response);


				$('div#edit-modal select#relationshipss option[value="'+data.relationship_contact_id+'"]').attr('selected',true);
				$('div#edit-modal select#reltionn option[value="'+data.relation_option+'"]').attr('selected',true);


				$('div#edit-modal textarea#notes').text(data.relationship_note);
				$('div#edit-modal input#u_id').val(data.id);
				$('div#edit-modal').modal('show');



			}
		}
  });



}

$(document).ready(function(){

	var value = $('#borrower_experience_selectbox select[name="borrower_experience"]').val();
	display_how_may_box(value);

	$('#contactActivetable').DataTable();
});

function display_how_may_box(that){

	if(that == 1){
		$('#how_may_box').css('display','block');
	}else{
		$('#how_may_box').css('display','none');
	}


}

function relationship_edit_fun(e){


	var u_id=$(e).parent().parent().find('input#u_id').val();
	var relationship_note = $('textarea.v').val();
	var relationship = $(e).parent().parent().find('#relationshipss option:selected').val();
	var relation_option = $(e).parent().parent().find('#reltionn option:selected').val();

	$.ajax({
        type: "post",
		url: "<?php echo base_url() ?>Contact/edit_update_data",
		data:{'id':u_id,'relationship_note':relationship_note,'relation_option':relation_option,'relationship':relationship},
		success : function(response){
		//$("div.msg").empty();
		//$("div.msg").append('<i class="fa fa-thumbs-o-up"></i> Data Updated Successfully').addClass("alert alert-success");
		window.location.reload();
		}
	});

}

$(".amount_format").keyup(function(){
	var a = this.value;
	var a = replace_dollar(a);
	if(a == '')
	{
		a = 0;
	}
	a = parseFloat(a);
	this.value = '$'+number_format(a);
});


function financial_value(that){

	var value = that.value;
	//alert(value);
	if($(that).is(':checked')){
		$('input#financial_situation_value').val(value);
	}else{
		$('input#financial_situation_value').val('');
	}
}

function net_worth_financial_value(that){

	var value = that.value;
	//alert(value);
	if($(that).is(':checked')){
		$('input#net_worth_financial_situation_value').val(value);
	}else{
		$('input#net_worth_financial_situation_value').val('');
	}
}

function liquid_financial_value(that){

	var value = that.value;
	//alert(value);
	if($(that).is(':checked')){
		$('input#liquid_financial_situation_value').val(value);
	}else{
		$('input#liquid_financial_situation_value').val('');
	}
}

function liquidity_needs_value(that){

	var value = that.value;
	//alert(value);
	if($(that).is(':checked')){
		$('input#lender_liquidity_needs_chkbx').val(value);
	}else{
		$('input#lender_liquidity_needs_chkbx').val('');
	}

}


function display_package_date_of_re(t){

	var val1 = t;

	var get_date=$('input.RE_date_val').val();


var date_two='<?php echo date("m-d-Y", strtotime("-1 year")) ?>';

	if(val1 == '1' && date_two <= get_date){

		$('input.RE_date_val').attr('disabled',false);
		$('input#status_check').val('Current');

	}
	else if(val1 == '1' && date_two >= get_date){

		$('input.RE_date_val').attr('disabled',false);
		$('input#status_check').val('Expired');

	}else{


		$('input.RE_date_val').attr('disabled',true);
		$('input#status_check').val('Not Complete');
	}
}

function display_package_date_nda_signed(tt){

	var val1 = tt;

	if(val1 == '1'){

		$('input[name="nda_date"]').attr('disabled',false);
	}else{

		$('input[name="nda_date"]').attr('disabled',true);
	}
}
$(document).ready(function(){
	var val = $('div#lender_data select[name="nda_signed"]').val();

	display_package_date_nda_signed(val);
});

$(document).ready(function(){
	var val = $('div#lender_data select[name="re_complete"]').val();

	display_package_date_of_re(val);
});

$(document).ready(function(){
	var val = $('div#lender_data select[name="setup_package"]').val();	
});



function loan_borrower_report(contact_id){	
	$('form#loan_borrower_reportdata input[name="contact_ids"]').val(contact_id);
	$('form#loan_borrower_reportdata').submit();
}

function investment_experience_value(that){
	var value = that.value;
	if($(that).is(':checked')){
		$('input#invest_ex_checkbox_value').val(value);
	}
}

$(document).ready(function(){

	var ee = '<?php echo $ee; ?>';
	
	if(ee == ''){
		$('#checkbx_2').prop('checked',true);


		$('#checkbx_1').attr('disabled',true);
		$('#checkbx_3').attr('disabled',true);
		$('#checkbx_4').attr('disabled',true);
		$('#checkbx_5').attr('disabled',true);

	}

	if($('#us_citizen').val() == '2'){

		$('.us_citizen_optn').css('display','block');

	}else{

		$('.us_citizen_optn').css('display','none');
	}

	if($('#litigation').val() == '1'){

		$('.litigation_optn').css('display','block');

	}else{
		$('.litigation_optn').css('display','none');

	}

	if($('#felony').val() == '1'){

		$('.felony_optn').css('display','block');

	}else{
		$('.felony_optn').css('display','none');

	}

	if($('#Bankruptcy').val() == '1'){

		$('.BK_optn').css('display','block');

	}else{
		$('.BK_optn').css('display','none');

	}

	if($('#BK').val() == '1'){

		$('.BK_textarea_div').css('display','block');

	}else{
		$('.BK_textarea_div').css('display','none');

	}

	//alert($('select#text_7year').val());

	if($('select#text_7year').val() == '1'){
		$('.bankruptcy_7year_text').css('display','block');
	}else{
		$('.bankruptcy_7year_text').css('display','none');
	}

});


function display_bankruptcy_7year(value){
	
	if(value == '1'){
		$('.bankruptcy_7year_text').css('display','block');
	}else{
		$('.bankruptcy_7year_text').css('display','none');
	}
}

function display_us_citizenNew(value){
	if(value == '2'){
		$('.us_citizen_optn').css('display','block');
	}else{
		$('.us_citizen_optn').css('display','none');
		
	}
}





 var k= 1;
function notes_add_more_row(){
	  k++;
		var notes_type = '<?php
foreach ($notes_type as $key => $type) {
	if ($key == '') {
		$selected = 'selected="selected"';
	} else {
		$selected = '';
	}
	echo '<option value="' . $key . '"  ' . $selected . ' >' . $type . '</option>';

}?>';

		var loan_id = '<?php
foreach ($fetch_loans as $key => $row) {
	if ($fetch_loans[$key]['id']) {
		if ($fetch_loans[$key]['id'] == $loan_id) {
			$selected = 'selected="selected"';
		} else {
			$selected = '';
		}
	} else {
		$selected = '';
	}
	echo '<option value="' . $fetch_loans[$key]['id'] . '" ' . $selected . ' >' . $fetch_loans[$key]["text"] . '</option>';

}?>';


		  var content = '<div class="row borrower_data_row" ><div class="col-md-3"><input type="hidden" value ="'+k+'" name="notes[]"/><input type="text" class="form-control datepicker" name="notes_date[]" id="notes_date_'+k+'" value="" placeholder="MM-DD-YYYY" /></div><div class="col-md-3"><select id="notes_type_'+k+'" name="notes_type[]" class=" form-control" data-live-search="true" >'+notes_type+'</select></div><div class="col-md-3"><select name="loan_id[]" onchange="loan_form_submit(this.value)" class="form-control" data-live-search="true">'+loan_id+'</select></div></div><div class="row borrower_data_row borrower_textarea_div" ><div class="col-md-9"><textarea class="form-control" name="personal_notes[]" placeholder="Text:" id="personal_notes_'+k+'" ></textarea></div></div>';


		$( ".main_notes" ).append(content);

			$('.datepicker').datepicker({ dateFormat: 'mm-dd-yy' });
			// $('.selectpicker').selectpicker('render');

		$('.selectpicker').selectpicker('refresh');
		// $('#notes_type_'+k+'').data('selectpicker');

}


function notes_delete_row(id,contact_id){
	if (confirm("Are you sure to delete this note?") == true) {
       window.location.href="<?php echo base_url(); ?>delete_notes/"+id+"/"+contact_id;
    }
}

function fetch_notes(value){
	//ajax call....
	$.ajax({
			type: "post",
			url: "<?php echo base_url() ?>Contact/fetch_notes/",
			data:{'id':value},
			success: function(response){
				if(response){
					$('#historyfrm').css('display','block');
					var data  = JSON.parse(response);

					$('#fetch_notes_id').val(data.id);
					$('#fetch_contact_id').val(data.contact_id);


					$('#label_notes_date').html(data.notes_date);
					$('#fetch_notes_date').val(data.notes_date);


					$('#label_notes_type').html(data.notes_type_name);
					$('#fetch_notes_type').val(data.notes_type_name);
					$('#fetch_notes_type_id').val(data.notes_type);
					$('#fetch_select_notes_type').val(data.notes_type_name);
					$('.select_notes_type .bootstrap-select .filter-option').text(data.notes_type_name);
					$('select[name=fetch_notes_type]').val(data.notes_type);


					 $('#label_loan_id').html(data.talimar_loan);
					$('#fetch_talimar_loan').val(data.talimar_loan);
					$('#fetch_select_talimar_loan').val(data.loan_id);
					$('.select_loan_id .bootstrap-select .filter-option').text(data.talimar_loan);

					$('#label_loan_id').html(data.property_address);
					$('#fetch_talimar_loan').val(data.property_address);
					$('#fetch_select_talimar_loan').val(data.loan_id);
					$('.select_loan_id .bootstrap-select .filter-option').text(data.property_address);


					$('#label_personal_notes').html(data.notes_text);
					$('#fetch_personal_notes').html(data.notes_text);

				}
			}
		});

}


$('#export-menu li').bind("click", function() {

			// var selection_note_id = $('#selection_note_id').val();
			// if(selection_note_id ){

				var target 	= $(this).attr('class');
				var id 		= $(this).data('id');
				var optn 	= $(this).data('option');

				console.log('Target is: ' + target);
				console.log('id is: ' + id);

				$('.hidden-type').val(target);

				 if(target == 'delete-note'){
					if (confirm("Are you sure to delete this note?") == true) {
						$('#selection_note_id_'+id).val(id);
						$('#frmq'+id).submit();
					}
					// document.getElementById('frmq'+id).submit();

				}else{


					$('.label_notes_date'+id).css('display','none');
					$('.input_notes_date'+id).css('display','block');

					$('.label_notes_type'+id).css('display','none');
					$('.select_notes_type'+id).css('display','block');

					$('.select_loan_id'+id).css('display','block');
					$('.label_loan_id'+id).css('display','none');

					$('.input_notes_text'+id).css('display','block');
					$('.label_notes_text'+id).css('display','none');


					$('#history_formsubmit_'+id).css('display','block');		// display update button...
					
				}
		
		});
	function fill_relationship_id(id,value){

		$('#relationship_contact_id'+id).val(value);
		$('#editrelationfrm'+id).submit();
	}

	//...........................my fucntion start............................//
	function contact_relationship_action(th)

	{
		var id=$(th).attr('id');
		var contact_id=$(th).parent('div').find('#x_id').val();

			if(confirm('Are you sure want to Delete')){
			$.ajax({
			type: "post",
			url: "<?php echo base_url() ?>Contact/contact_relationship_delete",
			data:{'id':id,'contact_id':contact_id},
			success: function(response){

          $(th).parent('div').parent().remove();

			}
		});
   }
	}

	//...........................my fucntion end............................//



	function edit_contact_task(id){

		$('#contact_task_'+id).css('display','none');
		$('.select_task_'+id).css('display','block');
	}


	function relation_field_hide(){
		$('.relationship_div').css('display','none');
	}

	function check_permission_option(value,id){

		if(value == '3'){
			$('.particular_user_dropdown').css('display','block');

			if($('#checkbx_2').prop('checked') == true){
				$('#checkbx_2').prop('checked',false);


			}
			if($('#checkbx_3').prop('checked') == false){
				$('.particular_user_dropdown').css('display','none');
			}
		
		}else if(value == '2'){

			if($('#checkbx_2').prop('checked') == true){

				$('#checkbx_1').attr('disabled',true);
				$('#checkbx_4').attr('disabled',true);
				$('#checkbx_5').attr('disabled',true);
				$('#checkbx_3').attr('disabled',true);

				$('#checkbx_1').prop('checked',false);
				$('#checkbx_4').prop('checked',false);
				$('#checkbx_5').prop('checked',false);
				$('#checkbx_3').prop('checked',false);
				$('#particular_user_id').val('');

			}
			 else{
				// $('#checkbx_3').prop('checked',true);
				$('#checkbx_1').attr('disabled',false);
				$('#checkbx_4').attr('disabled',false);
				$('#checkbx_5').attr('disabled',false);
				$('#checkbx_3').attr('disabled',false);

			}
			$('.particular_user_dropdown').css('display','none');
		}else if(value == '1'){

			$('#checkbx_3').prop('checked',false);
			$('.particular_user_dropdown').css('display','none');
		}else{


			// $('.particular_user_dropdown').css('display','none');

		}

	}



	 function display_inputfield(id){
		var filename = $('#filename_'+id).data('name');

		$('.new_filename_div'+id).toggle();
		$('#contact_document_id'+id).val(id);
		$('#old_filename'+id).val(filename);

	}
$('body').on('click', '.borrower_data_pop', function(){
	$('input[name="popup_data"]').val('borrower_data');
});

function load_modal(taa){
	$('input[name="popup_data"]').val('lender-data');
$('#lender_data').modal('show');

var WholeNoteInvestor = $("input[name='WholeNoteInvestor']:checked").val();
var ownership_type = $("input[name='ownership_type']:checked").val();
var lend_value = $("input[name='lend_value']:checked").val();
var ltv = $("input[name='ltv[]'][type=checkbox]:checked");
var position = $("input[name='position[]'][type=checkbox]:checked");
var loan = $("input[name='loan[]'][type=checkbox]:checked");
var property_type = $("input[name='loan[]'][type=checkbox]:checked");
var yiled= $("input[name='yield[]'][type=checkbox]:checked");

var property_type = $("input[name='property_type[]'][type=checkbox]:checked");
var committed_funds= $('input#committed_funds').val();






if((lend_value !='1' && lend_value !='2' || WholeNoteInvestor !='1' && WholeNoteInvestor !='2' || ownership_type !='1' && ownership_type !='2' || ltv.length=='' || position.length=='' || loan.length=='' || property_type.length=='' || yiled.length=='')){

$("span#show_img").css({"margin-left": "155px", "margin-top": "-22px", "display":"block"});





  }
  if(committed_funds=='$'){


$("span#show_imgg").css({"margin-left": "127px", "margin-top": "-24px", "display":"block"});

  }


}


	function check_filename_duplicacy(id,contact_id){

		var new_filename = $('#new_filename'+id).val();
		var old_filename = $('#old_filename'+id).val();

		var new_extension = new_filename.substr( (new_filename.lastIndexOf('.') +1) );
		var old_extension = old_filename.substr( (old_filename.lastIndexOf('.') +1) );
		switch(new_extension) {
			case 'pdf':
				var filename =  new_filename;
				$('#new_filename'+id).val(filename);

			break;
			default:
				var filename = new_filename+'.'+old_extension;
				$('#new_filename'+id).val(filename);
		}

		//ajax call to check duplicacy of file name....
		$.ajax({
			type 	: 'post',
			url  	: '<?php echo base_url() . 'Contact/check_contact_filename'; ?>',
			data	: { 'filename' : filename,'contact_id':contact_id},
			success : function(response){
				if(response == '1'){
					// duplicacy found...
					$('#new_filename'+id).val(' ').focus();
					alert('Filename: '  + filename + ' already exist for this Contact, Please try another filename!' );

				}
			}

		});

	}

$.noConflict();


function SendLinkToForget(contact_id,user_type,username)
	{


		$.ajax({
		        type : 'POST',
		        url  : '<?php echo base_url()."Contact/sendLinkToForgetPassword";?>',
		        data : {'contact_id':contact_id,'user_type':user_type,'username':username},
		        success : function(response){

		        //	console.log(response);

			        var json =  JSON.parse(response);
					
					var message = json.message;
				if(user_type==1)
				{
					$("#messagesChange").html(message);
				}
				else
				{
					$("#messagesChange1").html(message);
				}
					
						
		            
		        }
		    });
	}
</script>

<script type="text/javascript">
	fieldHideshow();

	function fieldHideshow()
	{
		var select = $("#employmentId").find(":selected").val(); //input[name='employment']; 

		if(select == 'yes')
		{
			
			$("input[name='current_position']").prop("disabled", false);
			$("input[name='cp_title']").prop("disabled", false);
			$("input[name='cp_length_of_position']").prop("disabled", false);
		}
		else
		{
			
			$("input[name='current_position']").val('');
			$("input[name='cp_title']").val('');
			$("input[name='cp_length_of_position']").val('');

			$("input[name='current_position']").prop("disabled", true);
			$("input[name='cp_title']").prop("disabled", true);
			$("input[name='cp_length_of_position']").prop("disabled", true);
		}
	}
</script>
<script type="text/javascript">
	fieldHideshow1();
	
	function fieldHideshow1()
	{
		
		var select = $("#re_complete").find(":selected").val(); //input[name='employment']; 
		if(select == '1')
		{
			
			$("input[name='RE_date']").prop("disabled", false);

			//$("input[name='lender_RE_form_type']").prop("disabled", false);
			
		}
		else
		{
			
			$("input[name='RE_date']").val('');
			// $("input[name='lender_RE_form_type']").val('');
			

			$("input[name='RE_date']").prop("disabled", true);
			$("select[name='lender_RE_form_type']").val("Not Complete");
            $("select[name='lender_RE_form_type']").css("color","black");;
		}
	}
    function get_checked_other(){
        if ($('#investment_experience_checkbox_other').is(':checked')) {
            $("#other_description").prop('disabled', true);
        }else{
             $("#other_description").prop('disabled', false);
        }
    }
</script>

<script type="text/javascript" src="<?php echo base_url("assets/js/contact/contactlist.js"); ?>"></script>