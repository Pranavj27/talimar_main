<?php 
function validateDate($date, $format = 'Y-m-d'){
    $d = DateTime::createFromFormat($format, $date);
    // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
    return $d && $d->format($format) === $date;
}
// echo '<pre>'; print_r($chk); echo '</pre>';

if ($fetch_contact_notes) {
	$sequence = 0;
	foreach ($fetch_contact_notes as $key => $notes) {
		$sequence++;
		if($sequence ==1){
			// echo '<pre>'; print_r($notes); echo '</pre>';
		}

		$ab_follow_up_date = $notes->ab_follow_up_date;
		if(validateDate($ab_follow_up_date) == true){
			$_follow_up_date = date("m-d-Y", strtotime($ab_follow_up_date));
		}else{

			$_follow_up_date = '';
		} 
		
		switch ($notes->notes_type) {
		case 1:$notes_type_val = 'Email';
			break;
		case 2:$notes_type_val = 'Phone';
			break;
		case 3:$notes_type_val = 'Letter';
			break;
		case 4:$notes_type_val = 'Meeting';
			break;
		case 5:$notes_type_val = 'Other';
			break;
		default:
			$notes_type_val = 'None';
		}
		?>
		<form  class ="history-filter" id="history-filter_<?php echo $notes->id; ?>" method="post" action="<?php echo base_url() . 'Contact/history_filter'; ?>" >
			<input type="hidden" id="total_record_notes_data" value="<?php echo count($fetch_contact_notes);?>">
			<input type="hidden" value='' id='hidden-type' class='hidden-type' name='action'/>
			<input type="hidden"  id='fetch_contact_id' value="<?php echo $this->uri->segment(2); ?>"  name='fetch_contact_id'/>
			<input type="hidden" id="fetch_notes_id" name="fetch_notes_id" value="<?php echo $notes->id; ?>" />

			<div class="row  borrower_data_row inline-label2 " >
				<div class="notes_class label_notes_date<?php echo $notes->id; ?> dates" >
					<label class="title_history">Date:</label>
					<label class="history_label form-control" id="label_notes_date" name="fetch_notes_date" ><?php echo date('m-d-Y', strtotime($notes->notes_date)); ?></label>
				</div>

				<div class="notes_class input_notes_date<?php echo $notes->id; ?>" style="display:none;">
					<label class="title_history">Date:</label>
					<label  class="history_label form-control" id="label_notes_type"><?php echo date('m-d-Y', strtotime($notes->notes_date)); ?></label>
				</div>


				<div class="notes_class label_notes_type<?php echo $notes->id; ?> types">
					<label class="title_history">Type:</label>
					<label  class="history_label form-control" id="label_notes_type"><?php echo $notes_type_val; ?></label>
				</div>

				<div class="notes_class select_notes_type select_notes_type<?php echo $notes->id; ?> " style="display:none;">
					<label class="title_history">Type:</label>
					<label  class="history_label form-control" id="label_notes_type"><?php echo $notes->notes_type; ?></label>
				</div>

				<div class="spc-loan-div  label_loan_id<?php echo $notes->id; ?> purpose">
					<label class="title_history">Purpose:</label>
					<label  class="history_label form-control" id="label_notes_type"><?php echo $notes->purpose_type; ?></label>
				</div>
				<?php if($this->session->userdata('user_role') == '2'){ ?>
					<a onclick="editContactNotes('<?php echo $notes->id; ?>');" class="btn btn-xs btn-info" style="float: right;margin-top: 10px;">Edit</a>
				<?php } ?>

				<div class="spc-loan-div selected_loans select_loan_id<?php echo $notes->id; ?>" style="display:none;">
					<input type="hidden"  class="form-control "  id="fetch_loan_id" name="fetch_loan_id" value="<?php echo $notes->loan_id; ?>" placeholder="loan" />
					<label class="title_history">Property:</label>

					<select id ="fetch_select_talimar_loan" name="fetch_talimar_loan" class="chosen" style="width: 181px !important;" disabled>
						<option value="">Select Property</option>
						<?php
					foreach ($property as $key => $row) {
						if ($property[$key]['id'] == $notes->loan_id) {
							$selected = 'selected';

						} else {

							$selected = '';
						}
						?>
						<option <?php echo $selected; ?> value="<?php echo $property[$key]['id']; ?>"  ><?php echo $property[$key]['text']; ?></option>
						<?php }?>
					</select>



					
				</div>

			</div>
			<div class="row borrower_data_row borrower_textarea_div " >
				<div class="col-md-9 label_notes_text<?php echo $notes->id; ?>" >
					<?php echo $notes->notes_text; ?>
				</div>
			</div>	
			
			<!-- <div class="row">&nbsp;</div> -->

			<div class="row borrower_data_row borrower_textarea_div ">	
				<div class="col-md-4 label_notes_text<?php echo $notes->id; ?>" >
					<i class="enter_by">Entered By : <?php echo $fetch_userss_data[$notes->user_id]; ?></i>
				</div>
				<div class="col-md-4"></div>
	            <div class="col-md-4 row_details">
	                <?php if($notes->loan_id != ''){
	                    $propertLINK = '<a href="'.base_url().'load_data/'.$notes->loan_id.'">'.$listAllProperty[$notes->loan_id].'</a>';
	                }else{
	                    $propertLINK = '';
	                } ?>
	            
	                <i class="property">Property: <?php echo $propertLINK; ?></i>
	            </div>
			</div>
			<div class="row borrower_data_row inline-label2 ">
				
					<div class="notes_class  label_loan_id<?php echo $notes->id; ?> follo_up" style="margin-left: 16px;">
						<label class="title_history">Follow Up:</label>
						<?php
						//echo $notes->ab_follow_up_task;
						if($notes->ab_follow_up_task == 'Yes'){
							 $d_data = '<a href="'.base_url().'add_task/'.$notes->ab_contact_task_id.'">'.$notes->ab_follow_up_task.'</a>';
							 //echo 'yes--';
						}else{
							 $d_data = $notes->ab_follow_up_task;
							  //echo 'No--';
						}
						?>
						<label  class="history_label form-control" id="label_notes_type" style="width: 40% !important"><?php /*echo $contact_purpose_menu[$notes->ab_follow_up_task]; */
						echo $d_data;
						 // echo ($notes->ab_follow_up_task=='Select One')?'No':$notes->ab_follow_up_task;?></label>
					</div>
				
				
					<div class="notes_class  label_loan_id<?php echo $notes->id; ?> follo_up_date">
						<label class="title_history">Follow Up Date:</label>
						<label  class="history_label form-control" id="label_notes_type" style="width: 40% !important"><?php
							echo $_follow_up_date; ?></label>
					</div>
				
				
					<div class="notes_class  label_loan_id<?php echo $notes->id; ?> follo_up_status">
						<label class="title_history">Follow Up Status:</label>
						<label  class="history_label form-control" id="label_notes_type" style="width: 40% !important"><?php
						 echo ($notes->ab_follow_up_status=='Select One')?'None':$notes->ab_follow_up_status; ?></label>
					</div>
				
				
			</div>
		<div class="row">&nbsp;</div>	
		<div class="row">&nbsp;</div>

		</form>
		
	<?php
	}
}
?>