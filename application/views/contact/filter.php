<?php $contact_specialty = $this->config->item('contact_specialty');
$contact_email_blast     = $this->config->item('contact_email_blast');
$contact_record_limit       = $this->config->item('contact_record_limit');
$contact_marketing_data       = $this->config->item('contact_marketing_data');
?>
<style>
</style>
<div class="page-container contactlist-outmost">
    <div class="container">
        <div class="tab-pane">

            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Tag Filter <small></small></h1>
                </div>
                <div class="main_select_div">

                    <form method="post" id="tagfilter_form" action="<?php echo base_url() . 'contactlist'; ?>">
                        <div class="main_select_div-btn-1" style="float:right;padding:15px;">
                            <button type="submit" class="btn btn-primary">Filter Contact</a>
                        </div>
                </div>
                <div class="row">
                    <div class="col-md-2">

                        <select name="record_limit" class="form-control">
                            <option value="">Select All</option>
                            <?php foreach($contact_record_limit as $key => $row){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $row; ?></option>
                                <?php } ?>
                        </select>
                    </div>
                </div>
                <?php
                echo '<div class="row"><div class="col-lg-12" style = "font-weight: bold;background-color: #444d58;color: white;"><h4>Contact Specialty:  <span class="outer-chkbx"><input onclick = "check_all_filter()" type = "checkbox" id = "select_all" />Select All  </span><span class="outer-chkbx"><input onclick = "uncheck_all_filter()" type = "checkbox" id = "deselect_all" />DeSelect All</span></h4></div></div><div class="row" style="padding: 20px;">';
                foreach ($contact_specialty as $key => $specialty) {
                    echo '
												<span class="col-lg-3">
												
												<input type = "hidden" value = "' . $key . '" id = "contact_specialty_val_' . $key . '" name = "contact_specialty_val[]"/>
												
												<input type = "hidden" class = "tag_fields" value = "0" id = "contact_specialty_' . $key . '" name = "contact_specialty[]"/>
												
												<input id = "contact_specialty_tag_' . $key . '" class = "all_tags"  type = "checkbox"  onclick = tag_filter(' . $key . ',"contact_specialty",this.id) /><label style = "cursor:pointer;" > ' . $specialty . '</label>
                                               </span>';
                }

                echo '</div><div class="row"><div class="col-lg-12" style = "font-weight: bold;background-color: #444d58;color: white;"><h4>Marketing Data:  </h4></div></div><div class="row" style="padding: 20px;">';

                    foreach ($contact_marketing_data as $key => $mdata) {

                        echo '<span class="col-lg-3">

                                <input type = "hidden" value = "' . $key . '" id = "contact_mar_val_' . $key . '" name = "contact_mar_val[]"/>
                                                
                                <input type = "hidden" class = "tag_fields" value = "0" id = "contact_mar_' . $key . '" name = "contact_mar[]"/>
                                
                                <input id = "contact_mar_tag_' . $key . '" class = "all_tags"  type = "checkbox"  onclick = tag_filter(' . $key . ',"contact_mar",this.id) /><label style = "cursor:pointer;" > ' . $mdata . '</label>
                                </span>';
                    }

                echo '</div>';
                
                /* New Section Start */



                /* New Section Start */

                echo '</div><div class="row"><div class="col-lg-12" style = "font-weight: bold;background-color: #444d58;color: white;"><h4>Marketing Blast:  <span class="outer-chkbx"><input onclick = "check_all_filter_marketing()" type = "checkbox" id = "select_all_marketing" />Select All  </span><span class="outer-chkbx"><input onclick = "uncheck_all_filter_marketing()" type = "checkbox" id = "deselect_all_marketing" />DeSelect All</span></h4></div></div><div class="row" style="padding: 20px;">';

                // hide two element from this array...
                unset($contact_email_blast[10]);
                unset($contact_email_blast[11]);
                unset($contact_email_blast[8]);

                foreach ($contact_email_blast as $key => $email_blast) {

                    echo '
                                          <span class="col-lg-3">
                        						<input type = "hidden" value = "' . $key . '" id = "contact_email_blast_val_' . $key . '" name = "contact_email_blast_val[]" />
                        						<input type = "hidden" class = "tag_fields_marketing" value = "0" id = "contact_email_blast_' . $key . '" name = "contact_email_blast[]"/>

                        						<input id = "contact_email_blast_tag_' . $key . '"   class = "all_tags_marketing"  type = "checkbox" onclick = tag_filter(' . $key . ',"contact_email_blast",this.id) /><label style = "cursor:pointer;" > ' . $email_blast . '</label>
                        					</span>';
                }
                echo "</div>";


                echo '<div class="row"><div class="col-lg-12" style = "font-weight: bold;background-color: #444d58;color: white;"><h4>Lender Data:</h4></div></div><div class="row" style="padding: 20px;">'; ?>

                <div class="col-md-3">
                        <label>Contacts w/ Lender Account:</label>
                        <select class="chosen" name="lenderwithacc">
                            <option value="">Select One</option>
                            <?php foreach($fetch_lender_option as $ldata){ ?>
                                <option value="<?php echo $ldata['id'];?>"><?php echo $ldata['text'];?></option>
                            <?php } ?>
                        </select>
                </div>

                <?php 
                echo "</div>";


                echo '<div class="row"><div class="col-lg-12" style = "font-weight: bold;background-color: #444d58;color: white;"><h4>Borrower Data:</h4></div></div><div class="row" style="padding: 20px;">'; ?>

               <div class="col-md-3">
                        <label>Contacts w/ Borrower Account:</label>
                        <select class="chosen" name="borrowerwithacc">
                            <option value="">Select One</option>
                            <?php foreach($fetch_borrower_option as $bdata){ ?>
                                <option value="<?php echo $bdata['id'];?>"><?php echo $bdata['text'];?></option>
                            <?php } ?>
                        </select>
                </div>
            <?php
                echo "</div>";

            ?>

                </form>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
<style>
.chosen-container.chosen-container-single {
    width: 250px !important;
    margin-top: 13px !important;
    
}
.chosen-container-single .chosen-single {
    padding: 4px 0px 28px 10px !important;
    border-radius: 3px !important;
    border: 1px solid #e5e5e5 !important;
}
.chosen-container .chosen-drop{
    border: 1px solid #e5e5e5 !important;
}
</style>
<script>

    $(".chosen").chosen();

    function tag_filter(value, tag_type, id) {

        if ($('#' + tag_type + '_tag_' + value).prop('checked') == true) {

            $('#' + tag_type + '_' + value).val('1');
            $('#' + tag_type + '_val_' + value).val(value);
            $('#deselect_all').prop('checked', false);

        } else {
            $('#' + tag_type + '_' + value).val('0');

        }
    }

    function check_all_filter() {

        if ($('#select_all').prop('checked') == true) {
            $('.tag_fields').val('1');
            $('.all_tags').prop('checked', true);
            $('#deselect_all').prop('checked', false);
        } else {

            $('.tag_fields').val('0');
            $('.all_tags').prop('checked', false);
        }
    }

    function check_all_filter_marketing() {
        if ($('#select_all_marketing').prop('checked') == true) {
            $('.tag_fields_marketing').val('1');
            $('.all_tags_marketing').prop('checked', true);
            $('#deselect_all_marketing').prop('checked', false);
        } else {

            $('.tag_fields_marketing').val('0');
            $('.all_tags_marketing').prop('checked', false);
        }
    }

    function uncheck_all_filter() {
        if ($('#deselect_all').prop('checked') == true) {

            $('.all_tags').prop('checked', false);
            $('#select_all').prop('checked', false);
            $('.tag_fields').val('0');
        } else {
            $('.tag_fields').val('0');
            $('.all_tags').prop('checked', false);
        }
    }

    function uncheck_all_filter_marketing() {
        if ($('#deselect_all_marketing').prop('checked') == true) {

            $('.all_tags_marketing').prop('checked', false);
            $('#select_all_marketing').prop('checked', false);
            $('.tag_fields_marketing').val('0');
        } else {
            $('.tag_fields_marketing').val('0');
            $('.all_tags_marketing').prop('checked', false);
        }

    }
</script>