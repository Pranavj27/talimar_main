<style type="text/css">
	/*.row.borrower_data_row.borrower_textarea_div {
    padding: 2px !important;
}*/
form.history-filter {
    border-top: 1px solid gray;
}
.main-left-div-res .notes_class.follo_up,.main-left-div-res .notes_class.follo_up_date,.main-left-div-res .notes_class.follo_up_status,.main-left-div-res .notes_class{
	width: 28% !important;
}
.notes_class.types {
    margin-left: 43px !important;
}
.spc-loan-div.purpose {
    margin-left: 43px !important;
}
.row.borrower_data_row.borrower_textarea_div .col-md-4 {
    width: 30% !important;
}
</style>
<div class="page-container load-screen">
	<!-- BEGIN PAGE HEAD -->
	<div class="container" id="page-viewcontact">
		<div class="tab-pane">
			<!--------------------MESSEGE SHOW-------------------------->
			<?php if ($this->session->flashdata('error') != '') {?>
				<div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error'); ?></div><?php }?>
			<?php if ($this->session->flashdata('success') != '') {?>
				<div id='success'><i class='fa fa-thumbs-o-up'></i> <?php echo $this->session->flashdata('success'); ?></div><?php }?>
			<div id="chkbox_err"></div>
			<!-------------------END OF MESSEGE-------------------------->
			<div class="page-head">
				<div class="main_select_div">
					<!------------------Buttons Section Starts--------------------------->
					<div class="row margin-top-10">
						<div class="lender_data_row main_actions">
							<div class="form-group">
								<div class="">
									<?php if (is_numeric($this->uri->segment(2))) {
										if (isset($contact_id)) {
											if ($user_role == '2') {?>
												<button type="button" class="btn btn-danger" onclick="delete_contact()" <?php echo $disabled; ?>>Delete</button>
											<?php
											}
										}
										$fetch_user_id = $this->User_model->query("SELECT user_id FROM contact WHERE contact_id = '" . $this->uri->segment(2) . "' AND user_id = '" . $this->session->userdata('t_user_id') . "'");
										if ($fetch_user_id->num_rows() > 0) {

											$acces_contcat = 1;
										} else {
											$acces_contcat = 2;
										}
										if ($loginuser_contacts == 1) {
											$blocked = '';
										} elseif ($loginuser_contacts == 2 && $acces_contcat == 1) {
											$blocked = '';
										} else {
											$blocked = 'disabled';
										}
										?>
										<a id="singlebutton"  class="btn btn-primary" href="<?php echo base_url() . 'contact/' . $this->uri->segment(2); ?>" <?php echo $blocked; ?>>Edit</a>
										<a id="singlebutton"  class="btn btn-primary" href="<?php echo base_url() . 'contactlist'; ?>" >Close</a>
									<?php 
									}
									?>
								 </div>
								</div>
							</div>
						</div>
						<!------------------End Buttons Section--------------------------->
					</div>
				</div>
				<div class="page-container">
					<!-- BEGIN PAGE BREADCRUMB -->
					<ul class="page-breadcrumb breadcrumb hide">
						<li>
							<a href="#">Home</a><i class="fa fa-circle"></i>
						</li>
						<li class="active">
							 Dashboard
						</li>
					</ul>
					<!-- END PAGE BREADCRUMB -->
					<input type="hidden" name="borower_contact_type_id" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_contact_id : ''; ?>">
					<input type="hidden" name="lender_contact_type_id" value="<?php echo isset($fetch_lender_contact_type) ? $fetch_lender_contact_type[0]->lender_contact_id : ''; ?>">
					<input type="hidden" name="contact_id" value="<?php echo isset($contact_id) ? $contact_id : ''; ?>" >
					<!------------------  MAIN  LEFT DIV ENDS ------------------------>
					<div class="main-left-div">
				   		<div class="main-left-div-res">
							<div class="main-title row borrower_data_row" >
								<div class="col-md-3">
  									<div class="<?php echo $class; ?>"><?php echo $msg; ?> </div>
										<h3>Contact Information</h3>
									</div>
								</div>
								<div class="row borrower_data_row inline-label" id="phone1_field">
									<div class="col-md-3">
										<label >Name:</label>
									</div>
									<div class="col-md-4">
										<label style="font-weight:normal !important;" >
											<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname : ''; ?>	
										</label>
									</div>
			                        <div class="col-md-3">
			                            <label>Legal First Name:</label>
			                        </div>
									<div class="col-md-4">
										<label style="font-weight:normal !important;"  class=" phone-format">
											<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_suffix : ''; ?>
										</label>
									</div>
								</div>
								<div class="row borrower_data_row inline-label" id="phone1_field">
									<div class="col-md-3">
										<label >Company:</label>
									</div>
									<div class="col-md-4">
										<label style="font-weight:normal !important;"  class=" phone-format">
											<?php echo isset($contact_id) ? $fetch_contact_data[0]->company_name_1 : ''; ?>	
										</label>
									</div>
									<div class="col-md-3">
										<label >Website:</label>
									</div>
									<div class="col-md-4">
										<label style="font-weight:normal !important;"  class="">
											<a target="_blank" href="<?php echo $fetch_contact_data[0]->contact_website; ?>">
												<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_website : ''; ?>
											</a>
										</label>
									</div>
								</div>
								<div class="row borrower_data_row inline-label" id="">
									<div class="col-md-3">
										<label >Primary Phone:</label>
									</div>
									<div class="col-md-4">
										<label style="font-weight:normal !important;"  class="phone-format">
											<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_phone : ''; ?>
										</label>
									</div>
									<div class="col-md-3">
										<label >Phone Type:</label>
									</div>
									<div class="col-md-4">
										<label style="font-weight:normal !important;"  class="phone-format">
											<?php echo isset($contact_id) ? $contact_number_option[$fetch_contact_data[0]->primary_option] : ''; ?>
										</label>
									</div>
									<div class="col-md-3">
										<label >Phone Extension:</label>
									</div>
									<div class="col-md-4">
										<label style="font-weight:normal !important;"  class="phone-format">
											<?php echo isset($contact_id) ? $fetch_contact_data[0]->phone_extension : ''; ?>
										</label>
									</div>
								</div>
			                    <?php 
			                    if($fetch_contact_data[0]->alter_phone != ''){ ?>
			    					<div class="row borrower_data_row inline-label">
			    						<div class="col-md-3">
			    							<label >Secondary Phone:</label>
			    						</div>
			    						<div class="col-md-4">
			    							<label style="font-weight:normal !important;"  class="phone-format">
			    								<?php echo isset($contact_id) ? $fetch_contact_data[0]->alter_phone : ''; ?>
			    							</label>
			    						</div>
			    						<div class="col-md-3">
			    							<label>Phone Type:</label>
			    						</div>
			    						<div class="col-md-4">
			    							<label style="font-weight:normal !important;"  class="phone-format">
			    								<?php echo isset($contact_id) ? $contact_number_option[$fetch_contact_data[0]->alter_option] : ''; ?>
			    							</label>
			    						</div>
			    						<div class="col-md-3">
			    							<label>Phone Extension:</label>
			    						</div>
			    						<div class="col-md-4">
			    							<label style="font-weight:normal !important;"  class="phone-format">
			    								<?php echo isset($contact_id) ? $fetch_contact_data[0]->alter_phone_extension : ''; ?>
			    							</label>
			    						</div>
			    					</div>
			                    <?php 
			                	} ?>
								<div class="row borrower_data_row inline-label" id="">
									<div class="col-md-3">
										<label>Primary E-Mail:</label>
									</div>
									<div class="col-md-4">
										<label style="font-weight:normal !important;"  class=" ">
											<a href="mailto:<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_email : ''; ?>">
												<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_email : ''; ?>
											</a> 
										</label>
									</div>
									<div class="col-md-3">
										<label>E-Mail Type:</label>
									</div>
									<div class="col-md-4">
										<label style="font-weight:normal !important;"  class=" ">
											<?php echo isset($contact_id) ? $email_types_option[$fetch_contact_data[0]->pemail_type] : ''; ?>
										</label>
									</div>
								</div>
			                    <?php 
			                    if($fetch_contact_data[0]->contact_email2 != ''){ ?>
			    					<div class="row borrower_data_row inline-label" id="">
			    						<div class="col-md-3">
			    							<label>Secondary E-Mail:</label>
			    						</div>
			    						<div class="col-md-4">
			    							<label style="font-weight:normal !important;"  class="">
			    								<a href="mailto:<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_email2 : ''; ?>">
			    									<?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_email2 : ''; ?>
			    								</a>
			    							</label>
			    						</div>
			    						<div class="col-md-3">
			    							<label>E-Mail Type:</label>
			    						</div>
			    						<div class="col-md-4">
			    							<label style="font-weight:normal !important;"  class=" ">
			    								<?php echo isset($contact_id) ? $email_types_option[$fetch_contact_data[0]->aemail_type] : ''; ?>
			    							</label>
			    						</div>
			    					</div>
			                    <?php 
			                	}?>
								<div class=" borrower_data_row inline-label" id="">
									<div class="col-md-3">
										<label >Address:</label>
									</div>
									<div class="col-md-9">
										<label style="font-weight:normal !important;"  class=" "><?php echo ((isset($contact_id)) && ($fetch_contact_data[0]->contact_address)) ? $fetch_contact_data[0]->contact_address : ''; ?></label>
										<label style="font-weight:normal !important;"  class=" "><?php echo ((isset($contact_id)) && ($fetch_contact_data[0]->contact_unit)) ? $fetch_contact_data[0]->contact_unit . '; ' : ';'; ?></label>
										<label style="font-weight:normal !important;"  class=""><?php echo ((isset($contact_id)) && ($fetch_contact_data[0]->contact_city)) ? $fetch_contact_data[0]->contact_city . ', ' : ''; ?></label>
										<label style="font-weight:normal !important;"  class=""><?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_state . ' ' : ''; ?></label>
										<label style="font-weight:normal !important;"  class=""><?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_zip : ''; ?></label>
									</div>
								</div>
								<div class=" borrower_data_row inline-label"  style ="float: left;width: 100%;" id="">
									<?php
			                        if(in_array('16', $fetch_contact_specialty)) {
			                        	$code = 'Yes';
			                        } else {
			                        	$code = 'No';
			                        }
			                        ?>
									<div class="col-md-5" style="width:15%;">
										<strong>Trust Deed Investor: </strong>
									</div>
									<div class="col-md-5">
										<label><?php echo $code; ?></label>
									</div>
								</div>
								<div class=" borrower_data_row inline-label"  style ="float: left;width: 100%;" id="">
									<div class="col-md-3" >
										<label style="font-size: 14px !important;">Tags:</label>
									</div>
									<div class="tags_display">
										<?php
										if($fetch_sql_tags) {
											foreach (array_filter($fetch_contact_specialty) as $con_specialty) {
												$bb[$con_specialty] = $con_specialty;
											}
											foreach (array_filter($fetch_contact_email_blast) as $con_email_blast) {
												$dd[$con_email_blast] = $con_email_blast;
											}
											foreach ($contact_specialty as $key => $specialty) {
												if ($key == $bb[$key]) {
													echo '<div  class="col-md-4"><label >' . $specialty . '</label></div>	';
												}
											}
											foreach ($contact_email_blast as $key => $email_blast) {
												if ($key == $dd[$key]) {
													echo '<div  class="col-md-4"><label>' . $email_blast . '</label></div>	';
												}
											}
										}
										?>
									</div>
								</div>
								<div class="row borrower_data_row" id="">
									<div class="col-md-2">
										<label style="padding-left: 0px !important;" ><strong>Internal Contact:</strong></label>
									</div>
									<div class="col-md-8">
										<label style="margin-top:0px;margin-left:-15px;"><?php echo isset($contact_id) ? $fetch_user_name : ''; ?></label>
									</div>
								</div>
								<div class="row borrower_data_row inline-label" id="">
									<?php
									if($user_role == '2'){
									?>
										<div class="col-md-3">
											<label >Assigned:</label>
										</div>
								 	<?php 
									}?>
									<div class="tags_display" style="font-weight:normal !important">
										<?php
										if($fetch_sql_assigned) {
											$fetch_contact_permission = unserialize($fetch_sql_assigned[0]->contact_permission);
											foreach ($fetch_contact_permission as $con_permission) {
												$ee[$con_permission] = $con_permission;
											}
											foreach ($contact_permission as $key => $permission) {
												if ($ee[$key] == 3) {
													echo '<div  class="col-md-4"><label >Selected User: ' . $fetch_sql_user_name . '</label></div>	';
												} else if ($key == $ee[$key]) {
													echo '<div  class="col-md-4"><label  >' . $permission . '</label></div>	';
												}
											}
										}
										?>
									</div>
								</div>
								<div class="row borrower_data_row  inline-label">
									<div class="col-md-3">
										<label style="padding-left: 0px !important;" >Marketing Data:</label>
									</div>
									<div class="col-md-8">
										<label style="margin-top: 6px;"><?php echo isset($contact_id) ? $contact_marketing_data[$fetch_contact_data[0]->contact_marketing_data] : ''; ?></label>
									</div>
								</div>
								<div class="row borrower_data_row  inline-label" >
									<div class="col-md-3">
										<label style="padding-left: 0px !important;" >Referral Source:</label>
									</div>
									<div class="col-md-8">
										<label style="margin-top: 6px;">
											<?php echo isset($contact_id) ? $referral_source[$fetch_contact_data[0]->referral_source] : ''; ?>	
										</label>
									</div>
								</div>
								<div class=" borrower_data_row inline-label">
									<div class="col-md-3">
										<label><strong>Facebook:</strong></label>
									</div>
									<div class="col-md-4">
										<label style="font-weight:normal !important;"  class=" ">
											<a target="_blank" href ="<?php echo $fetch_contact_data[0]->fb_profile; ?>">
												<?php echo ((isset($contact_id)) && ($fetch_contact_data[0]->fb_profile)) ? $fetch_contact_data[0]->fb_profile : ''; ?>
											</a>
										</label>
									</div>
								</div>
								<div class="row"></div>
								<div class=" borrower_data_row inline-label">
									<div class="col-md-3">
										<label><strong>Linkedin:</strong></label>
									</div>
									<div class="col-md-9">
										<label style="font-weight:normal !important;"  class=" ">
											<a target="_blank" href ="<?php echo $fetch_contact_data[0]->lkdin_profile; ?>">
												<?php echo ((isset($contact_id)) && ($fetch_contact_data[0]->lkdin_profile)) ? $fetch_contact_data[0]->lkdin_profile : ''; ?>
											</a>
										</label>
									</div>
							    </div>
								<div class="row"></div>
								<div class=" borrower_data_row inline-label">
									<div class="col-md-3">
										<label><strong>Bigger Pockets:</strong></label>
									</div>
									<div class="col-md-9">
										<label style="font-weight:normal !important;"  class=" ">
											<a target="_blank" href ="<?php echo $fetch_contact_data[0]->bigger_pocket_profile; ?>">
												<?php echo ((isset($contact_id)) && ($fetch_contact_data[0]->bigger_pocket_profile)) ? $fetch_contact_data[0]->bigger_pocket_profile : ''; ?>
											</a>
										</label>
									</div>
								</div>
								<div class="row"></div>
								<div class="row borrower_data_row  inline-label" >
									<div class="col-md-3">
										<label style="padding-left: 0px !important;" >About:</label>
									</div>
									<div class="col-md-8">
										<label><?php echo isset($contact_id) ? $fetch_contact_data[0]->contact_about : ''; ?></label>
									</div>
								</div>
								<div class="row"></div>
								<div class="borrower_data_row  inline-label" >
									<form method="post" action="<?php echo base_url() ?>contact_p_notes">
										<input type="hidden" name="contact_id" value="<?php echo isset($contact_id) ? $contact_id : ''; ?>" >
										<div class="col-md-12">
											<div class="col-md-3" id="p_loan">
												<label>Personal Notes:</label>
											</div>
											<div class="col-md-8">
												<?php $a = $fetch_p_notes->result_object;?>
												<textarea name="p_notes" type="text" class="form-control" rows="3" placeholder="Add Personal Notes Here...">
													<?php echo $a[0]->personal_note; ?>
												</textarea>
											</div>
											<div class="col-md-1">
												<button style="vertical-align:top;margin-left:-25px;" class="btn blue" type="submit">Save</button>
											</div>
										</div>
									</form>
								</div>
								<?php
								$tCountpaidoff = 0;
								foreach ($fetch_paidoff_loan_latest as $row) {
									$tCountpaidoff++;
								}
								$tActiveCount = 0;
								foreach ($fetch_active_loan_latest as $r) {
									$tActiveCount++;
								}
								$tpipeCount = 0;
								foreach ($fetch_pipe_loan_latest as $r) {
									$tpipeCount++;
								}
								$BorrowerTransaction = $tCountpaidoff + $tActiveCount + $tpipeCount;
								foreach ($active_loan as $row) {
									$lenderActivecount = $row['count_loan'];
								}
								foreach ($paidoff_loan as $row) {
									$lenderpaidoffcount = $row['count_loan'];
								}
								$LEndertransaction = $lenderActivecount + $lenderpaidoffcount;
								?>
			                   <div class="new-notes">
									<div class="title row borrower_data_row ">
										<div class="col-md-3">
											<h3>Transaction Count</h3>
										</div>
									</div>
									<div class="col-md-4">
										<label>&nbsp;&nbsp;Borrower: <a onclick="borrowerTransectionform(this);"><?php echo $BorrowerTransaction; ?></a></label>
									</div>
									<div class="col-md-4">
										<label>&nbsp;&nbsp;Lender: <a onclick="lenderTransectionform(this);"><?php echo $LEndertransaction; ?></a></label>
									</div>
									<div class="col-md-4">
										<label>&nbsp;&nbsp;Affiliated: <a onclick="AffiliatedTransectionform(this);"><?php echo $affi_total_count; ?></a></label>
									</div>
								</div>
								<!------------------ NEW NOTES DIV STARTS ---------------------------->
								<div class="new-notes">
									<?php 
									if(is_numeric($this->uri->segment(2))) { ?>
										<form method="post" name="" action="<?php echo base_url() . 'contact_notes/' . $contact_id; ?>">
											<div class="title row borrower_data_row ">
												<div class="col-md-3">
													<h3>Contact Notes</h3>
												</div>
												<!-- 12-07-2021 By @aj -->
												<div class="col-md-2">
													<a class="btn blue" data-toggle="modal" href="<?php echo base_url().'add_contact_note/'.$this->uri->segment(2);?>"><i class="fa fa-plus" aria-hidden="true"></i> Add Note </a>
												</div>
												<?php 
												foreach ($fetch_check_user as $key => $row) {
					                                if($row->msg != ''){ ?>
						                                <div class="col-md-3">
						                                    <h5><?php echo $row->msg.' <strong>'.date('m-d-Y', strtotime($row->date)); ?></strong>.</h5>
						                                </div>
					                                <?php 
					                            	}else{ ?>
						    							<div class="col-md-3">
						    								<h5>
						    									<strong><?php echo $row->fname . ' ' . $row->middle_name . ' ' . $row->lname; ?></strong> added 
						    									<strong>
						    										<?php echo isset($row->contact_id) ? $fetch_contact_data[0]->contact_firstname . ' ' . $fetch_contact_data[0]->contact_middlename . ' ' . $fetch_contact_data[0]->contact_lastname : ''; ?>
						    									</strong> on <strong><?php echo date('m-d-Y', strtotime($row->date)); ?></strong>.
						    								</h5>
						    							</div>
													<?php 
													} 
												}?>
											</div>
											<div class="row11">
												<div class="row  borrower_data_row inline-label2" id="">
													<div class="notes_class">
														<label class="title_history" >Date:</label>
														<input type="hidden" value ="1" name="notes[]"/>
														<input type="text" class="dates_class form-control datepicker" name="notes_date[]" id="notes_date_1" value="<?php echo date('m-d-Y'); ?>" placeholder="MM-DD-YYYY" />
													</div>
													<div class="notes_class">
														<label class="title_history" >Type:</label>
														<select id="notes_type_1" name="notes_type[]" class="form-control" data-live-search="true" style="width:150px !important;">
															<?php 
															foreach ($notes_type as $key => $type) {?>
																<option value="<?php echo $key; ?>" <?php echo $selected; ?> ><?php echo $type; ?></option>
															<?php 
															}?>
														</select>
													</div>
													<div class="spc-loan-div notes_class ">
														<label class="title_history">Purpose :</label>
														<select id="purpose_type" name ="purpose_type[]" class="form-control"  style="width:130px !important;">
																<?php foreach ($contact_purpose_menu as $key => $types) { ?>
																	<option value="<?php echo $key; ?>" ><?php echo $types; ?></option>
																<?php }?>
														</select>
													</div>
													<input style="width:100px;float: right;" id ="formsubmit" value="Save" type="submit" name="submit" class="btn btn-primary b_right"/>
												</div>
												<div class="row borrower_data_row borrower_textarea_div" >
													<div class="col-md-9">
														<textarea class="form-control" name="personal_notes[]" placeholder="Text:" id="personal_notes_1" >
														</textarea>
													</div>
												</div>
											</div>
										</form>
									<?php 
									}?>
								</div>
								<!------------------ SAVED NOTES DIV STARTS ------------------------>
								<div class="saved-notes <?php echo $permission_class; ?>" >
									<div id="list_contact_note_data"></div>

									<div id="loader_link_btn">
										<input type="hidden" name="notes_page_no" id="notes_page_no" value="1">
										<a  onclick="load_notes_data_new()">Load More<span class="loader_image"></span></a>
									</div>
								</div>
								<!------------------ SAVED NOTES DIV ENDS -------------------->
							</div> <!--res-->
						</div>
						<form id="lendertra_form" action="<?php echo base_url('loan_schedule_investor'); ?>" method="post">
							<input type="hidden" name="contact_id" value="<?php echo $this->uri->segment(2);?>">
						</form>
						<form id="affiliated_form" action="<?php echo base_url('affiliated_trancation'); ?>" method="post">
							<input type="hidden" name="contact_id" value="<?php echo $this->uri->segment(2);?>">
						</form>
						<form id="borrowertra_form" action="<?php echo base_url('loan_schedule_borrower'); ?>" method="post">
							<input type="hidden" name="contact_id" value="<?php echo $this->uri->segment(2);?>">
						</form>	
						<!-- Modal -->
						<!------------------  MAIN  LEFT DIV STARTS ------------------------>
						<!------------------  MAIN  RiGHT DIV STARTS ------------------------>
						<div class="main-right-div">
							<?php
							$fetch_contact_permission = unserialize($fetch_sql_assigned[0]->contact_permission);
							$fetch_particular_user_id = $fetch_sql_assigned[0]->particular_user_id;
							foreach ($fetch_contact_permission as $con_permission) {
								if ($con_permission == '3') {
									$user_idd[] = $fetch_particular_user_id;
									$user_rolee[] = '1';
								} else if ($con_permission == '2') {
									$user_rolee[] = '1';
								}
								if (($con_permission == '1') || ($con_permission == '2')) {
									$user_rolee[] = '2';
								}
							}
							if (($user_idd) && ($user_role == '1')) {
								if (in_array($user_id, $user_idd)) {
									$permission_class = 'show_acc_to_permission';
								} else {
									$permission_class = 'hide_acc_to_permission';
								}
							}
							if (($user_rolee) && ($user_role == '2')) {
								if ((in_array('2', $user_rolee)) && ($user_role == '2')) {
									$permission_class = 'show_acc_to_permission';
								} else {
									$permission_class = 'hide_acc_to_permission';
								}
							} else {
								$permission_class = 'show_acc_to_permission';
							}
							if ($user_role == '1') {
								if ((in_array('1', $user_rolee)) && ($user_role == '1')) {
									$permission_class = 'show_acc_to_permission';
								}
							}
							?>
							<form method="POST" action="<?php echo base_url(); ?>Contact/update_contact_data" id="formID" name="formID_bro" >
								<input type="hidden" name="borower_contact_type_id" value="<?php echo isset($fetch_borrower_contact_type) ? $fetch_borrower_contact_type[0]->borrower_contact_id : ''; ?>">
								<input type="hidden" name="lender_contact_type_id" value="<?php echo isset($fetch_lender_contact_type) ? $fetch_lender_contact_type[0]->lender_contact_id : ''; ?>">
								<input type="hidden" name="contact_id" value="<?php echo isset($contact_id) ? $contact_id : ''; ?>" >
								<input type="hidden" name="first_name" value="<?php echo $fetch_contact_data[0]->contact_firstname; ?>" >
								<input type="hidden" name="middle_name" value="<?php echo $fetch_contact_data[0]->contact_middlename; ?>" >
								<input type="hidden" name="last_name" value="<?php echo $fetch_contact_data[0]->contact_lastname; ?>" >
								<div class="contactlist_list_modal_form"></div>
								<!---  Borrower Data modal   Starts---->
								<!---  Borrower Data modal  End ---->
        						<!---  Lender Data modal   Starts---->
       							<!---  Lender Data modal   End---->
								<!---  Contact Tags modal  Starts---->
								<!--- Contact Tags modal  End-->
								<!---  Assigned modal   Starts---->
								<!--- Assigned modal  End ---->
                        	</form>
							<hr style="width:97%;margin-left:15px;float: left;">
							<div class="row" id="model_left_align">								
								<div class="col-md-12">&nbsp;</div>
								<div class="col-md-12">
									<a class="btn btn-primary borrower_data_pop"  data-toggle="modal" href="#borrower_data" style="width: 55% !important;">Borrower Data </a>
								</div>								
								<div class="col-md-12">&nbsp;</div>
								<div class="col-md-12">
                                	<?php                                  
                                    if (in_array('16', $fetch_contact_specialty)) {
                                        echo '<a class="btn btn-primary" data-toggle="modal" href="#lender_data" onclick="load_modal(this)"  style="width: 55% !important;">Lender Data </a>';
                                    } else {                                        
                                        echo '<a class="btn btn-primary"  style="width: 55% !important;" disabled>Lender Data </a>';
                                    }
                                	?>									
								</div>
								<div class="col-md-12">&nbsp;</div>
								<div class="col-md-12">
									<a class="btn btn-primary" data-toggle="modal" href="#BrokerData" style="width: 55% !important;">Broker Data</a>
								</div>
                                <div class="col-md-12">&nbsp;</div>
                                <div class="col-md-12">
                                    <a class="btn btn-primary" data-toggle="modal" href="#hardmoney" style="width: 55% !important;">Hard Money Program</a>
                                </div>
                                <div class="col-md-12">&nbsp;</div>
                                <div class="col-md-12">
                                    <a class="btn btn-primary" data-toggle="modal" href="#Marketing" style="width: 55% !important;">Marketing</a>
                                </div>								
								<div class="col-md-12">&nbsp;</div>
								<div class="col-md-12">&nbsp;</div>
								<div class="col-md-12">
									<a class="btn  btn-primary" data-toggle="modal" href="#contact_document" style="width: 55% !important;">Contact Documents</a>
								</div>
							</div>
							<div class="col-md-12 "><br><br></div>
                            <div class="col-md-12 ">
								<label style="font-weight:600 !important;">Contact Tasks:</label>
								<?php                              
                                if (isset($fetch_contact_task)) {
                                	foreach ($fetch_contact_task as $key => $row) {
                                	?>
										<div class="relation_labels" id="contact_task_<?php echo $row->id; ?>">
											<div class="col-md-12">
								    			<label>
                                    				<strong>Task Type: </strong> 
                                    				<a href="<?php echo base_url() . 'Contact/task_view/' . $row->contact_id . '/' . $row->id; ?>">
                                    					<?php echo is_numeric($row->contact_task) ? $selct_contact_task[$row->contact_task] : $row->contact_task; ?>
                                    				</a>
                                    			</label>
											</div>
											<div class="col-md-12">
												<label><strong>Date: </strong> <?php echo date('m-d-Y', strtotime($row->contact_date)); ?></label>
											</div>
											<div class="col-md-12">
												<label><strong>Note: </strong> <?php echo $row->task_notes; ?></label>
											</div>
											<div class="col-md-12">
												<a class="col-md-1" title="Edit" id="<?php echo $row->id; ?>" onclick="edit_contact_task(this.id);"><i class="fa fa-edit" aria-hidden="true"></i></a>
												<a class="col-md-1" title="Delete" id="<?php echo $row->id; ?>" onclick="delete_contact_task(this.id);"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
											</div>
										</div>
										<div class="row"></div>
										<form method ="post" action="<?php echo base_url(); ?>edit_contact_task" name="forms_edit_contact_task" id="forms_edit_contact_task">
											<input type="hidden" name="contact_id" value ="<?php echo $row->contact_id; ?>">
											<input type="hidden" name="row_id" value ="<?php echo $row->id; ?>">
											<div class="col-md-12 select_task_<?php echo $row->id; ?>" style="display:none;">
												<div class="row">
													<label><strong>Task Type: </strong> </label>												
													<select name="contact_task" style="width:50% !important;">
														<?php 
														foreach ($selct_contact_task as $key => $option) {?>
															<option value="<?php echo $key; ?>" <?php if ($row->contact_task == $key) {echo 'selected';}?>><?php echo $option; ?></option>
														<?php 
														}?>
													</select>
												</div>
												<div class="row">
													<label><strong>Date: </strong></label>
													<input style="width:70%;" type="text" class="datepicker" name="contact_date" value="<?php echo date('m-d-Y', strtotime($row->contact_date)); ?>">
												</div>
												<div class="row">
													<label style="vertical-align:top;"><strong>Note: </strong></label>	
													<textarea style="width:70%;" type="text" name="edit_task_notes" rows="3">
														<?php echo $row->task_notes; ?>
													</textarea>
												</div>
												<div class="row">
													<label><strong>Status: </strong></label>
													<select name="contact_status" style="width:50% !important;">
														<?php 
														foreach ($contact_status_option as $key => $option) {?>
															<option value="<?php echo $key; ?>" <?php if ($key == $row->contact_status) {echo 'Selected';}?>><?php echo $option; ?></option>
														<?php }?>
													</select>
													<button type="submit" class="btn blue">Save</button>
												</div>
											</div>
										</form>
										<div class="row"></div>

									<?php 
									} 
								} ?>
								<div class="col-md-12">
									<a class="btn blue" data-toggle="modal" href="#contact_modal_tasks"><i class="fa fa-plus" aria-hidden="true"></i> Add </a>
								</div>
							</div>
							<hr style="float: left;width: 100%;">
							<div class="col-md-12 ">
								<label style="font-weight:600 !important;">Relationships:</label>
								<!----------------- FOR 'WAS REFERRED' --------------->
								<?php
								if(isset($contact_relationship_data)) {
									foreach ($contact_relationship_data as $relationship_name) {?>
										<form id="editrelationfrm<?php echo $relationship_name['cr_id']; ?>" method="post" action="<?php echo base_url() . 'Contact/edit_contact_relationship' ?>" name="forms_edit_contact_relationship" id="forms_edit_contact_relationship">
											<div class=" relation_labels">
												<div class="col-md-8 contact_relationship_name<?php echo $relationship_name['contact_id']; ?>">
													<label><span> <a href="<?php echo base_url() . 'viewcontact/' . $relationship_name['contact_id']; ?>"><?php echo $relationship_name['contact_firstname']; ?> </a></span></label>
												</div>
												<div class="col-md-4">
													<input type="hidden" value="<?php echo $this->uri->segment(2); ?>" id="x_id">
													<a title="Edit" id="<?php echo $relationship_name['cr_id']; ?>" onclick="edit_relationship_details(this)"><i class="fa fa-edit" aria-hidden="true"></i></a>&nbsp;&nbsp;
														<!---view button start------->
													<a title="View" id="<?php echo $relationship_name['cr_id']; ?>" onclick="view_relationship_details(this);"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;&nbsp;
													<!---view button end------->
													<a title="Delete" id="<?php echo $relationship_name['cr_id']; ?>" onclick="contact_relationship_action(this)"><i class="fa fa-trash-o" aria-hidden="true"></i></a>&nbsp;&nbsp;
												</div>
											</div>
										</form>

									<?php	
									} 
								} ?>
								<!---------------------- FOR REFERRED --------------->
								<?php
								if($relationship_contact_names) {
									foreach ($relationship_contact_names as $key => $relationship_name) {
									?>
										<form id="editrelationgfrm<?php echo $relationship_id[$key]; ?>" method="post" action="<?php echo base_url() . 'Contact/edit_contact_relationship' ?>">
											<input type="hidden" name="contact_id" value="<?php echo $this->uri->segment(2); ?>">
											<input type="hidden" name="relationship_action" id="relationship_action_<?php echo $relationship_id[$key]; ?>" value="">
											<input type="hidden" name="relationship_id" id="relationship_id_<?php echo $relationship_id[$key]; ?>" value="<?php echo $relationship_id[$key]; ?>">
											<input type="hidden" name="relationship_contact_id" id="relationship_contact_id<?php echo $relationship_id[$key]; ?>" value="<?php echo $relationship_contact_id[$key]; ?>">
											<div class=" relation_labels">
												<div class="col-md-6 contact_relationship_name<?php echo $relationship_id[$key]; ?>">
													<label>
														<a href="<?php echo base_url() . 'viewcontact/' . $relationship_contact_id[$key]; ?>">
															<?php echo $relationship_name; ?>
														</a>
													</label>
												</div>
												<div class="col-md-9 select_relationship<?php echo $relationship_id[$key]; ?>" style="display:none;">
													<select  onchange="fill_relationship_id(<?php echo $relationship_id[$key]; ?>,this.value)" class="chosen" style="">
														<option value=''>SEARCH </option>
															<?php foreach ($fetch_all_contact as $row) {
															if ($row->contact_id == $relationship_contact_id[$key]) {
															$selected = 'selected';
															} else {
															$selected = '';
															} ?>
															<option <?php echo $selected; ?> value="<?php echo $row->contact_id; ?>"  ><?php echo $row->contact_firstname . ' ' . $row->contact_middlename . ' ' . $row->contact_lastname; ?></option>
															<?php } ?>
													</select>
												</div>
												<div class="col-md-3 contact_relationship_name"></div>
												<div class="col-md-3">
													<a title="Edit" id="<?php echo $relationship_id[$key]; ?>" onclick="edit_relationship_details(this)"><i class="fa fa-edit" aria-hidden="true"></i></a>&nbsp;&nbsp;
													<!---view button start------->
													<a title="View" id="<?php echo $relationship_id[$key]; ?>" onclick="view_relationship_details(this);"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;&nbsp;
													<!---view button end------->
													<a title="Delete" id="<?php echo $relationship_id[$key]; ?>" onclick="contact_relationship_action(this.id,'delete')"><i class="fa fa-trash-o" aria-hidden="true"></i></a>&nbsp;&nbsp;
												</div>
											</div>
										</form>
									<?php 
									} 
								} ?>
								<!--<hr style="float: left;width: 100%;">-->
								<div class="col-md-12 ">
									<a class="btn blue" data-toggle="modal" href="#add_relationship"><i class="fa fa-plus" aria-hidden="true"></i> Add a Relationship</a>
								</div>
							</div>
							<!------------------ MAIN RiGHT DIV ENDS -------------------------------->
							<hr style="float: left;width: 100%;">
							<div class="col-md-12 ">
								<label style="font-weight:600 !important;">History:</label>
								<?php 
								foreach ($data_fetch_tags as $k => $r) { 
									if ($r['email'] != '') { ?>
										<h5>
											<strong>
												<?php echo date('m/d/Y', strtotime($r['modified_date'])); ?></strong> – Modified by <strong><?php echo $r['email']; ?>
											</strong>
										</h5>
									<?php 
									}
								}?>
								<?php
                        		foreach ($fetch_check_user as $key => $row) {
                            		if($row->msg != ''){ ?>
                                		<h5><?php echo $row->msg.' <strong>'.date('m-d-Y', strtotime($row->date)); ?></strong>.</h5>
                                	<?php 
                                	}else{ ?>
						          		<h5><strong><?php echo date('m/d/Y', strtotime($row->date)); ?></strong> – Created by <strong><?php echo $row->fname . ' ' . $row->middle_name . ' ' . $row->lname; ?></strong></h5>
									<?php  
									} 
								}?>
							</div>
							<hr style="float: left;width: 100%;">
						</div>
					</div>
				</div>
			</div>
			<form id="delete_contact_form" method="POST" action="<?php echo base_url(); ?>delete_contact">
				<input type="hidden" value="<?php echo $contact_id; ?>" name="contact_id">
				<input type="hidden" value="<?php echo $contact_id; ?>" name="contact_id">
			</form>

			<!----------------- loan borrower report ------------------->
			<form method="POST"  action="<?php echo base_url(); ?>loan_schedule_borrower" id="loan_borrower_reportdata">
				<input type="hidden" name="contact_ids" >
			</form>
			<!----------------- loan borrower report ------------------->

			<!----------------- lender trust deed report ------------------->
			<form method="POST"  action="<?php echo base_url(); ?>loan_schedule_investor" id="trust_deed_reportdata">
				<input type="hidden" name="contact_ids" >
			</form>
		</div>
	</div>
</div>
<input type="hidden" name="popup_modal_hit" id="popup_modal_hit" value="<?php echo $this->session->flashdata('popup_modal_hit'); ?>">
<input type="hidden" name="contact_list_id" id="contact_list_id" value="<?php echo $contact_id_list; ?>">
<input type="hidden" name="browser_data_disclouser_id" id="browser_data_disclouser_id" value="<?php echo $ee; ?>">
<div class="contactlist_list_modal"></div>
<!----------------- lender trust deed report ------------------->
<script type="text/javascript">								
	function addmoreswag(){
		var count = $('#Marketing #marktingdata .row').length;									
		count++;
		$('#Marketing #marktingdata').append('<input type="hidden" name="rowid[]" value="new"><div class="row" id="row_'+count+'"><div class="col-md-1"><label style="visibility:hidden;">Action:</label><a title="Remove" onclick="remove_row('+count+');"><i class="fa fa-trash"></i></a></div><div class="col-md-2"><label style="visibility:hidden;">Status:</label><select class="form-control" name="title_status[]"><?php foreach($contact_marketing_status_option as $key => $row){ ?><option value="<?php echo $key;?>"><?php echo $row;?></option><?php } ?></select></div><div class="col-md-2"><label style="visibility:hidden;">Date:</label><input type="text" placeholder="Enter Date" name="date[]" class="form-control datepicker" value=""></div><div class="col-md-2"><label style="visibility:hidden;">Requested By:</label><select class="form-control" name="contact_id[]">  <?php $contactLenth = count($fetch_all_contact); foreach($fetch_all_contact as $key => $rowContact){ ?><option value="<?php echo $rowContact->contact_id;?>"><?php echo str_replace("'", '', $rowContact->contact_firstname) . ' ' . str_replace("'", '', $rowContact->contact_lastname) ;?></option> <?php if($contactLenth==$key){break;}} ?>  </select></div><div class="col-md-2"><label style="visibility:hidden;">Item:</label><input type="text" name="swag[]" placeholder="Enter Swag" class="form-control" value=""></div><div class="col-md-3"><label style="visibility:hidden;">Note:</label><textarea type="text" placeholder="Enter Note..." name="note[]" class="form-control" rows="2"></textarea></div></div>');
		$(".datepicker").datepicker({ dateFormat: 'mm-dd-yy' });
	}
	function remove_row(rowid){
		$('#Marketing #marktingdata #row_'+rowid).remove();
	}
	function removedata(id){
		if(confirm('Are you sure to remove this?')){
			$.ajax({
						type : 'POST',
						url  : '<?php echo base_url()."Contact_marketing/removerow"?>',
						data : {'id':id},
						success : function(response){

							alert('Row removed successfully');
							window.location.reload();
						}
			});
		}
	}
</script>
<script type="text/javascript">
	function display_package_date_of_re(t){
		var val1 = t;
		var get_date=$('input.RE_date_val').val();
		var date_two='<?php echo date("m-d-Y", strtotime("-1 year")) ?>';
		if(val1 == '1' && date_two <= get_date){
			$('input.RE_date_val').attr('disabled',false);
			$('input#status_check').val('Current');
		}
		else if(val1 == '1' && date_two >= get_date){
			$('input.RE_date_val').attr('disabled',false);
			$('input#status_check').val('Expired');
		}else{
			$('input.RE_date_val').attr('disabled',true);
			$('input#status_check').val('Not Complete');
		}
	}
	var k= 1;
	function notes_add_more_row(){
		 k++;
		var notes_type = '<?php
		foreach ($notes_type as $key => $type) {
			if ($key == '') {
				$selected = 'selected="selected"';
			} else {
				$selected = '';
			}
			echo '<option value="' . $key . '"  ' . $selected . ' >' . $type . '</option>';

		}?>';
		var loan_id = '<?php
		foreach ($fetch_loans as $key => $row) {
			if ($fetch_loans[$key]['id']) {
				if ($fetch_loans[$key]['id'] == $loan_id) {
					$selected = 'selected="selected"';
				} else {
					$selected = '';
				}
			} else {
				$selected = '';
			}
			echo '<option value="' . $fetch_loans[$key]['id'] . '" ' . $selected . ' >' . $fetch_loans[$key]["text"] . '</option>';
		}?>';
		var content = '<div class="row borrower_data_row" ><div class="col-md-3"><input type="hidden" value ="'+k+'" name="notes[]"/><input type="text" class="form-control datepicker" name="notes_date[]" id="notes_date_'+k+'" value="" placeholder="MM-DD-YYYY" /></div><div class="col-md-3"><select id="notes_type_'+k+'" name="notes_type[]" class=" form-control" data-live-search="true" >'+notes_type+'</select></div><div class="col-md-3"><select name="loan_id[]" onchange="loan_form_submit(this.value)" class="form-control" data-live-search="true">'+loan_id+'</select></div></div><div class="row borrower_data_row borrower_textarea_div" ><div class="col-md-9"><textarea class="form-control" name="personal_notes[]" placeholder="Text:" id="personal_notes_'+k+'" ></textarea></div></div>';
		$( ".main_notes" ).append(content);
		$('.datepicker').datepicker({ dateFormat: 'mm-dd-yy' });
		/* $('.selectpicker').selectpicker('render');*/
		$('.selectpicker').selectpicker('refresh');
		/* $('#notes_type_'+k+'').data('selectpicker');*/
	}
</script>
<script type="text/javascript" src="<?php echo base_url("assets/js/contact/contactlist-modal.js"); ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/contact/viewcontact.css"); ?>">

