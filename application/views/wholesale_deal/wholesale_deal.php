<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<div class="page-container">
	<style type="text/css">
		.row.margin-top-10 {
	    margin-top: 144px !important;
	}
	section{
	padding: 11px 181px;
    margin: 0px;
  }
  .chosen-container-single .chosen-single, .pro_Head .chosen-container-single .chosen-single, .page-head .chosen-container-single .chosen-single{
  	height: 44px !important;
  }
  input,select{
  	height: 44px !important;
  }

  p.error {
    height: 5px;
}
.select2-container .select2-selection--single{
		height: 44px !important;
    text-align: inherit;
    padding: 6px;
	}
	span.select2-selection.select2-selection--single {
    height: 44px !important;
    text-align: inherit;
    padding: 6px;
}
h4.divd{
	background: #eeeeee;
    padding: 7px 3px;
    font-weight: 600;	
}
    
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
<?php
$loan_id=$this->uri->segment(3);
if($loan_id)
{
	$fetch['id'] = $loan_id;
	$fetch_loan_data = $this->User_model->select_where('loan',$fetch);
	$fetch_loan_result = $fetch_loan_data->result();
	 // echo '<pre>'; print_r($fetch_loan_result); echo '</pre>';


	foreach($fetch_loan_result as $row){
		
		$talimar_loan 			= $row->talimar_loan;
		$fci 					= $row->fci;
		$borrower 				= $row->borrower;
		$loan_amount 			= $row->loan_amount;
		$current_balance 		= $row->current_balance;
		$lender_fee 			= $row->lender_fee.'%';
		$intrest_rate 			= $row->intrest_rate.'%';
		$term_month 			= $row->term_month;
		$per_payment_period 	= $row->per_payment_period;
		$payment_held_close 	= $row->payment_held_close;
		$calc_of_year 			= $row->calc_of_year;
		$loan_type 				= $row->loan_type;
		$transaction_type 		= $row->transaction_type;
		$position 				= $row->position;
		$payment_type 			= $row->payment_type;
		$exit_strategy 			= $row->exit_strategy;
		$loan_highlights 		= $row->loan_highlights;
		$baloon_payment_type 	= $row->baloon_payment_type;
		$payment_sechdule 		= $row->payment_sechdule;
		$intrest_type 			= $row->intrest_type;
		$payment_amount 		= $row->payment_amount;
		$minium_intrest 		= $row->minium_intrest;
		$min_intrest_month 		= $row->min_intrest_month;
		$owner_occupied 		= $row->owner_occupied;
		$owner_purpose 			= $row->owner_purpose;
		$costruction_loan 		= $row->costruction_loan;
		$project_cost 			= $row->project_cost;
		$total_project_cost 	= $row->total_project_cost;
		$borrower_enquity 		= $row->borrower_enquity;
		$grace_period 			= $row->grace_period;
		$late_charges_due 		= $row->late_charges_due;
		$default_rate 			= $row->default_rate."%";
		$return_check_rate 		= $row->return_check_rate;
		$refi_existing 			= $row->refi_existing;
		$loan_document_date 	= $row->loan_document_date ? date('m-d-Y',strtotime($row->loan_document_date)) : '';
		$loan_funding_date 		= $row->loan_funding_date  ? date('m-d-Y',strtotime($row->loan_funding_date)) : '';
		$first_payment_date 	= $row->first_payment_date ? date('m-d-Y',strtotime($row->first_payment_date)) : '';
		$maturity_date 			= $row->maturity_date ? date('m-d-Y',strtotime($row->maturity_date)) : '';
		$modify_maturity_date 	= $row->modify_maturity_date ? date('m-d-Y',strtotime($row->modify_maturity_date)) : '';
		$extention_option 		= $row->extention_option;
		$extention_month 		= $row->extention_month;
		$extention_percent_amount = $row->extention_percent_amount;
		// $ballon_payment 		= $row->ballon_payment;
		$ballon_payment 		= ($row->loan_amount) + (($row->intrest_rate/100) * $row->loan_amount / 12);
		
		$trustee 				= $row->trustee;
		$promissory_note_val 	= $row->promissory_note_val;
		$payment_requirement 	= $row->payment_requirement;
		$add_broker_data_desc 	= $row->add_broker_data_desc;
		$min_bal_type 			= $row->min_bal_type;
		$payment_gurranty 		= $row->payment_gurranty;
		$payment_gurr_explain 	= $row->payment_gurr_explain;
	}	
}else
{
	$talimar_loan 			= '';
	// $fci 					= date('y').'-T'.rand(1,999999);
	$fci 					= '';
	$borrower 				= '';
	$loan_amount 			= '00';
	$current_balance 		= '00';
	$lender_fee 			= '2.5%';
	$intrest_rate 			= '10%';
	$term_month 			= '12';
	$per_payment_period 	= '12';
	$payment_held_close 	= '6';
	$calc_of_year 			= '1';
	$loan_type 				= '2';
	$transaction_type 		= '1';
	$position 				= '1';
	$payment_type 			= '1';
	$payment_sechdule 		= '30';
	$intrest_type 			= '1';
	$payment_amount 		= '0.00';
	$minium_intrest 		= '';
	$min_intrest_month 		= '';
	$owner_occupied 		= '';
	$owner_purpose 			= '1';
	$costruction_loan 		= '';
	$project_cost 			= '0.00';
	$total_project_cost 	= '0.00';
	$borrower_enquity 		= '0.00';
	$grace_period 			= '10';
	$late_charges_due 		= '10';
	$default_rate 			= '17%';
	$return_check_rate 		= '150';
	$refi_existing 			= '0';
	$loan_document_date 	= '';
	$loan_funding_date 		= '';
	// $loan_funding_date 		= date('m-d-Y');
	$first_payment_date 	= '';
	$maturity_date 			= '';
	// $maturity_date 			=  date('m-d-Y', strtotime('+1 years'));
	$extention_option 		= '1';
	$extention_month 		= '6';
	$extention_percent_amount = '1%';
	$ballon_payment 		= '0.00';
	$trustee 				= 'FCI Lender Services';
	// $trustee 				= '';
	$promissory_note_val 	= '';
	$baloon_payment_type 	= '1';
	$payment_requirement 	= '0';
	$exit_strategy 			= 'Sale of Property';
	$loan_highlights 		= '';
	$add_broker_data_desc 	= '';
	$min_bal_type 			= '1';
	$payment_gurranty 		= '0'; // by default set with 1...
	$payment_gurr_explain 	= '';
	
}
?>
	<div class="container" >
		<!--------------------MESSEGE SHOW-------------------------->
			<?php if($this->session->flashdata('error')!=''){  ?>
				<div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error');?></div><?php } ?>
			<?php if($this->session->flashdata('success')!=''){  ?>
				<div id='success'><i class='fa fa-thumbs-o-up'></i> <?php echo $this->session->flashdata('success');?></div><?php } ?>
			<div id = "chkbox_err"></div>		 
			<!-------------------END OF MESSEGE-------------------------->
			
		<section>		
			
		<?php ////echo '<pre>'; print_r($the_street_add); echo '</pre>';?>
			<form name="add_wholesale_deal" id="add_wholesale_deal" method="POST" action="" >
				<div class="row"  style="margin-top: 33px;   margin-bottom: 33px; ">
					<div class="col-md-6">					
							<h3 style="margin-top: 6px"><b>Add</b></h3>
					</div>
					<div class="col-md-6 mn" >				
						<button type="submit" name="form_button" value="save" class="btn blue submit_frm" style="float: right;">Save</button>						
					</div>	
				</div>
				<!-- Status -->
				<?php /*
				<div class="row ">
					<div class="m">						
						<div class="col-md-4">
							<label  for="textinput">Status</label>
							<select class="form-control" name="status" id="status">
								<option value="" >Select One</option>
								<option value="Active" <?php echo ($contact_status == "Active")?'selected':'';?>>Active</option>
								<option value="Complete" <?php echo ($contact_status == "Complete")?'selected':'';?>>Complete</option>					
							</select>
							<p class="error"></p>
						</div>						
					</div>
					<div class="m">						
						<div class="col-md-4">
							<label  for="textinput">Date Entered</label>
								<input type="text"  placeholder = "MM-DD-YYYY" id="date_entered" class="date_entered  form-control"  value="<?php echo $contact_date;?>" name = "date_entered[]"  /> 
								<p class="error"></p>
						</div>						
					</div>
					<div class="m">						
						<div class="col-md-4">
							<label  for="textinput">Due Date</label>
								<input type="text"  placeholder = "MM-DD-YYYY" id="date_due" class="date_due  form-control"  value="<?php echo $due_date;?>" name = "date_due[]"  /> 
								<p class="error"></p>
						</div>						
					</div>
				</div>
				<div class="row">&nbsp;</div>	
				<div class="row">&nbsp;</div>		
				<?php */ ?>
				<!-- Due Date -->
				<?php 

				// echo '<pre>'; print_r($the_street_add); echo '</pre>';
				//echo '<pre>'; print_r(get_loan_street_address($talimar_loan)); echo '</pre>';

				
				?>
				<div class="row">
					<div class="m">						
						<div class="col-md-4">
							<label  for="textinput">Wholesale Deal</label>
							<select class="form-control " name="wholesale_deal" id="wholesale_deal">
								<option value="" >Select One</option>
								<?php foreach ($the_street_add as $key => $row) {  
								// $row['loan_status'] = '';
							if($row['loan_status']=='A'){
								$color = "style='color:green'";
							}else if($row['loan_status']=='C'){
								$color = "style='color:red'";
							}else{
									$color = '';
							}

							// $valueStatus =  $fetch_search_option[$key]['text']. ' <span '.$color.'>( '.$row['loan_status']. ' )</span>'; 

							$valueStatus =  $the_street_add[$key]['text']; ?>


					?>
					<option <?php echo $color; ?> value = "<?php echo $the_street_add[$key]['id']; ?>" ><?php echo $valueStatus; ?></option>
					<?php } ?>
																		
							</select>
							<p class="error"></p>
						</div>
					</div>
					<div class="m">						
						<div class="col-md-4">&nbsp;</div>
					</div>	
					<div class="m">						
						<div class="col-md-4">&nbsp;</div>
					</div>						
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>


				<div class="row">
					<div class="m">						
						<div class="col-md-6">
							<label  for="textinput">Post</label>
							<select class="form-control " name="post" id="post">
								<option value="no">No</option>
								<option value="yes">Yes</option>
							</select>
							<p class="error"></p>
						</div>
					</div>
					<div class="m">						
						<div class="col-md-6">
							<label  for="textinput">Status</label>
							<select class="form-control " name="wholesale_status" id="wholesale_status">
								<option value="available">Available</option>
								<option value="reserved">Reserved</option>
							</select>
							<p class="error"></p>
						</div>
					</div>
					<!-- <div class="m">						
						<div class="col-md-4">&nbsp;</div>
					</div> -->
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>

				<div class="row">
					<div class="m">						
						<div class="col-md-4">
							<label  for="textinput">Wholesaler</label>
							<select class="form-control select_name" name="wholesaler" id="wholesaler">
								<option value="">Select One</option>
																		
							</select>
							<p class="error"></p>
							
						</div>
					</div>
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>

				<div class="row">
					<div class="m">						
						<div class="col-md-6">
							<label  for="textinput">City</label>
							<input type="text" name="city" class="form-control city" value="">
							<p class="error"></p>
						</div>
					</div>
					<div class="m">						
						<div class="col-md-6">
							<label  for="textinput">State</label>
							<input type="text" name="state" class="form-control state" value="">
							<p class="error"></p>
						</div>
					</div>
					<!-- <div class="m">						
						<div class="col-md-4">&nbsp;</div>
					</div> -->
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>

				<div class="row">
					<div class="m">						
						<div class="col-md-4">
							<label  for="textinput">Purchase Price</label>
							<input type="text" name="purchase_price" class="form-control purchase_price" value="">
							<p class="error"></p>
						</div>
					</div>
					<div class="m">						
						<div class="col-md-4">
							<label  for="textinput">Rehab Cost</label>
							<input type="text" name="rehab_cost" class="form-control rehab_cost" value="">
							<p class="error"></p>
						</div>
					</div>
					<div class="m">						
						<div class="col-md-4">
							<label  for="textinput">Completion Value</label>
							<input type="text" name="completion_value" class="form-control state" value="">
							<p class="error"></p>

						</div>
					</div>
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>	
				

				<div class="row">
					<div class="m">						
						<div class="col-md-4">
							<label  for="textinput">Close Date</label>
							<input type="text" name="close_date" class="form-control close_date" value="">
							<p class="error"></p>
						</div>
					</div>
					<div class="m">						
						<div class="col-md-4">&nbsp;</div>
					</div>
					<div class="m">						
						<div class="col-md-4">&nbsp;</div>
					</div>
				</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>

				<h4 class="divd">Interest List</h4>

				<div class="row">&nbsp;</div>


				<div class="row">
					<div class="m">						
						<div class="col-md-3">
							<label  for="textinput">Name</label>
							<select class="form-control select_name" name="contact_name" id="contact_name">
								<option value="" >Select One</option>
																		
							</select>
							<p class="error"></p>

						</div>
					</div>
					<div class="m">						
						<div class="col-md-3">
							<label>Phone</label>
							<input type="" id="phone" class="phone  form-control"  value="" name = "phone" placeholder="(xxx)-xxx-xxxx" maxlength="14">
							<p class="error"></p>
						</div>
					</div>
					<div class="m">						
						<div class="col-md-3">
							<label>Primary E-Mail</label>
							<input type="email" id="email" class="email  form-control"  value="" name = "email" placeholder="abc@xyz.com">
							<p class="error"></p>
							
						</div>
					</div>
					<div class="m">						
						<div class="col-md-3">
							<label>Status</label>
							<select class="form-control" name="wholesale_status" id="wholesale_status">
								<option value="">Select One</option>
								<option value="interested">Interested</option>
								<option value="passed">Passed</option>
								<option value="due_diligence">Due Diligence</option>
								<option value="purchased">Purchased</option>
							</select>
							<p class="error"></p>
							
						</div>
					</div>
				</div>		
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>

				<div class="row">
					<div class="m">						
						<div class="col-md-4">
							<label  for="textinput">Image</label>
							<input type="file" name="wholesale_image" class="form-control" id="wholesale_image">
							<p class="error"></p>

						</div>
					</div>
					<div class="m">						
						<div class="col-md-4">&nbsp;</div>
					</div>
					<div class="m">						
						<div class="col-md-4">&nbsp;</div>
					</div>		

			</form>							
		</section>
	</div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>
<script type="text/javascript">
//$( ".close_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
$('#wholesaler , #contact_name').select2({
    // dropdownParent: $("#modal-compose") ,
    //placeholder: contact_name,
    minimumInputLength: 1,
    // allowClear: true,
    tags: true,
    ajax: {
        url: "/Task/contact_name_lists1",
        dataType: 'json',
        delay: 250,
        cache: false,
        data: function (params) {
            return {
                term: params.term,
                page: params.page || 1,
            };
        },
        processResults: function(data, params) {
           // console.log(data);
            //  NO NEED TO PARSE DATA `processResults` automatically parse it
            //var c = JSON.parse(data);
           // console.log(data);
           jQuery('ul#select2-contact_name-results li:first').prop('disabled',true);
            var page = params.page || 1;
            return {
                results: $.map(data, function (item) { return {id: item.id, text: item.col , source: item.total_count,}}),
                pagination: {
                // THE `10` SHOULD BE SAME AS `$resultCount FROM PHP, it is the number of records to fetch from table` 
                    more: (page * 10) <= data[0].total_count
                }
            };
        },              
    }
});
// $('#contact_name').append('<option value="'+cnoncat_ids+'" selected="selected">'+contact_name+'</option>');
jQuery('input.select2-search__field').keyup(function(){
	jQuery('ul#select2-contact_name-results li:first').prop('disabled',true);
});
// console.log(cnoncat_ids);
$(document).on( 'keyup' , '.select2-container input' , function() {
	console.log(jQuery(this).val());

	 setTimeout(function(){	  	
	  	jQuery('span.select2-results ul').find('li:first').hide();
	 }, 500);
	
	
});
</script>