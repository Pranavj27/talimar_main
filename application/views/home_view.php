<?php //$this->load->view("header.php") ;


?>	
<!DOCTYPE html>
<head>
	<meta charset="utf-8"/>
	<title>Talimar Financial </title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/admin/layout4/img/favicon1-32x32.png" sizes="32x32" />

	<link href="<?php echo base_url("assets/admin/pages/css/login.css"); ?>" rel="stylesheet" type="text/css"/>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url("assets/global/plugins/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url("assets/global/plugins/simple-line-icons/simple-line-icons.min.css"); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url("assets/global/plugins/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url("assets/global/plugins/uniform/css/uniform.default.css"); ?>" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="<?php echo base_url("assets/admin/pages/css/login.css"); ?>" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME STYLES -->
	<link href="<?php echo base_url("assets/global/css/components-rounded.css"); ?>" id="style_components" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url("assets/global/css/plugins.css"); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url("assets/admin/layout/css/layout.css"); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url("assets/admin/layout/css/themes/default.css"); ?>" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<?php echo base_url("assets/admin/layout/css/custom.css"); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url()."assets/css/vishnu.css"; ?>" rel="stylesheet" type="text/css"/>

</head>
<style>
	body {
		background-color: #364150;
	}
	.login {
		background-color: #c2c2c2 !important;
	}

	body {
		background-color: #c2c2c2;
	} 
</style>
<body>

<div class="login">
<div class="logo">
	<a href="<?php echo base_url(); ?>">
	<img src="<?php echo base_url("assets/admin/layout4/img/logo-big.png"); ?>" alt=""/>
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
<div  class="login-form">

<!----------------Success and Error message----------------->
				<?php if($this->session->flashdata('success')!=''){  ?>
				 <div id='success'><i class='fa fa-thumbs-o-up'></i> <?php echo $this->session->flashdata('success');?></div><?php } ?>
				 <?php if($this->session->flashdata('error')!=''){  ?>
				 <div id='error'><i class='fa fa-thumbs-o-down'></i> <?php echo $this->session->flashdata('error');?></div><?php } ?>
<!------------------------------------------------------>
	<!-- BEGIN LOGIN FORM -->
			<form method="POST" action="<?php echo base_url().'Home';?>">
			
			<legend>Login</legend>
			<div class="form-group">
				<label for="name">Username</label>
				<input class="form-control" name="email" placeholder="Enter your username" type="text" value="" />
				<span class="text-danger"></span>
			</div>
			<div class="form-group">
				<label for="name">Password</label>
				<input class="form-control" name="password" placeholder="Enter your password" type="password" value="" />
				<span class="text-danger"></span>
			</div>
			<div class="form-group">
				<input name="submit" type="submit" class="btn btn-info" value = "Login"> 
				
			<label class="rememberme check">
			<input type="checkbox" name="remember" value="1"/>Remember </label>
			<a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
			</div>
			</form>
		
		</div>
	<!-- END LOGIN FORM -->
	
	 <!-- BEGIN FORGOT PASSWORD FORM -->
	<form class="forget-form <?php echo $this->session->flashdata('class_1'); ?>" action="<?php echo base_url();?>forget_password" method="post"> 
		<legend>Forget Password ?</legend>
		<p>
			 Enter your e-mail address below to reset your password.
		</p>
		<div class="form-group">
			<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" required />
		</div>
		<div class="form-actions">
			<button type="button" id="back-btn" class="btn btn-default">Back</button>
			<button type="submit" class="btn btn-info pull-right">Submit</button>
		</div>
		<div class="form-group">
		
		</div>
	</form>
	<!-- END FORGOT PASSWORD FORM -->
	 

</div>

<div class="copyright"> 2017 - <?php echo date('Y');?> © TaliMar Financial.</div>


</div>

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo base_url("assets/global/plugins/jquery.min.js"); ?>" type="text/javascript"></script>
<script src="<?php echo base_url("assets/global/plugins/jquery-migrate.min.js"); ?>" type="text/javascript"></script>
<script src="<?php echo base_url("assets/global/plugins/bootstrap/js/bootstrap.min.js"); ?>" type="text/javascript"></script>
<script src="<?php echo base_url("assets/global/plugins/jquery.blockui.min.js"); ?>" type="text/javascript"></script>
<script src="<?php echo base_url("assets/global/plugins/uniform/jquery.uniform.min.js"); ?>" type="text/javascript"></script>
<script src="<?php echo base_url("assets/global/plugins/jquery.cokie.min.js"); ?>" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url("assets/global/plugins/jquery-validation/js/jquery.validate.min.js"); ?>" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url("assets/global/scripts/metronic.js"); ?>" type="text/javascript"></script>
<script src="<?php echo base_url("assets/admin/layout/scripts/layout.js"); ?>" type="text/javascript"></script>
<script src="<?php echo base_url("assets/admin/layout/scripts/demo.js"); ?>" type="text/javascript"></script>
<script src="<?php echo base_url("assets/admin/pages/scripts/login.js"); ?>" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {     
Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Login.init();
Demo.init();
});
</script>


<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

