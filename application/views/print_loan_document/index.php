<div class="page-content-wrapper">
	<div class="page-content">
			<?php if($this->session->flashdata('error')!=''){  ?>
				 <div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error');?></div><?php } ?>
				  <?php if($this->session->flashdata('success')!=''){  ?>
				 <div id='success'><i class='fa fa-thumbs-o-up'></i><?php echo $this->session->flashdata('success');?></div><?php } ?>
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Loan Data <small></small></h1>
					
						
					
				</div>
				
				
				<!-- END PAGE TITLE -->
				<!-- BEGIN PAGE TOOLBAR -->
				
				<!-- END PAGE TOOLBAR -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					 Dashboard
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			
			<div class="print_document_div_section">
						 <div class="row" >
						 <input type="checkbox" onclick="select_all_print_loan(this)" id="print_select_all">Select All<br>
						 <input type="checkbox" onclick="deselect_all_print_loan(this)" id="print_deselect_all">Deselect All<br>
						 </div>
							<table id="table_print_document">
							<tr>
								<td><input type="checkbox" name="loan_term">Loan Term</td>
								<td><input type="checkbox" name="fair_lending_notice">Fair Lending Notice</td>
							</tr>
							
							<tr>
								<td><input type="checkbox" name="lender_closing_statement">Lender Closing Statement
								</td>
								<td><input type="checkbox" name="fedral_equal_opprtunity_act">Federal Equal Opportunity Act
								</td>
							</tr>
							
							<tr>
								<td><input type="checkbox" name="escrow_instrusction">Broker / Lender Escrow Instructions
								</td>
								<td><input type="checkbox" name="first_payment_notify">First Payment Notification</td>
							</tr>
							
							<tr>
								<td><input type="checkbox" name="arbitration_of_disputes">Arbitration of Disputes

								</td>
								<td><input type="checkbox" name="insurance_endrosement">Insurance Endorsement Request
								</td>
							</tr>
							
							<tr>
								<td><input type="checkbox" name="automated_valuation_notice">Automated Valuation Model Notice
								</td>
								<td><input type="checkbox" name="privacy_notice_disclosure">Privacy Notice Disclosure
								</td>
							</tr>
							
							
							<tr>
								<td><input type="checkbox" name="authorization_to_release">Authorization to Release Information
								</td>
								<td><input type="checkbox" name="promissory_note_secure_trust">Promissory Note Secured by Deed of Trust </td>
							</tr>
							
							<tr>
								<td><input type="checkbox" name="ballon_payment_disclosures">Balloon Payment Disclosures
								</td>
								<td><input type="checkbox" name="attachment_a_construct_draw">Attachment A—Construction Draws
								</td>
							</tr>
							<tr>
								<td></td>
								<td><input type="checkbox" name="fci_construction_disbursement_agreement">FCI Construction Disbursement Agreement
								</td>
							</tr>
							
							<tr>
								<td><input type="checkbox" name="borrower_certification">Borrower Certification & Authorization</td>
								<td><input type="checkbox" name="default_provision">Default Provision Rider</td>
							</tr>
							
							<tr>
								<td><input type="checkbox" name="california_insurance_disclosure">California Insurance Disclosure
								</td>
								<td><input type="checkbox" name="ach_construction_draw">ACH Construction Draw Deposit
								</td>
							</tr>
							
							<tr>
								<td><input type="checkbox" name="capacity_to_repay_disclosure">Capacity to Repay Disclosure
								</td>
								<td><input type="checkbox" name="transfer_servicing_disclosure">Transfer of Servicing Disclosure (RESPA)
								</td>
							</tr>
							
							<tr>
								<td><input type="checkbox" name="certificate_bussiness_purpose_loan">Certificate of Business Purpose Loan</td>
								<td><input type="checkbox" name="payment_gurranty">Payment Guaranty
								</td>
							</tr>
							
							<tr>
								<td><input type="checkbox" name="compliance_agreement">Compliance Agreement
								</td>
							</tr>
							
							<tr>
								<td><input type="checkbox" name="consumer_notice">Consumer Notices
								</td>
							</tr>
							
							<tr>
								<td><input type="checkbox" name="credit_score_disclosure">Credit Score Disclosure
								</td>
							</tr>
							
							<tr>
								<td><input type="checkbox" name="deed_trust_assigment_rents">Deed of Trust w/ Assignment of Rents</td>
							</tr>
							
							<tr>
								<td><input type="checkbox" name="exclusive_devt_ratio">Excessive Debt Ratio</td>
							</tr> 
							
							</table>
						 </div>
			
	</div>
	<!-- END CONTENT -->
</div>

