<?php
		$loan_type_option			= $this->config->item('loan_type_option');
		$loan_status_option			= $this->config->item('loan_status_option');
		$closing_status_option		= $this->config->item('closing_status_option');
		$serviceing_reason_option	= $this->config->item('serviceing_reason_option');
		
?>	
<!-- BEGIN CONTENT -->
<style>
.rc_class{
	padding:0px;
}
.rc_class td{
	text-align:left;
}
</style>
	<div class="page-container">
			<!-- BEGIN PAGE HEAD -->
		
		<div class="container">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>Portfolio Dashboard </h1>
				</div>
				<?php 
				error_reporting(0);
				// echo "<pre>";
				// print_r($active_loan_result);
				// echo "</pre>";
				
				// echo "<pre>";
				// print_r($pipeline_loan_result);
				// echo "</pre>";
				
				// echo "<pre>";
				// print_r($paidoff_loan_result);
				// echo "</pre>";
				
				// echo "<pre>";
				// print_r($all_pipeline_data);
				// echo "</pre>";
				
				// echo "<pre>";
				// print_r($trust_deed);
				// echo "</pre>";
				
				/* echo "<pre>";
				print_r($default_loan);
				echo "</pre>"; */
				$default_total_amount = 0;
				$default_count_number = 0;
				foreach($default_loan as $key => $row)
				{
					$default_total_amount = $default_total_amount + $default_loan[$key]['loan_amount'];
					$default_count_number++;
				}
				// echo $default_count_number;
				?>
				
				<div class="row rc_class">
					
					
					<div class="col-md-4">
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead class="thead_dark_blue">
							<tr>
								<th colspan="2">Active Loan Portfolio</th>
							</tr>
							</thead>
							
							<tbody>
								<tr>
									<td style = "text-align: left;" ># of Loans</td>
									<td style = "text-align: left;"><?php echo $active_loan_result['active_count'];?></td>
								</tr>
								
								<tr>
									<td style = "text-align: left;">Portfolio Balance</td>
									<td style = "text-align: left;" >$<?php echo number_format($active_loan_result['total_loan_amount']);?></td>
								</tr>
								<?php
									$total_intrest_percent = number_format($active_loan_result['total_intrest_rate'],3);
									$total_intrest_dollar = $active_loan_result['total_loan_amount'] * ($total_intrest_percent/1200)/$active_loan_result['active_count'];
									
									//$active_ltv = (($active_loan_result['total_loan_amount'] + $active_loan_result['senior_current'] ) /$active_loan_result['total_arv'])*100;
									
								$active_ltv = ($active_loan_result['total_loan_amount']/$active_loan_result['total_future_value'])*100;
									
								$active_ltv_total = $active_ltv / $active_loan_result['active_count']; 
									
									/* echo'<pre>';
									print_r($fetch_property_data);
									echo'</pre>'; */
									/* echo $active_ltv;
									echo'<br>';
									echo $active_ltv_total; */
									
								?>
								<tr>
									
									<td style = "text-align: left;">Avg. Loan Amount</td>
									<td style = "text-align: left;">$<?php echo number_format($active_loan_result['total_loan_amount']/$active_loan_result['active_count']); ?></td>
								</tr>
								
								<tr>
									<td style = "text-align: left;">Avg. Loan Rate</td>
									<td style = "text-align: left;" ><?php echo number_format($total_intrest_percent, 2); ?>%</td>
								</tr>
								
								<tr>
									<td style = "text-align: left;" >Avg. LTV</td>
									<td style = "text-align: left;" ><?php echo number_format($active_ltv_total,2);?>%</td>
								</tr>
								
								<tr>
									<td style = "text-align: left;" >Avg. Term</td>
									<td style = "text-align: left;"><?php echo number_format($active_loan_result['total_term_month'],1).' Months';?></td>
								</tr>
								
								<tr>
									<td style = "text-align: left;">Loans in Default #</td>
									<td style = "text-align: left;" ><?php echo $default_count_number; ?></td>
								</tr>
								
								<tr>
									<td style = "text-align: left;">Loans in Default $</td>
									<td style = "text-align: left;" ><?php echo '$'.number_format($default_total_amount); ?></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<div class="col-md-4">
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead class="thead_dark_blue">
							<tr>
								<th colspan="2">Loan Servicing</th>
							</tr>
							</thead>
							
							<tbody>
								
								<!--<tr>
									<td style = "text-align: left;" ># of Loans w/ Loan Servicer</td>
									<td style = "text-align: left;" ><?php echo $fetch_serviced_loans['count_loans'];?></td>
								</tr>
								
								<tr>
									<td style = "text-align: left;" >$ of Loans w/ Loan Servicer</td>
									<td style = "text-align: left;" ><?php echo '$'.number_format($fetch_serviced_loans['sum_loan_amount']); ?></td>
								</tr>-->
								
								<!--<tr>
									<td style = "text-align: left;">Total # of Assigned Interests</td>
									<td style = "text-align: left;" ><?php echo $lender_count_active; ?></td>
								</tr>
								
								<tr>
									<td style = "text-align: left;" >Avg. # of Lenders / Loan</td>
									<td style = "text-align: left;" ><?php echo number_format($lender_count_active/$active_loan_result['active_count'],2);?></td>
								</tr>
								
								<tr>
									<td style = "text-align: left;" >Avg. $ Assigned Interest</td>
									<td style = "text-align: left;" ><?php echo '$'.number_format($active_loan_result['total_loan_amount']/$lender_count_active); ?></td>
								</tr>
								
								<tr>
									<td style = "text-align: left;">Avg. Loan Spread</td>
									<td style = "text-align: left;"><?php echo number_format($fetch_spread_data['sum_servicing']/$fetch_spread_data['count_loans'],2); ?>%</td>
								</tr>
								-->
																
								
								<!--								
								<tr>
									<td style = "text-align: left;">Loan Servicing Net Income</td>
									<td style = "text-align: left;" ><?php echo '$'.number_format($total_servicing_income); ?></td>
								</tr>-->
									<tr>
									<td style = "text-align: left;"><a href="<?php echo base_url();?>loan_boarding_schedule">Loans in Boarding #</a></td>
									<td style = "text-align: left;"><?php echo $count_loan_boarding_schdule; ?></td>
								</tr>
								<tr>
									<td style = "text-align: left;">Loans in Boarding $</td>
									<td style = "text-align: left;"><?php echo '$'.number_format($total_loan_boarding_amount); ?></td>
								</tr>
								<tr>
									<td style = "text-align: left;" ><a href="<?php echo base_url();?>property_insurance">Insurance Request</td>
									<td style = "text-align: left;" ><?php echo $fetch_property_loan; ?></td>
								</tr>
								<tr>
									<td style = "text-align: left;" >Assignments in Process #</td>
									<td style = "text-align: left;" ><?php echo $count_assigment_in_process; ?></td>
								</tr>
								<tr>
									<td style = "text-align: left;" >Assignments in Process $</td>
									<td style = "text-align: left;" ><?php echo '$'.number_format($investment_assigment_in_process); ?></td>
								</tr>
								<tr>
									<td style = "text-align: left;" >Monthly Net Servicing Income</td>
									<td style = "text-align: left;" >$<?php echo number_format($total_servicing_income); ?></td>
								</tr>
								
									
								<tr>
									<td style = "text-align: left;" style = "text-align: left;">Monthly Servicing Cost
									<td style = "text-align: left;" ><?php echo '$'.number_format($total_servicing_cost); ?></td>
								</tr>
								<tr>
									<td style = "text-align: left;" ><a href="<?php echo base_url();?>upcoming_payoff_schedule">Upcoming Payoffs #</a></td>
									<td style = "text-align: left;" ><?php echo number_format($payoff_report_count_payoff); ?></td>
								</tr>
								<tr>
									<td style = "text-align: left;" >Upcoming Payoffs $</td>
									<td style = "text-align: left;" ><?php echo '$'.number_format($payoff_report_amount); ?></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<div class="col-md-4">
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead>
								<tr class="thead_dark_blue">
									<th colspan="5">Available Trust Deeds</th>
								</tr>
								<tr>
									<th>Loan #</th>
									<th>Available</th>
									<th>Loan Amount</th>
									<th>Lender Yield</th>
									<th>Loan Status</th>
								</tr>
							</thead>
							<tbody>
								
								<?php
									$number = 0;
									$total_loan_amount = 0;
									$total_available	= 0;
									if(isset($trust_deed)){
										
										foreach($trust_deed as $key => $row)
										{
											$available = $trust_deed[$key]['loan_amount'] - $trust_deed[$key]['total_investment'];
											$lender_yield = $trust_deed[$key]['intrest_rate'] - $trust_deed[$key]['servicing_fee']
								?>
									<tr>
										<td style = "text-align: left;" >
											<a href="<?php echo base_url().'load_data/'.$trust_deed[$key]['loan_id']; ?>"><?php echo $trust_deed[$key]['talimar_loan'];?></a>
										</td>
										<td style = "text-align: left;">
											$<?php echo number_format($available);?>
										</td>
								
										<td style = "text-align: left;" >
											$<?php echo number_format($trust_deed[$key]['loan_amount']);?>
										</td>
										<td style = "text-align: left;" >
											<?php echo number_format($lender_yield,2);?>%
										</td>
										<td style = "text-align: left;" >
											<?php echo $loan_status_option[$trust_deed[$key]['loan_status']];?>
										</td>
									</tr>
									<?php 
										$total_loan_amount = $total_loan_amount + $trust_deed[$key]['loan_amount'];
										$total_available = $total_available + $available;
										$number++;
									} }else {?>
									<tr>
										<td style = "text-align: left;"  colspan="5">No Data Found!</td>
									</tr>
									<?php } ?>
							</tbody>
							<tfoot class="foot_light_blue">
							<tr>
								<th>Total: <?php echo $number; ?></th>
								
								<th>$<?php echo number_format($total_available); ?></th>
								<th>$<?php echo number_format($total_loan_amount); ?></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
						</table>	
					</div>
					
				</div>
				
				
				<div class="row rc_class">
					<!-------------- PIPELINE TABLE STARTS ------------->
					<div class="col-md-12">
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead >
								<tr class="thead_dark_blue">
									<th colspan="9">Pipeline</th>
								</tr>
								<tr>
									<th>Loan #</th>
									<th>Property Address</th>
									<th>Borrower</th>
									<th>Loan Amount</th>
									<th>Interest Rate</th>
									<th>LTV</th>
									<th>Term</th>
									<th>Closing Status</th>
									<th>Closing Date</th>
									
									
								</tr>
							</thead>
							<tbody>
								
								<?php
									$key 						= 0;
									$total_pipeline_available 	= 0;
									$total_interst_rate 		= 0;
									$total_loan_amount			= 0;
									$total_ltv					= 0;
									$total_term					= 0;
									if(isset($all_pipeline_data)){
										
										// echo'<pre>';
										// print_r($all_pipeline_data);
										// echo'</pre>';  
									
										foreach($all_pipeline_data as $key => $row)
										{
											if($all_pipeline_data[$key]['talimar_loan']){
											$calculate_lender_points = $all_pipeline_data[$key]['loan_amount'] * ($all_pipeline_data[$key]['lender_fee']/100);
											
											$total_pipeline_available = $total_pipeline_available + $all_pipeline_data[$key]['avilable'];
										
										$loan_to_value = (($all_pipeline_data[$key]['loan_amount'] + $all_pipeline_data[$key]['select_ecum']) / $all_pipeline_data[$key]['uv_value']) * 100;
										$loan_to_value = is_infinite($loan_to_value) ? 0 : $loan_to_value;
										$total_interst_rate += 	$all_pipeline_data[$key]['intrest_rate'];
										$total_loan_amount  +=  $all_pipeline_data[$key]['loan_amount'];	
										$total_ltv  +=  $loan_to_value;	
										$total_term += $all_pipeline_data[$key]['term_month'];	
								?>
									<tr>
										<td style = "text-align: left;">
											<a href="<?php echo base_url();?>load_data/<?php echo $all_pipeline_data[$key]['loan_id'];?>"><?php echo $all_pipeline_data[$key]['talimar_loan']; ?>
										</a></td>
										<td style = "text-align: left;">
											<?php echo $all_pipeline_data[$key]['property_address']; ?>
										</td>
										<td style = "text-align: left;" >
											<?php echo $all_pipeline_data[$key]['borrower_name']; ?>
										</td>
										<td style = "text-align: left;">
											$<?php echo number_format($all_pipeline_data[$key]['loan_amount']); ?>
										</td>
										<td style = "text-align: left;" ><?php echo number_format($all_pipeline_data[$key]['intrest_rate'],3)?>%</td>
										
										
										<td style = "text-align: left;" ><?php echo is_infinite($loan_to_value) ? 0 : number_format($loan_to_value,2); ?>%</td>
										<td style = "text-align: left;" ><?php echo $all_pipeline_data[$key]['term_month']?> Months</td>
										
										<td style = "text-align: left;" >
										<?php echo $closing_status_option[$all_pipeline_data[$key]['closing_status']];?>
										</td>
										<td style = "text-align: left;" >
											<?php echo $all_pipeline_data[$key]['funding_date'] ? date('m-d-Y',strtotime($all_pipeline_data[$key]['funding_date'])) : ''; ?>
										</td>
									
									
									<?php $key++;?>
									</tr>
										<?php }} }else {?>
									<tr>
										<td style = "text-align: left;" colspan="4">No Data Found!</td>
									</tr>
									<?php } ?>
							</tbody>
							<tfoot class="foot_light_blue">
								<tr>
									<th>Total / Average</th>
									<th></th>
									<th></th>
									<th>$<?php echo number_format($pipeline_loan_result['total_loan_amount']);?></th>
									<th><?php echo number_format($total_interst_rate/$key,3);?>%</th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									
								</tr>
								<tr>
									<th>Average</th>
									<th></th>
									<th></th>
									<th>$<?php echo number_format($total_loan_amount/$key,2);?></th>
									<th></th>
									<th><?php echo number_format($total_ltv/$key,2);?>%</th>
									<th><?php echo number_format($total_term/$key);?> Months</th>
									<th></th>
									<th></th>
									
								</tr>
							</tfoot>
							
						</table>	
					</div>
					<!-------------- PIPELINE TABLE ENDS ------------->
					
					<!------- AVAILABLE TRUST DEEDS TABLE STARTS 
					
					<div class="col-md-4">
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead class="thead_dark_blue">
							<tr>
								<th colspan="2">Pipeline Loan Portfolio</th>
							</tr>
							</thead>
							
							<tbody>
								<tr>
									<td style = "text-align: left;"  ># of Loans</td>
									<td style = "text-align: left;" ><?php echo $pipeline_loan_result['pipeline_count'];?></td>
								</tr>
								
								<tr>
									<td style = "text-align: left;">Loan Balance</td>
									<td style = "text-align: left;">$<?php echo number_format($pipeline_loan_result['total_loan_amount']);?></td>
								</tr>
								
								<tr>
									<?php
									$total_intrest_percent = number_format($pipeline_loan_result['total_intrest_rate'],2);
									// $total_intrest_dollar = $pipeline_loan_result['total_loan_amount'] * ($total_intrest_percent/1200)/$pipeline_loan_result['pipeline_count'];

									// old
									// $loan_to_value = (($pipeline_loan_result['total_loan_amount'] + $pipeline_loan_result['senior_current']) / $pipeline_loan_result['total_arv'])* 100;
									// echo $loan_to_value .'lone to <br>';
									// new
									$loan_to_value = $pipeline_loan_result['ltv_av'];
									
									// $total_intrest_dollar  = total_loan_amount * ((total_intrest/100)/12(months))
									?>
									<td style = "text-align: left;">Avg. Balance</td>
									<td style = "text-align: left;">$<?php echo number_format($pipeline_loan_result['total_loan_amount']/$pipeline_loan_result['pipeline_count']); ?></td>
								</tr>
								 
								<tr>
									<td style = "text-align: left;" >Avg. Rate</td>
									<td style = "text-align: left;" ><?php echo number_format($pipeline_loan_result['total_intrest_rate']/$pipeline_loan_result['pipeline_count'],2); ?>%</td>
								</tr>
								
								<tr>
									<td style = "text-align: left;" >Avg. LTV</td>
									<td style = "text-align: left;" ><?php echo number_format($loan_to_value,2);?>%</td>
								</tr>
								
								<tr>
									<td style = "text-align: left;" >Avg. Term</td>
									<td style = "text-align: left;"><?php echo number_format($pipeline_loan_result['total_term_month'],1).' Months';?></td>
								</tr>
								<!--
								<tr>
									<td style = "text-align: left;" >Lender Fees</td>
									<td style = "text-align: left;">$<?php echo $pipeline_loan_result['lender_fee']; ?></td>
								-->
								<!--<tr>
									<td style = "text-align: left;">Lender Fees</td>
									<td style = "text-align: left;">$<?php echo number_format($total_pipeline_lender_fees_amount); ?></td>
								</tr>
							</tbody>
						</table>
					</div>------>
					
					
					
					
					<!------- AVAILABLE TRUST DEEDS TABLE ENDS ------>
					
					<!------- START LOAN BOARDING SCHEDULE TABLE  ------>
					
					
					
					<!------- END LOAN BOARDING SCHEDULE TABLE  ------>
					
					
					
				</div>
				
				<div class="row rc_class">
					
					<!-------------- TOP BORROWER's TABLE ENDS ------------->
					
					
					
					
					<!-------------- ACTIVE BORROWER TABLE ENDS ------------->
					
				
					<div class="col-md-4 scrolling-dashboard">
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead >
								<tr class="thead_dark_blue">
									<th colspan="2">Loan Boarding Schedule</th>
								</tr>
								<tr>
									<th>Loan #</th>
									<th>Borrower Name</th>
								</tr>
							</thead>
							<tbody>
								<?php
									foreach($loan_boarding_schedule as $boarding_val){
										
									
								?>
								<tr>
									<td style = "text-align: left;"><a href="<?php echo base_url().'load_data/'.$boarding_val->id;?>"><?php echo $boarding_val->talimar_loan; ?></a></td>
									<td style = "text-align: left;"><?php echo $boarding_val->b_name; ?></td>
								</tr>
									<?php } ?>
								
							</tbody>
						</table>
					</div>
				
					<!-------------- Portfolio Statistics STARTS ------------->
				<div class="col-md-4 ">
					<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
						<thead>
							<tr class="thead_dark_blue">
								<th colspan="2">Portfolio Statistics</th>
							</tr>
							<!--<tr>
								<th>Loan #</th>
								<th>Lender name</th>
							</tr>-->
						</thead>
						<tbody>
							<tr>
								<td style = "text-align: left;">Total # of Assigned Interests</td>
								<td style = "text-align: left;"><?php echo $lender_count_active; ?></td>
							</tr>
							<tr>
								<td style = "text-align: left;" >Avg. # of Lenders / Loan</td>
								<td style = "text-align: left;" ><?php echo number_format($lender_count_active/$active_loan_result['active_count'],2);?></td>
							</tr>
							<tr>
								<td style = "text-align: left;" >Avg. $ Assigned Interest</td>
								<td style = "text-align: left;" ><?php echo '$'.number_format($active_loan_result['total_loan_amount']/$lender_count_active); ?></td>
							</tr>
							<tr>
								<td style = "text-align: left;">Avg. Loan Spread</td>
								<td style = "text-align: left;"><?php echo number_format($fetch_spread_data['sum_servicing']/$fetch_spread_data['count_loans'],2); ?>%</td>
							</tr>
							<!--<tr>
								<td style = "text-align: left;" >Monthly Net Servicing Income</td>
								<td style = "text-align: left;" >$<?php echo number_format($total_servicing_income); ?></td>
							</tr>
							<tr>
								<td style = "text-align: left;" style = "text-align: left;">Monthly Servicing Cost
								<td style = "text-align: left;" ><?php echo '$'.number_format($total_servicing_cost); ?></td>
							</tr>-->
							<tr>
								<td style = "text-align: left;" style = "text-align: left;"># Paid Off Loans
								<td style = "text-align: left;" ><?php echo $paid_off_loans; ?></td>
							</tr>
							<tr>
								<td style = "text-align: left;" style = "text-align: left;">$ Paid Off Loans
								<td style = "text-align: left;" ><?php echo $paid_off_loans_amt; ?></td>
							</tr>
							<tr>
								<td style = "text-align: left;" style = "text-align: left;"># Loans Funded
								<td style = "text-align: left;" ><?php echo $loans_funded_loans; ?></td>
							</tr>
							<tr>
								<td style = "text-align: left;" style = "text-align: left;">$ Loans Funded
								<td style = "text-align: left;" ><?php echo $loans_funded_loans_amt; ?></td>
							</tr>
						</tbody>
							
					</table>	
				</div>
					
					
					
					<!-------------45 Day Maturity Schedule-------------->
					<div class="col-md-4">
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead>
								<tr class="thead_dark_blue">
								<th colspan="4">45 Day Maturity Schedule</th>
								</tr>
								<tr>
									<th>Loan #</th>
									<th>Loan Amount</th>
									<th>Maturity</th>
									<th>Days</th>
								</tr>
							</thead>
							<tbody>
								<?php 
								if(isset($maturity_report_data))
								{
									foreach($maturity_report_data as $row)
									{
										?>
										<tr>
											<td><a href="<?php echo base_url().'load_data/'.$row['loan_id'];?>"><?php echo $row['talimar_loan'];?></td>
											<td>$<?php echo number_format($row['loan_amount']);?></td>
											<td><?php echo $row['maturity_date'];?></td>
											<td><?php echo $row['duration']+1;?></td>
										</tr>
										<?php
									}
								}
								?>
							</tbody>
						</table>
						
					</div>	
					<!--------------------------->
					<!------- LOANS IN DEFAULT TABLE STARTS 	 ------>
					
					
					
					
						
					<!------- LOANS IN DEFAULT TABLE ENDS 	 	------>
				</div>
				<div class="row rc_class">
					<!-------------- TOP BORROWER's TABLE STARTS ------------->
					<div class="col-md-4">
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead>
								<tr class="thead_dark_blue">
									<th colspan="3">Top Borrower's</th>
								</tr>
								<tr>
									<th>Borrower 	 </th>
									<th>Active<br>Loans</th>
									<th>Total<br>Balance</th>
								
								</tr>
							</thead>
							<tbody>
							<?php
							// echo '<pre>';
							// print_r($top_borrower_datas);
							// echo '</pre>';
							
							foreach($top_borrower_datas as $row)
							{
							?>
							<tr>
								<td><a href="<?php echo base_url().'borrower_data/view/'.$row['borrower_id']; ?>"><?php echo $row['borrower_name']; ?></a></td>
								<td class="data-center"><?php echo $row['count']; ?></td>
								<td>$<?php echo number_format($row['total_loan_amount']); ?></td>
							</tr>
							<?php } ?>
							</tbody>
							
						</table>	
					</div>
					
					
					<!----------TOP LENDER DATA-------------->
					
					
					
						<div class="col-md-4">
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead>
								<tr class="thead_dark_blue">
									<th colspan="3">Top Lender's</th>
								</tr>
								<tr>
									<th>Contact Name</th>
									<th>Active<br>Loans</th>
									<th>Current Balance</th>
								
								</tr>
							</thead>
							<tbody>
								<?php 
								// echo '<pre>';
								// print_r($top_lender_datas);
								// echo '</pre>';
								$number_counting = 0;
								foreach($top_lender_datas as $key => $row)
								{
										// echo '<pre>';
										// print_r($row);
										// echo '</pre>';
										$next_key = $key+1;
										$previous_key = $key-1;
										if($top_lender_datas[$key]['contact_id'] == $top_lender_datas[$next_key]['contact_id'])
										{
											$inv_total = $top_lender_datas[$key]['total_investment'] + $top_lender_datas[$next_key]['total_investment'];
											
											$count_row  = $top_lender_datas[$key]['count'] + $top_lender_datas[$next_key]['count'];
										}
										else
										{
											$inv_total = $row['total_investment'];
											$count_row = $row['count'];
										}
										
										if($top_lender_datas[$key]['contact_id'] != $top_lender_datas[$previous_key]['contact_id'])
										{
											$number_counting++;
									?>
									<tr>
										<td><a href = "<?php echo base_url().'viewcontact/'.$row['contact_id']?>"><?php echo $row['contact_name'];?></a></td>
										<td class="data-center"><?php echo $count_row;?></td>
										<td align = "right">$<?php echo number_format($inv_total);?></td>
										
									</tr>
									<?php
										}
 									 if($number_counting >=5)
									{
										break;
									}  
								}
								?>
							</tbody>
							
						</table>	
					</div>
					
					
					
					<!-------------- Assignments in Process STARTS ------------->
					<div class="col-md-4 scrolling-dashboard">
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead>
								<tr class="thead_dark_blue">
									<th colspan="3">Assignments in Process</th>
								</tr>
								<tr>
									<th>Loan #</th>
									<th>Loan Status</th>
									<th>Lender name</th>
								</tr>
							</thead>
							<tbody>
								<?php 
								
									foreach($assignment_in_process as $assignment_val){
										
									$status = ($assignment_val->loan_status == '1') ? 'Pipeline' : 'Active';
								
								?>
								<tr>
									<td style = "text-align: left;"><a href="<?php echo base_url().'load_data/'.$assignment_val->id;?>"><?php echo $assignment_val->talimar_loan; ?></a></td>
									<td style = "text-align: center;"><?php echo $status; ?></td>
									<td style = "text-align: left;"><?php echo $assignment_val->name ; ?></td>
								</tr>
									<?php } ?>
							</tbody>
							
						</table>	
					</div>
					<!-------------- Assignments in Process ENDS ------------->
					
					<!-------------- SERVICING COMPANIES STARTS ------------->
					
					
				</div>
				
				<!-------------- SERVICING COMPANIES ENDS ------------->
				<div class="row rc_class">	
				
				
				<div class="col-md-4">
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead>
								<tr class="thead_dark_blue">
									<th colspan="2">Construction Loans</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td># of Construction Loans:</td>
									<td><?php echo $count_construction_loans; ?></td>
								</tr>
								<tr>
									<td>$ of Construction Loans:</td>
									<td>$<?php echo number_format($construction_total_loan_amount); ?></td>
								</tr>
								<tr>
									<td>Total Reserve Amount:</td>
									<td>$<?php echo number_format($fetch_draws_total['total_draws_amount']); ?></td>
								</tr>
								<tr>
									<td>Total Reserve Balance:</td>
									<td>$<?php echo number_format($fetch_draws_total['total_draws_remaining']);?></td>
								</tr>
							</tbody>
						</table>
				</div>
					
				
				
				<div class="col-md-4 scrolling-dashboard">
					<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
						<thead>
							<tr class="thead_dark_blue">
								<th colspan="2">Outstanding Draws</th>
							</tr>
							<tr>
								<th>Loan # </th>
								<th>Reserve Requested</th>
							</tr>
						</thead>
						<tbody>
						<?php 
						if(isset($outstanding_draws))
						{
							foreach($outstanding_draws as $row)
							{
								?>
								<tr>
									<td><a href="<?php echo base_url().'load_data/'.$row->loan_id;?>"><?php echo $row->talimar_loan; ?></a></td>
									<td>$<?php echo number_format($row->draws_release_amount,2); ?></td>
								</tr>
								<?php
							}
						}
						?>
						</tbody>
					</table>
				</div>
				
				<div class="col-md-4">
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead>
								<tr class="thead_dark_blue">
									<th colspan="2">Servicing Companies</th>
								</tr>
								<tr>
									<td>Loans in Boarding #</td>
									<td><?php echo $count_both_loan_services_with_bs; ?></td>
								</tr>
								<tr>
									<td>Loans in Boarding $</td>
									<td><?php echo '$'. number_format($both_loan_services_total_amount_with_bs); ?></td>
								</tr>
								<tr>
									<td>FCI Lender Services # </td>
									<td><?php echo $count_fci_loan; ?></td>
								</tr>
								<tr>
									<td>FCI Lender Services $ </td>
									<td><?php echo '$'.number_format($fci_loans_total_amount); ?></td>
								</tr>
								<tr>
									<td>Del Toro Loan Services #</td>
									<td><?php echo $count_del_toro_loan; ?></td>
								</tr>
								<tr>
									<td>Del Toro Loan Services $</td>
									<td><?php echo '$'.number_format($del_toro_loans_total_amount); ?></td>
								</tr>
								
								<tr>
									<td>TaliMar Financial # </td>
									<td><?php echo $count_talimar_loan ; ?></td>
								</tr>
								<tr>
									<td>TaliMar Financial $</td>
									<td><?php echo '$'.number_format($talimar_loans_total_amount); ?></td>
								</tr>
								
								<!--<tr>
									<!--<td># of Loans Serviced Loan</td>-->
									<!--<td>Add Loans w/ TaliMar Financial # </td>
									<td><?php echo $count_serviced_loan; ?></td>
								</tr>
								<tr>
									<!--<td>$ of Loans Serviced Loan</td>-->
									<!--<td>Add Loans w/ TaliMar $</td>
									<td><?php echo '$'.number_format($serviced_loans_total_amount); ?></td>
								</tr>-->
								
							</thead>
							<tbody>
								
							</tbody>
						</table>	
					</div>
				<!-------------- Portfolio Statistics ENDS ------------->
				</div>
				
			<div class="row rc_class">	
				<div class="col-md-4">
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead>
								<tr class="thead_dark_blue">
									<th colspan="3">Loans in Default</th>
								</tr>
								<tr>
									<th>Loan #</th>
									<th>Loan Amount</th>
									<th>Reason</th>
								
								</tr>
							</thead>
							<tbody>
								
								<?php
									$total_loan_amount = 0;
									$count_loan_default = 0;
									if(isset($default_loan)){
										foreach($default_loan as $key => $row){
								?>
								<tr>
									<td style = "text-align: left;" >
										<a href="<?php echo base_url().'load_data/'.$default_loan[$key]['loan_id'];?>"><?php echo $default_loan[$key]['talimar_loan'];?></a>
									</td>
									<td style = "text-align: left;" >
										$<?php echo number_format($default_loan[$key]['loan_amount']); ?>
									</td>
									<td style = "text-align: left;" >
										<?php echo $serviceing_reason_option[$default_loan[$key]['servicing_reason']];?>
									</td>
								</tr>
								<?php 
									$total_loan_amount = $total_loan_amount + $default_loan[$key]['loan_amount'];
									$count_loan_default++;
										
									} }else {
								?>
								<tr>
									<td style = "text-align: left;" colspan="3">No Data Found!</td>
								</tr>
								<?php } ?>
							</tbody>
							<tfoot class="foot_light_blue">
								<tr>
								<th> Total: <?php echo $count_loan_default;?></th>
								<th>$<?php echo  number_format($total_loan_amount);?> </th>
								<th></th>
								</tr>
							</tfoot>
						</table>	
					</div>
					<div class="col-md-4">
					<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
						<thead>
							<tr class="thead_dark_blue">
								<th colspan="3">Portfolio Diversification</th>
							</tr>
							<tr>
								<th>Loan Type</th>
								<th># of Loans</th>
								<th>% of Portfolio</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Bridge</td>
								<td><?php echo $fetch_type_1;?></td>
								<td><?php echo  number_format($fetch_type_1/$fetch_total*100,2).'%';?></td>
								
							</tr>
							<tr>
								<td>Construction (Existing)</td>
								<td><?php echo $fetch_type_2;?></td>
								<td><?php echo  number_format($fetch_type_2/$fetch_total*100,2).'%';?></td>
								
							</tr>
							<tr>
								
								<td>Construction (New)</td>
								<td><?php echo $fetch_type_3;?></td>
								<td><?php echo  number_format($fetch_type_3/$fetch_total*100,2).'%';?></td>
								
							</tr>
						
						</tbody>
					</table>
				</div>
				
			</div>
				
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	
	
	<!-- END CONTENT -->

<!-- END CONTAINER -->
