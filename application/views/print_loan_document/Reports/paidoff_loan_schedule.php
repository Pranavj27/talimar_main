<?php
error_reporting(0);
// echo "<pre>";
// print_r($fetch_paid_off_data);
// echo "</pre>";
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Paidoff Loan Schedule </h1>	
					
				</div>
				<div class="top_download">
				<a href="<?php echo base_url();?>download_paidoff_loan">
				<button  class="btn blue">Download</button>
				</a>
				</div>
		</div>
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Paidoff Date</th>
					<th>Loan Number </th>
					<th>Loan Amount</th>
					<th>Borrower Name</th>
					<th>Sale/Refinace Price</th>
					<th>Interest Rate</th>
					
					<th>Term</th>
					
					<th>Address #1</th>
					<th>State</th>
					<th>City</th>
					<th>Zip</th>
					</tr>
				</thead>
				<tbody>
				<?php
				$key = 0;
					foreach($fetch_paid_off_data['talimar_loan'] as $row )
					{
						?>
						<tr>
						
							<td><?php echo $fetch_paid_off_data['paid_off_date'][$key]; ?></td>
							<td><?php echo $fetch_paid_off_data['talimar_loan'][$key]; ?></td>
							<td><?php echo '$'.number_format($fetch_paid_off_data['loan_amount'][$key]); ?></td>
							<td><?php echo $fetch_paid_off_data['borrower_name'][$key]; ?></td>
							<td><?php echo '$'.number_format($fetch_paid_off_data['sales_refinance_value'][$key]); ?></td>
							<td><?php echo number_format($fetch_paid_off_data['interest_rate'][$key], 3)."%"; ?></td>
							<td><?php echo $fetch_paid_off_data['term_month'][$key];?></td>
							<td><?php echo $fetch_paid_off_data['property_address'][$key];?></td>
							<td><?php echo $fetch_paid_off_data['state'][$key];?></td>
							<td><?php echo $fetch_paid_off_data['city'][$key];?></td>
							<td><?php echo $fetch_paid_off_data['zip'][$key];?></td>
							
						</tr>
						<?php
					$key++;
					}
				?>
				</tbody>
			</table>
			
	</div>
	<!-- END CONTENT -->
</div>