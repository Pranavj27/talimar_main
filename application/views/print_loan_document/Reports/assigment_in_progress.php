<?php
error_reporting(0);
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Assigment in Progress </h1>	
					
				</div>
				<div class="top_download">
				<a href="<?php echo base_url();?>download_assigment">
				<button  class="btn blue">Download</button>
				</a>
				</div>
		</div>
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Lender Name</th>
					<th>Investment $</th>
					<th>Investment %</th>
					<th>Talimar #</th>
					<th>FCI</th>
					<th>Loan Number</th>
					<th>Disclosures Submitted</th>
					<th>Disclosures Executed</th>
					<th>Fund Received</th>
					<th>Assigment Submitted</th>
					<th>Submitted Servicer</th>
					<th>Servicer Boarded</th>
					</tr>
				</thead>
				<tbody>
				<?php
				$key = 0;
				foreach($all_assigment_result as  $row)
				{
				?>
						<tr>
							<td><?= $all_assigment_result[$key]['lender_name'];?></td>
							<td>$<?= number_format($all_assigment_result[$key]['investment']);?></td>
							<td><?= $all_assigment_result[$key]['percent_loan'];?></td>
							<td><?= $all_assigment_result[$key]['talimar_loan'];?></td>
							<td><?= $all_assigment_result[$key]['fci'];?></td>
							<td><?= $all_assigment_result[$key]['lender_account'];?></td>
							
							<td><input type="checkbox" <?php if($all_assigment_result[$key]['disclosures_submit'] == '1'){ echo 'checked'; }?> disabled ></td>
							<td><input type="checkbox" <?php if($all_assigment_result[$key]['disclosures_execute'] == '1'){ echo 'checked'; }?>  disabled ></td>
							<td><input type="checkbox" <?php if($all_assigment_result[$key]['fund_received'] == '1'){ echo 'checked'; }?>  disabled ></td>
							<td><input type="checkbox" <?php if($all_assigment_result[$key]['assigment_submit'] == '1'){ echo 'checked'; }?>  disabled ></td>
							<td><input type="checkbox" <?php if($all_assigment_result[$key]['servicer_submit'] == '1'){ echo 'checked'; }?>  disabled ></td>
							<td><input type="checkbox"  disabled ></td>
							
							
						</tr>
					<?php
					$key++;
				}
					?>
				</tbody>
			</table>
			
	</div>
	<!-- END CONTENT -->
</div>