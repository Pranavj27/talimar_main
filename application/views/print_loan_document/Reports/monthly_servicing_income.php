<?php
error_reporting(0);
// echo "<pre>";
// print_r($all_monthly_income);
// echo "</pre>";
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Monthly Servicing Income  </h1>	
					
				</div>
				<div class="top_download">
					<a href="<?php echo base_url();?>download_monthly_income">
					<button  class="btn blue">Download</button>
					</a>
				</div>
		</div>
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						<th>Loan Number</th>
						<th>Loan Amount</th>
						<th>Servicing Fee</th>
						<th>Gross Servicing</th>
						<th># of Investors</th>
						<th>$ per Investor</th>
						<th>Servicing Cost</th>
						<th>Servicing Income</th>
						
					</tr>
				</thead>
				<tbody>
				  <?php
				  $key = 0;
				  $total_amount = 0;
				  $total_servicing_income = 0;
					foreach($all_monthly_income as $row)
					{
						  $loan_amount = $all_monthly_income[$key]['loan_amount'];
						  $servicing_fee = $all_monthly_income[$key]['servicing_fee'];
						  
						  $gross_servicing = ($loan_amount * ($servicing_fee / 100 ))/12;
						  
						  $minimum_servicing_fee = $all_monthly_income[$key]['minimum_servicing_fee'];
						  $servicing_cost = $all_monthly_income[$key]['count_lender'] *$minimum_servicing_fee;
						  
						  $servicing_income = $gross_servicing - $servicing_cost;
						?>
						<tr>
							<td><?php echo $all_monthly_income[$key]['talimar_loan'];?></td>
							<td><?php echo '$'.number_format($all_monthly_income[$key]['loan_amount']);?></td>
							<td><?php echo $all_monthly_income[$key]['servicing_fee'].'%';?></td>
							<td><?php echo '$'.number_format($gross_servicing,2);?></td>
							<td><?php echo $all_monthly_income[$key]['count_lender'];?></td>
							<td><?php echo '$'.$all_monthly_income[$key]['minimum_servicing_fee'];?></td>
							<td><?php echo '$'.number_format($servicing_cost,2);?></td>
							<td><?php echo '$'.number_format($servicing_income,2); ?></td>
							
						</tr>
						<?php
						$total_amount = $total_amount + $all_monthly_income[$key]['loan_amount'];
						$total_servicing_income = $total_servicing_income + $servicing_income;
						$key++;
					}
					?>    
				</tbody>
				<tfoot>
					<tr>
						<th>Total</th>
						<th><?php echo '$'.number_format($total_amount); ?></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th>Total</th>
						<th><?php echo '$'.number_format($total_servicing_income,2); ?></th>
					</tr>
				</tfoot>
			</table>
			
	</div>
	<!-- END CONTENT -->
</div>