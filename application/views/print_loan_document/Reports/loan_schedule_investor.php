<?php
error_reporting(0);
$loan_status_option = $this->config->item('loan_status_option');
// echo "<pre>";
// print_r($loan_schedule_investor);
// echo "</pre>";
// echo "working";
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Trust Deed Schedule by Investor</h1>	
					
				</div>
				<!--
				<div class="top_download">
				<a href="<?php echo base_url();?>download_loan_schedule_borrower">
				<button  class="btn blue">Download</button>
				</a>
				</div>
				-->
		</div>
		<div class="row">
			<form id="select_investor" method="POST" action="<?php echo base_url();?>loan_schedule_investor">
			<div class="talimar_no_dropdowns">
				Investor :  &nbsp;
							<select name="investor_id" onchange="select_investor_report(this.value)" class="selectpicker" data-live-search="true" >
								<option value='all'>All</option>
								<?php 
								foreach($all_investor as $row)
								{
									?>
									<option value="<?php echo $row->id; ?>" <?php if(isset($select_investor_id)){ if($select_investor_id == $row->id ){ echo 'selected'; } } ?> > <?php echo $row->name;?></option>
									<?php
								}
								?>
							</select>
			</div>
			</form>
		</div>
		
		
		<!---
			<div class="row">
				<div class="borrower_title"><h3>&nbsp;&nbsp;Borrower name</h3></div>
			</div>
			-->
				<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
					<thead>
					<tr>
						<th>Investor Name</th>
						<th>TaliMar Loan</th>
						<th>FCI</th>
						<th>Loan Amount</th>
						<th>Loan Status</th>
						<th>Investment</th>
						<th>% Yield</th>
						<th>Available</th>
						<!--
						<th>Address</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
						-->
					</tr>
					
					</thead>
					<tbody>
					<?php
					$key = 0;
					foreach($loan_schedule_investor as $row)
					{
					?>
					<tr>
						<td><?php echo $loan_schedule_investor[$key]['investor_name'];?></td>
						<td><?php echo $loan_schedule_investor[$key]['talimar_loan'];?></td>
						<td><?php echo $loan_schedule_investor[$key]['fci'];?></td>
						<td><?php echo '$'.number_format($loan_schedule_investor[$key]['loan_amount']);?></td>
						<td><?php echo $loan_status_option[$loan_schedule_investor[$key]['loan_status']];?></td>
						<td><?php echo '$'.number_format($loan_schedule_investor[$key]['investment']);?></td>
						<td><?php echo number_format($loan_schedule_investor[$key]['percent_yield'],2); ?>%</td>
						<td><?php echo '$'.number_format($loan_schedule_investor[$key]['available']);?></td>
						<!--
						<td><?php echo $loan_schedule_investor[$key]['property_address'];?></td>
						<td><?php echo $loan_schedule_investor[$key]['city'];?></td>
						<td><?php echo $loan_schedule_investor[$key]['state'];?></td>
						<td><?php echo $loan_schedule_investor[$key]['zip'];?></td>
						-->
					</tr>
					<?php
					$key++;
					}
					?>
					</tbody>
				</table>
		
		
			
	</div>
	<!-- END CONTENT -->
</div>
<script>
function select_investor_report(id)
{
	document.getElementById('select_investor').submit();
}
</script>