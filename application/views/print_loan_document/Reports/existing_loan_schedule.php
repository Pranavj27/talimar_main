<?php
error_reporting(0);
// echo "<pre>";
// print_r($fetch_active_data);
// echo "</pre>";
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Active Loan Schedule </h1>	
					
				</div>
				<div class="top_download">
				<a href="<?php echo base_url();?>download_existing_loan">
				<button  class="btn blue">Download</button>
				</a>
				</div>
		</div>
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Borrower Name</th>
					<th>TaliMar Loan Number </th>
					<th>FCI Loan Number</th>
					<th>Loan Amount</th>
					<th>Interest Rate</th>
					<!--
					<th>Lender Fees</th>
					<th>Lender Fees</th>
					-->
					<th>LTV</th>
					<th>Term</th>
					<th>Maturity Date</th>
					<th>Address</th>
					<th>Unit</th>
					<th>State</th>
					<th>City</th>
					<th>Zip</th>
					<th>Available</th>
					</tr>
				</thead>
				<tbody>
				<?php
				$key = 0;
					foreach($fetch_active_data['talimar_loan'] as $row )
					{
						$loan_amount 	= $fetch_active_data['loan_amount'][$key];
						$arv 			= $fetch_active_data['property_arv'][$key];
						$ltv = ($loan_amount/$arv) * 100;
						?>
						<tr>
						
							<td><?php echo $fetch_active_data['borrower_name'][$key]; ?></td>
							
							<td><?php echo $fetch_active_data['talimar_loan'][$key]; ?></td>
							<td><?php echo $fetch_active_data['fci'][$key]; ?></td>
							
							<td>$<?php echo number_format($fetch_active_data['loan_amount'][$key]); ?></td>
							<td><?php echo number_format($fetch_active_data['interest_rate'][$key], 3); ?>%</td>
							<!--
							<td><?php echo $fetch_active_data['lender_fee'][$key]; ?></td>
							<?php $calculate_lender_amount = $fetch_active_data['loan_amount'][$key] * ($fetch_active_data['lender_fee'][$key] / 100 );?>
							<td><?php echo number_format($calculate_lender_amount); ?></td>
							-->
							<td><?php echo number_format($ltv,2); ?>%</td>
							
							<td><?php echo $fetch_active_data['term_month'][$key];?></td>
							<td><?php echo $fetch_active_data['maturity_date'][$key];?></td>
							<td><?php echo $fetch_active_data['property_address'][$key];?></td>
							<td><?php echo $fetch_active_data['unit'][$key];?></td>
							<td><?php echo $fetch_active_data['state'][$key];?></td>
							<td><?php echo $fetch_active_data['city'][$key];?></td>
							<td><?php echo $fetch_active_data['zip'][$key];?></td>
							<td>$<?php echo number_format($fetch_active_data['available'][$key]); ?></td>
							
						</tr>
						<?php
					$key++;
					}
				?>
				</tbody>
			</table>
			
	</div>
	<!-- END CONTENT -->
</div>