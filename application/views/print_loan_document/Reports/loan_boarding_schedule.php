<?php
error_reporting(0);
// echo "<pre>";
// print_r($all_boarding_schdule);
// echo "</pre>";
?>
<style>

</style>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Loan Boarding Schedule </h1>	
					
				</div>
				<div class="top_download">
					<a href="<?php echo base_url();?>download_boarding_schedule">
					<button  class="btn blue">Download</button>
					</a>
				</div>
		</div>
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						<th>TaliMar</th>
						<th>FCI #</th>
						<th>Loan Amount</th>
						<th>Borrower Name</th>
						<th>Address</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
						<th>Documment Signed</th>
						<th>Funded</th>
						<th>Release To Close</th>
						<th>Submited</th>
						<th>Boarded</th>
					</tr>
				</thead>
				<tbody>
				<?php
				$key = 0;
					foreach($all_boarding_schdule as $row)
					{
				?>
						<tr>
							<td><?php echo $all_boarding_schdule[$key]['talimar_loan'];?></td>
							<td><?php echo $all_boarding_schdule[$key]['fci'];?></td>
							<td><?php echo '$'.number_format($all_boarding_schdule[$key]['loan_amount']);?></td>
							<td><?php echo $all_boarding_schdule[$key]['borrower_name'];?></td>
							<td><?php echo $all_boarding_schdule[$key]['property_address'];?></td>
							<td><?php echo $all_boarding_schdule[$key]['city'];?></td>
							<td><?php echo $all_boarding_schdule[$key]['state'];?></td>
							<td><?php echo $all_boarding_schdule[$key]['zip'];?></td>
							
							<td><input type="checkbox" <?php if($all_boarding_schdule[$key]['document_sign'] == '1'){ echo 'checked'; } ?> disabled ></td>
							<td><input type="checkbox" <?php if($all_boarding_schdule[$key]['fund_released'] == '1'){ echo 'checked'; } ?> disabled ></td>
							<td><input type="checkbox" <?php if($all_boarding_schdule[$key]['file_closed'] == '1'){ echo 'checked'; } ?> disabled ></td>
							<td><input type="checkbox" <?php if($all_boarding_schdule[$key]['submited_servicer'] == '1'){ echo 'checked'; } ?> disabled ></td>
							<td><input type="checkbox" <?php if($all_boarding_schdule[$key]['loan_boarded'] == '1'){ echo 'checked'; } ?> disabled ></td>
							
							
							
						</tr>
					<?php
					$key++;
					}
					?>
				</tbody>
			</table>
			
	</div>
	<!-- END CONTENT -->
</div>