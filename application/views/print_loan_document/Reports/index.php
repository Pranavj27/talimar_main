<?php
// echo "<pre>";
// print_r($fetch_pipeline_data);
// echo "</pre>";
error_reporting(0);
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Pipeline Loan Schedule </h1>	
					
				</div>
				<div class="top_download">
				<a href="<?php echo base_url();?>reports/download_pdf_pipeline">
				<button  class="btn blue">Download</button>
				</a>
				</div>
		</div>
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
				<tr>
					<th>Borrower Name</th>
					<th>Loan Number </th>
					<th>Loan Amount</th>
					<th>Interest Rate</th>
					<th>Lender Fees</th>
					<th>Lender Fees</th>
					<th>LTV</th>
					<th>Term</th>
					<th>Funding Date</th>
					<th>Contact Name</th>
					<th>Available</th>
					</tr>
				</thead>
				<tbody>
				<?php
				$key = 0; 
					foreach($fetch_pipeline_data['talimar_loan'] as  $row )
					{
						$loan_amount 	= $fetch_pipeline_data['loan_amount'][$key];
						$arv 			= $fetch_pipeline_data['property_arv'][$key];
						$ltv = ($loan_amount/$arv) * 100;
						?>
						<tr>
							<td><?php echo $fetch_pipeline_data['borrower_name'][$key]; ?></td>
							<td><?php echo $fetch_pipeline_data['talimar_loan'][$key]; ?></td>
							<td>$<?php echo number_format($fetch_pipeline_data['loan_amount'][$key]); ?></td>
							<td><?php echo number_format($fetch_pipeline_data['interest_rate'][$key], 3); ?>%</td>
							<td><?php echo $fetch_pipeline_data['lender_fee'][$key]; ?>%</td>
							<?php $calculate_lender_amount = $fetch_pipeline_data['loan_amount'][$key] * ($fetch_pipeline_data['lender_fee'][$key] / 100 );?>
							<td>$<?php echo number_format($calculate_lender_amount); ?></td>
							
							<td><?php  echo number_format($ltv,2).'%'; ?></td>
							<td><?php echo $fetch_pipeline_data['term_month'][$key];?></td>
							<td><?php echo $fetch_pipeline_data['loan_funding_date'][$key];?></td>
							<td><?php echo $fetch_pipeline_data['borrower_contact_name'][$key];?></td>
							<td>$<?php echo number_format($fetch_pipeline_data['available'][$key]);?></td>
						</tr>
						<?php
					$key++;
					}
				?>
				</tbody>
			</table>
			
	</div>
	<!-- END CONTENT -->
</div>