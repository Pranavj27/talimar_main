<?php
error_reporting(0);
// echo "<pre>";
// print_r($maturity_report_data);
// echo "</pre>";
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Maturity Report Schedule  </h1>	
					
				</div>
				<div class="top_download">
					<a href="<?php echo base_url();?>download_maturity_reports">
					<button  class="btn blue">Download</button>
					</a>
				</div>
		</div>
			<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
				<thead>
					<tr>
						<th>Loan Number</th>
						<th>FCI loan</th>
						<th>Borrower Name</th>
						<th>Address</th>
						<th>Unit</th>
						<th>City</th>
						<th>St </th>
						<th>Zip</th>
						<th>Loan Amount</th>
						<th>Origination Date</th>
						<th>Maturity Date</th>
						<th>Remaining Days</th>
						
					</tr>
				</thead>
				<tbody>
				  <?php
				  $key = 0;
				  if(count($maturity_report_data)>0){
				  foreach($maturity_report_data as $row){
				  ?>
						<tr>
							<td><?php echo $maturity_report_data[$key]['talimar_loan'];?></td>
							<td><?php echo $maturity_report_data[$key]['fci'];?></td>
							<td><?php echo $maturity_report_data[$key]['borrower_name'];?></td>
							<td><?php echo $maturity_report_data[$key]['property_address'];?></td>
							<td><?php echo $maturity_report_data[$key]['unit'];?></td>
							<td><?php echo $maturity_report_data[$key]['city'];?></td>
							<td><?php echo $maturity_report_data[$key]['square_feet'];?></td>
							<td><?php echo $maturity_report_data[$key]['zip'];?></td>
							<td><?php echo '$'.number_format($maturity_report_data[$key]['loan_amount']);?></td>
							<td><?php echo $maturity_report_data[$key]['loan_document_date'];?></td>
							<td><?php echo $maturity_report_data[$key]['maturity_date'];?></td>
							
							<?php
							$maturity = $maturity_report_data[$key]['maturity_date'];
							$myDateTime = DateTime::createFromFormat('m-d-Y', $maturity);
							$newDateString = $myDateTime->format('d-m-Y');
							
								$from=date_create(date('d-m-Y'));
								$to=date_create(date('d-m-Y',strtotime($newDateString)));
								$diff=date_diff($from,$to);
								// print_r($diff);
								// echo $diff->format('%R%a days');
							?>
							 <td><?php echo trim($diff->format('%R%a days'),'+');?></td>
							
						</tr>
					<?php
					$key++;
				  }
				  }
					?>
				</tbody>
			</table>
			
	</div>
	<!-- END CONTENT -->
</div>