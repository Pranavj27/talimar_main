<?php
error_reporting(0);
$loan_status_option = $this->config->item('loan_status_option');
// echo "<pre>";
// print_r($loan_schedule_borrower);
// echo "</pre>";
// echo "working";
?>
<div class="page-content-wrapper">
	<div class="page-content responsive">
		<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1> &nbsp; Loan Schedule by Borrower </h1>	
					
				</div>
				<!--
				<div class="top_download">
				<a href="<?php echo base_url();?>download_loan_schedule_borrower">
				<button  class="btn blue">Download</button>
				</a>
				</div>
				--> 
		</div>
		<div class="row">
			<form id="select_borrower" method="POST" action="<?php echo base_url();?>loan_schedule_borrower">
			<div class="talimar_no_dropdowns">
				Borrower :  &nbsp;
							<select name="borrower_id" onchange="select_borrower_report(this.value)" class="selectpicker" data-live-search="true" >
								<option value='all'>All</option>
								<?php
								foreach($fetch_all_borrwer as $row)
								{
									?>
									<option value="<?php echo $row->id;?>" <?php if(isset($select_b_id)){  if($select_b_id == $row->id){ echo 'selected'; } }?> ><?php echo $row->b_name;?></option>
									<?php
								}
								?>
							</select>
			</div>
			</form>
		</div>
		
		
		<!---
			<div class="row">
				<div class="borrower_title"><h3>&nbsp;&nbsp;Borrower name</h3></div>
			</div>
			-->
				<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" >
					<thead>
					<tr>
						<th>Borrower Name</th>
						<th>TaliMar Loan</th>
						<th>FCI</th>
						<th>Loan Status</th>
						<th>Loan Amount</th>
						<th>Address</th>
						<th>City</th>
						<th>State</th>
					</tr>
					
					</thead>
					<tbody>
					<?php 
					$key=0;
					foreach($loan_schedule_borrower as $row)
					{
					?>
							<tr>
								<td><?php echo $loan_schedule_borrower[$key]['borrower_name']; ?></td>
								<td><?php echo $loan_schedule_borrower[$key]['talimar_loan']; ?></td>
								<td><?php echo $loan_schedule_borrower[$key]['fci']; ?></td>
								<td><?php echo $loan_status_option[$loan_schedule_borrower[$key]['loan_status']]; ?></td>
								<td><?php echo '$'.number_format($loan_schedule_borrower[$key]['loan_amount'],2); ?></td>
								<td><?php echo $loan_schedule_borrower[$key]['property_address']; ?></td>
								<td><?php echo $loan_schedule_borrower[$key]['city']; ?></td>
								<td><?php echo $loan_schedule_borrower[$key]['state']; ?></td>
							</tr>
					<?php 
					$key++;
					} ?>
					</tbody>
				</table>
		
		
			
	</div>
	<!-- END CONTENT -->
</div>
<script>
function select_borrower_report(id)
{
	document.getElementById('select_borrower').submit();
	// if(id == 'all')
	// {
		// window.location.href = "<?php echo base_url();?>loan_schedule_borrower";
	// }
	// else
	// {
		// window.location.href = "<?php echo base_url();?>loan_schedule_borrower/"+id;
	// }
	
}
</script>