<?php //$this->load->view("header.php") ;?>	
<!DOCTYPE html>
<head>
	<meta charset="utf-8"/>
	<title>Talimar Financial </title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	<link href="<?php echo base_url("assets/admin/pages/css/login.css"); ?>" rel="stylesheet" type="text/css"/>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url("assets/global/plugins/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url("assets/global/plugins/simple-line-icons/simple-line-icons.min.css"); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url("assets/global/plugins/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url("assets/global/plugins/uniform/css/uniform.default.css"); ?>" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="<?php echo base_url("assets/admin/pages/css/login.css"); ?>" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME STYLES -->
	<link href="<?php echo base_url("assets/global/css/components-rounded.css"); ?>" id="style_components" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url("assets/global/css/plugins.css"); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url("assets/admin/layout/css/layout.css"); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url("assets/admin/layout/css/themes/default.css"); ?>" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<?php echo base_url("assets/admin/layout/css/custom.css"); ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url()."assets/css/vishnu.css"; ?>" rel="stylesheet" type="text/css"/>

</head>
<style>
	body {
		background-color: #364150;
	}
	.login {
		background-color: #c2c2c2 !important;
	}

	body {
		background-color: #c2c2c2;
	} 
</style>
<body>

<div class="login">
<div class="logo">
	<a href="<?php echo base_url(); ?>">
	<img src="<?php echo base_url("assets/admin/layout4/img/logo-big.png"); ?>" alt=""/>
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
	<div  class="login-form">

			<form method="POST" action="<?php echo base_url().'form_create_newpassword'; ?>" id="form_newpassword" >
			<legend>Create New Password</legend>
			<div class="form-group">
				<label for="name">New Password</label>
				<input class="form-control" name="password"  type="password" id="Password" />
				<span class="text-danger"></span>
			</div>
			<div class="form-group">
				<label for="name">Confirm Password</label>
				<input class="form-control" name="c_password" id="c_Password" type="password" value="" />
				<span class="text-danger"></span>
			</div>
			<div class="form-group">
				
				<input type = "hidden" value="<?php echo $user_id; ?>" name="user_id" >
				<input type = "hidden" value="<?php echo $this->uri->segment(2); ?>" name="security_code" >
				<button type="submit" class="btn btn-info" >Submit</button> 
			</div>
			</form>
		
		</div>
	<!-- END LOGIN FORM -->
	
	 

</div>

<div class="copyright"> 2017 © TaliMar Financial.</div>


</div>

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->

<!-- END PAGE LEVEL SCRIPTS -->
<script>


jQuery(document).ready(function() {     
Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Login.init();
Demo.init();
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

