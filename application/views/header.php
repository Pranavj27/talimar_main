<html lang="en" class="no-js">
<head>
<meta name="googlebot-news" content="noindex">
<meta name="robots" content="noindex,nofollow">

<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<!--<![endif]-->
<!-- BEGIN HEAD -->
<meta charset="utf-8"/>
<?php
$url_title = $this->uri->segment(1);
if($url_title == '')
{
	$title = 'Dashboard -';
}
else if($url_title == 'load_data')
{
	$title = 'Loan Data -';
}
else if($url_title == 'borrower_data')
{
	$title = 'Borrower Data -';
}
else if($url_title == 'investor_data')
{
	$title = 'Lender Data -';
}
else if($url_title == 'users')
{
	$title = 'Users -';
}
else if($url_title == 'vendor_data')
{
	$title = 'Vendor Data -';
}
else if($url_title == 'loan_overview')
{
	$title = 'Loan Overview -';
}
else if($url_title == 'reports' || $url_title == 'master_investor_schedule' || $url_title == 'master_investor_schedule' || $url_title == 'master_borrower_schedule' || $url_title == 'existing_loan_schdule' || $url_title == 'paid_of_loan_schedule' || $url_title == 'fci_service_loan' || $url_title == 'maturity_report_schedule' || $url_title == 'assigment_in_progress' || $url_title == 'loan_boarding_schedule' || $url_title == 'trust_deed_schedule' || $url_title == 'loan_default_schedule')
{
	$title = 'Reports -';
}
else
{
	$title = '';
}
?>
<title><?php echo $title; ?> TaliMar Financial </title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>

<link href="<?php echo base_url("assets/global/plugins/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url("assets/global/plugins/simple-line-icons/simple-line-icons.min.css"); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url("assets/global/plugins/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url("assets/global/plugins/uniform/css/uniform.default.css"); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url("assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"); ?>" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="<?php echo base_url("assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url("assets/global/plugins/fullcalendar/fullcalendar.min.css"); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url("assets/global/plugins/jqvmap/jqvmap/jqvmap.css"); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url("assets/global/plugins/morris/morris.css"); ?>" rel="stylesheet" type="text/css">
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="<?php echo base_url("assets/admin/pages/css/tasks.css"); ?>" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
<link href="<?php echo base_url("assets/global/css/components-rounded.css"); ?>" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url("assets/global/css/plugins.css"); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="<?php echo base_url();?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css">
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="<?php echo base_url();?>assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url("assets/admin/layout4/css/layout.css"); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url("assets/admin/layout4/css/themes/light.css"); ?>" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?php echo base_url("assets/admin/layout4/css/custom.css"); ?>" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
<link href="<?php echo base_url();?>assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="<?php echo base_url();?>assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
<link href="<?php echo base_url()."assets/css/database.css"; ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url()."assets/css/responsive.css"; ?>" rel="stylesheet" type="text/css"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-sidebar-closed-hide-logo page-sidebar-closed-hide-logo">
<div class="page-header">
	<!-- BEGIN HEADER TOP -->
	<div class="page-header-top">
		<div class="container">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="<?php echo base_url();?>"><img src="<?php echo base_url("assets/admin/layout4/img/logo-light.png"); ?>" alt="logo" class="logo-default"></a>
			</div>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a  class="menu-toggler" onclick="show_menu_section(this)" id="show"></a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown dropdown-user dropdown-dark">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<?php
						$where2['t_user_id'] = $this->session->userdata('t_user_id');
		
						$fetch_profile_images = $this->User_model->select_where('profile_image',$where2);
						
						if($fetch_profile_images->num_rows() > 0)
						{
							
							$fetch_profile_images1 = $fetch_profile_images->result();
							$image_name1 = $fetch_profile_images1[0]->image;
							?>
							<img alt="" class="" src="<?php echo aws_s3_document_url('profile/images/'.$image_name1.''); ?>">
							<?php
						}
						else
						{
							?>
							<img alt="" class="" src="<?php echo base_url("assets/global/img/profile.png"); ?>">
							<?php
						}
						?>
						
						<span class="username username-hide-mobile"><?php echo $this->session->userdata('user_name'); ?></span>
						</a>
						<ul class="dropdown-menu dropdown-menu-default"> 
							
							<?php
							if($this->session->userdata('user_role') == 2)
							{
							?>
							<li>
						
							<a href="<?php echo base_url().'users';?>"><i class="icon-user"></i>Users</a>
							</li>
							<?php } ?>
							<li>
								<a href="<?php echo base_url(); ?>profile">
								<i class="icon-wrench"></i> Maintenance </a>
							</li>
							<li>
								<a href="<?php echo base_url(); ?>logged_out">
								<i class="icon-key"></i> Log Out </a>
							</li>
							
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
	</div>
	<!-- END HEADER TOP -->
	<!-- BEGIN HEADER MENU -->
	<div class="page-header-menu">
		<div class="container">
			<!-- BEGIN HEADER SEARCH BOX 
			
			 END HEADER SEARCH BOX -->
			<!-- BEGIN MEGA MENU -->
			<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
			<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
			<div class="hor-menu ">
				<ul class="nav navbar-nav">
					<?php 
							$uri_segment_one = $this->uri->segment(1);
							if($uri_segment_one == 'contact'  ){
								$class_contact = 'class = "active"';
							}else{
								
								$class_contact = '';
							}
						?>
						<li <?php echo $class_contact;?>>
							<!--<a href = "<?echo base_url().'contact'?>" >Contacts</a>
							<a href = "<?echo base_url().'viewcontact'?>" >Contacts</a>-->
							<a href = "<?echo base_url().'contactlist'?>" >Contacts</a>
						</li>
					<?php
					$uri_segment_one = $this->uri->segment(1);
					if($uri_segment_one == '')
					{
						echo '<li class="active">';
					}
					else
					{
						echo '<li>';
					}
					?>
					
						<a href="<?php echo base_url();?>">Dashboard</a>
					</li>
					
					<?php
					if($uri_segment_one == 'loan_overview')
					{
						echo '<li class="active">';
					}
					else
					{
						echo '<li>';
					}
					?>
						<a href="<?php echo base_url().'loan_overview';?>">Overview</a>
					</li>
					
					<?php
					if($uri_segment_one == 'load_data')
					{
						echo '<li class="active">';
					}
					else
					{
						echo '<li>';
					}
					?>
					
						<a  href="<?php echo base_url()."load_data"?>" class="dropdown-toggle">
						Loan Data </i>
						</a>
						
					</li>
					
					
					<?php
					if($uri_segment_one == 'borrower_data')
					{
						echo '<li class="active">';
					}
					else
					{
						echo '<li>';
					}
					?>

						<a href="<?php echo base_url();?>borrower_data">
						Borrower Data
						</a>
					</li>
					<?php
					if($uri_segment_one == 'investor_data')
					{
						echo '<li class="active">';
					}
					else
					{
						echo '<li>';
					}
					?>
						<a  href="<?php echo base_url();?>investor_data">
						Lender Data <!--<i class="fa fa-angle-down">--></i>
						</a>
						
					</li>
					<?php
					if($uri_segment_one == 'vendor_data' ) 
					{
						$class_vendor = 'class = "active"';
					}
					else
					{
						$class_vendor = '';
					}
					?>	
						<li <?php echo $class_vendor;?>>
						
						<a href="<?php echo base_url().'vendor_data';?>">Vendor Data</a>
						</li>
					
					<?php
					if($uri_segment_one == 'reports'  || $uri_segment_one == 'existing_loan_schdule' || $uri_segment_one == 'paid_of_loan_schedule' || $uri_segment_one == 'maturity_report_schedule' || $uri_segment_one == 'assigment_in_progress' || $uri_segment_one == 'loan_boarding_schedule' || $uri_segment_one == 'monthly_servicing_income' || $uri_segment_one == 'loan_schedule_borrower' || $uri_segment_one == 'loan_schedule_investor') 
					{
						echo '<li class="active">';
					}
					else
					{
						echo '<li>';
					}
					?>
						<a  href="<?php echo base_url();?>reports" class="dropdown-toggle">
						Reports <!--<i class="fa fa-angle-down">--></i>
						</a>
					
						
						<?php
						/* if($this->session->userdata('user_role') == 2)
						{ */
						?>
						<!--<li>
						
						<a href="<?php echo base_url().'users';?>">Users</a>
						</li>-->
						<?php /*} */ ?>
						
				</ul>
				
			</div>
			<!-- END MEGA MENU -->
		</div>
	</div>
	<!-- END HEADER MENU -->
</div>


<link rel="stylesheet" href="<?php echo base_url()?>assets/extra_css_js/jquery-ui.css">
<script src="<?php echo base_url()?>assets/extra_css_js/jquery-1.12.4.js"></script>
<script src="<?php echo base_url()?>assets/extra_css_js/jquery-ui.js"></script>
<script>
function show_menu_section(that)
{
	if(that.id == 'show')
	{
		that.id = 'display_block';
		$('.page-header-menu').css('display','block');
	}
	else
	{
		that.id = 'show';
		$('.page-header-menu').css('display','none');
	}
}

</script>

<!-------------------------------------SELECT WITH SEARCH------------>

<link rel="stylesheet" href="<?php echo base_url();?>assets/js/select_with_search/bootstrap-select.css">
<script src="<?php echo base_url();?>assets/js/select_with_search/bootstrap-select.js"></script>

<style>
	table{
		font-size:14px !important;
	}
</style>