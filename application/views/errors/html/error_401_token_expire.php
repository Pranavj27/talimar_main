<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php echo $title?></title>

<style>
	* {
		-webkit-box-sizing: border-box;
		box-sizing: border-box;
	}

	body {
		padding: 0;
		margin: 0;
	}

	#notfound {
		position: relative;
		height: 100vh;
	}

	#notfound .notfound {
		position: absolute;
		left: 50%;
		top: 50%;
		-webkit-transform: translate(-50%, -50%);
		-ms-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
	}

	.notfound {
		max-width: 920px;
		width: 100%;
		line-height: 1.4;
		text-align: center;
		padding-left: 15px;
		padding-right: 15px;
	}

	.notfound .notfound-404 {
		position: absolute;
		height: 100px;
		top: 0;
		left: 50%;
		-webkit-transform: translateX(-50%);
		-ms-transform: translateX(-50%);
		transform: translateX(-50%);
		z-index: -1;
	}

	.notfound .notfound-404 h1 {
		font-family: 'Maven Pro', sans-serif;
		color: #ececec;
		font-weight: 900;
		font-size: 276px;
		margin: 0px;
		position: absolute;
		left: 50%;
		top: 50%;
		-webkit-transform: translate(-50%, -50%);
		-ms-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
	}

	.notfound h2 {
		font-family: 'Maven Pro', sans-serif;
		font-size: 30px;
		color: #000;
		font-weight: normal;
		text-transform: uppercase;
		margin: 0px;
	}

    .notfound h2 span{
        font-family: 'Maven Pro', sans-serif;
        font-size: 12px;
        color: #000;
        font-weight: normal;
        text-transform: uppercase;
        margin: 0px;
    }

	@media only screen and (max-width: 480px) {
		.notfound .notfound-404 h1 {
			font-size: 140px;
		}
		.notfound h2 {
			font-size: 20px;
		}
	}
</style>
</head>
<body>
	<div id="notfound">
		<div class="notfound">
			<div class="notfound-404">
				<h1><?php echo $errorCode;?></h1>
			</div>
			<h2><?php echo $errorMsg?></h2>
		</div>
	</div>
</body>
</html>