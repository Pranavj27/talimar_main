<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php ob_start(); ?>
<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

    <h4>An uncaught Exception was encountered</h4>

    <p>Type: <?php echo get_class($exception); ?></p>
    <p>Message: <?php echo $message; ?></p>
    <p>Filename: <?php echo $exception->getFile(); ?></p>
    <p>Line Number: <?php echo $exception->getLine(); ?></p>

    <?php if (defined('SHOW_DEBUG_BACKTRACE') && SHOW_DEBUG_BACKTRACE === TRUE): ?>

        <p>Backtrace:</p>
        <?php foreach ($exception->getTrace() as $error): ?>

            <?php if (isset($error['file']) && strpos($error['file'], realpath(BASEPATH)) !== 0): ?>

                <p style="margin-left:10px">
                    File: <?php echo $error['file']; ?><br/>
                    Line: <?php echo $error['line']; ?><br/>
                    Function: <?php echo $error['function']; ?>
                </p>
            <?php endif ?>

        <?php endforeach ?>

    <?php endif ?>

</div>
<?php
$content = ob_get_contents();
ob_end_clean();
        

$content = $content . "<br><br> Page Referral - " . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$content = $content . "<br><br> IP Address - " . get_client_ip();//$_SERVER['REMOTE_ADDR'];
$content = $content . "<br><br> Browser Used - " . $_SERVER['HTTP_USER_AGENT'];
print($content);
// if($_SERVER['HTTP_HOST']!='localhost') {
//     $ci =& get_instance();
//     $ci->email->initialize(SMTP_SENDGRID);
//     $ci->email->from('mail@talimarfinancial.com', 'TaliMar Financial');
//     $ci->email->to('chetan@bitcot.com');
//     $ci->email->cc('intjarkhan@bitcot.com','deepesh@bitcot.com');        
//     $ci->email->subject('Talimar Financial '.http_response_code().' Internal Server Error');
//     $ci->email->message($content);
//     $ci->email->set_mailtype('html'); 
//     $ci->email->send();
// }else{
//     print($content);
// }
?>

