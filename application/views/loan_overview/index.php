<?php

$search_by_option = $this->config->item('search_by_option');
$borrower_option = $this->config->item('borrower_option');
$yes_no_option = $this->config->item('yes_no_option');
$yes_no_option3 = $this->config->item('yes_no_option3');
$loan_type_option = $this->config->item('loan_type_option');
$transaction_type_option = $this->config->item('transaction_type_option');
$position_option = $this->config->item('position_option');
$loan_payment_type_option = $this->config->item('loan_payment_type_option');
$loan_payment_schedule_option = $this->config->item('loan_payment_schedule_option');
$loan_intrest_type_option = $this->config->item('loan_intrest_type_option');
$loan_purpose_option = $this->config->item('loan_purpose_option');
$escrow_account_option = $this->config->item('escrow_account_option');
$calc_day_option = $this->config->item('calc_day_option');
$min_bal_type_option = $this->config->item('min_bal_type_option');
$STATE_USA = $this->config->item('STATE_USA');
$closing_status_option = $this->config->item('closing_status_option');
$construction_status_option = $this->config->item('construction_status_option');
$loan_status_option = $this->config->item('loan_status_option');
$boarding_status_option = $this->config->item('boarding_status_option');
$property_type_option = $this->config->item('property_modal_property_type');
$usa_city_county = $this->config->item('usa_city_county');

$paidoff_status_option = $this->config->item('paidoff_status_option');
$affiliated_option = $this->config->item('affiliated_option');
$loan_reserve_draw_status = $this->config->item('loan_reserve_draw_status');
$template_yes_no_option = $this->config->item('template_yes_no_option');
$no_yes_option = $this->config->item('no_yes_option');

//21-07-2021 By @Aj optional_insurance_option
     $optional_insurance_option				= $this->config->item('optional_insurance_option');

/* fetch loans... */
$loan_id = $this->uri->segment(2);
if ($loan_id == 'new') {
	$btn_name = 'new';
} elseif ($loan_id) {
	$fetch['id'] = $loan_id;

	if ($this->session->userdata('user_role') == 1) {

		$user_settings = $this->User_model->query("SELECT * FROM `user_settings` WHERE `user_id`= '" . $this->session->userdata('t_user_id') . "' AND `loan_organization`='1'");
		if ($user_settings->num_rows() > 0) {

			$fetch['t_user_id'] = $this->session->userdata('t_user_id');
			$fetch_loan_data = $this->User_model->query("SELECT * FROM loan WHERE (t_user_id = '".$this->session->userdata('t_user_id')."' OR loan_originator = '".$this->session->userdata('t_user_id')."') ");
		} else {
			$fetch_loan_data = $this->User_model->select_where('loan', $fetch);
		}
	} else {
		$fetch_loan_data = $this->User_model->select_where('loan', $fetch);
	}


	if ($fetch_loan_data->num_rows() > 0) {
		$fetch_loan_result = $fetch_loan_data->result();
	} else {

		$this->session->set_flashdata('error', 'You have no permission to access this loan.');
		redirect(base_url() . "load_data", 'refresh');
	}

	$fetch_loan_result = $fetch_loan_data->result();
	foreach ($fetch_loan_result as $row) {

		
		$talimar_loan = $row->talimar_loan;
		$fci = $row->fci;
		$borrower = $row->borrower;
		$loan_amount = $row->loan_amount;
		$current_balance = $row->current_balance;
		$lender_fee = $row->lender_fee . '%';
		$intrest_rate = $row->intrest_rate;
		$term_month = $row->term_month;
		$per_payment_period = $row->per_payment_period;
		$payment_held_close = $row->payment_held_close;
		$calc_of_year = $row->calc_of_year;
		$loan_type = $row->loan_type;
		$transaction_type = $row->transaction_type;
		$position = $row->position;
		$payment_type = $row->payment_type;
		$exit_strategy = $row->exit_strategy;
		$loan_highlights = $row->loan_highlights;
		$baloon_payment_type = $row->baloon_payment_type;
		$payment_sechdule = $row->payment_sechdule;
		$intrest_type = $row->intrest_type;
		$payment_amount = $row->payment_amount;
		$minium_intrest = $row->minium_intrest;
		$min_intrest_month = $row->min_intrest_month;
		$owner_occupied = $row->owner_occupied;
		$owner_purpose = $row->owner_purpose;
		$costruction_loan = $row->costruction_loan;
		$project_cost = $row->project_cost;
		$total_project_cost = $row->total_project_cost;
		$borrower_enquity = $row->borrower_enquity;
		$grace_period = $row->grace_period;
		$late_charges_due = $row->late_charges_due;
		$default_rate = $row->default_rate . "%";
		$return_check_rate = $row->return_check_rate;
		$refi_existing = $row->refi_existing;
		$loan_document_date = $row->loan_document_date ? date('m-d-Y', strtotime($row->loan_document_date)) : '';
		$loan_funding_date = $row->loan_funding_date ? date('m-d-Y', strtotime($row->loan_funding_date)) : '';
		$first_payment_date = $row->first_payment_date ? date('m-d-Y', strtotime($row->first_payment_date)) : '';
		$maturity_date = $row->new_maturity_date ? date('m-d-Y', strtotime($row->new_maturity_date)) : '';
		$modify_maturity_date = $row->modify_maturity_date ? date('m-d-Y', strtotime($row->modify_maturity_date)) : '';
		$extention_option = $row->extention_option;
		$extention_month = $row->extention_month;
		$extention_percent_amount = $row->extention_percent_amount;
		
		$ballon_payment = ($row->loan_amount) + (($row->intrest_rate / 100) * $row->loan_amount / 12);

		$trustee = $row->trustee;
		$promissory_note_val = $row->promissory_note_val;
		$payment_requirement = $row->payment_requirement;
		$add_broker_data_desc = $row->add_broker_data_desc;
		$min_bal_type = $row->min_bal_type;
		$payment_gurranty = $row->payment_gurranty;
		$payment_gurr_explain = $row->payment_gurr_explain;
		$min_payment = $row->min_payment;
		$interest_reserve = $row->interest_reserve;

	}
} else {
	$talimar_loan = '';
	$fci = '';
	$borrower = '';
	$loan_amount = '';
	$current_balance = '';
	$lender_fee = '';
	$intrest_rate = '';
	$term_month = '';
	$per_payment_period = '';
	$payment_held_close = '';
	$calc_of_year = '';
	$loan_type = '';
	$transaction_type = '';
	$position = '';
	$payment_type = '';
	$payment_sechdule = '';
	$intrest_type = '';
	$payment_amount = '';
	$minium_intrest = '';
	$min_intrest_month = '';
	$owner_occupied = '';
	$owner_purpose = '';
	$costruction_loan = '';
	$project_cost = '';
	$total_project_cost = '';
	$borrower_enquity = '';
	$grace_period = '';
	$late_charges_due = '';
	$default_rate = '';
	$return_check_rate = '';
	$refi_existing = '';
	$loan_document_date = '';
	$loan_funding_date = '';
	$first_payment_date = '';
	$maturity_date = '';
	$extention_option = '';
	$extention_month = '';
	$extention_percent_amount = '';
	$ballon_payment = '';
	$trustee = '';
	$promissory_note_val = '';
	$baloon_payment_type = '';
	$payment_requirement = '';
	$exit_strategy = '';
	$loan_highlights = '';
	$add_broker_data_desc = '';
	$min_bal_type = '';
	$payment_gurranty = ''; /* by default set with 1... */
	$payment_gurr_explain = '';
	$min_payment = '';
	$interest_reserve = '';

}

/* vendor names for trustee drop-down options... */
foreach ($all_vendor_data as $vendor) {
	$vendor_name[$vendor->vendor_id] = $vendor->vendor_name;
}

$btn_name = isset($btn_name) ? $btn_name : '';
?>

<style>
.view_inputs{
	font-size : 14px;
}
div#loan-data-page-top-heading {
    background: #eeeeee;
    padding: 0px !important;
    margin: 0px;
}
div#loan-data-page-top-heading div.col-sm-4, div.col-sm-8 {
    padding: 0px !important;
    margin: 0px !important;
}
ul#loan-data-sidebar-right {
    padding-top: 97px;
}
div.page-head-page-load-data .talimar_no_dropdowns.col-sm-6 {
    padding-bottom: 0px;
}

.row {
    margin-right: 0px;
    margin-left:  0px;
}
.page-head {
    margin-right: 15px;
}
.talimar_no_dropdowns div {
    float: left;
}
.inner-talimar_no_dropdowns {
    float: right !important;
}
@media only screen and (min-width: 992px) and (max-width: 1199px) {
	.row {
    margin-left:  0px !important;
}
.page-head-page-load-data .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    width: 150px !important;
}

.talimar_no_dropdowns select {
    width: 150px !important;
}


}

@media only screen and (min-width: 768px) and (max-width: 992px) {
	.row {
    margin-left:  0px !important;
}
.page-sidebar .page-sidebar-menu > li > a, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu > li > a {
    padding: 11px 11px !important;
}
.page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu > li > a, .page-sidebar .page-sidebar-menu > li > a {
    border-top: 1px solid white;
    border-right: 1px solid #ffffff82;
}

.page-head-page-load-data .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    width: 142px !important;
}

.inner-talimar_no_dropdowns{
	margin-right:10px;
}
.talimar_no_dropdowns select {
    width: 142px
}
ul#loan-data-sidebar-right {
    padding-top: 0px !important;
}
}
@media (max-width: 767px) {
.page-content-wrapper .page-content {
    padding: 0 !important;
    float: left;
	width: 100% !important;
}


.inner-talimar_no_dropdowns{
	margin-right: 0px !important;
}
.page-head-page-load-data .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
     width: 100% !important;
}

.talimar_no_dropdowns select {
        width: 100% !important;
		margin-top: 8px !important;
}
.div#loan-data-page-top-heading .col-sm-3{
	margin-right:0px !important;
}

div#page-loan-data-info div.row {
    margin-right: 0px;
}
div#page-loan-data-info h3 {
    margin-right: 0px !important;
}

.page-header .page-header-top .top-menu {
    display: block;
    clear: none !important;
    margin-top: 0px !important;
}
ul#loan-data-sidebar-right {
    padding-top: 0px;
}
.loan-overview-top-buttons ul.block-option {
    list-style-type: none;
    float: right;
    width: 100% !important;
    margin-bottom: 20px !important;
    margin: 0;
    padding: 0;
}
.loan-overview-top-buttons ul{
	display:none !important;
}

.loan-overview-top-buttons ul.block-option li {
    float: left;
    margin: 0 !important;
    width: 50% !important;
}
.loan-overview-top-buttons ul.block-option li .btn {
    width: 99% !important;
}
.talimar_no_dropdowns.col-sm-6 {
    float: left;
    width: 100%;
}
.talimar_no_dropdowns div {
    float: left;
    width: 99%;
}

}
@media (max-width: 469px) {
.page-header .page-header-top .top-menu {
    display: block;
    clear: none !important;
    margin-top: 0px !important;
}
ul#loan-data-sidebar-right {
    padding-top: 0px;
}
.loan-overview-top-buttons ul.block-option {
    list-style-type: none;
    float: right;
    width: 100% !important;
    margin-bottom: 20px !important;
    margin: 0;
    padding: 0;
}
.loan-overview-top-buttons ul.block-option li {
    float: left;
    margin: 0 !important;
    width: 100% !important;
}
.inner-talimar_no_dropdowns{
	margin-right: 0px !important;
}
}

label{

	font-weight:600 !important;
}

.table-scrollable table#add_test_new_row td {
    min-width: 110px !important;
}
.table-scrollable table#add_test_new_row td.draw_width {
    min-width: 460px !important;
}
.table-scrollable table#add_test_new_row th.draw_width {
    min-width: 460px !important;
}

.table-scrollable table#add_test_new_row td#counting {
    min-width: 15px !important;
	text-align:center;
}

.table-scrollable table#add_test_new_row th#counting {
    min-width: 15px !important;
	text-align:center;
}
</style>

<?php

	$property_addresss = $primary_loan_property->property_address;
?>

<!-- BEGIN PAGE HEAD -->
<div class="page-head-page-load-data">
	<!-- BEGIN PAGE TITLE -->
	<div class="page-title col-sm-6">
		<!--<h3> &nbsp; Loan Overview : <?php// echo '#'.$talimar_loan; ?> <small></small></h3>-->
		<h3> &nbsp; Loan Overview: <?php echo $property_addresss; ?> <small></small></h3>
	</div>

	<div class="talimar_no_dropdowns col-sm-6">
		<div class="inner-talimar_no_dropdowns al_box">
			<div>
				<form method="POST" action="<?php echo base_url(); ?>load_data" id="form_search_loan_by">
					Search by:
					<select onchange="search_loan_by()" name="search_by">
						<?php foreach ($search_by_option as $key => $row) { ?>
						<option value="<?php echo $key; ?>" <?php if ($key == $selected_by) {echo 'selected';}?> ><?php echo $row; ?></option>
						<?php }?>
					</select>
				</form>
			</div>

			<div>
				<select id = "search_loan_id" name="loan_id" onchange="loan_form_submit(this)" class="selectpicker" data-live-search="true">
					<option value=''></option>
					<?php 
					foreach ($fetch_search_option as $key => $row) {
						$optionStatus=0;  
						if(!empty($all_particular_data)){
							$color ='style="color:white;background-color:#808080;" ';
							foreach($all_particular_data as $userRow){
								if($userRow->id==$fetch_search_option[$key]['id']){
									$color='';
									$optionStatus=1;
									break;
								}
							}
						}else{
							if($row['loan_status']=='A')
							{$color = "style='color:green'";}
							else if($row['loan_status']=='C')
							{$color = "style='color:red'";}
							else{
								$color = '';
							}
						}
						$valueStatus =  $fetch_search_option[$key]['text']. ' <span '.$color.'>( '.$row['loan_status']. ' )'; ?></span>

					?>
					<option status="<?php echo $optionStatus;?>" <?php echo $color; ?> value = "<?php echo $fetch_search_option[$key]['id']; ?>" <?php if ($fetch_search_option[$key]['id']) {if ($fetch_search_option[$key]['id'] == $loan_id) {echo 'selected';}}?> ><?php echo $valueStatus; ?></option>
					<?php } ?>
				</select>
				<input type = "hidden" id = "user_login_status" name = "user_login_status" value = "<?php echo $userLoginStatus ;?>" />
			</div>

		</div>

		<!-- END PAGE TOOLBAR -->
	</div>


	<div class="page-content-wrapper">
		<div class="page-content" id="page-loan-data-info" >
			<?php if ($this->session->flashdata('error') != '') {?>
				<div id='error'><?php echo $this->session->flashdata('error'); ?></div>
			<?php }?>
			<?php if ($this->session->flashdata('success') != '') {?>
				<div id='success'><?php echo $this->session->flashdata('success'); ?></div>
			<?php }?>
			

			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					Dashboard
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->


			<!---   FORM START  --->
			<div class="row" id = "loan-data-page-top-heading">
				<div class="col-sm-8">
					<h3>Loan Information (<?php echo $talimar_loan ? '#' . $talimar_loan : ''; ?>)</h3>
				</div>
				<div class="col-sm-4">
					<div class="loan-overview-top-buttons">
						<ul class="block-option">
							<li><a href="javascript:void(0);" onclick = "open_loan_terms()"><button class="btn btn-primary">New</button></a></li>
							<?php if ($user_role == 2) { ?>
								<li><button class="btn red" onclick="delete_loan()">Delete</button></li>
								<?php } ?>
						</ul>
					</div>
				</div>
			</div>
<script>


myVar = window.setInterval( function(){

	if($('#loan-data-page-top-heading').length == '1')
	{

		$("#loading").delay(50).fadeOut("fast");
		stop();

	}

} , 50 );


function stop() {
	clearInterval(myVar);

}

</script>
			<div class="row">

				<div class="col-sm-3">
					<input  type="hidden" id="btn_name" name="btn_name" value="<?php echo $btn_name; ?>">
					<label class="label-control">Loan Amount:</label>
					<span class = "view_inputs"><?php echo $loan_amount ? '$' . number_format($loan_amount, 2) : ''; ?></span>

				</div>

				<div class="col-sm-3">
					<label class="label-control">Outstanding Balance:</label>
					<span class = "view_inputs"><?php echo $outstanding_balance ? '$' . number_format($outstanding_balance, 2) : ''; ?></span>

				</div>

				<div class="col-sm-3">
					<label class="label-control">Loan Type:</label>
					<span class = "view_inputs"><?php echo $loan_type ? $loan_type_option[$loan_type] : ''; ?></span>

				</div>
				



				<div class="col-sm-3">
					<label class="label-control">Accrued Charges:</label>

					<?php
						$accured_bal = 0;
						$accuredtotal = 0;
						$accuredpaid_amountsssx = 0;
						
						if (isset($fetch_accured_data) && is_array($fetch_accured_data)) {
							foreach ($fetch_accured_data as $key => $row) {
								
									$accured_bal += $row->accured_amount;

									$accuredtotal += (double)$row->accured_amount;
									$accuredpaid_amountsssx += (double)$row->paid_amount;
							
							}
							$toAccredTotal = $accuredtotal - $accuredpaid_amountsssx;
							
							$accured_bal = $toAccredTotal;
						} else {
							$accured_bal = '0.00';
						}

					?>

					<?php if ($accured_bal > 0) {?>

						<span class ="view_inputs" style="color:red;">$<?php echo number_format($accured_bal, 2); ?></span>

					<?php } else {?>

						<span class ="view_inputs">$<?php echo number_format($accured_bal, 2); ?></span>

					<?php }?>

				</div>

			</div>

			<div class="row">
				<div class="col-sm-3">
					<label class="label-control">Underwriting Status:</label>
					<?php if ($fetch_all_input_datas['underwriting_page']['delligenge_required'] == 1) {?>
						<span class = "view_inputs">Completed</span>
					<?php } else {?>
						<span class = "view_inputs" style="color:#ff0000;">Processing</span>
					<?php }?>
				</div>
				<div class="col-sm-3">
					<label class="label-control">Closing Status:</label>
				<span class = "view_inputs"><?php echo isset($loan_servicing_data) ? $closing_status_option[$loan_servicing_data[0]->closing_status] : ''; ?></span>

				</div>

				<?php
				$file_status = '';
				if ($loan_servicing_data[0]->loan_status != 2) {

					$file_status = 'Not Applicable';

				} elseif ($loan_servicing_data[0]->loan_status == 2 && $fetch_all_input_datas['loan_servicing_checklist']['checklist_complete'] == 0 || $fetch_all_input_datas['loan_servicing_checklist']['checklist_complete'] == null) {

					$file_status = 'Processing';
				} elseif ($loan_servicing_data[0]->loan_status == 2 && $fetch_all_input_datas['loan_servicing_checklist']['checklist_complete'] == 1) {

					$file_status = 'Complete';

				} else {

					$file_status = '';
				}

				?>
				<div class="col-sm-3">
					<label class="label-control">File Status:</label>
					<?php if ($file_status == 'Processing') {?>
					<span class = "view_inputs" style="color:#ff0000;"><?php echo $file_status; ?></span>
					<?php } else {?>
					<span class = "view_inputs"><?php echo $file_status; ?></span>
					<?php }?>
				</div>

				<?php
				$loan_status = '';
				if ($loan_servicing_data[0]->loan_status == 1) {
					$loan_status = 'Pipeline';
				} elseif ($loan_servicing_data[0]->loan_status == 2) {
					$loan_status = 'Active';
				} elseif ($loan_servicing_data[0]->loan_status == 3) {
					$loan_status = 'Paid off';
				} elseif ($loan_servicing_data[0]->loan_status == 4) {
					$loan_status = 'Cancelled';
				} elseif ($loan_servicing_data[0]->loan_status == 5) {
					$loan_status = 'Brokered';
				} elseif ($loan_servicing_data[0]->loan_status == 6) {
					$loan_status = 'Term Sheet';
				} elseif ($loan_servicing_data[0]->loan_status == 7) {
					$loan_status = 'Real Estate Owned';
				} else {
					$loan_status = 'N/A';
				}

				?>
				

				<div class="col-sm-3">
					<label class="label-control">Loan Status:</label>
					<span class = "view_inputs"><?php echo $loan_status; ?></span>


				</div>

			</div>

			<div class="row">
				<div class="col-sm-3">
					<label class="label-control" style="color">Interest Rate:</label>
					<span class = "view_inputs"><?php echo $intrest_rate ? number_format($intrest_rate, 3) . '%' : '0.0%'; ?></span>


				</div>
				<?php
				$aa = 0;				
				if(isset($fetch_impound_account)){
					
					foreach($fetch_impound_account as $key=>$row){
						$impound_amount = $row->amount;
						$readonly = '';
												
						if($row->items == 'Mortgage Payment')
						{
								 $aa +=($outstanding_balance*($fetch_loan_result[0]->intrest_rate)/100)/12;
						}
						if($row->items == 'Property / Hazard Insurance Payment')
						{
							if($row->impounded == 1){
								$aa += isset($loan_property_result) ? ($loan_property_insurance_result->annual_insurance_payment / 12) : 0;
							}
						}
												
						if($row->items == 'County Property Taxes')
						{
							if($row->impounded == 1){
								$aa += isset($loan_property_result) ? ($loan_property_result[0]->annual_property_taxes / 12) : 0;
							}
						}
						if($row->items == 'Flood Insurance')
						{
							if($row->impounded == 1){
								$aa += isset($row->amount) ? ($row->amount) : 0;
							}
						}
												
						if($row->items == 'Mortgage Insurance')
						{
							if($row->impounded == 1){
								$aa += isset($row->amount) ? ($row->amount) : 0;
							}
						}
					}
				}

				$balloon_payment = $aa + $loan_amount;
				?>
				<div class="col-sm-3">
					<label class="label-control">Monthly Payment:</label>
					<span class = "view_inputs"><?php echo '$' . number_format($aa, 2); ?></span>


				</div>
				<div class="col-sm-3">
					<label class="label-control">Balloon Payment:</label>
					<span class = "view_inputs"><?php echo isset($balloon_payment) ? '$' . number_format($balloon_payment, 2) : ''; ?></span>

				</div>
					<div class="col-sm-3">
				<?php

				$total_uw_value = 0;
				$total_sl_value = 0;
				$sum_secondary_uw = 0;
				$sum_secondary_sl = 0;
				$total_of_senior_lien = 0;
				if (isset($fetch_property_home)) {
					foreach ($fetch_property_home as $key => $row) {

						$total_of_senior_lien = $total_of_senior_lien + $ecum_right_data[$row->id]['sum_current_balance'];

						if ($row->primary_property == 1) {
							$primary_uw_value = $row->future_value;

							$primary_senior_liens_value = $row->senior_liens ? $row->senior_liens : 0;

						}

						if ($row->primary_property == 2) {

							$secondary_uw_value[] = $row->future_value;

							$secondary_senior_liens_value[] = $row->senior_liens ? $row->senior_liens : 0;

							$sum_secondary_uw += $row->future_value;

							$sum_secondary_sl += $row->senior_liens;
						}

					}

					$total_uw_value = $primary_uw_value + $sum_secondary_uw;

					$total_sl_value = $primary_senior_liens_value + $sum_secondary_sl;
				}

				$LOAN_TO_UV = ($fetch_loan_result[0]->loan_amount + $total_of_senior_lien) / $total_uw_value * 100;

				?>

				<?php

				$total_cv_value = 0;
				$total_cv_sl_value = 0;
				$sum_secondary_cv = 0;
				$sum_secondary_cv_sl = 0;
				$total_of_senior_lien = 0;
				if (isset($fetch_property_home)) {
					foreach ($fetch_property_home as $key => $row) {

						if($ecum_right_data[$row->id]['existing_priority_to_talimar'] == '1'){
							$total_of_senior_lien = $total_of_senior_lien + $ecum_right_data[$row->id]['sum_current_balance'];
						}

						if ($row->primary_property == 1) {
							$primary_cv_value = $row->current_value;

							$primary_senior_liens_value = $row->senior_liens ? $row->senior_liens : 0;

						}

						if ($row->primary_property == 2) {

							$secondary_cv_value[] = $row->current_value;

							$secondary_senior_liens_value[] = $row->senior_liens ? $row->senior_liens : 0;

							$sum_secondary_cv += $row->current_value;

							$sum_secondary_cv_sl += $row->senior_liens;
						}

					}

					$total_cv_value = $primary_cv_value + $sum_secondary_cv;

					$total_cv_sl_value = $primary_senior_liens_value + $sum_secondary_cv_sl;
				}

				$LOAN_TO_CV = ($fetch_loan_result[0]->loan_amount + $total_of_senior_lien) / $total_uw_value * 100;


				?>
					<label class="label-control">Loan to Value:</label>
					<span class = "view_inputs"><?php echo number_format($LOAN_TO_CV, 2) . '%'; ?></span>


				</div>

			</div>

			<div class="row">
				<div class="col-sm-3">
					<label class="label-control">Position:</label>
					<span class = "view_inputs"><?php echo $position ? $position_option[$position] : ''; ?></span>



				</div>
				<div class="col-sm-3">

					<label class="label-control"># of Properties:</label>
					<span class = "view_inputs"><?php echo $fetch_total_property_home; ?></span>


				</div>

				<div class="col-sm-3">
					<label class="label-control">Term:</label>
					<span class = "view_inputs"><?php echo $term_month; ?> Months</span>


				</div>


			</div>

			<div class="row">
				<div class="col-sm-3">
					<label class="label-control">Payment Due Date:</label>
					<span class = "view_inputs"><?php echo $first_payment_date; ?></span>


				</div>

				<div class="col-sm-3">
					<label class="label-control">Origination Date:</label>
					<span class = "view_inputs"><?php echo $loan_funding_date; ?></span>

				</div>

				<div class="col-sm-3">
					<label class="label-control">Maturity Date:</label>
					<span class = "view_inputs"><?php echo $maturity_date; ?></span>

				</div>

				<?php

					if ($loan_servicing_data[0]->loan_status == 3) {

						$payoff_date_value = $loan_servicing_data[0]->payoff_date ? date('m-d-Y', strtotime($loan_servicing_data[0]->payoff_date)) : '';

					} else {

						$payoff_date_value = 'N/A';

					}
				?>

				<div class="col-sm-3">
					<label class="label-control"> Payoff Date:</label>
					<span class = "view_inputs"><?php echo $payoff_date_value; ?></span>

				</div>

			</div>
			
			<div class="row">
				<div class="col-sm-3">
					<label class="label-control"> Payment Reserve:</label>
					<span class = "view_inputs"><?php echo $yes_no_option3[$interest_reserve]; ?></span>
				</div>
				
				<?php
					if($yes_no_option3[$interest_reserve] == 'Yes'){
						$hashof_payment = $payment_held_close;
					}else{
						$hashof_payment = 'Not Applicable';
					}
				?>
				
				<div class="col-sm-3">
					<label class="label-control"> # of Payments:</label>
					<span class = "view_inputs"><?php echo $hashof_payment; ?></span>
				</div>
				
				<?php
				if($yes_no_option3[$interest_reserve] == 'Yes'){
					if($loan_servicing_data[0]->payment_reserve_opt == '2'){
						$payment_reserve_opt = 'Depleted';
						$Txtcolor = 'style="color:red;"';
					}elseif($loan_servicing_data[0]->payment_reserve_opt == '3'){
						$payment_reserve_opt = 'Available Funds';
						$Txtcolor = '';
					}else{
						$payment_reserve_opt = 'Not Applicable';
						$Txtcolor = '';
					}
				}else{
					$payment_reserve_opt = 'Not Applicable';
					$Txtcolor = '';
				}
				?>
				
				<div class="col-sm-3">
					<label class="label-control"> Reserve Status:</label>
					<span class = "view_inputs" <?php echo $Txtcolor;?>><?php echo $payment_reserve_opt; ?></span>
				</div>
			</div>
			
			<div class="row">
				<?php

				if ($extention_option == 1) {

					$month = $extention_month;
					$extension_fee = '$' . number_format(($extention_percent_amount / 100) * $loan_amount, 2);
				} else {

					$month = 'N/A';
					$extension_fee = 'N/A';
				}

				?>
				<div class="col-sm-3">
					<label class="label-control">Extension Option:</label>
					<span class = "view_inputs"><?php echo $yes_no_option[$extention_option]; ?></span>

				</div>
				<div class="col-sm-3">
					<label class="label-control"># of Months:</label>
					<span class = "view_inputs"><?php echo $month; ?></span>

				</div>
				<div class="col-sm-3">
					<label class="label-control">Extension Fee:</label>
					<span class = "view_inputs"><?php echo $extension_fee; ?></span>

				</div>
			</div>

			<div class="row">
			<?php
			if ($min_payment == 1) {

				$Payments = $minium_intrest;
			} else {
				$Payments = 'N/A';
			}
			?>
				<div class="col-sm-3">
					<label class="label-control">Prepayment Penalty:</label>
					<span class = "view_inputs"><?php echo $yes_no_option[$min_payment]; ?></span>

				</div>
				<div class="col-sm-3">
					<label class="label-control"># of Payments:</label>
					<span class = "view_inputs"><?php echo $Payments; ?></span>

				</div>
			</div>

			<h3>Loan Overview  <small></small></h3>

			<?php 
					if(!empty($loanDetailsFCI))
					{
						$totalPayment = $loanDetailsFCI->totalPayment;
						$nextPaymentDue = $loanDetailsFCI->nextPaymentDue;
						$loanMaturity = $loanDetailsFCI->loanMaturity;
						$aCHStatus = $loanDetailsFCI->aCHStatus;
						$aCHStatusEnum = $loanDetailsFCI->aCHStatusEnum;
					}
					else
					{
						$totalPayment = '';
						$nextPaymentDue = '';
						$loanMaturity = '';
						$aCHStatus = '';
						$aCHStatusEnum = '';
					}

			?>
			<div class="row">
				<div class="col-sm-3">
					<label class="label-control">Monthly Payment:</label>
					<span class = "view_inputs"><?php echo '$' . number_format($totalPayment, 2); ?></span>
				</div>
				<div class="col-sm-3">
					<label class="label-control">Next Payment Date:</label>
					<span class = "view_inputs">
					<?php 
						$nextPaymentDue = !empty($nextPaymentDue)?str_replace('/', '-', $nextPaymentDue):'';
						echo $nextPaymentDue;
					?>
					</span>
				</div>
				<div class="col-sm-3">
					<label class="label-control">Days Past Due:</label>
					<span class = "view_inputs"><?php echo $fciData->daysLate > 0 ? $fciData->daysLate : 0; ?></span>
				</div>
				<div class="col-sm-3"></div>
			</div>
			<div class="clearfix"></div>
			<div class="row">
				
				<div class="col-sm-3">
					<label class="label-control">Auto Debit:</label>
					<span class = "view_inputs"><?php echo (int)$aCHStatus > 0 ? 'Yes' : 'No'; ?></span>
				</div>
				<div class="col-sm-3">
					<label class="label-control">Auto Debit Day:</label>
					<span class = "view_inputs"><?php echo (int)$aCHStatus > 0 ? $aCHStatusEnum : 'N/A'; ?></span>
				</div>
				<div class="col-sm-6"></div>
				
			</div>
			<div class="clearfix"></div>
			<div class="row">
				<div class="col-sm-3">
					<label class="label-control">Maturity Date:</label>
					<span class = "view_inputs">
					<?php 
						$loanMaturity = str_replace('/', '-', $loanMaturity);
						echo $loanMaturity ;
					?>
					</span>
				</div>
				<div class="col-sm-3">
					<label class="label-control">Days to Maturity:</label>
					<span class = "view_inputs">
					<?php
						if(!empty($loanMaturity) && $loanMaturity != '00-00-0000')
						{
							$explode = explode('-', $loanMaturity);
							$explode = $explode[2].'-'.$explode[0].'-'.$explode[1];

							$end = strtotime($explode);

							$start = strtotime(date('Y-m-d'));
							$difference = $days_between = ceil(abs($end - $start) / 86400);
							echo $difference.' Days';
						}
					 	
						
					?>
	
					</span>
				</div>
				<div class="col-sm-3">
					<label class="label-control">Status:</label>
					<span class = "view_inputs">
						<?php 

						if($difference > 0)
						{
							echo '<span style="color:green">Current</spa>';
						}
						else{
							echo '<span style="color:red">Matured</span>';
						}

						?>
					</span>
				</div>
				<div class="col-sm-3"></div>
			</div>


			<h3>Property Information <small>(Primary Property)</small></h3>
			<div class="row">
				<div class="col-sm-6">
					<label class="label-control">Property Address:</label>
					<span class = "view_inputs"><?php echo isset($primary_loan_property) ? $primary_loan_property->property_address : ''; ?></span>


				</div>
				<div class="col-sm-3">
					<label class="label-control">Unit #:</label>
					<span class = "view_inputs"><?php echo isset($primary_loan_property) ? $primary_loan_property->unit : ''; ?></span>


				</div>
			</div>

			<div class="row">
				<div class="col-sm-3">
					<label class="label-control">Property City:</label>
					<span class = "view_inputs"><?php echo isset($primary_loan_property) ? $primary_loan_property->city : ''; ?></span>


				</div>
				<div class="col-sm-3">
					<label class="label-control">Property State:</label>
					<span class = "view_inputs"><?php echo isset($primary_loan_property) ? $primary_loan_property->state : ''; ?></span>


				</div>
				<div class="col-sm-3">
					<label class="label-control">Property Zip:</label>
					<span class = "view_inputs"><?php echo isset($primary_loan_property) ? $primary_loan_property->zip : ''; ?></span>
				</div>
				<div class="col-sm-3">
					<label class="label-control">Property County:</label>
					<span class = "view_inputs"><?php echo isset($primary_loan_property) ? $usa_city_county[$primary_loan_property->country] : ''; ?></span>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-3">
					<label class="label-control">Property Type:</label>
					<span class = "view_inputs"><?php echo isset($primary_loan_property) ? $property_type_option[$primary_loan_property->property_type] : ''; ?></span>

				</div>
				<div class="col-sm-3">
					<label class="label-control">APN:</label>
					<span class = "view_inputs"><?php echo isset($primary_loan_property) ? $primary_loan_property->apn: ''; ?></span>

				</div>
			</div>

			<div class="row">
				<div class="col-sm-3">
					<label class="label-control">Purchase Price:</label>
					<?php if ($primary_loan_property->sales_refinance_value == '' || $primary_loan_property->sales_refinance_value == '0') {?>

						<span class = "view_inputs"><?php echo '$' . number_format($primary_loan_property->purchase_price); ?></span>

					<?php }
					elseif($transaction_type == '2' || $transaction_type == '3'){?>

						<span class = "view_inputs">N/A</span>
					<?php }

					 else {?>

						<span class = "view_inputs">Not Applicable</span>

					<?php }?>

				</div>
				<div class="col-sm-3">
					<label class="label-control">Property Value:</label>
					<span class = "view_inputs"><?php echo isset($primary_loan_property) ? '$' . number_format($primary_loan_property->underwriting_value) : ''; ?></span>


				</div>


				<div class="col-sm-3">
					<label class="label-control">Payoff Value:</label>
					<span class = "view_inputs"><?php echo isset($primary_loan_property) ? '$' . number_format($primary_loan_property->sales_refinance_value, 2) : ''; ?></span>


				</div>
			</div>

			<div class="row">

				<div class="col-md-3">
					<label class="label-control">Property Size:</label>
					<span class = "view_inputs"><?php echo isset($primary_loan_property) ? number_format($primary_loan_property->square_feet) : ''; ?> SF</span>
				</div>
				<div class="col-md-3">
					<label class="label-control"># of Bedrooms:</label>
					<span class = "view_inputs"><?php echo isset($primary_loan_property) ? $primary_loan_property->bedrooms : ''; ?></span>
				</div>
				<div class="col-md-3">
					<label class="label-control"># of Bathrooms:</label>
					<span class = "view_inputs"><?php echo isset($primary_loan_property) ? $primary_loan_property->bathrooms : ''; ?></span>
				</div>
				<div class="col-md-3">
					<label class="label-control">Lot Size:</label>
					<span class = "view_inputs"><?php echo isset($primary_loan_property) ? number_format($primary_loan_property->lot_size) : ''; ?> SF</span>
				</div>

			</div>
			
			<div class="row">

				<div class="col-md-12">
					<label class="label-control">Property Link:</label>
					<span class = "view_inputs"><a href="<?php echo $primary_loan_property->property_link; ?>" target="_blank"><?php echo $primary_loan_property->property_link; ?></a></span>
				</div>
				
			</div>
			

			<h3>Borrower Information</h3>
			<div class="row">
				<div class="col-sm-6">
					<label class="label-control">Borrower Name:</label>
					<?php if (isset($fetch_loan_borrower)) {?>

					<span class = "view_inputs"><a onclick="redirect_to_page('borrower')"><?php echo $fetch_loan_borrower[0]->b_name; ?></a></span>
					<form method="POST" action="<?php echo base_url() . 'borrower_view'; ?>" id="redirect-borrower-view" >
						<input type="hidden" name="borrower_ids" value="<?php echo $fetch_loan_borrower[0]->id; ?>">
						<input type="hidden" name="btn_name" value="view">
						<input type="hidden" name="contact_id" value="">
					</form>


					<?php }?>
				</div>
				<?php
				

				?>
				<div class="col-sm-6">
					<label class="label-control">FCI Account #:</label>
					<span class = "view_inputs"><?php echo $fetch_loan_borrower[0]->fci_acc_text; ?></span>
				</div>
			</div>

			

			<?php 
			// array_multisort(array_column($b_contact, 'b_signed'), SORT_ASC, array_column($b_contact, 'last'), SORT_ASC, $b_contact);
		
			if(isset($b_contact) && is_array($b_contact)){

				//echo '<pre>'; print_r($b_contact); echo '</pre>';
			foreach ($b_contact as $key => $b_contactss) {

			$seleccontact_id1=$b_contactss['contact_id'];
			$seleccontact1=$b_contactss['first'].' '.$b_contactss['middle'].' '.$b_contactss['last'];

			if ($b_contactss['b_signed'] == '1') {

				 $b1_sign='Yes';
			} else {
				$b1_sign = 'No';
			}
				

			 ?>
			<div class="row">
				<div class="col-sm-3">
					<label class="label-control">Contact Name:</label>
					<span class = "view_inputs"><a  href = "<?php echo isset($seleccontact_id1) ? base_url() . 'viewcontact/' . $seleccontact_id1 : '' ?>"><?php echo isset($seleccontact1) ? $seleccontact1 : ''; ?></a>
					</span>
				</div>

				<div class="col-sm-3">
					<label class="label-control">Phone:</label>
					<span class = "view_inputs"><?php echo isset($b_contactss['contact_phone']) ? $b_contactss['contact_phone'] : ''; ?></span>

				</div>
				<div class="col-sm-3">
					<label class="label-control">E-Mail:</label>
						<span class = "view_inputs"><a  href="mailto:<?php echo isset($b_contactss['contact_email']) ? $b_contactss['contact_email'] : ''; ?>"><?php echo isset($b_contactss['contact_email']) ? $b_contactss['contact_email'] : ''; ?></a>
						</span>

				</div>



				<div class="col-sm-3">
					<label class="label-control">Sign Documents:</label>
						<span class = "view_inputs"><?php echo $b1_sign; ?></span>

				</div>
			</div>

	<?php  }}?>


			<div class="row">
				<div class="col-sm-6">
					<label class="label-control">Borrower Street Address:</label>
					<span class = "view_inputs"><?php echo isset($fetch_loan_borrower) ? $fetch_loan_borrower[0]->b_address : ''; ?></span>


				</div>
				<div class="col-sm-3">
					<label class="label-control">Borrower Unit #:</label>
					<span class = "view_inputs"><?php echo isset($fetch_loan_borrower) ? $fetch_loan_borrower[0]->b_unit : ''; ?></span>

				</div>

			</div>

			<div class="row">
				<div class="col-sm-3">
					<label class="label-control">Borrower City:</label>
					<span class = "view_inputs"><?php echo isset($fetch_loan_borrower) ? $fetch_loan_borrower[0]->b_city : ''; ?></span>


				</div>
				<div class="col-sm-3">

					<label class="label-control">Borrower State:</label>
					<span class = "view_inputs"><?php echo ((isset($fetch_loan_borrower)) && ($fetch_loan_borrower[0]->b_state)) ? $STATE_USA[$fetch_loan_borrower[0]->b_state] : ''; ?></span>


				</div>
				<div class="col-sm-3">
					<label class="label-control">Borrower Zip:</label>
					<span class = "view_inputs"><?php echo isset($fetch_loan_borrower) ? $fetch_loan_borrower[0]->b_zip : ''; ?></span>


				</div>
			</div>




			<h3>Guarantor Information</h3>

		<?php if ($fetch_payment_gurrantor) {
	$i = 0;
	foreach ($fetch_payment_gurrantor as $key => $payment_gurrantor) {
		$fetch_contact = $this->User_model->select_where('contact', array('contact_id' => $payment_gurrantor->contact_name));
		$fetch_contact = $fetch_contact->result();
		$fetch_contact_id = $fetch_contact[0]->contact_id;
		$full_name = $fetch_contact[0]->contact_firstname . ' ' . $fetch_contact[0]->contact_lastname;
		$i++;
		?>
			<div class="row">
				<div class="col-sm-3">
					<label class="label-control">Guarantor Name <?php echo $i; ?>:</label>
					<span class = "view_inputs"><a  href = "<?php echo isset($fetch_contact_id) ? base_url() . 'viewcontact/' . $fetch_contact_id : ''; ?>"><?php echo isset($full_name) ? $full_name : ''; ?></a>
					</span>
		    </div>
		   	<div class="col-sm-3">
					<label class="label-control phone-format">Phone:</label>
					<span class = "view_inputs"><?php echo $payment_gurrantor->phone ? $payment_gurrantor->phone : ''; ?></span>


			</div>

			<div class="col-sm-6">
					<label class="label-control">Guarantor E-Mail:</label>
					<span class = "view_inputs">
						<a  href="#"><?php echo $payment_gurrantor->email ? $payment_gurrantor->email : ''; ?></a>
					</span>

			</div>




		    </div>
	<?php }}?>

			<h3>Servicing Information</h3>

			<div class="row">
				<div class="col-sm-3">
					<label class="label-control">Servicer Name:</label>
					<span class = "view_inputs"><?php echo ((isset($loan_servicing_data)) && ($loan_servicing_data[0]->sub_servicing_agent)) ? $vendor_name[$loan_servicing_data[0]->sub_servicing_agent] : ''; ?></span>
				</div>
				<div class="col-sm-3">
					<label class="label-control">Loan Servicer #:</label>
					<span class = "view_inputs"><?php echo isset($fci) ? $fci : ''; ?></span>
				</div>

				<div class="col-sm-3">
					<label class="label-control">Boarding Status:</label>

					<?php
					
					 	if($servicing_boarding['1008'] == '1' && $servicing_boarding['1015'] == '1' && $servicing_boarding['1029'] == '1' && $servicing_boarding['1035'] != '1'){

					 		echo '<span class = "view_inputs">Final Boarding</span>';

					 	}elseif($servicing_boarding['1008'] == '1' && $servicing_boarding['1029'] == '1'){

					 		echo '<span class = "view_inputs">Boarding</span>';
					 	}elseif($servicing_boarding['1008'] == '1' && $servicing_boarding['1029'] != '1'){

					 		echo '<span class = "view_inputs">Pre-Boarding</span>';
					 	}elseif($servicing_boarding['1008'] != '1'){

					 		echo '<span class = "view_inputs">Pre-Submittal</span>';
					 	}else{
					 		echo '<span class = "view_inputs"></span>';
					 	}

					 ?>

				</div>



				<?php

				foreach ($loan_assigment_data as $row) {
					$nfiles_status[] = $row->nfiles_status;
				}

				if (in_array(0, $nfiles_status)) {
					$assigment = 'Processing';
					$style = 'style=color:red';
				} else {
					$assigment = 'Complete';
					$style = '';
				}
				?>
				<div class="col-sm-3">
					<label class="label-control">Assignment Status:</label>

						<span class = "view_inputs" <?php echo $style; ?>><?php echo $assigment; ?></span>
				</div>
			</div>

			<div class="row">
				<?php
				if ($loan_servicing_data[0]->servicing_lender_rate == 'NaN' || $loan_servicing_data[0]->servicing_lender_rate == '') {
					$lender_ratessss = $loan_assigment_data[0]->invester_yield_percent;
				} else {
					$lender_ratessss = $loan_servicing_data[0]->servicing_lender_rate;
				}

				$new_val = $intrest_rate - $lender_ratessss;

				$servicing_feees_new = (($new_val / 100) * $outstanding_balance) / 12;
				?>
				<div class="col-sm-3">
					<label class="label-control">Servicing Fee (%):</label>
					<span class = "view_inputs"><?php echo number_format($new_val, 2); ?>%</span>

				</div>

				<?php
				$servicing_fees_percent = isset($loan_servicing_data) ? $loan_servicing_data[0]->servicing_fee : 0;

				$loan_amount = $loan_amount ? $loan_amount : 0;

				$servicing_fees_dollar = ($servicing_fees_percent / 100) * $loan_amount;

				$count_lender = isset($loan_assigment_data) ? count($loan_assigment_data) : 0;

				$intrest_r = $intrest_rate ? $intrest_rate : 0;

				$servicing_feees = $servicing_fees_percent;

				$lender_yield_percent = $intrest_r - $servicing_feees;

				$lender_yield_dollar = ($lender_yield_percent / 100) * $loan_amount;

				$lender_net_payment = ($lender_yield_percent * $loan_amount) / 100;

				$lender_net_payment_monthly = ($lender_yield_percent * $loan_amount) / 1200;

				$minimum_servicing_fee = isset($loan_servicing_data) ? $loan_servicing_data[0]->minimum_servicing_fee : 0;

				$monthly_income_servicing = ((($servicing_feees / 100) / 12) * $loan_amount) - ($count_lender * $minimum_servicing_fee);

				$monthly_servicing_cost = $count_lender * $minimum_servicing_fee;

				$servicing_fee_cal = (($servicing_fees_percent / 100) * $loan_amount) / 12;

				$payment_pro_fees = $loan_amount;
				$pro_fees = '0';
				if($payment_pro_fees > 0 && $payment_pro_fees <= 400000){
					$pro_fees = '0';
				}elseif($payment_pro_fees >= 400001 && $payment_pro_fees <= 500000){
					$pro_fees = '25.00';
				}elseif($payment_pro_fees >= 500001 && $payment_pro_fees <= 600000){
					$pro_fees = '35.00';
				}elseif($payment_pro_fees >= 600001 && $payment_pro_fees <= 700000){
					$pro_fees = '45.00';
				}elseif($payment_pro_fees >= 700001 && $payment_pro_fees <= 800000){
					$pro_fees = '55.00';
				}elseif($payment_pro_fees >= 800001 && $payment_pro_fees <= 900000){
					$pro_fees = '65.00';
				}elseif($payment_pro_fees >= 900001 && $payment_pro_fees <= 1000000){
					$pro_fees = '75.00';
				}elseif($payment_pro_fees >= 1000001 && $payment_pro_fees <= 2000000){

					$pro_fees = '95.00';
				}
				elseif($payment_pro_fees >= 2000001 && $payment_pro_fees <= 3000000){

					$pro_fees = '115.00';
				}
				elseif($payment_pro_fees >= 3000001 && $payment_pro_fees <= 4000000){

					$pro_fees = '135.00';
				}
				elseif($payment_pro_fees >= 4000001 && $payment_pro_fees <= 5000000){

					$pro_fees = '155.00';
				}
				elseif($payment_pro_fees >= 5000001 && $payment_pro_fees <= 6000000){

					$pro_fees = '175.00';
				}
				elseif($payment_pro_fees >= 6000001 && $payment_pro_fees <= 7000000){

					$pro_fees = '195.00';
				}
				elseif($payment_pro_fees >= 7000001 && $payment_pro_fees <= 8000000){

					$pro_fees = '215.00';
				}
				elseif($payment_pro_fees >= 8000001 && $payment_pro_fees <= 9000000){

					$pro_fees = '235.00';
				}	
				elseif($payment_pro_fees >= 9000001 && $payment_pro_fees <= 10000000){

					$pro_fees = '255.00';
				}
				elseif($payment_pro_fees >= 10000001 && $payment_pro_fees <= 11000000){

					$pro_fees = '275.00';
				}
				elseif($payment_pro_fees >= 11000001 && $payment_pro_fees <= 12000000){

					$pro_fees = '295.00';
				}
				else{

					$pro_fees = '0';
				}


				$lender_servicing_fee = $count_lender * $minimum_servicing_fee;

				if($lender_servicing_fee > $pro_fees){

					$pro_fees = 0;

				}else{

					$lender_servicing_fee = 0;	

				}

				$servicing_cost = $lender_servicing_fee + $pro_fees + $loan_servicing_data[0]->broker_servicing_fees;
				?>
				<div class="col-sm-3">
					<label class="label-control">Servicing Fee ($):</label>
					<span class = "view_inputs"><?php echo '$' . number_format($servicing_feees_new, 2); ?></span>


				</div>
				<?php if ($loan_servicing_data[0]->who_pay_servicing == '2') {

					$servicing_co = 0;
					$net_service = $servicing_feees_new - $servicing_co;

				} else {

					$servicing_co = $servicing_cost;
					$net_service = $servicing_feees_new - $servicing_co;
				}

				?>
				<div class="col-sm-3">
					<label class="label-control">Servicing Cost ($):</label>
					<span class = "view_inputs"><?php echo '$' . number_format(($servicing_co), 2); ?></span>

				</div>
				<div class="col-sm-3">
					<label class="label-control">Net Servicing Income:</label>
					<span class = "view_inputs"><?php echo '$' . number_format($net_service, 2); ?></span>


				</div>
			</div>

			<div class="row">
				<div class="col-sm-3">
					<label class="label-control"># of Lenders:</label>
					<span class = "view_inputs"><?php echo $count_lender; ?></span>
				</div>
				<div class="col-sm-3">
					<label class="label-control">Lender Net Yield (%):</label>
					<span class = "view_inputs"><?php echo number_format($lender_ratessss, 2) . '%'; ?></span>
				</div>
				<?php $lender_net_yield_new_val = ($outstanding_balance * $lender_ratessss) / 12; ?>
				<div class="col-sm-3">
					<label class="label-control">Lender Net Yield ($):</label>
					<span class = "view_inputs"><?php echo '$' . number_format($lender_net_yield_new_val/100, 2); ?></span>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-3">
					<label class="label-control">Construction Status:</label>
					<span class = "view_inputs"><?php echo isset($loan_servicing_data) ? ($loan_servicing_data[0]->construction_status == '1' ? 'Active' : 'N/A') : ''; ?></span>
				</div>
			</div>

			<h3>Escrow Information</h3>
			<div class="row">
				<div class="col-sm-3">
					<label class="label-control">Escrow:</label>
					<span class = "view_inputs"><?php echo $escrow_contact_name['company']; ?></span>
				</div>

				<div class="col-sm-3">
				<?php if ($escrow_contact_name['full_name'] == '0') {?>

					<label class="label-control">Escrow Contact:</label>
					<span class = "view_inputs"><a  href = "<?php echo isset($escrow_contact_name) ? base_url() . 'viewcontact/' . $escrow_contact_name['c_idd'] : '' ?>"><?php echo $escrow_contact_name['first_name'] . ' ' . $escrow_contact_name['last_name']; ?></a></span>

				<?php } else {?>
					<label class="label-control">Escrow Contact:</label>
					<span class = "view_inputs"><a href="<?php echo isset($escrow_contact_name) ? base_url() . 'viewcontact/' . $escrow_contact_name['c_id'] : '' ?>"><?php echo $escrow_contact_name['full_name']; ?></a></span>
				<?php }?>

				</div>
				<div class="col-sm-3">
					<label class="label-control">Escrow Phone:</label>
					<span class = "view_inputs"><?php echo $escrow_contact_name['phone']; ?></span>
				</div>

				<div class="col-sm-3">
					<label class="label-control" style="float:left;">Escrow Email:</label>
					<span class = "view_inputs"><a href="mailto:<?php echo $escrow_contact_name['email']; ?>"><?php echo $escrow_contact_name['email']; ?></a></span>
				</div>
			</div>

			<h3>Title Information</h3>

			<div class="row">
				<div class="col-sm-3">
					<label class="label-control">Title:</label>
					<span class = "view_inputs"><?php echo $title_contact['company']; ?></span>

				</div>
				<div class="col-sm-3">
					<?php if ($title_contact['full_name'] == '0') {?>

					<label class="label-control">Title Contact:</label>
					<span class = "view_inputs"><a href="<?php echo isset($title_contact) ? base_url() . 'viewcontact/' . $title_contact['iddd'] : '' ?>"><?php echo $title_contact['first_name'] . ' ' . $title_contact['last_name']; ?></a></span>

					<?php } else {?>

					<label class="label-control">Title Contact:</label>
					<span class = "view_inputs"><a href="<?php echo isset($title_contact) ? base_url() . 'viewcontact/' . $title_contact['idd'] : '' ?>"><?php echo $title_contact['full_name']; ?></a></span>

					<?php }?>
				</div>
				<div class="col-sm-3">
					<label class="label-control phone-format">Title Phone:</label>
					<span class = "view_inputs"><?php echo $title_contact['phone']; ?></span>
				</div>
				<div class="col-sm-3">
					<label class="label-control">Title Email:</label>
					<span class = "view_inputs"><a href="mailto:<?php echo $title_contact['email']; ?>"><?php echo $title_contact['email']; ?></a></span>

				</div>
			</div>

			<div class="row">
				<div class="col-sm-3">
					<label class="label-control">Prelim Received:</label>
					<span class = "view_inputs"><?php echo $no_yes_option[$title_contact['prelim_receive']]; ?></span>
				</div>

				<div class="col-sm-3">
					<label class="label-control">Prelim Approved:</label>
					<span class = "view_inputs"><?php echo $no_yes_option[$title_contact['prelim_approve']]; ?></span>
				</div>
			</div>

			<h3>Other Contacts</h3>


			<?php if (isset($affiliated_parties_data) && is_array($affiliated_parties_data)) {

				foreach ($affiliated_parties_data as $key => $rows) {

					if ($rows->name != 'Select One') {

						$fetch_contact = $this->User_model->select_where('contact', array('contact_id' => $rows->name));
						$fetch_contact = $fetch_contact->result();
						$fetch_contact_id = $fetch_contact[0]->contact_id;
						$full_name = $fetch_contact[0]->contact_firstname . ' ' . $fetch_contact[0]->contact_middlename . '' . $fetch_contact[0]->contact_lastname;

			?>
			<div class="row">
				<div class="col-sm-3">
				<label class="label-control">Contacts Name:</label><span class = "view_inputs"><a target="_blank" href="<?php echo isset($fetch_contact_id) ? base_url() . 'viewcontact/' . $fetch_contact_id : '' ?>"><?php echo $full_name; ?></a></span>
				</div>
				<div class="col-sm-3">
					<label class="label-control">Role:</label>
					<span class = "view_inputs"><?php echo $rows->affiliation ? $affiliated_option[$rows->affiliation] : ''; ?></span>
				</div>
				<div class="col-sm-3">
					<label class="label-control">Phone:</label>
					<span class = "view_inputs"><?php echo $rows->phone ? $rows->phone : ''; ?></span>
				</div>
				<div class="col-sm-3">
					<label class="label-control">E-mail:</label>
					<span class = "view_inputs"><a href="mailto:<?php echo $rows->email; ?>"><?php echo $rows->email ? $rows->email : ''; ?></a></span>
				</div>
			</div>

			<?php }}}?>
		</div>
		<!---   FORM END  	--->
	</div>
	<!-- END CONTENT -->
</div>




<!----------------------------Test Modal----------------------------->


<!---------------------------- end Test Modal----------------------------->


<!----------------------------Wire Instruction2 Modal----------------------------->

<div class="modal fade bs-modal-lg" id="wire_information2" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Deposit Account: <?php echo $property_addresss; ?></h4>
			</div>
			<form method="POST" action="<?php echo base_url(); ?>form_wire_information2">
				<!-----------Hidden fields----------->
				<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
				<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
				<!-----------End of hidden Fields---->
				<div class="modal-body pad-inp-body">
					<div class="row">
						<div class="col-sm-6">
							<label>Name on Account:</label>
							<input type="text" class="form-control" name="account_name" value="<?php echo isset($fetch_draw_wire_instruction2) ? $fetch_draw_wire_instruction2[0]->account_name : ''; ?>">
						</div>

						<div class="col-sm-6">
							<label>Bank Name:</label>
							<input type="text" class="form-control" name="bank_name" value="<?php echo isset($fetch_draw_wire_instruction2) ? $fetch_draw_wire_instruction2[0]->bank_name : ''; ?>">
						</div>
					</div>



					<div class="row">
						<div class="col-sm-6">
							<label>Routing Number:</label>
							<input type="text" class="form-control" name="routing_number" value="<?php echo isset($fetch_draw_wire_instruction2) ? $fetch_draw_wire_instruction2[0]->routing_number : ''; ?>">
						</div>
						<div class="col-sm-6">
							<label>Account Number:</label>
							<input type="text" class="form-control" name="account_number" value="<?php echo isset($fetch_draw_wire_instruction2) ? $fetch_draw_wire_instruction2[0]->account_number : ''; ?>">
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn blue">Save</button>


					<?php if (!empty($modal_talimar_loan)) {?>


						<a href="<?php echo base_url(); ?>Load_data/print_instructions/<?php echo $modal_talimar_loan; ?>" class="btn blue">Print Instructions</a>

				<?php } else {?>


						<a class="btn blue" title="No Data in Loan Reserve" onclick="no_data_drawd(this);">Print Instructions</a>
				<?php }?>


					</div>
				</div>
			</form>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
</div>

<!----------------------------End of Wire Information2 Modal------------------->




<!----------------------------Wire Instruction Modal----------------------------->

<div class="modal fade bs-modal-lg" id="wire_information" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Wire Inforkjmation</h4>
			</div>
			<form method="POST" action="<?php echo base_url(); ?>form_wire_information">
				<!-----------Hidden fields----------->
				<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
				<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
				<!-----------End of hidden Fields---->
				<div class="modal-body pad-inp-body">
					<div class="row">
						<div class="col-sm-4">
							<label>Name on Account:</label>
							<input type="text" class="form-control" name="draw_borrower_name" value="<?php echo isset($fetch_draw_wire_instruction) ? $fetch_draw_wire_instruction[0]->draw_borrower_name : ''; ?>">
						</div>

						<div class="col-sm-4">
							<label>Bank Name:</label>
							<input type="text" class="form-control" name="draw_bank_name" value="<?php echo isset($fetch_draw_wire_instruction) ? $fetch_draw_wire_instruction[0]->draw_bank_name : ''; ?>">
						</div>
					</div>

					<div class="row">
						<div class="col-sm-4">
							<label>Bank Street Address:</label>
							<input type="text" class="form-control" name="draw_bank_street" value="<?php echo isset($fetch_draw_wire_instruction) ? $fetch_draw_wire_instruction[0]->draw_bank_street : ''; ?>">
						</div>
						<div class="col-sm-4">
							<label>Bank Unit #:</label>
							<input type="text" class="form-control" name="draw_bank_unit" value="<?php echo isset($fetch_draw_wire_instruction) ? $fetch_draw_wire_instruction[0]->draw_bank_unit : ''; ?>">
						</div>
					</div>

					<div class="row">
						<div class="col-sm-4">
							<label>Bank City:</label>
							<input type="text" class="form-control" name="draw_bank_city" value="<?php echo isset($fetch_draw_wire_instruction) ? $fetch_draw_wire_instruction[0]->draw_bank_city : ''; ?>">
						</div>
						<div class="col-sm-4">
							<label>Bank State:</label>
							<select type="text" class="form-control" name="draw_bank_state" >
								<?php
foreach ($STATE_USA as $key => $row) {
	?>
								<option value="<?php echo $key; ?>" <?php if (isset($fetch_draw_wire_instruction)) {if ($key == $fetch_draw_wire_instruction[0]->draw_bank_state) {echo 'selected';}}?> ><?php echo $row; ?></option>
								<?php
}
?>
							</select>
						</div>
						<div class="col-sm-4">
							<label>Bank Zip:</label>
							<input type="text" class="form-control" name="draw_bank_zip" value="<?php echo isset($fetch_draw_wire_instruction) ? $fetch_draw_wire_instruction[0]->draw_bank_zip : ''; ?>">
						</div>
					</div>

					<div class="row">
						<div class="col-sm-4">
							<label>Routing Number:</label>
							<input type="text" class="form-control" name="draw_routing_number" value="<?php echo isset($fetch_draw_wire_instruction) ? $fetch_draw_wire_instruction[0]->draw_routing_number : ''; ?>">
						</div>
						<div class="col-sm-4">
							<label>Account Number:</label>
							<input type="text" class="form-control" name="draw_account_number" value="<?php echo isset($fetch_draw_wire_instruction) ? $fetch_draw_wire_instruction[0]->draw_account_number : ''; ?>">
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn blue">Save</button>
					</div>
				</div>
			</form>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
</div>

<!----------------------------End of Wire Information Modal------------------->


<!----- Borrower data modal -------------------->
<div class="modal fade bs-modal-lg" id="Borrower_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Borrower Data</h4>
			</div>
			<div class="modal-body" id="load-borrower-content">
				Borrower Detail
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal" id="display-block">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-----End of Borrower data modal -------------------->


<div class="modal fade bs-modal-lg" id="property" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-footer">
				<button type="submit" value = "save" class="btn blue borrower_save_button" name="button">Save</button>
				<button type="button" class="btn default borrower_save_button" data-dismiss="modal">Close</button>

			</div>

		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>


<form method = "POST" action = "<?php echo base_url(); ?>delete_talimar_loan" id="delete_loan_form">
	<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>" >
</form>

<div id="image_section_div" style="display:none;">
	<div class="image_main_section" >
		<div class="images_part">
		<input type="hidden" name="marketing[]" value="0">
			<img src="<?php echo base_url(); ?>assets/images/no_image.png" />
		</div>
		<div class="image_description">
			<input type="file" class="select_images_file" name="files[]" onchange="change_this_image(this)">
			<input type="text" class="form-control input-md" name="discription[]" >
		</div>
	</div>
</div>

<div id="loan_document_div" style="display:none;">
	<div class="image_main_section" >
		<div class="images_part">
			<input type="hidden" name="doc_type[]" value="0">
				<div class="document_iframe_part">
					<h6>Upload File</h6>
				</div>
		</div>
		<div class="image_description">
			<input type="file" class="select_images_file" name="files[]" onchange="change_this_image(this)">
			<input type="text" class="form-control input-md" name="discription[]" value="">
		</div>
	</div>
</div>


<div id="album_number_new" style="display:none;">
	<div class="image_main_section" >
		<div class="images_part">
			<img src="<?php echo base_url(); ?>assets/images/no_image.png" />
		</div>
		<div class="image_description">
			<input type="hidden" name="title[]" value="" id="title_add">
			<input type="file" class="select_images_file" onchange="change_this_image(this)" name="files[]">
			<input type="text" class="form-control input-md" name="discription[]" >
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url("assets/js/load_data/load_data.js"); ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/load_data/load_data.css"); ?>" />
<script>
	// $( "#new_maturity_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
function delete_loan()
{
	/*var loan_id = $('#search_loan_id').val();*/
	var loan_id = '<?php echo $this->uri->segment(2) ?>';
	/*alert(loan_id);*/
	if(loan_id){

		if(confirm("Are you sure to delete")){
			$('#delete_loan_form').submit();
		}
	}else {
        alert('Please first select Loan!');
    }
}

function open_loan_terms(){
	var btn = $('#btn_name').val('new');
	window.location.href = "<?php echo base_url() . 'load_data/new' ?>";
	/* $('#loan_terms').modal('show');*/
}

function loan_form_submit(obj)
{
	var id= $('option:selected', obj).val();
	if($("#user_login_status").val()=="1"){
		var option = $('option:selected', obj).attr('status');
		if(option=="1"){
		  	window.location.href = "<?php echo base_url(); ?>load_data/"+id;
		}
	}else{
		window.location.href = "<?php echo base_url(); ?>load_data/"+id;
	}
	
	/* $("#select_loan_form").submit();*/
	
}

function add_image_sections(folder_id){
	var rowCount = $('.property_imgs_'+folder_id+' >div').length;
	rowCount++;
	var div = '<div class="col-sm-3  img-div_'+rowCount+'"><div class="property-image-inner-main"><div class="property-image-inner-image"><a onclick="click_on_image_buttons(this)"><img src="<?php echo base_url(); ?>assets/images/no_image.png" class="property-imagesss"></a><input type="file" name="file[]" class="image_upload_inputs" onchange="image_readURL(this)" style="display:none;"><input type="hidden" name="id[]" value="new" ></div><div class="property-image-delete"><a data-id = "'+rowCount+'" onclick="delete_images(this)"><i class="fa fa-trash" aria-hidden="true"></i></a></div></div></div></div>';
	$('div#property-imagesss-'+folder_id).append(div);
}

function load_borower_content_data()
{
	var borrower_id = $('select#loan-borrower-id').val();
	load_url = '<?php echo base_url() . 'borrower_data/'; ?>'+borrower_id;

	$.ajax({
		type : 'POST',
		url : '<?php echo base_url() . 'borrower_data_loading/'; ?>',
		data : {'borrower_id':borrower_id },
		success : function(response)
		{
			$('div#Borrower_modal div#load-borrower-content').empty();
			$('div#Borrower_modal div#load-borrower-content').append(response);
		}
	});
}
function add_draw_image_folder(that)
{

	var folder_div = '<div class="col-sm-3"><div class="folder-main-images"><img src="<?php echo base_url() . 'assets/images/folder.png'; ?>"></div><div class="folder-title"><input type="hidden" value="new" name="id[]"><input type="text" name="title[]" value="New Folder"></div></div>';
	$('div#add-image-folderss').append(folder_div);
}
/*--------------------- start loan reserve 2 checkboxes -------------------------//*/

function draw_approved_1(that){

	if($(that).is(':checked')){
		$('input#approved_1').val('1');

		var amount = 0;
	    $('input[name="enter_amount_1[]"]').each(function(){
	       amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });

		var talimar_loan = '<?php echo $talimar_loan; ?>';
		$.ajax({

			type : 'POST',
			url : '<?php echo base_url() . 'Load_data/approval_email'; ?>',
			data : {'amount':amount,'talimar_loan':talimar_loan},
			success : function(response)
			{
			}
		});
	}else{
		$('input#approved_1').val('0');
	}
}

function draw_approved_2(that){

	if($(that).is(':checked')){
		$('input#approved_2').val('1');

		var amount = 0;
	    $('input[name="enter_amount_2[]"]').each(function(){
	       amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });

		var talimar_loan = '<?php echo $talimar_loan; ?>';
		$.ajax({
			type : 'POST',
			url : '<?php echo base_url() . 'Load_data/approval_email'; ?>',
			data : {'amount':amount,'talimar_loan':talimar_loan},
			success : function(response)
			{
			}
		});
	}else{
		$('input#approved_2').val('0');
	}
}

function draw_approved_3(that){

	if($(that).is(':checked')){
		$('input#approved_3').val('1');

		var amount = 0;
	    $('input[name="enter_amount_3[]"]').each(function(){
	       amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });

		var talimar_loan = '<?php echo $talimar_loan; ?>';
		$.ajax({
			type : 'POST',
			url : '<?php echo base_url() . 'Load_data/approval_email'; ?>',
			data : {'amount':amount,'talimar_loan':talimar_loan},
			success : function(response)
			{
			}
		});

	}else{
		$('input#approved_3').val('0');
	}
}

function draw_approved_4(that){
	if($(that).is(':checked')){
		$('input#approved_4').val('1');

		var amount = 0;
	    $('input[name="enter_amount_4[]"]').each(function(){
	       amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });
		var talimar_loan = '<?php echo $talimar_loan; ?>';
		$.ajax({
			type : 'POST',
			url : '<?php echo base_url() . 'Load_data/approval_email'; ?>',
			data : {'amount':amount,'talimar_loan':talimar_loan},
			success : function(response)
			{
			}

		});
	}else{

		$('input#approved_4').val('0');
	}
}

function draw_approved_5(that){

	if($(that).is(':checked')){
		$('input#approved_5').val('1');

		var amount = 0;
	    $('input[name="enter_amount_5[]"]').each(function(){
	       amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });

		var talimar_loan = '<?php echo $talimar_loan; ?>';
		$.ajax({
			type : 'POST',
			url : '<?php echo base_url() . 'Load_data/approval_email'; ?>',
			data : {'amount':amount,'talimar_loan':talimar_loan},
			success : function(response)
			{
			}

		});
	}else{
		$('input#approved_5').val('0');
	}
}

function draw_approved_6(that){

	if($(that).is(':checked')){
		$('input#approved_6').val('1');
		var amount = 0;
	    $('input[name="enter_amount_6[]"]').each(function(){
	       amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });

		var talimar_loan = '<?php echo $talimar_loan; ?>';
		$.ajax({

			type : 'POST',
			url : '<?php echo base_url() . 'Load_data/approval_email'; ?>',
			data : {'amount':amount,'talimar_loan':talimar_loan},
			success : function(response)
			{
			}
		});
	}else{
		$('input#approved_6').val('0');
	}
}
function draw_approved_7(that){

	if($(that).is(':checked')){
		$('input#approved_7').val('1');

		var amount = 0;
	    $('input[name="enter_amount_7[]"]').each(function(){
	       amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });

		var talimar_loan = '<?php echo $talimar_loan; ?>';
		$.ajax({
			type : 'POST',
			url : '<?php echo base_url() . 'Load_data/approval_email'; ?>',
			data : {'amount':amount,'talimar_loan':talimar_loan},
			success : function(response)
			{
			}
		});
	}else{
		$('input#approved_7').val('0');
	}
}

function draw_approved_8(that){
	if($(that).is(':checked')){
		$('input#approved_8').val('1');
		var amount = 0;
	    $('input[name="enter_amount_8[]"]').each(function(){
	       amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });

		var talimar_loan = '<?php echo $talimar_loan; ?>';
		$.ajax({
			type : 'POST',
			url : '<?php echo base_url() . 'Load_data/approval_email'; ?>',
			data : {'amount':amount,'talimar_loan':talimar_loan},
			success : function(response)
			{
			}
		});
	}else{
		$('input#approved_8').val('0');
	}
}

function draw_approved_9(that){

	if($(that).is(':checked')){
		$('input#approved_9').val('1');

		var amount = 0;
	    $('input[name="enter_amount_9[]"]').each(function(){
	       amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });
		var talimar_loan = '<?php echo $talimar_loan; ?>';
		$.ajax({
			type : 'POST',
			url : '<?php echo base_url() . 'Load_data/approval_email'; ?>',
			data : {'amount':amount,'talimar_loan':talimar_loan},
			success : function(response)
			{
			}
		});
	}else{

		$('input#approved_9').val('0');
	}
}
function draw_approved_10(that){

	if($(that).is(':checked')){
		$('input#approved_10').val('1');

		var amount = 0;
	    $('input[name="enter_amount_10[]"]').each(function(){
	       amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });

		var talimar_loan = '<?php echo $talimar_loan; ?>';
		$.ajax({
			type : 'POST',
			url : '<?php echo base_url() . 'Load_data/approval_email'; ?>',
			data : {'amount':amount,'talimar_loan':talimar_loan},
			success : function(response)
			{
			}
		});
	}else{
		$('input#approved_10').val('0');
	}
}
function draw_approved_11(that){
	if($(that).is(':checked')){
		$('input#approved_11').val('1');
		var amount = 0;
	    $('input[name="enter_amount_11[]"]').each(function(){
	       amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });
		var talimar_loan = '<?php echo $talimar_loan; ?>';
		$.ajax({
			type : 'POST',
			url : '<?php echo base_url() . 'Load_data/approval_email'; ?>',
			data : {'amount':amount,'talimar_loan':talimar_loan},
			success : function(response)
			{
			}
		});
	}else{
		$('input#approved_11').val('0');
	}
}
function draw_approved_12(that){

	if($(that).is(':checked')){
		$('input#approved_12').val('1');

		var amount = 0;
	    $('input[name="enter_amount_12[]"]').each(function(){
	       amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });

		var talimar_loan = '<?php echo $talimar_loan; ?>';
		$.ajax({
			type : 'POST',
			url : '<?php echo base_url() . 'Load_data/approval_email'; ?>',
			data : {'amount':amount,'talimar_loan':talimar_loan},
			success : function(response)
			{
			}
		});
	}else{
		$('input#approved_12').val('0');
	}
}
function draw_approved_13(that){

	if($(that).is(':checked')){
		$('input#approved_13').val('1');

		var amount = 0;
	    $('input[name="enter_amount_13[]"]').each(function(){
	       amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });

		var talimar_loan = '<?php echo $talimar_loan; ?>';
		$.ajax({
			type : 'POST',
			url : '<?php echo base_url() . 'Load_data/approval_email'; ?>',
			data : {'amount':amount,'talimar_loan':talimar_loan},
			success : function(response)
			{
			}
		});

	}else{

		$('input#approved_13').val('0');
	}
}

function draw_approved_14(that){

	if($(that).is(':checked')){
		$('input#approved_14').val('1');

		var amount = 0;
	    $('input[name="enter_amount_14[]"]').each(function(){
	       amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });

		var talimar_loan = '<?php echo $talimar_loan; ?>';
		$.ajax({

			type : 'POST',
			url : '<?php echo base_url() . 'Load_data/approval_email'; ?>',
			data : {'amount':amount,'talimar_loan':talimar_loan},
			success : function(response)
			{
			}

		});
	}else{

		$('input#approved_14').val('0');
	}
}

function draw_approved_15(that){
	if($(that).is(':checked')){
		$('input#approved_15').val('1');
		var amount = 0;
	    $('input[name="enter_amount_15[]"]').each(function(){
	       amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });

		var talimar_loan = '<?php echo $talimar_loan; ?>';

		$.ajax({

			type : 'POST',
			url : '<?php echo base_url() . 'Load_data/approval_email'; ?>',
			data : {'amount':amount,'talimar_loan':talimar_loan},
			success : function(response)
			{
			}

		});
	}else{

		$('input#approved_15').val('0');
	}
}
//-------------------- loan reserve checkbos end ---------------------------//


function delete_loan_document(that)
{
	var id		= that.id;
	var loan_id	= '<?php echo $loan_id; ?>';
	if(id)
	{
		var r = confirm("Are you sure to delete?");
		if(r == true) {
			window.location.href = '<?php echo base_url(); ?>delete_loan_document/'+id +'/'+loan_id;
		}
	}
}

function delete_property_image(id)
{
	var loan_id	= '<?php echo $loan_id; ?>';
	if(id)
	{
		var r = confirm("Are you sure to delete?");
		if(r == true) {
			window.location.href = '<?php echo base_url(); ?>delete_property_image/'+id +'/'+loan_id;
		}
	}
}


$(document).ready(function(){	
	var popup_modal_hit = '<?php echo $this->session->flashdata('popup_modal_hit'); ?>';
	//alert(popup_modal_hit);
	if(popup_modal_hit == 'testmodal'){
		$('#test').modal('show');
	}

	$('select#draw_escrow_account').change(function(){
		var fund_control_servicer = $('select#draw_escrow_account').val();
		if(fund_control_servicer == '3')
		{
			$('input[name="draw_escrow_other"]').val('<?php echo isset($fetch_loan_result) ? $fetch_loan_result[0]->fci : ''; ?>');
			$('input[name="draw_escrow_other"]').prop('readonly',true);
		}
		else
		{
			// $('input[name="draw_escrow_other"]').val('');
			$('input[name="draw_escrow_other"]').val('<?php echo isset($fetch_extra_details[0]->draw_escrow_other) ? $fetch_extra_details[0]->draw_escrow_other : ''; ?>');
			$('input[name="draw_escrow_other"]').prop('readonly',false);
		}
	});
	
});
</script>

