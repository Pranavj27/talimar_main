<style>
   .content div.page-container {
   padding: 0px 30px!important;
   }
   div.page-title h3,h4 {
   padding: 7px 11px;
   font-weight: 600;
   }
   div.page-container .heading{
   border-bottom: 1px solid #d0d0d0;
   }
   .page-head .page-title {
   display: inline-block;
   float: left;
   padding: 19px 22px;
   }

   .datatables .col-md-3 h4 {
    padding-left: 0;
}

.datatables .col-md-3 {
    padding-left: 0;
}


.datatables .col-md-3:last-child {
    padding-right: 0;
}
.head_fund {
    float: right;
    padding: 40px 0px;
}
</style>
<div class="page-container contactlist-outmost">
   <!-- BEGIN PAGE HEAD -->
   <div class="container">
      <div class="tab-pane">
      	<!--------------------MESSEGE SHOW-------------------------->
			<?php if($this->session->flashdata('error')!=''){  ?>
				<div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error');?></div><?php } ?>
			<?php if($this->session->flashdata('success')!=''){  ?>
				<div id='success'><i class='fa fa-thumbs-o-up'></i> <?php echo $this->session->flashdata('success');?></div><?php } ?>
			<div id = "chkbox_err"></div>		 
			<!-------------------END OF MESSEGE-------------------------->
         <form method="POST" action = "<?php echo base_url();?>Fund_data/add_update_data" id="formid">
       
         <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
               <h3>Credit Line</h3>
            </div>
          <div class="head_fund">

            <input id ="formsubmit" value = "Save" type = "submit"  class = "btn btn-primary"/>	
								
			
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit_contact">Close</button> 	
            	
         </div>
         		
         </div>

				<div class="modal fade" id="edit_contact" tabindex="-1" role="basic" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							
							<div class="modal-body">
								<div class="row" style="padding-top:8px;">
							
									<div class="col-md-8" style="padding-top:6px;"><p>Do you want to Save Information?</p></div>
									<div class="col-md-2">
										<!--<a style="margin-left:38px;" class="btn blue borrower_save_button" href="javascript:void(0);" onclick = "save_info('edit_contact')">Yes</a>-->
										
										<input style="margin-left:40px;" id = "formsubmit" value = "Yes" type = "submit"  class = "btn btn-primary"/>
										
									</div>
									<div class="col-md-2">
										
										<input type="button" id = "" onclick="go_back(this)" value = "No" class = "btn nk-default"/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>



         <div class="page-container">


          <div class="datatables">	
            <div class="row heading">
               <div class="col-md-3">
                  <h4>Account Name</h4>
               </div>
               <div class="col-md-2">
                  <h4>Credit Limit</h4>
               </div>
               <div class="col-md-2">
                  <h4 style="white-space:nowrap;">Outstanding Credit</h4>
               </div>
               <div class="col-md-2">
                  <h4>Available Credit</h4>
               </div>
                <div class="col-md-2">
                  <h4>Action</h4>
               </div>
            </div>
            <div id="abc">
           <?php 

          
            if(isset($fund_data_result) && is_array($fund_data_result)){

               foreach($fund_data_result as $row){

              $total_rem  =$row->available_credit-$row->drawn;
              

               	?>
           <input type="hidden" name="id[]" value="<?php echo $row->id ? $row->id:'new'?>">

            <div class="row textbox">
               <div class="col-md-3">
                  <br>
                  <input  type="text" id="account_name" class="form-control" name="account_name[]" value ="<?php echo $row->account_name ? $row->account_name:''?>" >
               </div>
               <div class="col-md-2">
                  <br>
                  <input  type="text" id="available_credit" class="form-control number_only" onchange="amount_format_change(this);amount_av_total(this);" name="available_credit[]" value ="<?php echo $row->available_credit ? '$'.number_format($row->available_credit,2):''?>">
               </div>
               <div class="col-md-2">
                  <br>
                  <input  type="text" id="drawn" class="form-control number_only" onchange="amount_format_change(this);amount_drawn_total(this)" name="drawn[]" value ="<?php echo $row->drawn ? '$'.number_format($row->drawn,2):''?>">
               </div>
               <div class="col-md-2">
                  <br>
                  
                  <input  type="text" id="remaining" class="form-control number_only "  onchange="amount_format_change(this)" name="remaining[]" value ="<?php echo $total_rem ? '$'.number_format($total_rem,2):'';?>" readonly>
               </div>
                <div class="col-md-2">
                  <br>
               <a title="Delete" onclick="delete_fund_data(this);" id="<?php echo $row->id ? $row->id:''; ?>" style="margin-left:31px;position:relative;top:6px;"><i class="fa fa-trash"></i></a>
               </div>
            </div>

        <?php }

         }else{?>

     	  <input type="hidden" name="id[]" value="new">

            <div class="row textbox">
               <div class="col-md-3">
                  <br>
                  <input  type="text" id="account_name" class="form-control" name="account_name[]" value ="">
               </div>
               <div class="col-md-2">
                  <br>
                  <input  type="text" id="available_credit" class="form-control number_only" onchange="amount_format_change(this);amount_av_total(this);" name="available_credit[]" value ="">
               </div>
               <div class="col-md-2">
                  <br>
                  <input  type="text" id="drawn" class="form-control number_only" onchange="amount_format_change(this);amount_drawn_total(this)" name="drawn[]" value ="">
               </div>
               <div class="col-md-2">
                  <br>
                  <input  type="text" id="remaining" class="form-control"  name="remaining[]" onchange="amount_format_change(this)" value ="" readonly>
               </div>
                <div class="col-md-2">
                  <br>
               <a title="Delete" onclick="delete_fund_data(this);" id="new" style="margin-left:31px;position:relative;top:6px;"><i class="fa fa-trash"></i></a>
               </div>
            </div>

         <?php } ?>
            </div>
            
           
            <div class="row heading">&nbsp;</div>
            <div class="row sum">
               <div class="col-md-3"></div>
               <div class="col-md-2">
                  <br>
                 <strong> <span class="avaliable_credit_sum"></span> </strong>
               </div>
               <div class="col-md-2">
                  <br>
                 <strong> <span class="drawn"></span> </strong>
               </div>
               <div class="col-md-2">
                  <br>
                 <strong> <span class="remaining"></span> </strong>
               </div>
                <div class="col-md-2">
                  
                  
               </div>
            </div>
            <div class="row">&nbsp;</div>
            <div class="row">
               <div class="col-md-3">
                  <button type="button" onclick="fund_data(this)" class="btn blue">Add</button>
               </div>
            </div>
        
         </div>
         </div>

</form>
      </div>
   </div>
</div>
</div>
</div>

<script type="text/javascript">
	
function fund_data(t){

$('div#abc').append('<div class="row textbox"> <input type="hidden" name="id[]" value="new"><div class="col-md-3"><br><input type="text" id="account_name" class="form-control" name="account_name[]" value ="" ></div><div class="col-md-2"><br><input  type="text" onchange="amount_format_change(this);amount_av_total(this)" id="available_credit" class="form-control number_only" name="available_credit[]" value =""></div><div class="col-md-2"><br><input  type="text" onchange="amount_format_change(this);amount_drawn_total(this)" id="drawn" class="form-control number_only" name="drawn[]" value =""></div><div class="col-md-2"><br><input  type="text" id="remaining" onchange="amount_format_change(this);" class="form-control number_only" name="remaining[]" value ="" readonly></div><div class="col-md-2"><br><a title="Delete" onclick="delete_fund_data(this)" id="new" style="margin-left:31px;position:relative;top:6px;" ><i class="fa fa-trash"></i></a></div></div>');

}
function replace_dollar(n)
{
	var a = n.replace('$', '');
	var b = a.replace(',', '');
	var b = b.replace(',', '');
	var b = b.replace(',', '');
	var b = b.replace(',', '');
	var c = b.replace('%', '');
	return c;
}
function amount_format_change(that)
{
	
	var a = that.value;
	
	var a = replace_dollar(a);
	if(a == '')
	{
		a = 0;
	}
	a = parseFloat(a);
	
	that.value = '$'+number_format(a);
	}

function number_format(n) {
    return n.toFixed(2).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}


function delete_fund_data(that){

var id=$(that).attr('id');

if(id=='new'){

$(that).parent().parent().remove();

}else{
	if(confirm('Are You Sure To Delete?')){
		$.ajax({
			
			type	:'POST',
			url		:'<?php echo base_url()."Fund_data/delete_fund_data";?>',
			data	:{'id':id},
			success : function(response){
				
				$(that).parent().parent().remove();

				}
			
				
		});
	}

}

}

   $(document).ready(function() {
    grand_total();
    amount_av_total(this);

   
    amount_drawn_total('.drawn');

   	});

function amount_av_total(that){

	var sum = 0;
$("input[name='available_credit[]']").each(function(){
        sum += + replace_dollar($(this).val());
    });

  $('span.avaliable_credit_sum').html('$'+number_format(sum));
var ac=replace_dollar($(that).val());
var dr=replace_dollar($(that).parent().parent().find('input[name="drawn[]"]').val());

 var remaining_val=ac-dr;

$(that).parent().parent().find('input[name="remaining[]"]').val('$'+number_format(remaining_val));

grand_total();


}

 amount_drawn_total(this);

function amount_drawn_total(that){


	var sums = 0;
 
	$("input[name='drawn[]']").each(function(){
        sums += + replace_dollar($(this).val());
   
    });

$('span.drawn').html('$'+number_format(sums));

var acc=replace_dollar($(that).parent().parent().find('input[name="available_credit[]"]').val());
var drr=replace_dollar($(that).val());
 var remaining_vall=acc-drr;

$(that).parent().parent().find('input[name="remaining[]"]').val('$'+number_format(remaining_vall));
  grand_total();
 }

function grand_total(){

 var new_sum = 0;
$("input[name='remaining[]']").each(function(){
        new_sum += + replace_dollar($(this).val());
    });

 $('span.remaining').html('$'+number_format(new_sum));

}

	function go_back(){

		window.location.href='<?php echo base_url()."fund_data";?>';
	}

</script>