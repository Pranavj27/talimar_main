<?php
		$fractional                             = $this->config->item('fractional');
		$responsibility							= $this->config->item('responsibility');

		
if(isset($wait_list_result))
{
	$count = 0;
	$total_amount = 0;
	foreach($wait_list_result as $roww)
	{
		$count++;
		$total_amount +=$roww->amount;
		?>
			<tr>
				<td><?php echo $count; ?><input type="hidden" name="id[]" value="<?php echo $roww->id; ?>"></td>
			
				<td>
					<select name ="contact_name[]"  class="chosen_4">
							<option value=''>Search Contact</option>	
						<?php 
						foreach($fetch_search_option as $key => $row ){
						?>
						<option value = "<?php echo $row['id'];?>" <?php  if($row['id'] == $roww->lender_name){ echo 'selected'; } ?> ><?php  echo $row['text'];?></option>
						<?php
						}
						?>
					</select>
				
				</td>
				<td>
					<input type="text" name="date_request[]" class="form-control" value="<?php echo date('m-d-Y',strtotime($roww->date_request)); ?>">
				</td>

					<td>
					<select  class="form-control input-md" name ="confirmed[]">
						<?php
						foreach($responsibility as $key => $row)
						{
							?>
							<option value="<?php echo $key;?>" <?php if($key == $roww->confirmed){ echo 'selected'; }  ?>><?php echo $row; ?></option>
							<?php
						}
						?>
					</select>
				</td>
				
				<td>
					<select  class="form-control input-md" name ="fractional[]">
						<?php
						foreach($fractional as $key => $row)
						{
							?>
							<option value="<?php echo $key;?>" <?php  if($key == $roww->fractional){ echo 'selected'; }  ?>><?php echo $row; ?></option>
							<?php
						}
						?>
					</select>
				</td>
				<td>
					<input type="text" name="amount[]" class="form-control" value="<?php echo '$'.number_format($roww->amount,2); ?>" onchange="amount_format_change(this)" onkeyup="number_only_format(this)"></td>
				
			
				
				<td>
					<a id="<?php echo $roww->id?>" onclick="remove_table_wait_lists(this)"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
				</td>
			</tr>
		<?php
	}
}

?>

 <script>

 $('.chosen_4').chosen();
</script>
<style>
.chosen-drop {
    width: 253px !important;
    margin-left: 0px !important;
}	
.row.borrower_data_row div {
    width: 96% !important;
}
.chosen-container{
	display: block !important;
}
.chosen-container.chosen-container-single {
    width: 253px !important;
}
.chosen-container-single .chosen-single {
    padding: 4px 0px 26px 6px !important;
    border-radius: 4px !important;
    border: 1px solid #e1e1e1 !important;
}
</style>
