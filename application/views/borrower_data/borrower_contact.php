<div class="page-container">
			<!-- BEGIN PAGE HEAD -->
		
			<div class="container">
			<!--------------------MESSEGE SHOW-------------------------->
			<?php if($this->session->flashdata('error')!=''){  ?>
				 <div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error');?></div><?php } ?>
				  <?php if($this->session->flashdata('success')!=''){  ?>
				 <div id='success'><i class='fa fa-thumbs-o-up'></i><?php echo $this->session->flashdata('success');?></div><?php } ?>
				 
			<!-------------------END OF MESSEGE-------------------------->
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>Borrower Contact <small></small></h1>
				</div>
				<!-- END PAGE TITLE -->
				<!-- BEGIN PAGE TOOLBAR -->
				
				<!-- END PAGE TOOLBAR -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					 Dashboard
				</li>
			</ul>
				<form method="POST" action = "<?php echo base_url();?>add_borrower_contact">
					<div class="row borrower_data_row">
						<div class="col-md-3">
							<label>First Name :</label>
							<input type="text" class="form-control" name="first_name" required />
						</div>
						<div class="col-md-3">
						<label>Middle initial :</label>
							<input type="text" class="form-control" name = "middle_inital" required />
						</div>
						<div class="col-md-3">
						<label>Last Name :</label>
							<input type="text" class="form-control" name = "last_name" required />
						</div>
						
					</div>
					
					<div class="row borrower_data_row">
						<div class="col-md-3">
							<label> Address 1 :</label>
							<input type="text" class="form-control" name = "address">
						</div>
						<div class="col-md-3">
						<label>Unit # :</label>
							<input type="text" class="form-control number_only" name = "unit">
						</div>
						
						
						
					</div>
					
					<div class="row borrower_data_row">
						<div class="col-md-3">
						<label>City :</label>
							<input type="text" class="form-control" name="city">
						</div>
						<div class="col-md-3">
						<label>State :</label>
							<select  class="form-control" name="state">
							<option value="Califonia">Califonia</option>
							<option value="A">A</option>
							<option value="B">B</option>
							<option value="C">C</option>
							</select>
						</div>
						<div class="col-md-3">
						<label>Zip :</label>
							<input type="text" class="form-control number_only" name="zip">
						</div>
						
					</div>
					
					<div class="row borrower_data_row">
						
								
						<div class="col-md-3">
							<label>DOB :</label>
								<input type="text" class="form-control" name="dob" id="dob">
							</div>
							<div class="col-md-3">
							<label>Title :</label>
								<input type="text" class="form-control" name="title">
						</div>
						
					</div>
					
					
					<div class="row borrower_data_row">
						<div class="col-md-3">
						<label>Phone :</label>
							<input type="text" class="form-control number_only" name="phone_1">
						</div>
						<div class="col-md-3">
						<label>Phone 2 :</label>
							<input type="text" class="form-control number_only" name="phone_2">
						</div>
						<div class="col-md-3">
						<label>Email :</label>
							<input type="email" class="form-control" name="email">
						</div>
					</div>
					
					<div class="row borrower_data_row">
						<div class="col-md-3">
						<label>Tax ID:</label>
							<input type="text" class="form-control" name="tax_id">
						</div>
						<div class="col-md-3">
						<label>Credit Report Date:</label>
							<input id="credit_report_date" type="text" class="form-control" name="credit_report_date">
						</div>
						<div class="col-md-3">
						<label>Credit Score:</label>
							<input type="text" class="form-control number_only" name="credit_score">
						</div>
					</div>
					
					
					
					<div class="row borrower_data_row">
						<div class="col-md-3">
						<label>Licence # :</label>
							<input type="text" class="form-control" name="licence">
						</div>
						<div class="col-md-3">
						<label>Licence State :</label>
							<select  class="form-control" name="licence_state">
							<option value="Califonia">Califonia</option>
							<option value="A">A</option>
							<option value="B">B</option>
							<option value="C">C</option>
							</select>
						</div>
						
					</div>
					
					<div class="row borrower_data_row">
						<div class="col-md-3">
						<p>Has the Borrower declare BK in last 12 Months </p>
							
						</div>
						<div class="col-md-3">
							<div class="radio_button_section">
							<p>Yes
							<input type="radio" class="form-control radio_button" name="bk_12" value = '1'>
							No
							<input type="radio" class="form-control radio_button" name="bk_12"value = '0' checked ></p>
							</div>
						</div>
						
					</div>
					
					<div class="row borrower_data_row">
						<div class="col-md-3">
						<p>If Yes has the BK been Discharge : </p>
							
						</div>
						<div class="col-md-3">
							<div class="radio_button_section">
							<p>Yes
							<input type="radio" class="form-control radio_button" name="bk_discharge" value="1">
							No
							<input type="radio" class="form-control radio_button" name="bk_discharge" value="0" checked ></p>
							</div>
						</div>
						
					</div>
					
					<div class="row borrower_data_row">
						<div class="col-md-3">
						<p>Has the Borrower been covicted of a felony : </p>
							
						</div>
						<div class="col-md-3">
							<div class="radio_button_section">
							<p>Yes
							<input type="radio" class="form-control radio_button" name="felony" value="1">
							No
							<input type="radio" class="form-control radio_button" name="felony"value="0" checked ></p>
							</div>
						</div>
						
					</div>
					
					<div class="row borrower_data_row">
						<div class="col-md-3">
						<p>Sign Document : </p>
							
						</div>
						<div class="col-md-3">
							<div class="radio_button_section">
							<p>Yes
							<input type="radio" class="form-control radio_button" name="sign_d" value="1">
							No
							<input type="radio" class="form-control radio_button" name="sign_d"value="0" checked ></p>
							</div>
						</div>
						
					</div>
					
					<div class="row borrower_data_row">
						<div class="col-md-3">
						<p>Guaranter : </p>
							
						</div>
						<div class="col-md-3">
							<div class="radio_button_section">
							<p>Yes
							<input type="radio" class="form-control radio_button" name="guranter" value="1">
							No
							<input type="radio" class="form-control radio_button" name="guranter" value="0" checked ></p>
							</div>
						</div>
						
					</div>
					
					
						
						<div class="row borrower_data_row">
							<div class="form-group">
								  
								  <div class="col-md-9">
									<button id="" name="button" class="btn btn-primary borrower_save_button" value="save">Save</button>
									<button id="" name="button" class="btn btn-primary borrower_save_button" value="add">Add</button>
								  </div>
							</div>

								<!-- Button -->
							
						</div>
				</form>	
				
						
						
			</div>
	</div>		
</div>	
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$( function() {
    $( "#credit_report_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
    $( "#dob" ).datepicker({ dateFormat: 'dd-mm-yy' });
    // $( "#loan_funding_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
    // $( "#loan_document_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
  } );
</script>