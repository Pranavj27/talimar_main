<div class="page-container">
			<!-- BEGIN PAGE HEAD -->
		
			<div class="container">
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>Loan view <small></small></h1>
				</div>
				<!-- END PAGE TITLE -->
				<!-- BEGIN PAGE TOOLBAR -->
				
				<!-- END PAGE TOOLBAR -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					 Dashboard
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<form class="form-horizontal">
			<div class="row margin-top-10">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="width:100%;">
					<div class="dashboard-stat2 table-scrollable">
						<!------------form 1---------------->
						<table class="table table-striped table-bordered table-hover dataTable no-footer">
						<thead>
						<tr>
						<td>Borrower Name</td>
						<td>Address</td>
						<td>Email</td>
						<td>Phone</td>
						<td>Tax id </td>
						<td>Credit Score</td>
						<td>Licence</td>
						</tr>
						</thead>
						<tbody>
						
						
						<?php 
						foreach($fetch_result as $row)
						{
							?>
							<tr>
							<td><?php  echo $row->first_name." ".$row->last_name;?></td>
							<td><?php  echo $row->address." ".$row->city;?></td>
							<td><?php  echo $row->email;?></td>
							<td><?php  echo $row->phone_1;?></td>
							<td><?php  echo $row->tax_id;?></td>
							<td><?php  echo $row->credit_score;?></td>
							<td><?php  echo $row->licence;?></td>
							
							</tr>
							<?php
						}
						?>
						
						
						</tbody>
						</table>
						
							

							

						<!-----------Close form 1------------------------>
					</div>
				</div>
				
				
				
			</div> 
			
			
			
			
			<!-- END PAGE CONTENT INNER -->
		</div>
		
			<div class="row margin-top-10">
				<!----------form 3---------------------->
			</div>
	</div>
	<!-- END CONTENT -->