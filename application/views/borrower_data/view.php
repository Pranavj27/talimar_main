<?php
		$STATE_USA 					= $this->config->item('STATE_USA'); 
		$borrower_type_option 		= $this->config->item('borrower_type_option'); 
		$company_type_option 		= $this->config->item('company_type_option');
		$yes_no_option 				= $this->config->item('yes_no_option');
		$no_yes_option 				= $this->config->item('no_yes_option');
		$responsibility 			= $this->config->item('responsibility');
		$loan_status_option 		= $this->config->item('loan_status_option');
		$pro_app_req_status 		= $this->config->item('pro_app_req_status');
		
		$paid_count 				= isset($fetch_paidoff_loan) ? $fetch_paidoff_loan['count_loan'] : 0;
		$paid_amount 				= isset($fetch_paidoff_loan) ? $fetch_paidoff_loan['total_loan_amount'] : 0;
		$primary_contact_option		= $this->config->item('primary_contact_option');
		
		$active_count 		= isset($fetch_active_loan) ? $fetch_active_loan['count_loan'] : 0;
		$active_amount 		= isset($fetch_active_loan) ? $fetch_active_loan['total_loan_amount'] : 0;
		$pipeline_count 	= isset($fetch_pipeline_loan) ? $fetch_pipeline_loan['count_loan'] : 0;
		$pipeline_amount 	= isset($fetch_pipeline_loan) ? $fetch_pipeline_loan['total_loan_amount'] : 0;
		
		// $borrower_id				= $borrower_idd ? $borrower_idd : $this->uri->segment(2);
		// $borrower_id				= $borrower_idd  ? $borrower_idd  : $borrower_url_idd;
		 $borrower_id				= $borrower_idd ;
		
		
		/* if($borrower_id)
		{
			$borrower_id = $borrower_id;
		}
		else
		{
			$borrower_id = '';
		}	 
		 */
	// echo 'btn_name is: ' . $btn_name;
	if($btn_name == 'view'){
		$fields_disabled 	= 'readonly ';
		$class_disabled 	= 'disabled ';
	
	}else{
		$fields_disabled 	= ' ';
		$class_disabled 	= ' ';
				
	}
	
	
	// echo 'field disabled is: ' . $fields_disabled;
?>
<style>

#ct_Documentsadd{
	float: left;
	width: 100%;
}
label.col-md-4.control-label.l {
	padding-left:47px; 
}


label.col-md-6.new_lable.l {
		padding-left:47px; 

}
.addmarginright{
	margin-top: 15px;
}

.main_left_div{
	float:left;
	width:70%;
	border-right:1px solid #eee;
}
.main_right_div{
	float:right;
	width:30%;
}
.row.button_section_div {
    padding: 9.5px 30px !important;
}
#c2_information{
	display:none;
}
.active-class , no-class{
	display:block !important;
}
.inactive-class{
	display:none ;
}
.borrower_document_lists li{
	list-style: none;
	padding: 5px;
	font-size: 14px;
}
div#borrower_history_data {
    margin-top: 40px !important;
}
.page-head{
	border-bottom:1px solid #eee;
	border-bottom:1px solid #eee;
}
.primary_contactdiv{
	float: left;
	width: initial !important;
	margin-top: 12px;
	font-size: 14px;
}
#uniform-primary_contact1{
	width: 20px !important;
}
.col-md-3.primary_contactdiv div {
    width: initial !important;
}
.col-md-3.primary_contactdiv h4 {
    margin: 0;
}
.borrower_data_row{
	padding: 11px 0px !important;
}
div#bordered_div_section{
	margin:0 !important;
}
.bs-searchbox {
    width: 100% !important;
}
table td, th{
	border : none !important;
}
.view_inputs{
	font-size : 14px;
}
.borrower_data_row>h4 {
    background-color: #e8e8e8;
	height: 30px;
	line-height: 30px;
}

@media only screen and (min-width: 992px) and (max-width: 1199px) {
	
	
}

@media only screen and (min-width: 768px) and (max-width: 992px) {


}



@media (max-width: 767px) {
.main_left_div {
    float: left;
    width: 70% !important;
    border-right: 0px solid #eee !important;
}
.main_right_div {
    float: left;
    width:30% !important;
}
.row.button_section_div {
    padding: 10px 0px !important;
    width: 100%;
	margin-left: 0px !important;
}
.row.borrower_data_row{
	float:left;
	width:100%;
}
.row.borrower_data_row.col-md-3{
	float:left;
	margin-left: -15px !important;
	margin-right: -15px !important;
}
.row.borrower_data_row.table-borrower table {
    margin-left: 0px !important;
}
}

@media (max-width: 469px) {
.page-header .page-header-top .top-menu {
    display: block;
    clear: none !important;
    margin-top: 0px !important;
}
.row.borrower_data_row.table-borrower table {
    margin-left: 0px !important;
}
}
label {
    font-weight: 600 !important;
}


div#model_left_align {
    margin-left: 7px !important;
}

.main_right_div {
    float: right;
    width: 30%;
    margin-top: 96px;
}
.borrower_document_lists li{
	list-style: none;
	padding: 5px;
	font-size: 14px;
}

.doc-div{
	margin-bottom: 15px;
}
.table-scrollable{

	border:0px !important;
}

div#contact_information_div {
     border-bottom: 0px !important;
}

a.btn.btn-danger.borrower_save_button {
    float: right !important;
    border-radius: 0px;
    margin-top: 3px;
}

#PropertyApproval .table>tbody>tr>td, .table>tbody>tr>th{

	padding: 5px !important;
}

</style>

<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="container">
		<!--------------------MESSEGE SHOW-------------------------->
		<?php if($this->session->flashdata('error')!=''){  ?>
			<div id='error'><?php echo $this->session->flashdata('error');?></div><?php } ?>
		<?php if($this->session->flashdata('success')!=''){  ?>
			<div id='success'><?php echo $this->session->flashdata('success');?></div><?php } ?>
				 
		<!-------------------END OF MESSEGE-------------------------->
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Borrower Accounts<small></small></h1>
			</div>
			<?php
				
			if($btn_name == 'view'){
				$btn_class = 'style ="display:block;" ';
			}else{
				$btn_class = 'style ="display:none;" ';
						
			}
			?>
			<!--<div class="talimar_no_dropdowns" <?php echo $btn_class;?>>
				<?php
				
					$page_url = $_SERVER['REQUEST_URI'];
				?>
				<form id = "borrower_form"  method = "post" action = "<?php echo $page_url;?>">
					
					
					Borrower Name : 
					<select  name = "borrower_ids" onchange="select_borrower_id(this.value)" class="selectpicker" data-live-search="true" >
						<option value=''>Create new</option>
						<?php
						foreach($all_borrower_data as $row)
						{
						?>
						<option value='<?= $row->id; ?>' <?php if(isset($borrower_id)) { if($borrower_id == $row->id){ echo 'selected'; } } ?> ><?= $row->b_name;?></option>
						<?php
						}
						?>
					</select>
					
					<input type = "hidden" id = "btn_name" name = "btn_name" value = "<?php echo $btn_name ;?>" />	
				</form>
			</div>-->
			<!-- END PAGE TITLE -->
			<!-- BEGIN PAGE TOOLBAR -->
				
			<!-- END PAGE TOOLBAR -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		
		<ul class="page-breadcrumb breadcrumb hide">
			<li>
				<a href="#">Home</a><i class="fa fa-circle"></i>
			</li>
			<li class="active">
				Dashboard
			</li>
		</ul>
		
	
	<div class="main_left_div">
		
				<!---------- Action Buttons Starts -------------->	
				<div class="row button_section_div" <?php echo $btn_class;?>>
					<div class="form-group" style="float:left;position:absolute;margin-top:21px;">
					<?php
						$page_url = $_SERVER['REQUEST_URI'];
					?>
					<form id = "borrower_form"  method = "post" action = "<?php echo $page_url;?>">
					<b style="margin-left: -29px;">Borrower Name:</b>
					
						<select id="search_borrower_id" name = "borrower_ids" onchange="select_borrower_id(this)" class="selectpicker" data-live-search="true" >
							<option value=''>Select One</option>
							<?php
							foreach($all_borrower_data as $row)
							{
								$style=' style="color:white;background-color:#808080;" ';	
								$optionStatus=0;							
								if(!empty($all_particular_data)){
									foreach($all_particular_data as $userRow){
										if($userRow->id==$row->id){
											$style='';
											$optionStatus=1;
											break;
										}
									}
								}
							?>
							<option status="<?php echo $optionStatus;?>"  value='<?= $row->id; ?>' <?php if(isset($borrower_id)) { if($borrower_id == $row->id){ echo 'selected'; } } ?> <?php echo $style;?> > <span <?php echo $style;?> ><?php echo $row->b_name;?></span></option>
							<?php
							}
							?>
						</select>
						<input type = "hidden" id = "user_login_status" name = "user_login_status" value = "<?php echo $userLoginStatus ;?>" />
						<input type = "hidden" id = "btn_name" name = "btn_name" value = "<?php echo $btn_name ;?>" />	
						</form>
					</div>
				</div>
				
				<div class="row button_section_div" >
								
				<div class="form-group" style="float:right;">
						<div class="">
							<?php
							if($btn_name == 'view'){
								$new_class = "style = 'display:block;' ";
								if($btn_name == 'new'){
									$new_class = "style = 'display:none;' ";
								}
							?>
											
							<a  class="btn btn-danger borrower_save_button"  onclick="delete_borrower('<?= $borrower_id; ?>')">Delete</a>
							<?php //if($user_role == '2'){ ?>
							<a  name = "button" class="btn btn-primary borrower_save_button" onclick = "fetch_borrower_id();" href = "javascript:void(0);" >Edit</a>
							<?php //} ?>
							<a <?php echo $new_class;?> id=""  href="<?php echo base_url().'borrower_data/new';?>" class="btn btn-primary borrower_save_button" value="add">New</a>
							<?php	}
												
								$new_class = "style = 'display:block;' ";
								if($btn_name == 'new'){
									$new_class = "style = 'display:none;' ";
							?>
							<button  type = "submit" name = "button" class="btn btn-primary borrower_save_button" value="save">Save</button>
							<a  class="btn btn-primary borrower_save_button" href = "<?php echo base_url().'borrower_data';?>" >Close</a>
							
							<?php	} if($btn_name == 'edit'){ ?>
							
							<button  type = "submit" name = "button" class="btn btn-primary borrower_save_button" value="save">Save</button>
									
							<a  class="btn btn-primary borrower_save_button" href = "<?php echo base_url().'borrower_data';?>" >Close</a>
								
							<?php }?>
									
						</div>
					</div>
				</div>
				
			<form method="POST" action = "<?php echo base_url();?>add_borrower_data" id="formID" class="lender-page">
					
				<input type="hidden" id = "borrower_id" value="<?php  echo $borrower_id; ?>" name="borrower_id" />
				<input type="hidden" id = "contact_idd" value="<?php  echo $contact_idd; ?>" name="contact_idd" />
			
				<!---------- Action Buttons Ends --------------->		
				<div class="row borrower_data_row">
			
					<h4>Borrower Information:</h4>

					<div class="col-md-12">
						<label>Borrower Name: </label>
						&nbsp;<span class = "view_inputs"><?php echo isset($fetch_borrower_id_data[0]->b_name) ? $fetch_borrower_id_data[0]->b_name : ''; ?>
						</span>
					</div>
				</div>

				<div class="row borrower_data_row" id="company_information_data">
					<div class="col-md-12">
						<label>Legal Entity Name:</label>
						&nbsp;<span class = "view_inputs"><?php echo isset($fetch_borrower_id_data[0]->company_name) ? $fetch_borrower_id_data[0]->company_name : ''; ?></span>	
					</div>		
				</div>
				
						
				<div class="row borrower_data_row">
					<div class="col-md-4">
						<label>Borrower #: </label>
						&nbsp;<span class = "view_inputs"><?php echo isset($fetch_borrower_id_data[0]->borrower_hash) ? $fetch_borrower_id_data[0]->borrower_hash : ''; ?></span>
					</div>
					<div class="col-md-4">
						<label>Primary Contact: </label>
						&nbsp;<span class = "view_inputs"><?php echo isset($fetch_borrower_id_data[0]->t_user_id) ? $all_users[$fetch_borrower_id_data[0]->t_user_id] : ''; ?>
						</span>
					</div>
					<div class="col-md-4">
						<label>Date Created: </label>
						&nbsp;<span class = "view_inputs"><?php echo isset($fetch_borrower_id_data[0]->bdate_created) ? date('m-d-Y', strtotime($fetch_borrower_id_data[0]->bdate_created)) : ''; ?>
						</span>
					</div>
				</div>

				<div class="row borrower_data_row">
					
					<div class="col-md-4">
						<label>Borrower Type:</label>
						&nbsp;<span class = "view_inputs"><?php echo isset($fetch_borrower_id_data[0]->borrower_type) ? $borrower_type_option[$fetch_borrower_id_data[0]->borrower_type] : '';?></span>
									
					</div>
					<div class="col-md-4">
						<label>Tax ID #:</label>
						&nbsp;<span class = "view_inputs"><?php echo isset($fetch_borrower_id_data[0]->b_tax_id) ? $fetch_borrower_id_data[0]->b_tax_id : '';?></span>
									
					</div>
					

				</div>

				
				
				<div class="row borrower_data_row">
					<div class="col-md-4">
						<label>State Entity Number:</label>
					
					
						&nbsp;<span class = "view_inputs"><?php echo isset($fetch_borrower_id_data[0]->state_entity_no) ? $fetch_borrower_id_data[0]->state_entity_no : ''; ?></span>
								
					</div>
						<div class="col-md-4">
						<label>Registered State:</label>
					
					
						&nbsp;<span class = "view_inputs"><?php if(isset($fetch_borrower_id_data[0]->c_reg_state)){ if($fetch_borrower_id_data[0]->c_reg_state){
								echo $STATE_USA[$fetch_borrower_id_data[0]->c_reg_state]; }} ?></span>
					</div>
				</div>
					
				<div class="row borrower_data_row">
					<div class="col-md-8">
						<label>Street Address:</label>
				
						
						&nbsp;<span class = "view_inputs"><?php echo isset($fetch_borrower_id_data[0]->b_address) ? $fetch_borrower_id_data[0]->b_address : ''; ?></span>
							
					</div>
					<div class="col-md-4">
						<label>Unit #:</label>
					
					
						&nbsp;<span class = "view_inputs"><?php echo isset($fetch_borrower_id_data[0]->b_unit) ? $fetch_borrower_id_data[0]->b_unit : ''; ?></span>
							
					</div>
						
				</div>
					
				<div class="row borrower_data_row">
					<div class="col-md-4">
						<label>City:</label>
					
					
						&nbsp;<span class = "view_inputs"><?php echo isset($fetch_borrower_id_data[0]->b_city) ? $fetch_borrower_id_data[0]->b_city : ''; ?></span>
							
					</div>
						
					<div class="col-md-4">
						<label>State: </label>
					
					
						&nbsp;<span class = "view_inputs"><?php if(isset($fetch_borrower_id_data[0]->b_state)){ if($fetch_borrower_id_data[0]->b_state){
						echo $STATE_USA[$fetch_borrower_id_data[0]->b_state]; }} ?></span>
						
					</div>
					
					<div class="col-md-4">
						<label>Zip:</label>
					
					
						&nbsp;<span class = "view_inputs"><?php echo isset($fetch_borrower_id_data[0]->b_zip) ? $fetch_borrower_id_data[0]->b_zip : ''; ?></span>
					</div>
				</div>
					
			<!-- 	<div class="row borrower_data_row" id="vesting_section_div">
						
					<div class="col-md-9">
						<label>Loan Vesting:</label>
					
				
						&nbsp;<span class = "view_inputs"><?php echo isset($fetch_borrower_id_data[0]->b_vesting) ? $fetch_borrower_id_data[0]->b_vesting : ''; ?></span>
					</div>
				</div> -->

		<!-- 		<div class="row borrower_data_row" id="vesting_section_div">
						
					<div class="col-md-9">
				<label>Additional Guarantor:</label>
							<?php if(isset($borrower_contact_data))
					{
						foreach($borrower_contact_data as $key => $cdata){
						
							$cc_id[]=$cdata->contact_id;

							foreach($fetch_all_contact as $row)
							{
								if($row->contact_id == $cdata->contact_id){ 
									$cname[] = $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname;
								}
						
						}

				}

				}

					?>

					<?php if(isset($cname[0]) && isset($cname[1]) && isset($cc_id[0])  && isset($cc_id[1]))

					{
						?>
					<a href="<?php echo base_url().'viewcontact/'.$cc_id[0];?>"><span class = "view_inputs"><?php echo isset($cname[0]) ? $cname[0] : ''; ?></span></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php echo base_url().'viewcontact/'.$cc_id[1];?>"><span class = "view_inputs"><?php echo isset($cname[1]) ? $cname[1] : ''; ?></span></a>

				
			<?php }elseif(isset($cname[0]) && isset($cc_id[0])) {?>

					<a href="<?php echo base_url().'viewcontact/'.$cc_id[0];?>"><span class = "view_inputs"><?php echo isset($cname[0]) ? $cname[0] : ''; ?></span></a>


				<?php }else{
				if(isset($cc_id[1])){
					?>
 				<a href="<?php echo base_url().'viewcontact/'.$cc_id[1];?>"><span class = "view_inputs"><?php echo isset($cname[1]) ? $cname[1] : ''; ?></span></a>
				<?php }}?>

				</div> 	</div> -->
				<!--
				<div class="row borrower_data_row" >
					<div class="col-md-9">
						<label>Borrower / Contact OFAC Search:</label>
						&nbsp;<span class = "view_inputs"><?php echo isset($fetch_borrower_id_data[0]->ofac_search) ? $no_yes_option[$fetch_borrower_id_data[0]->ofac_search] : ''; ?></span>
					</div>
				</div>-->
					
				<!--<div class="row borrower_data_row" id="vesting_section_div">
					<div class="col-md-9">
						<label>Personal Guaranty Vesting:</label>
					
					
						&nbsp;<span class = "view_inputs"><?php echo isset($fetch_borrower_id_data[0]->b_pg_vesting) ? $fetch_borrower_id_data[0]->b_pg_vesting : ''; ?></span>
					</div>
				</div>-->
					
					
					<div class="row borrower_data_row ">
						<h4>Borrower Financial Data</h4>
					</div>
					<div class="row borrower_data_row">
						<div class="col-md-6 radio_buton_div">
							<label class="lable_border_section" >Is the Balance Sheet / Income Statement available?</label>
						</div>
							<div class="col-md-6 radio_buton_div"> 
								<?php if(isset($fetch_borrower_id_data[0]->in_balance_income_sheet)){
								if($fetch_borrower_id_data[0]->in_balance_income_sheet == '1'){ $optn_incme_statement = 'Yes'; }
								else if($fetch_borrower_id_data[0]->in_balance_income_sheet == '0'){ $optn_incme_statement = 'No'; }
								else{ $optn_incme_statement = ''; }
								}else{ $optn_incme_statement = ''; } ?>	  
								<span class = "view_inputs"><?php echo $optn_incme_statement;?></span>	
							</div>
						
					</div>

					<div class = 'row  balancesheet_dependent'>
						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label l" for="textinput">Balance Sheet date: </label>  
							<div class="col-md-6 float_left_section">
								&nbsp;&nbsp;&nbsp;<label>From: </label>
								<span class = "view_inputs"><?php echo isset($fetch_borrower_id_data[0]->balancesheet_date) ? $fetch_borrower_id_data[0]->balancesheet_date : ''; ?></span>&nbsp;&nbsp;
								<label>To: </label>
								<span class = "view_inputs"><?php echo isset($fetch_borrower_id_data[0]->balancesheet_to) ? $fetch_borrower_id_data[0]->balancesheet_to : ''; ?></span>
							</div>
						</div>
					</div>
								
					<div class='row  balancesheet_dependent'> 
						<label class="col-md-6 new_lable l">Income Statement : </label>
						<div class="col-md-6 short_input_section">
							<input  <?php echo $fields_disabled;?> type="hidden" class="working" name="income_sta" value="<?php echo isset($fetch_borrower_id_data[0]->income_statement) ? $fetch_borrower_id_data[0]->income_statement : ''; ?>">
							<label>From: </label>
								
							<span class = "view_inputs"><?php echo isset($fetch_borrower_id_data[0]->income_s_from) ? $fetch_borrower_id_data[0]->income_s_from : ''; ?> </span>&nbsp;&nbsp;
							<label>To:</label>
								
							<span class = "view_inputs"><?php echo isset($fetch_borrower_id_data[0]->income_s_to) ? $fetch_borrower_id_data[0]->income_s_to : ''; ?></span>
								
						</div>
					</div>
					
					<div class="row  balancesheet_dependent">
						<div class="form-group">
							<label class="col-md-6  lable_border_section" >Has the Financial Statement been audited by a CPA / PA </label>
							<div class="col-md-6 radio_buton_div" id="financial_s_radio"> 
								<?php if(isset($fetch_borrower_id_data[0]->cpa_pa)){
									if($fetch_borrower_id_data[0]->cpa_pa == '1'){ $optn_cpa_pa = 'Yes'; }
									else if($fetch_borrower_id_data[0]->cpa_pa == '0'){ $optn_cpa_pa = 'No'; }
										else{ $optn_cpa_pa = ''; }
									}else{ $optn_cpa_pa = ''; } ?>	  
								<span class = "view_inputs"><?php echo $optn_cpa_pa;?></span>	
										
							</div>
						</div>
					</div>
					
								
				<div class="row borrower_data_row">
					<div class="col-md-6 radio_buton_div">
						
						<label class="lable_border_section" >Additional Information is included on the Attached Addendum </label>
					</div>	
						<div class="col-md-6 radio_buton_div"> 
								
							<?php if(isset($fetch_borrower_id_data[0]->attached_addedeum)){
								if($fetch_borrower_id_data[0]->attached_addedeum == '1'){ $optn_attached_addedeum = 'Yes'; }
								else if($fetch_borrower_id_data[0]->attached_addedeum == '0'){ $optn_attached_addedeum = 'No'; }
								else{ $optn_attached_addedeum = ''; }
								}else{ $optn_attached_addedeum = ''; }  ?>	
							<span class = "view_inputs"><?php echo $optn_attached_addedeum;?></span>
						</div>
					
				</div>

				<div id="contact_information_div">
					
					<div class="row borrower_data_row">
						<h4>Borrower Contacts</h4> 
						
					</div>	
									
					<?php
					$count_contact = 0;
					$contactEmailId="";	
					$contactPhoneNum="";	
					if(isset($borrower_contact_data))
					{
						foreach($borrower_contact_data as $key => $cdata){
							
							foreach($fetch_all_contact as $row)
							{
								if($row->contact_id == $cdata->contact_id){ 
									$contactEmailId=$row->contact_email;
									$contactPhoneNum=$row->contact_phone;
									$cname = $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname;
								}
							}
					?>
						<div class="new_contact_div">
							<?php
								$count_contact = $key + 1;
								if($contact_idd){
									if( $cdata->contact_id == $contact_idd){
										$contact_class = "active-class";
									}else {
										$contact_class = "inactive-class";
									}
								}	else{
									$contact_class = "no-class";
								}
								
								if($count_contact == '1'){
									$contact = 'Primary Contact:';
								}else{
									$contact = 'Secondary Contact:';
								}
												
							?>	
							<div class = "contacts_div <?php echo $contact_class;?>">
													
								<div class="row borrower_data_row">
								<h5 style="font-size:15px;font-weight:600;"></h5>
									<div class="col-md-4">
									
										<label>Contact Name: </label>
										<input  <?php echo $fields_disabled;?>type = "hidden" name = "borrower_contact_id" value = "<?php echo $cdata->id;?>">
										&nbsp;<a href= "<?php echo base_url().'viewcontact/'.$cdata->contact_id;?>"><span class = "view_inputs marg"><?php
											echo isset($row->contact_id) ? $cname : '';
										?></span></a>
									</div>
										<div class="col-md-4">
										<label>Title:</label>
									&nbsp;<span class = "view_inputs margg"><?php echo $cdata->c_title; ?></span>
									</div>
								
																			
									<div class="col-md-4">
										<!--  <?php echo base_url().'borrower_data/edit/'.$borrower_id;?> --->
										<label>Sign Documents:</label>
										
										&nbsp;<span class = "view_inputs marg"><?php echo ($cdata->contact_responsibility == '1') ? 'Yes': 'No';?></span>
									</div>
									
									
								</div>
				
								<div class="row borrower_data_row">
								
													
									<div class="col-md-4">
										<label>Phone:</label>
										&nbsp;<span class = "view_inputs margg"><?php echo $contactPhoneNum; ?></span>
									</div>
									<div class="col-md-4"> 
										<label>Email:</label>
										&nbsp;<a href = "mailto:<?php echo $cdata->contact_email; ?>"><span class = "view_inputs vb"><?php echo $contactEmailId; ?></span></a>
									</div>

									<div class="col-md-4 contact_gurrantor_div">
										<label>Primary Contact: </label>
										<span class = "view_inputs marg"><?php echo ($cdata->contact_primary !='') ? $primary_contact_option[$cdata->contact_primary]: '';?></span>
									</div>
								</div>
							</div>				
						</div>
						<?php	
						}
					}
						?>
				</div>
								
					
				<div id="hidden_contact_information_div" style="display:none;">
					<div class="new_contact_div">
						
						<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>Contact Name:</label>
								<input type = "hidden" name = "borrower_contact_id" value = "">
								<select  <?php echo $class_disabled;?> class="form-control" name="contact_contact_id[]" onchange="fetch_contact_details(this)" >
									<option value=""></option>
									<?php
									foreach($fetch_all_contact as $row)
									{
										echo '<option value="'.$row->contact_id.'" >'.$row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname.'</option>';
									}
									?>
								</select>
							</div>
														
							<div class="col-md-3">
								<label>Sign Documents:</label>
								<select <?php echo $class_disabled;?> name = "contact_responsibility[]" class = "selectpicker form-control">
									<?php foreach($responsibility as $key => $optn){
										if($key == $row_data['responsibility']){
											$selected = 'selected';
										}else{
											$selected = '';
										}
									?>
									<option <?php echo $selected;?> value = "<?php echo $key;?>"><?php echo $optn;?></option>
									<?php } ?>
								</select>
							</div>
														
							<div class="col-md-3 contact_gurrantor_div">
								<label>Personal Guarantor :</label>
								<select  <?php echo $class_disabled;?> class="form-control contact_gurrantor" name="contact_gurrantor[]" >
									<option value=""></option>
									<?php
									foreach($yes_no_option as $key => $row)
									{
									?>
									<option value="<?php echo $key; ?>"><?php echo $row;?></option>
									<?php
									}
									?>
								</select>
							</div>	
						</div>
						
						<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>Title:</label>
								<input  type = "textbox" <?php echo $fields_disabled;?> class="form-control contact_title" <?php echo $class_disabled;?> name="contact_title[]" >
							</div>
								
							<div class="col-md-3">
								<label>Phone:</label>
								<input  type = "textbox" <?php echo $fields_disabled;?> class="form-control contact_phone" name="contact_phone[]"  readonly>
							</div>
							
							<div class="col-md-3"> 
								<label>Email:</label>
								<input type = "textbox" <?php echo $fields_disabled;?> class="form-control contact_email" name="contact_email[]" readonly>
							</div>
						</div>
						
						<div class="row borrower_data_row">
								
							<div class="col-md-3 primary_contactdiv" >
								<input  <?php echo $class_disabled;?> onclick = "check_primary_contact(this.id)" class = "chkbx_primary_contact" type = "checkbox" id = "primary_contact_1" /> Primary Contact
								<input type = "hidden" class = "chkbx_primary_contact_val" value = "" id = "primary_contact_val1" name = "primary_contact[]"/>
							</div>
						</div>						
					</div>	
				</div>	
					
						
						
			</form>	

			<div class="row borrower_data_row table-borrower" id="">
					<h4>Borrower History:</h4>
			<div class="col-md-6">
					<!--<table class="table ">
						<tbody>
							<tr>
								<td style="width:33.3%;"># of Active Loans</td>
								<td style="width:33.3%;"><?php echo $active_count; ?></td>
							</tr>
							<tr>
								<td style="width:33.3%;">$ of Active Loans</td>
										
								<td style="width:33.3%;">$<?php echo number_format($active_amount);?></td>
							</tr>
							<tr>
								<td># of Paid Off Loans</td>
								<td><?php echo $paid_count; ?></td>
							</tr>
							<tr>
								<td>$ of Paid Off Loans</td>
								<td>$<?php echo number_format($paid_amount); ?></td>
							</tr>
									
							<tr>
								<td># of Pipeline Loans</td>
								<td><?php echo $pipeline_count; ?></td>
							</tr>
							<tr>
								<td>$ of Pipeline Loans</td>
								<td>$<?php echo number_format($pipeline_amount); ?></td>
							</tr>	
							<tr>
								<td># of Total Loans</td>
								<td><?php echo isset($fetch_all_loan) ? $fetch_all_loan['all_count_loan'] : 0; ?></td>
							</tr>
							<tr>
								<td>$ of Total Loans</td>
								<td>$<?php echo isset($fetch_all_loan) ? number_format($fetch_all_loan['all_total_loan_amount']) : 0; ?></td>
							</tr>	
						</tbody>
					</table>-->
					<table class="table table-border table-responsive table-stripe">
						<thead>
							<tr>
								<th>Loan Status</th>
								<th># of Loans</th>
								<th>$ of Loans</th>
							</tr>
						<thead>
						<tbody>
							<tr>
								<td>Pipeline Loans</td>
								<td><?php echo $pipeline_count; ?></td>
								<td>$<?php echo number_format($pipeline_amount);?></td>
							</tr>
							<tr>
								<td>Active Loans</td>
								<td><?php echo $active_count; ?></td>
								<td>$<?php echo number_format($active_amount);?></td>
							</tr>
							<tr>
								<td>Paid Off Loans</td>
								<td><?php echo $paid_count; ?></td>
								<td>$<?php echo number_format($paid_amount);?></td>
							</tr>
							<?php 
							$total_loan_count = $pipeline_count + $active_count + $paid_count;
							$total_loan_amount = $pipeline_amount + $active_amount + $paid_amount;
							
							?>
							<tr>
								<th>Total Loans</th>
								<th><?php echo $total_loan_count; ?></th>
								<th>$<?php echo number_format($total_loan_amount);?></th>
							</tr>
							
						</tbody>
					</table>
			</div>
			</div>
				<!----------------- Borrower History Div Ends--------->

				<!----------------------------------- Borrower loan portfolio ---------------------->
				<?php
				// echo '<pre>';
				// print_r($borrower_related_loans_data);
				// echo '</pre>';



				?>
				<div class="row borrower_data_row">
					<h4>Borrower Loan Portfolio:</h4>
						<div class="col-md-12">
							<table class="table table-striped table-condensed flip-content" id="borrowertable">
								<thead>
									<tr>
										<th>Street Address</th>
										<th>Unit #</th>
										<th>City</th>
										<th>State</th>
										<th>Zip</th>
										<th>Loan Amount</th>
										<th>Loan Status</th>
									</tr>
								</thead>
								<tbody>
								<?php 
								$count = 0;
								$total_loan_amount = 0;
								if(isset($borrower_related_loans_data) && is_array($borrower_related_loans_data)){ 
										foreach($borrower_related_loans_data as $row){

										if($row->property_address != ''){
										
										$count++;
										echo '<tr>
											<td><a href='.base_url().'load_data/'.$row->id.'>'.$row->property_address.'</a></td>
											<td>'.$row->unit.'</td>
											<td>'.$row->city.'</td>
											<td>'.$row->state.'</td>
											<td>'.$row->zip.'</td>
											<td>$'.number_format($row->loan_amount,2).'</td>
											<td>'.$loan_status_option[$row->loan_status].'</td>
											
										</tr>';

										$total_loan_amount += $row->loan_amount;

								} } }else{

										echo '<tr>
											<td colspan="7">No data found!</td>
										</tr>';
								} 
								?>
								</tbody>
								<tfoot>
									<tr>
										<th>Total: <?php echo $count;?></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th>$<?php echo number_format($total_loan_amount,2);?></th>
										<th></th>
									</tr>
								</tfoot>
							</table>

						</div>
				</div>
				<!----------------------------------- Borrower loan portfolio ---------------------->

				<!--<div class="row borrower_data_row">
					<h4>Loan Servicer Information:</h4>
				</div>
				<div class="row borrower_data_row">
					<div class="col-md-4">
						<label>FCI Account #:</label>
						<span class="view_inputs"><?php echo $fetch_borrower_id_data[0]->fci_acc_text;?></span>
					</div>

					<div class="col-md-4">
						<label>Del Toro Account #:</label>
						<span class="view_inputs"><?php echo $fetch_borrower_id_data[0]->del_toro_text;?></span>
					</div>
				</div>-->
			
	</div>
	
					
			<!---------- main left div Ends--------------->			
				
			
		
			<span class="hr"></span>
				
				
			<!---------- main right div Ends--------------->			
				
			<div class = "main_right_div">
				<div class="col-md-12 top" style="margin: -3px -16px 0px;">&nbsp;
					<!--<h4 style="font-family:sans-serif;background-color: #e8e8e8;height: 30px;margin-top: 3px;line-height: 2;padding: 0px 21px;">Saved Documents</h4>-->
				</div>
				
				<div class="row" id="model_left_align">
					<?php if($borrower_id != ''){ ?>		
			
						<div class="col-md-12">
							<a class="btn  btn-primary btn-block" data-toggle="modal" href="#borrower_document">Documents</a>
						</div>
						<div class="col-md-12 addmarginright">
							
							<a class="btn btn-primary btn-block" data-toggle="modal" href="#LineofCredit">Preferred Borrower Account</a>
						</div>
						<div class="col-md-12 addmarginright">
							
							<a class="btn btn-primary btn-block" data-toggle="modal" href="#PropertyApproval">Property Approval</a>
						</div>
						<div class="col-md-12 addmarginright">
							<a class="btn btn-primary btn-block" data-toggle="modal" href="#PriorProjects">Project Portfolio</a>
						</div>
					<?php } ?>
				</div>
						
			</div>

			<!----------Contact Document modal---->
<div class="modal fade bs-modal-lg" id="borrower_document" tabindex="-1" role="large" aria-hidden="true">
	<div class="modal-dialog">		
		<div class="modal-content">		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Borrower Data - Documents Page </h4>
			</div>
			
			<div class="modal-body">
				<div class="doc-div">
				
					<div class="row">
						<input type = "hidden" name = "document_id" id = "document_id" >
					</div>
					
					<div id="ct_Documentsadd" class="row">						
						<div class="col-md-2">&nbsp;</div>
						<div class="col-md-8">
							<div class="ct_loan_saved_document" style="display: none;">
								<form id="uploaddpdfNew" name="uploaddpdfNew" method="POST" action="<?php echo base_url()."Borrower_data/update_borrower_documents";?>" enctype="multipart/form-data">
									<input type="hidden" value="<?php echo $borrower_id;?>" name="borrower_idd">
									<div class="form-group">
										<label for="document_name">Document Name:</label>
										<input type="text" class="form-control" id="document_name" name="document_name" required>
									</div>
									<div class="form-group">
										<div id="ad">
											<label>Select Document:</label>
											<input type="file" id = "borrower_upload_1" name ="borrower_upload_doc[]" class="select_images_file" onchange="change_this_image(this)" required>
										</div>
									</div>
									<button type="submit" id="uploadd" name="upload_file" class="btn blue">Save</button>
								</form>
							</div>
						</div>
						<div class="col-md-2">&nbsp;</div>
					</div>

					<div class="col-md-12 ct_Documentsadd_table">
			
						<table class="table table-responsive" id="loan_doc_table" style="width: 100% !important;">
							<thead>
								<tr>
									<th>Action</th>
									<th>Name of Document</th>
									<th>Uploaded Date</th>
									<th>User</th>
									<th>View</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if($fetch_borrower_document){
									foreach($fetch_borrower_document as $contactDocuments) {

									$FileNameGet = '';
									
									$UploadedDate = date("m-d-Y", strtotime($contactDocuments->created_at));
									
									$filename = $contactDocuments->borrower_document;
									$FileArr = explode('/', $filename);
									$FileNameGet = $FileArr[count($FileArr)-1];

									$file1 = explode('/', $FileNameGet);
									$file2 = explode('.', $file1[count($file1)-1]);

									$FileNameGet = $NameUploadedcalss = $file2[0];

									if($contactDocuments->document_name){
										$NameUploaded = $contactDocuments->document_name;
									}else{
										$NameUploaded = $FileNameGet;
									}


									$up_user_id = $contactDocuments->user_id;
									$UpUserDate = $this->User_model->query("SELECT * FROM user WHERE id = '$up_user_id'")->row();
									if($UpUserDate){
										$UploadedUser = $UpUserDate->fname.' '.$UpUserDate->lname;
									}
								?>
								<tr attrremove="<?php echo $contactDocuments->id; ?>">
									<td>
										<a><i id="<?php echo $contactDocuments->id; ?>" onclick="delete_doc(this);" title = "Delete File" class = "fa fa-trash"  aria-hidden="true"></i></a>
									</td>
									<td>
										<?php echo $NameUploaded; ?>
									</td>
									<td><?php echo $UploadedDate; ?></td>
									<td><?php echo $UploadedUser; ?></td>
									<td>
										<a href="<?php echo aws_s3_document_url($contactDocuments->borrower_document); ?>" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a>
									</td>
								</tr>
								<?php
									}
								}
								?>

							</tbody>
						</table>
					</div>
				
				</div>
				<!--<div class="row">
					<div class="col-md-12">
						<button onclick = "borrower_document_add_more_row();" id = "add_row" class = "btn blue" type = "button"><i class="fa fa-plus" aria-hidden="true"> Add More</i></button>
					</div>
				</div>-->
			</div>
		
			<div class="modal-footer">
				<button type="button" id="ct_loan_saved_documentadd" class="btn btn-primary">Add Document</button>
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				
			</div>
	</div>
		<!-- /.modal-content -->
</div>
	<!-- /.modal-dialog -->
</div>
			<!---  Contact Document modal  End ---->

			<!----------------- main right div Ends--------->

			<!--------------------Project Portfolio START ------------>
			<div class="modal fade bs-modal-lg" id="PriorProjects" tabindex="-1" role="large" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title"><b>Project Portfolio</b></h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12" style="overflow: auto;">
									<table class="table" width="100%">
										<thead>
											<tr>
												<th>Street Address</th>
												<!-- <th>State</th> -->
												<th>Project</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php if(isset($borrower_prior_project) && is_array($borrower_prior_project)){ 
													foreach($borrower_prior_project as $row){ 
														$address = '';
														if(!empty($row->street))
														{
															$address .= $row->street;
														}

														if(!empty($row->unit))
														{
															$address .=!empty($address)?' '.$row->unit : ' '.$row->unit;
														}

														if(!empty($row->city))
														{
															$address .= !empty($address)?'; '.$row->city : $row->city;
														}

														if(!empty($row->state))
														{
															$address .= !empty($address)?', '.$row->state : $row->state;
														}

														if(!empty($row->zip))
														{
															$address .= !empty($address)?' '.$row->zip : $row->zip;
														}
														

														?>
														<tr>
															<td><?php echo $address;?></td>
															
															<td><a target="_SERVER" href="<?php echo $row->balink;?>" target="_blank">Link</a></td>
															<td>
																<a title="Edit" id="<?php echo $row->id;?>" onclick="addmoreproject(this)" class="btn btn-info btn-sm" style="margin-right : 3px"><i class="fa fa-edit"></i></a>	
																<a title="Edit" id="<?php echo $row->id;?>" onclick="deletemoreproject(this)" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
															</td>
														</tr>
											<?php } }else{ ?>
														<tr>
															<td colspan="4">No project found!</td>
														</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue" id="new" onclick="addmoreproject(this);">Add More</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<!----------- ADD and EDIT ----------->
			<div class="modal fade bs-modal-lg" id="ProjectPortfolio" tabindex="-1" role="large" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<form method="post" action="<?php echo base_url();?>Borrower_data/priorProjects" enctype="multipart/form-data">

							<input type="hidden" value="<?php echo $borrower_id;?>" name="pborrower_id">
							<input type="hidden" value="new" name="prow_id">
							<input type="hidden" value="" name="hiddenImage" id="hiddenImage">
							<div class="modal-header">
								<h4 class="modal-title"><b>Project Portfolio</b></h4>
							</div>
							<div class="modal-body">

								<div class="row borrower_data_row">
									<div class="col-md-6">
										<label>Street Address:</label>
										<input type="text" name="pstreet" class="form-control" value="" required="required">
									</div>
									<div class="col-md-6">
										<label>Unit #:</label>
										<input type="text" name="punit" class="form-control" value="">
									</div>
									<div class="col-md-6">
										<label>City:</label>
										<input type="text" name="pcity" class="form-control" value="" required="required">
									</div>
									<div class="col-md-6">
										<label>State:</label>
										<select class="form-control" name="pstate" id="pstatessb">
											<?php foreach($STATE_USA as $key => $row){ ?>
												<option value="<?php echo $key;?>"><?php echo $row;?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-6">
										<label>Zip:</label>
										<input type="text" name="pzip" class="form-control" value="">
									</div>
								</div>

								<div class="row borrower_data_row">
									<div class="col-md-12">
										<label>Project Link:</label>
										<input type="url" name="baimg" class="form-control" value="" required="required">
									</div>
								</div>

								<div class="row borrower_data_row">
									<div class="col-md-12">

										<label>Project Images:</label><br>
										<label id="ProImage"></label>
										<input type="file" name="pimage[]" class="form-control" accept=".jpg, .jpeg, .png" multiple>
									</div>
								</div>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn default" data-dismiss="modal">Close</button>
								<button type="submit" name="priorsubmit" class="btn blue">Save</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!--
				Description :This function use for Edit function in project portpolio details 
							  image show functionality, zip
				Author      :Bitcot
				Created     : 
				Modified    :25-03-2021
			-->
			<script type="text/javascript">
				
				function addmoreproject(that){

					var id = that.id;
					var proImage = '';

					$('#ProjectPortfolio input').val('');
					$('#ProjectPortfolio input[name="pborrower_id"]').val('<?php echo $borrower_id;?>');

					if(id == 'new'){
						
						$('#ProjectPortfolio input[name="prow_id"]').val('new');
						$('#ProjectPortfolio').modal('show');
					}else{
						//alert(id);
						$('#ProjectPortfolio input[name="prow_id"]').val(id);

						$.ajax({

		    				type : 'post',
		    				url  : '<?php echo base_url()."Borrower_data/get_b_projects";?>',
		    				data : {'rowid':id},
		    				success : function(response){
		    					var msg = 'Are you sure？'; 
		    					var data = JSON.parse(response);
		    					var imageArray = data.projectImage;
		    					var i;
		    					if(imageArray.length > 0)
		    					{
		    						for (i = 0; i < imageArray.length; i++) {
									  proImage += '<img src="'+imageArray[i]+'" width="50px" height="50px">&nbsp&nbsp';
									  proImage += '<br><span class="text-danger"><a class="text-danger" href="javascript:void(0)" onclick="deleteImage()">Delete</a></span>';
									}
		    					}
								
		    					$('#ProjectPortfolio input[name="pstreet"]').val(data.street);
		    					$('#ProjectPortfolio input[name="punit"]').val(data.unit);

		    					$('#ProjectPortfolio input[name="pcity"]').val(data.city);
		    					$('#ProjectPortfolio input[name="pzip"]').val(data.zip);
		    					$('#ProjectPortfolio input[name="baimg"]').val(data.balink);
		    					$('#hiddenImage').val(data.hiddenImage);
		    					$('#ProImage').html(proImage);

		    					$('#ProjectPortfolio select#pstatessb option[value="'+data.state+'"]').attr('selected',true);
		    					$('#ProjectPortfolio').modal('show');
		    				}
		    			});
					}
				}
			</script>

			<script type="text/javascript">

				function deleteImage()
				{
					if (confirm('Are you sure delete this image?')) 
					{
						$('#hiddenImage').val('');
						$('#ProImage').html('');	
					}	
				}
				
				function deletemoreproject(that){

					var id = that.id;
			

						$.ajax({

		    				type : 'post',
		    				url  : '<?php echo base_url()."Borrower_data/get_b_projectsDelete";?>',
		    				data : {'rowid':id},
		    				success : function(response){

		    					if(response > 0)
		    					{
		    				
		    						location.reload();
		    					}

		    			
		    				}
		    			});
					
				}
			</script>
			<!--------------------Project Portfolio END ------------>

			<!--------------------Property Approval START ------------>
			<div class="modal fade bs-modal-lg" id="PropertyApproval" tabindex="-1" role="large" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title"><b>Property Approval</b></h4>
						</div>
						<div class="modal-body">

							<div class="row">
								<div class="col-md-12">
									<table class="table">
										<thead>
											<tr>
												<th>Address</th>
												<th>Amount</th>
												<th>Date</th>
												<th>Request Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>

										<?php if(isset($get_property_approval) && is_array($get_property_approval)){ 
												foreach ($get_property_approval as $value) { 

													$req_status = $value->req_status;
													$property_id = $value->id;
													?>

												<tr id="row_<?php echo $value->id; ?>">
													<td><?php echo $value->pro_address.' '.$value->pro_unit.'; '.$value->pro_city.', '.$value->pro_state.' '.$value->pro_zip;?></td>
													<td>$<?php echo number_format($value->ApprovalAmount,2);?></td>
													<td><?php if($value->pro_date != ''){ echo date('m-d-Y', strtotime($value->pro_date)); }else{ echo ''; } ?></td>
													<td>
														<select class="form-control" name="dp_reqt_status" id="dp_reqt_status" property_id="<?php echo $property_id; ?>" borrower_id="<?php  echo $borrower_id; ?>">
															<option value="">Select One</option>
															<?php foreach($pro_app_req_status as $keyStatus => $rowStatus){
																$checkedSelect = '';
																if($req_status){
																	if($req_status == $keyStatus){
																		$checkedSelect = 'selected';
																	}
																}
															 ?>
																<option <?php echo $checkedSelect; ?> value="<?php echo $keyStatus;?>"><?php echo $rowStatus;?></option>
															<?php } ?>
														</select>
													</td>
													<td>
														<a title="Remove" onclick="deletePropertyApproval('<?php echo $value->id; ?>');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>

														<a title="Edit" onclick="editPropertyApproval('<?php echo $value->id; ?>');" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>

														<a title="Print" class="btn btn-success btn-sm" href="<?php echo base_url();?>Borrower_data/pre_approval_letter_property/<?php echo $borrower_id; ?>/<?php echo $value->id; ?>"><i class="fa fa-print"></i></a>
													</td>
												</tr>

										<?php } }else{ ?>

												<tr>
													<td colspan="5">No data found!</td>
												</tr>

										<?php } ?>
										</tbody>
										
									</table>
								</div>
							</div>

						</div>
						<div class="modal-footer">

							<div class="col-md-3">
								<a title="Edit" onclick="editPropertyApproval('new');" class="btn btn-primary pull-left"><i class="fa fa-plus"></i> Add</a>
							</div>

						</div>
					</div>
				</div>
			</div>


			<div class="modal fade bs-modal-lg" id="PropertyApprovalView" tabindex="-1" role="large" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<form method="POST" action="<?php echo base_url();?>Borrower_data/PropertyApproval">

							<input type="hidden" value="<?php echo $borrower_id;?>" name="borrower_iddss">
							<input type="hidden" value="" name="pro_app_ids"> <!-- value blank for new --->
							<div class="modal-header">

								<button type="submit" name="submitppa" class="btn blue pull-right" style="margin-right: 8px;">Save</button>

								<button type="button" class="btn blue pull-right" data-toggle="modal" data-target="#saveAgainppa" style="margin-right: 8px;">Close</button>

								<h4 class="modal-title"><b>Property Approval</b></h4>
							</div>
							<div class="modal-body">
								
								<div class="row">
									<div class="col-md-4">
										<label>Request Status:</label>
										<select class="form-control" name="req_status" id="reqt_status">
											<option>Select One</option>
											<?php foreach($pro_app_req_status as $key => $row){ ?>
												<option value="<?php echo $key;?>"><?php echo $row;?></option>
											<?php } ?>
										</select>
									</div>
								</div>

								<div class="row borrower_data_row">

									<div class="col-md-8">
										<label>Street Address:</label>
										<input type="text" name="pro_address" class="form-control" value="" required="required">
									</div>

									<div class="col-md-4">
										<label>Unit #:</label>
										<input type="text" name="pro_unit" class="form-control" value="">
									</div>

								</div>
								<div class="row borrower_data_row">

									<div class="col-md-4">
										<label>City:</label>
										<input type="text" name="pro_city" class="form-control" value="">
									</div>

									<div class="col-md-4">
										<label>State:</label>
										<select class="form-control" name="pro_state" id="pro_state">
											<?php foreach($STATE_USA as $key => $state_option){ ?>
												<option value="<?php echo $key;?>" ><?php echo $state_option;?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-4">
										<label>Zip:</label>
										<input type="text" name="pro_zip" class="form-control number_only" value="">
									</div>

								</div>
								<div class="row borrower_data_row">

									<div class="col-md-4">
										<label>Purchase Price:</label>
										<input type="text" name="purchase_price" class="form-control" value="">
									</div>

									<div class="col-md-4">
										<label>Rehab Cost:</label>
										<input type="text" name="rehab_cost" class="form-control" value="">
									</div>

									<div class="col-md-4">
										<label>Completion Value:</label>
										<input type="text" name="completion_val" class="form-control" value="">
									</div>

								</div>
								<div class="row borrower_data_row">

									<div class="col-md-4">
										<label>Approval Amount:</label>
										<input type="text" name="ApprovalAmount" class="form-control" value="" required="required">
									</div>

									<div class="col-md-4">
										<label>Loan Term:</label>
										<input type="text" name="LoanTerm" class="form-control number_only" value="">
									</div>

									<div class="col-md-4">
										<label>Date:</label>
										<input type="text" name="pro_date" class="form-control datepicker" value="">
									</div>

								</div>

							</div>

							<!---------------Do you want to save the info start------------------>
							<div class="modal fade" id="saveAgainppa" tabindex="-1" role="dialog" aria-labelledby="example1ModalLabel" aria-hidden="true">
							  <div class="modal-dialog modal-sm" role="document">
							    <div class="modal-content">
							      <div class="modal-body">
							        
							        <h5><b>Do you want to save the information?</b></h5>
							        	
							      </div>
							      <div class="modal-footer">
							        <a type="button" href="<?php echo base_url();?>borrower_view/<?php echo $borrower_id;?>" class="btn blue">No</a>
							        <button type="submit" name="submitppa" class="btn blue">Yes</button>
							      </div>
							    </div>
							  </div>
							</div>
							<!---------------Do you want to save the info end-------------------->

						</form>
					</div>
				</div>
			</div>
			<!--------------------Property Approval end -------------->


			<!--------------------LINE OF CREDIT START ------------>
			<div class="modal fade bs-modal-lg" id="LineofCredit" tabindex="-1" role="large" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<form method="POST" action="<?php echo base_url();?>Borrower_data/updateLineofcredit">	
							<input type="hidden" value="<?php echo $borrower_id;?>" name="borrower_idds">
							<div class="modal-header">

								<a class="btn btn-success pull-right" href="<?php echo base_url();?>Borrower_data/download_pre_approval_letter/<?php  echo $borrower_id; ?>">Print</a>

								<button type="submit" name="submitline" class="btn blue pull-right" style="margin-right: 8px;">Save</button>

								<button type="button" class="btn blue pull-right" data-toggle="modal" data-target="#saveAgainpreapp" style="margin-right: 8px;">Close</button>

								<h4 class="modal-title"><b>Letter of Credit</b></h4>
							</div>
							<div class="modal-body">

								<div class="row borrower_data_row">

									<div class="col-md-12">
										<input type="checkbox" name="PreApproved" class="form-control" id="PreApproved_chk" onclick="checkPreApprovedbox(this,'');" value="<?php echo isset($fetch_borrower_id_data[0]->PreApproved) ? $fetch_borrower_id_data[0]->PreApproved : '0'; ?>" <?php if($fetch_borrower_id_data[0]->PreApproved == 1){echo 'checked';}?>><b style="font-weight: 600;"> Preferred Borrower Account</b> 
									</div>

								</div>
								<div class="row borrower_data_row">
									<div class="col-md-4">
										<label>Approved Amount:</label>
										<input type="text" name="ApprovedAmount" class="form-control" value="<?php echo isset($fetch_borrower_id_data[0]->ApprovedAmount) ? '$'.number_format($fetch_borrower_id_data[0]->ApprovedAmount) : ''; ?>">
									</div>
									<div class="col-md-4">
										<label>Date:</label>
										<input type="text" name="ApprovedDate" class="form-control datepicker" value="<?php echo isset($fetch_borrower_id_data[0]->ApprovedDate) ? date('m-d-Y', strtotime($fetch_borrower_id_data[0]->ApprovedDate)) : ''; ?>">
									</div>
									<div class="col-md-4">
										<label>Term (Months):</label>
										<input type="text" name="ApprovedTerm" class="form-control number_only" value="<?php echo isset($fetch_borrower_id_data[0]->ApprovedTerm) ? $fetch_borrower_id_data[0]->ApprovedTerm : ''; ?>">
									</div>
								</div>

								<div class="row borrower_data_row">
									<div class="col-md-4">
										<label>% of Purchase Price:</label>
										<input type="text" name="PurchasePrice" class="form-control" value="<?php echo isset($fetch_borrower_id_data[0]->PurchasePrice) ? number_format($fetch_borrower_id_data[0]->PurchasePrice,2).'%' : ''; ?>">
									</div>
									<div class="col-md-4">
										<label>% of Renovation Costs:</label>
										<input type="text" name="RenovationCosts" class="form-control" value="<?php echo isset($fetch_borrower_id_data[0]->RenovationCosts) ? number_format($fetch_borrower_id_data[0]->RenovationCosts,2).'%' : ''; ?>">
									</div>
									<div class="col-md-4">
										<label>% of Completion Value:</label>
										<input type="text" name="CompletionValue" class="form-control" value="<?php echo isset($fetch_borrower_id_data[0]->CompletionValue) ? number_format($fetch_borrower_id_data[0]->CompletionValue,2).'%' : ''; ?>">
									</div>
								</div>

								<div class="row borrower_data_row">
									<div class="col-md-4">
										<?php
											if($fetch_borrower_id_data[0]->ApprovedTerm != ''){
												$ExpirationDateval = date('m-d-Y', strtotime( '+'.$fetch_borrower_id_data[0]->ApprovedTerm.' months'. $fetch_borrower_id_data[0]->ApprovedDate));
											}else{
												$ExpirationDateval = date('m-d-Y', strtotime($fetch_borrower_id_data[0]->ApprovedDate));
											}

										?>
										<label>Expiration Date:</label>
										<input type="text" name="ExpirationDate" class="form-control" value="<?php echo $ExpirationDateval;?>" readonly>
									</div>
									
									<div class="col-md-4">
										<label>State:</label>
										<select class="form-control" name="loc_state">
											<?php foreach($STATE_USA as $key => $state_option){ ?>
												<option value="<?php echo $key;?>" <?php if($fetch_borrower_id_data[0]->loc_state == $key){ echo 'selected';}?> ><?php echo $state_option;?></option>
											<?php } ?>
										</select>
									</div>

								<script type="text/javascript">
									
									checkPreApprovedbox(this,'<?php echo isset($fetch_borrower_id_data[0]->PreApproved) ? $fetch_borrower_id_data[0]->PreApproved : '0'; ?>');
									function checkPreApprovedbox(that,key){

										if($(that).is(':checked') || key == '1'){

											$('input[name="ApprovedAmount"]').prop('disabled',false);
											$('input[name="ApprovedDate"]').prop('disabled',false);
											$('input[name="ApprovedTerm"]').prop('disabled',false);
											$('input[name="PurchasePrice"]').prop('disabled',false);
											$('input[name="RenovationCosts"]').prop('disabled',false);
											$('input[name="CompletionValue"]').prop('disabled',false);

											$('input[name="PreApproved"]').val('1');

										}else{

											$('input[name="ApprovedAmount"]').prop('disabled',true);
											$('input[name="ApprovedDate"]').prop('disabled',true);
											$('input[name="ApprovedTerm"]').prop('disabled',true);
											$('input[name="PurchasePrice"]').prop('disabled',true);
											$('input[name="RenovationCosts"]').prop('disabled',true);
											$('input[name="CompletionValue"]').prop('disabled',true);

											$('input[name="PreApproved"]').val('0');
										}
									}
								</script>

							</div>
							<div class="modal-footer">
								
							</div>

							<!---------------Do you want to save the info start------------------>
							<div class="modal fade" id="saveAgainpreapp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							  <div class="modal-dialog modal-sm" role="document">
							    <div class="modal-content">
							      <div class="modal-body">
							        
							        <h5><b>Do you want to save the information?</b></h5>
							        	
							      </div>
							      <div class="modal-footer">
							        <a type="button" href="<?php echo base_url();?>borrower_view/<?php echo $borrower_id;?>" class="btn blue">No</a>
							        <button type="submit" name="submitline" class="btn blue">Yes</button>
							      </div>
							    </div>
							  </div>
							</div>
							<!---------------Do you want to save the info end-------------------->

						</form>
					</div>
				</div>
			</div>
			<!--------------------LINE OF CREDIT END -------------->
			
			
						</div>
					</div>	


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>


$(document).ready(function() {
	 
    //$('#borrowertable').DataTable();
    $('#borrowertable').DataTable({
       // "paging":   false,
       // "ordering": false,
        //"info":     false,
        "searching": false,
        "bLengthChange": false,
        "aaSorting": false,
    } );
	
});



</script>
<script>


 // var particular_contact_idd  = '<?php echo isset($contact_idd) ? $contact_idd : '';?>';
 // alert('contact id is: ' + particular_contact_idd);
 
 // if(particular_contact_idd){
	 
	 // $('.contact_'+particular_contact_idd).css('display','none');
 // }
/*  var dd = '<?php echo $borrower_document_hit;?>';
 alert(dd); */
$( function() {
	var hit = '<?php echo $this->session->flashdata('borrower_document_hit');?>';
	if(hit == '1'){
		$('#borrower_document').modal('show');
	}
    $( ".cl_exiration_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
    $( "#income_to" ).datepicker({ dateFormat: 'mm-dd-yy' });
    $( "#income_from" ).datepicker({ dateFormat: 'mm-dd-yy' });
	 $( "#credit_report_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
	 $( "#dob" ).datepicker({ dateFormat: 'mm-dd-yy' });
	 $( "#c2_dob" ).datepicker({ dateFormat: 'mm-dd-yy' });
	 $( "#balancesheet_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
	 $( ".open_datepicker" ).datepicker({ dateFormat: 'mm-dd-yy' });
	// $(".dataTable").datatable();
    // $( "#loan_funding_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
    // $( "#loan_document_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
  } );
  
  function select_borrower_id(obj)
  {
	  // alert(id);
	  /*if($("#user_login_status").val()=="1"){
	  	var option = $('option:selected', obj).attr('status');
	  	if(option=="1"){
	  		$('#borrower_form').submit();
	  	}
	  }else{
	  	$('#borrower_form').submit();
	  }*/
	  
	  // window.location.href = "<?php //echo base_url().'borrower_data/'?>"+id;
  }
  
  function delete_borrower(id)
  {
	  var borrower_id = $('#borrower_id').val() ? $('#borrower_id').val() : id;
	  if(borrower_id){
		  
	  if(confirm('Are you sure to delete this Borrower?') === true)
	  {
		 window.location.href = "<?php echo base_url();?>delete_borrower/"+borrower_id;
	  }
	 
	  }else{
		  alert('Please select Borrower first!');
	  }
  }

	function formatDate(date) {
	     var d = new Date(date),
	         month = '' + (d.getMonth() + 1),
	         day = '' + d.getDate(),
	         year = d.getFullYear();

	     if (month.length < 2) month = '0' + month;
	     if (day.length < 2) day = '0' + day;

	     return [month, day, year].join('-');
	}

    function editPropertyApproval(rowid){

    	$('#PropertyApprovalView select#pro_state option[value=""]').attr('','');
    	$('#PropertyApprovalView select#reqt_status option[value=""]').attr('','');
    	$('#PropertyApprovalView input[name="pro_app_ids"]').val('');
    	$('#PropertyApprovalView input[name="pro_address"]').val('');
    	$('#PropertyApprovalView input[name="pro_unit"]').val('');
		$('#PropertyApprovalView input[name="pro_city"]').val('');
		$('#PropertyApprovalView input[name="pro_zip"]').val('');
		$('#PropertyApprovalView input[name="purchase_price"]').val('');
		$('#PropertyApprovalView input[name="rehab_cost"]').val('');
		$('#PropertyApprovalView input[name="completion_val"]').val('');
		$('#PropertyApprovalView input[name="ApprovalAmount"]').val('');
		$('#PropertyApprovalView input[name="LoanTerm"]').val('');
		$('#PropertyApprovalView input[name="pro_date"]').val('');

    	if(rowid == 'new'){

    		$('#PropertyApprovalView').modal('show');
    	}else{

    		$.ajax({

    				type : 'post',
    				url  : '<?php echo base_url()."Borrower_data/fetch_pro_app_data";?>',
    				data : {'rowid':rowid},
    				success : function(response){

    					var data = JSON.parse(response);
 
    					if(data[0].pro_date != 'null'){
    						var pro_date = formatDate(data[0].pro_date);
    						$('#PropertyApprovalView input[name="pro_date"]').val(pro_date);
    					}else{
    						$('#PropertyApprovalView input[name="pro_date"]').val('');
    					}

    					$('#PropertyApprovalView input[name="pro_app_ids"]').val(data[0].id);
    					$('#PropertyApprovalView input[name="pro_address"]').val(data[0].pro_address);
    					$('#PropertyApprovalView input[name="pro_unit"]').val(data[0].pro_unit);
    					$('#PropertyApprovalView input[name="pro_city"]').val(data[0].pro_city);
    					$('#PropertyApprovalView input[name="pro_zip"]').val(data[0].pro_zip);
    					$('#PropertyApprovalView input[name="purchase_price"]').val(data[0].purchase_price);
    					$('#PropertyApprovalView input[name="rehab_cost"]').val(data[0].rehab_cost);
    					$('#PropertyApprovalView input[name="completion_val"]').val(data[0].completion_val);
    					$('#PropertyApprovalView input[name="ApprovalAmount"]').val(data[0].ApprovalAmount);
    					$('#PropertyApprovalView input[name="LoanTerm"]').val(data[0].LoanTerm);

    					$('#PropertyApprovalView select#pro_state option[value="'+data[0].pro_state+'"]').attr('selected',true);
    					$('#PropertyApprovalView select#reqt_status option[value="'+data[0].req_status+'"]').attr('selected',true);
    					$('#PropertyApprovalView').modal('show');
    				}
    		})
    	}

    }
	
	function deletePropertyApproval(rowid){

		if(confirm('Are you sure to remove this property approval?')){

			$.ajax({

    				type : 'post',
    				url  : '<?php echo base_url()."Borrower_data/remove_pro_app_data";?>',
    				data : {'rowid':rowid},
    				success : function(response){

    					$('#PropertyApproval tr#row_'+rowid).remove();
    					//window.location.reload();
    				}
    		})
		}
	}
  
  // function fetch_contact_data(id)
  // {
	  // $.ajax({
		  // url : "<?php echo base_url();?>borrower_data/load_conact_data",
		  // type : "POST",
		  // data : { 'id' : id },
		  // success : function(response)
		  // {
			  // var data = JSON.parse(response);
			  // var name = data.fname;
			  // var lname = data.lname;
			  // var address = data.address;
			  // var unit = data.unit;
			  // var zip = data.zip;
			  // var city = data.city;
			  // var tax_id = data.tax_id;
			  // $("#borrower_name").val(name + ' ' + lname);
			  // $("#address").val(address);
			  // $("#unit").val(unit);
			  // $("#zip").val(zip);
			  // $("#city").val(city);
			  // $("#tax_id").val(tax_id);
			  
		  // }
	  // });
  // }
  
 // $('.phone').keyup(function(){

	 // var val = this.value.replace(/\D/g, '');
	 
        // var newVal = '';
        // while (val.length > 3) {
          // newVal += val.substr(0, 3) + '-';
          // val = val.substr(3);
        // }
        // newVal += val;
        // this.value = newVal;
// });

  $('.phone').keyup(function()
  {
	  var val = this.value.replace(/\D/g, '');
	 if(val.length < 10)
	 {
        var newVal = '';
        while (val.length > 3) {
          newVal += val.substr(0, 3) + '-';
          val = val.substr(3);
        }
        newVal += val;
        this.value = newVal;
	 }
	 else
	 {
     this.value = this.value.replace(/(\d{3})\-?(\d{3})\-?(\d{4})/,'$1-$2-$3');
	 }
    //alert ("OK");
  });
  
  $(document).ready(function(){
	  var contact_2 = '<?php echo isset($fetch_borrower_id_data[0]->c2_first_name) ? $fetch_borrower_id_data[0]->c2_first_name : '0'; ?>';
	  if(contact_2!= '' && contact_2!= '0' )
	  {
		  $('#c2_information').css('display','block');
		  $('#add_another_person').css('display','none');
	  }
  });
  function show_contact_2()
  {
	  $('#c2_information').slideDown('slow');
	  $('#add_another_person').css('display','none');
  }
  function show_contact_new()
  {
	  $('#c3_information_new').css('display','block');
	  $('#add_another_person1').css('display','none');
  }
  $(document).ready(function(){
	  var c_bk_declare = $('#c_bk_declare').val();
	  // alert('c_bk_declare'+c_bk_declare);
	  if(c_bk_declare == 0)
	  {
		  
		  $('#select_c_bk_dismis').css('display','none');
		  // $('#select_c_bk_dismis select').prop('disabled',true);
	  }
	  else
	  {
		 $('#select_c_bk_dismis').css('display','block');
		  // $('#select_c_bk_dismis select').prop('disabled',false); 
	  }
  });
  
  $(document).ready(function(){
	  var c2_bk_declare = $('#c2_bk_declare').val();
	  // alert('c2_bk_declare'+c2_bk_declare);
	  if(c2_bk_declare == 0)
	  {
		  $('#select_c2_bk_dismis').css('display','none');
		  // $('#select_c2_bk_dismis select').prop('disabled',true);
	  }
	  else
	  {
		 $('#select_c2_bk_dismis').css('display','block');
		  // $('#select_c2_bk_dismis select').prop('disabled',false); 
	  }
  });
  
  $('#c_bk_declare').change(function(){
	  var c_bk_declare = $('#c_bk_declare').val();
	  if(c_bk_declare == 0)
	  {
		  $('#select_c_bk_dismis').css('display','none');
		  // $('#select_c_bk_dismis select').prop('disabled',true);
	  }
	  else
	  {
		 $('#select_c_bk_dismis').css('display','block');
		  // $('#select_c_bk_dismis select').prop('disabled',false); 
	  }
  });
  $('#c2_bk_declare').change(function(){
	  var c2_bk_declare = $('#c2_bk_declare').val();
	  if(c2_bk_declare == 0)
	  {
		  $('#select_c2_bk_dismis').css('display','none');
		  // $('#select_c2_bk_dismis select').prop('disabled',true);
	  }
	  else
	  {
		 $('#select_c2_bk_dismis').css('display','block');
		  // $('#select_c2_bk_dismis select').prop('disabled',false); 
	  }
  });
  
  
  var i= 1;
  function add_more_row(){
	  i++;
	 
		 // $('#add_row').css("display","block");
	 if(i < 6){
		 
	  $( ".doc-div" ).append( '<br><div class="row"><div class="col-md-4"><input type="file" id = "upload_'+i+'" name ="upload_doc[]" class="select_images_file" onchange="change_this_image(this)" name="files[]"></div></div>' );
	 }else{
		 // $('#add_row').css("display","none");
		 // alert('no mpre row added');
	 }
	  
  }
   function display_inputfield(id){
		var filename = $('#filename_'+id).data('name');
		
		$('.new_filename_div'+id).toggle();
		$('#contact_document_id'+id).val(id);
		$('#old_filename'+id).val(filename);

	}

  
function delete_doc(mn){
	var name = doc_id = mn.id;
	//alert(doc_id);
	if(confirm('Are You Sure To Delete?')){
		$.ajax({
		type	:'POST',
		url		:'<?php echo base_url()."Borrower_data/borrrower_delete_document";?>',
		data	:{'id':doc_id},
			success : function(response){
				if(response == 1){
					$('table tr[attrremove="'+name+'"]').remove();
				}
			}
				
		});
	}
}


function check_filename_duplicacy(id,contact_id){
		
		var new_filename = $('#new_filename'+id).val();
		var old_filename = $('#old_filename'+id).val();
		
		var new_extension = new_filename.substr( (new_filename.lastIndexOf('.') +1) );
		var old_extension = old_filename.substr( (old_filename.lastIndexOf('.') +1) );
		switch(new_extension) {
			case 'pdf': 
				var filename =  new_filename;
				$('#new_filename'+id).val(filename);
				
			break;
			default:
				var filename = new_filename+'.'+old_extension;
				$('#new_filename'+id).val(filename);
		}
		
		//ajax call to check duplicacy of file name....
		$.ajax({
			type 	: 'post',
			url  	: '<?php echo base_url().'Borrower_data/check_borrower_contact_filename';?>',
			data	: { 'filename' : filename,'contact_id':contact_id},
			success : function(response){
				if(response == '1'){
					// duplicacy found...
					$('#new_filename'+id).val(' ').focus();
					alert('Filename: '  + filename + ' already exist for this Contact, Please try another filename!' );
					
				}
			}
			
		});
		
	}

  function balancesheet_dependent(a)
  {
	  if(a == 'Yes')
	  {
		 $('.balancesheet_dependent').css('display','block');
	  }
	  else if(a == 'No')
	  {
		  $('.balancesheet_dependent').css('display','none');
		  $('.balancesheet_dependent input').val('');

	  }
	  else
	  { 
	  
	  }
  }
  
  $(document).ready(function(){
	  var income_statement = '<?php echo isset($fetch_borrower_id_data[0]->in_balance_income_sheet) ? $fetch_borrower_id_data[0]->in_balance_income_sheet : '0'; ?>';
	  // alert(income_statement);
	  if(income_statement == 1)
	  {
		  $('.balancesheet_dependent').css('display','block');
	  }
	   else if(income_statement == 0)
	  {
		  $('.balancesheet_dependent').css('display','none');
		  $('.balancesheet_dependent input').val('');
	  }
	  else
	  { 
	  
	  }
  });    
  
  $('#event_borrower_type').change(function()
  {
		var borrower_type = $('#event_borrower_type').val();
		var btn_name 		= $('#btn_name').val();
		if(btn_name != 'view'){
		  
	 
		// alert(borrower_type);
			if(borrower_type == 1)
			{
				$('.contact_gurrantor_div select').attr('readonly',true);
				$('.contact_title').attr('readonly',true);
				$('.individual_dependent input').attr('readonly',true);
				$('.individual_dependent select').attr('disabled',true);
				$('#company_information_data input').attr('readonly',true);
				$('#company_information_data select').attr('disabled',true);
			}
			else
			{
			  $('.contact_title').attr('readonly',false);
			  $('.contact_gurrantor_div select').attr('readonly',false);
				
			  $('#company_information_data input').attr('readonly',false);
			  $('#company_information_data select').attr('disabled',false);

				// $('#company_information_data input').val('');
				$('.individual_dependent input').val('');
				$('.individual_dependent input').attr('readonly',false);
				$('.individual_dependent select').attr('disabled',false);
			}
		}
  });
  
  $(document).ready(function()
  {
		var borrower_type = $('#event_borrower_type').val();
		var btn_name 		= $('#btn_name').val();
		if(btn_name != 'view'){
		
		// alert(borrower_type);
		if(borrower_type == 1)
		{
			$('.contact_title').attr('readonly',true);
			$('.contact_gurrantor_div select').attr('readonly',true);
				
			$('.individual_dependent input').attr('readonly',true);
			$('.individual_dependent select').attr('disabled',true);
			$('#company_information_data input').attr('readonly',true);
			$('#company_information_data select').attr('disabled',true);
		}
		else
		{
			$('.contact_title').attr('readonly',false);
			$('.contact_gurrantor_div select').attr('readonly',false);
			
			$('#company_information_data input').attr('readonly',false);
			$('#company_information_data select').attr('disabled',false);

			// $('#company_information_data input').val('');
			$('.individual_dependent input').val('');
			$('.individual_dependent input').attr('readonly',false);
			$('.individual_dependent select').attr('disabled',false);
		}
	}
  });
  
  function autofill_data()
  {
	  var street = $('#address').val();
	  var unit = $('#unit').val();
	  var city = $('#city').val();
	  var state = $('#state').val();
	  var zip = $('#zip').val();
	  var tax_id = $('#tax_id').val();
	  
	  $('#c_street').val(street);
	  $('#c_unit').val(unit);
	  $('#c_city').val(city);
	  $('#c_state').val(state);
	  $('#c_zip').val(zip);
  }
  
  function auto_blank_contact()
  {
	  $('div.c1 input').val('');
	  $('div.c1 select').val('');
	  $('div.c1 textarea').val('');
  } 
  
  function auto_blank_contact_c2()
  {
	  $('div#c2_information input').val('');
	  $('div#c2_information select').val('');
	  $('div#c2_information textarea').val('');
  }
  
  function fetch_borrower_data(id){
	    if(id){
		  
	 
		var contact_type = $('#display_borrower_name').find('option:selected').attr('contacttype');
		// alert(contact_type);
		  $('#contact_type').val(contact_type);
	
		//ajax_call...
		$.ajax({
			type: "post",
			url: "<?php echo base_url()?>Borrower_data/fetch_borrower_details/",
			data:{'id':id,'contact_type':contact_type},
			success: function(response){
				if(response){
					var data  = JSON.parse(response);
					$('#display_borrower_email').val(data.email);
					$('#display_borrower_phone').val(data.phone);
					// $('#display_borrower_title').val(data.phone);
					$('#display_borrower_guarantor').val(data.guarantor);
					
				}
			}
		});
	}
  }
  
  function add_new_contact(){
	  // alert('gone');
	$('div#hidden_contact_information_div').css('display','block');
	var count = $('#formID h4.count_borrower_contact').length;
	// alert(count);
	// var new_count = count + 1;
	var new_count = count ;
	// $("div#hidden_contact_information_div div.new_contact_div h4.count_borrower_contact").text('Contact Name '+new_count);
	$("div#hidden_contact_information_div div.primary_contactdiv input.chkbx_primary_contact").prop('id','primary_contact_'+new_count);
	$("div#hidden_contact_information_div div.primary_contactdiv input.chkbx_primary_contact_val").prop('id','primary_contact_val'+new_count);
	$("div#hidden_contact_information_div div.new_contact_div").clone().insertAfter("div#contact_information_div div.new_contact_div:last");
	$('div#hidden_contact_information_div').css('display','none');
 }
 
 function fetch_contact_details(that)
 {
	 var contact_id = that.value;
	 $.ajax({
			type: "post",
			url: "<?php echo base_url()?>Ajax_call/fetch_contact_details",
			data:{'contact_id':contact_id},
			success: function(response){
				if(response){
					var data  = JSON.parse(response);
					// alert(data.phone);
					$(that).parent().parent().parent().find('input.contact_phone').val(data.phone);
					$(that).parent().parent().parent().find('input.contact_email').val(data.email);
					$(that).parent().parent().parent().find('input.contact_email').val(data.email);
					$(that).parent().parent().parent().find('input.contact_title').val(data.title);
					
				}
			}
	 });
	 
 }
/*  function display_search_div(){
	 $('.talimar_no_dropdowns').css('display','block');
 } */
 function hide_this_contact(that)
	{
		$(that).parent().parent().parent().css('display','none');
	}
	
	function delete_borrower_contact(id)
	{
		if(id){
			if(confirm('Are you sure to delete?') == true)
			{
				window.location.href = '<?php echo base_url();?>delete_borrower_contact/D'+id;
			}
		}
	}
	
function display_inputfield(id){
		var filename = $('#filename_'+id).data('name');
		
		$('.new_filename_div'+id).toggle();
		$('#borrower_document_id'+id).val(id);
		$('#old_filename'+id).val(filename);

}

function check_filename_duplicacy(id,borrower_id){
	
	var new_filename = $('#new_filename'+id).val();
	var old_filename = $('#old_filename'+id).val();
	
	var new_extension = new_filename.substr( (new_filename.lastIndexOf('.') +1) );
	var old_extension = old_filename.substr( (old_filename.lastIndexOf('.') +1) );
	switch(new_extension) {
        case 'pdf': 
			var filename =  new_filename;
			$('#new_filename'+id).val(filename);
			
		break;
		default:
			var filename = new_filename+'.'+old_extension;
			$('#new_filename'+id).val(filename);
    }
	
	//ajax call to check duplicacy of file name....
	$.ajax({
		type 	: 'post',
		url  	: '<?php echo base_url().'borrower_data/check_borrower_filename';?>',
		data	: { 'filename' : filename,'borrower_id':borrower_id},
		success : function(response){
			if(response == '1'){
				// duplicacy found...
				$('#new_filename'+id).val(' ').focus();
				alert('Filename: '  + filename + ' already exist for this Borrrower, Please try another filename!' );
				
			}
		}
		
	});
	
}
 function read_document(id){
	 
	 // alert('ddd');
	 $('#document_id').val(id);
	// // $('#documentfrm').submit();
	  window.location.href = "<?php echo base_url().'borrower_data/borrower_document_read/'?>"+id;
	  // window.open = ('<?php echo base_url().'borrower_data/borrower_document_read/'?>'+id, '_blank');
 }

function check_primary_contact(id){
	$('.chkbx_primary_contact').prop('checked',false);
	$('.chkbx_primary_contact_val').val('0');
	var rowcnt = id.split('_');
	console.log(rowcnt);
	if($('#'+id).prop('checked',true)){
		// alert('checked checkbox number : ' + rowcnt[2]);
		$('#primary_contact_val'+rowcnt[2]).val('1');
	}
} 

function fetch_borrower_id(){
	
	var borrower_id = $('#borrower_id').val();
	if(borrower_id){
		$('#btn_name').val('edit');
		// $('#borrower_form').submit();
		window.location.href = "<?php echo base_url().'borrower_data/edit/';?>"+borrower_id;
	}else{
		alert('Please select Borrower first!');
	}
	
}
var i= 1;
function borrower_document_add_more_row(){
	  i++;
	 
		 // $('#add_row').css("display","block");
	 if(i < 6){
		 
	  $( ".doc-div" ).append( '<br><div class="row"><div class="col-md-4"><input type="file" id = "borrower_upload_'+i+'" name ="borrower_upload_doc[]" class="select_images_file" onchange="change_this_image(this)" name="borrower_files[]"></div></div>' );
	 }else{
		 // $('#add_row').css("display","none");
		 // alert('no mpre row added');
	 }
	  
}

    //$(document).ready(function(){

        $('#uploaddpdf').submit(function(e){

        	//alert('lllll');
        	
			$.ajax({
                     type:"post",
                     url:'<?php echo base_url()."Borrower_data/update_borrower_documents";?>',
                     data:new FormData(this),
                     enctype: 'multipart/form-data',
                     processData:false,
                     contentType:false,
                     cache:false,
                     async:false,
                     success: function(response){
				
						alert('Document uploaded successfully.');
						$('div#borrower_document').modal('hide');
						window.location.reload();
					}
            }); 
	  
        });
     
   // });
$(document).ready(function(){
	$('body').on('click', '#ct_loan_saved_documentadd', function(){
		$(".ct_loan_saved_document").toggle('slow');
	});

});
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/borrower_view/borrower_view.js'); ?>"></script>