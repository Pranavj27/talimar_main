<?php
		$STATE_USA 					= $this->config->item('STATE_USA'); 
		$borrower_type_option 		= $this->config->item('borrower_type_option'); 
		$company_type_option 		= $this->config->item('company_type_option');
		$yes_no_option 				= $this->config->item('yes_no_option');
		$no_yes_option 				= $this->config->item('no_yes_option');
		$nno_yes_option 			= $this->config->item('nno_yes_option');
		$responsibility 			= $this->config->item('responsibility');
		$template_yes_no_option 	= $this->config->item('template_yes_no_option');
		$yes_no_option 				= $this->config->item('yes_no_option');
		/*By @aj 21-07-2021*/
		$sign_loan_doc_option		= $this->config->item('sign_loan_doc_option');
		$primary_contact_option		= $this->config->item('primary_contact_option');
		$borrower_portal_option		= $this->config->item('borrower_portal_option');
		/*End*/
		$paid_count 		= isset($fetch_paidoff_loan) ? $fetch_paidoff_loan['count_loan'] : 0;
		$paid_amount 		= isset($fetch_paidoff_loan) ? $fetch_paidoff_loan['total_loan_amount'] : 0;
		$account_status_option         		= $this->config->item('account_status_option');
		$active_count 		= isset($fetch_active_loan) ? $fetch_active_loan['count_loan'] : 0;
		$active_amount 		= isset($fetch_active_loan) ? $fetch_active_loan['total_loan_amount'] : 0;
		$pipeline_count 	= isset($fetch_pipeline_loan) ? $fetch_pipeline_loan['count_loan'] : 0;
		$pipeline_amount 	= isset($fetch_pipeline_loan) ? $fetch_pipeline_loan['total_loan_amount'] : 0;

		$borrower_id				= $borrower_idd ;
	
	if($btn_name == 'view'){
		$fields_disabled 	= 'readonly ';
		$class_disabled 	= 'disabled ';
	
	}else{
		$fields_disabled 	= ' ';
		$class_disabled 	= ' ';
				
	}
	
?>
<style>

input#balancesheet_date {
    padding: 10px !important;
}

.row.borrower_data_row label {
    
    font-weight: 600 !important;
}
.main_left_div{
	float:left;
	width:70%;
	border-right:1px solid #eee;
}
.main_right_div{
	float:right;
	width:30%;
	margin-top:60px;

}
.row.button_section_div {
    padding: 13px 30px !important;
}
#c2_information{
	display:none;
}
.active-class , no-class{
	display:block !important;
}
.inactive-class{
	display:none ;
}
.borrower_document_lists li{
	list-style: none;
	padding: 5px;
	font-size: 14px;
}
.page-head{
	border-bottom:1px solid #eee;
	border-bottom:1px solid #eee;
}

#uniform-primary_contact1{
	width: 20px !important;
}


.borrower_data_row{
	padding: 11px 0px !important;
}
div#bordered_div_section{
	margin:0 !important;
}
.bs-searchbox {
    width: 100% !important;
}
table td, th{
	border : none !important;
}


.borrower_data_row>h4 {
    background-color: #e8e8e8;
	height: 30px;
	line-height: 30px;

}
@media only screen and (min-width: 992px) and (max-width: 1199px) {
	
	
}

@media only screen and (min-width: 768px) and (max-width: 992px) {


}



@media (max-width: 767px) {
.main_left_div {
    float: left;
    width: 100% !important;
    border-right: 0px solid #eee !important;
}
.main_right_div {
    float: left;
    width: 100% !important;
}
.row.button_section_div {
    padding: 10px 0px !important;
    width: 100%;
	margin-left: 0px !important;
}
.row.borrower_data_row{
	float:left;
	width:100%;
}
.row.borrower_data_row.col-md-3{
	float:left;
	margin-left: -15px !important;
	margin-right: -15px !important;
}
.row.borrower_data_row.table-borrower table {
    margin-left: 0px !important;
}
}

@media (max-width: 469px) {
.page-header .page-header-top .top-menu {
    display: block;
    clear: none !important;
    margin-top: 0px !important;
}
.row.borrower_data_row.table-borrower table {
    margin-left: 0px !important;
}




</style>

<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
		
	<div class="container">
		<!--------------------MESSEGE SHOW-------------------------->
		<?php if($this->session->flashdata('error')!=''){  ?>
			<div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error');?></div><?php } ?>
		<?php if($this->session->flashdata('success')!=''){  ?>
			<div id='success'><i class='fa fa-thumbs-o-up'></i><?php echo $this->session->flashdata('success');?></div><?php } ?>
				 
		<!-------------------END OF MESSEGE-------------------------->
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Borrower Accounts<small></small></h1>
			</div>
			<?php
				
			if($btn_name == 'view'){
				$btn_class = 'style ="display:block;" ';
			
			}else{
				$btn_class = 'style ="display:none;" ';
						
			}
			?>
			
			<!-- END PAGE TITLE -->
			<!-- BEGIN PAGE TOOLBAR -->
				
			<!-- END PAGE TOOLBAR -->
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb hide">
			<li>
				<a href="#">Home</a><i class="fa fa-circle"></i>
			</li>
			<li class="active">
				Dashboard
			</li>
		</ul>
		
		<form method="POST" action = "<?php echo base_url();?>add_borrower_data" id="formID" class="lender-page">
		<!--<form method="POST" action = "<?php echo base_url();?>add_borrower_data" enctype="multipart/form-data" id="formID" class="lender-page">-->
					
			<input type="hidden" id = "borrower_id" value="<?php  echo $borrower_id; ?>" name="borrower_id" />
			<input type="hidden" id = "contact_idd" value="<?php  echo $contact_idd; ?>" name="contact_idd" />
			<div class = "main_left_div">
					
				<!---------- Action Buttons Starts --------------->			
				<div class="row button_section_div">
								
					<div class="form-group">
						<div class="">
							<?php
							if($btn_name == 'view'){
										
								$new_class = "style = 'display:block;' ";
								if($btn_name == 'new'){
									$new_class = "style = 'display:none;' ";
								}
							?>
										
							<?php if($user_role == '2'){ ?>
							<a  class="btn btn-primary borrower_save_button"  onclick="delete_borrower('<?= $borrower_id; ?>')">Delete</a>
							<?php } ?>
												
							<a  name = "button" class="btn btn-primary borrower_save_button" onclick = "fetch_borrower_id();" href = "javascript:void(0);" >Edit</a>
												
							<a <?php echo $new_class;?> id=""  href="<?php echo base_url().'borrower_data/new';?>" class="btn btn-primary borrower_save_button" value="add">New</a>

							<?php	}
											
							$new_class = "style = 'display:block;' ";
							if($btn_name == 'new'){
								$new_class = "style = 'display:none;' ";
											
							?>
							<button  type = "submit" name = "button" class="btn btn-primary borrower_save_button" value="save">Save</button>
							
							<a  class="btn btn-primary borrower_save_button" href = "<?php echo base_url().'borrower_view';?>" >Close</a>
								
							<?php	} if($btn_name == 'edit'){ ?>
									
							<button  type = "submit" name = "button" class="btn btn-primary borrower_save_button" value="save">Save</button>
							
							<!--<a  class="btn btn-primary borrower_save_button" href = "<?php echo base_url().'borrower_view';?>" >Close</a>-->
							
							
							<a class="btn btn-primary borrower_save_button" data-toggle="modal" href="#edit_b_contact">Close</a>
							
							<?php }?>
								
						</div>
					</div>
				</div>
				<!---------- Action Buttons Starts --------------->	

				
				<div class="row borrower_data_row">
					<div class="errorrr alert alert-danger" style="display:none;"></div>	
					<h4>Borrower Status</h4>
				</div>

				<div class="row borrower_data_row">
						<div class="col-md-3">
							<label>Account Status:</label>
							<select type="text" class="form-control" name="account_status" id="account_status">
							<?php
							foreach($account_status_option as $key => $row)
							{
								?>
								<option value="<?= $key;?>" <?php if($key == $fetch_borrower_id_data[0]->account_status){echo 'selected';}?>><?= $row;?></option>
								<?php
							}
							?>
							</select>
					</div>
				</div>

				<div class="row borrower_data_row">
					<h4>Borrower Information:</h4>
				</div>

				<div class="row borrower_data_row">

					<div class="col-md-3">
						<label>Primary Contact:</label>
								
						<select class="form-control" name="primary_cont" id="primary_cont">
							
							<option value="">Select One</option>
							<?php
							foreach($all_user as $key => $row)
							{
							?>
							<option value="<?php  echo $row->id;?>" <?php if(isset($fetch_borrower_id_data[0]->t_user_id)){ if($fetch_borrower_id_data[0]->t_user_id == $row->id){ echo 'selected'; }} ?>><?php  echo $row->fname.' '.$row->middle_name.' '.$row->lname; ?></option>
							<?php } ?>
					
						</select>
					</div>
					
					<div class="col-md-3">
						<?php
							$generateHash = substr(str_shuffle("0123456789"), 1, 7);
						?>
						<label>Borrower #:</label>
						<input type="text" name="borrower_hash" class="form-control" value="<?php echo isset($fetch_borrower_id_data[0]->borrower_hash) ? $fetch_borrower_id_data[0]->borrower_hash : $generateHash; ?>" readonly="readonly">
					</div>

					<div class="col-md-3">
						<label>Date Created:</label>
						<input type="text" name="bdate_created" class="form-control" value="<?php echo isset($fetch_borrower_id_data[0]->bdate_created) ? date('m-d-Y', strtotime($fetch_borrower_id_data[0]->bdate_created)) : date('m-d-Y'); ?>" readonly="readonly">
					</div>

				</div>
				<hr>
				<div class="row borrower_data_row">

					<div class="col-md-3">
						<label>Borrower Name:</label>
						<input <?php echo $fields_disabled;?> type="text" id="borrower_name" class="form-control" name="borrower_name" value="<?php echo isset($fetch_borrower_id_data[0]->b_name) ? $fetch_borrower_id_data[0]->b_name : ''; ?>" required/>
					</div>

					<div class="col-md-3">
						<label>Borrower Type:</label>
								
						<select <?php echo $class_disabled;?>  class="form-control" name = "borrower_type" id="event_borrower_type">
							<?php
							foreach($borrower_type_option as $key => $row)
							{
							?>
							<option value="<?= $key;?>" <?php if(isset($fetch_borrower_id_data[0]->borrower_type)){ if($fetch_borrower_id_data[0]->borrower_type == $key){ echo 'selected'; }} ?> ><?= $row?> </option>
							<?php } ?>
					
						</select>
					</div>

					<div class="col-md-3">
						<label> Borrower Tax ID:</label>
							<input type="text" class="form-control" name="tax_id" id='tax_id' value="<?php echo isset($fetch_borrower_id_data[0]->b_tax_id) ? $fetch_borrower_id_data[0]->b_tax_id : ''; ?>">
					</div>


				</div>
			
						
				<div class="row borrower_data_row" id="company_information_data" >
					<div class="col-md-12">
						<label>Legal Entity / Trust Name: *</label>
						<input  type="text" class="form-control"  <?php echo $class_disabled;?> name="company_name" value="<?php echo isset($fetch_borrower_id_data[0]->company_name) ? $fetch_borrower_id_data[0]->company_name : ''; ?>">
						<span>* Will appear as the company name / trust name on the signature block in the Loan Documents</span>
					</div>
				</div>
				
				<div class="row borrower_data_row" id="state_information_data">
				
					<div class="col-md-3">
					<label>Registered State:</label>
					<select type="text" class="form-control" name="c_registration_state" id="register_state">
						<option value="">Not Applicable</option>
						<?php 
						foreach($STATE_USA as $key => $row)
						{ ?>
						<option value="<?= $key; ?>" <?php if(isset($fetch_borrower_id_data[0]->c_reg_state)){ if($fetch_borrower_id_data[0]->c_reg_state == $key){ echo 'selected'; }} ?> ><?= $row;?></option>
						<?php } ?>
					</select>
					</div>
					<div class="col-md-3">
						<label>State Entity Number:</label>
						<input  type="text" class="form-control" id="state_entity_no" name="state_entity_no" value="<?php echo isset($fetch_borrower_id_data[0]->state_entity_no) ? $fetch_borrower_id_data[0]->state_entity_no : ''; ?>">
					</div>
				</div>

				<div class="row borrower_data_row" id="ira_information">

						<div class="col-md-3">
							<label>IRA Custodian Name:</label>
							<input type="text" class="form-control <?php echo $class_disabled;?>" name="ira_name" value="<?php echo isset($fetch_borrower_id_data[0]->ira_name) ? $fetch_borrower_id_data[0]->ira_name : ''; ?>" id="ira_name">
						</div>

						<div class="col-md-3">
							<label>IRA Custodian Account #:</label>
							<input type="text" class="form-control <?php echo $class_disabled;?>" name = "ira_account" value="<?php echo isset($fetch_borrower_id_data[0]->ira_account) ? $fetch_borrower_id_data[0]->ira_account : ''; ?>" id="ira_account">
						</div>

				</div>

				<div class="row borrower_data_row">
					<div class="col-md-6">
						<label>Street Address:</label>
						<input <?php echo $fields_disabled;?> type="text" class="form-control" name = "address" id="address" value="<?php echo isset($fetch_borrower_id_data[0]->b_address) ? $fetch_borrower_id_data[0]->b_address : ''; ?>">
					</div>
					
					<div class="col-md-3">
						<label>Unit #:</label>
						<input <?php echo $fields_disabled;?> type="text" class="form-control" name = "unit" id= "unit" value="<?php echo isset($fetch_borrower_id_data[0]->b_unit) ? $fetch_borrower_id_data[0]->b_unit : ''; ?>">
					</div>
				</div>
					
				<div class="row borrower_data_row">
					<div class="col-md-3">
						<label>City:</label>
						<input <?php echo $fields_disabled;?>  type="text" class="form-control" name = "city" id="city" value="<?php echo isset($fetch_borrower_id_data[0]->b_city) ? $fetch_borrower_id_data[0]->b_city : ''; ?>">
					</div>
						
					<div class="col-md-3">
						<label>State: </label>
						<select <?php echo $class_disabled;?> class="form-control" name = "state" id="state" >
							<option value=""></option>
							<?php 
							foreach($STATE_USA as $key => $row)
							{ ?>
							<option value="<?= $key; ?>" <?php if(isset($fetch_borrower_id_data[0]->b_state)){ if($fetch_borrower_id_data[0]->b_state == $key){ echo 'selected'; }} ?> ><?= $row;?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-3">
						<label>Zip:</label>
						<input <?php echo $fields_disabled;?> type="text" class="form-control number_only" name="zip" id="zip" value="<?php echo isset($fetch_borrower_id_data[0]->b_zip) ? $fetch_borrower_id_data[0]->b_zip : ''; ?>">
					</div>
						
				</div>
				
				<div class="row borrower_data_row">
					
					<div class="col-md-3">
						<label>Phone Number:</label>
						<input <?php echo $fields_disabled;?>  type="text" class="form-control phone-format" name="c_phone" id="phone" value="<?php echo isset($fetch_borrower_id_data[0]->c_phone) ? $fetch_borrower_id_data[0]->c_phone : ''; ?>">
					</div>
						
					<div class="col-md-3">
						<label>Email: </label>
						<input <?php echo $fields_disabled;?>  type="text" class="form-control" name = "c_email" id="email" value="<?php echo isset($fetch_borrower_id_data[0]->c_email) ? $fetch_borrower_id_data[0]->c_email : ''; ?>">
					</div>
					
				</div>
					
		

				<!------------------- Document review start -------------->
				<?php if($this->uri->segment(2) == 'edit'){ ?>
				<div id="">

					<div class="row borrower_data_row ">
						<h4>Document Review</h4>
					</div>

					<!------------------- llc_checkbox start -------------->

					<div id="llc_checkbox">
						<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>Articles of Organization:</label>
							</div>

							<div class="col-md-3">
								<input type="checkbox" onclick="llc_checkbox_received(this);" <?php if($fetch_borrower_id_data[0]->artical_received == 1){echo 'checked';}?>> Received 
								<input type="hidden" name="artical_received" id="llc_received" value="<?php echo isset($fetch_borrower_id_data[0]->artical_received) ? $fetch_borrower_id_data[0]->artical_received : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="llc_checkbox_approved(this);" <?php if($fetch_borrower_id_data[0]->artical_approved == 1){echo 'checked';}?>> Approved 
								<input type="hidden" name="artical_approved" id="llc_approved" value="<?php echo isset($fetch_borrower_id_data[0]->artical_approved) ? $fetch_borrower_id_data[0]->artical_approved : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="llc_checkbox_uploaded(this);" <?php if($fetch_borrower_id_data[0]->artical_uploaded == 1){echo 'checked';}?>> Uploaded
								<input type="hidden" name="artical_uploaded" id="llc_uploaded" value="<?php echo isset($fetch_borrower_id_data[0]->artical_uploaded) ? $fetch_borrower_id_data[0]->artical_uploaded : '';?>">
							</div>
						</div>

						<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>Operating Agreement:</label>
							</div>

							<div class="col-md-3">
								<input type="checkbox" onclick="llc_operating_checkbox_received(this);" <?php if($fetch_borrower_id_data[0]->operating_received == 1){echo 'checked';}?>> Received 
								<input type="hidden" name="operating_received" id="llc_operating_received" value="<?php echo isset($fetch_borrower_id_data[0]->operating_received) ? $fetch_borrower_id_data[0]->operating_received : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="llc_operating_checkbox_approved(this);" <?php if($fetch_borrower_id_data[0]->operating_approved == 1){echo 'checked';}?>> Approved 
								<input type="hidden" name="operating_approved" id="llc_operating_approved" value="<?php echo isset($fetch_borrower_id_data[0]->operating_approved) ? $fetch_borrower_id_data[0]->operating_approved : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="llc_operating_checkbox_uploaded(this);" <?php if($fetch_borrower_id_data[0]->operating_uploaded == 1){echo 'checked';}?>> Uploaded
								<input type="hidden" name="operating_uploaded" id="llc_operating_uploaded" value="<?php echo isset($fetch_borrower_id_data[0]->operating_uploaded) ? $fetch_borrower_id_data[0]->operating_uploaded : '';?>">
							</div>

						</div>
						<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>Tax ID # verified:</label>
							</div>

							<div class="col-md-3">
								<input type="checkbox" onclick="llc_texid_checkbox_received(this);" <?php if($fetch_borrower_id_data[0]->texid_received == 1){echo 'checked';}?>> Received 
								<input type="hidden" name="texid_received" id="llc_texid_received" value="<?php echo isset($fetch_borrower_id_data[0]->texid_received) ? $fetch_borrower_id_data[0]->texid_received : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="llc_texid_checkbox_approved(this);" <?php if($fetch_borrower_id_data[0]->texid_approved == 1){echo 'checked';}?>> Approved 
								<input type="hidden" name="texid_approved" id="llc_texid_approved" value="<?php echo isset($fetch_borrower_id_data[0]->texid_approved) ? $fetch_borrower_id_data[0]->texid_approved : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="llc_texid_checkbox_uploaded(this);" <?php if($fetch_borrower_id_data[0]->texid_uploaded == 1){echo 'checked';}?>> Uploaded
								<input type="hidden" name="texid_uploaded" id="llc_texid_uploaded" value="<?php echo isset($fetch_borrower_id_data[0]->texid_uploaded) ? $fetch_borrower_id_data[0]->texid_uploaded : '';?>">
							</div>
						</div>


						<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>Entity Standing Confirmed:</label>
							</div>

							<div class="col-md-3">
								<input type="checkbox" onclick="llc_es_checkbox_received(this);" <?php if($fetch_borrower_id_data[0]->es_received == 1){echo 'checked';}?>> Received 
								<input type="hidden" name="es_received" id="llc_es_received" value="<?php echo isset($fetch_borrower_id_data[0]->es_received) ? $fetch_borrower_id_data[0]->es_received : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="llc_es_checkbox_approved(this);" <?php if($fetch_borrower_id_data[0]->es_approved == 1){echo 'checked';}?>> Approved 
								<input type="hidden" name="es_approved" id="llc_es_approved" value="<?php echo isset($fetch_borrower_id_data[0]->es_approved) ? $fetch_borrower_id_data[0]->es_approved : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="llc_es_checkbox_uploaded(this);" <?php if($fetch_borrower_id_data[0]->es_uploaded == 1){echo 'checked';}?>> Uploaded
								<input type="hidden" name="es_uploaded" id="llc_es_uploaded" value="<?php echo isset($fetch_borrower_id_data[0]->es_uploaded) ? $fetch_borrower_id_data[0]->es_uploaded : '';?>">
							</div>
						</div>
					</div>




					<!------------------- corp_checkbox start -------------->

					<div id="corp_checkbox">
						<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>Articles of Incorporation:</label>
							</div>

							<div class="col-md-3">
								<input type="checkbox" onclick="corp_checkbox_received(this);" <?php if($fetch_borrower_id_data[0]->corpartical_received == 1){echo 'checked';}?>> Received 
								<input type="hidden" name="corpartical_received" id="llc_artical_received" value="<?php echo isset($fetch_borrower_id_data[0]->corpartical_received) ? $fetch_borrower_id_data[0]->corpartical_received : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="corp_checkbox_approved(this);" <?php if($fetch_borrower_id_data[0]->corpartical_approved == 1){echo 'checked';}?>> Approved 
								<input type="hidden" name="corpartical_approved" id="llc_artical_approved" value="<?php echo isset($fetch_borrower_id_data[0]->corpartical_approved) ? $fetch_borrower_id_data[0]->corpartical_approved : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="corp_checkbox_uploaded(this);" <?php if($fetch_borrower_id_data[0]->corpartical_uploaded == 1){echo 'checked';}?>> Uploaded
								<input type="hidden" name="corpartical_uploaded" id="llc_artical_uploaded" value="<?php echo isset($fetch_borrower_id_data[0]->corpartical_uploaded) ? $fetch_borrower_id_data[0]->corpartical_uploaded : '';?>">
							</div>
						</div>

						<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>Corporate Bylaws:</label>
							</div>

							<div class="col-md-3">
								<input type="checkbox" onclick="corp_corporate_received(this);" <?php if($fetch_borrower_id_data[0]->corporate_received == 1){echo 'checked';}?>> Received 
								<input type="hidden" name="corporate_received" id="llc_corporate_received" value="<?php echo isset($fetch_borrower_id_data[0]->corporate_received) ? $fetch_borrower_id_data[0]->corporate_received : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="corp_corporate_approved(this);" <?php if($fetch_borrower_id_data[0]->corporate_approved == 1){echo 'checked';}?>> Approved 
								<input type="hidden" name="corporate_approved" id="llc_corporate_approved" value="<?php echo isset($fetch_borrower_id_data[0]->corporate_approved) ? $fetch_borrower_id_data[0]->corporate_approved : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="corp_corporate_uploaded(this);" <?php if($fetch_borrower_id_data[0]->corporate_uploaded == 1){echo 'checked';}?>> Uploaded
								<input type="hidden" name="corporate_uploaded" id="llc_corporate_uploaded" value="<?php echo isset($fetch_borrower_id_data[0]->corporate_uploaded) ? $fetch_borrower_id_data[0]->corporate_uploaded : '';?>">
							</div>
						</div>
						<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>Tax ID # verified:</label>
							</div>

							<div class="col-md-3">
								<input type="checkbox" onclick="corp_texid_received(this);" <?php if($fetch_borrower_id_data[0]->corps_texid_received == 1){echo 'checked';}?>> Received 
								<input type="hidden" name="corps_texid_received" id="llc_corp_texid_received" value="<?php echo isset($fetch_borrower_id_data[0]->corps_texid_received) ? $fetch_borrower_id_data[0]->corps_texid_received : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="corp_texid_approved(this);" <?php if($fetch_borrower_id_data[0]->corps_texid_approved == 1){echo 'checked';}?>> Approved 
								<input type="hidden" name="corps_texid_approved" id="llc_corp_texid_approved" value="<?php echo isset($fetch_borrower_id_data[0]->corps_texid_approved) ? $fetch_borrower_id_data[0]->corps_texid_approved : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="corp_texid_uploaded(this);" <?php if($fetch_borrower_id_data[0]->corps_texid_uploaded == 1){echo 'checked';}?>> Uploaded
								<input type="hidden" name="corps_texid_uploaded" id="llc_corp_texid_uploaded" value="<?php echo isset($fetch_borrower_id_data[0]->corps_texid_uploaded) ? $fetch_borrower_id_data[0]->corps_texid_uploaded : '';?>">
							</div>
						</div>

						<div class="row borrower_data_row">
							<div class="col-md-3">
								<label> Entity Standing Confirmed:</label>
							</div>

							<div class="col-md-3">
								<input type="checkbox" onclick="corp_es_received(this);" <?php if($fetch_borrower_id_data[0]->corps_es_received == 1){echo 'checked';}?>> Received 
								<input type="hidden" name="corps_es_received" id="llc_corp_es_received" value="<?php echo isset($fetch_borrower_id_data[0]->corps_es_received) ? $fetch_borrower_id_data[0]->corps_es_received : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="corp_es_approved(this);" <?php if($fetch_borrower_id_data[0]->corps_es_approved == 1){echo 'checked';}?>> Approved 
								<input type="hidden" name="corps_es_approved" id="llc_corp_es_approved" value="<?php echo isset($fetch_borrower_id_data[0]->corps_es_approved) ? $fetch_borrower_id_data[0]->corps_es_approved : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="corp_es_uploaded(this);" <?php if($fetch_borrower_id_data[0]->corps_es_uploaded == 1){echo 'checked';}?>> Uploaded
								<input type="hidden" name="corps_es_uploaded" id="llc_corp_es_uploaded" value="<?php echo isset($fetch_borrower_id_data[0]->corps_es_uploaded) ? $fetch_borrower_id_data[0]->corps_es_uploaded : '';?>">
							</div>
						</div>
					</div>

					<!------------------- lp_checkbox start -------------->

					<div id="lp_checkbox">
						<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>Certificate of Partnership:</label>
							</div>

							<div class="col-md-3">
								<input type="checkbox" onclick="lp_certi_chck_received(this);" <?php if($fetch_borrower_id_data[0]->certi_received == 1){echo 'checked';}?>> Received 
								<input type="hidden" name="certi_received" id="lp_certi_received" value="<?php echo isset($fetch_borrower_id_data[0]->certi_received) ? $fetch_borrower_id_data[0]->certi_received : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="lp_certi_chck_approved(this);" <?php if($fetch_borrower_id_data[0]->certi_approved == 1){echo 'checked';}?>> Approved 
								<input type="hidden" name="certi_approved" id="lp_certi_approved" value="<?php echo isset($fetch_borrower_id_data[0]->certi_approved) ? $fetch_borrower_id_data[0]->certi_approved : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="lp_certi_chck_uploaded(this);" <?php if($fetch_borrower_id_data[0]->certi_uploaded == 1){echo 'checked';}?>> Uploaded
								<input type="hidden" name="certi_uploaded" id="lp_certi_uploaded" value="<?php echo isset($fetch_borrower_id_data[0]->certi_uploaded) ? $fetch_borrower_id_data[0]->certi_uploaded : '';?>">
							</div>
						</div>

						<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>Partnership Agreement:</label>
							</div>

							<div class="col-md-3">
								<input type="checkbox" onclick="lp_part_chck_received(this);" <?php if($fetch_borrower_id_data[0]->part_received == 1){echo 'checked';}?>> Received 
								<input type="hidden" name="part_received" id="lp_part_received" value="<?php echo isset($fetch_borrower_id_data[0]->part_received) ? $fetch_borrower_id_data[0]->part_received : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="lp_part_chck_approved(this);" <?php if($fetch_borrower_id_data[0]->part_approved == 1){echo 'checked';}?>> Approved 
								<input type="hidden" name="part_approved" id="lp_part_approved" value="<?php echo isset($fetch_borrower_id_data[0]->part_approved) ? $fetch_borrower_id_data[0]->part_approved : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="lp_part_chck_uploaded(this);" <?php if($fetch_borrower_id_data[0]->part_uploaded == 1){echo 'checked';}?>> Uploaded
								<input type="hidden" name="part_uploaded" id="lp_part_uploaded" value="<?php echo isset($fetch_borrower_id_data[0]->part_uploaded) ? $fetch_borrower_id_data[0]->part_uploaded : '';?>">
							</div>
						</div>
						<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>Tax ID # verified:</label>
							</div>

							<div class="col-md-3">
								<input type="checkbox" onclick="lp_texid_chck_received(this);" <?php if($fetch_borrower_id_data[0]->texidlp_received == 1){echo 'checked';}?>> Received 
								<input type="hidden" name="texidlp_received" id="lp_texid_received" value="<?php echo isset($fetch_borrower_id_data[0]->texidlp_received) ? $fetch_borrower_id_data[0]->texidlp_received : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="lp_texid_chck_approved(this);" <?php if($fetch_borrower_id_data[0]->texidlp_approved == 1){echo 'checked';}?>> Approved 
								<input type="hidden" name="texidlp_approved" id="lp_texid_approved" value="<?php echo isset($fetch_borrower_id_data[0]->texidlp_approved) ? $fetch_borrower_id_data[0]->texidlp_approved : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="lp_texid_chck_uploaded(this);" <?php if($fetch_borrower_id_data[0]->texidlp_uploaded == 1){echo 'checked';}?>> Uploaded
								<input type="hidden" name="texidlp_uploaded" id="lp_texid_uploaded" value="<?php echo isset($fetch_borrower_id_data[0]->texidlp_uploaded) ? $fetch_borrower_id_data[0]->texidlp_uploaded : '';?>">
							</div>
						</div>

							<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>Entity Standing Confirmed:</label>
							</div>

							<div class="col-md-3">
								<input type="checkbox" onclick="lp_es_chck_received(this);" <?php if($fetch_borrower_id_data[0]->eslp_received == 1){echo 'checked';}?>> Received 
								<input type="hidden" name="eslp_received" id="lp_es_received" value="<?php echo isset($fetch_borrower_id_data[0]->eslp_received) ? $fetch_borrower_id_data[0]->eslp_received : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="lp_es_chck_approved(this);" <?php if($fetch_borrower_id_data[0]->eslp_approved == 1){echo 'checked';}?>> Approved 
								<input type="hidden" name="eslp_approved" id="lp_es_approved" value="<?php echo isset($fetch_borrower_id_data[0]->eslp_approved) ? $fetch_borrower_id_data[0]->eslp_approved : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="lp_es_chck_uploaded(this);" <?php if($fetch_borrower_id_data[0]->eslp_uploaded == 1){echo 'checked';}?>> Uploaded
								<input type="hidden" name="eslp_uploaded" id="lp_es_uploaded" value="<?php echo isset($fetch_borrower_id_data[0]->eslp_uploaded) ? $fetch_borrower_id_data[0]->eslp_uploaded : '';?>">
							</div>
						</div>
					</div>


					<!------------------- trust_checkbox start -------------->

					<div id="trust_checkbox">
						<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>Trust Agreement:</label>
							</div>

							<div class="col-md-3">
								<input type="checkbox" onclick="trust_chck_received(this);" <?php if($fetch_borrower_id_data[0]->trusta_received == 1){echo 'checked';}?>> Received 
								<input type="hidden" name="trusta_received" id="trust_received" value="<?php echo isset($fetch_borrower_id_data[0]->trusta_received) ? $fetch_borrower_id_data[0]->trusta_received : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="trust_chck_approved(this);" <?php if($fetch_borrower_id_data[0]->trusta_approved == 1){echo 'checked';}?>> Approved 
								<input type="hidden" name="trusta_approved" id="trust_approved" value="<?php echo isset($fetch_borrower_id_data[0]->trusta_approved) ? $fetch_borrower_id_data[0]->trusta_approved : '';?>">
							</div>
							<div class="col-md-3">
								<input type="checkbox" onclick="trust_chck_uploaded(this);" <?php if($fetch_borrower_id_data[0]->trusta_uploaded == 1){echo 'checked';}?>> Uploaded
								<input type="hidden" name="trusta_uploaded" id="trust_uploaded" value="<?php echo isset($fetch_borrower_id_data[0]->trusta_uploaded) ? $fetch_borrower_id_data[0]->trusta_uploaded : '';?>">
							</div>
						</div>
						
					</div>

					<!------------------- other, Self-Directed IRA, Individuals checkbox start -------------->

					<div id="no_checkbox">
						<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>Not Applicable </label>
							</div>
						</div>
						
					</div>

				</div>

				<?php } ?>

				<!------------------- Document review end -------------->
					
					
				<div id="">
					<div class="row borrower_data_row ">
						<h4>Borrower Financial Data</h4>
					</div>
					<div class="row ">
							<div class="col-md-6">
								<label class="lable_border_section" >Is the Balance Sheet / Income Statement available?</label>
							</div>	
							<div class="col-md-6 radio_buton_div"> 
									
								<input  <?php echo $class_disabled;?> type="radio" name="income_statement"  value="1" <?php if(isset($fetch_borrower_id_data[0]->in_balance_income_sheet)){ if($fetch_borrower_id_data[0]->in_balance_income_sheet == '1'){ echo 'checked'; }} ?> onchange="balancesheet_dependent('Yes')">
								Yes
										
								<input  <?php echo $class_disabled;?> type="radio" name="income_statement"  value="0" <?php if(isset($fetch_borrower_id_data[0]->in_balance_income_sheet)){ if($fetch_borrower_id_data[0]->in_balance_income_sheet == '0'){ echo 'checked'; }} ?> onchange="balancesheet_dependent('No')" >
								No
										
							</div>
						
					</div>

					<div class = 'row  balancesheet_dependent'>
							<div class="col-md-6">
								<label class="lable_border_section">Balance Sheet Date: </label> 
							</div>	
							<div class="col-md-1">
								<label style="padding-left:6px">From: </label>
							</div> 

							<div class="col-md-2">
								<input type="textbox" <?php echo $fields_disabled;?> id="balancesheet_date" name="balancesheet_date"  class="form-control" value="<?php echo isset($fetch_borrower_id_data[0]->balancesheet_date) ? $fetch_borrower_id_data[0]->balancesheet_date : ''; ?>" >
									 <!-- <span class="help-block">help</span> --> 
							</div>
							<div class="col-md-1">
								<label>To:</label>
							</div>
							<div class="col-md-2">	
								<input type="textbox" class="form-control datepicker" value="<?php echo isset($fetch_borrower_id_data[0]->balancesheet_to) ? $fetch_borrower_id_data[0]->balancesheet_to : ''; ?>" <?php echo $fields_disabled;?> name="balancesheet_to">
							</div>
						
					</div>
								
					<div class='row  balancesheet_dependent' style="margin-top: 10px;"> 
						<div class="col-md-6">
							<label class="lable_border_section">Income Statement Date: </label>
						</div>
						
						<div class="col-md-1">
							<input  <?php echo $fields_disabled;?> type="hidden" class="working" name="income_sta" value="<?php echo isset($fetch_borrower_id_data[0]->income_statement) ? $fetch_borrower_id_data[0]->income_statement : ''; ?>">
							
							<label style="padding-left:6px;">From: </label>
						</div>
						<div class="col-md-2">	
							<input type = "textbox"    class="form-control working date-picker" name="income_from" id= "income_from" value="<?php echo isset($fetch_borrower_id_data[0]->income_s_from) ? $fetch_borrower_id_data[0]->income_s_from : ''; ?> <?php echo $fields_disabled;?>">
						</div>
						<div class="col-md-1">	
							<label>To:</label>
						</div>
						<div class="col-md-2">	
							<input type = "textbox" class="form-control working"   id="income_to" value="<?php echo isset($fetch_borrower_id_data[0]->income_s_to) ? $fetch_borrower_id_data[0]->income_s_to : ''; ?>" <?php echo $fields_disabled;?> name="income_to">
						</div>
					</div>
					
					<div class="row  balancesheet_dependent">
							<div class="col-md-6">
								<label class="lable_border_section" >Has the Financial Statement been audited by a CPA / PA </label>
							</div>
							<div class="col-md-6 radio_buton_div" id="financial_s_radio"> 
								<input  <?php echo $class_disabled;?> type="radio" name="cpa_pa"  value="1" <?php if(isset($fetch_borrower_id_data[0]->cpa_pa)){ if($fetch_borrower_id_data[0]->cpa_pa == '1'){ echo 'checked'; }} ?> >
								Yes
										
								<input <?php echo $class_disabled;?> type="radio" name="cpa_pa"  value="0" <?php if(isset($fetch_borrower_id_data[0]->cpa_pa)){ if($fetch_borrower_id_data[0]->cpa_pa == '0'){ echo 'checked'; }} ?> >
								No
										
							</div>
						
					</div>
								
				</div>	
								
				<div class="row ">
						<div class="col-md-6">
							<label class="lable_border_section" >Additional Information is included on the Attached Addendum </label>
						</div>	
						<div class="col-md-6 radio_buton_div">  
							<input <?php echo $class_disabled;?> type="radio" name="attached_adden"  value="1" <?php if(isset($fetch_borrower_id_data[0]->attached_addedeum)){ if($fetch_borrower_id_data[0]->attached_addedeum == '1'){ echo 'checked'; }} ?> >
							Yes
							<input <?php echo $class_disabled;?> type="radio" name="attached_adden"  value="0" <?php if(isset($fetch_borrower_id_data[0]->attached_addedeum)){ if($fetch_borrower_id_data[0]->attached_addedeum == '0'){ echo 'checked'; }} ?> >
							No
						</div>
					
				</div>

					
				<div id="contact_information_div">
					<div class="row borrower_data_row">
						<h4>Borrower Contacts</h4> 
					</div>	
					
						<?php
						//$count_contact = 0;
								
						if(isset($borrower_contact_data))
						{	
							$count = 0;
							foreach($borrower_contact_data as $key => $cdata){
								$count++;
							?>
								<div class="existing_contact ">
									<?php
									$count_contact = $key + 1;
										if($contact_idd){
											if( $cdata->contact_id == $contact_idd){
												$contact_class = "active-class";
											}else {
												$contact_class = "inactive-class";
											}
										}	else{
											$contact_class = "no-class";
										}
									
										$contact = 0;
										if($count_contact == '1'){
											
											$contact = 'Primary Contact:';
										}else{
											$contact = 'Secondary Contact:';
										}
										
									?>		
									
									<div class="row borrower_data_row">
									
										
										<div class="col-md-3">
											<label>Contact Name: </label>
										<?php if($this->session->userdata('user_role') == '2'){ ?>	
											<a title="Delete" onclick="delete_this_b_contact(<?php echo $cdata->id;?>,<?php echo $fetch_borrower_id_data[0]->id;?>);"style="color:#f9022e;"><i class="fa fa-trash" aria-hidden="true"></i></a>
										<?php } ?>	
											<input  <?php echo $fields_disabled;?>type = "hidden" name = "borrower_contact_id" value = "<?php echo $cdata->id;?>">
												
											<select <?php echo $class_disabled;?><?php echo $fields_disabled;?> class="chosen" name="contact_contact_id[]" onchange="fetch_contact_details(this)">
												<option value="">Select One</option>
												<option value="">None</option>
													<?php
													$contactEmailId="";	
													$contactPhoneNum="";	
													foreach($fetch_all_contact as $row)
													{
														$selected="";
														if($row->contact_id == $cdata->contact_id){
															$selected="selected";
															$contactEmailId=$row->contact_email;
															$contactPhoneNum=$row->contact_phone;
														}
													?>
												<option value="<?php echo $row->contact_id; ?>" <?php echo $selected;  ?> ><?php echo $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname; ?> </option>';
												<?php
												}
												?>
											</select>
										</div>

										<div class="col-md-3">
											<label>Title:</label>
											<input  type = "text" <?php echo $class_disabled;?>  class="form-control contact_title" name="contact_title[]" value="<?php echo $cdata->c_title; ?>" >
										</div>
											

										<div class="col-md-3">
											<label>Sign Loan Documents:</label>
											<select  <?php echo $class_disabled;?>  name = "contact_responsibility[]" class = "form-control">
												<?php foreach($sign_loan_doc_option as $key => $optn){
												if($key == $cdata->contact_responsibility){
													$selected = 'selected';
												}else{
													$selected = '';
												}
												?>
												<option <?php echo $selected;?> value = "<?php echo $key;?>"><?php echo $optn;?></option>
												<?php } ?>
											</select>
										</div>

										<div class="col-md-3">
											<label>Borrower Portal Access:</label>
											<select  <?php echo $class_disabled;?>  name="borrower_portal_access[]" class="form-control">
												<?php foreach($borrower_portal_option as $key => $optn){
												if($key == $cdata->borrower_portal_access){
													$selected = 'selected';
												}else{
													$selected = '';
												}
												?>
												<option <?php echo $selected;?> value = "<?php echo $key;?>"><?php echo $optn;?></option>
												<?php } ?>
											</select>
										</div>
									</div>
										
									<div class="row borrower_data_row">
																							
										<div class="col-md-3">
											<label>Phone:</label>
											<input  <?php echo $fields_disabled;?> class="form-control contact_phone" name="contact_phone[]" value="<?php echo $contactPhoneNum; ?>" readonly>
										</div>
											
										<div class="col-md-3"> 
											<label>Email:</label>
											<input <?php echo $fields_disabled;?> class="form-control contact_email" name="contact_email[]" value="<?php echo $contactEmailId; ?>" readonly>
										</div>

										<?php
														//print_r($cdata->contact_primary);
														?>	
										<div class="col-md-3 " >
											<label>Primary Contact:</label>
											<select name = "primary_contact[]" class = "form-control">
												<?php foreach($primary_contact_option as $key => $optn){
													echo "key".$cdata->contact_primary;
													$selected = '';
												if($cdata->contact_primary!="" && $key == $cdata->contact_primary){
													$selected = 'selected';
												}
												?>
												<option <?php echo $selected;?> value = "<?php echo $key;?>"><?php echo $optn;?></option>
												<?php } ?>
											</select>
										</div>
								</div>
								
						<?php

							}							
						}else{ 

								$uri_segment_edit = $this->uri->segment(2);
								if($uri_segment_edit == 'edit'){

							?>

									<div class="single_new_contact ">
										<label><strong></strong></label>
										<div class="row borrower_data_row">
											<div class="col-md-3">
												<label>Contact Name:</label>
												
												<input <?php echo $fields_disabled;?>  type = "hidden" name = "borrower_contact_id" value = "">
												
												<select  <?php echo $class_disabled;?>  class="chosen" name="contact_contact_id[]" onchange="fetch_contact_details(this)">
													<option value="">Select One</option>
													<option value="">None</option>
													<?php
													foreach($fetch_all_contact as $row)
													{
														echo '<option value="'.$row->contact_id.'" >'.$row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname.'</option>';
													}
													?>
												</select>
											</div>
													
											<div class="col-md-3">
												<label>Title:</label>
												<input type = "text" name="contact_title[]"   class="form-control contact_title "  <?php echo $class_disabled;?>  />
											</div>	

											<div class="col-md-3">
												<label>Sign Loan Documents:</label>
												<select  <?php echo $class_disabled;?>  name = "contact_responsibility[]" class = "form-control">
													<?php foreach($sign_loan_doc_option as $key => $optn){
													?>
													<option  value = "<?php echo $key;?>"><?php echo $optn;?></option>
													<?php } ?>
												</select>
											</div>

											<div class="col-md-3">
												<label>Borrower Portal Access:</label>
												<select  <?php echo $class_disabled;?>  name = "borrower_portal_access[]" class = "form-control">
													<?php foreach($borrower_portal_option as $key => $optn){
													?>
													<option  value = "<?php echo $key;?>"><?php echo $optn;?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										
										<div class="row borrower_data_row">
											<div class="col-md-3">
												<label>Phone:</label>
												<input  type = "textbox" <?php echo $fields_disabled;?> class="form-control contact_phone" name="contact_phone[]"  readonly>
											</div>
											
											<div class="col-md-3"> 
												<label>Email:</label>
												<input type = "textbox" <?php echo $fields_disabled;?>  class="form-control contact_email" name="contact_email[]" readonly>
											</div>
											<div class="col-md-3 " >
												<label>Primary Contact:</label>
												<select name = "primary_contact[]" class = "form-control">
													<?php foreach($primary_contact_option as $key => $optn){ ?>
													<option value = "<?php echo $key;?>"><?php echo $optn;?></option>
													<?php } ?>
												</select>
											</div>
										</div>
									</div>

						<?php } } ?> <!------- End Edit uri section -------->

					<?php 

					$uri_segment = $this->uri->segment(2);
					//echo $uri_segment;
					if($uri_segment == 'new'){
					?>
						
						<div class="single_new_contact ">
							<label><strong></strong></label>
							<div class="row borrower_data_row">
								<div class="col-md-3">
									<label>Contact Name:</label>
									
									<input <?php echo $fields_disabled;?>  type = "hidden" name = "borrower_contact_id" value = "">
									
									<select  <?php echo $class_disabled;?>  class="chosen" name="contact_contact_id[]" onchange="fetch_contact_details(this)">
										<option value="">Select One</option>
										<option value="">None</option>
										<?php
										foreach($fetch_all_contact as $row)
										{
											echo '<option value="'.$row->contact_id.'" >'.$row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname.'</option>';
										}
										?>
									</select>
								</div>
										
								<div class="col-md-3">
									<label>Title:</label>
									<input type = "text" name="contact_title[]"   class="form-control contact_title "  <?php echo $class_disabled;?>  />
								</div>	

								<div class="col-md-3">
									<label>Sign Loan Documents:</label>
									<select  <?php echo $class_disabled;?>  name = "contact_responsibility[]" class = "form-control">
										<?php foreach($sign_loan_doc_option as $key => $optn){
										?>
										<option  value = "<?php echo $key;?>"><?php echo $optn;?></option>
										<?php } ?>
									</select>
								</div>
								<div class="col-md-3">
									<label>Borrower Portal Access:</label>
									<select  <?php echo $class_disabled;?>  name = "borrower_portal_access[]" class = "form-control">
										<?php foreach($borrower_portal_option as $key => $optn){
										?>
										<option  value = "<?php echo $key;?>"><?php echo $optn;?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							
							<div class="row borrower_data_row">
								
								
								
								<div class="col-md-3">
									<label>Phone:</label>
									<input  type = "textbox" <?php echo $fields_disabled;?> class="form-control contact_phone" name="contact_phone[]"  readonly>
								</div>
								
								<div class="col-md-3"> 
									<label>Email:</label>
									<input type = "textbox" <?php echo $fields_disabled;?>  class="form-control contact_email" name="contact_email[]" readonly>
								</div>
								<div class="col-md-3 " >
									<label>Primary Contact:</label>
									<select name = "primary_contact[]" class = "form-control">
										<?php foreach($primary_contact_option as $key => $optn){ ?>
										<option value = "<?php echo $key;?>"><?php echo $optn;?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						
					<?php } ?>	

						<div id="hidden_contact_information_div" style="display:none;">
						<div class="new_contact_div">
						<!--	<label><strong>Borrower Contact:</strong></label>-->
							<div class="row borrower_data_row">
								<div class="col-md-3">
									<label>Contact Name:</label>
									<input type = "hidden" name = "borrower_contact_id" value = "">
									<a onclick="hide_this_contact(this)"><i class="fa fa-trash" aria-hidden="true"></i></a>
									
									<select  <?php echo $class_disabled;?>  class="chosen" name="contact_contact_id[]" onchange="fetch_contact_details(this)">
										<option value="">Select One</option>
										<option value="">None</option>
										<?php
										
										foreach($fetch_all_contact as $row)
										{
											
											echo '<option value='.$row->contact_id .'>'.$row->contact_firstname .' '.$row->contact_middlename .' '.$row->contact_lastname .'</option>';
										}
										?>
									</select>
								</div>

								<div class="col-md-3">
									<label>Title:</label>
									<input  type = "textbox" <?php echo $fields_disabled;?> class="form-control contact_title" <?php echo $class_disabled;?> name="contact_title[]" >
								</div>
														
								<div class="col-md-3">
									<label>Sign Documents:</label>
									<select  <?php echo $class_disabled;?>  name = "contact_responsibility[]" class = "form-control">
										<?php foreach($nno_yes_option as $key => $optn){
										?>
										<option  value = "<?php echo $key;?>"><?php echo $optn;?></option>
										<?php } ?>
									</select>
								</div>
								<div class="col-md-3">
									<label>Borrower Portal Access:</label>
									<select  <?php echo $class_disabled;?>  name = "borrower_portal_access[]" class = "form-control">
										<?php foreach($borrower_portal_option as $key => $optn){
										?>
										<option  value = "<?php echo $key;?>"><?php echo $optn;?></option>
										<?php } ?>
									</select>
								</div>
								
								
							</div>
							<div class="row borrower_data_row">
								
								
								<div class="col-md-3">
									<label>Phone:</label>
									<input  type = "textbox" <?php echo $fields_disabled;?> class="form-control contact_phone" name="contact_phone[]"  readonly>
								</div>
								<div class="col-md-3"> 
									<label>Email:</label>
									<input type = "textbox" <?php echo $fields_disabled;?> class="form-control contact_email" name="contact_email[]" readonly>
								</div>
								<div class="col-md-3 " >
									<label>Primary Contact:</label>
									<select name = "primary_contact[]" class = "form-control">
										<?php foreach($primary_contact_option as $key => $optn){ ?>
										<option value = "<?php echo $key;?>"><?php echo $optn;?></option>
										<?php } ?>
									</select>
								</div>
							</div>
									
						</div>	
				</div>		
					<div class="row borrower_data_row">
						<div class="col-md-3 pull-left">
							<button class="btn blue" type="button" onclick="add_new_contact()">(+) Add Contact</button>
						</div>
					</div>

			</div>		
		</div>
	
			<!---------- main right div Ends--------------->			
				
		<div class = "main_right_div">
			
				<?php if($btn_name == 'view'){  ?>
						
					<!----------------- Borrower History Div starts--------->
					<div class="row borrower_data_row table-borrower">
						<h4>Borrower History:</h4>
						<table class="table ">
							<tbody>
								<tr>
									<td style="width:33.3%;"># of Active Loans</td>
									<td style="width:33.3%;"><?php echo $active_count; ?></td>
											
								</tr>
								<tr>
									<td style="width:33.3%;">$ of Active Loans</td>
									<td style="width:33.3%;">$<?php echo number_format($active_amount);?></td>
								</tr>
								<tr>
									<td># of Paid Off Loans</td>
									<td><?php echo $paid_count; ?></td>
								</tr>
								<tr>
									<td>$ of Paid Off Loans</td>
									<td>$<?php echo number_format($paid_amount); ?></td>
								</tr>
										
								<tr>
									<td># of Pipeline Loans</td>
									<td><?php echo $pipeline_count; ?></td>
								</tr>
								<tr>
									<td>$ of Pipeline Loans</td>
									<td>$<?php echo number_format($pipeline_amount); ?></td>
								</tr>	
								<tr>
									<td># of Total Loans</td>
									<td><?php echo $paid_count + $active_count + $pipeline_count; ?></td>
								</tr>
								<tr>
									<td>$ of Total Loans</td>
									<td>$<?php echo number_format($paid_amount + $active_amount + $pipeline_amount); ?></td>
								</tr>	
							</tbody>
						</table>
						<hr>
					</div>
					<!----------------- Borrower History Div Ends--------->
				<?php } ?>	
			</div>
					
			<!----------------- main right div Ends--------->
				<div class="modal fade" id="edit_b_contact" tabindex="-1" role="basic" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							
							<div class="modal-body">
								<div class="row" style="padding-top:8px;">
							
									<div class="col-md-8" style="padding-top:6px;"><p>Do you want to Save Information?</p></div>
									<div class="col-md-2">
										<!--<a style="margin-left:38px;" class="btn blue borrower_save_button" href="javascript:void(0);" onclick = "save_info('edit_contact')">Yes</a>-->
										
										<button style="margin-left:38px;" type = "submit" name = "button" class="btn blue borrower_save_button" value="save">Yes</button>
									</div>
									<div class="col-md-2">
										<a class="btn default borrower_save_button" href = "<?php echo base_url().'borrower_view';?>" >No</a>
									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

		</form>	
	</div>
</div>	

	
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>

<style>
.chosen-container.chosen-container-single {
    width: 200px !important;
}
div#contact_id_chosen {
    width: 200px !important;
}
.chosen-container-single .chosen-single {
    padding: 4px 0px 26px 6px !important;
    border-radius: 4px !important;
    border: 1px solid #e1e1e1 !important;
}
</style>

<script>

$(".chosen").chosen();


	function save_info(popup){
		alert(popup);
		 if(popup == 'edit_contact'){
			
			alert(popup);
			$('#borrower_action').val('close');
			$('#formID').submit();
		
		} 
	}


	function display_guarantor_data(that){

		var contact_id = that.value;
		window.location.href = '<?php echo base_url()."viewcontact/"?>'+contact_id;
	}


	function add_additioanal(vc){

	$(".cla").append('<div class="row borrower_data_row"><div class="a_error" style="display:none;"></div><div class="col-md-3" style="width:25%;"><label>Additional Guarantor:</label><input type="hidden" name="iddd[]" value="new"><select class="form-control" name="con_id[]" onchange="additional_guarantee_insert(this)"><option value="">Select One</option><?php foreach($fetch_all_contact as $rows) { ?>
					<option value="<?php echo $rows->contact_id;?>"><?php echo str_replace("'", "", $rows->contact_firstname).' '.$rows->contact_middlename.' '.str_replace("'","",$rows->contact_lastname);?></option><?php } ?></select></div></div>');

	}



	function llc_checkbox_received(that){

		if($(that).is(':checked')){
			$('input#llc_received').val('1');
		}else{
			$('input#llc_received').val('0');
		}
	}
	function llc_checkbox_approved(that){

		if($(that).is(':checked')){
			$('input#llc_approved').val('1');
		}else{
			$('input#llc_approved').val('0');
		}
	}
	function llc_checkbox_uploaded(that){

		if($(that).is(':checked')){
			$('input#llc_uploaded').val('1');
		}else{
			$('input#llc_uploaded').val('0');
		}
	}



	function llc_operating_checkbox_received(that){

		if($(that).is(':checked')){
			$('input#llc_operating_received').val('1');
		}else{
			$('input#llc_operating_received').val('0');
		}
	}
	function llc_operating_checkbox_approved(that){

		if($(that).is(':checked')){
			$('input#llc_operating_approved').val('1');
		}else{
			$('input#llc_operating_approved').val('0');
		}
	}
	function llc_operating_checkbox_uploaded(that){

		if($(that).is(':checked')){
			$('input#llc_operating_uploaded').val('1');
		}else{
			$('input#llc_operating_uploaded').val('0');
		}
	}



	function llc_texid_checkbox_received(that){

		if($(that).is(':checked')){
			$('input#llc_texid_received').val('1');
		}else{
			$('input#llc_texid_received').val('0');
		}
	}
	function llc_texid_checkbox_approved(that){

		if($(that).is(':checked')){
			$('input#llc_texid_approved').val('1');
		}else{
			$('input#llc_texid_approved').val('0');
		}
	}
	function llc_texid_checkbox_uploaded(that){

		if($(that).is(':checked')){
			$('input#llc_texid_uploaded').val('1');
		}else{
			$('input#llc_texid_uploaded').val('0');
		}
	}

	function llc_es_checkbox_received(that){

		if($(that).is(':checked')){
			$('input#llc_es_received').val('1');
		}else{
			$('input#llc_es_received').val('0');
		}
	}
	function llc_es_checkbox_approved(that){

		if($(that).is(':checked')){
			$('input#llc_es_approved').val('1');
		}else{
			$('input#llc_es_approved').val('0');
		}
	}
	function llc_es_checkbox_uploaded(that){

		if($(that).is(':checked')){
			$('input#llc_es_uploaded').val('1');
		}else{
			$('input#llc_es_uploaded').val('0');
		}
	}

	function corp_checkbox_received(that){

		if($(that).is(':checked')){
			$('input#llc_artical_received').val('1');
		}else{
			$('input#llc_artical_received').val('0');
		}
	}
	function corp_checkbox_approved(that){

		if($(that).is(':checked')){
			$('input#llc_artical_approved').val('1');
		}else{
			$('input#llc_artical_approved').val('0');
		}
	}
	function corp_checkbox_uploaded(that){

		if($(that).is(':checked')){
			$('input#llc_artical_uploaded').val('1');
		}else{
			$('input#llc_artical_uploaded').val('0');
		}
	}



	function corp_corporate_received(that){

		if($(that).is(':checked')){
			$('input#llc_corporate_received').val('1');
		}else{
			$('input#llc_corporate_received').val('0');
		}
	}
	function corp_corporate_approved(that){

		if($(that).is(':checked')){
			$('input#llc_corporate_approved').val('1');
		}else{
			$('input#llc_corporate_approved').val('0');
		}
	}
	function corp_corporate_uploaded(that){

		if($(that).is(':checked')){
			$('input#llc_corporate_uploaded').val('1');
		}else{
			$('input#llc_corporate_uploaded').val('0');
		}
	}


	function corp_texid_received(that){

		if($(that).is(':checked')){
			$('input#llc_corp_texid_received').val('1');
		}else{
			$('input#llc_corp_texid_received').val('0');
		}
	}
	function corp_texid_approved(that){

		if($(that).is(':checked')){
			$('input#llc_corp_texid_approved').val('1');
		}else{
			$('input#llc_corp_texid_approved').val('0');
		}
	}
	function corp_texid_uploaded(that){

		if($(that).is(':checked')){
			$('input#llc_corp_texid_uploaded').val('1');
		}else{
			$('input#llc_corp_texid_uploaded').val('0');
		}
	}

	function corp_es_received(that){

		if($(that).is(':checked')){
			$('input#llc_corp_es_received').val('1');
		}else{
			$('input#llc_corp_es_received').val('0');
		}
	}
	function corp_es_approved(that){

		if($(that).is(':checked')){
			$('input#llc_corp_es_approved').val('1');
		}else{
			$('input#llc_corp_es_approved').val('0');
		}
	}
	function corp_es_uploaded(that){

		if($(that).is(':checked')){
			$('input#llc_corp_es_uploaded').val('1');
		}else{
			$('input#llc_corp_es_uploaded').val('0');
		}
	}

	function lp_certi_chck_received(that){

		if($(that).is(':checked')){
			$('input#lp_certi_received').val('1');
		}else{
			$('input#lp_certi_received').val('0');
		}
	}
	function lp_certi_chck_approved(that){

		if($(that).is(':checked')){
			$('input#lp_certi_approved').val('1');
		}else{
			$('input#lp_certi_approved').val('0');
		}
	}
	function lp_certi_chck_uploaded(that){

		if($(that).is(':checked')){
			$('input#lp_certi_uploaded').val('1');
		}else{
			$('input#lp_certi_uploaded').val('0');
		}
	}




	function lp_part_chck_received(that){

		if($(that).is(':checked')){
			$('input#lp_part_received').val('1');
		}else{
			$('input#lp_part_received').val('0');
		}
	}
	function lp_part_chck_approved(that){

		if($(that).is(':checked')){
			$('input#lp_part_approved').val('1');
		}else{
			$('input#lp_part_approved').val('0');
		}
	}
	function lp_part_chck_uploaded(that){

		if($(that).is(':checked')){
			$('input#lp_part_uploaded').val('1');
		}else{
			$('input#lp_part_uploaded').val('0');
		}
	}




	function lp_texid_chck_received(that){

		if($(that).is(':checked')){
			$('input#lp_texid_received').val('1');
		}else{
			$('input#lp_texid_received').val('0');
		}
	}
	function lp_texid_chck_approved(that){

		if($(that).is(':checked')){
			$('input#lp_texid_approved').val('1');
		}else{
			$('input#lp_texid_approved').val('0');
		}
	}
	function lp_texid_chck_uploaded(that){

		if($(that).is(':checked')){
			$('input#lp_texid_uploaded').val('1');
		}else{
			$('input#lp_texid_uploaded').val('0');
		}
	}


	function lp_es_chck_received(that){

		if($(that).is(':checked')){
			$('input#lp_es_received').val('1');
		}else{
			$('input#lp_es_received').val('0');
		}
	}
	function lp_es_chck_approved(that){

		if($(that).is(':checked')){
			$('input#lp_es_approved').val('1');
		}else{
			$('input#lp_es_approved').val('0');
		}
	}
	function lp_es_chck_uploaded(that){

		if($(that).is(':checked')){
			$('input#lp_es_uploaded').val('1');
		}else{
			$('input#lp_es_uploaded').val('0');
		}
	}




	function trust_chck_received(that){

		if($(that).is(':checked')){
			$('input#trust_received').val('1');
		}else{
			$('input#trust_received').val('0');
		}
	}
	function trust_chck_approved(that){

		if($(that).is(':checked')){
			$('input#trust_approved').val('1');
		}else{
			$('input#trust_approved').val('0');
		}
	}

	function trust_chck_uploaded(that){

		if($(that).is(':checked')){
			$('input#trust_uploaded').val('1');
		}else{
			$('input#trust_uploaded').val('0');
		}
	}



	
	$(document).ready(function(){
	  /***phone number format***/
	  $(".phone-format").keypress(function (e) {
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		  return false;
		}
		var curchr = this.value.length;
		var curval = $(this).val();
		if (curchr == 3 && curval.indexOf("(") <= -1) {
		  $(this).val("(" + curval + ")" + "-");
		} else if (curchr == 4 && curval.indexOf("(") > -1) {
		  $(this).val(curval + ")-");
		} else if (curchr == 5 && curval.indexOf(")") > -1) {
		  $(this).val(curval + "-");
		} else if (curchr == 9) {
		  $(this).val(curval + "-");
		  $(this).attr('maxlength', '14');
		}
	  });
	});

	$( function() {
		var hit = '<?php echo $this->session->flashdata('borrower_document_hit');?>';
		if(hit == '1'){
			$('#borrower_document').modal('show');
		}
	    $( ".cl_exiration_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
	    $( "#income_to" ).datepicker({ dateFormat: 'mm-dd-yy' });
	    $( "#income_from" ).datepicker({ dateFormat: 'mm-dd-yy' });
		 $( "#credit_report_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
		 $( "#dob" ).datepicker({ dateFormat: 'mm-dd-yy' });
		 $( "#c2_dob" ).datepicker({ dateFormat: 'mm-dd-yy' });
		 $( "#balancesheet_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
		 $( ".open_datepicker" ).datepicker({ dateFormat: 'mm-dd-yy' });
	} );
  
	function select_borrower_id(id)
	{
	  // alert(id);
	  $('#borrower_form').submit();
	  // window.location.href = "<?php echo base_url().'borrower_data/'?>"+id;
	}

	function delete_borrower(id)
	{
	  var borrower_id = $('#borrower_id').val() ? $('#borrower_id').val() : id;
	  if(borrower_id){
		  
	  if(confirm('Are you sure to delete this Borrower?') === true)
	  {
		 window.location.href = "<?php echo base_url();?>delete_borrower/"+borrower_id;
	  }
	 
	  }else{
		  alert('Please select first Borrower!');
	  }
	}
  
  
	function delete_this_b_contact(id,borrower){
		 //alert(id); 
		if(id){
			  
			if(confirm('Are you sure to delete this borrower contact?') === true){
		  
				window.location.href = "<?php echo base_url();?>delete_borrower_contacts/"+id+"/"+borrower;
			}  
			  
		}
	}
	  
  

  $('.phone').keyup(function()
  {
	  var val = this.value.replace(/\D/g, '');
	 if(val.length < 10)
	 {
        var newVal = '';
        while (val.length > 3) {
          newVal += val.substr(0, 3) + '-';
          val = val.substr(3);
        }
        newVal += val;
        this.value = newVal;
	 }
	 else
	 {
     this.value = this.value.replace(/(\d{3})\-?(\d{3})\-?(\d{4})/,'$1-$2-$3');
	 }
    //alert ("OK");
  });
  
  $(document).ready(function(){
	  var contact_2 = '<?php echo isset($fetch_borrower_id_data[0]->c2_first_name) ? $fetch_borrower_id_data[0]->c2_first_name : '0'; ?>';
	  if(contact_2!= '' && contact_2!= '0' )
	  {
		  $('#c2_information').css('display','block');
		  $('#add_another_person').css('display','none');
	  }
  });
  function show_contact_2()
  {
	  $('#c2_information').slideDown('slow');
	  $('#add_another_person').css('display','none');
  }
  function show_contact_new()
  {
	  $('#c3_information_new').css('display','block');
	  $('#add_another_person1').css('display','none');
  }
  $(document).ready(function(){
	  var c_bk_declare = $('#c_bk_declare').val();
	  // alert('c_bk_declare'+c_bk_declare);
	  if(c_bk_declare == 0)
	  {
		  
		  $('#select_c_bk_dismis').css('display','none');
		  // $('#select_c_bk_dismis select').prop('disabled',true);
	  }
	  else
	  {
		 $('#select_c_bk_dismis').css('display','block');
		  // $('#select_c_bk_dismis select').prop('disabled',false); 
	  }
  });
  
  $(document).ready(function(){
	  var c2_bk_declare = $('#c2_bk_declare').val();
	  // alert('c2_bk_declare'+c2_bk_declare);
	  if(c2_bk_declare == 0)
	  {
		  $('#select_c2_bk_dismis').css('display','none');
		  // $('#select_c2_bk_dismis select').prop('disabled',true);
	  }
	  else
	  {
		 $('#select_c2_bk_dismis').css('display','block');
		  // $('#select_c2_bk_dismis select').prop('disabled',false); 
	  }
  });
  
  $('#c_bk_declare').change(function(){
	  var c_bk_declare = $('#c_bk_declare').val();
	  if(c_bk_declare == 0)
	  {
		  $('#select_c_bk_dismis').css('display','none');
		  // $('#select_c_bk_dismis select').prop('disabled',true);
	  }
	  else
	  {
		 $('#select_c_bk_dismis').css('display','block');
		  // $('#select_c_bk_dismis select').prop('disabled',false); 
	  }
  });
  $('#c2_bk_declare').change(function(){
	  var c2_bk_declare = $('#c2_bk_declare').val();
	  if(c2_bk_declare == 0)
	  {
		  $('#select_c2_bk_dismis').css('display','none');
		  // $('#select_c2_bk_dismis select').prop('disabled',true);
	  }
	  else
	  {
		 $('#select_c2_bk_dismis').css('display','block');
		  // $('#select_c2_bk_dismis select').prop('disabled',false); 
	  }
  });
  
  
  var i= 1;
  function add_more_row(){
	  i++;
	 if(i < 6){
		 
	  $( ".doc-div" ).append( '<br><div class="row"><div class="col-md-4"><input type="file" id = "upload_'+i+'" name ="upload_doc[]" class="select_images_file" onchange="change_this_image(this)" name="files[]"></div></div>' );
	 }else{

	 }
	  
  }
  function balancesheet_dependent(a)
  {
	  if(a == 'Yes')
	  {
		 $('.balancesheet_dependent').css('display','block');
	  }
	  else if(a == 'No')
	  {
		  $('.balancesheet_dependent').css('display','none');
		  $('.balancesheet_dependent input').val('');

	  }
	  else
	  { 
	  
	  }
  }
  
  $(document).ready(function(){
	  var income_statement = '<?php echo isset($fetch_borrower_id_data[0]->in_balance_income_sheet) ? $fetch_borrower_id_data[0]->in_balance_income_sheet : '0'; ?>';
	  // alert(income_statement);
	  if(income_statement == 1)
	  {
		  $('.balancesheet_dependent').css('display','block');
	  }
	   else if(income_statement == 0)
	  {
		  $('.balancesheet_dependent').css('display','none');
		  $('.balancesheet_dependent input').val('');
	  }
	  else
	  { 
	  
	  }
  });    
  
  $('#event_borrower_type').change(function()
  {
		var borrower_type = $('#event_borrower_type').val();
		var btn_name 		= $('#btn_name').val();
		var register_state = $("select#register_state").val();
		var c_registration_state = $('#company_information_data input').val();
		var state_entity_no = $('input#state_entity_no').val();
		if(btn_name != 'view'){
		  
			if(borrower_type == 1)
			{
				$('.contact_gurrantor_div select').attr('readonly',true);
				$('.contact_title').attr('readonly',true);
				$('.individual_dependent input').attr('readonly',true);
				$('.individual_dependent select').attr('disabled',true);
				$('#company_information_data select').attr('disabled',true);

				$('#company_information_data input').val('');
				$("select#register_state").val("none").change();
				$('input#state_entity_no').val('');
				$('select#ira_name').val('');
				$('input#ira_account').val('');
				$('#company_information_data input').attr('disabled',true);
				$('select#register_state').attr('disabled',true);
				$('input#state_entity_no').attr('disabled',true);
				
				$('input#ira_name').attr('disabled',true);
				$('input#ira_account').attr('disabled',true);

				$('div#llc_checkbox').css('display','none');
				$('div#lp_checkbox').css('display','none');
				$('div#corp_checkbox').css('display','none');
				$('div#trust_checkbox').css('display','none');
				$('div#no_checkbox').css('display','block');
				
			}else if(borrower_type == 5){

				$('#company_information_data input').val(c_registration_state);
				$("select#register_state").val(register_state).change();
				$('input#state_entity_no').val(state_entity_no);
				$('select#ira_name').val('');
				$('input#ira_account').val('');
				$('#company_information_data input').attr('disabled',false);
				$('select#register_state').attr('disabled',false);
				$('input#state_entity_no').attr('disabled',false);
				$('input#ira_name').attr('disabled',true);
				$('input#ira_account').attr('disabled',true);
				
			 	$('.contact_title').attr('readonly',false);
			 	// $('div#ira_information').css('display','none');
			 	$('div#llc_checkbox').css('display','none');
				$('div#lp_checkbox').css('display','none');
				$('div#corp_checkbox').css('display','none');
				$('div#trust_checkbox').css('display','block');
				$('div#no_checkbox').css('display','none');

			}else if(borrower_type == 2 || borrower_type == 3 || borrower_type == 7){

				$('#company_information_data input').val(c_registration_state);
				$("select#register_state").val(register_state).change();
				$('input#state_entity_no').val(state_entity_no);
				$('select#ira_name').val('');
				$('input#ira_account').val('');
				$('#company_information_data input').attr('disabled',false);
				$('select#register_state').attr('disabled',false);
				$('input#state_entity_no').attr('disabled',false);
				$('input#ira_name').attr('disabled',true);
				$('input#ira_account').attr('disabled',true);

				if(borrower_type == 2){

					$('.contact_title').attr('readonly',false);
					$('div#llc_checkbox').css('display','block');
					$('div#lp_checkbox').css('display','none');
					$('div#corp_checkbox').css('display','none');
					$('div#trust_checkbox').css('display','none');
					$('div#no_checkbox').css('display','none');

				}else if(borrower_type == 3){

					$('.contact_title').attr('readonly',false);
					$('div#llc_checkbox').css('display','none');
					$('div#lp_checkbox').css('display','block');
					$('div#corp_checkbox').css('display','none');
					$('div#trust_checkbox').css('display','none');
					$('div#no_checkbox').css('display','none');

				}else{

					$('.contact_title').attr('readonly',false);
					$('div#llc_checkbox').css('display','none');
					$('div#lp_checkbox').css('display','none');
					$('div#corp_checkbox').css('display','block');
					$('div#trust_checkbox').css('display','none');
					$('div#no_checkbox').css('display','none');

				}

			}else if(borrower_type == 6){

				$('#company_information_data input').val(c_registration_state);
				$("select#register_state").val(register_state).change();
				$('input#state_entity_no').val(state_entity_no);
				$('#company_information_data input').attr('disabled',false);
				$('select#register_state').attr('disabled',false);
				$('input#state_entity_no').attr('disabled',false);
				$('input#ira_name').attr('disabled',false);
				$('input#ira_account').attr('disabled',false);

				$('.contact_title').attr('readonly',true);
				$('div#llc_checkbox').css('display','none');
				$('div#lp_checkbox').css('display','none');
				$('div#corp_checkbox').css('display','none');
				$('div#trust_checkbox').css('display','none');
				$('div#no_checkbox').css('display','block');
			}
			else
			{
				$('#company_information_data input').val(c_registration_state);
				$("select#register_state").val(register_state).change();
				$('input#state_entity_no').val(state_entity_no);
				$('select#ira_name').val('');
				$('input#ira_account').val('');
				$('#company_information_data input').attr('disabled',false);
				$('select#register_state').attr('disabled',false);
				$('input#state_entity_no').attr('disabled',false);
				$('input#ira_name').attr('disabled',true);
				$('input#ira_account').attr('disabled',true);

				$('.contact_title').attr('readonly',false);
				$('.contact_gurrantor_div select').attr('readonly',false);
				$('#company_information_data select').attr('disabled',false);
				$('.individual_dependent input').val('');
				$('.individual_dependent input').attr('readonly',false);
				$('.individual_dependent select').attr('disabled',false);
				$('div#llc_checkbox').css('display','none');
				$('div#lp_checkbox').css('display','none');
				$('div#corp_checkbox').css('display','none');
				$('div#trust_checkbox').css('display','none');
				$('div#no_checkbox').css('display','block');
			}
		}
  });
  
  $(document).ready(function()
  {
		var borrower_type = $('#event_borrower_type').val();
		var btn_name 		= $('#btn_name').val();
		var register_state = $("select#register_state").val();
		var c_registration_state = $('#company_information_data input').val();
		var state_entity_no = $('input#state_entity_no').val();

		if(btn_name != 'view'){
		
		
			if(borrower_type == 1)
			{
				$('.contact_gurrantor_div select').attr('readonly',true);
				$('.contact_title').attr('readonly',true);
				$('.individual_dependent input').attr('readonly',true);
				$('.individual_dependent select').attr('disabled',true);
				$('#company_information_data input').attr('disabled',true);
				$('#company_information_data select').attr('disabled',true);

				$('#company_information_data input').val('');
				$("select#register_state").val("none").change();
				$('input#state_entity_no').val('');
				$('select#ira_name').val('');
				$('input#ira_account').val('');
				$('#company_information_data input').attr('disabled',true);
				$('select#register_state').attr('disabled',true);
				$('input#state_entity_no').attr('disabled',true);
				

				$('input#ira_name').attr('disabled',true);
				$('input#ira_account').attr('disabled',true);
				

				$('div#llc_checkbox').css('display','none');
				$('div#lp_checkbox').css('display','none');
				$('div#corp_checkbox').css('display','none');
				$('div#trust_checkbox').css('display','none');
				$('div#no_checkbox').css('display','block');
				
			}else if(borrower_type == 5){
				$('#company_information_data input').val(c_registration_state);
				$("select#register_state").val(register_state).change();
				$('input#state_entity_no').val(state_entity_no);
				$('select#ira_name').val('');
				$('input#ira_account').val('');
				$('#company_information_data input').attr('disabled',false);
				$('select#register_state').attr('disabled',false);
				$('input#state_entity_no').attr('disabled',false);
				$('input#ira_name').attr('disabled',true);
				$('input#ira_account').attr('disabled',true);

			 	$('.contact_title').attr('readonly',false);
			 	$('div#llc_checkbox').css('display','none');
				$('div#lp_checkbox').css('display','none');
				$('div#corp_checkbox').css('display','none');
				$('div#trust_checkbox').css('display','block');
				$('div#no_checkbox').css('display','none');

			}else if(borrower_type == 2 || borrower_type == 3 || borrower_type == 7){

				$('#company_information_data input').val(c_registration_state);
				$("select#register_state").val(register_state).change();
				$('input#state_entity_no').val(state_entity_no);
				$('select#ira_name').val('');
				$('input#ira_account').val('');
				$('#company_information_data input').attr('disabled',false);
				$('select#register_state').attr('disabled',false);
				$('input#state_entity_no').attr('disabled',false);
				$('input#ira_name').attr('disabled',true);
				$('input#ira_account').attr('disabled',true);

				if(borrower_type == 2){
					
					$('.contact_title').attr('readonly',false);
					$('div#llc_checkbox').css('display','block');
					$('div#lp_checkbox').css('display','none');
					$('div#corp_checkbox').css('display','none');
					$('div#trust_checkbox').css('display','none');
					$('div#no_checkbox').css('display','none');

				}else if(borrower_type == 3){


					$('.contact_title').attr('readonly',false);
					$('div#llc_checkbox').css('display','none');
					$('div#lp_checkbox').css('display','block');
					$('div#corp_checkbox').css('display','none');
					$('div#trust_checkbox').css('display','none');
					$('div#no_checkbox').css('display','none');

				}else{

					$('.contact_title').attr('readonly',false);
					$('div#llc_checkbox').css('display','none');
					$('div#lp_checkbox').css('display','none');
					$('div#corp_checkbox').css('display','block');
					$('div#trust_checkbox').css('display','none');
					$('div#no_checkbox').css('display','none');
				}
			}else if(borrower_type == 6){

				$('#company_information_data input').val(c_registration_state);
				$("select#register_state").val(register_state).change();
				$('input#state_entity_no').val(state_entity_no);
				$('#company_information_data input').attr('disabled',false);
				$('select#register_state').attr('disabled',false);
				$('input#state_entity_no').attr('disabled',false);
				$('input#ira_name').attr('disabled',false);
				$('input#ira_account').attr('disabled',false);

				$('.contact_title').attr('readonly',true);
				$('div#llc_checkbox').css('display','none');
				$('div#lp_checkbox').css('display','none');
				$('div#corp_checkbox').css('display','none');
				$('div#trust_checkbox').css('display','none');
				$('div#no_checkbox').css('display','block');
			}
			else
			{
				$('#company_information_data input').val(c_registration_state);
				$("select#register_state").val(register_state).change();
				$('input#state_entity_no').val(state_entity_no);
				$('select#ira_name').val('');
				$('input#ira_account').val('');
				$('#company_information_data input').attr('disabled',false);
				$('select#register_state').attr('disabled',false);
				$('input#state_entity_no').attr('disabled',false);
				$('input#ira_name').attr('disabled',true);
				$('input#ira_account').attr('disabled',true);

				$('.contact_title').attr('readonly',false);
				$('.contact_gurrantor_div select').attr('readonly',false);
				$('#company_information_data select').attr('disabled',false);
				$('.individual_dependent input').val('');
				$('.individual_dependent input').attr('readonly',false);
				$('.individual_dependent select').attr('disabled',false);
				$('div#llc_checkbox').css('display','none');
				$('div#lp_checkbox').css('display','none');
				$('div#corp_checkbox').css('display','none');
				$('div#trust_checkbox').css('display','none');
				$('div#no_checkbox').css('display','block');
			}
		}
  });
  
  function autofill_data()
  {
	  var street = $('#address').val();
	  var unit = $('#unit').val();
	  var city = $('#city').val();
	  var state = $('#state').val();
	  var zip = $('#zip').val();
	  var tax_id = $('#tax_id').val();
	  
	  $('#c_street').val(street);
	  $('#c_unit').val(unit);
	  $('#c_city').val(city);
	  $('#c_state').val(state);
	  $('#c_zip').val(zip);
  }
  
  function auto_blank_contact()
  {
	  $('div.c1 input').val('');
	  $('div.c1 select').val('');
	  $('div.c1 textarea').val('');
  } 
  
  function auto_blank_contact_c2()
  {
	  $('div#c2_information input').val('');
	  $('div#c2_information select').val('');
	  $('div#c2_information textarea').val('');
  }
  
  function fetch_borrower_data(id){
	    if(id){
		  
	 
		var contact_type = $('#display_borrower_name').find('option:selected').attr('contacttype');
		// alert(contact_type);
		  $('#contact_type').val(contact_type);
	
		//ajax_call...
		$.ajax({
			type: "post",
			url: "<?php echo base_url()?>Borrower_data/fetch_borrower_details/",
			data:{'id':id,'contact_type':contact_type},
			success: function(response){
				if(response){
					var data  = JSON.parse(response);
					$('#display_borrower_email').val(data.email);
					$('#display_borrower_phone').val(data.phone);
					// $('#display_borrower_title').val(data.phone);
					$('#display_borrower_guarantor').val(data.guarantor);
					
				}
			}
		});
	}
  }
  
  function add_new_contact(){
	  // alert('gone');
	$('div#hidden_contact_information_div').css('display','block');
	var new_count = $('#formID .new_contact_div').length - 1;
	
	
	
	new_count++;
	// $("div#hidden_contact_information_div div.new_contact_div h4.count_borrower_contact").text('Contact Name '+new_count);
	$("div#hidden_contact_information_div div.primary_contactdiv select.chkbx_primary_contact").prop('id','primary_contact_'+new_count);
	
	$("div#hidden_contact_information_div div.primary_contactdiv input.chkbx_primary_contact_val").prop('id','primary_contact_val'+new_count);
	
	$("div#hidden_contact_information_div div.new_contact_div").clone().insertAfter("div#single_new_contact  div.new_contact_div:last");
	
	$('div#hidden_contact_information_div').css('display','block');
 }
 
 function fetch_contact_details(that)
 {
	 var contact_id = that.value;
	 $.ajax({
			type: "post",
			url: "<?php echo base_url()?>Ajax_call/fetch_contact_details",
			data:{'contact_id':contact_id},
			success: function(response){
				if(response){
					var data  = JSON.parse(response);
					// alert(data.phone);
					$(that).parent().parent().parent().find('input.contact_phone').val(data.phone);
					$(that).parent().parent().parent().find('input.contact_email').val(data.email);
					$(that).parent().parent().parent().find('input.contact_email').val(data.email);
					$(that).parent().parent().parent().find('input.contact_title').val(data.title);
					
				}
			}
	 });
	 
 }

 	function hide_this_contact(that)
	{
		$(that).parent().parent().parent().css('display','none');
	}
	
	function delete_borrower_contact(id)
	{
		if(id){
			if(confirm('Are you sure to delete?') == true)
			{
				window.location.href = '<?php echo base_url();?>delete_borrower_contact/D'+id;
			}
		}
	}
	
	function display_inputfield(id){
			var filename = $('#filename_'+id).data('name');
			
			$('.new_filename_div'+id).toggle();
			$('#borrower_document_id'+id).val(id);
			$('#old_filename'+id).val(filename);

	}

	function check_filename_duplicacy(id,borrower_id){
		
		var new_filename = $('#new_filename'+id).val();
		var old_filename = $('#old_filename'+id).val();
		
		var new_extension = new_filename.substr( (new_filename.lastIndexOf('.') +1) );
		var old_extension = old_filename.substr( (old_filename.lastIndexOf('.') +1) );
		switch(new_extension) {
	        case 'pdf': 
				var filename =  new_filename;
				$('#new_filename'+id).val(filename);
				
			break;
			default:
				var filename = new_filename+'.'+old_extension;
				$('#new_filename'+id).val(filename);
	    }
		
		//ajax call to check duplicacy of file name....
		$.ajax({
			type 	: 'post',
			url  	: '<?php echo base_url().'Borrower_data/check_borrower_filename';?>',
			data	: { 'filename' : filename,'borrower_id':borrower_id},
			success : function(response){
				if(response == '1'){
					// duplicacy found...
					$('#new_filename'+id).val(' ').focus();
					alert('Filename: '  + filename + ' already exist for this Borrrower, Please try another filename!' );
					
				}
			}
			
		});
		
	}
	 function read_document(id){
		 
		 // alert('ddd');
		 $('#document_id').val(id);
		 // $('#documentfrm').submit();
		  window.location.href = "<?php echo base_url().'borrower_data/borrower_document_read/'?>"+id;
		  // window.open = ('<?php echo base_url().'borrower_data/borrower_document_read/'?>'+id, '_blank');
	 }
 


	function check_primary_contact_input(id){
		
		$('.chkbx_primary_contact').prop('checked',false);
		$('.chkbx_primary_contact_val').val('0');
		var rowcnt = id.split('_');
		
		if($('#'+id).prop('checked',true)){
			// alert('if part');
			// alert('checked checkbox number : ' + rowcnt[2]);
			$('#primary_contact_val'+rowcnt[2]).val('1');
		}
	}

  

  function check_primary_contact(id){
	
	var rowcnt = id.split('_');
	var rr =  $('#'+id).val();
	alert(rr);
	if(rr == 1){
		$('#primary_contact_val'+rowcnt[2]).val('1');
		
		$("select.chkbx_primary_contact").not(this).find("option[value='1']").attr('disabled', true);
	
		$("select#primary_contact_"+rowcnt[2]).not(this).find("option[value='1']").attr('disabled', false);
	}else if(rr == 0){
		$('#primary_contact_val'+rowcnt[2]).val('0');
		
		$("select.chkbx_primary_contact").not(this).find("option[value='1']").attr('disabled', false);
		
	}
	
}

function delete_doc(dili){

	var doc_id = dili.id;
	
	//alert(doc_id);
	if(confirm('Are You Sure To Delete?')){
		$.ajax({
			
			type	:'POST',
			url		:'<?php echo base_url()."Borrower_data/borrrower_delete_document";?>',
			data	:{'id':doc_id},
			success : function(response){
				if(response == 1){
					$(dili).parent().parent('li').remove();
				}
			}
				
		});
	}
}  

function fetch_borrower_id(){
	
	var borrower_id = $('#borrower_id').val();
	if(borrower_id){
		$('#btn_name').val('edit');
		$('#borrower_form').submit();
		// window.location.href = "<?php echo base_url().'borrower_data/edit/';?>"+borrower_id;
	}else{
		alert('Please select first Borrower!');
	}
	
}

function nevent_borrower_type(){
	$('#register_state').prop( "disabled", false );
	$('#state_entity_no').prop( "disabled", false );
	if($('#event_borrower_type').val() == 5){
		$('#register_state').prop( "disabled", true );
		$('#state_entity_no').prop( "disabled", true );
	}
}
$(document).ready(function(){
	$('body').on('click', '#event_borrower_type', function(){
		nevent_borrower_type();
	});
	nevent_borrower_type();
});

</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
