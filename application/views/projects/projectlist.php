<?php

$project_status = $this->config->item('project_status');
$project_role_option = $this->config->item('project_role_option');

?>
<style>
div.page-title h3 {
    padding: 7px 11px;
    font-weight: 600;
}
.col-md-2#dd {
    margin-left: 116px;
}
.col-md-2#marr {

  
    margin: -6px -120px 0px;
}
.modal .modal-header .close {
    margin-top: -14px !important;
}
label{
	
	font-weight: bold !important;
}

.addspace{
	
	margin-bottom: 12px !important;
}

tr.thead_dark_blue {
    background: #003468;
    color: white;
    text-align: center;
}

div#table_contactlist_filter {
    float: right;
}

div#table_contactlist_length {
    margin-left: -15px;
}
div#table_contactlist_filter {
    margin-right: -15px;
}
.margin_uppair{
	margin-top: 12px;
}
</style>

<div class="page-container contactlist-outmost">
			<!-- BEGIN PAGE HEAD -->
	<div class="container">
		<?php if($this->session->flashdata('error')!=''){  ?>
		 <div id='error'><i class='fa fa-thumbs-o-down'></i> <?php echo $this->session->flashdata('error');?></div><?php } ?>
		  <?php if($this->session->flashdata('success')!=''){  ?>
		 <div id='success'><i class='fa fa-thumbs-o-up'></i> <?php echo $this->session->flashdata('success');?></div><?php } ?>
		<div class="tab-pane">
				<div class="page-head">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h3>Projects</h3>
					</div>
						
				</div>
			
			<!-- END PAGE HEAD -->
			<div class="page-container">
                <div class="contactlist-res-div">	
				 
                <div class="row">
					<form method="post" action="<?php echo base_url();?>projects">
						<div class="col-md-2">	
							<label>Project Status:</label>
							<select class="form-control" name="project_status_opt">
								<option value="">Select All</option>
								<?php foreach($project_status as $key => $row){ ?>
									<option value="<?php echo $key;?>" <?php if($project_status_opt == $key){echo 'selected';}?>><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-2">	
							<label>Participants:</label>
							<select class="form-control" name="participants">
								<option value="">Select All</option>
								<?php foreach($fetchcontact as $row){ ?>
									<option value="<?php echo $row->id;?>" <?php if($participants == $row->id){echo 'selected';}?>><?php echo $row->fname.' '.$row->lname ;?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-1">
							<button type="submit" name="submit" class="btn btn-sm btn-primary" style="margin-top: 24px;">Filter</button>
						</div>
					</form>

					<div class="col-md-3">	
						<button type="button" class="btn btn-primary pp_Addproject" data-toggle="modal" data-target="#Addproject" style="margin-top: 8px;">Add Project</button>
					</div>
				</div>
				
				<div class="row"><br></div>
				
                <div class="col-md-12">	
					
					<table id ="table_contactlist" class="table table-responsive table-bordered table-striped table-condensed flip-content table-hover">
							<thead>
								<tr class="thead_dark_blue">
									<th style="width:30%;">Project Name</th>
									<th style="width:10%;">Status</th>
									<th style="width:10%;">Initiated By</th>
									<th style="width:10%;">Deadline</th>
									<th style="width:30%;">Description</th>
									<th style="width:10%;">Action</th>
									
							
								</tr>
							</thead>
							<tbody>
							<?php
							$count = 0;
							if(isset($fetchproject) && is_array($fetchproject)) { 
									foreach($fetchproject as $row){ $count++; 
									
									$loginUser = $this->session->userdata('t_user_id');
									$strtoarray = explode(",", $row->contact_ids);
						
									//if(in_array($loginUser, $strtoarray) || $row->user_id == $loginUser){
									
									?>
									
										<tr class="row_<?php echo $row->id;?>">
											<td><?php echo $row->project_name;?></td>
											<td><?php echo $project_status[$row->project_status];?></td>
											<td><?php echo $getuserNames[$row->user_id];?></td>
											<td><?php echo date('m-d-Y', strtotime($row->complete_date));?></td>
											<td><?php echo $row->description;?></td>
											<td>
											
												<?php if($loginUser == $row->user_id){ ?>
													<a onclick="editRow('<?php echo $row->id;?>');" class="btn btn-xs btn-success">Edit</a>
													
												<?php }else{ ?>
													
													<a class="btn btn-xs btn-success" title="No permission to edit">Edit</a>										
													
												<?php } ?>

												<a onclick="viewRow('<?php echo $row->id;?>')" class="btn btn-xs btn-info">View</a>
												
											</td>
										</tr>
										
									<?php  } }else{ ?>
							
									<tr>
										<td colspan="6" class="text-center"> No projects found!</td>
									</tr>
							<?php } ?>
							</tbody>
							
					</table>
				</div>
				
				
			</div>
		</div>
								<!------------>
	
		</div>
	</div>
</div>


<!-- Modal Add-->
<div class="modal fade" id="Addproject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
	<form method="post" action="<?php echo base_url();?>ProjectData/add_editdata" enctype="multipart/form-data">
      <div class="modal-header">
        <h4><b>Add Project</b></h4>
      </div>
      <div class="modal-body">
        
		<input type="hidden" id="rowid" name="rowid" value="">
		<input type="hidden" id="userid" name="userid" value="<?php echo $this->session->userdata('t_user_id');?>">
		
		<div class="row addspace">
			<div class="col-md-6">
				<label>Project Status:</label>
				<select type="text" class="form-control" name="project_status">
					<?php foreach($project_status as $key => $row){ ?>
						<option value="<?php echo $key;?>"><?php echo $row;?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		
		<div class="row addspace">
			<div class="col-md-12">
				<label>Name of Project:</label>
				<input type="text" class="form-control" name="project_name" placeholder="Name of Project" autocomplete="off">
			</div>
		</div>
		
		<div class="row addspace">
			<div class="col-md-6 pp_Addproject">
				<label>Start Date:</label>
				<input type="text" class="form-control datepicker" name="start_date" placeholder="Start Date" autocomplete="off">
			</div>
			<div class="col-md-6 pp_Addproject">
				<label>Completion Date:</label>
				<input type="text" class="form-control datepicker" name="completion_date" placeholder="Completion Date" autocomplete="off">
			</div>
		</div>
		
		<div class="row addspace">
			<div class="col-md-6">
				<label>Created By:</label>
				<input type="text" class="form-control" name="CreatedBy" autocomplete="off" value="<?php echo $this->session->userdata('user_name');?>" readonly>
			</div>
			
		</div>
		
		<div id="morerows">
			<div class="row addspace">
				<div class="col-md-6">
					<label>Participants:</label>
					<select class="form-control" name="contact_name[]">
					
					<?php foreach($fetchcontact as $row){?>
						<option value="<?php echo $row->id;?>"><?php echo $row->fname.' '.$row->lname;?></option>
					<?php } ?>
					</select>
				</div>
				
				<div class="col-md-4">
					<label>Role:</label>
					<select class="form-control" name="role[]">
					
					<?php foreach($project_role_option as $key => $row){?>
						<option value="<?php echo $key;?>"><?php echo $row;?></option>
					<?php } ?>
					</select>
				</div>
				<div class="col-md-1">
					<a title="Add Participants" onclick="add_morerow(this);" class="btn btn-sm btn-info" style="margin-top: 25px;"><i class="fa fa-plus"></i></a>
				</div>

			</div>
		</div>
		
		<div class="row addspace">
			<div class="col-md-12">
				<label>Project Description:</label>
				<textarea type="text" class="form-control" name="description" rows="5" placeholder="Project Description..."></textarea>
			</div>
		</div>
		<div class="row addspace">
			<label>Attachment:</label>
		</div>
		<div class="row addspace" id="add_attachment_list">
			<div class="col-md-12" id="attachment_list_row_0">
				<div class="col-md-9">
					<input type="file" name="project_attachment[]" >
				</div>
				<div class="col-md-3">
					<a href="javascript:void(0)" onclick="add_more_attachment('add_attachment_list')">Add More</a>
				</div>
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Save</button>
      </div>
	  </form>
    </div>
  </div>
</div>

<!-- Modal View-->
<div class="modal fade" id="viewmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4><b>View Project</b></h4>
      </div>
      <div class="modal-body">
			
			<div class="row addspace">
				<div class="col-md-12">
					<label>Project Name:</label>
					<input type="text" class="form-control" id="pro_name" value="" readonly>
				</div>
			</div>
			
			<div class="row addspace">
				<div class="col-md-6">
					<label>Start Date:</label>
					<input type="text" class="form-control" id="sdate" value="" readonly>
				</div>
				<div class="col-md-6">
					<label>End Date:</label>
					<input type="text" class="form-control" id="edate" value="" readonly>
				</div>
			</div>
			
			<div class="row addspace">
				<div class="col-md-12">
					<label>Initiated By:</label>
					<input type="text" class="form-control" id="addedby" value="" readonly>
				</div>
			</div>
			
			<div class="row addspace">
				<div class="col-md-12">
					<label>Participants:</label>
					<input type="text" class="form-control" id="participants" value="" readonly>
				</div>
			</div>

			<div class="row addspace">
				<div class="col-md-12">
					<label>Role:</label>
					<input type="text" class="form-control" id="role" value="" readonly>
				</div>
			</div>
			
			<div class="row addspace">
				<div class="col-md-12">
					<label>Description:</label>
					<textarea type="text" class="form-control" rows="4" id="description" readonly></textarea>
				</div>
			</div>
			<div class="row addspace">
				<label>Attachment:</label>
			</div>
			<div class="row addspace" id="view_attachment_list">
				
			</div>
	  </div>
	  <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal Edit-->
<div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
	<form method="post" action="<?php echo base_url();?>ProjectData/add_editdata" enctype="multipart/form-data">
      <div class="modal-header">
        <h4><b>Update Project</b>  <a onclick="deleteRow(this)" class="btn btn-sm btn-danger pull-right">Delete Project</a></h4>
      </div>
      <div class="modal-body">
        
		<input type="hidden" id="rowid" name="rowid" value="">
		<input type="hidden" id="userid" name="userid" value="">
		
		<div class="row addspace">
			<div class="col-md-6">
				<label>Project Status:</label>
				<select type="text" class="form-control" name="project_status" id="project_status">
					<?php foreach($project_status as $key => $row){ ?>
						<option value="<?php echo $key;?>"><?php echo $row;?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="row addspace">
			<div class="col-md-12">
				<label>Name of Project:</label>
				<input type="text" class="form-control" name="project_name" placeholder="Name of Project" autocomplete="off">
			</div>
		</div>
		
		<div class="row addspace">
			<div class="col-md-6">
				<label>Start Date:</label>
				<input type="text" class="form-control datepicker" name="start_date" placeholder="Start Date" autocomplete="off">
			</div>
			<div class="col-md-6">
				<label>Completion Date:</label>
				<input type="text" class="form-control datepicker" name="completion_date" placeholder="Completion Date" autocomplete="off">
			</div>
		</div>
		
		<div id="morerows">
			<div class="row addspace"> 
				<div class="col-md-6">
					<label>Participants:</label>
					<select class="form-control" name="contact_name[]">
					
					
					</select>
				</div>

				<div class="col-md-6">
					<a onclick="add_morerow(this);" class="btn btn-sm btn-info" style="margin-top: 25px;">Add Contact</a>
				</div>
			</div>
		</div>
		
		<div class="row addspace">
			<div class="col-md-12">
				<label>Project Description:</label>
				<textarea type="text" class="form-control" name="description" rows="5" placeholder="Project Description..."></textarea>
			</div>
		</div>
		<div class="row addspace">
			<label>Attachment:</label>
		</div>
		<div class="row addspace" id="update_attachment_list">
			<div class="col-md-12" id="attachment_list_row_0">
				<div class="col-md-9">
					<input type="file" name="project_attachment[]" >
				</div>
				<div class="col-md-3">
					<a href="javascript:void(0)" onclick="add_more_attachment('update_attachment_list')">Add More</a>
				</div>
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Update</button>
      </div>
	  </form>
    </div>
  </div>
</div>



<script type="text/javascript">
	var row_no=0;
function add_more_attachment(id_name){
	row_no=parseInt(row_no)+1;
	var str_attch='<div class="col-md-12 margin_uppair" id="attachment_list_row_'+row_no+'" >';
	str_attch+='<div class="col-md-9"><input type="file" name="project_attachment[]" ></div>';
	str_attch+='<div class="col-md-3"><a href="javascript:void(0)" onclick="remove_attchment(`'+row_no+'`)" style="color:red;">Remove</a></div>';
	str_attch+='</div>';
	console.log(str_attch);
	$("#"+id_name).append(str_attch);
}
function remove_dynamic_attachment(id_no,row){
	$.ajax({
			
			type : 'POST',
			url  : '<?php echo base_url()."ProjectData/deleteAttachment";?>',
			data : {'id':id_no},
			async: false,
			success : function(response){				
				$("#attachment_list_row_"+row).remove();
			}
		});
}
function remove_attchment(row){
	$("#attachment_list_row_"+row).remove();
}
function add_morerow(that){
				
	var count = $('#morerows .row').length;
	count++;
	
	$('#morerows').append('<div class="row addspace" id="rowss_'+count+'"><div class="col-md-6"><label style="visibility:hidden;">Participants:</label><select id="app-'+count+'" class="form-control" name="contact_name[]"><?php foreach($fetchcontact as $row){?><option value="<?php echo $row->id;?>"><?php echo $row->fname.' '.$row->lname;?></option><?php } ?></select></div><div class="col-md-4"><label style="visibility:hidden;">Role:</label><select class="form-control" name="role[]"><?php foreach($project_role_option as $key => $row){ ?><option value="<?php echo $key;?>"><?php echo $row;?></option><?php } ?></select></div><div class="col-md-1"><button title="Remove" type="button" id="'+count+'" onclick="removerow(this.id);" class="btn btn-sm btn-danger" style="margin-top: 25px;"><i class="fa fa-trash"></i></button></div></div>');

}


function removerow(that){
	
	$('#rowss_'+that).remove();
	
}

function formatDate(date) {
     var d = new Date(date),
         month = '' + (d.getMonth() + 1),
         day = '' + d.getDate(),
         year = d.getFullYear();

     if (month.length < 2) month = '0' + month;
     if (day.length < 2) day = '0' + day;

     return [month, day, year].join('-');
 }

function viewRow(id){
	
		
		$.ajax({
			
			type : 'POST',
			url  : '<?php echo base_url()."ProjectData/viewrow";?>',
			data : {'id':id},
			async: false,
			success : function(response){
							
				var data = JSON.parse(response);
				var username = GetUsernames(data[0].user_id);
				
				var contact_idss = data[0].contact_ids;
				var username1 = GetUsernames1(contact_idss);

				var role = data[0].role;
				var roleval = roledata(role);
								
				var startdate = formatDate(data[0].start_date);
				var complete_date = formatDate(data[0].complete_date);
							
				$('#viewmodal input#pro_name').val(data[0].project_name);
				$('#viewmodal input#sdate').val(startdate);
				$('#viewmodal input#edate').val(complete_date);
				$('#viewmodal textarea#description').val(data[0].description);
				$("#view_attachment_list").html(data.html_content);
				$('#viewmodal').modal('show');
			}
		});
}
				
				
function GetUsernames(that){
	
	$.ajax({
			
			type : 'POST',
			url  : '<?php echo base_url()."ProjectData/getuserinfo";?>',
			data : {'id':that},
			success : function(response){
				
				$('#viewmodal input#addedby').val(response);
				
			}
	});
}

function GetUsernames1(that){
	//alert(that);
	$.ajax({
			
			type : 'POST',
			url  : '<?php echo base_url()."ProjectData/getuserinfo1";?>',
			data : {'id':that},
			success : function(response){
				
				$('#viewmodal input#participants').val(response);
				
			}
	});
}

function roledata(that){
	//alert(that);
	$.ajax({
			
			type : 'POST',
			url  : '<?php echo base_url()."ProjectData/getroledata";?>',
			data : {'id':that},
			success : function(response){
				
				$('#viewmodal input#role').val(response);
				
			}
	});
}


function editRow(id){
	
		$.ajax({
			
			type : 'POST',
			url  : '<?php echo base_url()."ProjectData/editrow";?>',
			data : {'id':id},
			success : function(response){
							
				var data = JSON.parse(response);
				
				var startdate = formatDate(data[0].start_date);
				var complete_date = formatDate(data[0].complete_date);
				
				$('#editmodal input#rowid').val(data[0].id);
				$('#editmodal input#userid').val(data[0].user_id);
				$('#editmodal input[name="project_name"]').val(data[0].project_name);
				$('#editmodal input[name="start_date"]').val(startdate);
				$('#editmodal input[name="completion_date"]').val(complete_date);
				$('#editmodal textarea[name="description"]').val(data[0].description);
				$('#editmodal select#project_status option[value="'+data[0].project_status+'"]').attr('selected','selected');

				
				var contact_idss = data[0].contact_ids;
				var strArr = contact_idss.split(',');

				var role = data[0].role;
				
				if(role === null){
					roleVAL = '1,';
					var roleArr = roleVAL.split(',');
				}else{

					var roleArr = role.split(',');
				}
				
				
				$('#editmodal #morerows .row').remove();
				
				$.each(strArr, function(key, row){
					
					//alert(key);
					//alert(roleArr[key]);
					
					var count = $('#editmodal #morerows .row').length;
					count++;
					
					if(count == '1'){
						
						var AddMore = '<div class="col-md-1"><a title="Add Participants" onclick="add_morerowbyuser(this);" class="btn btn-sm btn-info" style="margin-top: 25px;"><i class="fa fa-plus"></i></a></div>';
						
						var part = '';
					}else{
						
						var AddMore = ''; 
						var part = 'style="visibility:hidden;"';
					}
					
					$('#editmodal #morerows').append('<div class="row addspace" id="rowss_'+count+'"><div class="col-md-6"><label '+part+'>Participants:</label><select id="user_'+row+'" class="form-control" name="contact_name[]"><?php foreach($fetchcontact as $row){?><option value="<?php echo $row->id;?>"><?php echo $row->fname.' '.$row->lname;?></option><?php } ?></select></div><div class="col-md-3"><label '+part+'>Role:</label><select class="form-control" id="role_'+roleArr[key]+'" name="role[]"><?php foreach($project_role_option as $key => $row){?><option value="<?php echo $key;?>"><?php echo $row;?></option><?php } ?></select></div><div class="col-md-1"><button type="button" id="'+count+'" title="Remove" onclick="removerow(this.id);" class="btn btn-sm btn-danger" style="margin-top: 25px;"><i class="fa fa-trash"></i></button></div>'+AddMore+'</div>');
					
					$('#editmodal select#user_'+row+' option[value="'+row+'"]').attr('selected','selected');
					$('#editmodal select#role_'+roleArr[key]+' option[value="'+roleArr[key]+'"]').attr('selected','selected');
					
				});
				
				row_no	 =data.html_count;
				$("#update_attachment_list").html(data.html_content);
				$('#editmodal').modal('show');
			}
			
		});
	
}

function add_morerowbyuser(){
	
	var count = $('#editmodal #morerows .row').length;
	count++;
	
	$('#editmodal #morerows').append('<div class="row addspace" id="rowss_'+count+'"><div class="col-md-6"><label style="visibility:hidden;">Participants:</label><select id="app-'+count+'" class="form-control" name="contact_name[]"><?php foreach($fetchcontact as $row){?><option value="<?php echo $row->id;?>"><?php echo $row->fname.' '.$row->lname;?></option><?php } ?></select></div><div class="col-md-3"><label style="visibility:hidden;">Role:</label><select class="form-control" name="role[]"><?php foreach($project_role_option as $key => $row){ ?><option value="<?php echo $key;?>"><?php echo $row;?></option><?php } ?></select></div><div class="col-md-1"><button type="button" id="'+count+'" title="Remove" onclick="removerow(this.id);" class="btn btn-sm btn-danger" style="margin-top: 25px;"><i class="fa fa-trash"></i></button></div></div>');
	
}

function deleteRow(that){

	var rowid = $('#editmodal input#rowid').val();
	//alert(rowid);
	
	if(confirm('Are you sure to remove this project?')){
		
		$.ajax({
			
			type : 'POST',
			url  : '<?php echo base_url()."ProjectData/removerow";?>',
			data : {'id':rowid},
			success : function(response){
				
				if(response == '1'){
					
					//$('.row_'+id).remove();
					alert('Project Removed successfully!');
					window.location.reload();
				}
			}
			
		});
	}
}

$(document).ready(function(){
	$(".datepicker").datepicker({ dateFormat: "mm-dd-yy" });

	$('.pp_Addproject').click(function(){
		$(".datepicker").datepicker({ dateFormat: "mm-dd-yy" });
	});	
	/*$('#table_contactlist').dataTable({
		"ordering": false
	});*/



	
});
</script>