<?php

// echo '<pre>';
// print_r($user_data);
// echo '<pre>';
?>
<div class="container">
	<div class="page-title">
		<h1>Users Login Details </h1>
	</div>

	<div class="rc_class">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class="thead_dark_blue">
				<th>Name</th>
				<th>Username</th>
				<th>Status</th>
				<th>Date</th>
				<th>Time</th>
			</thead>
			
			<tbody>
			<?php
			foreach($user_login_detail as $key => $row)
			{
			?>
				<tr>
					<td><?php echo $user_data[$row->t_user_id]['name']; ?></td>
					<td><?php echo $user_data[$row->t_user_id]['username']; ?></td>
					<td><strong><?php echo $row->status; ?></strong></td>
					<td><?php echo date('d-m-Y' ,strtotime($row->date)); ?></td>
					<td><?php echo $row->time; ?></td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
</div>