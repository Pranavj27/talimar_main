<style>	
#success{
	width: 100%;
    border-radius: 6px;
}
#error{
	width: 100%;
    border-radius: 6px;
}
.user_content{
	padding : 10px 25px;
}
/*.user_content table thead.flip-content th {
    background: #103a60;
    color: white;
}*/

a.lock_account {
    background: #F44336;
    padding: 1px 2px 1px 2px;
    border-radius: 2px;
    color: #fff;
    text-align: center;
    text-decoration: none;
}

a.lock_accounts {
    background: #058003;
    padding: 1px 2px 1px 2px;
    border-radius: 2px;
    color: #fff;
    text-align: center;
    text-decoration: none;
}

.fa-lg, .icon-lg {
    font-size: 20px !important;
    margin-left: 3px !important;
}

</style>
	
		<div class="tab-pane">
				<div class="page-container">	
					<div class="tabbable tabbable-custom tabbable-noborder tabbable-reversed">	
						<div class="tab-content">	
						
						<?php
						if($this->session->flashdata('success'))
						{
							?>
							
							<div id="success"><?php echo $this->session->flashdata('success');?></div>
							
							<?php
						}
						if($this->session->flashdata('error'))
						{
							?>
							
							<div id="error"><?php echo $this->session->flashdata('error');?></div>
							
							<?php
						}
						?>
						
							<div class="portlet box green" id="sd">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i>CREATE USER
										</div>
										<div class="tools">
											<a class="user_login_detai_link" href="<?php echo base_url();?>Users/portal_login_detail">Portal Login Details</a>
											<a class="user_login_detai_link" href="<?php echo base_url();?>user_login_detail">Users Login Details</a>
											<a href="javascript:;" class="expand" id="a">
											</a>
										</div>
									</div>
									<div class="portlet-body form" style="display: none;">
										<!-- BEGIN FORM-->
										<form action="<?php echo base_url();?>form_user" method="POST" class="form-horizontal">
											<div class="form-body">
												<div class="form-group">
													<label class="col-md-3 control-label">First Name</label>
													<div class="col-md-4">
														<input type="text" class="form-control input-circle" placeholder="" name="fname" required>
														<!--<span class="help-block">
														A block of help text. </span>-->
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label">Middle Initial</label>
													<div class="col-md-4">
														<input type="text" class="form-control input-circle" name="middle" >
														<!--<span class="help-block">
														A block of help text. </span>-->
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label">Last Name</label>
													<div class="col-md-4">
														<input type="text" class="form-control input-circle" name="lname">
														<!--<span class="help-block">
														A block of help text. </span>-->
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label">Email Address</label>
													<div class="col-md-4">
														<div class="input-group">
															<span class="input-group-addon input-circle-left">
															<i class="fa fa-envelope"></i>
															</span>
															<input type="email" class="form-control input-circle-right" name="email" required>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label">Phone</label>
													<div class="col-md-4">
														<div class="input-group">
															<input type="text" class="form-control input-circle-left phone" name="phone">
															<span class="input-group-addon input-circle-right">
															<i class="fa fa-phone" aria-hidden="true"></i>
															</span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label">Username</label>
													<div class="col-md-4">
														<input type="text" class="form-control input-circle" name="username" required>
														<!--<span class="help-block">
														A block of help text. </span>-->
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label">Create Password</label>
													<div class="col-md-4">
														<div class="input-group">
															<input type="password" class="form-control input-circle-left" name="password" min="8" required>
															<span class="input-group-addon input-circle-right">
															<i class="fa fa-key" aria-hidden="true"></i>
															</span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label">Confirm Password</label>
													<div class="col-md-4">
														<div class="input-group">
															<input type="password" class="form-control input-circle-left" name="c_password" min="8" required>
															<span class="input-group-addon input-circle-right">
															<i class="fa fa-key" aria-hidden="true"></i>
															</span>
														</div>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-3 control-label">Role</label>
													<div class="col-md-4">
														<select  class="form-control input-circle" name="role" required>
															<option value="">Select One</option>
															<option value="1">Regular User</option>
															<option value="2">Admin User</option>
														</select>
														<!--<span class="help-block">
														A block of help text. </span>-->
													</div>
												</div>
												
											
											</div>
											<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
														<button type="submit" class="btn btn-circle blue">Submit</button>
														<button type="button" class="btn btn-circle default">Cancel</button>
													</div>
												</div>
											</div>
										</form>
										<!-- END FORM-->
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="user_content">
					<div class="portlet-body rc_class">
						<div class="filter_user_status">
								<div class="form-group">
									<div class="col-xs-12">
									<label>Status: </label><select id="filter_user_status" name="filter_user_status" class="form-control">
										<option value="">Select All</option>
										<option value="1">Active</option>
										<option value="2">Inactive</option>
									</select>
									</div>
								</div>
						</div>
						<table id="user_con_datatable" class="table table-bordered table-striped table-condensed flip-content">
							<thead class="flip-content">
								<tr>
									<!-- <th>Sr No.</th> -->
									<th>Last Name</th>
									<th>First Name</th>
									<!--<th>Middle Initial</th>-->
									<th>User Name</th>
									<th>Role</th>
									<th style="width:10%;">Phone</th>
									<th>E-mail</th>
									<!-- <th>Role</th>
									<th>Save/Delete</th>
									<th>Account</th> -->
									<th>Status</th>
									<th>Action</th>
									<th class="statusP">statusP</th>
									<!--<th>Edit</th>-->								
									<!--<th>Action</th>-->
								</tr>
							</thead>
							<tbody>
							<?php
							foreach($fetch_users as $key => $row){
								if($row->superadmin == 1){
										$class = 'row_readonly';
									}else{
										$class = 'row_readonly';
									}
								?>
								<tr class="<?php echo $class; ?>">
									
									
							
									<td><?php echo $row->lname; ?> </td>
									<td><?php echo $row->fname; ?></td>
									<td><?php echo $row->email; ?></td>
									<td>
										<?php if($row->role == '1')
										{
											echo 'User';
										}
										else if($row->role == '2')
										{
											echo 'Admin';
										}											
											?>
									</td>
									<td><?php echo $row->phone; ?></td>
									<td><?php echo $row->email_address; ?></td>
									 <td><?php if($row->account=='1'){
									 	echo "Active";

									 }elseif($row->account=='2'){
										
										echo "Inactive";

									}else{
										echo "Select One";

									}?>										
									</td>
									 <?php									      
									      if($this->session->userdata('user_role') == 2){

									      	?>
									     
									     <td>
									     	<input type="hidden" value="<?php echo $row->id;?>" name="user_id">
											<a href="<?php echo base_url();?>edit_users/<?php echo $row->id;?>" title="edit" style="text-align:center;"><i class="fa fa-cog fa-lg"></i></a>
										</td>
									<?php  }else{
										echo '<td><input type="hidden" value="'.$row->id.'>" name="user_id"></td>';

									} ?>
									<td class="statusP"><?php echo $row->account; ?></td>										
									</tr>
								<?php } ?>
								</tbody>
							</table>
					
							<button type="button" class="btn btn blue" onclick="open_popup(this)" style="padding:8px; "> Add User </button>

					</div>
					
					</div>
					
					
				</div>
				
<script>


function open_popup(that)
{

$('#a').removeClass('expand');
$('#a').addClass('collapse');
$('div.form').css('display','block');


}

$('.phone').keyup(function(){

	var val = this.value.replace(/\D/g, '');
	 if(val.length < 10)
	 {
        var newVal = '';
        while (val.length > 3) {
          newVal += val.substr(0, 3) + '-';
          val = val.substr(3);
        }
        newVal += val;
        this.value = newVal;
	 }
	 else
	 {
     this.value = this.value.replace(/(\d{3})\-?(\d{3})\-?(\d{4})/,'$1-$2-$3');
	 }
});

$(document).ready(function(){
	$('tr.row_readonly td input').attr('readonly',true);
	$('tr.row_readonly td button').attr('disabled',true);
})

function lock_this_user(id){

	var value_ac = 'lock';
	var ids = id;

	//alert(ids);
	//alert(value_ac);


	if(confirm('Are you sure to lock this account?')){

		$.ajax({
					type : 'POST',
					url  : '<?php echo base_url()."Users/lock_accounts";?>',
					data : {'value':value_ac,'t_id':ids},
					success : function(response){
						if(response == 1){

							window.location.reload();
						}	
					}
		});

	}
}

function unlock_this_user(id){

	
	var ids = id;
	var value_ac = 'unlock';

	//alert(value_ac);
	//alert(ids);

	if(confirm('Are you sure to unlock this account?')){

		$.ajax({
					type : 'POST',
					url  : '<?php echo base_url()."Users/lock_accounts";?>',
					data : {'value':value_ac, 't_id':ids},
					success : function(response){
						if(response == 1){

							window.location.reload();
						}	
					}
		});

	}
}

$(document).ready(function(){


	var table = $('#user_con_datatable').DataTable( {
	    responsive: true,
		    'columnDefs': [ {
	        'targets': [4,7], // column index (start from 0)
	        'orderable': false, // set orderable false for selected columns
	     }]
	});
	

	$('#filter_user_status').change( function() {
		table.column( 8 ).search( $(this).val() ).draw();
    });

});
	



</script>
<style type="text/css">
.dataTable > thead > tr > th.sorting, 
.dataTable > thead > tr > th.sorting_asc, 
.dataTable > thead > tr > th.sorting_desc{
    background-repeat: no-repeat !important;
    background-position: center right !important;
}
.dataTable > thead > tr > th{
	 background: #C0C0C0;
    color: #fff;
    padding: 15px;
}
.statusP {
    display: none;
}
.filter_user_status {
    position: relative;
    bottom: 10px;
}
.filter_user_status .col-md-3.pull-right {
    padding-right: 0;
}
select#filter_user_status {
    width: 145px !important;
    float: left;
}
.filter_user_status label {
     margin: 8px 15px 0 0;
    float: left;
}
.dataTable table tr th {
    color: #fff;
    padding: 15px;
    background: #C0C0C0;
}
</style>