<?php
// echo '<pre>';
// print_r($user_setting_data);
// echo '</pre>';

if(isset($user_setting_data) && is_array($user_setting_data)){

	$ids 					= $user_setting_data[0]->id;
	$loan_organization 		= $user_setting_data[0]->loan_organization;
	$loan_servicing			= $user_setting_data[0]->loan_servicing;
	$investor_relation		= $user_setting_data[0]->investor_relation;
	$marketing				= $user_setting_data[0]->marketing;
	$lender_portal_track	= $user_setting_data[0]->lender_portal_track;
	$daily_report 			= $user_setting_data[0]->daily_report;

}else{

	$ids 					= '';
	$loan_organization 		= '';
	$loan_servicing			= '';
	$investor_relation		= '';
	$marketing				= '';
	$lender_portal_track	= '';
	$daily_report 			= '';
}

?><style type="text/css">
	h4{
		background-color: #e8e8e8;
		height: 30px;
		line-height: 30px;
	}
	
label{

	font-weight:600!important;
}

div#outer label {
    width: 99px;
    line-height: 15px;
  
    padding-top: 3px;
    font-weight:600!important;	
}	
input.form-control.test {
    width: 541px;
}

</style>
<div class="page-container">
	
	<div class="container">
			<?php if($this->session->flashdata('error')!=''){  ?>
				<div id='error'><?php echo $this->session->flashdata('error');?></div>
			<?php } ?>
			<?php if($this->session->flashdata('success')!=''){  ?>
				<div id='success'><?php echo $this->session->flashdata('success');?></div>
			<?php } ?>
<br>
			<div id="msg" style="padding-top:"></div>
			<div>&nbsp;</div>
		<div class="tab-pane">

		<form method="post" class="form-inline" action="<?php echo base_url();?>Users/update_users">

			<div class="page-head">
				<div class="col-md-6 page-title">
					<h1>Admin Settings</h1>
				</div>
				<div class="col-md-6" style="margin-top: 18px;">
 
				<button type="button"  onclick="edit_update_delete(this.value)" value="1" class="btn btn-primary pull-right" style="margin-left:10px;">Save</button>	 
				<button type="button" value="2"  onclick="edit_update_delete(this.value)" class="btn btn-primary pull-right" style="margin-left:10px;">Delete</button>	
			     <button type="button" onclick="fun_close(this)" class="btn btn-primary pull-right" >Close</button>
				<input type="hidden" name="update_ida" value="<?php echo $fetch_userss[0]->id; ?>">
				</div>
				
			</div>

			<div class="page-container">
				<h4>&nbsp; User Information:</h4>
				<div id="outer">
				<div class="row">
					<div class="col-md-3">
						<label>First Name</label>
				  <input type="text" class="form-control" value="<?php echo $fetch_userss[0]->fname; ?>" name="fname">
					</div>
					<div class="col-md-3">
						<label>Middle Initial</label>
				
				 <input type="text"  class="form-control"  value="<?php echo $fetch_userss[0]->middle_name; ?>" name="middle_name">
					</div>
					<div class="col-md-3">
				<label>Last Name</label>
					  <input type="text"  class="form-control"  value="<?php echo $fetch_userss[0]->lname; ?>" name="lname">
					</div>
					</div>

					<div class="row">&nbsp;</div>

					<div class="row">
						<div class="col-md-3">
						<label>Work Phone</label>
							 <input type="text"  class="form-control"  value="<?php echo $fetch_userss[0]->phone; ?>" name="phone">
						</div>

						<div class="col-md-3">
						<label>Ext.</label>
							 <input type="text" class="form-control"  value="<?php echo $fetch_userss[0]->Ext; ?>" name="Ext">
						</div>

					</div>

					<div class="row">&nbsp;</div>

					<div class="row">
						
					    <div class="col-md-3">
						 <label>Cell Phone</label>
						 <input type="text"  class="form-control"  value="<?php echo $fetch_userss[0]->cell_phone; ?>" name="cell_phone">
						</div>
					</div>

					<div class="row">&nbsp;</div>

					<div class="row">

					<div class="col-md-3">
					<label>Work E-Mail</label>
					<input type="text"  class="form-control"  value="<?php echo $fetch_userss[0]->email_address; ?>" name="email_address">
					</div>
				


					</div>

						<div class="row">&nbsp;</div>
					<div class="row">

						<div class="col-md-3">
					<label>Home E-Mail</label>
					<input type="text"  class="form-control"  value="<?php echo $fetch_userss[0]->personal_address; ?>" name="personal_address">
					</div>
					</div>

		</div>

			<div class="row">&nbsp;</div>
	
			<h4>&nbsp;Home Address:</h4>
			<div class="row">
		       	<div class="col-md-6">
					<label>Street Address</label>
					<input type="text"  class="form-control test"  value="<?php echo $fetch_userss[0]->home_address; ?>" name="home_address">
			  	</div>
				<div class="col-md-3">
					<label>Unit #</label>
					<input type="text"  class="form-control"  value="<?php echo $fetch_userss[0]->home_unit; ?>" name="home_unit">
			  	</div>
			</div>
			<div class="row">&nbsp;</div>
			<div class="row">
				<div class="col-md-3">
					<label>City</label>
					<input type="text"  class="form-control"  value="<?php echo $fetch_userss[0]->home_city; ?>" name="home_city">
			  	</div>
			  	<div class="col-md-3">
					<label>State</label>
					<input type="text"  class="form-control"  value="<?php echo $fetch_userss[0]->home_state; ?>" name="home_state">
			  	</div>
			   	<div class="col-md-3">
					<label>Zip</label>
					<input type="text"  class="form-control"  value="<?php echo $fetch_userss[0]->home_zip; ?>" name="home_zip">
			  	</div>
			</div>

			<div class="row">&nbsp;</div>
			<h4>&nbsp;Work Address:</h4>
				<div class="row">
					<div class="col-md-6">
						<label>Street Address</label>
						<input type="text"  class="form-control test"  value="<?php echo $fetch_userss[0]->work_address; ?>" name="work_address">
						</div>

					<div class="col-md-3">
						<label>Unit #</label>
						<input type="text"  class="form-control"  value="<?php echo $fetch_userss[0]->work_unit; ?>" name="work_unit">
				  </div>
			</div>
					
			<div class="row">&nbsp;</div>
				<div class="row">

				<div class="col-md-3">
					<label>City</label>
					<input type="text"  class="form-control"  value="<?php echo $fetch_userss[0]->work_city; ?>" name="work_city">
			  	</div>

			  	<div class="col-md-3">
					<label>State</label>
					<input type="text"  class="form-control"  value="<?php echo $fetch_userss[0]->work_state; ?>" name="work_state">
			  	</div>
 				<div class="col-md-3">
					<label>Zip</label>
					<input type="text"  class="form-control"  value="<?php echo $fetch_userss[0]->work_zip; ?>" name="work_zip">
			  	</div>

				</div>
				
			<div class="row">&nbsp;</div>
			<h4>User Settings</h4>

			<div class="row">
				<div class="col-md-4">
					<label>User Name</label>
					<input type="text" class="form-control" name="UserName" value="<?php echo $fetch_userss[0]->email;?>">
				</div>
			</div>
			<div class="row">&nbsp;</div>
		 	<div class="row">
				<div class="col-md-3">
					<label>Role</label>
					<select name="role" class="form-control">
			
						<option value="1" <?php if($fetch_userss[0]->role=='1'){echo "Selected";} ?> >User</option>		
						<option value="2" <?php if($fetch_userss[0]->role=='2'){echo "Selected";} ?>>Admin</option>		
					</select>
				</div>

				<div class="col-md-3">
					<label >Account Status</label>
			   		<select name="lacoount" class="form-control">
						<option value="">Select One</option>
						<option value="1" <?php if($fetch_userss[0]->account=='1'){echo "Selected";} ?>>Active</option>		
						<option value="2" <?php if($fetch_userss[0]->account=='2'){echo "Selected";} ?>>Inactive </option>
					</select>
				</div>
			</div>
			<div class="row">&nbsp;</div>
			
			<div class="page-container">
			<div class="row">&nbsp;</div>
			<h4>&nbsp;User Functions</h4>
		
				<div class="row">
					<div class="col-md-12">
					
						
					</div>
				
					<div class="col-md-12">	

						<input type="hidden" name="update_id" value="<?php echo $ids ? $ids : 'new';?>">
						<input type="hidden" name="user_id" value="<?php echo $this->uri->segment(2);?>">

						<input type="checkbox" onclick="loan_origination_chkbox(this);" <?php if($loan_organization == 1){echo 'checked';}?>> Loan Origination
						<input type="hidden" name="loan_origination" id="origination" value="<?php echo $loan_organization ? $loan_organization : '';?>">
						<br><br>

						<input type="checkbox" onclick="loan_servicing_chkbox(this);" <?php if($loan_servicing == 1){echo 'checked';}?>> Loan Servicing
						<input type="hidden" name="loan_servicing" id="servicing" value="<?php echo $loan_servicing ? $loan_servicing : '';?>">
						<br><br>

						<input type="checkbox" onclick="investor_relations_chkbox(this);" <?php if($investor_relation == 1){echo 'checked';}?>> Investor Relations
						<input type="hidden" name="investor_relations" id="investor" value="<?php echo $investor_relation ? $investor_relation : '';?>">
						<br><br>

						<input type="checkbox" onclick="marketing_chkbox(this);" <?php if($marketing == 1){echo 'checked';}?>> Marketing
						<input type="hidden" name="marketing" id="marketing" value="<?php echo $marketing ? $marketing : '';?>">
						<br><br>

						<input type="checkbox" onclick="lenderPortaltracking(this);" <?php if($lender_portal_track == 1){echo 'checked';}?>> Lender Portal Tracking
						<input type="hidden" name="lender_portal_track" id="lenderportaltrack" value="<?php echo $lender_portal_track ? $lender_portal_track : '';?>">
						<br><br>

						<input type="checkbox" onclick="daily_report_chkbox(this);" <?php if($daily_report == 1){echo 'checked';}?>> Daily Report
						<input type="hidden" name="daily_report" id="daily" value="<?php echo $daily_report ? $daily_report : '';?>">
						<br><br>
						
					</div>

				</div>	
			</div>

</form>
		</div>


		</div>
	</div>
</div>


<script type="text/javascript">

function loan_origination_chkbox(that){

if($(that).is(':checked')){

  $('input#origination').val('1');
	}else{
		$('input#origination').val('0');
	}
}

function loan_servicing_chkbox(that){

if($(that).is(':checked')){

	$('input#servicing').val('1');
	}else{
		$('input#servicing').val('0');
	}
}

function investor_relations_chkbox(that){

	if($(that).is(':checked')){

	$('input#investor').val('1');
		}else{
			$('input#investor').val('0');
	}
}

function daily_report_chkbox(that){

	if($(that).is(':checked')){

		$('input#daily').val('1');
	}else{
			$('input#daily').val('0');
	}
 }

function marketing_chkbox(that){

	if($(that).is(':checked')){

		$('input#marketing').val('1');
	}else{
			$('input#marketing').val('0');
	}
 }

 function lenderPortaltracking(that){

	if($(that).is(':checked')){
		$('input#lenderportaltrack').val('1');
	}else{
		$('input#lenderportaltrack').val('0');
	}
 }

function edit_update_delete(that){

var id=that;
var user_id=$('input[name="update_ida"]').val();
var fname=$('input[name="fname"]').val();
var middle_name=$('input[name="middle_name"]').val();
var lname=$('input[name="lname"]').val();
var phone=$('input[name="phone"]').val();
var Ext=$('input[name="Ext"]').val();
var email_address=$('input[name="email_address"]').val();
var role=$('select[name="role"]').val();
var lacoount=$('select[name="lacoount"]').val();

var cell_phone=$('input[name="cell_phone"]').val();
var personal_address=$('input[name="personal_address"]').val();
var work_address=$('input[name="work_address"]').val();
var work_unit=$('input[name="work_unit"]').val();
var work_city=$('input[name="work_city"]').val();
var work_state=$('input[name="work_state"]').val();
var work_zip=$('input[name="work_zip"]').val();
var home_address=$('input[name="home_address"]').val();
var home_unit=$('input[name="home_unit"]').val();
var home_city=$('input[name="home_city"]').val();
var home_state=$('input[name="home_state"]').val();
var home_zip=$('input[name="home_zip"]').val();

var loan_origination=$('input[name="loan_origination"]').val();
var loan_servicing=$('input[name="loan_servicing"]').val();
var investor_relations=$('input[name="investor_relations"]').val();
var daily_report=$('input[name="daily_report"]').val();
var marketing=$('input[name="marketing"]').val();
var lender_portal_track=$('input[name="lender_portal_track"]').val();
var update_idd=$('input[name="update_id"]').val();

   if(id==1){

		$.ajax({
		   type : 'POST',
			url  : '<?php echo base_url()."Users/update_users";?>',
			data : {'user_id':user_id,'id':id,'fname':fname,'middle_name':middle_name,'lname':lname,'phone':phone,'email_address':email_address,'role':role,'lacoount':lacoount,'Ext':Ext,'cell_phone':cell_phone,'personal_address':personal_address,'work_address':work_address,'work_unit':work_unit,'work_city':work_city,'work_state':work_state,'work_zip':work_zip,'home_address':home_address,'home_unit':home_unit,'home_city':home_city,'home_state':home_state,'home_zip':home_zip,'loan_origination':loan_origination,'loan_servicing':loan_servicing,'investor_relations':investor_relations,'daily_report':daily_report,'marketing':marketing,'lender_portal_track':lender_portal_track,'update_idd':update_idd},
					success : function(response){
						if(response == 1){

							$('div#msg').text('User updated successfully').addClass('alert alert-success');
						}	
					}
		});


   }else if(id==2){
    
	if(confirm("Are you sure you want to remove this item")){

		$.ajax({
					type : 'POST',
					url  : '<?php echo base_url()."Users/update_users";?>',
					data : {'user_id':user_id,'id':id},
					success : function(response){
						if(response == 2){

							window.location.href = "<?php echo base_url().'users'?>";
						}	
					}
		});

	}

   }

	}

	function fun_close(that){

		window.location.href = "<?php echo base_url().'users'?>";
	}


</script>