<?php
// echo '<pre>';
// print_r($user_setting_data);
// echo '</pre>';

if(isset($user_setting_data) && is_array($user_setting_data)){

	$ids 					= $user_setting_data[0]->id;
	$loan_organization 		= $user_setting_data[0]->loan_organization;
	$loan_servicing			= $user_setting_data[0]->loan_servicing;
	$investor_relation		= $user_setting_data[0]->investor_relation;
	$daily_report 			= $user_setting_data[0]->daily_report;

}else{

	$ids 					= '';
	$loan_organization 		= '';
	$loan_servicing			= '';
	$investor_relation		= '';
	$daily_report 			= '';
}

?>
<style type="text/css">
	h4{
		background-color: #e8e8e8;
		height: 30px;
		line-height: 30px;
	}

</style>
<div class="page-container">
	
	<div class="container">
			<?php if($this->session->flashdata('error')!=''){  ?>
				<div id='error'><?php echo $this->session->flashdata('error');?></div>
			<?php } ?>
			<?php if($this->session->flashdata('success')!=''){  ?>
				<div id='success'><?php echo $this->session->flashdata('success');?></div>
			<?php } ?>
		<div class="tab-pane">

		<form method="post" action="<?php echo base_url();?>Users/add_setting">

			<div class="page-head">
				<div class="col-md-6 page-title">
					<h1>User Settings</h1>
				</div>
				<div class="col-md-6" style="margin-top: 18px;">
					<input type="submit" name="submit" value="Save" class="btn btn-primary pull-right">	
				</div>
				
			</div>

			<div class="page-container">
				<h4>&nbsp; User Settings Information:</h4>

				<div class="row">
					<div class="col-md-12">
						<label style="font-size:16px;">User Options:</label>
					</div>
					<br><br>
					<div class="col-md-12" style="margin-left: 20px;">	

						<input type="hidden" name="update_id" value="<?php echo $ids ? $ids : 'new';?>">
						<input type="hidden" name="user_id" value="<?php echo $this->uri->segment(2);?>">

						<input type="checkbox" onclick="loan_origination_chkbox(this);" <?php if($loan_organization == 1){echo 'checked';}?>> Loan Origination
						<input type="hidden" name="loan_origination" id="origination" value="<?php echo $loan_organization ? $loan_organization : '';?>">
						<br><br>

						<input type="checkbox" onclick="loan_servicing_chkbox(this);" <?php if($loan_servicing == 1){echo 'checked';}?>> Loan Servicing
						<input type="hidden" name="loan_servicing" id="servicing" value="<?php echo $loan_servicing ? $loan_servicing : '';?>">
						<br><br>

						<input type="checkbox" onclick="investor_relations_chkbox(this);" <?php if($investor_relation == 1){echo 'checked';}?>> Investor Relations
						<input type="hidden" name="investor_relations" id="investor" value="<?php echo $investor_relation ? $investor_relation : '';?>">
						<br><br>

						<input type="checkbox" onclick="daily_report_chkbox(this);" <?php if($daily_report == 1){echo 'checked';}?>> Daily Report
						<input type="hidden" name="daily_report" id="daily" value="<?php echo $daily_report ? $daily_report : '';?>">
						<br><br>
						
					</div>

				</div>	
			</div>
		</form>	

		</div>
	</div>
</div>
<script type="text/javascript">
	
	function loan_origination_chkbox(that){

		if($(that).is(':checked')){

			$('input#origination').val('1');
		}else{
			$('input#origination').val('0');
		}
	}

	function loan_servicing_chkbox(that){

		if($(that).is(':checked')){

			$('input#servicing').val('1');
		}else{
			$('input#servicing').val('0');
		}
	}

	function investor_relations_chkbox(that){

		if($(that).is(':checked')){

			$('input#investor').val('1');
		}else{
			$('input#investor').val('0');
		}
	}

	function daily_report_chkbox(that){

		if($(that).is(':checked')){

			$('input#daily').val('1');
		}else{
			$('input#daily').val('0');
		}
	}
</script>