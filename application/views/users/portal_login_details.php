<style type="text/css">
	div.dataTables_info {
	    margin-left: -12px;
	}

	div#table_filter{
		margin-top: -38px !important;
		margin-right: -20px !important;
	}
</style>
<div class="container">

	<div class="col-md-6">
		<h3><b>Login Details</b></h3>
		<table id="table" class="table table-bordered table-striped table-condensed flip-content">
			<thead class="thead_dark_blue">
				<th>Name</th>
				<th>Date</th>
				<th>Login</th>
				<th>Logout</th>
			</thead>
			
			<tbody>
			<?php
			//print_r($portal_login_detailss);
			foreach($portal_login_detailss as $key => $row)
			{

			?>
				<tr>
					<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row->p_user_id;?>"><?php echo $user_data[$row->p_user_id]['name']; ?></a></td>
					<td><?php echo date('m-d-Y' ,strtotime($row->date)); ?></td>
					<td><?php echo $row->time; ?></td>
					<td><?php echo $row->timeout; ?></td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>

	<div class="col-md-6">
		<h3><b>Trust Deed Views</b></h3>
		<table class="table_trust table table-bordered table-striped table-condensed flip-content">
			<thead class="thead_dark_blue">
				<th>Contact Name</th>
				<th>Property Address</th>
				<th>Views</th>
				<th>Date</th>
				<th>Time</th>
			</thead>
			
			<tbody>
			<?php
			//print_r($portal_login_detailss);
			foreach($trust_deed_views as $key => $row)
			{

			?>
				<tr>
					<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row->p_user_id;?>"><?php echo $user_data[$row->p_user_id]['name']; ?></a></td>
					<td><?php echo $row->prop_address; ?></td>
					<td><?php echo $row->view_count; ?></td>
					<td><?php echo date('m-d-Y' ,strtotime($row->date)); ?></td>
					<td><?php echo $row->time; ?></td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>


	<div class="row"><br></div>
	<div class="row">
		<div class="col-md-12">
			<h3><b>Online Subscriptions</b></h3>
			<table class="table table_trust table-bordered table-striped table-condensed flip-content">
				<thead class="thead_dark_blue">
					<th>Street Address</th>
					<th>Contact Name</th>
					<th>Contact Phone</th>
					<th>Contact E-mail</th>
					<th>Lender Name</th>
					<th>Amount</th>
					<th>Status</th>
					<th>Date</th>
				</thead>
				
				<tbody>
					<?php if(isset($subscriptionsData) && is_array($subscriptionsData)){ 
							foreach ($subscriptionsData as $row) {
					?>
						<tr>
							<td><a href="<?php echo base_url();?>load_data/<?php echo $row->loan_id;?>"><?php echo $row->street_address;?></a></td>
							<td><a href="<?php echo base_url();?>viewcontact/<?php echo $row->contactID;?>"><?php echo $user_data[$row->contactID]['name']; ?></a></td>
							<td><?php echo $user_data[$row->contactID]['phone']; ?></td>
							<td><a href="mailto:<?php echo $user_data[$row->contactID]['email']; ?>"><?php echo $user_data[$row->contactID]['email']; ?></a></td>
							<td><a href="<?php echo base_url();?>investor_view/<?php echo $row->lender_id;?>"><?php echo $row->ts_lenderName;?></a></td>
							<td><a href="<?php echo base_url();?>load_data/<?php echo $row->loan_id;?>#assigment">$<?php echo number_format($row->investment,2);?></a></td>
							<td><?php echo $assi_status[$row->ts_id];?></td>
							<td><?php echo date('m-d-Y', strtotime($row->datetime));?></td>
						</tr>
					<?php } }else{ ?>
						<tr>
							<td colspan="8">No data found!</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>

</div>
<script>
$(document).ready(function() {
    $('#table').DataTable({
    	"aaSorting": false,
    	"lengthChange": false,
    	//"searching": false,
    });

    $('.table_trust').DataTable({
    	"aaSorting": false,
    	"lengthChange": false,
    	"searching": false,
    });


    $('input[type="search"]').focus();
});

</script>