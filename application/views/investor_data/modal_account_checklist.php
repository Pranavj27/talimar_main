<style type="text/css">
	input[type=checkbox], input[type=radio] {
    margin: 4px 0 0;
    margin-top: 1px \9;
    line-height: normal;
    height: 26px;
    width: 20px;
}
</style>
<div class="modal fade" id="account_checklist" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-body">
				<form method="POST" action="<?php echo base_url()?>Investor_data/add_account_checklist" id="wholesale_deal_frm" enctype="multipart/form-data">	
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Lender Accounts</h4>
						<h4 class="modal-title">Lender Name: <?php echo $search_investor_name; ?></h4>
					</div>
					<!-----------Hidden fields----------->
					<input type="hidden" name = "checklist_investor_id" id="checklist_investor_id" value = "<?php echo $search_investor_id; ?>">
					<input type="hidden" name = "checklist_total_count" id="checklist_total_count" value = "<?php echo count($lender_account_checklist); ?>">
					<!-----------Pu content here----------->
					<div class="row">
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							<thead>
								<tr>
									<th>Completed</th>
									<th>Not Applicable</th>
									<th style="float: left">Item</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$countCompleteCheck=0;
								foreach ($lender_account_checklist as $key => $row) {
									$completedCheck="";
									$completedDisabled="disabled";
									$incompletedCheck="";
									$incompletedDisabled="disabled";
									$dbIncreId="";
									if(!empty($checkListResult)){
										foreach ($checkListResult as $result_key => $result_value) {
											if($key==$result_value->hud){
												$dbIncreId=$result_value->id;
												if($result_value->completed=="1"){
													$completedCheck="checked";
													$completedDisabled="";
													$countCompleteCheck++;
													break;
												}else if($result_value->not_applicable=="1"){
													$incompletedCheck="checked";
													$incompletedDisabled="";	
													$countCompleteCheck++;												
													break;
												}else{
													$completedDisabled="";
													$incompletedDisabled="";
												}
											}
										}
									}else{
										$completedDisabled="";
										$incompletedDisabled="";
									}
									
									$style_bold="";
									$style_show="";
									if($key=="1008"){
										$style_bold='style="font-weight: bold;"';
										$style_show='style="display: none;"';
										$incompletedDisabled="disabled";
										if($countCompleteCheck>=(count($lender_account_checklist)-1)){
											$completedDisabled="";
										}else{
											$completedDisabled="disabled";
										}
										
									}
									?>
									<tr>
										<td >
											<input type="hidden" name="checklist_id_<?php echo $key;?>" id="checklist_id_<?php echo $key;?>" value="<?php echo $dbIncreId;?>">
											<input type="checkbox" class="get_total_complete_check" name="checklist_complete_<?php echo $key;?>" id="checklist_complete_<?php echo $key;?>" <?php if($completedCheck=="checked"){ echo "checked"; } if($completedDisabled=="disabled"){ echo " disabled"; }?> onchange="get_other_disabled('<?php echo $key;?>');" value="1">
										</td>
										<td>
											<input type="checkbox" class="get_total_complete_check" name="checklist_not_applicable_<?php echo $key;?>" id="checklist_not_applicable_<?php echo $key;?>" <?php if($incompletedCheck=="checked"){ echo "checked"; } if($incompletedDisabled=="disabled"){ echo " disabled"; }?> onchange="get_other_disabled('<?php echo $key;?>');" value="1" <?php echo $style_show;?>>
										</td>
										<td style="float: left" >
											<span <?php echo $style_bold;?>><?php echo $row;?></span>
											<input type="hidden" name="checklist_name_<?php echo $key;?>" id="checklist_name_<?php echo $key;?>" value="<?php echo $row;?>" >
										</td>
									</tr>

									<?php
								}?>
							</tbody>
						</table>
						<?php
						/*foreach ($lender_account_checklist as $key => $row) {
							foreach ($checkListResult as $re_key => $value) {
								if($key==$value->hud){
									$completedCheck="";
									$completedCheck="disabled";
									if($value->completed=="1"){
										$completedCheck="checked";
										$completedCheck="";
									}
									
								}
							}
							
						}
						echo "<pre>";
						print_r($lender_account_checklist);*/
						?>
					</div>
					
					<!-----------Pu content here----------->
					<br>
					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn blue">Save</button>					
					</div>
				</form>
			</div>	
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<script type="text/javascript">
	function get_other_disabled(row) {
		var totalCount=$("#checklist_total_count").val();
		if($("#checklist_complete_"+row).prop('checked') == true){
		    $('#checklist_not_applicable_'+row).prop('checked', false);
		    $('#checklist_not_applicable_'+row).prop('disabled', true);
		}
		else if($("#checklist_not_applicable_"+row).prop('checked') == true){
			$('#checklist_complete_'+row).prop('checked', false);
		    $('#checklist_complete_'+row).prop('disabled', true);
		}else{
			 $('#checklist_complete_'+row).prop('disabled', false);
			 $('#checklist_not_applicable_'+row).prop('disabled', false);
		}
		var count=0;
		$(".get_total_complete_check").each(function() {
			if($(this).is(':checked'))
			{
				var select_id_name=$(this).attr('id');
				if(select_id_name!="checklist_complete_1008"){
					count=parseInt(count)+1;
				}
			}
		});
		totalCount=parseInt(totalCount)-1;
		if(totalCount==count){
			$('#checklist_complete_1008').prop('disabled', false);
		}
		else{
			$('#checklist_complete_1008').prop('checked', false);
			$('#checklist_complete_1008').prop('disabled', true);
		}
	}
</script>
<script type="text/javascript">
</script>