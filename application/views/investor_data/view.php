<?php
$yes_no_option 						= $this->config->item('yes_no_option');
$yes_no_option1 					= $this->config->item('yes_no_option1');
$STATE_USA 							= $this->config->item('STATE_USA');
$company_type_option 				= $this->config->item('company_type_option');
$investor_type_option 				= $this->config->item('investor_type_option');
$entity_type_option 				= $this->config->item('entity_type_option');
$account_type_option 				= $this->config->item('account_type_option');
$payment_option_distribution 		= $this->config->item('payment_option_distribution');
$lender_contact_role 				= $this->config->item('lender_contact_role');
$yes_no_with_not_applicable			= $this->config->item('yes_no_with_not_applicable');
$link_to_lender						= $this->config->item('link_to_lender');
$Broker_Authorization_option        = $this->config->item('Broker_Authorization_option');

$active_count 						= isset($fetch_active_loan) ? $fetch_active_loan['count_loan'] : 0;
$active_investment 					= isset($fetch_active_loan) ? $fetch_active_loan['total_investment'] : 0;
$active_total_loan_amount 			= isset($fetch_active_loan) ? $fetch_active_loan['total_loan_amount'] : 0;
						
$paidoff_count 						= isset($fetch_paidoff_loan) ? $fetch_paidoff_loan['count_loan'] : 0;
$paidoff_investment 				= isset($fetch_paidoff_loan) ? $fetch_paidoff_loan['total_investment'] : 0;
$paidoff_total_loan_amount 			= isset($fetch_paidoff_loan) ? $fetch_paidoff_loan['total_loan_amount'] : 0;

$avg_loan_active 					= isset($avg_loan_active) ? $avg_loan_active : 0;
$avg_loan_paid						= isset($avg_loan_paid) ? $avg_loan_paid : 0;

$investor_id				= $investor_idd;
$idInve = $fetch_investor_data['id'];	
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');

if($btn_name == 'view'){
	$fields_disabled 	= 'readonly ';
	$class_disabled 	= 'disabled ';

}else{
	$fields_disabled 	= ' ';
	$class_disabled 	= ' ';
			
}
?>

<style>
	div#model_left_align {
    margin-bottom: -12px;
    
    margin-top: -20px;
 
}
#c2_information{
	display:none;
}
.border-top h4{
    background-color: #e8e8e8;
    height: 30px;
    line-height: 30px;
	padding-left: 15px !important;
}
.view_inputs{
	font-size:14px !important;
}
div#page-invester-view .row.button_section_div {
    padding: 0px 180px;
}
div.lender_information_div
{
	border-top:none;
}
label {
    font-weight: 600 !important;
}

table.mortgageFundAccounts {
  border: 1px solid black;
  border-collapse: collapse;
  font-size: 10px;
}
.main_left_div{
	float:left;
	width:70%;
	border-right:1px solid #eee;
}
.main_right_div{
	float:right;
	width:30%;
	margin-top:65px;

}
.page-head {
    border-bottom: 1px solid #eee;

}

h4{
	width:100% !important;
}

.row.top {
    margin: 12px 0px 0px;
}
.row{
	padding: 11px 0px !important;
}.doc-div{
	margin-bottom: 15px;
}
.row>h4 {
    font-family:sans-serif;
}
table td, th{
	border : none !important;
}
.row.table-borrower table {
    margin-left: 15px !important;
}

.col-md-6.ds {
    margin-left: 15px!important;
}

a.btn.btn-danger.borrower_save_button{

	float: right !important;
	border-radius: 0px;
	margin-top: 3px;
}

.table-scrollable{
	border: 0px !important;
	margin-top: -30px !important;
}

</style>
<div class="page-container" id="page-invester-view">
			<!-- BEGIN PAGE HEAD -->
		
			<div class="container">
			<!--------------------MESSEGE SHOW-------------------------->
			<?php if($this->session->flashdata('error')!=''){  ?>
				<div id='error'><i class='fa fa-thumbs-o-down'></i>&nbsp;<?php echo $this->session->flashdata('error');?></div><?php } ?>
			<?php if($this->session->flashdata('success')!=''){  ?>
				<div id='success'><i class='fa fa-thumbs-o-up'></i>&nbsp;<?php echo $this->session->flashdata('success');?></div>
			<?php } ?>
				 
			<!-------------------END OF MESSEGE-------------------------->
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>Lender Accounts<small></small></h1>
				</div>
				
				<?php
				
				if($btn_name == 'view'){
					$btn_class = 'style ="display:block;" ';
				}else{
					$btn_class = 'style ="display:none;" ';
							
				}
				
					$page_url = $_SERVER['REQUEST_URI'];
				?>
				
				
			</div>
			
			<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					 Dashboard
				</li>
			</ul>
			
		<div class="main_left_div">
			<div class="form-group" style="float:left;position:absolute;margin-top:40px;">
				<?php
				$url_lender_name_id="";
				if(!empty($this->uri->segment(2))){
					$url_lender_name_id=$this->uri->segment(2);
					$page_url='/Pranav/Talimar/investor_view';
				}
				?>
				<form id = "lender_form"  method = "post" action = "<?php echo $page_url;?>">
					
					<b>Lender Name:</b>&nbsp;
					<select name = "search_investor_id" id="search_investor_id" onchange="investor_select(this.value)" class="selectpicker" data-live-search="true" >
						<option value=''>Select One</option>	
						<?php
						foreach($all_investor_data as $row)
						{
							
							?>
							<option value="<?= $row->id;?>" <?php if(isset($fetch_investor_data['id'])){ if($fetch_investor_data['id'] == $row->id){ echo 'selected'; }} ?> ><?= $row->name; ?></option>
							<?php
						}
						
						?>
					</select>
					<input type = "hidden" id = "url_lender_name_id" name = "url_lender_name_id" value = "<?php echo $url_lender_name_id ;?>" />
					<input type = "hidden" id = "btn_name" name = "btn_name" value = "<?php echo $btn_name ;?>" />	
				</form>
			</div>
			<div class="row">
					<div class="form-group fo" style="margin-right:14px;margin-top:26px;">
						<div class="">
							<?php
							if($btn_name == 'view'){
								$new_class = "style = 'display:block;' ";
								if($btn_name == 'new'){
									$new_class = "style = 'display:none;' ";
								}
									
								if($this->session->userdata('user_role') == 2)
								{
							?>
							<a class="btn btn-danger borrower_save_button" onclick="delete_investor('<?= $investor_id; ?>')" >Delete</a>
							<?php } ?>				
							
							<a  name = "button" class="btn btn-primary borrower_save_button" onclick = "fetch_investor_id();" href = "javascript:void(0);" >Edit</a>
								
								
							<a <?php echo $new_class;?> id=""  href="<?php echo base_url().'investor_data/new';?>" class="btn btn-primary borrower_save_button" value="add">New</a>
							
							
							<?php	}
												
							$new_class = "style = 'display:block;' ";
							if($btn_name == 'new'){
								$new_class = "style = 'display:none;' ";
							?>
							<button  type = "submit" name = "button" class="btn btn-primary borrower_save_button" value="save">Save</button>
							<a  class="btn btn-primary borrower_save_button" href = "<?php echo base_url().'investor_data';?>" >Close</a>
							
							<?php	} if($btn_name == 'edit'){ ?>
							
							<button  type = "submit" name = "button" class="btn btn-primary borrower_save_button" value="save">Save</button>
									
							<a  class="btn btn-primary borrower_save_button" href = "<?php echo base_url().'investor_data';?>" >Close</a>
								
							<?php } ?>
									
						</div>
					</div>

					<!-- Button -->
				</div>
			<form method="POST" action="<?php echo base_url();?>add_investor_data">
			
				
					
				<input type="hidden" id="investor_id" name="investor_id" value="<?= $investor_id;?>">
				<input type="hidden" id = "contact_idd" value="<?php  echo $contact_idd; ?>" name="contact_idd" />
				
				<?php 
			$qeury = $this->User_model->query("SELECT * FROM loan_assigment where lender_name='".$investor_id."'");
			
			if($qeury->num_rows() > 0)
			{
			 $fetch_query 	= $qeury->result();	
			 
			
			 $takli=$fetch_query[0]->talimar_loan;
			$wher['talimar_loan'] 	= $takli;

			$fetch_l	= $this->User_model->select_where('loan',$wher);
			
			$fetch_lon_da 	= $fetch_l->result();
			$loan_id = $fetch_lon_da[0]->id;
			$talimar_loan = $fetch_lon_da[0]->talimar_loan;
			/*$account_status_option         		= $this->config->item('account_status_option');
			$statusData = $fetch_investor_data['account_status'];*/
			}
			$account_status_option         		= $this->config->item('account_status_option');
			$statusData = $fetch_investor_data['account_status'];
			?>

				<!--
					Description : This function use for get Lender Account status
					Author      : Bitcot
					Created     : 29-04-2021
					Modified    : 
				-->
				<div class="row border-top">
					<h4>Lender Status</h4>
					<div class="col-md-6">
						<label>Account Status:</label>
						<span class="view_inputs">&nbsp;<?= isset($fetch_investor_data['account_status']) ? $account_status_option[$statusData] : '';?></span>
					</div>
					<div class="col-md-6">
						<label>Account Checklist:</label>
						<?php
						$checklist_text="";
						if(isset($lender_account_checklist)){
							if($lender_account_checklist>0){
								$checklist_style='style="color:green;" ';
								$checklist_text="Complete";								
							}else if(isset($lender_account_checklist)){
								$checklist_style='style="color:red;" ';
								$checklist_text="Not Complete";
							}
						}
						?>
						<span class="view_inputs" <?php echo $checklist_style;?>>&nbsp;<?= isset($lender_account_checklist) ? $checklist_text : '';?></span>
					</div>
				</div>
				
				<div class="row border-top">
					<h4>Lender Information</h4>
					<div class="col-md-12">
						<label>Lender Name:</label>
						<span class="view_inputs">&nbsp;<?= isset($fetch_investor_data['name']) ? $fetch_investor_data['name'] : '';?></span>
					</div>
									
					
				</div>

				<?php if($optn != 1){?>	
					<div class="row" id="investor_type_based">
						<div class="col-md-12">
							<label>Legal Entity / IRA Name:</label> 
						
							<span class="view_inputs"><?= isset($fetch_investor_data['entity_name']) ? $fetch_investor_data['entity_name'] : '';?></span>
						</div>
						
					</div>	
				<?php } ?>
				
				<div class="row">

					<div class="col-md-4">
						<label>Account #:</label> 
						<span class="view_inputs"><?= isset($fetch_investor_data['talimar_lender']) ? $fetch_investor_data['talimar_lender'] : '';?></span>
					</div>

					<div class="col-md-4">
						<label>Date Created:</label>
						<span class="view_inputs">&nbsp;<?= isset($fetch_investor_data['ldate_created']) ? date('m-d-Y', strtotime($fetch_investor_data['ldate_created'])) : '';?></span>
					</div>
				</div>
				<div class="row">
					
					<div class="col-md-4">
						<label>Lender Type:</label>
							
						<span class="view_inputs"> <?php $optn = isset($fetch_investor_data['investor_type']) ? $fetch_investor_data['investor_type'] : 0;	
								echo $investor_type_option[$optn]; 		
							?></span>
					</div>
						
					<div class="col-md-4">
						<label>Tax ID:</label> 
						<span class="view_inputs"><?= isset($fetch_investor_data['tax_id']) ? $fetch_investor_data['tax_id'] : '';?></span>
					</div>
										
				</div>
				<div class="row">
                    <div class="col-md-4">
                            <label>Date Of Trust:</label> 
                            <span class="view_inputs">
                                <?php
                                if(!empty($fetch_investor_data['date_of_trust'])){
                                    echo date('m/d/Y',strtotime($fetch_investor_data['date_of_trust']));
                                }
                                ?> 
                            </span>
                    </div>
                </div>

								
				<?php if($optn != 3){?>
					<div class="row" id="investor_type_based">
						
										
						<div class="col-md-4">
							<label>Registered State:</label> 
							<span class="view_inputs"><?php
								$optn = isset($fetch_investor_data['entity_res_state']) ? $fetch_investor_data['entity_res_state'] : 'none';
								echo $STATE_USA[$optn];		
								?></span>
						</div>
		
						<div class="col-md-4">
							<label>State Entity #:</label> 
							<span class="view_inputs"><?= isset($fetch_investor_data['state_entity_no']) ? $fetch_investor_data['state_entity_no'] : '';?></span>
				
						
						</div>
						
					</div>

					<!-- <div class="row" >
					
					</div> -->

				<?php } ?>
				<?php if($optn == 3){?>
				
				<div class="row" >
					<div class="col-md-4">
						<label>Custodian:</label> 
						<span class="view_inputs"><?= isset($fetch_investor_data['directed_IRA_NAME']) ? $fetch_investor_data['directed_IRA_NAME'] : '';?></span>
					</div>					
				</div>
				<?php } ?>
						
				<div class="row">
					<div class="col-md-8">
						<label>Street:</label> 
					<span class="view_inputs"><?= isset($fetch_investor_data['address']) ? $fetch_investor_data['address'] : '';?></span>
					</div>
					
					<div class="col-md-4">
						<label>Unit #:</label> 
						<span class="view_inputs"><?= isset($fetch_investor_data['unit']) ? $fetch_investor_data['unit'] : '';?></span>
					</div>
					
				</div>
						
					
				<div class="row ">
					<div class="col-md-4">
						<label>City:</label>
					<span class="view_inputs"> <?= isset($fetch_investor_data['city']) ? $fetch_investor_data['city'] : '';?></span>
					</div>
										
					<div class="col-md-4">
						<label>State:</label> 
					
					<span class="view_inputs"> <?php 
							$optn = isset($fetch_investor_data['state']) ? $fetch_investor_data['state'] : 'none';
							echo $STATE_USA[$optn];
						?></span>
					</div>
					
					<div class="col-md-4">
						<label>Zip:</label> 
					<span class="view_inputs"> <?php echo isset($fetch_investor_data['zip']) ? $fetch_investor_data['zip'] : '';?></span>
					</div>
					
				</div>
					

				<div class="row" id="">
					<div class="col-md-12">
						<label>Vesting:</label>
						<span class="view_inputs">  <?php echo isset($fetch_investor_data['vesting']) ? $fetch_investor_data['vesting'] : '';?></span>
					</div> 
				</div>

				<!--
					Description : This function use for get Lender Mortgage Fund add new
					Author      : Bitcot
					Created     : 29-04-2021
					Modified    : 
				-->
					
				<div class="row border-top">
					<h4>Mortgage Fund</h4>
					
					<div class="col-md-4">
						<label>Activate:</label>
						<span class="view_inputs"><?php echo ($fetch_investor_data['mortgageActivate']!=''?ucfirst($fetch_investor_data['mortgageActivate']):'No'); ?></span>
					</div>
					<div class="col-md-4">
						<label>Status:</label>
						<span class="view_inputs"><?php echo !empty($fetch_investor_data['mortgageStatus'])?$fetch_investor_data['mortgageStatus']:'';?></span>
					</div>
					<div class="col-md-4">
						<label>Amount Invested:</label>
						<span class="view_inputs">$<?php echo number_format($fetch_investor_data['amountInvested'], 2);?></span>
					</div>
				</div>
					
				<!--<div id="lender_contact_information_div">-->
				<div class="row border-top">
					<!--<h2>Primary Contact Information</h2>-->
					<h4>Lender Contacts</h4>
				</div>
				<div class="lender_information_div ">
					<?php

					
					if(isset($fetch_lender_contact))
					{
						$primary_count = 0;
						foreach($fetch_lender_contact as $key_4=> $row_data)
						{

							foreach($fetch_all_contact as $row)
							{
								if($row->contact_id == $row_data->contact_id){ 
									$cname = $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname;
								}
								
								if($row->contact_id == $row_data->contact_id){ 
									$cphone = $row->contact_phone;
								}
								
								if($row->contact_id == $row_data->contact_id){ 
									$cemail = $row->contact_email;
								}
							}
							$primary_count++;
							$contact_heading = '';
							if($key_4 == 0)
							{
								$contact_heading = 'Primary';
							}
							elseif($key_4 > 1)
							{
								$contact_heading = 'Secondary';
							}
							if($primary_count == '1'){
								$primary_count_name = 'Primary';
							}else{
								$primary_count_name = 'Secondary';
							}
								
							if($search_contact_id){
								if( $row_data->contact_id == $search_contact_id){
									$contact_class = "active-class";
								}else {
									$contact_class = "inactive-class";
								}
							}	else{
								$contact_class = "no-class";
							}

					?>
					<div class="contacts_div <?php echo $contact_class;?>">
						<div class="row">
							<div class="col-md-4">								
								<label>Contact #:</label> 
								<span class="view_inputs"> Contact <?php echo $primary_count;?></span>
							</div>
						</div>

						<div class="row <?php echo $contact_class;?>">
							
							<div class="col-md-4">								
								<label>Contact Name:</label> <a href="<?php echo base_url();?>viewcontact/<?php echo $row_data->contact_id; ?>"><span class="view_inputs"><?php echo isset($row->contact_id) ? $cname : ''; ?> &nbsp; </span><i class="fa fa-th" aria-hidden="true" style="color:#5b9bd1;"></i></a>
							</div>
							
							
							<div class="col-md-4">
								<label>Title:</label>
								<span class="view_inputs"><?php echo $row_data->c_title;?></span>
							</div>


							
							<div class="col-md-4" >
								<label>Primary Contact:</label> <?php
										if($row_data->contact_primary == '1'){
											$c_p='Yes';
										}else{
											$c_p='No';
											
										}
									?>
							
								<span class="view_inputs"><?php echo $c_p; ?></span>
							</div>
							
						</div>
						
						
						<div class="row">
							<div class="col-md-4">								
								<label>Phone:</label>
							<span class="view_inputs"><?php echo isset($row->contact_id) ? $cphone : ''; ?></span>
							</div>
							
							<div class="col-md-4">								
								<label>Email:</label> 
							<span class="view_inputs"><?php echo isset($row->contact_id) ? $cemail : ''; ?></span>
							</div>

							<div class="col-md-4">								
								<label>Link to Lender Portal:</label> 
							<span class="view_inputs"><?php echo $link_to_lender[$row_data->link_to_lender];?></span>
							</div>									
							
						</div>
						
					</div>
					<!--</div>-->
					<?php  }
						}
						
					?>
					
				</div>
						
				<div class="row border-top">
					<!--<h2>Primary Contact Information</h2>-->
					<h4>Loan Servicer Information</h4>
				</div>
							
				<div class="row">
					<div class="col-md-4">	
						<label>FCI Account:</label> 
					<span class="view_inputs"><?php
							$optn  = isset($fetch_investor_data['fci_acct_service']) ?  $fetch_investor_data['fci_acct_service'] : '';
							echo $yes_no_with_not_applicable[$optn];
						?></span>

					</div>
							
					<div class="col-md-4" >
						<label>FCI Account #:</label>
					<span class="view_inputs"> <?php echo isset($fetch_investor_data['fci_acct']) ? $fetch_investor_data['fci_acct'] : '';?></span>

					</div>
					<div class="col-md-4">	
						
					</div>
				</div>
					
				<div class="row">
					<div class="col-md-4">
						<label>Del Toro Account:</label> 
						<span class="view_inputs"><?php
							$optn = isset($fetch_investor_data['del_acct_service']) ? $fetch_investor_data['del_acct_service'] : '';
							echo $yes_no_with_not_applicable[$optn];
						?></span>
					</div>
					
					<div class="col-md-4">
						<label>Del Toro Account #:</label>
					<span class="view_inputs">  <?php echo isset($fetch_investor_data['del_toro_acct']) ? $fetch_investor_data['del_toro_acct'] : '';?>	</span>
					</div>
					<div class="col-md-4">	
						
					</div>
				</div>

			
			<?php
			
			$option = isset($fetch_investor_data['ach_disbrusement']) ? $fetch_investor_data['ach_disbrusement'] : '0';
			$checkbox = '';
			
			if($option == '1'){
				$checkbox = 'Checked';
				
			}else{
				$checkbox = '';
			}
			
			$option1 = isset($fetch_investor_data['check_disbrusement']) ? $fetch_investor_data['check_disbrusement'] : '0';
			$checkbox1 = '';
			if($option1 == '1'){
				$checkbox1 = 'Checked';
			}else{
				$checkbox1 = '';
			}
			
			?>

			
			<div class="row border-top">
				<h4>Broker Authorization and Approvals</h4>
			</div>
			<?php

				$explode_bro_talimar = explode(',', $fetch_investor_data['bro_talimar']);
				$explode_bro_lender = explode(',', $fetch_investor_data['bro_lender']);
			?>
			<?php foreach($Broker_Authorization_option as $key => $bro_opt){ 

					if(in_array($key, $explode_bro_talimar)){

						$bro_talimar = 'checked';
					}else{
						$bro_talimar = 'disabled';
					}

					if(in_array($key, $explode_bro_lender)){

						$bro_lender = 'checked';
					}else{
						$bro_lender = 'disabled';
					}

				?>
				<div class="row">
					<div class="col-md-4">
						<label><?php echo $bro_opt;?>:</label>	
					</div>

					<div class="col-md-3">
						<input type="checkbox" class="form-control" value="<?php echo $key;?>" <?php echo $bro_talimar;?>> TaliMar Financial
					</div>

					<div class="col-md-2">
						<input type="checkbox" class="form-control" value="<?php echo $key;?>" <?php echo $bro_lender;?>> Lender
					</div>
				</div>
			<?php } ?>


			<div class="row border-top">
					<h4>Monthly Disbursement Data</h4>
			</div>


			<div class="row ach_dependent">				
					
					<div class="col-md-4 ">	
					<!-- 	<input type="checkbox" class="form-control" <?php echo $checkbox; ?> disabled>		 -->			
						<label>Disbursement Type:</label>
						<span class = "view_inputs">
						<?php if($fetch_investor_data['ach_disbrusement'] == '1')
						{
						   echo 'ACH';
						}elseif($fetch_investor_data['check_disbrusement'] == '1'){
							echo "Checking";
						}else{
							echo "Not Applicable";
						}

						?>
						</span>
					</div>
					<div class="col-md-4 ach_dependent" >	

						<?php 
						if($fetch_investor_data['ach_disbrusement'] == '1')
						{
							$valaccounttype = $account_type_option[$fetch_investor_data['account_type']];

						}elseif($fetch_investor_data['check_disbrusement'] == '1'){
							$valaccounttype = 'Not Applicable';
						}else{
							$valaccounttype = '';
						}
						?>
										
						<label>Account Type:</label>
						<span class = "view_inputs"><?php echo $valaccounttype;?></span>			
					</div>
			</div>

		<?php if($option == '1'){?>

			<div class="row ach_dependent">				
					<div class="col-md-4">								
						<label>Bank Name:</label>
						<span class = "view_inputs"><?= isset($fetch_investor_data['bank_name']) ? $fetch_investor_data['bank_name'] : '';?></span>
					</div>
					<div class="col-md-4">							
						<label>Account #:</label>
						<span class = "view_inputs"><?= isset($fetch_investor_data['account_number']) ? $fetch_investor_data['account_number'] : '';?></span>
					</div>
					<div class="col-md-4">								
						<label>Routing Number:</label>
						<span class = "view_inputs"><?= isset($fetch_investor_data['routing_number']) ? $fetch_investor_data['routing_number'] : '';?></span>
					</div>
			</div>
			<div class="row ach_dependent">	
			
					</div>
					<?php } ?>
		<?php 	if($option1 == '1'){ ?>
			
			<div class="row ach_dependent">				
					<div class="col-md-4">								
						<label>Street Address:</label>
						<span class = "view_inputs"><?= isset($fetch_investor_data['mail_address']) ? $fetch_investor_data['mail_address'] : '';?></span>
					</div>
					<div class="col-md-4">							
						<label>Unit #:</label>
						<span class = "view_inputs"><?= isset($fetch_investor_data['mail_unit']) ? $fetch_investor_data['mail_unit'] : '';?></span>
					</div>
					
			</div>
			<div class="row ach_dependent">				
					<div class="col-md-4">								
						<label>City:</label>
						<span class = "view_inputs"><?= isset($fetch_investor_data['mail_city']) ? $fetch_investor_data['mail_city'] : '';?></span>
					</div>
					<div class="col-md-4">							
						<label>State:</label>
						<span class = "view_inputs"><?= isset($fetch_investor_data['mail_state']) ? $STATE_USA[$fetch_investor_data['mail_state']] : '';?></span>
					</div>
					<div class="col-md-4">							
						<label>Zip:</label>
						<span class = "view_inputs"><?= isset($fetch_investor_data['mail_zip']) ? $fetch_investor_data['mail_zip'] : '';?></span>
					</div>
			</div>
			<div class="row ach_dependent">				
					<div class="col-md-12">								
						<label>Other Instructions:</label>
						<span class = "view_inputs"><?php echo $fetch_investor_data['other_instruction'];?></span>
					</div>
					
			</div>
			<?php } ?>


			</form>	

			<div class="row border-top">
				<h4 style="margin-top: 10px !important;">Pipeline Trust Deed Portfolio</h4>
				<div class="col-md-12">	
					<table id="pipelendertable" class="table table-border table-responsive table-stripe">
						<thead>
							<tr>
								<th>Street Address</th>
								<th>Loan Amount</th>
								<th>Investment Amount</th>
								<th>Payment Amount</th>
							</tr>
						</thead>
						<tbody>
						<?php 
						if(isset($pipeline_trust_deed_portfolio) && is_array($pipeline_trust_deed_portfolio)){
								$loanAmountTotal = 0; $investmentTotal = 0; $paymentTotal = 0; 
								foreach($pipeline_trust_deed_portfolio as $row){
									$loanAmountTotal = $loanAmountTotal + (double)$row['loan_amount']; $investmentTotal = $investmentTotal + (double)$row['investment']; $paymentTotal = $paymentTotal + (double)$row['payment'];
								 ?>

									<tr>
										<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['fulladdress'];?></a></td>
										<td>$<?php echo number_format((double)$row['loan_amount'], 2);?></td>
										<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>#assignment">$<?php echo number_format((double)$row['investment'], 2);?></a></td>
										<td>$<?php echo number_format((double)$row['payment'], 2);?></td>
									</tr>
						<?php } }else{ ?>
									<tr>
										<td colspan="4">No data found!</td>
									</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="padding: 10px;">Total : <?php echo count($pipeline_trust_deed_portfolio);?></a></td>
								<td style="padding: 10px;">$<?php echo number_format($loanAmountTotal, 2);?></td>
								<td style="padding: 10px;">$<?php echo number_format($investmentTotal, 2);?></a></td>
								<td style="padding: 10px;">$<?php echo number_format($paymentTotal, 2);?></td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			
			<div class="row border-top">
				<h4 style="margin-top: 10px !important;">Active Trust Deed Portfolio</h4>
				<div class="col-md-12">	
					<table id="lendertable" class="table table-border table-responsive table-stripe">
						<thead>
							<tr>
								<th>Street Address</th>
								<th>Loan Amount</th>
								<th>Investment Amount</th>
								<th>Payment Amount</th>
							</tr>
						</thead>
						<tbody>
						<?php 
						if(isset($fetch_active_status_assigment_data) && is_array($fetch_active_status_assigment_data)){ 	$loanAmountTotal = 0; $investmentTotal = 0; $paymentTotal = 0;
								foreach($fetch_active_status_assigment_data as $row){ $loanAmountTotal = $loanAmountTotal + (double)$row['loan_amount']; $investmentTotal = $investmentTotal + (double)$row['investment']; $paymentTotal = $paymentTotal + (double)$row['payment']; ?>
									<tr>
										<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>"><?php echo $row['fulladdress'];?></a></td>
										<td>$<?php echo number_format($row['loan_amount'], 2);?></td>
										<td><a href="<?php echo base_url();?>load_data/<?php echo $row['loan_id'];?>#assignment">$<?php echo number_format($row['investment'], 2);?></a></td>
										<td>$<?php echo number_format($row['payment'], 2);?></td>
									</tr>
						<?php } }else{ ?>
									<tr>
										<td colspan="4">No data found!</td>
									</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="padding: 10px;">Total : <?php echo count($fetch_active_status_assigment_data);?></a></td>
								<td style="padding: 10px;">$<?php echo number_format($loanAmountTotal, 2);?></td>
								<td style="padding: 10px;">$<?php echo number_format($investmentTotal, 2);?></a></td>
								<td style="padding: 10px;">$<?php echo number_format($paymentTotal, 2);?></td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>

			<div class="row border-top">
				<h4 style="margin-top: 10px !important;">Lender History</h4>
				<div class="col-md-6 ds">	
					<table class="table table-border table-responsive table-stripe">
						<thead>
							<tr>
								<th>Loans</th>
								<th># of Loans</th>
								<th>$ of Loans</th>
							</tr>
						</thead>
						<tbody>
							
							<tr>
								<td>Active Loans</td>
								<td><?php echo $active_count;?></td>
								<td>$<?php echo number_format($active_investment);?></td>
							</tr>
							<tr>
								<td>Paid Off Loans</td>
								<td><?php echo $paidoff_count;?></td>
								<td>$<?php echo number_format($paidoff_investment);?></td>
							</tr>
							
							<tr>
								<th><a href="<?php echo base_url().'loan_schedule_investor'?>">Total Loans</a></th>
								<th><?php echo $active_count + $paidoff_count; ?></th>
								<th>$<?php echo number_format($active_investment + $paidoff_investment); ?></th>
							</tr>
							
						</tbody>
					</table>
				</div>
			</div>
				<!----------------- Borrower History Div Ends--------->
			
			
		</div>	
	<div class = "main_right_div">
			<div class="row top" >&nbsp;
				<!--<h4 style=" font-family: sans-serif;background-color: #e8e8e8;height: 30px;margin-top: 3px;line-height: 2;padding: 0px 21px;">Saved Documents</h4>-->
			</div>
				<!--<hr style="width:97%;margin-left:15px;float: left;">-->
						   
  

							<?php 
 							if(!empty($fetch_investor_data['id']))
							{

							?>
							<div class="row" id="model_left_align">
						
									<?php 
										/*if($fetch_lender_document){
												echo '<ul class = "borrower_document_lists" style="list-style-type: none;margin-left:-26px;">';
												foreach($fetch_lender_document as $lend_documents){
													echo '<li>
														<a href ="'.base_url().'Investor_data/investor_document_read/'.$lend_documents->id .'" target="_blank">'.basename($lend_documents->lender_document).'</a>
																						
													
														<div class = "new_filename_div'.$lend_documents->id.'" style = "display:none;">
													
															<input type = "hidden" value ="" name = "old_filename[]" id = "old_filename'.$lend_documents->id.'" />
															<input type = "hidden" value ="" name = "borrower_document_id[]" id = "borrower_document_id'.$lend_documents->id.'" />
														</div>
													</li>';
													// The location of the PDF file on the server.
												}
												echo '</ul>';
											} */
											?>
								<br><br>
								<div class="col-md-12"  style="margin-top: 10px;">
									<a data-toggle="modal" href="#new_lender_document" class="btn btn-primary">Print</a>
								</div>
								<div class="col-md-12" style="margin-top: 10px;">
									<a class="btn btn-primary" data-toggle="modal" href="#lender_document">Lender Documents</a>
								</div>
								<div class="col-md-12" style="margin-top: 10px;">
									<a class="btn btn-primary" onclick="account_checklist_load_model()">Account Checklist</a>
								</div>
							</div>
								<?php }
						else{
						
						echo "";	

						}?>
						
						
				
				
			</div>	
<div class="modal fade bs-modal-lg" id="lender_document" tabindex="-1" role="large" aria-hidden="true">
<div class="modal-dialog">
	
	
	<div class="modal-content">
		
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h4 class="modal-title">Lender Documents</h4>
		</div>
		
		<div class="modal-body">
				<?php /*New Code*/ ?>

				<div id="ct_Documentsadd" class="row">	
					<form action="<?php echo base_url('Investor_data/update_lender_documents');?>" method="POST" enctype="multipart/form-data">	
					<input type = "hidden" name = "document_id" id = "document_id" />
					<input type="hidden" value="<?php echo $investor_id;?>" name="lender_idd">					
					<div class="col-md-2">&nbsp;</div>
					<div class="col-md-8">
						<div class="ct_loan_saved_document" style="display: none;">
							<form id="uploaddpdf" method="POST">
								<input type="hidden" value="<?php echo $borrower_id;?>" name="borrower_idd">
								<div class="form-group">
									<label for="document_name">Document Name:</label>
									<input type="text" class="form-control" id="document_name" name="document_name" required>
								</div>
								<div class="form-group">
									<div id="ad">
										<label>Select Document:</label>
										<input type="file" id = "lender_upload_1" name ="lender_upload_docc[]" class="select_images_file"  required>
									</div>
								</div>
								<button type="submit" id="uploadd" name="upload_file" class="btn blue">Save</button>
							</form>
						</div>
					</div>
					<div class="col-md-2">&nbsp;</div>
					</form>
				</div>
				<div class="col-md-12 ct_Documentsadd_table">

				<table class="table table-responsive" id="loan_doc_table" style="width: 100% !important;">
					<thead>
						<tr>
							<th>Action</th>
							<th>Name of Document</th>
							<th>Uploaded Date</th>
							<th>User</th>
							<th>View</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if($fetch_lender_document){
							foreach($fetch_lender_document as $contactDocuments) {

							$FileNameGet = '';
							
							$UploadedDate = date("m-d-Y", strtotime($contactDocuments->created_at));
							
							$filename = $contactDocuments->lender_document;
							$FileArr = explode('/', $filename);
							$FileNameGet = $FileArr[count($FileArr)-1];

							$file1 = explode('/', $FileNameGet);
							$file2 = explode('.', $file1[count($file1)-1]);

							$FileNameGet = $NameUploadedcalss = $file2[0];

							if($contactDocuments->document_name){
								$NameUploaded = $contactDocuments->document_name;
							}else{
								$NameUploaded = $FileNameGet;
							}


							$up_user_id = $contactDocuments->user_id;
							$UpUserDate = $this->User_model->query("SELECT * FROM user WHERE id = '$up_user_id'")->row();
							if($UpUserDate){
								$UploadedUser = $UpUserDate->fname.' '.$UpUserDate->lname;
							}
						?>
						<tr attrremove="<?php echo $contactDocuments->id; ?>">
							<td>
								<a><i id="<?php echo $contactDocuments->id; ?>" onclick="delete_doc(this);" title = "Delete File" class = "fa fa-trash"  aria-hidden="true"></i></a>
							</td>
							<td>
								<?php echo $NameUploaded; ?>
							</td>
							<td><?php echo $UploadedDate; ?></td>
							<td><?php echo $UploadedUser; ?></td>
							<td>
								<a href="<?php echo aws_s3_document_url($contactDocuments->lender_document); ?>" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a>
							</td>
						</tr>
						<?php
							}
						}
						?>

					</tbody>
				</table>
			</div>
			<?php /* close code */ ?>
		</div>
	
		<div class="modal-footer">
			<button type="button" id="ct_loan_saved_documentadd" class="btn btn-primary">Add Document</button>
			<button type="button" class="btn default" data-dismiss="modal">Close</button>
			
		</div>
	</div>
	<!-- /.modal-content -->

</div>
<!-- /.modal-dialog -->
</div>

</div>

</div>		
</div>	

		<div class="modal fade bs-modal-lg" id="new_lender_document" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form method="POST" action="<?php echo base_url();?>print_loan_document" id="form_print_loan_document">
					<?php 		
					
			$qeuryy = $this->User_model->query("SELECT * FROM loan_assigment where lender_name='".$investor_id."'");
			
			if($qeuryy->num_rows() > 0)
			{
			 $fetch_queryy 	= $qeuryy->result();	
	
			
			 $takli=$fetch_query[0]->talimar_loan;
			$wher['talimar_loan'] 	= $takli;

			$fetch_l	= $this->User_model->select_where('loan',$wher);
			
			$fetch_lon_da 	= $fetch_l->result();
			 $loan_id = $fetch_lon_da[0]->id;
			$talimar_loan = $fetch_lon_da[0]->talimar_loan;
			
				
				
			}?>			
						<!-----------Hidden fields----------->
						<input type="hidden" name = "talimar_no" value = "<?php if(isset($talimar_loan) && !empty($talimar_loan)){echo $talimar_loan; } ?>">
						<input type="hidden" name="loan_id" value="<?php if(isset($loan_id) && !empty($loan_id)){ echo $loan_id;} ?>">
						<input type="hidden" name="inves" value="<?php if(isset($investor_id) && !empty($investor_id)){ echo $investor_id;} ?>">
						<!-----------End of hidden Fields---->
									
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							
							<h4 class="modal-title">Lender Print Page</h4>
						</div>
						<div class="modal-body">
						 <div class="print_document_div_section">
						
						<table id="table_print_document">
							<tr>
								<td><input type="checkbox" name="setup_package">New Lender Setup Package 
								
						
								
							</tr>
							<tr>		
							<td><input type="checkbox" name="non_corcumvent_agreementt">Non Disclosure Agreement </td>
								<td><input type="checkbox" name="leder_data_re870">RE870 Disclosure
							
								
								
							</tr>
							
								<tr>
								<td><input type="checkbox" name="ach_form">ACH Deposit Form
								
								
								
							</tr>
							
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn blue" >Print</button>
				</div>
			</form>
		</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>			
	<!-----------------FORM FOR LINK CONTACT------------------->
	<form method="POST" name="search_contact" action="<?php echo base_url();?>contact" id="form_search_contact">
		<input type="hidden" name="search_contact" id="search_contact_lender">
	</form>
	<!-----------------END FORM FOR LINK CONTACT------------------->
<div id="modal_account_checklist"></div><!-- Account Checklist model load here -->
<script>
$(document).ready(function(){
	$( "#re_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
	var url_lender_name_id=$("#url_lender_name_id").val();
	if(url_lender_name_id!=""){
		$("#search_investor_id").val(url_lender_name_id).trigger('change');
	}
});


function investor_select(id)
{
	$('#lender_form').submit();
	// window.location.href = "<?php echo base_url();?>investor_data/"+id;
	
} 



function delete_investor(id)
  {
	  var investor_id = $('#investor_id').val() ? $('#investor_id').val() : id;
	  if(investor_id){
		  
	  if(confirm('Are you sure to delete this Lender?') === true)
	  {
		window.location.href = "<?php echo base_url();?>delete_investor/"+investor_id;
	  }
	 
	  }else{
		  alert('Please select Lender first!');
	  }
  }
function fetch_investor_id(){
	
	var investor_id = $('#investor_id').val();
	if(investor_id){
		$('#btn_name').val('edit');
		// $('#borrower_form').submit();
		window.location.href = "<?php echo base_url().'investor_data/edit/';?>"+investor_id;
	}else{
		alert('Please select Lender first!');
	}
	
}
$(document).ready(function(){
    var investor_type  = $("#investor_type").val();
	if(investor_type == '2' )
	{
		$('.contact_title input').prop("readonly", false);
		$('#investor_type_based input').prop("readonly", false);
		$('#investor_type_based select').attr("disabled", false);
		
	}
	else if(investor_type == '1' || investor_type == '3')
	{
		$('.contact_title input').prop("readonly", true);
		$('#investor_type_based input').prop("readonly", true);
		$('#investor_type_based input').val('');
		$('#investor_type_based select').val('');
		// $('#investor_type_based input').prop("readonly", true);
		$('#investor_type_based select').attr("disabled","disabled");
	}
	else
	{
		$('.contact_title input').prop("readonly", false);
		$('#investor_type_based input').val('');
		$('#investor_type_based input').prop("readonly", true);
		$('#investor_type_based select').val('');
		// $('#investor_type_based input').prop("readonly", true);
		$('#investor_type_based select').attr("disabled","disabled");
	}
	if(investor_type == '3'){
		$('.lender_3_otpn').show();
		$('#investor_type_based').hide();
		
	}else{
		$('.lender_3_otpn').hide();
		$('#investor_type_based').show();
		// $('input[name=custodian]').val();
		
	}
});

$('#investor_type').change(function(){ 
 var investor_type  = $("#investor_type").val();
	if(investor_type == '2' )
	{
		$('.contact_title input').prop("readonly", false);
		$('#investor_type_based input').prop("readonly", false);
		$('#investor_type_based select').attr("disabled", false);
		$('input[name=entity_name]').val('<?php echo isset($fetch_investor_data['entity_name']) ? $fetch_investor_data['entity_name']: '';?>');
		$('select[name=entity_type]').val('<?php echo isset($fetch_investor_data['entity_type'])? $fetch_investor_data['entity_type']:'' ;?>');
		$('select[name=entity_res_state]').val('<?php echo isset($fetch_investor_data['entity_res_state']) ? $fetch_investor_data['entity_res_state']: '';?>');
		
	}
	else if(investor_type == '1'|| investor_type == '3')
	{
		$('.contact_title input').prop("readonly", true);
		$('#investor_type_based input').val('');
		$('#investor_type_based input').prop("readonly", true);
		$('#investor_type_based select').val('');
		// $('#investor_type_based input').prop("readonly", true);
		$('#investor_type_based select').attr("disabled","disabled");
	}
	else
	{
		$('.contact_title input').prop("readonly", false);
		$('#investor_type_based input').val('');
		$('#investor_type_based input').prop("readonly", true);
		$('#investor_type_based select').val('');
		// $('#investor_type_based input').prop("readonly", true);
		$('#investor_type_based select').attr("disabled","disabled");
	}
	if(investor_type == '3'){
		$('.lender_3_otpn').show();
		$('#investor_type_based').hide();
	}else{
		$('.lender_3_otpn').hide();
		$('#investor_type_based').show();
	
		
	}
});

$(document).ready(function(){
	$("#dob").datepicker({
		
	dateFormat :"mm-dd-yy"});
});

  $('.phone').keyup(function()
  {
	  var val = this.value.replace(/\D/g, '');
	 if(val.length < 10)
	 {
        var newVal = '';
        while (val.length > 3) {
          newVal += val.substr(0, 3) + '-';
          val = val.substr(3);
        }
        newVal += val;
        this.value = newVal;
	 }
	 else
	 {
     this.value = this.value.replace(/(\d{3})\-?(\d{3})\-?(\d{4})/,'$1-$2-$3');
	 }
    //alert ("OK");
  });
  
  $(document).ready(function(){
	  var contact_2 = '<?php echo isset($fetch_investor_data['c2_first_name']) ? $fetch_investor_data['c2_first_name'] : '0'; ?>';
	  if(contact_2!= '' && contact_2!= '0' )
	  {
		  $('#c2_information').css('display','block');
		  $('#add_another_person').css('display','none');
	  }
  });
  function show_contact_2()
  {
	  // alert('onclick working');
	  $('#c2_information').slideDown('slow');
	  $('#add_another_person').css('display','none');
  }
  
  $('#ach_deposit').change(function(){
	  var ach = $('#ach_deposit').val();
	  if(ach == '' || ach == 0)
	  {
		  // When select None from Payment Distribution 
		  // Hide ach dependent input box
		 $('.ach_dependent input').val(''); 
		 $('.ach_dependent input').prop('readonly',true); 
		 $('.ach_dependent select').prop('disabled',true); 
		 $('#ach_deposit').prop('disabled',false); 		 		 
		 $('.ach_dependent').css('display','none');
		 $('.acct_field').css('display','none');

		 // Hide mail dependent input box
		 $('div.mail_payment_distribution_depend input').prop('readonly',true);
		 $('div.mail_payment_distribution_depend input').prop('disabled',true);
		 $('div.mail_payment_distribution_depend').css('display','none');

	  }
	  else if(ach == 1)
	  {
		  // Show ach dependent input box
		 $('.ach_dependent input').prop('readonly',false); 
		 $('.ach_dependent select').prop('disabled',false); 
		 $('#ach_deposit').prop('disabled',false); 		  		 
		 $('.ach_dependent').css('display','block'); 
		 $('.acct_field').css('display','block'); 
		  
		 // Hide mail dependent input box
		 $('div.mail_payment_distribution_depend input').prop('readonly',true);
		 $('div.mail_payment_distribution_depend input').prop('disabled',true);
		 $('div.mail_payment_distribution_depend').css('display','none');
	  }
	  else if(ach == 2)
	  {
		  // Hide ach dependent input box
		 $('.ach_dependent input').val(''); 
		 $('.ach_dependent input').prop('readonly',true); 
		 $('.ach_dependent select').prop('disabled',true); 
		 $('#ach_deposit').prop('disabled',false); 		 		 
		 $('.ach_dependent').css('display','none'); 
		 $('.acct_field').css('display','none'); 
		 
		 // Show mail dependent input box
		 $('div.mail_payment_distribution_depend input').prop('readonly',false);
		 $('div.mail_payment_distribution_depend input').prop('disabled',false);
		 $('div.mail_payment_distribution_depend').css('display','block');
	  }
	  else
	  {
		  
	  }
  });
  	   
  function checkbox_account_type(value){
	  
	 var cnt = value;
	 $('.account_type').not(this).prop('checked', false);  
	 // $('.account_type').prop('checked', false);  
	 
	 if($('#account_type_chkbox_'+cnt).prop('checked',true)){
		  $('#account_type').val(cnt);
	 }else{
		  $(".account_type").prop("checked", false);
	 }
  }
   $(document).ready(function(){
	 var ach = $('#ach_deposit').val();
	  if(ach == '' || ach == 0)
	  {
		  // When select None from Payment Distribution 
		  // Hide ach dependent input box
		 $('.ach_dependent input').val(''); 
		 $('.ach_dependent input').prop('readonly',true); 
		 $('.ach_dependent select').prop('disabled',true); 
		 $('#ach_deposit').prop('disabled',false); 		 		 
		 $('.ach_dependent').css('display','none');
		 $('.acct_field').css('display','none');

		 // Hide mail dependent input box
		 $('div.mail_payment_distribution_depend input').prop('readonly',true);
		 $('div.mail_payment_distribution_depend input').prop('disabled',true);
		 $('div.mail_payment_distribution_depend').css('display','none');

	  }
	  else if(ach == 1)
	  {
		  // Show ach dependent input box
		 $('.ach_dependent input').prop('readonly',false); 
		 $('.ach_dependent select').prop('disabled',false); 
		 $('#ach_deposit').prop('disabled',false); 		  		 
		 $('.ach_dependent').css('display','block'); 
		 $('.acct_field').css('display','block'); 
		  
		 // Hide mail dependent input box
		 $('div.mail_payment_distribution_depend input').prop('readonly',true);
		 $('div.mail_payment_distribution_depend input').prop('disabled',true);
		 $('div.mail_payment_distribution_depend').css('display','none');
	  }
	  else if(ach == 2)
	  {
		  // Hide ach dependent input box
		 $('.ach_dependent input').val(''); 
		 $('.ach_dependent input').prop('readonly',true); 
		 $('.ach_dependent select').prop('disabled',true); 
		 $('#ach_deposit').prop('disabled',false); 		 		 
		 $('.ach_dependent').css('display','none'); 
		 $('.acct_field').css('display','none'); 
		 
		 // Show mail dependent input box
		 $('div.mail_payment_distribution_depend input').prop('readonly',false);
		 $('div.mail_payment_distribution_depend input').prop('disabled',false);
		 $('div.mail_payment_distribution_depend').css('display','block');
	  }
	  else
	  {
		  
	  }
  });
  
    function add_new_lender_contact(){ 
	  // alert('gone'); 
	  var numItems = $('.lender_information_div').length;
	  // alert(numItems);
	  var label = 'Contact Name';
	  if(numItems == '0')
	  {
		  label = 'Secondary Contact Name';
	  }
	  else if(numItems > '1')
	  {
		  label = 'Secondary Contact Name';
	  }
	  // $('div#hidden_lender_contact_information_div label#contacy-label-name span').text(label);
	   $('div#hidden_lender_contact_information_div h4.count_borrower_contact').text(label);
	  
	  $('div#hidden_lender_contact_information_div').css('display','block');
      $("div#hidden_lender_contact_information_div div.lender_information_div").clone().insertAfter("div#lender_contact_information_div div.lender_information_div:last");
	  // $('div#hidden_lender_contact_information_div').css('display','none');
	}
	
	function fetch_lender_contact_data(that)
	{
		 var contact_id = that.value;
		 $.ajax({
				type: "post",
				url: "<?php echo base_url()?>Ajax_call/fetch_lender_contact_details",
				data:{'contact_id':contact_id},
				success: function(response){
					if(response){
						var data  = JSON.parse(response);
						// alert(data.phone);
						// $(that).parent().parent().parent().css('background','red');
						$(that).parent().parent().parent().find('input.contact_phone').val(data.phone);
						$(that).parent().parent().parent().find('input.contact_email').val(data.email);
						// $(that).parent().parent().parent().find('input.contact_title').val(data.title);
						$(that).parent().parent().parent().find('input.contact_re_date').val(data.re_date);
						$(that).parent().parent().parent().find('a.contact_view_link').prop('id',contact_id);
						
						
					}
				}
		 });
	 
		
	}
	
	function hide_this_contact(that)
	{
		$(that).parent().parent().parent().css('display','none');
	}
	
	function delete_this_contact(id)
	{
		if(id){
			if(confirm('Are you sure to delete') == true)
			{
				window.location.href = '<?php echo base_url();?>delete_lender_contact/D'+id;
			}
		}
	}
	
	function go_contact_link(that)
	{
		
		var contact_id = that.id; 
		// alert(contact_id);
		if(contact_id)
		{
			$('#search_contact_lender').val(contact_id);
			$('#form_search_contact').submit();
		}
	}
	
	function autofill_lender_data()
	{
		var address =	$('input[name="address"]').val();
		var unit 	=	$('input[name="unit"]').val();
		var city 	=	$('input[name="city"]').val();
		var state 	=	$('select[name="state"]').val();
		var zip 	=	$('input[name="zip"]').val();
		
		$('input[name="mail_address"]').val(address);
		$('input[name="mail_unit"]').val(unit);
		$('input[name="mail_city"]').val(city);
		$('select[name="mail_state"]').val(state);
		$('input[name="mail_zip"]').val(zip);
	}

var i= 1;
function lender_document_add_more_row(){
	  

	i++;
	 
		 // $('#add_row').css("display","block");
	 if(i < 6){
		 
	  $( ".doc-div" ).append( '<br><div class="row"><div class="col-md-4"><input type="file" id = "lender_upload_'+i+'" name ="lender_upload_docc[]" class="select_images_file" onchange="change_this_image(this)" name="lender_files[]"></div></div>' );
	 }else{
		 // $('#add_row').css("display","none");
		 // alert('no mpre row added');
	 }
	  
}
	
	function check_primary_contact(id){
		
		$('.chkbx_primary_contact').prop('checked',false);
		$('.chkbx_primary_contact_val').val('0');
		var rowcnt = id.split('_');
		
		if($('#'+id).prop('checked',true)){
			// alert('if part');
			// alert('checked checkbox number : ' + rowcnt[2]);
			$('#primary_contact_val'+rowcnt[2]).val('1');
		}
	}
</script>
<script>


$(document).ready(function() {
	 
    //$('#borrowertable').DataTable();
    $('#lendertable').DataTable({
       // "paging":   false,
       // "ordering": false,
        //"info":     false,
        "searching": false,
        "bLengthChange": false,
    });
	
});



$(document).ready(function() {
  
	$('#table').DataTable({
		//"aaSorting": false,
	});


	$('body').on('click', '#ct_loan_saved_documentadd', function(){
		$(".ct_loan_saved_document").toggle('slow');
	});

});
function delete_doc(mn){
	var name = doc_id = mn.id;
	if(confirm('Are You Sure To Delete?')){
		$.ajax({			
			type	:'POST',
			url		:'<?php echo base_url()."Investor_data/lender_delete_document";?>',
			data	:{'id':doc_id},
			success : function(response){
				if(response == 1){
					$('table tr[attrremove="'+name+'"]').remove();
				}
			}				
		});
	}
}

</script>
<script type="text/javascript">
	function account_checklist_load_model(){
		if($('#modal_account_checklist').html() == ''){
			var search_investor_id = $('#search_investor_id').val();
			var search_investor_name = $('#search_investor_id option:selected').text();
			$.ajax({
		        type    : 'POST',
		        url     : '/Pranav/Talimar/Investor_data/accountChecklist',
		        data    : { 'search_investor_id':search_investor_id,'search_investor_name':search_investor_name },
		        success : function(response)
		        {
		            $('#modal_account_checklist').html(response);
		        	$('#account_checklist').modal('show');
		        }
		    });
		}else{
			$('#account_checklist').modal('show');
		}
	}
</script>
<style>
	.active-class , no-class{
		display:block !important;
	}
	.inactive-class{
		display:none ;
	}
	.primary_contactdiv{
		float: left;
		width: initial !important;
		margin-top: 12px;
	}
	#uniform-primary_contact1{
		width: 20px !important;
	}
	.col-md-3.primary_contactdiv div {
		width: initial !important;
	}
	.col-md-3.primary_contactdiv h4 {
		margin: 0;
	}
</style>

