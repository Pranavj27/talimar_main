<?php
$yes_no_option 						= $this->config->item('yes_no_option');
$STATE_USA 							= $this->config->item('STATE_USA');
$company_type_option 				= $this->config->item('company_type_option');
$investor_type_option 				= $this->config->item('investor_type_option');
$entity_type_option 				= $this->config->item('entity_type_option');
$account_type_option 				= $this->config->item('account_type_option');
$payment_option_distribution 		= $this->config->item('payment_option_distribution');
$lender_contact_role 				= $this->config->item('lender_contact_role');
$yes_no_with_not_applicable         = $this->config->item('yes_no_with_not_applicable');
$link_to_lender         			= $this->config->item('link_to_lender');
$account_status_option         		= $this->config->item('account_status_option');
$Broker_Authorization_option        = $this->config->item('Broker_Authorization_option');
$no_yes_option        				= $this->config->item('no_yes_option');
$ct_funding_entity					= $this->config->item('funding_entity');

$active_count 						= isset($fetch_active_loan) ? $fetch_active_loan['count_loan'] : 0;
$active_investment 					= isset($fetch_active_loan) ? $fetch_active_loan['total_investment'] : 0;
$active_total_loan_amount 			= isset($fetch_active_loan) ? $fetch_active_loan['total_loan_amount'] : 0;
						
$paidoff_count 						= isset($fetch_paidoff_loan) ? $fetch_paidoff_loan['count_loan'] : 0;
$paidoff_investment 				= isset($fetch_paidoff_loan) ? $fetch_paidoff_loan['total_investment'] : 0;
$paidoff_total_loan_amount 			= isset($fetch_paidoff_loan) ? $fetch_paidoff_loan['total_loan_amount'] : 0;

$investor_id				= $investor_idd ;
$idInve = $fetch_investor_data['id'];	
$loan_type_option = $this->config->item('loan_type_option');
$property_type_option = $this->config->item('property_modal_property_type');
	
if($btn_name == 'view'){
	$fields_disabled 	= 'readonly ';
	$class_disabled 	= 'disabled ';

}else{
	$fields_disabled 	= ' ';
	$class_disabled 	= ' ';
			
}
?>

<style>
#c2_information{
	display:none;
}
.row.button_section_div {
    padding: 13px 30px !important;
}

.active-class , no-class{
	display:block !important;
}
.inactive-class{
	display:none ;
}
.borrower_document_lists li{
	list-style: none;
	padding: 5px;
	font-size: 14px;
}
.page-head{
	border-bottom:1px solid #eee;
	border-bottom:1px solid #eee;
}

#uniform-primary_contact1{
	width: 20px !important;
}


.borrower_data_row{
	padding: 11px 0px !important;
}
div#bordered_div_section{
	margin:0 !important;
}
.bs-searchbox {
    width: 100% !important;
}
table td, th{
	border : none !important;
}


.borrower_data_row>h4 {
    background-color: #e8e8e8;
	height: 30px;
	line-height: 30px;

}
.row.borrower_data_row label {
    
    font-weight: 600 !important;
}
.main_left_div{
	float:left;
	width:70%;
	border-right:1px solid #eee;
}
.main_right_div{
	float:right;
	width:30%;
	margin-top:60px;

}

.border-top h4{
    background-color: #e8e8e8;
    height: 30px;
    line-height: 30px;
	padding-left: 15px !important;
}
div.lender_information_div
{
	border-top:none;
}
label {
    font-weight: 600 !important;
}
.doc-div{
	margin-bottom: 15px;
}

</style>
<div class="page-container">
			<!-- BEGIN PAGE HEAD -->
		
			<div class="container">
			<!--------------------MESSEGE SHOW-------------------------->
				<?php if($this->session->flashdata('error')!=''){  ?>
				 <div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error');?></div><?php } ?>
				  <?php if($this->session->flashdata('success')!=''){  ?>
				 <div id='success'><i class='fa fa-thumbs-o-up'></i><?php echo $this->session->flashdata('success');?></div><?php } ?>
				 
			<!-------------------END OF MESSEGE-------------------------->
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>Lender Accounts</h1>
				</div>
				
				<?php
				
				if($btn_name == 'view'){
					$btn_class = 'style ="display:block;" ';
				
				}else{
					$btn_class = 'style ="display:none;" ';
							
				}
				$page_url = $_SERVER['REQUEST_URI'];
				?>
				<div class="talimar_no_dropdowns " <?php echo $btn_class;?>>
					<form id = "lender_form"  method = "post" action = "<?php echo $page_url;?>">
						
						&nbsp;&nbsp;Lender Name: 
						<select name = "search_investor_id" onchange="investor_select(this.value)" class="selectpicker" data-live-search="true" >
							<option value=''>Create New</option>	
							<?php
							foreach($all_investor_data as $row)
							{
								?>
								<option value="<?= $row->id;?>" <?php if(isset($fetch_investor_data['id'])){ if($fetch_investor_data['id'] == $row->id){ echo 'selected'; }} ?> ><?= $row->name; ?></option>
								<?php
							}
							
							?>
						</select>
					
					
						<input type = "hidden" id = "btn_name" name = "btn_name" value = "<?php echo $btn_name ;?>" />	
					</form>
				</div>
				<!-- END PAGE TITLE -->
				<!-- BEGIN PAGE TOOLBAR -->
				
				<!-- END PAGE TOOLBAR -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					 Dashboard
				</li>
			</ul>

				<form method="POST" action = "<?php echo base_url();?>add_investor_data"  onsubmit="return data_checkk();">
					
						<input type="hidden" name="investor_id" value="<?= $investor_id;?>">
						
						<input type="hidden" id = "contact_idd" value="<?php  echo $contact_idd; ?>" name="contact_idd" />
						<div class = "main_left_div">
							<div class="row button_section_div">
							<div class="form-group">
								  
								<div class="">
									<?php
									if($btn_name == 'view'){
										
										$new_class = "style = 'display:block;' ";
										if($btn_name == 'new'){
											$new_class = "style = 'display:none;' ";
										}
									
										if($this->session->userdata('user_role') == 2)
										{
												
									?>
									<a class="btn btn-primary borrower_save_button" onclick="delete_investor('<?= $investor_id; ?>')" >Delete</a>
									<?php } ?>				
							
									<a  name = "button" class="btn btn-primary borrower_save_button" onclick = "fetch_investor_id();" href = "javascript:void(0);" >Edit</a>
									
									<a <?php echo $new_class;?> id=""  href="<?php echo base_url().'investor_data/new';?>" class="btn btn-primary borrower_save_button" value="add">New</a>
									<?php	}
												
									$new_class = "style = 'display:block;' ";
									if($btn_name == 'new'){
										$new_class = "style = 'display:none;' ";
									?>
										<button  type = "submit" name = "button" class="btn btn-primary borrower_save_button" value="save">Save</button>
										<a  class="btn btn-primary borrower_save_button" href = "<?php echo base_url().'investor_view';?>" >Close</a>
							
									<?php	} if($btn_name == 'edit'){ ?>
							
										<button  type = "submit" name = "button" class="btn btn-primary borrower_save_button" value="save">Save</button>
									
										<!--<a  class="btn btn-primary borrower_save_button" href = "<?php //echo base_url().'investor_view';?>" >Close</a>-->
										
										<a class="btn btn-primary borrower_save_button" data-toggle="modal" href="#edit_lender_data">Close</a>
										
										
								
									<?php } ?>
									
								</div>  
							</div>

								<!-- Button -->
							
						</div>
						
						<!---Saved changes--->
						<div class="modal fade" id="edit_lender_data" tabindex="-1" role="basic" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content" style="top:100px !important;">
									
									<div class="modal-body">
										<div class="row" style="padding-top:8px;">
									
											<div class="col-md-8" style="padding-top:6px;"><p>Do you want to Save Information?</p></div>
											<div class="col-md-2">
												<!--<a style="margin-left:38px;" class="btn blue borrower_save_button" href="javascript:void(0);" onclick = "save_info('edit_contact')">Yes</a>-->
												
												<button style="margin-left:38px;" type = "submit" name = "button" class="btn btn-primary" value="save">Yes</button>
												
											</div>
											<div class="col-md-2">
												<a class="btn default borrower_save_button" href = "<?php echo base_url().'investor_view/'.$investor_id;?>" >No</a>
											
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					<div class="row borrower_data_row">
							<div class="errorrr alert alert-danger" style="display:none;"></div>	
						<h4>Lender Status</h4>
					</div>

					<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>Account Status:</label>
								<input type="hidden" name="previous_account_status" value="<?php if(!empty($fetch_investor_data['account_status'])){ echo $fetch_investor_data['account_status'];}?>">
								<select type="text" class="form-control" name="account_status" id="account_status">
								<?php
								foreach($account_status_option as $key => $row)
								{
									?>
									<option value="<?= $key;?>" <?php if($key == $fetch_investor_data['account_status']){echo 'selected';}?>><?= $row;?></option>
									<?php
								}
								?>
								</select>
						</div>
					</div>


					<div class="row borrower_data_row ">
							<!--<div class="errorrr alert alert-danger" style="display:none;"></div>-->	
						<h4>Lender Information</h4>
					</div>
					<div class="row borrower_data_row">
						<div class="col-md-3">
								<label>Lender Type:</label>
								<select type="text" class="form-control" name ="investor_type" id="investor_type" >
								<?php
								foreach($investor_type_option as $key => $row)
								{
									?>
									<option value="<?= $key;?>" <?php if(isset($fetch_investor_data['investor_type'])){ if($fetch_investor_data['investor_type'] == $key){ echo 'selected'; }} ?> ><?= $row;?></option>
									<?php
								}
								?>
								</select>
						</div>
						<div class="col-md-3">
							<label>Date Created:</label>
							<input type="text" name="ldate_created" class="form-control" value="<?php echo isset($fetch_investor_data['ldate_created']) ? date('m-d-Y', strtotime($fetch_investor_data['ldate_created'])) : date('m-d-Y'); ?>" readonly="readonly">
						</div>
					</div>
					<div class="row borrower_data_row">
					
						<div class="col-md-3">
							<label>Lender Name:</label>
							<input type="text" id="investor_name" class="form-control" name="investor_name" value ="<?= isset($fetch_investor_data['name']) ? $fetch_investor_data['name'] : '';?>"  />
						</div>
						<div class="col-md-3">
							<label for="" class="label_mod">Date of trust </label>
		                                    <input type="date" name="entity_date_trust" id="entity_date_trust" placeholder="Type here" class="form-control input_mod" value ="<?= isset($fetch_investor_data['date_of_trust']) ? $fetch_investor_data['date_of_trust'] : $generate_random_number;?>">
						</div>
					</div>
					<div class="row borrower_data_row">					
				
						
						<div class="col-md-3" id="tax_id">
							<label>Tax ID:</label>
							<input type="text" class="form-control" name = "tax_id" value ="<?= isset($fetch_investor_data['tax_id']) ? $fetch_investor_data['tax_id'] : '';?>">
						</div>
						<div class="col-md-3" id="tax_id">
							<label>Tax ID:</label>
							<input type="text" class="form-control" name = "tax_id" value ="<?= isset($fetch_investor_data['tax_id']) ? $fetch_investor_data['tax_id'] : '';?>">
						</div>
						
					</div>
					
				<?php //if($fetch_investor_data['investor_type'] == 1 || $fetch_investor_data['investor_type'] == 3){ ?>	

					<div class="row borrower_data_row" id="investor_type_based">
					
						<div class="col-md-9" id="entity_name_row">
							<label>Legal Entity / IRA Name: *</label>
							<input type="text" class="form-control" name = "entity_name" value ="<?= isset($fetch_investor_data['entity_name']) ? $fetch_investor_data['entity_name'] : '';?>" >
							<small>* Will appear as the company name / IRA on the signature block on the Lender Disclosures.</small>
						</div>
					</div>
					<div class="row borrower_data_row" >
						<div class="col-md-3" id="entity_res_state">
						<label>Registered State:</label>
							<select type="text" class="form-control" name = "entity_res_state" >
							<?php
							
							foreach($STATE_USA as $key => $row)
							{
								?>
								<option value="<?= $key;?>" <?php if(isset($fetch_investor_data['entity_res_state'])){ if($fetch_investor_data['entity_res_state'] == $key){ echo 'selected'; }} ?>><?= $row;?></option>
								<?php
							}
							?>
							</select>
						</div>

						<div class="col-md-3" id="state_entity_no">
							<label>State Entity Number:</label>
							<input type="text" class="form-control" name="state_entity_no" value ="<?= isset($fetch_investor_data['state_entity_no']) ? $fetch_investor_data['state_entity_no'] : '';?>" >
						</div>
					
					</div>

				<?php// } ?>

					
					
					<div class="row borrower_data_row" id="sdtc">
						<div class="col-md-3 ">
							<label>Self Directed IRA Custodian:</label>
								<input type="text" class="form-control " id="directed_IRA_NAME" name = "directed_IRA_NAME" value ="<?= isset($fetch_investor_data['directed_IRA_NAME']) ? $fetch_investor_data['directed_IRA_NAME'] : '';?>">
						</div>
						
						<div class="col-md-3 " id="self_dir_account">
							<label>Account #:</label>
							<input type="text" class="form-control " id="self_dir_account" name ="self_dir_account" value ="<?= isset($fetch_investor_data['self_dir_account']) ? $fetch_investor_data['self_dir_account'] : '';?>">
						</div>
					</div>
						
						<div class="row borrower_data_row">
							<div class="col-md-3" id="address">
								<label>Street:</label>
								<input type="text" class="form-control" name = "address" value ="<?= isset($fetch_investor_data['address']) ? $fetch_investor_data['address'] : '';?>" >
							</div>
														
							<div class="col-md-3" id="unit">
							<label>Unit #:</label>
								<input type="text" class="form-control" name = "unit" value ="<?= isset($fetch_investor_data['unit']) ? $fetch_investor_data['unit'] : '';?>">
							</div>
						</div>
						
					
					<div class="row borrower_data_row">
						<div class="col-md-3" id="city">
							<label>City:</label>
							<input type="text" class="form-control" name="city"  value ="<?= isset($fetch_investor_data['city']) ? $fetch_investor_data['city'] : '';?>" />
						</div>
						<div class="col-md-3" id="state">
						<label>State:</label>
							<select type="text" class="form-control" name = "state">
							<?php 
								foreach($STATE_USA as $key => $row)
								{
									?>
									<option value="<?= $key;?>" <?php if(isset($fetch_investor_data['state'])){ if($fetch_investor_data['state'] == $key){ echo 'selected'; }} ?> ><?= $row;?></option>
									<?php
								}
								?>
							</select>
						</div>
						<div class="col-md-3" id="zip">
						<label>Zip:</label>
						<input type="text" class="form-control" name = "zip"  value ="<?= isset($fetch_investor_data['zip']) ? $fetch_investor_data['zip'] : '';?>" />
						</div>
						
					
			

					<div class="row borrower_data_row" id="vesting_section_div">
						<div class="col-md-9" id="vesting">
							<label>Vesting:</label>
								<textarea class="form-control" rows="3" name="vesting"><?= isset($fetch_investor_data['vesting']) ? $fetch_investor_data['vesting'] : '';?></textarea>
						</div> 
					</div>
					
					<div class="row border-top">
						<h4>Mortgage Fund</h4>
						
						<div class="col-md-4">
							<label>Activate:</label>
							<select name="mortgageActivate" class="form-control" id="mortgageActivate" onchange="MortgageActiveValue(this.value)">
								<option value='no' <?php echo $fetch_investor_data['mortgageActivate']=='no'?'selected':'' ?>>No</option>
								<option value='yes' <?php echo $fetch_investor_data['mortgageActivate']=='yes'?'selected':'' ?>>Yes</option>	
							</select>
						</div>
						<div class="col-md-4">
							<label>Status:</label>
							<select name="mortgageStatus" class="form-control" id="mortgageStatus">
								<option value='' <?php echo !empty($fetch_investor_data['mortgageStatus'])?'selected':'' ?>>Select One</option>
								<option value='Processing' <?php echo $fetch_investor_data['mortgageStatus']=='Processing'?'selected':'' ?>>Processing</option>
								<option value='Active' <?php echo $fetch_investor_data['mortgageStatus']=='Active'?'selected':'' ?>>Active</option>	
							</select>
						</div>
						<div class="col-md-4">
							<label>Amount Invested $:</label>
							<input type="text" name="amountInvested" class="form-control" id="amountInvested" value="<?php echo $fetch_investor_data['amountInvested'] ?>">
						</div>
					</div>
					<br>
							
					<!--<div id="lender_contact_information_div">-->
					<div class="row borrower_data_row border-top">
						<!--<h2>Primary Contact Information</h2>-->
						<h4>Lender Contact</h4>
					</div>
					<div class="lender_information_div " id="contaact_div_new">
					<?php
					
						$primary_count = 0;
						$i=0;
		                 $co=count($fetch_lender_contact);
						if(isset($fetch_lender_contact))
						{
							foreach($fetch_lender_contact as $key_4=> $row_data)
						 {
						 	$i++;
								$primary_count ;
								$contact_heading = '';
								if($key_4 == 0)
								{
									$contact_heading = 'Primary';
								}
								elseif($key_4 > 1)
								{
									$contact_heading = 'Secondary';
								}
								if($primary_count == '1'){
									$primary_count_name = 'Primary';
								}else{
									$primary_count_name = 'Secondary';
									
								}
								
								if($search_contact_id){
											
									if( $row_data->contact_id == $search_contact_id){
										$contact_class = "active-class";
									}else {
										$contact_class = "inactive-class";
									}
								
								}	else{
									$contact_class = "no-class";
								}


								
								
					?>
					<div class="contacts_div <?php echo $contact_class;?>" id="contaact_div">
						<div class="row borrower_data_row <?php echo $contact_class;?>">
							<!--<div class="col-md-3">	
									<h4 class=""><?php echo $primary_count_name;?> Contact Name
							</div>-->
							
						</div>
						<div class="row borrower_data_row <?php echo $contact_class;?>">
							<div class="col-md-3">								
								<label>Contact Name:</label>
								
								<a onclick="delete_this_contact(<?php echo $row_data->id?>)"><i class="fa fa-trash" aria-hidden="true"></i></a>	
								
								<select class="chosen"  name="contact_id[]" onchange="fetch_lender_contact_data(this)" data-live-search="true">
									<option value="">Select One</option>
									<?php 
										foreach($fetch_all_contact as $row)
										{
									?>
									<option value="<?php echo $row->contact_id;?>" <?php if($row_data->contact_id == $row->contact_id){ echo 'selected'; } ?> ><?php echo $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname; ?></option>
									<?php
									}
									?>
								</select>	
								<a Title="View Contact" id="<?php echo $row_data->contact_id; ?>" class="contact_view_link"onclick="go_contact_link(this)" ></a>
							</div>
										
							
							<!--<div class="col-md-3">
								<label>Contact Role:</label>								
								<select name="contact_role[]" class="form-control">
								<?php foreach($lender_contact_role as $key => $row){ ?>
									<option value="<?php echo $key;?>" <?php if($key == $row_data->contact_role){echo 'Selected';}?>><?php echo $row;?></option>
								<?php } ?>
								</select>
							</div>-->
							
						
						
						
						
							
							<!--<div class="col-md-3">
								<label>RE870 Date:</label>	
								<?php
								
									if($row_data->contact_re_date && ($row_data->contact_re_date != '0000-00-00')){
										
										$contact_re_date = date('m-d-Y',strtotime($row_data->contact_re_date));
									
									}else{
										$contact_re_date = '';
									}
								
								?>		
								<input type="text" class="form-control contact_re_date" name = "contact_re_date[]" value ="<?php echo $contact_re_date; ?>" readonly>
							</div>-->

								
							<div class="col-md-3">								
								<label>Signature Authority:</label>								
								<select  class="form-control required_sign" name = "required_sign[]"  onchange="hidetitledata1111(this.value,'<?php echo $i;?>')">
									<?php
										foreach($yes_no_option as $key => $row)
										{
									?>
										<option value="<?php echo $key; ?>" <?php if($key == $row_data->required_sign){ echo 'selected'; } ?> ><?php echo $row; ?></option>
									<?php
										}
									?>
								</select>
							</div>

							<div class="col-md-3 title_ty">
								<label>Title:</label>								
								<input type="text" class="form-control contact_title" name = "contact_title[]" value ="<?php echo $row_data->c_title;?>" id="row_<?php echo $i;?>">
							</div>
							<script type="text/javascript">
									
									hidetitledata1111('<?php echo $row_data->required_sign;?>','<?php echo $i;?>');
									function hidetitledata1111(that,key){

										if(that != '1'){

											$('input#row_'+key).val('').attr('disabled',true);

										}else{
											$('input#row_'+key).attr('disabled',false);
										}

									}
								</script>

							<div class="col-md-3" id="select_div_<?php echo $i;?>">								
								<label>Primary Contact:</label>								
								<select  class="form-control pri_cont" name = "primary_contact[]" id="select_<?php echo $i;?>" >
									<?php
										foreach($yes_no_option as $key => $row)
										{
									?>
										<option value="<?php echo $key; ?>" <?php if($key == $row_data->contact_primary){ echo 'selected'; } ?> ><?php echo $row; ?></option>
									<?php
										}
									?>
								</select>
							</div>

							<div class="col-md-3" style="margin-top: 12px;">								
								<label>Link to Lender Portal:</label>								
								<select class="form-control" name = "link_lender[]"  >
									<?php
										foreach($link_to_lender as $key => $row)
										{ 
											
									?>
										<option value="<?php echo $key; ?>" <?php if($key == $row_data->link_to_lender){ echo 'selected'; } ?>><?php echo $row; ?></option>
									<?php
										} 
									?>
								</select>
							</div>
						</div>	
							<!--<div class="col-md-3 primary_contactdiv" >
								<input <?php// if($row_data->contact_primary == '1'){echo 'checked';}?> onclick = "check_primary_contact(this.id)" class = "chkbx_primary_contact" type = "checkbox" id = "primary_contact_<?php //echo $primary_count;?>" /> <span class="primary_contact_span" id="pri">Primary Contact</span>
								<input class = "chkbx_primary_contact_val" type = "hidden" value = "<?php// echo $row_data->contact_primary ? $row_data->contact_primary : 0 ?>" id = "primary_contact_val<?php// echo $primary_count;?>" name = "primary_contact[]"/>
							</div>
						</div>-->
						<!--<div class="row borrower_data_row">
							<div class="col-md-3">								
								<label>Contact's Phone:</label>								
								<input type="text" class="form-control contact_phone" name = "contact_phone[]" value ="<?php// echo $row_data->contact_phone; ?>" >
							</div>	
							
							<div class="col-md-3">								
								<label>E-Mail:</label>								
								<input type="text" class="form-control contact_email" name = "contact_email[]" value ="<?php //echo $row_data->contact_email; ?>" >
							</div>	
						</div>	-->
					<!--</div>-->
					   <?php 
					   // if($co == 1){ ?>
							<!--<div class="row borrower_data_row">
							
							<div class="col-md-3">								
								<label class="">Contact Name:</label>
								
								<!--<a onclick="hide_this_contact(this)"><i class="fa fa-trash" aria-hidden="true"></i></a>	-->								
								<!--<select class="chosen ff" id="" name="contact_id[]" onchange="fetch_lender_contact_data_new(this)" data-live-search="true" >
									<option value="">Select One</option>
									<?php 
										foreach($fetch_all_contact as $row)
										{
											?>
											<option value="<?php echo $row->contact_id;?>"><?php echo $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname; ?></option>
											<?php
										}
									?>
								</select>
								<a id="" class="contact_view_link" onclick="go_contact_link(this)"></a>
							</div>
								
							<div class="col-md-3">
								<label>Title:</label>								
								<input type="text" class="form-control contact_title" name = "contact_title[]" value ="" >
							</div>
							<!--<div class="col-md-3">
								<label>Contact Role:</label>								
								<select name="contact_role[]" class="form-control">
								<?php foreach($lender_contact_role as $key => $row){ ?>
									<option value="<?php echo $key;?>"><?php echo $row;?></option>
								<?php } ?>
								</select>
							</div>-->
							<!--<div class="col-md-3">								
								<label>Required to Sign:</label>
								<select  class="form-control required_sign" name = "required_sign[]" value ="" >
									<?php
										foreach($yes_no_option as $key => $row)
										{
									?>
										<option value="<?php echo $key; ?>"><?php echo $row; ?></option>
									<?php
									}
									?>
								</select>
							</div>
							
								
								
						</div>-->
							
						
								
				<!--
							<div class="col-md-3">
								<label>R870 Date:</label>								
								<input type="text" class="form-control contact_re_date" name = "contact_re_date[]" value ="" readonly>
							</div>-->
					
							<!--<div class="col-md-3 primary_contactdiv" >
									<input onclick = "check_primary_contact(this.id)" class = "chkbx_primary_contact" type = "checkbox" id = "pri" /> Primary Contact
									<input class = "chkbx_primary_contact_val" type = "hidden" value = "" id = "primary_contact_val1" name = "primary_contact[]"/>
							</div>-->	
						
					
						<!--<div class="row borrower_data_row">
								
						<div class="col-md-3">								
								<label>Contact's Phone:</label>								
								<input type="text" class="form-control contact_phonec" name ="contact_phone[]" value ="">
							</div>	
							<div class="col-md-3">								
								<label>E-Mail:</label>								
								<input type="text" class="form-control contact_emailc" name ="contact_email[]" value ="">
							</div>
								
						</div>-->
					
					<?php  
						//}
						 
						 }
	
					}else{
					
				
						
					?>
					
						<div class="row borrower_data_row" >
							
							<div class="col-md-3">								
								<label class="d">Contact Name:</label>
								
								<!--<a onclick="hide_this_contact(this)"><i class="fa fa-trash" aria-hidden="true"></i></a>	-->								
								<select class="chosen ff" id="cn_new" name="contact_id[]" onchange="fetch_lender_contact_data_new1(this)" data-live-search="true" >
									<option value="">Select One</option>
									<?php 
										foreach($fetch_all_contact as $row)
										{
											?>
											<option value="<?php echo $row->contact_id;?>"><?php echo $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname; ?></option>
											<?php
										}
									?>
								</select>
								<a id="" class="contact_view_link" onclick="go_contact_link(this)"></a>
							</div>
								
							
							<!--
							<div class="col-md-3">
								<label>Contact Role:</label>								
								<select name="contact_role[]" class="form-control">
								<?php foreach($lender_contact_role as $key => $row){ ?>
									<option value="<?php echo $key;?>"><?php echo $row;?></option>
								<?php } ?>
								</select>
							</div>-->
							<div class="col-md-3">								
								<label>Signature Authority:</label>
								<select  class="form-control required_sign" name = "required_sign[]" onchange="hidetitledata1(this.value);" value ="" >
									<?php
										foreach($yes_no_option as $key => $row)
										{
									?>
										<option value="<?php echo $key; ?>"><?php echo $row; ?></option>
									<?php
									}
									?>
								</select>
							</div>

							<div class="col-md-3 title_ty">
								<label>Title:</label>								
								<input type="text" class="form-control contact_title" name = "contact_title[]" value ="" id="cdd11">
							</div>

								<script type="text/javascript">
									
									function hidetitledata1(that){

										if(that != '1'){

											$('input#cdd11').val('').attr('disabled',true);

										}else{
											$('input#cdd11').attr('disabled',false);
										}

									}
								</script>
								
							<div class="col-md-3">								
								<label>Primary Contact:</label>								
								<select  class="form-control pri_cont" name = "primary_contact[]"  >
									<?php
										foreach($yes_no_option as $key => $row)
										{
									?>
										<option value="<?php echo $key; ?>" <?php if($key == $row_data->contact_primary){ echo 'selected'; } ?> ><?php echo $row; ?></option>
									<?php
										}
									?>
								</select>
							</div>

							<div class="col-md-3" style="margin-top: 12px;">								
								<label>Link to Lender Portal:</label>								
								<select class="form-control" name = "link_lender[]"  >
									<?php
										foreach($link_to_lender as $key => $row)
										{ 
											
									?>
										<option value="<?php echo $key; ?>" <?php if($key == $row_data->link_to_lender){ echo 'selected'; } ?>><?php echo $row; ?></option>
									<?php
										} 
									?>
								</select>
							</div>	
						</div>
							
						<!--<div class="row borrower_data_row">
								
							<div class="col-md-3">								
								<label>Phone:</label>								
								<input type="text" class="form-control contact_phone" name = "contact_phone[]" value ="" readonly>
							</div>	
							<div class="col-md-3">								
								<label>Email:</label>								
								<input type="text" class="form-control contact_email" name = "contact_email[]" value ="" readonly>
							</div>
						</div>
							<div class="col-md-3">
								<label>R870 Date:</label>								
								<input type="text" class="form-control contact_re_date" name = "contact_re_date[]" value ="" readonly>
							</div>-->
							
							
							<!--<div class="col-md-3 primary_contactdiv" >
									<input onclick = "check_primary_contact(this.id)" class = "chkbx_primary_contact" type = "checkbox" id = "pri" /> Primary Contact
									<input class = "chkbx_primary_contact_val" type = "hidden" value = "" id = "primary_contact_val1" name = "primary_contact[]"/>
							</div>-->	
						
					<!--</div>-->
					
						<!--<div class="row borrower_data_row">
								
						<div class="col-md-3">								
								<label>Contact's Phone:</label>								
								<input type="text" class="form-control contact_phone" name ="contact_phone[]" value ="">
							</div>	
							<div class="col-md-3">								
								<label>E-Mail:</label>								
								<input type="text" class="form-control contact_email" name ="contact_email[]" value ="">
							</div>
								
						</div>-->

<!--
						<div class="row borrower_data_row">
							
							<div class="col-md-3">								
								<label class="da">Contact Name:</label>
								
								<!--<a onclick="hide_this_contact(this)"><i class="fa fa-trash" aria-hidden="true"></i></a>	-->								
								<!--<select class="chosen ff" id="cn_newm" name="contact_id[]" onchange="fetch_lender_contact_data_new2(this)" data-live-search="true" >
									<option value="">Select One</option>
									<?php 
										foreach($fetch_all_contact as $row)
										{
											?>
											<option value="<?php echo $row->contact_id;?>"><?php echo $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname; ?></option>
											<?php
										}
									?>
								</select>
								<a id="" class="contact_view_link" onclick="go_contact_link(this)"></a>
							</div>
								
							<div class="col-md-3">
								<label>Title:</label>								
								<input type="text" class="form-control contact_title" name = "contact_title[]" value ="" >
							</div>-->
						<!--	<div class="col-md-3">
								<label>Contact Role:</label>								
								<select name="contact_role[]" class="form-control">
								<?php foreach($lender_contact_role as $key => $row){ ?>
									<option value="<?php echo $key;?>"><?php echo $row;?></option>
								<?php } ?>
								</select>
							</div>-->

							<!--<div class="col-md-3">								
								<label>Required to Sign:</label>
								<select  class="form-control required_sign" name = "required_sign[]" value ="" >
									<?php
										foreach($yes_no_option as $key => $row)
										{
									?>
										<option value="<?php echo $key; ?>"><?php echo $row; ?></option>
									<?php
									}
									?>
								</select>
							</div>
							
								
								
						</div>-->
						<!--<div class="row borrower_data_row">
								
						<div class="col-md-3">								
								<label>Contact's Phone:</label>								
								<input type="text" class="form-control contact_phones" name ="contact_phone[]" value ="">
							</div>	
							<div class="col-md-3">								
								<label>E-Mail:</label>								
								<input type="text" class="form-control contact_emails" name ="contact_email[]" value ="">
							</div>
								
						</div>-->
							
						
			<?php  } ?>
				
						<div id="hidden_lender_contact_information_div" style="display:none;" >
							<div class="row borrower_data_row">
								<!--<div class="col-md-3">	
									<h4 class="count_borrower_contact"> <span class="primary_contact_span">Contact Name</span></h4>
								</div>-->
								
							</div>
							
							<div class="row borrower_data_row">
								<div class="col-md-3">								
									<label id="contacy-label-name">Contact Name:</label>								
									<a onclick="hide_this_contact(this)"><i class="fa fa-trash" aria-hidden="true"></i></a>
									<select class="chosen" name="contact_id[]" onchange="fetch_lender_contact_data(this)">
									<option value="">Select One</option>
									<?php 
										foreach($fetch_all_contact as $row)
										{
											?>
											<option value="<?php echo $row->contact_id;?>" ><?php echo $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname; ?></option>
											<?php
										}
									?>
									</select>	
									<a id="" class="contact_view_link" onclick="go_contact_link(this)"></a>
								</div>
								
								
								<div class="col-md-3">								
									<label>Signature Authority:</label>								
										<select  class="form-control required_sign" name = "required_sign[]" value ="" onchange="hidetitledata(this.value);">
											<?php
											foreach($yes_no_option as $key => $row)
											{
												?>
												<option value="<?php echo $key; ?>"><?php echo $row; ?></option>
												<?php
											}
											?>
										</select>
								</div>
								<div class="col-md-3 title_ty">
									<label>Title:</label>								
									<input type="text" class="form-control contact_title" name = "contact_title[]" id="cdd1" value ="" >
								</div>
								<script type="text/javascript">
									
									function hidetitledata(that){

										if(that != '1'){

											$('input#cdd1').val('').attr('disabled',true);

										}else{
											$('input#cdd1').attr('disabled',false);
										}

									}
								</script>
								<div class="col-md-3">								
									<label>Primary Contact:</label>								
									<select  class="form-control pri_cont" name = "primary_contact[]"  >
										<?php
											foreach($yes_no_option as $key => $row)
											{
										?>
											<option value="<?php echo $key; ?>"><?php echo $row; ?></option>
										<?php
											}
										?>
									</select>
								</div>
								<div class="col-md-3" style="margin-top: 12px;">
									<label>Link to Lender Portal:</label>								
									<select class="form-control" name = "link_lender[]"  >
										<?php
											foreach($link_to_lender as $key => $row)
											{ 
												
										?>
											<option value="<?php echo $key; ?>" ><?php echo $row; ?></option>
										<?php
											} 
										?>
									</select>
								</div>
								<!--
							<div class="col-md-3">
									<label>Contact Role:</label>								
									<select name="contact_role[]" class="form-control">
									<?php foreach($lender_contact_role as $key => $row){ ?>
										<option value="<?php echo $key;?>"><?php echo $row;?></option>
									<?php } ?>
									</select>
								</div>-->
								
								
							</div>
							
							
						<!--	<div class="row borrower_data_row">
								<!--<div class="col-md-3">								
									<label>Phone:</label>								
										<input type="text" class="form-control contact_phone" name = "contact_phone[]" value ="" readonly>
								</div>	
								<div class="col-md-3">								
									<label>Email:</label>								
										<input type="text" class="form-control contact_email" name = "contact_email[]" value ="" readonly>
								</div>-->
								<!--
								<div class="col-md-3">
									<label>RE870 Date:</label>								
									<input type="text" class="datepicker form-control contact_re_date" name = "contact_re_date[]" value ="" readonly >
								</div>-->
						
							<!--	<div class="col-md-3 primary_contactdiv" >
									<input onclick = "check_primary_contact(this.id)" class = "chkbx_primary_contact" type = "checkbox" id = "primary_contact_2" /> Primary Contact
									<input class = "chkbx_primary_contact_val" type = "hidden" value = "" id = "primary_contact_val2" name = "primary_contact[]"/>
								</div>							
							</div>-->
							
							
					</div>	
				
				</div>


				</div>
				
					
				
				<div class="row borrower_data_row">
						<div class="col-md-3" >
						<button class="btn blue" type="button" onclick="add_new_lender_contact()">(+) Add Contact</button>
						</div>
					</div>
					
					<!--<div class="row borrower_data_row border-top">
		
						<h4>Other Lender Contacts</h4>
					</div>
					<div class="row borrower_data_row">
									<div class="col-md-3">	
                              <input type="hidden" value="" id="other_contact_val">									
								<label >Contact Name:</label>
								
														
								<select class="chosen ff" id="" name="other_contact_id" onchange="fetch_lender_contact_dataa(this)" data-live-search="true" >
									<option value="">Select One</option>
									<?php 
										foreach($fetch_all_contact as $row)
										{
											?>
											<option value="<?php echo $row->contact_id;?>" <?php if($o_contact_id==$row->contact_id){ echo "selected";}?>><?php echo $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname; ?></option>
											<?php
										}
									?>
								</select>
								<a id="" class="contact_view_link" onclick="go_contact_link(this)"><i class="fa fa-th" aria-hidden="true"></i></a>
							</div>
								
								
								<div class="col-md-3">								
								<label>Contact's Phone:</label>		
								
								<input type="text" class="form-control contact_phone_other" name ="contact_phone_other" value ="<?php echo isset($contact_phone_other) ? $contact_phone_other : '';?>">
							</div>	
							<div class="col-md-3">								
								<label>E-Mail:</label>								
								<input type="text" class="form-control contact_email_other" name ="contact_email_other" value ="<?php echo isset($contact_email_other) ? $contact_email_other: '';?>">
							</div>
							
						</div>-->
					
					
					<div class="row borrower_data_row border-top">
						<!--<h2>Primary Contact Information</h2>-->
						<h4>Loan Servicer Information</h4>
					</div>
							
						
					<div class="row borrower_data_row">
						<div class="col-md-4">	
							<label>FCI Account</label>
																		
							<select class="form-control" name="fci_acct_service" id="fci_type" onchange="hide_fci_lender(this.value);">
								
								<?php
								foreach($yes_no_with_not_applicable as $key_opt => $row_optn)
								 {
								 	if($key_opt != ''){

									 $selected = '';
									 if($fetch_investor_data['fci_acct_service'] == $key_opt)
									 {
										 $selected = 'selected';
									 }
										echo '<option value="'.$key_opt.'" '.$selected.' >'.$row_optn.'</option>';
									}
								 }
								
								?>	
							</select>
						</div>
								
						<div class="col-md-4" >
							<label>FCI Account #</label>								
							<input type="text" class="form-control contact_title" id="fci_acct" name ="fci_acct" value ="<?php echo isset($fetch_investor_data['fci_acct']) ? $fetch_investor_data['fci_acct'] : '';?>" >
						</div>

						<div class="col-md-4">
							
							
						</div>
					</div>
					
					<div class="row borrower_data_row">
						<div class="col-md-4">
							<label>Del Toro Account</label>
										
							<select class="form-control" name="del_acct_service" id="del_toro_type" onchange="hide_del_toro_lender(this.value);">
								 
								 <?php
								 foreach($yes_no_with_not_applicable as $key_opt => $row_optn)
								 {
								 	if($key_opt != ''){

									 $selected = '';
									 if($fetch_investor_data['del_acct_service'] == $key_opt)
									 {
										 $selected = 'selected';
									 }
										echo '<option value="'.$key_opt.'" '.$selected.' >'.$row_optn.'</option>';
									}
								 }
							
								?>	
							</select>
						</div>

						<div class="col-md-4">
							
							<label>Del Toro Account #</label>								
							<input type="text" class="form-control contact_title" id="del_toro_acct" name = "del_toro_acct" value ="<?php echo isset($fetch_investor_data['del_toro_acct']) ? $fetch_investor_data['del_toro_acct'] : '';?>" >
						</div>

						<div class="col-md-4">
							
						</div>
							
						
					</div>
					
						
					<div class="row borrower_data_row border-top">
						<h4>Broker Authorization and Approvals</h4>
					</div>

					<?php

						$explode_bro_talimar = explode(',', $fetch_investor_data['bro_talimar']);
						$explode_bro_lender = explode(',', $fetch_investor_data['bro_lender']);
					?>
					<?php foreach($Broker_Authorization_option as $key => $bro_opt){ 

							if(in_array($key, $explode_bro_talimar)){

								$bro_talimar = 'checked';
							}else{
								$bro_talimar = '';
							}

							if(in_array($key, $explode_bro_lender)){

								$bro_lender = 'checked';
							}else{
								$bro_lender = '';
							}

					?>
						<div class="row borrower_data_row">
							<div class="col-md-4">
								<label><?php echo $bro_opt;?>:</label>	
							</div>

							<div class="col-md-3">
								<input type="checkbox" class="form-control" name="Btalimar[]" value="<?php echo $key;?>" <?php echo $bro_talimar;?>> TaliMar Financial
							</div>

							<div class="col-md-2">
								<input type="checkbox" class="form-control" name="Blender[]" value="<?php echo $key;?>" <?php echo $bro_lender;?>> Lender
							</div>
						</div>
					<?php } ?>




						<div class="row borrower_data_row border-top">
							<!-- <h4>Lender Disbursement Information</h4> -->
							<h4>Monthly Disbursement Data</h4>
						</div>
						
						<div class="row borrower_data_row">
							<div class="col-md-4">	
							
							<?php $ach_disbrusement = isset($fetch_investor_data['ach_disbrusement']) ? $fetch_investor_data['ach_disbrusement'] : '';?>
							
								<input type="checkbox" class="form-control" id="ach_disbrusement" onclick="ach_disbrusement_value(this);" <?php if($ach_disbrusement == 1){echo 'Checked';} ?>>
								<input type="hidden" name="ach_disbrusement" id="ach_disbrusement_val" value="<?= isset($fetch_investor_data['ach_disbrusement']) ? $fetch_investor_data['ach_disbrusement'] : '';?>">
								<label class="f">ACH Disbursement</label>
							</div>
						</div>
						
						<div class="row borrower_data_row">
							<div class="col-md-3">					
								<label>Bank Name:</label>
								<input type="text" class="form-control" name = "bank_name" value ="<?= isset($fetch_investor_data['bank_name']) ? $fetch_investor_data['bank_name'] : '';?>">
							</div>
							<div class="col-md-3">							
								<label>Account Number:</label>								
									<input type="text" class="form-control" name = "account_number" value ="<?= isset($fetch_investor_data['account_number']) ? $fetch_investor_data['account_number'] : '';?>">						
							</div>
							<div class="col-md-3">								
								<label>Routing Number:</label>								
									<input type="text" class="form-control" name = "routing_number" value ="<?= isset($fetch_investor_data['routing_number']) ? $fetch_investor_data['routing_number'] : '';?>">							
							</div>
								<div class="col-md-3">								
								<label>Account Type:</label>	
								
								<input type = "hidden" id ="account_type" name = "account_type" value = "<?php echo isset($fetch_investor_data['account_type']) ? $fetch_investor_data['account_type'] : '';?>">
								
								<select type="text" class="form-control" name = "account_type" value ="<?= isset($fetch_investor_data['account_type']) ? $fetch_investor_data['account_type'] : '';?>" >
								
								
								<?php
								foreach($account_type_option as $key => $row){
									 ?>
									<option value="<?php echo $key; ?>" <?php if(isset($fetch_investor_data['account_type'])){ if($fetch_investor_data['account_type'] == $key){ echo "selected"; } }?> > <?php echo $row; ?></option>
									 <?php
								}
								?>
								</select>					
							</div>
							
							
						</div>
						
						<div class="row borrower_data_row">
							<div class="col-md-4">	
							<?php $check_disbrusement = isset($fetch_investor_data['check_disbrusement']) ? $fetch_investor_data['check_disbrusement'] : '';?>
							
								<input type="checkbox" class="form-control" id="check_disbrusement" onclick="check_disbrusement_value(this);" <?php if($check_disbrusement == 1){echo 'Checked';} ?>>	
								<input type="hidden" name="check_disbrusement" id="check_disbrusement_val" value="<?= isset($fetch_investor_data['check_disbrusement']) ? $fetch_investor_data['check_disbrusement'] : '';?>">
								<label class="f">Check Disbursement</label>
							</div>
						</div>
							<div class="row borrower_data_row">
							
								<div class="col-md-3">								
								<label>Street Address:</label>								
									<input type="text" class="form-control" name = "mail_address" value ="<?= isset($fetch_investor_data['mail_address']) ? $fetch_investor_data['mail_address'] : ''; ?>">
								</div>
							
								<div class="col-md-3">								
								<label>Unit:</label>								
									<input type="text" class="form-control" name = "mail_unit" value ="<?= isset($fetch_investor_data['mail_unit']) ? $fetch_investor_data['mail_unit'] : ''; ?>">
								</div>
								
							</div>
							
							<div class="row borrower_data_row">
							
								<div class="col-md-3">								
								<label>City:</label>								
									<input type="text" class="form-control" name = "mail_city" value ="<?= isset($fetch_investor_data['mail_city']) ? $fetch_investor_data['mail_city'] : ''; ?>">
								</div>
							
								<div class="col-md-3">								
								<label>State:</label>								
									<select type="text" class="form-control" name = "mail_state"   />
									<?php 
										foreach($STATE_USA as $key => $row)
										{
											?>
											<option value="<?= $key;?>"  <?php if(isset($fetch_investor_data['mail_state'])){ if($fetch_investor_data['mail_state'] == $key){ echo 'selected'; } } ?> ><?= $row;?></option>
											<?php
										}
										?>
									</select>
								</div>
							
								<div class="col-md-3">								
								<label>Zip:</label>								
									<input type="text" class="form-control" name = "mail_zip" value ="<?= isset($fetch_investor_data['mail_zip']) ? $fetch_investor_data['mail_zip'] : ''; ?>">
								</div>
								
							</div>

							<div class="row borrower_data_row border-top">
								<!-- <h4>Lender Disbursement Information</h4> -->
								<h4>Lender Administration</h4>
							</div>
							<div class="row borrower_data_row">

								<?php
									$usertypeff = $this->session->userdata('user_role');
									if($usertypeff == 2){
										$fundisable = '';
									}else{
										$fundisable = 'disabled="disabled"';
									}

								?>
							
								<div class="col-md-3">								
									<label>Funding Entity:</label>
									<select class="form-control" name="funding_entity" <?php echo $fundisable;?>>
										<?php foreach($ct_funding_entity as $key => $row){?>
											<option value="<?php echo $key;?>" <?php if($fetch_investor_data['funding_entity'] == $key){echo 'selected';}?>><?php echo $row;?></option>
										<?php } ?>
									</select>		
								</div>
							</div>


							<div class="row borrower_data_row border-top">
								<!-- <h4>Lender Disbursement Information</h4> -->
								<h4>Other Instructions</h4>
							</div>
							<div class="row borrower_data_row">
							
								<div class="col-md-12">								
									<label>Other Instructions:</label>
									<textarea name="other_instruction" class="form-control" rows="3" placeholder="Enter instruction here..."><?php echo $fetch_investor_data['other_instruction'];?></textarea>		
								</div>
							</div>
						
						
						
						
						
		
						<!---Saved changes--->
				</div>
			<?php if($this->uri->segment(2) == 'edit'){
				?>
				</div>
				
			<?php } ?>
				
<!---------- main right div Ends--------------->			
				
			<div class = "main_right_div"></div>
					

				<!---  Borrower Document modal   Starts---->
		
				<div class="modal fade bs-modal-lg" id="lender_document" tabindex="-1" role="large" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<form method="POST" enctype="multipart/form-data" action = "<?php echo base_url('Investor_data/upload_lender_documents');?>" >	

								<input type="hidden" value="<?php echo $investor_id;?>" name="lender_iddd">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Lender Document</h4>
							</div>
							<!-----------Hidden fields----------->
							<!-----------End of hidden Fields---->
							<div class="modal-body">
								<div class=" doc-div">
									<div class="row">
										<div class="col-md-4">
											<label>Selected documents:</label>
														
										</div>
									</div>
									<div class="row">
										<input type = "hidden" name = "document_id" id = "document_id" />
										<div class="col-md-12">
										<?php 
											if($fetch_lender_document){
												echo '<ul class = "borrower_document_lists">';
												foreach($fetch_lender_document as $lend_documents){
													echo '<li>
														<a href ="'.base_url().'Investor_data/investor_document_read/'.$lend_documents->id .'" target="_blank">'.basename($lend_documents->lender_document).'</a>
																					
																			
														<a data-name = "'.basename($lend_documents->lender_document).'" id="filename_'.$lend_documents->id.'" onclick = display_inputfield('.$lend_documents->id .')>
															<i  title = "Rename File name" class = "fa fa-edit"  aria-hidden="true"></i>
														</a>
														
													</li>';
													// The location of the PDF file on the server.
												}
												echo '</ul>';
											} 
											?> 
										</div>
									</div>
									
									<hr>
									
									<div class="row">
										<div class="col-md-4">
											<input  type="file" id = "lenderr_upload_1" name ="lenderr_upload[]" class="select_images_file" onchange="change_this_image(this)" >
										</div>
									</div>
								</div>
								<br>
								<button onclick = "lender_document_add_more_roww();" id = "add_row" class = "btn blue" type = "button"><i class="fa fa-plus" aria-hidden="true"> Add More</i></button>
										
							</div>
									
							<div class="modal-footer">
								<button type="submit" value="save" name = "button"  class="btn blue">Save Document</button>
								<button type="button" class="btn default" data-dismiss="modal">Close</button>
													
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
						
				<!---  Borrower Document modal  End ---->
			
				<?php if($btn_name == 'view'){  ?>
						
					<!----------------- Borrower History Div starts--------->
					<div class="row borrower_data_row table-borrower">
						<h4>Borrower History:</h4>
						<table class="table ">
							<tbody>
								<tr>
									<td style="width:33.3%;"># of Active Loans</td>
									<td style="width:33.3%;"><?php echo $active_count; ?></td>
											
								</tr>
								<tr>
									<td style="width:33.3%;">$ of Active Loans</td>
									<td style="width:33.3%;">$<?php echo number_format($active_amount);?></td>
								</tr>
								<tr>
									<td># of Paid Off Loans</td>
									<td><?php echo $paid_count; ?></td>
								</tr>
								<tr>
									<td>$ of Paid Off Loans</td>
									<td>$<?php echo number_format($paid_amount); ?></td>
								</tr>
										
								<tr>
									<td># of Pipeline Loans</td>
									<td><?php echo $pipeline_count; ?></td>
								</tr>
								<tr>
									<td>$ of Pipeline Loans</td>
									<td>$<?php echo number_format($pipeline_amount); ?></td>
								</tr>	
								<tr>
									<td># of Total Loans</td>
									<td><?php echo $paid_count + $active_count + $pipeline_count; ?></td>
								</tr>
								<tr>
									<td>$ of Total Loans</td>
									<td>$<?php echo number_format($paid_amount + $active_amount + $pipeline_amount); ?></td>
								</tr>	
							</tbody>
						</table>
						<hr>
					</div>
					<!----------------- Borrower History Div Ends--------->
				<?php } ?>	
			</div>
		</form>	
						
			
			
	

				

	<!-----------------FORM FOR LINK CONTACT------------------->
	<form method="POST" name="search_contact" action="<?php echo base_url();?>contact" id="form_search_contact">
		<input type="hidden" name="search_contact" id="search_contact_lender">
	</form>
	<!-----------------END FORM FOR LINK CONTACT------------------->

	<!-----------------FORM FOR LINK CONTACT------------------->
	<form method="POST"  action="<?php echo base_url();?>loan_schedule_investor" id="redirect_to_reportdata">
		<input type="hidden" name="investor_id" >
	</form>
	<!-----------------END FORM FOR LINK CONTACT------------------>
	

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
	
<style>
.chosen-drop {
    width: 193px !important;
    margin-left: 0px !important;
}	

.chosen-container{
	display: block !important;
}
.chosen-container.chosen-container-single {
    width: 193px !important;
}
.chosen-container-single .chosen-single {
    padding: 4px 0px 26px 6px !important;
    border-radius: 4px !important;
    border: 1px solid #e1e1e1 !important;
}	
</style>	
<script>

	$(".chosen").chosen();

$(document).ready(function(){
	$( "#re_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
});


 function investor_select(id)
{
	$('#lender_form').submit();
	// window.location.href = "<?php echo base_url();?>investor_data/"+id;
	
} 

function MortgageActiveValue(that)
{	
	if(that == 'no')
	{
		$('#mortgageStatus option:eq(0)').attr('selected','selected'); // Select first option
		// $('#mortgageStatus option[value=""]').change();
		$('#amountInvested').val(0);
	}
}


function delete_investor(id)
{
	
	if (confirm("Are you sure to delete") == true) {
       window.location.href = "<?php echo base_url();?>delete_investor/"+id;
    } else {
        
    }
	

}

function ach_disbrusement_value(that){
	
	if($(that).is(':checked')){
		$('input#ach_disbrusement_val').val('1');
		$('input#check_disbrusement').attr('disabled',true);
		$('input#check_disbrusement').val('0');
	}else{
		$('input#check_disbrusement').attr('disabled',false);
		$('input#ach_disbrusement_val').val('0');
	}
}

function check_disbrusement_value(that){
	
	if($(that).is(':checked')){
		$('input#check_disbrusement_val').val('1');
		$('input#ach_disbrusement').attr('disabled',true);
		$('input#ach_disbrusement').val('0');
	}else{
		$('input#ach_disbrusement').attr('disabled',false);
		$('input#check_disbrusement_val').val('0');
	}
}

function hide_del_toro_lender(that){
	
	var val = that;
	if(val == '1'){
		
		$('input#del_toro_acct').attr('readonly',true);
	}else{
		$('input#del_toro_acct').attr('readonly',false);
	}	
}

function hide_fci_lender(that){
	
	var val = that;
	if(val == '1'){
		
		$('input#fci_acct').attr('readonly',true);
	}else{
		$('input#fci_acct').attr('readonly',false);
	}	
}

$(document).ready(function(){
	
	var fci = $('#fci_type').val();
	if(fci == '1'){
		$('input#fci_acct').attr('readonly',true);
	}else{
		$('input#fci_acct').attr('readonly',false);
	}

	var fci = $('#del_toro_type').val();
	if(fci == '1'){
		$('input#del_toro_acct').attr('readonly',true);
	}else{
		$('input#del_toro_acct').attr('readonly',false);
	}
	
});



$(document).ready(function(){
	
	var check = $('input#check_disbrusement_val').val();
	if(check == '1'){
		$('input#ach_disbrusement').attr('disabled',true);
		$('input#ach_disbrusement').val('0');
	}else{
		$('input#ach_disbrusement').attr('disabled',false);
	}
	
	var ach = $('input#ach_disbrusement_val').val();
	if(ach == '1'){
		$('input#check_disbrusement').attr('disabled',true);
		$('input#check_disbrusement').val('0');
	}else{
		$('input#check_disbrusement').attr('disabled',false);
	}
	
});





$(document).ready(function(){
	$("#dob").datepicker({
		
	dateFormat :"mm-dd-yy"});
});

  $('.phone').keyup(function()
  {
	  var val = this.value.replace(/\D/g, '');
	 if(val.length < 10)
	 {
        var newVal = '';
        while (val.length > 3) {
          newVal += val.substr(0, 3) + '-';
          val = val.substr(3);
        }
        newVal += val;
        this.value = newVal;
	 }
	 else
	 {
     this.value = this.value.replace(/(\d{3})\-?(\d{3})\-?(\d{4})/,'$1-$2-$3');
	 }
    //alert ("OK");
  });
  
  $(document).ready(function(){
	  var contact_2 = '<?php echo isset($fetch_investor_data['c2_first_name']) ? $fetch_investor_data['c2_first_name'] : '0'; ?>';
	  if(contact_2!= '' && contact_2!= '0' )
	  {
		  $('#c2_information').css('display','block');
		  $('#add_another_person').css('display','none');
	  }
  });
  function show_contact_2()
  {
	  // alert('onclick working');
	  $('#c2_information').slideDown('slow');
	  $('#add_another_person').css('display','none');
  }
  
  $('#ach_deposit').change(function(){
	  var ach = $('#ach_deposit').val();
	  if(ach == '' || ach == 0)
	  {
		  // When select None from Payment Distribution 
		  // Hide ach dependent input box
		 $('.ach_dependent input').val(''); 
		 $('.ach_dependent input').prop('readonly',true); 
		 $('.ach_dependent select').prop('disabled',true); 
		 $('#ach_deposit').prop('disabled',false); 		 		 
		 $('.ach_dependent').css('display','none');
		 $('.acct_field').css('display','none');

		 // Hide mail dependent input box
		 $('div.mail_payment_distribution_depend input').prop('readonly',true);
		 $('div.mail_payment_distribution_depend input').prop('disabled',true);
		 $('div.mail_payment_distribution_depend').css('display','none');

	  }
	  else if(ach == 1)
	  {
		  // Show ach dependent input box
		 $('.ach_dependent input').prop('readonly',false); 
		 $('.ach_dependent select').prop('disabled',false); 
		 $('#ach_deposit').prop('disabled',false); 		  		 
		 $('.ach_dependent').css('display','block'); 
		 $('.acct_field').css('display','block'); 
		  
		 // Hide mail dependent input box
		 $('div.mail_payment_distribution_depend input').prop('readonly',true);
		 $('div.mail_payment_distribution_depend input').prop('disabled',true);
		 $('div.mail_payment_distribution_depend').css('display','none');
	  }
	  else if(ach == 2)
	  {
		  // Hide ach dependent input box
		 $('.ach_dependent input').val(''); 
		 $('.ach_dependent input').prop('readonly',true); 
		 $('.ach_dependent select').prop('disabled',true); 
		 $('#ach_deposit').prop('disabled',false); 		 		 
		 $('.ach_dependent').css('display','none'); 
		 $('.acct_field').css('display','none'); 
		 
		 // Show mail dependent input box
		 $('div.mail_payment_distribution_depend input').prop('readonly',false);
		 $('div.mail_payment_distribution_depend input').prop('disabled',false);
		 $('div.mail_payment_distribution_depend').css('display','block');
	  }
	  else
	  {
		  
	  }
  });
  	   
  function checkbox_account_type(value){
	  
	 var cnt = value;
	 $('.account_type').not(this).prop('checked', false);  
	 // $('.account_type').prop('checked', false);  
	 
	 if($('#account_type_chkbox_'+cnt).prop('checked',true)){
		  $('#account_type').val(cnt);
	 }else{
		  $(".account_type").prop("checked", false);
	 }
  }
   $(document).ready(function(){
	 var ach = $('#ach_deposit').val();
	  if(ach == '' || ach == 0)
	  {
		  // When select None from Payment Distribution 
		  // Hide ach dependent input box
		 $('.ach_dependent input').val(''); 
		 $('.ach_dependent input').prop('readonly',true); 
		 $('.ach_dependent select').prop('disabled',true); 
		 $('#ach_deposit').prop('disabled',false); 		 		 
		 $('.ach_dependent').css('display','none');
		 $('.acct_field').css('display','none');

		 // Hide mail dependent input box
		 $('div.mail_payment_distribution_depend input').prop('readonly',true);
		 $('div.mail_payment_distribution_depend input').prop('disabled',true);
		 $('div.mail_payment_distribution_depend').css('display','none');

	  }
	  else if(ach == 1)
	  {
		  // Show ach dependent input box
		 $('.ach_dependent input').prop('readonly',false); 
		 $('.ach_dependent select').prop('disabled',false); 
		 $('#ach_deposit').prop('disabled',false); 		  		 
		 $('.ach_dependent').css('display','block'); 
		 $('.acct_field').css('display','block'); 
		  
		 // Hide mail dependent input box
		 $('div.mail_payment_distribution_depend input').prop('readonly',true);
		 $('div.mail_payment_distribution_depend input').prop('disabled',true);
		 $('div.mail_payment_distribution_depend').css('display','none');
	  }
	  else if(ach == 2)
	  {
		  // Hide ach dependent input box
		 $('.ach_dependent input').val(''); 
		 $('.ach_dependent input').prop('readonly',true); 
		 $('.ach_dependent select').prop('disabled',true); 
		 $('#ach_deposit').prop('disabled',false); 		 		 
		 $('.ach_dependent').css('display','none'); 
		 $('.acct_field').css('display','none'); 
		 
		 // Show mail dependent input box
		 $('div.mail_payment_distribution_depend input').prop('readonly',false);
		 $('div.mail_payment_distribution_depend input').prop('disabled',false);
		 $('div.mail_payment_distribution_depend').css('display','block');
	  }
	  else
	  {
		  
	  }
  });
  
    function add_new_lender_contact(){ 

	  $('div#hidden_lender_contact_information_div').css('display','block');
      $("div#hidden_lender_contact_information_div div.lender_information_div").clone().appendTo("div#lender_contact_information_div div.lender_information_div");
	  // $('div#hidden_lender_contact_information_div').css('display','none');
	}
	 function add_new_lender_contact(){ 
	  // alert('gone'); 
	  var numItems = $('.lender_information_div').length;
	  // alert(numItems);
	  var label = 'Contact Name';
	  if(numItems == '0')
	  {
		  label = 'Secondary Contact Name';
	  }
	  else if(numItems > '1')
	  {
		  label = 'Secondary Contact Name';
	  }
	  // $('div#hidden_lender_contact_information_div label#contacy-label-name span').text(label);
	   $('div#hidden_lender_contact_information_div h4.count_borrower_contact').text(label);
	  
	  $('div#hidden_lender_contact_information_div').css('display','block');
      $("div#hidden_lender_contact_information_div div.lender_information_div").clone().insertAfter("div#lender_contact_information_div div.lender_information_div:last");
	  // $('div#hidden_lender_contact_information_div').css('display','none');
	}
	function fetch_lender_contact_data(that)
	{
		 var contact_id = that.value;
		 $.ajax({
				type: "post",
				url: "<?php echo base_url()?>Ajax_call/fetch_lender_contact_details",
				data:{'contact_id':contact_id},
				success: function(response){
					if(response){
						var data  = JSON.parse(response);
						// alert(data.phone);
						// $(that).parent().parent().parent().css('background','red');
						$(that).parent().parent().parent().find('input.contact_phone').val(data.phone);
						$(that).parent().parent().parent().find('input.contact_email').val(data.email);
						// $(that).parent().parent().parent().find('input.contact_title').val(data.title);
						$(that).parent().parent().parent().find('input.contact_re_date').val(data.re_date);
						$(that).parent().parent().parent().find('a.contact_view_link').prop('id',contact_id);
						
						
					}
				}
		 });
	 
		
	}

	function fetch_lender_contact_data_new1(that)
	{
		 var contact_id = that.value;
		 $.ajax({
				type: "post",
				url: "<?php echo base_url()?>Ajax_call/fetch_lender_contact_details",
				data:{'contact_id':contact_id},
				success: function(response){
					if(response){
						var data  = JSON.parse(response);
						// alert(data.phone);
						// $(that).parent().parent().parent().css('background','red');
						$(that).parent().parent().parent().find('input.contact_phone').val(data.phone);
						$(that).parent().parent().parent().find('input.contact_email').val(data.email);
						
						
						
					}
				}
		 });
	 
		
	}

	function fetch_lender_contact_data_new2(that)
	{
		 var contact_id = that.value;
		 $.ajax({
				type: "post",
				url: "<?php echo base_url()?>Ajax_call/fetch_lender_contact_details",
				data:{'contact_id':contact_id},
				success: function(response){
					if(response){
						var data  = JSON.parse(response);
						// alert(data.phone);
						// $(that).parent().parent().parent().css('background','red');
						$(that).parent().parent().parent().find('input.contact_phones').val(data.phone);
						$(that).parent().parent().parent().find('input.contact_emails').val(data.email);
						
						
						
					}
				}
		 });
	 
		
	}
	
	function fetch_lender_contact_dataa(that)
	{
		 var contact_id = that.value;
		 $.ajax({
				type: "post",
				url: "<?php echo base_url()?>Ajax_call/fetch_lender_contact_details",
				data:{'contact_id':contact_id},
				success: function(response){
					if(response){
						var data  = JSON.parse(response);
						// alert(data.phone);
						// $(that).parent().parent().parent().css('background','red');
						$(that).parent().parent().parent().find('input.contact_phone_other').val(data.phone);
						$(that).parent().parent().parent().find('input.contact_email_other').val(data.email);
						
						
						
					}
				}
		 });
	 
		
	}
	
	function fetch_lender_contact_data_new(ta)
	{
		 var contact_id = ta.value;
		 $.ajax({
				type: "post",
				url: "<?php echo base_url()?>Ajax_call/fetch_lender_contact_details",
				data:{'contact_id':contact_id},
				success: function(response){
					if(response){
						var data  = JSON.parse(response);
						// alert(data.phone);
						// $(that).parent().parent().parent().css('background','red');
						$(ta).parent().parent().parent().find('input.contact_phonec').val(data.phone);
						$(ta).parent().parent().parent().find('input.contact_emailc').val(data.email);
						
						
						
					}
				}
		 });
	 
		
	}
	

	
	function hide_this_contact(that)
	{
		$(that).parent().parent().parent().css('display','none');
	}
	
	function delete_this_contact(id)
	{
		if(id){
			if(confirm('Are you sure to delete this Lender Contact') == true)
			{
				window.location.href = '<?php echo base_url();?>delete_lender_contact/D'+id;
			}
		}
	}
	
	function go_contact_link(that)
	{
		
		var contact_id = that.id; 
		// alert(contact_id);
		if(contact_id)
		{
			$('#search_contact_lender').val(contact_id);
			$('#form_search_contact').submit();
		}
	}
	
	function autofill_lender_data()
	{
		var address =	$('input[name="address"]').val();
		var unit 	=	$('input[name="unit"]').val();
		var city 	=	$('input[name="city"]').val();
		var state 	=	$('select[name="state"]').val();
		var zip 	=	$('input[name="zip"]').val();
		
		$('input[name="mail_address"]').val(address);
		$('input[name="mail_unit"]').val(unit);
		$('input[name="mail_city"]').val(city);
		$('select[name="mail_state"]').val(state);
		$('input[name="mail_zip"]').val(zip);
	}
	var i=1;
	function lender_document_add_more_roww(){
	  

	i++;
	 
		 // $('#add_row').css("display","block");
	 if(i < 6){
		 
	  $( ".doc-div" ).append( '<br><div class="row"><div class="col-md-4"><input type="file" id = "lenderr_upload_'+i+'" name ="lenderr_upload[]" class="select_images_file" onchange="change_this_image(this)" name="lender_files[]"></div></div>' );
	 }else{
		 // $('#add_row').css("display","none");
		 // alert('no mpre row added');
	 }
	  
}
	function check_primary_contact(id){
		
		$('.chkbx_primary_contact').prop('checked',false);
		$('.chkbx_primary_contact_val').val('0');
		var rowcnt = id.split('_');
		
		if($('#'+id).prop('checked',true)){
			// alert('if part');
			// alert('checked checkbox number : ' + rowcnt[2]);
			$('#primary_contact_val'+rowcnt[2]).val('1');
		}
	}
	
	function redirect_to_reportdata(id)
	{
		$('form#redirect_to_reportdata input[name="investor_id"]').val(id);
		$('form#redirect_to_reportdata').submit();
	}



function data_checkk(){
	array=[];
	var primaryCount=0;
	$("select[name='primary_contact[]']").each(function()
	{
		if($(this).val()=="1"){
			primaryCount=parseInt(primaryCount)+1;     
		}
		var primary_contact=$(this).val();
	    array.push(primaryCount);

	});
	console.log("count"+primaryCount);
 	if(primaryCount>1){
 		console.log("in if");
 		$("div.errorrr").css('display','block'); 
		$("div.errorrr").text("Only One Contact Selected As Primary Contact");	
		return false;
 	}
	else if(primaryCount==1) {
		console.log("in else if");
		var lender_name= $("#investor_name").val();
		var ach_disbrusement= $('input#ach_disbrusement[type=checkbox]:checked');
		var check_disbrusement= $('input#check_disbrusement[type=checkbox]:checked');		
		//var contact_name_2= $("#cn_newm").val();
		var contact_name= $("#cn_new").val();		
		var fci_type= $("#fci_type").val();
		var del_toro_type= $("#del_toro_type").val();
		var lender_type= $("#investor_type").val();
		var account_status= $("#account_status").val();
   		if(lender_name == '') 
   		{
			$("div.errorrr").css('display','block'); 
			$("div.errorrr").text("Lender Name is Required");	 
			$("#investor_name").focus().css('border','1px solid red');
			return false;	 
	 	}
	 	else if(lender_type =='0'){
			$("div.errorrr").css('display','block'); 
			$("div.errorrr").text("Lender Type is Required");	 
			$("#investor_type").focus().css('border','1px solid red');
			$("#investor_name").focus().css('border','');
			return false;
		 }else if(account_status == ''){
			$("div.errorrr").css('display','block'); 
			$("div.errorrr").text("Account status is Required");	 
			$("#account_status").focus().css('border','1px solid red');
			$("#account_status").focus().css('border','');
			return false;
		 }
	 	else if(contact_name ==''){
			$("div.errorrr").css('display','block'); 
			$("div.errorrr").text("Contact Name is Required");	 
			//	$(".da").focus().css('border','');
			$("#investor_name").focus().css('border','');
			$("#investor_type").focus().css('border','');
			$(".d").focus().css('border','1px solid red');
			return false;
	 	}
	 	else if(fci_type==''){
			$("div.errorrr").css('display','block'); 
			$("div.errorrr").text("Active Account w/ FCI Lender Services is Required");	 
			$("#fci_type").focus().css('border','1px solid red');
			$(".d").focus().css('border','');
			$("#investor_type").focus().css('border','');
			$("#investor_name").focus().css('border','');
			//$(".da").focus().css('border','');
			return false;
	 	} 
		else if(del_toro_type ==''){
			$("div.errorrr").css('display','block'); 
			$("div.errorrr").text("Active Account w/ Del Toro Loan Services is Required");	 
			$("#del_toro_type").focus().css('border','1px solid red');
			$("#fci_type").focus().css('border','');
			$("#investor_name").focus().css('border','');
			//$(".da").focus().css('border','');
			return false;
		} 	 
	}else{
		console.log("in else");
		$("div.errorrr").css('display','block'); 
		$("div.errorrr").text("Atleast One Contact Selected As Primary Contact");	
		return false;	
	}
}



$('select[name="primary_contact[]"').change(function(){ 

var selectbox=$(this).val();

if(selectbox == '1')
{

$(this).parents('#contaact_div').find('select.pri_cont').val('0');

$(this).val('1');

}


 });

$('select[name="primary_contact[]"').change(function(){ 

var selectbox=$(this).val();

if(selectbox == '1')
{

$(this).parents('#contaact_div_new').find('select.pri_cont').val('0');

$(this).val('1');

}


 });


$(document).ready(function(){
 
 	var lender_type=$("#investor_type").val();
	if(lender_type == '1')
	{

		$('#entity_name_row').css("display",'none');
		$('#entity_res_state').css("display",'none');
		$('#sdtc').css("display",'none');

	}else if(lender_type == '3')
	{

		$('#sdtc').css("display",'block');
		$('#entity_res_state').css("display",'none');
		$('#state_entity_no').css("display",'none');

	}
	else{

		$('#entity_name_row').css("display",'block');
		$('#entity_res_state').css("display",'block');
		$('#state_entity_no').css("display",'block');
		$('#sdtc').css("display",'none');
	}

});

$('#investor_type').change(function(){ 
	var lender_type = $(this).val();
 	
	$('#entity_name_row input').val('');
	$('#entity_res_state select').val('');
	$('#sdtc input').val('');
	$('#state_entity_no input').val('');

	if(lender_type == '1')
	{
		$('#entity_name_row').css("display",'none');
		$('#entity_res_state').css("display",'none');

	}else if(lender_type == '3')
	{
		$('#sdtc').css("display",'block');
		$('#entity_res_state').css("display",'none');
		$('#state_entity_no').css("display",'none');
	}
	else{
		$('#entity_name_row').css("display",'block');
		$('#entity_res_state').css("display",'block');
		$('#state_entity_no').css("display",'block');
		$('#sdtc').css("display",'none');
	}
});


</script>
<style>
	.active-class , no-class{
		display:block !important;
	}
	.inactive-class{
		display:none ;
	}
	.primary_contactdiv{
		float: left;
		width: initial !important;
		margin-top: 12px;
	}
	#uniform-primary_contact1{
		width: 20px !important;
	}
	.col-md-3.primary_contactdiv div {
		width: initial !important;
	}
	.col-md-3.primary_contactdiv h4 {
		margin: 0;
	}
</style>
