<?php 
// echo $loan_id;
//------------values from myConfig---------------
$search_by_option 					= $this->config->item('search_by_option');
$borrower_option 					= $this->config->item('borrower_option');
$yes_no_option 						= $this->config->item('yes_no_option');
$loan_type_option 					= $this->config->item('loan_type_option');
$transaction_type_option 			= $this->config->item('transaction_type_option');
$position_option 					= $this->config->item('position_option');
$loan_payment_type_option 			= $this->config->item('loan_payment_type_option');
$loan_payment_schedule_option 		= $this->config->item('loan_payment_schedule_option');
$loan_intrest_type_option 			= $this->config->item('loan_intrest_type_option');
$loan_purpose_option 				= $this->config->item('loan_purpose_option');
$escrow_account_option 				= $this->config->item('escrow_account_option');
$calc_day_option 					= $this->config->item('calc_day_option');
$min_bal_type_option 				= $this->config->item('min_bal_type_option');
$STATE_USA 							= $this->config->item('STATE_USA'); 
$loan_id 							= $this->uri->segment(2);
if($loan_id)
{
$fetch['id'] = $loan_id;
$fetch_loan_data = $this->User_model->select_where('loan',$fetch);
$fetch_loan_result = $fetch_loan_data->result();
foreach($fetch_loan_result as $row){
	
	$talimar_loan 			= $row->talimar_loan;
	$fci 					= $row->fci;
	$borrower 				= $row->borrower;
	$loan_amount 			= $row->loan_amount;
	$current_balance 		= $row->current_balance;
	$lender_fee 			= $row->lender_fee.'%';
	$intrest_rate 			= $row->intrest_rate.'%';
	$term_month 			= $row->term_month;
	$per_payment_period 	= $row->per_payment_period;
	$payment_held_close 	= $row->payment_held_close;
	$calc_of_year 			= $row->calc_of_year;
	$loan_type 				= $row->loan_type;
	$transaction_type 		= $row->transaction_type;
	$position 				= $row->position;
	$payment_type 			= $row->payment_type;
	$exit_strategy 			= $row->exit_strategy;
	$loan_highlights 		= $row->loan_highlights;
	$baloon_payment_type 	= $row->baloon_payment_type;
	$payment_sechdule 		= $row->payment_sechdule;
	$intrest_type 			= $row->intrest_type;
	$payment_amount 		= $row->payment_amount;
	$minium_intrest 		= $row->minium_intrest;
	$min_intrest_month 		= $row->min_intrest_month;
	$owner_occupied 		= $row->owner_occupied;
	$owner_purpose 			= $row->owner_purpose;
	$costruction_loan 		= $row->costruction_loan;
	$project_cost 			= $row->project_cost;
	$total_project_cost 	= $row->total_project_cost;
	$borrower_enquity 		= $row->borrower_enquity;
	$grace_period 			= $row->grace_period;
	$late_charges_due 		= $row->late_charges_due;
	$default_rate 			= $row->default_rate."%";
	$return_check_rate 		= $row->return_check_rate;
	$refi_existing 			= $row->refi_existing;
	$loan_document_date 	= $row->loan_document_date ? date('m-d-Y',strtotime($row->loan_document_date)) : '';
	$loan_funding_date 		= $row->loan_funding_date  ? date('m-d-Y',strtotime($row->loan_funding_date)) : '';
	$first_payment_date 	= $row->first_payment_date ? date('m-d-Y',strtotime($row->first_payment_date)) : '';
	$maturity_date 			= $row->maturity_date ? date('m-d-Y',strtotime($row->maturity_date)) : '';
	$modify_maturity_date 	= $row->modify_maturity_date ? date('m-d-Y',strtotime($row->modify_maturity_date)) : '';
	$extention_option 		= $row->extention_option;
	$extention_month 		= $row->extention_month;
	$extention_percent_amount = $row->extention_percent_amount;
	// $ballon_payment 		= $row->ballon_payment;
	$ballon_payment 		= ($row->loan_amount) + (($row->intrest_rate/100) * $row->loan_amount / 12);
	
	$trustee 				= $row->trustee;
	$promissory_note_val 	= $row->promissory_note_val;
	$payment_requirement 	= $row->payment_requirement;
	$add_broker_data_desc 	= $row->add_broker_data_desc;
	$min_bal_type 			= $row->min_bal_type;
	$payment_gurranty 		= $row->payment_gurranty;
	$payment_gurr_explain 	= $row->payment_gurr_explain;
	
}
}
else
{
	$talimar_loan 			= '';
	// $fci 					= date('y').'-T'.rand(1,999999);
	$fci 					= '';
	$borrower 				= '';
	$loan_amount 			= '00';
	$current_balance 		= '00';
	$lender_fee 			= '2.5%';
	$intrest_rate 			= '10%';
	$term_month 			= '12';
	$per_payment_period 	= '12';
	$payment_held_close 	= '6';
	$calc_of_year 			= '1';
	$loan_type 				= '2';
	$transaction_type 		= '1';
	$position 				= '1';
	$payment_type 			= '1';
	$payment_sechdule 		= '30';
	$intrest_type 			= '1';
	$payment_amount 		= '0.00';
	$minium_intrest 		= '';
	$min_intrest_month 		= '';
	$owner_occupied 		= '';
	$owner_purpose 			= '1';
	$costruction_loan 		= '';
	$project_cost 			= '0.00';
	$total_project_cost 	= '0.00';
	$borrower_enquity 		= '0.00';
	$grace_period 			= '10';
	$late_charges_due 		= '10';
	$default_rate 			= '17%';
	$return_check_rate 		= '150';
	$refi_existing 			= '0';
	$loan_document_date 	= '';
	$loan_funding_date 		= '';
	// $loan_funding_date 		= date('m-d-Y');
	$first_payment_date 	= '';
	$maturity_date 			= '';
	// $maturity_date 			=  date('m-d-Y', strtotime('+1 years'));
	$extention_option 		= '1';
	$extention_month 		= '6';
	$extention_percent_amount = '1%';
	$ballon_payment 		= '0.00';
	$trustee 				= 'FCI Lender Services';
	// $trustee 				= '';
	$promissory_note_val 	= '';
	$baloon_payment_type 	= '1';
	$payment_requirement 	= '0';
	$exit_strategy 			= 'Sale of Property';
	$loan_highlights 		= '';
	$add_broker_data_desc 	= '';
	$min_bal_type 			= '1';
	$payment_gurranty 		= '0'; // by default set with 1...
	$payment_gurr_explain 	= '';
	
}

// vendor names for trustee drop-down options...
foreach($all_vendor_data as $vendor){
	$vendor_name[$vendor->vendor_id] = $vendor->vendor_name;
}



?>
<style>

.row {
    margin-right: 0px;
    margin-left: -15px !important;
}
.page-head {
    margin-right: 15px;
}

</style>

<div class="page-content-wrapper">
	<div class="page-content">
		<?php if($this->session->flashdata('error')!=''){  ?>
			<div id='error'><?php echo $this->session->flashdata('error');?></div>
		<?php } ?>
		<?php if($this->session->flashdata('success')!=''){  ?>
			<div id='success'><?php echo $this->session->flashdata('success');?></div>
		<?php } ?>
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1> &nbsp; Loan Data <small></small></h1>
					
			</div>
				
			<div class="talimar_no_dropdowns">
				<div>
							
					<select name="loan_id" onchange="loan_form_submit(this.value)" class="selectpicker" data-live-search="true">
						<option value=''></option>
						<?php 
						foreach($fetch_search_option as $key => $row ){
						?>
						<option value = "<?php echo $fetch_search_option[$key]['id'];?>" <?php if($fetch_search_option[$key]['id']){ if($fetch_search_option[$key]['id'] == $loan_id){ echo 'selected'; } } ?> ><?php echo $fetch_search_option[$key]['text'];?></option>
						<?php  } ?>
					</select>
				</div>
			
				<div>
					<form method="POST" action="<?php echo base_url();?>load_data" id="form_search_loan_by">
						Search by:
						<select onchange="search_loan_by()" name="search_by">
							<option></option>
							<?php 
							foreach($search_by_option as $key => $row){
							?>
							<option value="<?php echo $key; ?>" <?php if($key == $selected_by){ echo 'selected'; } ?> ><?php echo $row; ?></option>
							<?php } ?>
						</select>
					</form>
				</div>
						
			</div>	
				
			<!-- END PAGE TITLE -->
			<!-- BEGIN PAGE TOOLBAR -->
				
			<!-- END PAGE TOOLBAR -->
		</div>
			<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb hide">
			<li>
				<a href="#">Home</a><i class="fa fa-circle"></i>
			</li>
			<li class="active">
				Dashboard
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- BEGIN PAGE CONTENT INNER -->
		<form class="form-horizontal" method="POST" action= "<?php echo base_url();?>add_load_data_form">
				<div class="row margin-top-10">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form_loan_data">
						<div class="dashboard-stat2">
							<!------------form 1---------------->
							
								

								<!-- Form Name -->
								<input type="hidden" name = "loan_id" value="<?php echo $loan_id;?>">

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">TaliMar Loan #:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="textinput" name="talimar_loan" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $talimar_loan; ?>" <?php if($loan_id != ''){ echo 'readonly'; } ?> />
								  <span class="help-block"></span>  
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Servicer Loan #:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="textinput" name="fci_loan_no" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $fci; ?>"   />
								  <span class="help-block"></span>  
								  </div>
								</div>
								
								
								
								 
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Borrower Name <a data-toggle="modal" href="#Borrower_modal" onclick="load_borower_content_data();"><i class="fa fa-eye" aria-hidden="true"></i></a> :</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="loan-borrower-id" name="borrower" class="form-control load_data_inputbox selectpicker" data-live-search="true" >
									<option value="">Select Borrower</option>
									<?php foreach($fetch_borrower_data as  $row) { ?>
									  <option value="<?php echo $row->id;?>" <?php if($borrower == $row->id) { echo 'selected'; } ?> ><?php echo $row->b_name;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>
								
								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Loan Amount:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="loan_amount" name="loan_amount" type="text" class="form-control input-md load_data_inputbox number_only"  value="<?php echo '&#36;'.number_format($loan_amount,2); ?>"  >
								  <span class="help-block"></span>  
								  </div>
								</div>  

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Current Balance:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="current_balance" name="current_balance" type="text" class="form-control input-md load_data_inputbox number_only amount_format"  value="<?php echo '$'.number_format($current_balance,2); ?>" >
								  <span class="help-block"></span>  
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Lender Fee:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="lender_fee" name="lender_fee" type="text" class="form-control input-md load_data_inputbox number_only" value="<?php echo $lender_fee; ?>">
								  <span class="help-block"></span>  
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Interest Rate (%):</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="intrest_rate" name="intrest_rate" type="text" class="form-control input-md load_data_inputbox number_only" value="<?php echo  number_format($intrest_rate,3); ?>">
								  <span class="help-block"></span>  
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Term (months):</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="term_month" name="term_month" type="text" class="form-control input-md load_data_inputbox number_only" value="<?php echo $term_month; ?>">
								  <span class="help-block"></span>  
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput"># of Payments per Period:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="textinput" name="per_payment_period" type="text" class="form-control input-md load_data_inputbox number_only" value="<?php echo $per_payment_period; ?>">
								  <span class="help-block"></span>  
								  </div>
								</div>
        
								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput"># of Payments Held at Close:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="textinput" name="payment_held_close" type="text" class="form-control input-md load_data_inputbox number_only" value="<?php echo $payment_held_close; ?>">
								  <span class="help-block"></span>  
								  </div>
								</div>
								
								<!-- Text input-->
								<!--
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Payment Requirement (Months)</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input name="payment_requirement" type="text" class="form-control input-md load_data_inputbox number_only" value="<?php echo $payment_requirement; ?>">
								  <span class="help-block"></span>  
								  </div>
								</div>
								
								<!-- Select Basic -->
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Calc Type:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="selectbasic" name="calc_of_year" class="form-control load_data_inputbox">
									  <?php foreach($calc_day_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key == $calc_of_year){ echo 'selected'; } ?>><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>
								
								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Trustee:</label>  
								  <div class="col-md-4 load_data_input_div">
								 <!-- <input id="textinput" name="trustee" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $trustee; ?>" >
								 --> 
								 
								  <select name = "trustee" class="form-control load_data_inputbox">
										<option value = ''>Select</option>
										<?php 
											foreach($vendor_name as $id => $name){
												// if($name == $trustee){
												if($id == $trustee){
													$selected =  'selected = "selected"';
												}
												elseif($name == $trustee){
													$selected =  'selected = "selected"';
													
												} 
												else{
													$selected = '';
												}
												echo '<option '.$selected.' value = '.$id.' >'.$name.'</option>';
											}		
										?>
								  </select>
								  <span class="help-block"></span>  
								  </div>
								</div>

								<div class="form-group">
								  <label class="loan_data_lable" for="textinput"></label>  
								  <div class="col-md-4 load_data_input_div">
									<a class="btn blue" data-toggle="modal" href="#promissory_note">Additional Loan Provisions</a>
							
								  </div>
								</div>

								

								

							<!-----------Close form 1------------------------>
						</div>
					</div>
					
					<!---  Email Promissory Notes modal   ---->
		
		<div class="modal fade bs-modal-sm" id="promissory_note" tabindex="-1" role="large" aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Loan: #<?php echo $talimar_loan;?></h4>
					</div>
					
					<div class="modal-body">
								<div class="row">
									<div class="col-md-8">
										<input type="textbox" id = "promissory_note_val" name ="promissory_note_val" value = "<?php echo $promissory_note_val;?>" class="form-control" >
									</div>
									
								</div>
							
							
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
						<button id="singlebutton" name="form_button" class="btn btn-primary" value="save" >Save</button>
							
					
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
		<!-- /.modal-dialog -->
		</div>
		
		<!---  End of Promissory Notes modal   ---->
					
					
					
					
					
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form_loan_data">
						<div class="dashboard-stat2">
							<!------------form 2---------------->
								<!-- Select Basic -->
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Priority:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="selectbasic" name="position" class="form-control load_data_inputbox">
									<option value="" ></option>
									  <?php foreach($position_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key == $position) { echo 'selected';} ?> ><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>

								<!-- Select Basic -->
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Amorization Type:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="loan_payment_type" name="payment_type" class="form-control load_data_inputbox">
									<option value="" ></option>
									  <?php 
									  foreach($loan_payment_type_option as $key => $row)
									  {
										  ?>
										  <option value="<?= $key;?>" <?php if($key == $payment_type) { echo "selected";} ?> ><?= $row;?></option>
										  <?php
									  }
									  ?>
									</select>
								  </div>
								</div>

								<!-- Select Basic -->
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Balloon Payment:</label>
								  <div class="col-md-4 load_data_input_div">
									<select  name="baloon_payment_type" class="form-control load_data_inputbox" id="baloon_payment_type" >
									<option value="" ></option>
									  <?php 
									  foreach($yes_no_option as $key => $row)
									  {
										  ?>
										  <option value="<?= $key;?>" <?php if($key == $baloon_payment_type) { echo "selected";} ?> ><?= $row;?></option>
										  <?php
									  }
									  ?>
									</select>
								  </div>
								</div>
								
								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Balloon Payment:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="loan_ballon_payment" name="ballon_payment" type="text" class="form-control input-md load_data_inputbox  number_only" value="<?php echo '$'.number_format($ballon_payment,2); ?>" readOnly >
								  <span class="help-block"></span>  
								  </div>
								</div>

								<!-- Select Basic -->
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Payment Frequency:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="selectbasic" name="payment_sechdule" class="form-control load_data_inputbox">
									<option value="" ></option>
									  <?php 
									  foreach($loan_payment_schedule_option as $key => $row)
									  {
									  ?>
									  <option value="<?= $key;?>" <?php if($payment_sechdule == $key) { echo 'selected'; } ?> ><?= $row;?></option>
									  <?php
									  }
									  ?>
									  
									</select>
								  </div>
								</div>

								<!-- Select Basic -->
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Interest Type:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="selectbasic" name="intrest_type" class="form-control load_data_inputbox">
									<option value="" ></option>
									<?php 
									foreach($loan_intrest_type_option as $key => $row)
									{
									?>
									  <option value="<?= $key;?>" <?php if($key == $intrest_type) { echo 'selected'; } ?> ><?= $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>
 
								<!-- Text input-->
								<!--
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Payment Amount:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="payment_amount" name="payment_amount" type="text" class="form-control input-md load_data_inputbox  number_only" value="<?php echo '$'.number_format($payment_amount,2); ?>">
								  <span class="help-block"></span>  
								  </div>
								</div>
								-->
								<!-- Select Basic -->
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Minimum Payment Requirement:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="minium_intrest" name="minium_intrest" class="form-control load_data_inputbox" onchange = "minimum_select(this.value)">
									<option value="" ></option>
									   <?php foreach($yes_no_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key == $minium_intrest) { echo 'selected'; } ?> ><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group" id="minimum_select_yes" style="display:none;">
								  <label class="loan_data_lable" for="textinput">If Yes, # of months:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="min_intrest_month" name="min_intrest_month" type="text" class="form-control input-md load_data_inputbox" onchange="check_month_valid(this.value)" value="<?php echo $min_intrest_month ? $min_intrest_month : ''; ?>">
								  <span class="help-block"></span>  
								  </div>
								</div>
								
								<!-- Text input-->
								<div class="form-group" id="min_bal_type_option" style="display:none;">
								  <label class="loan_data_lable" for="textinput">Minimum Balance Type:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <select id="min_bal_type" name="min_bal_type" class="form-control load_data_inputbox">
									<?php
									
									foreach($min_bal_type_option as $key => $row)
									{
										?>
										<option value="<?php echo  $key; ?>" <?php if($key == $min_bal_type){ echo 'selected'; } ?> ><?php echo  $row; ?></option>
										<?php
									}
									
									?>
								  </select>
								  <span class="help-block"></span>  
								  </div>
								</div>

								<!-- Select Basic -->
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Owner Occupied?:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="selectbasic" name="owner_occupied" class="form-control load_data_inputbox">
									<option value="" ></option>
									   <?php foreach($yes_no_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key == $owner_occupied) { echo 'selected'; } ?> ><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>

								<!-- Select Basic -->
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Business Purpose?:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="selectbasic" name="owner_purpose" class="form-control load_data_inputbox">
									   <?php foreach($yes_no_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key == $owner_purpose) { echo 'selected'; } ?> ><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>

								<!-- Select Basic -->
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Payment Guaranty:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="loan_payment_gurranty" name="payment_gurranty" class="form-control load_data_inputbox">
									   <?php foreach($yes_no_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key == $payment_gurranty) { echo 'selected'; } ?> ><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>
								
								<!--<div class="form-group" id="payment_gurranty_dependent">
								  <label class="loan_data_lable" for="selectbasic">Explanation:</label>
								  <div class="col-md-4 load_data_input_div">
									<textarea name="payment_gurr_explain" class="form-control"><?php echo $payment_gurr_explain; ?></textarea>
								  </div>
								</div>-->

								<!-- Select Basic -->
								<!--
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Construction Loan</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="selectbasic" name="costruction_loan" class="form-control load_data_inputbox">
									   <?php foreach($yes_no_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <? if($key == $costruction_loan) { echo 'selected'; } ?> ><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>
								-->

								
								
								
								<!-- Select Basic -->
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Loan Type:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="loan_type" name="loan_type" class="form-control load_data_inputbox">
									<option value="" ></option>
									  <?php foreach($loan_type_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key ==$loan_type ){ echo "selected"; }?> ><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>
								
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Transaction Type:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="transaction_type" name="transaction_type" class="form-control load_data_inputbox">
									<option value="" ></option>
									  <?php foreach($transaction_type_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key ==$transaction_type ){ echo "selected"; }?> ><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>
								
								
																							
								<div class="form-group" id="construction_div">
								  <label class="loan_data_lable" for="textinput">Renovation Draw:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <select id="select_draws" name="draws" type="text" class="form-control input-md load_data_inputbox" >
								  <option value=""></option>
								  <?php
								  foreach($yes_no_option as $key => $row)
								  {
									  ?>
									  <option value="<?= $key;?>" <?php if(isset($fetch_loan_result[0]->draws)){ if($fetch_loan_result[0]->draws == $key){ echo 'selected'; } } ?>><?= $row; ?></option>
									  <?php
								  }
								  ?>
								  </select>
								  <span class="help-block"></span>  
								  </div>
								</div>
								
							

								<!-- Text input-->
								<!--
								<div class="form-group loan_type_dependent">
								  <label class="loan_data_lable" for="textinput">Construction Budget:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="project_cost" name="project_cost" type="text" class="form-control input-md load_data_inputbox  number_only" value="<?php echo '$'.number_format($project_cost,2); ?>">
								  <span class="help-block"></span>  
								  </div>
								</div>
								-->
								<!-- Text input
								<div class="form-group loan_type_dependent">
								  <label class="loan_data_lable" for="textinput">Total Project Budget:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="total_project_cost" name="total_project_cost" type="text" class="form-control input-md load_data_inputbox  number_only" value="<?php echo '$'.number_format($total_project_cost,2); ?>">
								  <span class="help-block"></span>  
								  </div>
								</div>

								
								<div class="form-group loan_type_dependent">
								  <label class="loan_data_lable" for="textinput">Borrower Cash Contribution:</label>  
								  <div class="col-md-4 load_data_input_div">
									  <input id="" name="borrower_enquity" type="text" class="form-control input-md load_data_inputbox number_only amount_format" value="<?php echo '$'.number_format($borrower_enquity); ?>">
									  <span class="help-block"></span>  
								  </div>
								</div>
								
								
								<div class="form-group">
									<div class="col-md-12">
										<a class="btn blue full-width-button" data-toggle="modal" href="#construcion_type_sources_and_uses" >Sources & Uses</a>
									</div>
								</div>
								-->
								
								<?php
								if($loan_id)
								{
								?>
								<!--<div class="form-group">
								  <div class="col-md-4 load_data_input_div">
									 <a class="btn blue" data-toggle="modal" href="#additional_broker_data" id="additional_broker_data_button">Additional Broker Data </a>
								  </div>
								  
								</div>-->
								<?php } ?>
								
								
								
								<!-- Text input -->
								<!--
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Loan Highlights</label>  
								  <div class="col-md-4 load_data_input_div">
									  <input id="" name="loan_highlights" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $loan_highlights; ?>" >
									  <span class="help-block"></span>  
								  </div>
								</div>

								-->

							
							<!-------------Close form 2---------->
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form_loan_data">
						<div class="dashboard-stat2">
							<!------------form 3---------------->
							
							

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Grace Period (Days):</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="textinput" name="grace_period" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $grace_period; ?>" >
								  <span class="help-block"></span>  
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Late Charge: (% of Payment): </label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="late_charges_due" name="late_charges_due" type="text" class="form-control input-md load_data_inputbox number_only" value="<?php echo $late_charges_due.'%'; ?>">
								  <span class="help-block"></span>  
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Default Rate:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="default_rate" name="default_rate" type="text" class="form-control input-md load_data_inputbox number_only" value="<?php echo $default_rate; ?>">
								  <span class="help-block"></span>  
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Return Check Rate:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="return_check_rate" name="return_check_rate" type="text" class="form-control input-md load_data_inputbox  number_only" value="<?php echo '$'.$return_check_rate; ?>" >
								  <span class="help-block"></span>  
								  </div>
								</div>

								<!-- Select Basic -->
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Refi Existing TaliMar Loan:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="selectbasic" name="refi_existing" class="form-control load_data_inputbox">
									<option value="" ></option>
									   <?php foreach($yes_no_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key == $refi_existing) { echo 'selected'; } ?> ><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Loan Document Date:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="loan_document_date" name="loan_document_date" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $loan_document_date; ?>" placeholder = "MM-DD-YYYY">
								  <span class="help-block"></span>  
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Loan Closing Date:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="loan_funding_date" name="loan_funding_date" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $loan_funding_date; ?>"  placeholder = "MM-DD-YYYY">
								  <span class="help-block"></span>  
								  </div>
								</div>

								<!-- Text input -->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">First Payment Date:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="first_payment_date" name="first_payment_date" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $first_payment_date; ?>"  placeholder = "MM-DD-YYYY">
								  <span class="help-block"></span>  
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Original Maturity Date:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="maturity_date" name="maturity_date" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $maturity_date; ?>"  placeholder = "MM-DD-YYYY" readonly>
								  <span class="help-block"></span>  
								  </div>
								</div>
								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Modified Maturity Date</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="modify_maturity_date" name="modify_maturity_date" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $modify_maturity_date; ?>"  placeholder = "MM-DD-YYYY" >
								  <span class="help-block"></span>  
								  </div>
								</div>

								<!-- Select Basic -->
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Extension Option:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="extention_option" name="extention_option" class="form-control load_data_inputbox" onchange="extention_select(this.value)">
										<option value="" ></option>
									   <?php foreach($yes_no_option as $key => $row) { ?>
										<option value="<?php echo $key;?>" <?php if($key == $extention_option) { echo 'selected'; }?> ><?php echo $row;?></option>
										<?php } ?>
									</select>
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group extention_select_yes" style="display:none;">
								  <label class="loan_data_lable" for="textinput">If Yes , # of months:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="textinput" name="extention_month" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $extention_month; ?>">
								  <span class="help-block"></span>  
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group extention_select_yes" style="display:none;">
								  <label class="loan_data_lable" for="textinput">If Yes , % loan Amount:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="textinput" name="extention_percent_amount" type="text" class="form-control input-md load_data_inputbox  number_only" value="<?php echo $extention_percent_amount; ?>">
								  <span class="help-block"></span>  
								  </div>
								</div>
								
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Exit Strategy:</label>  
								  <div class="col-md-4 load_data_input_div">
									  <input id="" name="exit_strategy" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $exit_strategy; ?>">
									  <span class="help-block"></span>  
								  </div>
								</div>
							<!-------------  Close form 3  --------->
						</div>
					</div>
					
				</div> 
				
				
				
				
				
			
				<div class="row margin-top-10">
					<div class="load_data_buttons">
					<!------------------Buttons Section--------------------------->
						<div class="form-group">
						  
						  <div class="col-md-4">
							<button id="singlebutton" name="form_button" class="btn btn-primary" value="save" >Save</button>
							<a id="singlebutton"  class="btn btn-primary" href="<?php echo base_url();?>load_data" >New</a>
							<?php if($loan_id ) { 
							$uri_loan_id = $this->uri->segment(2);
							
							?>
							
							<?php  if($user_role == '2'){ ?>
							<a  class="btn btn-primary" onclick = "delete_loan()" >Delete</a> 
							<?php }
							} ?>
							<a id="singlebutton"  class="btn btn-primary" href="<?php echo base_url();?>load_data" >Main menu</a>
						  </div>
						</div>
					<!------------------End Buttons Section--------------------------->
					</div>
				</div>
				
			</form>	
	</div>
	<!-- END CONTENT -->
</div>

<div class="modal fade bs-modal-lg" id="Draws" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
		<form method="POST" action ="<?php echo base_url();?>draws_form">
			<input type="hidden" name="talimar_loan" value="<?= $talimar_loan; ?>">
			<input type="hidden" name="loan_id" value="<?= $loan_id; ?>">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Construction Reserve: Loan #(<?= $talimar_loan?>)</h4>
			</div>
			<div class="modal-body">
				<div class="rc_class">
				<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center"  id="draws_table">
					<thead>
					<tr>
						<th>Draw #</th>
						<th>Draw Requirements</th>
						<th>Reserve Amount</th>
						<th>Reserve Released</th>
						<th>Reserve Balance</th>
						<th>Request<br>Submitted</th>
						<th>Funds<br>Disbursed</th>
						<th>Reserve Notes</th>
						<!--
						<th style="width:1%">Draw Released</th>
						-->
						<th style=""></th>
					</tr>
					</thead>
					<tbody>
					<?php if(count($loan_draws_data) > 0 ) { 
						foreach($loan_draws_data as $row)
						{
							
						?>
						
						<tr>
							<td><input type="text" onkeypress = "return isNumber(event)" name = 'draws[]' value="<?= $row->draws; ?>" /></td>
							<td><textarea type="text" name="draws_require[]"  class=""  /><?= $row->draws_require; ?></textarea></td>
							<td><input type="text" name="draws_amount[]"  class="number_only amount_format autocal_draws_amount" value="<?= '$'.number_format($row->draws_amount,2); ?>"  id="draws_amount"/></td>
							<td><input type="text" name="draws_release_amount[]"  class="number_only amount_format autocal_draws_release_amount" value="<?= '$'.number_format($row->draws_release_amount,2); ?>"   id="draws_release_amount"/></td>
							<td><input type="text" readonly = "readonly" name="draws_remaining[]"  class="number_only amount_format autocal_draws_remaining" value="<?= '$'.number_format(($row->draws_amount)-($row->draws_release_amount),2); ?>" id="draws_remaining" /></td>
							<td>
								<input type="checkbox" onclick="construction_reserve_request_submitted(this)" <?php if($row->request_submitted == 1){ echo 'checked'; } ?> class="construction_reserve_request_submitted">
								<input type="hidden" name="request_submitted[]" value="<?php echo $row->request_submitted ? $row->request_submitted : '0'; ?>" >
							</td>
							<td>
								<input type="checkbox" onclick="construction_reserve_funds_disbursed(this)" <?php if($row->funds_disbursed == 1){ echo 'checked'; } ?> >
								<input type="hidden" name="funds_disbursed[]" value="<?php echo $row->funds_disbursed ? $row->funds_disbursed : '0'; ?>">
							</td>
							
							<td><textarea type="text" name="draws_notes[]"  class=""  /><?= $row->draws_notes; ?></textarea></td>
							<!--
							<td><input type="checkbox" name="checkbox_draws_release[]" class="checkbox_draws_release" <?php if($row->draws_release == '1'){ echo 'checked'; } ?> > <input type="hidden" name="draws_release[]" id="input_draws_release" value="<?= $row->draws_release; ?>"  ></td>
							-->					
						   <td><a class="draws_remove"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
						</tr>
						
						<?php
						}
					}
					 ?>
					</tbody>
					<tfoot>
					
						<tr>
						<th>Total</th>
						
						<th><!--Funds Disbursed: <?php echo '$'.number_format($fetch_extra_details[0]->draw_fund_released,2); ?>--></th>
						
						<th><input type="text" id="total_draws" value="<?= '$'.number_format($total_draws_amount,2); ?>"></th>
						
						<th><input type="text" id="total_release_amount" value="<?= '$'.number_format($total_draws_release_amount,2); ?>"></th>
						
						<th><input type="text" id="total_remaining" value="<?= '$'.number_format($total_draws_remaining,2); ?>"></th>
						
						<th><!--Available Funds: <?php  echo '$'.number_format($fetch_extra_details[0]->draw_fund_avilable,2); ?> --></th>
							<th colspan="5"></th>
						</tr>
					
					<tfoot>
				</table>
				</div>
				
				<div class="row">
					<div class="add_rows_button">
					<a class="btn blue shape_rectangle" id="draws_addrow">Add Draw</a>
					</div>
				</div>
				<div class="row ecrw_select_div">
					<div class="col-md-3">
					<strong>Fund Control Servicer:</strong>
						<select class="form-control" name="draw_escrow_account"  id="draw_escrow_account">
						<?php 
						foreach($escrow_account_option as $key => $row)
						{
							?>
							<option value="<?php echo $key; ?>" <?php if(isset($fetch_extra_details[0]->draw_escrow_account)){ if($fetch_extra_details[0]->draw_escrow_account == $key){ echo 'selected'; } }else if($key == 2){echo 'selected';} ?>><?php echo $row; ?></option>
							<?php
						}
						?>
						</select>
					</div>
					
					<?php
					$input_readonly = '';
					if(isset($fetch_extra_details[0]->draw_escrow_account))
					{ 
						if($fetch_extra_details[0]->draw_escrow_account == '3')
						{ 
							$input_readonly =  'readonly'; 
						} 
					} 
					
					?>
					<div class="col-md-3" id="draw_escrow_account_depend">
						<strong>Fund Control Account #:</strong>
						<input class="form-control" name="draw_escrow_other"  value="<?php echo isset($fetch_extra_details[0]->draw_escrow_other) ? (($fetch_extra_details[0]->draw_escrow_account == '3') ? $fci : $fetch_extra_details[0]->draw_escrow_other) : ''; ?>" 
						
						<?php echo $input_readonly; ?> ></input>
					</div>
					<div class="col-md-3" id="draw_escrow_account_depend">
						<strong>Cost per Draw:</strong>
						<input class="form-control number_only amount_format" name="draw_cost_per" 
						value="<?php echo isset($fetch_extra_details[0]->draw_cost_per) ? '$'.number_format($fetch_extra_details[0]->draw_cost_per,2): '55.00'; ?>" 
						
						
						<?php echo $input_readonly; ?> ></input>
					</div>
				</div>
				
				
				
                 <div class="row draws_row_padding">
					
					<div class="col-md-3">
						<strong>Borrower Name:</strong>
						<input class="form-control" name="borrower_name"  value="<?php echo isset($draws_borrower_name) ? $draws_borrower_name: ''; ?>" readonly = "readonly" ></input>
					</div>
					<div class="col-md-3">
						<strong>Contact Email:</strong>
						<input class="form-control" name="contact_email"  value="<?php echo isset($draws_contact_email) ? $draws_contact_email : ''; ?>" readonly = "readonly" ></input>
					</div>
					<div class="col-md-3">
						<strong>Contact Name:</strong>
						<input class="form-control" name="contact_name"  value="<?php echo isset($draws_contact_name) ? $draws_contact_name : ''; ?>" readonly = "readonly" ></input>
					</div>
				</div>
				
				
				
                <div class="row draws_row_padding">
					
					
					
				</div>
                                                                                
				<table class="draws_table_blank"  id="" style="display:none;">
					
					<tbody>
					<tr>
						<td><input type="text" name = 'draws[]'  /></td>
						<td><textarea type="text" name="draws_require[]"  class="" /></textarea></td>
						<td><input type="text" id = "draws_amount" name="draws_amount[]"  class="number_only amount_format autocal_draws_amount" /></td>
						<td><input type="text" id = "draws_release_amount" name="draws_release_amount[]"  class="number_only amount_format autocal_draws_release_amount" /></td>
						<td><input readonly = "readonly" type="text" name="draws_remaining[]" id ="draws_remaining"  class="number_only amount_format autocal_draws_remaining" /></td>
						
						<td>
							<input type="checkbox" onclick="construction_reserve_request_submitted(this)" class="construction_reserve_request_submitted">
							<input type="hidden" name="request_submitted[]" value="0">
						</td>
						<td>
							<input type="checkbox" onclick="construction_reserve_funds_disbursed(this)">
							<input type="hidden" name="funds_disbursed[]" value="0">
						</td>
						
						<td><textarea type="text" name="draws_notes[]"  class="" /></textarea></td>
								
					   <td><a class="draws_remove"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<a class="btn red" data-toggle="modal" href="#wire_information">Wire Information</a>
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn blue">Save</button>
			</div>
		</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

	<!----------------------------Wire Instruction Modal----------------------------->
		
		<div class="modal fade bs-modal-lg" id="wire_information" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Wire Information</h4>
					</div>
					<form method="POST" action="<?php echo base_url();?>form_wire_information">
					<!-----------Hidden fields----------->
					<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
					<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
					<!-----------End of hidden Fields---->
					<div class="modal-body pad-inp-body">
						 <div class="row">
							<div class="col-sm-4">
								<label>Name on Account:</label>
								<input type="text" class="form-control" name="draw_borrower_name" value="<?php echo isset($fetch_draw_wire_instruction) ? $fetch_draw_wire_instruction[0]->draw_borrower_name : ''; ?>">
							</div>
						
							<div class="col-sm-4">
								<label>Bank Name:</label>
								<input type="text" class="form-control" name="draw_bank_name" value="<?php echo isset($fetch_draw_wire_instruction) ? $fetch_draw_wire_instruction[0]->draw_bank_name : ''; ?>">
							</div>
						</div>
						
						<div class="row">
							
							<div class="col-sm-4">
								<label>Bank Street Address:</label>
								<input type="text" class="form-control" name="draw_bank_street" value="<?php echo isset($fetch_draw_wire_instruction) ? $fetch_draw_wire_instruction[0]->draw_bank_street : ''; ?>">
							</div>
							<div class="col-sm-4">
								<label>Bank Unit #:</label>
								<input type="text" class="form-control" name="draw_bank_unit" value="<?php echo isset($fetch_draw_wire_instruction) ? $fetch_draw_wire_instruction[0]->draw_bank_unit : ''; ?>">
							</div>
						</div>
						
						<div class="row">
							<div class="col-sm-4">
								<label>Bank City:</label>
								<input type="text" class="form-control" name="draw_bank_city" value="<?php echo isset($fetch_draw_wire_instruction) ? $fetch_draw_wire_instruction[0]->draw_bank_city : ''; ?>">
							</div>
							<div class="col-sm-4">
								<label>Bank State:</label>
								<select type="text" class="form-control" name="draw_bank_state" >
							<?php
								foreach($STATE_USA as $key =>$row)
								{
									?>
									<option value="<?php echo $key; ?>" <?php if(isset($fetch_draw_wire_instruction)){ if($key == $fetch_draw_wire_instruction[0]->draw_bank_state){ echo 'selected'; } } ?> ><?php echo $row; ?></option>
									<?php
								}
							?>
								</select>
							</div>
							<div class="col-sm-4">
								<label>Bank Zip:</label>
								<input type="text" class="form-control" name="draw_bank_zip" value="<?php echo isset($fetch_draw_wire_instruction) ? $fetch_draw_wire_instruction[0]->draw_bank_zip : ''; ?>">
							</div>
						</div>
					
						<div class="row">
							<div class="col-sm-4">
								<label>Routing Number:</label>
								<input type="text" class="form-control" name="draw_routing_number" value="<?php echo isset($fetch_draw_wire_instruction) ? $fetch_draw_wire_instruction[0]->draw_routing_number : ''; ?>">
							</div>
							<div class="col-sm-4">
								<label>Account Number:</label>
								<input type="text" class="form-control" name="draw_account_number" value="<?php echo isset($fetch_draw_wire_instruction) ? $fetch_draw_wire_instruction[0]->draw_account_number : ''; ?>">
							</div>
						</div>	
					
						<div class="modal-footer">
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn blue">Save</button>
						</div>
					</div>
				</form>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		
		<!----------------------------End of Wire Information Modal------------------------->
		
		
<!----- Borrower data modal -------------------->
<div class="modal fade bs-modal-lg" id="Borrower_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Borrower Data</h4>
			</div>
			<div class="modal-body" id="load-borrower-content">
				Borrower Detail
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal" id="display-block">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-----End of Borrower data modal -------------------->


<div class="modal fade bs-modal-lg" id="property" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-footer">
							<button type="submit" value = "save" class="btn blue borrower_save_button" name="button">Save</button>
							<button type="button" class="btn default borrower_save_button" data-dismiss="modal">Close</button>
							
							
					</div>
				
				</div>
									<!-- /.modal-content -->
			</div>
								<!-- /.modal-dialog -->
	</div>


<form method = "POST" action = "<?php echo base_url(); ?>delete_talimar_loan" id="delete_loan_form">
<input type="hidden" name="loan_id" value="<?= $uri_loan_id; ?>">
</form>

<div id="image_section_div" style="display:none;">
	<div class="image_main_section" >
		<div class="images_part">
		<input type="hidden" name="marketing[]" value="0">
			<img src="<?php echo base_url(); ?>assets/images/no_image.png" />
		</div>
		<div class="image_description">
			<input type="file" class="select_images_file" name="files[]" onchange="change_this_image(this)">
			<input type="text" class="form-control input-md" name="discription[]" >
		</div>
	</div>
</div>

<div id="loan_document_div" style="display:none;">
	<div class="image_main_section" >
		<div class="images_part">
			<input type="hidden" name="doc_type[]" value="0">
				<div class="document_iframe_part">
					<h6>Upload File</h6>		
				</div>
		</div>
		<div class="image_description">
			<input type="file" class="select_images_file" name="files[]" onchange="change_this_image(this)">
			<input type="text" class="form-control input-md" name="discription[]" value="">
		</div>
	</div>
</div>


<div id="album_number_new" style="display:none;">
	<div class="image_main_section" >
		<div class="images_part">
			<img src="<?php echo base_url(); ?>assets/images/no_image.png" />
		</div>
		<div class="image_description">
			<input type="hidden" name="title[]" value="" id="title_add">
			<input type="file" class="select_images_file" onchange="change_this_image(this)" name="files[]">
			<input type="text" class="form-control input-md" name="discription[]" >
		</div>
	</div>
</div>




<?php
if(isset($fetch_property_album)){
	foreach($fetch_property_album as $row)
	{
		?>
		<script>
		
		function add_new_images_album_<?php echo $row->id;?>(){
		$('#album_number_new').css('display','block');
		var title = '<?php echo $row->album_name; ?>';
		$('#album_number_new input#title_add').val(title);
		$("div#album_number_new div.image_main_section").clone().insertAfter("div#album_number_<?php echo $row->id; ?> div.image_main_section:last");
		$('#album_number_new').css('display','none');
		}
		</script>
		<?php
	}
}
?>

<script>

function isNumber(evt){
	var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
		
        return false;
		return true;
}
$(document).ready(function(){
	$('table#draws_table tbody input[name="request_submitted[]"]').each(function(){
		construction_reserve_readonly_css_setting1(this);
	});
});


$(document).ready(function(){
	var payment_gurranty = $('select#loan_payment_gurranty').val();
	if(payment_gurranty == '1')
	{
		$('div#payment_gurranty_dependent').css('display','block');
	}
	else
	{
		$('div#payment_gurranty_dependent textarea').val('');
		$('div#payment_gurranty_dependent').css('display','none');
	}
});

$('select#loan_payment_gurranty').change(function(){
	var payment_gurranty = $('select#loan_payment_gurranty').val();
	if(payment_gurranty == '1')
	{
		$('div#payment_gurranty_dependent').css('display','block');
	}
	else
	{
		$('div#payment_gurranty_dependent textarea').val('');
		$('div#payment_gurranty_dependent').css('display','none');
	}
});

function change_this_image(that)
{
   if (that.files && that.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                // $(that).parent().parent().find('.images_part img').css('background','red');
                 $(that).parent().parent().find('.images_part img')
                    .attr('src', e.target.result)
                    .width('100%')
                    .height(175);
            };

            reader.readAsDataURL(that.files[0]);
        }
};

function add_new_images(){
	$('#image_section_div').css('display','block');
	$("div#image_section_div div.image_main_section").clone().insertAfter("div.images_tab_section div.image_main_section:last");
	$('#image_section_div').css('display','none');
}

function add_new_images2(){
	$('#image_section_div').css('display','block');
	$("div#image_section_div div.image_main_section").clone().insertAfter("div.images_tab_section2 div.image_main_section:last");
	$('#image_section_div').css('display','none');
}

function add_new_file(){
	$('#loan_document_div').css('display','block');
	$("div#loan_document_div div.image_main_section").clone().insertAfter("div.loan_document_tab div.image_main_section:last");
	$('#loan_document_div').css('display','none');
}
  
  //--------------------------------------------------------

  
  //  Add row in Ecumbrances modal
  
     jQuery(document).ready(function() {
		
        var id = 0;
      jQuery("#draws_addrow").click(function() {
        id++;           
        var row = jQuery('.draws_table_blank tr').clone(true);
        row.find("input:text").val("");
        row.attr('id',id); 
        row.appendTo('#draws_table');        
        return false;
    });        
        
  $('.draws_remove').on("click", function() {
  $(this).parents("tr").remove();
});
});


  
  //  Add row in Sources & Uses modal
     jQuery(document).ready(function() {
        var id = 0;
      jQuery("button#add_loan_source_and_uses").click(function() {
        id++;           
        var row = jQuery('table#table_loan_source_and_uses_hidden tr').clone(true);
        row.find("input:text").val("");
        row.attr('id',id); 
        row.appendTo('table#table_loan_source_and_uses');        
        return false;
    });        
  
});
  
 
 $('#loan_funding_date').change(function(){
	 calculate_maturity_value();
 });
//    Onchange event on 

// for show div if select yes in minimum interst
function minimum_select(val)
{
	// alert(val);
	if(val == '1')
	{
		$("#minimum_select_yes").css("display" , "block");
		$("#min_bal_type_option").css("display" , "block");
		$("#min_intrest_month").focus();
	}
	else
	{
		$("#minimum_select_yes").css("display" , "none");
		$("#min_bal_type_option").css("display" , "none");
	}
}

$(document).ready(function(){
	var val_data = $('select#minium_intrest').val();
	if(val_data == '1')
	{
		$("#minimum_select_yes").css("display" , "block");
		$("#min_bal_type_option").css("display" , "block");
		$("#min_intrest_month").focus();
	}
	else
	{
		$("#minimum_select_yes").css("display" , "none");
		$("#min_bal_type_option").css("display" , "none");
	}
});

function extention_select(val)
{
	// alert(val);
	if(val == '1')
	{
		$(".extention_select_yes").css("display" , "block");
	}
	else
	{
		$(".extention_select_yes").css("display" , "none");
	}
}

function loan_form_submit(id)
{
	// $("#select_loan_form").submit();
	window.location.href = "<?php echo base_url();?>load_data/"+id;
}

function delete_loan()
{
	if (confirm("Are you sure to delete") == true) {
        $('#delete_loan_form').submit();
    } else {
        
    }
	
}



//    check extention

	
	var extention = $('#extention_option').val();
if(extention == '1')
	{
		$(".extention_select_yes").css("display" , "block");
	}
	else
	{
		$(".extention_select_yes").css("display" , "none");
	}
 // --------------------------------------
 //       onchange on loan  amount same to current balance


// ---------------------------------------

//   onchange in Intrest rate   
$('#intrest_rate').change(function(){
	var interst_rate  = parseFloat($('#intrest_rate').val());
	interst_rate		= number_format(interst_rate);	
	$('#intrest_rate').val(interst_rate+'%');
	auto_cal_payment_amount();
});


//   onchange in lender fee   
$('#lender_fee').change(function(){
	var interst_rate  	= parseFloat($('#lender_fee').val());
	interst_rate		= number_format(interst_rate);
	$('#lender_fee').val(interst_rate+'%');
	
});


function number_format(n) {
    return n.toFixed(2).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}

function number_format_percent_loan(n) {
    return n.toFixed(8).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "" + c : c;
    });
}

function replace_dollar(n)
{
	if(n){
			
		var a = n.replace('$', '');
		var b = a.replace(',', '');
		var b = b.replace(',', '');
		var b = b.replace(',', '');
		var b = b.replace(',', '');
		return b;
	}
}

function replace_percent(n)
{
	var a = n.replace('%', '');
	var b = a.replace(',', '');
	var b = b.replace(',', '');
	var b = b.replace(',', '');
	return b;
}

function auto_cal_payment_amount()
{
	var rate 			= parseFloat($('#intrest_rate').val());
	var loan_amount 	= $('#loan_amount').val();
	loan_amount 		= replace_dollar(loan_amount);
	var result  = 0 + (rate/100) * parseFloat(loan_amount) / 12;
	var result_a  = number_format(parseFloat(result));
	$('#payment_amount').val('$'+result_a);       
}

//   ----  onchange in defalt rate
$('#default_rate').change(function() {
	var default_rate  = $('#default_rate').val();
	if(default_rate != '')
	{
		var rate = number_format(parseFloat(default_rate));
		$('#default_rate').val(rate+'%');
	}
	else
	{
		var rate = 0;
		$('#default_rate').val('0.00%');
	}
	
	
});
 

//   ----  onchange in defalt rate
$('#return_check_rate').change(function() {
	var check_rate  = replace_dollar($('#return_check_rate').val());
	if(check_rate != '')
	{
	var new_rate    = parseFloat(check_rate);
	var result = number_format(new_rate);
	$('#return_check_rate').val('$'+result); 
	}
	else
	{
		$('#return_check_rate').val('0.00'); 
	}	
});

$('#project_cost').change(function(){
	var income = replace_dollar($('#project_cost').val());
	if(income != '')
	{
		var result = parseFloat(income);
		var new_result = number_format(result);
		$('#project_cost').val('$'+new_result);
	}
	else
	{
		$('#project_cost').val('$0.00');
	}
	
});

$('#total_project_cost').change(function(){
	var income = replace_dollar($('#total_project_cost').val());
	if(income != '')
	{
		var result = parseFloat(income);
		var new_result = number_format(result);
		$('#total_project_cost').val('$'+new_result);
	}
	else
	{
		$('#total_project_cost').val('$0.00');
	}
	
});

$('#borrower_enquity').change(function(){
	var income = replace_dollar($('#borrower_enquity').val());
	if(income != '')
	{
		var result = parseFloat(income);
		var new_result = number_format(result);
		$('#borrower_enquity').val('$'+new_result);
	}
	else
	{
		$('#borrower_enquity').val('$0.00');
	}
	
});

$('#payment_amount').change(function(){
	var income = replace_dollar($('#payment_amount').val());
	if(income != '')
	{
		var result = parseFloat(income);
		var new_result = number_format(result);
		$('#payment_amount').val('$'+new_result);
	}
	else
	{
		$('#payment_amount').val('$0.00');
	}
	
});

$('#late_charges_due').change(function() {
	var late_charges_due  = $('#late_charges_due').val();
	if(late_charges_due != '')
	{
		var rate = number_format(parseFloat(late_charges_due));
		$('#late_charges_due').val(rate+'%');
	}
	else
	{
		var rate = 0;
		$('#late_charges_due').val('0.00%');
	}
	
	
});


//---------On change in payment_type
$('#loan_payment_type').change(function(){
		calulate_baloon_payment();
});


function calulate_baloon_payment()
{
	var payment_type = $('#loan_payment_type').val();
	if(payment_type == '1')
	{
		document.getElementById('loan_ballon_payment').readOnly = true;
		
		var payment_amount 	= replace_dollar($('#payment_amount').val());
		if(payment_amount == '')
		{
			payment_amount = 0;
		}
		payment_amount		= parseFloat(payment_amount);
		
		var loan_amount 	= replace_dollar($('#loan_amount').val());
		if(loan_amount == '')
		{
			loan_amount = 0;
		}
		loan_amount		= parseFloat(loan_amount);
		
		var ballon  		= loan_amount + payment_amount + 0;
		
		$('#loan_ballon_payment').val('$'+number_format(ballon));
	}
	else
	{
		$('#loan_ballon_payment').val('');
		document.getElementById('loan_ballon_payment').readOnly = true;
	}
}


//   onchange event on term_month

$('#term_month').change(function()
{
	calculate_maturity_value();
});


function calculate_maturity_value()
{
	var doc_date 	= $('#loan_funding_date').val();
	var month 		= $('#term_month').val();
	$.ajax({
		type 	: 'POST',
		url 	: '<?php echo base_url()."ajax_call/ajax_calculate_maturity_date"; ?>',
		data 	: { "doc_date" : doc_date , "month" : month },
		success	: function(result){
			$('#maturity_date').val(result);
		}
	});
}


$(".amount_format").change(function(){
	var a = this.value;
	var a = replace_dollar(a);
	if(a == '')
	{
		a = 0;
	}
	if(isNaN(a) == true)
	{
		a = 0;
	}
	a = parseFloat(a);
	this.value = '$'+number_format(a);
});

$('.checkbox_draws_release').change(function(){
	var result = this.value;
	if($(this).attr('checked'))
	{
		$(this).parent().parent().parent().find("input[name='draws_release[]']").val('1');
	}
	else
	{
		$(this).parent().parent().parent().find("input[name='draws_release[]']").val('0');
	}
	
});

function checkbox_draws_release(a)
{
	var result = a.value;
	if($(a).attr('checked'))
	{
		$(a).parent().parent().parent().find("input[name='draws_release[]']").val('1');
	}
	else
	{
		$(a).parent().parent().parent().find("input[name='draws_release[]']").val('0');
	}
}

function draws_amount_format(that)
{
	var a = that.value;
	var a = replace_dollar(a);
	if(a == '')
	{
		a = 0;
	}
	a = parseFloat(a);
	$(that).val('$'+number_format(a))
}

function draws_datepicker(that)
{
	
	$(that).datepicker({
		dateFormat : 'mm-dd-yy'
	});
	$(that).parent().parent().parent().find("input[name='draws_require[]']").focus();
	$(that).focus();
}

$('#select_draws').change(function(){
	var result = $('#select_draws').val();
	if(result == '1')
	{
		$(".draws_button_list").css("display","block");
	}
	else
	{
		$(".draws_button_list").css("display","none");
	}
});

$(document).ready(function(){
	
	var result = $('#select_draws').val();
	
	if(result == '1')
	{
		$(".draws_button_list").css("display","block");
	}
	else
	{
		$(".draws_button_list").css("display","none");
	}
});

$(document).on("change", ".autocal_draws_amount", function() {
    var sum = 0;
    var sum1 = 0;
    var sum2 = 0;
    $(".autocal_draws_amount").each(function(){
		var draw = parseFloat(replace_dollar($(this).val()));
		var release = $(this).parent().parent().find('input#draws_release_amount').val();
		if(release != '')
		{
			release = parseFloat(replace_dollar(release));
			draws_remaining = draw - release;
			$(this).parent().parent().find('input#draws_remaining').val('$'+number_format(draws_remaining));
		}			
		sum += + replace_dollar($(this).val());
    });
    $("#total_draws").val('$'+number_format(sum));
	
	$(".autocal_draws_remaining").each(function(){
        sum1 += + replace_dollar($(this).parent().parent().find('input#draws_remaining').val());
    });
    $("#total_remaining").val('$'+number_format(sum1));
	
	
}); 

$(document).on("change", ".autocal_draws_release_amount", function() {
    var sum = 0;
    var sum1 = 0;
    $(".autocal_draws_release_amount").each(function(){
		var release = parseFloat(replace_dollar($(this).val()));
		var draw = $(this).parent().parent().find('input#draws_amount').val();
		if(draw != '')
		{
			draw = parseFloat(replace_dollar(draw));
			draws_remaining = draw - release;
			$(this).parent().parent().find('input#draws_remaining').val('$'+number_format(draws_remaining));
		}
        sum += + replace_dollar($(this).val());
    });
	// alert('sum');
    $("#total_release_amount").val('$'+number_format(sum));
	
	$(".autocal_draws_remaining").each(function(){
        sum1 += + replace_dollar($(this).parent().parent().find('input#draws_remaining').val());
    });
    $("#total_remaining").val('$'+number_format(sum1));
}); 

$(document).on("change", ".autocal_draws_remaining", function() {
    var sum = 0;
    $(".autocal_draws_remaining").each(function(){
        sum += + replace_dollar($(this).val());
    });
    $("#total_remaining").val('$'+number_format(sum));
}); 
    
function search_loan_by()
{
	$('#form_search_loan_by').submit();
}   

$('#baloon_payment_type').change(function(){
	var baloon_payment_type = $('#baloon_payment_type').val();
	if(baloon_payment_type == 1)
	{
		calulate_baloon_payment();
	}
	else
	{
		$('#loan_ballon_payment').val('$0');
	}
});  

$(document).ready(function(){
	var baloon_payment_type = $('#baloon_payment_type').val();
	if(baloon_payment_type == 1)
	{
		calulate_baloon_payment();
	}
	else
	{
		$('#loan_ballon_payment').val('$0');
	}
});           

$('#loan_type').change(function(){
	var loan_type = $('#loan_type').val();
	if(loan_type == 1)
	{
		$('.loan_type_dependent').css('display','none');
		$('#construction_div').css('display','none');
	}
	else
	{
		$('.loan_type_dependent').css('display','block');
		$('#construction_div').css('display','block');
	}
});        



$(document).ready(function(){
	var loan_type = $('#loan_type').val();
	if(loan_type == 1)
	{
		$('.loan_type_dependent').css('display','none');
		$('#construction_div').css('display','none');

	}
	else
	{
		$('.loan_type_dependent').css('display','block');
		$('#construction_div').css('display','block');

	}
});


function delete_loan_document(that)
{
	var id		= that.id;
	var loan_id	= '<?php echo $loan_id; ?>';
	if(id)
	{
		var r = confirm("Are you sure to delete?");
		if(r == true) {
			window.location.href = '<?php echo base_url();?>delete_loan_document/'+id +'/'+loan_id;
		} 
	}
}

function delete_property_image(id)
{
	var loan_id	= '<?php echo $loan_id; ?>';
	if(id)
	{
		var r = confirm("Are you sure to delete?");
		if(r == true) {
			window.location.href = '<?php echo base_url();?>delete_property_image/'+id +'/'+loan_id;
		} 
	}
}

$('select#draw_escrow_account').change(function(){
	var fund_control_servicer = $('select#draw_escrow_account').val();
	if(fund_control_servicer == '3')
	{
		$('input[name="draw_escrow_other"]').val('<?php echo $fetch_loan_result[0]->fci; ?>');
		$('input[name="draw_escrow_other"]').prop('readonly',true);
	}
	else
	{
		// $('input[name="draw_escrow_other"]').val('');
		$('input[name="draw_escrow_other"]').val('<?php echo $fetch_extra_details[0]->draw_escrow_other; ?>');
		$('input[name="draw_escrow_other"]').prop('readonly',false);
	}
});

function loan_source_and_uses_change(that)
{
	var project_cost 	= $(that).parent().parent('tr').find('input[name="project_cost[]"]').val();
	var borrower_funds 	= $(that).parent().parent('tr').find('input[name="borrower_funds[]"]').val();
	var loan_funds 		= $(that).parent().parent('tr').find('input[name="loan_funds[]"]').val();
	
	project_cost = replace_dollar(project_cost);
	if(project_cost != '')
	{
		project_cost = parseFloat(replace_dollar(project_cost));
	}
	else
	{
		project_cost = 0;
	}
	
	loan_funds = replace_dollar(loan_funds);
	// alert(loan_funds);
	if(loan_funds != '')
	{
		loan_funds = parseFloat(replace_dollar(loan_funds));
	}
	else
	{
		loan_funds = 0;
	}
	
	// calculate borower funds
	borrower_funds = project_cost - loan_funds;
	
	$(that).parent().parent('tr').find('input[name="project_cost[]"]').val('$'+number_format(project_cost));
	
	$(that).parent().parent('tr').find('input[name="borrower_funds[]"]').val('$'+number_format(borrower_funds));
	
	$(that).parent().parent('tr').find('input[name="loan_funds[]"]').val('$'+number_format(loan_funds));
	
	// calculate total project_cost
	var sum = 0;
    $('table#table_loan_source_and_uses tbody input[name="project_cost[]"]').each(function(){
		
		var check = replace_dollar($(this).val());
		if($(this).val() && check != '')
		{
			sum += + parseFloat(replace_dollar($(this).val()));
		}
    });
	$('table#table_loan_source_and_uses tfoot th#total_project_cost').text('$'+number_format(sum));
	
	// calculate total project_cost
	var sum = 0;
    $('table#table_loan_source_and_uses tbody input[name="borrower_funds[]"]').each(function(){
		var check = replace_dollar($(this).val());
		if($(this).val() && check != '')
		{
			sum += + parseFloat(replace_dollar($(this).val()));
		}
    });
	$('table#table_loan_source_and_uses tfoot th#total_borrower_funds').text('$'+number_format(sum));
	// calculate total project_cost
	var sum = 0;
    $('table#table_loan_source_and_uses tbody input[name="loan_funds[]"]').each(function(){
		var check = replace_dollar($(this).val());
		if($(this).val() && check != '')
		{
			sum += + parseFloat(replace_dollar($(this).val()));
		}
    });
	$('table#table_loan_source_and_uses tfoot th#total_loan_funds').text('$'+number_format(sum));
}

// $('input[name="fci_loan_no"]').keydown(function(){
	// var fci = this.value;
	// var d = new Date();
	// var year = d.getFullYear();
	// year = year.toString().substr(-2);
	// alert(fci.length);
// });

function load_borower_content_data()
{
	var borrower_id = $('select#loan-borrower-id').val();
	load_url = '<?php echo base_url().'borrower_data/'; ?>'+borrower_id;
	
	$.ajax({
		type : 'POST',
		url : '<?php echo base_url().'borrower_data_loading/'; ?>',
		data : {'borrower_id':borrower_id },
		success : function(response)
		{
			// alert(response);
			$('div#Borrower_modal div#load-borrower-content').empty();
			$('div#Borrower_modal div#load-borrower-content').append(response);
			
			// $('div#Borrower_modal div#load-borrower-content div.page-head').css('display','none');
			// $('div#Borrower_modal div#load-borrower-content div.row.borrower_data_row').css('padding','6px 0px');
			// $('div#Borrower_modal div#load-borrower-content div#bordered_div_section').css('margin','0px');
			// $('div#Borrower_modal div#load-borrower-content div.row.padding_200').css('padding','0px 3px');
			// $('div#Borrower_modal div#load-borrower-content input[type="button"],input[type="submit"],a,button ').css('display','none');
			// $('div#Borrower_modal div#load-borrower-content form#formID input, select, textarea ,button ,a ').attr('disabled',true);
			
			// $('div#Borrower_modal div#load-borrower-content button#display-block').css('display','block');
			// $('div#Borrower_modal div#load-borrower-content button#display-block').attr('disabled',false);
		}
	});
	
}

function construction_reserve_funds_disbursed(that)
{
	if($(that).is(':checked'))
	{
		$(that).parent().parent().parent().parent('tr').find('input[name="funds_disbursed[]"]').val('1');
		construction_reserve_readonly_css_setting(that);
	}
	else
	{
		$(that).parent().parent().parent().parent('tr').find('input[name="funds_disbursed[]"]').val('0');
		construction_reserve_readonly_css_setting(that);
	}
}

function construction_reserve_request_submitted(that)
{
	if($(that).is(':checked'))
	{
		$(that).parent().parent().parent().parent('tr').find('input[name="request_submitted[]"]').val('1');
		construction_reserve_readonly_css_setting(that);
	}
	else
	{
		$(that).parent().parent().parent().parent('tr').find('input[name="request_submitted[]"]').val('0');
		construction_reserve_readonly_css_setting(that);
	}
}

function construction_reserve_readonly_css_setting(that)
{
	var request_submitted = $(that).parent().parent().parent().parent('tr').find('input[name="request_submitted[]"]').val();
	var funds_disbursed = $(that).parent().parent().parent().parent('tr').find('input[name="funds_disbursed[]"]').val();
	if(request_submitted == '1' || funds_disbursed == '1')
	{
		$(that).parent().parent().parent().parent('tr').find('input').attr('readonly',true);
		$(that).parent().parent().parent().parent('tr').find('textarea:not([name="draws_notes[]"])').attr('readonly',true);
		$(that).parent().parent().parent().parent('tr').find('input').css('background','#dedede');
		$(that).parent().parent().parent().parent('tr').find('input').css('border','1px solid darkgrey');
		$(that).parent().parent().parent().parent('tr').find('textarea:not([name="draws_notes[]"])').css('background','#dedede');
	}
	else
	{
		$(that).parent().parent().parent().parent('tr').find('input').attr('readonly',false);
		$(that).parent().parent().parent().parent('tr').find('textarea').attr('readonly',false);
		$(that).parent().parent().parent().parent('tr').find('input').css('background','none');
		$(that).parent().parent().parent().parent('tr').find('input').css('border','1px solid darkgrey');
		$(that).parent().parent().parent().parent('tr').find('textarea').css('background','none');
	}
}

function construction_reserve_readonly_css_setting1(that)
{
	
	var request_submitted = $(that).parent().parent('tr').find('input[name="request_submitted[]"]').val();
	var funds_disbursed = $(that).parent().parent('tr').find('input[name="funds_disbursed[]"]').val();
	if(request_submitted == '1' || funds_disbursed == '1')
	{
		$(that).parent().parent('tr').find('input').attr('readonly',true);
		$(that).parent().parent('tr').find('textarea:not([name="draws_notes[]"])').attr('readonly',true);
		$(that).parent().parent('tr').find('input').css('background','#dedede');
		$(that).parent().parent('tr').find('input').css('border','1px solid darkgrey');
		$(that).parent().parent('tr').find('textarea:not([name="draws_notes[]"])').css('background','#dedede');
		
	}
	else
	{
		$(that).parent().parent('tr').find('input').attr('readonly',false);
		$(that).parent().parent('tr').find('textarea').attr('readonly',false);
		$(that).parent().parent('tr').find('input').css('background','none');
		$(that).parent().parent('tr').find('input').css('border','1px solid darkgrey');
		$(that).parent().parent('tr').find('textarea').css('background','none');
	}
}




  </script>