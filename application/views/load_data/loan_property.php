<?php 
//------------values from myConfig---------------
$borrower_option = $this->config->item('borrower_option');
$yes_no_option = $this->config->item('yes_no_option');
$loan_type_option = $this->config->item('loan_type_option');
$position_option = $this->config->item('position_option');
$STATE_USA = $this->config->item('STATE_USA');
 ?>
 <style>
 .property_loan_row {
    padding-top: 21px;
   
	    padding-left: 117px;
}
</style>
<div class="page-content-wrapper">
	<div class="page-content">
			<?php if($this->session->flashdata('error')!=''){  ?>
				 <div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error');?></div><?php } ?>
				  <?php if($this->session->flashdata('success')!=''){  ?>
				 <div id='success'><i class='fa fa-thumbs-o-up'></i><?php echo $this->session->flashdata('success');?></div><?php } ?>
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>Property <small></small></h1>
				</div>
				<!-- END PAGE TITLE -->
				<!-- BEGIN PAGE TOOLBAR -->
				
				<!-- END PAGE TOOLBAR -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					 Dashboard
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<form method="POST" action = "<?php echo base_url();?>add_property">
				<div class="row property_loan_row">
							<div class="col-md-3">
								<label>Property Address :</label>
								<input type="text" id="" class="form-control" name="property_address"  required />
							</div>
							<div class="col-md-3">
							<label>Unit# :</label>
								<input type="text" class="form-control number_only" name = "unit" required />
							</div>
							<div class="col-md-3">
							<label>City :</label>
								<input type="text" class="form-control" name = "city" required />
							</div>
							
				</div>
				
				<div class="row property_loan_row">
							<div class="col-md-3">
								<label>State :</label>
								<select  class="form-control" name = "state">
								<?php 
								foreach($STATE_USA as $row => $key)
								{
									echo "<option value = '$row' >".$key."</option>";
								}
								?>
									
									
								</select>
							</div>
							<div class="col-md-3">
							<label>Zip :</label>
								<input type="text" class="form-control number_only" name = "zip" required />
							</div>
							<div class="col-md-3">
							<label>Country :</label>
								<select  class="form-control" name = "country">
									<option value = 'A'>A</option>
									<option value = 'B'>B</option>
									
								</select>
							</div>
							
				</div>
				
				<div class="row property_loan_row">
							<div class="col-md-3">
								<label>YR Built :</label>
								<select id="yr_built" class="form-control " name="yr_built" >
									<?php 
									for($year = 2016; $year >= 1900; $year--)
									{
										echo "<option value = '$year'>".$year."</option>";
									}
									?>
								</select>
							</div>
							<div class="col-md-3">
							<label>APN :</label>
								<input type="text" class="form-control" name = "apn"  />
							</div>
							<div class="col-md-3">
							<label>Property type :</label>
								<select  class="form-control" name = "property_type">
									<option value = 'A'>Type A</option>
									<option value = 'B'>Type B</option>
									<option value = 'C'>Type C</option>
									
								</select>
							</div>
							
				</div>
				
				<div class="row property_loan_row">
							<div class="col-md-3">
								<label>Square Feet :</label>
								<input type="text"  class="form-control" name="square_feet"  />
							</div>
							<div class="col-md-3">
							<label># of Bedrooms  :</label>
								<input type="text" class="form-control number_only" name = "bedrooms"  />
							</div>
							<div class="col-md-3">
							<label># of Bathrooms :</label>
								<input type="text" class="form-control number_only" name = "bathrooms"  />
							</div>
							
				</div>
				
				<div class="row property_loan_row">
							<div class="col-md-3">
								<label>Garage :</label>
								<select  class="form-control" name = "garage">
									<option value = 'A'>Garage A</option>
									<option value = 'B'>Garage B</option>
									<option value = 'C'>Garage C</option>
									
								</select>
							</div>
							<div class="col-md-3">
							<label>Pool :</label>
								<select  class="form-control" name = "pool">
									<option value = 'A'>Pool A</option>
									<option value = 'B'>Pool B</option>
									<option value = 'C'>Pool C</option>
									
								</select>
							</div>
							<div class="col-md-3">
							<label>Conditions :</label>
								<select  class="form-control" name = "conditions">
									<option value = 'A'>A</option>
									<option value = 'B'>B</option>
									<option value = 'C'>C</option>
									
								</select>
							</div>
							
				</div>
				
				<div class="row property_loan_row">
							<div class="col-md-3">
								<label>Gross Monthly Income :</label>
								<input type="text"  id="gross_monthly_income" class="form-control number_only cal_amount" name="gross_monthly_income"  />
							</div>
							<div class="col-md-3">
							<label>Gross Monthly Expence :</label>
								<input type="text" id="gross_monthly_expense" class="form-control number_only cal_amount" name = "gross_monthly_expense"  />
							</div>
							<div class="col-md-3">
							<label>Net Income :</label>
								<input type="text" id="net_income" class="form-control number_only" name = "net_income"  readonly />
							</div>
							
				</div>
				
				<div class="row property_loan_row">
							<div class="col-md-3">
								<label>Purchase Price :</label>
								<input type="text"  class="form-control number_only" name="purchase_price"  />
							</div>
							<div class="col-md-3">
							<label>Renovation Cost :</label>
								<input type="text" class="form-control number_only" name = "renovation_cost"  />
							</div>
							<div class="col-md-3">
							<label>Appraised / Finished ARV :</label>
								<input type="text" class="form-control number_only" name = "arv"  />
							</div>
							
				</div>
				
				<div class="row property_loan_row">
							<div class="col-md-3">
								<label>Annual Property Taxes :</label>
								<input type="text"  class="form-control" name="annual_property_taxes"  />
							</div>
							<div class="col-md-3">
							<label>Insurance Carrier :</label>
								<input type="text" class="form-control" name = "insurance_carrier"  />
							</div>
							<div class="col-md-3">
							<label>Insurance Expiration :</label>
								<input type="text" id="expiration" class="form-control" name = "insurance_expiration"  placeholder = 'dd-mm-yyyy' />
							</div>
							
				</div>
				
				<div class="row property_loan_row">
							<div class="col-md-3">
								<label>Valuation Type :</label>
								<select  class="form-control" name = "valuation_type">
									<option value = 'A'>Type A</option>
									<option value = 'B'>Type B</option>
									<option value = 'C'>Type C</option>
									
								</select>
							</div>
							<div class="col-md-3">
							<label>Appraiser Name :</label>
								<input type="text" class="form-control" name = "appraiser_name"  />
							</div>
							<div class="col-md-3">
							<label>Appraisal date :</label>
								<input type="text" id="apraisal_date" class="form-control" name = "apraisal_date" placeholder = 'dd-mm-yyyy' />
							</div>
							
				</div>
				
				<div class="row property_loan_row">
							<div class="col-md-3">
								<label># of unit :</label>
								<input type="text"  class="form-control number_only" name="of_unit"  />
							</div>
							<div class="col-md-3">
							<label>Sales Refinance Value :</label>
								<input type="text" class="form-control number_only" name = "sales_refinance_value"  />
							</div>
							
				</div>
				
				<div class="row property_loan_row">
							<div class="col-md-8">
								<label>Construction Type :</label>
								<textarea type="text"  class="form-control" name="construction_type" ></textarea>
							</div>	
				</div>
				
				<div class="row property_loan_row">
							<div class="col-md-8">
								<label>Legal Description :</label>
								<textarea type="text"  class="form-control" name="legal_description" ></textarea>
							</div>	
				</div>
				
				<div class="row property_loan_row">
							<div class="form-group">
								  
								  <div class="col-md-9">
									<button id="" name="button" class="btn btn-primary borrower_save_button" value="save">Save</button>
									<button id="" name="button" class="btn btn-primary borrower_save_button" value="add">Add</button>
								  </div>
							</div>
				</div>
				
			</form>
	</div>
	<!-- END CONTENT -->
</div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  // for datepicker
  $( function() {
    $( "#apraisal_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
    $( "#expiration" ).datepicker({ dateFormat: 'dd-mm-yy' });
    // $( "#loan_document_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
  } );
  
  //    --------- for calculate net income in url  talimar/loan_property
  $(".cal_amount").change( function(){
	  
	  var income = $("#gross_monthly_income").val();
	  var expense = $("#gross_monthly_expense").val();
	  // alert(expense);
	  var net = income - expense - 0;
	  $("#net_income").val(net);
  });
  //--------------------------------------------------------
 </script>