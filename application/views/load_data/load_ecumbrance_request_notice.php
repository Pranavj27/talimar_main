<div class="ecumbrance_foot_section">
	<input type="hidden" name="ecumbrance_id" value="<?php echo $ecumbrance_id; ?>">
	<div class="row">
		<div class="col-sm-4">
			<label>Instrument Number: </label>
			<input class="form-control" name="instrument" value="<?php echo isset($encumbrance_request_notice) ? $encumbrance_request_notice->instrument : ''; ?>">
		</div>
		<div class="col-sm-4">
			<label>Recording Date: </label>
			<input class="form-control datepicker" name="recording_date" value="<?php echo isset($encumbrance_request_notice) ? $encumbrance_request_notice->recording_date : ''; ?>">
		</div>
		<div class="col-sm-4">
			<label>County Recorded: </label>
			<input class="form-control" name="county" value="<?php echo isset($encumbrance_request_notice) ? $encumbrance_request_notice->county : ''; ?>">
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-4">
			<label>Trustor: </label>
			<input class="form-control" name="trustor" value="<?php echo isset($encumbrance_request_notice) ? $encumbrance_request_notice->trustor : ''; ?>">
		</div>
		<div class="col-sm-4">
			<label>Beneficiary: </label>
			<input class="form-control" name="beneficiary" value="<?php echo isset($encumbrance_request_notice) ? $encumbrance_request_notice->beneficiary : ''; ?>">
		</div>
		<div class="col-sm-4">
			<label>Executive By: </label>
			<input class="form-control" name="executed_by" value="<?php echo isset($encumbrance_request_notice) ? $encumbrance_request_notice->executed_by : ''; ?>">
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-4">
			<label>Trustee: </label>
			<input class="form-control" name="trustee" value="<?php echo isset($encumbrance_request_notice) ? $encumbrance_request_notice->trustee : ''; ?>">
		</div>
	</div>
</div>	