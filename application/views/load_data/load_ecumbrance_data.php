<?php
$position_option  			= $this->config->item('position_option');
$yes_no_option  			= $this->config->item('yes_no_option');
$yes_no_option3  			= $this->config->item('yes_no_option3');
$yes_no_option4  			= $this->config->item('yes_no_option4');
$no_yes_option  			= $this->config->item('no_yes_option');
$yes_how_many_times  		= $this->config->item('yes_how_many_times');
$loan_intrest_type_option  	= $this->config->item('loan_intrest_type_option');
$ecum_position_option  		= $this->config->item('ecum_position_option');

$request_for_notice_option  		= $this->config->item('request_for_notice_option');
?>
<?php
// echo'<pre>';
// print_r($fetch_ecumbrances);
// echo'</pre>';
?>

<div class="row property_loan_row lien_data">
	<h4>Lien Information </h4>
	<div class="col-sm-4">
		<label class="label-control">Lender Name</label>
		<input type="text" class="form-control" name="lien_holder" value="<?php echo $fetch_ecumbrances->lien_holder; ?>" >
	</div>
	
</div>

<div class="row property_loan_row lien_data">
	<div class="col-sm-4">
		<label class="label-control">Original Balance</label>
		<input type="text" class="form-control number_only" name="original_balance" value="<?php echo '$'.number_format($fetch_ecumbrances->original_balance,2); ?>" onchange="amount_format_with_dollar(this)">
	</div>
	<div class="col-sm-4">
		<label class="label-control">Current Balance</label>
		<input type="text" class="form-control number_only" name="current_balance" value="<?php echo '$'.number_format($fetch_ecumbrances->current_balance,2); ?>" onchange="amount_format_with_dollar(this)">
	</div>
	<div class="col-sm-4">
		<label class="label-control">Balance at Close</label>
		<input type="text" class="form-control" name="balance_at_close" value="<?php echo '$'.number_format($fetch_ecumbrances->balance_at_close,2); ?>" onchange="amount_format_with_dollar(this)">
	</div>
</div>

<div class="row property_loan_row">
	<div class="col-sm-4">
		<label class="label-control">Note Rate</label>
		<input type="text" class="form-control number_only" name="intrest_rate" value="<?php echo number_format($fetch_ecumbrances->intrest_rate,3).'%'; ?>" onchange="rate_format_with_percent(this)">
	</div>

	
	<div class="col-sm-4">
		<label class="label-control">Monthly Payment</label>
		<input type="text" class="form-control number_only" name="monthly_payment" value="<?php echo '$'.number_format($fetch_ecumbrances->monthly_payment,2); ?>" onchange="amount_format_with_dollar(this)">
		
	</div>
	<div class="col-sm-4">
		<label class="label-control">Balloon Payment</label>
		<input type="text" class="form-control number_only" name="ballon_payment_b" value="<?php echo '$'.number_format($fetch_ecumbrances->ballon_payment_b,2); ?>" onchange="amount_format_with_dollar(this)">
	</div>
</div>

<div class="row property_loan_row">
	<div class="col-sm-4">
		<label class="label-control">Rate Type</label>
		<select class="form-control" name="rate_type">
			<?php
			foreach($loan_intrest_type_option as $key => $option)
			{
				$selected = '';
				if($fetch_ecumbrances->rate_type == $key)
				{
					$selected = 'selected';
				}
				?>
				<option value="<?php echo $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
				<?php
			}
			?>
		</select>
	</div>
	<div class="col-sm-4">
		<label class="label-control">Variable</label>
		<select class="form-control" name="variable">
			<?php
			foreach($yes_no_option as $key => $option)
			{
				$selected = '';
				if($fetch_ecumbrances->variable == $key)
				{
					$selected = 'selected';
				}
				?>
				<option value="<?php echo $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
				<?php
			}
			?>
		</select>
	</div>
	<div class="col-sm-4">
		<label class="label-control">Max Rate</label>
		<input type="text" class="form-control number_only" name="max_rate" value="<?php echo number_format($fetch_ecumbrances->max_rate,3).'%'; ?>" onchange="rate_format_with_percent(this)">
	</div>
</div>
<div class="row property_loan_row">
	<div class="col-sm-4">
		<label class="label-control">Instrument #</label>
		<input type="text" class="form-control" name="instrument" value="<?php echo $fetch_ecumbrances->instrument; ?>">
	</div>
	<div class="col-sm-4">
		<label class="label-control">Document Date</label>
		
		<input type="text" class="form-control datepicker" name="document_date" value="<?php echo date('m-d-Y',strtotime($fetch_ecumbrances->document_date)); ?>">
	</div>
	<div class="col-sm-4">
		<label class="label-control">Recording Date</label>
		
		<input type="text" class="form-control datepicker" name="recording_date" value="<?php echo date('m-d-Y',strtotime($fetch_ecumbrances->recording_date)); ?>">
	</div>
	
	<!--<div class="col-sm-4">
		<label class="label-control">Beneficiary Name</label>
		<input type="text" class="form-control" name="beneficiary_name" value="<?php echo $fetch_ecumbrances->beneficiary_name; ?>">
	</div>-->
</div>

<div class="row property_loan_row">
	<div class="col-sm-4">
		<label class="label-control">Trustee</label>
		<input type="text" class="form-control" name="trustee" value="<?php echo $fetch_ecumbrances->trustee; ?>">
	</div>


	<div class="col-sm-4">
			<label class="label-control">Maturity Date</label>
			<input type="text" class="form-control datepicker" name="maturity_date" value="<?php echo date('m-d-Y', strtotime($fetch_ecumbrances->maturity_date)); ?>">
	</div>
</div>

<!--<div class="row property_loan_row">
	<div class="col-sm-12">
		<label class="label-control">Beneficiary Vesting</label>
		
		<textarea type="text" name="beneficiary_vesting" class="form-control" rows="3"><?php echo $fetch_ecumbrances->beneficiary_vesting; ?></textarea>
	</div>
</div>-->

<div class="row property_loan_row">
	<div class="col-sm-4">
		<label class="label-control">Beneficiary Contact Name</label>
		<input type="text" name="b_contact_name" class="form-control" value="<?php echo $fetch_ecumbrances->b_contact_name;?>">
		<!--<select name="b_contact_name" class="form-control" onchange="fetch_contcat_title(this);">
		<option value="">Select One</option>
			<?php foreach($fetch_all_contact_ecum as $key => $row){ ?>
				<option value="<?php echo $row->contact_id;?>" <?php if($row->contact_id == $fetch_ecumbrances->b_contact_name){echo 'Selected';}?>><?php echo $row->contact_firstname .' '.$row->contact_middlename .' '.$row->contact_lastname;?></option>
			<?php } ?>
		</select>-->
	</div>
	<div class="col-sm-4">
		<label class="label-control">Beneficiary Title</label>
		<input type="text" name="b_title" id="b_title" class="form-control" value="<?php echo $fetch_ecumbrances->b_title; ?>">
	</div>
</div>

<!--<div class="row property_loan_row">
	<div class="col-sm-4">
		<label class="label-control">Proposed Priority</label>
		<select class="form-control" name="position">
			<?php
			foreach($position_option as $key => $option)
			{
				$selected = '';
				if($fetch_ecumbrances->position == $key)
				{
					$selected = 'selected';
				}
				?>
				<option value="<?php echo  $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
				<?php
			}
			?>
		</select>
	</div>-->
	
	<!--<div class="col-sm-4">
		<label class="label-control">Priority to Talimar</label>
		<select name="priority_to_talimar" class="form-control">
			<?php
			foreach($yes_no_option as $key => $option)
			{
				$selected = '';
				if($key == $fetch_ecumbrances->priority_to_talimar)
				{
					$selected = 'selected';
				}
				?>
				<option value="<?php echo $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
				<?php
			}
			?>
		</select>
	</div>
</div>-->
<!--
<div class="row property_loan_row">
	<div class="col-sm-4">
		<label class="label-control"> Priority</label>
		<select class="form-control" name="priority">
			<?php
			foreach($yes_no_option as $key => $option)
			{
				$selected = '';
				if(isset($fetch_ecumbrances->priority) && ($fetch_ecumbrances->priority !='')){
					if($fetch_ecumbrances->priority == $key)
					{
						$selected = 'selected';
					}else{
						
						$selected = '';
					}
				}else{
					$selected = '';
				}
				?>
				<option value="<?php echo  $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
				<?php
			}
			?>
		</select>
	</div>
</div>
-->
<div class="ecumbrance_foot_section">
	<input type="hidden" name="ecumbrance_id" value="<?php echo $ecumbrance_id; ?>">
	<?php
			$readonly = '';
			$disabled = '';
			$readonly1 = '';
			$disabled1 = '';
			if(isset($fetch_encumbrance_data_table->payment_lates)){
				if($fetch_encumbrance_data_table->payment_lates == '0'){
					$readonly = 'readonly';
					$disabled = 'disabled';
					
					
				}else{
					$readonly = '';
					$disabled = '';
					
				}
			}
			
			if(isset($fetch_ecumbrances->existing_lien)){
				if($fetch_ecumbrances->existing_lien == '0'){
					$readonly1 = 'readonly';
					$disabled1 = 'disabled';
					
					
				}else{
					$readonly1 = '';
					$disabled1 = '';

					
				}
			}
			
		?>
		
	<div class="row property_loan_row lien_data">
	<h4>Lien Disclosures:</h4>
	<div class="row">
		<div class="col-sm-8">
			<label>Is this an existing Loan? </label>
		</div>
		<div class="col-sm-3">
			<select class="form-control" name="existing_lien" onchange="lock_all_rows(this.value);">
				<?php
				foreach($yes_no_option3 as $key => $option)
				{
					$selected = '';
					if($fetch_ecumbrances->existing_lien == $key)
					{
						$selected = 'selected';
					}
					?>
					<option value="<?php echo  $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
					<?php
				}
				?>
			</select>
			<input type="hidden" id="existing_lien" value="<?php echo $fetch_ecumbrances->existing_lien; ?>">
		</div>
	</div>
</div>
<div class="row property_loan_row lien_data">		
	<div class="row">
		<div class="col-sm-8">
			<label  style="margin-left:20px !important;">What position is the loan?</label>
		</div>
		<div class="col-sm-3">
			<select <?php echo $disabled1; ?> class="form-control" name="current_position" id="current_position">
				<?php
				foreach($position_option as $key => $option)
				{
					$selected = '';
					if($fetch_ecumbrances->current_position == $key)
					{
						$selected = 'selected';
					}
					?>
					<option value="<?php echo  $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
					<?php
				}
				?>
			</select>
		</div>
	</div>
</div>

<div class="row property_loan_row lien_data">
	<div class = "row">
		<div class="col-sm-8">
			<label style="margin-left:20px !important;">Will the loan be paid off close?</label>
		</div>
		<div class="col-sm-3">
			<select <?php echo $disabled1; ?> onchange = "enable_est_field(this.value);" class="form-control" name="will_it_remain" id="will_it_remain">
				<?php
				foreach($yes_no_option as $key => $option)
				{
					$selected = '';
					if($fetch_ecumbrances->will_it_remain == $key)
					{
						$selected = 'selected';
						
					}
					if($fetch_ecumbrances->will_it_remain != '1'){

                             $text="N/A";
                              $dis="disabled";
					}else{

                      $text='$'.number_format($fetch_ecumbrances->est_payoff,2);
                       $dis="";
					}


					?>
					<option value="<?php echo  $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
					<?php
				}
				?>
			</select>
		</div>
	</div>
</div>
<div class="row property_loan_row lien_data">
	<div class="row">
		<div class="col-sm-8">
			<label class="label-control" style="margin-left:40px !important;">What is the payoff balance?</label>
		</div>	
		<div class="col-sm-3">

		
			<input  <?php echo $readonly1; ?><?php echo $dis;?> type="text" class="amount_format form-control number_only" id ="est_payoff" name="est_payoff" value="<?php echo $text; ?>" onchange="amount_format_with_dollar(this)">
		</div>
		
	</div>
</div>
	
	
	
<!--<div class="ecumbrance_foot_section">
	<input type="hidden" name="ecumbrance_id" value="<?php echo $ecumbrance_id; ?>">-->
	<div class="row property_loan_row">
		
		<div class = "row">
		
			<div class="col-sm-8" >
				<label style="margin-left:20px !important;">Has the loan been delinquent greater than 60 days in the last 12 months?</label>
			</div>	
			<div class="col-sm-3">
				<select <?php echo $disabled1; ?> id="ecum_data_id_payment_lates" onchange="ecum_data_payment_lates(this)" name = "payment_lates" class = "form-control">
					<?php
					foreach($yes_no_option3 as $key => $option)
					{
						$selected = '';
						if(isset($fetch_encumbrance_data_table)){
							if($fetch_encumbrance_data_table->payment_lates == $key)
							{
								$selected = 'selected';
							}
						}
						?>
						<option value="<?php echo  $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
						<?php
					}
					?>
				</select>
			
				<!--<input type = 'radio' name="payment_lates" value="1" <?php echo (isset($fetch_encumbrance_data_table) && $fetch_encumbrance_data_table->payment_lates == 1) ? 'checked' : ''; ?>  onchange="ecum_data_payment_lates(1)" id="ecum_data_id_payment_latesy"> Yes &nbsp;&nbsp;<input type = 'radio' name="payment_lates" value="2" <?php echo (isset($fetch_encumbrance_data_table) && $fetch_encumbrance_data_table->payment_lates == 2) ? 'checked' : ''; ?> onchange="ecum_data_payment_lates(0)" id="ecum_data_id_payment_lates"> No-->
			</div>
	</div>
	
	

	<div class="row property_loan_row">
	
		<div class="col-sm-8 padding-left-40">
			<label style="margin-left:40px !important;">If Yes, how many times</label>
		</div>
		<div class="col-sm-3">
			<!--<input <?php echo $readonly; ?> <?php echo $readonly1; ?> class = "form-control <?php echo $class_color;?>" type = 'text' name="yes_how_many" id="yes_how_many" value="<?php echo isset($fetch_encumbrance_data_table) ? $fetch_encumbrance_data_table->yes_how_many : ''; ?>">-->
			
			<select <?php echo $disabled; ?> <?php echo $disabled1; ?> name="yes_how_many" class="form-control <?php echo $class_color;?>" id="yes_how_many">
			<?php foreach($yes_how_many_times as $key => $row){ ?>
				<option value="<?php echo $key; ?>" <?php if($key == $fetch_encumbrance_data_table->yes_how_many){echo 'Selected';}?>><?php echo $row; ?></option>
			<?php } ?>
			</select>
		</div>
	</div>
	
	

	<div class="row property_loan_row">
		<div class="col-sm-8">
			<label style="margin-left:40px !important;">If Yes, will any of these payments remain unpaid</label>
		</div>
		<div class="col-sm-3">		
		<select <?php echo $disabled; ?> <?php echo $disabled1; ?> name ="payment_unpaid" class = "form-control <?php echo $class_color;?>" id="payment_unpaid">
			<?php
				foreach($yes_no_option4 as $key => $option)
				{
					$selected = '';
					if(isset($fetch_encumbrance_data_table)){
						if($fetch_encumbrance_data_table->payment_unpaid == $key)
						{
							$selected = 'selected';
						}
					}
					?>
					<option value="<?php echo  $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
					<?php
				}
			?>
		</select>
			<!--<input disabled type = 'radio' name="payment_unpaid" value="1" <?php echo (isset($fetch_encumbrance_data_table) && $fetch_encumbrance_data_table->payment_unpaid == 1) ? 'checked' : ''; ?> > Yes &nbsp;&nbsp;
			<input disabled type = 'radio' name="payment_unpaid" value="2" <?php echo (isset($fetch_encumbrance_data_table) && $fetch_encumbrance_data_table->payment_unpaid == 2) ? 'checked' : ''; ?> > No-->
		</div>
	</div>

	<div class="row property_loan_row">
		<div class="col-sm-8">
			<!--<label>(i) If Yes, will the proceeds of the Subject Loan be used to cure the delinquency?</label>-->
			<label style="margin-left:40px !important;">If Yes, will the loan proceeds repay the delinquent payments</label>
		</div>	
		<div class="col-sm-3">
			<select <?php echo $disabled; ?> <?php echo $disabled1; ?> name ="yes_proceeds_subject_loan" class = "form-control <?php echo $class_color;?>" id="yes_proceeds_subject_loan">
				<?php
				foreach($yes_no_option4 as $key => $option)
				{
					$selected = '';
					if(isset($fetch_encumbrance_data_table->yes_proceeds_subject_loan)){
						if($fetch_encumbrance_data_table->yes_proceeds_subject_loan == $key)
						{
							$selected = 'selected';
						}
					}
					?>
					<option value="<?php echo  $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
					<?php
				}
				?>
			
			</select>
			
			<!--<input disabled type = 'radio' name="yes_proceeds_subject_loan" value="1"<?php echo (isset($fetch_encumbrance_data_table) && $fetch_encumbrance_data_table->yes_proceeds_subject_loan == 1) ? 'checked' : ''; ?>  > Yes &nbsp;&nbsp;
			<input disabled type = 'radio' name="yes_proceeds_subject_loan" value="2"  <?php echo (isset($fetch_encumbrance_data_table) && $fetch_encumbrance_data_table->yes_proceeds_subject_loan == 2) ? 'checked' : ''; ?>> No-->
		</div>
	</div>	
	<div class="row property_loan_row">
		<div class="col-sm-8">
			<label style="margin-left:60px !important;">If No, what is the source of the funds to bring the loan current?</label>
		</div>
		<div class="col-sm-3">		
			<input <?php //echo $readonly; ?> <?php echo $readonly1; ?> class = "form-control <?php echo $class_color;?>" type = 'text' name="no_source_of_fund" id="no_source_of_fund" value="<?php echo isset($fetch_encumbrance_data_table) ? $fetch_encumbrance_data_table->no_source_of_fund : ''; ?>">
		</div>
	</div>
	
 </div>
</div>
		
<!--<div class="row property_loan_row">
	<div class = "row">
		
		<div class="col-sm-4">
			<label class="label-control">Priority</label>
			<select class="form-control" name="existing_priority">
				<?php
				foreach($position_option as $key => $option)
				{
					$selected = '';
					if($fetch_ecumbrances->existing_priority == $key)
					{
						$selected = 'selected';
					}
					?>
					<option value="<?php echo  $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
					<?php
				}
				?>
			</select>
		</div>

	
		
		
	</div>
	
	
</div>-->

<div class="row property_loan_row lien_data">
	<!--<h4>If Loan is Proposed / Remain:</h4>-->
	<div class = "row">
		
		<div class="col-sm-8">
			<label class="label-control">What will be the position of the loan after close?</label>
		</div>
		<div class="col-sm-3">
			<!--<input type = "textbox"  class = "form-control" value = "<?php echo $fetch_ecumbrances->proposed_priority ? $fetch_ecumbrances->proposed_priority : '';?>" name = "proposed_priority"/>-->
			
			
			<select class = "form-control" name = "proposed_priority">
				<?php foreach($ecum_position_option as $key => $optn) { ?>
				<option value="<?php echo $key;?>" <?php if($key == $fetch_ecumbrances->proposed_priority) { echo 'selected';} ?> ><?php echo $optn;?></option>
				<?php } ?>
			</select>
			
		</div>
	</div>
</div>

<div class="row property_loan_row lien_data">
	<div class = "row">
		<div class="col-sm-8">
			<label class="label-control">Will the loan have priority over the proposed loan?</label>
		</div>
		<div class="col-sm-3">
			<select class="form-control" name="existing_priority_to_talimar">
				<?php
				foreach($yes_no_option4 as $key => $option)
				{
					$selected = '';
					if($fetch_ecumbrances->existing_priority_to_talimar == $key)
					{
						$selected = 'selected';
					}
					?>
					<option value="<?php echo  $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
					<?php
				}
				?>
			</select>
		</div>
	</div>	
</div>



<div class="row property_loan_row lien_data">
	<div class ="row">
		<div class="col-sm-8">
			<label class="label-control">Will a Request for Notice be required?</label>
		</div>
		<div class="col-sm-3">
			<select class="form-control" name="Request_for_Notice_talimar">
				<?php
				foreach($request_for_notice_option as $key => $option)
				{
					$selected = '';
					if($fetch_ecumbrances->request_for_notice_talimar == $key)
					{
						$selected = 'selected';
					}
					?>
					<option value="<?php echo  $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
					<?php
				}
				?>
			</select>
		</div>
	</div>	
</div>


<div class="row">&nbsp;&nbsp;</div>
<div class="row property_loan_row lien_data" style="position:relative;float: right;margin-top: 23px !important; margin-left: 5px !important;">
	<div class="row">
		<a class="btn blue pull-right" href="<?php echo base_url().'Load_data/subordination_agreement/'.$ecumbrance_id;?>">Subordination Agreement</a>
	</div>
</div>
<div class="row property_loan_row lien_data" style="position:relative;float: right;margin-top: 23px !important; margin-left: 5px !important;">
	<div class="row">
		<a class="btn blue pull-right" href="<?php echo base_url().'Load_data/request_for_notice/'.$ecumbrance_id;?>">Request for Notice</a>
	</div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
<style>
.chosen-drop {
    width: 253px !important;
    margin-left: 0px !important;
}	
.row.borrower_data_row div {
    width: 96% !important;
}
.chosen-container{
	display: block !important;
}
.chosen-container.chosen-container-single {
    width: 253px !important;
}
.chosen-container-single .chosen-single {
    padding: 4px 0px 26px 6px !important;
    border-radius: 4px !important;
    border: 1px solid #e1e1e1 !important;
}
</style>
					
<script>
 $(".chosen").chosen();

$('input[name="maturity_date"]').datepicker({ dateFormat: 'mm-dd-yy' });
$('input[name="recording_date"]').datepicker({ dateFormat: 'mm-dd-yy' });
$('input[name="document_date"]').datepicker({ dateFormat: 'mm-dd-yy' });

function enable_est_field(value){
	var val1 = value;

	if(val1 == '1'){
		//$('#est_payoff').attr('disabled',false);
		$('#est_payoff').attr('disabled',false);
		$('#est_payoff').val('');
		
	}else{
		
		$('#est_payoff').attr('disabled',true);
		$('#est_payoff').val('N/A');
		
	}
}



function fetch_contcat_title(that){
	
	var c_id = that.value;
	//alert(c_id);
	$.ajax({
		type	: 'POST',
		url		: '<?php echo base_url()."Ajax_call/fetch_contact_details";?>',
		data	: {'contact_id':c_id},
		success	: function(response){
			if(response){
				//alert(response);
				var data = JSON.parse(response);
				
				$('div.property_loan_row input#b_title').val(data.title);
				$('div.property_loan_row input#b_title').text(data.title);
			}
		}
		
			
	})
}



ecum_data_payment_lates(this);
function ecum_data_payment_lates(that)
{
	//alert(payment_lates);

	var payment_lates = $('div.ecumbrance_foot_section select#ecum_data_id_payment_lates').val();
	//alert(payment_lates);
	if(payment_lates == '2')
	{
		//$('div.ecumbrance_foot_section input[type="text"]').val('');
		//$('div.ecumbrance_foot_section input[type="radio"]').attr('checked',false);
		//$('div.ecumbrance_foot_section input#ecum_data_id_payment_lates').attr('checked',true);
		
		//$('div.ecumbrance_foot_section input[type="radio"]').attr('readonly',true);
		$('div.ecumbrance_foot_section select#yes_how_many').attr('disabled',true);
		$('div.ecumbrance_foot_section select#yes_proceeds_subject_loan').attr('disabled',true);
		$('div.ecumbrance_foot_section select#payment_unpaid').attr('disabled',true);
		//$('div.ecumbrance_foot_section input[type="text"]').attr('readonly',true);
		//$('div.ecumbrance_foot_section input[name="no_source_of_fund"]').attr('readonly',false);
		//$('div.ecumbrance_foot_section input#ecum_data_id_payment_lates').attr('disabled',false);
		//$('div.ecumbrance_foot_section input#ecum_data_id_payment_latesy').attr('readonly',false);
		$('div.ecumbrance_foot_section input#no_source_of_fund').val('N/A').attr('readonly',true);
		
	}
	else
	{
		//$('div.ecumbrance_foot_section input[type="radio"]').attr('readonly',false);
		//$('div.ecumbrance_foot_section select').attr('readonly',false);
		$('div.ecumbrance_foot_section select#yes_how_many').attr('disabled',false);
		$('div.ecumbrance_foot_section select#yes_proceeds_subject_loan').attr('disabled',false);
		$('div.ecumbrance_foot_section select#payment_unpaid').attr('disabled',false);
		//$('div.ecumbrance_foot_section input[type="text"]').attr('readonly',false);
		$('div.ecumbrance_foot_section input#no_source_of_fund').val('').attr('readonly',false);
	}
}



$(document).ready(function(){
	
	if($('select#ecum_data_id_payment_lates').val() == 2)
	{
		//$('div.ecumbrance_foot_section input[type="text"]').val('');
		//$('div.ecumbrance_foot_section input[type="radio"]').attr('checked',false);
		//$('div.ecumbrance_foot_section input#ecum_data_id_payment_lates').attr('checked',true);
		// $('div.ecumbrance_foot_section select#yes_proceeds_subject_loan').attr('disabled',true);
		// $('div.ecumbrance_foot_section select#payment_unpaid').attr('disabled',true);
		// $('div.ecumbrance_foot_section input[type="radio"]').attr('readonly',true);
		// $('div.ecumbrance_foot_section input[type="text"]').attr('readonly',true);
		// //$('div.ecumbrance_foot_section input#ecum_data_id_payment_lates').attr('disabled',false);
		// $('div.ecumbrance_foot_section input#ecum_data_id_payment_latesy').attr('readonly',false);

		$('div.ecumbrance_foot_section select#yes_how_many').attr('disabled',true);
		$('div.ecumbrance_foot_section select#yes_proceeds_subject_loan').attr('disabled',true);
		$('div.ecumbrance_foot_section select#payment_unpaid').attr('disabled',true);
		$('div.ecumbrance_foot_section input#no_source_of_fund').val('N/A').attr('readonly',true);
		
	}
	else
	{
		// $('div.ecumbrance_foot_section input[type="radio"]').attr('disabled',false);
		//$('div.ecumbrance_foot_section select').attr('readonly',false);
		$('div.ecumbrance_foot_section select#yes_how_many').attr('disabled',false);
		$('div.ecumbrance_foot_section select#yes_proceeds_subject_loan').attr('disabled',false);
		$('div.ecumbrance_foot_section select#payment_unpaid').attr('disabled',false);
		// $('div.ecumbrance_foot_section input[type="text"]').attr('readonly',false);
		$('div.ecumbrance_foot_section input#no_source_of_fund').val('').attr('readonly',false);
	}
	
	
	if($('input#existing_lien').val() == 0)
	{
		
		$('div.ecumbrance_foot_section select#current_position').attr('disabled',true);
		$('div.ecumbrance_foot_section select#will_it_remain').attr('disabled',true);
		$('div.ecumbrance_foot_section input#est_payoff').attr('readonly',true);
		
		$('div.ecumbrance_foot_section select#ecum_data_id_payment_lates').attr('disabled',true);
		$('div.ecumbrance_foot_section select#yes_proceeds_subject_loan').attr('disabled',true);
		$('div.ecumbrance_foot_section select#payment_unpaid').attr('disabled',true);
		
		$('div.ecumbrance_foot_section select#yes_how_many').attr('disabled',true);
		$('div.ecumbrance_foot_section input#no_source_of_fund').attr('readonly',true);
		
	}else{
		
		$('div.ecumbrance_foot_section select#current_position').attr('disabled',false);
		$('div.ecumbrance_foot_section select#will_it_remain').attr('disabled',false);
		$('div.ecumbrance_foot_section input#est_payoff').attr('readonly',false);
		
		$('div.ecumbrance_foot_section select#ecum_data_id_payment_lates').attr('disabled',false);
		$('div.ecumbrance_foot_section select#yes_proceeds_subject_loan').attr('disabled',false);
		$('div.ecumbrance_foot_section select#payment_unpaid').attr('disabled',false);
		
		$('div.ecumbrance_foot_section select#yes_how_many').attr('disabled',false);
		$('div.ecumbrance_foot_section input#no_source_of_fund').attr('readonly',false);
		
	}
	
	
	
})

function lock_all_rows(that){
	
	if(that == '0'){
		
		$('div.ecumbrance_foot_section select#current_position').attr('disabled',true);
		$('div.ecumbrance_foot_section select#will_it_remain').attr('disabled',true);
		$('div.ecumbrance_foot_section input#est_payoff').attr('readonly',true);
		
		$('div.ecumbrance_foot_section select#ecum_data_id_payment_lates').attr('disabled',true);
		$('div.ecumbrance_foot_section select#yes_proceeds_subject_loan').attr('disabled',true);
		$('div.ecumbrance_foot_section select#payment_unpaid').attr('disabled',true);
		
		$('div.ecumbrance_foot_section select#yes_how_many').attr('disabled',true);
		$('div.ecumbrance_foot_section input#no_source_of_fund').attr('readonly',true);
		
	}else{
		
		$('div.ecumbrance_foot_section select#current_position').attr('disabled',false);
		$('div.ecumbrance_foot_section select#will_it_remain').attr('disabled',false);
		$('div.ecumbrance_foot_section input#est_payoff').attr('readonly',false);
		
		$('div.ecumbrance_foot_section select#ecum_data_id_payment_lates').attr('disabled',false);
		$('div.ecumbrance_foot_section select#yes_proceeds_subject_loan').attr('disabled',false);
		$('div.ecumbrance_foot_section select#payment_unpaid').attr('disabled',false);
		
		$('div.ecumbrance_foot_section select#yes_how_many').attr('disabled',false);
		$('div.ecumbrance_foot_section input#no_source_of_fund').attr('readonly',false);
		
	}
	
	
}

</script>	
<style>						
	body .row.property_loan_row.rowB {
		margin-left: 25px !important;
	}
	.lien_data h4{
		line-height: 27px !important;
		padding-left: 4px;
	}
	
	.form-control.bg_color {
		background-color: #f0f0f0 !important;
	}
</style>						