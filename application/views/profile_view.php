<?php 

$STATE_USA = $this->config->item('STATE_USA');

if(isset($fetch_user_detail) && is_array($fetch_user_detail)){
	foreach($fetch_user_detail as $row)
	{
			/*echo '<pre>';
			print_r($row);
			echo '</pre>';*/

		$fname = $row->fname;
		$lname = $row->lname;
		$email = $row->email;
		$middle_initial = $row->middle_name;
		$phone = $row->phone;
		$cell_phone = $row->cell_phone;
		$email_address = $row->email_address;
		$personal_address = $row->personal_address;
		$work_address = $row->work_address;
		$work_unit = $row->work_unit;
		$work_city = $row->work_city;
		$work_state = $row->work_state;
		$work_zip = $row->work_zip;
		$home_address = $row->home_address;
		$home_unit = $row->home_unit;
		$home_city = $row->home_city;
		$home_state = $row->home_state;
		$home_zip = $row->home_zip;
	}
}else{

		$fname = '';
		$lname = '';
		$email = '';
		$middle_initial = '';
		$phone = '';
		$cell_phone = '';
		$email_address = '';
		$personal_address = '';
		$work_address = '';
		$work_unit = '';
		$work_city = '';
		$work_state = '';
		$work_zip = '';
		$home_address = '';
		$home_unit = '';
		$home_city = '';
		$home_state = '';
		$home_zip = '';

}
?>
<style type="text/css">
	
	label{
		font-weight: 600 !important;
	}

	.addspaces{

		margin-top:10px;
	}

	h4 {
	    background-color: #e8e8e8;
	    padding: 10px;
	}
</style>
<!-- BEGIN CONTENT -->
	<div class="page-container">
			<!-- BEGIN PAGE HEAD -->
		
			<div class="container">
				<!-- BEGIN PAGE TITLE -->
	
			<div class="page-head">
				<div class="col-md-6 page-title" >
					<h1 style="margin-left: 15px;">User Profile</h1>
				</div>

				<div class="col-md-6">
					<h1></h1>
					<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#exampleModal">Close</button>
					<button type="button" onclick="update_pro()" class="btn btn-primary pull-right" style="margin-right: 10px;">Update</button>
				</div>
			</div>


			<div class="col-md-12">
				<!----------------Success and Error message----------------->
					<?php if($this->session->flashdata('success')!=''){  ?>
					 <div id='success'><i class='fa fa-thumbs-o-up'></i> <?php echo $this->session->flashdata('success');?></div><?php } ?>
					 <?php if($this->session->flashdata('error')!=''){  ?>
					 <div id='error'><i class='fa fa-thumbs-o-down'></i> <?php echo $this->session->flashdata('error');?></div><?php } ?>
				<!------------------------------------------------------>
			</div>
				 	
			<form method="POST" action="<?php echo base_url();?>form_profile_update" id="ban">

				<!----- model check --->
				<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-sm" role="document">
				    <div class="modal-content">
				      <!--<div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>-->
				      <div class="modal-body">
				        
				        <h5><b>Do you want to save the information?</b></h5>
				        	
				      </div>
				      <div class="modal-footer">
				        <a type="button" href="<?php echo base_url();?>" class="btn btn-primary">No</a>
				        <button type="button" onclick="update_pro()" class="btn btn-primary">Yes</button>
				      </div>
				    </div>
				  </div>
				</div>
				<!----- model check --->

				<div class="row addspaces">
					<div class="col-md-4">
						<label>First Name:</label>
						<input type="text" class="form-control" name="fname" value="<?php echo $fname; ?>" required>
					</div>

					<div class="col-md-4">
						<label>Middle Initial:</label>
						<input type="text" class="form-control" name="middle_initial" value="<?php echo $middle_initial; ?>"/>
					</div>

					<div class="col-md-4">
						<label>Last Name:</label>
						<input type="text" class="form-control" name="lname" value="<?php echo $lname; ?>" />
					</div>
				</div>

				<div class="row addspaces">
					<div class="col-md-4">
						<label>Office Phone:</label>
						<input type="text" class="form-control phone-format" name="phone" value="<?php echo $phone; ?>" />
					</div>

					<div class="col-md-4">
						<label>Cell Phone:</label>
						<input type="text" class="form-control phone-format" name="cell_phone" value="<?php echo $cell_phone; ?>" />
					</div>
				</div>

				<div class="row addspaces">
					<div class="col-md-4">
						<label>Work E-Mail:</label>
						<input type="email" class="form-control" name="email_address" value="<?php echo $email_address; ?>" required="required" readonly />
					</div>

					<div class="col-md-4">
						<label>Home E-Mail:</label>
						<input type="email" class="form-control" name="personal_address" value="<?php echo $personal_address; ?>" />
					</div>
				</div>

				<div class="row addspaces">
					<h4>Home Address</h4>
				</div>

				<div class="row addspaces">
					<div class="col-md-4">
						<label>Street Address:</label>
						<input type="text" class="form-control" name="home_address" value="<?php echo $home_address; ?>" />
					</div>

					<div class="col-md-4">
						<label>Unit #:</label>
						<input type="text" class="form-control" name="home_unit" value="<?php echo $home_unit; ?>" />
					</div>
				</div>

				<div class="row addspaces">
					<div class="col-md-4">
						<label>City:</label>
						<input type="text" class="form-control" name="home_city" value="<?php echo $home_city; ?>" />
					</div>

					<div class="col-md-4">
						<label>State:</label>
						<!--<input type="text" class="form-control" name="home_state" value="<?php echo $home_state; ?>" />-->
						<select class="form-control" name="home_state">
							<?php foreach($STATE_USA as $key => $data){ ?>
								<option value="<?php echo $key;?>" <?php if($key == $home_state){ echo 'selected';}?>><?php echo $data;?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-4">
						<label>Zip:</label>
						<input type="text" class="form-control" name="home_zip" value="<?php echo $home_zip; ?>" />
					</div>
				</div>

				<div class="row addspaces">
					<h4>Work Address</h4>
				</div>

				<div class="row addspaces">
					<div class="col-md-4">
						<label>Street Address:</label>
						<input type="text" class="form-control" name="work_address" value="<?php echo $work_address; ?>" />
					</div>

					<div class="col-md-4">
						<label>Unit #:</label>
						<input type="text" class="form-control" name="work_unit" value="<?php echo $work_unit; ?>" />
					</div>
				</div>

				<div class="row addspaces">
					<div class="col-md-4">
						<label>City:</label>
						<input type="text" class="form-control" name="work_city" value="<?php echo $work_city; ?>" />
					</div>

					<div class="col-md-4">
						<label>State:</label>
						<!--<input type="text" class="form-control" name="work_state" value="<?php echo $work_state; ?>" />-->
						<select class="form-control" name="work_state">
							<?php foreach($STATE_USA as $key => $data){ ?>
								<option value="<?php echo $key;?>" <?php if($key == $work_state){ echo 'selected';}?>><?php echo $data;?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-4">
						<label>Zip:</label>
						<input type="text" class="form-control" name="work_zip" value="<?php echo $work_zip; ?>" />
					</div>
				</div>

			</form>

			<div class="row addspaces"><br></div>
			<div class="row addspaces">
				<h4>Change Password</h4>
			</div>

			<div class="row addspaces">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#PasswordModal">Password Change</button>
			</div>

		</div>
	</div>

	<!---------------- Password Modal ------------------>
	<div class="modal fade" id="PasswordModal" tabindex="-1" role="dialog" aria-labelledby="PasswordModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	    <form method="post" action="<?php echo base_url();?>Home/Update_User_password">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel" style="margin-left: 15px;font-size: 20px;"><b>Password Change</b></h5>
	      </div>
	      <div class="modal-body">

	        <div class="row">

	        	<div class="col-md-12 addspaces">
	        		<label>New Password:</label>
	        		<input type="password" name="newpass" class="form-control" placeholder="New Password" minlength="8" required="required">
	        	</div>

	        	<div class="col-md-12 addspaces">
	        		<label>Confirm Password:</label>
	        		<input type="password" name="repass" class="form-control" placeholder="Retype Password">
	        	</div>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" name="submit" class="btn btn-primary">Save</button>
	      </div>
	    </form>
	    </div>
	  </div>
	</div>
	<!--------------- Password Modal ------------------->

<script>

	function update_pro(){

		$('form#ban').submit();

	}

	$(document).ready(function(){
	  /***phone number format***/
	  $(".phone-format").keypress(function (e) {
	    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	      return false;
	    }
	    var curchr = this.value.length;
	    var curval = $(this).val();
	    if (curchr == 3 && curval.indexOf("(") <= -1) {
	      $(this).val("(" + curval + ")" + "-");
	    } else if (curchr == 4 && curval.indexOf("(") > -1) {
	      $(this).val(curval + ")-");
	    } else if (curchr == 5 && curval.indexOf(")") > -1) {
	      $(this).val(curval + "-");
	    } else if (curchr == 9) {
	      $(this).val(curval + "-");
	      $(this).attr('maxlength', '14');
	    }
	  });
	});
</script>

