<div class="page-container contactlist-outmost outmost_ajax_table">
	<!-- BEGIN PAGE HEAD -->
	<div class="container">
		<div class="tab-pane">
			<!--------------------MESSEGE SHOW-------------------------->
			<?php if($this->session->flashdata('error')!=''){ ?>
			<div id='error'><i class='fa fa-thumbs-o-down'></i>
				<?php echo $this->session->flashdata('error');?>
			</div>
			<?php } ?>
			<?php if($this->session->flashdata('success')!=''){ ?>
			<div id='success'><i class='fa fa-thumbs-o-up'></i> 
				<?php echo $this->session->flashdata('success');?>
			</div>
			<?php } ?>
			<div id="chkbox_err"></div>
			<!-------------------END OF MESSEGE-------------------------->
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>Students <small></small></h1>
				</div>
				
				<div id="messagesChange"></div>
				<!-- END PAGE HEAD -->
				<div class="page-container">
					<div class="contactlist-res-div">
						<!-- BEGIN PAGE BREADCRUMB -->
						<ul class="page-breadcrumb breadcrumb hide">
							<li>	<a href="#">Home</a><i class="fa fa-circle"></i>
							</li>
							<li class="active">Dashboard</li>
						</ul>
						<!-- END PAGE BREADCRUMB -->
						<input type="hidden" name="borower_contact_type_id" value="">
						<input type="hidden" name="lender_contact_type_id" value="">
						<input type="hidden" name="contact_id" value="">


						<div class="pro_Head">
							<div class="row">									
								<form id="contact_search" action="<?php echo base_url();?>studentlist" method="post">
									<div class="col-sm-3">
										<label><span class="search_title">Search By:</span></label>
										<select name="search_by" onchange="click_this()" class="form-control">
										
											<option value=""></option>
									</select>
									</div>

									<div class="col-sm-3">
										<label>&nbsp;</label>
										<select name="search" onchange="submit_this(this.value)" class="chosen">
									<option value=''>Search Contact</option>
									
									<option value="">
									</option>
								</select>
									</div>
									<input style="display:none;" type="submit" value="Go">
									</form>
								<div class="col-sm-3">
									<input type="hidden" name="internal_contact_ajax" id="internal_contact_ajax" value="">
									&nbsp;
								</div>							
								<div class="col-sm-3">
									<label class="search_titled">Company Name:</label>
									<select name="company_name_ajax" id="company_name_ajax" class="form-control chosen">
										<option value="">Select All</option>
										
											<option value=""></option>
									</select>
								</div>
								
								
							</div>										
							
							
						
						</div>	

						<div class="contactlist_left filter_Upp" id="contactlist_div">
							<div class="row">							
							<div class="col-md-12">
								<div id="b_line" align="left" class="col-Fil">


									<div class="row">
										<div class="col-sm-4">
											<div class="main_select_div">					
												<div class="main_select_div-btn-1" >
													<a href="" class="btn btn-primary">Add Contact</a>
												</div>
												<div class="search_contact">
													<a href="" id="download_contactlist_csv" class="btn btn-success">Excel</a>
													&nbsp;&nbsp;
													<a href="" id="download_contactlist_pdf" class="btn btn-danger">PDF</a>

												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="row">
												<div class="col-sm-12" class="filter_tag_main_title" style="display: none;"><h4>Showing results for</h4></div>
												<div class="col-sm-3 filter_tag_contact_specialty filter_tag_contact_list" style="display: none;">
														<div class="ul_Outer">
														<label>Contact Specialty</label>
														<ul></ul>
													</div>
												</div>

												<div class="col-sm-3 filter_tag_marketing_blast filter_tag_contact_list" style="display: none;">
													<div  class="ul_Outer">
														<label>Marketing Blast</label>
														<ul></ul>
													</div>
												</div>

												<div class="col-sm-3 filter_tag_lender_data filter_tag_contact_list" style="display: none;">
													<div class="ul_Outer">
														<label>Lender Data</label>
														<ul></ul>
													</div>
												</div>

												<div class="col-sm-3 filter_tag_borrower_data filter_tag_contact_list" style="display: none;">
													<div class="ul_Outer">
														<label>Borrower Data</label>
														<ul></ul>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-2">
											<a href="#" class="ct_filter_contacts" alt="Fiter options">
												<i class="fa fa-filter icons_active_filter" aria-hidden="true"></i>
												<i class="fa fa-times icons_hide_filter" aria-hidden="true"></i>
											</a>
											<a href="#" class="ct_reset_filter_contacts" alt="Reset Fiter options">
												<i class="fa fa-refresh" aria-hidden="true"></i>
											</a>
										</div>
									</div>
								</div>
							</div>
							</div>
							<div class="col-md-12">
								<div class="content_filter_contacts" style="display: none;">
									<?php //echo $content_filter; ?>
								</div>
							</div>
							<div class="table-upper-section ajax_table_talimar table_All">
								<table id="ajax_studentlist" class="table-responsive table table-bordered table-striped table-condensed flip-content dataTable no-footer">
									<thead>
										<tr>
											<th>ID</th>
											<th>Name</th>
											<th>Age</th>
											<th>Mobile No</th>
											<th width="15%">Action</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript" src="<?php echo base_url("assets/js/student/studentlist.js"); ?>"></script>
