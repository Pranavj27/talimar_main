<?php
error_reporting(0);

?>

<?php
$this->load->view('template_files/header');

$menu_payment_reminderM = '';
$menu_borrower_reminderM = '';
$menu_payment_reminder = '';
$menu_borrower_reminder = '';
$menu_payment_reminder_track = '';
$menu_borrower_reminder_track = '';
$talimat_loan_select = '';
$menu_maturity_remenderM='';
$menu_maturity_remender='';
$menu_maturity_remender_track = '';
$menu_Insurance_remenderM='';
$menu_Insurance_remender='';
$menu_Insurance_remender_track = '';

$menu_Due_NotificationsM='';
$menu_Due_Notifications_remender='';
$menu_Due_Notifications_track = '';
if($this->uri->segment(3)){
	$talimat_loan_select = $this->uri->segment(3);
}
$url_title = $this->uri->segment(2);
if($url_title == 'loan_payment_reminder'){
	$menu_payment_reminderM = 'open_tab';
	$menu_payment_reminder = 'open';
}
if($url_title == 'borrower_auto_mails'){
	$menu_borrower_reminderM = 'open_tab';
	$menu_borrower_reminder = 'open';
}


if($url_title == 'loan_reminder_track'){
	$menu_payment_reminderM = 'open_tab';
	$menu_payment_reminder_track = 'open';
}
if($url_title == 'borrower_reminder_track'){
	$menu_borrower_reminderM = 'open_tab';
	$menu_borrower_reminder_track = 'open';
}



if($url_title == 'maturity_mail_remender'){
	$menu_maturity_remenderM='open_tab';
	$menu_maturity_remender='open';
}
if($url_title == 'maturity_mail_track'){
	$menu_maturity_remenderM='open_tab';
	$menu_maturity_remender_track='open';
}

if($url_title == 'insurance_expiring_upcoming'){
	$menu_Insurance_remenderM='open_tab';
	$menu_Insurance_remender='open';
}
if($url_title == 'insurance_expiring_track'){
	$menu_Insurance_remenderM='open_tab';
	$menu_Insurance_remender_track='open';
}

if($url_title == 'insurance_expiring_upcoming'){
	$menu_Due_NotificationsM='open_tab';
	$menu_Due_Notifications_remender='open';
}
if($url_title == 'insurance_expiring_track'){
	$menu_Due_NotificationsM='open_tab';
	$menu_Due_Notifications_track='open';
}
?>

<div class="content" id="template-reports">
	<div class="page-container">

		<div class="page-sidebar-wrapper">			
			<div class="page-sidebar navbar-collapse">
				
				<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="display:block;">

					<li class="<?php echo $menu_payment_reminderM; ?>">
						<a href="javascript:;">
							<span class="title"><strong>Payment Reminder </strong></span>
							<span class="arrow "></span>
						</a>
						<ul class="sub-menu">
							<li class="<?php echo $menu_payment_reminder; ?>">
								<a href="<?php echo base_url('auto_mail/loan_payment_reminder');?>">
								<span class="title"><strong>Upcoming Emails </strong></span>
								</a>
							</li>

							<li class="<?php echo $menu_payment_reminder_track; ?>">
								<a href="<?php echo base_url('auto_mail/loan_reminder_track');?>">
								<span class="title"><strong>Sent Emails </strong></span>
								</a>
							</li>
						</ul>
					</li>


					<li class="<?php echo $menu_borrower_reminderM; ?>">
						<a href="javascript:;">
							<span class="title"><strong>Construction Reminder </strong></span>
							<span class="arrow "></span>
						</a>
						<ul class="sub-menu">
							<li class="<?php echo $menu_borrower_reminder; ?>">
								<a href="<?php echo base_url('auto_mail/borrower_auto_mails');?>">
									<span class="title"><strong>Upcoming Emails </strong></span>
								</a>
							</li>

							<li  class="<?php echo $menu_borrower_reminder_track; ?>">
								<a href="<?php echo base_url('auto_mail/borrower_reminder_track');?>">
								<span class="title"><strong>Sent Emails </strong></span>
								</a>
							</li>
						</ul>
					</li>
					<li class="<?php echo $menu_maturity_remenderM; ?>">
						<a href="javascript:;">
							<span class="title"><strong>Maturity Notification </strong></span>
							<span class="arrow "></span>
						</a>
						<ul class="sub-menu">
							<li class="<?php echo $menu_maturity_remender; ?>">
								<a href="<?php echo base_url('auto_mail/maturity_mail_remender');?>">
									<span class="title"><strong>Upcoming Emails </strong></span>
								</a>
							</li>

							<li  class="<?php echo $menu_maturity_remender_track; ?>">
								<a href="<?php echo base_url('auto_mail/maturity_mail_track');?>">
								<span class="title"><strong>Sent Emails </strong></span>
								</a>
							</li>
						</ul>
					</li>

					<li class="<?php echo $menu_Insurance_remenderM; ?>">
						<a href="javascript:;">
							<span class="title"><strong>Insurance Expiring</strong></span>
							<span class="arrow "></span>
						</a>
						<ul class="sub-menu">
							<li class="<?php echo $menu_Insurance_remender; ?>">
								<a href="<?php echo base_url('auto_mail/insurance_expiring_upcoming');?>">
									<span class="title"><strong>Upcoming Emails </strong></span>
								</a>
							</li>
							<li  class="<?php echo $menu_Insurance_remender_track; ?>">
								<a href="<?php echo base_url('auto_mail/insurance_expiring_track');?>">
								<span class="title"><strong>Sent Emails </strong></span>
								</a>
							</li>
						</ul>
					</li>

					<li class="<?php echo $menu_Due_NotificationsM; ?>">
						<a href="javascript:;">
							<span class="title"><strong>Past Due Notifications</strong></span>
							<span class="arrow "></span>
						</a>
						<ul class="sub-menu">
							<li class="<?php echo $menu_Due_Notifications_remender; ?>">
								<a href="<?php echo base_url('auto_mail/past_due_remender');?>">
									<span class="title"><strong>Upcoming Emails </strong></span>
								</a>
							</li>
							<li  class="<?php echo $menu_Due_Notifications_track; ?>">
								<a href="<?php echo base_url('auto_mail/past_due_mails_track');?>">
								<span class="title"><strong>Sent Emails </strong></span>
								</a>
							</li>
						</ul>
					</li>

				</ul>
			</div>
		</div>

		<?php echo $content; ?>
	</div>
</div>


<input type="hidden" name="talimat_loan_select" id="talimat_loan_select" value="<?php echo $talimat_loan_select; ?>" >
<?php 
$this->load->view('template_files/footer');
?>
	<!-- END SIDEBAR -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/layout4/css/themes/light.css');?>"/>