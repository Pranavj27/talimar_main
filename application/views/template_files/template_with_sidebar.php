<?php
$this->load->view('template_files/header');


?>

<div class="content">
<div class="page-container">
<?php  $this->load->view('template_files/sidebar'); ?>
<?php echo $content; ?>
</div>
<?php 
$this->load->view('template_files/footer');
?>