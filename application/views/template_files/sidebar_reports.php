<?php
error_reporting(0);
$STATE_USA = $this->config->item('STATE_USA');
$yes_no_option = $this->config->item('yes_no_option');
$all_countary = $this->config->item('all_countary');
$loan_type_option = $this->config->item('loan_type_option');
$dead_reason_option = $this->config->item('dead_reason_option');
$loan_status_option = $this->config->item('loan_status_option');
$serviceing_condition_option = $this->config->item('serviceing_condition_option');
$serviceing_reason_option = $this->config->item('serviceing_reason_option');
$serviceing_sub_agent_option = $this->config->item('serviceing_sub_agent_option');
$serviceing_who_pay_servicing_option = $this->config->item('serviceing_who_pay_servicing_option');
$serviceing_agent_option = $this->config->item('serviceing_agent_option');
?>

<div class="page-sidebar-wrapper">
		
		<div class="page-sidebar navbar-collapse">
			
			<?php
				$uri_first = $this->uri->segment(1);
			?>
			<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="display:block;">


				<li>
					<a href="javascript:;">
					<span class="title"><strong>Common Reports </strong></span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">

						<?php
							if ($uri_first == 'reports') {
								echo "<li class='active'>";
							} else {
								echo "<li>";
							}
							?>
						
						</li>

						<?php
							if ($uri_first == 'existing_loan_schdule') {
								echo "<li class='active'>";
							} else {
								echo "<li>";
							}
							?>
							<!--<a href="<?php //echo base_url();?>loan_property">-->
							<a href="<?php echo base_url(); ?>existing_loan_schedule" >
							<span class="title"> Loan Schedule</span>

							</a>

						</li>

						<?php
							if ($uri_first == 'loan_boarding_schedule') {
								echo "<li class='active'>";
							} else {
								echo "<li>";
							}
							?>
							<a href="<?php echo base_url(); ?>loan_boarding_schedule" >
							<span class="title">Loan Holding Report</span>

							</a>
						</li>
						<?php
							if ($uri_first == 'loan_default_schedule') {
								echo "<li class='active'>";
							} else {
								echo "<li>";
							}
							?>
							<a href="<?php echo base_url(); ?>loan_default_schedule" >
							<span class="title">Loan Default Schedule</span>

							</a>
						</li>
						<?php
							if ($uri_first == 'loan_funded_year') {
								echo "<li class='active'>";
							} else {
								echo "<li>";
							}
							?>
							<a href="<?php echo base_url(); ?>loan_funded_year" >
							<span class="title">Funded to Date Schedule</span>

							</a>
						</li>
						<li>
							<a href="<?php echo base_url();?>reports/MorningReport">
							<span class="title">Morning Report</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>reports/outstadingServicerDeposit">
								<span class="title"> Late Payment Schedule</span>
							</a>
						</li>	
						<li>
							<a href="<?php echo base_url(); ?>reports/paymentCompare">
								<span class="title">Loan Balance Comparison</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>reports/outstanding_servicer_dposit">
								<span class="title">Outstanding Servicer Deposit</span>
							</a>
						</li>	
						<?php
							if ($uri_first == 'monthly_servicing_income') {
								echo "<li class='active'>";
							} else {
								echo "<li>";
							}
							?>
							<a href="<?php echo base_url(); ?>monthly_servicing_income" >
							<span class="title">Loan Servicing Income</span>

							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>auto_reminder">
							<span class="title">Auto Reminder Report</span>
							</a>
						</li>	
						<li>
							<a href="<?php echo base_url(); ?>contact_schedule_by_borrower">Contact Schedule by Borrower </a>
						</li>
						
						<li>
							<a href="<?php echo base_url();?>Reports/PaymentReserveReport">
							<span class="title">Payment Reserve Report</span>
							</a>
						</li>
						<?php
							if ($uri_first == 'construction_loan_reports') {
								echo "<li class='active'>";
							} else {
								echo "<li>";
							}
							?>
							<a href="<?php echo base_url(); ?>construction_loan_reports" >
							<span class="title">Construction Loan Status</span>

							</a>
						</li>	
					</ul>
				<li>

				<li>
					<a href="javascript:;">
					<span class="title"><strong>Borrower Reports</strong></span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="<?php echo base_url(); ?>affiliated_trancation">Affiliated Transactions </a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>ReportData/LineofCredit">Preferred Borrower Accounts</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>ReportData/Newborroweraccounts">
								<span class="title">Borrower Accounts by Date</span>
							</a>
						</li>
						<?php
							if ($uri_first == 'loan_schedule_borrower') {
								echo "<li class='active'>";
							} else {
								echo "<li>";
							}
							?>
							<a href="<?php echo base_url(); ?>loan_schedule_borrower" >
							<span class="title">Borrower Loan Schedule</span>

							</a>
						</li>
					</ul>
				</li>

				<li>
					<a href="javascript:;">
					<span class="title"><strong>Lender Reports</strong></span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="<?php echo base_url(); ?>ReportData/lenderFilter">
								<span class="title">Lender Contact Schedule</span>
							</a>
						</li>
						<?php
							if ($uri_first == 'trust_deed_schedule') {
								echo "<li class='active'>";
							} else {
								echo "<li>";
							}
							?>
							<a href="<?php echo base_url(); ?>trust_deed_schedule" >
							<span class="title">Available Trust Deed Schedule</span>

							</a>
						</li>
						

						<?php
							if ($uri_first == 'self_directed_IRA_lenders') {
								echo "<li class='active'>";
							} else {
								echo "<li>";
							}
							?>
							<a href="<?php echo base_url(); ?>self_directed_IRA_lenders" >
							<span class="title">Self Directed IRA Lenders</span>

							</a>

						</li>

						<?php
							if ($uri_first == 're870_audit_report') {
								echo "<li class='active'>";
							} else {
								echo "<li>";
							}
							?>
							<a href="<?php echo base_url(); ?>re870_audit_report" >
							<span class="title">RE870 Audit Report</span>
							</a>
						</li>

						<?php
							if ($uri_first == 'loan_schedule_investor') {
								echo "<li class='active'>";
							} else {
								echo "<li>";
							}
							?>
							<a href="<?php echo base_url(); ?>loan_schedule_investor" >
							<span class="title">Loan Schedule by Contact</span>
							</a>
						</li>


						<li>
							<a href="<?php echo base_url(); ?>lender_schedule_contact">
								<span class="title">Lender Account Schedule</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url(); ?>recent_payoffs">
								<span class="title">Recent Lender Payoffs</span>
							</a>
						</li>
						
						
						<li>
							<a href="<?php echo base_url(); ?>lender_loan_schedule">
								<span class="title">Lender / Loan Schedule</span>
							</a>
						</li>
						
						<li>
							<a href="<?php echo base_url(); ?>ReportData/LenderAccountType">
								<span class="title">Lender Account Type</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url(); ?>ReportData/Newlenderaccounts">
								<span class="title">Lender Accounts by Date</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>ReportData/LenderMarketingReport">
								<span class="title">Lender Marketing Report</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>Reports/loanByLenderaccount">
								<span class="title">Loans by Lender Account</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>re881_disclosure_data">
							<span class="title">RE881 Disclosure Data</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url();?>ReportData/lender_approvals">
							<span class="title">Active Lender Approvals</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url();?>Reports/investor_access_report">
							<span class="title">Investor Access Report</span>
							</a>
						</li>

					</ul>
				</li>

				<li>
					<a href="javascript:;">
					<span class="title"><strong>Loan Servicing Reports</strong></span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">	
						<?php
							if ($uri_first == 'paid_of_loan_schedule') {
								echo "<li class='active'>";
							} else {
								echo "<li>";
							}
							?>
							<a href="<?php echo base_url(); ?>paid_of_loan_schedule">
							<span class="title">Paid Off Loan Schedule</span>

							</a>

						</li>

						<li>
							<a href="<?php echo base_url(); ?>cancelled_loan_schedule">
							<span class="title">Cancelled Loan Schedule</span>
							</a>
						</li>
						<?php
							if ($uri_first == 'assigment_in_progress') {
								echo "<li class='active'>";
							} else {
								echo "<li>";
							}
							?>
							<a href="<?php echo base_url(); ?>assigment_in_progress" >
							<span class="title">Assignments in Process</span>

							</a>

						</li>
						<li>
							<a href="<?php echo base_url(); ?>property_insurance">
							<span class="title">Property Insurance Report</span>
							</a>
						</li>
						
						<li>
							<a href="<?php echo base_url(); ?>Deliquent_property_taxes">
							<span class="title">Deliquent Property Taxes</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>Outstanding_draws_report">
							<span class="title">Outstanding Draw Requests</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>property_loan_schedule">
							<span class="title">Multi-Property Loan Schedule</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>accrued_charges">
								<span class="title">Accrued Charges</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>loan_servicer">
							<span class="title">Loan Schedule by Servicer</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>upcoming_payoff_schedule">
							<span class="title">Upcoming Payoff Schedule</span>
							</a>
						</li>
						
						<li>
							<a href="<?php echo base_url();?>Reports/LoanClosingChecklist">
							<span class="title">Loan Closing Checklist</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url();?>reports/LoanExtensionReport">
							<span class="title">Loan Extension Report</span>
							</a>
						</li>
						
						<li>
							<a href="<?php echo base_url();?>reports/LoanContacts">
							<span class="title">Loan Contacts</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url();?>reports/PropertiesListed">
							<span class="title">Properties Listed</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url();?>reports/ServicerCheck">
							<span class="title">Servicer Report Check</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url();?>reports/ActiveOutBalance">
							<span class="title">Active vs. Outstanding Balance</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url();?>ReportData/AutoPaymentReport">
							<span class="title">Auto Payment Report</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>reports/loanBoardingReport" >
								<span class="title"> Loan Boarding Report </span>
							</a>
						</li>
						<?php
						if ($uri_first == 'maturity_report_schedule') {
							echo "<li class='active'>";
						} else {
							echo "<li>";
						}
						?>
							<a href="<?php echo base_url(); ?>maturity_report_schedule" >
								<span class="title">Maturity Report Schedule</span>
							</a>
						</li>				
					</ul>
				</li>
				
				<li>
					<a href="javascript:;">
					<span class="title"><strong>Marketing</strong></span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="<?php echo base_url(); ?>social_media">
							<span class="title">Marketing Report</span>
							</a>
						</li>					
						<li>
							<a href="<?php echo base_url(); ?>reports/BorrowerDisbursement">
								<span class="title">Client Schwag - Borrower</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>reports/LenderDisbursement">
								<span class="title">Client Schwag – Lender</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>reports/before_and_after">
								<span class="title">Before & After Videos</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>ReportData/pof_letter">
								<span class="title">POF Letter Status</span>
							</a>
						</li>
					</ul>
				</li>
				
				<li>
					<a href="javascript:;">
					<span class="title"><strong>Investor Reports</strong></span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">

						<li>
							<a href="<?php echo base_url(); ?>peer_street_schedule">
							<span class="title">Investor Loan Schedule</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>prior_year_comparison">
							<span class="title">Loan Portfolio Overview</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>investor_tape">
							<span class="title">Investor Tape</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>ReportData/fix_flip_portfolio">
							<span class="title">Fix & Flip Portfolio</span>
							</a>
						</li>
					</ul>

				</li>
				
				<?php if ($this->session->userdata('user_role') == 2) {?>
				<li>
					<a href="javascript:;">
					<span class="title"><strong>Admin Reports</strong></span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">

						<li>
							<a href="<?php echo base_url(); ?>employment_database_goal">
							<span class="title">User Stats</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url(); ?>Outstanding_lender_fees">
								<span class="title">Lender Fee Schedule</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url(); ?>daily_report">
								<span class="title">Daily Reports</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>loan_income_report">
								<span class="title">Loan Income Report</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>reports/YRClosingComp">
								<span class="title">YR Closing Comp</span>
							</a>
						</li>

					</ul>
				</li>
				

				<li>
					<a href="javascript:;">
					<span class="title"><strong>Temp Projects</strong></span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="<?php echo base_url(); ?>closing_verification">
								<span class="title">Closing Verification</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>lender_negative_spread">
								<span class="title">Lender Negative Spread</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>new_report">
								<span class="title">Lender Rate Confirmation</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>multi_lender_loan">
								<span class="title">Multi Lender Loans</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>ReportData/renovation_reserve">
								<span class="title">Renovation Reserve</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>ReportData/TDEmail">
								<span class="title">TD E-Mail Check</span>
							</a>
						</li>
						
					</ul>
				</li>
				<li>
					<a href="javascript:;">
					<span class="title"><strong>Test Reports</strong></span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="<?php echo base_url(); ?>test_report">
								<span class="title">Test Report</span>
							</a>
						</li>
						
					</ul>
				</li>

				<?php }?>
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
	<!-- END SIDEBAR -->

