<?php
error_reporting(0);

?>

<?php
$this->load->view('template_files/header');

$menu_payment_reminderM = '';
$menu_borrower_reminderM = '';
$menu_payment_reminder = '';
$menu_borrower_reminder = '';
$menu_payment_reminder_track = '';
$menu_borrower_reminder_track = '';
$talimat_loan_select = '';
$menu_maturity_remenderM='';
$menu_maturity_remender='';
$menu_maturity_remender_track = '';
$menu_Insurance_remenderM='';
$menu_Insurance_remender='';
$menu_Insurance_remender_track = '';

$menu_Due_NotificationsM='';
$menu_Due_Notifications_remender='';
$menu_Due_Notifications_track = '';
if($this->uri->segment(3)){
	$talimat_loan_select = $this->uri->segment(3);
}
$url_title = $this->uri->segment(2);
if($url_title == 'loan_payment_reminder'){
	$menu_payment_reminderM = 'open_tab';
	$menu_payment_reminder = 'open';
}
if($url_title == 'borrower_auto_mails'){
	$menu_borrower_reminderM = 'open_tab';
	$menu_borrower_reminder = 'open';
}


if($url_title == 'loan_reminder_track'){
	$menu_payment_reminderM = 'open_tab';
	$menu_payment_reminder_track = 'open';
}
if($url_title == 'borrower_reminder_track'){
	$menu_borrower_reminderM = 'open_tab';
	$menu_borrower_reminder_track = 'open';
}



if($url_title == 'maturity_mail_remender'){
	$menu_maturity_remenderM='open_tab';
	$menu_maturity_remender='open';
}
if($url_title == 'maturity_mail_track'){
	$menu_maturity_remenderM='open_tab';
	$menu_maturity_remender_track='open';
}

if($url_title == 'insurance_expiring_upcoming'){
	$menu_Insurance_remenderM='open_tab';
	$menu_Insurance_remender='open';
}
if($url_title == 'insurance_expiring_track'){
	$menu_Insurance_remenderM='open_tab';
	$menu_Insurance_remender_track='open';
}

if($url_title == 'insurance_expiring_upcoming'){
	$menu_Due_NotificationsM='open_tab';
	$menu_Due_Notifications_remender='open';
}
if($url_title == 'insurance_expiring_track'){
	$menu_Due_NotificationsM='open_tab';
	$menu_Due_Notifications_track='open';
}
?>

<div class="content" id="template-reports">
	<div class="page-container">
		<?php echo $content; ?>
	</div>
</div>


<input type="hidden" name="talimat_loan_select" id="talimat_loan_select" value="<?php echo $talimat_loan_select; ?>" >
<?php 
$this->load->view('template_files/footer');
?>
	<!-- END SIDEBAR -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/admin/layout4/css/themes/light.css');?>"/>
<style type="text/css">
@media (min-width: 992px){
body.page-sidebar-reversed .page-content-wrapper .page-content {
    margin-left: 0 !important;
    margin-right: 0px !important;
    padding-left: 30px;
    padding-right: 30px;
}
}
</style>