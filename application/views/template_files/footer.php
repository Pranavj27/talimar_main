<?php
$no_yes_option = $this->config->item('no_yes_option');

?>
<div class="page-footer">
<div class="container">
	<div class="page-footer-inner">
		  2017 - <?php echo date('Y');?> © Talimar Financial.
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
	<div class="ssl_lcass">
		<span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=GVjhF8HhCMdLO7qbo49S7I92CaI0PMdFEq1eBR1UmwpaHMytdC2kTMTGJ9kM"></script></span>
	</div>
	
</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->

<!-----------MY JS-------------------------------------------------->
<script src="<?php echo base_url("assets/js/closing_statement.js"); ?>" type="text/javascript"></script>



<script src="<?php echo base_url("assets/global/plugins/jquery.min.js"); ?>" type="text/javascript"></script>

<script src="<?php echo base_url("assets/global/plugins/jquery-migrate.min.js"); ?>" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url("assets/global/plugins/jquery-ui/jquery-ui.min.js"); ?>" type="text/javascript"></script>
<script src="<?php echo base_url("assets/global/plugins/bootstrap/js/bootstrap.min.js"); ?>" type="text/javascript"></script>
<script src="<?php echo base_url("assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"); ?>" type="text/javascript"></script>
<script src="<?php echo base_url("assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"); ?>" type="text/javascript"></script>
<script src="<?php echo base_url("assets/global/plugins/uniform/jquery.uniform.min.js"); ?>" type="text/javascript"></script>
<script src="<?php echo base_url("assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"); ?>" type="text/javascript"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?php echo base_url("assets/global/plugins/jquery.sparkline.min.js"); ?>" type="text/javascript"></script>

<script src="<?php echo base_url("assets/global/scripts/metronic.js"); ?>" type="text/javascript"></script>
<script src="<?php echo base_url("assets/admin/layout4/scripts/layout.js"); ?>" type="text/javascript"></script>
<script src="<?php echo base_url("assets/admin/layout4/scripts/demo.js"); ?>" type="text/javascript"></script>
<script src="<?php echo base_url("assets/admin/pages/scripts/index3.js"); ?>" type="text/javascript"></script>
<script src="<?php echo base_url("assets/admin/pages/scripts/tasks.js"); ?>" type="text/javascript"></script>
<!--<script type="text/javascript" src="<?php //echo base_url();?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script> -->


<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>-->


<script type="text/javascript" src="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/vishnu.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>


<script type="text/javascript" src="<?php echo base_url("assets/js/custom.js"); ?>"></script>
<link rel="stylesheet" href="<?php echo base_url("assets/datatable/jquery.dataTables.min.css"); ?>"/>
<link rel="stylesheet" href="<?php echo base_url("assets/datatable/responsive.dataTables.min.css"); ?>"/>



<?php 	
$affiliated_option						= $this->config->item('affiliated_option');
 $fractional                             = $this->config->item('fractional');
 $responsibility							= $this->config->item('responsibility');
 ?>


<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {  
$( ".datepicker" ).datepicker({ dateFormat: 'mm-dd-yy' });
$( ".datepicker123" ).datepicker({ dateFormat: 'yy-mm-dd' });

	
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   Demo.init(); // init demo features 
    Index.init(); // init index page
 Tasks.initDashboardWidget(); // init tash dashboard widget  
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

	 
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>

<script>

 $('.chosen_21').chosen();

		$(window).load(function() {
		    $("#loading").fadeOut("slow");
		});
   

 function add_aff(affi){
	 
	var rows= $('#my_main1 #my_main').length;

 	rows++;

 	var html_add_aff = '';
 	html_add_aff += '<div id="my_main" class="del_'+rows+'">';
 	html_add_aff += '<div class="row">';
 	html_add_aff += '<input type="hidden" name="id[]" value="new">';
 	html_add_aff += '<input type="hidden" name="aff_i[]" value="">';
 	html_add_aff += '<div class="col-md-3">';
 	html_add_aff += '<select name="name[]" class="chosen_88"  id="aff_ac_'+rows+'" onchange="fetch_contact_details_append(this);" style="display:none;">';
 	html_add_aff += '<option>Select One</option>';
 	<?php foreach($fetch_all_contact as $rows) { ?>
 		html_add_aff += '<option value="<?php echo $rows->contact_id;?>"><?php echo str_replace("'","",$rows->contact_firstname).' '.$rows->contact_middlename.' '.str_replace("'","",$rows->contact_lastname);?></option>';
 	<?php } ?>
 	html_add_aff += '</select>';
 	html_add_aff += '</div>';
 	html_add_aff += '<div class="col-md-2">';
 	html_add_aff += '<select name="affiliation[]" class="form-control" id="select_affiliation" onchange="affiliation_value(this);">';
 		<?php foreach($affiliated_option as $key => $row){ ?>
 			html_add_aff += '<option value="<?php echo $key; ?>"><?php echo $row; ?></option>';
 		<?php } ?>
 		html_add_aff += '</select>';
 	html_add_aff += '</div>';
 	html_add_aff += '<div class="col-md-2">';
 	html_add_aff += '<select class="form-control" name="dis_loan_term[]">';
 		<?php foreach($no_yes_option as $key => $row){?>
 			html_add_aff += '<option value="<?php echo $key;?>"><?php echo $row;?></option>';
 		<?php } ?>
 		html_add_aff += '</select>';
 	html_add_aff += '</div>';
 	html_add_aff += '<div class="col-md-2">';
 	html_add_aff += '<input type="text" name="phone[]" id="phonee" class="form-control phone-format" value="" readonly >';
 	html_add_aff += '</div>';
 	html_add_aff += '<div class="col-md-2">';
 	html_add_aff += '<input type="email" name="email[]" id="emaill" class="form-control" value="" readonly ></div>';
 	html_add_aff += '<div class="col-md-1">';
 	html_add_aff += '<a title="Delete" class="btn btn-danger btn-xs" onclick="removeaffrow('+rows+');">';
 	html_add_aff += '<i class="fa fa-trash"></i>';
 	html_add_aff += '</a>';
 	html_add_aff += '</div>';
 	html_add_aff += '</div>';
 	html_add_aff += '<br>';
 	html_add_aff += '</div>';

	  $("#my_main1 #my_main select.chosen_88").chosen( $('#my_main1').append(html_add_aff));

	$.ajax({
			
			type	:'POST',
			url		:'<?php echo base_url()."Load_data/ajax_affilation_append";?>',
			data	:{},
			success : function(response){

			$('#aff_ac_'+rows+'').html('');
			$('#aff_ac_'+rows+'').html(response);
			 $('#aff_ac_'+rows+'').chosen();

             // var data = JSON.parse(response);

			 
				
			}
				
		});


	}

	function removeaffrow(rowid){

		$('#my_main1 .del_'+rowid).remove();
	}
		
   	function add_gurrantor(that){
	 

	var rows= $('#my_main2 #my_main3').length;

 	rows++;

	  $("#my_main2 #my_main3 select.chosen_3").chosen( $('#my_main2').append('<div id="my_main3" class="dell_'+rows+'"><input type="hidden" name="id[]" value="new"><div class="row"><div class="col-md-4"><label><a title="Delete" href ="javascript:void(0);" id='+rows+' onclick="second_delete_payment_gra_data(this);"><i class="fa fa-trash"></i></a> Name:</label><select name="name[]" id="add_gur'+rows+'" class="chosen_3"  onchange="fetch_contact_payments_append(this);" style="display:none;"><option>Select One</option><?php foreach($fetch_all_contact as $rows) { ?><option value="<?php echo $rows->contact_id;?>"><?php echo str_replace("'","",$rows->contact_firstname).' '.$rows->contact_middlename.' '.str_replace("'","",$rows->contact_lastname);?></option><?php } ?></select></div><div class="col-md-4"><label>Phone:</label><input type="text" name="phone[]" id="cont_phonee" class="form-control phone-format" value="" readonly></div><div class="col-md-4"><label>E-mail:</label><input type="email" name="email[]" id="cont_emaill" class="form-control" value="" readonly></div></div><div class="row ">&nbsp;</div><div class="row "><div class="col-md-4"><label>Street Address:</label><input type="text" name="address[]" id="cont_addresss" class="form-control" value="" readonly></div><div class="col-md-4"><label>Unit #:</label><input type="text" name="unit[]" id="cont_unitt" class="form-control" value="" readonly></div></div><div class="row ">&nbsp;</div><div class="row "><div class="col-md-4"><label>City:</label><input type="text" name="city[]" id="cont_cityy" class="form-control" value="" readonly></div><div class="col-md-4"><label>State:</label><input type="text" name="state[]" id="cont_statee" class="form-control" value="" readonly></div><div class="col-md-4"><label>Zip:</label><input type="text" name="zip[]" id="cont_zipp" class="form-control" value="" readonly></div></div><div class="row ">&nbsp;</div><br></div>'));
		
		// setTimeout( function(){ 
		//      $("#my_main2 #my_main3 select.chosen_3").chosen();
		//   }  , 10 );

			$.ajax({
			
			type	:'POST',
			url		:'<?php echo base_url()."Load_data/ajax_payment_append";?>',
			data	:{},
			success : function(response){

			$('#add_gur'+rows+'').html('');
			$('#add_gur'+rows+'').html(response);
			 $('#add_gur'+rows+'').chosen();

             // var data = JSON.parse(response);

			 
				
			}
				
		});

		 }	

   		function add_row_table_wait_lists(){


			var count_tr = parseFloat($('table#table_wait_lists tbody tr').length);
			
			count_tr++;


			$.ajax({
			
			type	:'POST',
			url		:'<?php echo base_url()."Load_data/ajax_affilation_append";?>',
			data	:{},
			success : function(response){

			$('#contact_append_id_'+count_tr+'').html('');
			$('#contact_append_id_'+count_tr+'').html(response);
			 $('#contact_append_id_'+count_tr+'').chosen();

             // var data = JSON.parse(response);

			 
				
			}
				
		});
			var new_tr = '<tr>';

			new_tr += '<td>'+count_tr+'<input type="hidden" name="id[]" value="new"></td>';
			new_tr += '<td><select name="contact_name[]"  id="contact_append_id_'+count_tr+'" class="chosen_4" style="display:none;">';
   //          new_tr += '<option value="">Search Contact</option>';	
			// new_tr += '<?php foreach($fetch_all_contact as $rows){?>';
			// new_tr += '<option value=<?php echo $rows->contact_id;?>><?php echo $rows->contact_firstname.' '.$rows->contact_middlename.' '.str_replace("'","",$rows->contact_lastname);?></option>';

			// new_tr += '<?php }?>';

			new_tr += '</select></td>';
			new_tr += '<td><input type="text" name="date_request[]" class="form-control"></td>';
			new_tr += '<td><select name="confirmed[]" class="form-control input-md">';

			new_tr += '<?php foreach($responsibility as $key => $row) { ?>';

			new_tr += '<option value=<?php echo $key;?>><?php echo $row;?></option>';

			new_tr += '<?php } ?>';

			new_tr += '</select></td>';

        	new_tr += '<td><select name="fractional[]" class="form-control input-md">';

			new_tr += '<?php foreach($fractional as $key => $row) { ?>';

			new_tr += '<option value=<?php echo $key;?>><?php echo $row;?></option>';

			new_tr += '<?php } ?>';

			new_tr += '</select></td>';
			
			new_tr += '<td><input type="text" name="amount[]" class="form-control" onkeyup="number_only_format(this)" onchange="amount_format_change(this)"></td>';
			
			
			
			new_tr += '<td><a onclick="remove_table_wait_lists(this)"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>';
			
			new_tr += '</tr>';

			$(".chosen_4").chosen( $('table#table_wait_lists tbody').append(new_tr));
			$('table#table_wait_lists input[name="date_request[]"]').datepicker({ dateFormat: 'mm-dd-yy' });
		
			// setTimeout( function(){ 
			//      $(".chosen_4").chosen();
			//   }  , 10 );

		
		}


		
  
</script>


<!-- <script type="text/javascript">
		$(document).bind("contextmenu",function(e) {
		     e.preventDefault();
		});

		document.onkeydown = function(e) {
	        if (e.ctrlKey && (e.keyCode === 85 || e.keyCode === 117)) { 
	            return false;
	        } else if (event.keyCode == 123) {
		        return false;
		    } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {        
		        return false;
		    }else {
	            return true;
	        }
		};
</script> -->
