<html lang="en" class="no-js">
<head>
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<!--<![endif]-->
<!-- BEGIN HEAD -->
<meta charset="utf-8"/>
<?php
$url_title = $this->uri->segment(1);
if($url_title == '')
{
	$title = 'Dashboard -';
}
else if($url_title == 'load_data')
{
	$title = 'Loan Data -';
}
else if($url_title == 'borrower_data')
{
	$title = 'Borrower Data -';
}
else if($url_title == 'investor_data')
{
	$title = 'Lender Data -';
}
else if($url_title == 'users')
{
	$title = 'Users -';
}
else if($url_title == 'vendor_data')
{
	$title = 'Vendor Data -';
}
else if($url_title == 'loan_overview')
{
	$title = 'Loan Overview -';
}else if($url_title == 'projects')
{
	$title = 'Projects -';
}
else if($url_title == 'Mortgage Fund - ')
{
	$title = 'Mortgage Fund - ';
}
else if($url_title == 'reports' || $url_title == 'master_investor_schedule' || $url_title == 'master_investor_schedule' || $url_title == 'master_borrower_schedule' || $url_title == 'existing_loan_schdule' || $url_title == 'paid_of_loan_schedule' || $url_title == 'fci_service_loan' || $url_title == 'maturity_report_schedule' || $url_title == 'assigment_in_progress' || $url_title == 'loan_boarding_schedule' || $url_title == 'trust_deed_schedule' || $url_title == 'loan_default_schedule')
{
	$title = 'Reports -';
}
else
{
	$title = '';
}
?>
<title><?php echo $title; ?> TaliMar Financial </title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>

<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/admin/layout4/img/favicon1-32x32.png" sizes="32x32" />


<script src="<?php echo base_url()?>assets/extra_css_js/jquery-1.12.4.js"></script>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>

<!--<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>-->

<link href="<?php echo base_url("assets/global/plugins/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url("assets/global/plugins/simple-line-icons/simple-line-icons.min.css"); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url("assets/global/plugins/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url("assets/global/plugins/uniform/css/uniform.default.css"); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url("assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"); ?>" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->

<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="<?php echo base_url("assets/admin/pages/css/tasks.css"); ?>" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
<link href="<?php echo base_url("assets/global/css/components-rounded.css"); ?>" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url("assets/global/css/plugins.css"); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->

<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="<?php echo base_url();?>assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url("assets/admin/layout4/css/layout.css"); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url("assets/admin/layout4/css/themes/light.css"); ?>" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?php echo base_url("assets/admin/layout4/css/custom.css"); ?>" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
<link href="<?php echo base_url();?>assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="<?php echo base_url();?>assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
<link href="<?php echo base_url()."assets/css/database.css"; ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url()."assets/css/responsive.css"; ?>" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/talimar-bit-custom.css"); ?>"/>


<!-- <script src="http://code.jquery.com/jquery-1.11.2.min.js" type="text/javascript"></script> -->
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> -->
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


 <link rel="stylesheet" href="<?php echo base_url();?>assets/js/select_with_search/bootstrap-select.css">
<script src="<?php echo base_url();?>assets/js/select_with_search/bootstrap-select.js"></script> 

<style>

div#loading {
    padding-top: 20%;
    padding-left: 50%;
	position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    opacity: 0.9;
	background-color: rgba(7, 7, 7, 0.3);
}

.page-header .page-header-menu 
.hor-menu .navbar-nav > li > a{
	padding: 16px 16px 15px 18px!important;
}

/*10-07-2021*/
.d_task {
  /*background-color: #555;*/
  color: white;
  text-decoration: none;
  padding: 15px 26px;
  position: relative;
  display: inline-block;
  border-radius: 2px;
}

.d_task:hover {
  background: red;
}

.d_task .badge {
  
  position: absolute;
    top: 31px;
    right: 0px; 
    padding: 3px 8px;
    border-radius: 50%;
    background: red;
    color: white;
    font-weight: 600;
}
.page-header .page-header-menu .hor-menu .navbar-nav > li .dropdown-submenu > a:after{
	color: #ffffff!important;
	
}
.hor-menu .supporting_action a {
   display: inline-block;
   vertical-align: 
}
.hor-menu .supporting_action a:hover {
	background: #55616f;
}
.supporting_action_dashboard {
	margin-right: -20px
}
.hor-menu .supporting_action ul li a {
	display: block;
}
.hor-menu .supporting_action .dropdown-menu {
   min-width: 207px;
}
.page-header .page-header-menu .hor-menu .supporting_action .dropdown-toggle {
	padding: 25px 16px 22px 18px!important;
}
.hor-menu .supporting_action .dropdown-toggle .caret {
	margin-left: 0;
}
.supporting_action:hover .supporting_action_dashboard, .supporting_action:hover .dropdown-toggle,
.supporting_action:focus .dropdown-toggle {
   background: #55616f;
}  

</style>



</head>

<body class="page-header-fixed page-sidebar-closed-hide-logo page-sidebar-closed-hide-logo page-sidebar-reversed">


<div class="page-header">
	<!-- BEGIN HEADER TOP -->
	<div class="page-header-top">
		<div class="container">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="<?php echo base_url();?>"><img src="<?php echo base_url("assets/admin/layout4/img/logo-light.png"); ?>" alt="logo" class="logo-default"></a>
			</div>


			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a  class="menu-toggler" onclick="show_menu_section(this)" id="show"></a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown dropdown-user dropdown-dark">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<?php
						$where2['t_user_id'] = $this->session->userdata('t_user_id');
		
						$fetch_profile_images = $this->User_model->select_where('profile_image',$where2);
						
						if($fetch_profile_images->num_rows() > 0)
						{
							
							$fetch_profile_images1 = $fetch_profile_images->result();
							$image_name1 = $fetch_profile_images1[0]->image;
							
							
							?>
													
							<img alt="" class="" src="<?php echo base_url("assets/global/img/profile.png"); ?>"> 
							<?php
						}
						else
						{
							?>
							<img alt="" class="" src="<?php echo base_url("assets/global/img/profile.png"); ?>">
							<?php
						}
						?>
						
						<span class="username username-hide-mobile"><?php echo $this->session->userdata('user_name'); ?></span>
						</a>
						<ul class="dropdown-menu dropdown-menu-default"> 
							
							<?php
							if($this->session->userdata('user_role') == 2)
							{
							?>
							<li>
						
							<a href="<?php echo base_url().'users';?>"><i class="icon-user"></i> Admin Settings</a>
							</li>
							<?php } ?>
							<li>
								<a href="<?php echo base_url(); ?>edit_users/<?php echo $this->session->userdata('t_user_id'); ?>">
								<i class="icon-wrench"></i> User Settings</a>
							</li>
							<li>
								<a href="<?php echo base_url(); ?>logged_out">
								<i class="icon-key"></i> Log Out</a>
							</li>
							
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
	</div>
	<!-- END HEADER TOP -->
	<!-- BEGIN HEADER MENU -->

	<?php
	//loan originator tabs
	$loan_originator_check = $this->User_model->query("SELECT * FROM `user_settings` as us JOIN user as u ON u.id = us.user_id WHERE u.id = '".$this->session->userdata('t_user_id')."' AND u.role = '1'");
	if($loan_originator_check->num_rows() > 0){

		$display_menu_tabs = $loan_originator_check->result();
		$loan_organization = $display_menu_tabs[0]->loan_organization;
		$lender_portal_track = $display_menu_tabs[0]->lender_portal_track;

		if($loan_organization == 1){

			$display_menu_tabs = 2;

		}else{

			$display_menu_tabs = 1;
		}

		//lender_portal_track
		if($lender_portal_track == 1){

			$display_tracking_tabs = 2;

		}else{

			$display_tracking_tabs = 1;
		}

	}else{

		$display_menu_tabs = 1;
		$display_tracking_tabs = 1;
	}


	// echo $display_menu_tabs;

	?>
	<div class="page-header-menu">
		<div class="container">

			<div class="hor-menu ">
				<ul class="nav navbar-nav" >
					
					<?php
					$uri_segment_one = $this->uri->segment(1);
					if($uri_segment_one == '')
					{
						echo '<li class="dropdown supporting_action active">';
					}
					else
					{
						echo '<li class="dropdown supporting_action">';
					}
					?>
					

			            <a href="<?php echo base_url();?>" class="supporting_action_dashboard"> <span class="nav-label">Dashboard</span> </a>
			            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><span class="caret" role="button" aria-haspopup="true" aria-expanded="true"></span></a>
			            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
			            	<li><a href="<?php echo base_url();?>">Portfolio Dashboard</a></li>
			            	<?php 
			            	if($display_menu_tabs == 1){?>
				            	<li><a href="<?php echo base_url('Home/action_item');?>">Action Items</a></li>
				            	<li><a href="<?php echo base_url('Home/loan_servicing');?>">Loan Servicing</a></li>
				            	<li><a href="<?php echo base_url('Home/lender_servicing');?>">Lender Servicing</a></li>	
				            	<li><a href="<?php echo base_url('Home/portfolio_servicing');?>">Portfolio Servicing</a></li>  
				            	<li><a href="<?php echo base_url('Home/marketing');?>">Marketing</a></li>
				            	<li><a href="<?php echo base_url('Home/talimar_balance_sheet');?>">TaliMar Balance Sheet</a></li>
				            	<li><a href="<?php echo base_url('Home/dashboard');?>">Old Dashboard</a></li>
				            <?php
				        	}?>
			            	
			            	
			            </ul>
			        </li>
					
					
					<?php
					if($uri_segment_one == 'load_data')
					{
						echo '<li class="active">';
					}
					else
					{
						echo '<li>';
					}
					?>
					
						<a  href="<?php echo base_url()."load_data"?>" class="dropdown-toggle">
						Loan Portfolio </i>
						</a>
						
					</li>
					
					
					<?php
					if($uri_segment_one == 'borrower_data')
					{
						echo '<li class="active">';
					}
					else
					{
						echo '<li>';
					}
					?>

						<a href="<?php echo base_url();?>borrower_view">
						Borrower Accounts
						</a>
					</li>

				<?php if($display_menu_tabs == 1){?>

					<?php
					if($uri_segment_one == 'investor_data')
					{
						echo '<li class="active">';
					}
					else
					{
						echo '<li>';
					}
					?>
						<a  href="<?php echo base_url();?>investor_view">
						Lender Accounts <!--<i class="fa fa-angle-down">--></i>
						</a>
						
					</li>

				<?php } ?>

					
					<?php 
							$uri_segment_one = $this->uri->segment(1);
							if($uri_segment_one == 'contact'  ){
								$class_contact = 'class = "active"';
							}else{
								
								$class_contact = '';
							}
						?>
						<li <?php echo $class_contact;?>>
							<a href = "<?php echo base_url().'contactlist'?>" >Contacts</a>
						</li>
					<?php
					if($display_menu_tabs == 1){
							if($uri_segment_one == 'mortageFund' || $uri_segment_one == 'MortgageFound')  
							{
								$class_mortageFund = 'class = "dropdown supporting_action active"';
							}
							else
							{
								$class_mortageFund = 'class = "dropdown supporting_action"';
							}
						  if($this->session->userdata('mortgage_status') == 'yes' || $this->session->userdata('user_role') == 2){	
					?>	
					
						<li <?php echo $class_mortageFund;?>>
						 	<a href="<?php echo base_url('MortgageFound/portfolioOverview');?>" class="supporting_action_dashboard"> <span class="nav-label">Mortgage Fund</span> </a>
	            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><span class="caret" role="button" aria-haspopup="true" aria-expanded="true"></span></a>
	            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
	            	<li><a href="<?php echo base_url('MortgageFound/portfolioOverview');?>">Portfolio Overview</a></li>
	            	<li><a href="<?php echo base_url('MortgageFound/investorOverview');?>">Investor Overview</a></li>
	            	<li><a href="<?php echo base_url('mortgageFund');?>">Investor Schedule</a></li>
	            	<li><a href="<?php echo base_url('MortgageFound/loanPortfolio');?>">Loan Portfolio Schedule</a></li>
	            	<li><a href="<?php echo base_url('MortgageFound/creditLine');?>">Credit Line</a></li>
	            </ul>
						</li>
						
					<?php
					}
					else
					{ ?>
						<li <?php echo $class_mortageFund;?>><a href="javaScript:void(0)" class="supporting_action_dashboard"> <span class="nav-label">Mortgage Fund</span> </a></li>
					<?php	
					} 
				}
					// Changes on 01-09-2021 By @bitcot
					// Task New menu added start from here
					$class_tasks 	= '';
					if($uri_segment_one == 'tasks' ) {
						$class_tasks = 'class = "active"';
					}
					?>	
					<li <?php echo $class_tasks;?>>					
						<a href="<?php echo base_url().'task_view_list';?>" class="d_task">Tasks 
							<span class="badge" style="display: none;">!</span>
						</a>
					</li>	
					<!--  Task New menu added end from here -->
					
					<?php
					if($uri_segment_one == 'vendor_data' ) 
					{
						$class_vendor = 'class = "active"';
					}
					else
					{
						$class_vendor = '';
					}
					if($display_menu_tabs == 1){
					?>	
						<li <?php echo $class_vendor;?>>
						
						<a href="<?php echo base_url().'vendor-primary';?>">Vendors</a>
						</li>
					<?php
					}?>

				<?php if($display_menu_tabs == 1){?>
					
					<?php
					if($uri_segment_one == 'reports'  || $uri_segment_one == 'existing_loan_schdule' || $uri_segment_one == 'paid_of_loan_schedule' || $uri_segment_one == 'maturity_report_schedule' || $uri_segment_one == 'assigment_in_progress' || $uri_segment_one == 'loan_boarding_schedule' || $uri_segment_one == 'monthly_servicing_income' || $uri_segment_one == 'loan_schedule_borrower' || $uri_segment_one == 'loan_schedule_investor') 
					{
						echo '<li class="active">';
					}
					else
					{
						echo '<li>';
					}
					?>
					<a href="<?php echo base_url();?>reports" class="dropdown-toggle">Reports</a>
					
					</li>

				<?php } ?>
				
				<?php 
							$uri_projects = $this->uri->segment(1);
							if($uri_projects == 'projects'  ){
								$class_uri_projects = 'class = "active"';
							}else{
								
								$class_uri_projects = '';
							}
						?>
						<li <?php echo $class_uri_projects;?>>
							
							<a href = "<?php echo base_url().'projects'?>" >Projects</a>
						</li>


				<?php if($this->session->userdata('user_role') == 2){ ?>

					<?php
					if($uri_segment_one == 'fund_data')
					{
						echo '<li class="active">';
					}
					else
					{
						echo '<li>';
					}
					?>
						<a  href="<?php echo base_url();?>credit_line">
						Credit Line <!--<i class="fa fa-angle-down">--></i>
						</a>

						<a  href="<?php echo base_url();?>Student">
						Student <!--<i class="fa fa-angle-down">--></i>
						</a>
						
					</li>
				<?php } ?>

				<?php if($display_menu_tabs == 1 && ($this->session->userdata('user_role') == 2 || $display_tracking_tabs == 2)){?>

					<!-- <li>
						<a href="<?php //echo base_url();?>Users/portal_login_detail">Tracking</a>
					</li> -->

					<li class="dropdown">
			            <a href="<?php echo base_url();?>Users/portal_login_detail" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="nav-label">Tracking</span> <span class="caret"></span></a>
			            <ul class="dropdown-menu">
			            	<li><a href="<?php echo base_url();?>Users/portal_login_detail">Tracking</a></li>

			            	<!-- <li><a href="<?php// echo base_url('auto_mail/loan_payment_reminder');?>">Auto Emails</a></li> -->	
			            	<li><a href="<?php echo base_url();?>CronJobFCI/apiConnection">API Connection</a></li>
			            	<li><a href="<?php echo base_url();?>auto_mail/trackingNotification">Tracking Notification</a></li>
			            </ul>
			        </li>

				<?php } ?>

						
				</ul>
				
			</div>
			<!-- END MEGA MENU -->
		</div>
	</div>
	<!-- END HEADER MENU -->
</div>
<?php 
if(!empty($this->session->userdata('t_user_id'))) {
	$currentLogginUser=$this->session->userdata('t_user_id');
	$_duedate_counts=  task_due_count($currentLogginUser);
	if(empty($_duedate_counts) || $_duedate_counts==0){
		$_duedate_counts=0;
	}
}else{
	$_duedate_counts=0;
}
?>
<?php if($this->uri->segment(1)=='load_data'){
?>
<!--<div id="loading">
  <img id="loading-image" src="<?php echo base_url();?>/assets/talimar_gif/25.gif" width="100px" height="100px" alt="Loading..." /> 
</div>-->
<?php } ?>
<!-- <div id="loading">
	<div><i class="fa fa-4x fa-sun-o fa-spin text-danger"></i></div>
</div> -->

<link rel="stylesheet" href="<?php echo base_url()?>assets/extra_css_js/jquery-ui.css">
<script src="<?php echo base_url()?>assets/extra_css_js/jquery-ui.js"></script>
<script src="<?php echo base_url()?>assets/extra_css_js/jquery-1.12.4.js"></script>



<script>

var counts = '<?php echo $_duedate_counts;?>';
if(counts > 0 ){
	// $('ul.nav.navbar-nav li a.d_task').css('background-color', 'red');
	// $('ul.nav.navbar-nav li a.d_task').css('color', '#fff');	
	$('span.badge').show();
}

function show_menu_section(that)
{
	if(that.id == 'show')
	{
		that.id = 'display_block';
		$('.page-header-menu').css('display','block');
	}
	else
	{
		that.id = 'show';
		$('.page-header-menu').css('display','none');
	}
}

</script>

<!-------------------------------------SELECT WITH SEARCH------------>

<link rel="stylesheet" href="<?php echo base_url();?>assets/js/select_with_search/bootstrap-select.css">


<style>
	table{
		font-size:14px !important;
	}
</style>