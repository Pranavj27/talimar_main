<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<?php 
	
	if($this->session->userdata('user_role') == 1){

		$user_id['user_id'] = $this->session->userdata('t_user_id');
		$user_id['investor_relation'] = '1';

		$fetch_setting = $this->User_model->select_where('user_settings',$user_id);
		if($fetch_setting->num_rows() > 0){
			$lender_invertor_page = 1;
		}else{
			$lender_invertor_page = 2;
		}
	}else{

		$lender_invertor_page = 1;

	}

	$uri_segment_two = $this->uri->segment(2);
	$loan_portfolio_id = $this->uri->segment(2);	
	if($uri_segment_two == 'new'){
		$add_disabled_class = "disabled_li_new";
	}else{		
		$add_disabled_class = "";
	}

	//echo $lender_invertor_page;
			
		error_reporting(0);
		$servicer_charge_paid_from 				= $this->config->item('servicer_charge_paid_from');
		$servicer_charge_paid_to 				= $this->config->item('servicer_charge_paid_to');
		$servicer_charge_description 			= $this->config->item('servicer_charge_description');
		$ach_activated 							= $this->config->item('ach_activated');

		$vendor_types 							= $this->config->item('vendor_type');
		$l_s_c 									= $this->config->item('l_s_c');
		
		$STATE_USA 								= $this->config->item('STATE_USA'); 
		$usa_city_county 						= $this->config->item('usa_city_county'); 
		$yes_no_option 							= $this->config->item('yes_no_option');
		$garge_option 							= $this->config->item('garge_option');
		$ext_PaymentType 						= $this->config->item('ext_PaymentType');
		$position_option 						= $this->config->item('position_option');
		$all_countary 							= $this->config->item('all_countary');
		$loan_type_option 						= $this->config->item('loan_type_option');
		$dead_reason_option 					= $this->config->item('dead_reason_option');
		$loan_status_option 					= $this->config->item('loan_status_option');
		$closing_status_option 					= $this->config->item('closing_status_option');
		$boarding_status_option 				= $this->config->item('boarding_status_option');
		$serviceing_condition_option 			= $this->config->item('serviceing_condition_option');
		$loan_payment_status 					= $this->config->item('loan_payment_status');
		$serviceing_reason_option 				= $this->config->item('serviceing_reason_option');
		$serviceing_sub_agent_option 			= $this->config->item('serviceing_sub_agent_option');
		$serviceing_who_pay_servicing_option 	= $this->config->item('serviceing_who_pay_servicing_option');
		$how_setup_fees_paid 					= $this->config->item('how_setup_fees_paid');
		$serviceing_agent_option 				= $this->config->item('serviceing_agent_option');
		$broker_capacity_option 				= $this->config->item('broker_capacity_option');
		$account_type_option 					= $this->config->item('account_type_option');
		$ach_account_type_option 				= $this->config->item('ach_account_type_option');
		$fee_disbursement_list 					= $this->config->item('fee_disbursement_list');
		$paidoff_status_option 					= $this->config->item('paidoff_status_option');
		$construction_status_option 			= $this->config->item('construction_status_option');
		$will_option 							= $this->config->item('will_option');
		$yes_no_option2 						= $this->config->item('yes_no_option2');
		$yes_no_option3 						= $this->config->item('yes_no_option3');
		$yes_no_option4 						= $this->config->item('yes_no_option4');
		$naveda_county 							= $this->config->item('naveda_counties_options');
		$junior_senior_option					= $this->config->item('junior_senior_option');
		$encumbrances_loan_type					= $this->config->item('encumbrances_loan_type');
		$loan_doc_type							= $this->config->item('loan_doc_type');
		$com_type								= $this->config->item('com_type');
		
		$extension_yes_no						= $this->config->item('extension_yes_no');
		$extension_agreement					= $this->config->item('extension_agreement');
		$extension_loan_servicer				= $this->config->item('extension_loan_servicer');
		$extension_fee							= $this->config->item('extension_fee');
		$loan_servicing_checklist				= $this->config->item('loan_servicing_checklist');
		$underwriting_items_option				= $this->config->item('underwriting_items_option');
		$boarding_brocker_disbrusement			= $this->config->item('boarding_brocker_disbrusement');
		$affiliated_option						= $this->config->item('affiliated_option');
		$loan_affiliated_parties				= $this->config->item('loan_affiliated_parties');
		$reason_for_default						= $this->config->item('reason_for_default');
		$foreclosuer_status						= $this->config->item('foreclosuer_status');
		$foreclosuer_processor					= $this->config->item('foreclosuer_processor');
		$estimale_complete_option				= $this->config->item('estimale_complete_option');
		$loan_note_option						= $this->config->item('loan_note_option');
		$property_modal_property_type			= $this->config->item('property_modal_property_type');
		$valuation_type							= $this->config->item('valuation_type');
		$transaction_type_option				= $this->config->item('transaction_type_option');
		$property_typee							= $this->config->item('property_typee');
		//$loan_typee								= $this->config->item('loan_typee');
		$loan_typee								= $this->config->item('loan_type_option');
		$payment_remind							= $this->config->item('payment_remind');
		$closing_statement_item_loads			= $this->config->item('closing_statement_item_loads');
		$new_servicer_option					= $this->config->item('new_servicer_option');
		$newsub_servicer_option					= $this->config->item('newsub_servicer_option');
        $fractional                             = $this->config->item('fractional');
		$responsibility							= $this->config->item('responsibility');
        $primary_contact_option					= $this->config->item('primary_contact_option');
        $pay_off_processing_one					= $this->config->item('pay_off_processing_one');
        $new_option					            = $this->config->item('new_option');
        $extension_status					    = $this->config->item('extension_status');
        $loan_source					        = $this->config->item('loan_source');
        $not_applicable_option					= $this->config->item('not_applicable_option');
		
        $assi_security_type					    = $this->config->item('assi_security_type');
        $assi_disclouser_status					= $this->config->item('assi_disclouser_status');
        $assi_servicer_status					= $this->config->item('assi_servicer_status');
        $assi_fund_received						= $this->config->item('assi_fund_received');
        $assi_file_status						= $this->config->item('assi_file_status');
        $term_sheet_option						= $this->config->item('term_sheet');
        $term_sheet_closing_option				= $this->config->item('closing_term_sheet');
        $yes_no_option_verified					= $this->config->item('yes_no_option_verified');
        $market_trust_deed_new_option			= $this->config->item('market_trust_deed_new_option');
        $template_yes_no_option					= $this->config->item('template_yes_no_option');
        $marketing_before_after_option			= $this->config->item('marketing_before_after_option');
        $before_after_option_new				= $this->config->item('before_after_option_new');
        $no_yes_option							= $this->config->item('no_yes_option');
        $debit_account_type						= $this->config->item('debit_account_type');
        $payment_due_option						= $this->config->item('payment_due_option');
        $yes_no_option_67						= $this->config->item('yes_no_option_67');
        $foreclosuer_step						= $this->config->item('foreclosuer_step');
        $recording_no_yes_option				= $this->config->item('recording_no_yes_option');
        $Ass_status_paidto						= $this->config->item('Ass_status_paidto');
        $Ass_Recording_Status					= $this->config->item('Ass_Recording_Status');
        $who_pay_setup_fees						= $this->config->item('who_pay_setup_fees');
        $lender_app_status						= $this->config->item('lender_app_status');
        $lender_app_request						= $this->config->item('lender_app_request');
        $ext_lender_app							= $this->config->item('ext_lender_app');
        $ext_process_option						= $this->config->item('ext_process_option');
        $ext_borrower_approval					= $this->config->item('ext_borrower_approval');
        $ext_PaymentAmount_opt					= $this->config->item('ext_PaymentAmount_opt');
        $ext_PaymentCalculation					= $this->config->item('ext_PaymentCalculation');
        $loan_maturity_status					= $this->config->item('loan_maturity_status');
        $process_payoff_type_opt				= $this->config->item('process_payoff_type_opt');
        //21-07-2021 By @Aj optional_insurance_option
        $optional_insurance_option				= $this->config->item('optional_insurance_option');
       
		$template_yes_no_option_3 = array(
											2 => 'Not Complete',
											1 => 'Complete',
											//'' => 'Not Required',											
										);
		
		// vendor names for trustee drop-down options...
		foreach($all_vendor_data as $vendor){
			$vendor_name[$vendor->vendor_id] = $vendor->vendor_name;
		}

				$loan_id = $this->uri->segment(2);
	
	            $servicing_fees_percent = isset($loan_servicing_data) ? $loan_servicing_data[0]->servicing_fee : 0;
				
				$loan_amount = $loan_amountt ? $loan_amountt : 0;
				
				$servicing_fees_dollar = ($servicing_fees_percent/100)*$loan_amount;
				
				
				
				$intrest_r 			= $intrest_ratee ? $intrest_ratee : 0 ;
				
				$servicing_feees 	= $servicing_fees_percent;
				
				$lender_yield_percent = $intrest_r - $servicing_feees;
	
					// echo '<pre>';
					// print_r($fetch_all_contact);
					// echo '</pre>';
		?>
		
	
<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse">
		<!-- BEGIN SIDEBAR MENU -->
			
		<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="display:block;" id="loan-data-sidebar-right">
			<!--<li>
				<a class="" data-toggle="modal" href="#vendor_data">
				<span class="title">Vendor Data</span>
				</a>
			</li>
				
			<li>
				<a class="" data-toggle="modal" href="#loan_view">
				<span class="title">Loan Overview</span>
				</a>
			</li>
			-->
				
			<li>
				<a class="" data-toggle="modal" href="#loan_terms">
					<!-- <i class="fa fa-shield"></i> -->
					<span class="title">Loan Terms</span>
				</a>
			</li>
			<li class="<?php echo $add_disabled_class;?>">
				<a class="" data-toggle="modal" href="#loan_notes">
					<span class="title">Loan Notes</span>
				
				</a>
			</li>
			<li class="<?php echo $add_disabled_class;?>">
				<a class="" data-toggle="modal" href="#diligence_materials">
					<!-- <i class="fa fa-shield"></i> -->
					<span class="title">Due Diligence Items</span>
				</a>
			</li>
			<!-- New Section 
			Date-04-10-2021
			Description- As per task required
			Authur--Bitcot	 -->
			<li class="<?php echo $add_disabled_class;?>">
				<a href="#condition_page_section" class="" data-toggle="modal" href="#condition_page_section">
					<!-- <i class="fa fa-shield"></i> -->
					<span class="title">Conditions Page</span>
				</a>
			</li>
			<li class="<?php echo $add_disabled_class;?>">
				<a class="" data-toggle="modal" href="#loan_servicing">
					<!-- <i class="fa fa-shield"></i> -->
					<span class="title">Loan Servicing</span>
				</a>
			</li>
				
			<!--<li>
				<a class="" data-toggle="modal" href="#accured_charges">
					 
					<span class="title">Accrued Charges</span>
				</a>
			</li>-->
				
			<li class="<?php echo $add_disabled_class;?>">
				<!--<a href="<?php //echo base_url();?>loan_property">-->
				<a class="" data-toggle="modal" href="javascript:void(0)"  onclick = "add_property_data_rows('<?php echo $loan_id;?>')">
					<!--<i class="fa fa-cogs"></i>-->
					<span class="title">Property Data</span>
				</a>
			</li>
	 	 
	 	 <?php 	

		if($payment_gurranty =='1'){?>

		<li class="<?php echo $add_disabled_class;?>">
				<a class="" data-toggle="modal" id="vc" onclick="payment_data(this)" href="#payment_gurrantor">
					<!-- <i class="fa fa-info-circle"></i> -->
					<span class="title">Payment Guarantor</span>
				</a>
		
			</li>
		
		<?php }

		else{?>

			<li class="disabled_li">
				<a class="" data-toggle="modal" href="#">
					<!-- <i class="fa fa-info-circle"></i> -->
					<span class="title">Payment Guarantor</span>
				</a>
		
			</li>	
		<?php }

		?>


			<!--<li class="<?php echo $add_disabled_class;?>" onclick="submit_closing_data_once();">-->
			<li class="<?php echo $add_disabled_class;?>">
				<a class="" data-toggle="modal" href="#closing_statement_new11">
					<!-- <i class="fa fa-info-circle"></i> -->
					<span class="title">Closing Statement</span>
				</a>
				<!--
					<ul class="sub-menu">
						<li>
							<a href="#">
							General Components</a>
						</li>
					</ul>
					-->
			</li>
			<!--
			<li>
				<a class="" data-toggle="modal" href="#monthly_payment">
					<span class="title">Monthly Payment</span>
				
				</a>
			</li>
			-->
				
			<li class="<?php echo $add_disabled_class;?>">
				<a class="" data-toggle="modal" href="#impound_accounts">
					<span class="title">Monthly Payment</span>
				</a>
			</li>
			<li class="<?php echo $add_disabled_class;?>">
				<a class="" data-toggle="modal" onclick="escrow_data(this);" href="#escrow">
					<!-- <i class="icon-diamond"></i> -->
					<span class="title">Escrow</span>
				</a>
			</li>

			<li class="<?php echo $add_disabled_class;?>">
				<a class="" data-toggle="modal" href="#title" onclick="title_data(this);" >
					<span class="title">Title</span>
				</a>
			</li>
			<li class="<?php echo $add_disabled_class;?>">
				<a class="" data-toggle="modal" href="#construcion_type_sources_and_uses">
					<!-- <i class="fa fa-header"></i> -->
					<span class="title">Loan Distribution</span>
				</a>
			</li>
			<li class="<?php echo $add_disabled_class;?>">
				<a  class="" data-toggle="modal" href="#recording_information">
					<!-- <i class="fa fa-info"></i> -->
					<span class="title">Recording Information</span>
				</a>
			</li>

			
			<?php if($lender_invertor_page == 1){ ?>	
			<li class="<?php echo $add_disabled_class;?>">
				<a data-toggle="modal" href="#assigment_main">
					<span class="title">Capital Markets</span>
				</a>
			</li>

			<?php }else{ ?>

			<li class="disabled_reserve">
				<a data-toggle="modal" href="#" title="Please update your settings to access this page!">
					<span class="title">Capital Markets</span>
				</a>
			</li>
			<?php } ?>

			
			
			
			
				
			<!--<li class="draws_button_list">
				<a class="" data-toggle="modal" href="#ach_deposite">
					<span class="title">ACH Deposit</span>
				</a>
			</li>-->

			<?php if($fetch_loan_result[0]->draws == '' || $fetch_loan_result[0]->draws == 2){ ?>

			<li class="disabled_reserve">
				<a data-toggle="modal" href="#">
					<span class="title">Loan Reserve</span>
				</a>
			</li>


			<?php }else{ ?>

			<li>
				<a data-toggle="modal" href="#test">
					<span class="title">Loan Reserve</span>
				</a>
			</li>

			<!-- <li>
				<a data-toggle="modal" href="#loan_reserve">
					<span class="title">Loan Reserve 2</span>
				</a>
			</li> -->
			

			<?php } ?>

			<!--23-07-2021 By@Aj -->
			<li class="<?php echo $add_disabled_class;?>">
				<a data-toggle="modal" href="#wholesale_deal" >					
					<span class="title">Wholesale Deal</span>					
				</a>
			</li>
			<!-- End -->
			
	
			<li class="<?php echo $add_disabled_class;?>">
				<a data-toggle="modal" href="#print_loan_document" >
					<!-- <i class="fa fa-print"></i> -->
					<span class="title">Print Documents</span>
					
				</a>
			</li>

			<?php if($lender_invertor_page == 1){ ?>
			<li class="<?php echo $add_disabled_class;?>">
				<a data-toggle="modal" href="#new_modal_2" >
					<!-- <i class="fa fa-print"></i> -->
					<span class="title">Save Documents</span>
					
				</a>
			</li>
			<?php }else{ 

				$fetch_setting = $this->User_model->select_where('user_settings', array('user_id'=>$this->session->userdata('t_user_id')));
				if($fetch_setting->num_rows() > 0){

					$fetch_setting = $fetch_setting->result();
					if($fetch_setting[0]->loan_servicing == '1'){
						
						$disblled='';			
						$variabe='#new_modal_2';	
						
					}else{

						$disblled='disabled_reserve';			
						$variabe='#';			
					}
				
				}
			

				?>
			<li class="<?php echo $disblled;?>">
				<a data-toggle="modal" href="<?php echo $variabe;?>" title="You have no permission to access this page.">
					<!-- <i class="fa fa-print"></i> -->
					<span class="title">Save Documents</span>
					
				</a>
			</li>
			<?php } ?>

			<li class="<?php echo $add_disabled_class;?>">
				<a class="" data-toggle="modal" href="#FCI_Documents">
					<!--<i class="fa fa-list-alt"></i> -->
					<span class="title">Save Documents FCI</span>
				</a>
			</li>
			
			<li class="<?php echo $add_disabled_class;?>">
				<a class="" data-toggle="modal" href="#errorss">
					<!--<i class="fa fa-list-alt"></i> -->
					<span class="title">Error</span>
				</a>
			</li>

			<li class="<?php echo $add_disabled_class;?>">
				<a class="" data-toggle="modal" href="#mytestmodal">
					<!--<i class="fa fa-list-alt"></i> -->
					<span class="title">My Test Modal</span>
				</a>
			</li>
			
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>



<?php		
		include('loan_terms.php');
		include('loan_reserve.php');
		//include('fci_apidoc.php');
		//include('lender_approval.php');
		
		$loan_id = $this->uri->segment(2);
		if($loan_id)
		{
			$loan_id = "/".$this->uri->segment(2);
			// $loan_id = $this->uri->segment(2);
		}
		else
		{
			$loan_id = '';
		}
		//     If loan_id exist then popup open else "select loan" modal open only
		if($loan_id != '')
		{
			include('property_modals.php');
?>

			<!---------------------------saved _document_modal one start------------------------------->


	<div class="modal fade" id="new_modal_44" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
			
					<div class="modal-header">
						<h4 class="modal-title">Save Document Page</h4>
					</div>
					
					<div class="modal-body">
						<div class="button_class">
							<button data-toggle="modal" href="#"  class="btn blue btn-block">Closing Statements</button>
							<button data-toggle="modal" href="#"  class="btn blue btn-block">Loan Documents</button>
							<button data-toggle="modal" href="#"  class="btn blue btn-block">Escrow/Title Documents</button>
							<button data-toggle="modal" href="#"  class="btn blue btn-block">Property Insurance</button>
						
						</div>
					</div>
				
					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
					</div>
				
				</div>
			<!-- /.modal-content -->
			</div>
		<!-- /.modal-dialog -->
	</div>



		<!----------------------------saved _document_modal one starts------------------------------>

	
			<!---------------------------new print_document_modal one start------------------------------->


	<div class="modal fade" id="new_modal_1" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
			
					<div class="modal-header">
						<h4 class="modal-title">Loan Document Page</h4>
					</div>
					
					<div class="modal-body">
						<div class="button_class">
							<button data-toggle="modal" href="#print_loan_document"  class="btn blue btn-block">Print Documents</button>
							<button data-toggle="modal" href="#new_modal_2"  class="btn blue btn-block">Saved Documents</button>
						
						</div>
					</div>
				
					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
					</div>
				
				</div>
			<!-- /.modal-content -->
			</div>
		<!-- /.modal-dialog -->
	</div>



		<!---------------------------new print_document_modal one end------------------------------->

 <!---------------------------new print_document_modal second start------------------------------->
	
	
	
	        <!---------------------------new print_document_modal second end------------------------------->
	
	




		<!---------------------------LENDER LOAN DOCUMENT MODAL------------------------------->

	<div class="modal fade" id="lender_loan_document" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
				<form id = "frm_loan_document" method="POST" action="<?php echo base_url();?>form_loan_documents" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<!--<h4 class="modal-title">Loan Documents :# <?php //echo $talimar_loan;?></h4>-->
						<h4 class="modal-title">Loan Documents: <?php echo $property_addresss;?></h4>
					</div>
					
					<div class="modal-body">
				
						<div class="loan_document_tab" style="width:125% !important;">
							<input type="hidden" name="talimar_loan" value="<?= $talimar_loan; ?>">
							<input type="hidden" name="loan_id" value="<?= $loan_id; ?>">
						
							<table class="table table-responsive" id="loan_doc_table">
								<thead>
									<tr>
										<th style = "text-align:center;">Saved Documents</th>
										<th style = "text-align:center;"><a title="Delete" href = "javascript:void(0);" onclick ="delete_doc();" ><i class="fa fa-trash" aria-hidden="true"></i></a></th>
										
										
										<!--<th style = "text-align:center;">View</th>
										<th>File Name</td>
										<th width="175px;">Date Modified</th>-->
									</tr>
								</thead>
								<tbody>
									<?php
										if(isset($fetch_loan_document)){
											foreach($fetch_loan_document as $key => $loan_document){
												
												/* echo '<pre>';
												print_r($fetch_loan_document);
												echo '</pre>'; */
												$view_url = aws_s3_document_url('loan_documents/'.$talimar_loan.'/'.$loan_document->doc_name);
												
									?>
									<tr>
									
										<td style = "text-align:left;"><a href="<?php echo $view_url;?>" target="_blank"><?php echo $loan_document->doc_name;?></a></td>
									
										<td style = "text-align:center;"><input id = "delete_doc_<?php echo $key;?>" onchange = "delete_document('<?php echo $key;?>',<?php echo $loan_document->id;?>)" type = "checkbox"/>
										<input type = "hidden" value = "" name = "doc_id[]" id = "doc_id_<?php echo $key;?>"></td>
										<!--<td style = "text-align:center;"> <i class="fa fa-eye" aria-hidden="true"></i></a></td>
										<td>
											
											<div class = "pdf_thumbnail">
											<iframe src='<?php  //echo  base_url().'loan_documents/'.$talimar_loan.'/'.$loan_document->doc_name;?>' width='100%' height='100%' frameborder='0'></iframe>
											</div>
										</td>
										<td><?php //echo isset($loan_document->modified_date) ? date('m/d/Y',strtotime($loan_document->modified_date)) :  '';?></td>-->
									</tr>
									<?php	}
										}
									?>
									
								</tbody>
							</table>
							
						</div>
					</div>
				
					<div class="modal-footer">
						<a style = "float:left;margin-left: 10px;" href = "javascript:void(0);"  class="btn blue" onclick = "add_loan_document_row()">(+) Add Document</a>
						<button type="submit" class="btn blue">Save</button>
						<!--<a class="btn blue"href = "javascript:void(0);"  onclick ="delete_doc();" >Delete</a>-->
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
						<!--<a  class="btn blue" onclick="add_new_file()">Add</a>-->
					</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>


	<!--------------------------END OF LENDER LOAN DOCUMENT MODAL---------------------------->
	<!--------------------------start errror modal  ---------------------------->
	
	<div class="modal fade" id="errorss" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
				<form id = "frm_loan_document" method="POST" action="#" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		
						<h4 class="modal-title">Loan Errors:</h4>
					</div>
					
					<div class="modal-body">
				
						<div class="loan_document_tab" style="width:125% !important;">
							<input type="hidden" name="talimar_loan" value="">
							<input type="hidden" name="loan_id" value="">
						
							<table class="table table-responsive c" id="loan_doc_table">
								<thead>
									<tr>
										<th style = "text-align:left;">Item:</th>
										<th style = "text-align:center;">Status:</th>									
									</tr>
								</thead>
								<tbody>
									
									<tr>
										<td style = "text-align:left;">Underwriting</td>
										<td style = "text-align:center;"></td>
									</tr>
									<tr>
										<td style = "text-align:left;">Loan Post Closing </td>
										<td style = "text-align:center;"></td>
									</tr>
									<tr>
										<td style = "text-align:left;">Loan Closing</td>
										<td style = "text-align:center;"></td>
									</tr>
									<tr>
										<td style = "text-align:left;">Loan Servicing</td>
										<td style = "text-align:center;"></td>
									</tr>
									<tr>
										<td style = "text-align:left;">Lender Servicing </td>
										<td style = "text-align:center;"></td>
									</tr>
									
								</tbody>
							</table>
							
						</div>
					</div>
				
					<div class="modal-footer">
					
					
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
					
					</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>


<!----  End of Source and Users Modal  ----->

<!----  Start Underwriting Modal  ----->

	<!-------------------------------- Due Diligence item Start ------------------------------------------>
	
	<!--------------------------------- Due Diligence item End ------------------------------------------->




<!----  End Underwriting Modal  ----->

	<!----------------------------closing statement Modal ----------------------->
	
	
	
	<!--------------------------------End Closing statement Modals--------------------->
	<div class="modal fade" id="check_closing_statement" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-body">
					<div class="row" style="padding-top:8px;">
				
						<div class="col-md-8" style="padding-top:6px;"><p>Do you want to Save Information?</p></div>
						<div class="col-md-2">
							<a style="margin-left:38px;" class="btn blue borrower_save_button" href="javascript:void(0);" onclick = "save_info('closing_statement')">Yes<span></span></a>
						</div>
						<div class="col-md-2">
							<a class="btn default borrower_save_button" href = "<?php echo base_url().'load_data';?>" >No</a>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!---------------------------MODAL Monthly Payment------------------->
	
	<div class="modal fade" id="monthly_payment" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<form action="<?php echo base_url();?>form_monthly_payment" method="POST" id="monthly_payment_form">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<!--<h4 class="modal-title">Monthly Payment: #<?php //echo $modal_talimar_loan;?></h4>-->
						<h4 class="modal-title">Monthly Payment: <?php echo $property_addresss;?></h4>
					</div>
					<div class="modal-body">
						<div class="portlet-body rc_class">
								<!-----------Hidden fields----------->
								<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
								<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
								<!-----------End of hidden Fields---->
							<table id="monthly_statement_table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
								<thead>
									<th>Items</th>
									<th>Monthly Payment</th>
									<th>&nbsp;</th>
								</thead>
									
								<?php 
								if(count($result_monthly_payment) > 0)
								{
									foreach($result_monthly_payment as $row)
									{
										
									if($row->items == "Mortgage Payment")
									{
										$m_amount = $fetch_loan_result[0]->payment_amount;
									}
									else
									{
										$m_amount = $row->amount;
									}
											
									// if($row->items == "Mortgage Payment")

								?>
								<tbody>
									<tr>
									
										<td><input type="text" name="items[]" value="<?= $row->items;?>" <?php if($row->items == 'Monthly Loan Payment' || $row->items == 'Property Tax Impound' || $row->items == 'Property Insurance Impound' || $row->items == 'Mortgage Payment' ) { echo "readonly "; } ?>required /></td>
										<td><input type="text" name="monthly_amount[]" value="<?= '$'.number_format($pay_amount,2);?>" id="monthly_amount[]" class="number_only amount_format m_p_amount" <?php if($row->items == "Mortgage Payment"){ echo 'readonly';} ?>/></td>
									
										<td> <?php if($row->items == 'Monthly Loan Payment' || $row->items == 'Property Tax Impound' || $row->items == 'Property Insurance Impound' ){ echo ''; } else { ?><a class="m_s_remove"><i class="fa fa-trash" aria-hidden="true"></i></a> <?php } ?></td>
								    </tr>
								 
								<?php 
									}
								}
								?>
									<tr id="0">
										<td><input type="text" name="items[]" /></td>
										<td><input type="text" name="monthly_amount[]" class="number_only amount_format m_p_amount" id="monthly_amount[]"/></td>
										
										<td><a class="m_s_remove"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
									</tr>
								</tbody>
								  
									<tfoot>
										<tr>
										<th>Total Monthly Payment</th>
										<td id="monthly_payment_total"><input type="text" value="<?= '$'.number_format($amount_monthly_payment[0]->amount,2);?>" readonly ></td>
										<td></td>
										
										</tr>
								
										<tr>
											<th>Escrow Impound Account</th>
											<td>
												<select name = "escrow_impound_account">
													<?php
													foreach($yes_no_option as $key => $row)
													{
														?>
														<option value="<?php echo $key;?>" <?php if(isset($fetch_extra_details[0]->escrow_impound_account)){ if($key == $fetch_extra_details[0]->escrow_impound_account){ echo 'selected'; } } ?>><?php echo $row; ?></option>
														<?php
													}
													?>
												</select>
											</td>
											<td></td>
										</tr>
									</tfoot>
								  
							</table>
							


							 <table class="samplerow" class="table table-bordered table-striped table-condensed flip-content th_text_align_center"  style="display:none" id="">
								<tr>
								  <td><input type="text" name="items[]"  /></td>
								  <td><input type="text" name="monthly_amount[]" id="monthly_amount[]" class="number_only amount_format m_p_amount" /></td>
								  
								   <td><a class="m_s_remove"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
								</tr>
							 </table>
						</div>
					</div>
					<div class="modal-footer">
						<input type="submit" value = "Save" class="btn blue" onclick="monthly_payment_form()">
						<button id="m_s_addrow" value="" class="btn blue" >Add</button>
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
								<!-- /.modal-dialog -->
	</div>
	
	<!-----------------------End of Monthly payment modal----------------------->
	
	
	<!---------------------MODAL Impound Account Payment------------------->
	
	
	
	<!---------------------------End MODAL Impound Account Payment------------------->	
	
	<!---------------------------MODAL Loan Distribution------------------->
	
	<div class="modal fade" id="loan_distribution" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<form action="<?php echo base_url();?>form_loan_distibution" method="POST" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<!--<h4 class="modal-title">Loan Distribution: #<?php// echo $modal_talimar_loan;?></h4>-->
						<h4 class="modal-title">Loan Distribution: <?php echo $property_addresss;?></h4>
					</div>
					<div class="modal-body">
						<div class="portlet-body rc_class">
								<!-----------Hidden fields----------->
								<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
								<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
								<!-----------End of hidden Fields---->
							<table id="loan_distibution_table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
									<thead>
										<th>Items</th>
										<th>Disbursement</th>
										<th>Description </th>
										<th>&nbsp;</th>
										
									</thead>
									
									
									<tbody>
									<?php
									if(isset($fetch_distribution_data)){
										foreach($fetch_distribution_data as $row)
										{
											$readonly = '';
											if($row->items == 'Purchase Price' || $row->items == 'Renovation Reserve' || $row->items == 'Interest Reserve')
											{
												$readonly = 'readonly';
											}
											?>
											<tr>
									
												<td><input type="text" name="items[]" value="<?php echo $row->items; ?>" <?php echo $readonly; ?> /></td>
												<td><input type="text" name="disbursement[]" class="number_only distribution_disbursement" value="<?php echo '$'.number_format($row->disbursement,2); ?>"  /></td>
												<td><input type="text" name="description[]"  value="<?php echo $row->description; ?>"  /></td>
												
												<td><a class="distribution_remove"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
											</tr>
											<?php
										}
									}
									?>
									<tr>
									
										<td><input type="text" name="items[]" /></td>
										<td><input type="text" name="disbursement[]" class="number_only distribution_disbursement" /></td>
										<td><input type="text" name="description[]" /></td>
										
										<td><a class="distribution_remove"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
									</tr>
								  </tbody>
								  
								  <tfoot>
									<tr>
									<th>Total Disbursements:</th>
									<td id="distribution_total"><input type="text" value="<?php echo '$'.number_format($total_sum_disbursement,2);?>" readonly ></td>
									<td></td>
									
									</tr>
									
									<tr>
									<th>Total Loan Amount:</th>
									<td id="distribution_total_loan"><input type="text" value="<?php echo '$'.number_format($fetch_loan_result[0]->loan_amount,2); ?>" readonly ></td>
									<td></td>
									</tr>
									<?php
									$distribution_difference = $fetch_loan_result[0]->loan_amount - $total_sum_disbursement;
									?>
									<tr>
									<th>Difference:</th>
									<td id="distribution_difference"><input type="text" value="<?php echo '$'.number_format($distribution_difference,2); ?>" readonly ></td>
									<td></td>
									</tr>
								  </tfoot>
								  
							</table>
							


							 <table class="distribution_hidden" class="table table-bordered table-striped table-condensed flip-content th_text_align_center"  style="display:none" id="">
								<tr>
								  <td><input type="text" name="items[]"  /></td>
								  <td><input type="text" name="disbursement[]"  class="number_only distribution_disbursement" /></td>
								  <td><input type="text" name="description[]"  /></td>
								  
								   <td><a class="distribution_remove"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
								</tr>
							 </table>
						</div>
					</div>
					<div class="modal-footer">
						<input type="submit" value = "Save" class="btn blue">
						<a id="distribution_addrow" value="" class="btn blue" >Add</a>
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
								<!-- /.modal-dialog -->
	</div>
		
			<!-----------  End of Recording Information ------------------------------->
			
			<!---------------Modal loan servicing----------------------->
			<?php 
			$field_name = array('id', 'talimar_loan', 'loan_status', 'closing_status', 'boarding_status', 'condition','loan_payment_status','loan_maturity_status','construction_status', 'reason', 'nod_record_date', 'nod_sale_date', 'list_market','list_market_SalePrice', 'demand_requested', 'demand_requested_date', 'payoff_request', 'pay_off_resquested_date', 'payoff_date','paidoff_status','document_released', 'document_sign', 'released_to_closed', 'servicer_submitted', 'lender_fees_received', 'website_updated', 'interest_payment_received', 'renovation_reserve_received', 'fund_released', 'loan_recorded' , 'file_closed', 'selling_price', 'fractional_intrest', 'min_lender_investment', 'date_inetrest_paid', 'servicing_agent', 'sub_servicing_agent','broker_disbrusement', 'assigment_record_close', 'lender_minimum', 'servicing_fee', 'minimum_servicing_fee','servicing_lender_rate', 'late_fee', 'extention_fee', 'min_interest', 'default_rate', 'who_pay_servicing', 'pay_setup_fee', 'broker_servicing_fees', 'trust_deed_posted_on_website','comming_soon','trust_deed_funded' ,'release_social_media', 'posted_on_website', 'posted_site_sign','market_trust_deed', 'trust_deed_posted_on_website_not_applicable', 'release_social_media_not_applicable','posted_on_website_not_applicable','posted_site_sign_not_applicable','marketing_complete','loan_article','youtube_url', 'process_bai', 'compleate_bai', 'posted_bai', 'blasted_bai', 'marketing_message','extension_borrower', 'extension_letter_sent', 'signed_extension_letter', 'extension_fee_received', 'extension_request_sent_servicer', 'is_multi_lender','lender_waives_apr', 'fund_intent_cell', 'fund_broker_controll', 'low_loan_doc', 'taxes_deliquent', 'servicing_arrangent', 'is_broker_s_agent' , 'd_multi_property_loan', 'broker_inquiry', 'borrower', 'credit_report', 'seller_note', 'trustor', 'borrower_d_fund_portion', 'borrower_d_arrange_behalf_other', 'b_d_principal_borrower_funds', 'broker_capacity' ,'broker_involved','a_d_behalf_another', 'a_d_principal_exit_loan', 'a_d_arrange_sale_portion','l_o_behalf_another','l_o_principal_borrower' , 'l_o_portion_loan' , 'broker_capacity_explain' ,'n_i_payment_more_then', 'n_i_if_yes','dead_reason', 'deliquences', 'deliq_no_explain', 'subordination_provision', 'optional_insurance', 'how_setup_fee_pay', 'n_subordination_explain', 't_user_id','payment_notification','payment_Exhausted','renovation','cancelled_date','available_release','boarding_fee_paid','servicing_disb','payment_reserve_opt','no_address','new_market_trust_deed','extension_signed','extension_offered','extension_received','extension_processed',	'extension_disbursed','extension_notes','extension_complete','extension_date_from','extension_to','add_extension_id','loan_source','loan_closing_not_post','loan_closing_post','term_sheet','servicing_desp_verified','loan_submited','loan_posted','board_complete','lender_approval','loan_board','closing_checklist_complete','term_sheet_status','lsc','ach_activated','image_ab_activate','image_ab_date_completed','image_ab_suggested_url','image_ab_actual_url','image_ab_Posted','image_ab_youtube_url');
			

			
			foreach($field_name as $row)
			{
				if(count($loan_servicing_data) > 0)
				{
					$loan_servicing[$row] = $loan_servicing_data[0]->$row;
				}
				else
				{
					$loan_servicing[$row] = '';
					if($row == 'sub_servicing_agent' )
					{
						$loan_servicing[$row] = '3';
					}
					if($row == 'selling_price' )
					{
						$loan_servicing[$row] = '0.00';
					}
					if($row == 'servicing_fee')
					{
						$loan_servicing[$row] = '1';
					}
					if($row == 'minimum_servicing_fee')
					{
						$loan_servicing[$row] = '20.00';
					}
					if($row == 'late_fee')
					{
						$loan_servicing[$row] = '75';
					}
					if($row == 'extention_fee')
					{
						$loan_servicing[$row] = '75';
					}
					if( $row == 'default_rate' )
					{
						$loan_servicing[$row] = '50';
					}
					if( $row == 'who_pay_servicing' )
					{
						$loan_servicing[$row] = '1';
					}
					if( $row == 'broker_involved' )
					{
						$loan_servicing[$row] = '1';
					}
					if( $row == 'broker_capacity' )
					{
						$loan_servicing[$row] = '1';
					}
					if( $row == 'broker_inquiry' )
					{
						$loan_servicing[$row] = '1';
					}
					if( $row == 'borrower' )
					{
						$loan_servicing[$row] = '1';
					}
					if( $row == 'credit_report' )
					{
						$loan_servicing[$row] = '1';
					}
					if( $row == 'seller_note' )
					{
						$loan_servicing[$row] = '1';
					}
					if( $row == 'trustor' )
					{
						$loan_servicing[$row] = '1';
					}
					/* if( $row == 'is_multi_lender' )
					{
						$loan_servicing[$row] = '1';
					} */
					if( $row == 'taxes_deliquent' )
					{
						$loan_servicing[$row] = '0';
					}
					/* if( $row == 'd_multi_property_loan' )
					{
						$loan_servicing[$row] = '0';
					} */
					/* if( $row == 'servicing_arrangent' )
					{
						$loan_servicing[$row] = '1';
					}
					if( $row == 'fund_intent_cell' )
					{
						$loan_servicing[$row] = '0';
					} */
					if( $row == 'n_i_payment_more_then' )
					{
						$loan_servicing[$row] = '0';
					}
					/* if( $row == 'subordination_provision' )
					{
						$loan_servicing[$row] = '0';
					} */
					if( $row == 'l_o_behalf_another' )
					{
						$loan_servicing[$row] = '1';
					}
					if( $row == 'a_d_principal_exit_loan' )
					{
						$loan_servicing[$row] = '1';
					}
					if( $row == 'trust_deed_funded' )
					{
						$loan_servicing[$row] = '1';
					}
					if( $row == 'fund_broker_controll' )
					{
						$loan_servicing[$row] = '3';
					}
					if( $row == 'low_loan_doc' )
					{
						$loan_servicing[$row] = '1';
					}
					if( $row == 'lender_waives_apr' )
					{
						$loan_servicing[$row] = '1';
					}
					if( $row == 'is_broker_s_agent' )
					{
						$loan_servicing[$row] = '1';
					}
					
				}
			}
			
			
			?>
			
			
			
	<div class="modal fade bs-modal-lg" id="vendor_data" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">					
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<!--<h4 class="modal-title">Vendor Data: #<?php//echo $modal_talimar_loan;?></h4>-->
					<h4 class="modal-title">Vendor Data: <?php echo $property_addresss;?></h4>
				</div>
				<form class="form-horizontal" method="POST" action= "<?php echo base_url();?>add_vendor_data">
					<div class="modal-body">
						<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
								
						<input type="hidden" name = "vendor_id" value="<?php echo isset($vendor->vendor_id) ? $vendor->vendor_id : '' ;?>">
						<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
					
						<div class="row ">
							
							<div class="col-md-3">
								<label>Vendor Name:</label>
								<input type="text" id="vendor_name" class="form-control" name="vendor_name" value ="<?= isset($vendor->vendor_name) ? $vendor->vendor_name : '';?>"  required/>
							</div>
							<div class="col-md-3">
								<label> Contact Name:</label>
								<input type="text" class="form-control" name = "contact_name" value ="<?= isset($vendor->contact_name) ? $vendor->contact_name : '';?>"  />
							</div>
						</div>
						<div class="row ">					
							<div class="col-md-3">
								<label>Fax:</label>
								<input type="text" class="form-control" name = "fax" value ="<?= isset($vendor->fax) ? $vendor->fax : '';?>">
							</div>
								
							<div class="col-md-3">
								<label>Email</label>
								<input type="text" class="form-control" name = "email" value ="<?= isset($vendor->email) ? $vendor->email : '';?>"  />
								
							</div>
								
						</div>
						<div class="row ">					
							<div class="col-md-3">
								<label>Address:</label>
								<input type="text" class="form-control" id = "address" name = "address" value ="<?= isset($vendor->address) ? $vendor->address : '';?>">
							</div>
								
							<div class="col-md-3">
								<label>Website</label>
								<input type="text" class="form-control" name = "website" value ="<?= isset($vendor->website) ? $vendor->website : '';?>"  />
									
							</div>
						</div>
							
						<div class="row ">					
							<div class="col-md-3">
								<label>Street Address:</label>
								<input type="text" class="form-control" id = "street_address" name = "street_address" value ="<?= isset($vendor->street_address) ? $vendor->street_address : '';?>">
							</div>
							<div class="col-md-3">
								<label>Unit #:</label>
								<input type="text" class="form-control" name = "unit" value ="<?= isset($vendor->unit) ? $vendor->unit : '';?>"  />
							</div>
						</div>
							
						<div class="row ">					
							<div class="col-md-3">
								<label>City:</label>
								<input type="text" class="form-control" id = "city" name = "city" value ="<?= isset($vendor->city) ? $vendor->city : '';?>">
							</div>
								
							<div class="col-md-3">
								<label>State:</label>
								<input type="text" class="form-control" name = "state" value ="<?= isset($vendor->state) ? $vendor->state : '';?>"  />
							</div>
						</div>
							
						<div class="row ">					
							<div class="col-md-3">
								<label>Zip:</label>
								<input type="text" class="form-control" id = "zip" name = "zip" value ="<?= isset($vendor->zip) ? $vendor->zip : '';?>">
							</div>
								
							<div class="col-md-3">
								<label>tax Id:</label>
								<input type="text" class="form-control" name = "tax_id" value ="<?= isset($vendor->tax_id) ? $vendor->tax_id : '';?>"  />
							</div>
								
						</div>
							
						<div class="row ">					
							<div class="col-md-3">
								<label>vendor_type:</label>
								<select class="form-control" name = "vendor_type" />
								
									<?php 
									foreach($vendor_types as $key => $type){
										$vendor_type_optn = isset($vendor->vendor_type) ? $vendor->vendor_type : '0';
										if($key == $vendor->vendor_type){
											$selected = 'selected = "selected"';
										}else{
											$selected = '';
										}
										
										echo '<option '.$selected.' value = '.$key.'>'.$type.'</option>';
									} 
									?>
								</select>
							</div>
						</div>
							
						<!--<div class="row borrower_data_row">					
							<div class="form-group">
								<div class="">
									<button  style= "float: left;margin-left: 30px;" type = "submit" name = "save" class="btn btn-primary " value="save">Save</button>
								</div>
							</div>
						</div>-->
					</div>
					
					<div class="modal-footer">
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
							<button type="submit" name="form_button" value="save" class="btn blue">Save</button>
					</div>
					
				</form>
			</div>
		</div>
	</div>
	
	<!--------------- LOAN SERVICING MODAL POPUP STARTS ----------------------------->
	
	
	



	<!--------------- loan Contacts start ----------------------------->
	
	
	<!--------------- Email script end ----------------------------->
	
	
	<!--------------- loan Payment History start ----------------------------->

	<div class="modal fade in" id="PaymentHistory" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<form method="POST" action ="#">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Payment History : <?php echo $property_addresss;?></h4>
						</div>
						<div class="modal-body">
							
							<input type="hidden" name="talimar_loan" value="<?php echo $talimar_loan;?>">
							<input type="hidden" name="loan_id" value="<?php echo $loan_id;?>">


							<div class="row">
								

								
									<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" width="100%">
									  	<thead>
											<tr>
												<th>Date Due</th>
												<th>Date Paid</th>
												<th>Difference</th>
												<th>Payment Type</th>
												<th>Payment</th>
												
											</tr>
										</thead>
										
									    <tbody id="PaymentHistoryres">
									
										</tbody>
									
										<tfoot id="NextPaymentDate">
											
											
										</tfoot>
									</table>
								
							</div>
						</div>
						<div class="modal-footer">
							<!-- <button type="button" name="submit" value="save" class="btn blue">Save</button> -->
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
	</div>
<script type="text/javascript">

<?php $loan_id1 = $this->uri->segment(2); 
	$paymentHistory= $this->User_model->query("select fci from loan where id = '".$loan_id1."' AND fci != ''");
	$paymentHistory = $paymentHistory->row(); 

	if(!empty($paymentHistory))
	{ ?>
		getPaymentHistory(<?php echo $paymentHistory->fci; ?>);
	<? } else
	{ ?>
		var result = '<tr><td colspan="5">No payment history found!</td></tr>';
		$("#PaymentHistoryres").html(result); 
	<?php } ?>

 function getPaymentHistory(loanAccount)
 {
 	var result;
 	var NextDate;
 	$.ajax({
			type : 'POST',
			url  : '<?php echo base_url()."Load_data/getPaymentHistoryFCI";?>',
			data : {'loanAccount':loanAccount},
			success : function(response){
				var json = JSON.parse(response);

				// console.log(json);

				if(json.status > 0)
				{
					result = json.result;
					NextDate = '<tr><td colspan="5" style="text-align: left;color: red;">Next payment date is '+json.NextDate+'</td></tr>';
				}
				else
				{
					result = '<tr><td colspan="5">No payment history found!</td></tr>';
					NextDate = '<tr><td colspan="5"></td></tr>';
				}

				$("#PaymentHistoryres").html(result); 
				 
				$("#NextPaymentDate").html(NextDate); 
				
			}
	});
 }
				
			

</script>
<div class="modal fade in" id="PaymentHistoryDetails1" tabindex="-1" role="dialog" aria-hidden="true" style="width: 80%; margin-left: auto;margin-right: auto;">
			<div class="modal-dialog" style="width: 100%; margin-left: auto;margin-right: auto;">
				<div class="modal-content">
					<form method="POST" action ="#">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Payment History : <?php echo $property_addresss;?></h4>
						</div>
						<div class="modal-body" style="overflow: auto;">

							<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" width="100%">
							  	<thead>
									<tr>
										<th>Date Received</th>
										<th>Date Due</th>
										<th>Payment Day Variance </th>
										<th>Payment Type</th>
										<th>Total Payment</th>

										<th>Interest Received</th>
										<th>Principal Received</th>
										<th>Accrued Charges</th>
										<th>Late Charges Paid</th>
										<th>Reserve Payment</th>

										<th>Escrow payment</th>
										<th>PPP Payment</th>
										<th>Charges Principal Payment</th>
										<th>Broker Fees</th> 	
										<th>Lender fees</th>

										<th>Other (Taxable)</th>
										<th>Other (Non-Taxable)</th>
										<th>Other Payment</th>
										<th>Accrued Unpaid Interest</th> 	
										<th>Additional Information</th> 		
									</tr>
								</thead>
								
							    <tbody id="PaymentHistoryres1">
							
								</tbody>
							
								
							</table>
							
						</div>
						<div class="modal-footer">
							<!-- <button type="button" name="submit" value="save" class="btn blue">Save</button> -->
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
<script type="text/javascript">
		function getPaymentHistoryDetails(loanAccount)
		 {
		 	var result;
		 	
		 	$.ajax({
					type : 'POST',
					url  : '<?php echo base_url()."Load_data/getPaymentHistoryDetailsFCI";?>',
					data : {'loanAccount':loanAccount},
					success : function(response){
						var json = JSON.parse(response);

						console.log(json);

						if(json.status > 0)
						{
							result = json.result;
							
						}
						else
						{
							result = '<tr><td colspan="20">No payment history found!</td></tr>';
						}

						$('tbody#PaymentHistoryres1').html(result);

						$('#PaymentHistoryDetails1').modal('show');

					}
			});
			 
		 	
		 }
	</script>

 
	<!--------------- loan Payment History end ----------------------------->
	<!---------------- Status --------------------------------------------->
	<div class="modal fade in" id="autoNotification" tabindex="-1" role="dialog" aria-hidden="true" style="width: 80%; margin-left: auto;margin-right: auto;">
			<div class="modal-dialog" style="width: 100%; margin-left: auto;margin-right: auto;">
				<div class="modal-content">
					
					<form method="POST" action ="#">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Auto Notifications : <?php echo $property_addresss;?></h4>
						</div>
						<div class="modal-body" style="overflow: auto;">

					<?php
					$remainderStatus = '';
					$googleReview = '';
					$paymentReminder = '';
					$paymentReminder3 = '';
					$paymentReminder7 = '';
					$paymentReminder10 = '';
					$propertyInsurance = '';
					$releaseDrawNotification = '';
					$constructionUpdate = '';
					$maturityNotification = '';
					$payoffReceived = '';
					$loanFunded = '';
					$loanBoarded = '';
					$renovationsCompleted = '';
					$loanPaidoff = '';
					if($uri_segment_two != 'new'){
						if($loan_id){
							$loan_idNew = (int)str_replace('/', '', $loan_id);
							$whereD = array();
							$whereD['id'] = $loan_idNew;
							$fetchloan_result = $this->User_model->select_where('loan',$whereD);
							$fetchloan_result = $fetch_loan_data->row();
							
							$remainderStatus = $fetchloan_result->remainderStatus;
							$googleReview = $fetchloan_result->googleReview;
							$paymentReminder = $fetchloan_result->paymentReminder;

							$paymentReminder3 = $fetchloan_result->paymentReminderDays3;
							$paymentReminder7 = $fetchloan_result->paymentReminderDays7;
							$paymentReminder10 = $fetchloan_result->paymentReminderDays10;

							$propertyInsurance = $fetchloan_result->propertyInsurance;
							$releaseDrawNotification = $fetchloan_result->releaseDrawNotification;
							$constructionUpdate = $fetchloan_result->constructionUpdate;
							$maturityNotification = $fetchloan_result->maturityNotification;
							$payoffReceived = $fetchloan_result->payoffReceived;
							$loanFunded = $fetchloan_result->loanFunded;
							$loanBoarded = $fetchloan_result->loanBoarded;
							$renovationsCompleted = $fetchloan_result->renovationsCompleted;
							$loanPaidoff = $fetchloan_result->loanPaidoff;
						}
					}
					?>
			
							<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" width="100%">
							  	<thead>
									<tr align="left" style="background-color: #dddddd">
										<th colspan="4" style="background-color: #dddddd; text-align: left;">Borrower Notifications</th> 		
									</tr>
								</thead>
								<tbody>
									<tr align="left">
										<th>Welcome E-Mail</th>
										<th>
											<select class="form-control input-md" onchange="AutoNotifications('remainderStatus', this.value, <?php echo $loan_idNew; ?>)">
												<option <?php if($remainderStatus == 'no'){ echo "selected"; } ?> value="on">On</option>
												<option <?php if($remainderStatus == 'off'){ echo "selected"; } ?> value="off">Off</option>
											</select>
										</th>
										<th>Not Functioning</th>
										<th>Link</th> 		
									</tr>
									<tr align="left">
										<th>Google Review</th>
										<th>
											<select class="form-control input-md" onchange="AutoNotifications('googleReview', this.value, <?php echo $loan_idNew; ?>)">
												<option <?php if($googleReview == 'no'){ echo "selected"; } ?> value="on">On</option>
												<option <?php if($googleReview == 'off'){ echo "selected"; } ?> value="off">Off</option>
											</select>
										</th>
										<th>Not Functioning</th>
										<th>Link</th> 		
									</tr>
									<tr align="left">
										<th>Payment Reminder</th>
										<th>
											<select class="form-control input-md" onchange="AutoNotifications('paymentReminder', this.value, <?php echo $loan_idNew; ?>)">
												<option <?php if($paymentReminder == 'no'){ echo "selected"; } ?> value="on">On</option>
												<option <?php if($paymentReminder == 'off'){ echo "selected"; } ?> value="off">Off</option>
											</select>
										</th>
										<th>Functioning</th>
										<th><a href="<?php echo base_url('auto_mail/loan_payment_reminder/'.$talimar_loan.''); ?>" target="_blank">Link</a></th>		
									</tr>
									<?php
									/*New Changes
									Changes By-Bitcot
									Updated Date-29-07-2021
									Description- Changes as per tesk requirement*/
									?>
									<tr align="left">
										<th>3 Day Payment Reminder</th>
										<th>
											<select class="form-control input-md" onchange="AutoNotifications('paymentReminderDays3', this.value, <?php echo $loan_idNew; ?>)">
												<option <?php if($paymentReminder3 == 'no'){ echo "selected"; } ?> value="on">On</option>
												<option <?php if($paymentReminder3 == 'off'){ echo "selected"; } ?> value="off">Off</option>
											</select>
										</th>
										<th>Functioning</th>
										<th><a href="<?php echo base_url('auto_mail/loan_payment_reminder/'.$talimar_loan.''); ?>" target="_blank">Link</a></th>		
									</tr>
									<tr align="left">
										<th>7 Day Payment Reminder</th>
										<th>
											<select class="form-control input-md" onchange="AutoNotifications('paymentReminderDays7', this.value, <?php echo $loan_idNew; ?>)">
												<option <?php if($paymentReminder7 == 'no'){ echo "selected"; } ?> value="on">On</option>
												<option <?php if($paymentReminder7 == 'off'){ echo "selected"; } ?> value="off">Off</option>
											</select>
										</th>
										<th>Functioning</th>
										<th><a href="<?php echo base_url('auto_mail/loan_payment_reminder/'.$talimar_loan.''); ?>" target="_blank">Link</a></th>		
									</tr>
									<tr align="left">
										<th>10 Day Payment Reminder</th>
										<th>
											<select class="form-control input-md" onchange="AutoNotifications('paymentReminderDays10', this.value, <?php echo $loan_idNew; ?>)">
												<option <?php if($paymentReminder10 == 'no'){ echo "selected"; } ?> value="on">On</option>
												<option <?php if($paymentReminder10 == 'off'){ echo "selected"; } ?> value="off">Off</option>
											</select>
										</th>
										<th>Functioning</th>
										<th><a href="<?php echo base_url('auto_mail/loan_payment_reminder/'.$talimar_loan.''); ?>" target="_blank">Link</a></th>		
									</tr>
									<?php 
									/*New Changes End*/
									?>
									<tr align="left">
										<th>Property Insurance</th>
										<th>
											<select class="form-control input-md" onchange="AutoNotifications('propertyInsurance', this.value, <?php echo $loan_idNew; ?>)">
												<option <?php if($propertyInsurance == 'no'){ echo "selected"; } ?> value="on">On</option>
												<option <?php if($propertyInsurance == 'off'){ echo "selected"; } ?> value="off">Off</option>
											</select>
										</th>
										<th>Not Functioning</th>
										<th>Link</th> 		
									</tr>
									<tr align="left">
										<th>Release Draw Notification</th>
										<th>
											<select class="form-control input-md" onchange="AutoNotifications('releaseDrawNotification', this.value, <?php echo $loan_idNew; ?>)">
												<option <?php if($releaseDrawNotification == 'no'){ echo "selected"; } ?> value="on">On</option>
												<option <?php if($releaseDrawNotification == 'off'){ echo "selected"; } ?> value="off">Off</option>
											</select>
										</th>
										<th>Not Functioning</th>
										<th>Link</th> 		
									</tr>
									<tr align="left">
										<th>Construction Update</th>
										<th>
											<select class="form-control input-md" onchange="AutoNotifications('constructionUpdate', this.value, <?php echo $loan_idNew; ?>)">
												<option <?php if($constructionUpdate == 'no'){ echo "selected"; } ?> value="on">On</option>
												<option <?php if($constructionUpdate == 'off'){ echo "selected"; } ?> value="off">Off</option>
											</select>
										</th>
										<th>Functioning</th>
										<th><a href="<?php echo base_url('auto_mail/borrower_auto_mails/'.$talimar_loan.''); ?>" target="_blank">Link</a></th> 		
									</tr>
									<tr align="left">
										<th>Maturity Notification</th>
										<th>
											<select class="form-control input-md"  onchange="AutoNotifications('maturityNotification', this.value, <?php echo $loan_idNew; ?>)">
												<option <?php if($maturityNotification == 'no'){ echo "selected"; } ?> value="on">On</option>
												<option <?php if($maturityNotification == 'off'){ echo "selected"; } ?> value="off">Off</option>
											</select>
										</th>
										<th>Not Functioning</th>
										<th>Link</th> 		
									</tr>
									<tr align="left">
										<th>Payoff Received</th>
										<th>
											<select class="form-control input-md" onchange="AutoNotifications('payoffReceived', this.value, <?php echo $loan_idNew; ?>)">
												<option <?php if($payoffReceived == 'no'){ echo "selected"; } ?> value="on">On</option>
												<option <?php if($payoffReceived == 'off'){ echo "selected"; } ?> value="off">Off</option>
											</select>
										</th>
										<th>Not Functioning</th>
										<th>Link</th> 		
									</tr>
							    	<tr style="background-color: #dddddd; text-align: left;">
										<th colspan="4" style="background-color: #dddddd; text-align: left;font-weight: 600"><b>Lender Notifications</b></th>
										<!-- <th  style="text-align: left;">On / Off</th>
										<th  style="text-align: left;">Status</th>
										<th  style="text-align: left;">Text</th> --> 		
									</tr>
									<tr align="left">
										<th>Loan Funded</th>
										<th>
											<select class="form-control input-md" onchange="AutoNotifications('loanFunded', this.value, <?php echo $loan_idNew; ?>)">
												<option <?php if($loanFunded == 'no'){ echo "selected"; } ?> value="on">On</option>
												<option <?php if($loanFunded == 'off'){ echo "selected"; } ?> value="off">Off</option>
											</select>
										</th>
										<th>Not Functioning</th>
										<th>Link</th> 		
									</tr>
									<tr align="left">
										<th>Loan Boarded</th>
										<th>
											<select class="form-control input-md" onchange="AutoNotifications('loanBoarded', this.value, <?php echo $loan_idNew; ?>)">
												<option <?php if($loanBoarded == 'no'){ echo "selected"; } ?> value="on">On</option>
												<option <?php if($loanBoarded == 'off'){ echo "selected"; } ?> value="off">Off</option>
											</select>
										</th>
										<th>Not Functioning</th>
										<th>Link</th> 		
									</tr>
									<tr align="left">
										<th>Renovations Completed</th>
										<th>
											<select class="form-control input-md"  onchange="AutoNotifications('renovationsCompleted', this.value, <?php echo $loan_idNew; ?>)">
												<option <?php if($renovationsCompleted == 'no'){ echo "selected"; } ?> value="on">On</option>
												<option <?php if($renovationsCompleted == 'off'){ echo "selected"; } ?> value="off">Off</option>
											</select>
										</th>
										<th>Not Functioning</th>
										<th>Link</th> 		
									</tr>
									<tr align="left">
										<th>Loan Paid Off</th>
										<th>
											<select class="form-control input-md"  onchange="AutoNotifications('loanPaidoff', this.value, <?php echo $loan_idNew; ?>)">
												<option <?php if($loanPaidoff == 'no'){ echo "selected"; } ?> value="on">On</option>
												<option <?php if($loanPaidoff == 'off'){ echo "selected"; } ?> value="off">Off</option>
											</select>
										</th>
										<th>Not Functioning</th>
										<th>Link</th> 		
									</tr>
								</tbody>
							
								
							</table>
							
							
						</div>
						<div class="modal-footer">
							<!-- <button type="button" name="submit" value="save" class="btn blue">Save</button> -->
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	<!--------------- loan Contacts start ----------------------------->
	
	<div class="modal fade in" id="LoanDocument" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<form method="POST" action ="<?php echo base_url();?>New_loan_data/uploadLoanDoc" enctype="multipart/form-data">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Loan Document : <?php echo $property_addresss;?></h4>
						</div>
						<div class="modal-body">
							
							<input type="hidden" name="talimar_loan" value="<?php echo $talimar_loan;?>">
							<input type="hidden" name="loan_id" value="<?php echo $loan_id;?>">


							<div class="row">
								<div class="col-md-6">
									<label>Upload Loan Document</label>
									<input type="file" name="uploanfile" class="form-control">
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" name="submit" value="save" class="btn blue" >Save</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
	</div>



	<!--------------- loan Contacts start ----------------------------->
	<div class="modal fade in" id="LoanBoarding" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
				<div class="modal-content">
					
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Servicer Charges : <?php echo $property_addresss;?></h4>
						</div>
						<div class="modal-body">
							
							<h4 class="custommy">Servicer Charges</h4>

							<form method="POST" id="form_loan_boarding_data" name="form_loan_boarding_data" action ="<?php echo base_url();?>New_loan_data/loan_boarding_data">
							<input type="hidden" name="talimar_loan" value="<?php echo $talimar_loan;?>">
							<input type="hidden" name="loan_id" value="<?php echo $loan_id;?>">
							<div class="row form_loan_boarding_data" style="display:none;">
								<div class="form-group">
									<div class="col-md-12">
									<button type="button" class="btn btn-primary close_ServicerCharges">
										<i class="fa fa-close"></i>
									</button>
									</div>
									<div class="col-md-4">
										<label>Date</label>
										<input type="text" name="ServicerCharge_date" id="ServicerCharge_date" class="form-control" required>
									</div>

									<div class="col-md-4">
										<label>Paid From</label>
										<select name="ServicerCharge_paidfrom" id="ServicerCharge_paidfrom" class="form-control" required>
											<?php foreach($servicer_charge_paid_from as $paid_fromKey => $paid_fromVal){ ?>
											<option value="<?php echo $paid_fromKey;?>"><?php echo $paid_fromVal; ?></option>
										<?php } ?>
										</select>
									</div>

									<div class="col-md-4">
										<label>Paid To</label>
										<select name="ServicerCharge_paidto" id="ServicerCharge_paidto" class="form-control" required>
											<?php foreach($servicer_charge_paid_to as $paid_toKey => $paid_toVal){ ?>
											<option value="<?php echo $paid_toKey;?>"><?php echo $paid_toVal; ?></option>
										<?php } ?>
										</select>
									</div>
									<div class="clearfix">&nbsp;</div>
									<div class="col-md-4">
										<label>Description</label>
										<select name="ServicerCharge_description" id="ServicerCharge_description" class="form-control" required>
											<?php foreach($servicer_charge_description as $descriptionKey => $descriptionVal){ ?>
											<option value="<?php echo $descriptionKey;?>"><?php echo $descriptionVal; ?></option>
										<?php } ?>
										</select>
									</div>

									<div class="col-md-4">
										<label>Invoice#</label>
										<input type="text" name="ServicerCharge_invoice" id="ServicerCharge_invoice" class="form-control" required>
									</div>

									<div class="col-md-4">
										<label>Amount</label>
										<input type="text" name="ServicerCharge_amount" id="ServicerCharge_amount" class="form-control number_only amount_format"  required>
									</div>
									<div class="clearfix">&nbsp;</div>
									<div class="col-md-4">
										<label>Paid</label>
										<select name="BoardingFeesOpt" id="BoardingFeesOpt" class="form-control" required>
										<?php foreach($no_yes_option as $key => $row){ ?>
											<option value="<?php echo $key;?>" <?php if($loan_boarding_data[0]->BoardingFeesOpt == $key){echo 'selected';}?>><?php echo $row; ?></option>
										<?php } ?>
										</select> 
									</div>
									<div class="col-md-12">&nbsp;</div>
									<div class="col-md-12">
									<input type="hidden" name="ServicerCharge_charges_id" id="ServicerCharge_charges_id" value="0">
									<button type="submit" name="submitBoarding" value="save" class="btn blue" >Save</button>
									</div>

								</div>
							</div>
							</form>



							<div class="row">
								<div class="col-md-12">
									<table id="table_loan_boarding_data" class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
										<tr>
											<th>Date</th>
											<th>Paid From</th>
											<th>Paid To</th>
											<th>Description</th>
											<th>Invoice#</th>
											<th>Amount</th>
											<th>Paid</th>
											<th>Action</th>
										</tr>
										<?php
										$totalInvoiceATM = 0;
										if($loan_boarding_data){
											foreach($loan_boarding_data AS $loanBoarding){
												$description = $loanBoarding->bordingText;
												if($loanBoarding->servicercharge_description){
													$description = $servicer_charge_description[$loanBoarding->servicercharge_description];
												}

												$servicercharge_date = '';
												if($loanBoarding->servicercharge_date){
													$servicercharge_date = date("m-d-Y", strtotime($loanBoarding->servicercharge_date));
												}
												$paidfrom = '';
												if($loanBoarding->paidfrom){
													$paidfrom = $servicer_charge_paid_from[$loanBoarding->paidfrom];
												}
												$paidto = '';
												if($loanBoarding->paidto){
													$paidto = $servicer_charge_paid_to[$loanBoarding->paidto];
												}

												$totalInvoiceATM = $totalInvoiceATM + $loanBoarding->bordingFee;
												?>
												<tr>
													<td><?php echo $servicercharge_date; ?></td>
													<td><?php echo $paidfrom; ?></td>
													<td><?php echo $paidto; ?></td>
													<td><?php echo $description; ?></td>
													<td><?php echo $loanBoarding->invoice; ?></td>
													<td>$<?php echo number_format($loanBoarding->bordingFee,2); ?></td>
													<td><?php echo $no_yes_option[$loanBoarding->BoardingFeesOpt]; ?></td>
													<td>


													<button type="button" class="btn btn-primary edit_ServicerCharges" 
													charges_id="<?php echo $loanBoarding->id; ?>" 
													fee="<?php echo $loanBoarding->bordingFee; ?>" 
													feesOpt="<?php echo $loanBoarding->BoardingFeesOpt; ?>" 
													date="<?php echo $servicercharge_date; ?>" 
													paidfrom="<?php echo $loanBoarding->paidfrom; ?>" 
													paidto="<?php echo $loanBoarding->paidto; ?>" 
													description="<?php echo $loanBoarding->servicercharge_description; ?>" 
													invoice="<?php echo $loanBoarding->invoice; ?>"
														>
														<i class="fa fa-edit"></i>
													</button>
													<button type="button" class="btn btn-primary delete_ServicerCharges" charges_id="<?php echo $loanBoarding->id; ?>">
														<i class="fa fa-trash"></i>
													</button>
													</td>
												</tr>
												<?php
											}
										}
										?>
										<tr>
											<th>&nbsp;</th>
											<th>&nbsp;</th>
											<th>&nbsp;</th>
											<th>&nbsp;</th>
											<th>Total:</th>
											<th >$<?php echo number_format($totalInvoiceATM, 2); ?></th>
											<th>&nbsp;</th>
											<th>&nbsp;</th>
										</tr>
									</table>
								</div>
							</div>
							<form action="<?php echo base_url();?>New_loan_data/servicer_charge_delete" name="servicer_charge_delete" id="servicer_charge_delete" method="post">
								<input type="hidden" name="talimar_loan" value="<?php echo $talimar_loan;?>">
								<input type="hidden" name="loan_id" value="<?php echo $loan_id;?>">
								<input type="hidden" name="servicer_charge_delete_id" id="servicer_charge_delete_id" value="">
							</form>
													
						</div>
						<div class="modal-footer">
							<button type="button" name="addServicerCharges" id="addServicerCharges" class="btn blue" >Add Charge</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>					
				</div>
		</div>
	</div>
	<script>
		
		function addBoardingFee(that){
			
			var count = $('#LoanBoarding #addnew .row').length;
			count++;
			$('#LoanBoarding #addnew').append('<div class="row mt-2" id="add_'+count+'"><div class="form-group"><label class="col-md-3 control-label" for="textinput" style="visibility:hidden;">Boarding Fees:</label><div class="col-md-4"><input type="text" name="bordingText[]" class="form-control" placeholder="Description"></div><div class="col-md-4"><input type="text" name="bordingFee[]" class="form-control number_only amount_format" onchange="SumAllBoarding(this);" placeholder="Amount"></div><div class="col-md-1"><a onclick="removethis('+count+');" title="Remove"><i class="fa fa-trash" style="margin: 10px 0px 0px 0px;font-size: 18px;"></i></a></div></div></div>');
		}
		
		function removethis(rowid){
			
			$('#LoanBoarding #add_'+rowid).remove();
		}
		
		//SumAllBoarding(this);
		function SumAllBoarding(that){
			
			var allsum = 0;
			$('#LoanBoarding div.col-md-4 input[name="bordingFee[]"]').each(function(){
				var allvalue = $(this).val();
				var replaceval = allvalue.replace(/\$/g,"");
				allsum += parseInt(replaceval);
			});
			//alert(allsum);
			var total = allsum.toFixed(2);
			$('#LoanBoarding input#TotalFee').val('');
			$('#LoanBoarding input#TotalFee').val('$'+total);
		}
		
		function removethisrow(rowid){
			
			if(confirm('Are you sure to remove this amount?')){
				$.ajax({
							type : 'POST',
							url  : '<?php echo base_url()."New_loan_data/Removeboarding";?>',
							data : {'id':rowid},
							success : function(response){
								
								$('#LoanBoarding #removeid_'+rowid).remove();
								window.location.reload();
							}
				});
			}
		}
	
	</script>					
						
	<div class="modal fade in" id="loan_contacts" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
				<div class="modal-content">
					<form method="POST" action ="<?php echo base_url();?>loan_servicing_contacts">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						
							<h4 class="modal-title">Internal Contacts: <?php echo $property_addresss;?></h4>
						</div>
						<div class="modal-body">
							<input type="hidden" name="talimar_loan" value="<?php echo $talimar_loan;?>">
							<input type="hidden" name="loan_id" value="<?php echo $loan_id;?>">
							
							<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Account Representative: </label>  
										<div class="col-md-6">
											<select name="loan_originator" class="form-control">
											<option value="">Select One</option>
											<?php foreach($all_user as $key => $row){ ?>
												<option value="<?php echo $row->id;?>" <?php if($row->id == $internal_contact_details[0]->loan_originator){echo 'Selected';}?>><?php echo $row->fname.' '.$row->middlename.' '.$row->lname; ?></option>
											<?php } ?>
											
											</select>  
										</div>
								</div>
							</div>
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Underwriter: </label>  
										<div class="col-md-6">
											<select name="underwriter" class="form-control">
											<option value="">Select One</option>
											<?php foreach($all_user as $key => $row){ ?>
												<option value="<?php echo $row->id;?>" <?php if($row->id == $internal_contact_details[0]->underwriter){echo 'Selected';}?>><?php echo $row->fname.' '.$row->middlename.' '.$row->lname; ?></option>
											<?php } ?>
											
											</select>  
										</div>
								</div>
							</div>
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Loan Closer: </label>  
										<div class="col-md-6">
											<select name="loan_closer" class="form-control">
											<option value="">Select One</option>
											<?php foreach($all_user as $key => $row){ ?>
												<option value="<?php echo $row->id;?>" <?php if($row->id == $internal_contact_details[0]->loan_closer){echo 'Selected';}?>><?php echo $row->fname.' '.$row->middlename.' '.$row->lname; ?></option>
											<?php } ?>
											</select> 
										</div>
								</div>
							</div>

							
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Investor Relations: </label>  
										<div class="col-md-6">
											<select name="investor_relations" class="form-control">
											<option value="">Select One</option>
											<?php foreach($all_user as $key => $row){ ?>
												<option value="<?php echo $row->id;?>" <?php if($row->id == $internal_contact_details[0]->investor_relations){echo 'Selected';}?>><?php echo $row->fname.' '.$row->middlename.' '.$row->lname; ?></option>
											<?php } ?>
											</select> 
										</div>
								</div>
							</div>

							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Loan Servicer: </label>  
										<div class="col-md-6">
											<select name="loan_servicer" class="form-control">
											<option value="">Select One</option>
											<?php 
											foreach($all_user as $key => $row){ ?>
												<option value="<?php echo $row->id;?>" <?php if($row->id == $internal_contact_details[0]->loan_servicer){echo 'Selected';}?>><?php echo $row->fname.' '.$row->middlename.' '.$row->lname; ?></option>
											<?php } ?>
											</select> 
										</div>
								</div>
							</div>
						</div>

						<?php
						//for closer email...
						$servicing = $loan_servicing['servicing_fee'] ? $loan_servicing['servicing_fee'] : '1.00';

						?>
						
						<div class="modal-footer">
							
							<!--<a class="btn blue" onclick="send_closer_email(this);"><i class="fa fa-envelope" aria-hidden="true"></i> Email</a>-->

							<button type="submit" name="submit" value="save" class="btn blue" >Save</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
		</div>
	</div>
	
	<!--------------- loan Contacts end ----------------------------->
	
	
	<!-------------- The Auto Debit Payment Modal start-->

	<div class="modal fade in" id="auto_debit_payment" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
		 <div class="modal-content">
		  <form method="post" action="<?php echo base_url();?>auto_debit_payment">
	      <div class="modal-header">
	        <h4 class="modal-title">Auto Debit Payment: <?php echo $property_addresss;?></h4>
	      </div>
		      <input type="hidden" name="talimar_no" value="<?php echo $modal_talimar_loan; ?>">
			  <input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">	      
	      <div class="modal-body">
	      	
	        <div class="row">
	        	<div class="col-md-6">
	        		<label>Account Type:</label>
	        		<select class="form-control" name="account_type">
	        			<?php foreach($debit_account_type as $key => $row){?>
	        				<option value="<?php echo $key;?>" <?php if($key == $auto_debit_payment[0]->account_type){echo 'selected';}?>><?php echo $row;?></option>
	        			<?php } ?>
	        		</select>
	        	</div>
	        	<div class="col-md-6">
	        		<label>Name on Account:</label>
	        		<input type="text" class="form-control" name="account_name" value="<?php echo $auto_debit_payment[0]->account_name ? $auto_debit_payment[0]->account_name : '';?>">
	        			
	        	</div>
	        </div>
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-md-6">
	        		<label>Add Bank Name:</label>
	        		<input type="text" class="form-control" name="bank_name" value="<?php echo $auto_debit_payment[0]->bank_name ? $auto_debit_payment[0]->bank_name : '';?>">
	        	</div>
	        
	        	<div class="col-md-6">
	        		<label>Account #:</label>
	        		<input type="text" class="form-control" name="account_no" value="<?php echo $auto_debit_payment[0]->account_no ? $auto_debit_payment[0]->account_no : '';?>">
	        			
	        	</div>
	        </div>
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-md-6">
	        		<label>Routing #:</label>
	        		<input type="text" class="form-control" name="routing_no" value="<?php echo $auto_debit_payment[0]->routing_no ? $auto_debit_payment[0]->routing_no : '';?>">
	        			
	        	</div>
	        	<div class="col-md-6">
	        		<label>Payment Date:</label>
	        		<select class="form-control" name="pament_date">
	        			<?php foreach($payment_due_option as $key => $row){?>
	        				<option value="<?php echo $key;?>" <?php if($key == $auto_debit_payment[0]->pament_date){echo 'selected';}?>><?php echo $row;?></option>
	        			<?php } ?>
	        		</select>
	        	</div>
	        </div>
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-md-6">
	        		<label>Payment Due Date:</label>
	        		<?php $date_due = $fetch_loan_result[0]->first_payment_date ? date('m-d-Y', strtotime($fetch_loan_result[0]->first_payment_date)) : '';?>
	        		<input type="text" class="form-control" name="payment_due_date" value="<?php echo $date_due;?>" readonly>
	        			
	        	</div>
	        	<div class="col-md-6">
	        		<label>Amount to Debit:</label>
	        		<input type="text" class="form-control" name="debit_amount" value="<?php echo $auto_debit_payment[0]->debit_amount ? '$'.number_format($auto_debit_payment[0]->debit_amount,2) : '';?>">
	        	</div>
	        </div>
	        <div class="row" style="margin-top: 10px;">
	        	<div class="col-md-6">
	        		<label>Monthly Payment Amount:</label>
	        		<input type="text" class="form-control" name="monthly_payment" value="$<?php echo number_format($aa,2);?>" readonly>
	        			
	        	</div>
	        	<div class="col-md-6">
	        		<label></label>
	        		
	        	</div>
	        </div>
	      </div>

	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="submit" name="submit" class="btn blue">Save</button>
	      </div>
	  	</form>
	    </div>
	  </div>
	</div>

	<!------------ The Auto Debit Payment Modal end -------------->
	
	
	
	<!-----------------------------forclosure modal custom-------------------------------------->
	
	
	
	
	<div class="modal fade bs-modal-lg" id="forcloser_status" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form method="POST" action = "<?php echo base_url();?>">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<!--<h4 class="modal-title">Extension Processing: #<?php// echo ''.$modal_talimar_loan; ?> </h4>-->
							<h4 class="modal-title">Foreclosure Status: <?php echo ''.$property_addresss; ?> </h4>
						</div>
						<div class="modal-body">
						
						<!--<input type="hidden" value="marketing_extentionn" name="form_typ"/>-->
						<input type="hidden" id="" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
					
						<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
					
						<div class="first_block_content">
					
						<div class="main_cc">
						<div class="col-md-12">
								
								<div class="col-md-3">
									<strong>Foreclosure #</strong>
						        </div>
								
						        <div class="col-md-3">
										<strong>Status</strong>
						       
						        </div>
								 

						        <div class="col-md-3">

						       			<strong>Reason</strong>
						        </div>

						        <div class="col-md-3">
										<strong>Phase</strong>
						       
						        </div>
								
						      
						</div>
					<?php 
					if(isset($add_forclosure_array)){
					
					$i=1;
					
					foreach($add_forclosure_array  as $ke=>$ro){
					

				 $term_query= $this->User_model->query("select foreclosuer_step,reason_for_default,foreclosuer_status from loan_servicing_default where talimar_loan = '".$ro->talimar_loan."' AND add_forclosure_id ='".$ro->id."'");
						$term_data = $term_query->result();
					$r_f_d=$term_data[0]->reason_for_default;
					$foreclosuer_statuss=$term_data[0]->foreclosuer_status;
					$foreclosuer_stepp=$term_data[0]->foreclosuer_step;
					
					
					?>

					
					<div class="row">
								<div class="form-group disclosures_lable">
								

								   <div class="col-md-3">
								 	<a  class="del_fro" title="Delete" id="<?php echo $ro->id;?>" onclick="delete_forclosure_data(this);" style="float: left;"><i class="fa fa-trash"></i></a>	
								  <a href="javascript:void(0);" id="<?php echo $ro->id;?>" onclick="open_for_popup(this)" style="text-decoration:none;"><span><h4 style="font-size:14px;">Foreclosure <?php echo $i;?></h4></span></a>
									<input type="hidden" name="for_id[]" value="<?php echo isset($ro->id) ? $ro->id :'new'; ?>">
									<input type="hidden" id="for_t_ali" name="" value="<?php echo $modal_talimar_loan; ?>">
									<input type="hidden" id="for_name" name="extension_name[]" value="<?php echo isset($ro->foreclosure_name) ? $ro->foreclosure_name:'';?>">


								</div>
								 
									<div class="col-md-3">

								  <p style="margin-top:8px;"><?php echo isset($foreclosuer_status[$foreclosuer_statuss]) ? $foreclosuer_status[$foreclosuer_statuss] :''; ?></p>

								   </div>

								    <div class="col-md-3">

									<p style="margin-top:8px;"><?php echo isset($reason_for_default[$r_f_d]) ? $reason_for_default[$r_f_d] :''; ?></p>
							 <!-- <select id="bb" name="for_box[]" onchange="change_for_slect(this)"  class="form-control">
									  <?php foreach($reason_for_default as $key => $row){
										 
										 ?>
										  <option value="<?php echo $key;?>"<?php if($key==$ro->status_option){ echo 'selected';}?>><?php echo $row;?></option>
										  <?php
											}
									   ?>
									  </select>-->
								
								  
								  </div>
								   <div class="col-md-3">

								   	<?php if($foreclosuer_statuss == '5'){ ?>
								   		<p style="margin-top:8px;margin-left: -8px;">Not Applicable</p>
								   	<?php }else{ ?>
								  		<p style="margin-top:8px;"><?php echo isset($foreclosuer_step[$foreclosuer_stepp]) ? $foreclosuer_step[$foreclosuer_stepp] :''; ?></p>
								    <?php } ?>

								   </div>
								</div>
					</div>	<?php $i++; } }else{?>
					
								<div class="row">
								<div class="form-group disclosures_lable">
								    <div class="col-md-6">
									<a  class="del_fro" title="Delete" id="new" onclick="delete_forclosure_data(this);" style="float: left;"><i class="fa fa-trash"></i></a>	
								  <a href="javascript:void(0);" id="" onclick="open_for_popup(this)" style="text-decoration:none;"><span><h4 style="font-size:14px;">Foreclosure 1</h4></span></a>
									<input type="hidden" name="for_id[]" value="new">
					             	<input type="hidden" id="for_name" name="for_name[]" value="Foreclosure 1">
					             	<input type="hidden" id="for_t_ali" name="<?php echo $modal_talimar_loan; ?>" value="">


								</div>
								  <div class="col-md-6">
								<!--
							  <select id="bb" name="for_box[]"  onchange="change_for_slect(this)" class="form-control">
									  <?php foreach($reason_for_default as $key => $row){
										 
										 ?>
										  <option value="<?php echo $key;?>"><?php echo $row;?></option>
										  <?php
											}
									   ?>
									  </select>-->
							
								  
								  </div>
								</div>
					</div>
					
					
					
					<?php }?>
							</div>
						<div class="row">
								
								<div class="form-group disclosures_lable">
								  
								 
                               <div class="col-md-4">
									<button type="button" onclick="add_for_request(this)" class="btn btn-sm btn-primary">(+) Foreclosure</button>
								  </div>
								 <div class="col-md-5">
								
								  </div>
								</div>
							</div>
							
						
						</div>
					</div>
						<div class="modal-footer">
							<!--<button type="button" class="btn default" data-dismiss="modal">Close</button>
							<button type="submit" name="form_button" value="save" class="btn blue" >Save</button>-->
						</div>
						
					</form>
				</div>
			</div>
	</div>
	
	
	
	<!--------------- Default Status MODAL POPUP start ----------------------------->
	
	
	<div class="modal fade bs" id="custom_add_foreclosure" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form method="POST" action = "<?php echo base_url();?>loan_servicing_default_status">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<!--<h4 class="modal-title">Loan Closing Checklist: #<?php //echo ''.$modal_talimar_loan; ?> </h4>-->
							<h4 class="modal-title">Foreclosure Status: <span id="for_c_name" style="text-transform:capitalize;"></span></h4>
						       <input type="hidden" name="for_ex_idd" value="" id="for_ex_idd" >
						       <input type="hidden" name="select_for" value="" id="select_for" >
						       <input type="hidden" name="forr_name" value="" id="forr_name" >
						
						
						
						</div>
						
						<div class="modal-body">
							<input type="hidden" name ="talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
							<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
							
						<!-- 	<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Activate Foreclosure:</label>  
										<div class="col-md-6">
											<select name="active_forclosure" class="form-control input-md" onchange="active_forclosure_fun(this)">
											<?php foreach($yes_no_option_verified as $key => $row){ ?>
												<option value="<?php echo $key;?>"><?php echo $row; ?></option>
											<?php } ?>
											</select> 
										</div>
								</div>
							</div>
 -->


 								<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label">Foreclosure Status: </label>  
										<div class="col-md-6">
											<select name="foreclosuer_status" class="form-control">
											<?php foreach($foreclosuer_status as $key => $row){ ?>
												<option value="<?php echo $key;?>" ><?php echo $row; ?></option>
											<?php } ?>
											</select>
										</div>
								</div>
							</div>
								<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label">Foreclosure Phase: </label>  
										<div class="col-md-6">
											<select name="foreclosuer_step" class="form-control">
											<?php foreach($foreclosuer_step as $key => $row){ ?>
												<option value="<?php echo $key;?>" ><?php echo $row; ?></option>
											<?php } ?>
											</select>
										</div>
								</div>
							</div>
								
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Reason for Default: </label>  
										<div class="col-md-6">
											<select name="reason_for_default" class="form-control input-md">
											<?php foreach($reason_for_default as $key => $row){ ?>
												<option value="<?php echo $key;?>"><?php echo $row; ?></option>
											<?php } ?>
											</select> 
										</div>
								</div>
							</div>
							<div class="row">&nbsp;</div>
						
							<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label">Foreclosure Processer: </label>  
										<div class="col-md-6">
											<select name="foreclosuer_processor" class="form-control">
											<?php foreach($foreclosuer_processor as $key => $row){ ?>
												<option value="<?php echo $key;?>"><?php echo $row; ?></option>
											<?php } ?>
											</select>
										</div>
								</div>
							</div>
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label">Contact Name: </label>  
										 <div class="col-md-6 rs" id="chosen_remove">
											<select name="contact_name" id="contact_ajx_name" class="chosen677" onchange="contact_details(this);">
											<!-- <option>Select One</option>
											<?php foreach($fetch_all_contact as $rows) { ?>
												<option value="<?php echo $rows->contact_id;?>" <?php if($rows->contact_id == $servicing_default[0]->contact_name){echo 'Selected';}?>><?php echo $rows->contact_firstname .' '. $rows->contact_middlename .' '. $rows->contact_lastname ;?></option>
											<?php } ?> -->
											</select>
										</div>
								</div>
							</div>
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label">Phone:</label>  
										<div class="col-md-6">
											<input type="text" name="c_phone" id="c_phone" class="form-control" value="" readonly >
										</div>
								</div>
							</div>
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label">E-mail:</label>  
										<div class="col-md-6">
											<input type="text" name="c_email" id="c_email" class="form-control" value="" readonly >
										</div>
								</div>
							</div>
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label">TS Number:</label>  
										<div class="col-md-6">
											<input type="text" name="ts_number" class="form-control" value="">
										</div>
								</div>
							</div>
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
									  <label class="col-md-4 control-label" for="textinput"></label>  
									  <div class="col-md-6">
										<a class="btn blue btn-block" id="fd" data-toggle="modal" href="#ts_number">Foreclosure Fees PTD</a>
								
									  </div>
								</div>
							</div>
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label">Status Update:</label>  
										<div class="col-md-6">
											<textarea type="text" name="status_update" class="form-control" rows="5"></textarea>
										</div>
								</div>
							</div>
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label">Borrower Outreach: </label>  
										<div class="col-md-4">
											<input type="text" name="borrower_outreach" class="form-control datepicker" placeholder="MM-DD-YYYY" value="">
										</div>
										<div class="col-md-4">
											
											<select name="borrower_outreach_option" class="form-control">
											<?php foreach($estimale_complete_option as $key => $row){ ?>
												<option value="<?php echo $key;?>"><?php echo $row;?></option>
											<?php } ?>
											</select>
										</div>
								</div>
							</div>
							
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label">NOD Recorded:</label>  
										<div class="col-md-4">
											<input type="text" name="nod_recorded" class="form-control datepicker" placeholder="MM-DD-YYYY" value="">
										</div>
										<div class="col-md-4">
											
											<select name="nod_recorded_option" class="form-control">
											<?php foreach($estimale_complete_option as $key => $row){ ?>
												<option value="<?php echo $key;?>"><?php echo $row;?></option>
											<?php } ?>
											</select>
										</div>
								</div>
							</div>
							
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label">NOS Recorded:</label>  
										<div class="col-md-4">
											<input type="text" name="nos_recorded" class="form-control datepicker" placeholder="MM-DD-YYYY" value="">
										</div>
										<div class="col-md-4">
											
											<select name="nos_recorded_option" class="form-control">
											<?php foreach($estimale_complete_option as $key => $row){ ?>
												<option value="<?php echo $key;?>"><?php echo $row;?></option>
											<?php } ?>
											</select>
										</div>
								</div>
							</div>
							
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label">Sale Date:</label>  
										<div class="col-md-4">
											<input type="text" name="sale_date" class="form-control datepicker" placeholder="MM-DD-YYYY" value="">
										</div>
										<div class="col-md-4">
											
											<select name="sale_date_option" class="form-control">
											<?php foreach($estimale_complete_option as $key => $row){ ?>
												<option value="<?php echo $key;?>"><?php echo $row;?></option>
											<?php } ?>
											</select>
										</div>
								</div>
							</div>
							
						</div>
						<div class="modal-footer">
							<button type="submit" name="submit" value="save" class="btn blue" >Save</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
							<!-- <a class="btn blue" href="<?php echo base_url();?>Load_data/print_default_status<?php echo $loan_id;?>">Foreclosure Update</a> -->
						</div>
					</form>
				</div>
			</div>
	</div>
	
	<!--------------- Default Status MODAL POPUP end ----------------------------->
	
<div class="modal fade in" id="ts_number" tabindex="-1" role="large" aria-hidden="true">		
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url();?>additional_forecloser">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Foreclosure Costs: <?php echo $property_addresss;?></h4>
				</div>
				<div class="modal-body page-property-taxes">
					<!----------- Hidden fields  ----------->
				<input type="hidden" name="talimar_loan" value="<?php echo $talimar_loan;?>">
				<input type="hidden" name="loan_id" value="<?php echo $loan_id;?>">
				
				<div id="append_main_promissory_note11">
				<?php if(isset($fetch_foreclosure_result))
				{
					$total_sum = 0;
					foreach($fetch_foreclosure_result as $row)
					{
						$total_sum += $row->forecloser_digit;
					?>
					<div class="row" id="main_promissory_note">
						
						<div class="col-md-11" >
						<div class="col-md-6">
							<input type="text" name="forecloser_text[]" id="forecloser_text_<?php echo $row->id;?>" class="form-control" value="<?php echo $row->forecloser_text;?>"> 
						</div>
						<div class="col-md-5">
							<input type="text" name="forecloser_digit[]" id="forecloser_digit_<?php echo $row->id;?>" class="form-control number_only amount_format" onchange="sumtheforclosuredata(this);" value="<?php echo '$'.number_format($row->forecloser_digit);?>">
						</div>
							<input type="hidden" name="hidden_id[]" value="<?php echo $row->id;?>">
						</div>	
					</div>
					
				<?php } } ?>
				</div>
				<script type="text/javascript">
					
					function sumtheforclosuredata(that){

						var allsum = 0;
						$('#main_promissory_note div.col-md-5 input[name="forecloser_digit[]"]').each(function(){
						    var allvalue = $(this).val();
						    var replaceval = allvalue.replace(/\$/g,"");
						    allsum += parseInt(replaceval);
						});
						//alert(allsum);
						$('#ts_number input#tottalSum').val('');
						$('#ts_number input#tottalSum').val('$'+allsum);
					}

				</script>
				<div class="row">
					<div class="col-md-11">
						<div class="col-md-6">					
							<button type="button" onclick="add_row_promissory_notte(this)" class="btn blue">Add Row</button>
						</div>
					</div>
				</div>
				<div class="row">					
					<div class="col-md-11">
						<div class="col-md-6">
							<label class="pull-right">Total:</label>
						</div>						
						<div class="col-md-5">
							
							<input type="text" id="tottalSum" class="form-control" value="$<?php echo number_format($total_sum);?>">
						</div>
					</div>
						
				</div>
					<!--<div class="row" id="main_promissory_note">
						
						<div class="col-md-11">
							<div class="col-md-6">
								<input type="text" name="forecloser_text[]" class="form-control">
							</div>						
							<div class="col-md-5">
								<input type="text" name="forecloser_digit[]" class="form-control number_only">
							</div>
						</div>
							
							
					</div>-->
					
				
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
					<button id="singlebutton" type="submit" name="form_button" class="btn btn-primary" value="save">Save</button>
					<a class="btn blue" href="<?php echo base_url();?>Load_data/Printforclouserdata/<?php echo str_replace('/', '', $loan_id)?>">Print</a>
				</div>
			</form>	
		</div>
		
	</div>
</div>
		<!--------------- payment gradntor MODAL POPUP start ----------------------------->
	
						
						
	<!---------------Payment g MODAL POPUP ENDS ----------------------------->
	
	
	<!--------------- Affiliated Parties MODAL POPUP start ----------------------------->
	<div class="modal fade bs-modal-lg" id="affiliated_parties" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form method="POST" action = "<?php echo base_url();?>add_affiliated_parties">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<!--<h4 class="modal-title">Loan Closing Checklist: #<?php //echo ''.$modal_talimar_loan; ?> </h4>-->
							<h4 class="modal-title">Affiliated Parties: <?php echo ''.$property_addresss; ?> </h4>
						</div>
						
						
						<div class="modal-body">
												<!----- hidden fields ------>
							<input type="hidden" name ="talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
							<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
							<input type="hidden" name="position[]" value="">
												<!----- hidden fields ------>
												
						<div id="my_main1">
							<div class="row"><h5></h5></div>
							
							<?php 
						
							if($affiliated_parties_data){
							$count= 0;
							foreach($affiliated_parties_data as $keys => $r){
								$count++;

								$affiliation_name = $affiliated_parties_data[$keys]->name ? $affiliated_parties_data[$keys]->name : '';
								$dis_loan_term = $affiliated_parties_data[$keys]->dis_loan_term ? $affiliated_parties_data[$keys]->dis_loan_term : '';

								if($count > 1){
									$hidelabel = 'style="display:none;"';
									$addmarginval = '';
								}else{
									$hidelabel = '';
									$addmarginval = 'style="margin-top: 25px;"';
								}
							
								?>
							<div id="my_main" class="avc_<?php echo $r->id;?>">
							<div class="row">

									<div class="col-md-3">
										<label <?php echo $hidelabel;?>>Name:</label>
										<select name="name[]" class="chosen7" id="affiliation_part" onchange="fetch_contact_details(this);">
										</select>
									</div>

										<input type="hidden" name="id[]" value="<?php echo $affiliated_parties_data[$keys]->id ? $affiliated_parties_data[$keys]->id : 'new';?>">

							 			<input type="hidden" name="aff_i[]" value="<?php echo $affiliated_parties_data[$keys]->name; ?>">

									<div class="col-md-2">
										<label <?php echo $hidelabel;?>>Role:</label>
										<select name="affiliation[]" class="form-control" id="select_affiliation" onchange="affiliation_value(this);">
											<?php 
												$affiliation =$affiliated_parties_data[$keys]->affiliation ? $affiliated_parties_data[$keys]->affiliation :'';
											?>
										
											<?php foreach($affiliated_option as $key => $row){ ?>
												<option value="<?php echo $key; ?>" <?php if($key == $affiliation){echo 'Selected';}?>><?php echo $row; ?></option>
											
											<?php } ?>
										</select>
									</div>
									<div class="col-md-2">
										<label <?php echo $hidelabel;?>>Disclose Loan:</label>
										<select class="form-control" name="dis_loan_term[]">
											<?php foreach($no_yes_option as $key => $row){?>
												<option value="<?php echo $key;?>" <?php if($key == $dis_loan_term){echo 'selected';}?>><?php echo $row;?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-2" id="phone_<?php echo $count;?>">
										<label <?php echo $hidelabel;?>>Phone:</label>
										<input type="text" name="phone[]" id="phone" class="form-control phone-format" value="<?php echo $affiliated_parties_data[$keys]->phone ? $affiliated_parties_data[$keys]->phone :''; ?>" readonly >
									
									</div>
									<div class="col-md-2" id="email_<?php echo $count;?>">
										<label <?php echo $hidelabel;?>>E-Mail:</label>
										<input type="email" name="email[]" id="email" class="form-control" value="<?php echo $affiliated_parties_data[$keys]->email ? $affiliated_parties_data[$keys]->email :'';?>" readonly >
									
									</div>
									<div class="col-md-1">

										<a title="Delete" class="btn btn-danger btn-xs" onclick="delete_affiliates_data('<?php echo $r->id;?>');" <?php echo $addmarginval;?>><i class="fa fa-trash"></i></a>&nbsp;<a title="Go to contact page" class="btn btn-info btn-xs" href="<?php echo base_url();?>viewcontact/<?php echo $affiliation_name;?>" <?php echo $addmarginval;?>><i class="fa fa-arrow-circle-o-right"></i></a>
									</div>
							</div>

							<!--<div class="row">
								<div class="form-group">
									<label class="col-md-12">Contact <?php echo $count;?>: &nbsp;&nbsp; <a title="Delete" onclick="delete_affiliates_data('<?php echo $r->id;?>');"><i class="fa fa-trash"></i></a>
										&nbsp; <a title="Go to contact page" href="<?php echo base_url();?>viewcontact/<?php echo $affiliation_name;?>"><i class="fa fa-arrow-circle-o-right"></i></a>

									</label>
									
								</div>
							</div>
							<div class="row" style="margin-top:10px;">
								<div class="form-group">
								  <label class="col-md-4">Name:</label> 
									<div class="col-md-6">
									
										<select name="name[]" class="chosen7" id="affiliation_part" onchange="fetch_contact_details(this);">
								
									
										</select>
									
									</div>
									
								</div>
							</div>
							<div class="row">&nbsp;
							 <input type="hidden" name="id[]" value="<?php echo $affiliated_parties_data[$keys]->id ? $affiliated_parties_data[$keys]->id : 'new';?>">

							 <input type="hidden" name="aff_i[]" value="<?php echo $affiliated_parties_data[$keys]->name; ?>">

							</div>
							<div class="row">
								<div class="form-group">
								  <label class="col-md-4">Role:</label> 
									<div class="col-md-6">
									<?php 
											$affiliation =$affiliated_parties_data[$keys]->affiliation ? $affiliated_parties_data[$keys]->affiliation :'';
										?>
									
										
										<select name="affiliation[]" class="form-control" id="select_affiliation" onchange="affiliation_value(this);">
										
											<?php foreach($affiliated_option as $key => $row){ ?>
												<option value="<?php echo $key; ?>" <?php if($key == $affiliation){echo 'Selected';}?>><?php echo $row; ?></option>
											
											<?php } ?>
										</select>
									
									</div>
								</div>
							</div>
							
							<div class="row" style="display:none" id="other_textbox" style="margin-top:10px;">
								<div class="form-group">
								  <label class="col-md-4">Other Textbox:</label> 
									<div class="col-md-6">
										<textarea name="affiliation_text[]" type="text" class="form-control" rows="3"></textarea>
									</div>
								</div>
							</div>
							
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
								  <label class="col-md-4">Phone:</label> 
									<div class="col-md-6">
										<input type="text" name="phone[]" id="phone" class="form-control phone-format" value="<?php echo $affiliated_parties_data[$keys]->phone ? $affiliated_parties_data[$keys]->phone :''; ?>" readonly >
									
									</div>
								</div>
							</div>
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
								  <label class="col-md-4">E-mail:</label> 
									<div class="col-md-6">
										<input type="email" name="email[]" id="email" class="form-control" value="<?php echo $affiliated_parties_data[$keys]->email ? $affiliated_parties_data[$keys]->email :'';?>" readonly >
									
									</div>
								</div>
							</div>

							<br><hr>-->
							<br>
							</div>
				
						<?php } }else{    ?>
										
							<div id="my_main" class="avc">
							<div class="row">

								<input type="hidden" name="id[]" value="new">
								<input type="hidden" name="aff_i[]" value="">

								<div class="col-md-3">
									<label>Name:</label> 
									<select name="name[]"  id="affiliation_part" class="chosen7" onchange="fetch_contact_details_append(this);">
									</select>
								</div>
								<div class="col-md-2">
									<label>Role:</label> 
									<select name="affiliation[]" class="form-control" id="select_affiliation" onchange="affiliation_value(this);">
											<?php foreach($affiliated_option as $key => $row){ ?>
												<option value="<?php echo $key; ?>"><?php echo $row; ?></option>
											<?php } ?>
									</select>
								</div>
								<div class="col-md-2">
										<label>Disclose Loan:</label>
										<select class="form-control" name="dis_loan_term[]">
											<?php foreach($no_yes_option as $key => $row){?>
												<option value="<?php echo $key;?>"><?php echo $row;?></option>
											<?php } ?>
										</select>
									</div>
								<div class="col-md-2">
									<label>Phone:</label> 
									<input type="text" name="phone[]" id="phonee" class="form-control phone-format" value="" readonly >
								</div>
								<div class="col-md-2">
									<label>E-Mail:</label> 
									<input type="email" name="email[]" id="emaill" class="form-control" value="" readonly >
								</div>
								<div class="col-md-1"></div>

							</div><br>

							<!--<div class="row">
								<div class="form-group">
								  <label class="col-md-4">Name:</label> 
									<div class="col-md-6">
	
										<select name="name[]"  id="affiliation_part" class="chosen7" onchange="fetch_contact_details_append(this);">
										
										</select>
									
									</div>
								</div>
							</div>
							<div class="row">&nbsp;
								<input type="hidden" name="id[]" value="new">
								 <input type="hidden" name="aff_i[]" value="">	
							</div>
							<div class="row">
								<div class="form-group">
								  <label class="col-md-4">Role:</label> 
									<div class="col-md-6">
									
									
									
										
										<select name="affiliation[]" class="form-control" id="select_affiliation" onchange="affiliation_value(this);">
										
											<?php foreach($affiliated_option as $key => $row){ ?>
												<option value="<?php echo $key; ?>"><?php echo $row; ?></option>
											
											<?php } ?>
										</select>
									
									</div>
								</div>
							</div>
							
							<div class="row" style="display:none" id="other_textbox" style="margin-top:10px;">
								<div class="form-group">
								  <label class="col-md-4">Other Textbox:</label> 
									<div class="col-md-6">
										<textarea name="affiliation_text[]" type="text" class="form-control" rows="3"></textarea>
									</div>
								</div>
							</div>
							
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
								  <label class="col-md-4">Phone:</label> 
									<div class="col-md-6">
										<input type="text" name="phone[]" id="phonee" class="form-control phone-format" value="" readonly >
									
									</div>
								</div>
							</div>
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
								  <label class="col-md-4">E-mail:</label> 
									<div class="col-md-6">
										<input type="email" name="email[]" id="emaill" class="form-control" value="" readonly >
									
									</div>
								</div>
							</div>
							<br><hr>-->	
						</div>
										
						<?php } ?>
					</div>
				
					</div>
				
						<div class="modal-footer">
							<button type="button"  onclick="add_aff(this)"  name="submit" value="save" class="btn blue pull-left" >Add</button>

							<button type="submit" name="submit" value="save" class="btn blue" >Save</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
	</div>
						
						
	<!--------------- Affiliated Parties MODAL POPUP ENDS ----------------------------->
	
	
	<!--------------- LOAN SERVICING MODAL POPUP ENDS ----------------------------->
	
	<!------------ Marketing Modal Starts ----------->
	<?php
	$closing_checklist_ct = 1;
	$checklist_Complete = array(1058,135,1056,1055,1037,138,1068);
	?>
	<div class="modal fade bs-modal-lg" id="loan_closing_checklist" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form method="POST" action = "<?php echo base_url();?>add_checklist_value">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<!--<h4 class="modal-title">Loan Closing Checklist: #<?php //echo ''.$modal_talimar_loan; ?> </h4>-->
							<h4 class="modal-title">Loan Closing Checklist: <?php echo ''.$property_addresss; ?> </h4>
						</div>
						<div class="modal-body">
							
							<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
							<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
							
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" id="checklist_table">
									
								
								<thead>
									<tr>
										<!--<th style="text-align:left !important;"></th>-->
										<th style="text-align:left !important;">Completed</th>
										<th style="text-align:left !important;">Not Applicable</th>
										<th style="text-align:left !important;">Description</th>
									</tr>
								</thead>	
								<tbody>	
										<?php
										$checklist_complete = '';
										if(isset($fetch_all_input_datas['loan_servicing_checklist']['checklist_complete']))
										{
											$checklist_complete = ($fetch_all_input_datas['loan_servicing_checklist']['checklist_complete'] == 1) ? 'checked' : '';
										}
										?>
									
									<?php /**************New Section***************/ ?>
									<tr><th colspan="3" class="tbl_heading_bak">Loan Origination</th></tr>
									
										<?php 
										foreach ($loan_servicing_checklist as $key => $value) 
											{
								
						  					$new_loan_servicing_checklist[$key][]= $value;
											
											}

											$array =array(101,102,2000,145,1051,1071,1074,1058);
											if(isset($array))
											{

											 	foreach($array as $number){
												
													foreach($new_loan_servicing_checklist[$number] as $row)
													{
														
														$servicing_array_final[$number] = $row;
													}
												}
											}


													foreach($servicing_array_final as $ke=> $val)
													    { 
																	
															foreach($servicing_checklist as $row){ 
																
																$CompleteCheck_b_class = '';
																$CompleteCheck_class = '';
																if(in_array($row->hud, $checklist_Complete)){
																	$CompleteCheck_b_class = 'ct_font_bold';
																	$CompleteCheck_class = 'complete_checklist_id';
																}

															  if($ke == $row->hud){
															  		
																//if($row->type == 'pre wire'){
															
																$chldd = '';
																$chldd1 = '';
																
																if($row->checklist == 1){
																	$chldd = '';
																	$chldd1 = 'disabled';
																}
																
																if($row->not_applicable == 1){
																	$chldd = 'disabled';
																	$chldd1 = '';
																}
																
																// echo 'chldd- '.$chldd.' == chldd1- '.$chldd1;;
																// echo '<br>';
													  
													         if($row->hud=='115' && ($dummy['req']=='2' || $dummy['req']==''))
															   {
																
																
															?>	

							                                   
															<tr>
																<input type="hidden" name="id_<?php echo $row->id;?>" value="<?php echo $row->id;?>">
																
																<!--<td style="text-align:center !important;"><?php echo $row->hud;?></td>-->
																
																<td style="text-align:center !important;"><input type="checkbox" id="onlyone_<?php echo $row->id;?>" onchange="change_checklist_value(this,'<?php echo $row->id;?>')" class="<?php echo $CompleteCheck_class; ?>" <?php if($row->checklist == 1){echo 'checked';}?> <?php echo $chldd; ?>>
																<input type="hidden" name="check_<?php echo $row->id;?>" id="checklist" value="<?php echo $row->checklist;?>" ></td>
																
																<td style="text-align:center !important;"><input type="checkbox" id="onlyone1_<?php echo $row->id;?>" onchange="change_not_applicable_val(this,'<?php echo $row->id;?>')" <?php if($row->not_applicable == 1){echo'checked';}?> <?php echo $chldd1; ?> class="<?php echo $CompleteCheck_class; ?>">
																<input type="hidden" name="not_applicable_<?php echo $row->id;?>" id="not_applicable_val" value="<?php echo $row->not_applicable;?>"  ></td>
																
																<td class="<?php echo $CompleteCheck_b_class; ?>" style="text-align:left;">
																	<?php echo $loan_servicing_checklist[$row->hud] ? $loan_servicing_checklist[$row->hud] : $row->items;?>
																		
																	</td>
															</tr>
															
															
															
															<?php }else{
																	
																	
																	//echo 'chldd1- '.$chldd1;

																	//COMPLETE COLUMN CHECKBOXES....
																	if($row->checklist == 1 || $row->not_applicable == 1 || $row->hud == '135'){
																		$disabled_com[] = '1';
																		
																	}else{
																		$disabled_com[] = '2';
																		
																	}
																		
																	if(in_array(2, $disabled_com)){

																		if($row->hud == '135'){

																			$disabled_135 = 'disabled';
																		}else{
																			$disabled_135 = '';
																		}
																	}else{

																		$disabled_135 = '';
																	}
																	
																	
																	
																?>
																	
															<tr>
																<input type="hidden" name="id_<?php echo $row->id;?>" value="<?php echo $row->id;?>">
										
																
																<td style="text-align:center !important;"><input type="checkbox" id="onlyone_<?php echo $row->id;?>" onchange="change_checklist_value(this,'<?php echo $row->id;?>')" <?php if($row->checklist == 1){echo 'checked';}?> <?php echo $chldd; ?> <?php echo $disabled_135; ?> class="<?php echo $CompleteCheck_class; ?>">
																<input type="hidden" name="check_<?php echo $row->id;?>" id="checklist" value="<?php echo $row->checklist;?>" /></td>
																
																<td style="text-align:center !important;"><input type="checkbox" id="onlyone1_<?php echo $row->id;?>" onchange="change_not_applicable_val(this,'<?php echo $row->id;?>')" <?php if($row->not_applicable == 1){echo'checked';} ?> <?php echo $chldd1; ?> <?php echo $disabled_135; ?> class="<?php echo $CompleteCheck_class; ?>"/>
																<input type="hidden" name="not_applicable_<?php echo $row->id;?>" id="not_applicable_val" value="<?php echo $row->not_applicable;?>" /></td>
																
																<td class="<?php echo $CompleteCheck_b_class; ?>" style="text-align:left;"><?php echo $loan_servicing_checklist[$row->hud] ? $loan_servicing_checklist[$row->hud]: $row->items;?></td>
															</tr>		
																
															
																	
																	
															<?php  } 
															    // } 
															
															  } ?>
															  
															  
															
														   <?php }
														 } 
														?>
															
															<tr id="app_row_<?php echo $closing_checklist_ct ; ?>">
																<th style="text-align:center !important;" colspan="4"><a onclick="add_checklist_items(this);" id="<?php echo ($closing_checklist_ct); ?>" ><i class="fa fa-plus" aria-hidden="true"></i></a></th>
															</tr>
															<?php $closing_checklist_ct++; ?>

															<?php $user_role = $this->session->userdata('user_role'); ?>

															



														<!--  <tr> --><!--<th colspan="">Item #</th>--><!-- <th colspan="3">Post Wire Checklist</th></tr> -->
													  
									<?php /*************New Section*****************/ ?>
									
									<tr><th colspan="3" class="tbl_heading_bak">Underwriting</th></tr>
									
				<?php 
				
				$servicing_array_final = array();
				foreach ($loan_servicing_checklist as $key => $value) 
					{
		
  					$new_loan_servicing_checklist[$key][]= $value;
					
					}
					
					//$array = array(101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,124,125,126,127,128,129,133,131,132,134,130);
					//$array =array(101,127,145,108,102,103,104,105,140,139,134,143,117,144,128,147,125,1011,131,130,135); OLD
					$array =array(127,1050,1044,108,1072,1061,1062,1063,1065,128,1066,1011,125,131,130,135);
					if(isset($array))
					{

					 	foreach($array as $number){
						
							foreach($new_loan_servicing_checklist[$number] as $row)
							{
								if(in_array($number, $array)){
									$servicing_array_final[$number] = $row;
								}
							}
						}
					}
						

							foreach($servicing_array_final as $ke=> $val)
							    { 
											
									foreach($servicing_checklist as $row){ 
										$CompleteCheck_b_class = '';
										$CompleteCheck_class = '';
										if(in_array($row->hud, $checklist_Complete)){
										   $CompleteCheck_b_class = 'ct_font_bold';
										   $CompleteCheck_class = 'complete_checklist_id';
										}
									  if($ke == $row->hud){
									  		
										//if($row->type == 'pre wire'){
									
										$chldd = '';
										$chldd1 = '';
										
										if($row->checklist == 1){
											$chldd = '';
											$chldd1 = 'disabled';
										}
										
										if($row->not_applicable == 1){
											$chldd = 'disabled';
											$chldd1 = '';
										}
										
										// echo 'chldd- '.$chldd.' == chldd1- '.$chldd1;;
										// echo '<br>';
							  
							         if($row->hud=='115' && ($dummy['req']=='2' || $dummy['req']==''))
									   {
										
										
									?>	

	                                   
									<tr>
										<input type="hidden" name="id_<?php echo $row->id;?>" value="<?php echo $row->id;?>">
										
										<!--<td style="text-align:center !important;"><?php echo $row->hud;?></td>-->
										
										<td style="text-align:center !important;"><input type="checkbox" id="onlyone_<?php echo $row->id;?>" onchange="change_checklist_value(this,'<?php echo $row->id;?>')" <?php if($row->checklist == 1){echo 'checked';}?> <?php echo $chldd; ?> class="<?php echo $CompleteCheck_class; ?>">
										<input type="hidden" name="check_<?php echo $row->id;?>" id="checklist" value="<?php echo $row->checklist;?>" ></td>
										
										<td style="text-align:center !important;"><input type="checkbox" id="onlyone1_<?php echo $row->id;?>" onchange="change_not_applicable_val(this,'<?php echo $row->id;?>')" <?php if($row->not_applicable == 1){echo'checked';}?> <?php echo $chldd1; ?> class="<?php echo $CompleteCheck_class; ?>">
										<input type="hidden" name="not_applicable_<?php echo $row->id;?>" id="not_applicable_val" value="<?php echo $row->not_applicable;?>"  ></td>
										
										<td style="text-align:left;" class="<?php echo $CompleteCheck_b_class; ?>"><?php echo $loan_servicing_checklist[$row->hud] ? $loan_servicing_checklist[$row->hud] : $row->items;?></td>
									</tr>
									
									
									
									<?php }else{
											
											
											//echo 'chldd1- '.$chldd1;

											//COMPLETE COLUMN CHECKBOXES....
											if($row->checklist == 1 || $row->not_applicable == 1 || $row->hud == '135'){
												$disabled_com[] = '1';
												
											}else{
												$disabled_com[] = '2';
												
											}
												
											if(in_array(2, $disabled_com)){

												if($row->hud == '135'){

													$disabled_135 = ''; //disabled
												}else{
													$disabled_135 = '';
												}
											}else{

												$disabled_135 = '';
											}
											
											
											
										?>
											
									<tr>
										<input type="hidden" name="id_<?php echo $row->id;?>" value="<?php echo $row->id;?>">
				
										
										<td style="text-align:center !important;"><input type="checkbox" id="onlyone_<?php echo $row->id;?>" onchange="change_checklist_value(this,'<?php echo $row->id;?>')" <?php if($row->checklist == 1){echo 'checked';}?> <?php echo $chldd; ?> <?php echo $disabled_135; ?> class="<?php echo $CompleteCheck_class; ?>">
										<input type="hidden" name="check_<?php echo $row->id;?>" id="checklist" value="<?php echo $row->checklist;?>" /></td>
										
										<td style="text-align:center !important;"><input type="checkbox" id="onlyone1_<?php echo $row->id;?>" onchange="change_not_applicable_val(this,'<?php echo $row->id;?>')" <?php if($row->not_applicable == 1){echo'checked';} ?> <?php echo $chldd1; ?> <?php echo $disabled_135; ?> class="<?php echo $CompleteCheck_class; ?>" />
										<input type="hidden" name="not_applicable_<?php echo $row->id;?>" id="not_applicable_val" value="<?php echo $row->not_applicable;?>" /></td>
										
										<td class="<?php echo $CompleteCheck_b_class; ?>" style="text-align:left;"><?php echo $loan_servicing_checklist[$row->hud] ? $loan_servicing_checklist[$row->hud]: $row->items;?></td>
									</tr>		
										
									
											
											
									<?php  } 
									    // } 
									
									  } ?>
									  
									  
									
								   <?php }
								 } 
								?>
									
									<tr id="app_row_<?php echo $closing_checklist_ct ; ?>">
										<th style="text-align:center !important;" colspan="4"><a onclick="add_checklist_items(this);" id="<?php echo ($closing_checklist_ct); ?>" ><i class="fa fa-plus" aria-hidden="true"></i></a></th>
									</tr>
									<?php $closing_checklist_ct++; ?>
									<?php $user_role = $this->session->userdata('user_role'); ?>

									



								<!--  <tr> --><!--<th colspan="">Item #</th>--><!-- <th colspan="3">Post Wire Checklist</th></tr> -->
							  <tr><th colspan="3" class="tbl_heading_bak">Investor Relations</th></tr> 
									
				<?php 

					foreach ($loan_servicing_checklist as $k => $v) 
					{
		
  					$post_new_loan_servicing_checklist[$k][]= $v;
					
					}
					//$post_array = array(1002,1003,1025,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1026,1017,1018,1019,1023,1024);
					//$post_array =array(141,142,111,113,114,116,124,126,132,1003,1009,1009,1024,136); OLD
					$post_array =array(1047,141,142,111,1060,116,1054,126,132,1003,1009,1024,1056);
					if(isset($post_array))
					{

					 	foreach($post_array as $num){
						
							foreach($post_new_loan_servicing_checklist[$num] as $r)
							{
								
								$post_servicing_array_final[$num] = $r;
							}
						}
					}



	                      foreach($post_servicing_array_final as $kee => $valu){ 
								
								foreach($servicing_checklist as $row){ 

										$CompleteCheck_b_class = '';
										$CompleteCheck_class = '';
										if(in_array($row->hud, $checklist_Complete)){
										   $CompleteCheck_b_class = 'ct_font_bold';
										   $CompleteCheck_class = 'complete_checklist_id';
										}

									   if($kee == $row->hud){
										   
										    $chlddir = '';
											$chlddir1 = '';
											
											if($row->checklist == 1){
												$chlddir = '';
												$chlddir1 = 'disabled';
											}
											
											if($row->not_applicable == 1){
												$chlddir = 'disabled';
												$chlddir1 = '';
											}

									   		//COMPLETE COLUMN CHECKBOXES....
											if($row->checklist == 1 || $row->not_applicable == 1 || $row->hud == '136'){
												$disabled_com1[] = '1';
												
											}else{
												$disabled_com1[] = '2';
												
											}
												
											if(in_array(2, $disabled_com1)){

												if($row->hud == '136'){

													$disabled_136 = 'disabled';
												}else{
													$disabled_136 = '';
												}
											}else{

												$disabled_136 = '';
											}
											
										?>
											
										<tr>
											<input type="hidden" name="id_<?php echo $row->id;?>" value="<?php echo $row->id;?>">
											
											<!--<td style="text-align:center !important;"><?php echo $row->hud;?></td>-->
											
											<td style="text-align:center !important;"><input type="checkbox" id="onlyone_<?php echo $row->id;?>" onchange="change_checklist_value(this,'<?php echo $row->id;?>')" <?php if($row->checklist == 1){echo 'checked';}?> <?php echo $chlddir;?> <?php echo $disabled_136;?> class="<?php echo $CompleteCheck_class; ?>">
											<input type="hidden" name="check_<?php echo $row->id;?>" id="checklist" value="<?php echo $row->checklist;?>"/></td>
											
											<td style="text-align:center !important;"><input type="checkbox" id="onlyone1_<?php echo $row->id;?>" onchange="change_not_applicable_val(this,'<?php echo $row->id;?>')" <?php if($row->not_applicable == 1){echo'checked';}?> <?php echo $chlddir1;?> <?php echo $disabled_136;?> class="<?php echo $CompleteCheck_class; ?>"/>
											<input type="hidden" name="not_applicable_<?php echo $row->id;?>" id="not_applicable_val" value="<?php echo $row->not_applicable;?>"/></td>
											
											<td class="<?php echo $CompleteCheck_b_class; ?>" style="text-align:left;"><?php echo $loan_servicing_checklist[$row->hud] ? $loan_servicing_checklist[$row->hud] : $row->items;?></td>
										</tr>	
											
								<?php } } }
								//} ?>
									
									<tr id="app_row_<?php echo $closing_checklist_ct; ?>">
										<th style="text-align:center !important;" colspan="4"><a onclick="add_checklist_items(this);" id="<?php echo $closing_checklist_ct; ?>" ><i class="fa fa-plus" aria-hidden="true"></i></a></th>
									</tr>
									<?php $closing_checklist_ct++; ?>

  							<tr><th colspan="3" class="tbl_heading_bak">Loan Closing</th></tr> 
  							 
									
				<?php 

					foreach ($loan_servicing_checklist as $kk => $vv) 
					{
		
  					$post_new_loan_servicing_checklists[$kk][]= $vv;
					
					}
					//$post_array = array(1002,1003,1025,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1026,1017,1018,1019,1023,1024);
					//$post_arrayy =array(106,109,1039,110,133,112,115,129,113,1030,1002,1006,137); OLD
					$post_arrayy =array(106,1059,1073,109,1039,1078,110,1069,112,1082,115,129,1030,1002,1005,1006,1055);
					if(isset($post_arrayy))
					{

					 	foreach($post_arrayy as $numm){
						
							foreach($post_new_loan_servicing_checklists[$numm] as $rr)
							{
								
								$post_servicing_array_finals[$numm] = $rr;
							}
						}
					}


	                      foreach($post_servicing_array_finals as $kee => $valu){ 
								
								foreach($servicing_checklist as $row){ 
										$CompleteCheck_b_class = '';
										$CompleteCheck_class = '';
										if(in_array($row->hud, $checklist_Complete)){
										   $CompleteCheck_b_class = 'ct_font_bold';
										   $CompleteCheck_class = 'complete_checklist_id';
										}

									   if($kee == $row->hud){
										   
										   $chlddlc = '';
											$chlddlc1 = '';
											
											if($row->checklist == 1){
												$chlddlc = '';
												$chlddlc1 = 'disabled';
											}
											
											if($row->not_applicable == 1){
												$chlddlc = 'disabled';
												$chlddlc1 = '';
											}

									   		//COMPLETE COLUMN CHECKBOXES....
											if($row->checklist == 1 || $row->not_applicable == 1 || $row->hud == '137'){
												$disabled_com2[] = '1';
												
											}else{
												$disabled_com2[] = '2';
												
											}
												
											if(in_array(2, $disabled_com2)){

												if($row->hud == '137'){

													$disabled_137 = 'disabled';
												}else{
													$disabled_137 = '';
												}
											}else{

												$disabled_137 = '';
											}

											//disabled for user
											if($row->hud == '1014' && $this->session->userdata('user_role') == '1'){

												$disabled_137 = 'disabled';
											}else{
												$disabled_137 = '';
											}
											
									//if($row->type == 'post wire' && $row->hud != '1001'){ ?>
											
										<tr>
											<input type="hidden" name="id_<?php echo $row->id;?>" value="<?php echo $row->id;?>">
											
											<!--<td style="text-align:center !important;"><?php echo $row->hud;?></td>-->
											
											<td style="text-align:center !important;"><input type="checkbox" id="onlyone_<?php echo $row->id;?>" onchange="change_checklist_value(this,'<?php echo $row->id;?>')" <?php if($row->checklist == 1){echo 'checked';}?> <?php echo $chlddlc;?> <?php echo $disabled_137;?> class="<?php echo $CompleteCheck_class; ?>">
											<input type="hidden" name="check_<?php echo $row->id;?>" id="checklist" value="<?php echo $row->checklist;?>"/></td>
											
											<td style="text-align:center !important;"><input type="checkbox" id="onlyone1_<?php echo $row->id;?>" onchange="change_not_applicable_val(this,'<?php echo $row->id;?>')" <?php if($row->not_applicable == 1){echo'checked';}?> <?php echo $chlddlc1;?> <?php echo $disabled_137;?> class="<?php echo $CompleteCheck_class; ?>"/>
											<input type="hidden" name="not_applicable_<?php echo $row->id;?>" id="not_applicable_val" value="<?php echo $row->not_applicable;?>"/></td>
											
											<td class="<?php echo $CompleteCheck_b_class; ?>" style="text-align:left;"><?php echo $loan_servicing_checklist[$row->hud] ? $loan_servicing_checklist[$row->hud] : $row->items;?></td>
										</tr>	
											
								<?php } } }
								//} ?>
									
									<tr id="app_row_<?php echo $closing_checklist_ct; ?>">
										<th style="text-align:center !important;" colspan="4"><a onclick="add_checklist_items(this);" id="<?php echo $closing_checklist_ct; ?>" ><i class="fa fa-plus" aria-hidden="true"></i></a></th>
									</tr>
									<?php $closing_checklist_ct++; ?>
						<tr><th colspan="3" class="tbl_heading_bak">Post Loan Closing</th></tr>

						<?php 

							foreach ($loan_servicing_checklist as $kk => $vv) 
							{
				
		  						$post_new_loan_servicing_checklistspp[$kk][]= $vv;
							
							}
							
							//$post_arrayypp =array(1005,1036,1040,1004,1043,1014,1012,1034,1037); OLD
							$post_arrayypp =array(1036,1076,1004,1043,1079,1067,1077,1080,1014,1081,1021,1034,1037);
							if(isset($post_arrayypp))
							{

							 	foreach($post_arrayypp as $numm){
								
									foreach($post_new_loan_servicing_checklistspp[$numm] as $rr)
									{
										
										$post_servicing_array_finalspp[$numm] = $rr;
									}
								}
							}


							foreach($post_servicing_array_finalspp as $kee => $valu){ 
								
								foreach($servicing_checklist as $row){ 

										$CompleteCheck_b_class = '';
										$CompleteCheck_class = '';
										if(in_array($row->hud, $checklist_Complete)){
										   $CompleteCheck_b_class = 'ct_font_bold';
										   $CompleteCheck_class = 'complete_checklist_id';
										}
											

									   if($kee == $row->hud){
										   
										   $chlddlcpp = '';
											$chlddlc1p = '';
											
											if($row->checklist == 1){
												$chlddlcpp = '';
												$chlddlc1p = 'disabled';
											}
											
											if($row->not_applicable == 1){
												$chlddlcpp = 'disabled';
												$chlddlc1p = '';
											}

									   		//COMPLETE COLUMN CHECKBOXES....
											if($row->checklist == 1 || $row->not_applicable == 1 || $row->hud == '1037'){
												$disabled_com2p[] = '1';
												
											}else{
												$disabled_com2p[] = '2';
												
											}
												
											if(in_array(2, $disabled_com2p)){

												if($row->hud == '1037'){

													$disabled_1037 = ''; //disabled
												}else{

													$disabled_1037 = '';
												
												}
											}else{

												$disabled_1037 = '';
											}

										?>
											
										<tr>
											<input type="hidden" name="id_<?php echo $row->id;?>" value="<?php echo $row->id;?>">
											
											<!--<td style="text-align:center !important;"><?php echo $row->hud;?></td>-->
											
											<td style="text-align:center !important;"><input type="checkbox" id="onlyone_<?php echo $row->id;?>" onchange="change_checklist_value(this,'<?php echo $row->id;?>')" <?php if($row->checklist == 1){echo 'checked';}?> <?php echo $chlddlcpp;?> <?php echo $disabled_1037;?> class="<?php echo $CompleteCheck_class; ?>">
											<input type="hidden" name="check_<?php echo $row->id;?>" id="checklist" value="<?php echo $row->checklist;?>"/></td>
											
											<td style="text-align:center !important;"><input type="checkbox" id="onlyone1_<?php echo $row->id;?>" onchange="change_not_applicable_val(this,'<?php echo $row->id;?>')" <?php if($row->not_applicable == 1){echo'checked';}?> <?php echo $chlddlc1p;?> <?php echo $disabled_1037;?> class="<?php echo $CompleteCheck_class; ?>" />
											<input type="hidden" name="not_applicable_<?php echo $row->id;?>" id="not_applicable_val" value="<?php echo $row->not_applicable;?>"/></td>
											
											<td class="<?php echo $CompleteCheck_b_class; ?>" style="text-align:left;"><?php echo $loan_servicing_checklist[$row->hud] ? $loan_servicing_checklist[$row->hud] : $row->items;?></td>
										</tr>	
											
								<?php } } } ?>
									
									<tr id="app_row_<?php echo $closing_checklist_ct; ?>">
										<th style="text-align:center !important;" colspan="4"><a onclick="add_checklist_items(this);" id="<?php echo $closing_checklist_ct; ?>" ><i class="fa fa-plus" aria-hidden="true"></i></a></th>
									</tr>
									<?php $closing_checklist_ct++; ?>
									<tr><th colspan="3" class="tbl_heading_bak">Marketing</th></tr>
									<?php 
									foreach ($loan_servicing_checklist as $kkk => $vvv){						
				  						$post_new_loan_servicing_checklistsss[$kkk][]= $vvv;									
									}
									$post_arrayyy =array(1025,1032,1038,1017,1068);
									if(isset($post_arrayyy)){
									 	foreach($post_arrayyy as $nummm){
											foreach($post_new_loan_servicing_checklistsss[$nummm] as $rrr){												
												$post_servicing_array_finalssspp[$nummm] = $rrr;
											}
										}
									}
									foreach($post_servicing_array_finalssspp as $kee => $valu){ 								
										foreach($servicing_checklist as $row){ 
											$CompleteCheck_b_class = '';
											$CompleteCheck_class = '';
											if(in_array($row->hud, $checklist_Complete)){
											   $CompleteCheck_b_class = 'ct_font_bold';
											   $CompleteCheck_class = 'complete_checklist_id';
											}
											if($kee == $row->hud){
										   
												   	$chlddls = '';
													$chlddls1 = '';
													if($row->checklist == 1){
														$chlddls = '';
														$chlddls1 = 'disabled';
													}													
													if($row->not_applicable == 1){
														$chlddls = 'disabled';
														$chlddls1 = '';
													}
											   		//COMPLETE COLUMN CHECKBOXES....
													if($row->checklist == 1 || $row->not_applicable == 1 || $row->hud == '1068'){
														$disabled_com3[] = '1';														
													}else{
														$disabled_com3[] = '2';														
													}														
													if(in_array(2, $disabled_com3)){
														if($row->hud == '1068'){
															$disabled_1068 = '';
														}else{
															$disabled_1068 = '';
														}
													}else{

														$disabled_1068 = '';
													}
													?>
													<tr>
														<input type="hidden" name="id_<?php echo $row->id;?>" value="<?php echo $row->id;?>">
														<td style="text-align:center !important;"><input type="checkbox" id="onlyone_<?php echo $row->id;?>" onchange="change_checklist_value(this,'<?php echo $row->id;?>')" <?php if($row->checklist == 1){echo 'checked';}?> <?php echo $chlddls;?> <?php echo $disabled_1068;?> class="<?php echo $CompleteCheck_class; ?>">
														<input type="hidden" name="check_<?php echo $row->id;?>" id="checklist" value="<?php echo $row->checklist;?>"/></td>
														
														<td style="text-align:center !important;"><input type="checkbox" id="onlyone1_<?php echo $row->id;?>" onchange="change_not_applicable_val(this,'<?php echo $row->id;?>')" <?php if($row->not_applicable == 1){echo'checked';}?> <?php echo $chlddls1;?> <?php echo $disabled_1068;?> class="<?php echo $CompleteCheck_class; ?>"/>
														<input type="hidden" name="not_applicable_<?php echo $row->id;?>" id="not_applicable_val" value="<?php echo $row->not_applicable;?>"/></td>
														
														<td class="<?php echo $CompleteCheck_b_class; ?>" style="text-align:left;"><?php echo $loan_servicing_checklist[$row->hud] ? $loan_servicing_checklist[$row->hud] : $row->items;?></td>
													</tr>
													<?php	
											}
										}
									}
									?>
 						<tr><th colspan="3" class="tbl_heading_bak">Loan Servicing</th></tr> 
									
				<?php 

					foreach ($loan_servicing_checklist as $kkk => $vvv) 
					{
		
  					$post_new_loan_servicing_checklistsss[$kkk][]= $vvv;
					
					}
					//$post_array = array(1002,1003,1025,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1026,1017,1018,1019,1023,1024);
						//$post_arrayyy =array(1008,1041,1019,1013,1007,1042,1029,1010,1035,1016,1026,1015,1023,1031,146,138); OLD
					$post_arrayyy =array(1070,1008,1041,1048,147,1013,1007,1042,1029,1010,1035,1016,146,1026,1049,1015,138);
					if(isset($post_arrayyy))
					{

					 	foreach($post_arrayyy as $nummm){
						
							foreach($post_new_loan_servicing_checklistsss[$nummm] as $rrr)
							{
								
								$post_servicing_array_finalsss[$nummm] = $rrr;
							}
						}
					}

	                      foreach($post_servicing_array_finalsss as $kee => $valu){ 
								
								foreach($servicing_checklist as $row){ 

										$CompleteCheck_b_class = '';
										$CompleteCheck_class = '';
										if(in_array($row->hud, $checklist_Complete)){
										   $CompleteCheck_b_class = 'ct_font_bold';
										   $CompleteCheck_class = 'complete_checklist_id';
										}

									   if($kee == $row->hud){
										   
										   $chlddls = '';
											$chlddls1 = '';
											
											if($row->checklist == 1){
												$chlddls = '';
												$chlddls1 = 'disabled';
											}
											
											if($row->not_applicable == 1){
												$chlddls = 'disabled';
												$chlddls1 = '';
											}

									   		//COMPLETE COLUMN CHECKBOXES....
											if($row->checklist == 1 || $row->not_applicable == 1 || $row->hud == '138'){
												$disabled_com3[] = '1';
												
											}else{
												$disabled_com3[] = '2';
												
											}
												
											if(in_array(2, $disabled_com3)){

												if($row->hud == '138'){

													$disabled_138 = '';
												}else{
													$disabled_138 = '';
												}
											}else{

												$disabled_138 = '';
											}
											
										?>
											
										<tr>
											<input type="hidden" name="id_<?php echo $row->id;?>" value="<?php echo $row->id;?>">
											
											<!--<td style="text-align:center !important;"><?php echo $row->hud;?></td>-->
											
											<td style="text-align:center !important;"><input type="checkbox" id="onlyone_<?php echo $row->id;?>" onchange="change_checklist_value(this,'<?php echo $row->id;?>')" <?php if($row->checklist == 1){echo 'checked';}?> <?php echo $chlddls;?> <?php echo $disabled_138;?> class="<?php echo $CompleteCheck_class; ?>">
											<input type="hidden" name="check_<?php echo $row->id;?>" id="checklist" value="<?php echo $row->checklist;?>"/></td>
											
											<td style="text-align:center !important;"><input type="checkbox" id="onlyone1_<?php echo $row->id;?>" onchange="change_not_applicable_val(this,'<?php echo $row->id;?>')" <?php if($row->not_applicable == 1){echo'checked';}?> <?php echo $chlddls1;?> <?php echo $disabled_138;?> class="<?php echo $CompleteCheck_class; ?>"/>
											<input type="hidden" name="not_applicable_<?php echo $row->id;?>" id="not_applicable_val" value="<?php echo $row->not_applicable;?>"/></td>
											
											<td class="<?php echo $CompleteCheck_b_class; ?>" style="text-align:left;"><?php echo $loan_servicing_checklist[$row->hud] ? $loan_servicing_checklist[$row->hud] : $row->items;?></td>
										</tr>	
											
								<?php } } }
								//} ?>

								<?php 
							
								
								
								if($user_role == '2'){ 
									
									$disabled_com33=array();
									//$disabled_com33='';
                                    foreach($servicing_checklist as $row){ 	
										
	                             		if($row->hud == '1056' || $row->hud == '135' || $row->hud == '137' || $row->hud == '138' || $row->hud == '1037' || $row->hud == '1068' || $row->hud == '1058'){ //$row->hud == '1037'

	                             			if($row->checklist == 1 || $row->not_applicable == 1)
											{
												$disabled_com33[] = '1';
											
											}else{
												$disabled_com33[] = '2';
											
											}
										
										
										}

									}
									// echo "<pre>";
									// print_r($disabled_com33);
									$disabled_disabled33 = '';
									if(in_array(1, $disabled_com33)){
										$disabled_disabled33 = '';
										
									}else{
										$disabled_disabled33 = 'disabled';
									}
									if($checklist_complete == 'checked'){
										$disabled_disabled33 = '';	
									}

										
										?>
									<tr>
										<td class="checklist_single_Complete">
											<input type="checkbox"  onchange="checkbox_single(this);" <?php echo $checklist_complete; ?> <?php echo $disabled_disabled33;?>>
										</td>
										
										<th colspan="2">
											<span>Checklist Complete and Release Loan to File</span>
										</th>
										<input type ="hidden" name="checklist_complete" value="" id="servicing_chklist" checked>
									
									</tr>
									
							<?php }else{  ?>
									
									<tr>
										<td class="checklist_single_Complete">
											<input type="checkbox"  onchange="checkbox_single(this);" <?php echo $checklist_complete; ?> disabled>
										</td>
										
										<th colspan="2">
											<span>Checklist Complete and Release Loan to File</span>
										</th>
									
										<input type ="hidden" name="checklist_complete" value="" id="servicing_chklist">
									</tr>
								
							<?php } ?>
								
															

								</tbody>	
								
						</table>	
						<table class="hide_samplerow_checklist" class="table table-bordered table-striped table-condensed flip-content th_text_align_center"  style="display:none" id="">
							<tr>
								<!--<td style="text-align:center !important;" >
									<input type="text" name="new_checklist_items[]" style="width: 50px !important;">
								</td>-->
								
								<td style="text-align:center !important;">
								<input type="checkbox" onchange="change_checklist_value(this)"  class="form-control" >
										
								<input type="hidden" value="0" name="new_checklist[]" id="checklist">
							
								</td>
								
								<td style="text-align:center !important;">
								
									<input type="checkbox" onchange="change_not_applicable_val(this)"  class="form-control" >
									
									<input type="hidden" value="0" name="new_applied[]" id="not_applicable_val">
									
								</td>
								<td>
									<input type="text" name="new_check_text[]" style="width:100%;">
								</td>
							  
							</tr>
							</table>
							
						</div>
						<div class="modal-footer">
							
							<a href="<?php echo base_url();?>New_loan_data/print_loan_servicing_checklist<?php echo $loan_id;?>" class="btn blue" >Print</a>
							<!-- <a href="<?php echo base_url();?>load_data/print_loan_servicing_checklist<?php echo $loan_id;?>" class="btn blue" >Print</a> -->
							<button type="submit" name="form_button" value="save" class="btn blue" >Save</button>
							<a class="btn btn-default" data-toggle="modal" href="#edit_checklist">Close</a>
						</div>
						
						<!---Saved changes--->
						<div class="modal fade" id="edit_checklist" tabindex="-1" role="basic" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content" style="top:600px !important;">
									
									<div class="modal-body">
										<div class="row" style="padding-top:8px;">
									
											<div class="col-md-8" style="padding-top:6px;"><p>Do you want to Save Information?</p></div>
											<div class="col-md-2">
												<!--<a style="margin-left:38px;" class="btn blue borrower_save_button" href="javascript:void(0);" onclick = "save_info('edit_contact')">Yes</a>-->
												
												<button style="margin-left:38px;" type="submit" name="form_button" value="save" class="btn blue" >Yes</button>
												
											</div>
											<div class="col-md-2">
												<a class="btn default borrower_save_button" href = "<?php echo base_url().'load_data/'.$loan_id;?>" >No</a>
											
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!---Saved changes--->
					</form>
				</div>
			</div>
	</div>
	
	<!------------ Marketing Modal Starts ----------->
	
	
	
	
	<!------------ Marketing Modal Starts ----------->
	<div class="modal fade bs-modal-lg" id="marketing_option" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form method="POST" action = "<?php echo base_url();?>loan_servicing_form">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<!--<h4 class="modal-title">Marketing: #<?php// echo ''.$modal_talimar_loan; ?> </h4>-->
							<h4 class="modal-title">Marketing: <?php echo ''.$property_addresss; ?> </h4>
						</div>
						<div class="modal-body">
							<input type="hidden" value="marketing_option" name="form_type"/>
							<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
					
							<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
							
						<h4 style="background-color:#e8e8e8;padding:4px;"><strong>Trust Deed Marketing:</strong></h4>
							<div class="row">
							<div class="col-md-4 control-label">
								
							<label>Property Type:</label>
								</div>
								
									<div class="col-md-3">
								
			
			<select  class="form-control" name = "property_typee" id="select_property_typee">
													<?php
													foreach($property_typee as $key => $opt)
													{
														?>
	<option value="<?php echo $key;?>"<?php if($key==$loan_servicing_data[0]->marketing_property_type) { echo 'selected';} ?>> <?php echo  $opt;?></option>
														<?php
													}
													?>
													
												</select>
							</div>
							
							</div><br>
								<div class="row">
									<div class="col-md-4 control-label">
								
							<label>Loan Type:</label>
								</div>
								
									<div class="col-md-3">
								
												<select  class="form-control" name = "loan_typee" id="loan_typee">
													<?php
													foreach($loan_typee as $key => $option)
													{
														?>
														<option value="<?php echo $key;?>" <?php if($key==$loan_servicing_data[0]->marketing_loan_type) { echo 'selected';} ?>><?php echo $option;?></option>
														<?php
													}
													?>
													
												</select>
							</div>
							
							</div><br>
							<div class="row">
							
								<label class="col-md-4 control-label" for="textinput">Market Trust Deed:</label> 
								  
								

								<div class="col-md-3">
									<select name="new_market_trust_deed" class="form-control">
										<?php foreach($market_trust_deed_new_option as $key => $row){?>
											<option value="<?php echo $key;?>" <?php if($key == $loan_servicing['new_market_trust_deed']){echo 'selected';}?>><?php echo $row;?></option>
										<?php } ?>
									</select>
								</div>
								
								
							</div>
							<div class="row">&nbsp;<br></div>
							<div class="row">
								<div class="col-sm-4">
									<button class="btn btn-primary" onclick="openPOPUPs();" type="button">Marketing Page</button>
								</div>
							</div>

							<script type="text/javascript">
								
								function openPOPUPs(){
									let params = `scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=1100,height=500,left=100,top=100`;

									open('<?php echo base_url();?>Lender_Portal/trust_deed_details/<?php echo $modal_talimar_loan;?>', 'Trust Deed', params);
								}
							</script>

							<div class="row">&nbsp;</div>

					
							<h4 style="background-color:#e8e8e8;padding:4px;"><strong>Loan Marketing:</strong></h4>
	
						


							<div class="row">
								<div class="form-group">
								  <label class="col-md-4 control-label" for="textinput">Suggest Website Address:</label> 
									<div class="col-md-8">
									<?php
										$websiteUrl = 'https://talimarfinancial.com/';
										$sugg_pro_type = $property_typee[$loan_servicing_data[0]->marketing_property_type];
										$sugg_loan_type = $loan_typee[$loan_servicing_data[0]->marketing_loan_type];
										$pro_city_state_loan = str_replace(" ", "-", $outlook_property[0]->city).'-'.$outlook_property[0]->state.'-'.$outlook_property[0]->talimar_loan;
										
										$replace_ptypefirst = str_replace(" ", "-", $sugg_pro_type);
										$replace_ltypefirst = str_replace(" ", "-", $sugg_loan_type);
										$newTextadd = 'hard-money';
										
										$full_suggest_URL = $websiteUrl.''.$replace_ptypefirst.'-'.$newTextadd.'-'.$replace_ltypefirst.'-'.$pro_city_state_loan;
									
									?>	
									<span><a><?php echo $full_suggest_URL;?></a></span>
									
									</div>
								</div>
							</div>
							
							<div class="row">&nbsp;</div>
							
							<div class="row">
								<div class="form-group">
								  <label class="col-md-4 control-label" for="textinput">Loan Closing Article URL:</label> 
									<div class="col-md-8">
										<input type="text" name="loan_article" class="form-control" value="<?php echo isset($loan_servicing['loan_article']) ? $loan_servicing['loan_article'] : '';?>"/>
									
									</div>
								</div>
							</div>
								
							
						<div class="row">&nbsp;</div>
						<?php
						
							

						if($loan_servicing['marketing_message'] != ''){

							$loanDescriptiondefaulttext = $loan_servicing['marketing_message'];
						}else{
$loanDescriptiondefaulttext = 'San Diego, CA– TaliMar Financial is pleased to announce our most recent funding of a $Loan Amount Loan Type loan secured on Property Type in City, State.

"Add Description of why the Borrower needed the funds"

The biggest hurdle on this transaction was...';
						}
												
						?>
							<div class="row">
								<div class="form-group">
								  <label class="col-md-4 control-label" for="textinput">Loan Description:</label> 
								  <div class="col-md-8">
								  
										<textarea wrap="hard" class="form-control" name="marketing_message" type="text" rows="12" ><?php echo $loanDescriptiondefaulttext;?></textarea>
								  
									</div>
								</div>
							</div>

					
							<div class="row">&nbsp;</div>
					
							<h4 style="background-color:#e8e8e8;padding:4px;"><strong>Loan Closing Advertising:</strong></h4>
							<div class="row">
								
								<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" id="checklist_table">					
									<thead>
										<tr>
											<!--<th style="text-align:left !important;">Completed </th>
											<th style="text-align:left !important;">Not Applicable</th>-->
											<th style="text-align:left !important;">Processed</th>
											<th style="text-align:left !important;">Description</th>
										</tr>
									</thead>	
									<tbody>
										 

										<?php 

								foreach ($loan_servicing_checklist as $kkkb => $vvcv) 
								{

								$post_new_loan_servicing_checklistvsss[$kkkb][]= $vvcv;

								}

								$post_arrayyyd =array(1027,1028,1033);
								if(isset($post_arrayyyd))
								{

									foreach($post_arrayyyd as $nusmmm){
									
										foreach($post_new_loan_servicing_checklistvsss[$nusmmm] as $rrrs)
										{
											
											$post_servicing_array_finalvsss[$nusmmm] = $rrrs;
										}
									}
								}

									  foreach($post_servicing_array_finalvsss as $keex => $valus){ 
											
											foreach($servicing_checklist as $row){ 

												   if($keex == $row->hud){

														//COMPLETE COLUMN CHECKBOXES....
														if($row->checklist == 1 || $row->not_applicable == 1 || $row->hud == '1033'){
															$disabled_com1033[] = '1';
															
														}else{
															
															$disabled_com1033[] = '2';
															
														}
														
														$disabled_1033 = '';
														if(in_array(2, $disabled_com1033)){

															if($row->hud == '1033' && $this->session->userdata('user_role') != '2'){

																$disabled_1033 = 'disabled';
															}else{
																$disabled_1033 = '';
															}
														}else{

															$disabled_1033 = '';
														}
														
												//if($row->type == 'post wire' && $row->hud != '1001'){ ?>
														
													<tr>
														<input type="hidden" name="id_<?php echo $row->id;?>" value="<?php echo $row->id;?>">
														
														<!--<td style="text-align:center !important;"><?php echo $row->hud;?></td>-->
														
														<td style="text-align:center !important;"><input type="checkbox" onchange="change_checklist_value(this,'<?php echo $row->id;?>')" <?php if($row->checklist == 1){echo 'checked';}?> <?php echo $disabled_1033;?>>
														<input type="hidden" name="check_<?php echo $row->id;?>" id="checklist" value="<?php echo $row->checklist;?>" /></td>
														
														<!--<td style="text-align:center !important;"><input type="checkbox" onchange="change_not_applicable_val(this,'<?php echo $row->id;?>')" <?php if($row->not_applicable == 1){echo'checked';}?> <?php echo $disabled_1033;?>/>
														<input type="hidden" name="not_applicable_<?php echo $row->id;?>" id="not_applicable_val" value="<?php echo $row->not_applicable;?>"/></td>-->
														
														<td style="text-align:left;"><?php echo $loan_servicing_checklist[$row->hud] ? $loan_servicing_checklist[$row->hud] : $row->items;?></td>
													</tr>	
														
										<?php } } } ?>
									</tbody>
								</table>
							
							</div>
							
							<div class="row">&nbsp;</div>
                             

							<h4 style="background-color:#e8e8e8;padding:4px;"><strong>Before & After Images:</strong></h4>
							<div class="row">&nbsp;</div>
							<?php
								if(isset($loan_servicing['process_bai'])){
									$valprocess_bai = $loan_servicing['process_bai'];
									if($valprocess_bai == 1){
										$process_baiCHK = 'checked';
										$process_baiVAL = '1';
									}else{
										$process_baiCHK = '';
										$process_baiVAL = '0';
									}
								}else{ $process_baiCHK = ''; $process_baiVAL = '0';}

								if(isset($loan_servicing['compleate_bai'])){
									$valcompleate_bai = $loan_servicing['compleate_bai'];
									if($valcompleate_bai == 1){
										$compleate_baiCHK = 'checked';
										$compleate_baiVAL = '1';
									}else{
										$compleate_baiCHK = '';
										$compleate_baiVAL = '0';
									}
								}else{ $compleate_baiCHK = ''; $compleate_baiVAL = '0';}

								if(isset($loan_servicing['posted_bai'])){
									$valposted_bai = $loan_servicing['posted_bai'];
									if($valposted_bai == 1){
										$posted_baiCHK = 'checked';
										$posted_baiVAL = '1';
									}else{
										$posted_baiCHK = '';
										$posted_baiVAL = '0';
									}
								}else{ $posted_baiCHK = ''; $posted_baiVAL = '0';}

								if(isset($loan_servicing['blasted_bai'])){
									$valblasted_bai = $loan_servicing['blasted_bai'];
									if($valblasted_bai == 1){
										$blasted_baiCHK = 'checked';
										$blasted_baiVAL = '1';
									}else{
										$blasted_baiCHK = '';
										$blasted_baiVAL = '0';
									}
								}else{ $blasted_baiCHK = ''; $blasted_baiVAL = '0';}


// $loan_servicing
// echo '<pre>??';print_r($loan_servicing);echo '</pre>';

							?>
							<div class="row">
								<div class="form-group">
									<label class="col-md-4" for="textinput">Activate</label>
									<div class="col-md-3">
										<select class="form-control" name="activate" id="activate">
											
											<option value="No" <?php if($loan_servicing['image_ab_activate'] == 'No'){ echo "selected"; } ?>>No</option>
											<option value="Yes" <?php if($loan_servicing['image_ab_activate'] == 'Yes'){ echo "selected"; } ?>>Yes</option>					
										</select>
									</div>
									<div class="col-md-5">
										&nbsp;
									</div>
								</div>
							</div>
							<div class="row">&nbsp;</div>
							<div class="row ">
								<div class="form-group">
									<label class="col-md-4" for="textinput">Status</label>
									<div class="col-md-3">
										<select class="form-control clr" name="before_after_image" id="before_after_image">
											<option value="" >Select One</option>
											<option value="Requested" <?php if(isset($process_baiCHK) && $process_baiCHK == 'checked'){ echo "selected"; } ?>>Requested</option>
											<option value="Completed" <?php if(isset($compleate_baiCHK) && $compleate_baiCHK == 'checked'){ echo "selected"; } ?>>Completed</option>
											<option value="Posted" <?php if(isset($posted_baiCHK) && $posted_baiCHK == 'checked'){ echo "selected"; } ?>>Posted</option>
											<!-- <option value="Blasted" <?php //if(isset($blasted_baiCHK) && $blasted_baiCHK == 'checked'){ echo "selected"; } ?>>Blasted</option> -->
										</select>
									</div>									
									<div class="col-md-5">
										&nbsp;		
									</div>
									<script type="text/javascript">
										$(document).ready(function(){
											
											$('body').on('change', "#before_after_image", function(){
												var val = $(this).val();
												$('input[name="process_bai"]').val(0);
												$('input[name="compleate_bai"]').val(0);
												$('input[name="posted_bai"]').val(0);
												$('input[name="blasted_bai"]').val(0);
												if(val == 'Requested'){
													$('input[name="process_bai"]').val(1);
												}
												if(val == 'Completed'){
													$('input[name="compleate_bai"]').val(1);
												}
												if(val == 'Posted'){
													$('input[name="posted_bai"]').val(1);
												}
												if(val == 'Blasted'){
													$('input[name="blasted_bai"]').val(1);
												}
											});
										});
									</script>
									<input type="hidden" name="process_bai" class="form-control" value="<?php echo $process_baiVAL;?>" onclick="process_checkbox_img(this);" <?php echo $process_baiCHK;?>> 
									<input type="hidden" name="compleate_bai" class="form-control" value="<?php echo $compleate_baiVAL;?>" onclick="complete_checkbox_img(this);" <?php echo $compleate_baiCHK;?>> 
									<input type="hidden" name="posted_bai" class="form-control" value="<?php echo $posted_baiVAL;?>" onclick="posted_checkbox_img(this);" <?php echo $posted_baiCHK;?>>
									<input type="hidden" name="blasted_bai" class="form-control" value="<?php echo $blasted_baiVAL;?>" onclick="blasted_checkbox_img(this);" <?php echo $blasted_baiCHK;?>> 
									
								</div>
							</div>
							<div class="row">&nbsp;</div>
							<div class="row r_dates" style="display: none;">
								<div class="form-group">
									<label class="col-md-4" for="textinput">Date Completed</label>
									<div class="col-md-3">
										<!-- <input id="date_completed" name="date_completed[]" type="text" class="form-control input-md date_completed hasDatepicker" value="" placeholder="MM-DD-YYYY"> -->
										<?php //echo $loan_servicing['image_ab_date_completed'];?>
										<input type="text"  placeholder = "MM-DD-YYYY" id="date_completed" class="date_completed  form-control clrs"  value="<?php echo $loan_servicing['image_ab_date_completed'];?>" name = "date_completed[]"  /> 
									</div>
									<div class="col-md-5">
										&nbsp;
									</div>
								</div>
							</div>
							<?php  //echo '<pre>';print_r($loan_servicing); echo '</pre>';
							?>
							<script type="text/javascript">
								$(document).ready(function(){
	 
									$( ".date_completed" ).datepicker({ dateFormat: 'mm-dd-yy', beforeShow: function(i) { if ($(i).attr('readonly')) { return false; } } });
									var selc =$( "select#before_after_image" ).val();
									if(selc === 'Completed'){
									  	$('.r_dates').show();
									  }else{
									  	$('.r_dates').hide();
									  }

									
									$('body').on('change', "#before_after_image", function(){
									  var s_val = $(this).val();
									  console.log(s_val);
									  if(s_val === 'Completed'){
									  	$('.r_dates').show();
									  }else{
									  	$('.r_dates').hide();
									  }

									});
								});
							</script>
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="form-group">
									<label class="col-md-4" for="textinput">Suggested URL</label>
									<div class="col-md-8">
										<!-- <input id="suggested_url" name="suggested_url" type="text" class="form-control suggested_url " value="<?php //echo $loan_servicing['image_ab_suggested_url']?>" >  -->
										<?php
										$BandAUrlss = 'https://talimarfinancial.com/';
										$sugg_pro_types = $property_typee[$loan_servicing_data[0]->marketing_property_type];
										$sugg_loan_types = $loan_typee[$loan_servicing_data[0]->marketing_loan_type];
										$pro_city_state_loans = str_replace(" ", "-", $outlook_property[0]->city);

										//.'-'.$outlook_property[0]->state.'-'.$outlook_property[0]->talimar_loan;
										
										$replace_ptypefirsts = str_replace(" ", "-", $sugg_pro_types);
										$replace_ltypefirsts = str_replace(" ", "-", $sugg_loan_types);
										
										$full_suggest_URLss = $BandAUrlss.''.$pro_city_state_loans.'-'.$replace_ltypefirsts.'-Before-And-After-'.$outlook_property[0]->talimar_loan;

										//echo $outlook_property[0]->city.'-'.$outlook_property[0]->state.'-'.$outlook_property[0]->talimar_loan;
										$fin_url = "https://www.talimarfinancial.com/hard-money-lender-before-and-after-images-".$outlook_property[0]->city.'-'.$outlook_property[0]->state.'-'.$outlook_property[0]->talimar_loan;
									
									?>	
									<span><a><?php echo $fin_url;?></a></span>
									<input id="suggested_url" name="suggested_url" type="hidden" class="form-control suggested_url " value="<?php echo $fin_url;?>" >
									</div>
									<!-- <div class="col-md-5">
										&nbsp;
									</div> -->
								</div>
							</div>
							<div class="row">&nbsp;</div>

							<div class="row">
								<div class="form-group">
									<label class="col-md-4" for="textinput">Website URL</label>
									<div class="col-md-8">
										<input id="actual_url" name="actual_url" type="text" class="form-control actual_url clrs" value="<?php echo $loan_servicing['image_ab_actual_url']?>" > 
									</div>
									<!-- <div class="col-md-5">
										&nbsp;
									</div> -->
								</div>
							</div>
							<div class="row">&nbsp;</div>

							<div class="row">
								<div class="form-group">
									<label class="col-md-4" for="textinput">YouTube URL</label>
									<div class="col-md-8">
										<input id="add_youtube_url" name="add_youtube_url" type="text" class="form-control add_youtube_url clrs" value="<?php echo $loan_servicing['image_ab_youtube_url']?>" > 
									</div>
									<!-- <div class="col-md-5">
										&nbsp;
									</div> -->
								</div>
							</div>
							<div class="row">&nbsp;</div>

							<div class="row">
								<div class="form-group">
									<label class="col-md-4" for="textinput">Shared</label>
									<div class="col-md-3">
										<select class="form-control clr" name="Posted" id="Posted">
											
											<option value="No" <?php if($loan_servicing['image_ab_Posted'] == 'No'){ echo "selected"; } ?>>No</option>
											<option value="Yes" <?php if($loan_servicing['image_ab_Posted'] == 'Yes'){ echo "selected"; } ?>>Yes</option>					
										</select>
										<input type="hidden" name="hiddn_posted" id="hiddn_posted" value="<?php echo isset($loan_servicing['image_ab_Posted']) ? $loan_servicing['image_ab_Posted'] : 'No';?>">
									</div>
									<div class="col-md-5">
										&nbsp;
									</div>
									<script type="text/javascript">
								$(document).ready(function(){
									var selc_hiddn_posted = $( "#hiddn_posted" ).val();
									if(selc_hiddn_posted === ''){

									}else{
										$('select#Posted').val(selc_hiddn_posted);
									}
									
									


									$('select#Posted').on('change',function(){
										var t = $(this).val();
										$('#hiddn_posted').val(t);
									});
								});
									</script>
								</div>
							</div>
							<div class="row">&nbsp;</div>

							<script type="text/javascript">
								
								function process_checkbox_img(that){
									if($(that).is(':checked')){
										$('input[name="process_bai"]').val('1');
									}else{
										$('input[name="process_bai"]').val('0');
									}
								}

								function complete_checkbox_img(that){
									if($(that).is(':checked')){
										$('input[name="compleate_bai"]').val('1');
									}else{
										$('input[name="compleate_bai"]').val('0');
									}
								}
								function posted_checkbox_img(that){
									if($(that).is(':checked')){
										$('input[name="posted_bai"]').val('1');
									}else{
										$('input[name="posted_bai"]').val('0');
									}
								}
								function blasted_checkbox_img(that){
									if($(that).is(':checked')){
										$('input[name="blasted_bai"]').val('1');
									}else{
										$('input[name="blasted_bai"]').val('0');
									}
								}
							</script>

							<div class="row">&nbsp;</div>
							<div class="row hidden">
								<div class="form-group">
								  <label class="col-md-4" for="textinput">Suggested Website Address</label> 
									<div class="col-md-8">
									<?php
										$BandAUrlss = 'https://talimarfinancial.com/';
										$sugg_pro_types = $property_typee[$loan_servicing_data[0]->marketing_property_type];
										$sugg_loan_types = $loan_typee[$loan_servicing_data[0]->marketing_loan_type];
										$pro_city_state_loans = str_replace(" ", "-", $outlook_property[0]->city);

										//.'-'.$outlook_property[0]->state.'-'.$outlook_property[0]->talimar_loan;
										
										$replace_ptypefirsts = str_replace(" ", "-", $sugg_pro_types);
										$replace_ltypefirsts = str_replace(" ", "-", $sugg_loan_types);
										
										$full_suggest_URLss = $BandAUrlss.''.$pro_city_state_loans.'-'.$replace_ltypefirsts.'-Before-And-After-'.$outlook_property[0]->talimar_loan;
									
									?>	
									<span><a><?php echo $full_suggest_URLss;?></a></span>
									
									</div>
								</div>
							</div>
							<div class="row">&nbsp;</div>

							

							<div class="row hidden">
								<div class="form-group">
								  <label class="col-md-4 control-label" for="textinput">Before / After Video URL</label> 
									<div class="col-md-8">
										<input type="text" name="youtube_url" class="form-control" value="<?php echo isset($loan_servicing['youtube_url']) ? $loan_servicing['youtube_url'] : '';?>"/>
									
									</div>
								</div>
							</div>
							<!-- <div class="row">&nbsp;</div>
                             <div class="row">
								<div class="form-group">
								  <label class="col-md-4" for="textinput">Before / After Video Status:</label>

								  	<?php $explodeArray = explode(",",$loan_servicing['available_release']); ?>
									<div class="col-md-3">	
								 		<select class="form-control" name="available_release" onchange="hoide_show(this.value)">
								 			<?php foreach($before_after_option_new as $key => $row){ ?>
								 				<option value="<?php echo $key;?>" <?php if($loan_servicing['available_release'] == $key){echo 'selected';}?>><?php echo $row;?></option>
								 			<?php } ?>
								 		</select>
								 	</div>
										

								</div>
							</div> -->
							
							
							
							<div class="row">&nbsp;</div>
					<script type="text/javascript">
						$(document).ready(function(){
							var selc_activate = $( "select#activate" ).val();
										if(selc_activate === 'No'){
										  	// $('.clr').val('');
										  	$('.clr').prop('disabled',true);
										  	$('.clrs').prop('readonly',true);
										  }else{
										  	$('.clr').prop('disabled',false);
										  	$('.clrs').prop('readonly',false);
										  }
							
							$('body').on('change', "#activate", function(){
								var sel_activate = $(this).val();
								console.log(sel_activate);
								if(sel_activate === 'No'){
									// $('.clr').val('');
									$('.clr').prop('disabled',true);
									$('.clrs').prop('readonly',true);
								}else{
									$('.clr').prop('disabled',false);
									$('.clrs').prop('readonly',false);

								}

							});
						});
					</script>
						
							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
							<button type="submit" name="form_button" value="save" class="btn blue" >Save</button>
						</div>
					</form>
				</div>
			</div>
	</div>
	
	
											
	
	
	<div class="modal fade bs-modal-lg" id="marketing_extention" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form method="POST" action ="<?php echo base_url();?>">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<!--<h4 class="modal-title">Extension Processing: #<?php// echo ''.$modal_talimar_loan; ?> </h4>-->
							<h4 class="modal-title">Extension Processing: <?php echo ''.$property_addresss; ?> </h4>
						</div>
						<div class="modal-body">
						
						
						<input type="hidden" value="marketing_extentionn" name="form_typ"/>
						<input type="hidden" id="" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
					
						<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
					
						<div class="first_block_content">
					
						<div class="main_c">
						
						<div class="row">
							<div class="col-md-4"><b>Extension #</b></div>
							<div class="col-md-4"><b>Extension Period</b></div>
							<div class="col-md-3"><b>Status</b></div>
							<div class="col-md-1"></div>
							
						</div>

						
					<?php 

					if(isset($add_extension_data)){
					
					$i=0;
					
					foreach($add_extension_data  as $ken=>$rob){
						
						$fetchex = $this->User_model->query("SELECT * FROM `extension_section` WHERE `add_extension_id`='".$rob->id."'");
						if($fetchex->num_rows() > 0){
							
							$fetchex = $fetchex->result();

							/*echo '<pre>';
							print_r($fetchex);
							echo '</pre>';*/
							$extension_complete = $fetchex[0]->extension_complete;

							$extension_from = date('m-d-Y', strtotime($fetchex[0]->extension_date_from));
							$extension_to = date('m-d-Y', strtotime($fetchex[0]->extension_to));

							//$extension_period = $extension_from .' to '.$extension_to;
							$extension_period = $extension_to .' to '.$extension_from;
						}else{
							$extension_complete = '';
							$extension_period = '';
						}

					?>
					
					<div class="row">
						<div class="form-group disclosures_lable">
						    <div class="col-md-4">
						
							  <a href="javascript:void(0);" id="<?php echo $rob->id;?>" onclick="open_popup(this)" style="text-decoration:none;"><p><?php echo isset($rob->id)? ucfirst($rob->extension_name):"Extension".' '.$i;?></p></a>
								<input type="hidden" name="ex_id[]" value="<?php echo isset($rob->id) ? $rob->id :'new'; ?>">
								<input type="hidden" id="t_ali" name="" value="<?php echo $modal_talimar_loan; ?>">
								<input type="hidden" id="e_name" name="extension_name[]" value="<?php echo isset($rob->extension_name) ? $rob->extension_name :  '';?>">


							</div>
							<div class="col-md-4">
								<p><?php echo $extension_period; ?></p>
							</div>
							<div class="col-md-3">
								
							  <?php echo $ext_process_option[$fetchex[0]->ext_process_option]; ?>
							  
							</div>

							<div class="col-md-1">
						  		<a title="Delete" onclick="delete_extension_data('<?php echo $rob->id;?>');"><i class="fa fa-trash"></i></a>
						  	</div>
						</div>
					</div>
						<?php $i++; } }else{?>
					
							
					
						<?php } ?>

						</div>
						<div class="row">
								
							<div class="form-group disclosures_lable"> 
								 
                               <div class="col-md-4">
								<button type="button" onclick="add_extension_request(this)" class="btn btn-sm btn-primary">(+) Extension Request</button>
								  </div>
								 <div class="col-md-5">
								
								  </div>
								</div>
							</div>
							
						
						</div>
					</div>
						<div class="modal-footer">
							<!--<button type="button" class="btn default" data-dismiss="modal">Close</button>
							<button type="submit" name="form_button" value="save" class="btn blue" >Save</button>-->
						</div>
						
					</form>
				</div>
			</div>
	</div>
	
	
	
	
	
		
	<!---------------custom Extention Processing Modal Starts ---------------->
		
	<div class="modal fade bs-modal-lg" id="custom_add_extension" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form method="POST" action = "<?php echo base_url();?>insert_extension_request" id="m_form">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<!--<h4 class="modal-title">Extension Processing: #<?php// echo ''.$modal_talimar_loan; ?> </h4>-->
							<h4 class="modal-title" >Extension Processing: <span id="ex_name" style="text-transform:capitalize;"></span> </h4>
						    <input type="hidden" name="ex_idd" value="" id="ex_idd" >
						    <input type="hidden" name="select_box_id" value="" id="select_b" >
						    <input type="hidden" name="exten_name" value="" id="exten_name" >
						
						</div>
						<div class="modal-body">
						
						<input type="hidden" value="marketing_extention" name="form_type"/>
						<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
					
						<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
						
						<div class="first_block_content">
			
						<h4><strong>Extension Status:</strong></h4>

						<div class="row">
								<div class="form-group disclosures_lable">
									  <label class="col-md-4 control-label" for="textinput">Extension Status: </label>  
									  <div class="col-md-4">
									  	<select name="ext_process_option" class="form-control" style="width: 150px !important;">
									  		<?php foreach($ext_process_option as $key => $row){ ?>
									  			<option value="<?php echo $key;?>"><?php echo $row;?></option>
									  		<?php } ?>
									  	</select>

									  </div>
								</div>
							</div>

						<div class="row">
							<div class="form-group disclosures_lable">
							    <label class="col-md-4 control-label" for="textinput">Current Maturity Date:</label> 
							    <div class="col-md-4">
							    	<input type="text" name="ext_maturity" class="form-control" value="<?php echo date('m-d-Y', strtotime($fetch_loan_result[0]->new_maturity_date));?>" readonly>
							    </div>
							</div>
						</div>

						<div class="row">
								<div class="form-group disclosures_lable">
								  <label class="col-md-4 control-label" for="textinput">Extension From:</label>  
								 
                            <?php 
							
								if($loan_servicing['extension_to'] == ''){

							$date_to='';
							}
							else{

 							$date_to = date('m-d-Y',strtotime($loan_servicing['extension_to']));
							}
							
							
							
							
							?>

								 <div class="col-md-4">
								
									<input type="text"  placeholder = "mm-dd-yyyy" id="ex_date" class="accured_date form-control"  value="" name ="ex_date" /required>
								
								  <span class="help-block"></span>  
								  </div>
								</div>
							</div>

             				<div class="row">
								<div class="form-group disclosures_lable">
								  <label class="col-md-4 control-label" for="textinput">Extension To:</label>  
								  <div class="col-md-4">
								
							<?php 
							if($loan_servicing['extension_date_from'] == ''){

							$date_from='';
							}
							else{

 							$date_from = date('m-d-Y',strtotime($loan_servicing['extension_date_from']));
							}
						
							
							?>
								
								
								<input type="text"  placeholder ="mm-dd-yyyy" id="ex_date_2" class="accured_datee form-control"  value="" name ="ex_date_2" required>
								
								  <span class="help-block"></span>  
								  </div>
								</div>
							</div>

							<div class="row">
								<div class="form-group disclosures_lable">
									  <label class="col-md-4 control-label" for="textinput">Payment Calculation:</label>  
									  <div class="col-md-4">

									  	<select name="PaymentCal" class="form-control" style="width: 150px !important;">
									  		<?php foreach($ext_PaymentCalculation as $key => $row){ ?>
									  			<option value="<?php echo $key;?>"><?php echo $row;?></option>
									  		<?php } ?>
									  	</select>

									  </div>
								</div>
							</div>

							<div class="row">
								<div class="form-group disclosures_lable">
									  <label class="col-md-4 control-label" for="textinput">Payment Type: </label>  
									  <div class="col-md-4">

									  	<select name="PaymentType" class="form-control" onchange="hideextcheckboxes(this.value);" style="width: 150px !important;">
									  		<?php foreach($ext_PaymentType as $key => $row){ ?>
									  			<option value="<?php echo $key;?>"><?php echo $row;?></option>
									  		<?php } ?>
									  	</select>

									  </div>
								</div>
							</div>

							<div class="row">
								<div class="form-group disclosures_lable">
									  <label class="col-md-4 control-label" for="textinput">Payment Amount: </label>  
									  <div class="col-md-2">
									  	<input type="text" id="pay_amt_val" class="form-control amount_format" value="" name ="pay_amt_val" style="margin-left: 10px;padding-left: 2px;">
									  </div>
									  <div class="col-md-2" style="margin-left: 50px !important;">
									  	<select name="pay_amt_opt" class="form-control" style="width: 150px !important;">
									  		<?php foreach($ext_PaymentAmount_opt as $key => $row){ ?>
									  			<option value="<?php echo $key;?>"><?php echo $row;?></option>
									  		<?php } ?>
									  	</select>

									  </div>
								</div>
							</div>

							<div class="row">
								<div class="form-group disclosures_lable">
									  <label class="col-md-4 control-label" for="textinput">Lender Approval: </label>  
									  <div class="col-md-4">
									  	<select name="ext_lender_app" class="form-control" style="width: 150px !important;">
									  		<?php foreach($ext_lender_app as $key => $row){ ?>
									  			<option value="<?php echo $key;?>"><?php echo $row;?></option>
									  		<?php } ?>
									  	</select>

									  </div>
								</div>
							</div>

							<div class="row">
								<div class="form-group disclosures_lable">
									  <label class="col-md-4 control-label" for="textinput">Borrower Approval: </label>  
									  <div class="col-md-4">
									  	<select name="BorrowerApproval" class="form-control" onchange="hideBorrowerApproval(this.value);" style="width: 150px !important;">
									  		<?php foreach($ext_borrower_approval as $key => $row){ ?>
									  			<option value="<?php echo $key;?>"><?php echo $row;?></option>
									  		<?php } ?>
									  	</select>

									  </div>
								</div>
							</div>

							
							<hr>
							<h4><strong>Extension Checklist:</strong></h4>

							<div class="row">
								<div class="form-group disclosures_lable">
								  <div class="col-md-4" style="width: 10%">
									<input type="checkbox" id="ext_req_borrower"  onclick="fun_ext_req_borrower(this)"  name="ext_req_borrower" value="" > 
								  	<span class="help-block"></span>  
								  </div>
								  <label class="col-md-4 control-label" for="textinput">Extension Requested by Borrower </label>
								</div>
							</div>

							<div class="row">
								<div class="form-group disclosures_lable">
								  <div class="col-md-4" style="width: 10%">
									<input type="checkbox" id="ext_app_rec_lender"  onclick="fun_ext_app_rec_lender(this)"  name="ext_app_rec_lender" value="" > 
								  	<span class="help-block"></span>  
								  </div>
								  <label class="col-md-4 control-label" for="textinput">Approval Received from Lender(s) </label>
								</div>
							</div>
							
							<!--<div class="row">
								<div class="form-group disclosures_lable">
								  <label class="col-md-4 control-label" for="textinput">Extension Agreement Offered: </label>  
								  <div class="col-md-4">
									<input type="checkbox" id="exten_off_yes"  onclick="fun_exten_off_yes(this)"  name="extension_agreement_offered" value="" > 
								  	<span class="help-block"></span>  
								  </div>
								</div>
							</div>-->
						
								<div class="row">
									<div class="form-group disclosures_lable">
									  <div class="col-md-4" style="width: 10%">
										<input type="checkbox" onclick="fun_extenagre_yes(this)" id="extenagre_yes" name="extension_agreement_signed" value="" > 
									  <span class="help-block"></span>  
									  </div>
									  <label class="col-md-4 control-label" for="textinput">Extension Agreement Signed</label>  
									</div>
								</div>
								
								
								
								<div class="row">
									<div class="form-group disclosures_lable">
									  <div class="col-md-4" style="width: 10%">
										<input type="checkbox"  onclick="fun_Accrued_Charges(this)" name="ext_accrued" id="ext_Accrued_Charges"  value=""> <!--Yes <input type="radio" id="extenrecived_no"  onclick="fun_extenrecived_no(this)" name="extension_agreement_payment_received" value="" > No-->
									  <span class="help-block"></span>  
									  </div>
									  <label class="col-md-4 control-label" for="textinput">Add Extension Fee to <a onclick="open_accrued_charge(this);">Accrued Charges</a>, (if deferred) </label>
									</div>
								</div>

								<script type="text/javascript">
									
									function open_accrued_charge(that){

										//$('#custom_add_extension').model('hide');
										//$('#marketing_extention').model('hide');
										$('#accured_charges').modal('show');
									}
								</script>

								<div class="row">
									<div class="form-group disclosures_lable">
									    
									  <div class="col-md-4" style="width: 10%">
										<input type="checkbox"  onclick="fun_notify_loanext(this)" name="notify_loanext" id="notify_loanext"  value=""> 
									  <span class="help-block"></span>  
									  </div>
									  <label class="col-md-4 control-label" for="textinput">Notify Servicer of Loan Extension </label>
									</div>
								</div>
								<div class="row">
									<div class="form-group disclosures_lable extension_agreement_submitted">
									    
									  <div class="col-md-4" style="width: 10%">
									
										<input type="checkbox"  onclick="fun_extensubmit_yes(this)" name="extension_agreement_servicer_submitted" id="extenrecived_yes12"  value=""> 
									  <span class="help-block"></span>  
									  </div>
									  <label class="col-md-4 control-label" for="textinput">Extension Fee Submitted to Servicer</label>
									</div>
								</div>

								<div class="row">
									<div class="form-group disclosures_lable">
									    
									  <div class="col-md-4" style="width: 10%">
									
										<input type="checkbox"  onclick="fun_extenrecived_yes(this)" name="extension_agreement_payment_received" id="extenrecived_yes11"  value=""> 
									  <span class="help-block"></span>  
									  </div>
									  <label class="col-md-4 control-label" for="textinput">Extension Payment Received by Servicer </label>
									</div>
								</div>

								
								
								<!--<div class="row">
									<div class="form-group disclosures_lable">
									  <label class="col-md-4 control-label" for="textinput">Extension Option Processed: </label>  
									  <div class="col-md-4">
										<input type="checkbox" id="extenopt_yes"  onclick="fun_extenopt_yes(this)" name="extension_option_processed" value="" >								
									  <span class="help-block"></span>  
									  </div>
									</div>
								</div>-->

								

								<div class="row">
									<div class="form-group disclosures_lable">
									    
									  <div class="col-md-4" style="width: 10%">
									 	<input type="checkbox" id="updated_loan_term"  onclick="fun_update_loan_term(this)" name="update_loan_term" value="" >		 
									  <span class="help-block"></span>  
									  </div>
									  <label class="col-md-4 control-label" for="textinput">Update Loan Term on Terms Page</label>
									</div>
								</div>

								<div class="row">
									<div class="form-group disclosures_lable">
									   
									  <div class="col-md-4" style="width: 10%">
									 	<input type="checkbox" id="confirm_maturity_date" onclick="fun_confirm_maturity_date(this)" name="confirm_maturity" value="">
									  <span class="help-block"></span>  
									  </div>
									  <label class="col-md-4 control-label" for="textinput">Confirm Loan Servicer updated Maturity Date</label> 
									</div>
								</div>

								<div class="row">
									<div class="form-group disclosures_lable">
									    
									  <div class="col-md-4" style="width: 10%">
									 	<input type="checkbox" id="extenpay_yes"  onclick="fun_extenpay_yes(this)" name="extension_payment_disbursed" value="" >
									  <span class="help-block"></span>  
									  </div>
									  <label class="col-md-4 control-label" for="textinput">Extension Payment Disbursed</label>
									</div>
								</div>
								
								<?php

									if($this->session->userdata('user_role') == '2'){
										$canedit = '';
									}else{
										$canedit = 'disabled';
									}

								?>

								<div class="row">
									<div class="form-group disclosures_lable">
										<div class="col-md-4" style="width: 10%">
									 		<input type="checkbox"  onclick="fun_extension_yes(this)" id="extension_yes" name="extension_complete" value="" <?php echo $canedit;?>> 
										  <span class="help-block"></span>  
										</div>
										<label class="col-md-4 control-label" for="textinput">Admin Review and Approved</label> 
									</div>
								</div>
							
								<div class="row">
									<div class="form-group">
									  <label class="col-md-4 control-label" for="textinput">Notes:</label><br>  
									  <div class="col-md-12">
									  	<textarea type ="text" id="extension_notes" rows="4" name="extension_notes" class="form-control "></textarea>
									  <span class="help-block"></span>  
									  </div>
									</div>
								</div>
						
						</div>
					</div>
						<div class="modal-footer">
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
							<button type="submit" name="form_button" value="save" id="sb" class="btn blue" >Save</button>
						</div>
						
					</form>
				</div>
			</div>
	</div>
	
	
         	<!------------ custom extension modal end----------->	
	
	
	
	
	
	
	

	
	
	
		         	<!------------ New Payoff_processing Modal Starts ----------->
											
	
	
	<div class="modal fade bs-modal-lg" id="payoff_pro" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form method="POST" action = "<?php echo base_url();?>pay_off_processing_form">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			
							<h4 class="modal-title">Payoff Checklist: <?php echo ''.$property_addresss; ?> </h4>
						</div>
						<div class="modal-body">
						
						<input type="hidden" value="marketing_extention" name="form_type"/>
						<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
					
						<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
						
						<div class="first_block_content">
							
							<h4 class="checklistsection">Demand Requests</h4>
							<div class="row">
								<div class="form-group">
								  	 
								  	<?php 
									  	if($pay_off_processing[0]['de_Payoff_Request'] == 1){
									  		$checkde_Payoff_Request = 'checked';
									  		$valuede_Payoff_Request = '1';
									  		$hidebelowcheckboxes = '';
									  	}else{
									  		$checkde_Payoff_Request = '';
									  		$valuede_Payoff_Request = '0';
									  		$hidebelowcheckboxes = 'disabled';
									  	} 
									?>
								  	<div class="col-md-1">
								  	 	<input type="checkbox" id="de_Payoff_Request" name="de_Payoff_Request" class="form-control" onclick="openbelowdateandbox(this);" value="<?php echo $valuede_Payoff_Request;?>" <?php echo $checkde_Payoff_Request;?>><span class="help-block"></span>
								  	</div>
								  	<div class="col-md-8">Payoff Requested</div>
								</div>
							</div>

							<div class="row">
								<div class="form-group">
								  	<div class="col-md-4" style="margin-left: 47px;">Good Thru Date</div>
								  	<?php 
									  	if($pay_off_processing[0]['de_Good_Thru_Date'] !=''){
									  		$valuede_Good_Thru_Date = date('m-d-Y',strtotime($pay_off_processing[0]['de_Good_Thru_Date']));
									  	}else{
									  		$valuede_Good_Thru_Date = '';
									  	} 
									?>
								  	<div class="col-md-4">
								  	 	<input type="text" id="de_Good_Thru_Date" name="de_Good_Thru_Date" class="form-control datepicker" value="<?php echo $valuede_Good_Thru_Date; ?>" <?php echo $hidebelowcheckboxes;?>>
								  	</div>
								  	 
								</div>
							</div>

							
							<div class="row">
								<div class="form-group">
								  	
								  	<?php 
									  	if($pay_off_processing[0]['de_Servicing_Approved'] == 1){
									  		$checkde_Servicing_Approved = 'checked';
									  		$valuede_Servicing_Approved = '1';
									  	}else{
									  		$checkde_Servicing_Approved = '';
									  		$valuede_Servicing_Approved = '0';
									  	} 
									?> 
								  	<div class="col-md-1">
								  	 	<input type="checkbox" id="de_Servicing_Approved" name="de_Servicing_Approved" class="form-control" onclick="ser_approve_check(this);" value="<?php echo $valuede_Servicing_Approved; ?>" <?php echo $checkde_Servicing_Approved; ?> <?php echo $hidebelowcheckboxes;?>><span class="help-block"></span>
								  	</div>
								  	<div class="col-md-8">Payoff Approved by Loan Servicing</div> 
								</div>
							</div>

							<div class="row">
								<div class="form-group">
								  	
								  	<?php 
									  	if($pay_off_processing[0]['de_Admin_Approved'] == 1){
									  		$checkde_Admin_Approved = 'checked';
									  		$valuede_Admin_Approved = '1';
									  	}else{
									  		$checkde_Admin_Approved = '';
									  		$valuede_Admin_Approved = '0';
									  	} 
									?> 
								  	<div class="col-md-1">
								  	 	<input type="checkbox" id="de_Admin_Approved" name="de_Admin_Approved" class="form-control" onclick="admin_approve_checkboxes(this);" value="<?php echo $valuede_Admin_Approved;?>" <?php echo $checkde_Admin_Approved;?> <?php echo $hidebelowcheckboxes;?>><span class="help-block"></span>
								  	</div>
								  	<div class="col-md-8">Payoff Approved by Admin</div> 
								</div>
							</div>
							
							

							<script type="text/javascript">
								
								function openbelowdateandbox(that){

									if($(that).is(':checked')){

										$('input[name="de_Payoff_Request"]').val('1');
										$('input[name="de_Good_Thru_Date"]').attr('disabled',false);
										$('input[name="de_Servicing_Approved"]').attr('disabled',false);
										$('input[name="de_Admin_Approved"]').attr('disabled',false);

										$('#uniform-de_Servicing_Approved').removeClass('disabled');
										$('#uniform-de_Admin_Approved').removeClass('disabled');

									}else{

										$('input[name="de_Payoff_Request"]').val('0');
										$('input[name="de_Good_Thru_Date"]').val('').attr('disabled',true);
										$('input[name="de_Servicing_Approved"]').val('').attr('disabled',true);
										$('input[name="de_Admin_Approved"]').val('').attr('disabled',true);

										$('#uniform-de_Servicing_Approved span').removeClass('checked');
										$('#uniform-de_Admin_Approved span').removeClass('checked');
										

									}
								}

								function ser_approve_check(that){
									if($(that).is(':checked')){
										$('input[name="de_Servicing_Approved"]').val('1');
									}else{
										$('input[name="de_Servicing_Approved"]').val('0');
									}
								}

								function admin_approve_checkboxes(that){
									if($(that).is(':checked')){
										$('input[name="de_Admin_Approved"]').val('1');
									}else{
										$('input[name="de_Admin_Approved"]').val('0');
									}
								}
							</script>

							<h4 class="checklistsection">Process Payoff</h4>

							<div class="row">
								<div class="form-group">
									<div class="col-md-2">
										<select class="form-control" name="payoff_type" id="payoff_type">
											<option value="">Select One</option>
											<?php foreach($process_payoff_type_opt as $key => $opt){ ?>
												<option value="<?php echo $key;?>" <?php if($pay_off_processing[0]['payoff_type'] == $key){ echo "selected"; } ?> ><?php echo $opt;?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-6">Payoff Type</div>
								</div>
							</div>

							<div class="row"><br></div>

							<div class="row">
								<div class="form-group">
								  <div class="col-md-1">
								  	 <input type="checkbox" id="active_paid_off" name="active_paid_off" class="form-control " <?php if($pay_off_processing[0]['active_paid_off'] == 2){ echo "checked";
									  } ?>>
								
								  <span class="help-block"></span>
								  </div>
								  <div class="col-md-8"><a onclick="openseriptpage(this);">Notify Lenders</a> of Payoff </div>
								</div>
							</div>
							<!-- 21-07-2021 By@Aj -->
							<div class="row">
								<div class="form-group">
								  <div class="col-md-1">
								  	 <input type="checkbox" id="process_before_after_img" name="process_before_after_img" class="form-control " <?php if($pay_off_processing[0]['process_before_after_img'] == 2){ echo "checked"; } ?>>
								
								  <span class="help-block"></span>
								  </div>
								  <div class="col-md-8">Process Before & After Images</div>
								</div>
							</div>
							<!-- End -->
							
							<div class="row">
								<div class="form-group">
								  
								  <div class="col-md-1">
								 
								  	 <input type="checkbox" id="loan_folder" name="loan_folder" class="form-control " <?php if($pay_off_processing[0]['loan_folder'] == 2){ echo "checked";
									  } ?>>
								
								  <span class="help-block"></span>
								  </div>
								  <div class="col-md-8">Remove hard loan folder from filing cabinet </div>  
								</div>
							</div>

							<div class="row">
								<div class="form-group">
								   
								  <div class="col-md-1">
								 
								  	 <input type="checkbox" id="lender_folder" name="lender_folder" class="form-control " <?php if($pay_off_processing[0]['lender_folder'] == 2){ echo "checked";
									  } ?>>
								
								  <span class="help-block"></span>
								  </div>
								  <div class="col-md-8">Move electronic loan folder to Paid Off Loans folder</div> 
								</div>
							</div>

							<div class="row">
								<div class="form-group">
								    
								  <div class="col-md-1">
								
								  	 <input type="checkbox" id="investor_payoff" name="investor_payoff" class="form-control" <?php if($pay_off_processing[0]['investor_payoff'] == 2){ echo "checked";
									  } ?>>
								  <span class="help-block"></span>  
								  </div>
								  <div class="col-md-8">Change loan from <a onclick="closethismodalpay(this);">Active to Paid Off</a> in database </div>
								</div>
							</div>	
							<div class="row">
								<div class="form-group">
								    
								  <div class="col-md-1">
								
								  	 <input type="checkbox" id="auto_notification_off" name="auto_notification_off" class="form-control" <?php if($pay_off_processing[0]['auto_notification_off'] == 2){ echo "checked";
									  } ?>>
								  <span class="help-block"></span>  
								  </div>
								  <div class="col-md-8">Turn off auto-email notifications</div>
								</div>
							</div>	
							<script type="text/javascript">
								function closethismodalpay(that){

									$('#payoff_pro').modal('hide');
								}
							</script>

							<div class="row">
								<div class="form-group">
								    
								  <div class="col-md-1">
									<a id="clickmybuttonss" onclick="add_property_data_rows('<?php echo $this->uri->segment(2);?>');" style="display: none;">sss</a>
								  	<input type="checkbox" id="update_sales" name="update_sales" class="form-control" <?php if($pay_off_processing[0]['update_sales'] == 2){ echo "checked"; } ?>>
								  <span class="help-block"></span>  
								  </div>
								  <div class="col-md-8"><a onclick="openpropertypage(this);">Update Sales / Refinance</a> price in database </div>
								</div>
							</div>	
							<script type="text/javascript">
								
								function openpropertypage(that){

									$('#clickmybuttonss').click();
									$('#payoff_pro').modal('hide');
									$('#loan_servicing').modal('hide');
								}
							</script>
							
							<!-- <div class="row">
								<div class="form-group">
								  <div class="col-md-8">Send Lender Confirmation Email</div> 
								  <div class="col-md-4">
								    <input type="checkbox" id="investor_payoff" name="investor_payoff" class="form-control" <?php if($pay_off_processing[0]['investor_payoff'] == 2){ echo "checked";
									  } ?>>
								  	<span class="help-block"></span>
								  </div>
								</div>
							</div> -->

							<div class="row">
								<div class="form-group">
								    
								  <div class="col-md-1">
									<input type="checkbox" id="thanku_email" name="thanku_email" class="form-control" <?php if($pay_off_processing[0]['thanku_email'] == 2){ echo "checked";
									  } ?>>
								  <span class="help-block"></span>
								  </div>
								  <div class="col-md-8">Send <a onclick="openseriptpage(this);">Thank You e-mail</a> to Borrower </div>
								</div>
							</div>
							<script type="text/javascript">
								function openseriptpage(that){
									$('#payoff_pro').modal('hide');
									$('#email_scripts').modal('show');
								}
							</script>	

							<!-- <div class="row">
								<div class="form-group">
								  <div class="col-md-8">Filing Cabinet</div>  
								  <div class="col-md-4">
						
								  		<input type="checkbox" id="cabinet" name="cabinet" class="form-control " <?php if($pay_off_processing[0]['cabinet'] == 2){ echo "checked";
									  } ?>>
								  <span class="help-block"></span>  
								  </div>
								</div>
							</div> -->
							
							<?php
								$active_paid_off_CHK = isset($pay_off_processing[0]['active_paid_off']) ? $pay_off_processing[0]['active_paid_off'] : 1; 

								
								$process_before_after_img_CHK = isset($pay_off_processing[0]['process_before_after_img']) ? $pay_off_processing[0]['process_before_after_img'] : 1; 


								$loan_folder_CHK = isset($pay_off_processing[0]['loan_folder']) ? $pay_off_processing[0]['loan_folder'] : 1; 
								$lender_folder_CHK = isset($pay_off_processing[0]['lender_folder']) ? $pay_off_processing[0]['lender_folder'] : 1; 
								$investor_payoff_CHK = isset($pay_off_processing[0]['investor_payoff']) ? $pay_off_processing[0]['investor_payoff'] : 1; 
								$update_sales_CHK = isset($pay_off_processing[0]['update_sales']) ? $pay_off_processing[0]['update_sales'] : 1; 
								$thanku_email_CHK = isset($pay_off_processing[0]['thanku_email']) ? $pay_off_processing[0]['thanku_email'] : 1; 
								
								$checkarraylist = array($active_paid_off_CHK,$loan_folder_CHK,$lender_folder_CHK,$investor_payoff_CHK,$update_sales_CHK,$thanku_email_CHK,$process_before_after_img_CHK);
								
								//print_r($checkarraylist);
							?>	
							<div class="row">
								<div class="form-group">
								    
								  <div class="col-md-1">

									 <?php 
											$p_o_p = '';
											if($this->session->userdata('user_role') == '2'){ 
																								
												if(in_array(1, $checkarraylist)){
													$p_o_p = 'disabled';
												}else{
													$p_o_p = '';
												}
											}else{
												$p_o_p = 'disabled';
											}
										?>
								
									<input type="checkbox" id="payoff_processing" name="payoff_processing" class="form-control " <?php if($pay_off_processing[0]['payoff_processing'] == 1){ echo "checked"; } ?> <?php echo $p_o_p;?>>
								 
								  <span class="help-block"></span>  
								  
								  </div>
								  <div class="col-md-8">Payoff Checklist Complete</div>
								</div>

							</div>							
						</div>
					</div>
						<div class="modal-footer">
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
							<button type="submit" name="pay_off_button" value="checksave" class="btn blue" >Save</button>
						</div>
						
					</form>
				</div>
			</div>
	</div>
	
	
	
	
	
	
	
	
	<!--------------------------Accured Charges Modal Starts --------------------------->
	<div class="modal fade bs-modal-lg" id="accured_charges" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg" style="width:80%">
				<div class="modal-content">
					<form method="POST" action = "<?php echo base_url();?>add_accured_charges">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<!--<h4 class="modal-title">Accrued Charges: #<?php //echo ''.$modal_talimar_loan; ?> </h4>-->
							<h4 class="modal-title">Accrued Charges: <?php echo ''.$property_addresss; ?> </h4>
						</div>
						<div class="modal-body">
							<div class="portlet-body ">
							<!-----------Hidden fields----------->
								<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
								<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
								<!-----------End of hidden Fields---->
								
								<!---------Content modal------------------->
								<div class="table-responsive scroll-res-div">    
								<table class="table table-striped table-condensed flip-content" id="table_accured_charges">
									<thead>
										<tr>
											<th></th>
											<th style="width:15% !important">Date</th>
											<th>Description</th>
											<th style="width:15% !important">Paid To</th>
											<th style="width:10% !important">Charge</th>
											<th style="width:10% !important">Paid</th>
											<th style="width:10% !important">Remaining </th>
											<!-- <th style="width:5% !important">Paid</th> -->
										</tr>
									</thead>
									<tbody>
										<?php 
										
										$total = 0;
										$rowcnt = 0;
										$paid_amountsssx = 0;
										if($fetch_accured_data){
											
											foreach($fetch_accured_data as $key => $accured_charges){
												
													$rowcnt++;
													
													//if($accured_charges->checkPaid != 1){
														$total += $accured_charges->accured_amount;
														$paid_amountsssx += $accured_charges->paid_amount;
													//}
										?>
								
											<tr id = "row_<?php echo $rowcnt;?>">
												<td>
													<a href = "javascript:void(0);" onclick = "delete_accured_charges_row('<?php echo $rowcnt;?>','<?php echo isset($accured_charges->id) ? $accured_charges->id:'';?>');" class="as_class"><i class="fa fa-trash" aria-hidden="true"></i></a>
													<input type="hidden" id = "accured_id_<?php echo $rowcnt;?>" name="accured_id[]" value="<?php echo isset($accured_charges->id) ? $accured_charges->id : ''; ?>">
												</td>
												<td><input type="text"  placeholder = "mm-dd-yyyy" id="accured_date_<?php echo $rowcnt;?>" class="accured_date  form-control"  value="<?php
												echo (($accured_charges->accured_date) && ($accured_charges->accured_date !='0000-00-00'))?

												 date('m-d-Y',strtotime($accured_charges->accured_date )) : '';?>" name = "accured_date[]"  /></td>
												<td><input style = "width:100% !important" name  = "accured_description[]" id = "accured_description_<?php echo $rowcnt;?>" type = "text" value = "<?php echo isset($accured_charges->accured_description) ? $accured_charges->accured_description : '' ;?>" class = "form-control" placeholder ="mm-dd-yyyy" required/></td>
												<td><input class ="form-control" type = "text" name = "accured_paid_to[]" value = "<?php echo isset($accured_charges->accured_paid_to) ? $accured_charges->accured_paid_to : '';?>" /></td>
												<td><input style = "width:100% !important"  name  = "accured_amount[]" id = "accured_amount_<?php echo $rowcnt;?>" type = "text" onchange = "get_sum_val(this.value)" value = "<?php echo isset($accured_charges->accured_amount) ? '$'.number_format($accured_charges->accured_amount,2) : '' ;?>" class = "accured_amount number_only  amount_format form-control" /></td>
												<td>
													<input style = "width:100% !important"  name="paid_amount[]" id="paid_amount_<?php echo $rowcnt;?>" type="text" value="<?php echo isset($accured_charges->paid_amount) ? '$'.number_format($accured_charges->paid_amount,2) : '' ;?>" class="number_only amount_format form-control">
												</td>
												<td>
													<input type="" name="remaning[]" value="<?php echo '$'.number_format($accured_charges->accured_amount - $accured_charges->paid_amount,2);?>" class="number_only amount_format form-control">
												</td>
												<!--<td style="text-align:center">
													<input type="checkbox" class="" onclick="CheckasPaid(this,<?php echo $rowcnt;?>);" <?php if($accured_charges->checkPaid == 1){echo 'checked';}?>>
													<input type="hidden" id="paidid_<?php echo $rowcnt;?>" name="checkPaid[]" value="<?php echo isset($accured_charges->checkPaid) ? $accured_charges->checkPaid : '' ;?>">
												</td>-->
											</tr>
										<?php	}
										}else{ 
											// echo 'else part';
										?>
										<tr>
												<td>
													
													<input type="hidden" id = "accured_id_1" name="accured_id[]" value="">
												</td>
												<td><input type="text"  placeholder = "mm-dd-yyyy" id="accured_date_1" class="accured_date  form-control"  value="" name = "accured_date[]"  /></td>
												<td><input style = "width:100% !important" name  = "accured_description[]" id = "accured_description_1" type = "text" value = "" class = "form-control" required/></td>
												<td><input class ="form-control" type = "text" name = "accured_paid_to[]" value = "" /></td>
												<td><input style = "width:100% !important"  name  = "accured_amount[]" id = "accured_amount_1" type = "text" onchange = "get_sum_val(this.value)" value = "" class = "accured_amount number_only  amount_format form-control" /></td>
												<td></td>
												<td></td>
												<!-- <td style="text-align:center">
													<input type="checkbox" class="" onclick="CheckasPaid(this,1);">
													<input type="hidden" id="paidid_1" name="checkPaid[]" value="">
												</td> -->
										</tr>
										<?php } ?>
									</tbody>
									<tfoot>
										
										<tr>
											<td></td>												
											<td></td>
											<td><strong>Total : </strong></td>
											<td></td>
											<td>
											<input id  = "accured_total_val" type = "hidden" value = "<?php echo isset($accured_charges->accured_amount) ? '$'.number_format($total) : '' ;?>" >
											<strong><span id = "accured_total" ><?php echo isset($accured_charges->accured_amount) ? '$'.number_format($total,2) : '' ;?> </span></strong>
											</td>
											<td>
												<span><strong>$<?php echo number_format($paid_amountsssx,2);?></strong></span>
											</td>
											<td>
												<span><strong>$<?php echo number_format($total - $paid_amountsssx,2);?></strong></span>
											</td>
											<!-- <td></td> -->
										</tr>
									</tfoot>
								</table>
								</div>
								<button type = "button"  id="900" onclick="add_row_accured_charges(this)"  class="btn blue" ><i class="fa fa-plus" aria-hidden="true"></i> Add Row</button>
											
								<!-- <script>
									
									function CheckasPaid(that,rows){
										if($(that).is(':checked')){
											$('#accured_charges input#paidid_'+rows).val('1');
										}else{
											$('#accured_charges input#paidid_'+rows).val('');
										}
									}
								</script> -->		
				
							<!----------End of Content modal------------------>
									
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" value = "save" class="btn blue borrower_save_button" name="button">Save</button>
							<button type="button" class="btn default borrower_save_button" data-dismiss="modal">Close</button>
							
							
						</div>
				</form>
				</div>
									<!-- /.modal-content -->
			</div>
								<!-- /.modal-dialog -->
	</div>
	
	
	
	
		<!------------------ MODAL NOTES ---------------->
		
		
		
							<!-- edit modal -->
							
		<div class="modal fade bs-modal-lg" id="edit_contact_note" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form method="post" action="<?php echo base_url();?>edit_loan_contact_notes">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							
							<h4 class="modal-title">Loan Notes: <?php echo $property_addresss; ?> </h4>
						</div>
						<div class="modal-body">	
										
											
							<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
							<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
							
							<input type="hidden" name="updata_id" id="updata_id" value="">
								
							<div class="row">
								<div class="col-md-6">
								<label>Date:</label>
									<input type="text" name="notes_date" id="notes_date" class="form-control datepicker" placeholder="MM/DD/YYYY" value="">
								
								</div>
								<div class="col-md-6">
								<label>Note Type:</label>
									<select name="note_type" class="form-control" id="note_type" required>
									<?php foreach($loan_note_option as $key => $row){ ?>
										<option value="<?php echo $key; ?>"><?php echo $row; ?></option>
									<?php } ?>
									</select>
								
								</div>
							</div>
							
							<div class="row">&nbsp;</div>
							
							<div class="row">
								<div class="col-md-6">
								<label>Contact Name:</label>
									<select name="contact_id" id="contact_id" class="form-control">
				<!-- 					<option>Select One</option>
									<?php foreach($fetch_all_contact as $rows) { ?>
										<option value="<?php echo $rows->contact_id;?>" ><?php echo $rows->contact_firstname .' '. $rows->contact_middlename .' '. $rows->contact_lastname ;?></option>
									<?php } ?> -->			
									</select>
								
								</div>
								<div class="col-md-6">
								<label>Entry By:</label>
									<input type="text" name="entry_by" id="entry_by" value="" class="form-control" readonly />
								</div>
							</div>
							
							<div class="row">&nbsp;</div>
							<div class="row">
								
								<div class="col-md-6">
								<label>Communication Type:</label>
									<select name="com_type" class="form-control" id="com_type" required>
									<?php foreach($com_type as $key => $row){ ?>
										<option value="<?php echo $key; ?>"><?php echo $row; ?></option>
									<?php } ?>
									</select>
								
								</div>
								<div class="col-md-6">
								
								
								</div>
							</div>
							
							<div class="row">&nbsp;</div>
							
							<div class="row">
								<div class="col-md-12">
								<label>Contact Notes:</label>
									<textarea name="notes_text" id="notes_text" type="text" rows="6" class="form-control" placeholder="Add Contact Note Here..."></textarea>
								</div>
							</div>
							
						</div>
					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
						<input type="submit" name="submit" class="btn blue" value="Save">
						
					</div>
				</form>
			</div>
			</div>
		</div>					
				
	<!-- edit notes modal -->
			
	<!--Add notes modal -->
							
		
		
		<!------------------   END OF MODAL NOTES    ---------------->
		
		<!-------------------  MODAL Assignments MAIN --------------->
		  
		
		
		<!-------------------END OF MODAL Assignment MAIN --------------->


		<!-------------------OPEN OF Diligence Documents button on Capital Markets page --------------->
		
		<!-------------------END OF Diligence Documents button on Capital Markets page --------------->






		
		<!-------------------MODAL Assignments --------------->
		
		


		<!-------------------------------- Due Diligence item Start ------------------------------------------>
			
	<!--------------------------------- Due Diligence item End ------------------------------------------->


	<!-- Lender Payment History Details start -->

	
	
	<script type="text/javascript">
		function lenderPaymentHistory(talimar_loan, lender)
		{
		 	var result = '';
		 	
		 	$.ajax({
				type : 'POST',
				url  : '<?php echo base_url()."Load_data/getLenderPaymentHistory";?>',
				data : {'talimar_loan':talimar_loan, 'lender':lender},
				beforeSend: function() {
				
					$('tbody#LenderPaymentDetailsHistory').html('<tr><td colspan="6"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></td></tr>');

					$('#lenderPaymentHistory').modal('show');
				},
				success : function(response){
					var json = JSON.parse(response);

					console.log(json);

					if(json.status > 0)
					{
						result = json.result;
						
					}
					else
					{
						result = '<tr><td colspan="6">No Payment History!</td></tr>';
					}

					$('tbody#LenderPaymentDetailsHistory').html(result);

					

				},
				error: function(xhr) { // if error occured
			        result = '<tr><td colspan="6">No Payment History!</td></tr>';
			        $('tbody#LenderPaymentDetailsHistory').html(result);
			        
			    },
			    complete: function() {
			    	if(result=='')
			    	{
			    		result = '<tr><td colspan="6">No Payment History!</td></tr>';
			    		$('tbody#LenderPaymentDetailsHistory').html(result);
			    	}else{
			    		$('tbody#LenderPaymentDetailsHistory').html(result);
			    	}
			    	
			    },	    
			});	
		 }
	</script>



	<!-- Lender Payment History Details end -->
		
		<!-------------------END OF MODAL Assignments --------------->
		
		<!-------------Start Modal Estimate Lender----------------------->
		
		<div class="modal fade" id="estimate_lender" tabindex="-1" role="basic" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
									<form method="POST" action="<?php echo base_url();?>estimate_lender_cost" >
									
										<!-----------Hidden fields----------->
										<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
										<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
										<!-----------End of hidden Fields---->
									
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Estimated Lender Costs</h4>
										</div>
										<div class="modal-body">
											<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" id="estimate_lender_table">
												<thead>
													<tr>
														<th>&nbsp;</th>
														<th>Items</th>
														<th>Cost</th>
													</tr>
												</thead>
												<tbody>
													<?php
													if(count($servicing_estimate_lender) > 0)
													{
														foreach($servicing_estimate_lender as $row)
														{
															?>
															<tr>
																<td><a class="estimate_lender_remove"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
																
																<td><input name="items[]" value = '<?php echo $row->items; ?>' ></td>
																
																<td><input class="number_only amount_format autosum_estimate_l_cost" name="amount[]" value = '<?php echo '$'.number_format($row->amount,2); ?>' ></td>
																
															</tr>
															<?php
														}
													}
													else
													{
													?>
													<tr>
														<td><a class="estimate_lender_remove"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
														<td><input name="items[]"></td>
														<td><input class="number_only amount_format autosum_estimate_l_cost" name="amount[]"></td>
													</tr>
													<?php
													}
													?>
												</tbody>
												<tfoot>
													<tr>
														<th></th>
														<th id="total_foot_estimate_lender">Total</th>
														<th><input id="total_estimate_lender_cost"></th>
													</tr>
												</tfoot>
											</table>
											 
											 <div class="add_rows_button">
												<a class="btn blue shape_rectangle" id="estimate_lender_addrow">Add</a>
											 </div>
											 
											 <table id="estimate_lender_hidden_table" style="display:none">
												<tr>
													<td><a class="estimate_lender_remove"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
													<td><input name="items[]"></td>
													<td><input class="number_only amount_format autosum_estimate_l_cost" name="amount[]" onchange="autosum_estimate_lender_cost(this)"></td>
												</tr>
											 </table>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn default" data-dismiss="modal">Close</button>
											<button type="submit" class="btn blue">Save</button>
										</div>
									</form>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							<!-- /.modal -->
		
		<!-------------End Modal Estimate Lender----------------------->




<!-------------------------------ADDITIONAL BROKER DATA MODAL----------------------->
	
<!-------------------------END OF ADDITIONAL BROKER DATA MODAL----------------------->		
		<!---- Start Modal Print Loan document -->
		<!-- Add css for top section checkbox design issue resolve Add Date -> 24-03-2021 -->
		
		
		<!---- End Modal Print Loan document -->
		
		<!-- Assigment print model -------> 
		
		
		<script type="text/javascript">
			
			
			$('body').on('change', 'form#assSignNowdoc input[type="checkbox"]', function(){ 

				if ($('#assSignNowdoc :checkbox:checked').length > 0){
				    //alert('1');
				    $('#assSignNowdoc #signbtn').attr('disabled',false);
				    $('#assSignNowdoc #printbtn').attr('disabled',false);
				}else {
					//alert('0');
					$('#assSignNowdoc #signbtn').attr('disabled',true);
					$('#assSignNowdoc #printbtn').attr('disabled',true);
				}

			});
		</script>
		<!-- /.modal -->
		

		
		<!-- End Assigment print model -------> 
		
		<!--  Modal Assigment  -->
		
		<div class="modal fade" id="ach_deposite" tabindex="-1" role="basic" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form method="POST" action="<?php echo base_url();?>form_ach_deposite">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<!--<h4 class="modal-title">Loan #:<?php echo $modal_talimar_loan; ?></h4>-->
							<h4 class="modal-title">Loan: <?php echo $property_addresss; ?></h4>
					</div>
					<div class="modal-body">
						<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								ACH Deposit
							</div>
							
						</div>
						<div class="portlet-body form">
							
								<div class="form-body">
								
								<!-----------Hidden fields----------->
								<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
								<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
								<!-----------End of hidden Fields---->
								
									<div class="form-group">
										<label>Account Name:</label>
										<input type="text" class="form-control" name="account_name" value="<?php echo isset($fetch_ach_deposite_details[0]->account_name) ? $fetch_ach_deposite_details[0]->account_name : ''; ?>">
									</div>
									
									<div class="form-group">
										<label>Bank Name:</label>
										<input type="text" class="form-control" name="bank_name" value="<?php echo isset($fetch_ach_deposite_details[0]->bank_name) ? $fetch_ach_deposite_details[0]->bank_name : ''; ?>" >
									</div>
									
									<div class="form-group">
										<label>Account #:</label>
										<input type="text" class="form-control" name="account_number" value="<?php echo isset($fetch_ach_deposite_details[0]->account_number) ? $fetch_ach_deposite_details[0]->account_number : ''; ?>" >
									</div>
									
									<div class="form-group">
										<label>Routing Number:</label>
										<input type="text" class="form-control" name="routing_number" value="<?php echo isset($fetch_ach_deposite_details[0]->routing_number) ? $fetch_ach_deposite_details[0]->routing_number : ''; ?>" >
									</div>
									
									<div class="form-group">
										<label>Account Type:</label>
										<select type="text" class="form-control" name="account_type">
											<?php 
											foreach($ach_account_type_option as $key => $row)
											{
												?>
												<option value="<?php echo $key; ?>"  <?php if(isset($fetch_ach_deposite_details[0]->account_type)){ if($fetch_ach_deposite_details[0]->account_type == $key){ echo 'selected'; } } ?> ><?php echo $row;?></option>
												<?php 
											}
											?>
										</select>
									</div>
									
								</div>
								<!--
								<div class="form-actions">
									<button type="submit" class="btn blue">Save</button>
									<button type="button" class="btn default">Cancel</button>
								</div>
								-->
							
						</div>
					</div>
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn blue" value="submit">Save</button>
					</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		
		<!--End Modal Assigment  -->
		
		
		<!---  Email assigment modals   ---->
		
		
		
		<!---  End of Email assigment modals   ---->
		
		<!---Lender Approval ---->
		

		

		<script type="text/javascript">

			function deleteLenderApp(appID){

				if(confirm('Are you sure to remove this?')){

					$.ajax({

						type : 'POST',
						url  : '<?php echo base_url()."New_loan_data/removeLenderappData";?>',
						data : {'rowid':appID },
						success : function(response){

							$('#lender_approval_main table tr#row_'+appID).remove();
						}
					});
				}

			}

			function MoreLenderApp(appID){

				$('#lender_approval_view textarea[name="description"]').val('');
				$('#lender_approval_view input[name="date_req"]').val('');
				$('#lender_approval_view input[name="date_complete"]').val('');
				$('#lender_approval_view input[name="rowid"]').val('new');

				if(appID == 'new'){

					$('#lender_approval_view table input[type="checkbox"]').attr('checked',false);

					$('#lender_approval_view').modal('show');

				}else{

					LenderAppchkbox(appID); //call new function

					//run ajax code here...
					$.ajax({

						type : 'POST',
						url  : '<?php echo base_url()."New_loan_data/get_loanApp_info";?>',
						data : {'rowid':appID },
						success : function(response){

							

							var data = JSON.parse(response);

							if(data[0].LP_date_req === '0000-00-00'){
								var date_Req = '';
							}else{
								var date_Req = formatDate(data[0].LP_date_req);
							}

							if(data[0].LP_date_complete === '0000-00-00'){
								var date_complete = '';
							}else{
								var date_complete = formatDate(data[0].LP_date_complete);
							}

							$('#lender_approval_view select[name="status"] option[value="'+data[0].LP_status+'"]').attr('selected',true);
							$('#lender_approval_view select[name="request"] option[value="'+data[0].LP_request+'"]').attr('selected',true);
							$('#lender_approval_view textarea[name="description"]').val(data[0].LP_description);
							$('#lender_approval_view input[name="date_req"]').val(date_Req);
							$('#lender_approval_view input[name="date_complete"]').val(date_complete);

							$('#lender_approval_view input[name="rowid"]').val(data[0].id);

							$('#lender_approval_view').modal('show');

						}
					});
				}
			}

			function LenderAppchkbox(appID){

				$('#lender_approval_view table input[type="checkbox"]').attr('checked',false);
            	$('#lender_approval_view table input[type="checkbox"]').attr('checked',false);
            	$('#lender_approval_view table input[type="checkbox"]').attr('checked',false);
            	$('#lender_approval_view table input[type="checkbox"]').attr('checked',false);

				$.ajax({

						type : 'POST',
						url  : '<?php echo base_url()."New_loan_data/get_loanApp_chkinfo";?>',
						data : {'rowid':appID },
						success : function(response){

							var data = JSON.parse(response);
							$.each(data, function(key,val) {
					            //alert(key+val.chk_value);

					            var mykey = key + 1;

					            if(val.chk_value == 'No Response'){
					            	$('#lender_approval_view table input#nores_'+mykey).attr('checked',true);
					            	$('#lender_approval_view table input#Reques_'+mykey).attr('checked',false)
					            	$('#lender_approval_view table input#appro_'+mykey).attr('checked',false)
					            	$('#lender_approval_view table input#deny_'+mykey).attr('checked',false)
					            }else if(val.chk_value == 'Deny'){
					            	$('#lender_approval_view table input#deny_'+mykey).attr('checked',true);
					            	$('#lender_approval_view table input#nores_'+mykey).attr('checked',false)
					            	$('#lender_approval_view table input#Reques_'+mykey).attr('checked',false)
					            	$('#lender_approval_view table input#appro_'+mykey).attr('checked',false)
					            }else if(val.chk_value == 'Approved'){
					            	$('#lender_approval_view table input#appro_'+mykey).attr('checked',true);
					            	$('#lender_approval_view table input#nores_'+mykey).attr('checked',false)
					            	$('#lender_approval_view table input#Reques_'+mykey).attr('checked',false)
					            	$('#lender_approval_view table input#deny_'+mykey).attr('checked',false)
					            }else if(val.chk_value == 'Requested'){
					            	$('#lender_approval_view table input#Reques_'+mykey).attr('checked',true);
					            	$('#lender_approval_view table input#nores_'+mykey).attr('checked',false)
					            	$('#lender_approval_view table input#appro_'+mykey).attr('checked',false)
					            	$('#lender_approval_view table input#deny_'+mykey).attr('checked',false)
					            }else{
					            	$('#lender_approval_view table input#nores_'+mykey).attr('checked',false);
					            	$('#lender_approval_view table input#Reques_'+mykey).attr('checked',false);
					            	$('#lender_approval_view table input#appro_'+mykey).attr('checked',false);
					            	$('#lender_approval_view table input#deny_'+mykey).attr('checked',false);
					            }
					        });
						}
					});
			}

			function formatDate(date) {
			     var d = new Date(date),
			         month = '' + (d.getMonth() + 1),
			         day = '' + d.getDate(),
			         year = d.getFullYear();

			     if (month.length < 2) month = '0' + month;
			     if (day.length < 2) day = '0' + day;

			     return [month, day, year].join('-');
			}
			
			function hidedenybox(that,key){

				if($(that).is(':checked')){

					$('#lender_approval_view input#deny_'+key).attr('disabled',true);
					$('#lender_approval_view input#nores_'+key).attr('disabled',true);
					$('#lender_approval_view input#Reques_'+key).attr('disabled',true);
				}else{
					$('#lender_approval_view input#deny_'+key).attr('disabled',false);
					$('#lender_approval_view input#nores_'+key).attr('disabled',false);
					$('#lender_approval_view input#Reques_'+key).attr('disabled',false);
				}
			}

			function hideRequesbox(that,key){

				if($(that).is(':checked')){

					$('#lender_approval_view input#deny_'+key).attr('disabled',true);
					$('#lender_approval_view input#nores_'+key).attr('disabled',true);
					$('#lender_approval_view input#appro_'+key).attr('disabled',true);
				}else{
					$('#lender_approval_view input#deny_'+key).attr('disabled',false);
					$('#lender_approval_view input#nores_'+key).attr('disabled',false);
					$('#lender_approval_view input#appro_'+key).attr('disabled',false);
				}
			}

			function hideapprobox(that,key){

				if($(that).is(':checked')){

					$('#lender_approval_view input#appro_'+key).attr('disabled',true);
					$('#lender_approval_view input#nores_'+key).attr('disabled',true);
					$('#lender_approval_view input#Reques_'+key).attr('disabled',true);
				}else{
					$('#lender_approval_view input#appro_'+key).attr('disabled',false);
					$('#lender_approval_view input#nores_'+key).attr('disabled',false);
					$('#lender_approval_view input#Reques_'+key).attr('disabled',false);
				}
			}

			function hideapproordenybox(that,key){

				if($(that).is(':checked')){

					$('#lender_approval_view input#appro_'+key).attr('disabled',true);
					$('#lender_approval_view input#deny_'+key).attr('disabled',true);
					$('#lender_approval_view input#Reques_'+key).attr('disabled',true);
				}else{
					$('#lender_approval_view input#appro_'+key).attr('disabled',false);
					$('#lender_approval_view input#deny_'+key).attr('disabled',false);
					$('#lender_approval_view input#Reques_'+key).attr('disabled',false);
				}
			}


		</script>
		

		<!---status  modals---->
		
		
		
		<!---End of status  modals---->



		<!----------------Start of Add lender lists -------------------->
		
		<!----------------End of Add lender lists -------------------->
		
		
		
		
		<!----------------------------Fee Disbursement Modal------------------------------->
		
		<div class="modal fade bs-modal-lg" id="fee_disbursement" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<!--<h4 class="modal-title">Fee Disbursement: #<?php echo $modal_talimar_loan; ?></h4>-->
						<!--<h4 class="modal-title">Other Servicing Fee: #<?php// echo $modal_talimar_loan; ?></h4>-->
						<h4 class="modal-title">Other Servicing Fee: <?php echo $property_addresss; ?></h4>
					</div>
					<form method="POST" action="<?php echo base_url();?>form_fee_disbursement">
					<!-----------Hidden fields----------->
							<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
							<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
					<!-----------End of hidden Fields---->
					<div class="modal-body">
						 <div class="rc_class">
							<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" id="td_width_same">
							
								<thead>
								<tr>
									<th></th>
									<th>TaliMar</th>
									<!--<th>Loan Servicer</th>-->
									<!--<th>Sub Servicer</th>-->
									<th>Lender(s)</th>
								</tr>
								</thead>
								
								<tbody>
								<?php
									$lendercnt = 0;
									
									foreach($fee_disbursement_list as $key => $row)
									{
										$fee_disbursement_item = isset($fee_disbursement_data) ? $fee_disbursement_data[$key]->items : $row ;
										
										if($fee_disbursement_item == 'Late Fee')
										{
											$fieldvalue = "late_fee";
											
											$broker_value = isset($fee_disbursement_data) ? number_format($fee_disbursement_data[$key]->broker) : '50';
											
											
											
											
											$loan_amount = $fetch_loan_result[0]->loan_amount;
											if($loan_amount <= 500000)
											{
												$fess_service_charge = 0;
											}
											elseif($loan_amount >= 500001  && $loan_amount <= 2000000)
											{
												$fess_service_charge = 25;
											}
											elseif($loan_amount >= 2000001)
											{
												$fess_service_charge = 15;
											}
											$loan_servicer_value = $fess_service_charge;
											
											$lender_value = 100 - $broker_value - $loan_servicer_value;
										

										}
										else if($fee_disbursement_item == 'Extension Fee')
										{
											$fieldvalue = "extention_fee";
											
											$broker_value = isset($fee_disbursement_data) ? number_format($fee_disbursement_data[$key]->broker) : '75';
										
											
											$loan_servicer_value = isset($fee_disbursement_data) ? number_format($fee_disbursement_data[$key]->loan_servicer) : '0';
											$lender_value = 100 - $broker_value - $loan_servicer_value;

										}
										else if($fee_disbursement_item == 'Default Rate')
										{
											$fieldvalue = "default_rate";
											
											
											$broker_value = isset($fee_disbursement_data) ? number_format($fee_disbursement_data[$key]->broker): '50';
										
											
											$loan_servicer_value = isset($fee_disbursement_data) ? number_format($fee_disbursement_data[$key]->loan_servicer) : '0';
											$lender_value = 100 - $broker_value - $loan_servicer_value;

										}
										else if($fee_disbursement_item == 'Pre Payment Penalty')
										{
											$fieldvalue = '';
											$broker_value = isset($fee_disbursement_data) ? number_format($fee_disbursement_data[$key]->broker) : '0';
										
											
											$loan_servicer_value = isset($fee_disbursement_data) ? number_format($fee_disbursement_data[$key]->loan_servicer) : '0';
											
											$lender_value = 100 - $broker_value - $loan_servicer_value;
										}
										else if($fee_disbursement_item == 'Minimum Interest')
										{
											$fieldvalue = "min_interest";
											
											
											$broker_value = isset($fee_disbursement_data) ? number_format($fee_disbursement_data[$key]->broker) : '0';
										
											
											$loan_servicer_value = isset($fee_disbursement_data) ? number_format($fee_disbursement_data[$key]->loan_servicer) : '0';
											$lender_value = 100 - $broker_value - $loan_servicer_value;
										}
										else
										{
											$fieldvalue = '';
											
											$broker_value = isset($fee_disbursement_data) ? number_format($fee_disbursement_data[$key]->broker): '0';
										
											
											$loan_servicer_value = isset($fee_disbursement_data) ? number_format($fee_disbursement_data[$key]->loan_servicer) : '0';
											$lender_value = 100 - $broker_value - $loan_servicer_value;
										}
								?>
									<tr>
										<input type="hidden" class="form-control" name="items[]" value="<?php echo isset($fee_disbursement_data) ? $fee_disbursement_data[$key]->items : $row ;?>">
										<th><?php echo isset($fee_disbursement_data) ? $fee_disbursement_data[$key]->items : $row ;?>:
										<?php 
											if($fieldvalue){ ?>
											<input type = "hidden" name = "custom_field[]" value = "<?php echo $fieldvalue;?>" />	
												
										<?php	}
										?>
										
										</th>
										
										<td >
										
										<input class="number_only form-control" type="text" id = "fee_dis_broker_<?php echo $key;?>" name="broker[]" value="<?php echo $broker_value.'%';?>" onchange="fee_distibution_form(this,<?php echo $key;?>)"></td>
											
										<!--<td>
											<!--<span id = "loan_servicer_label_<?php echo $key;?>"><?php echo $loan_servicer_value.'%';?></span>
											
										<input  class="form-control rate_format number_only" type="text" id = "loan_servicer_<?php echo $key;?>" name="loan_servicer[]" value="<?php if($loan_servicer_value=='0'){
											echo '0'.'%';
											$read='readonly';
										}else{echo $loan_servicer_value.'%'; $read='';}?>"<?php echo $read;?>></td>-->
										
										<td>
										<span id = "fee_dis_lender_label_<?php echo $key;?>"><?php echo $lender_value.'%';?></span>
											
										<input  class="form-control rate_format number_only" type="hidden" name="lender[]" value="<?php echo $lender_value.'%';?>" onchange="fee_lender_form(this,<?php echo $key;?>)"
										id="fee_dis_lender_<?php echo $key;?>"></td>
									
										
										
									</tr>
									
									<?php
									$lendercnt ++;
									}
									
									for ($x =1; $x <= $loan_assigment_count; $x++) {
										

										$basicfee = 55+(($x-1)  *10) ;
									} 
										
									// echo 'basic fee is: ' . $basicfee;
									// $fci_loan_setup = ($basicfee)+(($loan_assigment_count-1) * 10);
									
									$fci_loan_setup = ($basicfee);
									$del_loan_setup = (45)+(20 * $lendercnt) - 20;
									// echo 'lender count is: '.$lendercnt;
									
									if($loan_servicing_data[0]->fci_paid_option == '2'){
										$fci_checked2 =  'checked';
										$fci_checked1 =  '';
									
									}else{
										$fci_checked2 =  '';
										$fci_checked1 =  'checked';
									}
									if($loan_servicing_data[0]->del_paid_option == '2'){
										$del_checked2 =  'checked';
										$del_checked1 =  '';
									}else{
										$del_checked2 =  '';
										$del_checked1 =  'checked';
									}
									?>
									
								</tbody>
							</table>
							
						 </div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn blue">Save</button>
					</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		
		<!----------------------------End of Fee Disbursement Model-------------------------------------->
		
		
		

<?php 	} 
		else{
		
?>
			<div class="modal fade bs-modal-sm" id="property" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Alert</h4>
						</div>
						<div class="modal-body">
							 Please Select Loan First Or Create Loan
						</div>
						<div class="modal-footer">
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
							
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
<?php
		}
?>
	
<form action="<?php echo base_url();?>download_del_toro_excel_file" method="POST" id="form_download_del_toro_excel_file">
	<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
</form>

<form action="<?php echo base_url();?>FCI_API/FCIBoardingDataCSVFILE" method="POST" id="form_fciboardingdata">
	<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
</form>

 <?php 
	
	// echo '<pre>';
	// print_r($servicing_contact_details);
	// echo '</pre>';
	//$json_contact = json_encode($fetch_escrow_contact);
 ?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>

<script type="text/javascript">

 //$('.chosen').chosen();

function submit_closing_data_once(){

	//alert('ll');  //don't remove this
	$('#frm_closing_statement_items_id').submit();
}

$(document).ready(function(){
	
	<?php
		$count = 0;
		
		
		foreach($fetch_property_home as $row){
			if($row->property_address != ''){
				$count++;
			}
		}
	?>
	var count_property = '<?php echo $count; ?>';
	if(count_property > 1){
		$('select#multi_property_count').val('1');
	}else{
		$('select#multi_property_count').val('2');
	}
});


$(document).ready(function(){
  /***phone number format***/
  $(".phone-format").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
    }
    var curchr = this.value.length;
    var curval = $(this).val();
    if (curchr == 3 && curval.indexOf("(") <= -1) {
      $(this).val("(" + curval + ")" + "-");
    } else if (curchr == 4 && curval.indexOf("(") > -1) {
      $(this).val(curval + ")-");
    } else if (curchr == 5 && curval.indexOf(")") > -1) {
      $(this).val(curval + "-");
    } else if (curchr == 9) {
      $(this).val(curval + "-");
      $(this).attr('maxlength', '14');
    }
  });
});



$(document).ready(function(){

	display_selectbox();
	//fetch_rap_contact(this);
	//fetch_property(this);

});

function display_selectbox()
{

	var select_1=$('select[name="loan_submited"]').val();
	var select_6=$('select[name="loan_board"]').val();
	//var select_2=$('select[name="boarding_fee_paid"]').val();
	//var select_3=$('select[name="servicing_disb"]').val();
	//var select_4=$('select[name="servicing_desp_verified"]').val();
//	var select_5=$('select[name="closing_checklist_complete"]').val();
	// var select_7=$('select[name="loan_posted"]').val();

	if(select_1=='1' && select_6=='1'){

	  $('select[name="board_complete"]').css({"pointer-events":"","cursor":"","background-color":""});

	}else{

	  $('select[name="board_complete"]').css({"pointer-events":"none","cursor":"not-allowed","background-color":"#e9e9e9"});

	}
}


/*Moved code to assets/js/load_data/loan_portfolio_popup/modal-event.js" on model responed append
/*$(document).ready(function(){
	
		var premilive_onload = $('#title select[name="prelim_receive"]').val();
         prelim_rec(premilive_onload);
	});*/


	function prelim_rec(thawwt){

         if(thawwt==1){
	        
		        $('#title select[name="prelim_approve"]').css({"pointer-events":"none","cursor":"not-allowed","background-color":"#e9e9e9"});
		  
				$('#title input[name="ptr_date"]').attr('readonly',true);
				$('#title input[name="title_order_no"]').attr('readonly',true);
				$('#title select[name="lender_policy_type"]').css({"pointer-events":"none","cursor":"not-allowed","background-color":"#e9e9e9"});
	
				$('#title input[name="policy_amount_in_percent"]').attr('readonly',true);
				$('#title input[name="policy_amount_in_dolar"]').attr('readonly',true);
				
				$('#title input[name="endorsement"]').attr('readonly',true);
				$('#title textarea[name="exception"]').attr('readonly',true);
				$('#title textarea[name="report_elimination"]').attr('readonly',true);
	



         }else{

		        $('#title select[name="prelim_approve"]').css({"pointer-events":"","cursor":"","background-color":""});
		  
				$('#title input[name="ptr_date"]').attr('readonly',false);
				$('#title input[name="title_order_no"]').attr('readonly',false);
				$('#title select[name="lender_policy_type"]').css({"pointer-events":"","cursor":"","background-color":""});
	
				$('#title input[name="policy_amount_in_percent"]').attr('readonly',false);
				$('#title input[name="policy_amount_in_dolar"]').attr('readonly',false);
				
				$('#title input[name="endorsement"]').attr('readonly',false);
				$('#title textarea[name="exception"]').attr('readonly',false);
				$('#title textarea[name="report_elimination"]').attr('readonly',false);
		
	

          }


}
	/*Moved code to assets/js/load_data/loan_portfolio_popup/modal-event.js" on model responed append
	/*$(document).ready(function(){

		var is_escrow_val = $('#escrow select[name="escrow_opened"]').val();
		hide_below_all_rows(is_escrow_val);
	});*/


	function hide_below_all_rows(that){

		if(that == 1){

			$('#escrow select[name="first_name"]').attr('readonly',true);
			$('#escrow input[name="company"]').attr('readonly',true);
			$('#escrow input[name="phone"]').attr('readonly',true);
			$('#escrow input[name="email"]').attr('readonly',true);
			$('#escrow input[name="address"]').attr('readonly',true);
			$('#escrow input[name="address_b"]').attr('readonly',true);
			$('#escrow input[name="city"]').attr('readonly',true);
			$('#escrow select[name="state"]').attr('readonly',true);
			$('#escrow input[name="zip"]').attr('readonly',true);
			$('#escrow input[name="escrow"]').attr('readonly',true);
			$('#escrow input[name="expiration"]').attr('readonly',true);
			$('#escrow input[name="net_fund"]').attr('disabled',true);
			$('#escrow input[name="tax_service"]').attr('disabled',true);
			$('#escrow input[name="record_assigment_close"]').attr('disabled',true);
			$('#escrow textarea[name="instruction"]').attr('readonly',true);

			
		}else{

			$('#escrow select[name="first_name"]').attr('readonly',false);
			$('#escrow input[name="company"]').attr('readonly',false);
			$('#escrow input[name="phone"]').attr('readonly',false);
			$('#escrow input[name="email"]').attr('readonly',false);
			$('#escrow input[name="address"]').attr('readonly',false);
			$('#escrow input[name="address_b"]').attr('readonly',false);
			$('#escrow input[name="city"]').attr('readonly',false);
			$('#escrow select[name="state"]').attr('readonly',false);
			$('#escrow input[name="zip"]').attr('readonly',false);
			$('#escrow input[name="escrow"]').attr('readonly',false);
			$('#escrow input[name="expiration"]').attr('readonly',false);
			$('#escrow input[name="net_fund"]').attr('disabled',false);
			$('#escrow input[name="tax_service"]').attr('disabled',false);
			$('#escrow input[name="record_assigment_close"]').attr('disabled',false);
			$('#escrow textarea[name="instruction"]').attr('readonly',false);

			
		}
	}

function send_closer_email(that){

	var closer = '<?php echo $servicing_contact_details[0]->loan_closer;?>';
	var investor_relations = '<?php echo $servicing_contact_details[0]->investor_relations;?>';
	var talimar_loan = '<?php echo $talimar_loan;?>';
	var servicing = '<?php echo $servicing;?>';

	var closing_status = '<?php echo $loan_servicing['closing_status'];?>';
	//alert(closer);
	//alert(investor_relations);
	//alert(closing_status);

	if(closer == ''){

		alert('Please add Loan Closer first!');
		return false;

	}else if(investor_relations == '')
	{
		alert('Please add investor Relation first');
		return false;

	}else if(closing_status != 2)
	{
		alert('Please Select Closing Status as Underwriting!');
		return false;

	}else{

		$.ajax({
					type : 'POST',
					url  : '<?php echo base_url()."Load_data/send_mail_to_user";?>',
					data : {'closer_id':closer, 'investor_relations':investor_relations, 'talimar_loan':talimar_loan, 'servicing':servicing},
					success : function(response){
						//alert(response);
						if(response == 1){
							window.location.reload();
						}
					}

		});
	}

}


function add_row_promissory_notte(that){
	
	var rowCount = $('#main_promissory_note').length;
	
	var aa = '<div class="row" id="main_promissory_note"><div class="col-md-11"><div class="col-md-6"><input type="text" name="forecloser_text[]" class="form-control"></div><div class="col-md-5"><input type="text" name="forecloser_digit[]" class="form-control number_only amount_format" onchange="sumtheforclosuredata(this);"></div><input type="hidden" name="hidden_id[]" value="new"></div></div>';
	$('#append_main_promissory_note11').append(aa);
	
}



$(document).ready(function(){

	recording_data_display(this);
});

function recording_data_display(that){

	var record_option = $('#record_option').val();
	//alert(record_option);
	//alert(that.value);
	if(record_option == 0)
	{
		$('.recorded_dependent input').val('');
		$('.recorded_dependent input').attr('disabled',true);
	}
	else
	{
		$('.recorded_dependent input').attr('disabled',false);
	}

}

function delete_this_note(that){
	
	var id = that.id;
	
			if(confirm('Are you sure to delete this Note?'))
			{
				$.ajax({
					type 	: 'POST',
					url		: '<?php echo base_url()."Load_data/delete_loan_notes";?>',
					data  	: { 'id':id },
					success : function(response)
					{
						if(response == '1')
						{
							$(that).parent().parent('tr').remove();
						}
					}
				})
			}
}

function delete_payment_gra_data(id){


	if(confirm('Are you sure to delete this Contact?'))
	{

		$.ajax({
					type 	: 'POST',
					url		: '<?php echo base_url()."Load_data/delete_payment_data";?>',
					data  	: { 'id':id },
					success : function(response)
					{
						if(response == '1')
						{
							$('#payment_gurrantor .delete_divv_'+id).remove();
							//location.reload();
						}
					}
				})

	}

}

function delete_affiliates_data(id){


	if(confirm('Are you sure to delete this Contact?'))
	{

		$.ajax({
					type 	: 'POST',
					url		: '<?php echo base_url()."Load_data/delete_affilated_contact";?>',
					data  	: { 'id':id },
					success : function(response)
					{
						if(response == '1')
						{
							//$('#affiliated_parties .delete_div_'+id).remove();
							alert('Affiliated contact remove Successfully!');
							window.location.reload();
						}
					}
				})

	}

}

function fetch_contact_payment(that){
	
	var c_id = that.value;
	//alert(c_id);
	$.ajax({
				type	: 'POST',
				url		: '<?php echo base_url()."Ajax_call/fetch_contact_detailss";?>',
				data	: {'contact_id':c_id},
				success	: function(response){
					if(response){
						var data = JSON.parse(response);
						
												
						$(that).parent().parent().parent().find('input#cont_phone').val(data.phone);

						 $(that).parent().parent().parent().find('input#cont_email').val(data.email);
						 $(that).parent().parent().parent().find('input#cont_city').val(data.contact_city);
						 $(that).parent().parent().parent().find('input#cont_state').val(data.contact_state);
						 $(that).parent().parent().parent().find('input#cont_zip').val(data.contact_zip);
						 $(that).parent().parent().parent().find('input#cont_address').val(data.contact_address);
						 $(that).parent().parent().parent().find('input#cont_unit').val(data.contact_unit);

						
						
						
					}
					
				}
	});
	
	
}

function fetch_contact_details(that){
	
	var c_id = that.value;
	//alert(c_id);
	$.ajax({
				type	: 'POST',
				url		: '<?php echo base_url()."Ajax_call/fetch_contact_details";?>',
				data	: {'contact_id':c_id},
				success	: function(response){
					if(response){
						var data = JSON.parse(response);
						
												
						$(that).parent().parent().parent().find('input#phone').val(data.phone);

						 $(that).parent().parent().parent().find('input#email').val(data.email);

						
						// $('div#affiliated_parties input#phone').val(data.phone);
						// $('div#affiliated_parties input#phone').text(data.phone);
						
						// $('div#affiliated_parties input#email').val(data.email);
						// $('div#affiliated_parties input#email').text(data.email);
						
					}
					
				}
	});
	
	
}



function fetch_contact_payments_append(thatd){
	
	var c_idd = thatd.value;

	// alert(c_idd);
	$.ajax({
				type	: 'POST',
				url		: '<?php echo base_url()."Ajax_call/fetch_contact_detailss";?>',
				data	: {'contact_id':c_idd},
				success	: function(response){
					if(response){
						var data = JSON.parse(response);
											
						$(thatd).parent().parent().parent().find('input#cont_phonee').val(data.phone);
						$(thatd).parent().parent().parent().find('input#cont_emaill').val(data.email);
						 $(thatd).parent().parent().parent().find('input#cont_cityy').val(data.contact_city);
						 $(thatd).parent().parent().parent().find('input#cont_statee').val(data.contact_state);
						 $(thatd).parent().parent().parent().find('input#cont_zipp').val(data.contact_zip);
						 $(thatd).parent().parent().parent().find('input#cont_addresss').val(data.contact_address);
						 $(thatd).parent().parent().parent().find('input#cont_unitt').val(data.contact_unit);
						
					}
					
				}
	});
	
	
}

function fetch_contact_details_append(thattt){
	
	var c_idd = thattt.value;
	// alert(c_idd);
	$.ajax({
				type	: 'POST',
				url		: '<?php echo base_url()."Ajax_call/fetch_contact_details";?>',
				data	: {'contact_id':c_idd},
				success	: function(response){
					if(response){
						var data = JSON.parse(response);
						
						
					 

						$(thattt).parent().parent().parent().find('input#phonee').val(data.phone);
						$(thattt).parent().parent().parent().find('input#emaill').val(data.email);

						// $('div#affiliated_parties input#phonee').val(data.phone);
						// $('div#affiliated_parties input#phonee').text(data.phone);
						
						// $('div#affiliated_parties input#emaill').val(data.email);
						// $('div#affiliated_parties input#emaill').text(data.email);
						
					}
					
				}
	});
	
	
}
function contact_details(that){
	
	var contact_id = that.value;

	$.ajax({
		
				type	: 'post',
				url		: '<?php echo base_url()."Ajax_call/fetch_contact_details";?>',
				data	: {'contact_id':contact_id},
				success	: function(response){
					if(response){
						
						var data = JSON.parse(response);
			
						$('input#c_phone').val(data.phone);
						//$('div#default_status input#c_phone').text(data.phone);
						
						$('input#c_email').val(data.email);
						//$('div#default_status input#c_email').val(data.email);
					}
				}
	});
	
	
}

function delete_this_item(that){
	
	var id = that.id;
	//alert(id);
	if(confirm('Are you sure to delete this item?'))
	{
		$.ajax({
				type	: 'post',
				url		: '<?php echo base_url()."Load_data/delete_underwriting_item";?>',
				data	: {'id':id},
				success	: function(response){
					if(response == '1'){
						$(that).parent().parent().parent('tr').remove();
					}
				}
		});
	}
	
}


function delete_extension_data(id){

	if(confirm('Are you sure to delete this extension?'))
	{
		$.ajax({
				type	: 'post',
				url		: '<?php echo base_url()."Load_data/delete_extension_section";?>',
				data	: {'ids':id},
				success	: function(response){
					if(response == '1'){
						window.location.reload();
					}
				}
		});
	}
}

function delete_forclosure_data(that){
   
var id=$(that).attr('id');
   if(id!='new'){

	if(confirm('Are you sure to delete this extension?'))
	{
		$.ajax({
				type	: 'post',
				url		: '<?php echo base_url()."Load_data/delete_for_section";?>',
				data	: {'ids':id},
				success	: function(response){
					if(response == '1'){
						$(that).parent().parent().parent().remove();
					}
				}
		});
	}

}else{

$(that).parent().parent().parent().remove();

 }

}


function change_secure_dot_val(that,id){
	var ids = id;

	//alert(ids);
	if($(that).is(':checked')){
		var closing_date = '<?php echo date('m-d-Y', strtotime($fetch_loan_result[0]->loan_funding_date));?>';
		//alert(closing_date);
		$('input#start_date_val_'+ids).val(closing_date);
	}else{
		//alert('0');
		$('input#start_date_val_'+ids).val('');
	}
	
}

function change_checklist_value(that,key){
	if($(that).is(':checked'))
	{
		$(that).parent().parent().parent().parent('tr').find('input#checklist').val('1');
		$(that).parent().parent().parent().parent('tr').find('input#onlyone1_'+key).prop('disabled',true);
	}
	else
	{
		$(that).parent().parent().parent().parent('tr').find('input#checklist').val('0');
		$(that).parent().parent().parent().parent('tr').find('input#onlyone1_'+key).prop('disabled',false);
	}
}

function change_not_applicable_val(that,key){
	if($(that).is(':checked'))
	{
		$(that).parent().parent().parent().parent('tr').find('input#not_applicable_val').val('1');
		$(that).parent().parent().parent().parent('tr').find('input#onlyone_'+key).prop('disabled',true);
	}
	else
	{
		$(that).parent().parent().parent().parent('tr').find('input#not_applicable_val').val('0');
		$(that).parent().parent().parent().parent('tr').find('input#onlyone_'+key).prop('disabled',false);
	}
}

function underwriting_required1(that){
	if($(that).is(':checked'))
	{		
		$(that).parent().parent('tr').find('input.underwriting_required_checkbox').val('1');
	}
	else
	{		
		$(that).parent().parent('tr').find('input.underwriting_required_checkbox').val('0');
	}
}

function underwriting_received1(that){
		
		if($(that).is(':checked'))
		{
			
			$(that).parent().parent('tr').find('input.underwriting_received').val('1');
		}
		else
		{
			$(that).parent().parent('tr').find('input.underwriting_received').val('0');
		}
}

function checkbox_single(that){
	if($(that).is(':checked')){
		
		$('input#servicing_chklist').val('1');
	}else{
		
		$('input#servicing_chklist').val('0');
	}
		
}

//function for marketing in loan servicing...

$(document).ready(function(){
	
	var val = $('div#marketing_option select[name="market_trust_deed"]').val();
	change_market_trust_deed(val);
});

function change_market_trust_deed(that){
	var val1 = that;
	//alert(val);
	if(val1 == '2'){
		$('input#available_disable').attr('disabled',true);
		$('input#trust_websites').val('0');
	}else{
		$('input#available_disable').attr('disabled',false);
	}
}


function trust_deed_website(that){
	
	if($(that).is(':checked')){
		$('input#trust_websites').val('1');
		
	}else{
		$('input#trust_websites').val('0');
		
		
	}
	
}

function trust_deed_no_address(that){
	
	if($(that).is(':checked')){
		$('input#no_address').val('1');
		
	}else{
		$('input#no_address').val('0');
		
		
	}
	
}

function trust_deed_website_not_applicable(that){
	
	if($(that).is(':checked')){
		
		$('input#trust_websites_not_applicable').val('1');
	}else{
		$('input#trust_websites_not_applicable').val('0');
	}
	
}

function checkbox_post_trust_deed(that){

	if($(that).is(':checked')){
		$('input#post_trust_deed').val('1');
		
	}else{
		$('input#post_trust_deed').val('0');
		
	}
}

function release_social_medias(that){
	
	if($(that).is(':checked')){
		$('input#release_media').val('1');
	}else{
		$('input#release_media').val('0');
	}
	
}

function release_social_medias_not_applicable(that){
	
	if($(that).is(':checked')){
		
		$('input#release_media_not_applicable').val('1');
	}else{
		$('input#release_media_not_applicable').val('0');
	}
	
}



function loan_closing_posting(tm){
	
	if($(tm).is(':checked')){
		
		$('input#loan_closing_postin').val('1');
	}else{
		$('input#loan_closing_postin').val('0');
	}
	
}

function loan_closing_not_posting(tnm){
	
	if($(tnm).is(':checked')){
		
		$('input#loan_closing_not_postin').val('1');
	}else{
		$('input#loan_closing_not_postin').val('0');
	}
	
}


function posted_on_websites(that){
	
	if($(that).is(':checked')){
		$('input#post_website').val('1');
	}else{
		$('input#post_website').val('0');
	}
	
}

function posted_on_websites_not_applicable(that){
	
	if($(that).is(':checked')){
		
		$('input#post_website_not_applicable').val('1');
	}else{
		$('input#post_website_not_applicable').val('0');
	}
	
}

function posted_site_signs(that){
	
	if($(that).is(':checked')){
		$('input#posted_sign').val('1');
	}else{
		$('input#posted_sign').val('0');
	}
	
}

function posted_site_signs_not_applicable(that){
	
	if($(that).is(':checked')){
		
		$('input#posted_sign_not_applicable').val('1');
	}else{
		$('input#posted_sign_not_applicable').val('0');
	}
	
}

function marketing_completed(that){

	if($(that).is(':checked')){
		
		$('input#market_complete').val('1');
	}else{
		$('input#market_complete').val('0');
	}
	
}

function available_releasee(thes){

	if($(thes).is(':checked')){
		
		$('input#available_release').val('1');
	}else{
		$('input#available_release').val('0');
	}
	
}

//end of marketing in loan servicing...

//function for Loan servicing default status model...

function borrower_outreach1(that){
	
	if($(that).is(':checked')){
		$('input#borrower_outreach_completed').val('1');
	}else{
		$('input#borrower_outreach_completed').val('0');
	}
}

function borrower_outreach2(that){
	
	if($(that).is(':checked')){
		$('input#borrower_outreach_estimate').val('1');
	}else{
		$('input#borrower_outreach_estimate').val('0');
	}
}

function nod_recorded1(that){
	
	if($(that).is(':checked')){
		$('input#nod_recorded_completed').val('1');
	}else{
		$('input#nod_recorded_completed').val('0');
	}
}

function nod_recorded2(that){
	
	if($(that).is(':checked')){
		$('input#nod_recorded_estimate').val('1');
	}else{
		$('input#nod_recorded_estimate').val('0');
	}
}

function nos_recorded1(that){
	
	if($(that).is(':checked')){
		$('input#nos_recorded_completed').val('1');
	}else{
		$('input#nos_recorded_completed').val('0');
	}
}

function nos_recorded2(that){
	
	if($(that).is(':checked')){
		$('input#nos_recorded_estimate').val('1');
	}else{
		$('input#nos_recorded_estimate').val('0');
	}
}

function sale_date1(that){
	
	if($(that).is(':checked')){
		$('input#sale_date_completed').val('1');
	}else{
		$('input#sale_date_completed').val('0');
	}
}
function sale_date2(that){
	
	if($(that).is(':checked')){
		$('input#sale_date_estimate').val('1');
	}else{
		$('input#sale_date_estimate').val('0');
	}
}

	
function underwriting_single(that){
	if($(that).is(':checked')){
		
		$('input#underwriting_chkbox').val('1');
		
	}else{
		
		$('input#underwriting_chkbox').val('0');
	}
		
}

function no_assignment_data(that){
	alert('No data available in Lender Assigment!')
}

function affiliation_value(that,key){
	
	var keys = that;
	if(keys == '6'){
		$('div#other_textbox_'+key).css('display','block');
		$('div#other_textbox_'+key).css('margin-top','15px');
	}else{
		$('div#other_textbox_'+key).css('display','none');
	}
}

$(document).ready(function(){
	<?php foreach($loan_affiliated_parties as $key => $row){ ?>
		var keys = $('div select[id="select_affiliation_<?php echo $key;?>"]').val();
		if(keys == '6'){
			$('div#other_textbox_<?php echo $key;?>').css('display','block');
			$('div#other_textbox_<?php echo $key;?>').css('margin-top','15px');	
		}else{
			$('div#other_textbox_<?php echo $key;?>').css('display','none');
		}
	<?php } ?>
});


function underwriting_required2(){
	alert('jsydtg');
} 
function closing_statement_loan_reserves_checkbox(that)
{
	if($(that).is(':checked'))
	{
		$(that).parent().parent().parent().find('input#loan_reserves').val(1);
	}
	else
	{
		$(that).parent().parent().parent().find('input#loan_reserves').val(0);
	}
}


  
	// $( "#demand_requested_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
	
  $(document).ready(function(){
	  // $.noConflict();
	  // alert("HI"+$.ui.version); // is it there yet?
	 
	$( ".accured_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
    $( "#apraisal_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
    // $( "#loan_funding_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
    $( "#expiration" ).datepicker({ dateFormat: 'mm-dd-yy' });
    $( "#escrow_expiration" ).datepicker({ dateFormat: 'mm-dd-yy' });
    $( "#nod_record_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
    $( "#nod_sale_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
    $( "#pay_off_resquested_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
    $( "#payoff_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
    $( "#cancelled_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
    
    $( "#date_inetrest_paid" ).datepicker({ dateFormat: 'mm-dd-yy' });
    $( ".open_datepicker" ).datepicker({ dateFormat: 'mm-dd-yy' });
    $( "#ptr_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
    $('table#table_wait_lists input[name="date_request[]"]').datepicker({ dateFormat: 'mm-dd-yy' });
    $( "#demand_requested_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
	
	// loan index datepicker
	$( "#modify_maturity_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
    $( "#first_payment_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
    $( "#loan_funding_date" ).datepicker({ dateFormat: 'mm-dd-yy',
		
		onSelect: function(){
         calculate_maturity_value();
	}
	});

	
	
   $( "#loan_document_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
   calculate_assignments_total_data();
  });
  
  //  Add row in notes modal 
  
     jQuery(document).ready(function() {
        var id = 0;
      //jQuery("#notes_addrow").click(function() {
      jQuery('body').on('click', "#notes_addrow", function(){
        id++;           
        var row = jQuery('#notes_table_hidden tr').clone(true);
        row.find("input:text").val("");
        row.attr('id',id); 
       row.appendTo('#table_notes');        
        row.insertBefore('table#table_notes > tbody > tr:first');      
        return false;
    });        
        
	  jQuery('body').on('click', ".notes_remove", function(){
	  $(this).parents("tr").remove();
	});
	});
	
  
  $(document).ready(function(){
	var payoff = $('#payoff_request').val();
	if(payoff == '1')
	{
		$('#payoff_yes_selected').css("display" , 'block');
	}
	else
	{
		$('#payoff_yes_selected').css("display" , 'none');
	}
	
	var  popup_modal_hit = '<?php echo $this->session->flashdata('popup_modal_hit'); ?>';
	
	//alert(popup_modal_hit);
	
	if(popup_modal_hit == 'appraisal_document'){
		var  popup_modal_prt = '<?php echo $this->session->flashdata('popup_modal_prt'); ?>';
		$('#property-data-home-page').modal('show');
		$('#property-'+popup_modal_prt+'').modal('show');
		$('#appraisal_document_'+popup_modal_prt+'').modal('show');
	}else if(popup_modal_hit == 'construcion_type_sources_and_uses'){
		$('#'+popup_modal_hit+'').modal('show');
	}else if(popup_modal_hit == 'LoanBoarding'){
		$('#loan_servicing').modal('show');
		$('#LoanBoarding').modal('show');
	}else if(popup_modal_hit == 'closing_statement'){
		$('#closing_statement_new11').modal('show');
	}else if(popup_modal_hit == 'loan_document'){
			$('#lender_loan_document').modal('show');
	}else if(popup_modal_hit == 'assignment'){
		
			$('#assigment_main').modal('show');
			$('#assigment').modal('show');
	}else if(popup_modal_hit == 'property_home'){
		
			$('#property-data-home-page').modal('show');
			
	}else if(popup_modal_hit == 'extension_load'){
		
			$('#loan_servicing').modal('show');
			$('#marketing_extention').modal('show');
			
	}else if(popup_modal_hit == 'property_data'){

		var property_home_id = '<?php echo $this->session->flashdata('property_home_id'); ?>';
		$('#property-data-home-page').modal('show');
		$('#property-'+property_home_id).modal('show');	
		$('#Encumbrances-'+property_home_id).modal('show');
			
	}else if(popup_modal_hit == 'property_data_cash'){

		var property_home_id = '<?php echo $this->session->flashdata('property_home_id'); ?>';
		$('#property-data-home-page').modal('show');
		$('#property-'+property_home_id).modal('show');	
		$('#cashflow-'+property_home_id).modal('show');	
	}
	else if(popup_modal_hit == 'comparable_sales'){
		var property_home_id = '<?php echo $this->session->flashdata('property_home_id'); ?>';

		$('#property-data-home-page').modal('show');
		$('#property-'+property_home_id).modal('show');	
		$('#comparable_sale-'+property_home_id).modal('show');
			
	}
	else if(popup_modal_hit == 'property_insurance_modal'){
		var property_home_id = '<?php echo $this->session->flashdata('property_home_id'); ?>';
		$('#property-data-home-page').modal('show');
		$('#property-'+property_home_id).modal('show');	
		$('#property_insurance-'+property_home_id).modal('show');
			
	}else if(popup_modal_hit == 'loan_draw_data'){
			
		$('#loan_reserve').modal('show');	
	}
	else if(popup_modal_hit == 'property_data_folder'){
		var property_home_id = '<?php echo $this->session->flashdata('property_home_id'); ?>';
		$('#property-data-home-page').modal('show');
		$('#property-'+property_home_id).modal('show');	
		$('#image-folder-'+property_home_id).modal('show');
			
	}
	

	else if(popup_modal_hit == 'upload_docu'){

          $('#assigment_main').modal('show');
			$('#assigment').modal('show');
			$('#savemodal_'+id).modal('show');
			
	}


	else if(popup_modal_hit == 'escrow')
	{
		$('#escrow').modal('show');
	}else if(popup_modal_hit == 'loan_assignment')
	{
		$('#assigment_main').modal('show');
	
	}else if(popup_modal_hit == 'marketing_option')
	{
		$('#loan_servicing').modal('show');
		$('#marketing_option').modal('show');
	
	}else if(popup_modal_hit == 'accured_charges_data')
	{
		$('#loan_servicing').modal('show');
		$('#accured_charges').modal('show');
	
	}
	else if(popup_modal_hit == 'loan_servicing')
	{
		$('#loan_servicing').modal('show');
	
	}else if(popup_modal_hit == 'Foreclosure_modal')
	{
		$('#loan_servicing').modal('show');
		$('#forcloser_status').modal('show');
	
	}else if(popup_modal_hit == 'ts_number')
	{
		$('#loan_servicing').modal('show');
		$('#forcloser_status').modal('show');
		$('#custom_add_foreclosure').modal('show');
		$('#ts_number').modal('show');
	
	}
	else if(popup_modal_hit == 'diligence_materials')
	{
		$('#diligence_materials').modal('show');
	
	}
	else if(popup_modal_hit == 'test')
	{
		$('#test').modal('show');
	}
	else if(popup_modal_hit == 'test')
	{
		$('#test').modal('show');
	}
	else if(popup_modal_hit == 'add_contact_note')
	{
		$('#add_contact_note').modal('show');
	}

		else if(popup_modal_hit == 'assignment_checkbox')
	{
		$('#status').modal('show');
	}

	else if(popup_modal_hit == 'checklisting')
	{
		$('#loan_closing_checklist').modal('show');
	}

else if(popup_modal_hit == 'waitless'){

		$('#assigment_main').modal('show');
		$('#assigment_wait_list').modal('show');
	var idd='<?php echo str_replace('/','',$loan_id);?>';
		load_ajax_wait_list_data(idd);
	}

	
});
  
  
$('body').on('change', "#payoff_request", function(){
	var payoff = this.value;
	if(payoff == '1')
	{
		$('#payoff_yes_selected').css("display" , 'block');
	}
	else
	{
		$('#payoff_yes_selected').css("display" , 'none');
	}
});


   $(document).ready(function() {
	 var loan_status 	= $('#loan_status').val();
	 // var condition 		= $('#service_condition').val();
	 if(loan_status == '1' || loan_status == '7')
	 {
		$('#loan_status_active_base').css('display', 'none');
		
		
		$('#service_paidoff_requested').css('display', 'none');
		$('a.gayab').attr('disabled', true);
		
		$('#paid_of_based').css('display', 'none');
		
		
		$('#dead_reason').css('display', 'none');
		$('#cancel_date').css('display', 'none');
		$('div#term_sheet_div').css('display','none');
		//$('#ter').css('display', 'block');
		// $('#condition_based_div').css('display', 'none');
		// $('#paid_of_based').css('display', 'none');
	 

	 }
	 else if(loan_status == '2')
	 {
		$('#loan_status_active_base').css('display', 'block');
		$('div#term_sheet_div').css('display','none');
		$('#condition_based_div').css('display', 'none');
		$('#dead_reason').css('display', 'none');
		$('#paid_of_based').css('display', 'none');
		$('#cancel_date').css('display', 'none');
		$('a.gayab').attr('disabled', true);
		//$('#ter').css('display', 'none');
		condition_check_default();
	 }
	 else if(loan_status == '3')
	 {
		 $('#loan_status_active_base').css('display', 'none');
		 $('#condition_based_div').css('display', 'none');
		 $('#service_paidoff_requested').css('display', 'none');
		 $('#paid_of_based').css('display', 'block');
		 $('#dead_reason').css('display', 'none');
		 $('#cancel_date').css('display', 'none');
		 $('a.gayab').attr('disabled', false);
		 $('div#term_sheet_div').css('display','none');
		 //$('#ter').css('display', 'none');
	 }
	 else if(loan_status == '4')
	 {
			$('#loan_status_active_base').css('display', 'none');
			$('#condition_based_div').css('display', 'none');
			$('#service_paidoff_requested').css('display', 'none');
			$('#paid_of_based').css('display', 'none');
			$('#dead_reason').css('display', 'block');
			$('#cancel_date').css('display', 'block');
			//$('#ter').css('display', 'none');
			$('a.gayab').attr('disabled', true);
			$('div#term_sheet_div').css('display','none');
	 }
	 else if(loan_status == '5')
	 {
			$('#loan_status_active_base').css('display', 'none');
			$('#condition_based_div').css('display', 'none');
			$('#service_paidoff_requested').css('display', 'none');
			$('#paid_of_based').css('display', 'none');
			$('#dead_reason').css('display', 'none');
			$('#cancel_date').css('display', 'none');
			$('a.gayab').attr('disabled', true);
			$('div#term_sheet_div').css('display','none');
			//$('#ter').css('display', 'none');
			
	 }
	 else if(loan_status == '6')
	 {
			$('#loan_status_active_base').css('display', 'none');
			$('#condition_based_div').css('display', 'none');
			$('#service_paidoff_requested').css('display', 'none');
			$('#paid_of_based').css('display', 'none');
			$('#dead_reason').css('display', 'none');
			$('#cancel_date').css('display', 'none');
			$('a.gayab').attr('disabled', true);
			$('div#term_sheet_div').css('display','block');

			//$('#ter').css('display', 'none');
			
	 }
	 
 });
  
 
$('body').on('change', "#loan_status", function(){	
	 var loan_status 	= $('#loan_status').val();
	 var condition 		= $('#service_condition').val();
	 if(loan_status == '1' || loan_status == '7')
	 {
		$('#loan_status_active_base').css('display', 'none');
		$('#loan_status_active_base input').val('');
		$('#loan_status_active_base select').val('');
		
		$('#service_paidoff_requested').css('display', 'none');
		$('#service_paidoff_requested input').val('');
		$('#service_paidoff_requested select').val('');
		
		$('#paid_of_based').css('display', 'none');
		$('#paid_of_based input').val('');
		
		$('#cancel_date').css('display', 'none');
		$('#dead_reason').css('display', 'none');
		$('a.gayab').attr('disabled', true);
		$('#dead_reason select').val('');
		$('div#term_sheet_div').css('display','none');
		//$('#ter').css('display', 'block');

	 }
	 else if(loan_status == '2')
	 {
		$('#loan_status_active_base').css('display', 'block');
		
		$('#condition_based_div').css('display', 'none');
		$('#condition_based_div input').val('');
		$('#condition_based_div select').val('');
		$('#cancel_date').css('display', 'none');
		$('#dead_reason').css('display', 'none');
		$('#dead_reason input').val('');
		$('#dead_reason select').val('');
		
		$('#paid_of_based').css('display', 'none');
		 
		$('a.gayab').attr('disabled', true);
		$('#paid_of_based input').val('');
		$('#paid_of_based select').val('');
		$('div#term_sheet_div').css('display','none');
		//$('#ter').css('display', 'none');
		condition_check_default();
	 }
	 else if(loan_status == '3')
	 {
		 $('#loan_status_active_base').css('display', 'none');
		 $('#loan_status_active_base input').val('');
		 $('#loan_status_active_base select').val('');
		 
		 $('#condition_based_div').css('display', 'none');
		 $('#condition_based_div input').val('');
		 $('#condition_based_div select').val('');
		 $('#cancel_date').css('display', 'none');
		 $('#service_paidoff_requested').css('display', 'none');
		 $('#service_paidoff_requested input').val('');
		 $('#service_paidoff_requested select').val('');
		 
		 $('#dead_reason').css('display', 'none');
		 $('#dead_reason input').val('');
		 $('#dead_reason select').val('');
		 
		 $('#paid_of_based').css('display', 'block');
		 $('a.gayab').attr('disabled', false);
		 $('div#term_sheet_div').css('display','none');
		//$('#ter').css('display', 'none');
		 
	 }
	 else if(loan_status == '4')
	 {
			$('#loan_status_active_base').css('display', 'none');
			$('#loan_status_active_base input').val('');
			$('#loan_status_active_base select').val('');
			
			$('#condition_based_div').css('display', 'none');
			$('#condition_based_div input').val('');
			$('#condition_based_div select').val('');
			
			$('#service_paidoff_requested').css('display', 'none');
			$('#service_paidoff_requested input').val('');
			$('#service_paidoff_requested select').val('');
			
			$('#paid_of_based').css('display', 'none');
			$('#paid_of_based input').val('');
			$('#paid_of_based select').val('');
			$('#cancel_date').css('display', 'block');
			$('#dead_reason').css('display', 'block');
			$('a.gayab').attr('disabled', true);
			$('div#term_sheet_div').css('display','none');
			//$('#ter').css('display', 'none');
	 }
	 else if(loan_status == '5')
	 {
			$('#loan_status_active_base').css('display', 'none');
			$('#loan_status_active_base input').val('');
			$('#loan_status_active_base select').val('');
			
			$('#condition_based_div').css('display', 'none');
			$('#condition_based_div input').val('');
			$('#condition_based_div select').val('');
			
			$('#service_paidoff_requested').css('display', 'none');
			$('#service_paidoff_requested input').val('');
			$('#service_paidoff_requested select').val('');
			
			$('#paid_of_based').css('display', 'none');
			$('a.gayab').attr('disabled', true);
			$('#paid_of_based input').val('');
			$('#paid_of_based select').val('');
			$('#cancel_date').css('display', 'none');
			$('#dead_reason').css('display', 'none');
			$('div#term_sheet_div').css('display','none');
			$('#dead_reason').val('');
			//$('#ter').css('display', 'none');
		
	 }
	 
	 	 else if(loan_status == '6')
	 {
			$('#loan_status_active_base').css('display', 'none');
			$('#loan_status_active_base input').val('');
			$('#loan_status_active_base select').val('');
			
			$('#condition_based_div').css('display', 'none');
			$('#condition_based_div input').val('');
			$('#condition_based_div select').val('');
			
			$('#service_paidoff_requested').css('display', 'none');
			$('#service_paidoff_requested input').val('');
			$('#service_paidoff_requested select').val('');
			
			$('#paid_of_based').css('display', 'none');
			$('a.gayab').attr('disabled', true);
			$('#paid_of_based input').val('');
			$('#paid_of_based select').val('');
			$('#cancel_date').css('display', 'none');
			$('#dead_reason').css('display', 'none');
			$('#dead_reason').val('');
			$('div#term_sheet_div').css('display','block');
			//$('#ter').css('display', 'none');
		
	 }
 });
 

$('body').on('change', "#checkbox_s_dot", function(){
	var result = this.value;
	if($(this).attr('checked'))
	{
		$(this).parent().parent().parent().find("input[name='secured_dot[]']").val('1');
	}
	else
	{
		$(this).parent().parent().parent().find("input[name='secured_dot[]']").val('0');
	}
	
	
});
 
 

$('body').on('change', ".checkbox_d_submit", function(){
	var result = this.value;
	if($(this).attr('checked'))
	{
		$(this).parent().parent().parent().find("input[name='disclosures_submit[]']").val('1');
	}
	else
	{
		$(this).parent().parent().parent().find("input[name='disclosures_submit[]']").val('0');
	}
	
	
});


$('body').on('change', ".checkbox_d_execute", function(){
	var result = this.value;
	if($(this).attr('checked'))
	{
		$(this).parent().parent().parent().find("input[name='disclosures_execute[]']").val('1');
	}
	else
	{
		$(this).parent().parent().parent().find("input[name='disclosures_execute[]']").val('0');
	}
	
});


$('body').on('change', ".checkbox_recived_loan_doc", function(){
	var result = this.value;
	if($(this).attr('checked'))
	{
		$(this).parent().parent().parent().find("input[name='recived_loan_doc[]']").val('1');
	}
	else
	{
		$(this).parent().parent().parent().find("input[name='recived_loan_doc[]']").val('0');
	}
	
});


$('body').on('change', ".checkbox_f_recieved", function(){
	var result = this.value;
	if($(this).attr('checked'))
	{
		$(this).parent().parent().parent().find("input[name='fund_received[]']").val('1');
	}
	else
	{
		$(this).parent().parent().parent().find("input[name='fund_received[]']").val('0');
	}
	
});


$('body').on('change', ".checkbox_a_submit", function(){
	var result = this.value;
	if($(this).attr('checked'))
	{
		$(this).parent().parent().parent().find("input[name='assigment_submit[]']").val('1');
	}
	else
	{
		$(this).parent().parent().parent().find("input[name='assigment_submit[]']").val('0');
	}
	
});



$('body').on('change', ".checkbox_s_submit", function(){
	if($(this).attr('checked'))
	{
		$(this).parent().parent().parent().find("input[name='servicer_submit[]']").val('1');
	}
	else
	{
		$(this).parent().parent().parent().find("input[name='servicer_submit[]']").val('0');
	}
	
	   
});


$('body').on('change', ".checkbox_fund_released", function(){
	if($(this).attr('checked'))
	{
		$(this).parent().parent().parent().find("input[name='fund_released[]']").val('1');
	}
	else
	{
		$(this).parent().parent().parent().find("input[name='fund_released[]']").val('0');
	}
	
	   
});


$('body').on('change', ".checkbox_servicer_confirm", function(){
	if($(this).attr('checked'))
	{
		$(this).parent().parent().parent().find("input[name='servicer_confirm[]']").val('1');
	}
	else
	{
		$(this).parent().parent().parent().find("input[name='servicer_confirm[]']").val('0');
	}
	
	   
});

$('body').on('change', ".checkbox_file_close", function(){
	if($(this).attr('checked'))
	{
		$(this).parent().parent().parent().find("input[name='file_close[]']").val('1');
	}
	else
	{
		$(this).parent().parent().parent().find("input[name='file_close[]']").val('0');
	}
	
	   
});



function function_assigment_checkbox(id)
{
	// alert(id);   
	var a = id.replace("checkbox","input");
	var value = $('#'+a).val();
	if(value == '1')
	{
		$('#'+a).val('0');
	}	
	else
	{
		$('#'+a).val('1');          
	}

}

function feath_loan_notes(that){
	
	var id = that.id;
	var contact_id =$(that).parent().parent('tr').find('input#ncontact_id').val();
	
		$.ajax({
			type	:'POST',
			url		:'<?php echo base_url()."Load_data/fetch_contact_notee_2";?>',
			data	:{'id':contact_id},
			success : function(response){
		 $('#edit_contact_note #contact_id').html('');
		 $('#edit_contact_note #contact_id').html(response);
		  
 	     
		
			}
				
		});
	$('div#edit_contact_note select#contact_id option').attr('selected',false);
	$.ajax({
		
			type	: 'POST',
			url		: '<?php echo base_url()."Load_data/fetch_notes_details";?>',
			data	: {'id':id},
			success	: function(response){
				
				if(response){
					//alert(response);
					
					var data = JSON.parse(response);
					
					$('div#edit_contact_note input#updata_id').val(data.id);
					$('div#edit_contact_note input#updata_id').text(data.id);
					
					$('div#edit_contact_note input#notes_date').val(data.notes_date);
					$('div#edit_contact_note input#notes_date').text(data.notes_date);
					
					$('div#edit_contact_note textarea#notes_text').val(data.notes_text);
					$('div#edit_contact_note textarea#notes_text').text(data.notes_text);
					
					$('div#edit_contact_note input#entry_by').val(data.entry_by);
					$('div#edit_contact_note input#entry_by').text(data.entry_by);
					
					
					$('div#edit_contact_note select#contact_id option[value="'+data.contact_id+'"]').attr('selected',true);
					
					$('div#edit_contact_note select#note_type option[value="'+data.loan_note_type+'"]').attr('selected',true);
					$('div#edit_contact_note select#com_type option[value="'+data.com_type+'"]').attr('selected',true);
					
					$('div#edit_contact_note').modal('show');
					
				}
			}
	});
}
 
 jQuery(document).ready(function() {
	 var condition = $('#service_condition').val();
	 var loan_status = $('#loan_status').val();
	 if(condition == '1' )
	 {
		$('#condition_based_div').css('display', 'block');
		if(loan_status == '2')
		{
		$('#service_paidoff_requested').css('display', 'block');
		}
	 }
	 else
	 {
		 $('#condition_based_div').css('display', 'none');
		 $('#service_paidoff_requested').css('display', 'none');
	 }
 });
 
 function condition_check_default()
 {
	 var condition = $('#service_condition').val();
	 var loan_status = $('#loan_status').val();
	 if(condition == '1' )
	 {
		$('#condition_based_div').css('display', 'block');
		if(loan_status == '2')
		{
		$('#service_paidoff_requested').css('display', 'block');
		}
	 }
	 else
	 {
		 $('#condition_based_div').css('display', 'none');
		 $('#service_paidoff_requested').css('display', 'none');
	 }
 }
 
 
$('body').on('change', "#service_condition", function(){
	 var condition = $('#service_condition').val();
	 var loan_status = $('#loan_status').val();
	 if(condition == '1' )
	 {
		$('#condition_based_div').css('display', 'block');
		if(loan_status == '2')
		{
		$('#service_paidoff_requested').css('display', 'block');
		}
	 }
	 else
	 {
		 $('#condition_based_div').css('display', 'none');
		 $('#service_paidoff_requested').css('display', 'none');
	 }
	 
 }); 
  //    --------- for calculate net income in url  talimar/loan_property
  
$('body').on('change', ".cal_amount", function(){
	  var income = replace_dollar($("#gross_monthly_income").val());
	  var expense = replace_dollar($("#gross_monthly_expense").val());
	  // alert(expense);
	  var net = income - expense - 0;
	  $("#net_income").val('$'+ number_format(net));
  }); 
  



function get_sum_val(value){
	var sum = 0;
	$("input[name='accured_amount[]']").map(function(){
		var amount = $(this).val();
		sum += + replace_dollar($(this).val());
		
	}).get();
	
	$('#accured_total_val').val(sum);
	var total =  $('#accured_total_val').val();
	 
	$('#accured_total').html('$' + total+'.00');
	// $('#accured_total').html('$' + total.toFixed(2));
		
		
	
}  
  //--------------------------------------------------------
  
	// ------- start underwriting tab function -----//  
	
	function add_underwriting_items(that){
		console.log(that);
		var id	= parseFloat(that.id);

		if(id <= '299')
		{
			var item_id = 301;
		}
		else
		{
			id++;
			var item_id = id;
		}

		that.id = item_id;
		
		var row = jQuery('.hide_samplerow tr').clone(true);
        row.find("input:text").val("");
      	row.find('input[name="input_items_val[]"]').val(item_id);
		$('tr#other').before(row);	
		

	}
	
	function add_checklist_items(that){
	
		var id	= that.id;
		//alert(id);
		//$('.hide_samplerow tr').find('input[name="input_type"]').val(id);
		var row = jQuery('.hide_samplerow_checklist tr').clone(true);
        row.find("input:text").val("");
       //row.appendTo($('tr#'+id+':after')); 
		$('#app_row_'+id+'').before(row);	
		//$('.th.padding-zero.hud1').css('padding','0px');
	}
	
	
	
  
  // ------- end underwriting tab function -----// 
  
  //  Add row in monthly payment modal
  
     jQuery(document).ready(function() {
        var id = 0;
      
      jQuery('body').on('click', "#m_s_addrow", function(){
        id++;           
        var row = jQuery('.samplerow tr').clone(true);
        row.find("input:text").val("");
        row.attr('id',id); 
        row.attr('class','ui-sortable-handle'); 
        row.appendTo('#monthly_statement_table');        
        return false;
    });        
        
  jQuery('body').on('click', ".m_s_remove", function(){
	  $(this).parents("tr").remove();
	});
});    
  //  Add row in Loan distribution modal
  
     jQuery(document).ready(function() {
        var id = 0;
      
      jQuery('body').on('click', "#distribution_addrow", function(){
        id++;           
        var row = jQuery('.distribution_hidden tr').clone(true);
        row.find("input:text").val("");
        row.attr('id',id); 
        row.attr('class','ui-sortable-handle'); 
        row.appendTo('#loan_distibution_table');        
        return false;
    });        
        
  $('.distribution_remove').on("click", function() {
	  $(this).parents("tr").remove();
	});
});   


 //  Add row in Ipound Account modal
  
     jQuery(document).ready(function() {
        var id = 0;
       
        
  $('.table_impound_accounts_remove').on("click", function() {
	  $(this).parents("tr").remove();
	});
});  

  

  //  Add row in Ecumbrances modal
  
     jQuery(document).ready(function() {
        var id = 0;
      
      jQuery('body').on('click', "#ecum_addrow", function(){
        id++;           
        var row = jQuery('.ecum_table tr').clone(true);
        row.find("input:text").val("");
        row.attr('id',id); 
        row.attr('class','lien_center_tr'); 
        row.appendTo('#table_ecumbrances');        
        return false;
    });       

	
	jQuery('body').on('click', "#current_lien_add", function(){
		// alert('hello');
        var row = jQuery('#current_ecumbrance_hidden_table tr').clone(true);
        row.appendTo('table#table_ecumbrances_current tbody');        
        return false;
    });      

	
        
  $('.ecum_remove').on("click", function() {
	 var id  = $(this).find('input[name="id"]').val();
	 // alert(id);
	 if(id)
	 {
		if (confirm('Are you sure to delete?')) {
			 
				$.ajax({
				 type 	: 'POST',
				 url 	: '<?php echo base_url()."delete_ecumbrance_id";?>',
				 data 	: {'id' : id},
				 success : function(response)
					 {
						 
					 }
				});
			 
			 $(this).parents("tr").remove();
			 
		}
		 
	 }
	 else
	 {
		 $(this).parents("tr").remove();
	 }
	
});
});

//  Add row in Closing statement table
  
     jQuery(document).ready(function() {
        var id = 0;
      
      jQuery('body').on('click', "#closing_statement_addrow", function(){
        id++;   
			//alert('kjk');
        var row = jQuery('.closing_statement_hidden_table tr').clone(true);
        row.find("input:text").val("");
        row.attr('id',id); 
        row.appendTo('#closing_statement_table');        
        return false;
    });        
        
  $('.closing_items_remove').on("click", function() {
  $(this).parents("tr").remove();
});
});

  //  Add row in Assignment modal
    
     jQuery(document).ready(function() {
      
        
  $('.assigment_remove').on("click", function() {
 
  $(this).parents("tr").remove();

});

});

	function delete_assigment(that){

	$(that).parent().parent('tr').remove();

	}
     
  
  	function fun_assigment_addrow(){

			
var ass_new_tr = '<tr>';

ass_new_tr +='<input type="hidden" name="this_id[]" value ="">';
ass_new_tr +='<td style="width:18%;">';
ass_new_tr +='<i  class="fa fa-trash assigment_remove" onclick="delete_assigment(this)" aria-hidden="true"></i>&nbsp;&nbsp;';
ass_new_tr +='<a onclick="go_to_lender(this)"><i class="fa fa-th" aria-hidden="true"></i></a>&nbsp;&nbsp;';
ass_new_tr +='<a data-toggle="modal" href="#assigment_print_data"><i class="fa fa-print" aria-hidden="true"></i></a>&nbsp;&nbsp;';
ass_new_tr +='<a data-toggle="modal" href="#assigment_doc_save"><i class="fa fa-save" aria-hidden="true"></i></a></td>';
ass_new_tr +='<td style="width:5%;">';
ass_new_tr +='<select id="lender_name" type = "text" name="lender_name[]" class="assigment_investor_select chosen_41"  onchange="assigment_investor_select_new(this)"  data-live-search="true"><option value="">Select One</option>';
<?php foreach($investor_all_data as $investor) { ?>
ass_new_tr +="<option value='<?php echo $investor->id;?>'><?php echo $investor->name; ?></option>";
<?php } ?>
ass_new_tr +='</select></td><td></td><td></td><td></td>';
ass_new_tr +='<td style="width:13%;">';
ass_new_tr +='<input type = "text" name="investment[]" id="investment" class="number_only assautosum_investment" onchange="autocal_investment(this)" ></td>';
ass_new_tr +='<td style="width:13%;">';
ass_new_tr +='<input type = "text" name="investmenttotal[]" id="investment" class="number_only assautosum_investment" onchange="autocal_investmenttotal(this);"></td>';
ass_new_tr +='<td style="width:9%;">';
ass_new_tr +='<input type = "text" name="percent_loan[]" id="percent_loan" class="readonly_gray" readonly></td>';
ass_new_tr +='<td style="width:9%;">';
ass_new_tr +='<input type = "text" name="invester_yield_percent[]" id="invester_yield_percent" class="readonly_gray" readonly ></td>';
ass_new_tr +='<td style="width:8%;"><div class="row_td_print">';
ass_new_tr +='<input type = "text" name="payment[]" id="payment" class="readonly_gray" readonly >';

ass_new_tr +='</div>';
ass_new_tr +='</td>';
ass_new_tr +='<td style="width:8%;"><div class="row_td_print">';
ass_new_tr +='<input type = "text" name="paymenttotal[]" id="paymenttotal" class="readonly_gray" readonly >';
ass_new_tr +='</div>';
ass_new_tr +='</td>';

ass_new_tr +='<td></td>';
ass_new_tr +='</tr>';

			$(".chosen_41").chosen( $('#table_assigment').append(ass_new_tr));
		
			setTimeout( function(){ 
			     $(".chosen_41").chosen();
			  }  , 10 );

		}   

$(document).ready(function(){

jQuery('body').on('click', "input", function(){
        $(this).next().show();
        $(this).next().hide();
    });

});

  //--------------------------------------------------------
  
  //  Add row in estiate lender in servicing loan modal
  
     jQuery(document).ready(function() {
        var id = 0;
      
      jQuery('body').on('click', "#estimate_lender_addrow", function(){
        id++;           
        var row = jQuery('#estimate_lender_hidden_table tr').clone(true);
        row.find("input:text").val("");
        row.attr('id',id);  
        row.appendTo('#estimate_lender_table');        
        return false;
    });        
        
  $('.estimate_lender_remove').on("click", function() {
  $(this).parents("tr").remove();
});
});

function this_datepicker(that)
{
	$(that).datepicker({ dateFormat: 'mm-dd-yy' });
	 $(that).datepicker( "show" );
	// $(that)parent().parent().find("input[type='notes']").click();
	// $(that).datepicker({ dateFormat: 'mm-dd-yy' });
}


function current_paidoff_date(that)
{	
	if($(that).is(':checked'))
	{
		// $(that).parent().parent().parent().css('background','red');
		$(that).parent().parent().parent().find('input#paidoff_at_close').val(1);
	}
	else
	{
		$(that).parent().parent().parent().find('input#paidoff_at_close').val(0);
	}
}

//     Submit monthly_payment form

function monthly_payment_form()
{
	$("#monthly_payment_form").submit();
}


function form_encumbrances_submit()
{
	form_ecumbrance_total_calculation_update();
	$("#form_encumbrances").submit();
}



$(document).on("change", ".autosum_c_s", function(){
	var a = parseFloat(replace_dollar(this.value));
	// alert(number_format(a));
	if(a > 0)
	{ a = a;}
	else{ a=0.0; }
	
	this.value = '$' + number_format(a);
	var sum = 0;
    $(".autosum_c_s ").each(function(){
        sum += + replace_dollar($(this).val());
    });
    // $("#e_total_current_balance").val('$'+number_format(sum));
	$('#total_c_s').val('$'+number_format(sum));
	
	autocal_net_funding('1');
	
});

function add_row_closing_autosum_cs(that)
{
	var a = parseFloat(replace_dollar(that.value));
	// alert(number_format(a));
	if(a > 0)
	{ a = a;}
	else{ a=0.0; }
	that.value = '$' + number_format(a);
	// var that_value = that.value;
	// that_value = '$' + number_format(that_value);
	// that.value = that_value;
	var sum = 0;
    $(".autosum_c_s ").each(function(){
        sum += + replace_dollar($(this).val());
    });
    // $("#e_total_current_balance").val('$'+number_format(sum));
	$('#total_c_s').val('$'+number_format(sum));
	
	autocal_net_funding('1');
	
}

$(document).ready(function(){
	var sum = 0;
    $(".autosum_c_s ").each(function(){
        sum += + replace_dollar($(this).val());
    });
    // $("#e_total_current_balance").val('$'+number_format(sum));
	$('#total_c_s').val('$'+number_format(sum));
	
});

//

$('body').on('change', "#title_modal_policy_amount_in_percent", function(){
	var rate = replace_dollar($("#title_modal_policy_amount_in_percent").val());
	if(rate != ''){
	var total = replace_dollar($('#loan_amount').val());
	var cal = parseInt(rate) / 100 * parseInt(total);
	$('#title_policy_amount_in_dolar').val('$'+number_format(cal));
	$("#title_modal_policy_amount_in_percent").val(replace_dollar(rate)+'%');
	}
	else
	{
		var rate = 100;
		var total = replace_dollar($('#loan_amount').val());
		var cal = parseInt(rate) / 100 * parseInt(total);
		$('#title_policy_amount_in_dolar').val('$'+number_format(cal));
		$("#title_modal_policy_amount_in_percent").val(rate+'%');
	}
});

// ---    --  For Delete record information
function delete_record_information(id,loan_id)
{
	if(confirm('Are you sure to delete') === true )
	{
		$.ajax({
			type  : 'POST',
			url : '<?php echo base_url();?>delete_record_information',
			data : {"id" : id , "loan_id" : loan_id},
			success : function(result){
				if(result == 'Deleted')
				{
					location.href = '<?php echo base_url();?>load_data'+loan_id;
				}
			}
			
		});
	}
	
}

function number_format_comma(n)
{
	    return n.replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}


$('body').on('change', "#square_feet", function(){
	var a = number_format_comma($('#square_feet').val());
	
	$('#square_feet').val(a);
});

function number_formatnew(n) {
    return n.toFixed(2).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}

// on change event on gross_monthly_income


$('body').on('change', "#gross_monthly_income", function(){
	var income = replace_dollar($('#gross_monthly_income').val());
	if(income != '')
	{
		var result = parseFloat(income);
		var new_result = number_format(result);
		$('#gross_monthly_income').val('$'+new_result);
	}
	else
	{
		$('#gross_monthly_income').val('$0.00');
	}
});

// on change event on gross_monthly_expense


$('body').on('change', "#gross_monthly_expense", function(){
	var income = replace_dollar($('#gross_monthly_expense').val());
	if(income != '')
	{
		var result = parseFloat(income);
		var new_result = number_format(result);
		$('#gross_monthly_expense').val('$'+new_result);
	}
	else
	{
		$('#gross_monthly_expense').val('$0.00');
	}
});

// on change event on purchase_price


$('body').on('change', "#purchase_price", function(){
	var income = replace_dollar($('#purchase_price').val());
	if(income != '')
	{
		var result = parseFloat(income);
		var new_result = number_format(result);
		$('#purchase_price').val('$'+new_result);
	}
	else
	{
		$('#purchase_price').val('$0.00');
	}
	
});

// on change event on renovation_cost


$('body').on('change', "#renovation_cost", function(){
	var income = replace_dollar($('#renovation_cost').val());
	if(income != '')
	{
		var result = parseFloat(income);
		var new_result = number_format(result);
		$('#renovation_cost').val('$'+new_result);
	}
	else
	{
		$('#renovation_cost').val('$0.00');
	}
	
});


$('body').on('change', "#current_value", function(){
	var income = replace_dollar($('#current_value').val());
	if(income != '')
	{
		var result = parseFloat(income);
		var new_result = number_format(result);
		$('#current_value').val('$'+new_result);
	}
	else
	{
		$('#current_value').val('$0.00');
	}
	
});

// on change event on renovation_cost


$('body').on('change', "#sales_refinance_value", function(){
	var income = replace_dollar($('#sales_refinance_value').val());
	if(income != '')
	{
		var result = parseFloat(income);
		var new_result = number_format(result);
		$('#sales_refinance_value').val('$'+new_result);
	}
	else
	{
		$('#sales_refinance_value').val('$0.00');
	}
	
});
// on change event on renovation_cost


$('body').on('change', "#finished_arv", function(){
	var income = replace_dollar($('#finished_arv').val());
	if(income != '')
	{
		var result = parseFloat(income);
		var new_result = number_format(result);
		$('#finished_arv').val('$'+new_result);
	}
	else
	{
		$('#finished_arv').val('$0.00');
	}
	ajax_change_loan_or_arv_amount();
	
});



// on change event on renovation_cost


// on change event on Annual propert taxes in property modal


$('body').on('change', "#property_annual_p_taxes", function(){
	var income = replace_dollar($('#property_annual_p_taxes').val());
	if(income != '')
	{
		var result = parseFloat(income);
		var new_result = number_format(result);
		$('#property_annual_p_taxes').val('$'+new_result);
	}
	else
	{
		$('#property_annual_p_taxes').val('$0.00');
	}
});

function replace_dollar(n)
{
	var a = n.replace('$', '');
	var b = a.replace(',', '');
	var b = b.replace(',', '');
	var b = b.replace(',', '');
	var b = b.replace(',', '');
	var c = b.replace('%', '');
	return c;
}

function ajax_change_loan_or_arv_amount()
{
	var loan_amount 	= replace_dollar($("#loan_amount").val());
	var finished_arv = replace_dollar($("#finished_arv").val());
	
	if(loan_amount == '')
	{
		loan_amount = 0;
	}
	
	
	if(finished_arv == '')
	{
		finished_arv = 0;
	}
	
	equity_result 	 = parseFloat(finished_arv) - parseFloat(loan_amount);
	// value in negative
	
	// $("#borrower_enquity").val('$'+number_format(equity_result)); 
	
	
	$("#hidden_loan_amount").val(loan_amount);
	$("#hidden_finished_arv").val(finished_arv);
}





function amount_format_change(that)
{
	var a = that.value;
	var a = replace_dollar(a);
	if(a == '')
	{
		a = 0;
	}
	a = parseFloat(a);
	that.value = '$'+number_format(a);
}




// for moving Ecumbrances row up and down
if($('table#table_ecumbrances>tbody').length > 0){
	$('table#table_ecumbrances>tbody').sortable();
}

// for moving Assignment row up and down
if($('table#table_assigment>tbody').length > 0){
	$('table#table_assigment>tbody').sortable();
}


// $(".onselect_ballon_payment").change(function(){
	// var n = this.value;
	// if(n == '1')
	// { 
		// var a = $(this).parent().closest("td").find("#ballon_payment_b").val();
		// alert(a); 
	// }
	// else
	// {
		// alert('select no'); 
	// }
// });

function selling_price_amount()
{
	var loan_amount = $('#loan_amount').val();
	$('#selling_price').val(loan_amount);
}

function calculate_date_intrest_paid()
{
	var first_payment = $('#first_payment_date').val();
	$.ajax({
		type : 'POST',
		url 	: '<?php echo base_url();?>ajax_call/calculate_date_intrest_paid',
		data	: { "first_payment" : first_payment },
		success : function(result){
		
			$('#date_inetrest_paid').val(result);
		}
	});
	
}


$('.phone').keyup(function(){

	var val = this.value.replace(/\D/g, '');
	 if(val.length < 10)
	 {
        var newVal = '';
        while (val.length > 3) {
          newVal += val.substr(0, 3) + '-';
          val = val.substr(3);
        }
        newVal += val;
        this.value = newVal;
	 }
	 else
	 {
     this.value = this.value.replace(/(\d{3})\-?(\d{3})\-?(\d{4})/,'$1-$2-$3');
	 }
});


 
 
 
 
 jQuery(document).ready(function() {
	 var yes = $('#note_info_paymet_select').val();
	 if(yes != '1' )
	 {
		$('#note_info_paymet_select_depend').css('display', 'none');
	 }
	 else
	 {
		 $('#note_info_paymet_select_depend').css('display', 'block');
	 }
 });
 
 
 
 $('body').on('change', "#note_info_paymet_select", function(){
	 
	 var yes = $('#note_info_paymet_select').val();
	 if(yes != '1' )
	 {
		$('#note_info_paymet_select_depend').css('display', 'none');
	 }
	 else
	 {
		$('#note_info_paymet_select_depend').css('display', 'block');
	 }
	 
 }); 
 
 jQuery(document).ready(function() {
	 var delinquencies_cured = $('#delinquencies_cured').val();
	 if(delinquencies_cured != '0' )
	 {
		$('#delinquencies_cured_depend').css('display', 'none');
	 }
	 else
	 {
		 $('#delinquencies_cured_depend').css('display', 'block');
	 }
 });
 
 
 
$('body').on('change', "#delinquencies_cured", function(){
	 var delinquencies_cured = $('#delinquencies_cured').val();
	 if(delinquencies_cured != '0' )
	 {
		$('#delinquencies_cured_depend').css('display', 'none');
	 }
	 else
	 {
		$('#delinquencies_cured_depend').css('display', 'block');
	 }
	 
 });
 
 
 
$('body').on('change', ".assigment_investor_select", function(){	 
	 var id = $(this).val();
	 var that =	 $(this);
				 
	 $.ajax({
			type 	: 'POST',
			url		: '<?php echo base_url()."ajax_call/ajax_select_investor";?>',
			data	: { "id" : id },
			success	: function(response)
			{
				 var data = JSON.parse(response);
				 var talimar_lender = data.talimar_lender; 
				 // $(this).parent().parent().parent().child().find("input#lender_account").val('ooooo'); 
				that.parent().parent().find("input#lender_account").val(talimar_lender); 
				// that.css('background-color','red');   
			}
			
	 });
 });
 
 
 
 function ecumbrances_will_remain_change_readyfunction()
 {
	 $(".ecumbrances_will_it_remain").each(function(){
		var will_it_remain = $(this).val();
		ecumbrances_will_it_remain_change(this);
		ecum_change_existing_lien(this);
    });
	
 }
 
 
 
 $(document).on("change", ".m_p_amount", function() {
    var sum = 0;
    $(".m_p_amount ").each(function(){
        sum += + replace_dollar($(this).val());
    });
    $("td#monthly_payment_total input").val('$'+number_format(sum));
}); 
// 
  
 $(document).on("change", ".distribution_disbursement", function() {
    var sum = 0;
	var value1 = parseFloat(replace_dollar($(this).val()));
	this.value = '$'+number_format(value1);
    $(".distribution_disbursement ").each(function(){
		
        sum += + replace_dollar($(this).val());
    });
    $("td#distribution_total input").val('$'+number_format(sum));
	
	var loan_amount = parseFloat(replace_dollar($('#loan_amount').val()));
	var difference = loan_amount - sum ;
	$("td#distribution_difference input").val('$'+number_format(difference));
}); 
//

function assigment_investor_select_new(that)
{
	var a  = that;
	var id = that.value;
	
	 $.ajax({
			type 	: 'POST',
			url		: '<?php echo base_url()."ajax_call/ajax_select_investor";?>',
			data	: { "id" : id },
			success	: function(response)
			{
				 var data = JSON.parse(response);
				 var talimar_lender = data.talimar_lender;
				 $(a).parent().parent().find("input#lender_account").val(talimar_lender); 
				 // that.parent().parent().parent().children().children().find("input[name='lender_account[]']").val(talimar_lender); 
			}
			
	 });
	 
		var investor_val = $(that).parent().parent().find('#investment').val();
		if(investor_val){
	
			// update all hit with particular lender....
			update_all_assignments_record('<?php echo $loan_id;?>',id);
	
		}
	 
	 
}
function autocal_investment(that)
{
	//alert(that);

	var a = that;
	var investment  = that.value;
	
	var loan_amount = '<?php echo $fetch_loan_result[0]->loan_amount; ?>';
	
	investment = parseFloat(replace_dollar(investment));
	investment = investment.toString().replace(',', '');
	investment = parseFloat(investment);
	loan_amount = loan_amount.replace(',', '');
	loan_amount = parseFloat(loan_amount);
	if(investment>loan_amount){
		var errorHtml='<span style="color:red;">investment amount must be less than sum of loan amount</span>';
		$(that).after(errorHtml);
		that.value="";
		return false;
	}else{
		$(that).next("span").remove();
	}
	
	
	if(investment == '')
	{
		investment = 0;
	}
	if(isNaN(investment) == true)
	{
		investment = 0;
	}
	// alert('loan_amount=>'+loan_amount);
	// alert('investment=>'+investment);
	 if(loan_amount > 0){
		 
	 var percent_loan = parseFloat((investment / loan_amount)*100);
	 // alert('percent_loan=>'+percent_loan);
	 }else{
		 var percent_loan = 0;
	 }
	
	 // $(that).parent().parent().css('background-color','red');
	//$(that).parent().parent().find('input#percent_loan').val(number_format_percent_loan(percent_loan)+'%');
	
	var aa = replace_dollar('<?php echo $fetch_loan_result[0]->intrest_rate; ?>');
	
	var intrest_rate = parseFloat('<?php echo $fetch_loan_result[0]->intrest_rate; ?>');
	
	// alert('intrest_rate=>'+intrest_rate);
	<?php 
			if($loan_servicing['servicing_lender_rate'] == 'NaN' || $loan_servicing['servicing_lender_rate'] == '' ||  $loan_servicing['servicing_lender_rate'] == '0.00'){
				
				$lender_ratessss = ($loan_assigment_data[0]->invester_yield_percent) ? $loan_assigment_data[0]->invester_yield_percent :'0.00';
									
			}else{

				$lender_ratessss = ($loan_servicing['servicing_lender_rate'])? $loan_servicing['servicing_lender_rate'] :'0.00';
			}


		?>
								
	var servicing_fee = parseFloat('<?php echo $lender_ratessss; ?>');
//alert(servicing_fee);
    if(servicing_fee == '0'){

 		var nv= intrest_rate - servicing_fee;

    }else{

		var nv=servicing_fee;
    }
	
	$(that).parent().parent().find('input#invester_yield_percent').val(number_format(nv)+'%');
	// $(that).parent().parent().find('input#invester_yield_percent').val(number_format(percent_yield)+'%');
	// $(that).parent().parent().find('input#invester_yield_percent').val(number_format(new_al)+'%');
	 //  alert('percent_yield=>'+servicing_fee);
	 // alert('percent_yield=>'+percent_yield);
	 //  alert('percent_yield=>'+new_al);

	
	
	var payment = ((nv/100) * investment) / 12;
	 //alert(payment);
	$(that).parent().parent().find('input#payment').val('$'+number_format(payment));
	// alert(investment);
	// that.value = '$'+number_format(investment);
	// that.value = '$'+(investment.toLocaleString());
	calculate_assignments_total_data();
	var lender_id = $(that).parent().parent().find('#lender_name').val();
	if(lender_id){
		//alert(lender_id);
		// update all hit with particular lender....
		update_all_assignments_record('<?php echo $loan_id;?>',lender_id);
	
	}
}



function autocal_investmenttotal(that)
{
	if(that == '[object HTMLInputElement]'){
		var investment  = that.value;
	}else{
		var investment  = that;
	}
	investmenttotal = parseFloat(replace_dollar(investment));
	//alert(investmenttotal);
	var loan_amount = '<?php echo $fetch_loan_result[0]->loan_amount; ?>';
	loan_amount = loan_amount.replace(',', '');
	loan_amount = parseFloat(loan_amount);
	var percent_loan = parseFloat((investmenttotal / loan_amount)*100); 

	$(that).parent().parent().find('input#percent_loan').val(number_format_percent_loan(percent_loan)+'%');

	<?php 
		if($loan_servicing['servicing_lender_rate'] == 'NaN' || $loan_servicing['servicing_lender_rate'] == '' ||  $loan_servicing['servicing_lender_rate'] == '0.00'){
			
			$lender_ratessss = ($loan_assigment_data[0]->invester_yield_percent) ? $loan_assigment_data[0]->invester_yield_percent :'0.00';					
		}else{
			$lender_ratessss = ($loan_servicing['servicing_lender_rate'])? $loan_servicing['servicing_lender_rate'] :'0.00';
		}
	?>						
	var servicing_fee = parseFloat('<?php echo $lender_ratessss; ?>');
	var intrest_rate = parseFloat('<?php echo $fetch_loan_result[0]->intrest_rate; ?>');
    if(servicing_fee == '0'){
 		var nv= intrest_rate - servicing_fee;
    }else{
		var nv=servicing_fee;
    }

	var payment = (investmenttotal * (nv/12))/100;  
	//alert(payment);

	$(that).parent().parent().find('input#paymenttotal').val('$'+number_format(payment));

}


function calculate_assignments_total_data()
{
	var sum_investment = 0;
	var sum_percent = 0;
	var row = 0;
	// alert($(table#table_assigment tbody tr').length);
	$('#frm_assignment_id table#table_assigment tbody tr').each(function(){
		row++;
		if( parseFloat(replace_dollar($(this).find('input[name="investment[]"]').val())) > 0)
		{
			sum_investment += parseFloat(replace_dollar($(this).find('input[name="investment[]"]').val()));
			sum_percent += parseFloat(replace_dollar($(this).find('input[name="percent_loan[]"]').val()));
		}
	})
	var loan_amount = parseFloat('<?php echo $fetch_loan_result[0]->loan_amount ? $fetch_loan_result[0]->loan_amount : 0; ?>');
	var total_available_investment = loan_amount - sum_investment;
	var total_available_percent = 100 - sum_percent;
	$('#frm_assignment_id table#table_assigment tfoot input#assigment_total_investment').val('$'+number_format_comma_only(sum_investment));
	$('table#table_assigment tfoot input#percent_of_loan').val(sum_percent.toFixed(2)+'%');
	
	$('#frm_assignment_id table#table_assigment tfoot input#assigment_total_available').val('$'+number_format_comma_only(total_available_investment));
	
	$('#frm_assignment_id table#table_assigment tfoot input#percent_of_available_loan').val(total_available_percent.toFixed(2)+'%');
	$('#frm_assignment_id table#table_assigment tfoot input#assigment_total_assigned').val(row);
	
	//for auto add...
	if(row > 1){
		$('select#count_assignment').val('1');
	}else{
		$('select#count_assignment').val('2');
	}
	
}


function number_format_percent_loan(n) { 
	
    return n.toFixed(8).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "" + c : c;
    });
}

function update_all_assignments_record(loan_id,lender_id){
	/* if(lender_id){
			var url =  "<?php echo base_url().'update_all_assigment_data'?>"+loan_id+'/'+lender_id;
	}else{
		var url = "<?php echo base_url().'update_all_assigment_data'?>"+loan_id;
	} */
	
	var id = loan_id.replace('/', '');

	//alert(id);
	//alert(lender_id);

	
	// ajax call...
	
	// update_all_assigment_data
	$.ajax({
		'type' : 'POST',
		'url' : '<?php echo base_url().'update_all_assigment_data'?>',
		'data' : {'loan_id':id, 'lender_id':lender_id},
		success : function(response)
		{
			/* if(response == '1'){
				alert('TaliMar Database says: Assignments Records Updated Successfully! ');
			} */
			if(response == '2'){
				alert('TaliMar Database says: All Assignments Records Updated Successfully!');
			}
		}
	});
}


 
  

function print_this_row(that)
{
	$('#print_table').css('display','block');
	// $(that).parent().parent().parent().css('background-color','red');
	var row = $(that).parent().parent().parent().attr('id','print_this_row');
	var investor_name = $(that).parent().parent().parent().find(".assigment_investor_select option:selected").text();
	var lender_account = $(that).parent().parent().parent().find("#lender_account").val();
	var investment = $(that).parent().parent().parent().find("#investment").val();
	var percent_loan = $(that).parent().parent().parent().find("#percent_loan").val();
	var invester_yield_percent = $(that).parent().parent().parent().find("#invester_yield_percent").val();
	var payment = $(that).parent().parent().parent().find("#payment").val();
	var percent_loan = $(that).parent().parent().parent().find("#percent_loan").val();
	// checkboxes
	var disclosures_submit = $(that).parent().parent().parent().find("#disclosures_submit").val();
	var disclosures_execute = $(that).parent().parent().parent().find("#disclosures_execute").val();
	var fund_received = $(that).parent().parent().parent().find("#fund_received").val();
	var assigment_submit = $(that).parent().parent().parent().find("#assigment_submit").val();
	var servicer_submit = $(that).parent().parent().parent().find("#servicer_submit").val();
	
	//   ----------------------------
		$('#print_disclosures_submit').empty();
		$('#print_disclosures_execute').empty();
		$('#print_fund_received').empty();
		$('#print_assigment_submit').empty();
		$('#print_servicer_submit').empty();
		
	if(disclosures_submit == '1')
	{
		$('#print_disclosures_submit').append("<input type='checkbox' checked >");
	}
	else
	{
		$('#print_disclosures_submit').append("<input type='checkbox'  >");
	}
	
	//   ----------------------------
	
	if(disclosures_execute == '1')
	{
		$('#print_disclosures_execute').append("<input type='checkbox' checked >");
	}
	else
	{
		$('#print_disclosures_execute').append("<input type='checkbox'  >");
	}
	
	//   ----------------------------
	
	if(fund_received == '1')
	{
		$('#print_fund_received').append("<input type='checkbox' checked >");
	}
	else
	{
		$('#print_fund_received').append("<input type='checkbox' >");
	}
	
	//   ----------------------------
		
	if(assigment_submit == '1')
	{
		$('#print_assigment_submit').append("<input type='checkbox' checked >");
	}
	else
	{ 
		$('#print_assigment_submit').append("<input type='checkbox' >");
	}
	
	//   ----------------------------
		
	if(servicer_submit == '1')
	{
		$('#print_servicer_submit').append("<input type='checkbox' checked >");
	}
	else
	{
		$('#print_servicer_submit').append("<input type='checkbox'>");
	}
	
	// alert(row);
	
	$("#print_lender_account").text(lender_account);
	$("#print_lender_name").text(investor_name);
	$("#print_investment").text(investment);
	$("#print_percent_loan").text(percent_loan);
	$("#print_percent_yield").text(invester_yield_percent);
	$("#print_monthly_payment").text(payment);
	// alert(row);
	 // row.appendTo('#print_table tbody'); 
	printData(); 
}

function printData()
{
   
   // var row =jQuery('#table_assigment tr#print_this_row').clone(true)
   // $('#print_table').appendTo(row);
   // $('#print_table tbody').empty();
   // row.appendTo('#print_table'); 
   var divToPrint=document.getElementById("print_loan");
   newWin= window.open('');
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
   $('#print_table').css('display','none');
  
}



$(document).ready(function(){
var id=$('#marketing_option select[name="available_release"]').val();
hoide_show(id);
});

 function hoide_show(that){

if(that=='1'){
	$('#marketing_option input[name="youtube_url"]').attr('readonly', true);

}
else if(that=='3'){

$('#marketing_option input[name="youtube_url"]').attr('readonly', true);

}
else{

	$('#marketing_option input[name="youtube_url"]').attr('readonly', false);

}

 }

function broker_capacity_check_box()
{
	var broker_capacity = $("#select_broker_capacity").val();
	var explain = '';
	if(broker_capacity == '1')
	{
		if($("#a_d_behalf_another").is(':checked'))
		{  explain = 1; }
		else if($("#a_d_principal_exit_loan").is(':checked')){ explain = 1; }
		else if($("#a_d_arrange_sale_portion").is(':checked')){ explain = 1; }
		else{ explain = 0; }
		
		if(explain == '1')
		{
			$("#broker_capacity_explain textarea").prop('disabled', false);
			$('#broker_capacity_explain').css('display','block');
		}
		else
		{
			$("#broker_capacity_explain textarea").prop('disabled', true);
			$('#broker_capacity_explain').css('display','none');
		}
	}
	else if(broker_capacity == '2')
	{
		if($("#l_o_behalf_another").is(':checked'))
		{  explain = 1; }
		else if($("#l_o_principal_borrower").is(':checked')){ explain = 1; }
		else if($("#l_o_portion_loan").is(':checked')){ explain = 1; }
		else{ explain = 0; }
		
		if(explain == '1')
		{
			$("#broker_capacity_explain textarea").prop('disabled', false);
			$('#broker_capacity_explain').css('display','block');
		}
		else
		{
			$("#broker_capacity_explain textarea").prop('disabled', true);
			$('#broker_capacity_explain').css('display','none');
		}
	}
	else
	{
		$("#broker_capacity_explain textarea").prop('disabled', true);
		$('#broker_capacity_explain').css('display','none');
	}
	
}

$(document).ready(function(){
	var broker_capacity = $("#select_broker_capacity").val();
	var explain = '';
	if(broker_capacity == '1')
	{
		if($("#a_d_behalf_another").is(':checked'))
		{  explain = 1; }
		else if($("#a_d_principal_exit_loan").is(':checked')){ explain = 1; }
		else if($("#a_d_arrange_sale_portion").is(':checked')){ explain = 1; }
		else{ explain = 0; }
		
		if(explain == '1')
		{
			$("#broker_capacity_explain textarea").prop('disabled', false);
			$('#broker_capacity_explain').css('display','block');
		}
		else
		{
			$("#broker_capacity_explain textarea").prop('disabled', true);
			$('#broker_capacity_explain').css('display','none');
		}
	}
	else if(broker_capacity == '2')
	{
		if($("#l_o_behalf_another").is(':checked'))
		{  explain = 1; }
		else if($("#l_o_principal_borrower").is(':checked')){ explain = 1; }
		else if($("#l_o_portion_loan").is(':checked')){ explain = 1; }
		else{ explain = 0; }
		
		if(explain == '1')
		{
			$("#broker_capacity_explain textarea").prop('disabled', false);
			$('#broker_capacity_explain').css('display','block');
		}
		else
		{
			$("#broker_capacity_explain textarea").prop('disabled', true);
			$('#broker_capacity_explain').css('display','none');
		}
	}
	else
	{
		$("#broker_capacity_explain textarea").prop('disabled', true);
		$('#broker_capacity_explain').css('display','none');
	}
});


$('body').on('change', "#subordination_provision", function(){
	var provision = $('#subordination_provision').val();
	if(provision  == '1')
	{
		$("#n_subordination_explain textarea").prop('disabled', false);
		$('#n_subordination_explain').css('display','block');
	}
	else
	{
		$("#n_subordination_explain textarea").prop('disabled', true);
		$('#n_subordination_explain').css('display','none');
	}
});

$(document).ready(function(){
	var provision = $('#subordination_provision').val();
	if(provision  == '1')
	{
		$("#n_subordination_explain textarea").prop('disabled', false);
		$('#n_subordination_explain').css('display','block');
	}
	else
	{
		$("#n_subordination_explain textarea").prop('disabled', true);
		$('#n_subordination_explain').css('display','none');
	}
});

function autosum_estimate_lender_cost(that)
{
	var a = parseFloat(replace_dollar(that.value));
	// alert(number_format(a));
	if(a > 0)
	{ a = a;}
	else{ a=0.0; }
	that.value = '$' + number_format(a);
	var sum = 0;
    $(".autosum_estimate_l_cost ").each(function(){
        sum += + replace_dollar($(this).val());
    });
	$('#total_estimate_lender_cost').val('$'+number_format(sum));
	
}


$('body').on('change', ".autosum_estimate_l_cost", function(){
	var sum = 0;
    $(".autosum_estimate_l_cost ").each(function(){
        sum += + replace_dollar($(this).val());
    });
	$('#total_estimate_lender_cost').val('$'+number_format(sum));
});

$(document).ready(function(){
	var sum = 0;
    $(".autosum_estimate_l_cost ").each(function(){
        sum += + replace_dollar($(this).val());
    });
	$('#total_estimate_lender_cost').val('$'+number_format(sum));
});

/********************************************************Section Due Diligence Items**************************/
var globalDueDiligence=[2,3,4,24,5,12,13,16,17,18,19,6,11,20,25,7,8,9,23,22,10,26,21,114,118,119,120,121,102,103,104,105,110,115,116,122,117,111,112,202,203,204,205,206,215,207,208,213,214,209,216,212,301,302,303,401,402,403,404,405];
var fixFlipDueDiligence=[2,3,4,5,20,7,8,9,23,22,10,21,114,102,103,104,105,110,115,116,117,111,112,202,203,208,209,2012,404,405];
var bridgePurchaseDueDiligence=[2,3,4,5,6,20,7,8,9,23,22,10,21,114,102,103,104,105,110,116,117,111,112,202,203,208,214,209];
var bridgeRefinanceDueDiligence=[2,3,4,5,6,11,20,7,8,9,23,22,10,21,114,103,104,105,110,116,117,111,202,203,204,205,206,207,208,214,209,301,302];
var constructionLoanDueDiligence=[2,3,4,5,6,20,7,8,9,23,22,10,21,114,102,103,104,105,110,115,116,117,111,112,202,203,208,214,209,212,401,402,403,405];
var refiYearLoanDueDiligence=[2,3,4,24,20,25,7,8,9,23,22,26,114,118,119,120,103,104,105,110,122,203,206,215,214,209,216,302,303];
var purchaseYearLoanDueDiligence=[2,3,4,24,20,25,7,8,9,23,22,26,114,118,119,120,121,102,103,104,105,110,122,203,214,209,216];
function allDueDiligenceHeadingAction(that){	
	var arr = $('input[name="items_id_array_js_use[]"]').map(function () {
	    return this.value; 
	}).get();
	if(arr.length>0){
		globalDueDiligence=$.merge(arr, globalDueDiligence);
	}	
	console.log(globalDueDiligence);
	$(".diligence_materials_heading").each(function() {
	    if($(this).val()==$(that).val()){
	    	var hidden_recive_name=$(that).attr("item_no_id");
	    	$("#"+hidden_recive_name).val('1');
	    	var selectedArrayData=[];
	    	if($(that).val()=="Check All"){
	    		for(var i=0;i<globalDueDiligence.length;i++){
	    			$('#underwriting_table input[name="req_'+globalDueDiligence[i]+'"]').attr('checked',true);
					$('#underwriting_required_'+globalDueDiligence[i]).val('1');
					$('#underwriting_table input[name="rec_'+globalDueDiligence[i]+'"]').attr('checked',false);
					$('#underwriting_received_'+globalDueDiligence[i]).val('0');
	    		}
	    	}
	    	else if($(that).val()=="Uncheck All"){
	    		for(var i=0;i<globalDueDiligence.length;i++){
	    			$('#underwriting_table input[name="req_'+globalDueDiligence[i]+'"]').attr('checked',false);
					$('#underwriting_required_'+globalDueDiligence[i]).val('0');
					$('#underwriting_table input[name="rec_'+globalDueDiligence[i]+'"]').attr('checked',false);
					$('#underwriting_received_'+globalDueDiligence[i]).val('0');
	    		}
	    	}else{
	    			if($(that).val()=="Fix and Flip Loan"){
			    		selectedArrayData=fixFlipDueDiligence;
			    	}
			    	if($(that).val()=="Bridge Purchase Loan"){
			    		selectedArrayData=bridgePurchaseDueDiligence;
			    	}
			    	if($(that).val()=="Bridge Refinance Loan"){
			    		selectedArrayData=bridgeRefinanceDueDiligence;
			    	}
			    	if($(that).val()=="Construction Loan"){
			    		selectedArrayData=constructionLoanDueDiligence;
			    	}
			    	if($(that).val()=="30 Year Loan (Refi)"){
			    		selectedArrayData=refiYearLoanDueDiligence;
			    	}
			    	if($(that).val()=="30 Year Loan (Purchase)"){
			    		selectedArrayData=purchaseYearLoanDueDiligence;
			    	}
			    	if(selectedArrayData.length>1){
			    		for(var i=0;i<globalDueDiligence.length;i++){
			    			if(jQuery.inArray(globalDueDiligence[i], selectedArrayData) != -1) {
			    				/* first checkbox*/
							    $('#underwriting_table input[name="req_'+globalDueDiligence[i]+'"]').attr('checked',true);
							    $('#underwriting_required_'+globalDueDiligence[i]).val('1');
							    /* secound checkbox
							    $('#underwriting_table input[name="rec_'+globalDueDiligence[i]+'"]').attr('checked',false);
							    $('#underwriting_received_'+globalDueDiligence[i]).val('0');*/
							} else {
								/* first checkbox*/
							    $('#underwriting_table input[name="req_'+globalDueDiligence[i]+'"]').attr('checked',false);
							    $('#underwriting_required_'+globalDueDiligence[i]).val('0');
							    /* secound checkbox
							    $('#underwriting_table input[name="rec_'+globalDueDiligence[i]+'"]').attr('checked',false);
							    $('#underwriting_received_'+globalDueDiligence[i]).val('0');*/
							} 
			    		}
			    	}else{
			    		return false;
			    	}
	    	}  	
	    }else{
	    	$(this).prop("checked", false);
	    	var hidden_recive_name=$(this).attr("item_no_id");
	    	$("#"+hidden_recive_name).val('0');
	    }
	});

}
function fix_flip_select_print(that)
{
	$('#underwriting_table input').attr('checked',false);
	$('#income_property_select').attr('checked',false);
	$('#select_print').attr('checked',false);
	$('#income_construction_select').attr('checked',false);
	$('#income_refinance_select').attr('checked',false);
	$('input.underwriting_required_checkbox').val('0');
	/*Bitcot changes*/
	$('#refi_year_loan_select').attr('checked',false);
	$('#purchase_year_loan_select').attr('checked',false);
	$('#deselect_print').attr('checked',false);
	/* Bitcot changes end*/
	
	if($(that).is(':checked'))
	{
		
		$('#underwriting_table input[name="req_2"]').attr('checked',true);
		$('#underwriting_table input[name="req_3"]').attr('checked',true);
		$('#underwriting_table input[name="req_4"]').attr('checked',true);
		$('#underwriting_table input[name="req_5"]').attr('checked',true);
		$('#underwriting_table input[name="req_20"]').attr('checked',true);
		$('#underwriting_table input[name="req_7"]').attr('checked',true);
		$('#underwriting_table input[name="req_8"]').attr('checked',true);
		$('#underwriting_table input[name="req_9"]').attr('checked',true);
		$('#underwriting_table input[name="req_23"]').attr('checked',true);
		$('#underwriting_table input[name="req_22"]').attr('checked',true);
		$('#underwriting_table input[name="req_10"]').attr('checked',true);
		$('#underwriting_table input[name="req_21"]').attr('checked',true);
		$('#underwriting_table input[name="req_114"]').attr('checked',true);
		$('#underwriting_table input[name="req_102"]').attr('checked',true);
		$('#underwriting_table input[name="req_103"]').attr('checked',true);
		$('#underwriting_table input[name="req_104"]').attr('checked',true);
		$('#underwriting_table input[name="req_105"]').attr('checked',true);
		$('#underwriting_table input[name="req_110"]').attr('checked',true);
		$('#underwriting_table input[name="req_115"]').attr('checked',true);
		$('#underwriting_table input[name="req_116"]').attr('checked',true);
		$('#underwriting_table input[name="req_117"]').attr('checked',true);
		$('#underwriting_table input[name="req_111"]').attr('checked',true);
		$('#underwriting_table input[name="req_112"]').attr('checked',true);
		$('#underwriting_table input[name="req_202"]').attr('checked',true);
		$('#underwriting_table input[name="req_203"]').attr('checked',true);
		$('#underwriting_table input[name="req_208"]').attr('checked',true);
		$('#underwriting_table input[name="req_209"]').attr('checked',true);
		$('#underwriting_table input[name="req_212"]').attr('checked',true);
		$('#underwriting_table input[name="req_404"]').attr('checked',true);
		$('#underwriting_table input[name="req_405"]').attr('checked',true);
		

		$('#underwriting_required_2').val('1');
		$('#underwriting_required_3').val('1');
		$('#underwriting_required_4').val('1');
		$('#underwriting_required_5').val('1');
		$('#underwriting_required_20').val('1');
		$('#underwriting_required_7').val('1');
		$('#underwriting_required_8').val('1');
		$('#underwriting_required_9').val('1');
		$('#underwriting_required_23').val('1');
		$('#underwriting_required_22').val('1');
		$('#underwriting_required_10').val('1');
		$('#underwriting_required_21').val('1');
		$('#underwriting_required_114').val('1');
		$('#underwriting_required_102').val('1');
		$('#underwriting_required_103').val('1');
		$('#underwriting_required_104').val('1');
		$('#underwriting_required_105').val('1');
		$('#underwriting_required_110').val('1');
		$('#underwriting_required_115').val('1');
		$('#underwriting_required_116').val('1');
		$('#underwriting_required_117').val('1');
		$('#underwriting_required_111').val('1');
		$('#underwriting_required_112').val('1');
		$('#underwriting_required_202').val('1');
		$('#underwriting_required_203').val('1');
		$('#underwriting_required_208').val('1');
		$('#underwriting_required_209').val('1');
		$('#underwriting_required_212').val('1');
		$('#underwriting_required_404').val('1');
		$('#underwriting_required_405').val('1');
		
	}
}

function income_property_select_print(that)
{
	$('#underwriting_table input').attr('checked',false);
	$('#fix_flip_select').attr('checked',false);
	$('#select_print').attr('checked',false);
	$('#income_construction_select').attr('checked',false);
	$('#income_refinance_select').attr('checked',false);
	$('input.underwriting_required_checkbox').val('0');
	/*Bitcot changes*/
	$('#refi_year_loan_select').attr('checked',false);
	$('#purchase_year_loan_select').attr('checked',false);
	/* Bitcot changes end*/
	
	if($(that).is(':checked'))
	{

		$('#underwriting_table input[name="req_2"]').attr('checked',true);
		$('#underwriting_table input[name="req_3"]').attr('checked',true);
		$('#underwriting_table input[name="req_4"]').attr('checked',true);
		$('#underwriting_table input[name="req_5"]').attr('checked',true);
		$('#underwriting_table input[name="req_6"]').attr('checked',true);
		$('#underwriting_table input[name="req_20"]').attr('checked',true);
		$('#underwriting_table input[name="req_7"]').attr('checked',true);
		$('#underwriting_table input[name="req_8"]').attr('checked',true);
		$('#underwriting_table input[name="req_9"]').attr('checked',true);
		$('#underwriting_table input[name="req_23"]').attr('checked',true);
		$('#underwriting_table input[name="req_22"]').attr('checked',true);
		$('#underwriting_table input[name="req_10"]').attr('checked',true);
		$('#underwriting_table input[name="req_21"]').attr('checked',true);
		$('#underwriting_table input[name="req_114"]').attr('checked',true);
		$('#underwriting_table input[name="req_102"]').attr('checked',true);
		$('#underwriting_table input[name="req_103"]').attr('checked',true);
		$('#underwriting_table input[name="req_104"]').attr('checked',true);
		$('#underwriting_table input[name="req_105"]').attr('checked',true);
		$('#underwriting_table input[name="req_110"]').attr('checked',true);
		$('#underwriting_table input[name="req_116"]').attr('checked',true);
		$('#underwriting_table input[name="req_117"]').attr('checked',true);
		$('#underwriting_table input[name="req_111"]').attr('checked',true);
		$('#underwriting_table input[name="req_112"]').attr('checked',true);
		$('#underwriting_table input[name="req_202"]').attr('checked',true);
		$('#underwriting_table input[name="req_203"]').attr('checked',true);
		$('#underwriting_table input[name="req_208"]').attr('checked',true);
		$('#underwriting_table input[name="req_214"]').attr('checked',true);
		$('#underwriting_table input[name="req_209"]').attr('checked',true);

		

		$('#underwriting_required_2').val('1');
		$('#underwriting_required_3').val('1');
		$('#underwriting_required_4').val('1');
		$('#underwriting_required_5').val('1');
		$('#underwriting_required_6').val('1');
		$('#underwriting_required_20').val('1');
		$('#underwriting_required_7').val('1');
		$('#underwriting_required_8').val('1');
		$('#underwriting_required_9').val('1');
		$('#underwriting_required_23').val('1');
		$('#underwriting_required_22').val('1');
		$('#underwriting_required_10').val('1');
		$('#underwriting_required_21').val('1');
		$('#underwriting_required_114').val('1');
		$('#underwriting_required_102').val('1');
		$('#underwriting_required_103').val('1');
		$('#underwriting_required_104').val('1');
		$('#underwriting_required_105').val('1');
		$('#underwriting_required_110').val('1');
		$('#underwriting_required_116').val('1');
		$('#underwriting_required_117').val('1');
		$('#underwriting_required_111').val('1');
		$('#underwriting_required_112').val('1');
		$('#underwriting_required_202').val('1');
		$('#underwriting_required_203').val('1');
		$('#underwriting_required_208').val('1');
		$('#underwriting_required_214').val('1');
		$('#underwriting_required_209').val('1');
	
	}
}

function income_refinance_select_print(that)
{


	$('#underwriting_table input').attr('checked',false);
	$('#income_property_select').attr('checked',false);
	$('#select_print').attr('checked',false);
	$('#income_construction_select').attr('checked',false);
	// $('#income_refinance_select').attr('checked',false);
	$('input.underwriting_required_checkbox').val('0');
	/*Bitcot changes*/
	$('#refi_year_loan_select').attr('checked',false);
	$('#purchase_year_loan_select').attr('checked',false);
	/* Bitcot changes end*/
	
	if($(that).is(':checked'))
	{	
		$('#underwriting_table input[name="req_2"]').attr('checked',true);
		$('#underwriting_table input[name="req_3"]').attr('checked',true);
		$('#underwriting_table input[name="req_4"]').attr('checked',true);
		$('#underwriting_table input[name="req_5"]').attr('checked',true);
		$('#underwriting_table input[name="req_6"]').attr('checked',true);
		$('#underwriting_table input[name="req_11"]').attr('checked',true);
		$('#underwriting_table input[name="req_20"]').attr('checked',true);
		$('#underwriting_table input[name="req_7"]').attr('checked',true);
		$('#underwriting_table input[name="req_8"]').attr('checked',true);
		$('#underwriting_table input[name="req_9"]').attr('checked',true);
		$('#underwriting_table input[name="req_23"]').attr('checked',true);
		$('#underwriting_table input[name="req_22"]').attr('checked',true);
		$('#underwriting_table input[name="req_10"]').attr('checked',true);
		$('#underwriting_table input[name="req_21"]').attr('checked',true);
		$('#underwriting_table input[name="req_114"]').attr('checked',true);
		$('#underwriting_table input[name="req_103"]').attr('checked',true);
		$('#underwriting_table input[name="req_104"]').attr('checked',true);
		$('#underwriting_table input[name="req_105"]').attr('checked',true);
		$('#underwriting_table input[name="req_110"]').attr('checked',true);
		$('#underwriting_table input[name="req_116"]').attr('checked',true);
		$('#underwriting_table input[name="req_117"]').attr('checked',true);
		$('#underwriting_table input[name="req_111"]').attr('checked',true);
		$('#underwriting_table input[name="req_202"]').attr('checked',true);
		$('#underwriting_table input[name="req_203"]').attr('checked',true);
		$('#underwriting_table input[name="req_204"]').attr('checked',true);
		$('#underwriting_table input[name="req_205"]').attr('checked',true);
		$('#underwriting_table input[name="req_205"]').attr('checked',true);
		$('#underwriting_table input[name="req_206"]').attr('checked',true);
		$('#underwriting_table input[name="req_207"]').attr('checked',true);
		$('#underwriting_table input[name="req_208"]').attr('checked',true);
		$('#underwriting_table input[name="req_214"]').attr('checked',true);
		$('#underwriting_table input[name="req_209"]').attr('checked',true);
		$('#underwriting_table input[name="req_301"]').attr('checked',true);
		$('#underwriting_table input[name="req_302"]').attr('checked',true);
		
		$('#underwriting_required_2').val('1');
		$('#underwriting_required_3').val('1');
		$('#underwriting_required_4').val('1');
		$('#underwriting_required_5').val('1');
		$('#underwriting_required_6').val('1');
		$('#underwriting_required_11').val('1');
		$('#underwriting_required_20').val('1');
		$('#underwriting_required_7').val('1');
		$('#underwriting_required_8').val('1');
		$('#underwriting_required_9').val('1');
		$('#underwriting_required_23').val('1');
		$('#underwriting_required_22').val('1');
		$('#underwriting_required_10').val('1');
		$('#underwriting_required_21').val('1');
		$('#underwriting_required_114').val('1');
		$('#underwriting_required_103').val('1');
		$('#underwriting_required_104').val('1');
		$('#underwriting_required_105').val('1');
		$('#underwriting_required_110').val('1');
		$('#underwriting_required_116').val('1');
		$('#underwriting_required_117').val('1');
		$('#underwriting_required_111').val('1');
		$('#underwriting_required_202').val('1');
		$('#underwriting_required_203').val('1');
		$('#underwriting_required_204').val('1');
		$('#underwriting_required_205').val('1');
		$('#underwriting_required_205').val('1');
		$('#underwriting_required_206').val('1');
		$('#underwriting_required_207').val('1');
		$('#underwriting_required_208').val('1');
		$('#underwriting_required_214').val('1');
		$('#underwriting_required_209').val('1');
		$('#underwriting_required_301').val('1');
		$('#underwriting_required_302').val('1');
	}
}

function income_construction_select_print(that)
{
	$('#underwriting_table input').attr('checked',false);
	$('#fix_flip_select').attr('checked',false);
	$('#select_print').attr('checked',false);
	// $('#income_construction_select').attr('checked',false);
	$('#income_refinance_select').attr('checked',false);
	$('input.underwriting_required_checkbox').val('0');
	/*Bitcot changes*/
	$('#refi_year_loan_select').attr('checked',false);
	$('#purchase_year_loan_select').attr('checked',false);
	/* Bitcot changes end*/

	if($(that).is(':checked'))
	{
		$('#underwriting_table input[name="req_2"]').attr('checked',true);
		$('#underwriting_table input[name="req_3"]').attr('checked',true);
		$('#underwriting_table input[name="req_4"]').attr('checked',true);
		$('#underwriting_table input[name="req_5"]').attr('checked',true);
		$('#underwriting_table input[name="req_6"]').attr('checked',true);
		$('#underwriting_table input[name="req_20"]').attr('checked',true);
		$('#underwriting_table input[name="req_7"]').attr('checked',true);
		$('#underwriting_table input[name="req_8"]').attr('checked',true);
		$('#underwriting_table input[name="req_9"]').attr('checked',true);
		$('#underwriting_table input[name="req_23"]').attr('checked',true);
		$('#underwriting_table input[name="req_22"]').attr('checked',true);
		$('#underwriting_table input[name="req_10"]').attr('checked',true);
		$('#underwriting_table input[name="req_21"]').attr('checked',true);
		$('#underwriting_table input[name="req_114"]').attr('checked',true);
		$('#underwriting_table input[name="req_102"]').attr('checked',true);
		$('#underwriting_table input[name="req_103"]').attr('checked',true);
		$('#underwriting_table input[name="req_104"]').attr('checked',true);
		$('#underwriting_table input[name="req_105"]').attr('checked',true);
		$('#underwriting_table input[name="req_110"]').attr('checked',true);
		$('#underwriting_table input[name="req_115"]').attr('checked',true);
		$('#underwriting_table input[name="req_116"]').attr('checked',true);
		$('#underwriting_table input[name="req_117"]').attr('checked',true);
		$('#underwriting_table input[name="req_111"]').attr('checked',true);
		$('#underwriting_table input[name="req_112"]').attr('checked',true);
		$('#underwriting_table input[name="req_202"]').attr('checked',true);
		$('#underwriting_table input[name="req_203"]').attr('checked',true);
		$('#underwriting_table input[name="req_208"]').attr('checked',true);
		$('#underwriting_table input[name="req_214"]').attr('checked',true);
		$('#underwriting_table input[name="req_209"]').attr('checked',true);
		$('#underwriting_table input[name="req_212"]').attr('checked',true);
		$('#underwriting_table input[name="req_401"]').attr('checked',true);
		$('#underwriting_table input[name="req_402"]').attr('checked',true);
		$('#underwriting_table input[name="req_403"]').attr('checked',true);
		$('#underwriting_table input[name="req_405"]').attr('checked',true);

		$('#underwriting_required_2').val('1');
		$('#underwriting_required_3').val('1');
		$('#underwriting_required_4').val('1');
		$('#underwriting_required_5').val('1');
		$('#underwriting_required_6').val('1');
		$('#underwriting_required_20').val('1');
		$('#underwriting_required_7').val('1');
		$('#underwriting_required_8').val('1');
		$('#underwriting_required_9').val('1');
		$('#underwriting_required_23').val('1');
		$('#underwriting_required_22').val('1');
		$('#underwriting_required_10').val('1');
		$('#underwriting_required_21').val('1');
		$('#underwriting_required_114').val('1');
		$('#underwriting_required_102').val('1');
		$('#underwriting_required_103').val('1');
		$('#underwriting_required_104').val('1');
		$('#underwriting_required_105').val('1');
		$('#underwriting_required_110').val('1');
		$('#underwriting_required_115').val('1');
		$('#underwriting_required_116').val('1');
		$('#underwriting_required_117').val('1');
		$('#underwriting_required_111').val('1');
		$('#underwriting_required_112').val('1');
		$('#underwriting_required_202').val('1');
		$('#underwriting_required_203').val('1');
		$('#underwriting_required_208').val('1');
		$('#underwriting_required_214').val('1');
		$('#underwriting_required_209').val('1');
		$('#underwriting_required_212').val('1');
		$('#underwriting_required_401').val('1');
		$('#underwriting_required_402').val('1');
		$('#underwriting_required_403').val('1');
		$('#underwriting_required_405').val('1');
			
	}
}

function select_all_underwriting(that)
{
	$('#underwriting_table input').attr('checked',false);
	$('#fix_flip_select').attr('checked',false);
	$('#income_property_select').attr('checked',false);
	$('#income_construction_select').attr('checked',false);
	$('#income_refinance_select').attr('checked',false);
	$('input.underwriting_required_checkbox').val('0'); 
	/*Bitcot changes*/
	$('#refi_year_loan_select').attr('checked',false);
	$('#purchase_year_loan_select').attr('checked',false);
	/* Bitcot changes end*/
	if($(that).is(':checked'))
	{
		$('input.common').attr('checked',true);
		$('input.underwriting_required_checkbox').val('1'); 
		
	}
}

function deselected_all_print(that)
{
	if($(that).is(':checked'))
	{
		
		$('#underwriting_table input').attr('checked',false);
		$('#fix_flip_select').attr('checked',false);
		$('#income_property_select').attr('checked',false);
		$('#select_print').attr('checked',false);
		$('#income_construction_select').attr('checked',false);
		$('#income_refinance_select').attr('checked',false);
		$('input.underwriting_required_checkbox').val('0'); 
		/*Bitcot changes*/
		$('#refi_year_loan_select').attr('checked',false);
		$('#purchase_year_loan_select').attr('checked',false);
		/* Bitcot changes end*/
	}
}
function refi_year_loan_select_print(that){
	$('#underwriting_table input').attr('checked',false);
	$('#fix_flip_select').attr('checked',false);
	$('#income_property_select').attr('checked',false);
	$('#income_construction_select').attr('checked',false);
	$('#income_refinance_select').attr('checked',false);
	$('#select_print').attr('checked',false);
	$('#purchase_year_loan_select').attr('checked',false);
	
}
function purchase_year_loan_select_print(that){
	$('#underwriting_table input').attr('checked',false);
	$('#fix_flip_select').attr('checked',false);
	$('#income_property_select').attr('checked',false);
	$('#income_construction_select').attr('checked',false);
	$('#income_refinance_select').attr('checked',false);
	$('#select_print').attr('checked',false);
	$('#refi_year_loan_select').attr('checked',false);	
}
function bridge_select_print_doc(that)
{
	if($(that).is(':checked'))
	{
		$('table#table_print_document input').prop('checked',false);
		$('#print_select_all').prop('checked',false);
		$('#print_deselect_all').prop('checked',false);
		$('#auto_select_print_input').prop('checked',false);
		
		$('table#table_print_document input[name="loan_term"]').prop('checked',true);
		$('table#table_print_document input[name="lender_closing_statement"]').prop('checked',true);
		//$('table#table_print_document input[name="schedule_income_wire"]').prop('checked',true);
		$('table#table_print_document input[name="escrow_instrusction"]').prop('checked',true);
		$('table#table_print_document input[name="anti_money_document"]').prop('checked',true);
		$('table#table_print_document input[name="arbitration_of_disputes"]').prop('checked',true);
		//$('table#table_print_document input[name="automated_valuation_notice"]').prop('checked',true);
		//$('table#table_print_document input[name="authorization_to_release"]').prop('checked',true);
		$('table#table_print_document input[name="ballon_payment_disclosures"]').prop('checked',true);
		$('table#table_print_document input[name="borrower_certification"]').prop('checked',true);
		$('table#table_print_document input[name="california_insurance_disclosure"]').prop('checked',true);
	//	$('table#table_print_document input[name="capacity_to_repay_disclosure"]').prop('checked',true);
		$('table#table_print_document input[name="certificate_bussiness_purpose_loan"]').prop('checked',true);
		$('table#table_print_document input[name="compliance_agreement"]').prop('checked',true);
		$('table#table_print_document input[name="consumer_notice"]').prop('checked',true);
		$('table#table_print_document input[name="credit_score_disclosure"]').prop('checked',true);
		$('table#table_print_document input[name="deed_trust_assigment_rents"]').prop('checked',true);
		$('table#table_print_document input[name="default_provision"]').prop('checked',true);
		//$('table#table_print_document input[name="exclusive_devt_ratio"]').prop('checked',true);
		$('table#table_print_document input[name="fair_lending_notice"]').prop('checked',true);
		$('table#table_print_document input[name="fedral_equal_opprtunity_act"]').prop('checked',true);
		$('table#table_print_document input[name="first_payment_notify"]').prop('checked',true);
		$('table#table_print_document input[name="insurance_endrosement"]').prop('checked',true);
		//$('table#table_print_document input[name="privacy_notice_disclosure"]').prop('checked',true);
		$('table#table_print_document input[name="promissory_note_secure_trust"]').prop('checked',true);
		//$('table#table_print_document input[name="fci_construction_disbursement_agreement"]').prop('checked',true);
		$('table#table_print_document input[name="rider_interest_reserve"]').prop('checked',true);
			var a='<?php echo $fetch_loan_borrower[0]->borrower_type; ?>';
			
		$('table#table_print_document input[name="payment_gurranty"]').prop('checked',true);
		if(a==1){
		
		$('table#table_print_document input[name="corporate_resolution"]').prop('checked',false);
		
		}else{
			
			$('table#table_print_document input[name="corporate_resolution"]').prop('checked',true);

		}
	}
}

function auto_select_loan_app(that){

if($(that).is(':checked')){

	$('table#table_print_document input[name="loan_application"]').prop('checked',true);
	$('table#table_print_document input[name="re885_demo"]').prop('checked',true);
	$('table#table_print_document input[name="authorization_to_release"]').prop('checked',true);
	$('table#table_print_document input[name="privacy_notice_disclosure"]').prop('checked',true);
	$('table#table_print_document input[name="automated_valuation_notice"]').prop('checked',true);
	$('table#table_print_document input[name="diligence_materials_list"]').prop('checked',true);
	// $('table#table_print_document input[name="term_sheet"]').prop('checked',true);


}



}


function auto_select_print(that)
{
	if($(that).is(':checked'))
	{
		$('table#table_print_document input').prop('checked',false);
		$('#print_select_all').prop('checked',false);
		$('#print_deselect_all').prop('checked',false);
		$('#bridge_doc').prop('checked',false);
		$('table#table_print_document input[name="loan_term"]').prop('checked',true);
		$('table#table_print_document input[name="lender_closing_statement"]').prop('checked',true);
		//$('table#table_print_document input[name="schedule_income_wire"]').prop('checked',true);
		$('table#table_print_document input[name="escrow_instrusction"]').prop('checked',true);
		$('table#table_print_document input[name="anti_money_document"]').prop('checked',true);
		$('table#table_print_document input[name="arbitration_of_disputes"]').prop('checked',true);
		//$('table#table_print_document input[name="automated_valuation_notice"]').prop('checked',true);
	//	$('table#table_print_document input[name="authorization_to_release"]').prop('checked',true);
		$('table#table_print_document input[name="ballon_payment_disclosures"]').prop('checked',true);
		$('table#table_print_document input[name="borrower_certification"]').prop('checked',true);
		$('table#table_print_document input[name="california_insurance_disclosure"]').prop('checked',true);
		//$('table#table_print_document input[name="capacity_to_repay_disclosure"]').prop('checked',true);
		$('table#table_print_document input[name="certificate_bussiness_purpose_loan"]').prop('checked',true);
		$('table#table_print_document input[name="compliance_agreement"]').prop('checked',true);
		$('table#table_print_document input[name="consumer_notice"]').prop('checked',true);
		$('table#table_print_document input[name="credit_score_disclosure"]').prop('checked',true);
		$('table#table_print_document input[name="deed_trust_assigment_rents"]').prop('checked',true);
		$('table#table_print_document input[name="default_provision"]').prop('checked',true);
		//$('table#table_print_document input[name="exclusive_devt_ratio"]').prop('checked',true);
		$('table#table_print_document input[name="fair_lending_notice"]').prop('checked',true);
		$('table#table_print_document input[name="fedral_equal_opprtunity_act"]').prop('checked',true);
		$('table#table_print_document input[name="first_payment_notify"]').prop('checked',true);
		$('table#table_print_document input[name="insurance_endrosement"]').prop('checked',true);
		//$('table#table_print_document input[name="privacy_notice_disclosure"]').prop('checked',true);
		$('table#table_print_document input[name="promissory_note_secure_trust"]').prop('checked',true);
		$('table#table_print_document input[name="fci_construction_disbursement_agreement"]').prop('checked',true);
		$('table#table_print_document input[name="rider_interest_reserve"]').prop('checked',true);
		
		
		// $('table#table_print_document input[name="attachment_a_construct_draw"]').prop('checked',true);
		// $('table#table_print_document input[name="ach_construction_draw"]').prop('checked',true);
		var loan_type = $('select#loan_type').val();
		if(loan_type == 2 || loan_type == 3)
		{
			$('table#table_print_document input[name="attachment_a_construct_draw"]').prop('checked',true);
		}
		
		//$('table#table_print_document input[name="transfer_servicing_disclosure"]').prop('checked',true);
		$('table#table_print_document input[name="payment_gurranty"]').prop('checked',true);
		var a='<?php echo $fetch_loan_borrower[0]->borrower_type; ?>';
			if(a==1){
		$('table#table_print_document input[name="corporate_resolution"]').prop('checked',false);
	}else{

	$('table#table_print_document input[name="corporate_resolution"]').prop('checked',true);	
	}
	}
}




function select_all_print_loan(that)
{
	if($(that).is(':checked'))
	{
		$('table#table_print_document input').prop('checked',true);
		$('#print_deselect_all').prop('checked',false);
	}
}

function deselect_all_print_loan(that)
{
	if($(that).is(':checked'))
	{
		$('table#table_print_document input').prop('checked',false);
		$('#print_select_all').prop('checked',false);
		$('#auto_select_print_input').prop('checked',false);
		$('#bridge_doc').prop('checked',false);
	}
}


function lender_assign_doc(that)
{
	var lender_id = that.id;
	//var talimar_loan = '<?php// echo $modal_talimar_loan;?>';
	$("#selected_lender_doc").val(lender_id);

	
}


function assigment_print_data(that)
{
	
	var lender_id = that.id;
	$("#selected_lender_id_print").val(lender_id);
}

function assigment_print_selectall(that)
{
	if($(that).is(':checked'))
	{
		$('div.assigment_print_document input').prop('checked',true);
		$('#asigment_deselect_all').prop('checked',false);
	}
}

function assigment_print_deselectall(that)
{
	if($(that).is(':checked'))
	{
		$('div.assigment_print_document input').prop('checked',false);
		$('div#assigment_print_data div.print_assigment_top input').prop('checked',false);
		$('#asigment_select_all').prop('checked',false);
		$(that).prop('checked',true);
	}
}

function multiple_lender_select(that)
{
	if($(that).is(':checked'))
	{
		$('input#single_lender_select1').prop('checked',false);
	}
}

function single_lender_select(that)
{
	if($(that).is(':checked'))
	{    
		$('input#multiple_lender_select1').prop('checked',false);
	}
}

$(document).ready(function(){
	var assigment_total_investment	= replace_dollar($('#assigment_total_investment').val());
	var loan_amount	= replace_dollar($('#loan_amount').val());
	if(assigment_total_investment){
		
	assigment_total_investment = assigment_total_investment.replace(',','');
	}else{
		assigment_total_investment = '';
	}
	
	
	loan_amount = loan_amount.replace(',','');
	percent_of_loan = (parseFloat(assigment_total_investment) / parseFloat(loan_amount))*100;
	percent_of_available_loan = 100 - percent_of_loan;
	$("#percent_of_loan").val(percent_of_loan.toFixed(8) + '%');
	
	$("#percent_of_available_loan").val(percent_of_available_loan.toFixed(8) + '%');
});


$(document).ready(function(){
	var property_taxes_current = $('#property_taxes_current').val();
	if(property_taxes_current == 0)
	{
		$('#select_yes_p_tax_cur').css('display','none');
		$('#select_yes_p_tax_cur input').prop('disabled', true);
	}
	else{
		$('#select_yes_p_tax_cur').css('display','block');
		$('#select_yes_p_tax_cur input').prop('disabled', false);
	}
});

$('body').on('change', "#property_taxes_current", function(){
	var property_taxes_current = $('#property_taxes_current').val();
	if(property_taxes_current == 0)
	{
		$('#select_yes_p_tax_cur').css('display','none');
		$('#select_yes_p_tax_cur input').prop('disabled', true);
	}
	else{
		$('#select_yes_p_tax_cur').css('display','block');
		$('#select_yes_p_tax_cur input').prop('disabled', false);
	}
});


$('body').on('change', "#select_property_type", function(){
	var property_type = $('#select_property_type').val();
	if(property_type == 5)
	{
		$('.property_type_dependent').css('display','none');
	}
	else
	{
		$('.property_type_dependent').css('display','block');
	}
});

$(document).ready(function(){
	var property_type = $('#select_property_type').val();
	if(property_type == 5)
	{
		$('.property_type_dependent').css('display','none');
	}
	else
	{
		$('.property_type_dependent').css('display','block');
	}
});




$('body').on('change', ".autocal_net_funding", function(){
	autocal_net_funding(this);
});

function autocal_net_funding(that)
{
	var sum = 0;
	var amount = 0;
	$('.autocal_net_funding').each(function(){
		var net_funding_option = $(this).val();
		if(net_funding_option == 1)
		{
			 amount = $(this).parent().parent().find('#lender_amount').val();
		}
		else if(net_funding_option == 0)
		{
			 amount = '$0';
		}
		else
		{
			 amount = '$0';
		}
		sum += + replace_dollar(amount);
		
	})
	$('#c_s_total_net_of_funding').val('$'+number_format(sum));
	var loan_amount = replace_dollar($('#loan_amount').val());
	var net_disbursement = loan_amount - sum;
	$('#c_s_net_fund').val('$'+number_format(net_disbursement));
}

$(document).ready(function(){
	autocal_net_funding('1');
});

function go_to_lender(that)
{
	
	var id = $(that).parent().parent().find('#lender_name').val();
	if(id)
	{
	window.location.href = '<?php echo base_url();?>investor_view/'+id;
	}
}

function comparable_cal_per_sq_feet(that)
{
	if(that.id == 'sale_price')
	{
		// alert(that.value);
		if(that.value == '')
		{
			that.value = 0;
			$(that).parent().parent().find('input#per_sq_feet').val('$0');
		}
		var sale_price = parseFloat(replace_dollar(that.value));
		that.value = '$' + number_format(sale_price);
	}
	
	
	// $(that).parent().parent().css('background-color','red');
	var sq_feet = $(that).parent().parent().find('input#sq_feet').val();
	if(sq_feet == '')
	{
		sq_feet = 0;
		$(that).parent().parent().find('input#per_sq_feet').val('$0');
	}
	var sale_price_new = $(that).parent().parent().find('input#sale_price').val();
	if(sale_price_new == '')
	{
		sale_price_new = 0;
		$(that).parent().parent().find('input#per_sq_feet').val('$0');
	}
	var per_sq_feet = parseFloat(replace_dollar(sale_price_new)) / parseFloat(replace_dollar(sq_feet));
	// alert(per_sq_feet);
	 $(that).parent().parent().find('input#per_sq_feet').val('$' + number_format(per_sq_feet));
	 
	 if(that.id == 'sq_feet')
	{
		var sq_feet_format = number_format_comma(replace_dollar(that.value));
		that.value = sq_feet_format;
	}
	//$('button#asss').click();
}


jQuery('body').on('click', "#comp_sale_per_sq_ft", function(){
	var comp_sale_per_sq_ft = 0;
	var comp_sale_avg_per_sq_ft = $('#comp_sale_avg_per_sq_ft').val();
	if(comp_sale_avg_per_sq_ft)
	{
		$('input#comp_sale_per_sq_ft').val(comp_sale_avg_per_sq_ft);
	}
})
function textAreaAdjust(o) {
  o.style.height = "1px";
  o.style.height = (25+o.scrollHeight)+"px";
}

// function download_del_toro_excel_file()
// {
	
	// alert('gone in this script');
	// $('#download_del_toro_excel_file').submit();
	// $('#form_print_loan_document').submit();
// }
function download_del_toro_excel_file111(){
	//alert('aa');
    //$('form#form_download_del_toro_excel_file').submit();
}

function fn_fciboardingdata(that){
	//alert('aa');
	if($(that).is(':checked')){
    	$('form#form_fciboardingdata').submit();
    }
}

function remove_this_select(that)
{
	// alert('Working');
	$(that).parent().find('input').prop('disabled',true );
	$(that).parent().remove();
	$('a#link_go_email').href = "mailto:";
}

function add_lender_new(that)
{
	// $(that).parent().parent().css('background-color','red');
	var email = $(that).parent().parent().find('td.email').text();
	var contact_name = $(that).parent().parent().find('td.contact_name').text();
	// alert(email);
	var content = '<span>'+contact_name+'(Email:'+email+')<input type="hidden" name="contact_id[]" value="'+that.id+'"><input type="hidden" class="contact_emailaddress" value="'+email+'"><a onclick="remove_this_select(this)"><i class="fa fa-times" aria-hidden="true"></i></a></span>';
	$("p#contact_email_list").append(content);
	
	$(that).parent().parent().hide(2);
}

function go_email_outlook(that)
{
	var email;
	var a = '';
	var i = 0;
	$('input.contact_emailaddress').each(function(i, el) {
		 //alert(this.value);
		email = this.value;
		if(email)
		{
		    var href = $(that).attr('href');
			$(that).attr('href', href + email + '; ');
		}
	});
}

function fee_distibution_form(that,rowcnt)
{
	var broker 		= parseFloat(replace_dollar(that.value));
	
	
	var servicer 	= parseFloat(replace_dollar($('#loan_servicer_'+rowcnt).val()));
	// alert('Talimar is: ' + broker );
	// alert('servicer is: ' + servicer );
	if(broker > 100)
	{
		alert('Please enter valid value (0 - 100)');
		$(that).val('');
		$(that).focus();
	}
	else
	{
		// var lender = 100 - broker;
		var lender =  broker - servicer;
		// var servicer = 100-broker - lender;
		
		$(that).val((broker)+'%');
		
		$(that).parent().parent().find('input#fee_dis_lender_'+rowcnt).val((lender)+'%');
		$(that).parent().parent().find('span#fee_dis_lender_label_'+rowcnt).html((lender)+'%');
		
	/* 	$(that).parent().parent().find('input#loan_servicer').val(number_format(servicer)+'%');
		$(that).parent().parent().find('input#loan_servicer_label').html(number_format(servicer)+'%'); */
		
		
		// alert(broker);
		// alert(lender);
	}		
	
	
}
function fee_lender_form(that)
{
	var broker = parseFloat(replace_dollar($(that).parent().parent().find('input#fee_dis_broker').val()));
		
	var lender = parseFloat(replace_dollar(that.value));
		
	var servicer = 100-broker - lender;
	console.log('servicer is:' + servicer);
		
	$(that).val(number_format(lender)+'%');
		
	// $(that).parent().parent().find('input#fee_dis_lender').val(number_format(lender)+'%');
	$(that).parent().parent().find('input#loan_servicer').val(number_format(servicer)+'%');
	// alert(broker);
	// alert(lender);
		
}


$('body').on('change', "#fractional_intrest", function(){
	var fractional_intrest = $('#fractional_intrest').val();
	if(fractional_intrest == '0'){
		$('div#min_lender_investment_div').css('display','none');
	}
	else if(fractional_intrest == '1'){
		$('div#min_lender_investment_div').css('display','block');
	}
});

$(document).ready(function(){
	var fractional_intrest = $('#fractional_intrest').val();
	if(fractional_intrest == '0'){
		$('div#min_lender_investment_div').css('display','none');
	}
	else if(fractional_intrest == '1'){
		$('div#min_lender_investment_div').css('display','block');
	}
});

function wire_tali_lender_checkbox(that)
{
	if($(that).is(':checked'))
	{
		$('input[name="bank_name"]').val('Wells Fargo Bank');
		$('input[name="bank_street"]').val('10535 Craftsman Way');
		$('input[name="bank_city"]').val('San Diego');
		$('select[name="bank_state"] option[value="CA"]').prop('selected',true);
		$('select[name="bank_zip"]').val('92127');
		
		$('input[name="recipient_name"]').val('TaliMar Financial Inc.');
		$('input[name="recipient_street"]').val('11440 West Bernardo Ct., Suite #210');
		$('input[name="recipient_city"]').val('San Diego');
		$('select[name="recipient_state"] option[value="CA"]').prop('selected',true);
		$('select[name="recipient_zip"]').val('92127');
	}
}


$('body').on('change', "#checkbox_pre_paid_per_deim", function(){
	if($("#checkbox_pre_paid_per_deim").is(':checked'))
	{

		var cal_type 	= '<?php echo isset($fetch_loan_result) ? $fetch_loan_result[0]->calc_of_year : '0'; ?>';
		var intrest 	= parseFloat('<?php echo isset($fetch_loan_result) ?$fetch_loan_result[0]->intrest_rate : '0'; ?>');
		var loan_amount = parseFloat('<?php echo isset($fetch_loan_result) ? $fetch_loan_result[0]->loan_amount : '0'; ?>');
		// alert('cal_type=>'+cal_type+' loan_amount=>'+loan_amount+' intrest=>'intrest);
		if(cal_type == '1')
		{
			var day = 360;
		}
		else if(cal_type == '2')
		{
			
			var day = 365;
		}
		else
		{
			var day = 0; 
		}
		
		var per_day_amount = (intrest / (100 * day) ) * loan_amount;
		
		$('#pre_paid_per_deim').val('$'+number_format(per_day_amount)+' per day');
	}
	else
	{
		$('#pre_paid_per_deim').val('');
	}
});

function delete_ecumbrance(id,that)
{
	if(id)
	{
		if(confirm('Are you sure to delete from database')){
			
			$.ajax({
				'type' : 'POST',
				'url' : '<?php echo base_url().'ajax_call/ajax_delete_ecumbrances'?>',
				'data' : {'id':id},
				success : function(response)
				{
					if(response == '1')
					{
						$(that).parents("tr").remove();
					}
				}
			});
		}
	}
	else
	{
		$(that).parents("tr").remove();
	}
}

function fill_checkbx_value(that)
{
	if($(that).is(':checked'))
	{
		$(that).parent().parent().parent().find('input#request_updated_insurance').val(1);
	}
	else
	{
		$(that).parent().parent().parent().find('input#request_updated_insurance').val(0);
	}
}
function impound_accounts_checkbox(that)
{
	if($(that).is(':checked'))
	{
		$(that).parent().parent().parent().find('input#impounded').val(1);
	}
	else
	{
		$(that).parent().parent().parent().find('input#impounded').val(0);
	}
}

function lender_impound_payment_checkboxes(that)
{
	var lender_impound = that.value;
	if(lender_impound == '0')
	{
		$('#table_impound_accounts input[type="checkbox"]').prop('disabled',true);
	}
	else
	{
		$('#table_impound_accounts input[type="checkbox"]').prop('disabled',false);
	}
}



function print_disclouser_fund_at_close_multiple(that)
{
	if($(that).is(":checked"))
	{
		// first unchecked all 
		$('div.assigment_print_document input[type="checkbox"]').attr("checked",false);
		
		$('div.print_assigment_top input[type="checkbox"]').attr("checked",false);
		
		$(that).attr("checked",true);
		$('div.assigment_print_document input[name="lender_data_sheet"]').attr("checked",true);
		$('div.assigment_print_document input[name="principal_loss_disclosure"]').attr("checked",true);
		$('div.assigment_print_document input[name="lender_trust_deed_escrow"]').attr("checked",true);
		$('div.assigment_print_document input[name="arbitration_of_disputes"]').attr("checked",true);
		$('div.assigment_print_document input[name="broker_statement_security_exemption"]').attr("checked",true);
		$('div.assigment_print_document input[name="wire_instructions"]').attr("checked",false);
		$('div.assigment_print_document input[name="fci_loan_servicing"]').attr("checked",false);
		$('div.assigment_print_document input[name="fci_ach"]').attr("checked",false);
		$('div.assigment_print_document input[name="lender_qualification_statement"]').attr("checked",true);
		$('div.assigment_print_document input[name="non_corcumvent_agreement"]').attr("checked",true);
		$('div.assigment_print_document input[name="lender_servicing_agreement"]').attr("checked",true);
		$('div.assigment_print_document input[name="waiver_of_appraisal"]').attr("checked",true);
		$('div.assigment_print_document input[name="multiple_lender_disclosure"]').attr("checked",true);
		$('div.assigment_print_document input[name="multiple_lender_disclosure_list"]').attr("checked",true);
		$('div.assigment_print_document input[name="loan_organisation"]').attr("checked",true);
		$('div.assigment_print_document input[name="new_fci_loan_servicing"]').attr("checked",true);
		$('div.assigment_print_document input[name="new_fci_loan_foreclosure"]').attr("checked",true);
		$('div.assigment_print_document input[name="new_fci_loan_optional_insurance_services"]').attr("checked",true);
		$('div.assigment_print_document input[name="fci_loan_servicing"]').attr("checked",true);
		$('div.assigment_print_document input[name="fci_ach"]').attr("checked",true);
		
	}
}

function print_disclouser_fund_at_close_single(that)
{
	if($(that).is(":checked"))
	{
		// first unchecked all 
		$('div.assigment_print_document input[type="checkbox"]').attr("checked",false);
		
		$('div.print_assigment_top input[type="checkbox"]').attr("checked",false);
		
		$(that).attr("checked",true);
		$('div.assigment_print_document input[name="lender_data_sheet"]').attr("checked",true);
		$('div.assigment_print_document input[name="principal_loss_disclosure"]').attr("checked",true);
		$('div.assigment_print_document input[name="lender_trust_deed_escrow"]').attr("checked",true);
		$('div.assigment_print_document input[name="arbitration_of_disputes"]').attr("checked",true);
		$('div.assigment_print_document input[name="broker_statement_security_exemption"]').attr("checked",true);
		$('div.assigment_print_document input[name="lender_qualification_statement"]').attr("checked",true);
		$('div.assigment_print_document input[name="non_corcumvent_agreement"]').attr("checked",true);
		$('div.assigment_print_document input[name="lender_servicing_agreement"]').attr("checked",true);
		$('div.assigment_print_document input[name="waiver_of_appraisal"]').attr("checked",true);
		$('div.assigment_print_document input[name="loan_organisation"]').attr("checked",true);
		$('div.assigment_print_document input[name="fci_ach"]').attr("checked",true);
		$('div.assigment_print_document input[name="fci_loan_servicing"]').attr("checked",true);
		$('div.assigment_print_document input[name="new_fci_loan_servicing"]').attr("checked",true);
		$('div.assigment_print_document input[name="new_fci_loan_foreclosure"]').attr("checked",true);
		$('div.assigment_print_document input[name="new_fci_loan_optional_insurance_services"]').attr("checked",true);
	}
}

function print_disclouser_fund_after_close_multiple(that)
{
	if($(that).is(":checked"))
	{
		// first unchecked all 
		$('div.assigment_print_document input[type="checkbox"]').attr("checked",false);
		
		$('div.print_assigment_top input[type="checkbox"]').attr("checked",false);
		
		$(that).attr("checked",true);
		$('div.assigment_print_document input[name="lender_data_sheet"]').attr("checked",true);
		$('div.assigment_print_document input[name="principal_loss_disclosure"]').attr("checked",true);
		$('div.assigment_print_document input[name="lender_trust_deed_escrow"]').attr("checked",true);
		$('div.assigment_print_document input[name="arbitration_of_disputes"]').attr("checked",true);
		$('div.assigment_print_document input[name="broker_statement_security_exemption"]').attr("checked",true);
		$('div.assigment_print_document input[name="lender_qualification_statement"]').attr("checked",true);
		$('div.assigment_print_document input[name="non_corcumvent_agreement"]').attr("checked",true);
		$('div.assigment_print_document input[name="lender_servicing_agreement"]').attr("checked",true);
		$('div.assigment_print_document input[name="waiver_of_appraisal"]').attr("checked",true);
		$('div.assigment_print_document input[name="multiple_lender_disclosure"]').attr("checked",true);
		$('div.assigment_print_document input[name="multiple_lender_disclosure_list"]').attr("checked",true);
		$('div.assigment_print_document input[name="promissory_note_endorsement"]').attr("checked",true);
		// $('div.assigment_print_document input[name="loan_organisation"]').attr("checked",true);
		$('div.assigment_print_document input[name="sale_of_existing_note"]').attr("checked",true);
		$('div.assigment_print_document input[name="fci_loan_servicing"]').attr("checked",true);
		$('div.assigment_print_document input[name="fci_ach"]').attr("checked",true);
	}
}

// auto check RE851D..
function print_multiple_property(that)
{
	if($(that).is(":checked"))
	{
		// first unchecked all 
		$('div.assigment_print_document input[type="checkbox"]').attr("checked",false);
		
		$('div.print_assigment_top input[type="checkbox"]').attr("checked",false);
		
		$(that).attr("checked",true);
		$('div.assigment_print_document input[name="re851d_print"]').attr("checked",true);
		
	}
}

function print_disclouser_fund_after_close_single(that)
{
	if($(that).is(":checked"))
	{
		// first unchecked all 
		$('div.assigment_print_document input[type="checkbox"]').attr("checked",false);
		
		$('div.print_assigment_top input[type="checkbox"]').attr("checked",false);
		
		$(that).attr("checked",true);
		$('div.assigment_print_document input[name="lender_data_sheet"]').attr("checked",true);
		$('div.assigment_print_document input[name="principal_loss_disclosure"]').attr("checked",true);
		$('div.assigment_print_document input[name="lender_trust_deed_escrow"]').attr("checked",true);
		$('div.assigment_print_document input[name="arbitration_of_disputes"]').attr("checked",true);
		$('div.assigment_print_document input[name="broker_statement_security_exemption"]').attr("checked",true);
		$('div.assigment_print_document input[name="lender_qualification_statement"]').attr("checked",true);
		$('div.assigment_print_document input[name="non_corcumvent_agreement"]').attr("checked",true);
		$('div.assigment_print_document input[name="lender_servicing_agreement"]').attr("checked",true);
		$('div.assigment_print_document input[name="waiver_of_appraisal"]').attr("checked",true);
		$('div.assigment_print_document input[name="promissory_note_endorsement"]').attr("checked",true);
		// $('div.assigment_print_document input[name="loan_organisation"]').attr("checked",true);
		$('div.assigment_print_document input[name="sale_of_existing_note"]').attr("checked",true);
		$('div.assigment_print_document input[name="fci_ach"]').attr("checked",true);
		$('div.assigment_print_document input[name="fci_loan_servicing"]').attr("checked",true);
		$('div.assigment_print_document input[name="new_fci_loan_servicing"]').attr("checked",true);
		$('div.assigment_print_document input[name="new_fci_loan_foreclosure"]').attr("checked",true);
		$('div.assigment_print_document input[name="new_fci_loan_optional_insurance_services"]').attr("checked",true);
		
	}
}


function set_c_ballon_payment(that){
	var type = $(that).val();
	if(type == '0')
	{
		$(that).parent().parent('tr').find('input.c_ballon_payment').val('');
		$(that).parent().parent('tr').find('input.c_ballon_payment').prop('readonly',true);
		$(that).parent().parent('tr').find('input.c_ballon_payment').css('background','#dedede');
	}
	else
	{
		$(that).parent().parent('tr').find('input.c_ballon_payment').prop('readonly',false);
		$(that).parent().parent('tr').find('input.c_ballon_payment').css('background','white');
	}
}


$('body').on('change', 'div#property select[name="state"]', function(){
	var property_state = $('div#property select[name="state"]').val();
	if(property_state == 'NV')
	{
		$('div#property select#county_usa').css('display','none');
		$('div#property select#county_naveda').css('display','block');
	}
	else
	{
		$('div#property select#county_usa').css('display','block');
		$('div#property select#county_naveda').css('display','none');
	}
});

$(document).ready(function(){
	var property_state = $('div#property select[name="state"]').val();
	if(property_state == 'NV')
	{
		$('div#property select#county_usa').css('display','none');
		$('div#property select#county_naveda').css('display','block');
	}
	else
	{
		$('div#property select#county_usa').css('display','block');
		$('div#property select#county_naveda').css('display','none');
	}
});

function ecumbrances_will_it_remain_change(that)
{
	// var will_it_remain = that.value;
	var will_it_remain = $(that).parent().parent('tr').find('select[name="will_it_remain[]"]').val();
	
}

function load_ecumbrances_questions_ajax(ecum_id,lien_holder)
{
	$('a#modal-ecum-question').click();
	$('div#ecumbrance_questions span#ecum-load-lien-holder-name').empty();
	$('div#ecumbrance_questions span#ecum-load-lien-holder-name').text(lien_holder);
	
	$.ajax({
		type 	: 'POST',
		url		: '<?php echo base_url().'load_ecumbrances_questions_ajax'; ?>',
		data	: { 'ecum_id' : ecum_id },
		success	: function(result)
		{
			$('div#ecumbrance_questions_load_data').empty();
			$('div#ecumbrance_questions_load_data').append(result);
		}
	});
}


function load_ecumbrances_request_notice_ajax(ecum_id,lien_holder)
{
	$('a#modal-ecum-request_notice').click();
	$('#ecumbrance_request_notice span#ecum-load-lien-holder-name').empty();
	$('#ecumbrance_request_notice span#ecum-load-lien-holder-name').text(lien_holder);
	
	$.ajax({
		type 	: 'POST',
		url		: '<?php echo base_url().'load_ecumbrances_request_notice_ajax'; ?>',
		data	: { 'ecum_id' : ecum_id },
		success	: function(result)
		{
			$('div#ecumbrance_request_notice_load_data').empty();
			$('div#ecumbrance_request_notice_load_data').append(result);
		}
	});
}

function form_sumbit_ecumbrance_request_notice()
{
	
	var formData = new FormData(document.getElementById("form-ecumbrance-request_notice"));
	
	$.ajax({
		type	: 'POST', 
		url		: '<?php echo base_url()."form_sumbit_ecumbrance_request_notice"; ?>',
		data	: formData,
		
		async: false,
		contentType: false,
		processData: false,
		success : function(result)
		{
			if(result == 'Update')
			{
				$('#ecumbrance-data-status').text('Encumbrance Data Updated');
			}
			else if(result == 'Insert')
			{
				$('#ecumbrance-data-status').text('Encumbrance Data Updated');
			}
			
			$('button#ecum-request_notice-close-modal').click();
		}
	});
}

function ecum_change_existing_lien(that)
{
	var existing_lien = $(that).parent().parent('tr').find('select[name="existing_lien[]"]').val();
	
	ecumbrances_will_it_remain_change(that);
}
function fillamtval(id){
	var value = $('input[name=amount_'+id+']').val().replace(/[^\d\.]/g, ''); 
	$("input[name=amtval_"+id+"]").val(value);
	
	var selectoptn = $("select[name=impounded_"+id+"]").val();
	 if(selectoptn == '1'){
		
		fill_amt(id,selectoptn);
	} 
		
}

function fill_amt(id,value){

}

function cal_monthly_payment(){
	var monthly_payment 		= $('#monthy_payment_total').val().replace(/[^\d\.]/g, ''); 
	var monthly_impound 		= $('#monthly_impound_total').val().replace(/[^\d\.]/g, ''); 
	var monthly_payment_impound = parseFloat(monthly_payment) + parseFloat(monthly_impound);

	
	$('#monthly_payment_impound').val('$'+monthly_payment_impound.toFixed(2));
	$('#monthly_impound_total').val('$'+monthly_impound);
	$('#monthy_payment_total').val('$'+monthly_payment);
	
		
	
}




$('body').on('change', "div#loan_servicing select#boarding_status", function(){
	var boarding_status = $('div#loan_servicing select#boarding_status').val();
	if(boarding_status == '1')
	{
		$('div#loan_servicing div.boarding_status_dependent').css('display','block');
	}
	else
	{
		$('div#loan_servicing div.boarding_status_dependent').css('display','none');
	}
});
$(document).ready(function(){
	var boarding_status = $('div#loan_servicing select#boarding_status').val();
	if(boarding_status == '1')
	{
		$('div#loan_servicing div.boarding_status_dependent').css('display','block');
	}
	else
	{
		$('div#loan_servicing div.boarding_status_dependent').css('display','none');
	}
});



function add_row_accured_charges(){
	var rowCount = $('#table_accured_charges tbody tr').length;
	rowCount++;
	
	 var row = $('#table_accured_charges').append('<tr id = "row_'+rowCount+'"><td><a href = "javascript:void(0);" onclick = "delete_accured_charges_row('+rowCount+');" class="as_class"><i class="fa fa-trash" aria-hidden="true"></i></a><input type="hidden" id = "accured_id_'+rowCount+'" name="accured_id[]" value=""></td><td><input  type="text" id="accured_date_'+rowCount+'" placeholder = "mm-dd-yyyy" class=" accured_date form-control"  value="" name = "accured_date[]"  /></td><td><input style = "width:100% !important" name  = "accured_description[]" id = "accured_description" type = "text" value = "" class = "form-control" required/></td><td><input class ="form-control" type = "text" name = "accured_paid_to[]" value = "" /></td><td><input style = "width:100% !important"  name  = "accured_amount[]" id = "accured_amount" onchange = get_sum_val(this.value); type = "text" value = "" class = "accured_amount number_only  amount_format form-control " /></td><td><input style = "width:100% !important" name="paid_amount[]" type="text" value="" class="number_only amount_format form-control"></td><td><input style = "width:100% !important" name= "remaning[]" type="text" value="" class="number_only amount_format form-control"></td></tr>');
		
		// alert("accured_date_"+rowCount);
		// $( "#accured_date_"+rowCount).datepicker({ dateFormat: 'mm-dd-yy' });
		$( ".accured_date").datepicker({ dateFormat: 'mm-dd-yy' });
   
	 
}

function amount_format_with_dollar(that)
{
	a = parseFloat(replace_dollar(that.value));
	that.value = '$'+number_format(a);
}

function rate_format_with_percent(that)
{
	a = parseFloat(replace_dollar(that.value));
	that.value = number_format(a)+'%';
}

 function delete_accured_charges_row(rowcnt,accured_id){
	 
	// delete from accured_charges table by id...
	if(accured_id){
			
		 if (confirm('Are you sure to delete this Accured Charge row?')) {
			
				$.ajax({
				 type 	: 'POST',
				 url 	: '<?php echo base_url()."load_data/delete_accured_id/";?>'+accured_id,
				 data 	: {'id' : accured_id},
				 success : function(response)
				 {
					if(response == '1'){
						var amount 	= replace_dollar($('#accured_amount_'+rowcnt).val());
						var total 	= replace_dollar($('#accured_total_val').val());
	
						
						var new_total =   total - amount;
						$('#accured_total_val').val(new_total);
						$('#accured_total').html('$' + new_total+'.00');
						
						$('#row_'+rowcnt).remove();
					}
				 }
			 });
			 
			 
		}
	}else{
		$('#row_'+rowcnt).remove();
		
	}
	
		// window.location.href = '<?php echo base_url();?>delete_accured_id/'+accured_id;
}

function fill_checkbox_val(rowcnt,id){
	
	 if($('#chkbx_'+rowcnt).prop('checked') == true){
		
		$('#impount_acct_id_'+rowcnt).val(id);
		$('#checkbox_delete_'+rowcnt).val('1');
	
	}else{
		$('#checkbox_delete_'+rowcnt).val('0');
		$('#impount_acct_id_'+rowcnt).val(id);
		
	} 
}

function add_row_impound_accounts(){
	
	var rowCount = $('#table_impound_accounts tbody tr').length;
	rowCount++;
	
	var row = $('#table_impound_accounts').append('<tr id = "row_'+rowCount+'"><td><a href = "javascript:void(0);"  id=""  onclick ="delete_monthly_payments(this)" ><i class="fa fa-trash" aria-hidden="true"></i></a></td><td><input type="text" name="items[]"></td><td><input type="text" name="amount[]" class="number_only number_format" onchange="amount_format_change(this)" ></td></tr>');
		
	
}


function delete_monthly_payments(hj){

	var id=$(hj).attr('id');

if (confirm('Are you sure to delete?')) {
			

		$.ajax({
			type 	: 'POST',
			url 	: '<?php echo base_url()."Load_data/delte_load_pay";?>',
			data 	: {'id':id},
			success : function(response)
			{

			$(hj).parents("tr").remove(); 
				 	
				//alert(response);
			}
		});
				 
		 
	}	

}

function delete_hud(that){

var id=$(that).attr('id');

 if(id!=''){

   if (confirm('Are you sure to delete item ?')) {
			

		$.ajax({
			type 	: 'POST',
			url 	: '<?php echo base_url()."Load_data/delete_hudd";?>',
			data 	: {'id':id},
			success : function(response)
			{

			$(that).parents("tr").remove(); 
				 	
				//alert(response);
			}
		});
				 
		 
	 }	
  }else{
   
   $(that).parents("tr").remove(); 
  }
	
}



function open_custom_popup(popup){
	
	 //alert('#'+popup+'_confirmOverlay');
	// $('#'+popup+'_confirmOverlay').css('display','block');
	$('#'+popup+'_confirmOverlay').show();
}

function close_custom_popup(popup){
	$('#'+popup+'_confirmOverlay').css('display','none');
	
}

function save_info(popup){
	if(popup == 'closing_statement'){
		
		$('#closing_statement_action').val('close');
		$('#frm_closing_statement_items_id').submit();
		$('#closing_statement_confirmOverlay').css('display','none');
	
	}
	if(popup == 'assignment'){
		
		$('#assignment_action').val('close');
		$('#frm_assignment_id').submit();
		$('#assignment_confirmOverlay').css('display','none');
	
	}
	
}

function add_loan_document_row(){
	var rowCount = $('#loan_doc_table tbody tr').length;
	
	rowCount++;
	
	
	 var row = $('#loan_doc_table').append('<tr id = "row_'+rowCount+'"><td><input style="margin-left: 20px;" type="file" class="select_images_file" name="files[]"></td><input type = "hidden" id = "description_'+rowCount+'" value = "Promissory Note" name = "description[]" /><div style = "display:none;" class = "zero_value_'+rowCount+'"><input class = "form-control" type = "textbox" onchange = "fill_val(this.value,'+rowCount+');" id = "new_description_'+rowCount+'"  value = "" disabled/></div><td></td><tr>');
}
function fill_val(value,rowcnt){
	
		$('#description_'+rowcnt).val(value);
}
function fill_loan_document_description(rowcnt){
	var value = $('#doc_type_'+rowcnt).val();
	
	if(value == '0'){
		$('#new_description_'+rowcnt).prop('disabled',false);
		$('.zero_value_'+rowcnt).css('display','block');
		$('.other_value_'+rowcnt).css('display','none');
		$('#description_'+rowcnt).val('');
	}else{
		$('#new_description_'+rowcnt).prop('disabled',true);
		$('.zero_value_'+rowcnt).css('display','none');
		$('.other_value_'+rowcnt).css('display','block');
		var text = $('#doc_type_'+rowcnt+' option[value='+value+']').text();
		$('#description_'+rowcnt).val(text);
	}
	
	
}

function remove_doc_row(rowcnt){
	$('#row_'+rowcnt).remove();
}

function delete_document(rowcnt,id){
	if($('#delete_doc_'+rowcnt).prop('checked') == true){
		
		$('#doc_id_'+rowcnt).val(id);
	}else{
		$('#doc_id_'+rowcnt).val('');
		
	}
}
function delete_doc(){
	if (confirm('Are you sure to delete?')) {
			$('#frm_loan_document').submit();
	}
	
}

function fun_flip(val){
	if(val==2){
		$('input[name="broker_servicing_fees"]').val('<?php echo '$'.'0'; ?>');
		$('input[name="broker_servicing_fees"]').attr('disabled',true);
	}else{
		$('input[name="broker_servicing_fees"]').val('<?php echo '$'.number_format($loan_servicing['broker_servicing_fees'],2); ?>');
		$('input[name="broker_servicing_fees"]').attr('disabled',false);
	}
	
}


function add_property_data_rows(loan_id){
	// ajax call to add 4 rows (one primary and 3 secondary rows, if not exist ) for loan...
	 $.ajax({
		type 	: 'POST',
		url 	: '<?php echo base_url()."property_data/add_property_home_rows";?>',
		data 	: {'loan_id' : loan_id},
		success : function(response)
		{
			if(response == '1'){
				
				window.location.href = '<?php echo base_url();?>load_data/'+loan_id+'#property-data-home-page';	 
				window.location.reload(true);
			}else if(response == '0'){
				
				// window.location.href = '<?php echo base_url();?>load_data/'+loan_id+'#property-data-home-page';	 
				$('#property-data-home-page').modal('show');  
			}
			
		}
	}); 
	
	
}

function submit_loan_assignmentt(){
	
	
alert('TaliMar Database says: All Assignments Records Updated Successfully!');
$("form#frm_assignment_idd").submit();
	}

function submit_loan_assignment(){
	
	var total_investment = replace_dollar($('#assigment_total_investment').val());

	if(total_investment != 0){
		alert('TaliMar Database says: All Assignments Records Updated Successfully!');
	}
}

 $(document).ready(function(){
	var href = window.location.hash;
	
	if(href == '#property-data-home-page'){
		  
		$('#property-data-home-page').modal('show');  
	}
}); 



		

		
		
		
function check_wire_frame(ga){
  if ($(ga).is(':checked')) {
        
        $('#dis_print').removeAttr('disabled'); 
        
    } else {
        $('#dis_print').attr('disabled', true); 
    }
	
}

//..........................forclosure custom request starts................//

  function add_for_request(dd){
var c = $('.main_cc .row').length;
//var c = $('input#totalrowcnt').length;
var t =$('#for_t_ali').val();
	
	c++;
	 

 $('.main_cc').append('<div class="row"><div class="form-group disclosures_lable"><div class="col-md-6"><a  class="del_fro" title="Delete" id="new" onclick="delete_forclosure_data(this);" style="float: left;"><i class="fa fa-trash"></i></a>	<a href="javascript:void(0)" onclick="open_for_popup(this)"  id="" style="text-decoration:none;"><span><h4 style="font-size:14px;">Foreclosure '+c+'</h4></span></a><input type="hidden" name="for_id[]" value="new"><input type="hidden" id="for_t_ali" name="" value="'+t+'"><input type="hidden" id="for_name" name="for_name[]" value="foreclosure '+c+'"></div><div class="col-md-6"></div></div></div>');
	 
 }
 
 function change_for_slect(tccct){
	
var val_data=$(tccct).val();	
 var val = $(tccct).parent().parent().addClass('abc');
var val22 = $(tccct).parent().parent().find('a').attr('data-info', val_data);
var val22 = $(tccct).parent().parent().find('a').attr('data-info', val_data);


}
 
 function open_for_popup(pp){

var for_id = pp.id;

 var idAttr = $(pp).attr('data-info');

//$('#select_b').val(idAttr);


var for_name = $(pp).parent().find('input#for_name').val();
var for_tali = $(pp).parent().find('input#for_t_ali').val();

//$('#custom_add_foreclosure').modal('show'); 
				
if(for_id !=''){

$('#for_ex_idd').val(for_id);

$('select[name="contact_name"]').attr("class", "chosen677-"+for_id);
$('select[name="contact_name"]').attr("id", "contact_ajx_name-"+for_id);
$('.rs').attr("id", "chosen_removes-"+for_id);
$('#forr_name').val(for_name);

var idAttr = $(pp).attr('data-info');
var exten_box=$(pp).parent().parent().find('select#bb').val();
if(exten_box){
$('#select_for').val(exten_box);	
	
	
}

	 	$.ajax({
					    type 	: 'POST',
						url 	: '<?php echo base_url()."Load_data/fetch_data_forcloser"; ?>',
						data 	: {'for_id':for_id,'for_tali':for_tali},
						success : function(response){
							
						 var data = JSON.parse(response);
						  $.ajax({
									type	:'POST',
									url		:'<?php echo base_url()."Load_data/ajax_for_data";?>',
									data	:{'pay_pro':data.contact_name},
									success : function(response){
						
								 $('#chosen_removes-'+for_id).html('');
								
								var aaaa = '<select name="contact_name" id="contact_ajx_name" class="chosen677-'+for_id+'" onchange="contact_details(this);">'+response+'</select>';

								 $('#chosen_removes-'+for_id).html(aaaa);
								 $('.chosen677-'+for_id).chosen();
						 
										
									}
										
								});


						 if(data.active_forclosure=='2'){

 							 $('input[name="borrower_outreach"]').attr('disabled', true); 
 							 $('input[name="nod_recorded"]').attr('disabled', true); 
 							 $('input[name="nos_recorded"]').attr('disabled', true); 
 							 $('input[name="sale_date"]').attr('disabled', true); 

 							 $('select[name="contact_name"]').attr('disabled',true);
							 $('select[name="reason_for_default"]').attr('disabled',true);
							 $('select[name="foreclosuer_status"] ').attr('disabled',true);
							 $('select[name="foreclosuer_step"] ').attr('disabled',true);
							 $('select[name="foreclosuer_processor"]').attr('disabled',true);
							 $('select[name="borrower_outreach_option"]').attr('disabled',true);
							 $('select[name="nod_recorded_option"]').attr('disabled',true);
							 $('select[name="nos_recorded_option"]').attr('disabled',true);
							 $('select[name="sale_date_option"]').attr('disabled',true);
							 $('span#for_c_name').attr('disabled',true);
							 $('input[name="ts_number"]').attr('disabled',true);
						 	 $('textarea[name="status_update"]').attr('disabled',true);
						 	 $('a#fd').attr('disabled',true);

						 }else{

 						
 							 $('input[name="borrower_outreach"]').attr('disabled',false); 
 							 $('input[name="nod_recorded"]').attr('disabled', false); 
 							 $('input[name="nos_recorded"]').attr('disabled', false); 
 							 $('input[name="sale_date"]').attr('disabled', false); 

 							 $('select[name="contact_name"]').attr('disabled',false);
							 $('select[name="reason_for_default"]').attr('disabled',false);
							 $('select[name="foreclosuer_status"]').attr('disabled',false);
							 $('select[name="foreclosuer_step"]').attr('disabled',false);
							 $('select[name="foreclosuer_processor"]').attr('disabled',false);
							 $('select[name="borrower_outreach_option"]').attr('disabled',false);
							 $('select[name="nod_recorded_option"]').attr('disabled',false);
							 $('select[name="nos_recorded_option"]').attr('disabled',false);
							 $('select[name="sale_date_option"]').attr('disabled',false);
							 $('span#for_c_name').attr('disabled',false);
					 		 $('input[name="ts_number"]').attr('disabled',false);
					 		 $('textarea[name="status_update"]').attr('disabled',false);
					 		 $('a#fd').attr('disabled',false);
 							 
 							 




						 }
			
					 $('textarea[name="status_update"]').val(data.status_update);  
					 $('input#c_phone').val(data.c_phone);  
					 $('input#c_email').val(data.c_email); 
					 $('span#for_c_name').text(data.foreclosure_name); 
					 $('input[name="ts_number"]').val(data.ts_number); 
				     if(data.borrower_outreach=='12-31-1969'){
						date_borr='';
				     }else{

				     	date_borr=data.borrower_outreach;
				     }
					 $('input[name="borrower_outreach"]').val(date_borr);

					 if(data.nod_recorded=='12-31-1969'){
						date_nod='';
				     }else{

				     	date_nod=data.nod_recorded;
				     }

                     
					 $('input[name="nod_recorded"]').val(date_nod);

					 
					  if(data.nos_recorded=='12-31-1969'){
						date_nos='';
				     }else{

				     	date_nos=data.nos_recorded;
				     }


 
					 $('input[name="nos_recorded"]').val(date_nos);


					  if(data.sale_date=='12-31-1969'){
						date_sale='';
				     }else{

				     	date_sale=data.sale_date;
				     }

 
					 $('input[name="sale_date"]').val(date_sale);

					// $('select[name="contact_name"] option[value="'+data.contact_name+'"]').attr('selected',true);
					 $('select[name="reason_for_default"] option[value="'+data.reason_for_default+'"]').attr('selected',true);
					 $('select[name="foreclosuer_status"] option[value="'+data.foreclosuer_status+'"]').attr('selected',true);
					 $('select[name="foreclosuer_step"] option[value="'+data.foreclosuer_step+'"]').attr('selected',true);
					 $('select[name="foreclosuer_processor"] option[value="'+data.foreclosuer_processor+'"]').attr('selected',true);
					 $('select[name="borrower_outreach_option"] option[value="'+data.borrower_outreach_option+'"]').attr('selected',true);
					 $('select[name="nod_recorded_option"] option[value="'+data.nod_recorded_option+'"]').attr('selected',true);
					 $('select[name="nos_recorded_option"] option[value="'+data.nos_recorded_option+'"]').attr('selected',true);
					 $('select[name="sale_date_option"] option[value="'+data.sale_date_option+'"]').attr('selected',true);
					 $('select[name="active_forclosure"] option[value="'+data.active_forclosure+'"]').attr('selected',true);
					
				
                     $('#custom_add_foreclosure').modal('show'); 
					}
						
						
						
					});
	 
  }else{
	  
 var idAttr = $(pp).attr('data-info');
$('#select_for').val(idAttr);


$('#forr_name').val(for_name);

$('#for_ex_idd').val('new');
$('select[name="contact_name"]').attr("class", "chosen677-"+for_id);
$('select[name="contact_name"]').attr("id", "contact_ajx_name-"+for_id);
$('.rs').attr("id", "chosen_removes-"+for_id);
	
	
	$.ajax({
					    type    : 'POST',
						url 	: '<?php echo base_url()."Load_data/fetch_data_forcloser"; ?>',
						data 	: {'for_id':for_id},
						success : function(response){
							
						 var data = JSON.parse(response);
							 if(data.active_forclosure=='2'){

 							 $('input[name="borrower_outreach"]').attr('disabled', true); 
 							 $('input[name="nod_recorded"]').attr('disabled', true); 
 							 $('input[name="nos_recorded"]').attr('disabled', true); 
 							 $('input[name="sale_date"]').attr('disabled', true); 

 							 $('select[name="contact_name"]').attr('disabled',true);
							 $('select[name="reason_for_default"]').attr('disabled',true);
							 $('select[name="foreclosuer_status"] ').attr('disabled',true);
							 $('select[name="foreclosuer_step"] ').attr('disabled',true);
							 $('select[name="foreclosuer_processor"]').attr('disabled',true);
							 $('select[name="borrower_outreach_option"]').attr('disabled',true);
							 $('select[name="nod_recorded_option"]').attr('disabled',true);
							 $('select[name="nos_recorded_option"]').attr('disabled',true);
							 $('select[name="sale_date_option"]').attr('disabled',true);
							 $('span#for_c_name').attr('disabled',true);
							 $('input[name="ts_number"]').attr('disabled',true);
						 	 $('textarea[name="status_update"]').attr('disabled',true);
						 	  $('a#fd').attr('disabled',true);
 							 
						 }else{

 						
 							 $('input[name="borrower_outreach"]').attr('disabled',false); 
 							 $('input[name="nod_recorded"]').attr('disabled', false); 
 							 $('input[name="nos_recorded"]').attr('disabled', false); 
 							 $('input[name="sale_date"]').attr('disabled', false); 

 							 $('select[name="contact_name"]').attr('disabled',false);
							 $('select[name="reason_for_default"]').attr('disabled',false);
							 $('select[name="foreclosuer_status"]').attr('disabled',false);
							 $('select[name="foreclosuer_step"]').attr('disabled',false);
							 $('select[name="foreclosuer_processor"]').attr('disabled',false);
							 $('select[name="borrower_outreach_option"]').attr('disabled',false);
							 $('select[name="nod_recorded_option"]').attr('disabled',false);
							 $('select[name="nos_recorded_option"]').attr('disabled',false);
							 $('select[name="sale_date_option"]').attr('disabled',false);
							 $('span#for_c_name').attr('disabled',false);
					 		 $('input[name="ts_number"]').attr('disabled',false);
					 		 $('textarea[name="status_update"]').attr('disabled',false);
 							   $('a#fd').attr('disabled',false);
 							 




						 }
						 $.ajax({
									type	:'POST',
									url		:'<?php echo base_url()."Load_data/ajax_for_data";?>',
									data	:{},
									success : function(response){
						
								 $('#chosen_removes-'+for_id).html('');
								var aaaa = '<select name="contact_name" id="contact_ajx_name" class="chosen677-'+for_id+'" onchange="contact_details(this);">'+response+'</select>';

								 $('#chosen_removes-'+for_id).html(aaaa);
								$('.chosen677-'+for_id).chosen();
						 
										
									}
										
								});
			
					 $('textarea[name="status_update"]').val(data.status_update);  
					 $('input#c_phone').val(data.c_phone);  
					 $('input#c_email').val(data.c_email); 
					 $('span#for_c_name').text(data.foreclosure_name); 
					 $('input[name="ts_number"]').val(data.ts_number); 
					 $('input[name="borrower_outreach"]').val(data.borrower_outreach); 
					 $('input[name="nod_recorded"]').val(data.nod_recorded); 
					 $('input[name="nos_recorded"]').val(data.nos_recorded); 
					 $('input[name="sale_date"]').val(data.sale_date);   
					 
					//$('select[name="contact_name"] option[value="'+data.contact_name+'"]').attr('selected',true);
					
					 $('select[name="reason_for_default"] option[value="'+data.reason_for_default+'"]').attr('selected',true);
					 $('select[name="foreclosuer_status"] option[value="'+data.foreclosuer_status+'"]').attr('selected',true);
					 $('select[name="foreclosuer_step"] option[value="'+data.foreclosuer_step+'"]').attr('selected',true);
					 $('select[name="foreclosuer_processor"] option[value="'+data.foreclosuer_processor+'"]').attr('selected',true);
					 $('select[name="borrower_outreach_option"] option[value="'+data.borrower_outreach_option+'"]').attr('selected',true);
					 $('select[name="nod_recorded_option"] option[value="'+data.nod_recorded_option+'"]').attr('selected',true);
					 $('select[name="nos_recorded_option"] option[value="'+data.nos_recorded_option+'"]').attr('selected',true);
					 $('select[name="sale_date_option"] option[value="'+data.sale_date_option+'"]').attr('selected',true);
					 $('select[name="active_forclosure"] option[value="'+data.active_forclosure+'"]').attr('selected',true);
					
					 
					 
					 
					 
                     $('#custom_add_foreclosure').modal('show'); 
					}
						
						
						
					});
	
    
	 


}


 }
 

 function active_forclosure_fun(fuj){

   if($(fuj).val()=='2'){
		$('input[name="borrower_outreach"]').attr('disabled', true); 
		$('input[name="nod_recorded"]').attr('disabled', true); 
		$('input[name="nos_recorded"]').attr('disabled', true); 
		$('input[name="sale_date"]').attr('disabled', true); 

		$('select[name="contact_name"]').attr('disabled',true);
		$('select[name="reason_for_default"]').attr('disabled',true);
		$('select[name="foreclosuer_status"] ').attr('disabled',true);
		$('select[name="foreclosuer_step"] ').attr('disabled',true);
		$('select[name="foreclosuer_processor"]').attr('disabled',true);
		$('select[name="borrower_outreach_option"]').attr('disabled',true);
		$('select[name="nod_recorded_option"]').attr('disabled',true);
		$('select[name="nos_recorded_option"]').attr('disabled',true);
		$('select[name="sale_date_option"]').attr('disabled',true);
		$('span#for_c_name').attr('disabled',true);
		$('input[name="ts_number"]').attr('disabled',true);
		$('textarea[name="status_update"]').attr('disabled',true);
		$('a#fd').attr('disabled',true);

   }else{

			$('input[name="borrower_outreach"]').attr('disabled',false); 
			$('input[name="nod_recorded"]').attr('disabled', false); 
			$('input[name="nos_recorded"]').attr('disabled', false); 
			$('input[name="sale_date"]').attr('disabled', false); 

			$('select[name="contact_name"]').attr('disabled',false);
			$('select[name="reason_for_default"]').attr('disabled',false);
			$('select[name="foreclosuer_status"]').attr('disabled',false);
			$('select[name="foreclosuer_step"]').attr('disabled',false);
			$('select[name="foreclosuer_processor"]').attr('disabled',false);
			$('select[name="borrower_outreach_option"]').attr('disabled',false);
			$('select[name="nod_recorded_option"]').attr('disabled',false);
			$('select[name="nos_recorded_option"]').attr('disabled',false);
			$('select[name="sale_date_option"]').attr('disabled',false);
			$('span#for_c_name').attr('disabled',false);
			$('input[name="ts_number"]').attr('disabled',false);
			$('textarea[name="status_update"]').attr('disabled',false);
			$('a#fd').attr('disabled',false);

   }

 }
 
//..........................forclosure custom request end................// 
 
 
 
 
 
 
 function add_extension_request(d){
var c = $('.main_c .row').length;
var t =$('#t_ali').val();
	
	
$('.main_c').append('<div class="row" id="removeex_'+c+'"><div class="form-group disclosures_lable"><div class="col-md-4"><a href="javascript:void(0)" onclick="open_popup(this)"  id="" style="text-decoration:none;"><p>Extension '+c+'</p></a><input type="hidden" name="ex_id[]" value="new"><input type="hidden" id="t_ali" name="" value="'+t+'"><input type="hidden" id="e_name" name="extension_name[]" value="extension '+c+'"></div><div class="col-md-4"></div><div class="col-md-3"><p>In Process</p></div><div class="col-md-1"><a id="'+c+'" onclick="removeextensiondef(this.id);"><i class="fa fa-trash"></i></a></div></div></div>');
	 
	 c++;
 }
 
 function removeextensiondef(that){
	 
	 $('.main_c div#removeex_'+that).remove();
 }

function change_slect(tct){
	
var val_data=$(tct).val();	
 var val = $(tct).parent().parent().addClass('abc');
var val22 = $(tct).parent().parent().find('a').attr('data-info', val_data);
var val22 = $(tct).parent().parent().find('a').attr('data-info', val_data);


}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}


function open_popup(p){

var ext_id = p.id;

var e_name = $(p).parent().find('input#e_name').val();
var tali = $(p).parent().find('input#t_ali').val();


if(ext_id !=''){

$('#ex_idd').val(ext_id);
$('#exten_name').val(e_name);

var idAttr = $(p).attr('data-info');
var exten_box=$(p).parent().parent().find('select#b').val();
if(exten_box){
$('#select_b').val(exten_box);	
	
	
}

	 	$.ajax({
					    type 	: 'POST',
						url 	: '<?php echo base_url()."Load_data/fetch_data_extension"; ?>',
						data 	: {'ext_id':ext_id,'tali':tali},
						success : function(response){
							
						 var data = JSON.parse(response);
						 
						 
					
						var extension_complete = data.extension_complete;							
						var extension_disbursed = data.extension_disbursed;							
						var extension_processed = data.extension_processed;	
						var extension_received = data.extension_received;						
						var extension_submitted = data.extension_submitted;							
						var extension_offered = data.extension_offered;							
						var extension_signed = data.extension_signed;
						var PaymentType = data.PaymentType;
						var PaymentCal = data.PaymentCal;
						var BorrowerApproval = data.BorrowerApproval;
						var ext_accrued = data.ext_accrued;
						var update_loan_term = data.update_loan_term;
						var confirm_maturity = data.confirm_maturity;
						var ext_lender_app = data.ext_lender_app;
						var ext_process_option = data.ext_process_option;
						var ext_req_borrower = data.ext_req_borrower;
						var ext_app_rec_lender = data.ext_app_rec_lender;
						var pay_amt_opt = data.pay_amt_opt;
						var pay_amt_val = data.pay_amt_val;
						var notify_loanext = data.notify_loanext;

						//alert(update_loan_term);
					//alert(extension_received);
						
					 $('textarea#extension_notes').val(data.extension_notes);  
					 $('input#ex_date_2').val(data.extension_date_from); 
					 if(pay_amt_val > 0){ 
					 	$('input#pay_amt_val').val('$'+addCommas(pay_amt_val)); 
					 }else{
					 	$('input#pay_amt_val').val('');
					 } 
					 $('input#ex_date').val(data.extension_to); 
					 $('span#ex_name').text(data.extension_name); 
					 $('select[name="PaymentType"] option[value="'+PaymentType+'"]').attr('selected',true); 
					 $('select[name="PaymentCal"] option[value="'+PaymentCal+'"]').attr('selected',true); 
					 $('select[name="pay_amt_opt"] option[value="'+pay_amt_opt+'"]').attr('selected',true); 
					 $('select[name="BorrowerApproval"] option[value="'+BorrowerApproval+'"]').attr('selected',true); 
					 $('select[name="ext_lender_app"] option[value="'+ext_lender_app+'"]').attr('selected',true); 
					 $('select[name="ext_process_option"] option[value="'+ext_process_option+'"]').attr('selected',true); 

					 if(ext_app_rec_lender == 1){
					 	$('#uniform-ext_app_rec_lender input#ext_app_rec_lender').attr('checked','checked'); 
					 	$('#uniform-ext_app_rec_lender span').addClass('checked'); 
					 	$('#uniform-ext_app_rec_lender input#ext_app_rec_lender').val(ext_app_rec_lender); 
						//$('#uniform-extension_no span').removeClass('checked'); 
					 	//$('#uniform-extension_no input#extension_no').val(''); 
						
					 }else if(ext_app_rec_lender != 1){
						$('#uniform-ext_app_rec_lender input#ext_app_rec_lender').prop('checked', false);
					 	$('#uniform-ext_app_rec_lender span').removeClass('checked'); 
					 	$('#uniform-ext_app_rec_lender input#ext_app_rec_lender').val(''); 
						//$('#uniform-extension_no span').removeClass('checked'); 
					 	//$('#uniform-extension_no input#extension_no').val('');  
					 }
					 else{
						 //$('#uniform-extension_yes input#extension_yes').attr('','');
	                    $('#uniform-ext_app_rec_lender span').removeClass('checked'); 
					 	$('#uniform-ext_app_rec_lender input#ext_app_rec_lender').val(''); 
					 	//$('#uniform-extension_no input#extension_no').attr('checked','checked'); 
					 	//$('#uniform-extension_no span').addClass('checked'); 
					 	//$('#uniform-extension_no input#extension_no').val(extension_complete);  
					 }

					 if(ext_req_borrower == 1){
					 	$('#uniform-ext_req_borrower input#ext_req_borrower').attr('checked','checked'); 
					 	$('#uniform-ext_req_borrower span').addClass('checked'); 
					 	$('#uniform-ext_req_borrower input#ext_req_borrower').val(ext_req_borrower); 
						//$('#uniform-extension_no span').removeClass('checked'); 
					 	//$('#uniform-extension_no input#extension_no').val(''); 
						
					 }else if(ext_req_borrower != 1){
						$('#uniform-ext_req_borrower input#ext_req_borrower').prop('checked', false);
					 	$('#uniform-ext_req_borrower span').removeClass('checked'); 
					 	$('#uniform-ext_req_borrower input#ext_req_borrower').val(''); 
						//$('#uniform-extension_no span').removeClass('checked'); 
					 	//$('#uniform-extension_no input#extension_no').val('');  
					 }
					 else{
						 //$('#uniform-extension_yes input#extension_yes').attr('','');
	                    $('#uniform-ext_req_borrower span').removeClass('checked'); 
					 	$('#uniform-ext_req_borrower input#ext_req_borrower').val(''); 
					 	//$('#uniform-extension_no input#extension_no').attr('checked','checked'); 
					 	//$('#uniform-extension_no span').addClass('checked'); 
					 	//$('#uniform-extension_no input#extension_no').val(extension_complete);  
					 }

					 if(notify_loanext == 1){
					 	$('#uniform-notify_loanext input#notify_loanext').attr('checked','checked'); 
					 	$('#uniform-notify_loanext span').addClass('checked'); 
					 	$('#uniform-notify_loanext input#notify_loanext').val(notify_loanext); 
						//$('#uniform-extension_no span').removeClass('checked'); 
					 	//$('#uniform-extension_no input#extension_no').val(''); 
						
					 }else if(ext_req_borrower != 1){
						$('#uniform-notify_loanext input#notify_loanext').prop('checked', false);
					 	$('#uniform-notify_loanext span').removeClass('checked'); 
					 	$('#uniform-notify_loanext input#notify_loanext').val(''); 
						//$('#uniform-extension_no span').removeClass('checked'); 
					 	//$('#uniform-extension_no input#extension_no').val('');  
					 }
					 else{
						 //$('#uniform-extension_yes input#extension_yes').attr('','');
	                    $('#uniform-notify_loanext span').removeClass('checked'); 
					 	$('#uniform-notify_loanext input#notify_loanext').val(''); 
					 	//$('#uniform-extension_no input#extension_no').attr('checked','checked'); 
					 	//$('#uniform-extension_no span').addClass('checked'); 
					 	//$('#uniform-extension_no input#extension_no').val(extension_complete);  
					 }


					 if(extension_complete == 1){
					 	$('#uniform-extension_yes input#extension_yes').attr('checked','checked'); 
					 	$('#uniform-extension_yes span').addClass('checked'); 
					 	$('#uniform-extension_yes input#extension_yes').val(extension_complete); 
						$('#uniform-extension_no span').removeClass('checked'); 
					 	$('#uniform-extension_no input#extension_no').val(''); 
						
					 }else if(extension_complete != 1){
						$('#uniform-extension_yes input#extension_yes').prop('checked', false);
					 	$('#uniform-extension_yes span').removeClass('checked'); 
					 	$('#uniform-extension_yes input#extension_yes').val(''); 
						$('#uniform-extension_no span').removeClass('checked'); 
					 	$('#uniform-extension_no input#extension_no').val('');  
					 }
					 else{
						 //$('#uniform-extension_yes input#extension_yes').attr('','');
	                    $('#uniform-extension_yes span').removeClass('checked'); 
					 	$('#uniform-extension_yes input#extension_yes').val(''); 
					 	$('#uniform-extension_no input#extension_no').attr('checked','checked'); 
					 	$('#uniform-extension_no span').addClass('checked'); 
					 	$('#uniform-extension_no input#extension_no').val(extension_complete);  
					 }

					if(PaymentType == 1){

						$('#uniform-extenpay_yes span').removeClass('checked'); 
					 	$('#uniform-extenpay_yes input#extenpay_yes').val('').attr('disabled',true); 

					}else{
					 
						if(extension_disbursed == 1){
						 	$('#uniform-extenpay_yes input#extenpay_yes').attr('checked','checked'); 
						 	$('#uniform-extenpay_yes span').addClass('checked'); 
						 	$('#uniform-extenpay_yes input#extenpay_yes').val(extension_disbursed); 
							
							$('#uniform-extenpay_no span').removeClass('checked'); 
						 	$('#uniform-extenpay_no input#extenpay_no').val('');  
						} 
						else if(extension_disbursed ==''){
							$('#uniform-extenpay_yes input#extenpay_yes').prop('checked', false); 
						 	$('#uniform-extenpay_yes span').removeClass('checked'); 
						 	$('#uniform-extenpay_yes input#extenpay_yes').val(''); 
							
							$('#uniform-extenpay_no span').removeClass('checked'); 
						 	$('#uniform-extenpay_no input#extenpay_no').val('');  
							 
							 
						}else{
							 
							 
		                    $('#uniform-extenpay_yes span').removeClass('checked'); 
						 	$('#uniform-extenpay_yes input#extenpay_yes').val(''); 
							
						 	$('#uniform-extenpay_no input#extenpay_no').attr('checked','checked'); 
						 	$('#uniform-extenpay_no span').addClass('checked'); 
						 	$('#uniform-extenpay_no input#extenpay_no').val(extension_disbursed);  
						}
					}
					 
					  if(extension_processed == 1){
					 	$('#uniform-extenopt_yes input#extenopt_yes').attr('checked','checked'); 
					 	$('#uniform-extenopt_yes span').addClass('checked'); 
					 	$('#uniform-extenopt_yes input#extenopt_yes').val(extension_processed); 
						$('#uniform-extenopt_no span').removeClass('checked'); 
					 	$('#uniform-extenopt_no input#extenopt_no').val('');  
					 } 
					 	 
						 else if(extension_processed ==''){
						 
						$('#uniform-extenopt_yes input#extenopt_yes').prop('checked', false); 
					 	$('#uniform-extenopt_yes span').removeClass('checked'); 
					 	$('#uniform-extenopt_yes input#extenopt_yes').val(''); 
						$('#uniform-extenopt_no span').removeClass('checked'); 
					 	$('#uniform-extenopt_no input#extenopt_no').val('');  
						 
						 
					 }
					 
					 else
					 {
                       $('#uniform-extenopt_yes span').removeClass('checked'); 
					 	$('#uniform-extenopt_yes input#extenopt_yes').val(''); 
					 	$('#uniform-extenopt_no input#extenopt_no').attr('checked','checked'); 
					 	$('#uniform-extenopt_no span').addClass('checked'); 
					 	$('#uniform-extenopt_no input#extenopt_no').val(extension_processed);  
					   
					   }  
					 
					 
					if(PaymentType == 1){

						$('#uniform-extenrecived_yes11 span').removeClass('checked'); 
					 	$('#uniform-extenrecived_yes11 input#extenrecived_yes11').val('').attr('disabled',true); 

					} else{

						if(extension_received == 1){
							//alert(extension_received);
						 	$('#uniform-extenrecived_yes11 input#extenrecived_yes11').attr('checked','checked'); 
						 	$('#uniform-extenrecived_yes11 span').addClass('checked'); 
						 	$('#uniform-extenrecived_yes11 input#extenrecived_yes11').val(extension_received); 
						
							$('#uniform-extenrecived_no span').removeClass('checked'); 
						 	$('#uniform-extenrecived_no input#extenrecived_no').val('');  

						}else if(extension_received == ''){
							 
							$('#uniform-extenrecived_yes11 input#extenrecived_yes11').prop('checked', false); 
						 	$('#uniform-extenrecived_yes11 span').removeClass('checked'); 
						 	$('#uniform-extenrecived_yes11 input#extenrecived_yes11').val(''); 
						
							$('#uniform-extenrecived_no span').removeClass('checked'); 
						 	$('#uniform-extenrecived_no input#extenrecived_no').val('');  
							 
						}else{

	                        $('#uniform-extenrecived_yes11 span').removeClass('checked'); 
						 	$('#uniform-extenrecived_yes11 input#extenrecived_yes11').val(''); 
						 	$('#uniform-extenrecived_no input#extenrecived_no').attr('checked','checked'); 
						 	$('#uniform-extenrecived_no span').addClass('checked'); 
						 	$('#uniform-extenrecived_no input#extenrecived_no').val(extension_received);    
						}    
					}
					 
					 if(extension_offered == 1){
					 	$('#uniform-exten_off_yes input#exten_off_yes').attr('checked','checked'); 
					 	$('#uniform-exten_off_yes span').addClass('checked'); 
					 	$('#uniform-exten_off_yes input#exten_off_yes').val(extension_offered); 
						$('#uniform-exten_off_no span').removeClass('checked'); 
					 	$('#uniform-exten_off_no input#exten_off_no').val(''); 
					 } 
					 else if(extension_offered==''){
						 
						$('#uniform-exten_off_yes input#exten_off_yes').prop('checked', false); 
					 	$('#uniform-exten_off_yes span').removeClass('checked'); 
					 	$('#uniform-exten_off_yes input#exten_off_yes').val(''); 
						$('#uniform-exten_off_no span').removeClass('checked'); 
					 	$('#uniform-exten_off_no input#exten_off_no').val(''); 
						 
						 
					 }
						 else
					 {
						$('#uniform-exten_off_yes span').removeClass('checked'); 
					 	$('#uniform-exten_off_yes input#exten_off_yes').val(''); 
					 	$('#uniform-exten_off_no input#exten_off_no').attr('checked','checked'); 
					 	$('#uniform-exten_off_no span').addClass('checked'); 
					 	$('#uniform-exten_off_no input#exten_off_no').val(extension_offered);  
					   
					   }     
					  
						 
					 if(extension_signed == 1){
					 	$('#uniform-extenagre_yes input#extenagre_yes').attr('checked','checked'); 
					 	$('#uniform-extenagre_yes span').addClass('checked'); 
					 	$('#uniform-extenagre_yes input#extenagre_yes').val(extension_signed); 
						$('#uniform-extenagre_no span').removeClass('checked'); 
					 	$('#uniform-extenagre_no input#extenagre_no').val('');  
						
					 } 
					 
					 	 else if(extension_signed ==''){
						 
						$('#uniform-extenagre_yes input#extenagre_yes').prop('checked', false); 
					 	$('#uniform-extenagre_yes span').removeClass('checked'); 
					 	$('#uniform-extenagre_yes input#extenagre_yes').val(''); 
						$('#uniform-extenagre_no span').removeClass('checked'); 
					 	$('#uniform-extenagre_no input#extenagre_no').val('');  
						 
						 
					 }					 
					 
					 else
					 {
						$('#uniform-extenagre_yes span').removeClass('checked'); 
					 	$('#uniform-extenagre_yes input#extenagre_yes').val(''); 
					 	$('#uniform-extenagre_no input#extenagre_no').attr('checked','checked'); 
					 	$('#uniform-extenagre_no span').addClass('checked'); 
					 	$('#uniform-extenagre_no input#extenagre_no').val(extension_signed);  
					   
					 }


					if(PaymentType == 2){

						$('#uniform-ext_Accrued_Charges span').removeClass('checked'); 
					 	$('#uniform-ext_Accrued_Charges input#ext_Accrued_Charges').val('').attr('disabled',true); 

					} else{

					 if(ext_accrued == 1){
					 	$('#uniform-ext_Accrued_Charges input#ext_Accrued_Charges').attr('checked','checked'); 
					 	$('#uniform-ext_Accrued_Charges span').addClass('checked'); 
					 	$('#uniform-ext_Accrued_Charges input#ext_Accrued_Charges').val(ext_accrued); 
						//$('#uniform-extenagre_no span').removeClass('checked'); 
					 	//$('#uniform-extenagre_no input#extenagre_no').val('');  
						
					 }else{

					 	$('#uniform-ext_Accrued_Charges span').removeClass('checked'); 
					 	$('#uniform-ext_Accrued_Charges input#ext_Accrued_Charges').val(''); 
					 	//$('#uniform-extenagre_no input#extenagre_no').attr('checked','checked');
					 }
					}

					if(update_loan_term == 1){
					 	$('#uniform-updated_loan_term input#updated_loan_term').attr('checked','checked'); 
					 	$('#uniform-updated_loan_term span').addClass('checked'); 
					 	$('#uniform-updated_loan_term input#updated_loan_term').val(update_loan_term); 
						//$('#uniform-extenagre_no span').removeClass('checked'); 
					 	//$('#uniform-extenagre_no input#extenagre_no').val('');  
						
					 }else{

					 	$('#uniform-updated_loan_term span').removeClass('checked'); 
					 	$('#uniform-updated_loan_term input#updated_loan_term').val(''); 
					 	$('#uniform-extenagre_no input#updated_loan_term').attr('checked','');
					 }

					 if(confirm_maturity == 1){
					 	$('#uniform-confirm_maturity_date input#confirm_maturity_date').attr('checked','checked'); 
					 	$('#uniform-confirm_maturity_date span').addClass('checked'); 
					 	$('#uniform-confirm_maturity_date input#confirm_maturity_date').val(confirm_maturity); 
						//$('#uniform-extenagre_no span').removeClass('checked'); 
					 	//$('#uniform-extenagre_no input#extenagre_no').val('');  
						
					 }else{

					 	$('#uniform-confirm_maturity_date span').removeClass('checked'); 
					 	$('#uniform-confirm_maturity_date input#confirm_maturity_date').val(''); 
					 	$('#uniform-extenagre_no input#confirm_maturity_date').attr('checked','');
					 }
					console.log("extension submittion"+extension_submitted);
					 if(extension_submitted == 1){
							//alert(extension_received);
						 	$('.extension_agreement_submitted input#extenrecived_yes12').attr('checked','checked'); 
						 	$('.extension_agreement_submitted input#extenrecived_yes12').val(extension_submitted);  

						}else{ 
						 	$('.extension_agreement_submitted input#extenrecived_no').attr('checked','checked'); 
						 	$('.extension_agreement_submitted input#extenrecived_no').val(extension_submitted);    
						}    
                     $('#custom_add_extension').modal('show'); 
					}
						
						
						
					});
	 
  }else{
	  
 var idAttr = $(p).attr('data-info');
$('#select_b').val(idAttr);


$('#exten_name').val(e_name);

	$('#ex_idd').val('new');
	
	
	$.ajax({
					    type    : 'POST',
						url 	: '<?php echo base_url()."Load_data/fetch_data_extension"; ?>',
						data 	: {'ext_id':ext_id},
						success : function(response){
							
						 var data = JSON.parse(response);
					      
						var extension_complete = data.extension_complete;							
						var extension_disbursed = data.extension_disbursed;							
						var extension_processed = data.extension_processed;							
						var extension_received = data.extension_received;							
						var extension_offered = data.extension_offered;							
						var extension_signed = data.extension_signed;							
					 $('textarea#extension_notes').val(data.extension_notes);  
					 $('input#ex_date_2').val(data.extension_date_from);  
					 $('input#ex_date').val(data.extension_to); 
					 $('span#ex_name').text(data.extension_name); 

					 if(extension_complete == 1){
					 	$('#uniform-extension_yes input#extension_yes').attr('checked','checked'); 
					 	$('#uniform-extension_yes span').removeClass('checked'); 
					 	$('#uniform-extension_yes input#extension_yes').val(extension_complete); 
						$('#uniform-extension_no span').removeClass('checked'); 
					 	$('#uniform-extension_no input#extension_no').val('');  
					 } else{
	                         $('#uniform-extension_yes span').removeClass('checked'); 
					 	$('#uniform-extension_yes input#extension_yes').val(''); 
					 	$('#uniform-extension_no input#extension_no').attr('checked','checked'); 
					 	$('#uniform-extension_no span').removeClass('checked'); 
					 	$('#uniform-extension_no input#extension_no').val(extension_complete);  
					 }
					 
					 
					  if(extension_disbursed == 1){
					 	$('#uniform-extenpay_yes input#extenpay_yes').attr('checked','checked'); 
					 	$('#uniform-extenpay_yes span').removeClass('checked'); 
					 	$('#uniform-extenpay_yes input#extenpay_yes').val(extension_disbursed); 
						
						$('#uniform-extenpay_no span').removeClass('checked'); 
					 	$('#uniform-extenpay_no input#extenpay_no').val('');  
					 } else{
	                      $('#uniform-extenpay_yes span').removeClass('checked'); 
					 	$('#uniform-extenpay_yes input#extenpay_yes').val(''); 
						
					 	$('#uniform-extenpay_no input#extenpay_no').attr('checked','checked'); 
					 	$('#uniform-extenpay_no span').removeClass('checked'); 
					 	$('#uniform-extenpay_no input#extenpay_no').val(extension_disbursed);  
					   }
					 
					  if(extension_processed == 1){
					 	$('#uniform-extenopt_yes input#extenopt_yes').attr('checked','checked'); 
					 	$('#uniform-extenopt_yes span').addClass('checked'); 
					 	$('#uniform-extenopt_yes input#extenopt_yes').val(extension_processed); 
						$('#uniform-extenopt_no span').removeClass('checked'); 
					 	$('#uniform-extenopt_no input#extenopt_no').val('');  
					 } 
					 else
					 {
                       $('#uniform-extenopt_yes span').removeClass('checked'); 
					 	$('#uniform-extenopt_yes input#extenopt_yes').val(''); 
					 	$('#uniform-extenopt_no input#extenopt_no').attr('checked','checked'); 
					 	$('#uniform-extenopt_no span').removeClass('checked'); 
					 	$('#uniform-extenopt_no input#extenopt_no').val(extension_processed);  
					   
					   }  
					 
					 
					 
					 if(extension_received == 1){
					 	$('#uniform-extenrecived_yes input#extenrecived_yes').attr('checked','checked'); 
					 	$('#uniform-extenrecived_yes span').removeClass('checked'); 
					 	$('#uniform-extenrecived_yes input#extenrecived_yes').val(extension_received); 
					
						$('#uniform-extenrecived_no span').removeClass('checked'); 
					 	$('#uniform-extenrecived_no input#extenrecived_no').val('');  

					} 
					 else
					 {
                        $('#uniform-extenrecived_yes span').removeClass('checked'); 
					 	$('#uniform-extenrecived_yes input#extenrecived_yes').val(''); 
					 	$('#uniform-extenrecived_no input#extenrecived_no').attr('checked','checked'); 
					 	$('#uniform-extenrecived_no span').removeClass('checked'); 
					 	$('#uniform-extenrecived_no input#extenrecived_no').val(extension_received);  
					   
					   }  
					  
					   
					   
					 
					 if(extension_offered == 1){
						 

					 	$('#uniform-exten_off_yes input#exten_off_yes').attr('checked','checked'); 
					 	$('#uniform-exten_off_yes span').removeClass('checked'); 
					 	$('#uniform-exten_off_yes input#exten_off_yes').val(extension_offered); 
						$('#uniform-exten_off_no span').removeClass('checked'); 
					 	$('#uniform-exten_off_no input#exten_off_no').val(''); 
					 } 
					 else
					 {
						$('#uniform-exten_off_yes span').removeClass('checked'); 
					 	$('#uniform-exten_off_yes input#exten_off_yes').val(''); 
					 	$('#uniform-exten_off_no input#exten_off_no').attr('checked','checked'); 
					 	$('#uniform-exten_off_no span').removeClass('checked'); 
					 	$('#uniform-exten_off_no input#exten_off_no').val(extension_offered);  
					   
					   }     
					  
						 
					 if(extension_signed == 1){
					 	$('#uniform-extenagre_yes input#extenagre_yes').attr('checked','checked'); 
					 	$('#uniform-extenagre_yes span').removeClass('checked'); 
					 	$('#uniform-extenagre_yes input#extenagre_yes').val(extension_signed); 
						$('#uniform-extenagre_no span').removeClass('checked'); 
					 	$('#uniform-extenagre_no input#extenagre_no').val('');  
						
					 } 
					 else
					 {
						$('#uniform-extenagre_yes span').removeClass('checked'); 
					 	$('#uniform-extenagre_yes input#extenagre_yes').val(''); 
					 	$('#uniform-extenagre_no input#extenagre_no').attr('checked','checked'); 
					 	$('#uniform-extenagre_no span').removeClass('checked'); 
					 	$('#uniform-extenagre_no input#extenagre_no').val(extension_signed);  
					   
					   } 
                
                     $('#custom_add_extension').modal('show'); 
					}
						
						
						
					});
	
    
	 


}

$( ".ex_date").datepicker({ dateFormat: 'mm-dd-yy' });
$( ".accured_datee").datepicker({ dateFormat: 'mm-dd-yy' });

 }
 
 
  function fun_extension_yes(ey){
	  
	if($(ey).is(':checked')){
		$('#extension_yes').val('1');
		$('#extension_no').val('');
	}else{
		$('#extension_yes').val('');	
		
	}
	  
  }
function fun_extension_no(ewy){
	  
	if($(ewy).is(':checked')){
		$('#extension_yes').val('');
		$('#extension_no').val('0');
	}else{
		$('#extension_no').val('');	
		
	}
	  
  }


  function fun_extenpay_yes(eyb){
	  
	if($(eyb).is(':checked')){
		$('#extenpay_yes').val('1');
		$('#extenpay_no').val('');
	}else{
		$('#extenpay_yes').val('');	
		
	}
	  
  }

  function fun_update_loan_term(eyb){
	  
	if($(eyb).is(':checked')){
		$('#updated_loan_term').val('1');
		//$('#extenpay_no').val('');
	}else{
		$('#updated_loan_term').val('');	
		
	}
	  
  }

  function fun_confirm_maturity_date(eyb){
	  
	if($(eyb).is(':checked')){
		$('#confirm_maturity_date').val('1');
		//$('#extenpay_no').val('');
	}else{
		$('#confirm_maturity_date').val('');	
		
	}
	  
  }
  
function fun_extenpay_no(esy){
	  
	if($(esy).is(':checked')){
		$('#extenpay_yes').val('');
		$('#extenpay_no').val('0');
	}else{
		$('#extenpay_no').val('');	
		
	}
	
	  
  }


  function fun_extenopt_yes(deyb){
	  
	if($(deyb).is(':checked')){
		$('#extenopt_yes').val('1');
		$('#extenopt_no').val('');
	}else{
		$('#extenopt_yes').val('');	
		
	}
	  
  }
  
function fun_extenopt_no(essy){
	  
	if($(essy).is(':checked')){
		$('#extenopt_yes').val('');
		$('#extenopt_no').val('0');
	}else{
		$('#extenopt_no').val('');	
		
	}
	
	  
  }


  function fun_Accrued_Charges(desyb){
	  
	if($(desyb).is(':checked')){
		$('#ext_Accrued_Charges').val('1');
		//$('#extenrecived_no').val('');
	}else{
		$('#ext_Accrued_Charges').val('');	
		
	}
	  
  }

  function fun_extenrecived_yes(desyb){
	  
	if($(desyb).is(':checked')){
		$('#extenrecived_yes11').val('1');
		$('#extenrecived_no').val('');
	}else{
		$('#extenrecived_yes11').val('');	
		
	}
	  
  }
  function fun_extensubmit_yes(desyb){
	  
	if($(desyb).is(':checked')){
		$('#extenrecived_yes12').val('1');
		$('#extenrecived_no').val('');
	}else{
		$('#extenrecived_yes12').val('');	
		
	}
	  
  }
  function fun_notify_loanext(desyb){
	if($(desyb).is(':checked')){
		$('#notify_loanext').val('1');
	}else{
		$('#notify_loanext').val('');	
	}
  }
  
function fun_extenrecived_no(esasy){
	  
	if($(esasy).is(':checked')){
		$('#extenrecived_yes').val('');
		$('#extenrecived_no').val('0');
	}else{
		$('#extenrecived_no').val('');	
		
	}
	
	  
  }
  
  
  
  function fun_extenagre_yes(ddesyb){
	  
	if($(ddesyb).is(':checked')){
		$('#extenagre_yes').val('1');
		$('#extenagre_no').val('');
	}else{
		$('#extenagre_yes').val('');	
		
	}
	  
  }
  
function fun_extenagre_no(esadsy){
	  
	if($(esadsy).is(':checked')){
		$('#extenagre_yes').val('');
		$('#extenagre_no').val('0');
	}else{
		$('#extenagre_no').val('');	
		
	}
	
	  
  }


  function hideextcheckboxes(that){

  	if(that == 1){

  		$('#extenpay_yes').val('').attr('disabled',true);
  		$('#extenrecived_yes11').val('').attr('disabled',true);
  		$('#ext_Accrued_Charges').attr('disabled',false);

  	}else if(that == 2){
  		$('#ext_Accrued_Charges').val('').attr('disabled',true);
  		$('#extenpay_yes').attr('disabled',false);
  		$('#extenrecived_yes11').attr('disabled',false);
  	}else{

  		$('#ext_Accrued_Charges').attr('disabled',false);	
  		$('#extenpay_yes').attr('disabled',false);	
  		$('#extenrecived_yes11').attr('disabled',false);	
  	}

  }
  
  
  function fun_exten_off_yes(ddedsyb){
	  
	if($(ddedsyb).is(':checked')){
		$('#exten_off_yes').val('1');
		$('#exten_off_no').val('');
	}else{
		$('#exten_off_yes').val('');	
		
	}
	  
  }

  function fun_ext_req_borrower(ddedsyb){
	  
	if($(ddedsyb).is(':checked')){
		$('#ext_req_borrower').val('1');
	}else{
		$('#ext_req_borrower').val('');	
	}  
  }

  function fun_ext_app_rec_lender(ddedsyb){
	  
	if($(ddedsyb).is(':checked')){
		$('#ext_app_rec_lender').val('1');
	}else{
		$('#ext_app_rec_lender').val('');	
	}  
  }
  
function fun_exten_off_no(ensadsy){
	  
	if($(ensadsy).is(':checked')){
		$('#exten_off_yes').val('');
		$('#exten_off_no').val('0');
	}else{
		$('#exten_off_no').val('');	
		
	}
	
	  
  }
 

 
 


//...........................................................saved document append code end...........................................................//

function delete_affil_part(th){

   var del_id=$(th).attr('id');

var abc = '.del_'+del_id;
jQuery(abc).remove();



}

function second_delete_payment_gra_data(th){

   var del_id=$(th).attr('id');

var abcc = '.dell_'+del_id;
jQuery(abcc).remove();



}

function saved_document_add_more_row(hj){

	// i++;
	 
	//if(i < 6){
		 
	  $( ".doc-div" ).append(' <br><div class="row"><div class="col-md-4"><input type="file" id = "upload" name ="upload_docc[]" class="select_images_file" onchange="change_this_image(this)"></div></div><br> ');
	// }else{
		 // $('#add_row').css("display","none");
		 // alert('no mpre row added');
	 // }
	  
}


	
	function nsecurity_type(yum){


     var id= $(yum).parents('tr').find('input[name="this_idd[]"]').val();
     
     var closing_date='<?php echo date("m-d-Y",strtotime($c_date));?>';

	 if($(yum).val()=='1'){

      $('#start_date_val_'+id).val(closing_date);

	 }else{

	 		
	 	$('#start_date_val_'+id).val('');
	 }
	
	}

function upload_del(that){

	var name = that.id;

	var path = $(that).parent().parent().parent().parent().find('input#hydra').val();
	


	if(confirm('Are You Sure To Delete?')){
		$.ajax({
			
			type	:'POST',
			url		:'<?php echo base_url()."Print_document/delete_documentt";?>',
			data	:{'name':name,'path':path},
			success : function(response){
				if(response == 1){
					$(that).parent().parent('li').remove();
				}
			}
				
		});
	}
}  



function upload_document_del(that){

	var name = that.id;

	var path = $('input#hydra_2').val();
	


	if(confirm('Are You Sure To Delete?')){
		$.ajax({
			
			type	:'POST',
			url		:'<?php echo base_url()."Print_document/saved_delete_document";?>',
			data	:{'name':name,'path':path},
			success : function(response){
				if(response == 1){
					$('table tr[attrremove="'+name+'"]').remove();	
				}
			}
				
		});
	}
}  


function add_docu(that){
	
  $( ".docc_divv").append('<br><div class="row"><div class="col-md-12"><div class="col-md-6"><input type="file" name="lender_doc[]" class="form-control"></div><div class="col-md-6"></div></div></div>');
	

 }

  function contact_add_n(that){

		//var title_id='<?php echo $title_first_name;?>'; 
		$.ajax({
			type	:'POST',
			url		:'<?php echo base_url()."Load_data/fetch_contact_notee";?>',
			data	:{},
			success : function(response){
 
		 $('#contact_not_name').html('');
		 $('#contact_not_name').html(response);
		 $('.chosen43').chosen();
 			}
				
		});

 } 



//title contact ajax//


      function title_data(that){

		var title_id='<?php echo $title_first_name;?>'; 
		$.ajax({
			type	:'POST',
			url		:'<?php echo base_url()."Load_data/fetch_contac_title";?>',
			data	:{'title_id':title_id},
			success : function(response){
 
		$('#title_c_name').html('');
		$('#title_c_name').html(response);
		 $('.chosen1').chosen();
		
			}
				
		});
  }  

	 


	//escrow contact ajax//
    function escrow_data(that){

		var escrow_id='<?php echo $e_first_name;?>'; 
		
		$.ajax({
			
			type	:'POST',
			url		:'<?php echo base_url()."Load_data/escrow_fetch_contac";?>',
			data	:{'escrow_id':escrow_id},
			success : function(response){
 
			$('#escrow_c').html('');
			$('#escrow_c').html(response);
			$('.chosen2').chosen();

			}
				
		});
  	} 


//afflilation contact ajax//

 function afflilation_data(that){

 $('#my_main1 #my_main').each(function(){	
   
     var idsss=$(this).attr('class');
   
     var idss=$('.'+idsss+' input[name="aff_i[]"]').val();

  $.ajax({
			
			type	:'POST',
			url		:'<?php echo base_url()."Load_data/ajax_afflilation_data";?>',
			data	:{'aff_id':idss},
			success : function(response){

			$('.'+idsss+' #affiliation_part').html('');
			$('.'+idsss+' #affiliation_part').html(response);
			$('.'+idsss+' .chosen7').chosen();

			}
				
		});
	});

 }   

     //payment gaurantor ajax//

   function payment_data(that){
  
   
	$('#my_main2 #my_main3').each(function(){	
   
     var ids=$(this).attr('class');
   
     var idss=$('.'+ids+' input[name="c_names[]"]').val();

  $.ajax({
			
			type	:'POST',
			url		:'<?php echo base_url()."Load_data/ajax_payment_data";?>',
			data	:{'pay_id':idss},
			success : function(response){

			$('.'+ids+' #payment_ajax').html('');
			$('.'+ids+' #payment_ajax').html(response);
			$('.'+ids+' .chosen9').chosen();

			}
				
		});
	});

 }   
 </script>
 <script type="text/javascript">
 	function loan_lender_payment_model(loan_id,talimar_loan,lender_id){
 		$.ajax({
	        type    : 'POST',
	        url     : '/LoanPortfolioPopup/loanLenderPayment',
	        data    : { 'loan_id':loan_id,'talimar_loan':talimar_loan,'lender_id':lender_id },
	        success : function(response)
	        {
	            $('#loan_lender_payment_model').html(response);
	        	$('#loan_lender_payment').modal('show');
	        	datepicker();
	        }
	    });
 	}
 </script>
	
<input type="hidden" name="loan_portfolio_id" id="loan_portfolio_id" value="<?php echo $loan_portfolio_id; ?>">
<script type="text/javascript" src="<?php echo base_url("assets/js/load_data/loan_portfolio_popup/modal-event.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/load_data/loan_portfolio_popup/loan_term.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/load_data/loan_portfolio_popup/common_functions.js"); ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/load_data/sidebar.css"); ?>">

<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/load_data/loan-sidebar.css"); ?>">

<div id="ct_modal_loan_terms"></div>

<div id="modal_loan_notes"></div>
<div id="modal_diligence_materials"></div>

<div id="modal_payment_gurrantor"></div>
<div id="modal_closing_statement_new11"></div>
<div id="modal_impound_accounts"></div>
<div id="modal_escrow"></div>
<div id="modal_title"></div>
<div id="modal_construcion_type_sources_and_uses"></div>
<div id="modal_recording_information"></div>
<div id="modal_assigment_main"></div>
<div id="modal_modal_loan_reserve"></div>
<div id="modal_print_loan_document"></div>
<div id="modal_save_documents"></div>
<div id="modal_wholesale_deal"></div>
<div id="modal_condition_page"></div>
<input type="hidden" id="ct_first_monyly_payment" value="<?php echo $first_monyly_payment; ?>">
<div id="modal_FCI_Documents"></div>


<?php /* new */ ?>
<input type="hidden" name="listmarket" id="listmarket" value="<?php echo $loan_servicing['list_market'];?>">
<input type="hidden" name="demand_requested" id="demand_requested" value="<?php echo $loan_servicing['demand_requested'];?>">
<input type="hidden" name="cpt_interest_reserve" id="cpt_interest_reserve" value="<?php echo $fetch_loan_result[0]->interest_reserve ?>">
<input type="hidden" name="cpt_payment_reserve_opt" id="cpt_payment_reserve_opt" value="<?php echo $loan_servicing['payment_reserve_opt'];?>">
<div id="modal_loan_servicing"></div>


<?php
$popup_modal_cv = $this->session->flashdata('popup_modal_cv');
if($popup_modal_cv){
?>
<script type="text/javascript">
	$(document).ready(function(){
		var  popup_modal_hit = '<?php echo implode(',', $popup_modal_cv); ?>';
		var array = popup_modal_hit.split(",");
		var vix = 0;
		$.each(array,function(i){
			var popup_modal = array[i];
			if(vix == 0){
				setTimeout(function(){
					$('a[href="#'+popup_modal+'"]').trigger( "click" );
				}, 500);
			}else{
				setTimeout(function(){
					$("#"+popup_modal+"").modal('show');
				}, 7000);
			}
			vix++;
		});		
	});
</script>
<?php
}
?>