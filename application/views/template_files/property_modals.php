<style>
table#property-home-table tr td:last-child {
    text-align: left !important;
}
.ecumbrance-edit-notes {
	background: #e5e5e5;
}
table.table_ecumbrances_css tbody td {
    background: white !important;
}
table.table_ecumbrances_css thead th {
    background: #e5e5e5;
}
div.modal-property-insurance h4 {
    padding: 9px 5px 0px 5px !important;
}

.form-control[readonly] {
    background-color: #e9e9e9 !important;
}

#purchase_pricessss[readonly] {

	 background-color: #e9e9e9 !important;
}

.disabled{

pointer-events:none;
cursor:default;
text-decoration:none;
color:gray;

}
.dis{

pointer-events:auto;
cursor:default;
text-decoration:none;
color:#5b9bd1;

}
tr.s input[type="text"]{


border: none;
cursor:not-allowed;
pointer-events:none;
}

/*div#insert_add_property {
    z-index: 99999 ;
}
div#edit_property {
 z-index: 99999 ;
}*/
/*div#property_insurence_upload_doc {
	 z-index: 99999 ;
}*/
.col-md-4.aaawsd {
	margin-top:32px;
}

table#table-property-home-page tbody tr td:nth-child(2) {
    width: auto !important;
    text-align: center;
    vertical-align: top;
    font-weight: 500;
}

table#table-property-home-page td a {
    padding: 0px 0px;
}
</style>
<?php
$loan_intrest_type_option 	= $this->config->item('loan_intrest_type_option');
$underwriting_option 		= $this->config->item('underwriting_option');
$current_value_option 		= $this->config->item('current_value_option');
$occupancy_value_option 	= $this->config->item('occupancy_value_option');
$zoning_value_option 		= $this->config->item('zoning_value_option');
$yes_no_option3 			= $this->config->item('yes_no_option3');
$yes_no_option4 			= $this->config->item('yes_no_option4');
$valuation_based_upon 		= $this->config->item('valuation_based_upon');
$valuation_com              = $this->config->item('valuation_com');
$propp_type              	= $this->config->item('propp_type');
$property_newval_options    = $this->config->item('property_newval_options');
$property_Valuation_Status  = $this->config->item('property_Valuation_Status');
$cash_flow_option    		= $this->config->item('cash_flow_option');
$cashflow_edit_options    	= $this->config->item('cashflow_edit_options');

?>


<!-- Start property data homepage modal -->
<div class="modal fade bs-modal-lg" id="property-data-home-page" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<!--<h4 class="modal-title">Property Data : #<?php //echo $modal_talimar_loan; ?></h4>-->
				<h4 class="modal-title">Property Data : <?php echo $property_addresss; ?></h4>
			</div>
				<div class="modal-body">
				
					<!--<label>Multiple Properties</label>
					<?php 	 
						/*$check='';
						$class="disabled";
						foreach ($fetch_property_home as  $value) {
							
						
						if($value->multi_properties == '1' ){

						$check='checked';	

					    $class="dis";

						}
					}*/
				    ?>
					<input type="checkbox"  onclick="multiple_checkbox(this)" value="1" name="check" id="checkbox" <?php echo $check;?>>-->
					<!----------- Hidden fields  ----------->
					

					<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
					<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">

					<!------New layout start --------------->
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" id="table-property-home-page">
							<thead>
								<tr>
									<th>Add Address</th>
									<th>Value</th>
									<th>Senior Debt</th>
									<th>TaliMar Loan</th>
									<th>Total Debt</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>

								<?php
								$blankproperty 		= 0;
								$total_uw_value 	= 0; 
								$total_sl_value 	= 0;
								$sum_secondary_uw 	= 0;
								$sum_secondary_sl 	= 0;
								if(isset($fetch_property_home))
								{
									foreach($fetch_property_home as $key => $row)
									{ 

										if($row->primary_property == 1){
											$primary_uw_value = $row->future_value;
											$primary_senior_liens_value = $row->senior_liens ? $row->senior_liens : 0;
										}
											
										if($row->primary_property == 2){
											
											$secondary_uw_value[] = $row->future_value;
											$secondary_senior_liens_value[] = $row->senior_liens ? $row->senior_liens : 0;
											$sum_secondary_uw += $row->future_value;
											$sum_secondary_sl += $row->senior_liens;
										}
									
										$total_uw_value = $primary_uw_value + $sum_secondary_uw;
										$total_sl_value = $primary_senior_liens_value + $sum_secondary_sl;


										if($ecum_right_data[$row->id]['existing_priority_to_talimar'] == '1'){

											$total_of_senior_lien = $total_of_senior_lien + $ecum_right_data[$row->id]['sum_current_balance'];
										}
										
									?>

										<tr>
											<td style="text-align: left;">
												<?php if(isset($row->property_address)){ ?>
													
													<a href="#property-<?php echo $row->id; ?>" onclick="ajax_property_data(this,'<?php echo $row->id; ?>')" data-toggle="modal"><?php echo  $row->property_address .' '.$property_of_home_id[$row->id]->unit.'<br>'.$property_of_home_id[$row->id]->city.', '.$property_of_home_id[$row->id]->state.' '.$property_of_home_id[$row->id]->zip; ?>
														<input type="hidden" name="property_address[]" value="<?php echo $row->property_address; ?>">
													</a>
													
										  		<?php }else{ $blankproperty++; ?>

										  				<?php /*
				                                       <a href="#property-<?php echo $row->id; ?>" id="hiddeextrapp" data-toggle="modal" class="">Add Property</a> 
				                                        */ ?>
				                             	<?php break; } ?>
											</td>
											<td><?php echo '$'.number_format($row->future_value);?></td>
											<td><?php echo $ecum_right_data ? '$'.number_format($ecum_right_data[$row->id]['sum_current_balance']) : '$0.00';?></td>
											<td>-</td>
											<td><?php echo '$'.number_format($ecum_right_data[$row->id]['sum_current_balance'] + $fetch_loan_result[0]->loan_amount);?></td>
											<td>
												<?php //if($row->primary_property ==  2){ ?>
													<form method="POST" action="<?php echo base_url().'delete_property_home_id'?>" id="remove_property_home_page-<?php echo $row->id; ?>" style="float: right;margin-top: 6px;">
															<input type="hidden" name="id" value="<?php echo $row->id; ?>">
															<input type="hidden" id="p_p" name="p_p" value="<?php echo $row->primary_property; ?>">
															<input type="hidden" name="loan_id" value="<?php echo $this->uri->segment(2); ?>">
														</form>
														<button type="button" class="btn btn-primary delete_property_content" property_id="<?php echo $row->id; ?>">
															<i class="fa fa-trash"></i>
														</button>
												<?php //} ?>
												
											</td>
											
										</tr>

								<?php } } ?>

								
							</tbody>
							<tfoot>
								<tr>
									<th>Total:</th>
									<th><?php echo '$'.number_format($total_uw_value);?></th>
									<th><?php echo '$'.number_format($total_of_senior_lien);?></th>
									<th><?php echo '$'.number_format($fetch_loan_result[0]->loan_amount);?></th>
									<th><?php echo '$'.number_format($fetch_loan_result[0]->loan_amount + $total_of_senior_lien);?></th>
									<th></th>
								</tr>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th style="text-align: right;">CLTV:</th>
									<th><?php echo number_format((($fetch_loan_result[0]->loan_amount + $total_of_senior_lien)/$total_uw_value)*100,2).'%'; ?></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
					
					
					<!------New layout end ----------------->
					
					
				</div>
				<div class="modal-footer">
					<a href="#property-<?php echo $row->id; ?>" id="hiddeextrapp" data-id="<?php echo $row->id; ?>" data-toggle="modal" class="btn btn-primary">Add Property</a>
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
					
					<?php// if($blankproperty > 0){  }else{ ?>
						<!-- <button type="button" onclick="addmoreproperty(this);" class="btn btn-primary">Add Property</button> -->
					<?php// } ?>
					
				</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!----------  END OF PROPERY HOME PAGE ---------------->



<!--------------------------Modal Property --------------------------->

	<?php 	
	
	// echo '<pre>';
	// print_r($loan_property_result);
	// echo '</pre>';
	$property_modal_property_type 		= $this->config->item('property_modal_property_type'); 
	$property_modal_property_condition 	= $this->config->item('property_modal_property_condition'); 
	$property_modal_valuation_type 		= $this->config->item('property_modal_valuation_type');
	$valuation_type 					= $this->config->item('valuation_type');
	$relationship_type 					= $this->config->item('relationship_type');
	$property_modal_re851d_data 		= $this->config->item('property_modal_re851d_data');
	
	if(isset($fetch_property_home))
	{
		foreach($fetch_property_home as $property_home)
		{
			?>
			<div class="modal fade bs-modal-lg formoreprop" id="property-<?php echo $property_home->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
						<?php
						
					
						if(isset($loan_property_result[$property_home->id]))
						{
							foreach($loan_property_result[$property_home->id] as $row)
							{
							

							// echo '<pre>';
							// print_r($row);
							// echo '</pre>';

								$property_id					= $row->id; 
								$property_property_home_id 		= $row->property_home_id;
								$property_talimar_loan 			= $row->talimar_loan;
								$property_property_address 		= $row->property_address;
								$property_unit 					= $row->unit;
								$property_city 					= $row->city;
								$property_state 				= $row->state;
								$property_zip 					= $row->zip;
								$property_country 				= $row->country;
								$property_yr_built 				= $row->yr_built;
								$property_apn 					= $row->apn;
								$property_taxzz 					= $row->property_tax;
								$property_property_type 		= $row->property_type;
								$property_square_feet 			= $row->square_feet;
								$property_bedrooms 				= $row->bedrooms;
								$property_bathrooms 			= $row->bathrooms;
								$property_lot_size 				= $row->lot_size;
								$property_garage 				= $row->garage;
								$property_pool 					= $row->pool;
								$property_conditions 			= $row->conditions;
								$property_gross_monthly_income 	= $row->gross_monthly_income;
								$property_gross_monthly_expense = $row->gross_monthly_expense;
								$property_net_income 			= $row->net_income;
								$property_purchase_price 		= $row->purchase_price;
								$property_current_option 		= $row->current_option;
								$property_current_value 		= $row->current_value ? $row->current_value : $property_home->current_value;
								$property_underwriting_option 	= $row->underwriting_option;
								$property_underwriting_value 	= $row->underwriting_value ? $row->underwriting_value : $property_home->future_value;
								$property_renovation_cost 		= $row->renovation_cost;
								$property_arv 					= $row->arv; 
								$property_annual_property_taxes = $row->annual_property_taxes;
								$property_property_taxes_current = $row->property_taxes_current;
								$property_yes_p_tax_cur 		= $row->yes_p_tax_cur;
								$property_insurance_carrier 	= $row->insurance_carrier;
								$property_insurance_expiration 	= $row->insurance_expiration;
								$property_annual_insurance_payment = $row->annual_insurance_payment;
								$property_valuation_type 			= $row->valuation_type ? $row->valuation_type : '';
								$valuation_based					= $row->valuation_based ? $row->valuation_based : '';
								$property_relationship_type 		= $row->relationship_type ? $row->relationship_type : '';
								$valuation_status 				= $row->valuation_status ? $row->valuation_status : '';
								$property_valuation_date 		= $row->valuation_date ? date('m-d-Y',strtotime($row->valuation_date) ): '';
								$ex_com_date 					= $row->ex_com_date ? date('m-d-Y',strtotime($row->ex_com_date) ): '';
								
								$property_valuation_vendor 		= $row->valuation_vendor  ? $row->valuation_vendor  : '';
								$property_appraiser_name 		= $row->appraiser_name;
								//$property_appraiser_address 	= $row->appraiser_address;
								$appraiser_s_address			= $row->appraiser_s_address;
								$appraiser_unit					= $row->appraiser_unit;
								$appraiser_state				= $row->appraiser_state;
								$appraiser_city					= $row->appraiser_city;
								$appraiser_zip					= $row->appraiser_zip;
								$property_apraisal_date 		= $row->apraisal_date;
								$property_of_unit 				= $row->of_unit;
								$property_hoa 					= $row->hoa;
								$property_hoa_stand 			= $row->hoa_stand;
								$property_hoa_dues 				= $row->hoa_dues;
								$property_sales_refinance_value = $row->sales_refinance_value;
								$property_construction_type 	= $row->construction_type;
								$property_desc_property_impro 	= $row->desc_property_impro;
								$property_desc_property_p_impro = $row->desc_property_p_impro;
								$property_notes 				= $row->property_notes;
								$property_legal_description 	= $row->legal_description;
								$property_t_user_id 			= $row->t_user_id;
								$property_occupancy_type 		= $row->occupancy_type;
								$property_Cash_Flow 			= $row->Cash_Flow;
								$property_zoning 				= $row->zoning;
								$re851d_data 					= $row->re851d_data;
								$construction_budget 			= $row->construction_budget;
								$rehab_property 				= $row->rehab_property;
								$p_phone						= $row->p_phone;
								$p_email						= $row->p_email;
								$contractor_party				= $row->contractor_party;
								$contractor_name				= $row->contractor_name;
								$contractor_license				= $row->contractor_license;
								$purchase_price_text			= $row->purchase_price_text;
								$valuation_company			    = $row->valuation_company;

								$as_value_option			     = $row->as_value_option;
								$as_value_amount			     = $row->as_value_amount;
								$completion_option			     = $row->completion_option;
								$completion_amount			     = $row->completion_amount;
								$property_link			         = $row->property_link;
								
								//$current_market_value 			= $row->current_market_value;
								// echo'<pre>';
								// print_r($current_market_value);
								//print_r($property_underwriting_value);
								// echo'</pre>';
							}
						}
						else
						{
							$property_property_home_id 		= '';
							$property_talimar_loan 			= '';
							$property_property_address 		= '';
							$property_unit 					= '';
							$property_city 					= '';
							$property_state 				= '';
							$property_zip 					= '';
							$property_notes					= '';
							$property_country 				= '';
							$property_yr_built 				= '';
							$property_apn 					= '';
							$property_taxzz 				= '';
							$property_property_type 		= '';
							$property_square_feet 			= '';
							$property_bedrooms 				= '';
							$property_bathrooms 			= '';
							$property_lot_size 				= '';
							$property_garage 				= '';
							$property_pool 					= '';
							$property_conditions 			= '';
							$property_gross_monthly_income 	= '';
							$property_gross_monthly_expense = '';
							$property_net_income 			= '';
							$property_purchase_price 		= '';
							$property_current_value 		= $property_home->current_value;
							$property_renovation_cost 		= '';
							$property_arv 					= '';
							$property_annual_property_taxes 	= '';
							$property_property_taxes_current 	= '';
							$property_yes_p_tax_cur 			= '';
							$property_insurance_carrier 		= '';
							$property_insurance_expiration 		= '';
							$property_annual_insurance_payment 	= '';
							$property_valuation_type 			= '';
							$valuation_based					= '';
							$property_relationship_type 		= '';
							$valuation_status 					= '';
							$property_valuation_date 			= '';
							$ex_com_date 						= '';
							$property_valuation_vendor 			= '';
							$property_appraiser_name 			= '';
							//$property_appraiser_address 		= '';
							$appraiser_s_address				= '';
							$appraiser_unit						= '';
							$appraiser_state					= '';
							$appraiser_city						= '';
							$appraiser_zip						= '';
							$property_apraisal_date 			= '';
							$property_of_unit 					= '';
							$property_hoa 						= '';
							$property_hoa_stand 				= '';
							$property_hoa_dues 					= '';
							$property_sales_refinance_value 	= '';
							$property_construction_type 		= '';
							$property_desc_property_impro 		= '';
							$property_desc_property_p_impro 	= '';
							$property_t_user_id 				= '';
							$legal_description 					= '';
							$property_underwriting_option 		= '';
							$property_underwriting_value 		= $property_home->future_value;
							$property_current_option 			= '';
							//$current_market_value 			= '';
							$property_occupancy_type 			= '';
							$property_Cash_Flow 				= '';
							$property_zoning 					= '';
							$re851d_data 						= '';
							$construction_budget				= '';
							$rehab_property						= '';
							$p_phone							= '';
							$p_email							= '';
							$contractor_party					= '';
							$contractor_name					= '';
							$contractor_license					= '';
							$purchase_price_text				= '';
							$valuation_company			     	= '';

							$as_value_option			     	= '';
							$as_value_amount			     	= '';
							$completion_option			     	= '';
							$completion_amount			     	= '';
							$property_link			     		= '';
						}
						
					
						
						?>	
				
						<form method="POST" action = "<?php echo base_url();?>add_property" name="form_i_add_property" id="form_i_add_property">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<!--<h4 class="modal-title">Property Data : #<?php //echo ''.$modal_talimar_loan; ?> </h4>-->
									<h4 class="modal-title">Property Data: <?php echo $property_property_address; ?> </h4>
								</div>
								<div class="modal-body">
								<div class="portlet-body flip-scroll">
									<!-----------Hidden fields----------->
									
										<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
										
										<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
										<input type="hidden" name="property_home_id" value="<?php echo $property_home->id; ?>">
										<!-----------End of hidden Fields---->
										
										
										<!---------Content modal------------------->
									<div class="row">
									<div class="col-sm-9 property-left-content-data">
										<div class="row property_loan_row">
											<h4 class="property-edit-h4">Loan Stats:</h4>
											
										</div>
										<div class="row property_loan_row">
											<div class="col-md-4">
												<label>Position:</label>
											</div>
											<div class="col-md-3">
												<?php 
												if($property_home->primary_property == 1){ ?>
													<?php  echo $position_option[$fetch_loan_result[0]->position] ;?>
													<input type = "hidden" value = "<?php  echo $fetch_loan_result[0]->position ;?>" name = "property_position"/>	
												
												<?php } else{ ?>
												<select id="selectbasic" name="property_position" class="form-control load_data_inputbox">
													<option value="" ></option>
													  <?php foreach($position_option as $key => $optn) { ?>
													  <option value="<?php echo $key;?>" <?php if($key == $property_home->position) { echo 'selected';} ?> ><?php echo $optn;?></option>
													<?php } ?>
													<option value = "5" <?php if($property_home->position == 5) { echo 'selected';} ?>>5th</option>
												</select>
												<?php }
												?>
											</div>

											<div class="col-md-3">
												<label>Loan to Value:</label>
											</div>
											<div class="col-md-2">
												<?php 
													$primary_uw_value=$property_home->future_value;

													if($ecum_right_data[$property_home->id]['existing_priority_to_talimar'] == '1'){

														$addseniorLinewecum = $ecum_right_data[$property_home->id]['sum_current_balance'];
													}else{
														$addseniorLinewecum = 0;
													}

												 //$loan_valuee=($ecum_right_data[$property_home->id]['sum_current_balance']+$fetch_loan_result[0]->loan_amount)/$property_underwriting_value*100;
													$loan_valuee = ($fetch_loan_result[0]->loan_amount + $addseniorLinewecum)/$property_underwriting_value * 100;

												echo number_format(is_infinite($loan_valuee) ? 0.0:$loan_valuee,2);?>%
											</div>
											
											
										</div>
										
										<div class="row property_loan_row">
											<div class="col-md-4">
												<label>Priority Debt:</label>
											</div>
											<div class="col-md-3">
												<?php //echo '$'.number_format($property_senior_liens[$property_home->id],2);

													/*echo '<pre>';
													print_r($ecum_right_data[$property_home->id]);
													echo '</pre>';*/

													if($ecum_right_data[$property_home->id]['existing_priority_to_talimar'] == '1'){

														$senior_liensVAL = $ecum_right_data[$property_home->id]['sum_current_balance'];
														$SubordinateLINES = 0;
													}else{
														$senior_liensVAL = 0;
														$SubordinateLINES = $ecum_right_data[$property_home->id]['sum_current_balance'];
													}

												?>
												
												<?php echo $ecum_right_data ? '$'.number_format($senior_liensVAL) : '$0.00'; ?>
											</div>

																						
										</div>
										<div class="row property_loan_row">
											<div class="col-md-4">
												<label>Subordinate Debt:</label>
											</div>
											<div class="col-md-3">
												<?php echo '$'. number_format($SubordinateLINES); ?>
											</div>

										</div>
										<div class="row property_loan_row">

											<div class="col-md-4">
												<label>Total Debt:</label>
											</div>
											<div class="col-md-3">
												
												<?php echo '$'. number_format($senior_liensVAL + $SubordinateLINES + $fetch_loan_result[0]->loan_amount); ?>
											</div>
										</div>
										
										
										<div class="row property_loan_row">
											<h4 class="property-edit-h4">Underwriting Values: </h4>
											<div class="row">&nbsp;</div>
											<div class="col-md-9">
												<label>Purchase Price:</label>
											</div>
											
										

											<div class="col-md-3">
										<?php if($fetch_loan_result[0]->transaction_type == '2' || $fetch_loan_result[0]->transaction_type =='3')
											{ ?>
                                              
												<input type="text" id="purchase_pricessss" class="form-control number_only" name="purchase_price" value="$0" readonly>
											
												<?php }else{ ?>
												<input type="text" id="purchase_price" class="form-control number_only" name="purchase_price" value="<?php echo  '$'.number_format($property_purchase_price);?>" onchange="amount_format_change(this)" />
												
												
												<?php } ?>
											</div>
											
										</div>	
										<!--<div class="row property_loan_row">
											<div class="col-md-4">
												<label>Current Value:</label>
												
											</div>
											<div class="col-md-5">
												<select class="form-control" name="current_option">
													<?php
													foreach($current_value_option as $key => $option)
													{
														$selected = '';
														if($key == $property_current_option)
														{
															$selected = 'selected';
														}
														?>
														<option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $option; ?></option>
														<?php
													}
													?>
												</select>
												
												<input type="text" name="purchase_price_text" class="form-control" value="<?php echo $purchase_price_text ; ?>">
											</div>
											<div class="col-md-3">
												<input type="text" id="current_value" class="amount_format_without_decimal form-control number_only" name = "current_value" value="<?php echo '$'.number_format($property_current_value);?>" onchange="amount_format_change(this)" />
											</div>
										</div>-->

										<div class="row property_loan_row">
											<div class="col-md-4">
												<label>Underwriting Value:</label>
												<!--<label>Property Value:</label>-->
											</div>
											<!--<div class="col-md-5">
												<select class="form-control" name="underwriting_option">
													<?php
													foreach($underwriting_option as $key => $option)
													{
														$selected = '';
														if($key == $property_underwriting_option)
														{
															$selected = 'selected';
														}
														?>
														<option value="<?php echo $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
														<?php
													}
													
													?>
												</select>
											</div>-->
											<div class="col-md-3 col-sm-offset-5">
												<input class="form-control number_only amount_format_without_decimal" name="underwriting_value" value="<?php echo '$'.number_format($property_underwriting_value);?>">
											</div>
											<?php 
											if(!$property_arv){
												$property_arv = 0;
											}
											?>
											<div class="col-md-4">
												<input id="hidden_loan_amount" type="hidden" name="hidden_loan_amount" value="<?php echo $fetch_loan_result[0]->loan_amount;?>">
												<input type="text" id="finished_arv" class="form-control number_only" name = "arv" value="<?php echo '$'.number_format($property_arv); ?>" onchange="amount_format_change(this)" />
											</div>
												
										</div>
											

										<!--<div class="row property_loan_row">
											<div class="col-md-4">
												<label>Current Market Value:</label>
											</div>
											<div class="col-md-4">
												<input class="form-control number_only amount_format_without_decimal" name="current_market_value" value="<?php echo '$'.number_format($current_market_value); ?>">
											</div>
										</div>-->


										<!--<div class="row property_loan_row">
											<div class="col-md-4">
												
												<label>Valuation Type:</label>
											</div>
											<div class="col-md-5">
												
											</div>
											<div class="col-md-3">
												<select class="form-control" onchange="appraiser_disable(this)" name="valuation_type">
													<?php
													foreach($valuation_type as $key => $option)
													{
														$selected = '';
														if($key == $property_valuation_type)
														{
															$selected = 'selected';
														}
														?>
														<option value="<?php echo $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
														<?php
													}													
													?>
												</select>
											</div>
											
										</div>-->

										<div class="row property_loan_row">
											<div class="col-md-4">
												
												<label>Valuation Based Upon:</label>
											</div>
											<div class="col-md-5">
												
											</div>
											<div class="col-md-3">
												<select class="form-control" name="valuation_based">
													<?php
													foreach($valuation_based_upon as $key => $option)
													{
														?>
														<option value="<?php echo $key; ?>" <?php if($key == $valuation_based){echo 'selected';}?>><?php echo $option; ?></option>
														<?php
													}													
													?>
												</select>
											</div>
											
										</div>

									<!--	<div class="row property_loan_row">
											<div class="col-md-4">
												<label>As Is Value:</label>
											</div>
											<div class="col-md-5">
												<select class="form-control" name="as_value_option">
													<?php foreach($property_newval_options as $key => $row){?>
														<option value="<?php echo $key;?>" <?php if($as_value_option == $key){echo 'selected';}?>><?php echo $row;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-md-3">
												<input class="form-control number_only amount_format_without_decimal" name="as_value_amount" value="<?php echo '$'.number_format($as_value_amount);?>">
											</div>
										</div>


										<div class="row property_loan_row">
											<div class="col-md-4">
												<label>Completion Value:</label>
											</div>
											<div class="col-md-5">
												<select class="form-control" name="completion_option">
													<?php foreach($property_newval_options as $key => $row){?>
														<option value="<?php echo $key;?>" <?php if($completion_option == $key){echo 'selected';}?>><?php echo $row;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-md-3">
												<input class="form-control number_only amount_format_without_decimal" name="completion_amount" value="<?php echo '$'.number_format($completion_amount);?>">
											</div>
										</div>
-->
										
										
										
										<div class="row property_loan_row">
											<div class="col-md-9">
											<label>Payoff Value:</label>
											</div>
											<div class="col-md-3">
											
											<input type="text" class="form-control number_only" name = "sales_refinance_value" value="<?php echo $property_sales_refinance_value ? '$'.number_format($property_sales_refinance_value) : '';?>" onchange="amount_format_change(this)" />
											</div>
													
										</div>
										
										<div class="row property_loan_row" >
										 <h4 class="property-edit-h4">Property Details:</h4>
													<div class="col-md-8">
														<label>Street:</label>
														<input type="text" id="" class="form-control" name="property_address"  value = "<?php echo $property_property_address; ?>" required />
													</div>
													<div class="col-md-4">
													<label>Unit #:</label>
														<input type="text" class="form-control" name = "unit" value = "<?php echo $property_unit; ?>"  />
													</div>
													
													
										</div>
										
										<div class="row property_loan_row">
											<div class="col-md-4">
												<label>City:</label>
												<input type="text" class="form-control" name = "city" value = "<?php echo $property_city; ?>" required />
											</div>
											<div class="col-md-4">
												<label>State:</label>
												<select  class="form-control" name = "state" >
												<?php 
												foreach($STATE_USA as $key => $option)
												{
													?>
													<option value="<?php echo $key?>" <?php if($property_state == $key) { echo "selected"; } ?> ><?= $option?></option>
													<?php
												}
												?>
				
												</select>
											</div>
											
											<div class="col-md-4">
											<label>Zip:</label>
												<input type="text" class="form-control number_only" name = "zip" value = "<?php echo $property_zip; ?>" required />
											</div>
										</div>
										
										<div class="row property_loan_row">
													
											<div class="col-md-4">
												<label>County:</label>
												<select  class="form-control" name = "county" id="county_usa">
													<?php 
													asort($usa_city_county);
													foreach($usa_city_county as $key => $option )
													{
														?>
														<option value = '<?php echo $key;?>' <?php if($key == $property_country) { echo 'selected'; } ?>><?= $option;?></option>
														<?php
													}
													?>
													
												</select>
												<select  class="form-control" name = "country" id="county_naveda" style="display:none;">
													<?php 
													foreach($naveda_county as $key => $option )
													{
														?>
														<option value = '<?php echo $key;?>' <?php if($key == $property_country) { echo 'selected'; } ?>><?= $option;?></option>
														<?php
													}
													?>
												</select>
											</div>
											
											<div class="col-md-4">
												<label>APN:</label>
													<input type="text" class="form-control" name = "apn"  value = "<?php echo $property_apn;?>" />
											</div>
											<!--<div class="col-md-4">
												<label>Property Taxes:</label>
											
												<input type="text"  class="form-control number_only" name="property_tax" value="<?php// echo  '$'.number_format($property_taxzz);?>" onchange="amount_format_change(this)" />
											
													
											</div>-->
											
													
										</div>
										
										<div class="row property_loan_row property_type_dependent" >
													<div class="col-md-4">
														<label>Square Feet:</label>
														<input type="text"  class="form-control" name="square_feet" id="square_feet" value="<?php echo $property_square_feet;?>" />
													</div>
													<div class="col-md-4">
													<label># of Bedrooms:</label>
														<input type="text" class="form-control number_only" name = "bedrooms" value="<?php echo $property_bedrooms;?>" />
													</div>
													<div class="col-md-4">
													<label># of Bathrooms:</label>
														<input type="text" class="form-control number_only" name = "bathrooms" value="<?php echo $property_bathrooms;?>" />
													</div>
													
										</div>
										
										<div class="row property_loan_row">
											<div class="col-md-4">
												<label>Lot Size:</label>
												<input type="text" class="form-control number_only" name = "lot_size" value="<?php echo number_format($property_lot_size);?>" />
											</div>
											
											<div class="col-md-4">
												<label># of Units:</label>
												<input type="text"  class="form-control number_only" name="of_unit" value="<?php echo $property_of_unit; ?>" />
											</div>
											
											
										
											<div class="col-md-4">
												<label>YR Built:</label>
												<input id="yr_built" class="form-control " name="yr_built" value="<?php echo $property_yr_built; ?>" >
											</div>
										</div>
										
										<div class="row property_loan_row" id="edit-property-hoa-section-<?php echo $property_home->id; ?>">
											<div class="col-md-4">
												<label>HOA:</label>
												<select name="hoa" class="form-control" onchange="hoa_changes(this)">
													<?php 
													foreach($yes_no_option as $key => $option)
													{
														$selected = '';
														if($key == $property_hoa)
														{
															$selected = 'selected';
														}
													?>
														<option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $option; ?></option>
													<?php
													}
													?>
													
												</select>
											</div>
											<div class="col-md-4">
												<label>HOA Monthly Payment:</label>
												<input type="text"  class="form-control number_only amount_format" name="hoa_dues" value="<?php echo '$'.number_format($property_hoa_dues,2);?>" />
											</div>
											<div class="col-md-4">
												<label>HOA in Good Standing:</label>
												<select name="hoa_stand" class="form-control">
													<?php 
													$hoa_good_stand_option = $this->config->item('hoa_good_stand_option');
													foreach($hoa_good_stand_option as $key => $option)
													{
														$selected = '';
														if($key == $property_hoa_stand)
														{
															$selected = 'selected';
														}
													?>
														<option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $option; ?></option>
													<?php
													}
													?>
													
												</select>
											</div>
											
											<!-------  Start Script load  ------->
											<script>
											$(document).ready(function(){
												hoa_changes(<?php echo $property_home->id; ?>);
											});
											</script>
											<!-------  End Script load  ------->
										</div>
										
										
										
										
										<div class="row property_loan_row">
													<div class="col-md-4">
														<label># of Garage Spaces:</label>
														<input type = "text" class="form-control number_only " name="garage" value = "<?php echo ''.number_format($property_garage);?>"/>
														<!--<label>Garage:</label>
														<select  class="form-control" name = "garage">
															<?php  
															foreach($garage_option as $key => $option)
															{
																?>
																<option value="<?= $key;?>" <?php if($key == $property_garage ) { echo 'selected'; } ?> ><?= $option;?></option>
																<?php
															}
															?>
															
														</select>-->
													</div>
													<div class="col-md-4">
													<label>Pool:</label>
														<select  class="form-control" name = "pool">
															<?php  
															foreach($yes_no_option as $key => $option)
															{
																?>
																<option value="<?= $key;?>" <?php if($key == $property_pool ) { echo 'selected'; } ?> ><?= $option;?></option>
																<?php
															}
															?>
															
														</select>
													</div>
													<div class="col-md-4">
													<label>Condition:</label>
														<select  class="form-control" name = "conditions">
															<?php  
															foreach($property_modal_property_condition as $key => $option)
															{
																?>
																<option value="<?= $key;?>" <?php if($key == $property_conditions ) { echo 'selected'; } ?> ><?= $option;?></option>
																<?php
															}
															?>
															
														</select>
													</div>
													
										</div>
										
										
										<div class="row property_loan_row">
													<div class="col-md-4">
														
														<label>Occupancy Type:</label>
														<select  class="form-control" name = "occupancy_type">
															<?php  
															foreach($occupancy_value_option as $key => $option)
															{
																?>
																<option value="<?= $key;?>" <?php if($key == $property_occupancy_type ) { echo 'selected'; } ?> ><?= $option;?></option>
																<?php
															}
															?>
															
														</select>
													</div>
													<div class="col-md-4">
														
														<label>Cash Flow:</label>
														<select  class="form-control" name = "Cash_Flow">
															<?php  
															foreach($cash_flow_option as $key => $option)
															{
																?>
																<option value="<?= $key;?>" <?php if($key == $property_Cash_Flow ) { echo 'selected'; } ?>><?= $option;?></option>
																<?php
															}
															?>
															
														</select>
													</div>
													
													
													
										</div>
										<div class="row property_loan_row">
										
											<div class="col-md-4">
											<label>Property Type:</label>
												<select  class="form-control" name = "property_type" id="select_property_type">
													<?php
													foreach($property_modal_property_type as $key => $option)
													{
														?>
														<option value="<?= $key;?>" <?php if($key == $property_property_type){ echo 'selected'; } ?> ><?= $option;?></option>
														<?php
													}
													?>
													
												</select>
											</div>
											<div class="col-md-4">
													<label>Zoning:</label>
														<select  class="form-control" name = "zoning">
															<?php  
															foreach($zoning_value_option as $key => $option)
															{
																?>
																<option value="<?= $key;?>" <?php if($key == $property_zoning ) { echo 'selected'; } ?> ><?= $option;?></option>
																<?php
															}
															?>
															
														</select>
											</div>
											<div class="col-md-4">
												<label>RE851D Prop Data:</label>
												<select class="form-control" name="re851d_data">
												<?php
												foreach($property_modal_re851d_data as $key => $option){?>
													
													<option value="<?php echo $key;?>" 
													<?php if($key == $re851d_data){echo 'Selected';} ?>><?php echo $option;?></option>
													
											<?php	} ?>
												</select>
											</div>
										</div>
										
										<div class="row property_loan_row property-edit-border-bottom">
											<div class="col-md-4">
												<label>Annual Property Taxes:</label>										
												<input type="text"  class="form-control number_only" name="property_tax" value="<?php echo  '$'.number_format($property_taxzz);?>" onchange="amount_format_change(this)" />
											
											</div>
											<?php 	
										
													if(empty($property_taxzz) || ($property_taxzz=='NaN')){

														$cahcked='checked';
													}else{

														$cahcked='';
													}
															?>
											<div class="col-md-4 aaawsd">
																
												<input type="checkbox"  class="form-control" name="check_estimate" <?php echo $cahcked;?>> Check if estimate
											
											</div>
											
										</div>

										<div class="row property_loan_row">
											<div class="col-md-12">
												<label>Property Legal Description: </label>
												<textarea type="text"  class="form-control" name="legal_description"  ><?php echo  $property_legal_description ? $property_legal_description :'See Attached Legal Description'; ?></textarea>
											</div>	
										</div>
										
										<div class="row property_loan_row">
											<div class="col-md-12">
												<label>Construction Type:</label>
												<textarea type="text"  class="form-control" name="construction_type"  ><?php echo $property_construction_type ? $property_construction_type : 'Wood Frame Construction'; ?></textarea>
											</div>	
										</div>
										
										<div class="row property_loan_row">
										
											<div class="col-md-12">
												<label>Short Description of Property: </label>
												<textarea type="text"  class="form-control" name="desc_property_impro"  ><?php echo $property_desc_property_impro; ?></textarea>
											</div>	
										</div>
											<div class="row property_loan_row">
										
											<div class="col-md-12">
												<label>Property Link: </label>
												<input type="text"  class="form-control" name="property_link" value="<?php echo $property_link; ?>">
											</div>	
										</div>
										
										<div class="row property_loan_row">
											<h4 class="property-edit-h4">Appraiser Information: </h4>
											
										</div>	


										<!-- <script type="text/javascript">
											
											$(document).ready(function(){

												hide_appraiser_information(<?php echo $valuation_company ? $valuation_company : 0; ?>,<?php echo $property_home->id; ?>)
											});

										</script> -->
										
										<div class="row property_loan_row">
											<div class = "col-md-5">
												<label>Valuation Type:</label>

												<select class="form-control" onchange="appraiser_disable(this)" name="valuation_type">
													<?php
													foreach($valuation_type as $key => $option)
													{
														$selected = '';
														if($key == $property_valuation_type)
														{
															$selected = 'selected';
														}
														?>
														<option value="<?php echo $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
														<?php
													}													
													?>
												</select>

											</div>

										</div>

										<div class="row property_loan_row">
											<div class = "col-md-5">
												<label>Valuation Company:</label>
										
									
												<select class="form-control" name="valuation_company" onchange="copy_addresss(this)">
													<?php
													foreach($valuation_com as $key => $option)
													{
														?>
													<option value="<?php echo $key; ?>"<?php if($key == $valuation_company){ echo 'Selected';} ?>><?php echo $option; ?></option>
														<?php
													}
													
													?>
												</select>										
											</div>
											<div class = "col-md-6">
												<label>Appraiser Name:</label>
												<!--<input class="form-control" type = "text" name = "appraiser_name" value = "<?php //echo $property_appraiser_name;?>"/>-->
												
											<select class="chosen97_<?php echo $property_home->id; ?> appraiser_class_<?php echo $property_home->id; ?> " id="appraiser_name" name="appraiser_name">
													<?php /* <option value="">Select one</option>
													<?php foreach($fetch_all_contact as $row){ ?>
														
														<option value="<?php echo $row->contact_id;?>" <?php if($row->contact_id == $property_appraiser_name){echo 'Selected';}else{echo $property_appraiser_name;}?>><?php echo $row->contact_firstname .' '.$row->contact_middlename .' '.$row->contact_lastname; ?></option>
													<?php } ?> */ ?>
													<input type="hidden"  class="unique_<?php echo $property_home->id; ?>" value="<?php echo $property_appraiser_name;?>" id="appraiser_namee">
												</select>											
											</div>
										</div>
										
										<div class="row property_loan_row">
										
											<div class = "col-md-4">
												<label>Appraisal Company:</label>

												<input class="form-control company_<?php echo $property_home->id;?>" type = "text" id="valuation_vendor" name = "valuation_vendor" value = "<?php echo $property_valuation_vendor;?>"/>	
												<input type="hidden" id="valuation_vendorr" value = "<?php echo $property_valuation_vendor;?>">											
											</div>
													
											<div class="col-md-4">
												<label>Phone:</label>
												<input type="text" name="p_phone" id="p_phone" class="form-control phone-format number_only" value="<?php echo $p_phone;?>">
												<input type="hidden" id="p_phonee" value = "<?php echo $p_phone;?>">		
											</div>
											<div class="col-md-4">
												<label>E-mail:</label>
												<input type="email"  id="p_email" name="p_email" class="form-control" value="<?php echo $p_email;?>">
												<input type="hidden" id="p_emaill" value = "<?php echo $p_email;?>">		
											</div>
										
											
											
										</div>
										
								
										
										<div class="row property_loan_row">

											<div class="col-md-4">
												<label>Valuation Status:</label>

												<select class="form-control" id="valuation_status" name="valuation_status">
													<?php
													foreach($property_Valuation_Status as $key => $option)
													{
														$selected = '';
														if($key == $valuation_status)
														{
															$selected = 'selected';
														}
														?>
															
														<option value="<?php echo $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
														<?php
													}
													
													?>
												</select>
											</div>
											<div class = "col-md-4 divdate_<?php echo $property_home->id;?>">
												<label>Completion Date: </label>
												<input class="datepicker form-control" type = "text" id = "ex_com_date" name = "ex_com_date" placeholder = "MM-DD-YYYY"  value = "<?php echo $ex_com_date ;?>"/>												
											</div>

										</div>


										<div class="row property_loan_row">
											
											<div class="col-md-4">
												<label>Relationship Type:</label>

												<select class="form-control" id="relationship_type" name="relationship_type">
													<?php
													foreach($relationship_type as $key => $option)
													{
														$selected = '';
														if($key == $property_relationship_type)
														{
															$selected = 'selected';
														}
														?>
															
														<option value="<?php echo $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
														<?php
													}
													
													?>
													
													<input type="hidden" value="<?php echo $property_relationship_type;?>" id="relationship_typee">
												</select>
											</div>
											<div class = "col-md-4 divdate_<?php echo $property_home->id;?>">
												<label>Date of Valuation: </label>
												<input class="datepicker form-control" type = "text" id = "valuation_date" name = "valuation_date" placeholder = "MM-DD-YYYY"  value = "<?php echo $property_valuation_date ;?>"/>												
											</div>
											
											<!--<div class = "col-md-4">
												<label>Address of Appraiser:</label>
												<input class="form-control" type = "text" name = "appraiser_address" value = "<?php echo $property_appraiser_address;?>"/>												
											</div>-->
										</div>
										
										<div class="row property_loan_row">
											
											<div class="col-md-4">
												<label>Appraisal Street Address:</label>
												<input type="hidden" id="appraiser_s_addresss" value = "<?php echo $appraiser_s_address;?>">	
												<input class="form-control" type = "text" id="appraiser_s_address"  name = "appraiser_s_address" value = "<?php echo $appraiser_s_address;?>"/>
											</div>
											<div class="col-md-4">
										<input type="hidden" id="appraiser_unitt" value = "<?php echo $appraiser_unit;?>">
												<label>Appraiser Unit # :</label>
												<input class="form-control" type = "text" id="appraiser_unit" name = "appraiser_unit" value = "<?php echo $appraiser_unit; ?>"/>
											</div>
											
											
										</div>
										
										<div class="row property_loan_row">
											
											
											
											<div class="col-md-4">
												<label>Appraiser City :</label>
													<input type="hidden" id="appraiser_cityy" value = "<?php echo $appraiser_city;?>">

												<input class="form-control" type = "text" id="appraiser_city" name = "appraiser_city" value = "<?php echo $appraiser_city; ?>"/>
											</div>
											<div class="col-md-4">
												<label>Appraiser State :</label>

												<select name="appraiser_state" id="appraiser_state" class="form-control">
												<?php 
												foreach($STATE_USA as $key => $option)
												{
													?>
											
													<option value="<?php echo $key?>" <?php if($appraiser_state == $key) { echo "selected"; } ?> ><?= $option?></option>
													<?php
												}
												?><input type="hidden" value="<?php echo $appraiser_state;?>" id="appraiser_statee">
												</select>
											</div>
											<div class="col-md-4">
												<label>Appraiser Zip :</label>
												<input type="hidden" id="appraiser_zipp" value = "<?php echo $appraiser_zip;?>">
												<input class="form-control" type = "text" id="appraiser_zip" name = "appraiser_zip" value = "<?php echo $appraiser_zip;?>"/>
											</div>
										</div>
										
										
										
										<div class="row property_loan_row">
											<h4 class="property-edit-h4">Property Renovation Details:</h4>
											<div class="row">
												<label>&nbsp;</label>
											</div>
												<div class="col-md-9">
												<label>Does the Borrower intend to rehab the property? </label>
												</div>
											<div class="col-md-3">
												<select name="rehab_property" class="form-control" onchange="change_rehab_property(this,<?php echo $property_home->id; ?>);" id="rehab_property">
												<?php foreach($yes_no_option3 as $key => $row){?>
													<option value="<?php echo $key;?>" <?php if($key == $rehab_property){echo 'selected';}?>><?php echo $row;?></option>
												<?php } ?>
												
												</select>
											<script>
											
											$(document).ready(function(){
												
												change_rehab_property(<?php echo $rehab_property ? $rehab_property : 0; ?>,<?php echo $property_home->id; ?>);
												
											})
											</script>
											</div>
											<div class="row"><label>&nbsp;</label></div>
											
											<div class="col-md-9">
												<label>Construction Budget:</label>
											</div>

											<input type="hidden" value="<?php echo $construction_budget; ?>" id="constructions_budgetttt_<?php echo $property_home->id; ?>">
											<div class="col-md-3">	
												<input  type="text" class="form-control number_only amount_format_without_decimal" name = "construction_budget" id="constructions_budget_<?php echo $property_home->id; ?>" value="<?php echo '$'.number_format($construction_budget);?>"  />
											</div>
																						
										</div>
										
										<div class="row property_loan_row">
											<div class="col-md-9">
												<label>Will the work be completed by a 3<sup>rd</sup> party contractor?</label>
											</div>
											<div class="col-md-3">	
												<select class="form-control" name="contractor_party" id="contractor_party_<?php echo $property_home->id; ?>" onchange="change_constractor_party(this,<?php echo $property_home->id; ?>);">
												<?php foreach($yes_no_option4 as $key => $row){ ?>
												<option value="<?php echo $key;?>" <?php if($key == $contractor_party){echo 'Selected';}?>><?php echo $row;?></option>
												<?php } ?>
												</select>
											</div>
											<script>
											
											$(document).ready(function(){
												
												change_constractor_party(<?php echo $contractor_party ? $contractor_party : 0; ?>,<?php echo $property_home->id; ?>);
												
											})
											</script>
											<div class="row"><label>&nbsp;</label></div>
											
											<div class="col-md-6">
												<label>Name of Contractor:</label>
											</div>
											<div class="col-md-6">	
												<input type="text" class="form-control" name ="contractor_name" id="contractor_name_<?php echo $property_home->id; ?>" value="<?php echo $contractor_name ? $contractor_name : '';?>"  />
											</div>
											<div class="row"><label>&nbsp;</label></div>
											
											<div class="col-md-6">
												<label>Contractor License:</label>
											</div>
											<div class="col-md-6">	
												<input  type="text" class="form-control" name ="contractor_license" id="contractor_license_<?php echo $property_home->id; ?>" value="<?php echo $contractor_license ? $contractor_license : '';?>" />
											</div>
											<div class="row">&nbsp;</div>
											<div class="col-md-12">
												<label>Description of Proposed Improvements:</label>
												<textarea type="text" class="form-control" name="desc_property_p_impro" style="height:100px !important;" id="desc_property_<?php echo $property_home->id; ?>" ><?php echo $property_desc_property_p_impro; ?></textarea>
											</div>
											
													
										</div>
										
										<div class="row property_loan_row">
													<div class="form-group">
														  
														  <div class="col-md-12">
															<!--<button id="" name="button" class="btn btn-primary borrower_save_button" value="save">Save</button>-->
															
														  </div>
													</div>
										</div>
									</div>
									<div class="col-sm-3 property-right-content-data" >	
										<ul class="block-options">
											<li><a data-toggle="modal" href="#Encumbrances-<?php echo $property_home->id; ?>">Encumbrances</a></li>

											<?php
												if($property_Cash_Flow == '2'){
													echo '<li><a href="#cashflow-'.$property_home->id.'" data-toggle="modal">Cashflow</a></li>';
												}else{
													echo '<li class="disabled" style="opacity: 0.6;"><a href="#" data-toggle="modal">Cashflow</a></li>';
												}
											?>

											<li><a data-toggle="modal" href="#comparable_sale-<?php echo $property_home->id; ?>">Comparable Sales</a></li>
											
											<li><a href="#property_insurance-<?php echo $property_home->id; ?>" data-toggle="modal">Property Insurance</a></li>
											<li><a href="#property-tax-<?php echo $property_home->id; ?>" data-toggle="modal">Property Taxes</a></li>
											<li><a href="#image-folder-<?php echo $property_home->id; ?>" data-toggle="modal">Property Images</a></li>
											<li><a href="#appraisal_document_<?php echo $property_home->id; ?>" data-toggle="modal">Appraisal / BPO</a></li>
											<!--
											<li><a href="#property-images-<?php echo $property_home->id; ?>" data-toggle="modal">Property Images</a></li>
											-->
										</ul>
									</div>						
									</div>
									<!----------End of Content modal------------------------------->
											
								</div>
								</div>
								<div class="modal-footer">
									<button type="submit" value = "save" class="btn blue borrower_save_button" name="button" id="property-home-save-<?php echo $property_home->id; ?>">Save</button>
									<button type="button"  class="btn default borrower_save_button" onclick="property_edit_close('<?php echo $property_home->id; ?>')"  id="property-home-close-<?php echo $property_home->id; ?>">Close</button>
									<!------- CUSTOM POPUP MODAL STARTS --------------->
									<div class = "property_data_confirmOverlay" id="property_data_confirmOverlay_<?php echo $property_home->id; ?>" style = "display:none">
										<div id="confirmBox" style="top:75% !important">

											<!--<h4>TaliMar Database says:</h4>-->
											<p>Do you want to Save Information? 
											<a style="margin-left:15px;" class="btn blue borrower_save_button" href="javascript:void(0);" onclick = "save_property_info('<?php echo $property_home->id; ?>')">Yes<span></span> </a>
												
											<a class="btn default borrower_save_button" href="<?php echo base_url().'redirect_to_property_close'.$loan_id;?>">No<span></span></a></p>

											<!--<div id="confirmButtons">
												<a class="btn blue borrower_save_button" href="javascript:void(0);" onclick = "save_property_info('<?php echo $property_home->id; ?>')">Yes<span></span></a>
												
												
												<a class="btn default borrower_save_button" href="<?php echo base_url().'redirect_to_property_close'.$loan_id;?>">No<span></span></a>
											</div>-->
										</div>
									</div>
									
									<!------- CUSTOM POPUP MODAL ENDS --------------->
								</div>
								
						</form>
						<a id="property-home-link-<?php echo $property_home->id; ?>" href="<?php echo base_url().'redirect_to_property_close'.$loan_id; ?>"></a>
						</div>
											<!-- /.modal-content -->
					</div>
										<!-- /.modal-dialog -->
				</div>
<!--------------------- Property modal Ends ---------------->


<!--------------------- Start Ecumbrance Modals ------------>

<!-------------------------MODALS Encumbrances------------------------------->
	
<div class="modal fade" id="Encumbrances-<?php echo $property_home->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url(); ?>form_encumbrances" id="form_encumbrances">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<!--<h4 class="modal-title">Encumbrances : #<?php// echo $modal_talimar_loan; ?></h4>-->
					<h4 class="modal-title">Encumbrances : <?php echo $property_property_address; ?></h4>
				</div>
				<div class="modal-body">
						<!----------- Modal body goes here ----->
					<div class="portlet-body rc_class">
						<p><strong id="ecumbrance-data-status"></strong></p>
						<!----------- Hidden fields  ----------->
						<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
						<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
						<input type="hidden" name="property_home_id" value="<?php echo $property_home->id;?>">
						<!----------- End of hidden Fields  ---->
						
						<?php
							$monthly_payment_val = 0;
							foreach($fetch_impound_account as $key=>$row){
								$impound_amount = $row->amount;
								$readonly = '';
														
								if($row->items == 'Mortgage Payment')
								{
															
									$monthly_payment_val += ($fetch_loan_result[0]->intrest_rate/100) * $fetch_loan_result[0]->loan_amount / 12;
								}
								if($row->items == 'Property / Hazard Insurance Payment')
								{
									if($row->impounded == 1){
										$monthly_payment_val += isset($loan_property_result) ? ($loan_property_insurance_result->annual_insurance_payment / 12) : 0;
									}
								}
														
								if($row->items == 'County Property Taxes')
								{
									if($row->impounded == 1){
										$monthly_payment_val += isset($loan_property_result) ? ($loan_property_result[0]->annual_property_taxes / 12) : 0;
									}
								}
								if($row->items == 'Flood Insurance')
								{
									if($row->impounded == 1){
										$monthly_payment_val += isset($row->amount) ? ($row->amount) : 0;
									}
								}
														
								if($row->items == 'Mortgage Insurance')
								{
									if($row->impounded == 1){
										$monthly_payment_val += isset($row->amount) ? ($row->amount) : 0;
									}
								}
							}
						?>
						<div class="row">
							<div class="col-sm-12 ecumbrance-modal-left-data">
								
								<div class="ecumbrance-edit-notes">
									
									<strong>Other Encumbrances :</strong> (Add all <u>existing</u> and <u>expected</u> liens below).
								</div>

								
								<div class="ecumbrance-border-top-bottom-div">
									<table id="table_ecumbrances-<?php echo $property_home->id; ?>" class="table table-bordered table-striped table-condensed flip-content th_text_align_center table_ecumbrances_css">
							
										<thead>
											
											<tr>
												<th></th>
												<th style = "width: 16%;">Lender Name</th>
												<th style = "width: 11%;">Original Balance </th>
												<th style = "width: 11%;">Balance Prior<br> to Close</th>
												<th style = "width: 10%;">Balance at<br> Close</th>
												<th style = "width: 8%;">Position Prior to<br> Close</th>
												<th style = "width: 8%;">Paid Off at<br>Close</th>
												<th style = "width: 8%;">Position After<br> Close</th>
												<th style = "width: 8%;">Priority to<br> TaliMar</th>
												<th style = "width: 9%;">CLTV Calc</th>
												<th style = "width: 11%;">Monthly Payment</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
										<?php

									
											if(isset($ecumbrances_data[$property_home->id]))
											{
												$count = 0;
												$original_balance = '0.00';
												$current_balance = '0.00';
												$monthly_payment = '0.00';
												$total_balanceToClose = '0.00';

												$cltv = 0;
												foreach($ecumbrances_data[$property_home->id] as $row)
												{
													// echo '<pre>';	
													// print_r($row);
													// echo '</pre>';	
													//for total column
													$count++;
													$original_balance += $row->original_balance;
													$current_balance += $row->current_balance;
													$monthly_payment += $row->monthly_payment;
													$total_balanceToClose += $row->balance_at_close + $fetch_loan_result[0]->loan_amount;

													if($row->existing_priority_to_talimar == '1'){

														$PrioritytoTaliMar = 'Yes';
														$BalanceSenior = $row->balance_at_close;

													}else{
														$PrioritytoTaliMar = 'No';
														$BalanceSenior = 0;

													}

													$cltv += $BalanceSenior;

										/*$fetch_total_property_home = $this->User_model->query("select position from property_home where talimar_loan='".$row->talimar_loan."' AND id='".$row->property_home_id."'");
										$fetch_total_property_home = $fetch_total_property_home->result();
												 $position_priority = $fetch_total_property_home[0]->position;*/

										?>
											<tr id="tr-row-<?php echo $row->id; ?>">
												<td class="check_row"><a class="ecum_remove"><i class="fa fa-trash" aria-hidden="true"></i>
													<input type="hidden" value="<?php echo $row->id; ?>" name="id"></a>
												</td>
											
												<td><input type="text" name="lien_holder[]" value="<?= $row->lien_holder;?>" class="form-control"  /></td>
												
												<td><input type="text" name="original_balance[]" value="<?= '$'.number_format($row->original_balance);?>"  class="number_only form-control" onchange="autocalculate_encumbrance_left_data(this)" /></td>

												<td><input type="text" name="current_balance[]" value="<?= '$'.number_format($row->current_balance);?>"  class="number_only form-control" onchange="autocalculate_encumbrance_left_data(this)" /></td>
												
												<td><input type="text" name="balance_at_close[]" value="<?= '$'.number_format($row->balance_at_close);?>"  class="number_only form-control" onchange="autocalculate_encumbrance_left_data(this)" /></td>
											
												
												<td>
													<input type="hidden" name="ecum_id[]" value="<?php echo $row->id;?>">
													
													<input type="text" name="position[]" value="<?php echo $position_option[$row->current_position];?>" class="form-control" >
													
													
												</td>
												<td>
											
												<input type="text" name="will_it_remain[]" value="<?php echo $yes_no_option[$row->will_it_remain]; ?>" class="form-control" >
												</td>
												
												
												<td>
												<input type="hidden" name="ecum_id1[]" value="<?php echo $row->id;?>">
												<input type="text" name="proposed_priority[]" value="<?php echo ($row->will_it_remain == 1) ? 'N/A' : $position_option[$row->proposed_priority]; ?>" class="form-control" >
												
												</td>
												<td> <input type="text" name="PrioritytoTaliMar[]" value="<?php echo $PrioritytoTaliMar;?>" class="form-control" ></td>

												<td><input type="text" name="BalanceSenior[]" value="$<?php echo number_format($BalanceSenior,2);?>" class="form-control" ></td>

												<td><input type="text" name="monthly_payment[]" value="<?= '$'.number_format($row->monthly_payment);?>"  class="form-control number_only" onchange="autocalculate_encumbrance_left_data(this)" />
												</td>
												
												<td>
													<a onclick="load_ecumbrances_questions_ajax('<?php echo $row->id;?>','<?php echo $row->lien_holder; ?>')"><i class="fa fa-pencil" aria-hidden="true"></i></a>
												</td>
											</tr>
											<?php }
												}	
											?>

											<tr>
												<td></td>
												<td><input type="text" value="TaliMar Financial" class="form-control"></td>
												<td><input type="text" value="N/A" class="form-control"></td>
												<td><input type="text" value="N/A" class="form-control"></td>
												
												<td><input type="text" value="<?php echo '$'.number_format($fetch_loan_result[0]->loan_amount,2);?>" class="form-control"></td>
												<td><input type="text" value="N/A" class="form-control"></td>
												<td><input type="text" value="N/A" class="form-control"></td>	
												<td>	
													<input type="text" name="position[]" value="<?php echo $position_option[$fetch_loan_result[0]->position];?>" class="form-control" >
												</td>	
												<td><input type="text" value="N/A" class="form-control"></td>
												<td>
													<input type="text" value="<?php echo '$'.number_format($fetch_loan_result[0]->loan_amount,2);?>" class="form-control">
												</td>
												<td><input type="text" value="<?php echo '$'. number_format($monthly_payment_val,2);?>" class="form-control"></td>
												<td></td>
											
												
											</tr>
								
										</tbody>
										<tfoot>
											<tr>
												<th colspan="2"><input type="text" value="Total: <?php echo $count+1;?>" class="form-control" style="font-weight: 700;"></th>
												<th><input type="text" value="<?php echo '$'. number_format($original_balance,2);?>" class="form-control" style="font-weight: 700;"></th>
												<th><input type="text" value="<?php echo '$'. number_format($current_balance,2);?>" class="form-control" style="font-weight: 700;"></th>
												
												<th><input type="text" value="<?php echo  isset($total_balanceToClose) ? '$'.number_format($total_balanceToClose,2) :''; ?>" class="form-control" style="font-weight: 700;"></th>
												<th></th>
												<th></th>												
												<th></th>	
												<td></td>
												<td>
													<input type="text" value="<?php echo '$'.number_format($fetch_loan_result[0]->loan_amount + $cltv,2);?>" class="form-control">
												</td>											
												<th><input type="text" value="<?php echo '$'. number_format($monthly_payment + $monthly_payment_val,2);?>" class="form-control" style="font-weight: 700;">
												</th>
												<th></th>	
												
											</tr>
									

											<tr>
												<th colspan="2"><input type="text" value="Property Value:" class="form-control" style="font-weight: 700;"></th>
												<th></th>
												<th ></th>		
												<th ></th>
												<th></th>
												<th></th>											
												<th></th>											
												<th></th>											
												<th><input type="text" value="<?php echo '$'. number_format($property_underwriting_value);?>" class="form-control" style="font-weight:700;"></th>	
												<td></td>
												<td></td>																		
											</tr>
											<tr>
												
												<?php 
												
												//$loantovalue = ($fetch_loan_result[0]->loan_amount + $original_balance)/$property_underwriting_value*100; 
												//$loantovalue = (($current_balance + $fetch_loan_result[0]->loan_amount)/$property_underwriting_value)*100; 
												$loan_valuee = ($fetch_loan_result[0]->loan_amount + $cltv)/$property_underwriting_value*100;

												//$loan_valuee=($current_balance+$fetch_loan_result[0]->loan_amount)/$property_underwriting_value*100;
												?>
												
												
												<th colspan="2"><input type="text" value="Loan to Value:" class="form-control" style="font-weight: 700;"></th>
												<th></th>
												<th></th>
												<th></th>
												<th></th>
												<th></th>										
												<th></th>											
												<th></th>											
												<th><input type="text" value="<?php echo number_format($loan_valuee,2).'%';?>" class="form-control" style="font-weight: 700;"></th>																		
												<td></td>
												<td></td>									
																					
											</tr>

										</tfoot>
									</table>
								</div>

								<button  type="button" class="btn blue shape_rectangle" onclick="add_ecumbrance_tr(<?php echo $property_home->id; ?>)" >Add lien</button>
							  
							</div>

							<script>
							// script for autocalculate right div section (Ecumbrance Data)
							$(document).ready(function(){
								autocalculate_encumbrance_left_data(<?php echo $property_home->id; ?>);
							});
							</script>
							<!--<div class="col-sm-3 ecumbrance-modal-right-data" id="ecumbrance-data-right-<?php echo $property_home->id; ?>">
								<p><strong>Encumbrance Data</strong></p>
								<p># of Senior Liens : <?php echo $ecum_right_data ? $ecum_right_data[$property_home->id]['count_encum'] : 0; ?></p>
								
								<p>$ of Senior Liens : <?php echo $ecum_right_data ? '$'.number_format($ecum_right_data[$property_home->id]['sum_current_balance'],2) : '$0.00'; ?></p>
								
								<p>TaliMar Loan : $<?php echo number_format($fetch_loan_result[0]->loan_amount); ?></p>
								
								<?php $combine_balance = $fetch_loan_result[0]->loan_amount + $ecum_right_data[$property_home->id]['sum_current_balance']; ?>
								
								<p>Combined Loan Balance : $<?php echo number_format($combine_balance); ?></p>
								
								<p>Borrower Equity : <?php echo $ecum_left_equity = '$'.number_format($property_underwriting_value - $ecum_right_data[$property_home->id]['sum_current_balance'] - $fetch_loan_result[0]->loan_amount,2); ?></p>
								
								<p>TaliMar LTV : <?php /* echo $calculate_ecum_ltv = number_format(($fetch_loan_result[0]->loan_amount/ $property_underwriting_value)*100,2).'%'; */ 
								
								
								echo $calculate_ecum_ltv = number_format(($ecum_right_data[$property_home->id]['sum_current_balance'] + $fetch_loan_result[0]->current_balance)/$property_underwriting_value * 100,2).'%';
								
								/* echo'<pre>';
								print_r($ecum_right_data[$property_home->id]['sum_current_balance']);
								echo'<br>';
								print_r($fetch_loan_result);
								echo'<br>';
								print_r($property_underwriting_value);
								echo'</pre>'; */
								?> 
								
								</p>
							</div>-->
						</div>

						<div class="row">&nbsp;</div>
						<div class="row">
							<div class="col-md-12">
								<label>Other Financing Approval Notes:</label>
								<textarea name="other_ecum_notes" class="form-control" rows="4" placeholder="Enter Other Financing Approval Notes Here..."><?php echo $ecumbrances_data[$property_home->id][0]->other_ecum_approval_notes ? $ecumbrances_data[$property_home->id][0]->other_ecum_approval_notes : '';?></textarea>
							</div>
						</div>
					</div>		
					
					<!--End of Modal body   -->
											 
				</div>
				<div class="modal-footer">
					
					<button type="submit" name="button" class="btn blue" >Save</button>
					<button type="button"  class="btn default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!--------------------- End Ecumbrance Modals ------------>

<div class="modal fade bs-modal-lg" id="cashflow-<?php echo $property_home->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<!--<h4 class="modal-title">Property Cashflow  : #<?php //echo $modal_talimar_loan; ?></h4>-->
				<h4 class="modal-title">Property Cashflow  : <?php echo $property_addresss; ?></h4>
			</div>
			<form method="POST" action="<?php echo base_url().'form_monthly_income_expense'; ?>">
			<div class="modal-body">
				<!----------- Hidden fields  ----------->
				<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
				<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
				<input type="hidden" name="property_home_id" value="<?php echo $property_home->id;?>">
				<!----------- End of hidden Fields  ---->
				<div class="row">
					<div class="col-sm-6 property-cashflow-left">
						<h4 style="background: #efefef;padding: 6px;font-weight: 500;">Property Income</h4>
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" id="cashflow_monthly_income-<?php echo $property_home->id; ?>">
							<thead>
								<tr>
									<th style="width: 13%;"></th>
									<th>Description</th>
									<th>Amount</th>
								</tr>	
							</thead>
							<tbody>
							<?php
							if(isset($fetch_monthly_income[$property_home->id]))
							{
								foreach($fetch_monthly_income[$property_home->id] as $row)
								{
									?>
									<tr>
										<td>
											<a title="Delete" onclick="delete_monthly_income(this)"><i class="fa fa-trash" aria-hidden="true"></i></a>&nbsp;
											<a title="Edit" onclick="edit_monthly_income_values(this,'<?php echo $row->id; ?>');"><i class="fa fa-edit" aria-hidden="true"></i></a>	
										</td>
										<td>
											<input  type="hidden" name="id_income[]" value="<?php echo $row->id; ?>">
											<input class="form-control" type="text" name="items_income[]" value="<?php echo $row->items; ?>" <?php echo $input_readonnly; ?> readonly>
										</td>
										<td><input class="form-control number_only" type="text" name="amount_income[]" onchange="monthly_income_amounts(this)" value="<?php echo '$'.number_format($row->amount,2); ?>"></td>
									</tr>
									<?php
								}
							}
							else
							
							{
							?>
									<tr>
										<td>&nbsp;</td>
										<td>
											<input  type="hidden" name="id_income[]" value="new">
											<input type="text" name="items_income[]" class="form-control" value="" />
										</td>
										<td>
											<input type="text" name="amount_income[]" class="form-control number_only">
										</td>
										
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>
											<input  type="hidden" name="id_income[]" value="new">
											<input type="text" name="items_income[]" class="form-control" value="" />
										</td>
										<td>
											<input type="text" name="amount_income[]" class="form-control number_only">
										</td>
										
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>
											<input  type="hidden" name="id_income[]" value="new">
											<input type="text" name="items_income[]" class="form-control" value="" />
										</td>
										<td>
											<input type="text" name="amount_income[]" class="form-control number_only">
										</td>
										
									</tr>
							<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<th></th>
									<th style="vertical-align:middle;">Sub Total:</th>
									<th><input class="form-control" type="text" id="total_monthly_income" readonly ></th>
								</tr>
							</tfoot>
						</table>
						<button type="button" class="btn blue" onclick="add_roww_monthly_income(<?php echo $property_home->id;?>)">Add</button>
					</div>
					
					<div class="col-sm-6 property-cashflow-right">
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" id="cashflow_monthly_expense-<?php echo $property_home->id;?>">
							<thead>
								<tr>
									<th></th>
									<th>Expense (Monthly)</th>
									<th>Amount</th>
								</tr>	
							</thead>
							<tbody>
								<?php
							if(isset($fetch_monthly_expense[$property_home->id]))
							{
								foreach($fetch_monthly_expense[$property_home->id] as $row)
								{
									?>
									<tr>
										<td>
										<?php
											// if($row->items == 'Property Tax' || $row->items == 'Property Insurance' || $row->items == 'Vacancy' )
											// {
												// $input_readonnly = 'readonly';
											// }
											// else
											// {
												$input_readonnly = '';
												?>
												<a onclick="delete_monthly_expense(this)"><i class="fa fa-trash" aria-hidden="true"></i></a>
												<?php
											// }
										?>
										</td>
										<td>
											<input type="hidden" name="id_expense[]" value="<?php echo $row->id; ?>">
											<input class="form-control" type="text" name="items_expense[]" value="<?php echo $row->items; ?>" <?php echo $input_readonnly; ?> readonly>
										</td>
										<td>
										<input class="form-control number_only" type="text" name="amount_expense[]" onchange="monthly_expense_amounts(this)" value="<?php echo '$'.number_format($row->amount,2); ?>">
										</td>
									</tr>
									<?php
								}
							}
							else							
							{
							?>
								<tr>
									<td>&nbsp;</td>
									<td>
										<input  type="hidden" name="id_expense[]" value="new">
										<input type="text" name="items_expense[]" class="form-control" value="Property Taxes" />
									</td>
									<td>
										<input type="text" name="amount_expense[]" class="form-control number_only">
									</td>	
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>
										<input  type="hidden" name="id_expense[]" value="new">
										<input type="text" name="items_expense[]" class="form-control" value="Property Insurance" />
									</td>
									<td>
										<input type="text" name="amount_expense[]" class="form-control number_only">
									</td>	
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>
										<input  type="hidden" name="id_expense[]" value="new">
										<input type="text" name="items_expense[]" class="form-control" value="Property Management" />
									</td>
									<td>
										<input type="text" name="amount_expense[]" class="form-control number_only">
									</td>	
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>
										<input  type="hidden" name="id_expense[]" value="new">
										<input type="text" name="items_expense[]" class="form-control" value="Vacancy" />
									</td>
									<td>
										<input type="text" name="amount_expense[]" class="form-control number_only">
									</td>	
								</tr>
							<?php } ?>	
							</tbody>
							<tfoot>
								<tr>
									<th></th>
									<th style="vertical-align:middle;">Sub Total:</th>
									<th><input class="form-control" type="text" id="total_monthly_expense" readonly></th>
								</tr>
							</tfoot>
						</table>
						<button type="button" class="btn blue" onclick="add_roww_monthly_expense(<?php echo $property_home->id;?>)">Add</button>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3 pull-right">
						<label class="label-control">Net Operating Income:</label>
						<input id="net_operating_income-<?php echo $property_home->id;?>" class="form-control">
					</div>
				</div>
				
			</div>
			<script>
		$(document).ready(function(){
			monthly_expense_amounts(<?php echo $property_home->id; ?>);
			monthly_income_amounts(<?php echo $property_home->id; ?>);
		})
			</script>
			
			<script>
			$(document).ready(function(){
				auto_add_cashflow(<?php echo $property_home->id; ?>);
			});
			</script>
			<div class="modal-footer">
				<button type="submit" class="btn blue">Save</button>
				<a onclick="auto_add_cashflow(<?php echo $property_home->id; ?>);" class="btn green">Auto Calc</a>
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				
			</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->












<!--  Comparable sale-      ----------->
	



<div class="modal fade" id="comparable_sale-<?php echo $property_home->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url();?>form_comparable_sale">
				<!-----------Hidden fields----------->
					<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
					<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
					<input type="hidden" name="property_home_id" value="<?php echo $property_home->id; ?>">
				<!-----------End of hidden Fields---->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<!--<h4 class="modal-title">Comparable Sale: Loan #(<?php// echo $modal_talimar_loan; ?>)</h4>-->
					<h4 class="modal-title">Comparable Sales: <?php echo $property_property_address; ?></h4>
				</div>
				<div class="modal-body">
					 <table id="table_comparable_sale-<?php echo $property_home->id;?>" class="table table-bordered th_text_align_center compare_sale_css">
						<thead>
							<tr>
							
								<th style="width:30%;">Property Address</th>
								<!--<th style="width:7%;">Property Link</th>-->
								<th>Date Sold</th>
								<th>Property<br>Size</th>
								<th>Beds</th>
								<th>Baths</th>
								<th># of Units</th>
								<th>Lot Size</th>
								<th>Year Built</th>
								<th>Sale Price</th>
								<th>$ / SQ FT</th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						
						<tbody>
						 <?php
						 if(isset($fetch_comparable_sale[$property_home->id])){
							 
							 $sumallunit = 0;
							 foreach($fetch_comparable_sale[$property_home->id] as $row)
							 {

							 	// echo '<pre>';
							 	// print_r($row);
							 	// echo '</pre>';
							 	
							 
								?>
								<tr id="remove_<?php echo $row->id;?>" class="s">
						

								<td>
									<input type="hidden" name="comparable_id[]" value="<?php echo $row->id; ?>" >

									<?php  

										$add=explode("-",$row->property_address);
										$adddress1 = $add[0].' '.$row->unit.';';
										$adddress2 = $add[1].''.$add[2].''.$add[3];

									?>
									<!--<input type="text" name="property_address[]" value="<?php echo $adddress1.' '.str_replace(';', '', $adddress2);?>">-->
									<a target="_blank" href="<?php echo $row->prop_link;?>" style="float: left !important;"><?php echo $adddress1.' '.str_replace(';', '', $adddress2);?></a>
								</td> 


									<!--<td><a target="_blank" href="<?php echo $row->prop_link;?>">Link</a></td>-->

									<td><input type="text" name="date_sold[]" id="date_sold" value="<?php echo $row->date_sold; ?>" /></td>

									 <?php 
									 	if($row->prop_building==''){

										$prop_building=		$row->sq_ft;
									 	}else{
										$prop_building=		$row->prop_building;

									 	}

									 	$str_lot_size = str_replace(',', '', $row->lot_size);

									 ?>
									<td><input type="text" name="sq_feet[]" id="sq_feet" value="<?php echo $prop_building ? number_format($prop_building).' SF':''; ?>"  onchange="comparable_cal_per_sq_feet(this)" class="number_only"></td>
									<td><input type="text" name="bedrooms[]" value="<?php echo $row->bedrooms; ?>" class="number_only"></td>
									<td><input type="text" name="bathrooms[]" value="<?php echo $row->bathroom; ?>"  class="number_only"></td>
									<td><input type="text" name="unit[]" value="<?php echo $row->prop_unit; ?>"  class="number_only"></td>

									<td><input class="number_only" type="text" name="lot_size[]" value="<?php echo $str_lot_size ? number_format($str_lot_size).' SF':''; ?>"></td>
									<td><input type="text" name="year_built[]" value="<?php echo $row->year_built; ?>" class="number_only"></td>
									<td><input class="number_only" type="text" name="sale_price[]" value="<?php echo '$'.number_format($row->sale_price); ?>" id="sale_price" onchange="comparable_cal_per_sq_feet(this)"></td>
									<td><input type="text" name="per_sq_feet[]" value="<?php echo '$'.number_format($row->per_sq_ft,2); ?>" id="per_sq_feet" class="number_only"></td>
									<td><a  onclick="edit_com_model(this,'<?php echo $property_home->id; ?>')" id="<?php echo $row->id;?>"><i class="fa fa-edit" aria-hidden="true"></i></a></td>
								
									<td><a class="comparable_sale_remove" id="<?php echo $row->id;?>" onclick="comparable_sale(this);"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
								

								</tr>

								<?
								
								$sumallunit += $row->prop_unit;
							 }
						 }
						 ?>
							<!--<tr>
								<td>
								<input type="hidden" name="comparable_id[]" value="new">
								<input type="text" name="property_address[]">
								</td>
								<td><input type="text" name="date_sold[]" id="date_sold"></td>
								<td><input type="text" name="sq_feet[]" onchange="comparable_cal_per_sq_feet(this)" id="sq_feet"></td>
								<td><input type="text" name="bedrooms[]" class="number_only"></td>
						<td><input type="text" name="bathrooms[]" class="number_only"></td>
								<td><input class="number_only" type="text" name="lot_size[]"></td>
								<td><input type="text" name="year_built[]" class="number_only"></td>
								<td><input class="number_only" type="text" name="sale_price[]" id="sale_price" onchange="comparable_cal_per_sq_feet(this)"></td>
								<td><input type="text" name="per_sq_feet[]" id="per_sq_feet" class="number_only"></td>
								<td><a class="comparable_sale_remove" id="new" onclick="comparable_sale(this);"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
							</tr>-->
						

						</tbody>
						<tfoot>
							<tr><td ><button type="button" data-toggle="modal" data-target="#insert_add_property_<?php echo $property_home->id; ?>" class="btn blue pull-left">(+) Add Property</button></td>
								<td colspan="11"></td>
							</tr>
							<tr class="s">
							<?php 
							$count_comparable = isset($fetch_sum_comparable_data[0]->count_comparable) ? $fetch_sum_comparable_data[0]->count_comparable : 0;
							

							?>
								<th>Average:</th>
								
								<th></th>
								<th><input type="text" value="<?php echo ($fetch_sum_comparable_data[0]->sum_sq_ft != '') ? number_format($fetch_sum_comparable_data[0]->sum_sq_ft / $count_comparable) : 0; ?>" id="avg_sq_ft" readonly></th>
								
								<th><input type="text" value="<?php echo 
								($fetch_sum_comparable_data[0]->sum_bedrooms != '') ?number_format( $fetch_sum_comparable_data[0]->sum_bedrooms / $count_comparable) : ''; ?>" id="avg_bedrooms" readonly ></th>
								
								<th><input type="text" value="<?php echo ($fetch_sum_comparable_data[0]->sum_bathroom != '') ? number_format($fetch_sum_comparable_data[0]->sum_bathroom / $count_comparable) : ''; ?>" id="avg_bathroom" readonly ></th>
								
								<th></th>
								
								<th><input type="text" value="<?php echo ($fetch_sum_comparable_data[0]->sum_lot_size != '') ? number_format($fetch_sum_comparable_data[0]->sum_lot_size / $count_comparable):''; ?>" id="avg_lot_size" readonly ></th>
								
								<th><input type="text" value="<?php echo ($fetch_sum_comparable_data[0]->sum_year_built != '') ? ($fetch_sum_comparable_data[0]->sum_year_built/$count_comparable) : '';?>" id="avg_year_built" readonly ></th>
								
								<th><input type="text" value="<?php echo ($fetch_sum_comparable_data[0]->sum_sale_price != '') ? '$'.number_format($fetch_sum_comparable_data[0]->sum_sale_price / $count_comparable) : ''; ?>" id="avg_sale_price" readonly ></th>
								
								<th><input type="text" id="comp_sale_avg_per_sq_ft" value="<?php echo  ($fetch_sum_comparable_data[0]->sum_per_sq_ft != '') ? '$'.number_format($fetch_sum_comparable_data[0]->sum_per_sq_ft/$count_comparable) : ''; ?>"  readonly></th>
								<th></th>
								<th></th>
							</tr>

							<tr class="">
								<th >Value Modification (%):</th>
								<th colspan="8"></td>
								<td><input type="text" onkeyup="auto_clicked(this)" name="property_value_modification" value="<?php echo isset($propoerty_input_fields[$property_home->id]['property_value_modification']) ? number_format($propoerty_input_fields[$property_home->id]['property_value_modification'],2).'%' : '0%'; ?>" class="rate_format number_only_minus"></th>
								<th></th>
								<th></th>
							
							</tr> 
							
							<tr class="s">
								<th >Subject Property Value:</th>
								
								<td></td>
								<td><input type="text" id="spsq_feet" value="<?php echo $loan_property_result[$property_home->id][0]->square_feet ? number_format($loan_property_result[$property_home->id][0]->square_feet) : ''; ?>" readonly /></td>
								
								<td><input type="text" value="<?php echo $loan_property_result[$property_home->id][0]->bedrooms ? $loan_property_result[$property_home->id][0]->bedrooms : ''; ?>" readonly /></td>
								
								<td><input type="text" value="<?php echo $loan_property_result[$property_home->id][0]->bathrooms ? $loan_property_result[$property_home->id][0]->bathrooms : ''; ?>"  readonly /></td>
								
								<td></td>
								
								<td><input type="text" value="<?php echo $loan_property_result[$property_home->id][0]->lot_size ? number_format($loan_property_result[$property_home->id][0]->lot_size) : ''; ?>" readonly /></td>
								
								<td><input type="text" value="<?php echo $loan_property_result[$property_home->id][0]->yr_built ? $loan_property_result[$property_home->id][0]->yr_built : ''; ?>" readonly /></td>
								
								<?php 
								$avg_sq_feett = ($fetch_sum_comparable_data[0]->sum_per_sq_ft) ? $fetch_sum_comparable_data[0]->sum_per_sq_ft/$count_comparable : '';
								
								
								$square_ft =( isset($loan_property_result) && ($loan_property_result[$property_home->id][0]->square_feet != '')) ? $loan_property_result[$property_home->id][0]->square_feet : 0;
								
								$comp_sale_per_sq_ft = isset($fetch_extra_details[0]->comp_sale_per_sq_ft) ? $fetch_extra_details[0]->comp_sale_per_sq_ft : '0';
								
								$avg_p_sale_price = $square_ft * $comp_sale_per_sq_ft;
								?>
								<td><input type="text" value="" id="subject_property_sale_price" readonly /></td>
								
								<td><input type="text" name="comp_sale_per_sq_ft" id="comp_sale_per_sq_ft" value="" readonly />
								</td>
								<td></td>
								<td></td>
							</tr>
							
							
							
						<!-- 	<tr >
								<th>Valuation Notes:</th>
								<td colspan="11"><input type="text" name="comp_sale_val_notes" value="<?php echo isset($propoerty_input_fields[$property_home->id]['comp_sale_val_notes']) ? $propoerty_input_fields[$property_home->id]['comp_sale_val_notes'] : ''; ?>"></td>
							</tr>
 -->
						
							
							
						
							<tr>
								<th>Property Valuation Explanation:</th>
								<td colspan="11">
									
								<textarea type="text" rows="8" class="form-control" name="property_valuation_notes" style="height:100px !important;" ><?php echo isset($propoerty_input_fieldss[$property_home->id]['comp_sale_val_notes']) ? $propoerty_input_fieldss[$property_home->id]['comp_sale_val_notes'] : ''; ?></textarea>


								</td>
							</tr>


						</tfoot>
					 </table>
					 <?php //echo'<pre>';print_r($propoerty_input_fields);echo'</pre>';?>
					 <script>
					 $(document).ready(function(){
						autocalculate_compare_sale_totals(<?php echo $property_home->id; ?>);
					 });
					 
						
					 
					 
					 </script>
					 
				</div>
				
				<div class="modal-footer">
					<button type="button"  class="btn orange" id="asss"  onclick="autocalculate_compare_sale_totals(<?php echo $property_home->id; ?>)">Auto Calc</button>
					<a onclick="comparable_sale_add(<?php echo $property_home->id; ?>)" class="btn blue" >Add</a>
					<button type="submit" class="btn blue">Save</button>
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
<!-- /.modal-dialog -->
</div>
<!-- END of  Comparable sale-      ----------->

<!--.................................................New edit Property start....................................................-->

<div class="modal fade bs-modal-lg" id="edit_property_<?php echo $property_home->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form method="POST" action = "<?php echo base_url();?>Property_data/edit_property">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						
						<h4 class="modal-title">Edit Property : <?php echo ''.$property_addresss; ?> </h4>
					</div>
					<div class="modal-body">
						<div class="portlet-body flip-scroll">
					
						<!-----------Hidden fields----------->
							<input type="hidden" name ="talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
							<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
							<input type="hidden" name="property_home_id" value="<?php echo $property_home->id; ?>" >
							<input type="hidden" name="p_idd" value="" >
							
						
						
							<div class="row property_loan_row">
							 
								<div class="col-md-4">
								    <label>Property Type:</label>
									
									 <select type="text" class="form-control" name="prop_typee" >
										<?php
										foreach($propp_type as $key => $row)
										{
											// $selected = '';
											// if($key == 1)
											// {
											// 	$selected = 'selected';
											// }
											?>
											<option value="<?php echo $key; ?>"><?php echo $row; ?></option>
											<?php
										}
										?>
									 </select>


								</div>
								
								
							
							</div>
							
							
							<div class="row property_loan_row">
								<div class="col-md-4">
									<label>Street Address:</label>
									<input type="text"  class="form-control " name = "prop_streette"  value="" /> 
								</div>
								<div class="col-md-4">
									<label>Unit #:</label>
									<input type="text"  class="form-control " name = "uni"  value="" /> 
								</div>
								
							
							</div>
							
							<div class="row property_loan_row">
								<div class="col-md-4">
									<label>City:</label>
									<input type="text" class="form-control" name = "prop_cityy" value="" />
								</div>
										
								<div class="col-md-4">
									<label>State:</label>
									<input type="text"  id="" class="form-control" name = "prop_statee"  placeholder = 'State' value="" />
								</div>

									<div class="col-md-4">
									<label>Zip:</label>
									<input type="text"  id="" class="form-control " name = "prop_zipp"  placeholder = 'Zip' value="" />
								</div>
										
								
							</div>
							
							<div class="row property_loan_row">
								<div class="col-md-4">
									<label># of Units:</label>
									<input type="text" class="form-control" onchange="sales_price_change(this)" name = "prop_unitt" value="" required="required" />
								</div>
										
								<div class="col-md-4">
									<label>Property Size (SF):</label>
									<input type="text" class="form-control" onchange="sales_price_change(this)" name = "prop_buildingg"   value="" required="required"/>
								</div>
										
								<div class="col-md-4">
									<label>Lot Size (SF):</label>
									<input type="text" class="form-control " name = "prop_lott"  value="" /> 
								</div>
							</div>

								<div class="row property_loan_row">
								<div class="col-md-4">
									<label>	# of Beds:</label>
									<input type="text" class="form-control" name = "prop_bedss" value="" />
								</div>
										
								<div class="col-md-4">
									<label># of Baths:</label>
									<input type="text" class="form-control" name = "prop_bathss"   value="" />
								</div>
										
								<div class="col-md-4">
									<label>Year Built:</label>
									<input type="text" class="form-control " name = "prop_yearr"  value="" /> 
								</div>
							</div>
								<div class="row property_loan_row">
								<div class="col-md-4">
									<label>Sales Price:</label>
									<input type="text" onchange="sales_price_change(this)" class="form-control number_only amount_format_without_decimal" name = "prop_sale_pricee" value="" />
								</div>
										
								<div class="col-md-4">
									<label>Sales Date:</label>
									<input type="text"  id="" class="form-control datepicker" name = "prop_sale_datee"  placeholder = 'mm-dd-yyyy' value="" />
								</div>
										
								
							</div>
								<div class="row property_loan_row">
								<div class="col-md-4">
									<label>$ Per SF:</label>
									<input type="text" class="form-control number_only" name = "prop_sff" value="" / readonly="readonly" style="background-color:#eeee !important;">
								</div>
										
								<div class="col-md-4">
									<label>$ Per Unit:</label>
									<input type="text" class="form-control number_only" name = "prop_per_unitt"   value="" / readonly="readonly" style="background-color:#eeee !important;"> 
								</div>
										
								
							</div>

							<div class="row property_loan_row">
								<div class="col-md-12">
									<label>Property Link:</label>
									<input type="text" class="form-control" name = "prop_linkk" value="" />
								</div>
									
							</div>


				
						</div>
					</div>
					<div class="modal-footer">
						
						<button type="submit" value = "save" class="btn blue borrower_save_button" name="button">Save</button>
						<button type="button" class="btn default borrower_save_button" data-dismiss="modal">Close</button>
						
						
					</div>
			</form>
			</div>
								<!-- /.modal-content -->
		</div>
							<!-- /.modal-dialog -->
</div>


<!--------------------------------------------------New edit Property end....................................................-->





<!--.................................................New add Property start...................................................-->

<div class="modal fade bs-modal-lg" id="insert_add_property_<?php echo $property_home->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form method="POST" action = "<?php echo base_url();?>insert_new_add_property">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						
						<h4 class="modal-title">Add Property : <?php echo ''.$property_addresss; ?> </h4>
					</div>
					<div class="modal-body">
						<div class="portlet-body flip-scroll">
					
						<!-----------Hidden fields----------->
							<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
							<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
							<input type="hidden" name="property_home_id" value="<?php echo $property_home->id; ?>" >
							
							
						
						
							<div class="row property_loan_row">
							 
								<div class="col-md-4">
								    <label>Property Type:</label>
									
									 <select type="text" class="form-control" name="prop_type" >
										<?php
										foreach($propp_type as $key => $row)
										{
											// $selected = '';
											// if($key == 1)
											// {
											// 	$selected = 'selected';
											// }
											?>
											<option value="<?php echo $key; ?>"><?php echo $row; ?></option>
											<?php
										}
										?>
									 </select>


								</div>
								
								
							
							</div>
							
							
							<div class="row property_loan_row">
								<div class="col-md-4">
									<label>Street Address:</label>
									<input type="text"  class="form-control " name = "prop_street"  value="" /> 
								</div>
									<div class="col-md-4">
									<label>Unit #:</label>
									<input type="text"  class="form-control " name = "uni"  value="" /> 
								</div>
							
							</div>
							
							<div class="row property_loan_row">
								<div class="col-md-4">
									<label>City:</label>
									<input type="text" class="form-control" name = "prop_city" value="" />
								</div>
										
								<div class="col-md-4">
									<label>State:</label>
									<input type="text"  id="" class="form-control" name = "prop_state"  placeholder = 'State' value="" />
								</div>

									<div class="col-md-4">
									<label>Zip:</label>
									<input type="text"  id="" class="form-control " name = "prop_zip"  placeholder = 'Zip' value="" />
								</div>
										
								
							</div>
							
							<div class="row property_loan_row">
								<div class="col-md-4">
									<label># of Units:</label>
									<input type="text" onchange="sales_price_changess(this)" class="form-control" name = "prop_unit" value="" required="required"/>
								</div>
										
								<div class="col-md-4">
									<label>Property Size (SF):</label>
									<input type="text" class="form-control" onchange="sales_price_changess(this)" name = "prop_building"   value="" required="required"/>
								</div>
										
								<div class="col-md-4">
									<label>Lot Size (SF):</label>
									<input type="text" class="form-control " name = "prop_lot"  value="" /> 
								</div>
							</div>

								<div class="row property_loan_row">
								<div class="col-md-4">
									<label>	# of Beds:</label>
									<input type="text" class="form-control" name = "prop_beds" value="" />
								</div>
										
								<div class="col-md-4">
									<label># of Baths:</label>
									<input type="text" class="form-control" name = "prop_baths"   value="" />
								</div>
										
								<div class="col-md-4">
									<label>Year Built:</label>
									<input type="text" class="form-control " name = "prop_year"  value="" /> 
								</div>
							</div>
								<div class="row property_loan_row">
								<div class="col-md-4">
									<label>Sales Price:</label>
									<input type="text" class="form-control number_only amount_format_without_decimal" onchange="sales_price_changess(this)" name = "prop_sale_price" value="" required="required"/>
								</div>
										
								<div class="col-md-4">
									<label>Sales Date:</label>
									<input type="text"  id="" class="form-control datepicker" name = "prop_sale_date"  placeholder = 'mm-dd-yyyy' value="" />
								</div>
										
								
							</div>
								<div class="row property_loan_row">
								<div class="col-md-4">
									<label>$ Per SF:</label>
									<input type="text" class="form-control number_only" name = "prop_sf" value="" readonly="readonly" style="background-color:#eeee !important;">
								</div>
										
								<div class="col-md-4">
									<label>$ Per Unit:</label>
									<input type="text" class="form-control number_only" name = "prop_per_unit"   value=""  readonly="readonly" style="background-color:#eeee !important;">
								</div>
										
								
							</div>

							<div class="row property_loan_row">
								<div class="col-md-12">
									<label>Property Link:</label>
									<input type="text" class="form-control" name = "prop_link" value="" />
								</div>
									
							</div>


				
						</div>
					</div>
					<div class="modal-footer">
						
						<button type="submit" value = "save" class="btn blue borrower_save_button" name="button">Save</button>
						<button type="button" class="btn default borrower_save_button" data-dismiss="modal">Close</button>
						
						
					</div>
			</form>
			</div>
								<!-- /.modal-content -->
		</div>
							<!-- /.modal-dialog -->
</div>


<!--------------------------------------------------New add Property end....................................................-->




<!------------------ Property Insurance modal Starts -------->
<div class="modal fade bs-modal-lg modal-property-insurance" id="property_insurance-<?php echo $property_home->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form method="POST" action = "<?php echo base_url();?>add_property_insurance">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<!--<h4 class="modal-title">Property Insurance: #<?php// echo ''.$modal_talimar_loan; ?> </h4>-->
						<h4 class="modal-title">Property Insurance: <?php echo ''.$property_addresss; ?> </h4>
					</div>
					<div class="modal-body">
						<div class="portlet-body flip-scroll">
					
						<!-----------Hidden fields----------->
							<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
							<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
							<input type="hidden" name="property_home_id" value="<?php echo $property_home->id; ?>" >
							<!-----------End of hidden Fields---->
							<!--<div class="row">
								<div class="col-md-12">
									<label>Mortgage Clause: <a onclick ="autofill_mortage_clause(<?php echo $property_home->id; ?>)"><i class="fa fa-pencil-square" aria-hidden="true"></i></a></label>
									<textarea name="property_single_mortage" class="form-control" placeholder="" id="mortage_textarea_<?php echo $property_home->id; ?>"><?php echo $all_input_datas_of_property_modal[$property_home->id]['property_insurance']['property_single_mortage'] ? $all_input_datas_of_property_modal[$property_home->id]['property_insurance']['property_single_mortage'] : ''; ?></textarea>
									<input type="hidden" id="mortage_hidden_lender_<?php echo $property_home->id; ?>" value="<?php echo $all_lender_name_of_assigment ? implode(', ',$all_lender_name_of_assigment).', its successors and or assigns.' : '';?>">
								</div>
							</div>-->
							<!---------Content modal------------------->
							<?php
								$array_property_insurance_coloumns = array('id', 'talimar_loan', 'property_home_id', 'loan_id', 'insurance_type', 'policy_number','mortgagee', 'monthly_payment', 'payment_close', 'required','request_updated_insurance', 'donottrack', 'insurance_carrier', 'insurance_expiration', 'annual_insurance_payment', 'contact_name', 'contact_number', 'contact_email', 'c_notes', 'names_delete');
								
								$loan_property_insurance_type = $this->config->item('loan_property_insurance_type');
								foreach($loan_property_insurance_type as $insurance_key => $row_insurance)
								{
									
									if(isset($loan_property_insurance[$property_home->id][$insurance_key])){
										
											
										foreach($array_property_insurance_coloumns as $col)
										{
											$property_insurance_data[$insurance_key][$col] = $loan_property_insurance[$property_home->id][$insurance_key]->$col;
										}
										
									}
									else
									{
										foreach($array_property_insurance_coloumns as $col)
										{
											$property_insurance_data[$insurance_key][$col] = '';
										}
									}
									
							// Insurance Title		
							if($insurance_key == '1')
							{
								$insurance_type_title = 'General Property Insurance';
							}
							elseif($insurance_key == '2')
							{
								$insurance_type_title = 'Builder Risk Insurance';
							}
							elseif($insurance_key == '3')
							{
								$insurance_type_title = 'Flood Insurance';
							}
							
							
							?>
							<div class="selectbox_class" id="">
							<div class="row property_loan_row" >
								<div class="col-md-12">
									<h4><?php echo $insurance_type_title; ?></h4>
									<input type="hidden" name="insurance_type[]" value="<?php echo $insurance_key; ?>">
								</div>
								
							</div>
							

							<div class="row property_loan_row">

								<div class="col-md-4 property-insurance-checkbox-top-padding">
									<input type="hidden" name="id[]" value="<?php echo $property_insurance_data[$insurance_key]['id'] ? $property_insurance_data[$insurance_key]['id'] : 'new';?>">
									<input type="checkbox" name="check_requ[]" <?php if($property_insurance_data[$insurance_key]['required'] == 1){ echo 'checked'; }?> onclick="check_required_checkbox(this); disabled_selectbox(this);"> Not Required
									<input type="hidden" name="required[]" value="<?php echo $property_insurance_data[$insurance_key]['required'] ? $property_insurance_data[$insurance_key]['required'] : 0; ?>">
								</div>
								
								<div class="col-md-4 property-insurance-checkbox-top-padding">
									
								<input type="checkbox" id="cancless_<?php echo $insurance_key; ?>" class="form-control" name="check_request_upadted[]" onchange="check_request_updated_insurance_checkbox(this)" <?php if($property_insurance_data[$insurance_key]['request_updated_insurance'] == 1){ echo 'checked'; } ?> >Cancelled

								<input type="hidden"  id="request_updated_insurance" name="request_updated_insurance[]" value="<?php echo $property_insurance_data[$insurance_key]['request_updated_insurance'] ? $property_insurance_data[$insurance_key]['request_updated_insurance'] : 0;?>" >
								
								</div>

								<div class="col-md-4 property-insurance-checkbox-top-padding">

									<input type="checkbox" id="donottrackdata_<?php echo $insurance_key; ?>" class="form-control" name="donottrackdata[]" onchange="check_donottrack_checkbox(this)" <?php if($property_insurance_data[$insurance_key]['donottrack'] == 1){ echo 'checked'; } ?> > Do Not Track

									<input type="hidden"  id="donottrack" name="donottrack[]" value="<?php echo $property_insurance_data[$insurance_key]['donottrack'] ? $property_insurance_data[$insurance_key]['donottrack'] : 0;?>" >
								</div>

							</div>
							
							<div class="row property_loan_row">
							 
								<div class="col-md-4">
									<label>Policy Number:</label>
									<input type="text" id="" class="form-control" name="policy_number[]"  value = "<?php echo $property_insurance_data[$insurance_key]['policy_number'] ? $property_insurance_data[$insurance_key]['policy_number'] :'';?>"  />
								</div>
								
								
							</div>

							<div class="row property_loan_row">
								<div class="col-md-4">
									<label>Annual Payment:</label>
									<input type="text"  class="form-control number_only amount_format" name = "annual_insurance_payment[]"  value="<?= $property_insurance_data[$insurance_key]['annual_insurance_payment'] ? '$'.number_format($property_insurance_data[$insurance_key]['annual_insurance_payment'],2) : '';?>" /> 
								</div>
								<!--<div class="col-md-4">
									<label>Monthly Payment:</label>
									<input type="text"  class="form-control number_only amount_format" name = "monthly_payment[]"  value="<?= $property_insurance_data[$insurance_key]['monthly_payment'] ? '$'.number_format($property_insurance_data[$insurance_key]['monthly_payment'],2) : '';?>" /> 
								</div>-->
								<div class="col-md-4">
									<label>Required Payment at Close:</label>
									<!--<input type="text" class="form-control" name = "payment_close[]"  value="<?= $property_insurance_data[$insurance_key]['payment_close'] ? $property_insurance_data[$insurance_key]['payment_close'] : '';?>" /> -->
									
									<select name = "payment_close[]" class = "form-control">
										<?php
											foreach($yes_no_option as $key => $option)
											{
												$selected = '';
												if(isset($property_insurance_data[$insurance_key]['payment_close']) && ($property_insurance_data[$insurance_key]['payment_close'] != '')){
													
													if($key == $property_insurance_data[$insurance_key]['payment_close'])
													{
														$selected = 'selected';
													}
												}
												?>
												<option value="<?php echo $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
												<?php
											}
											?>
									</select>
								</div>
							</div>
							
							<div class="row property_loan_row">
								<div class="col-md-4">
									<label>Insurance Carrier:</label>
									<input type="text" class="form-control" name = "insurance_carrier[]" value="<?= $property_insurance_data[$insurance_key]['insurance_carrier'] ? $property_insurance_data[$insurance_key]['insurance_carrier'] : '';?>" />
								</div>
										
								<div class="col-md-4">
									<label>Insurance Expiration:</label>
									<input type="text"  id="insurance_expiration-<?php echo $insurance_key; ?>" class="form-control datepicker" name = "insurance_expiration[]"  placeholder = 'mm-dd-yyyy' value="<?= ($property_insurance_data[$insurance_key]['insurance_expiration'] !='0000-00-00') ? date('m-d-Y',strtotime($property_insurance_data[$insurance_key]['insurance_expiration'])) : '';?>" />
								</div>
										
								
							</div>
							
							<div class="row property_loan_row">
								<div class="col-md-4">
									<label>Contact Name:</label>
									<input type="text" class="form-control" name = "contact_name[]" value="<?php echo $property_insurance_data[$insurance_key]['contact_name'] ? $property_insurance_data[$insurance_key]['contact_name']:'';?>" />
								</div>
										
								<div class="col-md-4">
									<label>Contact Number:</label>
									<input type="text" class="form-control" name = "contact_number[]"   value="<?php echo $property_insurance_data[$insurance_key]['contact_number']? $property_insurance_data[$insurance_key]['contact_number'] : '' ;?>" />
								</div>
										
								<div class="col-md-4">
									<label>Contact Email:</label>
									<input type="text" class="form-control " name = "contact_email[]"  value="<?php echo $property_insurance_data[$insurance_key]['contact_email'] ? $property_insurance_data[$insurance_key]['contact_email'] : '' ;?>" /> 
								</div>
							</div>
							<!--
							<div class="row property_loan_row">
								<div class="col-md-12">
									<textarea class="form-control" name="mortgagee[]" placeholder="Mortgagee Clause"><?php echo $property_insurance_data[$insurance_key]['mortgagee'] ? $property_insurance_data[$insurance_key]['mortgagee']: ''; ?></textarea>
								</div>
							</div>
							-->
							<div class="row property_loan_row">
								<div class="col-md-4">
									<label>Names to Delete:</label>
									<input type="text" class="form-control" name = "names_delete[]" value="<?php echo $property_insurance_data[$insurance_key]['names_delete'] ? $property_insurance_data[$insurance_key]['names_delete']:'';?>" />
								</div>
										
							</div>
							<div class="row property_loan_row">
								<div class="col-md-8">
									<label>Insurance Notes:</label>
									<textarea name="c_notes[]" type="text" class="form-control" placeholder="Add Notes..."><?php echo $property_insurance_data[$insurance_key]['c_notes'] ? $property_insurance_data[$insurance_key]['c_notes'] : ''; ?></textarea>
								</div>
							
							<?php
							
							if($insurance_key == '1'){ ?>
								<div class="col-md-4 pull-right" style="text-align: right !important; margin-top:30px !important;">
								<label>&nbsp;</label>
								
								<?php if(!empty($all_input_datas_of_property_modal[$property_home->id]['property_insurance']['property_single_mortage'])){?>
								
								
									<!--<a href="<?php echo base_url();?>print_insurence<?php echo $loan_id;?>" class="btn blue borrower_save_button">Print</a>-->
									
								<?php }else{?>
								
									<!--<a  class="btn blue borrower_save_button" title="No Data in Property Insurance" onclick="print_insurencee(this);">Print</a>-->
									
									<?php } ?>
								</div>
								
								<?php	} ?>
						
							</div>
							</div>
							<?php	}
							?>
			
						<!----------End of Content modal------------------>
								
						</div>
					</div>
					<div class="modal-footer">
						<a class="btn blue" data-toggle="modal" href="#property_insurence_upload_doc">Upload</a>
						<button type="submit" value = "save" class="btn blue borrower_save_button" name="button">Save</button>
						<button type="button" class="btn default borrower_save_button" data-dismiss="modal">Close</button>
						
						
					</div>
			</form>
			</div>
								<!-- /.modal-content -->
		</div>
							<!-- /.modal-dialog -->
</div>


<!-------------------------- property_insurence_upload_doc start---------------->

<div class="modal fade" id="property_insurence_upload_doc" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url();?>Load_data/upload_property_insurance_document" enctype="multipart/form-data">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Property Insurence Document: <?php echo $property_addresss; ?> </h4>
				</div>
				<div class="modal-body">

					<input type="hidden" name="talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
					<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
					<input type="hidden" name="property_home_id" value="<?php echo $property_home->id;?>">

					<div class="row">
						<div class="col-md-12">
							<label>Selected documents:</label>
							<?php 
								$folder = 'property_insurance_doc/'.$modal_talimar_loan.'/'.$property_home->id.'/'; //FCPATH
								$base =$modal_talimar_loan;

									/*$hideName = array('.','..','.DS_Store');   
									$files = scandir($folder);*/
									$due_insurence_upload = $this->aws3->getBucketObjectList($folder);

									echo '<ul class = "document_lists" style="list-style-type: none;  margin-left:-40px;">';

									if($due_insurence_upload){
										foreach($due_insurence_upload as $filename) {
											$FileArr = explode('/', $filename);
											$FileNameGet = $FileArr[count($FileArr)-1];
											//if(!in_array($filename, $hideName)){
												echo '<li><a target="_blank" href ="'.aws_s3_document_url($filename).'">'.$FileNameGet.'</a></li>';	

											}
										}
									echo '</ul>';
							?>
									
						</div>
					</div>

					<div class="row"><br></div>

					<div class="row">
						<div class="col-md-6">
							<input type="file" name="upload_docc" class="form-control" required="required">
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="submit" class="btn blue" name="submit">Save</button>
					<button type="button" class="btn default" data-dismiss="modal">Close</button>	
				</div>
			</form>
		</div>
	</div>
</div>
<!------------------------- property_insurence_upload_doc end------------------>






<div class="modal fade" id="property-tax-<?php echo $property_home->id; ?>" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url().'form_property_taxes';?>">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<!--<h4 class="modal-title">Property Taxes: #<?php// echo $modal_talimar_loan; ?> </h4>-->
					<h4 class="modal-title">Property Taxes: <?php echo $property_addresss; ?> </h4>
				</div>
				<div class="modal-body page-property-taxes">
					<!----------- Hidden fields  ----------->
					<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
					<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
					<input type="hidden" name="property_home_id" value="<?php echo $property_home->id;?>">
					<!----------- End of hidden Fields  ---->
					
					<?php
					
					
					if($property_taxes['property_home_id'] == $property_home->id)
					{
						
							$annual_tax 			= $property_taxes['annual_tax'];
							$mello_roos 			= $property_taxes['mello_roos'];
							$delinquent 			= $property_taxes['delinquent'];
							$delinquent_amount 		= $property_taxes['delinquent_amount'];
							$post_delinquent 		= $property_taxes['post_delinquent'];
							$post_delinquent_amount = $property_taxes['post_delinquent_amount'];
							$tax_assessor_phone 	= $property_taxes['tax_assessor_phone'];
							
						
					} 
					?>
					
					<div class="row">
						<div class="col-md-6 ">
							<label>Annual Property Tax Payment:</label>
							<input type="text" class="form-control number_only amount_format" name="annual_tax" value="<?php echo '$'.number_format($annual_tax,2);?>">
						</div>
						<div class="col-md-6">
							<label>Mello Roos? :</label>
							<select type="text" class="form-control" name="mello_roos" >
								<?php
								foreach($yes_no_option as $key => $option)
								{
									$selected = '';
									if($key == $mello_roos)
									{
										$selected = 'selected';
									}
									?>
									<option value="<?php echo $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
									<?php
								}
								?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<label>Pre Close – Taxes Delinquent?:</label>
							<select type="text" class="form-control" name="delinquent" >
								<?php
								foreach($yes_no_option as $key => $option)
								{
									$selected = '';
									if($key == $delinquent)
									{
										$selected = 'selected';
									}
									?>
									<option value="<?php echo $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
									<?php
								}
								?>
							</select>
						</div>
						<div class="col-md-6">
							<label>If so, how much?:</label>
							<input type="text" class="form-control number_only amount_format" name="delinquent_amount" value="<?php echo '$'.number_format($delinquent_amount,2);?>">
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<label>Post Close – Taxes Delinquent?:</label>
							<select type="text" class="form-control" name="post_delinquent" >
								<?php
								foreach($yes_no_option as $key => $option)
								{
									$selected = '';
									if($key == $post_delinquent)
									{
										$selected = 'selected';
									}
									?>
									<option value="<?php echo $key; ?>" <?php echo $selected; ?> ><?php echo $option; ?></option>
									<?php
								}
								?>
							</select>
						</div>
						<div class="col-md-6">
							<label>If so, how much?:</label>
							<input type="text" class="form-control number_only amount_format" name="post_delinquent_amount" value="<?php echo '$'.number_format($post_delinquent_amount,2);?>">
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<label>Tax Assessor Phone:</label>
								<input type="text" class="form-control phone-format" name="tax_assessor_phone" value="<?php echo $tax_assessor_phone;?>"/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn blue">Save</button>
				</div>
			</form>	
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>


<?php /*new property_ appraisal document second start 18-06-2021 */ ?>     

<div class="modal fade bs-modal-lg" id="appraisal_document_<?php echo $property_home->id; ?>" tabindex="-1" role="large" aria-hidden="true">
<div class="modal-dialog">
     
     
     <div class="modal-content">
          
          <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
               <h4 class="modal-title">Appraisal Documents: <?php echo $property_addresss;?></h4>
          </div>
          
          <div class="modal-body">
                    <?php /*New Code*/ ?>

                    <div id="ct_Documentsadd" class="row">  
                         <form action = "<?php echo base_url('Load_data/property_appraisal_document');?>" name="appraisal_document_<?php echo $property_home->id; ?>"  id="appraisal_document_<?php echo $property_home->id; ?>" method="POST" enctype="multipart/form-data">  
                         <input type = "hidden" name = "document_id" id = "document_id" />
                                             
                         <div class="col-md-2">&nbsp;</div>
                         <div class="col-md-8">
                              <div class="ct_appraisal_document" style="display: none;">
                                   		<input type="hidden" name = "talimar_loan" value = "<?php echo $modal_talimar_loan; ?>">
                                        <input type="hidden" value="<?php echo (int)str_replace('/', '', $this->uri->segment(2));?>" name="loan_id">
                                        <input type = "hidden" name = "property_id" id = "property_id" value="<?php echo $property_home->id; ?>" />
                                        <div class="form-group">
                                             <label for="document_name">Document Name:</label>
                                             <input type="text" class="form-control" id="document_name" name="document_name" required>
                                        </div>
                                        <div class="form-group">
                                             <div id="ad">
                                                  <label>Select Document:</label>
                                                  <input type="file" id = "lender_upload_1" name ="appraisal_document_upload[]" class="select_images_file" accept="application/pdf" required>
                                             </div>
                                        </div>
                                        <button type="submit" id="uploadd" name="upload_file" class="btn blue">Save</button>
                                   
                              </div>
                         </div>
                         <div class="col-md-2">&nbsp;</div>
                         </form>
                    </div>
                    <div class="col-md-12 ct_Documentsadd_table">

                    <table  class="table table-responsive" id="appraisal_loan_doc_table" style="width: 100% !important;">
                         <thead>
                              <tr>
                                   <th>Action</th>
                                   <th>Name of Document</th>
                                   <th>Uploaded Date</th>
                                   <th>User</th>
                                   <th>View</th>
                              </tr>
                         </thead>
                         <tbody>
                              <?php
                              $loan_id_u = (int)str_replace('/', '', $this->uri->segment(2));
                              $property_id_u = $property_home->id;
                              $appraisal_document    = $this->User_model->query("SELECT * FROM property_appraisal_document WHERE property_id = '$property_id_u' AND loan_id = '$loan_id_u'");
                               $appraisal_document    = $appraisal_document->result();

                              if($appraisal_document){
                                   foreach($appraisal_document as $contactDocuments) {

                                   $FileNameGet = '';
                                   
                                   $UploadedDate = date("m-d-Y", strtotime($contactDocuments->created_at));
                                   
                                   $filename = $contactDocuments->document_path;
                                   $FileArr = explode('/', $filename);
                                   $FileNameGet = $FileArr[count($FileArr)-1];

                                   $file1 = explode('/', $FileNameGet);
                                   $file2 = explode('.', $file1[count($file1)-1]);

                                   $FileNameGet = $NameUploadedcalss = $file2[0];

                                   if($contactDocuments->document_name){
                                        $NameUploaded = $contactDocuments->document_name;
                                   }else{
                                        $NameUploaded = $FileNameGet;
                                   }


                                   $up_user_id = $contactDocuments->user_id;
                                   $UpUserDate = $this->User_model->query("SELECT * FROM user WHERE id = '$up_user_id'")->row();
                                   if($UpUserDate){
                                        $UploadedUser = $UpUserDate->fname.' '.$UpUserDate->lname;
                                   }
                              ?>
                              <tr attrremove="<?php echo $contactDocuments->id; ?>">
                                   <td>
                                        <a><i id="<?php echo $contactDocuments->id; ?>" onclick="appraisal_document_del(this);" title = "Delete File" class = "fa fa-trash"  aria-hidden="true"></i></a>
                                   </td>
                                   <td>
                                        <?php echo $NameUploaded; ?>
                                   </td>
                                   <td><?php echo $UploadedDate; ?></td>
                                   <td><?php echo $UploadedUser; ?></td>
                                   <td>
                                        <a href="<?php echo aws_s3_document_url($contactDocuments->document_path); ?>" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                   </td>
                              </tr>
                              <?php
                                   }
                              }
                              ?>

                         </tbody>
                    </table>
               </div>
               <?php /* close code */ ?>
          </div>
     
          <div class="modal-footer">
               <button type="button" id="ct_appraisal_documentadd" class="btn btn-primary">Add Document</button>
               <button type="button" class="btn default" data-dismiss="modal">Close</button>
               
          </div>
     </div>
     <!-- /.modal-content -->

</div>
<!-- /.modal-dialog -->
</div>

<?php /*new property_ appraisal document second start 18-06-2021 */ ?>  


<div class="modal fade bs-modal-lg" id="image-folder-<?php echo $property_home->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<!--<h4 class="modal-title">Images Folders : #<?php// echo $modal_talimar_loan; ?></h4>-->
				<h4 class="modal-title">Images Folders : <?php echo $property_addresss; ?></h4>
			</div>
			<form method="post" action="<?php echo base_url().'form_images_folder';?>">
			<div class="modal-body">
			<!----------- Hidden fields  ----------->
			<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
			<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
			<input type="hidden" name="property_home_id" value="<?php echo $property_home->id;?>">
			<!----------- End of hidden Fields ---->
				<div class="row" id="add-image-folders-<?php echo $property_home->id; ?>">
				<?php
				if(isset($property_folders[$property_home->id]))
				{ 
					foreach($property_folders[$property_home->id] as $row)
					{

						
						if($row->title == 'Site Photos'){
							$title = 'Site Visit Photos';
							$title = $row->title;
						
						
						}else{
						
							$title = $row->title;
						}
						
						?>
						<?php if($row->title != 'Site Photos'){
							
							?>
						<div class="col-sm-3">
							<div class="folder-main-images">
								<a data-toggle="modal" href="<?php echo '#property-folder-'.$row->id.'-images-'.$property_home->id; ?>"><img src="<?php echo base_url().'assets/images/folder.png'; ?>"></a>
							</div> 
							<div class="folder-title">
								<input type="hidden" value="<?php echo $row->id; ?>" name="id[]">
								<input type="text" name="title[]" value="<?php echo $title;?>">
							</div>
						</div>
						<?php
						
						
						}
						
						
					}
				} ?>
				
					
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn blue" type="button" onclick="add_images_folder(<?php echo $property_home->id; ?>)">Add Folder</button>
				<button type="submit" class="btn blue">Save</button>
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
			</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php
if(isset($property_folders[$property_home->id])){
	foreach($property_folders[$property_home->id] as $seq => $row_folder )
	{
		if($row_folder->title == 'Site Photos'){
			$title1 = 'Site Visit Photos';
		}else{
			$title1 = $row_folder->title;
		}
		$folder_id = $row_folder->id;
?>
		<div class="modal fade bs-modal-lg" id="<?php echo 'property-folder-'.$row_folder->id.'-images-'.$property_home->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Property Images > <?php echo $title1; ?> </h4>
					</div>
					 
					<form action="<?php echo base_url().'form_upload_property_images';?>" method="post" enctype="multipart/form-data">
						<!----------- Hidden fields  ----------->
						<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
						<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
						<input type="hidden" name="property_home_id" value="<?php echo $property_home->id;?>">
						<input type="hidden" name="folder_id" value="<?php echo $folder_id;?>">
						<!----------- End of hidden Fields  ---->
						<div class="modal-body" >
							<div class="row property_imgs_<?php echo $folder_id;?> " id="<?php echo 'property-images-'.$folder_id.'-section-'.$property_home->id; ?>">
								<?php
								if(isset($loan_property_images[$property_home->id][$folder_id]))
								{
									foreach($loan_property_images[$property_home->id][$folder_id] as $keyy => $row_imgs)
									{
																			
										// echo '<pre>';
										// print_r($row);
										// echo '</pre>';
		
										$keyy++;
										if($keyy == 1 && $row_folder->title == 'Underwriting'){
											
											$highlight_color = 'style = "border:1px solid red" ';
											$add_text = 'Featured Image';
											
										}else{
											
											$highlight_color = 'style = "border:1px solid white" ';
											$add_text = '';
											
										}
										
									$fetch_property_home = $this->User_model->select_where('property_folders',array('id'=>$row_folder->id));
								  if($fetch_property_home->num_rows() > 0)
								  {

								    $fetch_property_home=$fetch_property_home->result();
									$new_title=$fetch_property_home[0]->title;
								  }
								  
								 
									if($row_folder->title == 'Underwriting' || $row_folder->title == 'Site Photos' || $row_folder->title == 'Site Visit Photos' || $row_folder->title == 'Site Map' || $row_folder->title == $new_title){
												
									?>
										<h4 style="margin-left:40px !important;"><?php echo $add_text; ?></h4>
										<div class="col-sm-3  img-div_<?php echo $keyy;?>" >
											<div class="property-image-inner-main" >
											
												<div class="property-image-inner-image" <?php echo $highlight_color;?>>
												
													<a onclick="click_on_image_button(this)">
													
													<?php
													$property_image_path = aws_s3_document_url('all_property_images/'.$row_imgs->talimar_loan.'/'.$row_imgs->property_home_id.'/'.$folder_id.'/'.$row_imgs->image_name); //FCPATH

													/*if(file_exists($property_image_path))
													{
														$property_image_path = base_url().'all_property_images/'.$row_imgs->talimar_loan.'/'.$row_imgs->property_home_id.'/'.$folder_id.'/'.$row_imgs->image_name;
													}
													else
													{
														$property_image_path = base_url().'property_images/no_image.png';

													}*/

													$errproperty_image_path = aws_s3_document_url('property_images/no_image.png');

													// echo $property_image_path;
													// echo $property_image_path;
													
													
													?>
													
													<img src="<?php echo $property_image_path; ?>" class="property-image" onerror="this.onerror=null;this.src='<?php echo $errproperty_image_path; ?>';">
													</a>
													<input type="file" name="file[]" class="image_upload_input" >
													<input type="hidden" name="id[]" value="<?php echo $row_imgs->id; ?>" >
												</div>
												<div class="property-image-inner-date">
													<input name="date[]" value="<?php echo $row_imgs->date; ?>" class="form-control datepicker" Placeholder="Date" style="color:#000000;">
												</div>
												<div class="property-image-inner-description-main">
													<div class="property-image-description">
													
													<?php if($row_imgs->description == 'Street Level Map' || $row_imgs->description == 'Area Map' || $row_imgs->description == 'Front of Residence' || $row_imgs->description == 'Kitchen' || $row_imgs->description == 'Living Room' || $row_imgs->description == 'Bedroom' || $row_imgs->description == 'Bathroom' || $row_imgs->description == 'Rear of Residence'){ ?>
													
														<input name="description[]" value="" class="form-control" Placeholder="Description" style="color:#000000;" >
														
													<?php }else{?>
													
														<input name="description[]" value="<?php echo $row_imgs->description; ?>" class="form-control" Placeholder="Photo Description" style="color:#000000;">
													
													<?php } ?>
													</div>
													
													<div class="property-image-delete">
														<a data-id = "<?php echo $keyy;?>" onclick="delete_image(this)"><i class="fa fa-trash" aria-hidden="true"></i></a>
													</div>
												</div>
											</div>
										</div>

										<?php //if($row_folder->title == 'Site Map' && $keyy == 4){ break; }  ?>
										
									<?php }  ?>
									
									<?php //if($row_folder->title == 'Site Map'){ 
									
										?>
									
										<!-- <h4 style="margin-left:40px !important;"><?php echo $add_text; ?></h4>
										<div class="col-sm-3  img-div_<?php echo $keyy;?>" >
											<div class="property-image-inner-main" >
											
												<div class="property-image-inner-image" <?php echo $highlight_color;?>>
												
													<a onclick="click_on_image_button(this)">
													
													<?php
													$property_image_path = FCPATH.'all_property_images/'.$row->talimar_loan.'/'.$row->property_home_id.'/'.$folder_id.'/'.$row->image_name;
													if(file_exists($property_image_path))
													{
														$property_image_path = base_url().'all_property_images/'.$row->talimar_loan.'/'.$row->property_home_id.'/'.$folder_id.'/'.$row->image_name;
													}
													else
													{
														$property_image_path = base_url().'property_images/no_image.png';
													}
													// echo $property_image_path;
													
													
													?>
													
													<img src="<?php echo $property_image_path; ?>" class="property-image">
													</a>
													<input type="file" name="file[]" class="image_upload_input" >
													<input type="hidden" name="id[]" value="<?php echo $row->id; ?>" >
												</div>
												<div class="property-image-inner-date">
													<input name="date[]" value="<?php echo $row->date; ?>" class="form-control datepicker" Placeholder="Date" style="color:#000000;">
												</div>
												<div class="property-image-inner-description-main">
													<div class="property-image-description">
													<?php if($row->description == 'Street Level Map'){
														?>
														<input name="description[]" value="Street Level Image" class="form-control" Placeholder="Description" style="color:#000000;" readonly>
														
													<?php }elseif($row->description == 'Area Map'){?>
													
														<input name="description[]" value="Area Level Image" class="form-control" Placeholder="Description" style="color:#000000;" readonly>
													
													<?php }else{?>
													
													
													
														<input name="description[]" value="<?php echo $row->description; ?>" class="form-control" Placeholder="Photo Description" style="color:#000000;">
													
													<?php } ?>
													</div>
													
													<div class="property-image-delete">
														<a data-id = "<?php echo $keyy;?>" onclick="delete_image(this)"><i class="fa fa-trash" aria-hidden="true"></i></a>
													</div>
												</div>
											</div>
										</div> -->
									
									<?php //if($keyy == 2){ break; } } ?>
									
								<?php
									}
								}
								?>
								<!------- Initially Add 6 image file ------------->
								<?php
									// if($row_folder->title == 'Underwriting'){
									if($seq == 0){
									
									$initial_img = 4;
									$total_img 	 = count($loan_property_images[$property_home->id][$folder_id]);	
										
									$needed_img = $initial_img - $total_img;
									
									
									$counter = $keyy;
									
									// echo 'counter is: '.$counter;
									
									
									for($i=1;$i<=$needed_img;$i++){
										$counter++;
										if($counter == 1 || $counter == 3 || $counter == 4 || $counter == 5 || $counter == 6){
											$highlight_color = 'style = "border:1px solid red" ';
											
										}else{
											$highlight_color = 'style = "border:1px solid white" ';
										}
								?>
								
								<!------- Image  STARTS ------------>
								<div class="col-sm-3  img-div_<?php echo $counter;?>" >
									<div class="property-image-inner-main" >
										<div class="property-image-inner-image" <?php echo $highlight_color;?>>
											<a onclick="click_on_image_button(this)">
											<img src="<?php echo base_url(); ?>property_images/no_image.png" class="property-image">
											</a>
											<input type="file" name="file[]" class="image_upload_input" >
											<input type="hidden" name="id[]" value="new" >
										</div>
										<div class="property-image-inner-date">
											<input name="date[]" value="" class="form-control datepicker" Placeholder="Date" style="color:#000000;">
										</div>
										<div class="property-image-inner-description-main">
											<div class="property-image-description">
												<input name="description[]" value="" class="form-control" Placeholder="Photo Description" style="color:#000000;">
											</div>
											<div class="property-image-delete">
												<a data-id = "<?php echo $counter;?>" onclick="delete_image(this)"><i class="fa fa-trash" aria-hidden="true"></i></a>
											</div>
										</div>
								
									</div>
								</div>
								
								<!------- Image  ENDS ------------>
								
									<?php } }


										?>
								<!------- Image  STARTS ------------>
				<!-- 				<div class="col-sm-3  img-div_1">
									<div class="property-image-inner-main">
										<div class="property-image-inner-image">
											<a onclick="click_on_image_button(this)">
											<img src="<?php echo base_url(); ?>property_images/no_image.png" class="property-image">
											</a>
											<input type="file" name="file[]" class="image_upload_input" >
											<input type="hidden" name="id[]" value="new" >
										</div>
										<div class="property-image-inner-date">
											<input name="date[]" value="" class="form-control datepicker" Placeholder="Date">
										</div>
										<div class="property-image-inner-description-main">
											<div class="property-image-description">
												<input name="description[]" value="" class="form-control" Placeholder="Description">
											</div>
											<div class="property-image-delete">
												<a data-id = "1" onclick="delete_image(this)"><i class="fa fa-trash" aria-hidden="true"></i></a>
											</div>
										</div>
								
									</div>
								</div> -->
								
								<!------- Image  ENDS ------------>
								
							</div>
						</div>
					
					
						<div class="modal-footer">
							<button type="button" class="btn blue" onclick="add_image_section(<?php echo $property_home->id; ?>,<?php echo $folder_id; ?>)">Add</button>
							<button type="submit" class="btn blue">Save</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
<!-- /.modal -->
<?php

	}
}
/*        End of Property Images Loop */
?>

<?php

		}
	}
	?>
	





<!-- Start modal for ecum question -->
<a class="" data-toggle="modal" href="#ecumbrance_questions" id="modal-ecum-question"></a>
<div class="modal fade bs-modal-lg" id="ecumbrance_questions" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<!--<h4 class="modal-title">Encumbrance Data :#<?php //echo $modal_talimar_loan; ?>  </h4>-->
				<h4 class="modal-title">Encumbrance Data: <?php echo $property_addresss; ?>  </h4>
			</div>
			
			<form id="form-ecumbrance-data" method="POST">
			<!-----------Hidden fields--------------------------------------->
			<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
			<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
			<!-----------End of hidden Fields-------------------------------->
			<div class="modal-body" id="ecumbrance_questions_load_data">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn blue" onclick="form_sumbit_ecumbrance_data()">Save</button>
				<a class="btn default" data-toggle="modal" href="#edit_ecumbrance">Close</a>
			</div>
				<div class="modal fade" id="edit_ecumbrance" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content" style="top:750px !important;">
							
							<div class="modal-body">
								<div class="row" style="padding-top:8px;">
							
									<div class="col-md-8" style="padding-top:6px;"><p>Do you want to save the changes?</p></div>
									<div class="col-md-2">
										<!--<a style="margin-left:38px;" class="btn blue borrower_save_button" href="javascript:void(0);" onclick = "save_info('edit_contact')">Yes</a>-->
										
										<button type="button" class="btn blue" onclick="form_sumbit_ecumbrance_data()" style="margin-left:35px;">Yes</button>
										
									</div>
									<div class="col-md-2">
										<a class="btn default borrower_save_button" href = "<?php echo base_url().'load_data'.$loan_id;?>" >No</a>
									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Start modal for ecum request for notice -->
<a class="" data-toggle="modal" href="#ecumbrance_request_notice" id="modal-ecum-request_notice"></a>
<div class="modal fade bs-modal-lg" id="ecumbrance_request_notice" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<!--<h4 class="modal-title">Encumbrance Data :#<?php// echo $modal_talimar_loan; ?>  <span id="ecum-load-lien-holder-name"></span></h4>-->
				<h4 class="modal-title">Encumbrance Data: <?php echo $property_addresss; ?>  <span id="ecum-load-lien-holder-name"></span></h4>
			</div>
			<form id="form-ecumbrance-request_notice" method="POST">
			<!-----------Hidden fields--------------------------------------->
			<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
			<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
			<!-----------End of hidden Fields-------------------------------->
			<div class="modal-body" id="ecumbrance_request_notice_load_data">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal" id="ecum-request_notice-close-modal">Close</button>
				<button type="button" class="btn blue" onclick="form_sumbit_ecumbrance_request_notice()">Save</button>
			</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!------------------------------ Start Hidden Tables  -------------------------------->
<table class="ecum_table" class="table table-bordered table-striped table-condensed flip-content th_text_align_center"  style="display:none" id="ecum_hidden_table">
	<tr>
		<td class="check_row"><a class="ecum_remove"><i class="fa fa-trash" aria-hidden="true"></i>
		<input type="hidden" value="" name="id">
		</a></td>
		
		<td><input type="text" name="lien_holder[]" value="" class="form-control" /></td>
		<!--
		<td>
			<select name="ecum_loan_type[]">
			<?php
			foreach($encumbrances_loan_type as $key => $option)
			{
				?>
				<option value="<?php echo $key; ?>"  ><?php echo $option; ?></option>
				<?php
			}
			?>
			</select>
		</td>
		<td class = "lien_center">
			<a onclick="alert('Please first save the data')"><i class="fa fa-pencil" aria-hidden="true"></i></a>
		</td >
			
		<td>
			<select name="existing_lien[]" >
					<?php
					foreach($yes_no_option as $key => $option)
					{
						?>
						<option value="<?php echo $key; ?>" ><?php echo  $option; ?></option>
						<?php
					}
					?>
			</select>
		</td>
		<td>
			<select name="will_it_remain[]"  >
			
					<?php
					foreach($yes_no_option as $key => $option)
					{
						?>
						<option value="<?php echo $key; ?>"><?php echo  $option; ?></option>
						<?php
					}
					?>
			</select>
		</td>
		
		-->
		
		
		
		
		<td><input type="text" name="original_balance[]" value=""  class="form-control  number_only" onchange="autocalculate_encumbrance_left_data(this)"/></td>
		<td><input type="text" name="current_balance[]" value=""  class="form-control  number_only" onchange="autocalculate_encumbrance_left_data(this)"/></td>
		<td>
			<input type="hidden" name="ecum_id[]" value="new">
			<select type="text" name="position[]" class="form-control">
			<?php
			foreach($position_option as $key => $opt)
			{
				?>
				<option value="<?php echo $key;?>"  ><?php echo $opt; ?></option>
				<?php
			}
			?>
			</select>
		</td>
		<!--
		<td><input type="text" name="intrest_rate[]" value=""  class="number_only rate_format" onchange="autocalculate_encumbrance_left_data(this)"/></td>
		-->
		<td><input type="text" name="monthly_payment[]" value=""  class="form-control number_only" onchange="autocalculate_encumbrance_left_data(this)" /></td>
		<td class = "lien_center">
			<a onclick="alert('Please first save the data')"><i class="fa fa-pencil" aria-hidden="true"></i></a>
		</td>
		<!--
		<td><select type="text" name="ballon_payment[]" onchange="autocalculate_encumbrance_left_data(this)">
			<option value='0' >No</option>
			<option value='1' >Yes</option>
		</select>
		</td>

		<td>
		
		
		<input type="text"  name="ballon_payment_b[]" value="" class="number_only" onchange="autocalculate_encumbrance_left_data(this)" />
		
		</td>
		<td>
			<select name="rate_type[]">
				<?php
				foreach($loan_intrest_type_option as $key=>$option)
				{
					?>
						<option value="<?php echo $key; ?>"><?php echo $option; ?></option>
					<?php
				}
				?>
				
		</td>
		<td>
			<input type="text" name="max_rate[]" class="number_only" onchange="autocalculate_encumbrance_left_data(this)">
		</td>
		
		-->
	</tr>
</table>

<table  style="display:none;" id="cashflow_monthly_income_hidden">
<tr>
	<td>
		<a onclick="delete_monthly_income(this)"><i class="fa fa-trash" aria-hidden="true"></i></a>&nbsp;
		<a><i class="fa fa-edit" aria-hidden="true"></i></td>
	<td>
		<input  type="hidden" name="id_income[]" value="new">
		<input class="form-control" type="text" name="items_income[]" >
	</td>
	<td><input class="form-control number_only" type="text" name="amount_income[]" onchange="monthly_income_amounts(this)"></td>
</tr>
</table>

<table id="cashflow_monthly_expense_hidden" style="display:none;">
<tr>
	<td><a onclick="delete_monthly_expense(this)"><i class="fa fa-trash" aria-hidden="true"></i></td>
	<td>
		<input  type="hidden" name="id_expense[]" value="new">
		<input class="form-control" type="text" name="items_expense[]" >
	</td>
	<td><input class="form-control number_only" type="text" name="amount_expense[]" onchange="monthly_expense_amounts(this)"></td>
</tr>
</table>

<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center"  style="display:none" id="comparable_sale_hidden">
<tr id="new_tr">
		<td>
		<input type="hidden" name="comparable_id[]" value="new">
		<input type="text" name="property_address[]">
		</td>
		<!--<td><input type="text" name="prop_link[]" id="prop_link" ></td>-->
		<td><input type="text" name="date_sold[]" id="date_sold" ></td>
		<td><input type="text" name="sq_feet[]" id="sq_feet" onchange="comparable_cal_per_sq_feet(this)"></td>
		<td><input type="text" name="bedrooms[]"  class="number_only"></td>
		<td><input type="text" name="bathrooms[]"  class="number_only"></td>
		<td><input type="text" name="lot_size[]" class="number_only"></td>
		<td><input type="text" name="year_built[]" class="number_only"></td>
		<td><input type="text" name="sale_price[]" id="sale_price" onchange="comparable_cal_per_sq_feet(this)" class="number_only"></td>
		<td><input type="text" name="per_sq_feet[]" id="per_sq_feet"  class="number_only"></td>
<td></td>
		<td><a class="comparable_sale_remove" id="new" onclick="comparable_sale(this);"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
	</tr>
</table>
<!------------------------------ End Hidden Tables  -------------------------------->

<!-- Start Hidden Form -->
<form method="POST" action="<?php echo base_url().'property_hidden_form';?>" id="property_hidden_form">
<!----------- Hidden fields  ----------->
	<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
	<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
	<input type="hidden" name="property_home_id" value="">
	<input type="hidden" name="table" value="">
<!----------- End of hidden Fields  ---->
</form>
<!-- End Of Start Hidden Form -->


<a id="ecum_edit_hidden_link" onclick=""></a>


<style type="text/css">
	
	.addspaceabove{

		margin-top: 10px;
	}
</style>
<!--------------------------- UPdate MONTHLY INCOME TABLE --------------->

<div class="modal fade" id="monyhly_incomeModal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form method="post" action="<?php echo base_url();?>Property_data/update_monthly_income_fields">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><b>Update Information</b></h4>
      </div>
      <div class="modal-body">

      	<input type="hidden" name="rowid" value="">
      	<input type="hidden" name="loanid" value="">
        
      	<div class="row addspaceabove">
      		<div class="col-md-6">
      			<label>Unit Type:</label>
      			<select class="form-control" name="unit_type">
      				<?php foreach($cashflow_edit_options as $key => $row){ ?>
      					<option value="<?php echo $key;?>"><?php echo $row;?></option>
      				<?php } ?>
      			</select>
      		</div>
      		<div class="col-md-6">
      			<label>Unit #:</label>
      			<input type="text" name="items" class="form-control" value="" required="required">
      		</div>
      	</div>

      	<div class="row addspaceabove">
      		<div class="col-md-6">
      			<label>Size (SF):</label>
      			<input type="text" name="size" class="form-control" value="">
      		</div>

      		<div class="col-md-6">
      			<label># of Bedrooms:</label>
      			<input type="text" name="bedroom" class="form-control number_only" value="">
      		</div>
      	</div>

      	<div class="row addspaceabove">
      		<div class="col-md-6">
      			<label># of Bathrooms:</label>
      			<input type="text" name="bathroom" class="form-control number_only" value="">
      		</div>

      		<div class="col-md-6">
      			<label>Monthly Rent:</label>
      			<input type="text" name="amount" class="form-control" value=""  required="required">
      		</div>
      	</div>

      	<div class="row addspaceabove">
      		<div class="col-md-12">
      			<label>Notes:</label>
      			<textarea type="text" class="form-control" name="notes" rows="3" placeholder="Enter your note here..."></textarea>
      		</div>
      	</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Update</button>
      </div>
  	</form>
    </div>
  </div>
</div>

<!--------------------------- UPdate MONTHLY INCOME TABLE --------------->


<style>
.chosen-container.chosen-container-single {
    width: 180px !important;
}
.chosen-drop {
    width: 180px !important;
    margin-left: 0px !important;
}
</style>

<script type="text/javascript">

//$(".chosen").chosen();


$(document).ready(function(){

	$('div.selectbox_class input[name="check_requ[]"]').each(function(){
	
		      disabled_selectbox(this);
	});

	
 });



function addmoreproperty(that){
	
	var talimar_loan = '<?php echo $modal_talimar_loan; ?>';
	var loan_id = '<?php echo $loan_id; ?>';

	$.ajax({
			type : 'post',
			url  : '<?php echo base_url()."Property_data/add_more_property";?>',
			data : {'talimar_loan':talimar_loan, 'loan_id':loan_id},
			success : function(response){

				if(response == 1){
					alert('New property row added successfully!');
					window.location.reload();
				}
			}
		});
}
					

function disabled_selectbox(that){


if($(that).is(':checked') == true){

 

$(that).parents('div.selectbox_class').find('input[name="policy_number[]"]').css({"pointer-events":"none", "background-color": "#eeeeee", "cursor": "not-allowed"}).val('');
$(that).parents('div.selectbox_class').find('input[name="annual_insurance_payment[]"]').css({"pointer-events":"none", "background-color": "#eeeeee", "cursor": "not-allowed"}).val('');
$(that).parents('div.selectbox_class').find('input[name="insurance_carrier[]"]').css({"pointer-events":"none", "background-color": "#eeeeee", "cursor": "not-allowed"}).val('');
$(that).parents('div.selectbox_class').find('input[name="insurance_expiration[]"]').css({"pointer-events":"none", "background-color": "#eeeeee", "cursor": "not-allowed"}).val('');
$(that).parents('div.selectbox_class').find('input[name="contact_name[]"]').css({"pointer-events":"none", "background-color": "#eeeeee", "cursor": "not-allowed"}).val('');
$(that).parents('div.selectbox_class').find('input[name="contact_number[]"]').css({"pointer-events":"none", "background-color": "#eeeeee", "cursor": "not-allowed"}).val('');
$(that).parents('div.selectbox_class').find('input[name="names_delete[]"]').css({"pointer-events":"none", "background-color": "#eeeeee", "cursor": "not-allowed"}).val('');
$(that).parents('div.selectbox_class').find('input[name="contact_email[]"]').css({"pointer-events":"none", "background-color": "#eeeeee", "cursor": "not-allowed"}).val('');
$(that).parents('div.selectbox_class').find('textarea[name="c_notes[]"]').css({"pointer-events":"none", "background-color": "#eeeeee", "cursor": "not-allowed"}).val('');
$(that).parents('div.selectbox_class').find('select[name="payment_close[]"]').css({"pointer-events":"none", "background-color": "#eeeeee", "cursor": "not-allowed"}).val('');
//$(that).parents('div.selectbox_class').find('input[name="check_request_upadted[]"]').css({"pointer-events":"none", "background-color": "#eeeeee", "cursor": "not-allowed"}).prop('checked',false);
}else{

$(that).parents('div.selectbox_class').find('input[name="policy_number[]"]').css({"pointer-events":"", "background-color": "", "cursor": ""});
$(that).parents('div.selectbox_class').find('input[name="annual_insurance_payment[]"]').css({"pointer-events":"", "background-color": "", "cursor": ""});
$(that).parents('div.selectbox_class').find('input[name="insurance_carrier[]"]').css({"pointer-events":"", "background-color": "", "cursor": ""});
$(that).parents('div.selectbox_class').find('input[name="insurance_expiration[]"]').css({"pointer-events":"", "background-color": "", "cursor": ""});
$(that).parents('div.selectbox_class').find('input[name="contact_name[]"]').css({"pointer-events":"", "background-color": "", "cursor": ""});
$(that).parents('div.selectbox_class').find('input[name="contact_number[]"]').css({"pointer-events":"", "background-color": "", "cursor": ""});
$(that).parents('div.selectbox_class').find('input[name="contact_email[]"]').css({"pointer-events":"", "background-color": "", "cursor": ""});
$(that).parents('div.selectbox_class').find('textarea[name="c_notes[]"]').css({"pointer-events":"", "background-color": "", "cursor": ""});
$(that).parents('div.selectbox_class').find('input[name="names_delete[]"]').css({"pointer-events":"", "background-color": "", "cursor": ""});
$(that).parents('div.selectbox_class').find('select[name="payment_close[]"]').css({"pointer-events":"", "background-color": "", "cursor": ""});
//$(that).parents('div.selectbox_class').find('input[name="check_request_upadted[]"]').css({"pointer-events":"", "background-color": "", "cursor": ""}).prop('checked',false);


 }
}



// function hide_appraiser_information(that,property_home_id){

// 	if(typeof(that) == 'object')
// 	{
// 		var value = that.value;
// 	}
// 	else
// 	{
// 		var value = that;
// 	}
// 	alert(value);
// 	if(value == '0'){

// 		$('select.chosen97_'+property_home_id).val('');
// 		$('input.company_'+property_home_id).val('');
// 		$('input.company_'+property_home_id)..attr('disabled',true);

// 	}else{

// 		$('select.chosen97_'+property_home_id).attr('disabled',false);
// 		$('input.company_'+property_home_id).attr('disabled',false);
// 	}
// 	//alert(property_home_id);
// }



function change_rehab_property(that,property_home_id){
	
	if(typeof(that) == 'object')
	{
		value = that.value;
	}
	else
	{
		value = that;
	}
	

	if(value == '2'){
		
		$('input#constructions_budget_'+property_home_id).val('$0').attr('disabled',true);
		$('textarea#desc_property_'+property_home_id).val('Not Applicable').attr('disabled',true);
		$('input#contractor_name_'+property_home_id).val('Not Applicable').attr('disabled',true);
		$('input#contractor_license_'+property_home_id).val('Not Applicable').attr('disabled',true);
		$('select#contractor_party_'+property_home_id+' option[value="3"]').attr('selected','selected');
		$('select#contractor_party_'+property_home_id).attr('disabled',true);

	}else{


		if($('input#constructions_budgetttt_'+property_home_id).val() !=''){

	 		$('input#constructions_budget_'+property_home_id).val('$'+$('input#constructions_budgetttt_'+property_home_id).val()).attr('disabled',false);	
		
		}
		else{

			 $('input#constructions_budget_'+property_home_id).val('').attr('disabled',false);	
		}
	
		$('textarea#desc_property_'+property_home_id).attr('disabled',false);
		$('input#contractor_name_'+property_home_id).attr('disabled',false);
		$('input#contractor_license_'+property_home_id).attr('disabled',false);
		$('select#contractor_party_'+property_home_id).attr('disabled',false);
	}
}

function change_constractor_party(that,property_home_id){
	
	if(typeof(that) == 'object')
	{
		value = that.value;
	}
	else
	{
		value = that;
	}
	
	
	if(value == '2'){
		
		$('input#contractor_name_'+property_home_id).attr('disabled',true);
		$('input#contractor_license_'+property_home_id).attr('disabled',true);
		
		$('input#contractor_name_'+property_home_id).val('Not Applicable');
		$('input#contractor_license_'+property_home_id).val('Not Applicable');

	}else{

		$('input#contractor_name_'+property_home_id).attr('disabled',false);
		$('input#contractor_license_'+property_home_id).attr('disabled',false);

	}
}

$(document).ready(function(){
  /***phone number format***/
  $(".phone-format").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
    }
    var curchr = this.value.length;
    var curval = $(this).val();
    if (curchr == 3 && curval.indexOf("(") <= -1) {
      $(this).val("(" + curval + ")" + "-");
    } else if (curchr == 4 && curval.indexOf("(") > -1) {
      $(this).val(curval + ")-");
    } else if (curchr == 5 && curval.indexOf(")") > -1) {
      $(this).val(curval + "-");
    } else if (curchr == 9) {
      $(this).val(curval + "-");
      $(this).attr('maxlength', '14');
    }
  });
});



$('input#insurance_expiration-1').datepicker({ dateFormat: 'mm-dd-yy' });
$('input#insurance_expiration-2').datepicker({ dateFormat: 'mm-dd-yy' });
$('input#insurance_expiration-3').datepicker({ dateFormat: 'mm-dd-yy' });
var aa = '<?php echo $property_home->id;?>';
	
// $('#valuation_date_'+aa).datepicker({ dateFormat: 'mm-dd-yy'});
// $('.date_'+id).datepicker({ dateFormat: 'mm-dd-yy'});

	 // $('#valuation_date_'+aa).datepicker({ dateFormat: 'mm-dd-yy'});
	
	 $(".datepicker").datepicker({ dateFormat: 'mm-dd-yy'});
	
 
function edit_monthly_income_values(that,table_id){

	$('#monyhly_incomeModal select[name="unit_type"] option').attr('selected',false);

	$.ajax({
			type : 'post',
			url  : '<?php echo base_url()."Property_data/update_cashflow_income_data";?>',
			data : {'table_id':table_id},
			success : function(response){

				if(response == 1){

					alert('Error: Data not found!');
					window.location.reload();

				}else{

					var data = JSON.parse(response);
					$('#monyhly_incomeModal input[name="rowid"]').val(data[0].id);
					$('#monyhly_incomeModal input[name="loanid"]').val(data[0].loan_id);
					$('#monyhly_incomeModal input[name="items"]').val(data[0].items);
					$('#monyhly_incomeModal input[name="amount"]').val(data[0].amount);
					$('#monyhly_incomeModal input[name="size"]').val(data[0].size);
					$('#monyhly_incomeModal input[name="bedroom"]').val(data[0].bedroom);
					$('#monyhly_incomeModal input[name="bathroom"]').val(data[0].bathroom);
					$('#monyhly_incomeModal textarea[name="notes"]').val(data[0].notes);

					$('#monyhly_incomeModal select[name="unit_type"] option[value="'+data[0].unit_type+'"]').attr('selected',true);

					$('#monyhly_incomeModal').modal('show');
				}
			}
	})
}


function delete_monthly_income(that){
	var id = $(that).parent().parent('tr').find('input[name="id_income[]"]').val();
	if(id == 'new')
	{
		$(that).parent().parent('tr').remove();
	}
	else
	{
		if(confirm('Are you sure to delete ?') === true)
		{
			$.ajax({
				type  	: 'POST',
				url		: '<?php echo base_url()."delete_monthly_income_row"; ?>',
				data 	: { 'id':id },
				success : function(result)
				{
					$(that).parent().parent('tr').remove();
				}
			})
		}
		else
		{
			return false;
		}
	}
}

function delete_monthly_expense(that){
	var id = $(that).parent().parent('tr').find('input[name="id_expense[]"]').val();
	if(id == 'new')
	{
		$(that).parent().parent('tr').remove();
	}
	else
	{
		if(confirm('Are you sure to delete ?') === true)
		{
			$.ajax({
				type  	: 'POST',
				url		: '<?php echo base_url()."delete_monthly_expense_row"; ?>',
				data 	: { 'id':id },
				success : function(result)
				{
					$(that).parent().parent('tr').remove();
				}
			})
		}
		else
		{
			return false;
		}
	}
}

function monthly_income_amounts(that)
{
	if(typeof(that) == 'object')
	{
		var table = $(that).parent().parent().parent().parent('table').prop('id');
		var finder					= table.split("-");
		var property_home_id		= finder.slice(1,2);
	}
	else if(typeof(that) == 'number')
	{
		var table = 'cashflow_monthly_income-'+that;
		var property_home_id		= that;
	}
	var sum = 0;
	var number = 0;
	$('table#'+table+' tbody tr').each(function(){
		var amount = parseFloat(replace_dollar($(this).find('input[name="amount_income[]"]').val()));
		if(amount)
		{
			number++;
			$(this).find('input[name="amount_income[]"]').val('$'+number_format(amount));
			sum += amount;
		}
	});
	$('table#'+table+' tfoot input#total_monthly_income').val('$'+number_format(sum));
	// $('table#'+table+' tfoot input#count_monthly_income').val(number);
	
	var total_income = parseFloat(replace_dollar($('table#cashflow_monthly_income-'+property_home_id+' tfoot input#total_monthly_income').val()));
	var total_expense = parseFloat(replace_dollar($('table#cashflow_monthly_expense-'+property_home_id+' tfoot input#total_monthly_expense').val()));
	var net_operating = total_income - total_expense;
	$('input#net_operating_income-'+property_home_id).val('$'+number_format(net_operating));
}

function monthly_expense_amounts(that)
{
	if(typeof(that) == 'object')
	{
		var table = $(that).parent().parent().parent().parent('table').prop('id');
		var finder					= table.split("-");
		var property_home_id		= finder.slice(1,2);
	}
	else if(typeof(that) == 'number')
	{
		var table = 'cashflow_monthly_expense-'+that;
		var property_home_id		= that;
	}
	var sum = 0;
	var number = 0;
	$('table#'+table+' tbody tr').each(function(){
		var amount = parseFloat(replace_dollar($(this).find('input[name="amount_expense[]"]').val()));
		if(amount)
		{
			number++;
			$(this).find('input[name="amount_expense[]"]').val('$'+number_format(amount));
			sum += amount;
		}
	});
	$('table#'+table+' tfoot input#total_monthly_expense').val('$'+number_format(sum));
	// $('table#'+table+' tfoot input#count_monthly_expense').val(number);
	
	var total_income = parseFloat(replace_dollar($('table#cashflow_monthly_income-'+property_home_id+' tfoot input#total_monthly_income').val()));
	var total_expense = parseFloat(replace_dollar($('table#cashflow_monthly_expense-'+property_home_id+' tfoot input#total_monthly_expense').val()));
	var net_operating = total_income - total_expense;
	$('input#net_operating_income-'+property_home_id).val('$'+number_format(net_operating));
}




function add_roww_monthly_income(property_home_id)
{
	table_id = 'cashflow_monthly_income-'+property_home_id;
	var row = jQuery('#cashflow_monthly_income_hidden tr').clone(true);
	row.appendTo('#'+table_id);        
    return false;
}

function add_roww_monthly_expense(property_home_id)
{
	table_id = 'cashflow_monthly_expense-'+property_home_id;
	var row = jQuery('#cashflow_monthly_expense_hidden tr').clone(true);
	row.appendTo('#'+table_id);        
    return false;
}
function remove_property_home_page(popup_id)
{
	if(confirm("Are you sure to delete") == true)
	{
		var form_id = popup_id;
		$('form#'+form_id).submit();
	}
} 



function autocalculate_property_home_page()
{
	var sum_purchase_price 	= 0;
	var sum_current_value 	= 0;
	var sum_future_value 	= 0;
	var sum_sold_refi_value = 0;
	var number				= 0;
	$('#table-property-home-page tbody tr:not(tr.property-home-title)').each(function(){
		
		number++;
		
		// sum of purchase price value
		var purchase_price = parseFloat(replace_dollar($(this).find('input[name="purchase_price[]"]').val()));
		if(purchase_price)
		{
			$(this).find('input[name="purchase_price[]"]').val('$'+number_format(purchase_price));
			sum_purchase_price += purchase_price;
		}
		
		// sum of current value
		var current_value = parseFloat(replace_dollar($(this).find('input[name="current_value[]"]').val()));
		if(current_value)
		{
			$(this).find('input[name="current_value[]"]').val('$'+number_format(current_value));
			sum_current_value += current_value;
		}
		
		// sum of future value
		var future_value = parseFloat(replace_dollar($(this).find('input[name="future_value[]"]').val()));
		if(future_value)
		{
			$(this).find('input[name="future_value[]"]').val('$'+number_format(future_value));
			sum_future_value += future_value;
		}
		
		// sum of ltv value
		var sold_refi_value = parseFloat(replace_dollar($(this).find('input[name="sold_refi_value[]"]').val()));
		if(sold_refi_value)
		{
			$(this).find('input[name="sold_refi_value[]"]').val('$'+number_format(sold_refi_value));
			sum_sold_refi_value += sold_refi_value;
		}
		
	})
	
	
	$('table#table-property-home-page tfoot th#total-purchase_price').text('$'+sum_purchase_price.toFixed(2));
	$('table#table-property-home-page tfoot th#total-current-value').text('$'+sum_current_value.toFixed(2));
	$('table#table-property-home-page tfoot th#total-future-value').text('$'+sum_future_value.toFixed(2));
	$('table#table-property-home-page tfoot th#total-sold_refi_value').text('$'+sum_sold_refi_value.toFixed(2));
	$('table#table-property-home-page tfoot th span#count-number').text(number);
}

function autocalculate_encumbrance_left_data(that)
{
	// alert(typeof(that));
	if(typeof(that) == 'object')
	{
		var table = $(that).parent().parent().parent().parent('table').prop('id');
		var finder					= table.split("-");
		var property_home_id		= finder.slice(1,2);
	}
	else if(typeof(that) == 'number')
	{
		var table = 'table_ecumbrances-'+that;
		var property_home_id		= that;
	}
	
	var count 					= 0;
	var sum_current_balance 	= 0;
	var sum_monthly_payment 	= 0;
	var sum_original_balance 	= 0;
	var sum_original_balance 	= 0;
	$('table#'+table+' tbody tr').each(function(){
		// Count number of existing liens
		count++;
		
		// Sum of current balance
		var current_balance = parseFloat(replace_dollar($(this).find('input[name="current_balance[]"]').val()));
		if(current_balance)
		{
			$(this).find('input[name="current_balance[]"]').val('$'+number_format(current_balance));
			sum_current_balance += current_balance;
		}
		
		// Sum of original balance
		var original_balance = parseFloat(replace_dollar($(this).find('input[name="original_balance[]"]').val()));
		if(original_balance)
		{
			$(this).find('input[name="original_balance[]"]').val('$'+number_format(original_balance));
			sum_original_balance += original_balance;
		}
		
		// Sum of monthly balance
		var monthly_payment = parseFloat(replace_dollar($(this).find('input[name="monthly_payment[]"]').val()));
		if(monthly_payment)
		{
			$(this).find('input[name="monthly_payment[]"]').val('$'+number_format(monthly_payment));
			sum_monthly_payment += monthly_payment;
		}
		
	});
	
	// $('div#ecumbrance-data-right-'+property_home_id+' span#ecum_count_existing_lien').text(count);
	// $('div#ecumbrance-data-right-'+property_home_id+' span#ecum_sum_current_balance').text('$'+number_format(sum_current_balance));
	// $('div#ecumbrance-data-right-'+property_home_id+' span#ecum_total_monthly_payment').text('$'+number_format(sum_monthly_payment));
	
	// Loan to current value
	// var current_value = parseFloat(replace_dollar(($('input#property-home-current-value-'+property_home_id).val())));
	// var future_value = parseFloat(replace_dollar(($('input#property-home-current-value-'+property_home_id).val())));
	
	// var loan_to_current_value = (sum_current_balance/current_value)*100;
	
	// $('div#ecumbrance-data-right-'+property_home_id+' span#ecum_loan_to_current_value').text(number_format(loan_to_current_value)+'%');
	
	// Loan to Future value
	// var loan_to_future_value = (sum_current_balance/future_value)*100;
	
	// $('div#ecumbrance-data-right-'+property_home_id+' span#ecum_loan_to_future_value').text(number_format(loan_to_future_value)+'%');
	
	$('table#table_ecumbrances-'+property_home_id+' input').attr('readonly',true);
	// $('table#table_ecumbrances-'+property_home_id+' input').css('border','1px solid white !important');
	$('table#table_ecumbrances-'+property_home_id+' select option').attr('disabled',true);
}

$(document).ready(function(){
	autocalculate_property_home_page();
	calculate_senior_ecumbrance_ltv();
});





	
	



//  Add row in Comparab Sale Modal 
	
function comparable_sale_add(property_home_id){
	var row = jQuery('#comparable_sale_hidden tr').clone(true); 
	row.appendTo('#table_comparable_sale-'+property_home_id);        
	return false;
}   

function multiple_check(chec){

// if ($(chec).is(':checked')) {
 var id='12';
  // var primary_property=1;

 // var check='1';
 // $('.disabled').css({"color": "currentColor", "cursor": "not-allowed","opacity": "0.5","text-decoration": "none"}).removeAttr("href");
      
 
 

       $.ajax({

                type: 'POST',
                url: '<?php echo base_url().'load_data/update_multi_check';?>',

                data: { 'id' : id },
                success:function(response)
                 {
               
      
                       }
        });


	

}  
  
//  Add row in Comparab Sale Modal 

function comparable_sale(that){
	
	var cam_id = that.id;
	
	if(cam_id == 'new'){
		
		 $("tr#new_tr").remove();
		
	}else{
		if(confirm('Are you sure to delete?') == true)
			{
			   $.ajax({
				   type : 'POST',
				   url : '<?php echo base_url().'delete_comparable_sale_id'; ?>',
				   data	: { 'comp_id' : cam_id },
				   success : function(result)
				   {
					    $('tr#remove_'+cam_id).remove();
				   }
			   });
			}
	}
	
}
        
/* $('.comparable_sale_remove').on("click", function() {
	  var comp_id = this.id;
	  $(this).parent().parent('tr').attr('id','remove');
	  
	  if(comp_id)
	  {
			if(confirm('Are you sure to delete') == true)
			{
			   $.ajax({
				   type : 'POST',
				   url : '<?php echo base_url().'delete_comparable_sale_id'; ?>',
				   data	: { 'comp_id' : comp_id },
				   success : function(result)
				   {
					    $('tr#remove').remove();
				   }
			   });
			}
	  }
	  else
	  {
		   $(this).parents("tr").remove();
	  }
	  
	 
 
}); */


function auto_add_cashflow(that){
	
	//monthly_income calculation...
	
	if(typeof(that) == 'object')
	{
		var table = $(that).parent().parent().parent().parent('table').prop('id');
		var finder					= table.split("-");
		var property_home_id		= finder.slice(1,2);
	}
	else if(typeof(that) == 'number')
	{
		var table = 'cashflow_monthly_income-'+that;
		var property_home_id		= that;
	}
	var sum = 0;
	var number = 0;
	$('table#'+table+' tbody tr').each(function(){
		var amount = parseFloat(replace_dollar($(this).find('input[name="amount_income[]"]').val()));
		if(amount)
		{
			number++;
			$(this).find('input[name="amount_income[]"]').val('$'+number_format(amount));
			sum += amount;
		}
	});
	
	$('table#'+table+' tfoot input#total_monthly_income').val('$'+number_format(sum));
	
	
	//monthly_expense calculation...
	
	if(typeof(that) == 'object')
	{
		var table = $(that).parent().parent().parent().parent('table').prop('id');
		var finder					= table.split("-");
		var property_home_id		= finder.slice(1,2);
	}
	else if(typeof(that) == 'number')
	{
		var table = 'cashflow_monthly_expense-'+that;
		var property_home_id		= that;
	}
	var sum = 0;
	var number = 0;
	$('table#'+table+' tbody tr').each(function(){
		var amount = parseFloat(replace_dollar($(this).find('input[name="amount_expense[]"]').val()));
		if(amount)
		{
			number++;
			$(this).find('input[name="amount_expense[]"]').val('$'+number_format(amount));
			sum += amount;
		}
	});
	$('table#'+table+' tfoot input#total_monthly_expense').val('$'+number_format(sum));
	// $('table#'+table+' tfoot input#count_monthly_expense').val(number);
	
	var total_income = parseFloat(replace_dollar($('table#cashflow_monthly_income-'+property_home_id+' tfoot input#total_monthly_income').val()));
	var total_expense = parseFloat(replace_dollar($('table#cashflow_monthly_expense-'+property_home_id+' tfoot input#total_monthly_expense').val()));
	var net_operating = total_income - total_expense;
	$('input#net_operating_income-'+property_home_id).val('$'+number_format(net_operating));
	
}


  function auto_clicked(){

  $('#asss').click();
//autocalculate_compare_sale_totals();
   }

function autocalculate_compare_sale_totals(that)
{
	
	if(typeof(that) == 'object')
	{
		var table = $(that).parent().parent().parent().parent('table').prop('id');
		var finder					= table.split("-");
		var property_home_id		= finder.slice(1,2);
	}
	else if(typeof(that) == 'number')
	{
		var table = 'table_comparable_sale-'+that;
		var property_home_id		= that;
	}
		// calculate average sq_feet
	var sum = 0;
	var count = 0;
    $('table#'+table+' tbody input[name="sq_feet[]"]').each(function(){
		count++;
		var check = parseFloat(replace_dollar($(this).val()));
		if(check)
		{
			sum +=  check;
		}
		
    });
	var avg_sq_feet = 0;
	if(count > 0)
	{
		avg_sq_feet = sum / count;
	}
	var avg_sq_feet=number_format_comma_only(avg_sq_feet);
	$('table#'+table+' input#avg_sq_ft').val(avg_sq_feet);
	
	
	
	// calculate average bedrooms
	var sum = 0;
	var count = 0;
    $('table#'+table+' tbody input[name="bedrooms[]"]').each(function(){
		
		var check = replace_dollar($(this).val());
		if($(this).val() && check != '')
		{
			sum += + parseFloat(replace_dollar($(this).val()));
			count++;
		}
		
    });
	var avg_bedrooms = 0;
	if(count > 0)
	{
		avg_bedrooms = sum / count;
	}
	$('table#'+table+' input#avg_bedrooms').val(avg_bedrooms.toFixed(0));
	
	// calculate average bathrooms
	var sum = 0;
	var count = 0;
    $('table#'+table+' tbody input[name="bathrooms[]"]').each(function(){
		
		var check = replace_dollar($(this).val());
		if($(this).val() && check != '')
		{
			sum += + parseFloat(replace_dollar($(this).val()));
			count++;
		}
		
    });
	var avg_bathroom = 0;
	if(count > 0)
	{
		avg_bathroom = sum / count;
	}
	
	$('table#'+table+' input#avg_bathroom').val(avg_bathroom.toFixed(0));
	
	// calculate average lot_size
	var sum = 0;
	var count = 0;
    $('table#'+table+' tbody input[name="lot_size[]"]').each(function(){
			var check = replace_dollar($(this).val());
			if($(this).val() && check != '' )
			{
				sum += + parseFloat(replace_dollar($(this).val()));
				count++;
			}
		
    });
	var avg_lot_size = 0;
	if(count > 0)
	{
	avg_lot_size = sum / count;
	}
	var avg_lot_size=number_format_comma_only(avg_lot_size);
	$('table#'+table+' input#avg_lot_size').val(avg_lot_size);
	
	// calculate average year_built
	var sum = 0;
	var count = 0;
    $('table#'+table+' tbody input[name="year_built[]"]').each(function(){
			var check = replace_dollar($(this).val());
			if($(this).val() && check != '' )
			{
				sum += + parseFloat(replace_dollar($(this).val()));
				count++;
			}
		
    });
	var avg_year_built = 0;
	if(count > 0)
	{
		avg_year_built = sum / count;
	}
	
	$('table#'+table+' input#avg_year_built').val(avg_year_built.toFixed(0));
	
	
	// calculate average sale_price
	var sum = 0;
	var count = 0;
    $('table#'+table+' tbody input[name="sale_price[]"]').each(function(){
		
		var check = replace_dollar($(this).val());
		if($(this).val() && check != '')
		{
			sum += + parseFloat(replace_dollar($(this).val()));
			count++;
		}
		
    });
	var avg_sale_price = 0;
	if(count > 0)
	{
		avg_sale_price = sum / count;
	}
	$('table#'+table+' input#avg_sale_price').val('$'+number_format_comma_only(avg_sale_price));
	
	// calculate average per_sq_feet
	var sum = 0;
	var count = 0;
    $('table#'+table+' tbody input[name="per_sq_feet[]"]').each(function(){
		
		var check = replace_dollar($(this).val());
		if($(this).val() && check != '')
		{
			sum += + parseFloat(replace_dollar($(this).val()));
			count++;
		}
    });
	var avg_per_sq_feet = 0;
	if(count > 0)
	{
		avg_per_sq_feet = sum / count;
	}

	$('table#'+table+' input#comp_sale_avg_per_sq_ft').val('$'+number_format(avg_per_sq_feet));
	
	var property_value_modification = $('table#'+table+' input[name="property_value_modification"]').val();
	
	//property_value_modification = parseFloat(replace_dollar(property_value_modification)) / 100;
	property_value_modification_1 = parseFloat(property_value_modification);

		
	var subject_value_square_ftt = parseFloat($('div#property-'+property_home_id+' input[name="square_feet"]').val());
	
	
	//var subject_property_dollar_per_sq_feet = parseFloat((avg_per_sq_feet * (1+property_value_modification_1))*subject_value_square_ftt);
	//var subject_property_dollar_per_sq_feet = parseFloat((avg_per_sq_feet * (1+property_value_modification_1)/10));
	var subject_property_dollar_per_sq_feet = parseFloat((avg_per_sq_feet * property_value_modification_1/100)+(avg_per_sq_feet));


	
	
	$('table#'+table+' input#comp_sale_per_sq_ft').val('$'+number_format(Math.abs(subject_property_dollar_per_sq_feet)));
	
	var subject_value_square_ft = parseFloat($('div#property-'+property_home_id+' input[name="square_feet"]').val());


	if(isNaN(subject_property_dollar_per_sq_feet) == true){
		subject_property_dollar_per_sq_feet = 0;
	}
	if(isNaN(subject_value_square_ft) == true){
		subject_value_square_ft = 0;
	}

	var subject_property_sale_price = subject_property_dollar_per_sq_feet * subject_value_square_ft;
	
	
	
	$('table#'+table+' input#subject_property_sale_price').val('$'+number_format_comma_only(Math.abs(subject_property_sale_price)));
	
	
}


//..........autofilled...........//

//$(document).ready(function(){

function copy_addresss(thaa){

var selectbox=$(thaa).val();

		var appraiser_namee = $('#form_i_add_property #appraiser_name').val();
		var valuation_vendorr= $('#form_i_add_property input#valuation_vendorr').val();
		var p_phonee= $('#form_i_add_property #p_phonee').val();
		var p_emaill =$('#form_i_add_property #p_emaill').val();
		var relationship_typee=$('#form_i_add_property #relationship_typee').val();
		var appraiser_s_addresss=$('#form_i_add_property #appraiser_s_addresss').val();
		var appraiser_unitt=$('#form_i_add_property #appraiser_unitt').val();
		var appraiser_cityy= $('#form_i_add_property #appraiser_cityy').val();
		var appraiser_statee=$('#form_i_add_property #appraiser_statee').val();
		var appraiser_zipp= $('#form_i_add_property #appraiser_zipp').val();


	if(selectbox == 1){

    
		var appraiser_name 	    = $('#appraiser_name').val();

		var valuation_vendor 	= $('input#valuation_vendor_1').val();
		
		var p_phone 			= $('#p_phone').val();
		
		var p_email 			= $('#p_email').val();
		var relationship_type 	= $('#relationship_type').val();
		
		var appraiser_s_address = $('#appraiser_s_address').val();
		var appraiser_unit 	   = $('#appraiser_unit').val();
		var appraiser_city 	   = $('#appraiser_city').val();
		var appraiser_state    = $('#appraiser_state').val();
		var appraiser_zip 	   = $('#appraiser_zip').val();

		$('#form_i_add_property #appraiser_name').val('1').trigger("chosen:updated");
		$('#form_i_add_property input#valuation_vendor').val('TaliMar Financial Inc.');
		$('#form_i_add_property #p_phone').val('(858) 613-0111');
		$('#form_i_add_property #p_email').val('servicing@talimarfinancial.com');
		$('#form_i_add_property #relationship_type').val('1');
		$('#form_i_add_property #appraiser_s_address').val('16880 West Bernardo Drive');
		$('#form_i_add_property #appraiser_unit').val('#140');
		$('#form_i_add_property #appraiser_city').val('San Diego');
		$('#form_i_add_property #appraiser_state').val('CA');
		$('#form_i_add_property #appraiser_zip').val('92127');
		
		
		
	}else{

		$('#form_i_add_property #appraiser_name').val(appraiser_namee);
		$('#form_i_add_property input#valuation_vendor').val(valuation_vendorr);
		$('#form_i_add_property #p_phone').val(p_phonee);
		$('#form_i_add_property #p_email').val(p_emaill);
		$('#form_i_add_property #relationship_type').val(relationship_typee);
		$('#form_i_add_property #appraiser_s_address').val(appraiser_s_addresss);
		$('#form_i_add_property #appraiser_unit').val(appraiser_unitt);
		$('#form_i_add_property #appraiser_city').val(appraiser_cityy);
		$('#form_i_add_property #appraiser_state').val(appraiser_statee);
		$('#form_i_add_property #appraiser_zip').val(appraiser_zipp);

	}

	var idss='';
	$.ajax({
		type	:'POST',
		url		:'<?php echo base_url()."Load_data/fetch_property_app";?>',
		data	:{'pay_pro':idss},
		success : function(response){
			 $('#form_i_add_property select#appraiser_name').html('');
			 $('#form_i_add_property select#appraiser_name').html(response);
			 $('#form_i_add_property select#appraiser_name').chosen();			
		}			
	});

}
//});
function check_required_checkbox(that)
{
	if($(that).is(':checked') == true)
	{
		$(that).parent().parent().parent().find('input[name="required[]"]').val('1');
	}
	else
	{
		$(that).parent().parent().parent().find('input[name="required[]"]').val('0');
	}
}

function check_request_updated_insurance_checkbox(that)
{
	if($(that).is(':checked') == true)
	{
		$(that).parent().parent().parent().find('input[name="request_updated_insurance[]"]').val('1');
	}
	else
	{
		$(that).parent().parent().parent().find('input[name="request_updated_insurance[]"]').val('0');
	}
}

function check_donottrack_checkbox(that)
{
	if($(that).is(':checked') == true)
	{
		$(that).parent().parent().parent().find('input[name="donottrack[]"]').val('1');
	}
	else
	{
		$(that).parent().parent().parent().find('input[name="donottrack[]"]').val('0');
	}
}

function add_image_section(property_home_id,folder_id){
	// $("#foo > div").length
	
	var rowCount = $('.property_imgs_'+folder_id+' >div').length;
	
	
	rowCount++;
	

	var div = '<div class="col-sm-3 img-div_'+rowCount+'"><div class="property-image-inner-main"><div class="property-image-inner-image"><a onclick="click_on_image_button(this)"><img src="<?php echo base_url(); ?>property_images/no_image.png" class="property-image"></a><input type="file" name="file[]" class="image_upload_input"><input type="hidden" name="id[]" value="new" ></div><div class="property-image-inner-date"><input name="date[]"  value="" class="form-control datepicker" Placeholder="Date"></div><div class="property-image-inner-description-main"><div class="property-image-description"><input name="description[]" value="" class="form-control" Placeholder="Description"></div><div class="property-image-delete"><a data-id = "'+rowCount+'" onclick="delete_image(this)"><i class="fa fa-trash" aria-hidden="true"></i></a></div></div></div></div>';
	$('div#property-images-'+folder_id+'-section-'+property_home_id).append(div);
$(".datepicker").datepicker({ dateFormat: 'mm-dd-yy'});
}

function image_readURLLL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
		// alert($(input).parent().prop('class'));
      $(input).parent().find('img.property-image').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

function click_on_image_button(that)
{
	$(that).parent('div.property-image-inner-image').find('input.image_upload_input').click();
	$(that).parent('div.property-image-inner-image').find('input.image_upload_input').change(function() {
		image_readURLLL(this);
	});
}
// $('input.image_upload_input').change(function() {
    // image_readURL(this);
// });

function delete_image(that)
{
	var rowcnt = $(that).data('id');
	
	var id = $(that).parent().parent().parent().find('input[name="id[]"]').val();
	if(id == 'new')
	{
		$(that).parents('div.col-sm-3.img-div_'+rowcnt).remove();
	}
	else
	{
		if(confirm('Are you sure to delete') === true)
		{
			$.ajax({
				type 	: 'POST',
				url 	: '<?php echo base_url()."delete_property_images";?>',
				data 	: { 'id':id },
				success	: function(result)
				{
					$(that).parents('div.col-sm-3').remove();
				}
			})
		}
	}
}

function hoa_changes(that)
{
	if(typeof(that) == 'object')
	{
		var hoa = that.value;
		var div = $(that).parent().parent().prop('id');
		var finder					= div.split("-");
		var property_home_id		= finder.slice(4,5);
	}
	else if(typeof(that) == 'number')
	{
		var property_home_id		= that;
		var div		= 'edit-property-hoa-section-'+property_home_id;
		var hoa = $('div#'+div+' select[name="hoa"]').val();
	}
	
	if(hoa == '1' )
	{
		$('div#'+div+' input[name="hoa_dues"]').attr('disabled',false);
		$('div#'+div+' select[name="hoa_stand"]').attr('disabled',false);
	}
	else
	{
		$('div#'+div+' input[name="hoa_dues"]').val('');
		$('div#'+div+' input[name="hoa_dues"]').attr('disabled',true);
		
		$('div#'+div+' select[name="hoa_stand"] option[value=""]').attr('selected',true);
		$('div#'+div+' select[name="hoa_stand"]').attr('disabled',true);
	}
}

function calculate_senior_ecumbrance_ltv()
{
	var t_purchase_price = parseFloat(replace_dollar($('table#table-property-home-page tfoot th#total-purchase_price').text()));
	var t_current_value = parseFloat(replace_dollar($('table#table-property-home-page tfoot th#total-current-value').text()));
	var t_future_value = parseFloat(replace_dollar($('table#table-property-home-page tfoot th#total-future-value').text()));
	var t_sold_refi_value = parseFloat(replace_dollar($('table#table-property-home-page tfoot th#total-sold_refi_value').text()));
	
	
	var ecum_purchase_price = parseFloat(replace_dollar($('table#table-property-home-page tfoot input[name="ecum_purchase_price"]').val()));
	var ecum_current_value = parseFloat(replace_dollar($('table#table-property-home-page tfoot input[name="ecum_current_value"]').val()));
	var ecum_future_value = parseFloat(replace_dollar($('table#table-property-home-page tfoot input[name="ecum_future_value"]').val()));
	var ecum_sold_refi_value = parseFloat(replace_dollar($('table#table-property-home-page tfoot input[name="ecum_sold_refi"]').val()));
	var ltv_purchase_price = ltv_current_value = ltv_sold_refi_value = ltv_future_value = 0;
	if(ecum_purchase_price)
	{
		var ltv_purchase_price 	= (ecum_purchase_price/t_purchase_price)*100;
	}
	
	if(ecum_current_value)
	{
		var ltv_current_value 	= (ecum_current_value/t_current_value)*100;
	}
	
	if(ecum_future_value)
	{
		var ltv_future_value 	= (ecum_future_value/t_future_value)*100;
	}
	
	if(ecum_sold_refi_value)
	{
		var ltv_sold_refi_value = (ecum_sold_refi_value/t_sold_refi_value)*100;
	}
	$('table#table-property-home-page tfoot th#ltv_purchase_price').text(ltv_purchase_price.toFixed(2)+'%');
	$('table#table-property-home-page tfoot th#ltv_current_value').text(ltv_current_value.toFixed(2)+'%');
	$('table#table-property-home-page tfoot th#ltv_future_value').text(ltv_future_value.toFixed(2)+'%');
	$('table#table-property-home-page tfoot th#ltv_sold_refi_value').text(ltv_sold_refi_value.toFixed(2)+'%');
	
	
}
function save_property_info(property_home_id){
	
	$('button#property-home-save-'+property_home_id).val('close');
	$('button#property-home-save-'+property_home_id).click();
	// $('#frm_closing_statement_items_id').submit();
	$('#property_data_confirmOverlay_'+property_home_id).css('display','none');
	
}
function property_edit_close(id)
{
	
	$('#property_data_confirmOverlay_'+id).css('display','block');
	
	
} 

function add_images_folder(property_home_id)
{
	var div = '<div class="col-sm-3"><div class="folder-main-images"><img src="<?php echo base_url().'assets/images/folder.png'; ?>"></div><div class="folder-title"><input type="hidden" value="new" name="id[]"><input type="text" name="title[]" value="New Folder"></div></div>';
	$('div#add-image-folders-'+property_home_id).append(div);
}

function add_new_property_data(table)
{
	var rowCount = $('#table-property-home-page tbody tr.data-row').length;
	
	rowCount++;
	// alert(rowCount);
	if(rowCount >= 4){
		$('#add_secondary_rows').css('display','none');
	}else{
		$('#add_secondary_rows').css('display','inline-block');
		
	}
	var row = $('#table-property-home-page tr.secondary-property-rows').after('<tr class = "data-row"><td></td><td> </td><td><select id="selectbasic" name="position[]" class="form-control load_data_inputbox"><option value="" ></option><?php foreach($position_option as $key => $optn) { ?><option value="<?php echo $key;?>"   ><?php echo $optn;?></option><?php } ?><option value = "5" >5th</option></select></td><td><input type="text" name="senior_liens[]" class="form-control number_only amount_format_without_decimal" value="" onchange="autocalculate_property_home_page()" id="property-home-senior-liens-'+rowCount+'" ></td><td><input type="text" name="current_value[]" class="form-control number_only amount_format" value="" onchange="autocalculate_property_home_page()" id="property-home-current-value-'+rowCount+'"readonly ></td><td><input type="text" name="future_value[]" class="form-control number_only amount_format" value="" onchange="autocalculate_property_home_page()" id="property-home-future-value-'+rowCount+'"readonly ></td><td><input type="text" name="sold_refi_value[]" class="form-control number_only" value="" onchange="autocalculate_property_home_page()" readonly></td></tr></tr>');
	
	
	
	// $('form#property_hidden_form').find('input[name="table"]').val(table);
	// $('form#property_hidden_form').submit();
}
function add_ecumbrance_tr(property_home_id)
{
	
	var table 			= 'ecumbrances';
	var loan_id 		= '<?php echo $loan_id; ?>';
	var talimar_loan 	= '<?php echo $modal_talimar_loan; ?>';
	$.ajax({
		type	: 'POST',
		url		: '<?php echo base_url()."property_hidden_form";?>',
		data	: { 'table':table, 'loan_id':loan_id, 'talimar_no':talimar_loan,  'property_home_id':property_home_id },
		success : function(result){
		
			$('a#ecum_edit_hidden_link').attr('onclick','load_ecumbrances_questions_ajax("'+result+'","")');
			var div = '<tr id="tr-row-'+result+'"><td class="check_row"><a class="ecum_remove"><i class="fa fa-trash" aria-hidden="true"></i><input type="hidden" value="'+result+'" name="id"></a></td><td><input type="text" name="lien_holder[]" value="" class="form-control" readonly /></td><td><input type="text" name="original_balance[]" value=""  class="form-control  number_only" onchange="autocalculate_encumbrance_left_data(this)" readonly /></td><td><input type="text" name="current_balance[]" value=""  class="form-control  number_only" onchange="autocalculate_encumbrance_left_data(this)" readonly /></td><td><input type="text" name="balance_at_close[]" value=""  class="number_only form-control" onchange="autocalculate_encumbrance_left_data(this)" /></td><td><input type="text" name="will_it_remain[]" value="'+result+'" class="form-control" ></td><td><input type="text" name="position[]" class="form-control" value="'+result+'"></td><td><input type="text" name="proposed_priority[]" class="form-control" value="'+result+'"></td><td><input type="text" name="monthly_payment[]" value=""  class="form-control number_only" onchange="autocalculate_encumbrance_left_data(this)" readonly/></td><td class = "lien_center"><a onclick="load_ecumbrances_questions_ajax('+result+','+"''"+')" id="new_link_edit"><i class="fa fa-pencil" aria-hidden="true"></i></a></td></tr>';
			
			$('#table_ecumbrances-'+property_home_id+' tbody').append(div);
			$('a#ecum_edit_hidden_link').click();
		}
	})
}
function replace_dollar(n)
{
	var a = n.replace('$', '');
	var b = a.replace(',', '');
	var b = b.replace(',', '');
	var b = b.replace(',', '');
	var b = b.replace(',', '');
	return b;
}
function number_format(n) {
    return n.toFixed(2).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}

function number_format_comma_only(n) {
    return n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}

function number_format_percent_loan(n) {
    return n.toFixed(8).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "" + c : c;
    });
}

function appraiser_disable(that){
	var id=$(that).val();

	if(id==3){

		$('#valuation_vendor').val('').attr('disabled',true);	
		$('#p_phone').val('').attr('disabled',true);	
		$('#p_email').val('').attr('disabled',true);	
		$('#valuation_date').val('').attr('disabled',true);	
		$('#appraiser_s_address').val('').attr('disabled',true);	
		$('#appraiser_unit').val('').attr('disabled',true);	
		$('#appraiser_city').val('').attr('disabled',true);	
		$('#appraiser_zip').val('').attr('disabled',true);	
		$('#appraiser_state').val('').attr('disabled',true);	
		$('#appraiser_state').val('').attr('disabled',true);	
		$('#relationship_type').val('').attr('disabled',true);	
		$('select[name="valuation_company"]').val('').attr('disabled',true);	
		$('#appraiser_name').val('').trigger('chosen:updated');
		$('#appraiser_name').attr('disabled', true).trigger("chosen:updated");
	}else{

		$('#valuation_vendor').val('').attr('disabled',false);	
		$('#p_phone').val('').attr('disabled',false);	
		$('#p_email').val('').attr('disabled',false);	
		$('#valuation_date').val('').attr('disabled',false);	
		$('#appraiser_s_address').val('').attr('disabled',false);	
		$('#appraiser_unit').val('').attr('disabled',false);	
		$('#appraiser_city').val('').attr('disabled',false);	
		$('#appraiser_state').val('').attr('disabled',false);	
		$('#appraiser_state').val('').attr('disabled',false);	
		$('#relationship_type').val('').attr('disabled',false);	
		$('select[name="valuation_company"]').val('').attr('disabled',false);	
		
		$('#appraiser_name').attr('disabled',false).trigger("chosen:updated");
		$('#appraiser_zip').val('').attr('disabled',false);	
	}



}


function form_sumbit_ecumbrance_data()
{
	
	var formData = new FormData(document.getElementById("form-ecumbrance-data"));
	
	$.ajax({
		type	: 'POST', 
		url		: '<?php echo base_url()."form_sumbit_ecumbrance_data"; ?>',
		data	: formData,
		
		async: false,
		contentType: false,
		processData: false,
		success : function(response)
		{
			// if(result == 'Update')
			// {
				// $('#ecumbrance-data-status').text('Encumbrance Data Updated');
			// }
			var data = JSON.parse(response);
			var property_home_id = data.property_home_id;
			var id = data.id;
			var lien_holder = data.lien_holder;
			var original_balance = data.original_balance;
			var current_balance = data.current_balance;
			var monthly_payment = data.monthly_payment;
			var position 		= data.position;
			var will_it_remain 	= data.will_it_remain;
			var proposed_priority	= data.proposed_priority;
			
			var table_row = $('table#table_ecumbrances-'+property_home_id+' tbody tr#tr-row-'+id);
			
			
			table_row.find('input[name="lien_holder[]"]').val(lien_holder);
			table_row.find('input[name="original_balance[]"]').val(original_balance);
			table_row.find('input[name="current_balance[]"]').val(current_balance);
			table_row.find('input[name="monthly_payment[]"]').val(monthly_payment);
			table_row.find('input[name="position[]"]').val(position);
			table_row.find('input[name="will_it_remain[]"]').val(will_it_remain);
			table_row.find('input[name="proposed_priority[]"]').val(proposed_priority);
			
			//table_row.find('input[name="position[]"] option[value="'+position+'"]').attr('selected',true);
			// $('button#ecum-data-close-modal').click();

			alert('Encumbrance updated successfully!');

			//$('#ecumbrance_questions').modal('hide');
			//$('#edit_ecumbrance').modal('hide');

		}
	});
}

function autofill_mortage_clause(property_home_id)
{
	var autofill = $('input#mortage_hidden_lender_'+property_home_id).val();
	$('textarea#mortage_textarea_'+property_home_id).val(autofill);
}

function print_insurencee(r){
var idd=$(r).parents().find('input[name="property_home_id"]').val();


	var textbox=$('#mortage_textarea_'+idd).val();
	
	if(textbox==''){
	alert('No Data Available in Property Insurance!');
	return false;
	}

	
}
function multiple_checkbox(raa){
 
 

	if($(raa).is(":checked")){


    var checkbox_val='1';
    var loan_id='<?php echo $this->uri->segment(2);?>';
    var talimar_loan='<?php echo $modal_talimar_loan;?>';

   $.ajax({
		type	: 'POST',
		url		: '<?php echo base_url()."update_multi_checkk";?>',
		data	: { 'loan_id':loan_id, 'checkbox_val':checkbox_val,'talimar_loan':talimar_loan},
		success : function(result){

        if(result=='1'){
        $('.disabled').css({"pointer-events":"auto","cursor":"pointer","color":"#5b9bd1"});   
        $('.dis').css({"pointer-events":"auto","cursor":"pointer","color":"#5b9bd1"});   
       }
  }

	    });

 }else{

   var loan_id='<?php echo $this->uri->segment(2);?>';
   var talimar_loan='<?php echo $modal_talimar_loan;?>';
    var checkbox_val='0';
   $.ajax({
		type	: 'POST',
		url		: '<?php echo base_url()."update_multi_checkk";?>',
		data	: { 'loan_id':loan_id, 'checkbox_val':checkbox_val,'talimar_loan':talimar_loan},
		success : function(result){
      if(result=='1'){

$('.disabled').css({"pointer-events":"none","cursor":"default","color":"gray"});   
      	$('.dis').css({"pointer-events":"none","cursor":"default","color":"gray"});   
       }
  }

	    });

 }
}


function edit_com_model(tgd,property_home_ids){

var id=$(tgd).attr('id');


var talimar_loan_number='<?php echo $modal_talimar_loan;?>';

	$.ajax({
		type	: 'POST', 
		url		: '<?php echo base_url()."Property_data/edit_com_model";?>',
		data	: {'id':id,'talimar_loan_number':talimar_loan_number},
		success : function(response)
		{
			
            var data = JSON.parse(response);
             
             
          	var per_sq_ftt=data.per_sq_ft;
         
			var property_home_id = data.property_home_id;
			
			var prop_per_unitttt=data.prop_per_unit;
	            
			var id 	= data.id;
			var prop_link = data.prop_link;

			var prop_building = number_format_comma(data.prop_building);


			var prop_unit = data.prop_unit;
			var prop_type 	= data.prop_type;
			var sale_price	= number_format_comma(data.sale_price);
		
			var year_built	= data.year_built;
			var lot_size_new= removeCommas(data.lot_size);
			var lot_size	= number_format_comma(lot_size_new);
			var bathroom	= data.bathroom;
			var bedrooms	= data.bedrooms;
			var sq_ft		= data.sq_ft;
			var date_sold	= data.date_sold;
			var property_address = data.property_address;
			var talimar_loan	= data.talimar_loan;
			
	
			//alert(property_home_id+id+prop_link+bathroom+talimar_loan+property_address);
			

			$('input[name="prop_cityy"]').val(data.city);
			$('input[name="uni"]').val(data.unit);
			$('input[name="prop_streette"]').val(data.street);  
			$('input[name="prop_statee"]').val(data.state);  
			$('input[name="prop_zipp"]').val(data.zip);  
			$('input[name="prop_unitt"]').val(data.prop_unit); 

			//$('input[name="prop_streett"]').val(data.property_address);  
			$('input[name="p_idd"]').val(data.id);  

			$('input[name="prop_buildingg"]').val(prop_building);  
			$('input[name="prop_lott"]').val(lot_size);  
			$('input[name="prop_bedss"]').val(data.bedrooms);  
			$('input[name="prop_bathss"]').val(data.bathroom);  
			$('input[name="prop_yearr"]').val(data.year_built);  
			$('input[name="prop_sale_pricee"]').val('$'+sale_price);  
			$('input[name="prop_sale_datee"]').val(data.date_sold);  
			$('input[name="prop_sff"]').val('$'+per_sq_ftt);  
			$('input[name="prop_per_unitt"]').val('$'+prop_per_unitttt);  
			$('input[name="prop_linkk"]').val(data.prop_link);  
			$('select[name="prop_typee"] option[value="'+data.prop_type+'"]').attr('selected',true);
			$('#edit_property_'+property_home_ids).modal('show');

		}
	})

}


function sales_price_changess(mnn){

var prop_unit=$('input[name="prop_unit"]').val();
var prop_sale_price_d=replace_dollar($('input[name="prop_sale_price"]').val());

var prop_sale_price=removeCommas(prop_sale_price_d);
var prop_building_d=replace_dollar($('input[name="prop_building"]').val());
var prop_building=removeCommas(prop_building_d);

var valu=(prop_sale_price/prop_building).toFixed(0);
var value=(prop_sale_price/prop_unit).toFixed(0);

	if(isNaN(valu)) {

	var valu_1 = 0;

	}else{

     var valu_1 = valu;
	}
	if(isNaN(value)) {

	var value_2 =0;

	}
	else{
		
		var value_2 = value;

	}

$('input[name="prop_per_unit"]').val('$'+number_format_comma(value_2));
$('input[name="prop_sf"]').val('$'+number_format_comma(valu_1));

}

function sales_price_change(mn){

var prop_unitt=$('input[name="prop_unitt"]').val();
var prop_sale_pricee_d=replace_dollar($('input[name="prop_sale_pricee"]').val());
var prop_sale_pricee=removeCommas(prop_sale_pricee_d);
var prop_buildingg_d=replace_dollar($('input[name="prop_buildingg"]').val());
var prop_buildingg=removeCommas(prop_buildingg_d);



var value=(prop_sale_pricee/prop_buildingg).toFixed(0);

var valuee=(prop_sale_pricee/prop_unitt).toFixed(0);

	if(isNaN(value)) {

	var value_1 = 0;

	}else{

     var value_1 = value;
	}
	if(isNaN(valuee)) {

	var valuee_2 =0;

	}
	else{
		
		var valuee_2 = valuee;

	}

$('input[name="prop_per_unitt"]').val('$'+number_format_comma(valuee_2));
$('input[name="prop_sff"]').val('$'+number_format_comma(value_1));

}
 

// property ajax start//

   function ajax_property_data(that,key){
     
     var idss=$(that).parents().find('input.unique_'+key).val();
	$.ajax({
			type	:'POST',
			url		:'<?php echo base_url()."Load_data/fetch_property_app";?>',
			data	:{'pay_pro':idss},
			success : function(response){
 
		 $('.appraiser_class_'+key).html('');
		 $('.appraiser_class_'+key).html(response);
		 $('.chosen97_'+key).chosen();
 
				
			}
				
		});
		

     }  
  // property ajax end//

function removeCommas(str) {
    while (str.search(",") >= 0) {
        str = (str + "").replace(',', '');
    }
    return str;
};
function number_format_comma(n)
{
	    return n.replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}




</script>
<style>

.property_loan_row h4{
	background-color: #e8e8e8;
	height: 30px;

	line-height: 10px;
}

.form-control[readonly]{
	background-color: white !important;
}


.property_data_confirmOverlay{
    width:100%;
    height:100%;
    position:fixed;
    top:0;
    left:0;
    background:url('ie.png');
    background: -moz-linear-gradient(rgba(11,11,11,0.1), rgba(11,11,11,0.6)) repeat-x rgba(11,11,11,0.2);
    background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(rgba(11,11,11,0.1)), to(rgba(11,11,11,0.6))) repeat-x rgba(11,11,11,0.2);
    z-index:100000;
}
#confirmBox{
    background:url('body_bg.jpg') repeat-x left bottom #eeeeee;
    width:600px;
    position:fixed;
    left:40%;
    top:70%;
	border-radius: 10px;
    margin:-130px 0 0 -230px;
    border: 1px solid rgba(252, 247, 247, 0.6)
	height:100px;
    -moz-box-shadow: 0 0 2px rgba(255, 255, 255, 0.6) inset;
    -webkit-box-shadow: 0 0 2px rgba(255, 255, 255, 0.6) inset;
    box-shadow: 0 0 2px rgba(255, 255, 255, 0.6) inset;
	padding : 10px;
}


#confirmBox h4,
#confirmBox p{
	
    font:16px;
	text-shadow: 1px 1px 0 rgba(255, 255, 255, 0.6);
    color:#666;
	text-align:left;
}

#confirmBox h4{
    letter-spacing:0.3px;
    color:#666;
	border-bottom: 1px solid #494b4d;
}

#confirmBox p{
     font-size:16px;
    line-height:3.4;
    padding-top: 8px;
}




</style>
