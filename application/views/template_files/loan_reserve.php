<?php

	$loan_reserve_status_option = $this->config->item('loan_reserve_status_option');
	$no_yes_option = $this->config->item('no_yes_option');

	
?>
<style type="text/css">
	
	h4.paddingsection{

		padding: 10px;
		background: #ddd;
		width: 100%;
		font-weight: 600;
	}
	.addspace{
		margin-top: 8px;
	}
	.addspace11{
		margin-bottom: 10px;
	}

	#View_reserve #moredraw .col-md-1{

		width: 12%;
		padding-right: 5px !important;
    	padding-left: 5px !important;
	}

	

	#loan_reserve .table>thead>tr>th {
	    border-bottom: 1px solid #ddd !important;
	}

	#loan_reserve .table>tbody>tr>td {
	    border-top: 0px !important;
	}


</style>

<div class="modal fade bs-modal-lg" id="loan_reserve" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<!--<h4 class="modal-title">Loan Term :#<?php// echo $talimar_loan;?></h4>-->
				<h4 class="modal-title">Loan Reserve: <?php echo $property_addresss;?></h4>
			</div>
			 
			<div class="modal-body">

				<div class="row">
					<div class="col-md-12">
						<?php 
							/*echo '<pre>';
							print_r($fetch_loan_draw_schedule);
							echo '</pre>';*/
						?>

						<table class="table table-striped table-condensed flip-content">
							<thead>
								<tr>
									<th style="width:5%;">#</th>
									<th style="width:45%;">Draw Description</th>
									<th style="width:14%;">Total</th>
									<!-- <th style="width:14%;">Requested</th> -->
									<th style="width:14%;">Released</th>
									<th style="width:14%;">Remaining</th>
									<th style="width:8%;">View</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									$countdraw = 0;
									$TotalAmount = 0;
									$amount_Req = 0;
									$appAmount = 0;
									$remaining = 0;
									if(isset($fetch_loan_draw_schedule) && is_array($fetch_loan_draw_schedule)){
										foreach ($fetch_loan_draw_schedule as $value) {	 
										$countdraw++;

										if($value['status'] == 5){
											$reqAmoutVAl = 0;
										}else{
											$reqAmoutVAl = $value['reqAmount'];
										}

										$TotalAmount += $value['amount'];
										$amount_Req += $reqAmoutVAl;
										$appAmount += $value['appAmount'];
										$remaining += $value['remaining'];
								?>

										<tr id="row_<?php echo $value['id'];?>">
											<td><?php echo $countdraw;?></td>
											<td><textarea class="form-control" rows="2" readonly><?php echo $value['description'];?></textarea></td>
											<td><input type="text" class="form-control" value="$<?php echo number_format($value['amount'],2);?>" readonly></td>
											<!-- <td><input type="text" class="form-control" value="$<?php echo number_format($reqAmoutVAl,2);?>" readonly></td> -->
											<td><input type="text" class="form-control" value="$<?php echo number_format($value['appAmount'],2);?>" readonly></td>
											<td><input type="text" class="form-control" value="$<?php echo number_format($value['remaining'],2);?>" readonly></td>
											<td>
												<button class="btn blue btn-xs" type="button" id="<?php echo $value['id'];?>" onclick="drawScheduleData(this.id)">View</button>   
											</td>
										</tr>
								<?php } }else{ ?>

										<tr>
											<td colspan="6">No data found!</td>
										</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<th>Total</th>
									<th></th>
									<th>$<?php echo number_format($TotalAmount,2);?></th>
									<!-- <th>$<?php echo number_format($amount_Req,2);?></th> -->
									<th>$<?php echo number_format($appAmount,2);?></th>
									<th>$<?php echo number_format($remaining,2);?></th>
									<th></th>
								</tr>
							</tfoot>
							
						</table>
					</div>
				</div>

			</div>

			<div class="modal-footer">
					<button type="button" class="btn blue pull-left" id="new" onclick="drawScheduleData(this.id)">Add Draw</button>
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
					<a class="btn red" data-toggle="modal" href="#wire_information2">Deposit Account</a>
					<!--<button type="button" value="save" class="btn blue">Save</button>-->
			</div>
			
		</div>
		<!-- /.modal-content -->
	</div>
		<!-- /.modal-dialog -->
</div>


<div class="modal fade bs-modal-lg" id="View_reserve" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		<form method="post" action="<?php echo base_url();?>New_loan_data/loan_draw_schedule">
			<div class="modal-header">
				<button type="submit" name="save" class="btn blue pull-right">Save</button>
				<button type="button" data-dismiss="modal" class="btn btn-primary pull-right" style="margin-right: 5px;">Close</button>
				<button type="button" class="btn btn-danger pull-right" onclick="removeDRAW(this);" style="margin-right: 5px;">Delete</button>
				<h4 class="modal-title">Loan Draw #: <?php echo $property_addresss;?></h4>
			</div>
			 
			<div class="modal-body">
				<input type="hidden" name="talimar_loan" value="<?php echo $talimar_loan; ?>"/>
				<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>"/>
				<input type="hidden" name="drawID" value="new"/>

				<h4 class="paddingsection">Draw Setup</h4>
				<div class="row addspace">
					<div class="col-md-6">
						<label>Amount:</label>
						<input type="text" name="amount" class="form-control amount_format" value="" placeholder="Enter Amount">
					</div>

					<!--<div class="col-md-6">
						<a class="btn blue" data-toggle="modal" href="#add_draw_images" style="margin-top: 25px;">Draw Images</a>
					</div>-->
				</div>

				<div class="row addspace">
					<div class="col-md-12">
						<label>Description:</label>
						<textarea type="text" name="description" rows="4" class="form-control" placeholder="Enter Description" required="required"></textarea> 
					</div>
				</div>
				
				 
				<div class="row addspace"></div>
				<h4 class="paddingsection">Release Schedule</h4>

				<div id="moredraw">

					<div class="col-md-12 addspace11">
						<!-- <a title="Add draws"  onclick="addmoredrwas(this);" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> ADD</a> -->
						<a title="Add draws" id="Newrel" onclick="drawRequestData(this,'');" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> ADD</a>
					</div>

					<div class="col-md-12">
						<div class="col-md-1"><label style="margin-left: -10px;">Draw Status</label></div>
						<div class="col-md-1"><label>Approval Status</label></div>
						<div class="col-md-1"><label>Amount Requested</label></div>
						<div class="col-md-1"><label>Date Requested</label></div>
						<div class="col-md-1"><label>Amount Approved</label></div>
						<div class="col-md-1"><label>Date Approved</label></div>
						<div class="col-md-3"><label></label></div>
					</div>

				</div>

				<div class="row addspace"><br></div>

				<div class="row addspace">
					
					<div class="col-md-4" style="width: 48%;">
						<label class="pull-right">Released:</label>
					</div>

					<div class="col-md-2">
						<input type="text" name="sum" class="form-control amount_format" placeholder="Sum" readonly="readonly">
					</div>
				</div>
				<div class="row addspace">
					
					<div class="col-md-4" style="width: 48%;">
						<label class="pull-right">Remaining in Draw:</label>
					</div>
					<div class="col-md-2">
						<input type="text" name="Remaining" class="form-control amount_format" placeholder="Remaining" readonly="readonly">
					</div>
				</div>
			</div>
			<div class="modal-footer">	
			</div>
		</form>
		</div>
		<!-- /.modal-content -->
	</div>
		<!-- /.modal-dialog -->
</div>

<div class="modal fade bs-modal-lg" id="requestedData" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="post" action="<?php echo base_url();?>New_loan_data/loan_reqdraw_schedule">
				<div class="modal-header">

					<button type="button" class="btn default btn-sm pull-right" data-dismiss="modal">Close</button>
					<button type="submit" name="submit" class="btn blue btn-sm pull-right" style="margin-right: 5px;">Save</button>

					<h4 class="modal-title">Requested Draw #: <?php echo $property_addresss;?></h4>
				</div>
				<div class="modal-body">
							<input type="hidden" name="talimar_loan" value="<?php echo $talimar_loan; ?>"/>
							<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>"/>
							<input type="hidden" name="reqrowid" class="form-control" value="">
							<input type="hidden" name="draw_id" class="form-control" value="">
							<input type="hidden" name="amountdata" class="form-control" value="">

					<div class="row addspace">
						<div class="col-md-6">
							<label>Draw Status:</label>
							<select class="form-control" name="draw_status">
								<?php foreach($loan_reserve_status_option as $key => $row){ ?>
									<option value="<?php echo $key;?>"><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-6">
							<label>Approval Status:</label>
							<select class="form-control" name="app_status">
								<?php foreach($no_yes_option as $key => $row){ ?>
									<option value="<?php echo $key;?>"><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="row addspace">
						<div class="col-md-6">
							<label>Amount Requested:</label>
							<input type="text" name="AmountRequested" class="form-control amount_format" value="" required="required">
						</div>
						<div class="col-md-6">
							<label>Date Requested:</label>
							<input type="text" name="DateRequested" class="form-control datepicker" value="" required="required">
						</div>
					</div>

					<div class="row addspace">
						<div class="col-md-6">
							<label>Amount Approved:</label>
							<input type="text" name="AmountApproved" class="form-control amount_format" value="">
						</div>
						<div class="col-md-6">
							<label>Date Approved:</label>
							<input type="text" name="DateApproved" class="form-control datepicker" value="">
						</div>
					</div>
					

				</div>
				<div class="modal-footer">
					
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">

	function addmoredrwas(that){

		var countdraw = $('#moredraw .row').length;
		countdraw++;

		var userRole = '<?php echo $this->session->userdata('user_role');?>';
		if(userRole == '2'){
			var allowapprove = '';
		}else{
			var allowapprove = 'disabled="disabled"';
		}

		$('#View_reserve #moredraw').append('<div class="row addspace" id="remove_'+countdraw+'"><div class="col-md-3"><input type="hidden" name="draw_rel_id[]" value="new"><input type="text" name="amount_Req[]" id="amt_'+countdraw+'" class="form-control amount_format" value="" placeholder="Amount Requested" ></div><div class="col-md-3"><input type="text" name="amount_app[]" id="amtapp_'+countdraw+'" class="form-control amount_format" value="" placeholder="Amount Approved" ></div><div class="col-md-3"><select type="text" id="status_'+countdraw+'" onchange="displaydatereleased(this.value,'+countdraw+');" name="status[]" class="form-control" ><?php foreach($loan_reserve_status_option as $key => $row){ ?><option value="<?php echo $key;?>"><?php echo $row;?></option><?php } ?></select></div><div class="col-md-3"><a title="Remove" onclick="removemoredrwas('+countdraw+');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></div></div>');

			$(".datepicker").datepicker({ dateFormat: 'mm-dd-yy' });
	}

	function removemoredrwas(rowid){

		$('#moredraw #remove_'+rowid).remove();
	}


	function displaydatereleased(that,key){

		if(that == '4'){
			$('input#rel_'+key).attr('disabled',false);
		}else{
			$('input#rel_'+key).attr('disabled',true);
		}
	}

	function alllock(that,key){

		if(that == '1'){
			$('input#dreq_'+key).attr('disabled',false);
			$('input#amt_'+key).attr('disabled',false);
			//$('select#appro_'+key).attr('disabled',false);
			//$('select#status_'+key).attr('disabled',false);
			$('input#amtapp_'+key).attr('disabled',false);
		}else{
			$('input#dreq_'+key).attr('disabled',true);
			$('input#amt_'+key).attr('disabled',true);
			//$('select#appro_'+key).attr('disabled',true);
			//$('select#status_'+key).attr('disabled',true);
			$('input#amtapp_'+key).attr('disabled',true);
		}
	}

	function viewstatusdata(that,key){

		if(that == '1'){
			$('select#status_'+key).attr('disabled',false);
			$('input#amtapp_'+key).attr('disabled',false);
			$('input#rel_'+key).attr('disabled',false);
			//$('input#dsub_'+key).attr('disabled',false);
		}else{
			$('select#status_'+key).attr('disabled',true);
			$('input#amtapp_'+key).attr('disabled',true);
			$('input#rel_'+key).attr('disabled',true);
			//$('input#dsub_'+key).attr('disabled',true);
		}
	}
	
	
	function formatDate(date) {
	     var d = new Date(date),
	         month = '' + (d.getMonth() + 1),
	         day = '' + d.getDate(),
	         year = d.getFullYear();

	     if (month.length < 2) month = '0' + month;
	     if (day.length < 2) day = '0' + day;

	     return [month, day, year].join('-');
	}

	function drawScheduleData(drawID){

		$('#View_reserve input[name="drawID"]').val('new');
		$('#View_reserve input[name="amount"]').val('');
		$('#View_reserve textarea[name="description"]').val('');
		$('#View_reserve input[name="sum"]').val('');
		$('#View_reserve input[name="Remaining"]').val('');

		if(drawID == 'new'){

			$('#View_reserve #moredraw .row').remove();
			$('#View_reserve #Newrel').attr('disabled',true);

			$('#View_reserve').modal('show');

		}else{
			$('#View_reserve #Newrel').attr('disabled',false);
			//run ajax code here...
			$.ajax({

				type : 'POST',
				url  : '<?php echo base_url()."New_loan_data/get_loandraw_schedule_info";?>',
				data : {'rowid':drawID },
				success : function(response){

					var data = JSON.parse(response);

					$('#requestedData input[name="reqrowid"]').val(data.drawid); //for requestedData modal
					$('#View_reserve button#reqDAta').val(data.drawid);
					$('#requestedData input[name="amountdata"]').val(data.amount); //for requestedData modal
					$('#View_reserve input[name="drawID"]').val(data.drawid);
					$('#View_reserve input[name="amount"]').val('$'+addCommas(data.amount));
					$('#View_reserve textarea[name="description"]').val(data.description);

					$('#View_reserve #moredraw .row').remove();

					var userRole = '<?php echo $this->session->userdata('user_role');?>';

					var AmountReq = 0;
					var Amountappss = 0;

					$.each(data.drawrel, function(key,row){

						AmountReq += parseFloat(row.amount_Req);

						if(row.amount_app === null || row.amount_app <= 0){
							//Amountappss += '0';
						}else{
							Amountappss += parseFloat(row.amount_app);
						}

						if(row.date_Req === '0000-00-00'){
							var date_Req = '';
						}else{
							var date_Req = formatDate(row.date_Req);
						}
						
						if(row.date_Released === '0000-00-00'){
							var date_Released = '';
						}else{
							
							var date_Released = formatDate(row.date_Released);
						}
						

						var emailbody = 'FCI Representative -  %0D%0A %0D%0A Please release $'+addCommas(row.amount_Req)+' from the restricted suspense for Account #<?php echo $outlook_property[0]->fci ?> to <?php echo $outlook_borrower[0]->b_name ?>. Attached to this e-mail is the account information for the wire. %0D%0A%0D%0A Please confirm receipt of this email and notify us once the wire has been released.%0D%0A%0D%0A Should you have any questions, please contact TaliMar Financial immediately. %0D%0A%0D%0A Regards – %0D%0A';

						var emailheader = 'href="mailto:asullivan@trustfci.com?subject=Construction Draw Request – <?php echo $outlook_property[0]->property_address;?>&body= '+emailbody+'&cc=loanservicing@trustfci.com; servicing@talimarfinancial.com;<?php	foreach($fetch_draws_contact_email as $assig){ echo $assig->contact_email. ' ;';}?>&bcc=."';

						if(row.status == 6){
							var statusVal = 'Not Submitted';
						}else if(row.status == 3){
							var statusVal = 'Submitted';
						}else if(row.status == 4){
							var statusVal = 'Released';
						}else if(row.status == 5){
							var statusVal = 'Closed';
						}else{
							var statusVal = '';
						}

						if(row.approve == 2){
							var approveVal = 'No';
						}else if(row.approve == 1){
							var approveVal = 'Yes';
						}

						$('#View_reserve #moredraw').append('<div class="row addspace" id="rowdd_'+row.id+'" style="margin-left:5px;"><div class="col-md-1"><input type="hidden" name="draw_rel_id[]" value="'+row.id+'"><span>'+statusVal+'</span></div><div class="col-md-1"><span>'+approveVal+'</span></div><div class="col-md-1"><span>$'+addCommas(row.amount_Req)+'</span></div><div class="col-md-1"><span>'+date_Req+'</span></div><div class="col-md-1"><span>$'+addCommas(row.amount_app)+'</span></div><div class="col-md-1"><span>'+date_Released+'</span></div><div class="col-md-3"><a title="Remove" onclick="removedrwas('+row.id+');" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i></a>&nbsp;<a title="Add Images" class="btn btn-success btn-sm" data-toggle="modal" href="#add_draw_images" ><i class="fa fa-upload"></i></a>&nbsp;<a title="Send E-Mail" class="btn btn-primary btn-sm" '+emailheader+'><i class="fa fa-envelope"></i></a>&nbsp;<a title="Add Request" class="btn btn-info btn-sm" onclick="drawRequestData(this,'+row.id+');"><i class="fa fa-edit"></i></a></div></div>');
						
					}); 


					if(Amountappss > 0){
						var sumrelease = '$'+addCommas(Amountappss);
						var calrem = Amountappss;
					}else{
						var sumrelease = '$0.00';
						var calrem = '0.00';
					}

					$('#View_reserve input[name="sum"]').val(sumrelease);

					var AmountRemaining = parseFloat(data.amount - calrem);
					$('#View_reserve input[name="Remaining"]').val('$'+addCommas(AmountRemaining));

					$('#View_reserve').modal('show');
				}
			});
		}
	}

	function viewrelopt(that,key){

		if(that == '1'){
			$('select#ro_'+key).attr('disabled',false);
			$('input#rows_'+key).attr('disabled',false);
			$('input#eamtapp_'+key).attr('disabled',false);
			$('input#rowsub_'+key).attr('disabled',false);
		}else{
			$('select#ro_'+key).attr('disabled',true);
			$('input#rows_'+key).attr('disabled',true);
			$('input#eamtapp_'+key).attr('disabled',true);
			$('input#rowsub_'+key).attr('disabled',true);
		}

	}

	function alllockfields(that,key){

		if(that == '1'){
			//$('select#ro_'+key).attr('disabled',false);
			//$('select#app_'+key).attr('disabled',false);
			$('input#ereq_'+key).attr('disabled',false);
			$('input#eamt_'+key).attr('disabled',false);
			$('input#eamtapp_'+key).attr('disabled',false);
			//$('input#rows_'+key).attr('disabled',false);
		}else{
			//$('select#app_'+key).attr('disabled',true);
			//$('select#ro_'+key).attr('disabled',true);
			$('input#ereq_'+key).attr('disabled',true);
			$('input#eamt_'+key).attr('disabled',true);
			$('input#eamtapp_'+key).attr('disabled',true);
			//$('input#rows_'+key).attr('disabled',true);
		}

	}

	function displaydatereleaseddddd(that,key){

		if(that == '4'){
			$('input#rows_'+key).attr('disabled',false);
		}else{
			$('input#rows_'+key).attr('disabled',true);
		}

	}

	function removedrwas(rowid){

		if(confirm('Are you sure to remove this draw?')){

			$.ajax({

				type : 'POST',
				url  : '<?php echo base_url()."New_loan_data/remove_draw_rel";?>',
				data : {'rowid':rowid },
				success : function(response){

					$('#rowdd_'+rowid).remove();

				}
			})
		}
	}

	function addCommas(nStr)
	{
	    nStr += '';
	    x = nStr.split('.');
	    x1 = x[0];
	    x2 = x.length > 1 ? '.' + x[1] : '';
	    var rgx = /(\d+)(\d{3})/;
	    while (rgx.test(x1)) {
	        x1 = x1.replace(rgx, '$1' + ',' + '$2');
	    }
	    return x1 + x2;
	}

	function removeDRAW(that){

		var drawID = $('#View_reserve input[name="drawID"]').val();
		if(drawID != 'new'){
			if(confirm('Are you sure to remove this draw?')){

				$.ajax({

					type : 'POST',
					url  : '<?php echo base_url()."New_loan_data/remove_loan_draw";?>',
					data : {'rowid':drawID },
					success : function(response){

						$('#View_reserve').modal('hide');
						$('#loan_reserve tr#row_'+drawID).remove();
					}
				});
			}
		}
	}

	function drawRequestData(that,reqID){

		$('#requestedData input[name="AvailableFunds"]').val('');
		$('#requestedData input[name="AmountRequested"]').val('');
		$('#requestedData input[name="AmountApproved"]').val('');
		$('#requestedData input[name="DateRequested"]').val('');
		$('#requestedData input[name="DateApproved"]').val('');

		if(reqID !=''){

			$.ajax({

					type : 'POST',
					url  : '<?php echo base_url()."New_loan_data/requestdatainfo";?>',
					data : {'rowid':reqID },
					success : function(response){
						
						var data = JSON.parse(response);
						if(data.date_Req === '0000-00-00'){
							var date_Req = '';
						}else{
							var date_Req = formatDate(data.date_Req);
						}
						
						if(data.date_Released === '0000-00-00'){
							var date_Released = '';
						}else{
							
							var date_Released = formatDate(data.date_Released);
						}

						var amountdata = $('#requestedData input[name="amountdata"]').val();

						var AvailableFunds = parseFloat(amountdata - data.amount_app);
						//var RemainingFunds = parseFloat(AvailableFunds - data.amount_app);

						$('#requestedData input[name="reqrowid"]').val(data.id);
						$('#requestedData input[name="AvailableFunds"]').val('$'+addCommas(AvailableFunds));
						$('#requestedData input[name="AmountRequested"]').val('$'+addCommas(data.amount_Req));
						$('#requestedData input[name="AmountApproved"]').val('$'+addCommas(data.amount_app));

						$('#requestedData input[name="DateRequested"]').val(date_Req);
						$('#requestedData input[name="DateApproved"]').val(date_Released);

						$('#requestedData').modal('show');
					}
				});
		}else{

			var rowid = $('#View_reserve input[name="drawID"]').val();
			$('#requestedData input[name="reqrowid"]').val('new');
			$('#requestedData input[name="draw_id"]').val(rowid);

			$('#requestedData').modal('show'); 
		}
	}

</script>