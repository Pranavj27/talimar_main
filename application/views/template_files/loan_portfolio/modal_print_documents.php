<style type="text/css">
	.unbold_font label {
    font-weight: 400 !important;
}
</style>
<div class="modal fade bs-modal-lg" id="print_loan_document" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url();?>print_loan_document" id="form_print_loan_document">
							
				<!-----------Hidden fields----------->
				<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
				<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
				<!-----------End of hidden Fields---->
							
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Loan: <?php echo $property_addresss; ?> </h4>
				</div>
				<div class="modal-body unbold_font">
				 <div class="print_document_div_section">
				 <div class="row" style="font-weight:500 !important;">
				 	<div class="col-sm-6">
				 		<label>	
							<input type="checkbox" onchange="auto_select_loan_app(this)" id="">
							Loan Application
						</label>
					</div>
				    				
					<div class="col-sm-6">
						<label style="margin-left: 30px;">
						<input type="checkbox" onchange="auto_select_print(this)" id="auto_select_print_input">
						Fix and Flip Loan
						</label>
					</div>
					<div class="col-sm-6">
						<label>
						<input type="checkbox" onclick="select_all_print_loan(this)" id="print_select_all">
						Check All
						</label>
					</div>
				 	<div class="col-sm-6">
				 		<label style="margin-left: 30px;">
						<input type="checkbox" onchange="bridge_select_print_doc(this);" id="bridge_doc">
						Bridge Loan
						</label>
					</div>
					<div class="col-sm-6">
						<label>
						<input type="checkbox" onclick="deselect_all_print_loan(this)" id="print_deselect_all">
						Uncheck  All
						</label>
					</div>
				 </div>
				<table id="table_print_document">
					<tr>
						<td colspan="2"><h3>Executive Summary</h3></td>
					</tr>

					<tr>
						<td>
							<label>
							<input type="checkbox" name="trust_deed_overview">
							Trust Deed Overview
							</label>
						</td>
						<td>
							<label>
							<input type="checkbox" name="executive_summary_other_ecumbrance">
							Other Encumbrances
							</label>
						</td>
						
					</tr>
					<tr>
						<td>
							<label>
							<input type="checkbox" name="loan_summary">
							Loan Summary
							</label>
						</td>
						<td>
							<label>
							<input type="checkbox" name="executive_summary_cashflow">
							Property Cashflow
							</label>
						</td>
					
					
					</tr>
					<tr>
				<!-- 		<td><input type="checkbox" name="Loan_Purpose">Loan Purpose</td> -->
						<td>
							<label>
							<input type="checkbox" name="Property_Information">
							Property Information
							</label>
						</td>
						<td>
							<label>
							<input type="checkbox" name="executive_summary_sources_and_uses"> Sources & Uses
							</label>
						</td>
					
				
						
					</tr>
					<tr>
							<td>
								<label>
								<input type="checkbox" name="executive_summary_proposed_improvement">
								Proposed Improvements
								</label>
							</td>
							<td>
								<label>
								<input type="checkbox" name="Borrower_Information">
								Borrower Information
								</label>
								</td>	
					
						
						
					</tr>
					<tr>
					<td>
						<label>
						<input type="checkbox" name="executive_summary_valuation">
						Property Valuation
						</label>
					</td>	
					<td>
						<label>
						<input type="checkbox" name="Contact_Information"> Contact(s) Information
						</label>
						</td>
					
						
					
					</tr>
					<tr>
				<td></td>
					<td>
						<label>
						<input type="checkbox" name="executive_summary_site_photos">
						Property Images
						</label>
					</td>
				
						
			
					</tr>

					<tr>
					<!-- <td><input type="checkbox" name="property_valu">Property Valuation 1</td> -->
					<td></td> 
					<td>
						<label>
						<input type="checkbox" name="Loan_Servicing"> Loan Servicing</label></td>
					</tr>
				

					<tr>
						<td colspan="2"><h3>Underwriting Documents</h3></td>
					</tr>

					<tr>
						<td><label><input type="checkbox" name="pre_approval_letter">Pre-Approval Letter</label></td>
						<td><label><input type="checkbox" name="term_sheet">Term Sheet – Bridge Loan</label></td>
					</tr>

					<tr>
						<td><label><input type="checkbox" name="loan_application">Loan Application – Primary</label></td>
						<td><label><input type="checkbox" name="term_sheet_flip">Term Sheet – Flip Loan</label></td>
									
					</tr>
					<tr>
						<td><label><input type="checkbox" name="authorization_to_release">Loan Application – Credit Authorization</label></td>
						<td><label><input type="checkbox" name="term_sheet_new">Term Sheet – New</label></td>
						
											
					</tr>
					<tr>
								
					<td><label><input type="checkbox" name="privacy_notice_disclosure">Loan Application – Privacy Notice</label></td>
					<td><label><input type="checkbox" name="re885_demo">RE885</label></td>
					
					</tr>

					<tr>
						<td><label><input type="checkbox" name="automated_valuation_notice">Loan Application – Valuation Notice</label></td>
						<td><label><input type="checkbox" name="loan_folder_tabs">Loan Folder Tabs</label></td>				
						
					</tr>
					<tr>
						<td><label><input type="checkbox" name="diligence_materials_list">Due Diligence Schedule</label></td>
						<td><label><input type="checkbox" name="appraisal_assignment_form">Appraisal Assignment Form</label></td>		
						
					</tr>
					<tr>
						<td><label><input type="checkbox" name="ach_construction_draw">Construction Draw Wire Setup Form</label></td>
						<td><label><input type="checkbox" name="credit_report_form">Credit Report Form</label></td>										
					</tr>

					<tr>
						<td><label><input type="checkbox" name="transaction_contact">Transaction Contacts</label></td>

					</tr>

					<tr>
						<td colspan="2"><h3>Escrow / Title Documents</h3></td>
					</tr>
					<tr>
						<td><label><input type="checkbox" name="lender_closing_statement">Lender Closing Statement</label></td>
          				<td><label><input type="checkbox" name="schedule_income_wire">Incoming Wire Schedule</label></td>
					</tr>
					<tr>
						<td><label><input type="checkbox" name="lender_schedule_print">Lender Schedule</label></td>
						<td><label><input type="checkbox" name="new_loan_setup_form">New Loan Setup Form</label></td>
					</tr>

					<tr>
						<td><label><input type="checkbox" name="InitialLenderInstructions">Initial Lender Instructions</label></td>
						<td><label><input type="checkbox" name="prior_to_wire">Prior to Wire Requirements</label></td>
					</tr>

					<tr>
						<td colspan="2"><h3>Loan Documents</h3></td>
					</tr>
					<tr>
						<td><label><input type="checkbox" name="loan_verification">Loan Verification Form</label></td>
						<td><label><input type="checkbox" name="LoanDocumentCheck">Loan Document Check</label></td>
					</tr>		
			     	<tr>
						<td><label><input type="checkbox" name="loan_term">Loan Terms</label></td>
						<td></td>							
					</tr>
					<tr>
						<td><label><input type="checkbox" name="escrow_instrusction">Escrow Lender Instructions</label></td>
						<td></td>							
					</tr>



					<tr>
						<td><label><input type="checkbox" name="arbitration_of_disputes">Arbitration of Disputes</label></td>
						
						<td><label><input type="checkbox" name="fair_lending_notice">Fair Lending Notice</label></td>
					</tr>
					<tr>
						<td><label><input type="checkbox" name="anti_money_document">Anti-Money Laundering Declaration Document</label></td>
						<td><label><input type="checkbox" name="fci_construction_disbursement_agreement">FCI Construction Disbursement Agreement</label></td>
					</tr>
					<tr>
						<td><label><input type="checkbox" name="ass_of_construct_contract">Assignment of Construction Contracts</label></td>
						<td><label><input type="checkbox" name="fedral_equal_opprtunity_act">Federal Equal Opportunity Act</label></td>
					</tr>
					<tr>
						<td><label><input type="checkbox" name="ballon_payment_disclosures">Balloon Payment Disclosures</label></td>
						<td><label><input type="checkbox" name="first_payment_notify">First Payment Notification</label></td>
					</tr>
					<tr>
						<td><label><input type="checkbox" name="new_payment_form">Borrower Auto Payment Form</label></td>
						<td><label><input type="checkbox" name="payment_gurranty">Payment Guaranty</label></td>	
					</tr>
					<tr>
								
						
					</tr>
					<tr>
						<td><label><input type="checkbox" name="borrower_certification">Borrower Certification & Authorization</label></td>
					<td><label><input type="checkbox" name="promissory_note_secure_trust">Promissory Note Secured by Deed of Trust</label></td>
					</tr>
					<tr>
						<td><label><input type="checkbox" name="builders_risk_insurance_req">Builders Risk Insurance Requirements</label></td>
						<td><label><input type="checkbox" name="insurance_endrosement">General Liability Insurance Requirements</label></td>
					</tr>
					<tr>
						<td><label><input type="checkbox" name="california_insurance_disclosure">California Insurance Disclosure</label></td>
					
						<td></td>		
					</tr>
					<tr>
						
							<td><label><input type="checkbox" name="capacity_to_repay_disclosure">Capacity to Repay Disclosure</label></td>
						<td><label><input type="checkbox" name="request_for_notice">Request for Notice</label></td>			
					</tr>
					<tr>
						
					<td><label><input type="checkbox" name="certificate_bussiness_purpose_loan">Certificate of Business Purpose Loan</label></td>
						<td><label><input type="checkbox" name="default_provision">Rider – Default Provision</label></td>	
					</tr>
					<tr>
						
						
						<td><label><input type="checkbox" name="compliance_agreement">Compliance Agreement</label></td>
						<td><label><input type="checkbox" name="rider_interest_reserve">Rider – Interest Reserve</label></td>	
					</tr>
					<tr>
						
						<td><label><input type="checkbox" name="consumer_notice">Consumer Notices</label></td>			
						<td><label><input type="checkbox" name="attachment_a_construct_draw">Rider – Construction Draw Schedule</label></td>

					</tr>
					<tr>
						<?php 
						$daa='';
						$unchecked = '';
						if($fetch_loan_borrower)
						{
							if($fetch_loan_borrower->borrower_type == '1'){ 
								$daa='disabled';
								$unchecked='unchecked';
							}else{
								$daa='';
								
							}
						}

						?>
						<td><label><input type="checkbox" name="corporate_resolution" <?php echo $daa; ?> <?php echo $unchecked; ?>>Corporate Resolution</label></td>
						<td><label><input type="checkbox" name="subordination_lease_agreement">Subordination Lease Agreement</label></td>
					</tr>
					<tr>
						<td><label><input type="checkbox" name="title_lender_instrusction">Title/Lender Instruction</label></td>
						<td><label><input type="checkbox" name="credit_score_disclosure">Credit Score Disclosure</label></td>
					</tr>
					<tr>
						
						<td><label><input type="checkbox" name="TemporaryExtensionAgreement">Temporary Extension Agreement</label></td>
						<td></td>								
					</tr>
					<tr>
						

						
					</tr>
					<tr>
						
						<td><label><input type="checkbox" name="deed_trust_assigment_rents">Deed of Trust w/ Assignment of Rents</label></td>
						<td></td>	
					</tr>

                    <tr>
					<td><label><input type="checkbox" name="exclusive_devt_ratio">Excessive Debt Ratio</label></td>
						<td></td>
						</tr>
					<tr>
						<td colspan="2"><h3>Loan Servicing Documents</h3></td>
					</tr>

					<tr>
						<td><label><input type="checkbox" name="accrued_charges">Accrued Charges</label></td>
						<td><label><input type="checkbox" name="forclosure_update">Foreclosure Status Update</label></td>
					</tr>
					<tr>
						<td><label><input type="checkbox" name="ApprovaltoShareInformation">Approval to Share Information</label></td>
						<td><label><input type="checkbox" name="late_payment_notice">Late Payment Notice (10+ Days)</label></td>
					</tr>
					<tr>
						<td><label><input type="checkbox" name="loan_assignment">Assignment of Deed of Trust</label></td>
						<td><label><input type="checkbox" name="extention_approval">Lender Extension Approval Request</label></td>	
					</tr>
					<tr>
						<td><label><input type="checkbox" name="assignment_request">Assignment Request</label></td>
						<td><label><input type="checkbox" name="loan_maturity_notification">Loan Maturity Notification</label></td>
						
						
					</tr>
					<tr>
						<td><label><input type="checkbox" name="ballon_payment_demand">Balloon Payment Demand</label></td>
					
						<td><label><input type="checkbox" name="loan_maturity_notification_extopt">Loan Maturity Notification – Extension Option</label></td>
						
					</tr>
					<tr>
						<td><label><input type="checkbox" name="BorrowerAuthorizationForm">Borrower Authorization Form</label></td>
						<td><label><input type="checkbox" name="overview_loan">Overview of Loan Terms</label></td>
					</tr>
					<tr>
						<td><label><input type="checkbox" name="borrower_verification">Borrower Verification</label></td>
						
						<td><label><input type="checkbox" name="payoff_checklist">Payoff Checklist</label></td>	
					</tr>
					<tr>
						<td><label><input type="checkbox" name="construction_draw_modiAgree">Construction Draw Modification Agreement</label></td>
						<td><label><input type="checkbox" name="property_review">Property Data Review</label></td>
					</tr>
					<tr>
						<td><label><input type="checkbox" name="construction_draw_form">Construction Draw Approval Form</label></td>	
						<td><label><input type="checkbox" name="property_tax_notice">Property Tax Notice</label></td>
					</tr>
					<tr>
														
						<td><label><input type="checkbox" name="html_reserve">Construction Draw Schedule / Balance</label></td>
						<td><label><input type="checkbox" name="reserve_modification_agreement">Reserve Modification Agreement</label></td>
					</tr>
					<tr>
						
						<td><label><input type="checkbox" name="construction_draw_wire_instruction">Construction Draw Wire Instructions</label></td>
						<td><label><input type="checkbox" name="ServicingIntroductionCall">Servicing Introduction Call</label></td>
					</tr>
					<tr>
						
						
						<td><label><input type="checkbox" name="Servicer_Review_Checklist">Servicer Review Checklist</label></td>
						<td><label><input type="checkbox" name="extension_agreement">Extension Agreement</label></td>	
					</tr>
					<tr>
						<td><label><input type="checkbox" name="Servicer_Funds_Deposit_Notice">Servicer Funds Deposit Notice </label></td>
						<td><label><input type="checkbox" onclick="fn_fciboardingdata(this);">FCI Boarding Data</label></td>	
					</tr>
					<tr>
						
						<td><label><input type="checkbox" name="title_assigment_request">Title Assignment Request</label></td>
						<td><label><input type="checkbox" name="FCIPayoffRequest">FCI Payoff Request</label></td>
					</tr>
					<tr>
						<td><label><input type="checkbox" name="transfer_servicing_disclosure">Transfer of Servicing Disclosure (RESPA)</label></td>
						<td></td>
					</tr>
					<tr>
						<td colspan="2"><h3>Default Notices</h3></td>
					</tr>
					<tr>
						<td><label><input type="checkbox" name="default_notice">Default Notice - Late Payment</label></td>
						<td><label><input type="checkbox" name="default_notice_lm">Default Notice – Loan Maturity</label></td>
					</tr>
					<tr>
						
						<td><label><input type="checkbox" name="default_property_tax">Default Notice – Property Taxes</label></td>	
						<td><label><input type="checkbox" name="default_insurance_adv">Default Notice – Notice of Insurance Advancement</label></td>	
						
					</tr>
				</table>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn default" data-dismiss="modal">Close</button>
			<button type="submit" class="btn blue" >Print</button>
			<input type="submit" class="btn red" name="signloanNow" value="DocuSign">
		</div>
	</form>
</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>