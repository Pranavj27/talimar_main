<div class="modal fade" id="new_modal_2" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<h4 class="modal-title">Saved Documents: <?php echo $property_addresss;?></h4>
			</div>
					<div class="modal-body">
						<div class="doc-div">
							<div class="row">

								<div id="ct_Documentsadd" class="row">
									<div class="col-md-2">&nbsp;</div>
									<div class="col-md-8">
										<div class="ct_loan_saved_document" style="display: none;">			
											<form method="POST" enctype="multipart/form-data" action = "<?php echo base_url('Print_document/loan_saved_document');?>" >	

											<input type="hidden" value="<?php echo $loan_id;?>" name="loan_id">
											<input type = "hidden" name = "document_id" id = "document_id" />
											<div class="form-group">
												<label for="document_name">Document Name:</label>
												<input type="text" class="form-control" id="document_name" name="document_name" required>
											</div>
											<div class="form-group">
												<div id="ad">
													<label>Select document:</label>
													<input type="file" id = "upload" name ="upload_docc[]" class="select_images_file" onchange="change_this_image(this)" required>
												</div>
											</div>
											<button type="submit" id="uploadd" name="upload_file" class="btn blue">Upload</button>
											</form>
										</div>
									</div>
									<div class="col-md-2">&nbsp;</div>
								</div>
							</div>
										<?php 
												$folder = 'saved_document/'.$talimar_loan;
												echo '<input type="hidden" value="'.$folder.'" id="hydra_2">';
												$due_title_document = $this->aws3->getBucketObjectList($folder);
											?>

										<div class="col-md-12">
											
											<table class="table table-responsive" id="loan_doc_table_tab" style="width: 100% !important;">
												<thead>
													<tr>
														<th>Action</th>
														<th>Name of Document</th>
														<th>Uploaded Date</th>
														<th>User</th>
														<th>View</th>
													</tr>
												</thead>
												<tbody>
													<?php
													if($due_title_document){
														foreach($due_title_document as $filename) {
													      $FileArr = explode('/', $filename);
															$FileNameGet = $FileArr[count($FileArr)-1];

															$file1 = explode('/', $FileNameGet);
															$file2 = explode('.', $file1[count($file1)-1]);

															$NameUploaded = $file2[0];
															$UploadedDate = '-';
															$UploadedUser = '-';
															
															$documentsData = $this->User_model->select_where('loan_saved_documents', array('document_base_name' => $FileNameGet));
															$documentsData = $documentsData->row();
															//$documentsData = $this->User_model->query("SELECT * FROM loan_saved_documents WHERE document_base_name = '$FileNameGet'")->row();
															if($documentsData){
																$up_user_id = $documentsData->created_by;
																$NameUploaded = $documentsData->name_of_document;
																$UploadedDate = date("m-d-Y", strtotime($documentsData->created_at));

																$UpUserDate = $this->User_model->query("SELECT * FROM user WHERE id = '$up_user_id'")->row();
																if($UpUserDate){
																	$UploadedUser = $UpUserDate->fname.' '.$UpUserDate->lname;
																}
															}
													?>
													<tr attrremove="<?php echo $FileNameGet; ?>">
														<td>
															<a><i  id="<?php echo $FileNameGet; ?>" onclick="upload_document_del(this)" title = "Delete File" class = "fa fa-trash"  aria-hidden="true"></i></a>
														</td>
														<td>
															<?php echo $NameUploaded; ?>
														</td>
														<td><?php echo $UploadedDate; ?></td>
														<td><?php echo $UploadedUser; ?></td>
														<td>
															<a href ="<?php echo aws_s3_document_url($filename); ?>" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a>
														</td>
													</tr>
													<?php
														}
													}
													?>

												</tbody>
											</table>
										</div>
										
										<br>
									</div>
										<!--<button onclick = "saved_document_add_more_row(this);" id = "add_row" class = "btn blue" type = "button"><i class="fa fa-plus" aria-hidden="true">Add More</i></button>-->
								</div>
		                        <div class="modal-footer"> 
		                        	<button type="button" id="ct_loan_saved_documentadd" class="btn btn-primary">Add Document</button>
									<button type="button" class="btn default" data-dismiss="modal">Close</button>
									
								</div>			
							</div>
	<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>