<div class="modal fade bs-modal-lg" id="closing_statement_new11" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" style = "width:1200px !important">
		<div class="modal-content">
			<form method = "POST" id = "frm_closing_statement_items_id" action = "<?php echo base_url();?>form_closing_statement_items">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<!--<h4 class="modal-title">Closing Statement: #<?php //echo ''.$talimar_loan; ?></h4>-->
					<h4 class="modal-title">Closing Statement: <?php echo ''.$property_addresss; ?></h4>
				</div>
				<div class="modal-body">
					<div class="portlet-body rc_class">
						<!-----------Hidden fields----------->
						
						<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
						<input type="hidden" name="loan_id" value="/<?php echo $loan_id; ?>">
						<!-----------End of hidden Fields---->
						
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" id="closing_statement_new">
							
							<thead>
								<tr>
									<th style = "width: 3%;" class="padding-zero hud"></th>
									<th style = "width: 10%;" class="padding-zero hud">HUD-1</th>
									<th style = "width: 22%;" >Item</th>
									<th>Paid to<br>Others</th>
									<th>Paid to<br>TaliMar</th>
									<th>Total</th>
									<th style = "width: 15%;">Paid to</th>
									<th style = "width: 12%;" class="padding-zero">Net Funding</th>
									<th style = "width: 12%;" class="padding-zero">Direct to<br>Servicer</th>
									<!--<th style = "width: 12%;" class="padding-zero">Loan <br>Reserves</th>-->
									<th style = "width: 12%;" class="padding-zero">Pre Paid<br>Charge</th>
									<th style = "width: 12%;" class="padding-zero">Lender<br>Statement</th>
								</tr>
							</thead>
								
							<tbody>
								<?php
								if(isset($fetch_closing_statement_items_data)){
									foreach($fetch_closing_statement_items_data as $row)
									{
									
										if($row->hud == 800 || $row->hud == 900 || $row->hud == 1000 || $row->hud == 1100 || $row->hud == 1200 || $row->hud == 1300 )
										{
											if($row->hud != 800)
											{
								?>
												<tr id="<?php echo $row->hud; ?>">
													<th colspan="11"><a onclick="add_row_closing_items(this)" id="<?php echo $row->hud; ?>" ><i class="fa fa-plus" aria-hidden="true"></i></a></th>
												</tr>
										<?php } ?>
												<tr>
													<td></td>
													<td><strong><?php echo $row->hud; ?></strong></td>
													<td colspan="5"><strong><?php echo $row->items; ?></strong></td>
													<td><small>Net Funding</small></td>
													<td><small>Direct to Servicer</small></td>
													<td><small>Pre Paid Charge</small></td>
													<td><small>Lender Statement</small></td>
													
													
												</tr>
									<?php } else {  
									
											
												?>
											<tr id="<?php echo $row->hud; ?>">
										<?php if(!isset($closing_statement_item_loads[$row->hud])){?>
											<td><a href="javascript:void(0);" id="<?php echo $row->id;?>" onclick="delete_hud(this)"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
										<?php }else{

											echo '<td></td>';
										} ?>
											<td>
												<?php echo $row->hud; ?>
												<input type="hidden" value="<?php echo $row->id;?>" name="id_<?php echo $row->id;?>">
											</td>
											
											<?php
											if($row->hud == '808'){
												
												$paid_to_broker = $fetch_loan_result[0]->loan_amount * ($fetch_loan_result[0]->lender_fee / 100);
											}

											if($row->hud == '801'){
												
												$paid_to_broker = $fetch_loan_result[0]->loan_amount * ($fetch_loan_result[0]->lender_fee / 100);
											}
											if($row->hud == '901')
											{
												// calculate_days
												
												$month_days = 0;
												// calculate_per_day $
												$calc_of_year = $fetch_loan_result[0]->calc_of_year;
												if($calc_of_year == 1)
												{
													$calc_of_year = 360;
													$month_days = 30;
												}
												elseif($calc_of_year == 2)
												{
													$calc_of_year = 365;
													$month_days = date("t");
												}
												else
												{
													$calc_of_year = 0;
												}
												
												//Item #901 should only show a number if Collect First Payment at Close is Yes on the Loan Term window.
												
												
												$paid_to_other_901 	= '';
												$cal_of_day 		= '';
												$per_day 			= '0';
												
/* 													 echo '<pre>';
												print_r($fetch_loan_result);
												echo '</pre>';  */
												
												if($fetch_loan_result[0]->first_payment_collect == 2)
												{  
													$per_day = ($fetch_loan_result[0]->loan_amount / $calc_of_year) * ($fetch_loan_result[0]->intrest_rate / 100);
													$current_date_day = number_format(date('d'));
													
													$cal_of_days = $month_days;
													
													$cal_of_days = ($calc_of_year/12);
													
												}
												
												if($fetch_closing_input_datas['901_input1'])
												{
													$cal_of_days = $fetch_closing_input_datas['901_input1'];
												}
												
												$paid_to_other_901 = $per_day * $cal_of_day;
												$row_items = 'Interest for <input class="small_input" name="901_input1" value="'.$cal_of_days.'" > days at $<input class="small_input" name="901_input2" value="'.number_format($per_day,2).'"> per day';
																																									}
											elseif($row->hud == '1001')
											{
												$row_items = 'Hazard Insurance:<input class="small_input_day" name="1001_input1" value="'.$fetch_closing_input_datas['1001_input1'].'"> months at $<input class="small_input" name="1001_input2" value="'.$fetch_closing_input_datas['1001_input2'].'"> / mo';
											}
											elseif($row->hud == '1002')
											{
												$row_items = 'Mortgage Insurance:<input class="small_input_day" name="1002_input1" value="'.$fetch_closing_input_datas['1002_input1'].'"> months at $<input class="small_input" name="1002_input2" value="'.$fetch_closing_input_datas['1002_input2'].'">/ mo';
											}
											elseif($row->hud == '1004')
											{
												$row_items = 'CO. Property Taxes:<input class="small_input_day" name="1004_input1" value="'.$fetch_closing_input_datas['1004_input1'].'"> months at $<input class="small_input" name="1004_input2" value="'.$fetch_closing_input_datas['1004_input2'].'">/ mo';
											}
											else
											{
												$row_items = $row->items;
											}
											?>

										<?php

										if($row->hud == '901' || $row->hud == '815' || $row->hud == '816' || $row->hud == '1001' ||$row->hud == '1002' || $row->hud == '1004'){ ?>

												<td><?php echo $row_items; ?></td>

										<?php	}else{ ?> 

												<td><?php echo $row_items ? $row_items : $closing_statement_item_loads[$row->hud];?></td>

										<?php }	?>

											
											<?php
											
											 if($row->hud == 801){
											?>	
											<td><input type="text" class="number_only amount_format" name="paid_to_other_<?php echo $row->id;?>" value="<?php echo number_format($row->paid_to_other,2); ?>" readonly></td>
											
											<?php	
											}  
											elseif($row->hud == 901)
											//if($row->hud == 901)
											{
												?>
												<!--<td><input type="text" class="number_only amount_format" name="paid_to_other_<?php echo $row->id;?>" value="<?php echo '$'.number_format($row->paid_to_other,2); ?>"></td>-->
												
												<td><input type="text" class="number_only amount_format" name="paid_to_other_<?php echo $row->id;?>" value="<?php echo number_format($paid_to_other_901,2); ?>" ></td>
												<?php
											}
											
											else if($row->hud == 1101 )
											{
												
											?>
												<td><input type="text" class="number_only amount_format" name="paid_to_other_<?php echo $row->id;?>" value="<?php echo ($row->paid_to_other != 0.00) ? number_format($row->paid_to_other,2) : number_format(500,2) ;?>"></td>
										<?php }
										else if($row->hud == 1105 )
											{
												
											?>
												<td><input type="text" class="number_only amount_format" name="paid_to_other_<?php echo $row->id;?>" value="<?php echo ($row->paid_to_other != 0.00) ? number_format($row->paid_to_other,2) : number_format(450,2) ;?>"></td>
										<?php } 
										else if($row->hud == 1106 )
											{
												
											?>
												<td><input type="text" class="number_only amount_format" name="paid_to_other_<?php echo $row->id;?>" value="<?php echo ($row->paid_to_other != 0.00) ? number_format($row->paid_to_other,2) : number_format(120,2) ;?>"></td>
										<?php } 
										else if($row->hud == 1108 )
											{
												
											?>
												<td><input type="text" class="number_only amount_format" name="paid_to_other_<?php echo $row->id;?>" value="<?php echo ($row->paid_to_other != 0.00) ? number_format($row->paid_to_other,2) : number_format(650,2) ;?>"></td>
										<?php } 
										else if($row->hud == 1109 )
											{
												
											?>
												<td><input type="text" class="number_only amount_format" name="paid_to_other_<?php echo $row->id;?>" value="<?php echo ($row->paid_to_other != 0.00) ? number_format($row->paid_to_other,2) : number_format(300,2) ;?>"></td>
										<?php }
										
										else if($row->hud == 1201 )
											{
												
											?>
												<td><input type="text" class="number_only amount_format" name="paid_to_other_<?php echo $row->id;?>" value="<?php echo ($row->paid_to_other != 0.00) ? number_format($row->paid_to_other,2) : number_format(150,2) ;?>"></td>
										<?php } 
										else
											{
											?>
												<td><input type="text" class="number_only amount_format" name="paid_to_other_<?php echo $row->id;?>" value="<?php echo number_format($row->paid_to_other,2); ?>"></td>
										<?php } ?>
											<?php //if($row->hud == 808){ ?>
												
											<!--<td><input type="text" class="number_only amount_format" name="paid_to_broker_<?php// echo $row->id;?>" value="<?php// echo number_format($paid_to_broker,2); ?>" readonly></td>-->

											<!--<td><input type="text" class="number_only amount_format" name="paid_to_broker_<?php// echo $row->id;?>" value="<?php //echo number_format($row->paid_to_broker,2); ?>"></td>-->	
												
											<?php// }else{?>

											<?php if($row->hud == 801){ ?>
												<td><input type="text" class="number_only amount_format" name="paid_to_broker_<?php echo $row->id;?>" value="<?php echo number_format($paid_to_broker,2); ?>" readonly></td>
											<?php }else{ ?>
												<td><input type="text" class="number_only amount_format" name="paid_to_broker_<?php echo $row->id;?>" value="<?php echo number_format($row->paid_to_broker,2); ?>"></td>
											<?php } ?>

										<?php if($row->hud == 801){ ?>
												<td><input type="text" class="number_only amount_format"  value="<?php echo number_format($row->paid_to_other+$paid_to_broker,2); ?>" disabled></td>
											<?php }else{ ?>
										  <td><input type="text" class="number_only amount_format"  value="<?php echo number_format($row->paid_to_other+$row->paid_to_broker,2); ?>" disabled></td>
											<?php }?>

										<?php if($row->hud == 1101 || $row->hud == 1105 || $row->hud == 1106 || $row->hud == 1108 || $row->hud == 1109 ){ ?>
											
												<td><input style = "width: 160px;" type="text"  name="paid_to_<?php echo $row->id;?>" value="<?php echo $row->paid_to ? $row->paid_to : 'Title Company'; ?>"></td>
										<?php }elseif($row->hud == 801 || $row->hud == 810 || $row->hud == 813 || $row->hud == 815){?>
										
												<td><input style = "width: 160px;" type="text"  name="paid_to_<?php echo $row->id;?>" value="<?php echo $row->paid_to ? $row->paid_to : 'TaliMar Financial'; ?>" ></td>

										<?php }elseif($row->hud == 814 || $row->hud == 1005 || $row->hud == 1006){?>
										
												<td><input style = "width: 160px;" type="text"  name="paid_to_<?php echo $row->id;?>" value="<?php echo $row->paid_to ? $row->paid_to : 'FCI Lender Services'; ?>" ></td>
												
										<?php }elseif($row->hud == 904 || $row->hud == 1201 || $row->hud == 1202){ ?>
										
												<td><input style = "width: 160px;" type="text"  name="paid_to_<?php echo $row->id;?>" value="<?php echo $row->paid_to ? $row->paid_to :'County'; ?>"></td>
											
										<?php }else{ ?>
										
												<td><input style = "width: 160px;" type="text"  name="paid_to_<?php echo $row->id;?>" value="<?php echo $row->paid_to; ?>"></td>
										<?php } ?>									
											
											<td>
												<input type="checkbox" onchange="closing_statement_netfunding_checkbox(this)" <?php if($row->net_funding == 1){ echo 'checked'; } ?>>
												<input type="hidden" value="<?php echo $row->net_funding; ?>" name="net_funding_<?php echo $row->id; ?>" id="net_funding">
											</td>
											<?php 
											
											
											if($row->direct_to_servicer == 1){
												$checked11 = 'checked ="checked"';
												$dv = '1';
											
											}elseif($row->direct_to_servicer == 0){
													$checked11 = '';
													$dv = '0';
											
											}elseif(($row->hud == '813' || $row->hud == '814') && ($row->direct_to_servicer == '')){ 
													$checked11 = 'checked ="checked"';
													$dv = '1';
											
											}else{
												
													$checked11 = '';
													$dv = '0';
												} ?>
											<td>
												<input type="checkbox" onchange="closing_statement_directtoservicer_checkbox(this)" name = "servicer_chk[]" data-servicer = '<?php echo $row->hud;?>' <?php echo $checked11; ?>> 
												<input type="hidden" value="<?php echo $dv;?>" name="direct_to_servicer_<?php echo $row->id; ?>" id="direct_to_servicer">
											</td>
											
											<!--<td>
											
											<input type="checkbox" onchange="closing_statement_loan_reserves_checkbox(this)" name = "loan_reserves[]" <?php if($row->loan_reserves == 1){ echo 'checked'; } ?>> 
											
											<input type="hidden" value="<?php 
											echo $row->loan_reserves; ?>" name="loan_reserves_<?php echo $row->id; ?>" id="loan_reserves">
											
											</td>-->
											
											<td>
												<input type="checkbox" onchange="closing_statement_prepaid_checkbox(this)" <?php if($row->prepaid_charge == 1){ echo 'checked'; } ?>> 
												<input type="hidden" value="<?php echo $row->prepaid_charge; ?>" name="prepaid_charge_<?php echo $row->id; ?>" id="prepaid_charge">
											</td>
											
											<td>
											
												<input type="checkbox" onchange="lender_statement_checkbox(this)" <?php  $lender_statement = 0; 
												/*if($row->lender_statement == 1 || $row->hud == 808 || $row->hud == 810 || $row->hud == 812){ echo 'checked';  $lender_statement = '1';
												} */
												if($row->lender_statement == 1 ){ echo 'checked';  $lender_statement = '1';
												}
												?>> 
												<input type="hidden" value="<?php echo $lender_statement; ?>" name="lender_statement_<?php echo $row->id; ?>" id="lender_statement">
											</td>
											
											</tr>
											
													
													
											
											<?php
											$row_hubrow = $row->hud;
											}
									}
										
										?>
										<tr id="<?php echo $row_hubrow; ?>">
											<th colspan="11"><a id="<?php echo $row->hud; ?>" onclick="add_row_closing_items(this)"><i class="fa fa-plus" aria-hidden="true"></i></a></th>
										</tr>
										<?php
									}
						
									?>
								</tbody>
								
								<tfoot>
									<tr>
										<th colspan="3">Sub Total:</th>
										<td><strong><?php echo '$'.number_format($fetch_sum_closing_statement_item[0]->sum_paid_to_other,2); ?></strong></td>
										<td><strong><?php echo '$'.number_format($fetch_sum_closing_statement_item[0]->sum_paid_to_broker,2); ?></strong></td>
										<td colspan="6"></td>
									</tr>
									<tr>
										<th colspan="5">Total:</th>
										<?php 
										$total = $fetch_sum_closing_statement_item[0]->sum_paid_to_other + $fetch_sum_closing_statement_item[0]->sum_paid_to_broker;
										?>
										<td><strong><?php echo '$'.number_format($total,2)?></strong></td>
										<td><strong></strong></td>
										<td colspan="6"></td>
									</tr>
									<tr>
										<th colspan="3">Suspense Balance:</th>
										<?php 
										
											$sumss +=$fetch_suspense_balance[0]->sum_paid_to_other ? $fetch_suspense_balance[0]->sum_paid_to_other : '';
										
									
										?>
										<td><strong><?php echo '$'.number_format($sumss,2)?></strong></td>
										<td><strong></strong></td>
										<td colspan="6"></td>
									</tr>
								</tfoot>
							
							</table>
							
							<div class="additional-info-closing-new">
							
							<div class="row extra-padding">
								<div class="col-md-6">
									<strong>Compensation to Broker (Not Paid Out of Loan Proceeds):</strong>
								</div>
								<div class="col-md-3">
								</div>
								<div class="col-md-3">
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-6">
									<label>Mortgage Broker Commission / Fee</label>
								</div>
								<div class="col-md-3">
								</div>
								<div class="col-md-3">
									<input type="text" class="number_only amount_format txtField" name="mortage_broker_com" value="<?php echo '$'.number_format($fetch_closing_input_datas['mortage_broker_com'],2);?>" >
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-6">
									<label>Any Additional Compensation from Lender</label>
								</div>
								<div class="col-md-3">
									<input type="radio" name="additional_compensation_checkbox" value="1" <?php if($fetch_closing_input_datas['additional_compensation_checkbox'] == 1){ echo 'checked'; } ?> > Yes <input type="radio" name="additional_compensation_checkbox" value="0" <?php if($fetch_closing_input_datas['additional_compensation_checkbox'] == 0){ echo 'checked'; } ?> > No
								</div>  
								<div class="col-md-3">
									<input type="text" class="number_only amount_format txtField" name="additional_compensation_amount" value="<?php echo '$'.number_format($fetch_closing_input_datas['additional_compensation_amount'],2);?>">
								</div>
							</div> 
							
							<div class="row extra-padding">
								<div class="col-md-6">
									<strong>Proposed Loan Amount</strong>
								</div>
								<div class="col-md-3">
								</div>  
								<div class="col-md-3">
									<input type="text" class="number_only amount_format txtField" name="proposed_loan_amount" value="<?php echo '$'.number_format($fetch_loan_result[0]->loan_amount,2);?>">
								</div>
							</div>  
							
							<div class="row">
								<div class="col-md-6">
									<label>Initial Commissions, Fees, Costs, and<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Expenses Summarized on Page 1:</label>
								</div>
								<div class="col-md-3">
								&nbsp;<br>
									<input type="text" class="number_only amount_format txtField" name="fee_cost_expence" value="<?php echo '$'.number_format($total,2);?>">
								</div>  
								<div class="col-md-3">
									
								</div>
							</div>  
							
							<div class="row">
								<div class="col-md-6">
									<label>Payment of Other Obligations (List)</label>
								</div>
								<div class="col-md-3">
									<input type="text" class="number_only amount_format txtField" name="payment_of_other_obligation" value="<?php echo '$'.number_format($fetch_closing_input_datas['payment_of_other_obligation'],2);?>">
								</div>  
								<div class="col-md-3">
									
								</div>
							</div> 
							
							<div class="row">
								<div class="col-md-6">
									<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Credit Life and/or Disability Insurance</label>
								</div>
								<div class="col-md-3">
									<input type="text" class="number_only amount_format txtField" name="credit_life_insurance" value="<?php echo '$'.number_format($fetch_closing_input_datas['credit_life_insurance'],2);?>">	
								</div>
								<div class="col-md-3">
								
								</div>  
								
							</div> 
							
							<div class="row">
								<div class="col-sm-12">
									<table class="closing-statement-deduction" id="closing-statement-deduction" style="width:100%;">
										<tbody>
										<?php
										$deduction = $total;
										if(isset($fetch_closing_deduction))
										{
											foreach($fetch_closing_deduction as $dedct)
											{
												$deduction = $deduction + $dedct->value;
												?>
												<tr class="input-border-zero">
													<td style = "width: 50%;"><input type="text" class = "txtField" name="item_deduction_<?php echo $dedct->id; ?>" value="<?php echo $dedct->items; ?>"></td>
													
													<td style = "width: 25%;"><input type="text" class = "txtField" name="amount_deduction_<?php echo $dedct->id; ?>" onchange="amount_format_change(this)" value="<?php echo '$'.number_format($dedct->value,2); ?>"></td>
													<td style = "width: 25%;"></td>
												</tr>
												<?php
											}
										}
										$deduction = $deduction + $fetch_closing_input_datas['credit_life_insurance'];
										?>
											
										</tbody>
										<tfoot>
											<tr>
												<td  colspan="2"><a onclick="closing_statement_deductions_add()"><i class="fa fa-plus" aria-hidden="true"></i></a></td>
												<td></td>
											</tr>
										</tfoot>
									</table>
									
									<table id="closing-statement-deduction-hidden" style="display:none;">
									<tr>
										<td style = "width: 50%;"><input type="text" name="item_deduction[]"></td>
										
										
										<td style = "width: 25%;"><input type="text" class = "txtField" name="amount_deduction[]"onchange="amount_format_change(this)"></td>
										<td style = "width: 25%;"></td>
									</tr>
									</table>
								</div>
							</div>	
							
							<div class="row">
								<div class="col-md-6">
									<label>Subtotal of All Deductions</label>
								</div>
								<div class="col-md-3">
									
								</div>  
								<div class="col-md-3">
									<input type="text" class="number_only amount_format txtField" name="sub_total_all_deduction" value="<?php echo '$'.number_format($deduction,2)?>">
								</div>
							</div> 
							  
							<?php
									$estimate_cash_close_amount = $fetch_loan_result[0]->loan_amount - $deduction;
							?>
							
							<div class="row">
								<div class="col-md-6">
									<label>Estimated Cash at Close:  <input type="radio" name="estimate_cash_at_close" value="1" <?php if($estimate_cash_close_amount >= 0){ echo 'checked'; } ?> > To You <input type="radio" value="2" name="estimate_cash_at_close"<?php if($estimate_cash_close_amount < 0){ echo 'checked'; } ?> > That you must pay</label>
								</div>
								<div class="col-md-3">
									
								</div>  
								<div class="col-md-3">
								
									<input type="text" class="number_only amount_format txtField" name="estimate_cash_close_amount txtField" value="<?php echo '$'.number_format($estimate_cash_close_amount,2);?>">
								</div>
							</div> 
								
							</div>
							
							
							<table id="hidden_closing_statemnet_new" style="display:none;">
							<tr>
								<td><a href="javascript:void(0);" id="" onclick="delete_hud(this)"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
								<td><input name="hud[]"></td>
								<td><input type="text" name="items[]"></td>
								<td><input type="text" name="paid_to_other[]" class="number_only amount_format"  ></td>
								<td><input type="text" name="paid_to_broker[]" class="number_only amount_format" ></td>
								<td><input type="text" disabled></td>
								<td><input  style = "width: 160px;" type="text" name="paid_to[]" ></td>
								<td>
									<input type="checkbox" onchange="closing_statement_netfunding_checkbox(this)">
									<input type="hidden" value="0" id="net_funding" name="net_funding[]">
								</td>
								<td>
									<input type="checkbox" data-servicer= '' name = "servicer_chk[]" onchange="closing_statement_directtoservicer_checkbox(this)">
									<input type="hidden"  value="0" id="direct_to_servicer" name="direct_to_servicer[]">
								</td>
								<!--<td>
									<input type="checkbox" onchange="closing_statement_loan_reserves_checkbox(this)"> 
									<input type="hidden" value="0" id="loan_reserves" name="loan_reserves[]">
								</td>-->			
								<td>
									<input type="checkbox" onchange="closing_statement_prepaid_checkbox(this)"> 
									<input type="hidden" value="0" id="prepaid_charge" name="prepaid_charge[]">
								</td>
											
								<td>
									<input type="checkbox" onchange="lender_statement_checkbox(this)"> 
									<input type="hidden" value="0" id="lender_statement" name="lender_statement[]">
								</td>
							</tr>
							</table>
						</div>
					</div>
					
					<div class="modal-footer">
						<!--<button type="button" onclick = "open_custom_popup('closing_statement')" class="btn default" >Close</button>-->
						
						<!--<a href="<?php// echo base_url()?>load_data/print_closing_statement<?php// echo $loan_id;?>" class="btn blue">Closing Statement</a>-->
						
						<!--<a href="<?php// echo base_url()?>load_data/print_re885_disclosure/<?php echo $talimar_loan;?>" class="btn blue">RE885</a>-->
						
						<button type="submit" value = "save" class="btn blue">Save</button>
						<input type="hidden" id = "closing_statement_action" name = "action" value = "save" />
						
						<a class="btn default" data-toggle="modal" href="#check_closing_statement">Close</a>
						
						
						<!------- CUSTOM POPUP MODAL STARTS --------------->
								<div id="closing_statement_confirmOverlay" class="closing_statement_confirmOverlay" style = "display:none">
									<div id="closing_statement_confirmBox">

										<p>Do you want to Save Information?
											<a class="btn blue borrower_save_button" href="javascript:void(0);" onclick = "save_info('closing_statement')">Yes<span></span></a>
											<a onclick = "close_custom_popup('closing_statement')" data-dismiss="modal" class="btn default borrower_save_button">No<span></span></a>
										</p>

									</div>
								</div>							
						<!------- CUSTOM POPUP MODAL ENDS --------------->
					</div>
				</form>
			</div>
		</div>
</div>