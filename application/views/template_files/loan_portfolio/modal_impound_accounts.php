<div class="modal fade" id="impound_accounts" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id = "frm_impound_acct" action="<?php echo base_url();?>form_impound_accounts" method="POST" >
			
				<!-----------Hidden fields----------->
				<input type="hidden" name = "talimar_no" value = "<?php echo $modal_talimar_loan; ?>">
				<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
				<!-----------End of hidden Fields---->
				
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<!--<h4 class="modal-title">Monthly Payment: #<?php //echo $modal_talimar_loan;?></h4>-->
					<h4 class="modal-title">Monthly Payment: <?php echo $property_addresss;?></h4>
				</div>
				<div class="modal-body">
				
					
					<div class="row">
						<div style="width: 25%;">
							<label>Impound Account:</label>
							<select class="form-control" name="impound_yesno" onchange="disable_impound(this)">
								<?php 
								foreach($no_yes_option as $key => $row)
								{
									?>
									
									<option value="<?php echo $key; ?>" <?php if($fetch_impound_account[0]->impounded == $key){echo 'selected';}?>><?php echo $row; ?></option>
									
									<?php
								}
								?>
							</select>
						</div>
					</div>
					
					<div class="portlet-body rc_class">
					<!--<div class="row property_loan_row">
						
						
						<div class="col-sm-12">
							<strong>Will Lender Impound Payments &nbsp;&nbsp;&nbsp;</strong>
							<select name ="lender_impound_payment" onchange="lender_impound_payment_checkboxes(this)">
								<option value = "2" <?php if(isset($fetch_extra_details)){ if($fetch_extra_details[0]->lender_impound_payment== 2){ echo 'selected'; }}?>>Select</option>
								<?php 
								foreach($yes_no_option as $key => $row)
								{
									?>
									<option value="<?php echo $key; ?>" <?php if(isset($fetch_extra_details)){ if($fetch_extra_details[0]->lender_impound_payment== $key){ echo 'selected'; } }?> ><?php echo $row; ?></option>
									<?php
								}
								?>
							</select>
						</div>
					</div>--->
					  <div class="table-responsive">	
						<table class="table table-bordered table-striped table-condensed flip-content" id="table_impound_accounts">
							<thead>
								<tr>
									<th></th>
								
									<th style="width:50% !important">Items:</th>
									<th> Monthly Payment</th>
									<!--<th>Impound?</th>-->
								</tr>
							</thead>
							<tbody>
								
								<?php
				
								
								// if($fetch_extra_details[0]->lender_impound_payment == 0)
								// {
									// $impounded_check_box = 'disabled';
								// }
								
								if(isset($fetch_impound_account))
								{
									$aa = 0;
									$rowcnt = 0;
									//$impound_amountsss = 0;
									foreach($fetch_impound_account as $key=>$row)
									{
										$rowcnt++;										
										
										$readonly = '';
										
										if($row->items == 'Mortgage Payment')
										{
						
							            
							
									  

											// $impound_amount = ($fetch_loan_result[0]->intrest_rate/100) * $fetch_loan_result[0]->loan_amount / 12;
											
											// $aa += ($fetch_loan_result[0]->intrest_rate/100) * $fetch_loan_result[0]->loan_amount / 12;
											 $impound_amount =($outstanding_balance*($fetch_loan_result[0]->intrest_rate)/100)/12;
											 //$impound_amountsss[] =($outstanding_balance*($fetch_loan_result[0]->intrest_rate)/100)/12;
											  $aa +=($outstanding_balance*($fetch_loan_result[0]->intrest_rate)/100)/12;
											// $aa += ($fetch_loan_result[0]->intrest_rate/100) * $fetch_loan_result[0]->loan_amount / 12;
											$onchange =  '';
											
											if($row->impounded == '2'){
												$readonly = 'disabled';
											}else{
												$readonly = '';
											}
											
										
										}

										
										if($row->items == 'Property / Hazard Insurance Payment')
										{
											
											$impound_amount = isset($loan_property_insurance_result_with_1) ? ($loan_property_insurance_result_with_1->annual_insurance_payment / 12) : ($row->amount ? $row->amount : 0);
											
											if($row->impounded == 1){
												
												$aa += isset($loan_property_insurance_result_with_1) ? ($loan_property_insurance_result_with_1->annual_insurance_payment / 12) : ($row->amount ? $row->amount : 0);

												
											}

											$impound_amountsss[] += isset($loan_property_insurance_result_with_1) ? ($loan_property_insurance_result_with_1->annual_insurance_payment / 12) : ($row->amount ? $row->amount : 0);


											$onchange = '';
											if($row->impounded == '2'){
												$readonly = 'disabled';
											}else{
												$readonly = '';
											}
											
										}
										
										if($row->items == 'County Property Taxes')
										{
											$impound_amount = isset($primary_loan_property_taxes) ? ($primary_loan_property_taxes->annual_tax / 12) : ($row->amount ? $row->amount : 0);
											
											if($row->impounded == 1){
												
											
												$aa += isset($primary_loan_property_taxes) ? ($primary_loan_property_taxes->annual_tax / 12) : ($row->amount ? $row->amount : 0);

												
											}

											$impound_amountsss[] += isset($primary_loan_property_taxes) ? ($primary_loan_property_taxes->annual_tax / 12) : ($row->amount ? $row->amount : 0);

											$onchange = '';
											if($row->impounded == '2'){
												$readonly = 'disabled';
											}else{
												$readonly = '';
											}
										
										}
										
										if($row->items == 'Mortgage Insurance')
										{
											$impound_amount = 0;
											// $impound_amount = isset($loan_property_insurance_result_with_2) ? ($loan_property_insurance_result_with_2->annual_insurance_payment / 12) : ($row->amount ? $row->amount : 0);
											
											if($row->impounded == 1){
												
												/* $aa += isset(
												$loan_property_insurance_result_with_2) ? ($loan_property_insurance_result_with_2->annual_insurance_payment / 12) : ($row->amount ? $row->amount : 0); */
												$aa += 0;
											}
											$onchange = '';
											if($row->impounded == '2'){
												$readonly = 'disabled';
											}else{
												$readonly = '';
											}
											
											
										
										}
										
										if($row->items == 'Flood Insurance')
										{
											$impound_amount = 0;
											// $impound_amount = isset($loan_property_insurance_result_with_3) ? ($loan_property_insurance_result_with_3->annual_insurance_payment / 12) : ($row->amount ? $row->amount : 0);
											
											if($row->impounded == 1){
												
												
												// $aa += isset($loan_property_insurance_result_with_3) ? ($loan_property_insurance_result_with_3->annual_insurance_payment / 12) : ($row->amount ? $row->amount : 0);
												$aa += 0;
											}
											$onchange = '';
											if($row->impounded == '2'){
												$readonly = 'disabled';
											}else{
												$readonly = '';
											}
											
										}

										if($row->items == 'School Tax' || $row->items == 'City Tax' || $row->items == 'Water / Sewer Tax' || $row->items == 'Township Tax' || $row->items == 'Other Tax' || $row->items == 'Wind Insurance'){

											$impound_amount = $row->amount;
										}
										
										
										
										// if(($row->position != '0') && ($row->position != '1') && ($row->position != '2') &&
										//  ($row->position != '3') && ($row->position != '4')){
											
										// 	  $impound_amount = $row->amount;
										// 	if($row->impounded == 1){
											
										// 		$aa += isset($row->amount) ? ($row->amount) : 0; 
										// 	} 
										// }	
										
										
									
										
										
										?>
										
										<tr id ="row_<?php echo $rowcnt;?>">
											<td><a href = "javascript:void(0);"  id="<?php echo $row->id;?>"  onclick ="delete_monthly_payments(this)" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
											
											<!--<td><a href = "javascript:void(0);"  onclick ="delete_monthly_payments()" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>-->
											
											
											<td><?php echo $row->items; ?>:</td>
											
											<td><input type = "hidden" name = "amtval_<?php echo $row->id;?>" value = "<?php echo number_format($impound_amount,2)?>"/>
											<input type="text" name="amount_<?php echo $row->id; ?>" class="notedit" onchange="amount_format_change(this); <?php echo $onchange;?>" value="<?php echo '$'.number_format($impound_amount,2) ; ?>"  <?php echo $readonly; ?> ></td>
											
											
											
										</tr>
										
										<?php
										
										// $total_impound_amount = $total_impound_amount + $impound_amount;
										// $total_impound_amount = $total_impound_amount;
									}
								}
								?>
								</tbody>
								
							</table>
						   </div>	
							<a id="impound_accounts_add_row" href ="javascript:void(0);" onclick = "add_row_impound_accounts(this)"><button type="button" class="btn blue"><i class="fa fa-plus" aria-hidden="true"></i> Add</button></a>	
							<table class="table table-condensed flip-content">		
								<tbody>
									<tr> 
										<td colspan = '3'><strong>Impounds included in Payment:</strong> </td>
										<td  style = "text-align:center;"><input type="text" id = "monthly_impound_total" value="<?php echo '$'.number_format($sum_impound_account,2); ?>" onchange = "cal_monthly_payment()"></td>
										<td></td>
									</tr>
									<tr> 
										<td colspan = '3'><strong>Total Monthly Payment:</strong> </td>
										<td style = "text-align:center;"><input type="text" id = "monthy_payment_total" value="<?php echo '$'.number_format($aa,2); ?>" onchange = "cal_monthly_payment()"></td>
										<td></td>
									</tr>
									<?php// print_r($impound_amountsss);?>

									<?php

										$allSumlessmortage = 0;
										foreach ($impound_amountsss as $key => $value) {
											$allSumlessmortage += $value;
										}

									?>
									<tr> 
										<td colspan='3'><strong>Monthly Payment (less mortgage payment):</strong></td>
										<td style = "text-align:center;"><input type="text" id = "monthy2_payment_total" value="<?php echo '$'.number_format($allSumlessmortage,2); ?>"></td>
										<td></td>
									</tr>
								<!--<tr> 
									<th>Monthly Payment(w/ Impound):</th>
									<td><input type="text" id = "monthly_payment_impound" value="<?php echo '$'.number_format($aa + $sum_impound_account,2); ?>" ></td>
									<td></td>
									<td></td>
								</tr>-->
								</tbody>
						</table>
						<!--<div class="row property_loan_row">
						
						
							<div class="col-sm-12">
						<?php
							if($fetch_extra_details[0]->lender_impound_payment == '1'){
								$checked1 =  'checked';
								$checked0 =  '';
							}else{
								$checked0 =  'checked';
								$checked1 =  '';
							}
						
						?>
							<strong>Does the Loan include an Impound Account: &nbsp;&nbsp;&nbsp;</strong>
							<input type = "radio" name = "lender_impound_payment" value = "1" <?php echo $checked1;?>> YES
							<input type = "radio" name = "lender_impound_payment" value = "0" <?php echo $checked0;?>> NO
						</div>
					</div>-->
						<table id="table_impound_accounts_hidden" style="display:none;">
							<tr>
								<td><input data-id = "<?php echo $row->id;?>" type = "checkbox" id = "chkbx_<?php echo $row->id;?>" name = "checkbox_field[]" onclick = "fill_checkbox_val('<?php echo $row->id;?>');" /><input type = "hidden" id = "checkbox_delete_<?php echo $row->id;?>" name = "checkbox_delete[]"/></td>
								
								<td><input type="text" name="items[]"></td>
								<td><input type="text" name="amount[]" class="number_only number_format" onchange="amount_format_change(this)" ></td>
								
							</tr>
						</table>
						
					</div>
				</div>
				
				<script>
					
					function disable_impound(that){
						
						//alert(that.value);
						
						if(that.value == '2'){
							$('input .notedit').attr('disabled', 'disabled');
						}else{
							$('input .notedit').attr('disabled', '');
						}
													
					}
				
				</script>
				<div class="modal-footer">
					
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<input type="submit" value = "Save" class="btn blue" >
					<!--<button id="m_s_addrow" value="" class="btn blue" >Add</button>-->
				</div>
				
			</form>	
		</div>
	</div>
</div>