<div class="modal fade bs-modal-lg" id="diligence_materials" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg" style="width:1100px !important;">
			<div class="modal-content">
				<form method = "POST" action ="<?php echo base_url();?>form_underwriting_items" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<!--<h4 class="modal-title">Diligence Materials: #<?php echo ''.$talimar_loan; ?></h4>-->
						<h4 class="modal-title">Due Diligence Materials: <?php echo $property_addresss; ?></h4>
					</div>
					<?php
					foreach ($underwriting_items_option as $keay => $valuea) {		
  						$new_underwriting_items_optiont[$keay][]= $valuea;					
					}
     				$due_del_array=array(2,3,4,24,5,12,13,16,17,18,19,6,11,20,25,7,8,9,23,22,10,26,21,114,118,119,120,121,102,103,104,105,110,115,116,122,117,111,112,202,203,204,205,206,215,207,208,213,214,209,216,212,301,302,303,401,402,403,404,405,501,502,503,504,505,506,507,508);
					if(isset($due_del_array))
					{
					 	foreach($due_del_array as $numberssss){						
							foreach($new_underwriting_items_optiont[$numberssss] as $rowz){								
								$due_del_array_final[$numberssss] = $rowz;
							}
						}
					}
					?>
					<div class="modal-body">
						<div class="portlet-body rc_class ">
							<div class="row">
								<?php
								$countDueitem = 0;
								foreach($due_del_array_final as $kex => $vaaal)
								{ 
									foreach($fetch_underwriting1 as $row){ 								
										if($kex == $row->items_id){
											if($row->items_id>500 && $row->items_id<600){ 
												$countDueitem++;
												if($countDueitem%5==0){
													?>
													</div>
													<div class="row" style="margin-top: 10px;">
													<?php
												}
												?>
												<div class="col-md-3">
													<input type="hidden" name="id_<?php echo $row->id;?>" value="<?php echo $row->id;?>">
													<input type="hidden" name="items_id" value="<?php echo $row->items_id;?>">
													<input type="checkbox" name="DueDiligenceHead[]" onchange="allDueDiligenceHeadingAction(this)" value="<?php echo $underwriting_items_option[$row->items_id] ? $underwriting_items_option[$row->items_id] : $row->items ;?>" class="diligence_materials_heading" item_no_id="underwriting_required_<?php echo $row->items_id;?>" <?php if($row->required == 1){ echo 'checked'; } ?>><span><?php echo $underwriting_items_option[$row->items_id] ? $underwriting_items_option[$row->items_id] : $row->items ;?></span>
													<input type="hidden" value="<?php echo $row->required; ?>" name="required_<?php echo $row->id; ?>" id="underwriting_required_<?php echo $row->items_id;?>" >
												</div>
												<?php
											}

										}
									}
								}
								?>
							</div>
						
							<!-- <div class="col-md-3">
								<input type="checkbox" name="DueDiligenceHead[]" onchange="allDueDiligenceHeadingAction(this)" value="Fix and Flip Loan" class="diligence_materials_heading"><span>Fix and Flip Loan</span>
							</div>
							<div class="col-md-3">
								<input type="checkbox" name="DueDiligenceHead[]" onchange="allDueDiligenceHeadingAction(this)" value="Bridge Purchase Loan" class="diligence_materials_heading"><span>Bridge Purchase Loan</span>
							</div>
							<div class="col-md-3">
								<input type="checkbox" name="DueDiligenceHead[]" onchange="allDueDiligenceHeadingAction(this)" value="Bridge Refinance Loan" class="diligence_materials_heading"><span>Bridge Refinance Loan</span>
							</div> 
							<div class="col-md-3">
								<input type="checkbox" name="DueDiligenceHead[]" onchange="allDueDiligenceHeadingAction(this)" value="Construction Loan" class="diligence_materials_heading"><span>Construction Loan</span>
							</div>
						</div>
						<div class="row" style="margin-top: 10px;">
							<div class="col-md-3">
								<input type="checkbox" name="DueDiligenceHead[]"   onchange="allDueDiligenceHeadingAction(this)" value="30 Year Loan (Refi)" class="diligence_materials_heading"><span>30 Year Loan (Refi)</span>
							</div>
							<div class="col-md-3">
								<input type="checkbox" name="DueDiligenceHead[]"  onchange="allDueDiligenceHeadingAction(this)" value="30 Year Loan (Purchase)"  class="diligence_materials_heading"><span>30 Year Loan (Purchase)</span>
							</div>
						
							<div class="col-md-3">
								<input type="checkbox" name="DueDiligenceHead[]" onchange="allDueDiligenceHeadingAction(this)" value="Check All" class="diligence_materials_heading"><span>Check All</span>
							</div>
							<div class="col-md-3">
								<input type="checkbox" name="DueDiligenceHead[]" onchange="allDueDiligenceHeadingAction(this)" value="Uncheck All" class="diligence_materials_heading"><span>Uncheck All</span>
							</div>
						</div> -->
												
						<div class="row">&nbsp; </div>
							<!-----------Hidden fields---->
							
							<input type="hidden" name = "talimar_loan" value = "<?php echo $talimar_loan; ?>">
							<input type="hidden" name="loan_id" value="/<?php echo $loan_id; ?>">
							<!-----------End of hidden Fields---->
							
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" id="underwriting_table">
								
								<thead>
									<tr>
										<?php
										$delligenge_required_checked = '';
										if(isset($fetch_all_input_datas['underwriting_page']['delligenge_required']))
										{
											$delligenge_required_checked = ($fetch_all_input_datas['underwriting_page']['delligenge_required'] == 1) ? 'checked' : '';
										}
										?>
									 	<th style="text-align:left !important;"></th>
										<th style="text-align:left !important;">Required</th>
										<th style="text-align:left !important;">Received</th>
										<th style="text-align:left !important;">Diligence Items</th>
										<th style="text-align:left !important;">Completed By</th>
										<!-- <th style="text-align:left !important;">Page Location</th> -->
									</tr>
								</thead>
								<tbody>
									
								
								<tr>
									 <th style="text-align:left !important;">Item #</th>
									<th style="text-align:left !important;" colspan="4">Borrower Data</th>
									
								</tr>

					<?php
					$countDueitem = 0;
					foreach($due_del_array_final as $kex => $vaaal)
					{ 
						foreach($fetch_underwriting1 as $row){ 								
							if($kex == $row->items_id){
								if($row->type == '1'){ 
									$countDueitem++;
					?>
										
									<tr id="<?php echo $row->items_id; ?>">
										<td style="display:none;">
											<input type="text" name="id_<?php echo $row->id;?>" value="<?php echo $row->id;?>">
											<input type="text" name="items_id" value="<?php echo $row->items_id;?>">
											
										</td>
											<td style="text-align:center !important;" ><?php echo $countDueitem;?></td>
											
											<td style="text-align:center !important;" >
											
												<input type="checkbox" onchange="underwriting_required1(this);" name="req_<?php echo $row->items_id; ?>" class="form-control common" <?php if($row->required == 1){ echo 'checked'; } ?>>
											
												<input type="hidden" value="<?php echo $row->required; ?>" name="required_<?php echo $row->id; ?>" id="underwriting_required_<?php echo $row->items_id;?>" class="underwriting_required_checkbox">
											
											</td >
											
											<td style="text-align:center !important;" >
											
												<input type="checkbox" onchange="underwriting_received1(this)" name="rec_<?php echo $row->items_id; ?>" class="form-control" <?php if($row->received == 1){ echo 'checked'; } ?> >
												
												<input type="hidden" value="<?php echo $row->received; ?>" name="received_<?php echo $row->id; ?>" id="underwriting_received_<?php echo $row->items_id;?>" class="underwriting_received">
												
											</td>
											
											<td><?php echo $underwriting_items_option[$row->items_id] ? $underwriting_items_option[$row->items_id] : $row->items ;?>
												<!--<input type="hidden" name="items[]" value="<?php //echo $row->items;?>" style="width:100%;" readonly>-->
											</td>
										
											<?php if($row->items_id =='4' || $row->items_id =='10'){
												echo '<td>TaliMar Financial</td>';
											}elseif($row->items_id =='2' || $row->items_id =='3' || $row->items_id =='5' || $row->items_id =='12' || $row->items_id =='13' || $row->items_id =='16' || $row->items_id =='17' || $row->items_id =='18' || $row->items_id =='19' || $row->items_id =='6' || $row->items_id =='11' || $row->items_id =='20' || $row->items_id =='7' || $row->items_id =='8' || $row->items_id =='9' || $row->items_id =='23' || $row->items_id =='22'  || $row->items_id =='21' || $row->items_id =='24' || $row->items_id =='25' || $row->items_id =='26'){
												echo '<td>Borrower</td>';
											}else{
												echo '<td></td>';
											}?>
											
										</tr>	
										
									<?php 
										$last_number_items_id = $row->items_id;
									} }}}?>
																		
									<tr>
									<th style="text-align:left !important;">Item #</th>
										<th style="text-align:left !important;" colspan="4">Transaction Documents</th>	
									</tr>
									
							<?php 

						$countDueitem1 = 0;
						foreach($due_del_array_final as $kex => $vaaal)
						{ 

							  foreach($fetch_underwriting1 as $row){
									if($kex==$row->items_id){
										 if($row->type == '2'){ 
										 	$countDueitem1++;
										?>
												
									<tr id="<?php echo $row->items_id; ?>">
										<td style="display:none;">
											<input type="text" name="id_<?php echo $row->id;?>" value="<?php echo $row->id;?>">
											<input type="text" name="items_id" value="<?php echo $row->items_id;?>">
										</td>
									<!--<td style="text-align:center !important;" ><?php echo $row->items_id; ?></td> -->
									<td style="text-align:center !important;" ><?php echo $countDueitem1; ?></td> 
											
										<td style="text-align:center !important;" >
											
											<input type="checkbox" onchange="underwriting_required1(this);" name="req_<?php echo $row->items_id; ?>" class="form-control common" <?php if($row->required == 1){ echo 'checked'; } ?>>
										
											<input type="hidden" value="<?php echo $row->required; ?>" name="required_<?php echo $row->id; ?>" id="underwriting_required_<?php echo $row->items_id;?>" class="underwriting_required_checkbox">
											
										</td >
											
										<td style="text-align:center !important;" >
											
											<input type="checkbox" onchange="underwriting_received1(this)" class="form-control" name="rec_<?php echo $row->items_id; ?>" <?php if($row->received == 1){ echo 'checked'; } ?> >
											
											<input type="hidden" value="<?php echo $row->received; ?>" name="received_<?php echo $row->id; ?>" id="underwriting_received_<?php echo $row->items_id;?>" class="underwriting_received">
												
										</td>
											
										<td><?php echo $underwriting_items_option[$row->items_id] ? $underwriting_items_option[$row->items_id] : $row->items ;?>
											<!--<input type="hidden" name="items[]" value="<?php //echo $row->items;?>" style="width:100%;" readonly>-->
										</td>

										<?php if($row->items_id =='105' || $row->items_id =='110' || $row->items_id =='115' || $row->items_id =='116' || $row->items_id =='117' || $row->items_id =='111' || $row->items_id =='118' || $row->items_id =='119'){
												echo '<td>TaliMar Financial</td>';
											}elseif($row->items_id =='114' || $row->items_id =='102' || $row->items_id =='103' || $row->items_id =='104' || $row->items_id =='112' || $row->items_id =='122' || $row->items_id =='120' || $row->items_id =='121'){
												echo '<td>Borrower</td>';
											}else{
												echo '<td></td>';
											}?>
									</tr>
	
									<?php	
										$last_number_items_id = $row->items_id;
									} } }}?>
								
									
									<tr>
										<th style="text-align:left !important;">Item #</th>
										<th style="text-align:left !important;" colspan="4">Property Information</th>	
									</tr>
									
							<?php 

							$countDueitem11 = 0;

 							foreach($due_del_array_final as $kex => $vaaal)
							  { 
							  foreach($fetch_underwriting1 as $row){
									if($kex ==$row->items_id){

										if($row->type == '3'){ 

											$countDueitem11++;

										?>
												
									<tr id="<?php echo $row->items_id; ?>">
										<td style="display:none;">
											<input type="text" name="id_<?php echo $row->id;?>" value="<?php echo $row->id;?>">
											<input type="text" name="items_id" value="<?php echo $row->items_id;?>">
										</td>
										 
										<!--<td style="text-align:center !important;" ><?php echo $row->items_id; ?></td>-->
										<td style="text-align:center !important;" ><?php echo $countDueitem11; ?></td>
											
										<td style="text-align:center !important;" >
											
											<input type="checkbox" onchange="underwriting_required1(this);" name="req_<?php echo $row->items_id; ?>" class="form-control common" <?php if($row->required == 1){ echo 'checked'; } ?>>
										
											<input type="hidden" value="<?php echo $row->required; ?>" name="required_<?php echo $row->id; ?>" id="underwriting_required_<?php echo $row->items_id;?>" class="underwriting_required_checkbox">
											
										</td >
											
										<td style="text-align:center !important;" >
											
											<input type="checkbox" onchange="underwriting_received1(this)" name="rec_<?php echo $row->items_id; ?>" class="form-control" <?php if($row->received == 1){ echo 'checked'; } ?> >
											
											<input type="hidden" value="<?php echo $row->received; ?>" name="received_<?php echo $row->id; ?>" id="underwriting_received_<?php echo $row->items_id;?>" class="underwriting_received">
												
										</td>
											
										<td><?php echo $underwriting_items_option[$row->items_id] ? $underwriting_items_option[$row->items_id] : $row->items ;?>
											<!--<input type="hidden" name="items[]" value="<?php //echo $row->items;?>" style="width:100%;" readonly>-->
										</td>

										<?php if($row->items_id =='204' || $row->items_id =='208' || $row->items_id =='213' || $row->items_id =='214' || $row->items_id =='212' || $row->items_id =='216' || $row->items_id =='209'){
												echo '<td>TaliMar Financial</td>';
											}elseif($row->items_id =='202' || $row->items_id =='203' || $row->items_id =='205' || $row->items_id =='206' || $row->items_id =='207'  || $row->items_id =='215'){
												echo '<td>Borrower</td>';
											}else{
												echo '<td></td>';
											}?>

										

									</tr>

	
									<?php	
										$last_number_items_id = $row->items_id;
									} }}} ?>

									<tr>
										<th style="text-align:left !important;">Item #</th>
										<th style="text-align:left !important;" colspan="4">Existing Loan</th>	
									</tr>

									<?php 

							$countDueitem11 = 0;

 							foreach($due_del_array_final as $kex => $vaaal)
							  { 
							  foreach($fetch_underwriting1 as $row){
									if($kex ==$row->items_id){

										if($row->type == '4'){ 

											$countDueitem11++;

										?>
																<tr id="<?php echo $row->items_id; ?>">
										<td style="display:none;">
											<input type="text" name="id_<?php echo $row->id;?>" value="<?php echo $row->id;?>">
											<input type="text" name="items_id" value="<?php echo $row->items_id;?>">
										</td>
										 
										<!--<td style="text-align:center !important;" ><?php echo $row->items_id; ?></td>-->
										<td style="text-align:center !important;" ><?php echo $countDueitem11; ?></td>
											
										<td style="text-align:center !important;" >
											
											<input type="checkbox" onchange="underwriting_required1(this);" name="req_<?php echo $row->items_id; ?>" class="form-control common" <?php if($row->required == 1){ echo 'checked'; } ?>>
										
											<input type="hidden" value="<?php echo $row->required; ?>" name="required_<?php echo $row->id; ?>" id="underwriting_required_<?php echo $row->items_id;?>" class="underwriting_required_checkbox">
											
										</td >
											
										<td style="text-align:center !important;" >
											
											<input type="checkbox" onchange="underwriting_received1(this)" name="rec_<?php echo $row->items_id; ?>" class="form-control" <?php if($row->received == 1){ echo 'checked'; } ?> >
											
											<input type="hidden" value="<?php echo $row->received; ?>" name="received_<?php echo $row->id; ?>" id="underwriting_received_<?php echo $row->items_id;?>" class="underwriting_received">
												
										</td>
											
										<td><?php echo $underwriting_items_option[$row->items_id] ? $underwriting_items_option[$row->items_id] : $row->items ;?>
											<!--<input type="hidden" name="items[]" value="<?php //echo $row->items;?>" style="width:100%;" readonly>-->
										</td>

										<?php if($row->items_id =='301' || $row->items_id =='302' || $row->items_id =='303'){
												echo '<td>Borrower</td>';;
											}else{
												echo '<td></td>';
											}?>

										
									</tr>
							<?php	
								$last_number_items_id = $row->items_id;
							} }}} ?>
							<tr>
										<th style="text-align:left !important;">Item #</th>
										<th style="text-align:left !important;" colspan="4">Construction Loan</th>	
									</tr>
							<?php 

							$countDueitem11 = 0;

 							foreach($due_del_array_final as $kex => $vaaal)
							{ 
							  foreach($fetch_underwriting1 as $row){
									if($kex ==$row->items_id){

										if($row->type == '5'){ 

											$countDueitem11++;

										?>
									<tr id="<?php echo $row->items_id; ?>">
										<td style="display:none;">
											<input type="text" name="id_<?php echo $row->id;?>" value="<?php echo $row->id;?>">
											<input type="text" name="items_id" value="<?php echo $row->items_id;?>">
										</td>
										 
										<!--<td style="text-align:center !important;" ><?php echo $row->items_id; ?></td>-->
										<td style="text-align:center !important;" ><?php echo $countDueitem11; ?></td>
											
										<td style="text-align:center !important;" >
											
											<input type="checkbox" onchange="underwriting_required1(this);" name="req_<?php echo $row->items_id; ?>" class="form-control common" <?php if($row->required == 1){ echo 'checked'; } ?>>
										
											<input type="hidden" value="<?php echo $row->required; ?>" name="required_<?php echo $row->id; ?>" id="underwriting_required_<?php echo $row->items_id;?>" class="underwriting_required_checkbox">
											
										</td >
											
										<td style="text-align:center !important;" >
											
											<input type="checkbox" onchange="underwriting_received1(this)" name="rec_<?php echo $row->items_id; ?>" class="form-control" <?php if($row->received == 1){ echo 'checked'; } ?> >
											
											<input type="hidden" value="<?php echo $row->received; ?>" name="received_<?php echo $row->id; ?>" id="underwriting_received_<?php echo $row->items_id;?>" class="underwriting_received">
												
										</td>
											
										<td><?php echo $underwriting_items_option[$row->items_id] ? $underwriting_items_option[$row->items_id] : $row->items ;?>
											<!--<input type="hidden" name="items[]" value="<?php //echo $row->items;?>" style="width:100%;" readonly>-->
										</td>

										<?php if($row->items_id =='401' || $row->items_id =='402' || $row->items_id =='403' || $row->items_id =='404' || $row->items_id =='405'){
												echo '<td>Borrower</td>';
											}else{
												echo '<td></td>';
											}?>

										

									</tr>
							<?php	
								$last_number_items_id = $row->items_id;
							} }}} ?>
									
									<tr>
										<th style="text-align:left !important;">Item #</th>
										<th style="text-align:left !important;" colspan="4">Other</th>	
									</tr>
									
							<?php 


 						
  							$countDueitem111 = 0;
  							$last_number_items_id=1001;
							  foreach($fetch_underwriting1 as $row){
							  	
							  	if (!in_array($row->items_id, $due_del_array ) && $row->items_id>1000 && $row->type=="10") {
										
											$countDueitem111++;
										?>
									
									<tr id="<?php echo $row->items_id; ?>">
										<td style="display:none;">
											<input type="text" name="id_<?php echo $row->id;?>" value="<?php echo $row->id;?>">
											<input type="text" name="items_id" value="<?php echo $row->items_id;?>">
											<input type="hidden" name="items_id_array_js_use[]" value="<?php echo $row->items_id;?>">
										</td>
							 			<!--<td style="text-align:center !important;" ><?php echo $row->items_id; ?></td>-->
							 			<td style="text-align:center !important;" ><?php echo $countDueitem111; ?></td>
											
										<td style="text-align:center !important;" >
											
											<input type="checkbox" onchange="underwriting_required1(this);" name="req_<?php echo $row->items_id; ?>" class="form-control common" <?php if($row->required == 1){ echo 'checked'; } ?>>
										
											<input type="hidden" value="<?php echo $row->required; ?>" name="required_<?php echo $row->id; ?>" id="underwriting_required_<?php echo $row->items_id;?>" class="underwriting_required_checkbox">
											
										</td >
											
										<td style="text-align:center !important;" >
											
											<input type="checkbox" onchange="underwriting_received1(this)" name="rec_<?php echo $row->items_id; ?>" class="form-control" <?php if($row->received == 1){ echo 'checked'; } ?> >
											
											<input type="hidden" value="<?php echo $row->received; ?>" name="received_<?php echo $row->id; ?>" id="underwriting_received_<?php echo $row->items_id;?>" class="underwriting_received">
												
										</td>
											
										<td><?php echo $underwriting_items_option[$row->items_id] ? $underwriting_items_option[$row->items_id] : $row->items ;?>
										
											<span class="pull-right"><a id="<?php echo $row->id;?>" onclick="delete_this_item(this);" title="Delete"><i class="fa fa-trash"></i></a></span>
											
										</td>
										<td>Borrower</td>
										<!-- <td>Due Diligence Page</td> -->
									</tr>
									
								<?php 
									
      
 

									$last_number_items_id = $row->items_id;
								 } }

								?>
							
									<tr id="other">
										<th  colspan="5"><a onclick="add_underwriting_items(this)" id="<?php echo $last_number_items_id; ?>"  id="new_row_add" class="btn blue">Add</a></th>
									</tr>

									<!--<tr id="new_row">	
										<td style="text-align:center !important;" >
											<input type="text" name="input_items_val[]" style="width: 50px !important;" >
										</td>
										
										<td style="text-align:center !important;">
										<input type="checkbox" onchange="underwriting_required1(this)"  class="form-control" >
												
										<input type="hidden" value="0" name="new_required[]" class="underwriting_required_checkbox">
									
										</td>
										
										<td style="text-align:center !important;">
										
											<input type="checkbox" onchange="underwriting_received1(this)"  class="form-control" >
											
											<input type="hidden" value="0" name="new_received[]" id="underwriting_received">
											
										</td>
										<td>
										<input type="text" name="new_items[]" style="width:100%;">
										</td>
								  
									</tr>-->
									
							
									<tr>
										<th style="text-align:left;"></th>
										<th style="text-align:center !important;">

							<!-- -----------------------checkbox disabled code start.................. -->
							
							<?php 
							$checkAtLeastOne="";
						 	foreach($fetch_underwriting1 as $key=> $val){
						 			if($val->items_id!='108' && $val->items_id!='109' && $val->items_id!='501' && $val->items_id!='502' && $val->items_id!='503' && $val->items_id!='504' && $val->items_id!='505' && $val->items_id!='506' && $val->items_id!='507' && $val->items_id!='508'){
						 				if($val->received=='1' || $val->required=='1'){
						 					$checkAtLeastOne="exist";
						 				}
						 			}
								    $array[]=$val->received;
							        $array_second[]=$val->required;
							        $array_other[]=$val->items_id;
							}
							if($this->session->userdata('user_role')== '2')
							{   
								$status_underwriting = '';
								$item_id='';
								foreach($array as $key => $rowdata){
									if($array_other[$key]!='108' && $array_other[$key]!='109' && $array_other[$key]!='501' && $array_other[$key]!='502' && $array_other[$key]!='503' && $array_other[$key]!='504' && $array_other[$key]!='505' && $array_other[$key]!='506' && $array_other[$key]!='507' && $array_other[$key]!='508'){

										if($rowdata != $array_second[$key])
										{
											$status_underwriting = 'disabled';
											$item_id=$array_other[$key];
											break;
										}
									}
							    }
                            }
							else{
							    $status_underwriting='disabled';
							}	
							if(empty($checkAtLeastOne)){
								$status_underwriting='disabled';
							}				 
							?>

				<!-- -----------------------checkbox disabled code end.................. -->	


								<input type="checkbox" onchange="underwriting_single(this);" <?php echo $delligenge_required_checked; ?> <?php echo $status_underwriting;?>>
							</th>
							<th style="text-align:left;"></th>
							<th colspan="2" style="text-align:left;">Underwriting Complete</th>
						
								<input type="hidden" name="delligenge_required" value="" id="underwriting_chkbox">
							</tr>
							</tbody>
						</table>


							
							<table class="hide_samplerow" class="table table-bordered table-striped table-condensed flip-content th_text_align_center"  style="display:none" id="hide_samplerows">
								<tr id="">	
									<td style="text-align:center !important;" >
									
										<input type="hidden" name="input_items_val[]" style="width: 50px !important;" >
									</td>
									<td style="text-align:center !important;">
									<input type="checkbox" onchange="underwriting_required1(this)"  class="form-control" >
											
									<input type="hidden" value="0" name="new_required[]" class="underwriting_required_checkbox">
								
									</td>
									
									<td style="text-align:center !important;">
									
										<input type="checkbox" onchange="underwriting_received1(this)"  class="form-control" >
										
										<input type="hidden" value="0" name="new_received[]" id="underwriting_received" class="underwriting_received">
										
									</td>
									<td>
									<input type="text" name="new_items[]" style="width:100%;">
									</td>
									<td></td>
									<!-- <td></td> -->
								  
								</tr>
							</table>
						</div>
					</div>
					<div class="modal-footer">
						<!--<a href="<?php// echo base_url();?>load_data/print_delligenge_item<?php// echo $loan_id;?>" class="btn blue">Print</a>-->
						<a class="btn blue" data-toggle="modal" href="#upload_docto_sharepoint">Upload</a>
						<!--<a class="btn blue" data-toggle="modal" onclick="open_uplaod(this)" href="#">Upload</a>-->
						<input type="submit" class="btn blue" name="action" value="Save">
						<a class="btn btn-default" data-toggle="modal" href="#edit_underwriting">Close</a>
					</div>
					
					<!---Saved changes--->
					<div class="modal fade" id="edit_underwriting" tabindex="-1" role="basic" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content" style="top:600px !important;">
								
								<div class="modal-body">
									<div class="row" style="padding-top:8px;">
								
										<div class="col-md-8" style="padding-top:6px;"><p>Do you want to Save Information?</p></div>
										<div class="col-md-2">
											<!--<a style="margin-left:38px;" class="btn blue borrower_save_button" href="javascript:void(0);" onclick = "save_info('edit_contact')">Yes</a>-->
											
											<input style="margin-left:30px;" type="submit" class="btn blue" name="action" value="Save">
											
										</div>
										<div class="col-md-2">
											<a class="btn default borrower_save_button" href = "<?php echo base_url().'load_data/'.$loan_id;?>" >No</a>
										
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!---Saved changes--->
					
					
				</form>
			</div>
		</div>
</div>




<div class="modal fade bs-modal-lg" id="upload_docto_sharepoint" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<!--<form method="post" action="#" enctype="multipart/form-data">-->
			<form method="post" action="<?php echo base_url();?>Load_data/custom_doc_upload_function" enctype="multipart/form-data">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Due Diligence Document: <?php echo $property_addresss; ?></h4>
					<input type="hidden" name="property_address" value="<?php echo $property_addresss;?>">
					<input type="hidden" name="loan_id" value="<?php echo $loan_id;?>">
				</div>
				<div class="modal-body">
										<div class="doc-div">
										
											<div class="row">
												<div class="col-md-4">
													<label>Selected documents:</label>

												
												</div>
											</div>
											<div class="row">
												<input type = "hidden" name = "document_id" id = "document_id" />
												<div class="col-md-12">
													

													<?php 
													$folder = 'due_dillege_document/'.$talimar_loan.'/'; //FCPATH.
													$base =$talimar_loan;

														/*$hideName = array('.','..','.DS_Store');   
														$files = scandir($folder);*/
													$due_dillegelistFiles = $this->aws3->getBucketObjectList($folder);
																												

											echo '<ul class = "document_lists" style="list-style-type: none;  margin-left:-40px;">';
											if($due_dillegelistFiles){
												foreach($due_dillegelistFiles as $filename) {
												    //if(!in_array($filename, $hideName)){
													$FileArr = explode('/', $filename);
													$FileNameGet = $FileArr[count($FileArr)-1];
												      
													echo '<li><a href ="'.aws_s3_document_url($filename).'">'.$FileNameGet.'</a></li>';
													
												//}
											}}
											echo '</ul>';
										
										?>
												 
												</div>
											</div>
											<hr>
												<div class="row" id="ad">
													<div class="col-md-4">
														<input type="file" id = "upload" name ="upload_docc[]" class="select_images_file" onchange="change_this_image(this)" >
													</div>
												</div>
											
											<br>
										</div>
											<!--<button onclick = "saved_document_add_more_row(this);" id = "add_row" class = "btn blue" type = "button"><i class="fa fa-plus" aria-hidden="true">Add More</i></button>-->
									</div>
				<div class="modal-footer">
					<input type="hidden" name="diligence_upload" value="diligence">
					<button type="submit" class="btn blue">Save</button>
					<button class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>