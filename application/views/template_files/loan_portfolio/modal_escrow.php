

<?php  $escrow_opened_option				= $this->config->item('escrow_opened_option');
?>
<div class="modal fade bs-modal-lg" id="escrow" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		<form method="POST" action ="<?php echo base_url();?>form_escrow">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<!--<h4 class="modal-title">Escrow: #<?php //echo $talimar_loan; ?></h4>-->
				<h4 class="modal-title">Escrow: <?php echo $property_addresss; ?></h4>
			</div>
			<div class="modal-body">
			
							<!-----------Hidden fields----------->
							<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
							<input type="hidden" name="loan_id" value="/<?php echo $loan_id; ?>">
							<!-----------End of hidden Fields---->
				
				<div class="row property_loan_row">
					<div class="col-md-3">
						<label>Is escrow opened?</label>
						<select class="form-control" name="escrow_opened" onchange="hide_below_all_rows(this.value);">
							<?php
								foreach($escrow_opened_option as $key => $option)
								{
									
									?>
									<option value="<?php echo $key;?>" <?php if($escrow_opened == $key){echo 'selected';}?>><?php echo $option; ?></option>
									<?php
								}
								?>
						</select>
					</div>
				</div>
				<div class="row property_loan_row">
					<div class="col-md-12">
						<h4 style="padding: 10px;">Escrow Contact Information</h4> 
					</div>
				</div>
				<div class="row property_loan_row">
					<div class="col-md-4">
						<label>Escrow Contact:</label>
						<!--<input type="text"  class="form-control" name = "first_name" value="<?= $e_first_name; ?> <?= $e_last_name; ?>" required />-->
						
						
						<select name="first_name" class="chosen2" id="escrow_c">
					
						</select>
						
					
					</div>
					<div class="col-md-4">
						<label>Company:</label>
						<input type="text"  class="form-control" name = "company" value="<?= $e_company; ?>">	
					</div>
					
										
				</div>
				
				<!--<div class="row property_loan_row">
					<div class="col-md-4">
						<label>Escrow Contact:</label>
						<input type="text"  class="form-control" name = "first_name" value="<?= $e_first_name; ?>" required />
						
					</div>
					<div class="col-md-4">
					<label>Last Name:</label>
						<input type="text" class="form-control" name = "last_name" value="<?= $e_last_name; ?>"  />
					</div>
					
					
										
				</div>-->
				<div class="row property_loan_row">
					<div class="col-md-4">
					<label>Phone:</label>
						<input type="text" class="form-control phone-format" name = "phone" value="<?= $e_phone; ?>"  />
					</div>
					<div class="col-md-4">
					<label>Email:</label>
						<input  type="email" class="form-control" name = "email" value="<?= $e_email; ?>">
							
					</div>
				
				</div>
				<div class="row property_loan_row">
					<div class="col-md-8">
						<label>Street Address :</label>
						<input type="text"  class="form-control" name = "address" value="<?= $e_address; ?>" >
						
					</div>
					<div class="col-md-4">
					<label>Unit #:</label>
						<input type="text" class="form-control" name = "address_b" value="<?= $e_address_b; ?>"  />
					</div>
					
										
				</div>
				 
				<div class="row property_loan_row">
					<div class="col-md-4">
					<label>City:</label>
						<input  type="city" class="form-control" name = "city" value="<?= $e_city; ?> ">
							
					</div>
					<div class="col-md-4">
						<label>State:</label>
						<select  class="form-control" name = "state">
						<?php 
						foreach($STATE_USA as $key => $row)
						{
							?>
							<option value="<?= $key?>" <?php if($e_state==$key) { echo "selected"; } ?> ><?= $row?></option>
							<?php
						}
						?>
												
												
						</select>
					</div>
					<div class="col-md-4">
					<label>Zip:</label>
						<input type="text" class="form-control number_only" name = "zip" value = "<?php echo $e_zip; ?>"  />
					</div>
					
										
				</div>
				<div class="row property_loan_row">
					<div class="col-md-12">
					<h4 style="padding: 10px;">Escrow Requirements</h4> 
					</div>
				</div>
				<div class="row property_loan_row">
					<div class="col-md-4">
					<label>Escrow #:</label>
						<input type="text" class="form-control" name = "escrow" value="<?= $e_escrow; ?>"  />
					</div>
					<div class="col-md-4">
					<label>Expiration Days:</label>
						<input type="text" class="form-control number_only" name = "expiration" value="<?= $e_expiration;?>"  />
					</div>
					
					<div class="col-md-4">
						<label>&nbsp;<label>
						<!--<p class="short_size_p"><input type="checkbox"   value = '1' name = "first_payment_close" <?php if($first_payment_close == '1') echo "checked"; ?> > Collect First Payment at Close</p>-->
						<p class="short_size_p"><input type="checkbox"   value = '1' name = "net_fund" <?php if($e_net_fund == '1') echo "checked"; ?> > Net Fund</p>
						<p class="short_size_p"><input type="checkbox"   value="1" name = "tax_service" <?php if($e_tax_service == '1') echo "checked"; ?> > Tax Service Required?</p>
						<p class="short_size_p"><input type="checkbox"   value="1" name = "record_assigment_close" <?php if($record_assigment_close == '1') echo "checked"; ?> > Record Assignment at Close</p>
						
					</div>
			
				</div>
				
				<div class="row property_loan_row">
					<div class="col-md-12">
						<label class="font_weight_dark">Financing is Approved:</label>
						<textarea type="text"  class="form-control" name="financingApp"  ><?= $financingApp;?></textarea>
					</div>
				</div>
				
				<div class="row property_loan_row">
					<div class="col-md-12">
						<label class="font_weight_dark">Additional Escrow Instructions:</label>
						<textarea type="text"  class="form-control" name="instruction"  ><?= $e_instruction;?></textarea>
					</div>
				</div>
<!--
				<div class="row property_loan_row">
					<div class="col-md-12">
						<h4 style="padding: 10px;">Title Contact Information</h4> 
					</div>
					
						<div class="col-md-2">
							<a class="btn btn-primary btn-block" data-toggle="modal" href="#title"><span class="title">Title</span></a>
						</div>
					
				</div>-->
			</div>
			<div class="modal-footer">
				
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<input type="submit" class="btn blue" value="Save">
				<a href="#wired_instruction" data-toggle="modal"><button type="button" class="btn blue">Wire Instructions</button></a>
			
			</div>
		</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>







<div class="modal fade bs-modal-lg" id="wired_instruction" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
			<form method="POST" action="<?php echo base_url();?>form_wired_instruction">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<!--<h4 class="modal-title">Wire Instructions: #<?php //echo $talimar_loan;?></h4>-->
					<h4 class="modal-title">Wire Instructions: <?php echo $property_addresss;?></h4>
				</div>
				<div class="modal-body pad-inp-body">
						<!-----------Hidden fields----------->
						<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
						<input type="hidden" name="loan_id" value="/<?php echo $loan_id; ?>">
						<!-----------End of hidden Fields---->
					<div class="row">
						<div class="col-sm-4">
							<label>Bank Name:</label>
							<input type="text" class="form-control" name="bank_name" value="<?php echo isset($fetch_wire_instruction) ? $fetch_wire_instruction[0]->bank_name : ''; ?>">
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-4">
							<label>Bank Street Address:</label>
							<input type="text" class="form-control" name="bank_street" value="<?php echo isset($fetch_wire_instruction) ? $fetch_wire_instruction[0]->bank_street : ''; ?>">
						</div>
						<div class="col-sm-4">
							<label>Bank Unit #:</label>
							<input type="text" class="form-control" name="bank_unit" value="<?php echo isset($fetch_wire_instruction) ? $fetch_wire_instruction[0]->bank_unit : ''; ?>">
						</div>
					</div>
					
					
					<div class="row">
						<div class="col-sm-4">
							<label>Bank City:</label>
							<input type="text" class="form-control" name="bank_city" value="<?php echo isset($fetch_wire_instruction) ? $fetch_wire_instruction[0]->bank_city : ''; ?>">
						</div>
						<div class="col-sm-4">
							<label>Bank State:</label>
							<select type="text" class="form-control" name="bank_state" >
						<?php
							foreach($STATE_USA as $key =>$row)
							{
								?>
								<option value="<?php echo $key; ?>" <?php if(isset($fetch_wire_instruction)){ if($key == $fetch_wire_instruction[0]->bank_state){ echo 'selected'; } } ?> ><?php echo $row; ?></option>
								<?php
							}
						?>
							</select>
						</div>
						<div class="col-sm-4">
							<label>Bank Zips:</label>
							<input type="text" class="form-control" name="bank_zip" value="<?php echo isset($fetch_wire_instruction) ? $fetch_wire_instruction[0]->bank_zip : ''; ?>">
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-4">
							<label>Recipient Name:</label>
							<input type="text" class="form-control" name="recipient_name" value="<?php echo isset($fetch_wire_instruction) ? $fetch_wire_instruction[0]->recipient_name : ''; ?>">
						</div>
						<div class="col-sm-4">
							<label>Recipient Phone Number:</label>
							<input type="text" class="form-control number_only phone-format" name="recipient_phone_no" value="<?php echo isset($fetch_wire_instruction) ? $fetch_wire_instruction[0]->recipient_phone_no : ''; ?>">
						</div>
					
					</div>
					
					<div class="row">
						<div class="col-sm-4">
							<label>Recipient Street Address:</label>
							<input type="text" class="form-control" name="recipient_street" value="<?php echo isset($fetch_wire_instruction) ? $fetch_wire_instruction[0]->recipient_street : ''; ?>">
						</div>
						<div class="col-sm-4">
							<label>Recipient Unit #:</label>
							<input type="text" class="form-control" name="recipient_unit" value="<?php echo isset($fetch_wire_instruction) ? $fetch_wire_instruction[0]->recipient_unit : ''; ?>">
						</div>
					</div>
					
					
					<div class="row">
						<div class="col-sm-4">
							<label>Recipient City:</label>
							<input type="text" class="form-control" name="recipient_city" value="<?php echo isset($fetch_wire_instruction) ? $fetch_wire_instruction[0]->recipient_city : ''; ?>">
						</div>
						<div class="col-sm-4">
							<label>Recipient State:</label>
							<select type="text" class="form-control" name="recipient_state" >
						<?php
							foreach($STATE_USA as $key =>$row)
							{
								?>
								<option value="<?php echo $key; ?>" <?php if(isset($fetch_wire_instruction)){ if($key == $fetch_wire_instruction[0]->recipient_state){ echo 'selected'; } } ?> ><?php echo $row; ?></option>
								<?php
							}
						?>
							</select>
						</div>
						<div class="col-sm-4">
							<label>Recipient Zip:</label>
							<input type="text" class="form-control" name="recipient_zip" value="<?php echo isset($fetch_wire_instruction) ? $fetch_wire_instruction[0]->recipient_zip : ''; ?>">
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-4">
							<label>Routing Number:</label>
							<input type="text" class="form-control" name="routing_number" value="<?php echo isset($fetch_wire_instruction) ? $fetch_wire_instruction[0]->routing_number : ''; ?>">
						</div>
						<div class="col-sm-4">
							<label>Account Number:</label>
							<input type="text" class="form-control" name="account_number" value="<?php echo isset($fetch_wire_instruction) ? $fetch_wire_instruction[0]->account_number : ''; ?>">
						</div>
					</div>	
					<div class="row">	
						<div class="col-sm-12">
							<label>Further Credit To:</label>
							<input type="text" class="form-control" name="credit_to" value="<?php echo isset($fetch_wire_instruction) ? $fetch_wire_instruction[0]->credit_to : ''; ?>">
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-2">
							<strong>Reference: </strong>
							
						</div>
						<div class="col-sm-10">
							<p><?php echo $fetch_loan_borrower[0]->b_name; ?></p>
							
							<!--<p><?php echo isset($loan_property_result[0]->property_address) ? $loan_property_result[0]->property_address.', '.$loan_property_result[0]->unit.'; '.$loan_property_result[0]->city.', '.$loan_property_result[0]->state.' '.$loan_property_result[0]->zip : '' ; ?></p>-->
						
							<p><?php 

								$primary_p_address = isset($primary_property_result[0]->property_address) ?  $primary_property_result[0]->property_address : '';
								
								$primary_p_unit = $primary_property_result[0]->unit ? ', '.$primary_property_result[0]->unit: '';
								
								$primary_p_city = $primary_property_result[0]->city ? '; '.$primary_property_result[0]->city : '';
								
								$primary_p_state = $primary_property_result[0]->state ?  ', '.$primary_property_result[0]->state : '';
								
								$primary_p_zip = $primary_property_result[0]->zip ? ' '.$primary_property_result[0]->zip  : '';
								
								echo $primary_full_address = $primary_p_address.''.$primary_p_unit.''.$primary_p_city.'' .$primary_p_state .''. $primary_p_zip;
								?>
								
								
								</p>
							
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-2">
							<strong>Escrow # : </strong>
							
						</div>
						<div class="col-sm-10">
							<p><?php echo $e_escrow;?></p>
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-2">
							<strong>Title #: </strong>
							
						</div>
						<div class="col-sm-10">
							<p><?php echo $fetch_title_data[0]->title_order_no ? $fetch_title_data[0]->title_order_no : ''; ?></p>
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-12">
						
							<input type="checkbox" onclick="check_wire_frame(this)" name="wire_confirmed" <?php if(isset($fetch_wire_instruction)){ if($fetch_wire_instruction[0]->wire_confirmed == 1){ echo 'checked'; } }?> > 
							<strong>Wire information confirmed by TaliMar Financial</strong>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<span>
					<input type="checkbox" name="wire_tali_lender" onclick="wire_tali_lender_checkbox(this)" <?php if(isset($fetch_wire_instruction)){ if($fetch_wire_instruction[0]->wire_tali_lender == 1){ echo 'checked'; } }?>> <strong>Wire to TaliMar Financial Lender Account</strong>
					</span>
					<?php   
					$wire_instruction_disa='disabled';
					 if($fetch_wire_instruction[0]->wire_confirmed == 1){
					 	
						$wire_instruction_disa='';
						
					 }  ?>
					<a href="<?php echo base_url();?>load_data/wire_instructions<?php echo $loan_id;?>" id="dis_print" class="btn blue" <?php echo $wire_instruction_disa;?>>Print</a>
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn blue">Save</button>
				</div>
			</form>	
			</div>
		</div>	
	</div>	