<style type="text/css">
	.icon_position{
		margin-top: 30px;
	}
</style>
<div class="modal fade" id="loan_lender_payment" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-body">
				<form method="POST" action="<?php echo base_url()?>Load_data/add_wholesale_deal" id="wholesale_deal_frm" enctype="multipart/form-data">	
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Lender Assigment Payment : <?php echo $property_addresss; ?></h4>
					</div>
					
					<!-----------Pu content here----------->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">	
								<label  for="textinput">Lender Name:</label>
								<select id="loan_pay_lender_name" type = "text" name="loan_pay_lender_name[]" class="chosen"    data-live-search="true">
									<option value="">Select One</option>
									<?php
									if(!empty($investor_all_data)){
										foreach($investor_all_data as $investor)
										{	?>
											<option value="<?= $investor->id;?>" <?php if(!empty($fetchLenderPayment[0]->lender_name) && $investor->id == $fetchLenderPayment[0]->lender_name){ echo 'selected'; } ?>><?php echo $investor->name; ?></option>
										<?php
										}
									}									
									?>
								</select>
							</div>
						</div>
						<div class="col-md-6" style="float: right;">
							<div class="form-group">
								<div class="col-md-3">
									<button class="btn btn-primary">Submit Disclosures</button>
								</div>
								<div class="col-md-1" style="margin-right: 20px !important">
									<button class="btn btn-success">Save</button>
								</div>
								<div class="col-md-1" style="margin-right: 20px !important">
									<button class="btn btn-danger">Delete</button>
								</div>
								<div class="col-md-1">
									<button class="btn btn-danger">Close</button>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<label  for="textinput">Contact Name:</label>
							<input type="text" name="loan_pay_contact_name[]" id="loan_pay_contact_name" value="" readonly="readonly" class="form-control">
						</div>
						<div class="col-md-4">
							<label  for="textinput">Contact E-Mail:</label>
							<input type="text" name="loan_pay_contact_email[]" id="loan_pay_contact_email" value="" readonly="readonly" class="form-control">
						</div>
						<div class="col-md-4">
							<label  for="textinput">Contact Phone:</label>
							<input type="text" name="loan_pay_contact_phone[]" id="loan_pay_contact_phone" value="" readonly="readonly" class="form-control">
						</div>
					</div>
					<br>
					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn blue">Save</button>					
					</div>
				</form>
			</div>	
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<script type="text/javascript">
</script>