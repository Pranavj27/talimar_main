<?php  
$prelim_receive_approve_option				= $this->config->item('prelim_receive_approve_option');
$title_lender_policy_type_option			= $this->config->item('title_lender_policy_type_option');
?>
<div class="modal fade bs-modal-lg" id="title" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		<form method="POST" action="<?php echo base_url();?>form_loan_title">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<!--<h4 class="modal-title">Title: #<?php //echo $talimar_loan;?></h4>-->
				<h4 class="modal-title">Title: <?php echo $property_addresss;?></h4>
			</div>
			<div class="modal-body">
			<?php 
			$fetch['id']=str_replace("/","",$loan_id);
			$fetch_loan = $this->User_model->select_where('loan', $fetch);

				  $fetch_loan_result = $fetch_loan->result();
				  $borrower_id=$fetch_loan_result[0]->borrower;
		?>
							<!-----------Hidden fields----------->
							<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
							<input type="hidden" name="loan_id" value="/<?php echo $loan_id; ?>">
							<input type="hidden" name="borrower_id" value="<?php echo isset($borrower_id) ? $borrower_id :''; ?>">

							<!-----------End of hidden Fields---->
				<div class="row property_loan_row">
					<div class="col-md-12">
						<h4 style="padding: 10px;">Vesting</h4>
					</div>
				</div>


				<div class="row property_loan_row">
					<div class="col-md-12">
						<label>Loan Vesting:</label>

						<textarea class="form-control" rows="3" name="vesting"><?php echo isset($fetch_loan_borrower[0]->b_vesting) ? $fetch_loan_borrower[0]->b_vesting : ''; ?></textarea>
					</div>
					</div>

			<div class="row property_loan_row">
					<div class="col-md-12">
						<h4 style="padding: 10px;">Title Contact Information</h4>
					</div>
				</div>
				<div class="row property_loan_row">
					<div class="col-md-4">
					<label>Title Contact:</label>
					<?php //echo $title_first_name .' '.$title_last_name ;?>
					<select name="first_name" class="chosen1" id="title_c_name">
				
					</select>
					
					</div>						
					<div class="col-md-4">
						<label>Company:</label>
						<input type="text"  class="form-control" name = "company" value="<?php echo isset($title_company) ? $title_company : ''; ?>" >
						
					</div>					
				</div>
				
				
				
				<div class="row property_loan_row">
					<div class="col-md-4">
						<label>Phone:</label>
						<input type="text"  class="form-control phone-format" name="phone" value="<?= $title_phone?>" >
						
					</div>
					<div class="col-md-4">
					<label>E-mail:</label>
						<input type="email" class="form-control" name = "email" value="<?= $title_email; ?>"  />
					</div>
					
				</div>
				
				
				<div class="row property_loan_row">
					<div class="col-md-8">
						<label>Street Address:</label>
						<input type="text"  class="form-control" name = "address" value="<?= $title_address;?>" >
						
					</div>
					<!--<div class="col-md-4">
					<label>Address 2:</label>
						<input type="text" class="form-control" name = "address_b" value="<?= $title_address_b;?>" />
					</div>-->
					<div class="col-md-4">
					<label>Unit #:</label>
						<input type="text" class="form-control" name = "unit" value="<?= $title_unit;?>" />
					</div>
					
										
				</div>
				
				<div class="row property_loan_row">
					<div class="col-md-4">
						<label>City:</label>
						<input  type="city" class="form-control" name = "city" value="<?= $title_city;?>" >
							
					</div>
					<div class="col-md-4">
						<label>State:</label>
						<select class="form-control" name="state">
						<?php 
						foreach($STATE_USA as $key => $row)
						{
							?>
							<option value="<?= $key?>" <?php if($title_state==$key) { echo "selected"; } ?> ><?= $row?></option>
							<?php
						}
						?>
						</select>
						
					</div>
					<div class="col-md-4">
					<label>Zip:</label>
						<input type="text" class="form-control" name = "zip" value="<?= $title_zip;?>" />
					</div>					
				</div>
				
				<div class="row property_loan_row">
					<div class="col-md-12">
						<h4 style="padding: 10px;">Title Requirements</h4>
					</div>
				</div>

				<div class="row property_loan_row">
					<div class="col-md-4">
						<label>Prelim Received:</label>
						<select class="form-control" name="prelim_receive" onchange="prelim_rec(this.value)">
							<?php foreach($prelim_receive_approve_option as $key => $option){ ?>
									<option value="<?php echo $key;?>" <?php if($prelim_receive == $key){echo 'selected';}?>><?php echo $option; ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-4">
						<label>Prelim Approved:</label>
						<select class="form-control" name="prelim_approve">
							<?php foreach($prelim_receive_approve_option as $key => $option){ ?>
									<option value="<?php echo $key;?>" <?php if($prelim_approve == $key){echo 'selected';}?>><?php echo $option; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>

				<div class="row property_loan_row">
				
					<div class="col-md-4">
						<label>Preliminary Title Report Date:</label>
						<input type="text"  class="form-control" id="ptr_date" name = "ptr_date" value="<?php echo isset($title_ptr_date) ? date('m-d-Y',strtotime($title_ptr_date)) : ''; ?>" >
						
					</div>
					
					<div class="col-md-4">
						<label>Title Order #:</label>
						<input type="text"  class="form-control"  name = "title_order_no" value="<?php echo isset($title_order_no) ? $title_order_no : ''; ?>" >
						
					</div>
				
				</div>
				<div class="row property_loan_row">
					<div class="col-md-4">
						<label>Lender Policy Type:</label>
						<select  class="form-control" name = "lender_policy_type"  >
						<?php 
						foreach($title_lender_policy_type_option as $key => $row)
						{
							?>
							<option value="<?= $key;?>" <?php if($key == $title_lender_policy_type) { echo 'selected'; } ?> ><?= $row;?></option>
							<?php
						}
						?>
						</select>
					</div>
					<div class="col-md-4">
					<label>Policy Amount (%):</label>
						<input type="text" class="form-control" name = "policy_amount_in_percent" value="<?= $title_policy_amount_in_percent;?>" id="title_modal_policy_amount_in_percent" />
					</div>
					<div class="col-md-4">
					<label>Policy Amount ($):</label>
						<input  type="text" class="form-control" name = "policy_amount_in_dolar" value="<?= '$'.number_format($title_policy_amount_in_dolar);?>" id="title_policy_amount_in_dolar">
							
					</div>
										
				</div>
				
				<div class="row property_loan_row">
					<div class="col-md-12">
						<label>Endorsements:</label>
						<input type="text"  class="form-control" name = "endorsement" value="<?= $title_endorsement;?>" >
						
					</div>
					
										
				</div>
				
				<div class="row property_loan_row">
					<div class="col-md-12">
						<label>Title Exception:</label>
						<textarea   class="form-control" name = "exception"> <?= $title_exception;?></textarea>
						
					</div>
				</div>
				
				<div class="row property_loan_row">
					<div class="col-md-12">
						<label>Title Report Elimination:</label>
						<textarea   class="form-control" name = "report_elimination"  ><?= $title_report_elimination;?></textarea>
						
					</div>
				</div>
		
			</div>
			<div class="modal-footer">
			<a href="#title_docto_sharepoint" class="btn blue" data-toggle="modal">Title Documents</a>
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn blue">Save</button>
				<!--<a href="#wired_instruction" data-toggle="modal"><button type="button" class="btn blue">Wire Instructions</button></a>-->
			</div>
		</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>










<div class="modal fade bs-modal-lg" id="title_docto_sharepoint" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				  <form method="POST" enctype="multipart/form-data" action = "<?php echo base_url('Load_data/title_saved_document');?>">	

					<input type="hidden" value="/<?php echo $loan_id;?>" name="loan_id">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Upload Documents: <?php echo $property_addresss; ?></h4>
						<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
					</div>
					<div class="modal-body">
											<div class="doc-div">
											
												<div class="row">
													<div class="col-md-4">
														<label>Selected documents:</label>

													
													</div>
												</div>
												<div class="row">
													<input type = "hidden" name = "document_id" id = "document_id" />
													<div class="col-md-12">
														

														<?php 
														$t_folder = 'title_document/'.$talimar_loan.'/';

														$due_title_document = $this->aws3->getBucketObjectList($t_folder);

														/*$t_base =$talimar_loan;

															$t_hideName = array('.','..','.DS_Store');   
															$t_files = scandir($t_folder);*/
															

												echo '<ul class = "document_lists" style="list-style-type: none;  margin-left:-40px;">';
												if($due_title_document){
													foreach($due_title_document as $filename) {
														$FileArr = explode('/', $filename);
														$FileNameGet = $FileArr[count($FileArr)-1];
												    	//if(!in_array($t_filename, $t_hideName)){												      
													echo '<li>
													<a target="_blank" href ="'.aws_s3_document_url($filename).'">'.$FileNameGet.'</a>	
													
														
													</li>';
													
												}}
												echo '</ul>';
											
											?>
													 
													</div>
												</div>
												<hr>
													<div class="row" id="ad">
														<div class="col-md-4">
															<input type="file" id = "upload" name ="t_upload_doc[]" class="select_images_file" onchange="change_this_image(this)" >
														</div>
													</div>
												
												<br>
											</div>
												<!--<button onclick = "saved_document_add_more_row(this);" id = "add_row" class = "btn blue" type = "button"><i class="fa fa-plus" aria-hidden="true">Add More</i></button>-->
										</div>
					<div class="modal-footer">
						<button type="submit" class="btn blue">Save</button>
						<button class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>