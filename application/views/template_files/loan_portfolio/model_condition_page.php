<style type="text/css">
	.icon_position{
		margin-top: 30px;
	}
</style>
<div class="modal fade" id="condition_page" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-body">
				<form method="POST" action="<?php echo base_url()?>Load_data/add_condition_page" id="condition_page_frm" enctype="multipart/form-data">	
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Condition Page : <?php echo $property_addresss; ?></h4>
					</div>
					<br>
					<!-----------Hidden fields ---------->
					<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
					<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
			
					<!-----------Pu content here---------->
					<div class="row">
						<div class="col-md-6">
							<h3>Conditions</h3>
						</div>
						<div class="col-md-4"></div>
						<div class="col-md-2"  style="float: right">
							<a href="javascript:void(0)" class="btn btn-primary" onclick="add_condition_row()">Add</a>
							<button type="submit" class="btn btn-success">Save</button>
						</div>
					</div>
					<div class="row"  id="condition_rows_list">
						<?php
						if(!empty($fetch_condition_page)){
							for($i=0;$i<count($fetch_condition_page);$i++){
								?>
								<div class="col-md-12" id="condition_row_<?php echo $i;?>">
									<div class="col-md-3">
										<div class="form-group">	
											<label  for="textinput">Status</label>
											<input type="hidden" name = "codition_id[]" id="codition_id_<?php echo $i;?>" value = "<?php echo $fetch_condition_page[$i]->codition_id;?>">
											<select class="form-control " name="condition_status[]" id="condition_status_<?php echo $i;?>">
												<option value="">Select One</option>
												<option value="submitted" <?php if(!empty($fetch_condition_page[$i]->condition_status) && $fetch_condition_page[$i]->condition_status=="submitted"){ echo "selected"; }?>>Submitted</option>
												<option value="processing" <?php if(!empty($fetch_condition_page[$i]->condition_status) && $fetch_condition_page[$i]->condition_status=="processing"){ echo "selected"; }?>>Processing</option>
												<option value="completed" <?php if(!empty($fetch_condition_page[$i]->condition_status) && $fetch_condition_page[$i]->condition_status=="completed"){ echo "selected"; }?>>Completed</option>
											</select>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">	
											<label  for="textinput">Department</label>
											<select class="form-control" name="condition_department[]" id="condition_department_<?php echo $i;?>">
												<option value="">Select One</option>
												<option value="undewriting" <?php if(!empty($fetch_condition_page[$i]->condition_department) && $fetch_condition_page[$i]->condition_department=="undewriting"){ echo "selected"; }?> >Undewriting</option>
												<option value="escrow" <?php if(!empty($fetch_condition_page[$i]->condition_department) && $fetch_condition_page[$i]->condition_department=="escrow"){ echo "selected"; }?> >Escrow / Title</option>
												<option value="insurance" <?php if(!empty($fetch_condition_page[$i]->condition_department) && $fetch_condition_page[$i]->condition_department=="insurance"){ echo "selected"; }?> >Insurance</option>
												<option value="appraisal" <?php if(!empty($fetch_condition_page[$i]->condition_department) && $fetch_condition_page[$i]->condition_department=="appraisal"){ echo "selected"; }?> >Appraisal</option>
												<option value="other" <?php if(!empty($fetch_condition_page[$i]->condition_department) && $fetch_condition_page[$i]->condition_department=="other"){ echo "selected"; }?> >Other</option>
											</select>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">	
											<label  for="textinput">Description</label>
											<textarea name="condition_desc[]" id="condition_desc_<?php echo $i;?>" rows="1" class="form-control "><?php if(!empty($fetch_condition_page[$i]->condition_desc)){ echo $fetch_condition_page[$i]->condition_desc; } ?></textarea>
										</div>
									</div>
									<?php
									if($i>0){?>
									<div class="col-md-2">
										<div class="form-group icon_position">
											<label  for="textinput">&nbsp;</label>
											<a title="Add People" onclick="delete_condition_update('<?php echo $i;?>');" class="btn btn-sm btn-danger plus_icon_add_more" ><i class="fa fa-trash"></i></a>
										</div>
									</div>
									<?php
									}?>
							</div>
							<?php
							}
						}else
						{?>
						
							<div class="col-md-12" id="condition_row_0">
								<div class="col-md-3">
									<div class="form-group">	
										<label  for="textinput">Status</label>
										<input type="hidden" name = "codition_id[]" id="codition_id_0" value = "">
										<select class="form-control " name="condition_status[]" id="condition_status_0">
											<option value="">Select One</option>
											<option value="submitted" >Submitted</option>
											<option value="processing" >Processing</option>
											<option value="completed" >Completed</option>
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">	
										<label  for="textinput">Department</label>
										<select class="form-control" name="condition_department[]" id="condition_department_0">
											<option value="">Select One</option>
											<option value="undewriting" >Undewriting</option>
											<option value="escrow" >Escrow / Title</option>
											<option value="insurance" >Insurance</option>
											<option value="appraisal" >Appraisal</option>
											<option value="other" >Other</option>
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">	
										<label  for="textinput">Description</label>
										<textarea name="condition_desc[]" id="condition_desc_0" rows="1" class="form-control "></textarea>
									</div>
								</div>
								<!-- <div class="col-md-2">
									<div class="form-group icon_position">
										<label  for="textinput">&nbsp;</label>
										<a title="Add People" onclick="delete_condition_row('0');" class="btn btn-sm btn-danger plus_icon_add_more" ><i class="fa fa-trash"></i></a>
									</div>
								</div> -->
							</div>
						
						<?php
						}?>
					<!-----------Pu content here---------->
					</div>
				</form>
			</div>	
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<script type="text/javascript">
	var con_row_no='<?php echo count($fetch_condition_page);?>';
	function add_condition_row()
	{
		con_row_no=parseInt(con_row_no)+1;
		var str="";
		str+='<div class="col-md-12" id="condition_row_'+con_row_no+'">';
		str+='<div class="col-md-3"><div class="form-group"><label  for="textinput">Status</label><input type="hidden" name = "codition_id[]" id="codition_id_'+con_row_no+'" value = ""><select class="form-control " name="condition_status[]" id="condition_status_'+con_row_no+'"><option value="">Select One</option><option value="submitted" >Submitted</option><option value="processing" >Processing</option>	<option value="completed" >Completed</option></select></div></div>';
		str+='<div class="col-md-3"><div class="form-group"><label  for="textinput">Department</label><select class="form-control" name="condition_department[]" id="condition_department_'+con_row_no+'"><option value="">Select One</option><option value="undewriting" >Undewriting</option><option value="escrow" >Escrow / Title</option><option value="insurance" >Insurance</option><option value="appraisal" >Appraisal</option><option value="other" >Other</option></select></div></div>';
		str+='<div class="col-md-4"><div class="form-group">	<label  for="textinput">Description</label><textarea name="condition_desc[]" id="condition_desc_'+con_row_no+'" rows="1" class="form-control "></textarea></div></div>';
		str+='<div class="col-md-2"><div class="form-group icon_position"><label  for="textinput">&nbsp;</label><a title="Add People" onclick="delete_condition_row('+con_row_no+');" class="btn btn-sm btn-danger plus_icon_add_more" ><i class="fa fa-trash"></i></a></div></div>';
		str+='</div>';
		$("#condition_rows_list").append(str);

	}
	function delete_condition_row(id_no){
		id_no=parseInt(id_no);
		$("#condition_row_"+id_no).remove();
	}
	function delete_condition_update(delete_no){
		var condition_id=$("#codition_id_"+delete_no).val();
		$.ajax({
				type: "post",
				url: "/LoanPortfolioPopup/delete_condition_page_row",
				data:{'condition_id':condition_id},
				success: function(response){
					$("#condition_row_"+delete_no).remove();
				}
		 });
	}
</script>