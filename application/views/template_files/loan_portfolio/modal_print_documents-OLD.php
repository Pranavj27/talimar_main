<div class="modal fade bs-modal-lg" id="print_loan_document" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url();?>print_loan_document" id="form_print_loan_document">
							
				<!-----------Hidden fields----------->
				<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
				<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
				<!-----------End of hidden Fields---->
							
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Loan: <?php echo $property_addresss; ?> </h4>
				</div>
				<div class="modal-body">
				 <div class="print_document_div_section">
				 <div class="row" style="font-weight:500 !important;">
				 	<div class="col-sm-6">
						<input type="checkbox" onchange="auto_select_loan_app(this)" id=""><span>Loan Application</span>
					</div>
				   
					
					<div class="col-sm-6">
						<input type="checkbox" onchange="auto_select_print(this)" id="auto_select_print_input"><span>Fix and Flip Loan</span>
					</div>
					<div class="col-sm-6 ">
						<input type="checkbox" onclick="select_all_print_loan(this)" id="print_select_all"><span>Check All</span>
					</div>
				 	<div class="col-sm-6">
						<input type="checkbox" onchange="bridge_select_print_doc(this);" id="bridge_doc"><span>Bridge Loan</span>
					</div>
					<div class="col-sm-6 ">
						<input type="checkbox" onclick="deselect_all_print_loan(this)" id="print_deselect_all"><span>Uncheck  All</span>
					</div>
				 </div>
				<table id="table_print_document">
					<tr>
						<td colspan="2"><h3>Executive Summary</h3></td>
					</tr>

					<tr>
						<td><input type="checkbox" name="trust_deed_overview">Trust Deed Overview</td>
						<td><input type="checkbox" name="executive_summary_other_ecumbrance">Other Encumbrances</td>
						
					</tr>
					<tr>
						<td><input type="checkbox" name="loan_summary">Loan Summary</td>
						<td><input type="checkbox" name="executive_summary_cashflow">Property Cashflow</td>
					
					
					</tr>
					<tr>
				<!-- 		<td><input type="checkbox" name="Loan_Purpose">Loan Purpose</td> -->
						<td><input type="checkbox" name="Property_Information">Property Information</td>
						<td><input type="checkbox" name="executive_summary_sources_and_uses">Sources & Uses</td>
					
				
						
					</tr>
					<tr>
							<td><input type="checkbox" name="executive_summary_proposed_improvement">Proposed Improvements</td>
							<td><input type="checkbox" name="Borrower_Information">Borrower Information</td>	
					
						
						
					</tr>
					<tr>
					<td><input type="checkbox" name="executive_summary_valuation">Property Valuation</td>	
					<td><input type="checkbox" name="Contact_Information">Contact(s) Information</td>
					
						
					
					</tr>
					<tr>
				<td></td>
					<td><input type="checkbox" name="executive_summary_site_photos">Property Images</td>
				
						
			
					</tr>

					<tr>
					<!-- <td><input type="checkbox" name="property_valu">Property Valuation 1</td> -->
					<td></td> 
					<td><input type="checkbox" name="Loan_Servicing">Loan Servicing</td>
					</tr>
				

					<tr>
						<td colspan="2"><h3>Underwriting Documents</h3></td>
					</tr>

					<tr>
						<td><input type="checkbox" name="pre_approval_letter">Pre-Approval Letter</td>
						<td><input type="checkbox" name="term_sheet">Term Sheet – Bridge Loan</td>
					</tr>

					<tr>
						<td><input type="checkbox" name="loan_application">Loan Application – Primary</td>
						<td><input type="checkbox" name="term_sheet_flip">Term Sheet – Flip Loan</td>
									
					</tr>
					<tr>
						<td><input type="checkbox" name="authorization_to_release">Loan Application – Credit Authorization</td>
						<td><input type="checkbox" name="term_sheet_new">Term Sheet – New</td>
						
											
					</tr>
					<tr>
								
					<td><input type="checkbox" name="privacy_notice_disclosure">Loan Application – Privacy Notice</td>
					<td><input type="checkbox" name="re885_demo">RE885</td>
					
					</tr>

					<tr>
						<td><input type="checkbox" name="automated_valuation_notice">Loan Application – Valuation Notice</td>
						<td><input type="checkbox" name="loan_folder_tabs">Loan Folder Tabs</td>				
						
					</tr>
					<tr>
						<td><input type="checkbox" name="diligence_materials_list">Due Diligence Schedule</td>
						<td><input type="checkbox" name="appraisal_assignment_form">Appraisal Assignment Form </td>		
						
					</tr>
					<tr>
						<td><input type="checkbox" name="ach_construction_draw">Construction Draw Wire Setup Form</td>
						<td><input type="checkbox" name="credit_report_form">Credit Report Form</td>										
					</tr>

					<tr>
						<td><input type="checkbox" name="transaction_contact">Transaction Contacts </td>

					</tr>

					<tr>
						<td colspan="2"><h3>Escrow / Title Documents</h3></td>
					</tr>
					<tr>
						<td><input type="checkbox" name="lender_closing_statement">Lender Closing Statement</td>
          				<td><input type="checkbox" name="schedule_income_wire">Incoming Wire Schedule</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="lender_schedule_print">Lender Schedule</td>
						<td><input type="checkbox" name="new_loan_setup_form">New Loan Setup Form</td>
					</tr>

					<tr>
						<td><input type="checkbox" name="InitialLenderInstructions">Initial Lender Instructions</td>
						<td></td>
					</tr>

					<tr>
						<td colspan="2"><h3>Loan Documents</h3></td>
					</tr>
					<tr>
						<td><input type="checkbox" name="loan_verification">Loan Verification Form </td>
						<td><input type="checkbox" name="LoanDocumentCheck">Loan Document Check </td>
					</tr>		
			     	<tr>
						<td><input type="checkbox" name="loan_term">Loan Terms</td>
						<td></td>							
					</tr>
					<tr>
						<td><input type="checkbox" name="escrow_instrusction">Escrow Lender Instructions</td>
						<td></td>							
					</tr>



					<tr>
						<td><input type="checkbox" name="arbitration_of_disputes">Arbitration of Disputes</td>
						
						<td><input type="checkbox" name="fair_lending_notice">Fair Lending Notice</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="anti_money_document">Anti-Money Laundering Declaration Document</td>
						<td><input type="checkbox" name="fci_construction_disbursement_agreement">FCI Construction Disbursement Agreement</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="ass_of_construct_contract">Assignment of Construction Contracts</td>
						<td><input type="checkbox" name="fedral_equal_opprtunity_act">Federal Equal Opportunity Act</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="ballon_payment_disclosures">Balloon Payment Disclosures</td>
						<td><input type="checkbox" name="first_payment_notify">First Payment Notification</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="new_payment_form">Borrower Auto Payment Form</td>
						<td><input type="checkbox" name="payment_gurranty">Payment Guaranty</td>	
					</tr>
					<tr>
								
						
					</tr>
					<tr>
						<td><input type="checkbox" name="borrower_certification">Borrower Certification & Authorization</td>
					<td><input type="checkbox" name="promissory_note_secure_trust">Promissory Note Secured by Deed of Trust</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="builders_risk_insurance_req">Builders Risk Insurance Requirements</td>
						<td><input type="checkbox" name="insurance_endrosement">General Liability Insurance Requirements</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="california_insurance_disclosure">California Insurance Disclosure</td>
					
						<td></td>		
					</tr>
					<tr>
						
							<td><input type="checkbox" name="capacity_to_repay_disclosure">Capacity to Repay Disclosure</td>
						<td><input type="checkbox" name="request_for_notice">Request for Notice</td>			
					</tr>
					<tr>
						
					<td><input type="checkbox" name="certificate_bussiness_purpose_loan">Certificate of Business Purpose Loan</td>
						<td><input type="checkbox" name="default_provision">Rider – Default Provision</td>	
					</tr>
					<tr>
						
						
						<td><input type="checkbox" name="compliance_agreement">Compliance Agreement</td>
						<td><input type="checkbox" name="rider_interest_reserve">Rider – Interest Reserve</td>	
					</tr>
					<tr>
						
						<td><input type="checkbox" name="consumer_notice">Consumer Notices</td>			
						<td><input type="checkbox" name="attachment_a_construct_draw">Rider – Construction Draw Schedule</td>

					</tr>
					<tr>
						<?php 
						$daa='';
						$unchecked = '';
						if($fetch_loan_borrower)
						{
							if($fetch_loan_borrower->borrower_type == '1'){ 
								$daa='disabled';
								$unchecked='unchecked';
							}else{
								$daa='';
								
							}
						}

						?>
						<td><input type="checkbox" name="corporate_resolution" <?php echo $daa; ?> <?php echo $unchecked; ?>>Corporate Resolution</td>
						<td><input type="checkbox" name="subordination_lease_agreement">Subordination Lease Agreement</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="title_lender_instrusction">Title/Lender Instruction</td>
						<td><input type="checkbox" name="credit_score_disclosure">Credit Score Disclosure</td>
					</tr>
					<tr>
						
						<td><input type="checkbox" name="TemporaryExtensionAgreement">Temporary Extension Agreement</td>
						<td></td>								
					</tr>
					<tr>
						

						
					</tr>
					<tr>
						
						<td><input type="checkbox" name="deed_trust_assigment_rents">Deed of Trust w/ Assignment of Rents</td>
						<td></td>	
					</tr>

                    <tr>
					<td><input type="checkbox" name="exclusive_devt_ratio">Excessive Debt Ratio</td>
						<td></td>
						</tr>
					<tr>
						<td colspan="2"><h3>Loan Servicing Documents</h3></td>
					</tr>

					<tr>
						<td><input type="checkbox" name="accrued_charges">Accrued Charges</td>
						<td><input type="checkbox" name="forclosure_update">Foreclosure Status Update</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="ApprovaltoShareInformation">Approval to Share Information</td>
						<td><input type="checkbox" name="late_payment_notice">Late Payment Notice (10+ Days)</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="loan_assignment">Assignment of Deed of Trust</td>
						<td><input type="checkbox" name="extention_approval">Lender Extension Approval Request</td>	
					</tr>
					<tr>
						<td><input type="checkbox" name="assignment_request">Assignment Request</td>
						<td><input type="checkbox" name="loan_maturity_notification">Loan Maturity Notification</td>
						
						
					</tr>
					<tr>
						<td><input type="checkbox" name="ballon_payment_demand">Balloon Payment Demand</td>
					
						<td><input type="checkbox" name="loan_maturity_notification_extopt">Loan Maturity Notification – Extension Option</td>
						
					</tr>
					<tr>
						<td><input type="checkbox" name="BorrowerAuthorizationForm">Borrower Authorization Form</td>
						<td><input type="checkbox" name="overview_loan">Overview of Loan Terms</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="borrower_verification">Borrower Verification</td>
						
						<td><input type="checkbox" name="payoff_checklist">Payoff Checklist</td>	
					</tr>
					<tr>
						<td><input type="checkbox" name="construction_draw_modiAgree">Construction Draw Modification Agreement</td>
						<td><input type="checkbox" name="property_review">Property Data Review</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="construction_draw_form">Construction Draw Approval Form</td>	
						<td><input type="checkbox" name="property_tax_notice">Property Tax Notice</td>
					</tr>
					<tr>
														
						<td><input type="checkbox" name="html_reserve">Construction Draw Schedule / Balance</td>
						<td><input type="checkbox" name="reserve_modification_agreement">Reserve Modification Agreement</td>
					</tr>
					<tr>
						
						<td><input type="checkbox" name="construction_draw_wire_instruction">Construction Draw Wire Instructions</td>
						<td><input type="checkbox" name="ServicingIntroductionCall">Servicing Introduction Call</td>
					</tr>
					<tr>
						
						<td><input type="checkbox" name="download_del_toro_excel_file">Del Toro Boarding Form</td>
						<td><input type="checkbox" name="Servicer_Review_Checklist">Servicer Review Checklist</td>
							
					</tr>
					<tr>
						
						<td><input type="checkbox" name="extension_agreement">Extension Agreement</td>
						<td><input type="checkbox" name="Servicer_Funds_Deposit_Notice">Servicer Funds Deposit Notice </td>
							
					</tr>
					<tr>
						<td><input type="checkbox" onclick="fn_fciboardingdata(this);">FCI Boarding Data</td>
						<td><input type="checkbox" name="title_assigment_request">Title Assignment Request</td>
						
					</tr>
					<tr>
						<td><input type="checkbox" name="FCIPayoffRequest">FCI Payoff Request</td>
						<td><input type="checkbox" name="transfer_servicing_disclosure">Transfer of Servicing Disclosure (RESPA)</td>
					</tr>
					<tr>
						<td colspan="2"><h3>Default Notices</h3></td>
					</tr>
					<tr>
						<td><input type="checkbox" name="default_notice">Default Notice - Late Payment</td>
						<td><input type="checkbox" name="default_notice_lm">Default Notice – Loan Maturity</td>
					</tr>
					<tr>
						
						<td><input type="checkbox" name="default_property_tax">Default Notice – Property Taxes</td>	
						<td><input type="checkbox" name="default_insurance_adv">Default Notice – Notice of Insurance Advancement</td>	
						
					</tr>
				</table>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn default" data-dismiss="modal">Close</button>
			<button type="submit" class="btn blue" >Print</button>
			<input type="submit" class="btn red" name="signloanNow" value="DocuSign">
		</div>
	</form>
</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>