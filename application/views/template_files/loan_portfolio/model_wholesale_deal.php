<style type="text/css">
	.icon_position{
		margin-top: 30px;
	}
</style>
<div class="modal fade" id="wholesale_deal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-body">
				<form method="POST" action="<?php echo base_url()?>Load_data/add_wholesale_deal" id="wholesale_deal_frm" enctype="multipart/form-data">	
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Wholesale Deal : <?php echo $property_addresss; ?></h4>
					</div>
					<!-----------Hidden fields ---------->
					<input type="hidden" name = "wholesale_id" id="wholesale_id" value = "<?php if(!empty($fetch_wholesale_deal[0]->wholesale_id)){ echo $fetch_wholesale_deal[0]->wholesale_id;}?>">
					<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
					<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
			
					<!-----------Pu content here---------->
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">	
								<label  for="textinput">Post</label>
								<select class="form-control " name="wholesale_post" id="wholesale_post">
									<option value="0" <?php if(!empty($fetch_wholesale_deal[0]->wholesale_post) && $fetch_wholesale_deal[0]->wholesale_post=="0"){ echo "selected";}?>>No</option>
									<option value="1" <?php if(!empty($fetch_wholesale_deal[0]->wholesale_post) && $fetch_wholesale_deal[0]->wholesale_post=="1"){ echo "selected";}?>>Yes</option>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">	
								<label  for="textinput">Status</label>
								<select class="form-control" name="wholesale_status" id="wholesale_status">
									<option value="">Select One</option>
									<option value="available" <?php if(!empty($fetch_wholesale_deal[0]->wholesale_status) && $fetch_wholesale_deal[0]->wholesale_status=="available"){ echo "selected";}?>>Available</option>
									<option value="reserved" <?php if(!empty($fetch_wholesale_deal[0]->wholesale_status) && $fetch_wholesale_deal[0]->wholesale_status=="reserved"){ echo "selected";}?>>Reserved</option>
									<option value="coming_soon" <?php if(!empty($fetch_wholesale_deal[0]->wholesale_status) && $fetch_wholesale_deal[0]->wholesale_status=="coming_soon"){ echo "selected";}?>>Coming Soon</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">	
								<label  for="textinput">Wholesaler</label>
								<input type="hidden" name="hide_wholesaler" id="hide_wholesaler" value="<?php if(!empty($fetch_wholesale_deal[0]->wholesaler)){ echo $fetch_wholesale_deal[0]->wholesaler;}?>">
								<select class="form-control select_name" name="wholesaler" id="wholesaler">
									<option value="">Select One</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">	
								<label  for="textinput">City</label>
								<input type="text" name="wholesaler_city" id="wholesaler_city" class="form-control city" value="<?php if(!empty($fetch_wholesale_deal[0]->wholesaler_city)){ echo $fetch_wholesale_deal[0]->wholesaler_city;}?>">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">	
								<label  for="textinput">State</label>
								<select class="form-control select_name" name="wholesaler_state" id="wholesaler_state">
									<option value="">Select One</option>
									<?php
									if(!empty($STATE_USA)){
										foreach ($STATE_USA as $key => $value) {
										?>
										<option value = "<?php echo $key; ?>" <?php if(!empty($fetch_wholesale_deal[0]->wholesaler_state) && $fetch_wholesale_deal[0]->wholesaler_state==$key){ echo "selected";}?>><?php echo $value; ?></option>
										<?php 
										}
									}?>	
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">	
								<label  for="textinput">Purchase Price</label>
								<input type="text" name="wholesaler_purchase_price" id="wholesaler_purchase_price" class="form-control" value="<?php if(!empty($fetch_wholesale_deal[0]->wholesaler_purchase_price)){ echo "$".number_format($fetch_wholesale_deal[0]->wholesaler_purchase_price, 2);}?>">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">	
								<label  for="textinput">Rehab Cost</label>
								<input type="text" name="wholesaler_rehab_cost" id="wholesaler_rehab_cost" class="form-control" value="<?php if(!empty($fetch_wholesale_deal[0]->wholesaler_rehab_cost)){ echo "$".number_format($fetch_wholesale_deal[0]->wholesaler_rehab_cost, 2);}?>">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">	
								<label  for="textinput">Completion Value</label>
								<input type="text" name="wholesaler_completion_value" id="wholesaler_completion_value" class="form-control" value="<?php if(!empty($fetch_wholesale_deal[0]->wholesaler_completion_value)){ echo "$".number_format($fetch_wholesale_deal[0]->wholesaler_completion_value, 2);}?>">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">	
								<label  for="textinput">Close Date</label>
								<input type="text" name="wholesaler_close_date" id="wholesaler_close_date" class="form-control datepicker" value="<?php if(!empty($fetch_wholesale_deal[0]->wholesaler_close_date)){ echo date('m-d-Y',strtotime($fetch_wholesale_deal[0]->wholesaler_close_date));}?>" autocomplete="off">
							</div>
						</div>
					</div>
					<h2>Interest List</h2>
					<div class="row" id="interest_list_data">
						<?php
						if(!empty($fetch_interest_deal)){
							for($j=0;$j<count($fetch_interest_deal);$j++){
								?>
								<div class="col-md-12" id="interest_list_<?php echo $fetch_interest_deal[$j]->interest_id;?>">
									<div class="col-md-3">
										<div class="form-group">	
											<label  for="textinput">Name</label>
											<input type="hidden" name="wholesaler_interest_id[]" id="wholesaler_interest_id_<?php echo $j;?>" value="<?php echo $fetch_interest_deal[$j]->interest_id;?>"   onchange="get_populate_values(this,'<?php echo $j;?>')">
											<input type="hidden" name="wholesaler_contact_id[]" id="wholesaler_contact_id_<?php echo $j;?>" value="<?php if(!empty($fetch_interest_deal[$j]->contact_id)){ echo $fetch_interest_deal[$j]->contact_id;}?>"   >
											<select name="wholesaler_contact_list[]" id="wholesaler_contact_list_<?php echo $j;?>"  class="chosen43 wholesaler_contact_list" >
												<option value="">Select One</option>
											</select>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">	
											<label  for="textinput">Phone</label>
											<input type="text" name="wholesaler_contact_phone[]" id="wholesaler_contact_phone_<?php echo $j;?>" class="form-control city" value="<?php if(!empty($fetch_interest_deal[$j]->contact_phone)){ echo $fetch_interest_deal[$j]->contact_phone;}?>">
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">	
											<label  for="textinput">E-Mail</label>
											<input type="text" name="wholesaler_contact_email[]" id="wholesaler_contact_email_<?php echo $j;?>" class="form-control city" value="<?php if(!empty($fetch_interest_deal[$j]->contact_email)){ echo $fetch_interest_deal[$j]->contact_email;}?>">
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">	
											<label  for="textinput">Status</label>
											<select class="form-control select_name" name="wholesaler_contact_status[]" id="wholesaler_contact_status_<?php echo $j;?>">
												<option value="">Select One</option>
												<option value="1" <?php if(!empty($fetch_interest_deal[$j]->contact_status) && $fetch_interest_deal[$j]->contact_status=="1"){ echo "selected";}?>>Interested</option>
												<option value="2" <?php if(!empty($fetch_interest_deal[$j]->contact_status) && $fetch_interest_deal[$j]->contact_status=="2"){ echo "selected";}?>>Passed</option>
												<option value="3" <?php if(!empty($fetch_interest_deal[$j]->contact_status) && $fetch_interest_deal[$j]->contact_status=="3"){ echo "selected";}?>>Due Diligence</option>	
												<option value="4" <?php if(!empty($fetch_interest_deal[$j]->contact_status) && $fetch_interest_deal[$j]->contact_status=="4"){ echo "selected";}?>>Purchased</option>		
											</select>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group icon_position">
											<label  for="textinput">&nbsp;</label>
											<?php
											if($j==0){
												?>
												<a title="Add People" onclick="add_interest_row();" class="btn btn-sm btn-info plus_icon_add_more" >
													<i class="fa fa-plus"></i>
												</a>
											<?php
											}else{
												?>
												<a title="Add People" onclick="delete_interest_row('<?php echo $fetch_interest_deal[$j]->interest_id;?>');" class="btn btn-sm btn-danger plus_icon_add_more" >
													<i class="fa fa-trash"></i>
												</a>
												<?php
											}
											?>
										</div>
									</div>
								</div>
								<?php
							}
						}else{
						?>
							<div class="col-md-12" id="interest_list_0">
								<div class="col-md-3">
									<div class="form-group">	
										<label  for="textinput">Name</label>
										<input type="hidden" name="wholesaler_interest_id[]" id="wholesaler_interest_id_0" value=""   >
										<input type="hidden" name="wholesaler_contact_id[]" id="wholesaler_contact_id_0" value=""   >
										<select name="wholesaler_contact_list[]" id="wholesaler_contact_list_0"  class="chosen43 wholesaler_contact_list" onchange="get_populate_values(this,'0')">
											<option value="">Select One</option>	
										</select>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">	
										<label  for="textinput">Phone</label>
										<input type="text" name="wholesaler_contact_phone[]" id="wholesaler_contact_phone_0" class="form-control city" value="">
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">	
										<label  for="textinput">E-Mail</label>
										<input type="text" name="wholesaler_contact_email[]" id="wholesaler_contact_email_0" class="form-control city" value="">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">	
										<label  for="textinput">Status</label>
										<select class="form-control select_name" name="wholesaler_contact_status[]" id="wholesaler_contact_status_0">
											<option value="">Select One</option>
											<option value="1">Interested</option>
											<option value="2">Passed</option>
											<option value="3">Due Diligence</option>	
											<option value="4">Purchased</option>		
										</select>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group icon_position">
										<label  for="textinput">&nbsp;</label>
										<a title="Add People" onclick="add_interest_row();" class="btn btn-sm btn-info plus_icon_add_more" >
											<i class="fa fa-plus"></i>
										</a>
									</div>
								</div>
							</div>
						<?php
						}?>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">	
								<label  for="textinput">Image</label>
								<input type="file" name="image_name" id="image_name" class="form-control" onchange="loadFile(event)">
							</div>
						</div>
						<div class="col-md-4" >
							<?php 
							if(!empty($fetch_wholesale_deal[0]->image_name))
							{ 
								$uploadPath=base_url().'property_images/' . $talimar_loan . '/';
								$imageName= $fetch_wholesale_deal[0]->image_name;
								$imagePath=$uploadPath.$imageName;
								?>
								<img src="<?php echo $imagePath;?>" height="150" width="150" id="output">
							<?php
							}else{
								?>
								<img  height="150" width="150" id="output" style="display:none">
								<?php
							}?>
						</div>
					</div>
					<!-----------Pu content here---------->
					<br>
					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn blue">Save</button>					
					</div>
				</form>
			</div>	
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<script type="text/javascript">
	var row_no='<?php echo count($fetch_interest_deal);?>';
	contact_add_n();	
	function contact_add_n(){
		$.ajax({
			type	:'POST',
			url		:'/LoanPortfolioPopup/fetch_contact_details',
			data	:{},
			success : function(response){	
					if($('.wholesaler_contact_list').length>0){
						$(".wholesaler_contact_list").each(function() {							
							var select_id_name=$(this).attr('id');
							select_id=select_id_name.replace("wholesaler_contact_list_", "wholesaler_contact_id_");
							var select_value=$("#"+select_id).val();
						    $(this).html(response);
						    $(this).val(select_value);
					 		$('#'+select_id_name).chosen();
						});
						var hide_wholesaler=$("#hide_wholesaler").val();
						$("#wholesaler").html(response);
						if(hide_wholesaler!=""){
							$("#wholesaler").val(hide_wholesaler);
						}
						$("#wholesaler").chosen();
					}
				
 			}				
		});
 	} 
 	function newRowContact_list(row_value){
 		console.log("value row"+row_value);
 		var wholesaler_contact_list = 	$('select[name="wholesaler_contact_list[]"]').map(function () {
										    return this.value; 
										}).get().join(',');
 		$.ajax({
			type	:'POST',
			url		:'/LoanPortfolioPopup/fetch_contact_details',
			data	:{'contact_list':wholesaler_contact_list},
			success : function(response){	
					$("#wholesaler_contact_list_"+row_value).html(response);
					$("#wholesaler_contact_list_"+row_value).chosen();				
 			}				
		});
 	}
</script>
<script type="text/javascript">
	function add_interest_row(){
		row_no=parseInt(row_no)+1;
		console.log("row no"+row_no);
		var str="";
		str+='<div class="col-md-12" id="interest_list_'+row_no+'">';
		str+='<div class="col-md-3"><div class="form-group"><label  for="textinput">Name</label><input type="hidden" name="wholesaler_interest_id[]" id="wholesaler_interest_id_'+row_no+'" value=""   ><select class="chosen43 wholesaler_contact_list" name="wholesaler_contact_list[]" id="wholesaler_contact_list_'+row_no+'" onchange="get_populate_values(this,`'+row_no+'`)"  ><option value="">Select One</option>	</select></div></div>';
		str+='<div class="col-md-2"><div class="form-group"><label  for="textinput">Phone</label><input type="text" name="wholesaler_contact_phone[]" id="wholesaler_contact_phone_'+row_no+'" class="form-control city" value=""></div></div>';
		str+='<div class="col-md-2"><div class="form-group"><label  for="textinput">E-Mail</label><input type="text" name="wholesaler_contact_email[]" id="wholesaler_contact_email_'+row_no+'" class="form-control city" value=""></div></div>';
		str+='<div class="col-md-3"><div class="form-group"><label  for="textinput">Status</label><select class="form-control select_name" name="wholesaler_contact_status[]" id="wholesaler_contact_status_'+row_no+'"><option value="">Select One</option><option value="1">Interested</option><option value="2">Passed</option><option value="3">Due Diligence</option>	<option value="4">Purchased</option>	</select></div></div>';
		str+='<div class="col-md-2"><div class="form-group icon_position"><label  for="textinput">&nbsp;</label><a title="Remove Row" onclick="remove_interest_row(`'+row_no+'`);" class="btn btn-sm btn-danger plus_icon_add_more" ><i class="fa fa-trash"></i></a></div></div>';
		str+='</div>';
		$("#interest_list_data").append(str);
		newRowContact_list(row_no);
		
	}
	function remove_interest_row(del_row_no){
		var del_row_no=parseInt(del_row_no);
		$("#interest_list_"+del_row_no).remove();
	}
	function delete_interest_row(interested_id){
		 $.ajax({
				type: "post",
				url: "/LoanPortfolioPopup/delete_wholesale_interest_row",
				data:{'interested_id':interested_id},
				success: function(response){
					$("#interest_list_"+interested_id).remove();
				}
		 });
	}
	function get_populate_values(that,row_count){
		var element = $(that).find('option:selected'); 
        var phone = element.attr("phone"); 
        var email = element.attr("email"); 
		$("#wholesaler_contact_phone_"+row_count).val(phone);
		$("#wholesaler_contact_email_"+row_count).val(email);
	}
	var loadFile = function(event) {
		$("#output").show();
	    var output = document.getElementById('output');
	    output.src = URL.createObjectURL(event.target.files[0]);
	    output.onload = function() {
	      URL.revokeObjectURL(output.src) // free memory
	    }
  	};
</script>