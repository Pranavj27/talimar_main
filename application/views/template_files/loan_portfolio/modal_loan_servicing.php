<div class="modal fade bs-modal-lg" id="loan_servicing" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">					
		<div class="modal-content">
		<form method="POST" action="<?php echo base_url()?>loan_servicing_form">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<!--<h4 class="modal-title">Loan Servicing : #<?php // echo $talimar_loan;?></h4>-->
				<h4 class="modal-title">Loan Servicing : <?php echo $property_addresss;?></h4>
			</div>
			<div class="modal-body">
			
				<!-----------Hidden fields----------->
				<input type="hidden" value="loan_servicing" name="form_type"/>
				<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
				
				<input type="hidden" name="loan_id" value="/<?php echo $loan_id; ?>">
				<!-----------End of hidden Fields---->
				<div class="first_block_content">
						
					<div class="first_block first">
						
						<!--<div class="row">
							<div class="form-group">
							  <label style = "line-height: 45px;" class="col-md-4 control-label" for="textinput">Addl. Lender Disclosures: </label>  
							  <div class="col-md-4" style = "margin-top:10px;">
								<a class="btn blue" data-toggle="modal" href="#additional_broker_data">Lender Disclosure </a>
							
								<span class="help-block"></span>  
							  </div>
							</div>
						</div>-->
						<h4>Loan Status</h4>
						<div class="row">
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">Loan Status:</label>  
							  <div class="col-md-6">
							  <select id="loan_status" name="loan_status" type="text" class="form-control input-md">
							  <?php foreach($loan_status_option as $key => $row){
								  ?>
								  <option value="<?php echo $key;?>" <?php if($loan_servicing['loan_status'] == $key){ echo "selected";
								  } ?> ><?php echo $row;?></option>
								  <?php
									}
							  
							  ?>
							  </select>
							  <span class="help-block"></span>  
							  </div>
							</div>
							
						</div>
					

						<div class="row" id="term_sheet_div">
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">Term Sheet Status:</label>  
							  <div class="col-md-6">
							  <select id="term_sheet_status" name="term_sheet_status" type="text" class="form-control input-md">
							  <?php foreach($term_sheet_option as $key => $row){
								  ?>
								  <option value="<?php echo $key;?>" <?php if($loan_servicing['term_sheet_status'] == $key){ echo 'selected'; }?> ><?php echo $row;?></option>
								  <?php
									}
							  
							  ?>
							  </select>
							  <span class="help-block"></span>  
							  </div>
							</div>
						</div>

						<div id="loan_status_active_base">
						
							
								
							<div class="row">
								<div class="form-group">
								  <label class="col-md-4 control-label" for="textinput">Loan Condition:</label>  
								  <div class="col-md-6">
								  <select id="service_condition" name="condition" type="text" class="form-control input-md">
								  <?php foreach($serviceing_condition_option as $key => $row){
									  ?>
									  <option value="<?php echo $key;?>" <?php if($loan_servicing['condition'] == $key){ echo 'selected'; }?> ><?php echo $row;?></option>
									  <?php
										}
								  
								  ?>
								  </select>
								  <span class="help-block"></span>  
								  </div>
								</div>
							</div>
							
							<div class="row">
								<div class="form-group">
								  <label class="col-md-4 control-label" for="textinput">Payment Status:</label>  
								  <div class="col-md-6">
								  <select id="loan_payment_status" name="loan_payment_status" type="text" class="form-control input-md">
								  <?php foreach($loan_payment_status as $key => $row){
									  ?>
									  <option value="<?php echo $key;?>" <?php if($loan_servicing['loan_payment_status'] == $key){ echo 'selected'; }?> ><?php echo $row;?></option>
									  <?php
										}
								  
								  ?>
								  </select>
								  <span class="help-block"></span>  
								  </div>
								</div>
							</div>

							<div class="row">
								<div class="form-group">
								  <label class="col-md-4 control-label" for="textinput">Maturity Status:</label>  
								  <div class="col-md-6">
								  <select id="loan_maturity_status" name="loan_maturity_status" type="text" class="form-control input-md">
								  <?php foreach($loan_maturity_status as $key => $row){
									  ?>
									  <option value="<?php echo $key;?>" <?php if($loan_servicing['loan_maturity_status'] == $key){ echo 'selected'; }?> ><?php echo $row;?></option>
									  <?php
										}
								  
								  ?>
								  </select>
								  <span class="help-block"></span>  
								  </div>
								</div>
							</div>
							
							<?php 
							if($fetch_loan_result[0]->loan_type == '2' || $fetch_loan_result[0]->loan_type == '3')
							{
							?>
							<div class="row">
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Construction Status:</label>  
									<div class="col-md-6">
									  <select class="form-control input-md" name="construction_status">
									  <?php 
										foreach($construction_status_option as $key => $row)
										{
											?>
											<option value="<?php echo $key; ?>" <?php if($loan_servicing['construction_status'] == $key){ echo 'selected'; } ?> ><?php echo $row;?></option>
											<?php
										}
									  ?>
									  </select>
									</div>
								</div>
							</div>
							<?php } ?>
							
							
						
						
						
						
						
						<div class="row">
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">Listed of Market:</label>  
							  <div class="col-md-6">
							  <select id="textinput" name="list_market" type="text"  class="form-control input-md" onchange="enterSalePrice(this.value);">
							   <?php foreach($yes_no_option_67 as $key => $row){
								  ?>
								  <option value="<?php echo $key;?>" <?php if($loan_servicing['list_market'] == $key){ echo 'selected'; } ?> ><?php echo $row;?></option>
								  <?php
									}
							  
							  ?>
							  </select>
							  <span class="help-block"></span>  
							  </div>
							</div>
						</div>
						
						<div class="row" id="market_list" style="display:none;">
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">List Price:</label>  
							  <div class="col-md-6">
							  <input name="list_market_SalePrice" type="text" class="form-control input-md" value="<?php echo $loan_servicing['list_market_SalePrice'] ? '$'.number_format($loan_servicing['list_market_SalePrice']) : ''; ?>">
							  <span class="help-block"></span>  
							  </div>
							</div>
						</div>						
					</div>
						
						<div id="service_paidoff_requested">
						<div class="row" >
							<!--<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">Payoff Requested:</label>  
							  <div class="col-md-6">
							  <select id="payoff_request" name="payoff_request" type="text"  class="form-control input-md">
							   <?php foreach($yes_no_option as $key => $row){
								  ?>
								  <option value="<?php echo $key;?>" <?php if($loan_servicing['payoff_request'] == $key){ echo 'selected'; } ?>  ><?php echo $row;?></option>
								  <?php
									}
							  
							  ?>
							  </select>
							  <span class="help-block"></span>  
							  </div>
							</div>-->
						</div>
						 
						
						<div class="row" id="payoff_yes_selected">
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">If Yes , Expected Date:</label>  
							  <div class="col-md-6">
							  <input id="pay_off_resquested_date" name="pay_off_resquested_date" type="text" placeholder="DD-MM-YYYY" class="form-control input-md" value="<?= $loan_servicing['pay_off_resquested_date']; ?>">
							  <span class="help-block"></span>  
							  </div>
							</div>
						</div>
						
						</div>
						
						<div id="paid_of_based">
						
						<div class="row">
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">Payoff Date:</label>  
							  <div class="col-md-6">
							  <input id="payoff_date" name="payoff_date" type="text"  class="form-control input-md" value="<?= $loan_servicing['payoff_date'] ? date('m-d-Y',strtotime($loan_servicing['payoff_date'])) : ''; ?>" >
							  <span class="help-block"></span>  
							  </div>
							</div>
						</div>	
						
						<div class="row">
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">Payoff Status:</label>  
							  <div class="col-md-6">
							  <select  name="paidoff_status" class="form-control" >
							  <?php
							  foreach($paidoff_status_option as $key => $row)
							  {
								  ?>
								  <option value="<?php echo $key;?>" <?php if($loan_servicing['paidoff_status'] == $key){ echo 'selected'; } ?> ><?php echo $row; ?></option>
								  <?php
							  }
							  ?>
							  </select>
							  <span class="help-block"></span>  
							  </div>
							</div>
						</div>
						
						</div>
						
						<div id="dead_reason">
							<div class="row">
									<div class="form-group">
									  <label class="col-md-4 control-label" for="textinput">Reason:</label>  
									  <div class="col-md-6">
									  <select id="dead_reason" name="dead_reason" type="text"  class="form-control input-md">
									  <?php foreach($dead_reason_option as $key => $row){
										  ?>
										  <option value="<?php echo $key;?>" <?php if($loan_servicing['dead_reason'] == $key){ echo 'selected'; }?> ><?= $row;?></option>
										  <?php
											}
									  
									  ?>
									  </select>
									  <span class="help-block"></span>  
									  </div>
									</div>
								</div>
						</div>
						<?php 


						if($loan_servicing['cancelled_date'] == ''){

						$date='';
						}
						else{

							$date = date('m-d-Y',strtotime($loan_servicing['cancelled_date']));
						}
							

						?>
							<div id="cancel_date">
					<div class="row" >
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">Cancelled Date:</label>  
							  <div class="col-md-6">
							  <input id="cancelled_date" name="cancelled_date" type="text" class="form-control input-md" value="<?php echo $date; ?>" >
							  <span class="help-block"></span>  
							  </div>
							</div>
						</div>	
						</div>
						<h4>Closing Status</h4>
						<div class="row" id="ter">
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">Term Sheet Signed:</label>  
							  <div class="col-md-6">
							  <select id="term_sheet"  name="term_sheet" type="text" class="form-control input-md">
							  <?php foreach($term_sheet_closing_option as $key => $row){
								  ?>
								  <option value="<?php echo $key;?>" <?php if($loan_servicing['term_sheet'] == $key){ echo "selected";
								  } ?> ><?php echo $row;?></option>
								  <?php
									}
							  
							  ?>
							  </select>
							  <span class="help-block"></span>  
							  </div>
							</div>
							
						</div>
						<div class="row">
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">Closing Status:</label>  

							  <div class="col-md-6">

							  <select id="closing_status" name="closing_status" type="text" class="form-control input-md">
							  <?php foreach($closing_status_option as $key => $row){
								  ?>
								  <option value="<?php echo $key;?>" <?php if($loan_servicing['closing_status'] == $key){ echo "selected";
								  } ?> ><?php echo $row;?></option>
								  <?php
									}
							  
							  ?>
							  </select>
							  <span class="help-block"></span>  
							  </div>
							</div>
						</div>
						
						<h4>Loan Servicer</h4>
						<div class="row">
							<div class="form-group">
							
							  <label class="col-md-3 control-label" for="textinput">
							  Loan Servicer:</label> 
							 <!-- <label class="col-md-3 control-label" for="textinput">Servicing Agent:</label> 
							  -->
							  <div class="col-md-6">
							  <select id="servicing_agent" name="servicing_agent"  class="form-control input-md">
								<!--<option value = "">Select One</option>	-->
							 <?php 
								 foreach($new_servicer_option as $key => $row){
									//foreach($vendor_name as $key => $row){
								  
								  
								  ?>
								  <option value="<?php echo $key;?>" <?php if($loan_servicing['servicing_agent'] == $key){ echo 'selected'; }?> ><?php echo $row;?></option>
								  <?php
									}
							  
							  ?>
							  </select>
							  <span class="help-block"></span>  
							  </div>
							</div>
						</div>
						
						<div class="row">
							<div class="form-group">
							  <label class="col-md-3 control-label" for="textinput">Loan Subservicer:</label> 
							 <!-- <label class="col-md-4 control-label" for="textinput">Sub Servicing Agent:</label>  -->
							  <div class="col-md-6">
							  <select id="subservicing_agent" name="sub_servicing_agent" class="form-control input-md">
								<!--<option value = "">Select One</option>-->
							 <?php


								foreach($newsub_servicer_option as $key => $row){
									//foreach($vendor_name as $key => $row){
								  	
								?>
								  <option value="<?php echo $key;?>" <?php if($loan_servicing['sub_servicing_agent'] == $key){ echo 'selected'; } ?> ><?php echo $row;?></option>
								  <?php
									}
							  
							  ?>
							  </select>
							  <span class="help-block"></span>  
							  </div>
							</div>
						</div>

						<div class="row">
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">Broker Disb. Verified:</label>  
							  <div class="col-md-6">
							  <select name="servicing_disb" type="text" class="form-control input-md">
								
							  <?php
							
								foreach($yes_no_option3 as $key => $row){
								  ?>
								  <option value="<?php echo $key;?>" <?php if($loan_servicing['servicing_disb'] == $key){echo 'Selected';} ?>><?php echo $row;?></option>
								  <?php
								}
							  ?>
							  </select>
							  <span class="help-block"></span>  
							  </div>
							</div>
						</div>
						
						<!--<div class="row">
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">Payment Reserve:</label>  
							  <div class="col-md-6">
							  
								<input type="text" class="form-control input-md" value="<?php echo $yes_no_option3[$fetch_loan_result[0]->interest_reserve]; ?>" readonly>
							  <span class="help-block"></span>  
							  </div>
							</div>
						</div>-->
						
						<div class="row">
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">Payment Reserve Used:</label>  
							  <div class="col-md-6">
							  <select name="payment_reserve_opt" id="payment_reserve_opt" type="text" class="form-control input-md" >
								
							  <?php
								foreach($not_applicable_option as $key => $row){
								  ?>
								  <option value="<?php echo $key;?>"><?php echo $row;?></option>
								  <?php
								}
							  ?>
							  </select>
							  <span class="help-block"></span>  
							  </div>
							</div>
						</div>

						<div class="row">
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">ACH Activated:</label>  
							  <div class="col-md-6">
							  <select name="ach_activated" id="ach_activated" type="text" class="form-control input-md" >
								
							  <?php
								foreach($ach_activated as $key => $row){
								  ?>
								  <option value="<?php echo $key;?>" <?php if($loan_servicing['ach_activated'] == $key){ echo "selected"; } ?>><?php echo $row;?></option>
								  <?php
								}
							  ?>
							  </select>
							  <span class="help-block"></span>  
							  </div>
							</div>
						</div>
						<br>
							<?php	//Loan Boarding Request...
								$outlook_property_addressA = $outlook_property[0]->property_address;
								if($outlook_property[0]->unit){
									$outlook_property_addressA .= ' '.$outlook_property[0]->unit;
								}
								$outlook_property_addressA .= '; ';
								$outlook_property_addressA .= $outlook_property[0]->city.', '; 
								$outlook_property_addressA .= $outlook_property[0]->state.' '.$outlook_property[0]->zip;
								$outlook_property_address = $outlook_property_addressA;

								$bording_request = '';
								//$bording_request .= 'Hi '.$fname.' - %0D%0A';
								$bording_request .= 'Hello - %0D%0A';
								$bording_request .= '%0D%0A';
								$bording_request .= 'We are requesting that you board the attached loan. Below is a link to the underlying loan and lender documents. %0D%0A';
								$bording_request .= '%0D%0A';
								$bording_request .= 'Loan Position: '.$position_option[$outlook_property[0]->position].' %0D%0A';
								$bording_request .= 'Loan Amount: $'.number_format($outlook_property[0]->loan_amount).' %0D%0A';
								$bording_request .= 'Closing Date: '.date('m-d-Y', strtotime($outlook_property[0]->loan_funding_date)).' %0D%0A';
								$bording_request .= 'Property Address: '.$outlook_property_address.' %0D%0A';
								$bording_request .= '# of Lenders: '.$lender_co.' %0D%0A';
								$bording_request .='%0D%0A';

								//$bording_request .= 'Renovation Reserve*: $'.number_format($fetch_closing_statement_items_dataa[0]->paid_to_broker).'  %0D%0A';
								//$bording_request .= 'Interest Reserve / Pre-Paid Interest*: $'.number_format($fetch_closing_statement_items_dataaa[0]->paid_to_broker).'  %0D%0A';
								//$bording_request .= '%0D%0A';
								//$bording_request .= '*If the loan includes an interest reserve/partial months interest and/or a renovation reserve, the funds will be mailed to you directly with the property address included on the check.%0D%0A';
								$bording_request .='Please respond to this e-mail confirming receipt and please include the Loan Servicing Number. %0D%0A';
								$bording_request .= '%0D%0A';
								$bording_request .= 'Unless otherwise instructed, do not apply any payments to principal. Please bill TaliMar Financial for the boarding costs associated with this loan.%0D%0A';
								$bording_request .= '%0D%0A';
								$bording_request .= 'Please contact me immediately should have any questions or concerns regarding this loan. %0D%0A';
								//$bording_request .= '%0D%0A';
								$bording_request .= '%0D%0A';
								$bording_request .= 'Regards –  %0D%0A';

							?>
					
					<h4>Notifications</h4>

						<div class="row">
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">Payment Reserve Exhausted:</label>  
							  <div class="col-md-6">
							  <select id="payment_Exhausted" name="payment_Exhausted" type="text" class="form-control input-md" style="margin-top: 18px;">
							  <?php foreach($payment_remind as $key => $row){
								  ?>
								  <option value="<?php echo $key;?>" <?php if($loan_servicing['payment_Exhausted'] == $key){ echo "selected";
								  } ?> ><?php echo $row;?></option>
								  <?php
									}
							  
							  ?>
							  </select>
							  <span class="help-block"></span>  

							  </div>
							</div>

						</div>
						
						<div class="row">
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">Payment Reminder:</label>  
							  <div class="col-md-6">
							  <select id="payment_notification" name="payment_notification" type="text" class="form-control input-md">
							  <?php foreach($payment_remind as $key => $row){
								  ?>
								  <option value="<?php echo $key;?>" <?php if($loan_servicing['payment_notification'] == $key){ echo "selected";
								  } ?> ><?php echo $row;?></option>
								  <?php
									}
							  
							  ?>
							  </select>
							  <span class="help-block"></span>  

							  </div>
							</div>

						</div>	
						
						
						<div class="row">
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">Renovation Status Update:</label>  
							  <div class="col-md-6">
							  <select id="renovation" name="renovation" type="text" class="form-control input-md">
							  <?php foreach($payment_remind as $key => $row){
								  ?>
								  <option value="<?php echo $key;?>" <?php if($loan_servicing['renovation'] == $key){ echo "selected";
								  } ?> ><?php echo $row;?></option>
								  <?php
									}
							  
							  ?>
							  </select>
							  <span class="help-block"></span>  
							  
							  </div>
							</div>

						</div>
						
					
						
					</div>
				
					<div class="first_block">
						<div class="row">&nbsp;</div>
						
						<a class="btn btn-primary" data-toggle="modal" href="#email_scripts" style="width:37.5% !important; text-align:left !important;"><span class="">Email Scripts</span></a>
						<a class="btn btn-primary" href="mailto:Loanboarding-ls@trustfci.com?subject=Loan Boarding Request&body=<?php echo $bording_request;?>" style="width:37.5% !important; text-align:left !important;"><span class="">Submit to Servicer</span></a>
						<br><br>

						<!-- <a class="btn btn-primary" data-toggle="modal" href="#LoanDocument" style="width:37.5% !important; text-align:left !important;"><span class="">Loan Document</span></a>
						<br><br> -->
						
						<a class="btn btn-primary" data-toggle="modal" href="#LoanBoarding" style="width:37.5% !important; text-align:left !important;"><span class="">Servicer Charges</span></a>
						<a class="btn btn-primary" data-toggle="modal" href="#PaymentHistory" style="width:37.5% !important; text-align:left !important;"><span class="">Payment History</span></a>
						<br><br>
						
						<a class="btn btn-primary" data-toggle="modal" href="#loan_contacts" style="width:37.5% !important; text-align:left !important;"><span class="">Internal Contacts</span></a>
						<a class="btn btn-primary" data-toggle="modal" href="#autoNotification" style="width:37.5% !important; text-align:left !important;"><span class="">Status</span></a>
						<br><br>
						
						<a class="btn btn-primary" data-toggle="modal" href="#loan_closing_checklist" style="text-align:left !important;"><span class="">Loan Closing Checklist</span></a>
						<br><br>

						<a class="btn btn-primary" data-toggle="modal" href="#auto_debit_payment" style="width:37.5% !important;text-align:left !important;"><span class="">Auto Debit Payment</span></a>
						<br><br>
						
						<a class="btn btn-primary" data-toggle="modal" onclick="afflilation_data(this);" href="#affiliated_parties" style="width:37.5% !important; text-align:left !important;"><span class="">Affiliated Parties</span></a>
						<br><br>
						<a class="btn btn-primary" data-toggle="modal" href="#marketing_option" style="width:37.5% !important;text-align:left !important;"><span class="">Marketing</span></a>
						<br><br>
						<a class="btn btn-primary" data-toggle="modal" href="#accured_charges" style="width:37.5% !important;text-align:left !important;"><span class="" >Accrued Charges</span></a>
						<br><br>
						<a class="btn btn-primary" data-toggle="modal" href="#marketing_extention" style="width:37.5% !important;text-align:left !important;"><span class="" >Extension Processing</span></a>
							<br><br>
					
						<a class="btn btn-primary" data-toggle="modal" href="#forcloser_status" style="width:37.5% !important; text-align:left !important;"><span class="">Foreclosure Status</span></a>
					<br><br>
					<?php 

				
					 if($loan_servicing['loan_status'] == '3'){
						  
							
						 
						 ?>
						
						<a class="btn btn-primary gayabXXXXXXX"  data-toggle="modal" href="#payoff_pro" style="width:37.5% !important;text-align:left !important;"><span class="" >Process Payoff</span></a>
							
					 <?php }else{?>
						<a class="btn btn-primary gayabXXXXXXX"  data-toggle="modal" href="#payoff_pro" style="width:37.5% !important;text-align:left !important;" ><span class="" >Process Payoff</span></a>
				
					<?php }?>

					</div>				
					
				</div> 
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<button type="submit" name="form_button" value="save" class="btn blue" >Save</button>
			</div>
		</form>
		</div>
	<!-- /.modal-content -->
	</div>
<!-- /.modal-dialog -->
</div>	


<?php /* E-mail Scripts */ ?>
<div class="modal fade in" id="email_scripts" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form method="POST" action ="<?php echo base_url();?>">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					
						<h4 class="modal-title">E-mail Scripts: <?php echo $property_addresss;?></h4>
					</div>
					<div class="modal-body">
						
						<input type="hidden" name="talimar_loan" value="<?php echo $talimar_loan;?>">
						<input type="hidden" name="loan_id" value="<?php echo $loan_id;?>">
						<?php
							$aa = 0;
							if(isset($fetch_impound_account)){
								foreach($fetch_impound_account as $key=>$row){
									$impound_amount = $row->amount;
									$readonly = '';						
									if($row->items == 'Mortgage Payment')
									{						
										$aa += ($fetch_loan_result[0]->intrest_rate/100) * $fetch_loan_result[0]->loan_amount / 12;
									}
									if($row->items == 'Property / Hazard Insurance Payment')
									{
										if($row->impounded == 1){
											$aa += isset($loan_property_result) ? ($loan_property_insurance_result->annual_insurance_payment / 12) : 0;
										}
									}
															
									if($row->items == 'County Property Taxes')
									{
										if($row->impounded == 1){
											$aa += isset($loan_property_result) ? ($loan_property_result[0]->annual_property_taxes / 12) : 0;
										}
									}
									if($row->items == 'Flood Insurance')
									{
										if($row->impounded == 1){
											$aa += isset($row->amount) ? ($row->amount) : 0;
										}
									}
															
									if($row->items == 'Mortgage Insurance')
									{
										if($row->impounded == 1){
											$aa += isset($row->amount) ? ($row->amount) : 0;
										}
									}
								}
							}
							?>
							
							<?php
							// 5 Day Past Due Notice...
								$message = '';
								//$message .= 'Hi '.$draws_contact_first_name.' - %0D%0A';
								//$message .= '%0D%0A'; 
								$message .= 'This e-mail is a friendly reminder that payment is due on the loan listed below: %0D%0A';
								$message .= '%0D%0A';
								$message .= 'Due Date: '. date('m-01-Y').' %0D%0A';
								$message .= 'Payment Amount: $'.number_format($aa,2).' %0D%0A';
								$message .= 'Property Address: '.$outlook_property[0]->property_address.''.$outlook_property[0]->unit.'; '.$outlook_property[0]->city.', '.$outlook_property[0]->state.' '.$outlook_property[0]->zip .' %0D%0A';
								$message .= 'FCI Loan Number: #'.$outlook_property[0]->fci.' %0D%0A';
								$message .= 'Grace Period: 9 days after the Due Date %0D%0A';
								$message .= 'Late Payment Fee: 10% of payment due %0D%0A';
								$message .= '%0D%0A';
								$message .= 'Please submit payment directly to FCI Lender Services at 8180 East Kaiser Blvd.; Anaheim Hills, CA 92808 and include the account # listed above. You may also make a payment directly to FCI Lender Services by visiting their website at www.trustfci.com. For more immediate payment processing, you may call FCI at (714) 282-2424 to make a payment by phone. %0D%0A';
								$message .= '%0D%0A';
								$message .= 'To sign up for monthly autopay, please visit http://www.trustfci.com/BorrowerPaymentPaybyAutomatedPaymentsACH.html. %0D%0A';
								$message .= '%0D%0A';
								$message .= 'Please do not hesitate to contact TaliMar Financial with any questions. If you have already made the payment, please disregard this e-mail. %0D%0A';
								$message .= '%0D%0A';
								$message .= 'Regards - %0D%0A';
								//$message .= 'TaliMar Financial Loan Servicing %0D%0A';
								//$message .= 'Office: (858) 201-3253 %0D%0A';
								//$message .= '16880 West Bernardo Drive, Suite 140 %0D%0A';
								//$message .= 'San Diego, CA 92127 %0D%0A';
								//$message .= 'www.talimarfinancial.com';
								
								
							// Borrower Welcome E-mail...
							if($loan_servicing_data[0]->servicing_agent = '14'){
									$outlook_servicer = 'FCI Lender Services';
								}elseif($loan_servicing_data[0]->servicing_agent = '15'){
									$outlook_servicer = 'TaliMar Financial Inc.';
								}elseif($loan_servicing_data[0]->servicing_agent = '16'){
									$outlook_servicer = 'La Mesa Fund Control';
								}elseif($loan_servicing_data[0]->servicing_agent = '19'){
									$outlook_servicer = 'Del Toro Loan Servicing';
								}else{
									$outlook_servicer = '';
								}


								$paid_to_others_1005 = 0;
								$paid_to_others_1006 = 0;
					
								foreach($fetch_closing_statement_items_data as $huds){

									if($huds->hud == '1005'){

										$paid_to_others_1005 = $huds->paid_to_other + $huds->paid_to_broker;
										
									}
									
									if($huds->hud == '1006'){

										
										$paid_to_others_1006 = $huds->paid_to_other + $huds->paid_to_broker;
									}
								}


								$welcome = '';
								$welcome .= 'Hi '.$draws_contact_first_name.'-%0D%0A';
								$welcome .= '%0D%0A'; 
								$welcome .= 'We want to thank you for trusting TaliMar Financial to fund the loan secured on '.$outlook_property[0]->property_address.'; '.$outlook_property[0]->city.', '.$outlook_property[0]->state.' '.$outlook_property[0]->zip.'. Please take a moment to review the following items: %0D%0A'; 
								$welcome .= '%0D%0A';
								$welcome .= 'Loan Servicing: %0D%0A';
								$welcome .= ''.$vendor_name[$loan_servicing_data[0]->sub_servicing_agent].' will handle the servicing of this loan. Within the next two weeks you will receive a Welcome Borrower Package from them providing you instructions on how and where to make payments. %0D%0A';
								$welcome .= '%0D%0A';
								$welcome .= 'Payment Reserve: %0D%0A';
								$welcome .= 'This loan includes a payment reserve of $'.number_format($paid_to_others_1006,2).'.  All payments will be debited from this reserve until such time as the funds are fully drawn. %0D%0A';
								$welcome .= '%0D%0A';
								$welcome .= 'Construction Funds Reserve: %0D%0A';
								$welcome .= 'This loan includes a construction funds reserve of $'.number_format($paid_to_others_1005,2).'. Funds will be released to you based upon the draw schedule in the promissory note. Please submit all draw requests clearly showing the property address in the body of the e-mail to servicing@talimarfinancial.com. Include images clearly displaying the requirement(s) for that specific draw have been completed. Once approved, the funds will be wired to you within 24 hours.  %0D%0A';
								$welcome .= '%0D%0A';
								$welcome .= 'Payoff: %0D%0A';
								$welcome .= 'When requesting a payoff, please have the escrow officer e-mail a payoff demand request to servicing@talimarfinancial.com. Please note, the payoff demand may take up to 5 business days to process so please plan accordingly. %0D%0A';
								$welcome .= '%0D%0A';
								$welcome .= 'Should you have any questions, please contact us. %0D%0A';
								$welcome .= '%0D%0A';
								$welcome .= 'Regards -  %0D%0A';


								//Lender Proposal script...
							
								$outlook_property_address = $outlook_property[0]->property_address.''.$outlook_property[0]->unit.'; '.$outlook_property[0]->city.', '.$outlook_property[0]->state.' '.$outlook_property[0]->zip;
								
								$lender_proposal = '';
								$lender_proposal .= 'Hi '.$draws_contact_first_name.', %0D%0A';
								$lender_proposal .= '%0D%0A';
								$lender_proposal .= 'I have attached to this email our Term Sheet for the loan to be secured on '.$outlook_property[0]->property_address.'.  Please take a moment to review and approve the terms. Please note that this Term Sheet is not an approval to lend, but simply a proposal of terms based upon the information you have provided to us. %0D%0A';
								$lender_proposal .= '%0D%0A';
								$lender_proposal .= 'I have also attached to this email our Loan Input Form. In our best effort to expedite the processing of your loan request we ask that you complete and return the attached at your earliest convenience.  %0D%0A';
								$lender_proposal .= '%0D%0A';
								$lender_proposal .= 'Once we have received the attached back from you, we will proceed to the loan application and schedule a call with you to review the required diligence items. %0D%0A';
								$lender_proposal .= '%0D%0A';
								$lender_proposal .= 'Should you have any questions, please do not hesitate to contact Brock or myself.   %0D%0A';
								$lender_proposal .= '%0D%0A';
								$lender_proposal .= 'Regards, %0D%0A';

								
							//Loan Application Submitted script...
								
								$application_script = '';
								$application_script .= 'Hi {borrower_first_name},  %0D%0A';

								$application_script .= '%0D%0A';
								$application_script .= 'Shortly you will receive an e-mail from DocuSign with a link which will allow you to access and complete our Loan Application and Credit Authorization. We ask that you review and sign the documents at your earliest convenience. %0D%0A';
								$application_script .= '%0D%0A';
								$application_script .= 'I have attached to this email our Due Diligence Schedule with the list of items that we will require for underwriting. Please take a moment to review the list and submit any outstanding items at your earliest convenience. The items listed will be required to obtain final loan approval. %0D%0A';
								$application_script .= '%0D%0A';

								$application_script .= 'Should you have any questions, please do not hesitate to contact us.  %0D%0A';
								$application_script .= '%0D%0A';
								$application_script .= 'Regards,   %0D%0ATaliMar Financial';

								if($escrow_contact_name['full_name'] == '0'){
									$outlook_contact_name = $escrow_contact_name['first_name'].' '.$escrow_contact_name['last_name'];
									
									$fname = $escrow_contact_name['first_name'].' '.$escrow_contact_name['last_name'];
									
								}else{
									
									$outlook_contact_name = $escrow_contact_name['full_name'];
									
									$fname = $escrow_contact_name['full_fname'];
								}

							//EscrowOpened

								$EscrowOpened = '';
								$EscrowOpened .= 'Hi '.$draws_contact_first_name	.' - %0D%0A';
								$EscrowOpened .= '%0D%0A';
								$EscrowOpened .= 'This email is to provide you a status update on the closing status of '.$outlook_property[0]->property_address.'. TaliMar Financial has notified '.$fname.' with '.$escrow_contact_name['company'].' that we are the lender on this transaction. We have requested that they provide us with the preliminary title report and estimated settlement statement.  %0D%0A';
								$EscrowOpened .= '%0D%0A';
								$EscrowOpened .= 'Once these items have been received by our office, we will review for loan documents. TaliMar Financial will be prepared to wire closing funds within 48 hours of receipt of the following: %0D%0A';
								$EscrowOpened .= '%0D%0A';
								$EscrowOpened .= '1.) Copy of signed loan documents %0D%0A';
								$EscrowOpened .= '2.) Proof of Insurance %0D%0A';
								$EscrowOpened .= '2.) Confirmation that your closing funds have been deposited (if required)  %0D%0A';
								$EscrowOpened .= '2.) Confirmation all conditions to close have been met (including demands and lien releases)  %0D%0A';
								$EscrowOpened .= '%0D%0A';
								$EscrowOpened .= 'Should you have any questions, please do not hesitate to contact us. %0D%0A';
								$EscrowOpened .= '%0D%0A';
								$EscrowOpened .= 'Regards –%0D%0A';


							//appraisal_order

								
								$FinalLoanDisclosures = '';
								$FinalLoanDisclosures .= 'Hi {borrower_contact_name},  %0D%0A';
								$FinalLoanDisclosures .= '%0D%0A';
								$FinalLoanDisclosures .= 'Shortly you will receive an e-mail from DocuSign with a link which will request your approval of the Final Loan Application, RE885 Mortgage Loan Disclosure Statement, and ACH Auto Debit form.%0D%0A %0D%0A';								 
								
								$FinalLoanDisclosures .= 'We ask that you review and sign the documents at your earliest convenience. Once complete, we will proceed to final loan closing.%0D%0A %0D%0A';

								$FinalLoanDisclosures .= 'If you see any errors or have any questions, please contact us immediately. %0D%0A %0D%0A';
								
								$FinalLoanDisclosures .= 'Regards, %0D%0ATaliMar Financial'; 

								$appraisal_order = '';
								$appraisal_order .= 'Hi '.$draws_contact_first_name	.', %0D%0A';
								$appraisal_order .= '%0D%0A';
								$appraisal_order .= 'We have ordered the appraisal through Appraisal Nation. Shortly you will receive an e-mail from them requesting payment. Once you have paid for the appraisal, they will schedule an appraiser to visit the property. Should you have any questions regarding the appraisal process, please contact me at (858) 613-0111. %0D%0A';
								$appraisal_order .= '%0D%0A';
								$appraisal_order .= 'Regards,  %0D%0ATaliMar Financial';
								
								
							//Loan Documents Submitted...	
								
								

								//echo $outlook_contact_name;
								
								$doc_submit_script = '';
								$doc_submit_script .= 'Hi '.$draws_contact_first_name.'- %0D%0A';

								$outlook_property_addresss = $outlook_property[0]->property_address.' '.$outlook_property[0]->unit.'; '.$outlook_property[0]->city.', '.$outlook_property[0]->state.' '.$outlook_property[0]->zip;

								$doc_submit_script .= '%0D%0A';
								//$doc_submit_script .= 'The loan documents for '.$outlook_property_addresss.' have been submitted to escrow. Please contact '.$outlook_contact_name.' at '.$fetch_escrow_data[0]->phone.' or '.$fetch_escrow_data[0]->email.' to schedule the signing. You have the option to sign the loan documents at the Escrow office or request an Escrow designated notary to meet you at another location. %0D%0A';
								$doc_submit_script .= '%0D%0A';
								$doc_submit_script .= 'The Loan Documents for '.$outlook_property_addresss.' have been submitted to '.$fetch_escrow_data[0]->company.'.  Please contact '.$fname.' at '.$fetch_escrow_data[0]->phone.' or '.$fetch_escrow_data[0]->email.' to schedule the signing. You may coordinate a signing or request that an escrow designated Notary meet you at a convenient location.%0D%0A';
								$doc_submit_script .= '%0D%0A';


								$doc_submit_script .= 'Please remember to have a valid Driver’s License or other form of valid identification at time of signing. %0D%0A';
								$doc_submit_script .= '%0D%0A';
								$doc_submit_script .= 'Should you have any questions, please do not hesitate to contact us.   %0D%0A';
								$doc_submit_script .= '%0D%0A';
								//$doc_submit_script .= '%0D%0A';
								$doc_submit_script .= 'Regards –  %0D%0A';



								//new loan doc script...

								$loan_submit_new_script = '';
								$loan_submit_new_script .= 'Hi '.$draws_contact_first_name.' – %0D%0A';
								$loan_submit_new_script .= '%0D%0A';
								$loan_submit_new_script .= 'The loan documents for '.$outlook_property_address.' have been submitted to escrow. Please contact '.$outlook_contact_name.' at '.$fetch_escrow_data[0]->phone.' to schedule the signing. You have the option to sign the loan documents at the Escrow office or request an Escrow designated notary to meet you at another location.';
								$loan_submit_new_script .= '%0D%0A';
								$loan_submit_new_script .= 'Please remember to have a valid driver’s license or other form of identification for the signing.%0D%0A';
								$loan_submit_new_script .= '%0D%0A';
								$loan_submit_new_script .= 'Should you have any questions, please do not hesitate to contact TaliMar Financial at (858) 613-0111. %0D%0A';
								$loan_submit_new_script .= '%0D%0A';
								$loan_submit_new_script .= 'Regards - %0D%0A';

							
								//Property Insurance Request script...
			
								$property_insurance_script = '';
								$property_insurance_script .= 'Hi '.$draws_contact_first_name.' - %0D%0A';
								$property_insurance_script .= '%0D%0A';
								$property_insurance_script .= 'We received notification that the policy insuring '.$outlook_property_address.' is set to expire on '.date('m-d-Y',strtotime($loan_property_insurance_result->insurance_expiration)).' %0D%0A';
								$property_insurance_script .= '%0D%0A';
								$property_insurance_script .= 'Please provide us an updated declaration policy showing an active policy on the property. You may e-mail us a copy of the declaration page directly. %0D%0A';
								$property_insurance_script .= '%0D%0A';
								$property_insurance_script .= 'If you have any questions,  please contact us. %0D%0A';
								$property_insurance_script .= '%0D%0A';
							
								$property_insurance_script .= 'Regards – %0D%0A';
								
							//Payoff Received script...
							
								if($loan_servicing_data[0]->servicing_agent = '14'){
									$outlook_servicer = 'FCI Lender Services';
								}elseif($loan_servicing_data[0]->servicing_agent = '15'){
									$outlook_servicer = 'TaliMar Financial Inc.';
								}elseif($loan_servicing_data[0]->servicing_agent = '16'){
									$outlook_servicer = 'La Mesa Fund Control';
								}elseif($loan_servicing_data[0]->servicing_agent = '19'){
									$outlook_servicer = 'Del Toro Loan Servicing';
								}else{
									$outlook_servicer = '';
								}
							
							
								$payoff_script = '';
								$payoff_script .= 'Hi '.$draws_contact_first_name.' - %0D%0A';
								$payoff_script .= '%0D%0A';
								$payoff_script .= 'This e-mail is confirmation that TaliMar Financial has received the payoff for '.$outlook_property_address.'. '.$outlook_servicer.' will reimburse you any overpayment that may have occurred at payoff and issue you the 1098-INT early next year. %0D%0A';
								$payoff_script .= '%0D%0A';
								$payoff_script .= 'We want to thank you for trusting TaliMar Financial to finance this transaction. Please contact us for any hard money lending needs in the future. %0D%0A';
								$payoff_script .= '%0D%0A';
								$payoff_script .= 'Should you have any questions regarding this transaction and/or future transactions, please do not hesitate to contact me. %0D%0A';
								$payoff_script .= '%0D%0A';
								//$payoff_script .= '%0D%0A';
								$payoff_script .= 'Regards – %0D%0A';
								
								
							//Maturity Notification script...

								if($outlook_property[0]->extention_option == 1){

									$loan_amount = $outlook_property[0]->loan_amount;

									$month = $outlook_property[0]->extention_month;

									$extension_fee = '$'.number_format(($outlook_property[0]->extention_percent_amount/100) * $loan_amount,2);

								}else{

									$month = 'N/A';
									$extension_fee = 'N/A';
								}
								
								$maturity_script = '';
								$maturity_script .= 'Hi '.$draws_contact_first_name.' - %0D%0A';
								$maturity_script .= '%0D%0A';

								$maturity_script .= 'The loan secured on '.$outlook_property_address.' is set to mature on '.date('m-d-Y', strtotime($outlook_property[0]->maturity_date)).'. Please submit your payoff demand request to payoff@talimarfinancial.com at least 2 weeks prior to the maturity date to ensure we have sufficient time to provide you the final payoff demand. %0D%0A';
								$maturity_script .= '%0D%0A';

								//$maturity_script .= 'The loan includes an Extension option of '.$month.' months.  In order to extend the loan, you must sign the Extension Agreement Letter that will be e-mailed to you shortly via DocuSign and submit the Extension Fee of '.$extension_fee.' to '.$outlook_servicer.' at 8180 E. Kaiser Blvd; Anaheim Hills, CA 92808 prior to the maturity date. %0D%0A';
								//$maturity_script .= 'The loan includes an Extension option of '.$month.' months.  In order to extend the loan, you must submit a written request to servicing@talimarfinancial.com. Upon receipt of the request, we will submit for approval. If approved, you will sign the Extension Agreement and submit the extension fee of '.$extension_fee.' to FCI Lender Services at 8180 E. Kaiser Blvd; Anaheim Hills, CA 92808 prior to the maturity date. %0D%0A';
								$maturity_script .= 'If you do not feel you can payoff the loan prior to the maturity date, please contact TaliMar Financial immediately to discuss alternative options. %0D%0A';
								//$outlook_property_address
								$maturity_script .= '%0D%0A';
								//$maturity_script .= 'You also have the option of making the final balloon payment loan prior to the maturity date. If you prefer to make the balloon payment, please submit a payoff request to servicing@talimarfinancial.com within 14 days of maturity. %0D%0A';
								//$maturity_script .= '%0D%0A';
								$maturity_script .= 'Please do not hesitate to contact TaliMar Financial with any questions at (858) 613-0111. %0D%0A';
								$maturity_script .= '%0D%0A';
								//$maturity_script .= '%0D%0A';
								$maturity_script .= 'Regards – %0D%0A';

								//maturity_script_ext

								$maturity_script_ext = '';
								$maturity_script_ext .= 'Hi '.$draws_contact_first_name.' - %0D%0A';
								$maturity_script_ext .= '%0D%0A';

								$maturity_script_ext .= 'The loan secured on '.$outlook_property_address.' is set to mature on '.date('m-d-Y', strtotime($outlook_property[0]->maturity_date)).'. You have the option to extend the loan per the terms of the Promissory Note or make the final balloon payment. %0D%0A';
								$maturity_script_ext .= '%0D%0A';
								$maturity_script_ext .= 'If you intend to extend the loan, please submit your written request to servicing@talimarfinancial.com. %0D%0A';
								$maturity_script_ext .= '%0D%0A';
								$maturity_script_ext .= 'If you intend to payoff the loan, please submit your payoff request to payoff@talimarfinancial.com. %0D%0A';
								$maturity_script_ext .= '%0D%0A';
								$maturity_script_ext .= 'Please do not hesitate to contact TaliMar Financial with any questions at (858) 613-0111. %0D%0A';
								$maturity_script_ext .= '%0D%0A';
								$maturity_script_ext .= 'Regards – %0D%0A';



								$extension_payment = '';
								$extension_payment .= 'Hi '.$draws_contact_first_name.' - %0D%0A';
								$extension_payment .= '%0D%0A';
							
								$extension_payment .= 'This email is confirmation TaliMar Financial has received your signed Extension Agreement. Attached to this email is a copy of the signed Agreement. Please submit the Extension Fee totaling '.$extension_fee.' to FCI Lender Services within 10 business days of '.date('m-d-Y', strtotime($outlook_property[0]->maturity_date)).' to ensure the Loan Modification is processed prior to the Loan Maturity. Please make the check payable to FCI Lender Services and mail the check to 8180 E. Kaiser Blvd; Anaheim Hills, CA 92808. Please be sure to reference the Loan Number #'.$outlook_property[0]->fci.' on the check. %0D%0A';
								$extension_payment .= '%0D%0A';
								
								$extension_payment .= 'Please note, if the Extension Fee or the Balloon Payment are not received prior to the maturity date, the loan will be considered to be in default and subject to the default terms. %0D%0A';

								$extension_payment .= '%0D%0A';
								$extension_payment .= 'Should you have any questions, please do not hesitate to contact us. %0D%0A';
								$extension_payment .= '%0D%0A';
								
								$extension_payment .= 'Regards – %0D%0A';
						

								/*$loan_extension_approval = '';
				
								$loan_extension_approval .= 'TaliMar Financial has approved a 6-month extension for loan #'.$outlook_property[0]->fci.'. The signed Extension Agreement has been attached to this email.  %0D%0A';
								
								$loan_extension_approval .= '%0D%0A';
								
								$loan_extension_approval .= 'The Borrower has been instructed to submit the Extension Fee payment in the amount of '.$extension_fee.' directly to FCI Lender Services. The loan modification is not to be processed until FCI is in receipt of the Extension Fee payment.  %0D%0A';

								$loan_extension_approval .= 'Once received, the Extension Fee payment is to be disbursed to TaliMar Financial 75% and the Lender(s) 25% based upon their interest in the loan. Please notify us once the fee has been received.   %0D%0A';

								$loan_extension_approval .= '%0D%0A';
								$loan_extension_approval .= 'Should you have any questions, please do not hesitate to contact us.  %0D%0A';
								$loan_extension_approval .= '%0D%0A';

								
								$loan_extension_approval .= 'Regards – %0D%0A';*/
						
							//Escrow loan document script...
								
								
								$escrow_doc = '';
								$escrow_doc .= 'Hi '.$fname.' - %0D%0A';
								$escrow_doc .= '%0D%0A';
								$escrow_doc .= 'I have attached the loan documents for '.$outlook_property_address.'. The loan documents include our Lender Closing Statement and Lender Instructions. I have also attached the Incoming Wire Schedule if the loan has multiple beneficiaries. %0D%0A';
								$escrow_doc .= '%0D%0A';
								$escrow_doc .= 'If there is a partial months, interest payment, interest reserve, or renovation reserve associated with this loan, you are to wire the funds to FCI Lender Services (or other 3rd party if noted) per the Lender Instructions. %0D%0A';
								$escrow_doc .= '%0D%0A';
								$escrow_doc .= 'We are prepared to wire our funds to Title within 48 hours of receiving the following: %0D%0A';
								$escrow_doc .= '%0D%0A';
								$escrow_doc .= '(1) Electronic copies of the signed loan documents %0D%0A';
								$escrow_doc .= '(2) Proof of Insurance with Lender as Mortgagee %0D%0A';
								$escrow_doc .= '(3) Approval of the Estimated Closing Statement %0D%0A';
								$escrow_doc .= '(4) Confirmation that the Borrower has deposited their funds %0D%0A';
								$escrow_doc .= '%0D%0A';
								$escrow_doc .= 'Please confirm receipt of documents. Should you have any questions, please contact me. %0D%0A';
								//$escrow_doc .= '%0D%0A';
								$escrow_doc .= '%0D%0A';
								$escrow_doc .= 'Regards –  %0D%0A';
								
							//Escrow Introduction script...
													
								
								$escrow_intro = '';
								$escrow_intro .= 'Hi '.$fname .' - %0D%0A';
								$escrow_intro .= '%0D%0A';
						
								//$escrow_intro .= 'TaliMar Financial will be the Lender financing the '.$loan_type_option[$outlook_property[0]->loan_type].' of '.$outlook_property_address.' for '.$outlook_borrower[0]->b_name.'. Our loan in the amount of $'.number_format($outlook_property[0]->loan_amount).' is to be secured in '.$position_option[$outlook_property[0]->position].' position. %0D%0A';
								$escrow_intro .= 'TaliMar Financial has been selected to be the lender on '.$outlook_property_address.' for '.$outlook_borrower[0]->b_name.'. Our loan will be $'.number_format($outlook_property[0]->loan_amount).' and will be in '.$position_option[$outlook_property[0]->position].' position. %0D%0A';

								$escrow_intro .= '%0D%0A';

								$escrow_intro .='Attached to this e-mail is our Estimate Lender Closing Statement reflecting our loan amount, fees and any Lender Reserve hold backs. Please note, the actual Lender name may change prior to close and will be reflected on the Final Lender Instructions submitted with our loan document package. %0D%0A';
								$escrow_intro .= '%0D%0A';
								$escrow_intro .= 'At your earliest convenience, would you please e-mail me the Preliminary Title Report and the Title Wiring Instructions. Once we receive the PTR, we will draft and submit the loan documents to you for borrower signature. %0D%0A';
								$escrow_intro .= '%0D%0A';
								$escrow_intro .= 'We are prepared to wire our funds to Title within 48 hours of receiving the following:  %0D%0A';
								$escrow_intro .= '%0D%0A';
								$escrow_intro .= '(1) Electronic copies of the signed loan documents %0D%0A';
								$escrow_intro .= '(2) Proof of Insurance with Lender as Mortgagee  %0D%0A';
								$escrow_intro .= '(3) Approval of the Estimated Closing Statement  %0D%0A';
								$escrow_intro .= '(4) Confirmation that the Borrower has deposited their funds  %0D%0A';
								$escrow_intro .= '%0D%0A';
								// $escrow_intro .= 'Please note we will wire our funds directly to the title. %0D%0A';
								// $escrow_intro .= '%0D%0A';
								
								$escrow_intro .= 'Should you have any questions please do not hesitate to contact me. %0D%0A';
								//$escrow_intro .= '%0D%0A';
								$escrow_intro .= '%0D%0A';
								$escrow_intro .= 'Regards – %0D%0A';
								
								
								//.............executive_summary start ..................//
								$property_typee = $this->config->item('property_typee');
																	
								$executive_summary = '';
								//$executive_summary .= 'Hi '.$draws_contact_name.' - %0D%0A';
								$executive_summary .= 'Hi XXXX- %0D%0A';
								$executive_summary .= '%0D%0A';
								
								$executive_summary .= 'I have attached the Executive Loan Summary for a $'.number_format($outlook_property[0]->loan_amount).' trust deed investment opportunity. The trust deed is secured in '.$position_option[$outlook_property[0]->position].' position on a '.$property_typee[$loan_servicing_data[0]->marketing_property_type].' in '.$outlook_property[0]->city.', '.$outlook_property[0]->state.' at '.number_format($ltv,2).'% of '.$valuation_type[$outlook_property[0]->valuation_type].'. The term of the trust deed is '.$term_month.' months and pays a net '.$lender_yield_percent.'% yield to the Lender(s). We are offering the trust deed on a minimum fractional basis of $'.number_format($loan_servicing['lender_minimum']).'. %0D%0A';
								$executive_summary .= '%0D%0A';
								
								$executive_summary .= 'Please note, we offer these trust deeds on a first come, first serve basis and cannot hold an investment. Additionally, we will give preference to whole note investors if the entire loan has not been fully subscribed at the time of their commitment.  %0D%0A';
								$executive_summary .= '%0D%0A';
								
								$executive_summary .= 'If you are interested in proceeding with this trust deed opportunity, please notify us immediately to secure your interest. %0D%0A';
								$executive_summary .= '%0D%0A';
								$executive_summary .= '%0D%0A';
								$executive_summary .= 'Regards – %0D%0A';
								
								//.............confirm start ..................//
								
								$confirm = '';
							   // $confirm .=  'Hi '.$draws_contact_name.' - %0D%0A';
							    $confirm .=  'Hi XXXX - %0D%0A';
								$confirm .= '%0D%0A';
								$confirm .= 'To secure your interest in the loan, we ask that you review and confirm your approval of the following: %0D%0A';
								$confirm .= '%0D%0A';
								//foreach($final_array as $row){

										$confirm .= '. Property Address: '.$outlook_property[0]->property_address.' '.$outlook_property[0]->unit.'; '.$outlook_property[0]->city.' '.$outlook_property[0]->state.' '.$outlook_property[0]->zip.'%0D%0A . Investment Amount: XXX %0D%0A . Lender Vesting: XXX %0D%0A . Closing Date: '. date('m-d-Y',strtotime($outlook_property[0]->loan_funding_date)).'%0D%0A';
                    			
										$confirm .= '%0D%0A';
								//}
								$confirm .= '%0D%0A';
								 
								$confirm .= 'Please note, the trust deed lender disclosures are to be signed and funds are to be wired to title, or the recipient indicated in the disclosures, on or before '. date('m-d-Y',strtotime($outlook_property[0]->loan_funding_date)).'. If you are unable to fund by that date, please notify us immediately so that we can discuss any alternative arrangements. %0D%0A'; 
								$confirm .= '%0D%0A';

								$confirm .='If you are using a self-directed IRA to fund the trust deed, we request that you select “expedited processing” on the investment setup form issued to you by your IRA custodian. %0D%0A';
								$confirm .= '%0D%0A';
							
								$confirm .= 'Please note, if you are purchasing a fractional interest in this note and the loan has not been fully subscribed at the time of close, a whole note investor may still be given priority of the trust deed investment. %0D%0A';
								$confirm .= '%0D%0A';
								
								$confirm .= 'Regards – %0D%0A';
								
								
								//.............Disclosures Available start ..................//
								
								$disclose = '';
								//$disclose .= 'Hi '.$draws_contact_name.' - %0D%0A';
								
								//$disclose .= '%0D%0A';
								
								$disclose .= 'Shortly, you will receive an e-mail from DocuSign with a link to the Trust Deed Lender Disclosures.  Please review and sign at your earliest convenience.  If you find any errors, please contact us immediately so that we can make the appropriate corrections. The funding instructions will be sent to you under a separate email in short time. %0D%0A';
								$disclose .= '%0D%0A';
							
								
								$disclose .= '%0D%0A';
								$disclose .='Should you have any questions, please do not hesitate to contact us. %0D%0A';
								
								$disclose .= '%0D%0A';
								$disclose .= 'Regards – %0D%0A';
								
							//.............Funds Received start ..................//	
							
								
								$fund_rec = '';
							   // $fund_rec .= 'Hi '.$draws_contact_name.' - %0D%0A';
							    $fund_rec .= 'Hi XXXX- %0D%0A';
								//$fund_rec .= 'Hi - %0D%0A';
								$fund_rec .= '%0D%0A';
								
							     $fund_rec .='This e-mail is confirmation that '.$title_contact['company'].' has received your funds in the amount of $'.number_format($outlook_property[0]->loan_amount).' for the loan secured on  '.$primary_loan_property->property_address .'. %0D%0A';
								 $fund_rec .= '%0D%0A';
								 $fund_rec .='We will provide you a status update once the loan has recorded and we have submitted the loan to '.$vendor_name[$loan_servicing_data[0]->sub_servicing_agent].'. %0D%0A';
								 $fund_rec .= '%0D%0A';
								 $fund_rec .='Should you have any questions, please do not hesitate to contact me. %0D%0A';
								// $fund_rec .= '%0D%0A';
								// $fund_rec .= '%0D%0A';
								 $fund_rec .= '%0D%0A';
								 $fund_rec .= 'Regards – %0D%0A';

							//.............Closing Confrim start ..................//	
						
								$closing_conf = '';
								$closing_conf .= 'Hi XXXX- %0D%0A';
								//$closing_conf .= 'Hi '.$draws_contact_name.' - %0D%0A';
								//$closing_conf .= '%0D%0A';
								
							    $closing_conf .='Congratulations, we have received confirmation that the trust deed secured on '.$primary_loan_property->property_address.' was recorded on XXXXX. %0D%0A';
								 $closing_conf .= '%0D%0A';
								 $closing_conf .='Within the next 24 to 48 hours we will submit the loan to FCI Lender Services for boarding. Within the next 5 to 7 business days you will receive an e-mail from FCILender Services requesting that you sign their loan servicing agreement. Please note, they cannot complete processing your interest in the loan and release disbursements until you have signed the agreement. %0D%0A';
								 $closing_conf .= '%0D%0A';
								 $closing_conf .='Should you have any questions regarding the boarding process, please do not hesitate to contact us. %0D%0A';
						
								// $closing_conf .= '%0D%0A';
								 $closing_conf .= '%0D%0A';
								 $closing_conf .= 'Regards – %0D%0A';

							//.............loan Board start ..................//	
						
								$loan_board = '';
								//$loan_board .= 'Hi '.$draws_contact_name.' - %0D%0A';
								$loan_board .= 'Hi XXXX- %0D%0A';
								$loan_board .= '%0D%0A';
								$loan_board .='We have received confirmation that your interest in the trust deed secured on '.$primary_loan_property->property_address .' has been added to your account with FCI Lender Services.You may now log into your account and view your trust deed at http://www.trustfci.com/CustomerLogin.html. %0D%0A';
								 $loan_board .= '%0D%0A';
								 $loan_board .='If you have forgotten your password or have difficulty accessing your Lender account, you may reset your password by following the link to do so on the login page. For new Lender accounts, you may login using the following instructions: %0D%0A';
								 $loan_board .= '%0D%0A';
								 $loan_board .= 'Username: FCI Lender Account Number %0D%0A';
								
								 $loan_board .= 'Password: Last 4 digits of the SSN/Tax ID followed by the first six letters of the Lender name followed by an exclamation mark. %0D%0A';
								 $loan_board .='Should you have any questions, please do not hesitate to contact me. %0D%0A';
						
								 $loan_board .= '%0D%0A';									
								 $loan_board .= 'Regards – %0D%0A';
									
							//.............renovation start ..................//	
						
								$renovation = '';
								//$renovation .= 'Hi '.$draws_contact_name.' - %0D%0A';
								$renovation .= 'Hi XXXX- %0D%0A';
								$renovation .= '%0D%0A';
								$renovation .='The renovations on '.$primary_loan_property->property_address .' are complete and the property has been listed for sale. I have included a link to the listing below. %0D%0A';
								 $renovation .= '%0D%0A';
							     $renovation .='LINK %0D%0A';
								 $renovation .= '%0D%0A';
								 $renovation .='Once the Borrower has accepted an offer and escrow has been opened, the loan servicing company will submit a payoff demand. Proceeds from the payoff will be wired to the loan servicer, processed, and electronically deposited into your designated account. If you are not set up with electronic deposit or you used a self-directed IRA to fund the trust deed, a check will be mailed to the address on file within one week of the payoff. %0D%0A';
								 $renovation .= '%0D%0A';
								 $renovation .='Should you have any questions, please don’t hesitate to contact me directly. %0D%0A';
						
								 $renovation .= '%0D%0A';
								 $renovation .= '%0D%0A';
								 $renovation .= 'Regards – %0D%0A';
	
								
	                            //.............late payment start ..................//	
						
								$late_payment = '';
								//$late_payment .= 'Hi '.$draws_contact_name.' - %0D%0A';
								$late_payment .= 'Hi XXXX- %0D%0A';
								$late_payment .= '%0D%0A';
								$late_payment .='The trust deed secured on '.$primary_loan_property->property_address .' with '.$fetch_loan_borrower[0]->b_name.' is currently 15+ days late. I have reached out to the borrower to request immediate payment. Once FCI Lender Services receives the payment, they will process and disburse it per your % interest in the loan. %0D%0A';
								 $late_payment .= '%0D%0A';
							     $late_payment .='If the payment is not received within 30 days of the original payment date, the loan will be considered in default and TaliMar Financial will initiate a foreclosure action against the borrower. %0D%0A';
								 $late_payment .= '%0D%0A';
								 $late_payment .='You are not required to do anything at this time.  We will continue to update you on the status of this trust deed. %0D%0A';
								 $late_payment .= '%0D%0A';
								 $late_payment .='Should you have any question, please feel free to contact me directly. %0D%0A';
								 $late_payment .= '%0D%0A';
								 $late_payment .= '%0D%0A';
								 $late_payment .= 'Regards – %0D%0A';

									
								 //.............Payoff request start ..................//	
						
								if($loan_servicing_data[0]->servicing_agent = '14'){
									$outlook_servicerr = 'FCI Lender Services';
								}elseif($loan_servicing_data[0]->servicing_agent = '15'){
									$outlook_servicerr = 'TaliMar Financial Inc.';
								}elseif($loan_servicing_data[0]->servicing_agent = '16'){
									$outlook_servicerr = 'La Mesa Fund Control';
								}elseif($loan_servicing_data[0]->servicing_agent = '19'){
									$outlook_servicerr = 'Del Toro Loan Servicing';
								}else{
									$outlook_servicerr = '';
								}
							
								
                              //............Payoff Confirmation start ..................//	
						
								$payoff_confirmation = '';
								//$payoff_confirmation .= 'Hi '.$draws_contact_name.' - %0D%0A';
								$payoff_confirmation .= 'Hi XXXX- %0D%0A';
									$payoff_confirmation .= '%0D%0A';
					
								$payoff_confirmation .=''.$newsub_servicer_option[$loan_servicing_data[0]->sub_servicing_agent].' has received and processed the payoff for the trust deed secured on '.$primary_loan_property->property_address .' '.$outlook_property[0]->unit.' '.$outlook_property[0]->city.','.$outlook_property[0]->state.' '.$outlook_property[0]->zip.'. The proceeds from the payoff will be electronically deposited into your designated account within 24 to 48 hours. If you receive distributions by check, your final distribution will be mailed to you within 10 business days. %0D%0A';
								 $payoff_confirmation .= '%0D%0A';
							     $payoff_confirmation .='If you are interested in pursuing a new trust deed, you may view our available trust deeds by clicking on the link below. %0D%0A';
								 $payoff_confirmation .= '%0D%0A';
								 $payoff_confirmation .='http://talimarfinancial.com/investors/available-trust-deeds/. %0D%0A';
								 $payoff_confirmation .= '%0D%0A';
								 $payoff_confirmation .='We thank you for being a valued client of TaliMar Financial and look forward to being your source for trust deed investments in the future. %0D%0A';
								 //$payoff_confirmation .= '%0D%0A';
								 //$payoff_confirmation .= '%0D%0A';
								 $payoff_confirmation .= 'Regards – %0D%0A';

						
							//.............Payoff escow request start ..................//		
		
								$payoff_rq='';
								$payoff_rq .='TaliMar Financial has received your request for a payoff demand. %0D%0A';
								$payoff_rq .='%0D%0A';
								
								$payoff_rq .='To expedite your request, we ask that you submit your request directly to FCI Lender Services by e-mailing your request and a live signed Borrower Authorization to payoff@trustfci.com. Please be sure to include loan number #'.$outlook_property[0]->fci.' in your request. %0D%0A';
								
								 $payoff_rq .= '%0D%0A';
							     $payoff_rq .='FCI will typically respond with a payoff demand within 5 to 7 business days. %0D%0A';
								 $payoff_rq .= '%0D%0A';
								 $payoff_rq .= 'Regards – %0D%0A';
								
            
							//Loan Boarding Request...
								foreach($all_vendor_data as $vendor){
									$vendor_name[$vendor->vendor_id] = $vendor->vendor_name;
								}

								/*echo '<pre>';
								print_r($fetch_closing_statement_items_data);
								echo '</pre>';*/

								foreach($fetch_closing_statement_items_data as $row){

								 	if($row->hud == '814'){
								 		$FundControlFee814 = $row->paid_to_other;
								 	}

								 	if($row->hud == '1005'){
								 		$this_paid_to_other = $row->paid_to_other;
								 	}

								 	if($row->hud == '1006'){
								 		$InterestReserve1006 = $row->paid_to_other;
								 	}

								}

								
								$bording_request = '';
								
								$bording_request .= 'We are requesting that you board the attached loan. %0D%0A';
								$bording_request .= '%0D%0A';
								$bording_request .= 'Loan Position: '.$position_option[$outlook_property[0]->position].' %0D%0A';
								$bording_request .= 'Loan Amount: $'.number_format($outlook_property[0]->loan_amount).' %0D%0A';
								
								$bording_request .= 'Borrower Name: '.$fetch_loan_borrower[0]->b_name.' %0D%0A';
								$bording_request .= 'Closing Date: '.date('m-d-Y', strtotime($record_recorder_date)).' %0D%0A';
								$bording_request .= 'Property Address: '.$outlook_property_address.' %0D%0A';
								$bording_request .= '# of Lenders: '.$lender_co.' %0D%0A';
								
								$bording_request .= 'Interest Reserve: $'.number_format($InterestReserve1006,2).' %0D%0A';

								$bording_request .= 'Renovation Reserve: $'.number_format($this_paid_to_other,2).' %0D%0A';
								$bording_request .= 'Fund Control Fee: $'.number_format($FundControlFee814,2).' %0D%0A';
							
								$bording_request .= ' %0D%0A';
								
								$bording_request .='Please respond to this e-mail confirming receipt and please include the Loan Servicing Number. %0D%0A';
								$bording_request .= '%0D%0A';
								
								$bording_request .= 'Unless otherwise instructed, do not apply any payments to principal. Please contact me immediately should have any questions or concerns regarding this loan. %0D%0A';
								$bording_request .= '%0D%0A';
								
								$bording_request .= 'Regards –  %0D%0A';


								//escrow_post_close//
								
								$escrow_post_close .= 'Hi '.$fname.' - %0D%0A';
								$escrow_post_close .= '%0D%0A';
								$escrow_post_close .= 'We thank you for your assistance in completing this transaction. At your earliest convenience, would you please provide us a copy of the Final Settlement Statement and tracking information for the closing disbursement check to be expressed to TaliMar Financial. %0D%0A';
								$escrow_post_close .= '%0D%0A';
								$escrow_post_close .= 'As a reminder, all funds that are marked as payable to FCI Lender Services including Per Diem Interest are to be sent to FCI Lender Services per our Escrow Lender Instructions.%0D%0A';
								$escrow_post_close .= '%0D%0A';
								$escrow_post_close .= 'Should you have any questions, please do not hesitate to contact us. %0D%0A';
								$escrow_post_close .= '%0D%0A';
								$escrow_post_close .= 'Regards – %0D%0A';

							//incoming_wire_script...

								$paid_to_other = 0;
								foreach($fetch_closing_statement_items_data as $huds){

									if($huds->hud == '814' || $huds->hud == '1005' || $huds->hud == '1006'){

										$paid_to_other += $huds->paid_to_other;
									}
								}
								//echo $paid_to_other;
								$incoming_wire_script = '';
								$incoming_wire_script .= 'Hi Alex - %0D%0A';
								$incoming_wire_script .= '%0D%0A';
								$incoming_wire_script .= 'FCI Lender Services will be receiving a wire in the amount of $'.number_format($paid_to_other,2).' for a new loan secured on '.$outlook_property_address.' with '.$outlook_borrower[0]->b_name.'. %0D%0A';

								$incoming_wire_script .= 'The funds are to be allocated as follows: %0D%0A';

								foreach($fetch_closing_statement_items_data as $huds){

									if($huds->hud == '814' || $huds->hud == '1005' || $huds->hud == '1006'){

										if($huds->hud == '1006'){

											$incoming_wire_script .= '$'.number_format($huds->paid_to_other,2).' - '.$huds->items.' (apply as partial months payment on first payment) %0D%0A';

										}else{

											$incoming_wire_script .= '$'.number_format($huds->paid_to_other,2).' - '.$huds->items.' %0D%0A';
										}
										
									}
								}

								
								//$incoming_wire_script .= '$XXX,XXX - Total  %0D%0A';
								$incoming_wire_script .= '%0D%0A';
								$incoming_wire_script .= 'Once we receive confirmation that the loan has closed, we will submit the setup package along with the loan documents for boarding.   %0D%0A';
								//$incoming_wire_script .= '%0D%0A';
								$incoming_wire_script .= '%0D%0A';
								$incoming_wire_script .= 'Regards – %0D%0A';
								
							//release_draw_script...
								
								$release_draw_script = '';
								$release_draw_script .= 'FCI Representative -  %0D%0A';
								$release_draw_script .= '%0D%0A';
								$release_draw_script .= 'Please release $XXXXX from the restricted suspense for Account #'.$outlook_property[0]->fci.' to '.$outlook_borrower[0]->b_name.'. Attached to this e-mail is the account information for the wire. %0D%0A';
								
								$release_draw_script .= '%0D%0A';
								$outlook_property_addresss=$outlook_property[0]->property_address.' '.$outlook_property[0]->unit.' '.$outlook_property[0]->city.' '.$outlook_property[0]->state.' '.$outlook_property[0]->zip;
								

								$release_draw_script .= 'Please confirm receipt of this email and notify us once the wire has been released.  %0D%0A';
								$release_draw_script .= '%0D%0A';
								$release_draw_script .= 'Should you have any questions, please contact TaliMar Financial immediately. %0D%0A';
								$release_draw_script .= '%0D%0A';
								$release_draw_script .= '%0D%0A';
								$release_draw_script .= 'Regards – %0D%0A';
								


							//extension_app_script...

								$extension_app_script = '';
								$extension_app_script .= 'FCI Representative - %0D%0A';
								$extension_app_script .= '%0D%0A';
								//$extension_app_script .= 'TaliMar Financial has approved a # month extension for loan #['.$outlook_property[0]->fci.']. The signed extension approval has been signed.';
								$extension_app_script .= 'TaliMar Financial has approved a X month extension for loan #['.$outlook_property[0]->fci.']. The signed Extension Agreement has been attached to this email. The Borrower has been instructed to submit the Extension Fee payment in the amount of '.$extension_fee.' directly to FCI Lender Services. %0D%0A';
								
								$extension_app_script .= '%0D%0A';
								$extension_app_script .= 'Please note, the loan modification is not to be processed until FCI is in receipt of the Extension Fee payment. %0D%0A';
								$extension_app_script .= '%0D%0A';
								
								//$extension_app_script .= 'Once received, the Extension Fee is to be disbursed '.$extension_fee.' to TaliMar Financial and the remainder disbursed the Lender(s) based upon their interest in the loan.  %0D%0A';
								$extension_app_script .= 'Once received, the Extension Fee is to be disbursed 75% to TaliMar Financial and the remainder disbursed the Lender(s) based upon their interest in the loan.  %0D%0A';
								$extension_app_script .= '%0D%0A';
								$extension_app_script .= 'Please notify us once the payment has been received and do not disburse until given final approval.  %0D%0A';
								$extension_app_script .= '%0D%0A';
								$extension_app_script .= 'Should you have any questions, please do not hesitate to contact us.   %0D%0A';
								//$extension_app_script .= '%0D%0A';
								$extension_app_script .= '%0D%0A';
								$extension_app_script .= 'Regards – %0D%0A';


							//maturity_status_update...

								$maturity_date = $outlook_property[0]->maturity_date ? date('m-d-Y', strtotime($outlook_property[0]->maturity_date)) : '';


								$maturity_status_update = '';
								$maturity_status_update .= 'Hi XXXX- %0D%0A';
								$maturity_status_update .= '%0D%0A';
								$maturity_status_update .= 'The trust deed secured on '.$outlook_property_address.' with '.$outlook_borrower[0]->b_name.' is set to mature on '.$maturity_date.'. We have reached out to the Borrower to discuss the upcoming maturity. The terms of this Promissory Note allow for a [#] month extension. Should the Borrower choose the exercise the option to extends the loan, the Borrower will submit the Extension Fee in the amount of [$] to FCI Lender Services. Once received, FCI will process the loan modification and disburse the payment according to your interest in the loan. Should the Borrower choose to Pay Off the loan and not extend the loan, we have requested the Borrower submit a Payoff demand 10 days prior to the maturity date.  %0D%0A';
								$maturity_status_update .= '%0D%0A';
								$maturity_status_update .= 'We will continue to provide your status update. Should you have any questions and concerns, please do not hesitate to reach out. %0D%0A';
								$maturity_status_update .= '%0D%0A';
								$maturity_status_update .= 'Regards - %0D%0A';
								// $maturity_status_update .= 'Sarah - %0D%0A';
								// $maturity_status_update .= '%0D%0A';
								// $maturity_status_update .= 'Sarah Hosseini | Investor Relations %0D%0A';
								// $maturity_status_update .= '858.201.3429 Direct %0D%0A';
								// $maturity_status_update .= '858.201.3253 Office %0D%0A';
								 $maturity_status_update .= '%0D%0A';
								$maturity_status_update .= 'TaliMar Financial Inc. %0D%0A';
								$maturity_status_update .= '11440 West Bernardo Ct., Suite 210 | San Diego, CA 92127  %0D%0A';


								//google_review

								$google_review = '';
								$google_review .= 'I want to personally thank you for trusting TaliMar Financial with funding the loan on '.$outlook_property_address.'. %0D%0A';
								$google_review .= '%0D%0A';
								$google_review .= 'As a service provider, we rely heavily on positive reviews from our valued clients, vendors, and advisors. If you felt we met or exceeded your expectations, please consider giving TaliMar Financial a 5-star review on Google by using the link below. %0D%0A';
								$google_review .= '%0D%0A';
								$google_review .= 'We appreciate your consideration. %0D%0A';
								$google_review .= '%0D%0A';
								$google_review .= 'https://g.page/TaliMarFinancialHardMoney/review?gm %0D%0A';
								$google_review .= '%0D%0A';
								$google_review .= 'For our service providers, we would be more than happy to provide you the same review. %0D%0A';
								$google_review .= '%0D%0A';
								$google_review .= 'Regards – %0D%0A';





								$nsf_payment = '';
								$nsf_payment .= 'Hi '.$draws_contact_name.' - %0D%0A';
								$nsf_payment .= '%0D%0A';
								$outlook_property_addresss=$outlook_property[0]->property_address.' '.$outlook_property[0]->unit.'; '.$outlook_property[0]->city.', '.$outlook_property[0]->state.' '.$outlook_property[0]->zip;
								$nsf_payment .= 'Unfortunately, we received notice from '.$newsub_servicer_option[$loan_servicing_data[0]->sub_servicing_agent].' that your most recent payment for the loan secured on '.$outlook_property_addresss.' was returned for Not Sufficient Funds. Would you please contact '.$newsub_servicer_option[$loan_servicing_data[0]->sub_servicing_agent].' at (714) 282-2424 to make the payment. You may reference loan  #'.$outlook_property[0]->fci.'. %0D%0A';
						
								$nsf_payment .= '%0D%0A';
								$nsf_payment .= 'If you feel you received this message in error, please contact TaliMar Financial loan servicing at (858) 613-0111. %0D%0A';
								$nsf_payment .= '%0D%0A';
								$nsf_payment .= 'Please confirm receipt of this e-mail and once you have made the payment. %0D%0A';
								$nsf_payment .= '%0D%0A';
								
								$nsf_payment .= 'Regards – %0D%0A';
								$nsf_payment .= '%0D%0A';
								$nsf_payment .= 'TaliMar Financial Servicing';
								
								
								//partial_payment_due
								$partial_payment_due = '';
								$partial_payment_due .= 'Hi '.$draws_contact_name.' - %0D%0A';
								$partial_payment_due .= '%0D%0A';
								
								$partial_payment_due .= 'This e-mail is to notify you that the remaining funds in the payment reserve account are insufficient to make the next payment in full. You will be responsible for making a partial payment $##### by ##-##-#### to cover the difference. %0D%0A';
								
								$partial_payment_due .= '%0D%0A';
								
								$partial_payment_due .= 'Please submit payment directly to FCI Lender Services at 8180 East Kaiser Blvd; Anaheim Hills, CA 92808 and be sure to include the account #'.$outlook_property[0]->fci.'. You may also make a payment by phone by calling FCI Lender Services at (714) 282-2424. Please note, if the payment is not received within the 10-day grace period from the due date then a 10% late fee will be assessed on the full payment amount.  %0D%0A';
								
								$partial_payment_due .= '%0D%0A';
								$partial_payment_due .= 'Should you have any questions or concerns, please do not hesitate to contact us. %0D%0A';
								$partial_payment_due .= '%0D%0A';
								$partial_payment_due .= 'Regards – %0D%0A';
								
								
								//constructionUpdate_email
								$constructionUpdate_email = '';
								$constructionUpdate_email .= 'Hi '.$draws_contact_name.' - %0D%0A';
								$constructionUpdate_email .= '%0D%0A';
								
								$constructionUpdate_email .= 'Would you please respond with a construction status update for '.$outlook_property_addresss.'. Please include the % complete, remaining items to be completed, and estimated completion date. %0D%0A';
																	
								$constructionUpdate_email .= '%0D%0A';
								$constructionUpdate_email .= 'We appreciate your timely response. Should you have any questions regarding our request, please reach out to me. %0D%0A';
								$constructionUpdate_email .= '%0D%0A';
								$constructionUpdate_email .= 'Regards – %0D%0A';
								
								
								//PaymentReserveExhausted
								
								$fst_payment_date = $outlook_property[0]->first_payment_date ? date('m-d-Y', strtotime($outlook_property[0]->first_payment_date)) : '';
								
								$PaymentReserveExhausted = '';
								$PaymentReserveExhausted .= 'Hi '.$draws_contact_name.' - %0D%0A';
								$PaymentReserveExhausted .= '%0D%0A';
								
								$PaymentReserveExhausted .= 'This email is notification that the payment reserve account for the loan secured on '.$outlook_property_addresss.' has been exhausted and partial payment totaling $######## will be due on '.$fst_payment_date.'. %0D%0A';
								$PaymentReserveExhausted .= '%0D%0A';
								$PaymentReserveExhausted .= 'You may submit the payment directly to FCI Lender Services at 8180 East Kaiser Blvd; Anaheim Hills, CA 92808. Please include account #'.$outlook_property[0]->fci.' on the check. You may also make a payment by phone by calling FCI Lender Services at (714) 282-2424 or by logging into your account online at https://www.trustfci.com/CustomerLogin.html. . %0D%0A';
																	
								$PaymentReserveExhausted .= '%0D%0A';
								$PaymentReserveExhausted .= 'Should you have any questions or concerns, please do not hesitate to contact us. %0D%0A';
								$PaymentReserveExhausted .= '%0D%0A';
								$PaymentReserveExhausted .= 'Regards – %0D%0A';


								$extendsension_payment = '';
								$extendsension_payment .= 'Hi '.$draws_contact_first_name.' - %0D%0A';
								$extendsension_payment .= '%0D%0A';
								$extendsension_payment .= 'Attached to this email is a copy of the signed Extension Agreement. Please submit the Extension Fee totaling '.$extension_fee.' to '.$newsub_servicer_option[$loan_servicing_data[0]->sub_servicing_agent].' within 10 business days of the maturity date '.date('m-d-Y', strtotime($outlook_property[0]->maturity_date)).' to ensure the loan modification is processed prior to the maturity. Please make the check payable to FCI Lender Services and mail the check to 8180 E. Kaiser Blvd; Anaheim Hills, CA 92808. Please be sure to reference the FCI loan number #'.$outlook_property[0]->fci.' on the check.  %0D%0A';
								$extendsension_payment .= '%0D%0A';
								$extendsension_payment .= 'Please note, if the Extension Fee or the Balloon Payment are not received prior to '.date('m-d-Y', strtotime($outlook_property[0]->maturity_date)).', you will be subject to the 17.00% default rate. %0D%0A';
								$extendsension_payment .= '%0D%0A';
								$extendsension_payment .= 'Should you have any questions, please do not hesitate to contact us.  %0D%0A';
								$extendsension_payment .= '%0D%0A';
								$extendsension_payment .= 'Regards - %0D%0A';

								//fundrequestTalimar

								$fundrequestTalimar = '';
								//$fundrequestTalimar .= 'Hello - %0D%0A';
								//$fundrequestTalimar .= '%0D%0A';
								$fundrequestTalimar .= 'Attached to this email are the funding instructions for the trust deed secured on '.$outlook_property[0]->property_address.'.  %0D%0A';
								$fundrequestTalimar .= '%0D%0A';
								$fundrequestTalimar .= 'You have the option to wire your funds or visit your local Wells Fargo bank and deposit your funds into our account using the attached banking information. %0D%0A';
								$fundrequestTalimar .= '%0D%0A';
								$fundrequestTalimar .= 'Please notify us once you have funded so that we may confirm receipt of funds. If you are unable to fund within 24 hours of this request, please contact us immediately. Interest on your trust deed will begin to accrue once your trust deed has recorded or if you are funding subsequent to close, once TaliMar has received your funds and signed disclosures. %0D%0A';
								$fundrequestTalimar .= '%0D%0A';
								$fundrequestTalimar .= 'Should you have any questions, please do not hesitate to contact us.  %0D%0A';
								$fundrequestTalimar .= '%0D%0A';
								$fundrequestTalimar .= 'Regards - %0D%0A';

								if($title_contact['full_name'] !='0'){

									$Tfull_name_value = $title_contact['full_name'];
								}else{
									$Tfull_name_value = $title_contact['first_name'].' '.$title_contact['last_name'];
								}


								//fundrequestTitle
								$fundrequestTitle = '';
								//$fundrequestTitle .= 'Hello - %0D%0A';
								//$fundrequestTitle .= '%0D%0A';
								$fundrequestTitle .= 'Attached to this email are the wiring instructions for the trust deed secured on '.$outlook_property[0]->property_address.'. We ask that you wire your funds to '.$fetch_title_data[0]->company.' on or before '.date('m-d-Y', strtotime($outlook_property[0]->loan_funding_date)).'.  %0D%0A';
								$fundrequestTitle .= '%0D%0A';
								$fundrequestTitle .= 'To reduce the risk of wire fraud, we highly recommend contacting the Title Officer, '.$Tfull_name_value.' at '.$fetch_title_data[0]->phone.' to verbally verify the attached wiring instructions before sending your wire.  %0D%0A';
								$fundrequestTitle .= '%0D%0A';
								$fundrequestTitle .= 'When sending your wire, please have your bank reference the Title Order #'.$fetch_title_data[0]->title_order_no.' / '.$outlook_property[0]->property_address.'.%0D%0A';
								$fundrequestTitle .= '%0D%0A';
								$fundrequestTitle .= 'Please notify us once you have sent your wire so that we may confirm receipt of funds. If you are unable to wire your funds by this time, please contact us immediately. Interest on your trust deed will begin to accrue once your trust deed has recorded.  %0D%0A';
								$fundrequestTitle .= '%0D%0A';
								$fundrequestTitle .= 'Should you have any questions, please do not hesitate to contact us.%0D%0A';
								$fundrequestTitle .= '%0D%0A';
								$fundrequestTitle .= 'Regards - %0D%0A';


								//AssignmentTransfer
								$AssignmentTransfer = '';
								$AssignmentTransfer .= 'FCI Representative - %0D%0A';
								$AssignmentTransfer .= '%0D%0A';
								$AssignmentTransfer .= 'We have assigned an interest in loan #'.$outlook_property[0]->fci.'. I have attached the Assignment of Deed of Trust, FCI Setup Form and Servicing Agreement. %0D%0A';
								$AssignmentTransfer .= '%0D%0A';
								$AssignmentTransfer .= 'FCI Loan #: '.$outlook_property[0]->fci.' %0D%0A';
								$AssignmentTransfer .= 'Property Address: '.$outlook_property_addresss.' %0D%0A';
								$AssignmentTransfer .= 'Borrower Name: '.$outlook_borrower[0]->b_name.' %0D%0A';
								$AssignmentTransfer .= '# of Assignments: '.count($loan_assigment_data).' %0D%0A';
								$AssignmentTransfer .= 'Active Date of Assignments: '.date('m-d-Y').' %0D%0A';
								$AssignmentTransfer .= '%0D%0A';
								$AssignmentTransfer .= 'Please let me know if anything further is required for immediate boarding. Please confirm receipt of this request.  %0D%0A';
								$AssignmentTransfer .= '%0D%0A';
								$AssignmentTransfer .= 'Regards - %0D%0A';
								$AssignmentTransfer .= '%0D%0A';



								/*30 Yr Rental Loan E-Mail*/
								$outlook_property_addressA = $outlook_property[0]->property_address;
								if($outlook_property[0]->unit){
									$outlook_property_addressA .= ' '.$outlook_property[0]->unit;
								}
								$outlook_property_addressA .= '; ';
								$outlook_property_addressA .= $outlook_property[0]->city.', '; 
								$outlook_property_addressA .= $outlook_property[0]->state.' '.$outlook_property[0]->zip;

								$rental_loan_request  = '';
								// $rental_loan_request .= 'Hi '.$draws_contact_first_name.', %0D%0A';
								$rental_loan_request .= 'Hi {borrower_first_name}, %0D%0A';
								
								$rental_loan_request .= '%0D%0A';
								$rental_loan_request .= 'I will be assisting in the underwriting of your loan request on ';
								$rental_loan_request .= $outlook_property_addressA.'. Please see the attached list of required diligence items for conditional loan approval. We ask that you return all items as soon as possible to prevent any delays in closing. %0D%0A';
								$rental_loan_request .= '%0D%0A';
								$rental_loan_request .= 'Once these items have been received and reviewed, your loan request will be submitted for conditional loan approval. %0D%0A';
								$rental_loan_request .= '%0D%0A';
								$rental_loan_request .= 'Should you have any questions regarding the requested items or the process, please feel free to contact me. %0D%0A';
								$rental_loan_request .= '%0D%0A';
								$rental_loan_request .= '%0D%0A';
								$rental_loan_request .= 'Regards, %0D%0ATaliMar Financial';
								/*30 Yr Rental Loan E-Mail*/

							?>
							
						<div class="row"> 
								<div class="col-md-3">
									<!--<label>Borrower</label><br>-->
									<label>Underwriting</label><br>
									
									<a class="btn btn-block blue email_script_padding" href="mailto:
		
									<?php	foreach($fetch_draws_contact_email as $assig)
										
											
											{ 
													echo $assig->contact_email. ' ;';
											}
										
									?>?subject=Term Sheet – <?php echo $outlook_property[0]->property_address;?>&cc=underwriting@talimarfinancial.com&body=<?php echo $lender_proposal;?>">Lender Proposal </a>

									<a class="btn btn-block blue email_script_padding" href="mailto:
		
									<?php	
									$borrowe_app_name = '';	

									foreach($fetch_draws_contact_email as $keyi=>$assig)
										
											
											{ 
												if($keyi == 0){
													$borrowe_app_name = $assig->contact_firstname;
												}
													echo $assig->contact_email. ' ;';
											}
										
									?>?subject=Loan Request – <?php echo $outlook_property[0]->property_address." ".$outlook_property[0]->unit;?>&cc=underwriting@talimarfinancial.com&body=<?php echo str_replace('{borrower_first_name}', $borrowe_app_name, $rental_loan_request);?>">30 Yr Rental Loan Request </a>
									<?php
									
									//echo '<pre>??'; print_r($b_contact); echo '</pre>';
									// echo '<pre>'; print_r($fetch_draws_contact_email); echo '</pre>';?>
									<a class="btn blue btn-block email_script_padding" href="mailto:
									<?php
									$borrowe_app_name = '';	
									foreach($fetch_draws_contact_email as $keyi=>$assig)
										{ 
											if($keyi == 0){
												$borrowe_app_name = $assig->contact_firstname;
											}
												echo $assig->contact_email. ' ;';
										}
									?>
									?subject=Loan Application – <?php echo $outlook_property[0]->property_address.' '.$outlook_property[0]->unit;?>&cc=underwriting@talimarfinancial.com&body=<?php echo str_replace('{borrower_first_name}', $borrowe_app_name, $application_script);?>">Loan Application</a>


									<a class="btn blue btn-block email_script_padding" href="mailto:
									<?php	foreach($fetch_draws_contact_email as $assig)
										{ 
												echo $assig->contact_email. ' ;';
										}
									?>
									?subject=Status Update – <?php echo $outlook_property[0]->property_address;?>&cc=.&bcc=.&body=<?php echo $EscrowOpened;?>">Escrow Opened</a>

									<a class="btn blue btn-block email_script_padding" href="mailto:
									<?php foreach($fetch_draws_contact_email as $keyi => $assig)
										{ 
											echo $assig->contact_email. ' ;';
											if($keyi == 0){
												$nameb_name = $assig->contact_firstname;
											}
										}
										$appraisal_order = str_replace('{borrower_contact_name}', $nameb_name, $appraisal_order);
									?>
									?cc=underwriting@talimarfinancial.com&subject=Final Closing Disclosures – <?php echo $outlook_property[0]->property_address;?>&body=<?php echo $appraisal_order;?>">Appraisal Ordered</a>

									<a class="btn blue btn-block email_script_padding" href="mailto:
									<?php foreach($fetch_draws_contact_email as $keyi => $assig)
										{ 
											echo $assig->contact_email. ' ;';
											if($keyi == 0){
												$nameb_name = $assig->contact_firstname;
											}
										}
										$FinalLoanDisclosures = str_replace('{borrower_contact_name}', $nameb_name, $FinalLoanDisclosures);
									?>
									?cc=underwriting@talimarfinancial.com&subject=Final Closing Disclosures – <?php echo $outlook_property[0]->property_address;?>&body=<?php echo $FinalLoanDisclosures;?>">Final Loan Disclosures</a>
									

									 <a class="btn blue btn-block email_script_padding" href="mailto:	
									<?php foreach($fetch_draws_contact_email as $assig){ echo $assig->contact_email. ' ;';}?>
									?subject=Loan Documents – <?php echo $outlook_property[0]->property_address;?>&cc=.&bcc=.&body=<?php echo $doc_submit_script;?>">Loan Documents Submitted</a> 


									<br><label>Closing</label><br>	


									<a class="btn blue btn-block email_script_padding" href="mailto:<?php echo $escrow_contact_name['email'];?>?subject=Lender – <?php echo $outlook_property[0]->property_address.' '.$outlook_property[0]->unit; ?>&cc=.&bcc=.&body=<?php echo $escrow_intro;?>">Escrow Introduction</a>
									
									<a class="btn blue btn-block email_script_padding" href="mailto:<?php echo $escrow_contact_name['email'];?>?subject=Loan Documents – <?php echo $outlook_property[0]->property_address; ?>&body=<?php echo $escrow_doc;?>">Escrow Loan Documents</a>
									
									<a class="btn blue btn-block email_script_padding" href="mailto:<?php echo $escrow_contact_name['email'];?>?subject=<?php echo $outlook_property[0]->property_address; ?> – Closing Disbursement Instructions &cc=servicing@talimarfinancial.com&body=<?php echo $escrow_post_close;?>">Escrow Post Close Request</a>			
							
								</div>

								
								<div class="col-md-3">
									<label>Lender</label><br>
									
									<a class="btn blue btn-block email_script_padding" href="mailto:?subject=Executive Summary – <?php echo $outlook_property[0]->property_address; ?>&body=<?php echo $executive_summary;?>&bcc=.">Executive Summary</a> 
									
									
									<a class="btn blue btn-block email_script_padding" href="mailto:?bcc=.&subject=Status Update – <?php echo $outlook_property[0]->property_address; ?>&body=<?php echo $confirm;?>">Confirmation of Interest
									</a>

									
									
									<a class="btn blue btn-block email_script_padding" href="mailto:invest@talimarfinancial.com?subject=Lender Disclosures – <?php echo $outlook_property[0]->property_address; ?>&body=<?php echo $disclose;?>">Disclosures Available</a> 
									
									<a class="btn blue btn-block email_script_padding" href="mailto:?subject=Funds Received – <?php echo $outlook_property[0]->property_address; ?>&body=<?php echo $fund_rec;?>&bcc=
																							 
									<?php	foreach($loan_assigment_data as $assig)
									{
										foreach($email_contact[$assig->lender_name] as $key => $contact)
										{ 
												echo $contact['email']. ' ;';
										}
									}
									
								?>">Funds Received</a> 
									
									<a class="btn blue btn-block email_script_padding" href="mailto:
									
								<?php	foreach($loan_assigment_data as $assig)
									{
										foreach($email_contact[$assig->lender_name] as $key => $contact)
										{ 
												echo $contact['email'].' ,';
										}
									}
									
								?>?subject=Status Update – <?php echo $outlook_property[0]->property_address;?>&body=<?php echo $closing_conf;?>&bcc=<?php	foreach($loan_assigment_data as $assig)
									{
										foreach($email_contact[$assig->lender_name] as $key => $contact)
										{ 
												echo $contact['email']. ' ;';
										}
									}
									
								?> ">Closing Confirmation</a> 
								
									
								<a class="btn blue btn-block email_script_padding" href="mailto: ?>?subject=FCI – Loan Boarded&body=<?php echo $loan_board;?>&bcc=
																							 
									<?php	foreach($loan_assigment_data as $assig)
									{
										foreach($email_contact[$assig->lender_name] as $key => $contact)
										{ 
												echo $contact['email']. ' ;';
										}
									}
									
								?>">Loan Boarded</a> 	
								
								
								<a class="btn blue btn-block email_script_padding" href="mailto:
									
									<?php	foreach($loan_assigment_data as $assig)
									{
										foreach($email_contact[$assig->lender_name] as $key => $contact)
										{ 
												echo $contact['email'].' ,';
										}
									}?>?subject=Status Update – <?php echo $outlook_property[0]->property_address; ?>&body=<?php echo $renovation;?>&bcc=
																							 
									<?php	foreach($loan_assigment_data as $assig)
									{
										foreach($email_contact[$assig->lender_name] as $key => $contact)
										{ 
												echo $contact['email']. ' ;';
										}
									}
									
								?>">Renovations Complete</a> 
									
									<a class="btn blue btn-block email_script_padding" href="mailto:
									
									<?php	foreach($loan_assigment_data as $assig)
									{
										foreach($email_contact[$assig->lender_name] as $key => $contact)
										{ 
												echo $contact['email'].' ,';
										}
									}
									
									?>?subject=Status Update – <?php echo $outlook_property[0]->property_address; ?>&body=<?php echo $late_payment;?>&bcc=
																							 
									<?php	foreach($loan_assigment_data as $assig)
									{
										foreach($email_contact[$assig->lender_name] as $key => $contact)
										{ 
												echo $contact['email']. ' ;';
										}
									}
									
								?>">Late Payment</a> 
									
									
									
									<a class="btn blue btn-block email_script_padding" href="mailto:
									
									 <?php	//foreach($loan_assigment_data as $assig)
									// {
										// foreach($email_contact[$assig->lender_name] as $key => $contact)
										// { 
												// echo $contact['email'].' ,';
										// }
									// }
									
									?>?subject=Status Update – <?php echo $outlook_property[0]->property_address; ?>&body=<?php echo $payoff_confirmation;?>&bcc=
																							 
											<?php	foreach($loan_assigment_data as $assig)
											{
												foreach($email_contact[$assig->lender_name] as $key => $contact)
												{ 
														echo $contact['email']. ' ;';
												}
											}
											
										?>">Payoff Confirmation</a> 

										<a class="btn blue btn-block email_script_padding" href="mailto:?subject=Status Update – <?php echo $outlook_property[0]->property_address; ?>&body=<?php echo $maturity_status_update;?>">Maturity Status Update</a>

										<a class="btn blue btn-block email_script_padding" href="mailto:invest@talimarfinancial.com ?subject=Funding Instructions – <?php echo $outlook_property[0]->property_address; ?>&body=<?php echo $fundrequestTalimar;?>">Funds Request – TaliMar</a>

										<a class="btn blue btn-block email_script_padding" href="mailto:invest@talimarfinancial.com ?subject=Funding Instructions – <?php echo $outlook_property[0]->property_address; ?>&body=<?php echo $fundrequestTitle;?>">Funds Request – Title</a>
									
								</div>

								<div class="col-md-3">
									<label>Borrower Servicing</label><br>

									<a class="btn blue btn-block email_script_padding" href="mailto:<?php	foreach($fetch_draws_contact_email as $assig){ echo $assig->contact_email. ' ;';}?>?subject=Status Update -<?php echo $outlook_property[0]->property_address;?>&body=<?php echo $welcome;?>">Borrower Welcome E-mail </a>

									<a class="btn blue btn-block email_script_padding" href="mailto:<?php	foreach($fetch_draws_contact_email as $assig){ echo $assig->contact_email. ' ;';}?>?subject=Payment Due – <?php echo $outlook_property[0]->property_address;?>&body=<?php echo $message;?>&cc=.">Late Payment Notice</a>

									<a class="btn blue btn-block email_script_padding" href="mailto:<?php echo $fetch_draws_contact_email[0]->contact_email; ?>?subject=Important Notice – <?php echo $outlook_property[0]->property_address;?>&cc=.&bcc=.&body=<?php echo $nsf_payment;?>">NSF Payment</a>

									<a class="btn blue btn-block email_script_padding" href="mailto:
									<?php foreach($fetch_draws_contact_email as $assig){ echo $assig->contact_email. ' ;';}?>?subject=Status Update –<?php echo $outlook_property[0]->property_address; ?>&body=<?php echo $partial_payment_due;?>&cc=.">Partial Payment Due</a>
								
									<a class="btn blue btn-block email_script_padding" href="mailto:
										<?php foreach($fetch_draws_contact_email as $assig){ echo $assig->contact_email. ' ;';}?>?subject=Status Update –<?php echo $outlook_property[0]->property_address; ?>&body=<?php echo $constructionUpdate_email;?>&cc=.&bcc=.">Construction Update
									</a>

									<a class="btn blue btn-block email_script_padding" href="mailto:<?php foreach($fetch_draws_contact_email as $assig){ echo $assig->contact_email. ' ;';}?>?subject=Maturity Notification – <?php echo $outlook_property[0]->property_address;?>&body=<?php echo $maturity_script;?>">Maturity Notification</a>

									<a class="btn blue btn-block email_script_padding" href="mailto:<?php foreach($fetch_draws_contact_email as $assig){ echo $assig->contact_email. ' ;';}?>?subject=Maturity Notification – <?php echo $outlook_property[0]->property_address;?>&body=<?php echo $maturity_script_ext;?>">Maturity – Extension</a>
								
									<a class="btn blue btn-block email_script_padding" href="mailto:<?php	foreach($fetch_draws_contact_email as $assig){ echo $assig->contact_email. ' ;';}?>?subject=Extension Payment –<?php echo $outlook_property[0]->property_address; ?>&body=<?php echo $extendsension_payment;?>&cc=.">Ext Payment Instructions </a>

									<a class="btn blue btn-block email_script_padding" href="mailto:<?php	foreach($fetch_draws_contact_email as $assig){ echo $assig->contact_email. ' ;';}?>?subject=Important Notice – <?php echo $outlook_property[0]->property_address;?>&body=<?php echo $property_insurance_script;?>">Property Insurance Request</a>

									<a class="btn blue btn-block email_script_padding" href="mailto:?subject=Demand Request Instructions –<?php echo $outlook_property[0]->property_address;?>&body=<?php echo $payoff_rq;?>&cc=servicing@talimarfinancial.com&bcc=.">Demand Request (FCI)</a>

									<a class="btn blue btn-block email_script_padding" href="mailto:<?php	foreach($fetch_draws_contact_email as $assig){ echo $assig->contact_email. ' ;';}?>?subject=Important Notice – <?php echo $outlook_property[0]->property_address;?>&body=<?php echo $payoff_script;?>">Payoff Received </a>

								</div>


								<div class="col-md-3">
									<label>FCI Servicing </label><br>
									
									<a class="btn blue btn-block email_script_padding" href="mailto:Loanboarding-ls@trustfci.com?subject=Loan Boarding Request - <?php echo $outlook_property[0]->property_address; ?>&body=<?php echo $bording_request;?>&cc=servicing@talimarfinancial.com;&bcc=.">Loan Boarding Request</a> 
									
									<a class="btn blue btn-block email_script_padding" href="mailto:asullivan@trustfci.com?subject=Notification: Incoming Wire&body=<?php echo $incoming_wire_script;?>">Incoming Wire Notification</a>
									
									<a class="btn blue btn-block email_script_padding" href="mailto:asullivan@trustfci.com?subject=Construction Draw Request – <?php echo $outlook_property[0]->property_address;?>&body=<?php echo $release_draw_script;?>&cc=loanservicing@trustfci.com; servicing@talimarfinancial.com;<?php	foreach($fetch_draws_contact_email as $assig){ echo $assig->contact_email. ' ;';}?>&bcc=.">Release Draw Notification</a> 
									
									

									<a class="btn blue btn-block email_script_padding" href="mailto:loanservicing@trustfci.com?subject=Loan Extension – <?php echo $outlook_property[0]->fci .' / '. $outlook_property[0]->property_address ;?>&cc=servicing@talimarfinancial.com&bcc=.&body=<?php echo $extension_app_script;?>">Loan Extension Approval</a>

									<a class="btn blue btn-block email_script_padding" href="mailto:OwnershipTransfers@trustfci.com?subject=Ownership Transfer – <?php echo $outlook_property[0]->fci;?>&cc=servicing@talimarfinancial.com &bcc=.&body=<?php echo $AssignmentTransfer;?>">Assignment Transfer</a>


									
									<br><label>Marketing</label><br>									
									
									<a class="btn blue btn-block email_script_padding" href="<?php echo base_url();?>Load_data/message/<?php echo $talimar_loan;?>">Website / Bigger Pockets</a>

									<?php
									
									foreach($fetch_title_data as $row)
									{
										$title_email = $row->email;
									}

									foreach ($affiliated_parties_data as $key => $value) {
										$other_email[] = $value->email;
									}

									$array_other_email = $other_email ? array_unique($other_email) : array();

									$all_array_unique_email = array($select_borrower_contact1_email,$escrow_contact_name['email'],$title_email);
									$other_array_merge = array_merge($all_array_unique_email,$array_other_email);

									$send_email_to_unique = array_unique($other_array_merge);

									$array_other_email_string = implode(',',$send_email_to_unique);

									//echo $array_other_email_string;

									?>

									<a class="btn blue btn-block email_script_padding" href="mailto:?subject=Thank You – <?php echo $outlook_property[0]->property_address;?>&body=<?php echo $google_review;?>&cc=.&bcc=<?php echo $array_other_email_string;?>">Google Review Request</a>

								</div>
						</div>
					</div>
					
					<div class="modal-footer">
						<!--<button type="submit" name="submit" value="save" class="btn blue" >Save</button>-->
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
	</div>
</div>
<?php /* E-mail Scripts */ ?>