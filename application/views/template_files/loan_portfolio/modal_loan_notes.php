<div class="modal fade bs-modal-lg" id="loan_notes" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-full">					
			<div class="modal-content">
										
			<div class="modal-header">
				<!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>-->

				<button type="button" class="btn default pull-right" data-dismiss="modal">Close</button>
				<a class="btn btn-primary pull-right" onclick="contact_add_n(this);" data-toggle="modal" href="#add_contact_note" style="margin-right: 5px;">Add Note</a>
				
				<!--<h4 class="modal-title">Loan Notes: #<?php//echo $talimar_loan;?></h4>-->
				<h4 class="modal-title">Loan Notes123: <?= $property_addresss;?></h4>
			</div>
			<div class="modal-body">
				 <!-- Textarea -->
				
						<!-----------Hidden fields----------->
		
						<!-----------End of hidden Fields---->
				<div class="portlet-body rc_class">
					<table id="table_notes" class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
					<thead>	
						<tr>
							<th>Action</th>
							<th>Entry By</th>
							<th>Date</th>
							<th>Note Type</th>
							<th>Com Type</th>
							<th>Contact Name</th>
							<th>Notes</th>
							
						</tr>
					</thead>
						<tbody>	
						<?php 
						
						if($contact_notes_users)
						{
							foreach($contact_notes_users as $key => $row)
							{
								// echo'<pre>';
								// print_r($row);
								// echo'</pre>';
								
								?>
								
								
								<tr id="<?php echo $row['id'];?>">
									
									
									<?php if($this->session->userdata('user_role') == 2){ ?>
										<td style="width:5%;text-align:center;">
										<a title="Edit" id="<?php echo $row['id'];?>" onclick="feath_loan_notes(this);" class="fa fa-edit"></a>&nbsp;
										<a id="<?php echo $row['id'];?>" onclick="delete_this_note(this);" title="Delete" class="fa fa-trash"/>
										
								
									</td>	
									
							<?php }else{ ?>
										<td style="width:5%;text-align:center;">
										<a title="Edit" id="<?php echo $row['id'];?>" onclick="feath_loan_notes(this);" class="fa fa-edit"></a>
										</td>
									<?php } ?>
										
										<input type="hidden" id="ncontact_id" value="<?php echo $row['contact_id']; ?>">
										
									<td style="width:15%;"><?php echo $row['entry_by']; ?></td>
									
									<td style="width:10%;"><?php echo date('m-d-Y', strtotime($row['notes_date'])); ?></td>
									
									<td style="width:10%;"><?php echo $loan_note_option[$row['loan_note_type']]; ?></td>
									<td style="width:10%;"><?php echo $com_type[$row['com_type']]; ?></td>
									
									
									
									<td style="width:20%;"><?php echo $row['c_name'];?></td>
									
									<td style="width:30%;"><?php echo $row['notes_text']; ?></td>
																			
									
								</tr>
			
								<?php
								
							}
						}
						
						?>				
						</tbody>								
					</table>
				</div>
				
			</div>
			<div class="modal-footer">
			
				
			</div>
		
</div>
	<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>











<div class="modal fade bs-modal-lg" id="add_contact_note" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST" action = "<?php echo base_url();?>add_contact_notes">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<!--<h4 class="modal-title">Loan Closing Checklist: #<?php //echo ''.$talimar_loan; ?> </h4>-->
					<h4 class="modal-title">Loan Notes: <?php echo $property_addresss; ?> </h4>
				</div>
				<div class="modal-body">
					
					<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
					<input type="hidden" name="loan_id" value="<?php echo $loan_id;?>">
					
					<div class="row">
						<div class="col-md-6">
						<label>Date:</label>
							<input type="text" name="today_date" class="form-control datepicker" placeholder="MM/DD/YYYY" required value="<?php echo date('m-d-Y');?>">
						
						</div>
						<div class="col-md-6">
						<label>Note Type:</label>
							<select name="note_type" class="form-control" required>
							<?php foreach($loan_note_option as $key => $row){ ?>
								<option value="<?php echo $key; ?>"><?php echo $row; ?></option>
							<?php } ?>
							</select>
						
						</div>
					</div>
					
					<div class="row">&nbsp;</div>
					
					<div class="row">
						<div class="col-md-6">
						<label>Contact Name:</label>
								<select name="contact_id" id="contact_not_name" class="chosen43" required>
				<!-- 			<option>Select One</option>
							<?php foreach($fetch_all_contact as $row) { ?>
								<option value="<?php echo $row->contact_id;?>"><?php echo $row->contact_firstname .' '. $row->contact_middlename .' '. $row->contact_lastname ;?></option>
							<?php } ?> -->
							</select>
						
						</div>
						<div class="col-md-6">
						<label>Entry By:</label>
							<input type="text" name="entry_by" value="<?php echo $this->session->userdata('user_name');?>" class="form-control" readonly />
						</div>
					</div>
					
					<div class="row">&nbsp;</div>
					<div class="row">
						
						<div class="col-md-6">
						<label>Communication Type:</label>
							<select name="com_type" class="form-control" required>
							<?php //foreach($com_type as $key => $row){ ?>
								<!-- <option value="<?php //echo $key; ?>"><?php //echo $row; ?></option> -->
							<?php //} ?>
							<!-- Changed in config/myconfig.php line:444 -->
							<option value="0">Select One</option>
							<option value="1">Email</option>
							<option value="2">Phone</option>
							<option value="3">Letter</option>
							<option value="4">Meeting</option>
							<option value="5">Other</option>
							</select>
						
						</div>
						<div class="col-md-6">
						
						
						</div>
					</div>
					<div class="row">&nbsp;</div>
					
					<div class="row">
						<div class="col-md-12">
						<label>Contact Notes:</label>
							<textarea name="contact_note" type="text" rows="6" class="form-control" placeholder="Add Contact Note Here..." required></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
					<input type="submit" name="submit" class="btn blue" value="Save">
					<input type="hidden" name="action"  value="add">
				</div>
			</form>
		</div>
	</div>
</div>