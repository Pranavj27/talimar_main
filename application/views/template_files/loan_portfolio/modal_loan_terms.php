<div class="modal fade bs-modal-lg" id="loan_terms" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<!--<h4 class="modal-title">Loan Term :#<?php //echo $talimar_loan;?></h4>-->
				<h4 class="modal-title">Loan Term: <?php echo $property_addresss;?></h4>
			</div>

			 <form class="form-horizontal" method="POST" action= "<?php echo base_url();?>add_load_data_form">
				<div class="modal-body">
					 <!-----Form Start -------->
					
				<div class="row margin-top-10">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form_loan_data">
						<div class="dashboard-stat2">
							<!------------form 1---------------->
								

								<!-- Form Name -->
								<input type="hidden" name = "loan_id" value="<?php echo $loan_id;?>">

								<!-- Text input-->
								
								<?php 
								if($loan_id == '' || $loan_id == 'new')
								{
									
								$rand = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTVWXYZ0123456789"), 1, 6);

									?>
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">TaliMar Loan #:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input required id="textinput" name="talimar_loan" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $rand;?>"  readonly />
								  
								  </div>
								</div>
								<?php }else{
                              
									?>
								
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">TaliMar Loan #:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="textinput" name="talimar_loan" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $talimar_loan; ?>" <?php if($loan_id != '' && $loan_id != 'new'){ echo 'readonly'; } ?> required />
								  
								  </div>
								</div>
								<?php } ?>


								<?php
								$Nloan_originator = '';
								
								if($loan_id == '' || $loan_id == 'new'){
									$Nloan_originator = $this->session->userdata('t_user_id');
								}else{
									$Nloan_originator = $servicing_contact_details->loan_originator;
								}
								?>
								<div class="form-group">
									<label class="loan_data_lable" for="loan_originator">Account Representative: </label>  
									<div class="col-md-12">
										<select name="loan_originator" id="loan_originator" class="form-control">
										<option value="">Select One</option>
										<?php 
										if($all_user)
										{
											foreach($all_user as $key => $row)
											{
												$selected = '';
												if($Nloan_originator){
													if($row->id == $Nloan_originator){
														$selected = 'selected';
													}
												}
											?>
												<option value="<?php echo $row->id;?>" <?php echo $selected; ?>><?php echo $row->fname.' '.$row->middle_name.' '.$row->lname; ?></option>
											<?php 
											} 
										}
										?>
										
										</select>  
									</div>
								</div>

								
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Servicer Loan #:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="textinput" name="fci_loan_no" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $fci; ?>"   />
								 
								  </div>
								</div>							
								 
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Borrower Name<!-- <a data-toggle="modal" href="#Borrower_modal" onclick="load_borower_content_data();"><i class="fa fa-eye" aria-hidden="true"></i></a> -->:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="loan-borrower-id" name="borrower" class="chosen" data-live-search="true" >
									<option value="">Select Borrower</option>
									<?php 
									if(isset($fetch_borrower_data) && is_array($fetch_borrower_data)){
									foreach($fetch_borrower_data as  $row) { ?>
									  <option value="<?php echo $row->id;?>" <?php if($borrower == $row->id) { echo 'selected'; } ?> ><?php echo $row->b_name;?></option>
									<?php } } ?>
									</select>
								  </div>
								</div>

								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Cross Collateralized:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="selectbasic" name="cross_collateralized" class="form-control load_data_inputbox" onchange="hide_cross_provision(this.value);">
									  <?php foreach($loan_cross_collateralized as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key == $cross_collateralized) { echo 'selected';} ?> ><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>

								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Priority:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="selectbasic" name="position" class="form-control load_data_inputbox">
									<option value="" ></option>
									  <?php foreach($position_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key == $position) { echo 'selected';} ?> ><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>
								
								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Loan Amount:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="loan_amount" name="loan_amount" type="text" class="form-control input-md load_data_inputbox number_only amount_format"  value="<?php echo '&#36;'.number_format($loan_amount); ?>"  >
								  
								  </div>
								</div>  

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Current Balance:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="current_balance" name="current_balance" type="text" class="form-control input-md load_data_inputbox number_only amount_format"  value="<?php echo '$'.number_format($current_balance); ?>" >
							
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Lender Fee:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="lender_fee" name="lender_fee" type="text" class="form-control input-md load_data_inputbox number_only" value="<?php echo $lender_fee; ?>">
								
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Interest Rate (%):</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="intrest_rate" name="intrest_rate" type="text" class="form-control input-md load_data_inputbox number_only" value="<?php echo number_format($intrest_rate,3); ?>">
								 
								  </div>
								</div>

								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Default Rate:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="default_rate" name="default_rate" type="text" class="form-control input-md load_data_inputbox number_only" value="<?php echo number_format($default_rate,3).'%'; ?>">
							 
								  </div>
								</div>

								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">After how many days?:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="howmayday" name="how_many_days" type="text" class="form-control input-md load_data_inputbox number_only" value="<?php echo $how_many_days; ?>">
							 
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Term (months):</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="term_month" name="term_month" type="text" class="form-control input-md load_data_inputbox number_only" value="<?php echo $term_month; ?>">
								  <span class="help-block"></span>  
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput"># of Payments per Period:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input name="per_payment_period" type="text" class="form-control input-md load_data_inputbox number_only" value="<?php echo $per_payment_period; ?>">
						
								  </div>
								</div>

							<!--- NOTE : If user change this option value, then it will effect on First payment date And First payment Amount textbox-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Payment Reserve:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <select name="interest_reserve" class="form-control" onchange="interest_reserve_value(this.value);">
								  <?php foreach($yes_no_option3 as $key => $row){ ?>
									<option value="<?php echo $key; ?>" <?php if($key == $interest_reserve){ echo 'Selected';}?>><?php echo $row; ?></option>
								  <?php } ?>
								  </select>
						
								  </div>
								</div>
		
								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput"># of Payments:</label>  
								  <div class="col-md-4 load_data_input_div">
									<input id="of_payments" name="payment_held_close" type="text" class="form-control input-md load_data_inputbox number_only" value="<?php echo $payment_held_close; ?>">
									
								  </div>
								</div>
								
								<!-- Text input-->
								<!--
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Payment Requirement (Months)</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input name="payment_requirement" type="text" class="form-control input-md load_data_inputbox number_only" value="<?php echo $payment_requirement; ?>">
								  <span class="help-block"></span>  
								  </div>
								</div>
								
								<!-- Select Basic -->
								
								
								
								
								
								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Trustee:</label>  
								  <div class="col-md-4 load_data_input_div">
								 <!-- <input id="textinput" name="trustee" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $trustee; ?>" >
								 --> 
								 
								  <select name = "trustee" class="form-control load_data_inputbox">
										<option value = ''>Select</option>
										<?php 
											foreach($vendor_name as $vendor){
												$id   = $vendor->vendor_id;
												$name = $vendor->vendor_name;
												// if($name == $trustee){
												if($id == $trustee){
													$selected =  'selected = "selected"';
												}
												elseif($name == 'FCI Lender Services'){
													$selected =  'selected = "selected"';
													
												} 
												else{
													$selected = '';
												}
												echo '<option '.$selected.' value = '.$id.' >'.$name.'</option>';
											}		
										?>
								  </select>
								  
								  </div>
								</div>

                                   <br><br>
                      <?php /*
                               <!--      <div class="form-group">
								 <label class="loan_data_lable" for="textinput">Additional Guarantor:</label>
								  <div class="col-md-4 load_data_input_div">
								<input id="textinput" name="trustee" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $trustee; ?>" >
								 --> 
								 
					<!-- 		
						 <select name="add_adddiditonal_grad" onchange="additional_gra(this.value)" class="form-control input-md load_data_inputbox">
								  <?php foreach($yes_no_option1 as $key => $option){?>
									<option value="<?php echo $key;?>" <?php if($key==$additional_gra){ echo 'selected';} ?>><?php echo $option;?></option>
								  <?php } ?>
								 </select>
								  <span class="help-block"></span>  
								  </div>
								</div> --> 



							
                          <div class="form-group">
							
								  <div class="col-md-4 load_data_input_div">
								
						   <?php 


$id_array=explode(',',$add_gra_c_id);


$id =implode("','",$id_array);
$idd="'".$id."'";

 $query=$this->User_model->query("select contact_id,contact_firstname,contact_lastname from contact where contact_id IN (".$idd.") ");
    if($query->num_rows()>0){

 $fetch_query=$query->result();

}


foreach ($fetch_query as $key => $value) {
 ?>
          <p id="addd"><a href="<?php echo base_url().'viewcontact/'.$value->contact_id;?>"><?php echo $value->contact_firstname .' '.$value->contact_lastname;?></a></p>
          
<?php } ?>		



								  <span class="help-block"></span>  
								  </div>
								</div> */ ?>

						
								

							<!-----------Close form 1------------------------>
						</div>
						
				</div>
					
					
					
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form_loan_data">
						<div class="dashboard-stat2">
							<!------------form 2---------------->
								
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Balloon Payment:</label>
								  <div class="col-md-4 load_data_input_div">
									<select  name="baloon_payment_type" class="form-control load_data_inputbox" id="baloon_payment_type" >
									<option value="" ></option>
									  <?php 
									  foreach($yes_no_option as $key => $row)
									  {
										  ?>
										  <option value="<?= $key;?>" <?php if($key == $baloon_payment_type) { echo "selected";} ?> ><?= $row;?></option>
										  <?php
									  }
									  ?>
									</select>
								  </div>
								</div>
								
			<?php
			$aa = 0;
			
			if(isset($fetch_impound_account)){
				
		
				foreach($fetch_impound_account as $key=>$row){
					$impound_amount = $row->amount;
					$readonly = '';
											
					if($row->items == 'Mortgage Payment')
					{
												
						$aa += ($intrest_rate/100) * $loan_amount / 12;
					}
					if($row->items == 'Property / Hazard Insurance Payment')
					{
						if($row->impounded == 1){
							$aa += isset($loan_property_result) ? ($loan_property_insurance_result->annual_insurance_payment / 12) : 0;
						}
					}
											
					if($row->items == 'County Property Taxes')
					{
						if($row->impounded == 1){
							$aa += isset($loan_property_result) ? ($loan_property_result[0]->annual_property_taxes / 12) : 0;
						}
					}
					if($row->items == 'Flood Insurance')
					{
						if($row->impounded == 1){
							$aa += isset($row->amount) ? ($row->amount) : 0;
						}
					}
											
					if($row->items == 'Mortgage Insurance')
					{
						if($row->impounded == 1){
							$aa += isset($row->amount) ? ($row->amount) : 0;
						}
					}
				}
			}
			
			$balloon_payment_total = $aa + $loan_amount; 
			
			//echo $balloon_payment_total;
			?>
								
								
								
								
								
								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Balloon Payment:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="" name="ballon_payment" type="text" class="form-control input-md load_data_inputbox number_only" value="<?php echo '$'.number_format($balloon_payment_total,2);?>" readonly >
								
								  </div>
								</div>

								<!-- Select Basic -->
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Payment Frequency:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="selectbasic" name="payment_sechdule" class="form-control load_data_inputbox">
									
									  <?php 
									  foreach($loan_payment_schedule_option as $key => $row)
									  {
									  ?>
									  <option value="<?= $key;?>" <?php if($payment_sechdule == $key) { echo 'selected'; } ?> ><?= $row;?></option>
									  <?php
									  }
									  ?>
									  
									</select>
								  </div>
								</div>

								<!-- Select Basic -->
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Rate Type:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="selectbasic" name="intrest_type" class="form-control load_data_inputbox">
									
									<?php 
									foreach($loan_intrest_type_option as $key => $row)
									{
									?>
									  <option value="<?= $key;?>" <?php if($key == $intrest_type) { echo 'selected'; } ?> ><?= $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>
								
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Minimum Payment Required :</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="selectbasic" name="min_payment" class="form-control load_data_inputbox" onchange="min_payment_change(this.value);">
									   <?php foreach($yes_no_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key == $min_payment) { echo 'selected'; } ?> ><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>
								
								
								<!-- Text input-->
							
								<!-- Select Basic -->
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic"># of Minimum Payments:</label>
								  <div class="col-md-4 load_data_input_div">
								  <!--
									<select id="minium_intrest" name="minium_intrest" class="form-control load_data_inputbox" onchange = "minimum_select(this.value)">
									<option value="" ></option>
									   <?php foreach($yes_no_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key == $minium_intrest) { echo 'selected'; } ?> ><?php echo $row;?></option>
									<?php } ?>
									</select>-->
									
									<input type="text" id="min_intrest" name="minium_intrest" value="<?php echo $minium_intrest ? $minium_intrest : 0; ?>" class="form-control number_only" >
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group" id="minimum_select_yes" style="display:none;">
								  <label class="loan_data_lable" for="textinput">If Yes, # of months:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="min_intrest_month" name="min_intrest_month" type="text" class="form-control input-md load_data_inputbox" onchange="check_month_valid(this.value)" value="<?php echo $min_intrest_month; ?>">
								
								  </div>
								</div>
								
								<!-- Text input-->
								<div class="form-group" id="min_bal_type_option" style="display:none;">
								  <label class="loan_data_lable" for="textinput">Minimum Balance Type:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <select id="min_bal_type" name="min_bal_type" class="form-control load_data_inputbox">
									<?php
									
									foreach($min_bal_type_option as $key => $row)
									{
										?>
										<option value="<?php echo  $key; ?>" <?php if($key == $min_bal_type){ echo 'selected'; } ?> ><?php echo  $row; ?></option>
										<?php
									}
									
									?>
								  </select>
								 
								  </div>
								</div>

								<?php /*
								<!-- Select Basic -->
								<!--<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Owner Occupied?:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="selectbasic" name="owner_occupied" class="form-control load_data_inputbox">
									<option value="" ></option>
									   <?php foreach($yes_no_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key == $owner_occupied) { echo 'selected'; } ?> ><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>-->
								*/ ?>
								<!-- Select Basic -->
								
								
								
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Loan Purpose:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="selectbasic" name="owner_purpose" class="form-control load_data_inputbox">
									   <?php foreach($loan_purpose_option_type as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key == $owner_purpose) { echo 'selected'; } ?> ><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>

								<!-- Select Basic -->
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Payment Guaranty:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="loan_payment_gurranty" name="payment_gurranty" class="form-control load_data_inputbox">
									   <?php foreach($yes_no_not_applicable_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key == $payment_gurranty) { echo 'selected'; } ?> ><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>
								
								<?php /*
								<!--<div class="form-group" id="payment_gurranty_dependent">
								  <label class="loan_data_lable" for="selectbasic">Explanation:</label>
								  <div class="col-md-4 load_data_input_div">
									<textarea name="payment_gurr_explain" class="form-control"><?php echo $payment_gurr_explain; ?></textarea>
								  </div>
								</div>-->
								
								<!-- Select Basic -->
								<!--
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Construction Loan</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="selectbasic" name="costruction_loan" class="form-control load_data_inputbox">
									   <?php foreach($yes_no_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <? if($key == $costruction_loan) { echo 'selected'; } ?> ><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>
								-->
								*/ ?>
								
								
								
								<!-- Select Basic -->
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Loan Type:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="loan_type" name="loan_type" class="form-control load_data_inputbox">
									  <?php foreach($loan_type_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key ==$loan_type ){ echo "selected"; }?> ><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>
								
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Transaction Type:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="transaction_type" name="transaction_type" class="form-control load_data_inputbox">
									<option value="" ></option>
									  <?php foreach($transaction_type_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key ==$transaction_type ){ echo "selected"; }?> ><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>
								
								
								<div class="form-group" id="">
								  <label class="loan_data_lable" for="textinput">Construction Reserve Account:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <select name="draws" type="text" class="form-control input-md load_data_inputbox" >
								 
								  <?php
								 
								  foreach($yes_no_option3 as $key => $row)
								  {?>
									  <option value="<?= $key;?>" <?php if($key == $draws){
									  echo 'selected';} ?>><?= $row; ?></option>
									  <?php
								  }
								  ?>
								  </select>
								 
								  </div>
								</div>
								
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">UCC1 Filing:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <select name="ucc_filing" type="text" class="form-control input-md load_data_inputbox" >
								 
								  <?php
								 
								  foreach($yes_no_not_applicable_option as $key => $row)
								  {?>
									  <option value="<?= $key;?>" <?php if($key == $ucc_filing){
									  echo 'selected';} ?>><?= $row; ?></option>
									  <?php
								  }
								  ?>
								  </select>
								
								  </div>
								</div>
								
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Phase 1 Required :</label>  
								  <div class="col-md-4 load_data_input_div">
								  <select name="phase" type="text" class="form-control input-md load_data_inputbox">
								 
								  <?php
								 
								  foreach($yes_no_not_applicable_option as $key => $row)
								  {?>
									  <option value="<?= $key;?>" <?php if($key == $phase){
									  echo 'selected';} ?>><?= $row; ?></option>
									  <?php
								  }
								  ?>
								  </select>
						
								  </div>
								</div>
								
								<?php /*
								<!-- 		<div id="add" class="form-group">
								  <label class="loan_data_lable" for="textinput">Contact Names:</label>  
								  <div class="col-md-4 load_data_input_div">
						
									<select name="add_gra_c_id[]" class="chosen" multiple="multiple">
									<option>Select One</option>
									<?php foreach($fetch_all_contact as $rows) { ?>
										<option value="<?php echo $rows->contact_id;?>" <?php if($rows->contact_id == $add_gra_c_id){echo 'Selected';}?>><?php echo $rows->contact_firstname .' '. $rows->contact_middlename .' '. $rows->contact_lastname ;?></option>
									<?php } ?>
								</select>


								  <span class="help-block"></span>  
								  </div>
								</div> -->
																						
								<!--<div class="form-group" id="construction_div">
								  <label class="loan_data_lable" for="textinput">Renovation Draw:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <select id="select_draws" name="draws" type="text" class="form-control input-md load_data_inputbox" >
								 
								  <?php
								 
								  foreach($yes_no_option as $key => $row)
								  {
										if(isset($draws) && ($draws != '')){
											if($key == $draws){
												$selected =  'selected';
											}else if($key == ''){
												
												$selected =  'selected';
											}
										}else{
											$selected = '';
										}
									  ?>
									  <option value="<?= $key;?>" <?php echo $selected; ?>><?= $row; ?></option>
									  <?php
								  }
								  ?>
								  </select>
								  <span class="help-block"></span>  
								  </div>
								</div>-->
								
							

								<!-- Text input-->
								<!--
								<div class="form-group loan_type_dependent">
								  <label class="loan_data_lable" for="textinput">Construction Budget:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="project_cost" name="project_cost" type="text" class="form-control input-md load_data_inputbox  number_only" value="<?php echo '$'.number_format($project_cost,2); ?>">
								  <span class="help-block"></span>  
								  </div>
								</div>
								-->
								
								
								
								
								<!--<div class="form-group">
									<div class="col-md-12">
										<a class="btn blue full-width-button" data-toggle="modal" href="#construcion_type_sources_and_uses" >Sources & Uses</a>
									</div>
								</div>
								-->
								
								<?php
								if($loan_id)
								{
								?>
								<!--<div class="form-group">
								  <div class="col-md-4 load_data_input_div">
									 <a class="btn blue" data-toggle="modal" href="#additional_broker_data" id="additional_broker_data_button">Additional Broker Data </a>
								  </div>
								  
								</div>-->
								<?php } ?>
								
								
								
								<!-- Text input -->
								<!--
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Loan Highlights</label>  
								  <div class="col-md-4 load_data_input_div">
									  <input id="" name="loan_highlights" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $loan_highlights; ?>" >
									  <span class="help-block"></span>  
								  </div>
								</div>

								-->
								*/ ?>
							
							<!-------------Close form 2---------->
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form_loan_data">
						<div class="dashboard-stat2">
							<!------------form 3---------------->
							
							

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Payment Grace Period (Days):</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="textinput" name="grace_period" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $grace_period; ?>" >
						
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Late Charge: (% of Payment): </label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="late_charges_due" name="late_charges_due" type="text" class="form-control input-md load_data_inputbox number_only" value="<?php echo number_format($late_charges_due,2).'%'; ?>">
								 
								  </div>
								</div>

								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Default Rate Replaces Late Fee:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="replace_late_fee" name="replace_late_fee" class="form-control load_data_inputbox">
									<?php foreach($no_yes_option as $key => $row){ ?>

										  <option value="<?= $key;?>" <?php if($key == $replace_late_fee) { echo "selected";} ?> ><?= $row;?></option>

									<?php } ?>
									</select>
								  </div>
								</div>

								<!-- Text input-->
								

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Return Check Fee:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="return_check_rate" name="return_check_rate" type="text" class="form-control input-md load_data_inputbox  number_only" value="<?php echo '$'.$return_check_rate; ?>" >
								
								  </div>
								</div>

								<!-- Select Basic -->
								

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Loan Document Date:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="loan_document_date" name="loan_document_date" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $loan_document_date; ?>" placeholder = "MM-DD-YYYY">
						
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">
								  Loan Closing Date:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="loan_funding_date" name="loan_funding_date" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $loan_funding_date; ?>"  placeholder = "MM-DD-YYYY">
								 
								  </div>
								</div>

								<!-- Text input -->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">First Payment Date:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="first_payment_date" name="first_payment_date" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $first_payment_date; ?>"  placeholder = "MM-DD-YYYY">
						
								  </div>
								</div>


								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Amortizing Type:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="loan_payment_type" name="payment_type" class="form-control load_data_inputbox">
									<option value="" ></option>
									  <?php 
									  foreach($loan_payment_type_option as $key => $row)
									  {
										  ?>
										  <option value="<?= $key;?>" <?php if($key == $payment_type) { echo "selected";} ?> ><?= $row;?></option>
										  <?php
									  }
									  ?>
									</select>
								  </div>
								</div>
							

								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Calculation Type:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="selectbasic" name="calc_of_year" class="form-control load_data_inputbox">
									  <?php foreach($calc_day_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key == $calc_of_year){ echo 'selected'; } ?>><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>

								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Loan Balance Calculation:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="calbased" name="calc_based" class="form-control load_data_inputbox">
									  <?php foreach($calc_based_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key == $calc_based){ echo 'selected'; } ?>><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>
						
						


								<?php

							
									$aa = 0;				
										if(isset($fetch_impound_account)){
											
									
											foreach($fetch_impound_account as $key=>$row){
												$impound_amount = $row->amount;
												$readonly = '';
																		
												if($row->items == 'Mortgage Payment')
												{
														 $aa +=($outstanding_balance*($intrest_rate)/100)/12;						
														// $aa +=($outstanding_balance*$intrest_rate/100)/12;						
													// $aa += ($intrest_rate/100) * $loan_amount / 12;
												}
												if($row->items == 'Property / Hazard Insurance Payment')
												{
													if($row->impounded == 1){
														$aa += isset($loan_property_result) ? ($loan_property_insurance_result->annual_insurance_payment / 12) : 0;
													}
												}
																		
												if($row->items == 'County Property Taxes')
												{
													if($row->impounded == 1){
														$aa += isset($loan_property_result) ? ($loan_property_result[0]->annual_property_taxes / 12) : 0;
													}
												}
												if($row->items == 'Flood Insurance')
												{
													if($row->impounded == 1){
														$aa += isset($row->amount) ? ($row->amount) : 0;
													}
												}
																		
												if($row->items == 'Mortgage Insurance')
												{
													if($row->impounded == 1){
														$aa += isset($row->amount) ? ($row->amount) : 0;
													}
												}
											}
										}
									// $first_monyly_payment = '';	
									// if($interest_reserve == '1'){
										
										// $daily_rate = $aa/30;
										// $first_payment_day 	= date('d',strtotime($loan_funding_date));
										// $first_payment_remaining 	= 30 - $first_payment_day;
										// $first_monyly_payment = number_format($aa - ($daily_rate * $first_payment_remaining),2);
									// }else{
										
										$first_monyly_payment = number_format($aa,2);		
									//}
										function PMT($i, $n, $p) {
										 return $i * $p * pow((1 + $i), $n) / (1 - pow((1 + $i), $n));
										}
										/*$payment_type = 2;
										$intrest_rate = 4.250;
										$payment_held_close = 360;
										$loan_amount = 100000;*/
										if($payment_type == 2){
											if($term_month){
												if($term_month > 0){
													$newRateInt = number_format($intrest_rate,3);
													$first_monyly_payment = (number_format(PMT($newRateInt / 1200, $term_month, -$loan_amount),2));
												}
											}
										}
								
								?>
								
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">First Payment Amount:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="first_payment_amount" name="first_payment_amount" type="text" class="form-control input-md load_data_inputbox" value="$<?php echo $first_monyly_payment;?>" readonly >
							
								  </div>
								</div>

								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Collect First Payment at Close:</label> 
								 <div class="col-md-4 load_data_input_div">
								  <select name="first_payment_collect" class="form-control input-md load_data_inputbox">
								  <?php foreach($yes_no_option1 as $key => $option){?>
									<option value="<?php echo $key;?>" <?php if($key == $first_payment_collect){echo'Selected';}?>><?php echo $option;?></option>
								  <?php } ?>
								  </select>
								 </div>
								</div>

								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Payment Due Day:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <select name="payment_due" class="form-control load_data_inputbox">
									   <?php foreach($payment_due_option as $key => $row) { ?>
									  <option value="<?php echo $key;?>" <?php if($key == $payment_due) { echo 'selected'; } ?> ><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>

								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">ACH Required:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <select name="auto_payment" class="form-control load_data_inputbox">
									<?php foreach($yes_no_option3 as $key => $row) { ?>
									  	<option value="<?php echo $key;?>" <?php if($key == $auto_payment){ echo 'selected';}?>><?php echo $row;?></option>
									<?php } ?>
									</select>
								  </div>
								</div>
								<!-- Text input-->
								<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Maturity Date:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="maturity_date" name="maturity_date" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $maturity_date; ?>"  placeholder = "MM-DD-YYYY" readonly>
								
								  </div>
								</div>
								<!-- Text input-->
								<!--<div class="form-group">
								  <label class="loan_data_lable" for="textinput">Modified Maturity Date</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="modify_maturity_date" name="modify_maturity_date" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $modify_maturity_date; ?>"  placeholder = "MM-DD-YYYY" >
								  <span class="help-block"></span>  
								  </div>
								</div>-->

								<!-- Select Basic -->
								
								<div class="form-group">
								  <label class="loan_data_lable" for="selectbasic">Extension Option:</label>
								  <div class="col-md-4 load_data_input_div">
									<select id="extention_option" name="extention_option" class="form-control load_data_inputbox" onchange="extention_select(this.value)">
										
									   <?php foreach($yes_no_option as $key => $row) {
										   $selected = '';
											if($extention_option == $key)
											{
												$selected = 'selected';
											}
									
									   ?>
										<option value="<?php echo $key;?>" <?php echo $selected;?> ><?php echo $row;?></option>
										<?php } ?>
									</select>

									
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group extention_select_yes" >
								  <label class="loan_data_lable" for="textinput">If Yes , # of months:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="textinput" name="extention_month" type="text" class="form-control input-md load_data_inputbox" value="<?php echo $extention_month; ?>">
							
								  </div>
								</div>

								<!-- Text input-->
								<div class="form-group extention_select_yes" >
								  <label class="loan_data_lable" for="textinput">If Yes , % of loan Amount:</label>  
								  <div class="col-md-4 load_data_input_div">
								  <input id="textinput" name="extention_percent_amount" type="text" class="form-control input-md load_data_inputbox  number_only" value="<?php echo $extention_percent_amount; ?>">
								 
								  </div>
								</div>
								
						</div>
					</div>
					
				</div> 

				<div class="row">

					<div class="form-group">
						<label class="loan_data_lable" for="textinput">Loan Conditions:</label>  
						<div class="col-md-12" >
							<textarea rows="3" name="loan_conditions"  class="form-control"><?php echo $loan_conditions; ?></textarea>
						</div>	
					</div>
					
					
					<div class="form-group">
					  <label class="loan_data_lable" for="textinput">Exit Strategy:</label>  
						<div class="col-md-12" >
							<textarea rows="3" name="exit_strategy" class="form-control"><?php echo $exit_strategy; ?></textarea>
						</div>	
					</div>
		
					<div class="form-group">
						<label class="loan_data_lable" for="textinput">Loan Purpose</label>  
						
						
						<div id="append_main_promissory_notee">
						<?php
						
						if(isset($fetch_additional_loan_resultt)){
							foreach($fetch_additional_loan_resultt as $row){ ?>
						
							<div class="row" id="main_promissory_notee">
								 
								<div class="col-md-12" >
									<textarea rows="3"  name="promissory_textt[]" id="promissory_text_<?php echo $row->servicing_purpose_id;?>" placeholder="loan purpose.." class="form-control"><?php echo $row->loan_purpose;?></textarea>
									<input type="hidden" name="hidden_id[]" value="<?php echo $row->servicing_purpose_id;?>">
									
								</div>	
							</div>
				
						<?php }
						
							}else{?>
						
							<div class="row" id="main_promissory_notee">
								
								<div class="col-md-12">
									<textarea rows="3" col name="promissory_textt[]" placeholder="loan purpose..." class="form-control"></textarea>
									<input type="hidden" name="hidden_id[]" value="new">
								</div>
								
							</div>
							
						<?php } ?>
							
				
						</div>

					</div>
				
			
					<div class="form-group">
						<label class="loan_data_lable" for="textinput">Additional Loan Provisions</label>  
							
							<div id="append_main_promissory_note">
								<?php if(isset($fetch_additional_loan_result)){
									foreach($fetch_additional_loan_result as $row){?>
								
									<div class="row" id="main_promissory_note">
										
										<div class="col-md-12" >
											<textarea rows="6" name="promissory_text[]" id="promissory_text_<?php echo $row->id;?>" class="form-control"><?php echo $row->promissory_text;?></textarea>
											<input type="hidden" name="hidden_idd[]" value="<?php echo $row->id;?>">
											
										</div>	
									</div>
									
								<?php }
								
								}else{?>
								
									<div class="row" id="main_promissory_note">
										
										<div class="col-md-12">
											<textarea  rows="3" name="promissory_text[]" class="form-control"></textarea>
											<input type="hidden" name="hidden_idd[]" value="new">
										</div>
										
									</div>
									
								<?php } ?>
							</div>
					</div>

					<div class="form-group">
						<label class="loan_data_lable" for="textinput">Cross Collateral Provisions</label>  
						<div class="col-md-12">
							<textarea rows="5" name="cross_provision" id="cross_provision" class="form-control"><?php echo $cross_provision;?></textarea>
						</div>
					</div>

				</div>
				
			
					 <!-----Form End -------->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
					<input type = "hidden" value = "modal_loan_terms" name = "modal_hit" />
					<button type="submit" name="form_button" value="save" class="btn blue">Save </button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>


<script type="text/javascript">
hide_cross_provision('<?php echo $cross_collateralized; ?>');



var hit = '<?php echo $this->session->flashdata('loan_term_hit');?>';
if(hit == '1'){
	$('#loan_terms').modal('show');
}

// $( ".accured_date" ).datepicker({ dateFormat: 'mm-dd-yy' });
var impound_action = '<?php echo $this->session->flashdata('impound_action');?>';
if(impound_action == '1'){
	$('#impound_accounts').modal('show');				   
}
</script>