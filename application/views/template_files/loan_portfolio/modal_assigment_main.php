<?php  $optional_insurance_option				= $this->config->item('optional_insurance_option');
?>
<div class="modal fade" id="assigment_main" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
		<div class="modal-body">
			<form method="POST" action="<?php echo base_url()?>loan_servicing_form" id="assignment_home_id">
	
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<!--<h4 class="modal-title">Lender Assignments : #<?php// echo $talimar_loan; ?></h4>-->
					<h4 class="modal-title">Capital Markets : <?php echo $property_addresss; ?></h4>
				</div>
				
					<!-----------Hidden fields----------->
					<input type ="hidden" value="loan_assignment" name="form_type"/>
					
			
					<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
			
					<input type="hidden" name="loan_id" value="/<?php echo $loan_id; ?>">
			
					<!-----------End of hidden Fields---->
					
				<!-------Main div (left side) end---->
				    <br>
					<div class="col-md-5">
						<?php
						$FundingEntity="";
						if(!empty($fetch_loan_result[0]->FundingEntity)){
							$FundingEntity=$fetch_loan_result[0]->FundingEntity;
						}
						?>
						<h4>Capital Markets</h4>
						<div class="row">
							<div class="form-group">
								<label class="col-md-6" for="FundingEntitys" style="font-weight: normal !important;">Capital Markets: </label>  
								  <div class="col-md-6">
								  <select class="chosen" name="FundingEntity" id="FundingEntitys">
								  	<option value="">Select One</option>
								  	<?php foreach($investor_all_data as $row){ 
								  			if($row->funding_entity == 1){ ?>
								  				<option value="<?php echo $row->id;?>" <?php if($FundingEntity == $row->id){echo 'selected';}?>><?php echo $row->name;?></option>
								  	<?php } } ?>
								  </select>
									</div>
							</div>
						</div>
						
						<h4>Loan Summary</h4>
							<div class="row">
								<div class="form-group">
								  <label class="col-md-6" for="textinput">Loan Summary Complete:</label>  
								  <div class="col-md-6">
									<select id="textinput" name="loan_summary_complete"  class="form-control input-md">
									<?php foreach($l_s_c as $key => $row){
									?>
									<option value="<?php echo $key;?>" <?php if($loan_servicing['lsc'] == $key){ echo 'selected'; } ?> ><?php echo $row;?></option>
									<?php
									}
								  
									?>
								</select>
								<span class="help-block"></span>
								  </div>
								</div>
							</div>
						<h4>Lender Minimum</h4>
							<div class="row">
								<div class="form-group">
								  <label class="col-md-6" for="textinput">Lender Minimum:</label>  
								  <div class="col-md-6">
								  <input id="lender_minimum" name="lender_minimum" type="text"  class="form-control input-md amount_format_without_decimal number_only" value="<?= '$'.number_format($loan_servicing['lender_minimum']); ?>">
								  <span class="help-block"></span>  
								  </div>
								</div>
							</div>
						<h4>Servicing Fees: (% paid to TaliMar Financial)</h4>
							<div class="row">
								<?php

								$servicing_fee = ($loan_servicing['servicing_fee'] != '') ? number_format($loan_servicing['servicing_fee'],2).'%' : '1.00%';
								$lender_rate = $fetch_loan_result[0]->intrest_rate - $servicing_fee;

								if($loan_servicing['servicing_lender_rate'] == 'NaN' || $loan_servicing['servicing_lender_rate'] == '' || $loan_servicing['servicing_lender_rate'] == '0.00'){

									if($loan_assigment_data[0]->invester_yield_percent >0){

									$lender_ratessss = $loan_assigment_data[0]->invester_yield_percent;
								}}else{

								   $lender_ratessss = $loan_servicing['servicing_lender_rate'];
								}

								?>
									<div class="form-group">
									  <label class="col-md-8 control-label lv" for="textinput ">Lender Rate:</label>  
									  <div class="col-md-4">
										<input name="servicing_lender_rate" type="text" class="form-control input-md rate_format number_only" value="<?php echo number_format($lender_ratessss,3).'%';?>">
									  <span class="help-block"></span>  
									  </div>
									</div>
								</div>
								
								<div class="row">
								
									<div class="form-group">
									  <label class="col-md-8 control-label" for="textinput lv">Note Rate:</label>  
									  <div class="col-md-4">
										
									<?php 
										
								$interest_rate = (isset($fetch_loan_result[0]->intrest_rate)) ? $fetch_loan_result[0]->intrest_rate :'0.00'; ?>	
								
								   <input name="note_rate" type="text" class="form-control input-md" value="<?php echo number_format($interest_rate,3).'%';?>" disabled>
									 
									



									  <span class="help-block"></span>  
									  </div>
									</div>
								</div>

								<div class="row">
								<?php 
								//$servicing_fee = ($loan_servicing['servicing_fee'] != '') ? number_format($loan_servicing['servicing_fee'],2).'%' : '1.00%';
								$interest_rate = (isset($fetch_loan_result[0]->intrest_rate)) ? $fetch_loan_result[0]->intrest_rate :''; 
								$servicing_fee = ($loan_servicing['servicing_fee'] != '') ? number_format($loan_servicing['servicing_fee'],2).'%' : '1.00%';
								$lender_rate =    $fetch_loan_result[0]->intrest_rate - $servicing_fee;
								$servicer_spread = $interest_rate-$lender_rate;

								$new_val = $interest_rate - $lender_ratessss;

								if($new_val > 0){
									$BrokerServicingDisbursementFee = '15';
								}else{
									$BrokerServicingDisbursementFee = '0';
								}

								?>	

									<div class="form-group">
									  <!--<label class="col-md-8 control-label" for="textinput lv">Servicer Spread:</label> --> 
									  <label class="col-md-8 control-label" for="textinput lv">TaliMar Spread:</label>  
									  <div class="col-md-4">
							<input id="servicing_feess" name="servicing_fee" type="text"  class="form-control input-md rate_format number_only" value="<?php echo number_format($new_val,3).'%';?>" disabled>
									  <span class="help-block"></span>  
									  </div>
									</div>
								</div>
								
							
								<div class="row">
									<div class="form-group">
									  <!--<label class="col-md-8 control-label lv" for="textinput">Lender Servicing Fee:</label>--> 
									  <label class="col-md-8 control-label lv" for="textinput">Sub Servicer Fee:</label>  
									  <div class="col-md-4">
									  <input id="textinput" name="minimum_servicing_fee" type="text"  class="form-control input-md amount_format number_only" value="<?= $loan_servicing['minimum_servicing_fee']? '$'.number_format($loan_servicing['minimum_servicing_fee'],2) : '$15.00'; ?>" >
									  <span class="help-block"></span>  
									  </div>
									</div>
								</div>
							
								
								<div class="row">
									<div class="form-group">
									  <label class="col-md-8 control-label lv" for="textinput">Broker Servicing Disbursement Fee:</label>  
									  <div class="col-md-4">
									  
									  <input name="broker_servicing_fees" type="text"  class="form-control input-md amount_format_without_decimal number_only" value="$<?php echo number_format($BrokerServicingDisbursementFee); ?>" readonly>
									  <span class="help-block"></span>  
									  </div>
									</div>
								</div>

						<?php
							/* echo '<pre>';
							 print_r($fee_disbursement_list);
							 print_r($fee_disbursement_data);
							 echo '</pre>';*/
						?>

						<?php foreach($fee_disbursement_list as $key => $row){ 

							if($row == 'Late Fee'){ 
								$attrname = 'Newlate_fee';
								if(empty($fee_disbursement_data[$key]->broker) || $fee_disbursement_data[$key]->broker=="0.00"){
									$dfeesValue='50';
								}else{
									$dfeesValue=$fee_disbursement_data[$key]->broker;
								}
								//$dfeesValue = isset($fee_disbursement_data) ? $fee_disbursement_data[$key]->broker : '50';
							}elseif($row == 'Extension Fee'){ 
								$attrname = 'NewExtension_fee';
								if(empty($fee_disbursement_data[$key]->broker) || $fee_disbursement_data[$key]->broker=="0.00"){
									$dfeesValue='75';
								}else{
									$dfeesValue=$fee_disbursement_data[$key]->broker;
								}
								//$dfeesValue = isset($fee_disbursement_data) ? $fee_disbursement_data[$key]->broker : '75';
							}elseif($row == 'Minimum Interest'){ 
								$attrname = 'NewMin_interest';
								$dfeesValue = isset($fee_disbursement_data) ? $fee_disbursement_data[$key]->broker : '0';
							}elseif($row == 'Default Rate'){ 
								$attrname = 'NewDefault_rate';
								if(empty($fee_disbursement_data[$key]->broker) || $fee_disbursement_data[$key]->broker=="0.00"){
									$dfeesValue='75';
								}else{
									$dfeesValue=$fee_disbursement_data[$key]->broker;
								}
								//$dfeesValue = isset($fee_disbursement_data) ? $fee_disbursement_data[$key]->broker : '75';
							}else{ 
								$attrname = '';
								$dfeesValue = '';
							}

								//$eachfeevalue = isset($fee_disbursement_data) ? $fee_disbursement_data[$key]->broker : '0';

						?>
							<div class="row">
								<div class="form-group">
									<label class="col-md-8 control-label lv" for="textinput"><?php echo $row;?>:</label>  
									<div class="col-md-4">
										<input name="<?php echo $attrname;?>" type="text"  class="form-control input-md rate_format number_only" value="<?php echo number_format($dfeesValue,2).'%';?>">
										<span class="help-block"></span>  
									</div>
								</div>
							</div>

						<?php } ?>

							<h4>Sources of Borrower / Property Information:</h4>
							<div class="row">
								<div class="loan_servicing_checkboxex">
								  <input type = 'checkbox' name="broker_inquiry" value="1"  <?php if($loan_servicing['broker_inquiry'] == '1'){ echo 'checked'; }else{ echo 'checked';} ?> /> Broker Inquiry
								</div>
								<div class="loan_servicing_checkboxex">
								  <input type = 'checkbox' name="borrower" value="1"  <?php if($loan_servicing['borrower'] == '1'){ echo 'checked'; }else if ($loan_servicing['borrower'] == '0'){ echo 'unchecked'; }else{ echo 'checked';} ?> /> Borrower
								</div>
						
								<div class="loan_servicing_checkboxex">
								  <input type = 'checkbox' name="credit_report" value="1"  <?php if($loan_servicing['credit_report'] == '1'){ echo 'checked'; }else if ($loan_servicing['credit_report'] == '0'){ echo 'unchecked'; }else{ echo 'checked';} ?> /> Credit Report 
								</div>
						
								<div class="loan_servicing_checkboxex">
								  <input type = 'checkbox' name="seller_note" value="1"  <?php if($loan_servicing['seller_note'] == '1'){ echo 'checked'; }else if ($loan_servicing['seller_note'] == '0'){ echo 'unchecked'; }else{ echo 'checked';} ?> /> Seller of Note
								</div>
						
								<div class="loan_servicing_checkboxex">
								  <input type = 'checkbox' name="trustor" value="1"  <?php if($loan_servicing['trustor'] == '1'){ echo 'checked'; }else if ($loan_servicing['trustor'] == '0'){ echo 'unchecked'; }else{ echo 'checked';} ?> /> Trustor
								</div>
							</div>
					</div> 
				<!---- main div (left side) close---->

				<!---- main div (center) start---->
					<div class="col-md-5">
						<h4>Lender Disclosures</h4>
							<div class="row">
								<div class="form-group">
									<label class="col-md-8 control-label lv" for="textinput">Is this a Multi-Lender Loan:</label>  
									<div class="col-md-4">
										
									
										<select id="textinput" name="is_multi_lender"  class="form-control input-md">
										   <?php foreach($yes_no_option3 as $key => $row){
												
											?>
											  <option value="<?php echo $key;?>" <?php if($loan_servicing['is_multi_lender'] == $key){ echo 'selected'; } ?> ><?php echo $row;?></option>
											  <?php
												}
										  
										  ?>
										</select> 
										
										<span class="help-block"></span>  
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="form-group">
									<label class="col-md-8 control-label lv" for="textinput">Lender Waived Appraisal?:</label>  
									<div class="col-md-4">
										<select id="textinput" name="lender_waives_apr"  class="form-control input-md">
										<?php foreach($yes_no_option3 as $key => $row){	?>
										
											<option value="<?php echo $key;?>" <?php if($loan_servicing['lender_waives_apr'] == $key){echo 'Selected';}?> ><?php echo $row;?></option>
											
											<?php } ?>
										</select>
										<span class="help-block"></span>  
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
								   <label class="col-md-8 control-label lv" for="textinput">Are the Servicing Arrangment(s) included? </label>  
									<div class="col-md-4">
										<select id="textinput" name="servicing_arrangent"   class="form-control input-md">
										<?php foreach($yes_no_option3 as $key => $row){ ?>
										
											<option value="<?php echo $key;?>" <?php if($loan_servicing['servicing_arrangent'] == $key){echo 'Selected';}?> ><?php echo $row;?></option>
											<?php  } ?>
											
										</select>
										<span class="help-block"></span>  
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="form-group">
								  <label class="col-md-8 control-label lv" for="textinput">Is the Broker acting as Servicing Agent? </label>  
								  <div class="col-md-4">
								  <select id="textinput" name="is_broker_s_agent"  class="form-control input-md">
								   <?php foreach($yes_no_option3 as $key => $row){
										
									?>
									  <option value="<?php echo $key;?>" <?php if($loan_servicing['is_broker_s_agent'] == $key){echo 'Selected';}?> ><?php echo $row;?></option>
									  <?php
										}
								  
								  ?>
								  </select>
								  <span class="help-block"></span>  
								  </div>
								</div>
							</div>
					
							<div class="row">
								<div class="form-group">
								  <label class="col-md-8 control-label lv" for="textinput">Funded as Principal w/ Intent to Sell?</label>  
								  <div class="col-md-4">
								 
							
									<select id="textinput" name="fund_broker_controll"  class="form-control input-md">
								   <?php foreach($yes_no_option3 as $key => $row){
										
									?>
									  <option value="<?php echo $key;?>" <?php if($loan_servicing['fund_broker_controll'] == $key){ echo 'selected'; } ?> ><?php echo $row;?></option>
									  <?php
										}
								  
									?>
									</select> 
								

								  <span class="help-block"></span>  
								  </div>
								</div>
							</div>
					
							
							<div class="row">
								<div class="form-group">
								  <label class="col-md-8 control-label lv" for="textinput">Low Doc Loan:</label>  
								  <div class="col-md-4">
								  <select id="textinput" name="low_loan_doc"  class="form-control input-md">
								   <?php foreach($yes_no_option3 as $key => $row){
										
									?>
									  <option value="<?php echo $key;?>" <?php if($loan_servicing['low_loan_doc'] == $key){echo 'Selected';} ?> ><?php echo $row;?></option>
									  <?php
										}
								  
								  ?>
								  </select>
								  <span class="help-block"></span>  
								  </div>
								</div>
							</div>
					
							<div class="row">
								<div class="form-group">
								  <label class="col-md-8 control-label" style="font-weight:none!important;" for="textinput">Are there subordination provisions?</label>  
								  <div class="col-md-4">
								  <select id="subordination_provision" name="subordination_provision" type="text"  class="form-control input-md">
								  <?php foreach($yes_no_option4 as $key => $row){
										
									?>
									  <option value="<?php echo $key;?>" <?php if($loan_servicing['subordination_provision'] == $key){echo 'Selected';} ?> ><?php echo $row;?></option>
									  <?php
										}
								  
								  ?>
								  </select>
								  <span class="help-block"></span>  
								  </div>
								</div>
							</div>

							<div class="row">
								<div class="form-group">
								  <label class="col-md-8 control-label lv" for="textinput">Lender Approval for Draws:</label>  
								  <div class="col-md-4">
									<select id="textinput" name="lender_approval"  class="form-control input-md">
											   <?php foreach($yes_no_option4 as $key => $row){
													
												?>
												  <option value="<?php echo $key;?>" <?php if($loan_servicing['lender_approval'] == $key){ echo 'selected'; } ?> ><?php echo $row;?></option>
												  <?php
													}
											  
											  ?>
									</select> 
								  <span class="help-block"></span>  
								  </div>
								</div>
							</div>
							
							<div class="row">
								<div class="form-group">
								  <label class="col-md-8 control-label lv" for="textinput">Optional Insurance Services:</label>  
								  <div class="col-md-4">
									<select id="textinput" name="optional_insurance"  class="form-control input-md">
									   <?php foreach($optional_insurance_option as $key => $row){?>
										  <option value="<?php echo $key;?>" <?php if($loan_servicing['optional_insurance'] == $key){ echo 'selected'; } ?>><?php echo $row;?></option>
									   <?php } ?>
									</select> 
								  <span class="help-block"></span>  
								  </div>
								</div>
							</div>

							<div class="row">
								<div class="form-group">
									<label class="col-md-8 control-label lv" for="textinput">Who Pays Setup Fees:</label>  
									<div class="col-md-4">
										<select id="pay_setup_fee" name="pay_setup_fee"  class="form-control input-md">
										<?php foreach($who_pay_setup_fees as $key => $row){
										?>
											<option value="<?php echo $key;?>" <?php if($loan_servicing['pay_setup_fee'] == $key){ echo 'selected'; } ?> ><?php echo $row;?></option>
											<?php
											}
										  
											?>
										</select>
										<span class="help-block"></span>  
									</div>
								</div>
							</div>

							<div class="row">
								<div class="form-group">
									<label class="col-md-8 control-label lv" for="textinput">Who Pays Servicing Costs:</label>  
									<div class="col-md-4">
										<select id="textinput" onchange="fun_flip(this.value)" name="who_pay_servicing"  class="form-control input-md">
										<?php foreach($serviceing_who_pay_servicing_option as $key => $row){
										?>
											<option value="<?php echo $key;?>" <?php if($loan_servicing['who_pay_servicing'] == $key){ echo 'selected'; } ?> ><?php echo $row;?></option>
											<?php
											}
										  
											?>
										</select>
										<span class="help-block"></span>  
									</div>
								</div>
							</div>

							<div class="row">
								<div class="form-group">
									<label class="col-md-8 control-label lv" for="textinput">How are Setup Fees Paid:</label>  
									<div class="col-md-4">
										<select id="textinput" name="how_setup_fee_pay"  class="form-control input-md">
										<?php foreach($how_setup_fees_paid as $key => $row){ ?>
											<option value="<?php echo $key;?>" <?php if($loan_servicing['how_setup_fee_pay'] == $key){ echo 'selected'; } ?>><?php echo $row;?></option>
										<?php } ?>
										</select>
										<span class="help-block"></span>  
									</div>
								</div>
							</div>				
							
							<h4>RE851 Disclosures Data</h4>
							<div id="select_loan_organisation">
								<h4>New Loan – RE851A</h4>
								<div class="row">
									<div class="loan_servicing_checkboxex div_full_width">
									  <input type = 'checkbox' id="l_o_behalf_another" name="l_o_behalf_another" value="1"  <?php if($loan_servicing['l_o_behalf_another'] == '1'){ echo 'checked'; }else{ echo 'Checked'; }
									   ?> onclick="broker_capacity_check_box()" >Agent is arranging a loan on behalf of another  
									</div>
								</div>
					
								<div class="row">
									<div class="loan_servicing_checkboxex div_full_width">
									  <input type = 'checkbox' id="l_o_principal_borrower" name="l_o_principal_borrower" value="1"  <?php if($loan_servicing['l_o_principal_borrower'] == '1'){ echo 'checked'; } ?> onclick="broker_capacity_check_box()" >Principal as a borrower on the funds or will benefit other than from the note 
									</div>
								</div>
								
								<div class="row">
									<div class="loan_servicing_checkboxex div_full_width">
									  <input type = 'checkbox' id="l_o_portion_loan"  name="l_o_portion_loan" value="1"  <?php if($loan_servicing['l_o_portion_loan'] == '1'){ echo 'checked'; } ?> onclick="broker_capacity_check_box()" >Funding a portion of this loan (Multi Lender) 
									</div>
								</div>
					
							</div>
							
							
							<div id="select_sale_of_existing">
								<h4>Existing Loan – RE851B</h4>
								<div class="row">
									<div class="loan_servicing_checkboxex div_full_width">
									  <input type = 'checkbox' id="a_d_behalf_another" name="a_d_behalf_another" value="1"  <?php if($loan_servicing['a_d_behalf_another'] == '1'){ echo 'checked'; } ?>  onclick="broker_capacity_check_box()" >Agent is arranging a sale of an existing note on behalf of another 
									</div>
								</div>
					
								<div class="row">
									<div class="loan_servicing_checkboxex div_full_width">
									  <input type = 'checkbox' id="a_d_principal_exit_loan" name="a_d_principal_exit_loan" value="1"  <?php if($loan_servicing['a_d_principal_exit_loan'] == '1'){ echo 'checked'; }else{ echo 'Checked'; } ?> onclick="broker_capacity_check_box()" > Principal as owner and seller of an existing loan 
									</div>
								</div>
					
								<div class="row">
									<div class="loan_servicing_checkboxex div_full_width">
									  <input type = 'checkbox' id="a_d_arrange_sale_portion" name="a_d_arrange_sale_portion" value="1"  <?php if($loan_servicing['a_d_arrange_sale_portion'] == '1'){ echo 'checked'; } ?> onclick="broker_capacity_check_box()" > Agent and / or principal arranging the sale of a portion of an existing note 
									</div>
								</div>
					
							</div>
					</div>
				<!---- main div (center) close---->
				
				<!---- main div (right side) start---->
					<div class="col-md-2">
							<div class="row">
								<div class="form-group">
								  
								  <div class="col-md-12" style="margin-top:10px;">
									<a class="btn blue" style="width:160px;" data-toggle="modal" href="#assigment">Assignments</a>
								
									<span class="help-block"></span>  
								  </div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
								  
								  <div class="col-md-12" style="margin-top:10px;">
									<a class="btn blue" data-toggle="modal" href="#additional_broker_data" style="width:160px;">Lender Disclosures </a>
								
									<span class="help-block"></span>  
								  </div>
								</div>
							</div>

							<div class="row">
								<div class="form-group">
								  
								  <div class="col-md-12" style="margin-top:10px;">
									<a class="btn blue" data-toggle="modal" href="#assigment_wait_list" onclick="load_ajax_wait_list_data(<?php echo str_replace('/','',$loan_id); ?>)" style="width:160px;">Request List</a>
								
									<span class="help-block"></span>  
								  </div>
								</div>
							</div>

							<div class="row">
								<div class="form-group">
								  <div class="col-md-12" style="margin-top:10px;">
									<a class="btn blue" data-toggle="modal" href="#lender_approval_main" style="width:160px;">Lender Approval</a>
									<span class="help-block"></span>  
								  </div>
								</div>
							</div>

							<div class="row">
								<div class="form-group">
								  <div class="col-md-12" style="margin-top:10px;">
									<a class="btn blue" data-toggle="modal" href="#diligence_documents" style="width:160px;">Diligence Documents</a>
									<span class="help-block"></span>  
								  </div>
								</div>
							</div>

					</div>
				<!---- main div (right side) end---->
					<br>
					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal" onclick="open_custom_popup('assignment_home')">Close</button>
						<input type="hidden" name="action" value="save">
						<button type="submit" class="btn blue">Save</button>
						
					</div>
				
				
			</form>
		</div>	
		
			
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<?php /**********************Diligence Documents**********************/ ?>
<div class="modal fade bs-modal-lg" id="diligence_documents" tabindex="-1" role="large" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<!--<h4 class="modal-title"> Assignments: #<?php// echo $talimar_loan; ?></h4>-->
				<h4 class="modal-title"> Diligence Documents: <?php echo $property_addresss; ?></h4>
			</div>
			<div class="modal-body">
				
				<div id="ct_Documentsadd" class="row">						
					<div class="col-md-2">&nbsp;</div>
					<div class="col-md-8">
						<div class="ct_loan_saved_documenta" style="display: none;">	
							<form action="<?php echo base_url();?>Load_data/capital_diligence_documents" method="POST" enctype="multipart/form-data" id="capital_diligence_documents" name="capital_diligence_documents">
								
								<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
								<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">

			                    <div class="form-group">
									<label for="document_name">Document Name:</label>
									<input type="text" class="form-control" id="document_name" name="document_name" required>
								</div>
								<div class="form-group">
									<div id="ad">
										<label>Select Document:</label>
										<input type="file" id = "borrower_upload_1" name ="contact_upload_doc[]" class="select_images_file" accept=".pdf,.doc,.docx">
									</div>
								</div>

								<button type="submit"  class="btn blue">Save</button>
							</form>
						</div>
					</div>
					<div class="col-md-2">&nbsp;</div>
				</div>

				<div class="col-md-12 ct_Documentsadd_table">						
					<table class="table table-responsive" id="loan_doc_table" style="width: 100% !important;">
						<thead>
							<tr>
								<th>Action</th>
								<th>Name of Document</th>
								<th>Uploaded Date</th>
								<th>User</th>
								<th>View</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$diligence_document = $this->User_model->query("SELECT * FROM capital_diligence_document WHERE talimar_no = '$talimar_loan' ORDER BY id ASC");
							$diligence_document = $diligence_document->result();



							if($diligence_document){
								foreach($diligence_document as $contactDocuments) {

								$FileNameGet = '';
								
								$UploadedDate = date("m-d-Y", strtotime($contactDocuments->created_at));
								
								$filename = $contactDocuments->capital_diligence_document;
								$FileArr = explode('/', $filename);
								$FileNameGet = $FileArr[count($FileArr)-1];

								$file1 = explode('/', $FileNameGet);
								$file2 = explode('.', $file1[count($file1)-1]);

								$FileNameGet = $NameUploadedcalss = $file2[0];

								if($contactDocuments->document_name){
									$NameUploaded = $contactDocuments->document_name;
								}else{
									$NameUploaded = $FileNameGet;
								}


								$up_user_id = $contactDocuments->user_id;
								$UpUserDate = $this->User_model->query("SELECT * FROM user WHERE id = '$up_user_id'")->row();
								if($UpUserDate){
									$UploadedUser = $UpUserDate->fname.' '.$UpUserDate->lname;
								}
							?>
							<tr attrremove="<?php echo $FileNameGet; ?>">
								<td>
									<a><i id="<?php echo $contactDocuments->id; ?>" onclick="delete_DiligenceDocuments(this);" title = "Delete File" class = "fa fa-trash"  aria-hidden="true"></i></a>
								</td>
								<td>
									<?php echo $NameUploaded; ?>
								</td>
								<td><?php echo $UploadedDate; ?></td>
								<td><?php echo $UploadedUser; ?></td>
								<td>
									<a href="<?php echo aws_s3_document_url($filename); ?>" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a>
								</td>
							</tr>
							<?php
								}
							}
							?>

						</tbody>
					</table>
				</div>

			</div>				
		<div class="modal-footer">
			<button type="button" id="ct_loan_saved_documentaddn" class="btn btn-primary">Add Document</button>
			<button type="button" class="btn default" data-dismiss="modal">Close</button>
		</div>
	</div>
</div>
</div>
<?php /**********************Diligence Documents**********************/ ?>

<?php /**********************Lender Approval:**********************/ ?>
<div class="modal fade bs-modal-lg" id="lender_approval_main" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<!--<h4 class="modal-title">Loan Term :#<?php// echo $talimar_loan;?></h4>-->
				<h4 class="modal-title">Lender Approval: <?php echo $property_addresss;?></h4>
			</div>
			 
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">

						<table class="table table-bordered table-striped table-condensed flip-content">
							<thead>
								<tr>
									<th>Approval Requested</th>
									<th>Date Requested</th>
									<th>Date Completed</th>
									<th colspan="2">Approval Status</th>
																				
								</tr>
							</thead>
							<tbody>
								<?php 
									$countapp = 0;
									
									if(isset($LenderAppDAta) && is_array($LenderAppDAta)){
										foreach ($LenderAppDAta as $value) {	

											$countapp++;
											if($value->LP_date_req == '0000-00-00' || $value->LP_date_req == 'null'){
												$datereqVal = '';
											}else{
												$datereqVal = date('m-d-Y', strtotime($value->LP_date_req));
											}

											if($value->LP_date_complete == '0000-00-00' || $value->LP_date_complete == 'null'){
												$datecomplete = '';
											}else{
												$datecomplete = date('m-d-Y', strtotime($value->LP_date_complete));
											}
										?>

										<tr id="row_<?php echo $value->id;?>">
											<td><a class="text-primary" type="button" id="<?php echo $value->id;?>" onclick="MoreLenderApp(this.id)"><?php echo $lender_app_request[$value->LP_request];?></a></td>
											<td><?php echo $datereqVal;?></td>
											<td><?php echo $datecomplete;?></td>
											
											<td><?php echo $lender_app_status[$value->LP_status];?></td>
											<td>
												<!-- <button title="Edit" class="btn btn-primary btn-sm" id="<?php echo $value->id;?>" onclick="MoreLenderApp(this.id)"><i class="fa fa-edit"></i></button> -->
												<button title="Delete" class="btn btn-danger btn-sm" id="<?php echo $value->id;?>" onclick="deleteLenderApp(this.id)"><i class="fa fa-trash"></i></button>
											</td>
											
										</tr>
									<?php } }else{ ?>
											<tr>
												<td colspan="5">No data found!</td>
											</tr>
									<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<th>Total: <?php echo $countapp; ?></th>
									<th colspan="4"></th>
									
								</tr>
							</tfoot>
						</table>

					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn blue pull-left" id="new" onclick="MoreLenderApp(this.id)">Add More</button>
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<?php /**********************Lender Approval:**********************/ ?>

<?php /**********************Request List:**********************/ ?>
<div class="modal fade" id="assigment_wait_list" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Request List: <?php echo $property_addresss; ?></h4>
			</div>

			<form method="POST" action="<?php echo base_url().'form_wait_list'?>">

			<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
			<input type="hidden" name="loan_id" value="<?php echo str_replace('/','',$loan_id); ?>">

			<div class="modal-body">

				 <table class="table table-bordered table-striped table-condensed flip-content" id="table_wait_lists">
				 	<thead>
				 		<tr>
				 			<th>Number</th>
				 			<th style="width:34%;">Contact Name</th>
							<th style="width:15%;">Date Requested</th>
				 			<th style="width:15%;">Status</th>
				 			<th style="width:15%;">Interest Type</th>
				 			<th style="width:15%;">Amount</th>
				 		

				 			
				 			<th></th>
				 		</tr>
				 	</thead>

				 	<tbody>
				 		
				 	</tbody>
				 	  

				 </table>
				 <table class="table table-bordered table-striped table-condensed flip-content d"  style="margin-top:-21px;display:none;">
				 
					<tbody>
				<?php	
				$count = 0;
				$loan_idd=str_replace('/','',$loan_id);
				$fetch_wait_list = $this->User_model->select_where('wait_list',array('loan_id'=>$loan_idd,'talimar_loan'=>$talimar_loan));
				if($fetch_wait_list->num_rows() > 0)
				{
				$fetch_wait_list = $fetch_wait_list->result();
				$wait_list_result = $fetch_wait_list;
				}


				if(isset($wait_list_result))
				{

				$total_amount = 0;
				foreach($wait_list_result as $roww)
				{
				$count++;
				$total_amount +=$roww->amount;

				}}
				?>

				<tr>

				<td><strong>Total: <?php echo $count;?></strong></td>

				<td style="text-align:left;width:30.58%;"><strong>$<?php echo number_format($total_amount,2);?></strong></td>

				</tr>
					</tbody>			 	  

				 </table>				 	  
				 <button type="button" class="btn blue" onclick="add_row_table_wait_lists()"><i class="fa fa-plus" aria-hidden="true"></i> Add</button>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn blue" >Save</button>
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				
			</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<?php /**********************Request List:**********************/ ?>

<?php /**********************Additional Broker Data:**********************/ ?>
<div class="modal fade" id="additional_broker_data" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Additional Broker Data</h4>
				</div>
				<form method="POST" action="<?php echo base_url();?>additional_broker_data">
					<input type="hidden" name="talimar_loan" value="<?= $talimar_loan; ?>">
					<input type="hidden" name="loan_id" value="<?= $loan_id; ?>">
					
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<textarea class="form-control" name="add_broker_data_desc"><?php echo $fetch_loan_result[0]->add_broker_data_desc;?></textarea>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn blue">Save</button>
					</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
<?php /**********************Additional Broker Data:**********************/ ?>



<?php /**********************Lender Approval:**********************/ ?>
<div class="modal fade bs-modal-lg" id="lender_approval_view" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form method="post" action="<?php echo base_url();?>New_loan_data/Assignments_appORdeny">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<!--<h4 class="modal-title">Loan Term :#<?php// echo $talimar_loan;?></h4>-->
					<h4 class="modal-title">Lender Approval: <?php echo $property_addresss;?></h4>
				</div>
				 
				<div class="modal-body">
					<input type="hidden" name="loan_idd" value="/<?php echo $loan_id; ?>">
					<input type="hidden" name="talimar_no" value="<?php echo $talimar_loan; ?>">
					<input type="hidden" name="rowid" value="new"> <!--- for lenders --->
				
					<div class="row">
						<div class="col-md-6 rowapp_mb">
							<label>Approval Status:</label>
							<select class="form-control" name="status">
								<?php foreach($lender_app_status as $key => $row){ ?>
									<option value="<?php echo $key;?>"><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>

						<div class="col-md-6 rowapp_mb">
							<label>Request:</label>
							<select class="form-control" name="request">
								<?php foreach($lender_app_request as $key => $row){ ?>
									<option value="<?php echo $key;?>"><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="row rowapp_mb">
						<div class="col-md-12">
							<label>Description:</label>
							<textarea type="text" name="description" class="form-control" rows="4"></textarea>
						</div>
					</div>
					 <div class="row rowapp_mb">
						<div class="col-md-6">
							<label>Date Requested:</label>
							<input type="text" class="form-control datepicker" name="date_req" value="" required="required">
						</div>

						<div class="col-md-6">
							<label>Date Completed:</label>
							<input type="text" name="date_complete" class="form-control datepicker" value="">
						</div>
					</div> 

					<div class="row rowapp_mb">
						<div class="col-md-12">
							<h4 style="background: #efefef;padding: 10px;"><b>Lender(s)</b></h4>
						</div>
					</div>

					<div class="row rowapp_mb">
						<div class="col-md-12">

							<table class="table table-bordered table-striped table-condensed flip-content">
								<thead>
									<tr>
										<th>Lender Name </th>
										<th>Investment Amount</th>
										<th>% Interest </th>
										<th>Requested</th>
										<th>Approved</th>
										<th>Denied</th>
										<th>No Response</th>
										
									</tr>
								</thead>
								<tbody>
									<?php 
										$countapp = 0;
										if(count($loan_assigment_data) > 0){
											foreach($loan_assigment_data as $row){
									
												$countapp++;
												foreach($investor_all_data as $investor)
												{
													if($investor->id == $row->lender_name){

														$invest_name=$investor->name;
							                              $fci_acc =$investor->fci_acct;
							                              	$i=$investor->id;
													}	
												}

												/*if($row->assignment_appORdeny == 'Requested'){
													$reqcheck = 'checked="checked"';
													$appcheck = 'disabled';
													$denycheck = 'disabled';
													$NoResponse = 'disabled';
												}elseif($row->assignment_appORdeny == 'Approved'){
													$appcheck = 'checked="checked"';
													$reqcheck = 'disabled';
													$denycheck = 'disabled';
													$NoResponse = 'disabled';
												}elseif($row->assignment_appORdeny == 'Deny'){
													$appcheck = 'disabled';
													$reqcheck = 'disabled';
													$denycheck = 'checked="checked"';
													$NoResponse = 'disabled';
												}elseif($row->assignment_appORdeny == 'No Response'){
													$appcheck = 'disabled';
													$reqcheck = 'disabled';
													$denycheck = 'disabled';
													$NoResponse = 'checked="checked"';
												}else{*/
													$reqcheck = '';
													$appcheck = '';
													$denycheck = '';
													$NoResponse = '';
												//}	
									?>
											<input type="hidden" name="lrowid[]" value="<?php echo $row->id;?>">
											<tr>
												<td><a href="#"><?php echo $invest_name;?></a></td>
												<td><?php echo '$'.number_format($row->investment,2);?></td>
												<td><?php echo number_format($row->percent_loan,2);?>%</td>
												<td>
													<input type="checkbox" id="Reques_<?php echo $countapp;?>" onclick="hideRequesbox(this,'<?php echo $countapp;?>')" name="assignment_appORdeny_<?php echo $row->id;?>[]" class="form-control" value="Requested" <?php echo $reqcheck;?>>
												</td>
												<td>
													<input type="checkbox" id="appro_<?php echo $countapp;?>" onclick="hidedenybox(this,'<?php echo $countapp;?>')" name="assignment_appORdeny_<?php echo $row->id;?>[]" class="form-control" value="Approved" <?php echo $appcheck;?>>
												</td>
												<td>
													<input type="checkbox" id="deny_<?php echo $countapp;?>" onclick="hideapprobox(this,'<?php echo $countapp;?>')" name="assignment_appORdeny_<?php echo $row->id;?>[]" class="form-control" value="Deny" <?php echo $denycheck;?>>
												</td>
												<td>
													<input type="checkbox" id="nores_<?php echo $countapp;?>" onclick="hideapproordenybox(this,'<?php echo $countapp;?>')" name="assignment_appORdeny_<?php echo $row->id;?>[]" class="form-control" value="No Response" <?php echo $NoResponse;?>>
												</td>
											</tr>
										<?php } }else{ ?>
												<tr>
													<td colspan="6">No data found!</td>
												</tr>
										<?php } ?>
								</tbody>
								<tfoot>
									<tr>
										<th>Total: <?php echo $countapp; ?></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</tfoot>
							</table>

						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
					<button type="submit" name="save" class="btn blue">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
<?php /**********************Lender Approval:**********************/ ?>

<?php /**********************Assignments::**********************/ ?>
<div class="modal fade" id="assigment" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<!--<h4 class="modal-title"> Assignments: #<?php// echo $talimar_loan; ?></h4>-->
				<h4 class="modal-title"> Assignments: <?php echo $property_addresss; ?></h4>
			</div>
			<div class="modal-body">
			<form id = "frm_assignment_id" method="POST" action="<?php echo base_url();?>loan_assigment">
					
							<!-----------Hidden fields----------->
							<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
							<input type="hidden" name="loan_id" value="/<?php echo $loan_id; ?>">
							<!-----------End of hidden Fields---->
				<div class="portlet-body rc_class table-scrollable">
					<table  id="table_assigment" class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
						<thead>
							<tr>
								<th style="width:10%;">&nbsp;</th>
								<th style="width:5%;">Lender Name</th>
								<th style="width:13%;">Contact</th>
								<th style="width:13%;">Phone</th>
								<th style="width:13%;">Email</th>
								<th style="width:6%;">Investment $ <br><small>(Initial)</small></th>
								<th style="width:7%;">Investment $ <br><small>(Total)</small></th>
								<th style="width:7%;">% of Loan <br><small>(Total)</small></th>
								<th style="width:7%;">Lender Rate</th>
								<th style="width:7%;">Net<br> Payment <br><small>(Initial)</small></th>
								<th style="width:7%;">Net<br> Payment <br><small>(Total)</small></th>
								<!-- <th style="width:5%;">Sum</th> -->
								<th style="width:5%;">Status</th>

								
								
							</tr>
						</thead>
						
						<tbody>
						<?php
						$sql = "SELECT * FROM `loan_assigment` WHERE `talimar_loan` = '" . $talimar_loan . "' ORDER BY `position` ASC";
						$loan_assigmentdata = $this->User_model->query($sql);
						$loan_assigmentdata = $loan_assigmentdata->result();

						$payment = 0;
						$paymenttotal = 0;
						$totalInvestmentSum=0;
						if(count($loan_assigmentdata) > 0)
						{
							foreach($loan_assigmentdata as $row)
							{
								 

								$sqlquery = "SELECT c.contact_firstname, c.contact_lastname, c.contact_email, c.contact_phone FROM lender_contact as lc JOIN contact as c ON lc.contact_id = c.contact_id WHERE lc.lender_id = '" . $row->lender_name . "' AND lc.contact_primary = '1' ";
								$getdata = $this->User_model->query($sqlquery);
								if($getdata->num_rows() > 0){
									$getdata = $getdata->result();

									$cname = $getdata[0]->contact_firstname.' '.$getdata[0]->contact_lastname;
									$cemail = $getdata[0]->contact_email;
									$cphone = $getdata[0]->contact_phone;
								}else{

									$cname = '';
									$cemail = '';
									$cphone = '';
								}

								$payment += $row->payment;
								$paymenttotal += $row->paymenttotal;
								$totalInvestmentSum=$totalInvestmentSum+$row->investment;
								
							?>
							
							
							<tr>
								<input type="hidden" name="this_id[]" value = '<?php echo $row->id;?>'>
								<td style="width:18%;">

									<?php if($this->session->userdata('user_role') == '2' OR $remove_lender_from_loan == 'yes'){ ?>
										<i  class="assigment_remove fa fa-trash" aria-hidden="true" onclick="delete_assigment(this)"></i>&nbsp;
									<?php } ?>

									<a onclick="go_to_lender(this)"><i class="fa fa-th" aria-hidden="true"></i></a>&nbsp;
									<a data-toggle="modal" href="#assigment_print_data"><i class="fa fa-print" aria-hidden="true" id="<?php echo $row->lender_name;?>" onclick="assigment_print_data(this)" ></i></a>&nbsp;
									<a data-toggle="modal" href="javascript:void(0);" data-toggle="modal" data-target="#savemodal_<?php echo $row->lender_name; ?>" id="<?php echo $row->lender_name;?>"><i class="fa fa-save" aria-hidden="true"></i></a>

									&nbsp;
									<a  href="javascript:void(0);" onclick="lenderPaymentHistory('<?php echo $row->talimar_loan; ?>','<?php echo $row->lender_name;?>')"><i class="fa fa-usd" aria-hidden="true"></i></a>
									<a  href="javascript:void(0);" onclick="loan_lender_payment_model('<?php echo $loan_id;?>','<?php echo $row->talimar_loan; ?>','<?php echo $row->lender_name;?>')"><i class="fa fa-pencil" aria-hidden="true"></i></a>
								</td>
								<td>
									<select id="lender_name" type = "text" name="lender_name[]" class="assigment_investor_select chosen" onchange="assigment_investor_select_new(this)"  style="width:100%;" data-live-search="true" >
									<option value="">Select One</option>
									<?php
									// $lender_rate = $fetch_loan_result[0]->intrest_rate - $servicing_fee;

									foreach($investor_all_data as $investor)
									{	

										?>
										<option value="<?= $investor->id;?>" <?php if($investor->id == $row->lender_name){ echo 'selected'; } ?>><?= $investor->name; ?></option>
										<?php
									}
									?>
									</select>
								</td>
								<td>
									<a href="<?php echo base_url().'viewcontact/'.$email_contact[$row->lender_name][0]['contact_id'];?>"><?php echo $cname; ?></a>
								</td>
								<td>
									<?php echo $cphone; ?>
								</td>

								<td>
									<a href="mailto:<?php echo $cemail; ?>"><?php echo $cemail; ?></a>
								</td>

								
								<td>
									<input class=" number_only assautosum_investment" type = "text" name="investment[]"  value="<?= '$'.number_format($row->investment);?>" id="investment" onchange="autocal_investment(this)">
								</td>
								<td>
									<?php if($row->investmenttotal == '' || $row->investmenttotal == '0'){ 
										$totalamountinvest = $row->investment;
									}else{ $totalamountinvest = $row->investmenttotal; }?>
									<input class=" number_only assautosum_investment" type = "text" name="investmenttotal[]"  value="<?= '$'.number_format($totalamountinvest);?>" id="investment" onchange="autocal_investmenttotal(this);">
								</td>
								<td>
									<input class="rate_format number_only" type = "text" name="percent_loan[]" id="percent_loan" value="<?= number_format($row->percent_loan,8).'%';?>" style="width:110px;" readonly>
								</td>
								<?php
								$sub_servicing_fee_check = $loan_servicing['minimum_servicing_fee'];
								$check_assignment_val = ($row->investment * $new_val/100)/12;

								if($check_assignment_val < $sub_servicing_fee_check){
									$add_class_css = 'color:red';
								}else{
									$add_class_css = '';
								}
								?>

								<?php
						
								if($loan_servicing['servicing_lender_rate'] == 'NaN' || $loan_servicing['servicing_lender_rate'] == '' || $loan_servicing['servicing_lender_rate'] == '0.00'){

								$lender_ratessss = $row->invester_yield_percent;
								}else{

								    $lender_ratessss = $loan_servicing['servicing_lender_rate'];
								}

										?>
								<td>		<!-- $lender_rate = $fetch_loan_result[0]->intrest_rate - $servicing_fee; -->
									<input type = "text" name="invester_yield_percent[]" id="invester_yield_percent" value="<?= number_format($lender_ratessss,3).'%';?>" style="border: 1px solid #fff !important;<?php echo $add_class_css;?>" readonly>
						 		</td>
								<td>
									<div class="row_td_print">
									<input type = "text" name="payment[]" id="payment" value="<?= '$'.number_format($row->payment,2);?>" class="" style="width:100px;border: 1px solid #fff !important;<?php echo $add_class_css;?>" readonly>
									<!--<a data-toggle="modal" href="#assigment_print_data"><i class="fa fa-print" aria-hidden="true" onclick = "assigment_print_data(this)" ></i></a>-->
									</div> 
								</td>
								<td>
									<div class="row_td_print">
									<input type = "text" name="paymenttotal[]" id="paymenttotal" value="<?= '$'.number_format($row->paymenttotal,2);?>" class="" style="width:100px;border: 1px solid #fff !important;" readonly>
									</div> 
								</td>
								
								
								<?php 
								      if($row->nfiles_status=='1'){
										$class='black_color';
								      	$text='Complete';
								      }else{

								     	$text='Processing';
								     	$class='red_color';
								      }
								      	   ?>
								      
								      
									<td style="padding-left:25px;text-align:center;"><p class="<?php echo $class;?>">&nbsp;<?php echo $text;?></p></td>
								
							</tr>
							<script type="text/javascript">
								
								$(document).ready(function(){

									var investment = '<?php echo number_format($row->investment);?>';
									var totalamountinvest = '<?php echo number_format($totalamountinvest);?>';
									autocal_investment(investment);
									autocal_investmenttotal(totalamountinvest);
									update_all_assignments_record('<?php echo $loan_id;?>','<?php echo $row->lender_name;?>');

								});
							</script>
							
							<?php
							}
						}

							

						else
						{
						?>
						
							<tr>
							<input type="hidden" name="this_id[]" value = ''>
								<td style="width:18%;">
									<i  class="assigment_remove fa fa-trash" aria-hidden="true"></i>&nbsp;
									<a onclick="go_to_lender(this)"><i class="fa fa-th" aria-hidden="true"></i></a>&nbsp;
									<a data-toggle="modal" href="#assigment_print_data"><i class="fa fa-print" aria-hidden="true"></i></a>&nbsp;
									<a data-toggle="modal" href="#assigment_doc_save"><i class="fa fa-save" aria-hidden="true"></i></a>

								</td>
								<td style="width:5%;">
									<select id="lender_name" type = "text" name="lender_name[]" class="assigment_investor_select chosen"  onchange="assigment_investor_select_new(this)"  data-live-search="true">
									<option value="">Select One</option>
									<?php
									foreach($investor_all_data as $investor)
									{
										?>
										<option value="<?= $investor->id;?>"><?= $investor->name; ?></option>
										<?php
									}
									?>
									</select>
								</td>
								<td></td>
								<td></td>
								<td></td>
								<td style="width:13%;">
									<input type = "text" name="investment[]" id="investment" class="number_only  assautosum_investment" onchange="autocal_investment(this)" >
								</td>
								<td style="width:13%;">
									<input type = "text" name="investmenttotal[]" id="investment" class="number_only  assautosum_investment" onchange="autocal_investmenttotal(this);">
								</td>

								<td style="width:9%;">
									<input type = "text" name="percent_loan[]" id="percent_loan" class="readonly_gray" readonly>
								</td>
								<td style="width:9%;">
									<input type = "text" name="invester_yield_percent[]" id="invester_yield_percent" class="readonly_gray" readonly >
								</td>
								<td style="width:8%;"><div class="row_td_print">
									<input type = "text" name="payment[]" id="payment" class="readonly_gray" readonly >
									<!--<a data-toggle="modal" href="#assigment_print_data"><i class="fa fa-print" aria-hidden="true"></i></a>-->
									</div>
								</td>
								<td style="width:8%;"><div class="row_td_print">
									<input type = "text" name="paymenttotal[]" id="paymenttotal" class="readonly_gray" readonly >
									<!--<a data-toggle="modal" href="#assigment_print_data"><i class="fa fa-print" aria-hidden="true"></i></a>-->
									</div>
								</td>
								
								
								<td></td>
								
								
							</tr>
				<?php 	} 	?>
						</tbody>
						<tfoot>
							
							
							<tr class="footer_row">
								<th></th>
								<th>Total Lenders:</th>
								
								<th><input type="text" id="assigment_total_assigned" value="<?php echo count($loan_assigmentdata);?>" readonly></th>
								<th></th>
								<th></th>
								<th><input type="text" value = "<?php echo  '$'.number_format($totalInvestmentSum);?>" id="assigment_total_investment" readonly></th>
								<th></th>
								<th><input type="text" id="percent_of_loan" readonly></th>
								
								<th id="asgmnt-update-all-btn">
									<!--<a class="btn blue" onclick = "update_all_assignments_record('<?php
									echo $loan_id;?>')" >Update All</a>-->
								</th>
								
								<th>$<?php echo number_format($payment,2);?></th>
								<th>$<?php echo number_format($paymenttotal,2);?></th>
								<th></th>
								
							</tr>
							
							
							<tr class="footer_row">
								<?php
								$avalable=$fetch_loan_result[0]->loan_amount-$totalInvestmentSum;
								?>
								<th></th>
								<th>TaliMar Financial / (Available):</th>
								<th></th>
								<th></th>
								<th></th>
								<th> <input type="text" value="<?php echo  '$'.number_format($avalable);?>" id="assigment_total_available" readonly></th>
								<th></th>
								<th><input type="text" id="percent_of_available_loan" readonly></th>
								<th colspan="4"></th>
							</tr>
							<tr class="footer_row">
								<th></th>
								<th>Loan Amount:</th>
								<th></th>
								<th></th>
								<th></th>
								<th><input type="text" value="<?php echo '$'.number_format($fetch_loan_result[0]->loan_amount)?>" readonly></th>
								<th></th>
								<th colspan="5"></th>
							</tr>
							<tr class="footer_row">
								<th colspan="12" style="color:red;">&nbsp;*&nbsp;<small>If Lender Rate / Net Payment is in red, the Lender Investment is less than the Lender Minimum.</small></th>
							</tr>
							<tr class="footer_row">
								<th colspan="12" style="color:red;">&nbsp;**&nbsp;<small>If not fully assigned, TaliMar Financial is added as a Lender.</small></th>
							</tr>
							<tr class="footer_row">
								<th></th>
								<th>
									
								</th>
								<th></th>
								<th></th>
								<th colspan="8"></th>
							</tr>
						</tfoot>
					</table>
					
					
				</div>
				<div class="modal-footer">
					<span><strong>Draft Loan Docs as TaliMar Financial &nbsp;
					<?php
					$draft_talimar_inc_checkbox = '';
					if(isset($fetch_extra_details[0]->draft_talimar_inc))
					{
						if($fetch_extra_details[0]->draft_talimar_inc == 1)
						{
							$draft_talimar_inc_checkbox = 'checked';
						}
					}
					?>
					<input class="footer_checkbox" type="checkbox" name="set_talimar_data" <?php echo $draft_talimar_inc_checkbox; ?>></strong></span>

					<?php if($loan_assigment_data){ ?>
					
						<a class="btn red" href="<?php echo base_url();?>print_lender_pdf/<?php echo $loan_id;?>">Lender Schedule</a>
					
					<?php }else{ ?>
						
						<a class="btn red" title="No Data in Loan Assigment" onclick="no_assignment_data(this);">Lender Schedule</a>
						
					<?php } ?>

					<button type="button" id="assigment_addrow" onclick="fun_assigment_addrow(this)" value="" class="btn blue" >Add More</button>
					
					<a class="btn blue" data-toggle="modal" href="#status">Status</a>

					<!-- <a class="btn blue" href="<?php //echo base_url();?>Load_data/lender_disclosures">Print Lender Disclosures</a> -->

					<a class="btn blue" data-toggle="modal" href="#email_to_lenders"><i class="fa fa-envelope" aria-hidden="true"></i> Email</a>
					
					
					
				<!--	<a class="btn blue" href="<?php// echo base_url();?>print_lender_wire_schedule<?php// echo $loan_id;?>">Incoming Wire Schedule</a>-->
					
					<!--<button type="button" id="assigment_addrow" value="" class="btn blue" >Add</button>-->
					
					<button onclick = "submit_loan_assignment()" class="btn blue">Save</button>
					
					<input type="hidden" id = "assignment_action" name = "action" value = "save" />
					
					<button type="button" onclick = "open_custom_popup('assignment')" class="btn default" >Close</button>
					
					
					<!------- CUSTOM POPUP MODAL STARTS --------------->
					<div id="assignment_confirmOverlay" style = "display:none">
						<div id="assignment_confirmBox">

							
							<p>Do you want to Save Information?

								<a class="btn blue borrower_save_button" href="javascript:void(0);" onclick = "save_info('assignment')">Yes<span></span></a>
								
								<a onclick = "close_custom_popup('assignment')" data-dismiss="modal" class="btn default borrower_save_button">No<span></span></a>
								
							</p>
							
						</div>
					</div>
							
					<!------- CUSTOM POPUP MODAL ENDS --------------->
					
				</div>
			</form>
			
		</div>
			
						<table class="assigment_table table table-bordered table-striped table-condensed flip-content th_text_align_center"  style="display:none;" id="assigment_hidden_table">
							<tr>
							
								<td>
								<input type="hidden" name="this_id[]" value = ''>
									<i class="assigment_remove fa fa-trash" aria-hidden="true"></i>
									<a onclick="go_to_lender(this)"><i class="fa fa-th" aria-hidden="true"></i></a>
									<a data-toggle="modal" href="#assigment_print_data"><i class="fa fa-print" aria-hidden="true"></i></a>
									<a data-toggle="modal" href="#assigment_doc_save"><i class="fa fa-save" aria-hidden="true"></i></a>
								</td>
								<td>
									<select theme="google" id="lender_name"  name="lender_name[]" class="assigment_investor_select" onchange="assigment_investor_select_new(this)" style="width: 100%;" data-search="true">
									<option value="">Select One</option>
									<?php
									foreach($investor_all_data as $investor)
									{
										?>
										<option value="<?= $investor->id;?>"><?= $investor->name; ?></option>
										<?php
									}
									?>
									</select>
								</td>
								<td>
									
								</td>
								<td>
									
								</td>
								<td>
									
								</td>
								<td>
									<input type = "text" name="investment[]" id="investment" value = "" class="number_only  assautosum_investment" onchange="autocal_investment(this)">
								</td>
								<td>
									<input type = "text" name="investmenttotal[]" id="investment" value = "" class="number_only  assautosum_investment" onchange="autocal_investmenttotal(this);">
								</td>
								<td>
									<input type = "text" name="percent_loan[]" id="percent_loan" class="readonly_gray" readonly >
								</td>
								<td>
									<input type = "text" name="invester_yield_percent[]" id="invester_yield_percent" class="readonly_gray" readonly >
								</td>
								<td>
									<div class="row_td_print">
									<input type = "text" name="payment[]" id="payment" class="readonly_gray" readonly >
									<!--<a data-toggle="modal" href="#assigment_print_data"><i class="fa fa-print" aria-hidden="true"></i></a>-->
									</div>
								</td>
								<td>
									<div class="row_td_print">
									<input type = "text" name="paymenttotal[]" id="paymenttotal" class="readonly_gray" readonly >
									<!--<a data-toggle="modal" href="#assigment_print_data"><i class="fa fa-print" aria-hidden="true"></i></a>-->
									</div>
								</td>
								
								<td></td>
									
							</tr>
						</table>
						
			
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<?php /**********************Assignments::**********************/ ?>

<?php /**********************Email::**********************/ ?>
<div class="modal fade bs-modal-lg" id="email_to_lenders" tabindex="-1" role="large" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<!--<h4 class="modal-title">Email: Loan #(<?php //echo $talimar_loan; ?>)</h4>-->
				<h4 class="modal-title">Email: <?php echo $property_addresss; ?></h4>
			</div>
			<form method="POST" action="<?php echo base_url();?>lender_email">
				<!-----------Hidden fields----------->
					<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
					<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
				<!-----------End of hidden Fields---->
			<div class="modal-body">
				 <div class="row bcc_section">
				 To: 
				 <?php
				 // $loan_assigment_data1 = array_unique($loan_assigment_data);
				 //  echo '<pre>';
				 // print_r($loan_assigment_data1);
				 // echo '</pre>'; 
				 ?>
				 <p id="contact_email_list">
				 
				 <?php 
				 
					foreach($loan_assigment_data as $assig)
					{
						foreach($email_contact[$assig->lender_name] as $key => $contact)
						{

							?>
							 <span><?php echo $contact['contact_name'].' (Email:'.$contact['email'].')';?>
								<input type="hidden" name="contact_id[]" value="<?php echo $contact['contact_id'];?>">
								<input type="hidden" class="contact_emailaddress" value="<?php echo $contact['email']; ?>">
								<a onclick="remove_this_select(this)">
								<i class="fa fa-times" aria-hidden="true"></i>
								</a>
							</span>
							<?php
						}
					}
					?>
				
				 
				</p>
				
				 </div>
				 
				 <div class="row btn_addnew_contact">
					<a class="btn blue" href="mailto:?bcc=" onclick="go_email_outlook(this)" id="link_go_email">Go Outlook</a>
				 </div>
				
				 <div class="row btn_addnew_contact">
					<a data-toggle="modal" class="btn blue" href="#add_lender_lists">Add New</a>
				</div>
				
				 <div class="row message_section">
				 <textarea name="message" placeholder="Message"></textarea>
				 </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn blue">Submit</button>
			</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
<!-- /.modal-dialog -->
</div>
<?php /**********************Email::**********************/ ?>

<?php /**********************status::**********************/ ?>
<div class="modal fade" id="status" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-full" style="width:90%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<!--<h4 class="modal-title"> Assignments: #<?php// echo $talimar_loan; ?></h4>-->
				<h4 class="modal-title"> Assignments Status: <?php echo $property_addresss; ?></h4>
			</div>
			<div class="modal-body">
				
			<form id = "frm_assignment_idd" method="POST" action="<?php echo base_url();?>Load_data/loan_assigment_checkbox">
					
							<!-----------Hidden fields----------->
							<input type="hidden" name = "talimar_noo" value = "<?php echo $talimar_loan; ?>">
							<input type="hidden" name="loan_idd" value="/<?php echo $loan_id; ?>">
							<!-----------End of hidden Fields---->
				<div class="portlet-body rc_class">
					<table  id="table_assigment" class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
						<thead>
							<tr>

								<th style="width:18%;">Lender Name</th>
								<th style="width:9%;">Start Date</th>
								<th style="width:9%;">FCI Lender #</th>
								<th style="width:3%;">Investment</th>
								<th style="width:9% ">Security Type</th>
								<th style="width:10% ">Disclosure Status</th>
								<th style="width:9% ">Paid To</th>
								<th style="width:9% ">Funds Status</th>
								<th style="width:10% ">Servicer Status</th>
								<th style="width:7% ">Recording Status</th>
								<th style="width:7% ">File Status</th>
								
							</tr>
						</thead>
						
						<tbody>
						<?php

						if(count($loan_assigment_data) > 0)
						{
							foreach($loan_assigment_data as $row)
							{
									
							?>
						
							<tr>
								<input type="hidden" name="this_idd[]" value = '<?php echo $row->id;?>'>
							
							<td>
							  	<?php 	
									foreach($investor_all_data as $investor)
									{

										if($investor->id == $row->lender_name){

											$invest_name=$investor->name;
				                              $fci_acc =$investor->fci_acct;
				                              	$i=$investor->id;

										}
											
									}	
								?>
									
					            <a href="<?php echo base_url('investor_view/'.$i);?>"><?php echo $invest_name;?></a>
							</td>
							
								<?php if($row->secured_dot == '1'){ ?>

									<td>
										<input type = "text" name="start_date[]"  value="<?= $row->start_date;?>" class="form-control" id="start_date_val_<?php echo $row->id;?>" readonly="readonly">
									</td>

								<?php }else{ ?>

									<td >
										<input type = "text" name="start_date[]"  value="<?= $row->start_date;?>" class="form-control open_datepicker " id="start_date_val_<?php echo $row->id;?>">
									</td>

								<?php } ?>


						
								<td><?php echo $fci_acc;?></td>
                                <td>$<?php echo number_format($row->investment);?></td>



								<td style="text-align:center;">

									<select class="form-control ass" name="nsecurity_type[]" onchange="nsecurity_type(this)">
										<?php foreach($assi_security_type as $key => $new_nse){ 
											if($key=='0'){


												?>

											
                                        <option  class="black" value="<?php echo $key;?>"><?php echo $new_nse;?></option>
												<?php }else{
											?>
											<option value="<?php echo $key;?>" <?php if($key==$row->nsecurity_type){ echo 'selected'; }?>><?php echo $new_nse;?></option>
										<?php }}?>
									</select> 
								</td>

								<td style="text-align:center;">
									<?php 
										  if($row->EnvolopStatus == "completed")
										  {
										  	echo "Signed";

										  }elseif($row->EnvolopStatus == "sent")
										  {
										  		echo "Sent";
										  }
										  else
										  {
										  		echo "Not Ready";
										  }

									?>
								</td>

								<td style="text-align:center;">
									
									<select class="form-control ass" name="paid_to[]">
										<?php foreach($Ass_status_paidto as $key => $payto){ ?>
											<option value="<?php echo $key;?>"  <?php if($row->paid_to == $key){ echo 'selected'; } ?>><?php echo $payto;?></option>
										<?php } ?>
									</select>
								</td>

								<td style="text-align:center;">
									<select class="form-control ass" name="nfunds_received[]">
										<?php foreach($assi_fund_received as $key => $new_nf){ 

											if($key=='0'){


												?>

									 				<option  class="black" value="<?php echo $key;?>"><?php echo $new_nf;?></option>
											<?php }else{  ?>

											?>
											<option value="<?php echo $key;?>"  <?php if($row->nfunds_received == $key){ echo 'selected'; } ?>><?php echo $new_nf;?></option>
										<?php }} ?>
									</select>
								</td>
								<td style="text-align:center;">
									<select class="form-control ass" name="nservicer_status[]">
										<?php foreach($assi_servicer_status as $key => $nser){ 
											if($key=='0'){?>

									 				<option  class="black" value="<?php echo $key;?>"><?php echo $nser;?></option>
											<?php }else{  ?>
											?>
											<option value="<?php echo $key;?>"  <?php if($row->nservicer_status == $key){ echo 'selected'; } ?>><?php echo $nser;?></option>
										<?php } }?>
									</select>
								</td>
								<td style="text-align:center;padding-left: 6px !important;padding-right: 6px !important;">
									<select class="form-control ass" name="recording_status[]">
										<?php foreach($Ass_Recording_Status as $key => $rdata){ ?>
											<option value="<?php echo $key;?>"  <?php if($row->recording_status == $key){ echo 'selected'; } ?>><?php echo $rdata;?></option>
										<?php } ?>
									</select>
								</td>
								<td style="text-align:center;padding-left: 6px !important;padding-right: 6px !important;">
									<select class="form-control ass" name="nfiles_status[]">
										<?php foreach($assi_file_status as $key => $new_nfil){

											if($key=='0'){ ?>

									 		<option class="black" value="<?php echo $key;?>"><?php echo $new_nfil;?></option>
											<?php }else{  ?>	

											<option value="<?php echo $key;?>"  <?php if($row->nfiles_status == $key){ echo 'selected'; } ?>><?php echo $new_nfil;?></option>
										<?php } } ?>
									</select>
								</td>
								


							</tr>
							
							<?php
							}
						}
						else
						{
						?>
						
							<tr>
								<input type="hidden" name="this_idd[]" value = ''>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
								<td style="text-align:center;">
									<select class="form-control" name="nsecurity_type[]">
										<?php foreach($assi_security_type as $key => $new_nse){ ?>
											<option value="<?php echo $key;?>"><?php echo $new_nse;?></option>
										<?php } ?>
									</select>
								</td>
								<td style="text-align:center;">
									
								</td>
								<td style="text-align:center;">
									<select class="form-control ass" name="paid_to[]">
										<?php foreach($Ass_status_paidto as $key => $pdata){ ?>
											<option value="<?php echo $key;?>"><?php echo $pdata;?></option>
										<?php } ?>
									</select>
								</td>
								<td style="text-align:center;">
									<select class="form-control" name="nfunds_received[]">
										<?php foreach($assi_fund_received as $key => $new_nf){ ?>
											<option value="<?php echo $key;?>"><?php echo $new_nf;?></option>
										<?php } ?>
									</select>
								</td>
								<td style="text-align:center;">
									<select class="form-control" name="nservicer_status[]">
										<?php foreach($assi_servicer_status as $key => $nser){ ?>
											<option value="<?php echo $key;?>"><?php echo $nser;?></option>
										<?php } ?>
									</select>
								</td>
								
								<td style="text-align:center;padding-left: 6px !important;padding-right: 6px !important;">
									<select class="form-control ass" name="recording_status[]">
										<?php foreach($Ass_Recording_Status as $key => $rdata){ ?>
											<option value="<?php echo $key;?>"><?php echo $rdata;?></option>
										<?php } ?>
									</select>
								</td>
								<td style="text-align:center;padding-left: 6px !important;padding-right: 6px !important;">
									<select class="form-control" name="nfiles_status[]">
										<?php foreach($assi_file_status as $key => $new_nfil){ ?>
											<option value="<?php echo $key;?>"><?php echo $new_nfil;?></option>
										<?php } ?>
									</select>
								</td>
							</tr>
				<?php 	} 	?>
						</tbody>
					
					</table>
					
					
				</div>
				<div class="modal-footer">
					
					<?php
					$draft_talimar_inc_checkbox = '';
					if(isset($fetch_extra_details[0]->draft_talimar_inc))
					{
						if($fetch_extra_details[0]->draft_talimar_inc == 1)
						{
							$draft_talimar_inc_checkbox = 'checked';
						}
					}
					?>
				
					<button type="button" onclick = "submit_loan_assignmentt()" class="btn blue">Save</button>
					
					<input type="hidden" id = "assignment_action" name = "actionn" value = "savee" />
					
					<button type="button" data-dismiss="modal" class="btn default" >Close</button>
					
					
					<!------- CUSTOM POPUP MODAL STARTS --------------->
					<div id="assignment_confirmOverlay" style = "display:none">
						<div id="assignment_confirmBox">

							
							<p>Do you want to Save Information?

								<a class="btn blue borrower_save_button" href="javascript:void(0);" onclick = "save_info('assignment')">Yes<span></span></a>
								
								<a onclick = "close_custom_popup('assignment')" data-dismiss="modal" class="btn default borrower_save_button">No<span></span></a>
								
							</p>
							
						</div>
					</div>
							
					<!------- CUSTOM POPUP MODAL ENDS --------------->
					
				</div>
			</form>
			
		</div>
			
						<table class="assigment_tablerrrdf table table-bordered table-striped table-condensed flip-content th_text_align_center"  style="display:none" id="assigment_hidden_table">
							<tr>
							
								<td>
								<input type="hidden" name="this_idd[]" value = ''>
									<i class="assigment_remove fa fa-trash" aria-hidden="true"></i>
									<a onclick="go_to_lender(this)"><i class="fa fa-th" aria-hidden="true"></i></a>
									<a data-toggle="modal" href="#assigment_print_data"><i class="fa fa-print" aria-hidden="true"></i></a>
								</td>
								<td>
									<select id="lender_name" type = "text" name="lender_name[]" class="assigment_investor_select" onchange="assigment_investor_select_new(this)" style="width: 100%;" >
									<option value=""></option>
									<?php
									foreach($investor_all_data as $investor)
									{
										?>
										<option value="<?= $investor->id;?>"><?= $investor->name; ?></option>
										<?php
									}
									?>
									</select>
								</td>
								<!--<td>
									<input type = "text" name="lender_account[]" id="lender_account" readonly >
								</td>-->
								<td>
									<input type = "text" name="investment[]" id="investment" value = "" class="number_only  assautosum_investment" onchange="autocal_investment(this)">
								</td>
								<td>
									<input type = "text" name="percent_loan[]" id="percent_loan" class="readonly_gray" readonly >
								</td>
								<td>
									<input type = "text" name="invester_yield_percent[]" id="invester_yield_percent" class="readonly_gray" readonly >
								</td>
								<td>
									<div class="row_td_print">
									<input type = "text" name="payment[]" id="payment" class="readonly_gray" readonly >
									<!--<a data-toggle="modal" href="#assigment_print_data"><i class="fa fa-print" aria-hidden="true"></i></a>-->
									</div>
								</td>
								<td>
									<input type = "text" name="start_date[]" onclick="this_datepicker(this)">
								</td>
								
							</tr>
						</table>
						
			
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<?php /**********************status::**********************/ ?>

<?php /**********************Lender Contacts::**********************/ ?>
<div class="modal fade bs-modal-lg" id="add_lender_lists" tabindex="-1" role="large" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Lender Contacts</h4>
			</div>
			<div class="modal-body rc_class">
				<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
					<thead>
						<tr>
							<th>Contact Name</th>
							<th>Email</th>
							<th>Add</th>
						</tr>
					</thead>
					<tbody>
					<?php if(isset($fetch_all_lender_contact))
					{
						foreach($fetch_all_lender_contact as $row)
						{
						?>
						<tr>
							<td class="contact_name"><?php echo $row->contact_firstname.' '.$row->contact_middlename.' '.$row->contact_lastname;?></td>
							<td class="email"><?php echo $row->contact_email;?></td>
							<td><a class="btn green" onclick="add_lender_new(this)" id="<?php echo $row->id; ?>">Add</td>
						</tr>
						<?php
						}
					}
					?>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
			</div>
		</div>
	<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<?php /**********************Lender Contacts::**********************/ ?>

<?php /**********************Loan::**********************/ ?>
<div class="modal fade bs-modal-lg" id="assigment_print_data" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		<form method="POST" action="<?php echo base_url();?>assigment_print_document" id="assSignNowdoc">
							
				<!-----------Hidden fields----------->
				<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
				<input type="hidden" name="loan_id" value="/<?php echo $loan_id; ?>">
				<!-----------End of hidden Fields---->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<!--<h4 class="modal-title">Loan #:<?php //echo $talimar_loan; ?></h4>-->
				<h4 class="modal-title">Loan: <?php echo $property_addresss; ?></h4>
			</div>
			<div class="modal-body">
				<input type="hidden" id="selected_lender_id_print" name="selected_lender_id_print">
				<div class="print_assigment_top">
					<div class="row">
						<div class="col-sm-6">
							<input type="checkbox" onclick="print_disclouser_fund_at_close_multiple(this)"> Fund at Close (Multi Lender)
						</div>
						<div class="col-sm-6">
							<input type="checkbox" onclick="print_disclouser_fund_at_close_single(this)"> Fund at Close (Single Lender)
						</div>
						
					</div>
					
					<div class="row">
						<div class="col-sm-6">
							<input type="checkbox" onclick="print_disclouser_fund_after_close_multiple(this)"> Fund after Close (Multi Lender)
						</div>
						
						<div class="col-sm-6">
							<input type="checkbox" onclick="print_disclouser_fund_after_close_single(this)"> Fund after Close (Single Lender)
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-6">
							<input type="checkbox" onclick="print_multiple_property(this)"> Multi Property
						</div>
						
					</div>
					
					<div class="row">
						<div class="col-sm-6">
							<input type="checkbox" onclick="assigment_print_selectall(this)" id="asigment_select_all" > Check All
						</div>
						<div class="col-sm-6">
							<input type="checkbox" id="asigment_deselect_all" onclick="assigment_print_deselectall(this)"> Uncheck All
						</div>
					</div>
				</div>
				
				
				<div class="row">
					<div class="assigment_print_document col-sm-4">
						<label>Primary Disclosures</label>
					</div>
					<div class="assigment_print_document col-sm-4">
						<label>Multi-Lender Disclosures</label>
					</div>
					<div class="assigment_print_document col-sm-4">
						<label>Assignment Disclosures</label>
					</div>
				</div>


				<div class="row">
					<div class="assigment_print_document col-sm-4">
						<input type="checkbox"  name="lender_data_sheet" >&nbsp;&nbsp;Lender Data Sheet
					</div>
					<div class="assigment_print_document col-sm-4">
						<input type="checkbox"  name="multiple_lender_disclosure">&nbsp;&nbsp;Multiple Lender Disclosure
					</div>

					<div class="assigment_print_document col-sm-4">
						<input type="checkbox"  name="promissory_note_endorsement">&nbsp;&nbsp;Promissory Note Endorsement
					</div>
					<!--
					<div class="assigment_print_document col-sm-4">
					<input type="checkbox" name="executive_summary">&nbsp;&nbsp;Executive Summary – Fix & Flip
					</div>-->
				</div>

				<div class="row">
					<div class="assigment_print_document col-sm-4">
						<input type="checkbox"  name="lender_trust_deed_escrow">&nbsp;&nbsp;Lender Trust Deed Instructions
					</div>
					<div class="assigment_print_document col-sm-4">
						<input type="checkbox"  name="multiple_lender_disclosure_list">&nbsp;&nbsp;Multiple Lender Disclosure List
					</div>
					<div class="assigment_print_document col-sm-4">
						<input type="checkbox" name="sale_of_existing_note">&nbsp;&nbsp;RE851B—Sale of Existing Note
					</div>
					<!--
					<div class="assigment_print_document col-sm-4">
					<input type="checkbox" name="loan_sevicing_checklist">&nbsp;&nbsp;Loan Servicing Checklist
					</div>-->
				</div>

				<div class="row">
					<div class="assigment_print_document col-sm-4">
						<input type="checkbox"  name="principal_loss_disclosure">&nbsp;&nbsp;Risk of Principal Loss Disclosure
					</div>
					<div class="assigment_print_document col-sm-4">
					</div>
					<div class="assigment_print_document col-sm-4">
					</div>
					<!--
					<div class="assigment_print_document col-sm-4">
					<input type="checkbox" name="leder_data_re870">&nbsp;&nbsp;RE870
					</div>-->
				</div>
				
				
				<div class="row">
					<div class="assigment_print_document col-sm-4">
					<input type="checkbox"  name="arbitration_of_disputes">&nbsp;&nbsp;Arbitration of Disputes
					</div>
					<div class="assigment_print_document col-sm-4">
					</div>
					<div class="assigment_print_document col-sm-4">
					</div>
                     <!--
					<div class="assigment_print_document col-sm-4">
					<input type="checkbox" name="new_fci_loan_servicing">&nbsp;&nbsp;LS-Servicing Setup
					</div>-->
			
				</div>
				
				<div class="row">
					<div class="assigment_print_document col-sm-6">
					<input type="checkbox"  name="broker_statement_security_exemption">&nbsp;&nbsp;Broker Statement of Security Exemption(s)
					</div>

				</div>
				
				<div class="row">
					<div class="assigment_print_document col-sm-4">
					<input type="checkbox"  name="wire_instructions">&nbsp;&nbsp;Wire Instructions 
					</div>
				</div>
				
				<!--<div class="row">
					<div class="assigment_print_document">
					<input type="checkbox"  name="fci_loan_servicing">&nbsp;&nbsp;FCI Loan Servicing Setup Form
					</div>
				</div>-->
				<div class="row">
					<div class="assigment_print_document col-sm-4">
					<input type="checkbox"  name="fci_ach">&nbsp;&nbsp;FCI ACH Form
					</div>
				</div>
				
				<div class="row">
					<div class="assigment_print_document col-sm-4">
					<input type="checkbox"  name="lender_qualification_statement">&nbsp;&nbsp;Lender Qualification Statement
					</div>
				</div>
				
				<div class="row">
					<div class="assigment_print_document col-sm-6">
					<input type="checkbox"  name="non_corcumvent_agreement">&nbsp;&nbsp;Non Disclosure / Circumvent Agreement
					</div>
				</div>
				
				<div class="row">
					<div class="assigment_print_document col-sm-4">
					<input type="checkbox"  name="lender_servicing_agreement">&nbsp;&nbsp;Lender Servicing Agreement
					</div>
				</div>
				
				<div class="row">
					<div class="assigment_print_document col-sm-4">
					<input type="checkbox"  name="waiver_of_appraisal">&nbsp;&nbsp;Waiver of Appraisal
					</div>
				</div>

				<div class="row">
					<div class="assigment_print_document col-sm-4">
					<input type="checkbox" name="loan_organisation">&nbsp;&nbsp;RE851A—Loan Origination
					</div>
				</div>
				
				
				
				<div class="row">
					<div class="assigment_print_document col-sm-4">
					<input type="checkbox" name="re851d_print">&nbsp;&nbsp;RE851D
					</div>
				</div>
				

				<div class="row">
					<div class="assigment_print_document col-sm-4">
						<input type="checkbox" name="new_fci_loan_servicing">&nbsp;&nbsp;FCI Setup Form 
					</div>
						
				</div>

				<div class="row">
					<div class="assigment_print_document col-sm-4" style="padding-right: 0px;">
						<input type="checkbox" name="new_fci_loan_foreclosure">&nbsp;&nbsp; Foreclosure Preventive Alternatives
					</div>
						
				</div>

				<div class="row">
					<div class="assigment_print_document col-sm-4">
						<input type="checkbox" name="new_fci_loan_optional_insurance_services">&nbsp;&nbsp; Optional Insurance Services 
					</div>
						
				</div>

				<div class="row">&nbsp;<br>&nbsp;</div>

				<div class="row">
					<div class="assigment_print_document col-sm-4">
						<label>New Lender</label>
					</div>
					<div class="assigment_print_document col-sm-4">
						<label>Other</label>
					</div>
					<div class="assigment_print_document col-sm-4">
						<label></label>
					</div>
				</div>

				<div class="row">
					<div class="assigment_print_document col-sm-4">
						<input type="checkbox" name="lender_account_setup_form">&nbsp;&nbsp;Lender Account Setup Form 
					</div>
				
					<div class="assigment_print_document col-sm-4">
						<input type="checkbox"  name="trust_deed_advertizement">&nbsp;&nbsp;Trust Deed Advertisement
					</div>
				</div>
				
				<div class="row">
					<div class="assigment_print_document col-sm-4">
					</div>
					<div class="assigment_print_document col-sm-4">
						<input type="checkbox"  name="initiate_foreclosure_action">&nbsp;&nbsp;Initiate Foreclosure Action
					</div>
				</div>
				
				
				<div class="row">
					<div class="assigment_print_document col-sm-4">
					</div>
					<div class="assigment_print_document col-sm-4">
					<input type="checkbox" name="del_toro_authorization">&nbsp;&nbsp;Del Toro Lender Authorization
					</div>
				</div>
				
				<div class="row">
					<div class="assigment_print_document col-sm-4">
					</div>
					<div class="assigment_print_document col-sm-4">
					<input type="checkbox" name="lender_intrest_reimbursement">&nbsp;&nbsp;Lender Interest Reimbursement 
					</div>
				</div>
				<div class="row">
					<div class="assigment_print_document col-sm-4">
					</div>
					<div class="assigment_print_document col-sm-4">
					<input type="checkbox" name="lender_signature_page">&nbsp;&nbsp;Lender Signature
					</div>
				</div>
					<div class="row">
					<div class="assigment_print_document col-sm-4">
					</div>
					<div class="assigment_print_document col-sm-4">
					<input type="checkbox" name="lender_data_codes">&nbsp;&nbsp;Lender Data Codes
					</div>
				</div>
				
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<!-- <button type="submit" class="btn blue">Save</button> -->
				<button type="submit" id="printbtn" class="btn blue" disabled="disabled">Print</button>
				<input type="submit" class="btn red" id="signbtn" name="signNow" value="DocuSign" disabled="disabled">
			</div>
		</form>	
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<?php /**********************Loan::**********************/ ?>

<?php /**********************Lender Document::**********************/ ?>
<?php
if(count($loan_assigment_data) > 0)
{
	foreach($loan_assigment_data as $ron)
	{
?>

<div class="modal fade bs-modal-lg" id="savemodal_<?php echo $ron->lender_name; ?>" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
	<div class="modal-content">
		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Lender Document: <?php echo $property_addresss; ?></h4>
			</div>
			<div class="modal-body">
				<div class="docc_divv">
				<input type="hidden" name="lender_docid" id="selected_lender_doc" value="<?php echo $ron->lender_name; ?>">
				<input type="hidden" name="talimar_loan" value="<?php echo $talimar_loan;?>">
				<input type="hidden" name="loan_id" value="<?php echo $loan_id;?>">
				
				<div class="row">
					<div class="col-md-12">
						
						<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
						  	<thead>
								<tr>
									<th>Document</th>
									<th>Status</th>
									<th>Date Signed</th>
								</tr>
							</thead>
						    <tbody>
							<?php 

							// echo '<pre>';
							// print_r($lenderSignDoc);
							// echo '</pre>';
							if(isset($lenderSignDoc[$ron->lender_name])){	
								$count = 0;
								foreach ($lenderSignDoc[$ron->lender_name] as $key => $value) {



									$documentsUri = $value->documentsUri.'/1';

									
									$count++; ?>

									  <tr class="lender_docsign_<?php echo $value->lender_id;?>">
									    <td><a href="<?php echo base_url('Load_data/downloadPdf?url='.$documentsUri); ?>" >Lender Disclosures</a></td>
									    <td><?php echo ucfirst($value->status);?></td>
									    <td><?php echo date('m-d-Y', strtotime($value->statusChangedDateTime));?></td>
									  </tr>

								<?php } }else{ ?>
										<tr>
											<td colspan="3">No document found!</td>
										</tr>
								<?php }  ?>
							</tbody>
						</table>
					</div>
				</div>

			</div>
			</div>
			<div class="modal-footer">
				<!--<button type="submit" class="btn blue">Save</button>
				<button class="btn btn-default" data-dismiss="modal">Close</button>-->
			</div>
		
	</div>
</div>
</div><?php } } ?>
<?php /**********************Lender Document::**********************/ ?>
<?php /**********************Lender Document::**********************/ ?>
<div class="modal fade bs-modal-lg" id="lenderPaymentHistory" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop='static' data-keyboard='false'>
	<div class="modal-dialog">
		<div class="modal-content">
			
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Lender Document: <?php echo $property_addresss; ?></h4>
				</div>
				<div class="modal-body">
					<div class="docc_divv">
					<input type="hidden" name="lender_docid" id="selected_lender_doc" value="<?php echo $ron->lender_name; ?>">
					<input type="hidden" name="talimar_loan" value="<?php echo $talimar_loan;?>">
					<input type="hidden" name="loan_id" value="<?php echo $loan_id;?>">
					
					<div class="row">
						<div class="col-md-12">
							
							<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center">
							  	<thead>
									<tr>
										<th>Disbursed</th>
										<th>Amount</th>
									</tr>
								</thead>
							    <tbody id="LenderPaymentDetailsHistory">
								
								</tbody>
							</table>
						</div>
					</div>

				</div>
				</div>
				<div class="modal-footer">
					<!-- <button type="submit" class="btn blue">Save</button> -->
					<button class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			
		</div>
	</div>
</div>
<?php /**********************Lender Document::**********************/ ?>
<?php /**********************Loan Lender Payment Model Start::**********************/ ?>
<div id="loan_lender_payment_model"></div>
<?php /**********************Loan Lender Payment Model ENd::**********************/ ?>
