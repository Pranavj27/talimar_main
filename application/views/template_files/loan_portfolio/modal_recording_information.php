<?php if(count($recording_information) > 0 )
	{
		foreach($recording_information as $row)
		{
			$record_recorder 		= $row->recorded;
			$record_option 			= $row->record_option ? $row->record_option : 0;
			$record_recorder_date 	= $row->recorded_date;
			$record_record_number 	= $row->record_number;
			$record_id				= $row->id;
			$PaidtoDate				= $row->PaidtoDate;
		}
	}
	else
	{
			$record_recorder 		= '';
			$record_recorder_date 	= '';
			$record_record_number 	= '';
			$record_id				= '';
			$PaidtoDate				= '';
			$record_option 			= '0';
	}

	?>
<div class="modal fade bs-modal-sm" id="recording_information" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">  
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url();?>form_recording_information" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<!--<h4 class="modal-title"> Recording Info: #<?php // echo $talimar_loan;?></h4>-->
					<h4 class="modal-title"> Recording Info: <?php echo $property_addresss;?></h4>
				</div>
				<div class="modal-body">
						<!-----------Hidden fields----------->
						<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
						<input type="hidden" name="loan_id" value="/<?php echo $loan_id; ?>">
						<!-----------End of hidden Fields---->
					<div class="modal_content_div">
					<!--
						<div class="row">
							<div class="col-md-12">
								<label>Recorded :</label>
								<input type="text"  class="form-control" name='recorded' value="<?= $record_recorder;?>" required />
							</div>
						</div>
						-->
						<div class="row">
							<div class="col-md-12">
								<label>Recorded?</label>
								<select  class="form-control" name='record_option' id="record_option" onchange="recording_data_display(this);">
									<?php
									foreach($recording_no_yes_option as $key => $row)
									{
										?>
										<option value="<?php echo $key; ?>" <?php if($key == $record_option){ echo 'selected'; } ?> ><?php echo $row; ?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
						<div class="row recorded_dependent" >
							<div class="col-md-12">
								<label>Paid to Date:</label>
								<input type="text"  class="form-control open_datepicker" name='PaidtoDate' value="<?= $PaidtoDate ? date('m-d-Y',strtotime($PaidtoDate)) : '';?>" placeholder="MM-DD-YYYY" />
							</div>
						</div>
						<div class="row recorded_dependent" >
							<div class="col-md-12">
								<label>Recording Date:</label>
								<input type="text"  class="form-control open_datepicker" name='recorded_date' value="<?= $record_recorder_date ? date('m-d-Y',strtotime($record_recorder_date)) : '';?>" placeholder="MM-DD-YYYY" />
							</div>
						</div>
						<div class="row recorded_dependent">
							<div class="col-md-12">
								<label>Recording Number:</label>
								<input type="text"  class="form-control" name="record_number" value="<?= $record_record_number;?>"  />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
					<button type="Submit" class="btn blue">Save</button>
					<!-- <a class="btn blue" id='<?= $record_id;?>' onclick="delete_record_information(this.id,'<?php echo $loan_id;?>')">Delete</a> -->
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>