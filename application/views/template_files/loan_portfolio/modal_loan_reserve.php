<div class="modal fade bs-modal-lg" id="test" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
		<form method="POST" action ="<?php echo base_url(); ?>loan_reserve_draws">
			<input type="hidden" name="talimar_loan" value="<?php echo $talimar_loan; ?>">
			<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">
			<?php
			$loan_borrower_idct = '';
			if ($fetch_loan_borrower) {
				if($fetch_loan_borrower->id){
					$loan_borrower_idct = $fetch_loan_borrower->id;
				}
			}
			?>
			<input type="hidden" name="loan_borrower_id" value="<?php echo $loan_borrower_idct; ?>">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Loan Reserve: <?php echo $property_addresss; ?></h4>
				
			</div>

			<div class="modal-body">
				<div class="rc_class">
					<?php
					$lock_loanval = '';
					if($reserve_draws){
						if($reserve_draws[0]->lock_loan_reserve == 1){
							$lock_loanval = 'checked="checked"';
						}else{
							$lock_loanval = '';
						}
					}

						if($this->session->userdata('user_role') == 2){
							$disabledlockrev = '';
						}else{
							$disabledlockrev = 'disabled="disabled"';
						}

						
					?>

					<label>
						<input type="checkbox" name="lock_loan_reserve" class="form-control" value="<?php echo $reserve_draws ? $reserve_draws[0]->lock_loan_reserve : '0';?>" onclick="loanreserveopt(this);" <?php echo $disabledlockrev;?> <?php echo $lock_loanval;?>> Lock loan reserve funds
					</label>
					<script type="text/javascript">
						function loanreserveopt(that){

							if($(that).is(':checked')){

								$('#test input[name="lock_loan_reserve"]').val('1');
								$('#test').find('input[type="text"], textarea, select').prop('readonly', true);

							}else{

								$('#test input[name="lock_loan_reserve"]').val('0');
								$('#test').find('input[type="text"], textarea, select').prop('readonly', false);
							}
						}

						$(document).ready(function(){

							var lock_loan_reserveVal = '<?php echo $reserve_draws ? $reserve_draws[0]->lock_loan_reserve : '0';?>';
							if(lock_loan_reserveVal == 1){
								$('#test').find('input[type="text"], textarea, select').prop('readonly', true);
							}else{
								$('#test').find('input[type="text"], textarea, select').prop('readonly', false);
							}
						});


					</script>
					
				<div class="table-scrollable">
					<table class="table table-bordered table-striped table-condensed flip-content th_text_align_center" id="add_test_new_row">
						<thead>
							<tr>
							    <th id="counting">Draw #</th>
								<th class="draw_width">Draw Description</th>
								<th>Budget</th>
								<th>Drawn</th>
								<th>Remaining</th>

								<th><input type="text" class="form-control datepicker" name="enter_date[]" value="<?php echo $reserve_date ? date('m-d-Y', strtotime($reserve_date[0]->date_1)) : ''; ?>" placeholder="Enter Date"></th>

								<th><input type="text" class="form-control datepicker" name="enter_date[]" value="<?php echo $reserve_date ? date('m-d-Y', strtotime($reserve_date[0]->date_2)) : ''; ?>" placeholder="Enter Date"></th>

								<th><input type="text" class="form-control datepicker" name="enter_date[]" value="<?php echo $reserve_date ? date('m-d-Y', strtotime($reserve_date[0]->date_3)) : ''; ?>" placeholder="Enter Date"></th>

								<th><input type="text" class="form-control datepicker" name="enter_date[]" value="<?php echo $reserve_date ? date('m-d-Y', strtotime($reserve_date[0]->date_4)) : ''; ?>" placeholder="Enter Date"></th>

								<th><input type="text" class="form-control datepicker" name="enter_date[]" value="<?php echo $reserve_date ? date('m-d-Y', strtotime($reserve_date[0]->date_5)) : ''; ?>" placeholder="Enter Date"></th>

								<th><input type="text" class="form-control datepicker" name="enter_date[]" value="<?php echo $reserve_date ? date('m-d-Y', strtotime($reserve_date[0]->date_6)) : ''; ?>" placeholder="Enter Date"></th>

								<th><input type="text" class="form-control datepicker" name="enter_date[]" value="<?php echo $reserve_date ? date('m-d-Y', strtotime($reserve_date[0]->date_7)) : ''; ?>" placeholder="Enter Date"></th>

								<th><input type="text" class="form-control datepicker" name="enter_date[]" value="<?php echo $reserve_date ? date('m-d-Y', strtotime($reserve_date[0]->date_8)) : ''; ?>" placeholder="Enter Date"></th>

								<th><input type="text" class="form-control datepicker" name="enter_date[]" value="<?php echo $reserve_date ? date('m-d-Y', strtotime($reserve_date[0]->date_9)) : ''; ?>" placeholder="Enter Date"></th>

								<th><input type="text" class="form-control datepicker" name="enter_date[]" value="<?php echo $reserve_date ? date('m-d-Y', strtotime($reserve_date[0]->date_10)) : ''; ?>" placeholder="Enter Date"></th>

								<th><input type="text" class="form-control datepicker" name="enter_date[]" value="<?php echo $reserve_date ? date('m-d-Y', strtotime($reserve_date[0]->date_11)) : ''; ?>" placeholder="Enter Date"></th>

								<th><input type="text" class="form-control datepicker" name="enter_date[]" value="<?php echo $reserve_date ? date('m-d-Y', strtotime($reserve_date[0]->date_12)) : ''; ?>" placeholder="Enter Date"></th>

								<th><input type="text" class="form-control datepicker" name="enter_date[]" value="<?php echo $reserve_date ? date('m-d-Y', strtotime($reserve_date[0]->date_13)) : ''; ?>" placeholder="Enter Date"></th>

								<th><input type="text" class="form-control datepicker" name="enter_date[]" value="<?php echo $reserve_date ? date('m-d-Y', strtotime($reserve_date[0]->date_14)) : ''; ?>" placeholder="Enter Date"></th>

								<th><input type="text" class="form-control datepicker" name="enter_date[]" value="<?php echo $reserve_date ? date('m-d-Y', strtotime($reserve_date[0]->date_15)) : ''; ?>" placeholder="Enter Date"></th>
							</tr>
						</thead>
						<tbody class="scrollable">

						<?php
						$total_of_all_remaining = 0;
						$total_of_drawn_date_total = 0;
						$total_budget = 0;
						$i = 1;
						if($reserve_draws){
						foreach ($reserve_draws as $key => $row) {
							$total_budget += $row->budget;

							if(empty($reserve_draws[$key]->date1_amount)){
								$reserve_draws[$key]->date1_amount = 0;
							}
							if(empty($reserve_draws[$key]->date2_amount)){
								$reserve_draws[$key]->date2_amount = 0;
							}
							if(empty($reserve_draws[$key]->date3_amount)){
								$reserve_draws[$key]->date3_amount = 0;
							}
							if(empty($reserve_draws[$key]->date4_amount)){
								$reserve_draws[$key]->date4_amount = 0;
							}
							if(empty($reserve_draws[$key]->date5_amount)){
								$reserve_draws[$key]->date5_amount = 0;
							}
							if(empty($reserve_draws[$key]->date6_amount)){
								$reserve_draws[$key]->date6_amount = 0;
							}
							if(empty($reserve_draws[$key]->date7_amount)){
								$reserve_draws[$key]->date7_amount = 0;
							}
							if(empty($reserve_draws[$key]->date8_amount)){
								$reserve_draws[$key]->date8_amount = 0;
							}
							if(empty($reserve_draws[$key]->date9_amount)){
								$reserve_draws[$key]->date9_amount = 0;
							}
							if(empty($reserve_draws[$key]->date10_amount)){
								$reserve_draws[$key]->date10_amount = 0;
							}
							if(empty($reserve_draws[$key]->date11_amount)){
								$reserve_draws[$key]->date11_amount = 0;
							}
							if(empty($reserve_draws[$key]->date12_amount)){
								$reserve_draws[$key]->date12_amount = 0;
							}
							if(empty($reserve_draws[$key]->date13_amount)){
								$reserve_draws[$key]->date13_amount = 0;
							}
							if(empty($reserve_draws[$key]->date14_amount)){
								$reserve_draws[$key]->date14_amount = 0;
							}
							if(empty($reserve_draws[$key]->date15_amount)){
								$reserve_draws[$key]->date15_amount = 0;
							}

							$remaining111 = $reserve_draws[$key]->budget - ($reserve_draws[$key]->date1_amount + $reserve_draws[$key]->date2_amount + $reserve_draws[$key]->date3_amount + $reserve_draws[$key]->date4_amount + $reserve_draws[$key]->date5_amount + $reserve_draws[$key]->date6_amount + $reserve_draws[$key]->date7_amount + $reserve_draws[$key]->date8_amount + $reserve_draws[$key]->date9_amount + $reserve_draws[$key]->date10_amount + $reserve_draws[$key]->date11_amount + $reserve_draws[$key]->date12_amount + $reserve_draws[$key]->date13_amount + $reserve_draws[$key]->date14_amount + $reserve_draws[$key]->date15_amount);

							$drawn_date_total = $reserve_draws[$key]->date1_amount + $reserve_draws[$key]->date2_amount + $reserve_draws[$key]->date3_amount + $reserve_draws[$key]->date4_amount + $reserve_draws[$key]->date5_amount + $reserve_draws[$key]->date6_amount + $reserve_draws[$key]->date7_amount + $reserve_draws[$key]->date8_amount + $reserve_draws[$key]->date9_amount + $reserve_draws[$key]->date10_amount + $reserve_draws[$key]->date11_amount + $reserve_draws[$key]->date12_amount + $reserve_draws[$key]->date13_amount + $reserve_draws[$key]->date14_amount + $reserve_draws[$key]->date15_amount;

							$total_of_all_remaining += $remaining111;
							$total_of_drawn_date_total += $drawn_date_total;

							?>
							<tr>
								<input type="hidden" name="draws[]" value="<?php echo $row->id; ?>"><!-- Table id -->
								<td id="counting">
								<input type="text" class="form-control" value="<?php echo $i; ?>" readonly>
								</td>

	 							<td class="draw_width"><a style="position:relative;top:42px;"><i id="<?php echo $row->id; ?>" onclick="delete_reserve_draw(this);" title="Delete File" class="fa fa-trash" aria-hidden="true"></i></a><textarea type="text" class="form-control" name="draw_description[]" placeholder="Enter Description" rows="3" style="width: 428px;margin-left: 20px;"><?php echo $row->draws_description; ?></textarea></td>


								<td><input type="text" class="form-control number_only amount_format" name="budget[]" value="<?php echo $row->budget ? '$' . number_format($row->budget, 2) : ''; ?>"></td>

								<th>$<?php echo number_format($drawn_date_total, 2); ?></th>
								<th>$<?php echo number_format($remaining111, 2); ?></th>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_1[]" value="<?php echo $row->date1_amount ? '$' . number_format($row->date1_amount, 2) : ''; ?>"></td>
								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_2[]" value="<?php echo $row->date2_amount ? '$' . number_format($row->date2_amount, 2) : ''; ?>"></td>
								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_3[]" value="<?php echo $row->date3_amount ? '$' . number_format($row->date3_amount, 2) : ''; ?>"></td>
								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_4[]" value="<?php echo $row->date4_amount ? '$' . number_format($row->date4_amount, 2) : ''; ?>"></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_5[]" value="<?php echo $row->date5_amount ? '$' . number_format($row->date5_amount, 2) : ''; ?>"></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_6[]" value="<?php echo $row->date6_amount ? '$' . number_format($row->date6_amount, 2) : ''; ?>"></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_7[]" value="<?php echo $row->date7_amount ? '$' . number_format($row->date7_amount, 2) : ''; ?>"></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_8[]" value="<?php echo $row->date8_amount ? '$' . number_format($row->date8_amount, 2) : ''; ?>"></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_9[]" value="<?php echo $row->date9_amount ? '$' . number_format($row->date9_amount, 2) : ''; ?>"></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_10[]" value="<?php echo $row->date10_amount ? '$' . number_format($row->date10_amount, 2) : ''; ?>"></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_11[]" value="<?php echo $row->date11_amount ? '$' . number_format($row->date11_amount, 2) : ''; ?>"></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_12[]" value="<?php echo $row->date12_amount ? '$' . number_format($row->date12_amount, 2) : ''; ?>"></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_13[]" value="<?php echo $row->date13_amount ? '$' . number_format($row->date13_amount, 2) : ''; ?>"></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_14[]" value="<?php echo $row->date14_amount ? '$' . number_format($row->date14_amount, 2) : ''; ?>"></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_15[]" value="<?php echo $row->date15_amount ? '$' . number_format($row->date15_amount, 2) : ''; ?>"></td>


							</tr>

						<?php $i++;} }?>



						</tbody>
						<tfoot>
							<tr>

								<th colspan="2">Total:</th>

							<input type="hidden" id="tali" value="<?php echo $talimar_loan; ?>">
							<input type="hidden" id="loa" value="<?php echo $loan_id; ?>">


								<th>$<?php echo number_format($total_budget, 2); ?></th>
								<th>$<?php echo number_format($total_of_drawn_date_total, 2); ?></th>
								<th>$<?php echo number_format($total_of_all_remaining, 2); ?></th>

								<th>$<?php echo number_format($total_of_date1, 2); ?></th>
								<th>$<?php echo number_format($total_of_date2, 2); ?></th>
								<th>$<?php echo number_format($total_of_date3, 2); ?></th>
								<th>$<?php echo number_format($total_of_date4, 2); ?></th>
								<th>$<?php echo number_format($total_of_date5, 2); ?></th>
								<th>$<?php echo number_format($total_of_date6, 2); ?></th>
								<th>$<?php echo number_format($total_of_date7, 2); ?></th>
								<th>$<?php echo number_format($total_of_date8, 2); ?></th>
								<th>$<?php echo number_format($total_of_date9, 2); ?></th>
								<th>$<?php echo number_format($total_of_date10, 2); ?></th>
								<th>$<?php echo number_format($total_of_date11, 2); ?></th>
								<th>$<?php echo number_format($total_of_date12, 2); ?></th>
								<th>$<?php echo number_format($total_of_date13, 2); ?></th>
								<th>$<?php echo number_format($total_of_date14, 2); ?></th>
								<th>$<?php echo number_format($total_of_date15, 2); ?></th>
							</tr>

							<tr>

								<th colspan="2" class="draw_width">Draw Requested by Borrower </th>
								<td></td>
								<td></td>
								<td></td>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_request_1(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date1_draw_request == 1) {echo 'Checked';}?>>
									<input type="hidden" id="request_1" name="draw_request[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date1_draw_request : ''; ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_request_2(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date2_draw_request == 1) {echo 'Checked';}?>>
									<input type="hidden" id="request_2" name="draw_request[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date2_draw_request : ''; ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_request_3(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date3_draw_request == 1) {echo 'Checked';}?>>
									<input type="hidden" id="request_3" name="draw_request[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date3_draw_request : ''; ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_request_4(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date4_draw_request == 1) {echo 'Checked';}?>>
									<input type="hidden" id="request_4" name="draw_request[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date4_draw_request : ''; ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_request_5(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date5_draw_request == 1) {echo 'Checked';}?>>
									<input type="hidden" id="request_5" name="draw_request[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date5_draw_request : ''; ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_request_6(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date6_draw_request == 1) {echo 'Checked';}?>>
									<input type="hidden" id="request_6" name="draw_request[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date6_draw_request : ''; ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_request_7(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date7_draw_request == 1) {echo 'Checked';}?>>
									<input type="hidden" id="request_7" name="draw_request[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date7_draw_request : ''; ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_request_8(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date8_draw_request == 1) {echo 'Checked';}?>>
									<input type="hidden" id="request_8" name="draw_request[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date8_draw_request : ''; ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_request_9(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date9_draw_request == 1) {echo 'Checked';}?>>
									<input type="hidden" id="request_9" name="draw_request[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date9_draw_request : ''; ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_request_10(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date10_draw_request == 1) {echo 'Checked';}?>>
									<input type="hidden" id="request_10" name="draw_request[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date10_draw_request : ''; ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_request_11(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date11_draw_request == 1) {echo 'Checked';}?>>
									<input type="hidden" id="request_11" name="draw_request[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date11_draw_request : ''; ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_request_12(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date12_draw_request == 1) {echo 'Checked';}?>>
									<input type="hidden" id="request_12" name="draw_request[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date12_draw_request : ''; ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_request_13(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date13_draw_request == 1) {echo 'Checked';}?>>
									<input type="hidden" id="request_13" name="draw_request[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date13_draw_request : ''; ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_request_14(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date14_draw_request == 1) {echo 'Checked';}?>>
									<input type="hidden" id="request_14" name="draw_request[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date14_draw_request : ''; ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_request_15(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date15_draw_request == 1) {echo 'Checked';}?>>
									<input type="hidden" id="request_15" name="draw_request[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date15_draw_request : ''; ?>">
								</td>
							</tr>

							<!--
								Description : Add "Draw Approved by Loan Servicing" section field	
								Author      : Bitcot
								Created     : 
								Modified    : 07-04-2021
							-->

							<tr>

								<th colspan="2" class="draw_width">Draw Approved by Loan Servicing </th>
								<td></td>
								<td></td>
								<td></td>
								<td>
									<input type="checkbox" class="form-control" onchange="drawLoanServices(this,1)" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date1_request_loan == 1) {echo 'Checked';}?>>
									<input type="hidden" id="requestLoan_1" name="draw_request_loan[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date1_request_loan : ''; ?>" total_amount="<?php echo number_format($total_of_date1, 2); ?>">
								</td>
								<td> 
									<input type="checkbox" class="form-control" onchange="drawLoanServices(this,2)" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date2_request_loan == 1) {echo 'Checked';}?>>
									<input type="hidden" id="requestLoan_2" name="draw_request_loan[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date2_request_loan : ''; ?>"  total_amount="<?php echo number_format($total_of_date2, 2); ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="drawLoanServices(this,3)" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date3_request_loan == 1) {echo 'Checked';}?>>
									<input type="hidden" id="requestLoan_3" name="draw_request_loan[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date3_request_loan : ''; ?>" total_amount="<?php echo number_format($total_of_date3, 2); ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="drawLoanServices(this,4)" <?php if ($reserve_checkbox[0]->date4_request_loan == 1) {echo 'Checked';}?> >
									<input type="hidden" id="requestLoan_4" name="draw_request_loan[]" value="<?php echo $reserve_checkbox[0]->date4_request_loan ? $reserve_checkbox[0]->date4_request_loan : ''; ?>" total_amount="<?php echo number_format($total_of_date4, 2); ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="drawLoanServices(this,5)" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date5_request_loan == 1) {echo 'Checked';}?>>
									<input type="hidden" id="requestLoan_5" name="draw_request_loan[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date5_request_loan : ''; ?>" total_amount="<?php echo number_format($total_of_date5, 2); ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="drawLoanServices(this,6)" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date6_request_loan == 1) {echo 'Checked';}?>>
									<input type="hidden" id="requestLoan_6" name="draw_request_loan[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date6_request_loan : ''; ?>" total_amount="<?php echo number_format($total_of_date6, 2); ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="drawLoanServices(this,7)" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date7_request_loan == 1) {echo 'Checked';}?>>
									<input type="hidden" id="requestLoan_7" name="draw_request_loan[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date7_request_loan : ''; ?>" total_amount="<?php echo number_format($total_of_date7, 2); ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="drawLoanServices(this,8)" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date8_request_loan == 1) {echo 'Checked';}?>>
									<input type="hidden" id="requestLoan_8" name="draw_request_loan[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date8_request_loan : ''; ?>" total_amount="<?php echo number_format($total_of_date8, 2); ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="drawLoanServices(this,9)" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date9_request_loan == 1) {echo 'Checked';}?>>
									<input type="hidden" id="requestLoan_9" name="draw_request_loan[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date9_request_loan : ''; ?>" total_amount="<?php echo number_format($total_of_date9, 2); ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="drawLoanServices(this,10)" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date10_request_loan == 1) {echo 'Checked';}?>>
									<input type="hidden" id="requestLoan_10" name="draw_request_loan[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date10_request_loan : ''; ?>" total_amount="<?php echo number_format($total_of_date10, 2); ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="drawLoanServices(this,11)" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date11_request_loan == 1) {echo 'Checked';}?>>
									<input type="hidden" id="requestLoan_11" name="draw_request_loan[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date11_request_loan : ''; ?>" total_amount="<?php echo number_format($total_of_date11, 2); ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="drawLoanServices(this,12)" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date12_request_loan == 1) {echo 'Checked';}?>>
									<input type="hidden" id="requestLoan_12" name="draw_request_loan[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date12_request_loan : ''; ?>" total_amount="<?php echo number_format($total_of_date12, 2); ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="drawLoanServices(this,13)" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date13_request_loan == 1) {echo 'Checked';}?> >
									<input type="hidden" id="requestLoan_13" name="draw_request_loan[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date13_request_loan : ''; ?>" total_amount="<?php echo number_format($total_of_date13, 2); ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="drawLoanServices(this,14)" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date14_request_loan == 1) {echo 'Checked';}?>>
									<input type="hidden" id="requestLoan_14" name="draw_request_loan[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date14_request_loan : ''; ?>" total_amount="<?php echo number_format($total_of_date14, 2); ?>">
								</td>
								<td>
									<input type="checkbox" class="form-control" onchange="drawLoanServices(this,15)" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date15_request_loan == 1) {echo 'Checked';}?> >
									<input type="hidden" id="requestLoan_15" name="draw_request_loan[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date15_request_loan : ''; ?>"  total_amount="<?php echo number_format($total_of_date15, 2); ?>">
								</td>
							</tr>

							<tr>

								<th colspan="2" class="draw_width">Draw Approved by Admin</th>
								<td></td>
								<td></td>
								<td></td>
								<?php
								$disabled = 'disabled';

								$where['id']=$this->session->userdata('t_user_id');
								$draw_users= $this->User_model->select_where('user',$where);
								$drawUser= $draw_users->result();
								if($drawUser){
									if($drawUser[0]->loan_draws == 'yes'){
										$disabled = '';
									}
								}

								/*if ($this->session->userdata('t_user_id') == '6' && $reserve_checkbox[0]->date1_draw_request == 1) {
									$disabled = '';
								}*/

								?>


								<td>
									<input type="checkbox" class="form-control" onchange="draw_approved_1(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date1_draw_approved == 1) {echo 'Checked';}?> <?php echo $disabled; ?>>
									<input type="hidden" id="approved_1" name="draw_approved[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date1_draw_approved : ''; ?>">
								</td>

								<?php
								$disabled = 'disabled';
								if($drawUser){
									if($drawUser[0]->loan_draws == 'yes'){
										$disabled = '';
									}
								}
								/*if ($this->session->userdata('t_user_id') == '6' && $reserve_checkbox[0]->date2_draw_request == 1) {
									$disabled = '';
								}*/

								?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_approved_2(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date2_draw_approved == 1) {echo 'Checked';}?> <?php echo $disabled; ?>>
									<input type="hidden" id="approved_2" name="draw_approved[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date2_draw_approved : ''; ?>">
								</td>

								<?php
								$disabled = 'disabled';
								if($drawUser){
									if($drawUser[0]->loan_draws == 'yes'){
										$disabled = '';
									}
								}
								/*if ($this->session->userdata('t_user_id') == '6' && $reserve_checkbox[0]->date3_draw_request == 1) {
									$disabled = '';
								}*/

								?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_approved_3(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date3_draw_approved == 1) {echo 'Checked';}?> <?php echo $disabled; ?>>
									<input type="hidden" id="approved_3" name="draw_approved[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date3_draw_approved : ''; ?>">
								</td>
								<?php
								$disabled = 'disabled';
								if($drawUser){
									if($drawUser[0]->loan_draws == 'yes'){
										$disabled = '';
									}
								}
								/*if ($this->session->userdata('t_user_id') == '6' && $reserve_checkbox[0]->date4_draw_request == 1) {
									$disabled = '';
								}*/

								?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_approved_4(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date4_draw_approved == 1) {echo 'Checked';}?> <?php echo $disabled; ?>>
									<input type="hidden" id="approved_4" name="draw_approved[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date4_draw_approved : ''; ?>">
								</td>
								<?php
								$disabled = 'disabled';
								if($drawUser){
									if($drawUser[0]->loan_draws == 'yes'){
										$disabled = '';
									}
								}
								/*if ($this->session->userdata('t_user_id') == '6' && $reserve_checkbox[0]->date5_draw_request == 1) {
									$disabled = '';
								}*/

								?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_approved_5(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date5_draw_approved == 1) {echo 'Checked';}?> <?php echo $disabled; ?>>
									<input type="hidden" id="approved_5" name="draw_approved[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date5_draw_approved : ''; ?>">
								</td>


								<?php
								$disabled = 'disabled';
								if($drawUser){
									if($drawUser[0]->loan_draws == 'yes'){
										$disabled = '';
									}
								}
								/*if ($this->session->userdata('t_user_id') == '6' && $reserve_checkbox[0]->date6_draw_request == 1) {
									$disabled = '';
								}*/

								?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_approved_6(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date6_draw_approved == 1) {echo 'Checked';}?> <?php echo $disabled; ?>>
									<input type="hidden" id="approved_6" name="draw_approved[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date6_draw_approved : ''; ?>">
								</td>
								<?php
								$disabled = 'disabled';
								if($drawUser){
									if($drawUser[0]->loan_draws == 'yes'){
										$disabled = '';
									}
								}
								/*if ($this->session->userdata('t_user_id') == '6' && $reserve_checkbox[0]->date7_draw_request == 1) {
									$disabled = '';
								}*/

								?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_approved_7(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date7_draw_approved == 1) {echo 'Checked';}?> <?php echo $disabled; ?>>
									<input type="hidden" id="approved_7" name="draw_approved[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date7_draw_approved : ''; ?>">
								</td>
								<?php
								$disabled = 'disabled';
								if($drawUser){
									if($drawUser[0]->loan_draws == 'yes'){
										$disabled = '';
									}
								}
								/*if ($this->session->userdata('t_user_id') == '6' && $reserve_checkbox[0]->date8_draw_request == 1) {
									$disabled = '';
								}*/

								?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_approved_8(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date8_draw_approved == 1) {echo 'Checked';}?> <?php echo $disabled; ?>>
									<input type="hidden" id="approved_8" name="draw_approved[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date8_draw_approved : ''; ?>">
								</td>
								<?php
								$disabled = 'disabled';
								if($drawUser){
									if($drawUser[0]->loan_draws == 'yes'){
										$disabled = '';
									}
								}
								/*if ($this->session->userdata('t_user_id') == '6' && $reserve_checkbox[0]->date9_draw_request == 1) {
									$disabled = '';
								}*/

								?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_approved_9(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date9_draw_approved == 1) {echo 'Checked';}?> <?php echo $disabled; ?>>
									<input type="hidden" id="approved_9" name="draw_approved[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date9_draw_approved : ''; ?>">
								</td>
								<?php
								$disabled = 'disabled';
								if($drawUser){
									if($drawUser[0]->loan_draws == 'yes'){
										$disabled = '';
									}
								}
								/*if ($this->session->userdata('t_user_id') == '6' && $reserve_checkbox[0]->date10_draw_request == 1) {
									$disabled = '';
								}*/

								?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_approved_10(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date10_draw_approved == 1) {echo 'Checked';}?> <?php echo $disabled; ?>>
									<input type="hidden" id="approved_10" name="draw_approved[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date10_draw_approved : ''; ?>">
								</td>
								<?php
								$disabled = 'disabled';
								if($drawUser){
									if($drawUser[0]->loan_draws == 'yes'){
										$disabled = '';
									}
								}
								/*if ($this->session->userdata('t_user_id') == '6' && $reserve_checkbox[0]->date11_draw_request == 1) {
									$disabled = '';
								}*/

								?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_approved_11(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date11_draw_approved == 1) {echo 'Checked';}?> <?php echo $disabled; ?>>
									<input type="hidden" id="approved_11" name="draw_approved[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date11_draw_approved : ''; ?>">
								</td>
								<?php
								$disabled = 'disabled';
								if($drawUser){
									if($drawUser[0]->loan_draws == 'yes'){
										$disabled = '';
									}
								}
								/*if ($this->session->userdata('t_user_id') == '6' && $reserve_checkbox[0]->date12_draw_request == 1) {
									$disabled = '';
								}*/

								?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_approved_12(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date12_draw_approved == 1) {echo 'Checked';}?> <?php echo $disabled; ?>>
									<input type="hidden" id="approved_12" name="draw_approved[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date12_draw_approved : ''; ?>">
								</td>
								<?php
								$disabled = 'disabled';
								if($drawUser){
									if($drawUser[0]->loan_draws == 'yes'){
										$disabled = '';
									}
								}
								/*if ($this->session->userdata('t_user_id') == '6' && $reserve_checkbox[0]->date13_draw_request == 1) {
									$disabled = '';
								}*/

								?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_approved_13(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date13_draw_approved == 1) {echo 'Checked';}?> <?php echo $disabled; ?>>
									<input type="hidden" id="approved_13" name="draw_approved[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date13_draw_approved : ''; ?>">
								</td>
								<?php
								$disabled = 'disabled';
								if($drawUser){
									if($drawUser[0]->loan_draws == 'yes'){
										$disabled = '';
									}
								}
								/*if ($this->session->userdata('t_user_id') == '6' && $reserve_checkbox[0]->date14_draw_request == 1) {
									$disabled = '';
								}*/

								?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_approved_14(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date14_draw_approved == 1) {echo 'Checked';}?> <?php echo $disabled; ?>>
									<input type="hidden" id="approved_14" name="draw_approved[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date14_draw_approved : ''; ?>">
								</td>
								<?php
								$disabled = 'disabled';
								if($drawUser){
									if($drawUser[0]->loan_draws == 'yes'){
										$disabled = '';
									}
								}
								/*if ($this->session->userdata('t_user_id') == '6' && $reserve_checkbox[0]->date15_draw_request == 1) {
									$disabled = '';
								}*/

								?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_approved_15(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date15_draw_approved == 1) {echo 'Checked';}?> <?php echo $disabled; ?>>
									<input type="hidden" id="approved_15" name="draw_approved[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date15_draw_approved : ''; ?>">
								</td>
							</tr>

							<tr>

								<th colspan="2" class="draw_width">Draw Submitted to Servicer</th>
								<td></td>
								<td></td>
								<td></td>

								<?php if ($reserve_checkbox[0]->date1_draw_approved == 1) {?>
								<td>
									<input type="checkbox" onchange="draw_submit_1(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date1_draw_submit == 1) {echo 'Checked';}?>>
									<input type="hidden" name="draw_submit[]" id="submit_servicer_1" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date1_draw_submit : ''; ?>">
								</td>

								<?php } else {?>

								<td>
									<input type="checkbox" onchange="draw_submit_1(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date1_draw_submit == 1) {echo 'Checked';}?> disabled="disabled">
									<input type="hidden" name="draw_submit[]" id="submit_servicer_1" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date1_draw_submit : ''; ?>">
								</td>

								<?php }?>
								<?php if ($reserve_checkbox && $reserve_checkbox[0]->date2_draw_approved == 1) {?>
								<td>
									<input type="checkbox" onchange="draw_submit_2(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date2_draw_submit == 1) {echo 'Checked';}?>>
									<input type="hidden" name="draw_submit[]" id="submit_servicer_2" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date2_draw_submit : ''; ?>">
								</td>
								<?php } else {?>

								<td>
									<input type="checkbox" onchange="draw_submit_2(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date2_draw_submit == 1) {echo 'Checked';}?> disabled="disabled">
									<input type="hidden" name="draw_submit[]" id="submit_servicer_2" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date2_draw_submit : ''; ?>">
								</td>

								<?php }?>

								<?php if ($reserve_checkbox && $reserve_checkbox[0]->date3_draw_approved == 1) {?>
								<td>
									<input type="checkbox" onchange="draw_submit_3(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date3_draw_submit == 1) {echo 'Checked';}?>>
									<input type="hidden" name="draw_submit[]" id="submit_servicer_3" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date3_draw_submit : ''; ?>">
								</td>

								<?php } else {?>

									<td>
										<input type="checkbox" onchange="draw_submit_3(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date3_draw_submit == 1) {echo 'Checked';}?> disabled="disabled">
										<input type="hidden" name="draw_submit[]" id="submit_servicer_3" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date3_draw_submit : ''; ?>">
									</td>

								<?php }?>
								<?php if ($reserve_checkbox && $reserve_checkbox[0]->date4_draw_approved == 1) {?>
									<td>
										<input type="checkbox" onchange="draw_submit_4(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date4_draw_submit == 1) {echo 'Checked';}?>>
										<input type="hidden" name="draw_submit[]" id="submit_servicer_4" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date4_draw_submit : ''; ?>">
									</td>
								<?php } else {?>

									<td>
										<input type="checkbox" onchange="draw_submit_4(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date4_draw_submit == 1) {echo 'Checked';}?> disabled="disabled">
										<input type="hidden" name="draw_submit[]" id="submit_servicer_4" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date4_draw_submit : ''; ?>">
									</td>

								<?php }?>
								<?php if ($reserve_checkbox[0]->date5_draw_approved == 1) {?>
									<td>
										<input type="checkbox" onchange="draw_submit_5(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date5_draw_submit == 1) {echo 'Checked';}?>>
										<input type="hidden" name="draw_submit[]" id="submit_servicer_5" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date5_draw_submit : ''; ?>">
									</td>

								<?php } else {?>

									<td>
										<input type="checkbox" onchange="draw_submit_5(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date5_draw_submit == 1) {echo 'Checked';}?> disabled="disabled">
										<input type="hidden" name="draw_submit[]" id="submit_servicer_5" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date5_draw_submit : ''; ?>">
									</td>

								<?php }?>

								<?php if ($reserve_checkbox && $reserve_checkbox[0]->date6_draw_approved == 1) {?>
									<td>
										<input type="checkbox" onchange="draw_submit_6(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date6_draw_submit == 1) {echo 'Checked';}?>>
										<input type="hidden" name="draw_submit[]" id="submit_servicer_6" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date6_draw_submit : ''; ?>">
									</td>
								<?php } else {?>

									<td>
										<input type="checkbox" onchange="draw_submit_6(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date6_draw_submit == 1) {echo 'Checked';}?> disabled="disabled">
										<input type="hidden" name="draw_submit[]" id="submit_servicer_6" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date6_draw_submit : ''; ?>">
									</td>

								<?php }?>
								<?php if ($reserve_checkbox && $reserve_checkbox[0]->date7_draw_approved == 1) {?>
									<td>
										<input type="checkbox" onchange="draw_submit_7(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date7_draw_submit == 1) {echo 'Checked';}?>>
										<input type="hidden" name="draw_submit[]" id="submit_servicer_7" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date7_draw_submit : ''; ?>">
									</td>

								<?php } else {?>

									<td>
										<input type="checkbox" onchange="draw_submit_7(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date7_draw_submit == 1) {echo 'Checked';}?> disabled="disabled">
										<input type="hidden" name="draw_submit[]" id="submit_servicer_7" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date7_draw_submit : ''; ?>">
									</td>

								<?php }?>
								<?php if ($reserve_checkbox && $reserve_checkbox[0]->date8_draw_approved == 1) {?>
									<td>
										<input type="checkbox" onchange="draw_submit_8(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date8_draw_submit == 1) {echo 'Checked';}?>>
										<input type="hidden" name="draw_submit[]" id="submit_servicer_8" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date8_draw_submit : ''; ?>">
									</td>
								<?php } else {?>

									<td>
										<input type="checkbox" onchange="draw_submit_8(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date8_draw_submit == 1) {echo 'Checked';}?> disabled="disabled">
										<input type="hidden" name="draw_submit[]" id="submit_servicer_8" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date8_draw_submit : ''; ?>">
									</td>
								<?php }?>

								<?php if ($reserve_checkbox && $reserve_checkbox[0]->date9_draw_approved == 1) {?>
									<td>
										<input type="checkbox" onchange="draw_submit_9(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date9_draw_submit == 1) {echo 'Checked';}?>>
										<input type="hidden" name="draw_submit[]" id="submit_servicer_9" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date9_draw_submit : ''; ?>">
									</td>
								<?php } else {?>

									<td>
										<input type="checkbox" onchange="draw_submit_9(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date9_draw_submit == 1) {echo 'Checked';}?> disabled="disabled">
										<input type="hidden" name="draw_submit[]" id="submit_servicer_9" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date9_draw_submit : ''; ?>">
									</td>

								<?php }?>

								<?php if ($reserve_checkbox && $reserve_checkbox[0]->date10_draw_approved == 1) {?>
									<td>
										<input type="checkbox" onchange="draw_submit_10(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date10_draw_submit == 1) {echo 'Checked';}?>>
										<input type="hidden" name="draw_submit[]" id="submit_servicer_10" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date10_draw_submit : ''; ?>">
									</td>
								<?php } else {?>

									<td>
										<input type="checkbox" onchange="draw_submit_10(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date10_draw_submit == 1) {echo 'Checked';}?> disabled="disabled">
										<input type="hidden" name="draw_submit[]" id="submit_servicer_10" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date10_draw_submit : ''; ?>">
									</td>

								<?php }?>

								<?php if ($reserve_checkbox && $reserve_checkbox[0]->date11_draw_approved == 1) {?>
									<td>
										<input type="checkbox" onchange="draw_submit_11(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date11_draw_submit == 1) {echo 'Checked';}?>>
										<input type="hidden" name="draw_submit[]" id="submit_servicer_11" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date11_draw_submit : ''; ?>">
									</td>

								<?php } else {?>
									<td>
										<input type="checkbox" onchange="draw_submit_11(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date11_draw_submit == 1) {echo 'Checked';}?> disabled="disabled">
										<input type="hidden" name="draw_submit[]" id="submit_servicer_11" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date11_draw_submit : ''; ?>">
									</td>

								<?php }?>

								<?php if ($reserve_checkbox && $reserve_checkbox[0]->date12_draw_approved == 1) {?>

									<td>
										<input type="checkbox" onchange="draw_submit_12(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date12_draw_submit == 1) {echo 'Checked';}?>>
										<input type="hidden" name="draw_submit[]" id="submit_servicer_12" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date12_draw_submit : ''; ?>">
									</td>

								<?php } else {?>

									<td>
										<input type="checkbox" onchange="draw_submit_12(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date12_draw_submit == 1) {echo 'Checked';}?> disabled="disabled">
										<input type="hidden" name="draw_submit[]" id="submit_servicer_12" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date12_draw_submit : ''; ?>">
									</td>

								<?php }?>

								<?php if ($reserve_checkbox && $reserve_checkbox[0]->date13_draw_approved == 1) {?>
									<td>
										<input type="checkbox" onchange="draw_submit_13(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date13_draw_submit == 1) {echo 'Checked';}?>>
										<input type="hidden" name="draw_submit[]" id="submit_servicer_13" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date13_draw_submit : ''; ?>">
									</td>

								<?php } else {?>

									<td>
										<input type="checkbox" onchange="draw_submit_13(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date13_draw_submit == 1) {echo 'Checked';}?> disabled="disabled">
										<input type="hidden" name="draw_submit[]" id="submit_servicer_13" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date13_draw_submit : ''; ?>">
									</td>

								<?php }?>

								<?php if ($reserve_checkbox && $reserve_checkbox[0]->date14_draw_approved == 1) {?>
									<td>
										<input type="checkbox" onchange="draw_submit_14(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date14_draw_submit == 1) {echo 'Checked';}?>>
										<input type="hidden" name="draw_submit[]" id="submit_servicer_14" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date14_draw_submit : ''; ?>">
									</td>
								<?php } else {?>

									<td>
										<input type="checkbox" onchange="draw_submit_14(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date14_draw_submit == 1) {echo 'Checked';}?> disabled="disabled">
										<input type="hidden" name="draw_submit[]" id="submit_servicer_14" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date14_draw_submit : ''; ?>">
									</td>

								<?php }?>

								<?php if ($reserve_checkbox && $reserve_checkbox[0]->date15_draw_approved == 1) {?>
									<td>
										<input type="checkbox" onchange="draw_submit_15(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date15_draw_submit == 1) {echo 'Checked';}?>>
										<input type="hidden" name="draw_submit[]" id="submit_servicer_15" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date15_draw_submit : ''; ?>">
									</td>
								<?php } else {?>

									<td>
										<input type="checkbox" onchange="draw_submit_15(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date15_draw_submit == 1) {echo 'Checked';}?> disabled="disabled">
										<input type="hidden" name="draw_submit[]" id="submit_servicer_15" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date15_draw_submit : ''; ?>">
									</td>

								<?php }?>
							</tr>

							<tr>

								<th colspan="2" class="draw_width">Draw Released to Borrower</th>
								<td></td>
								<td></td>
								<td></td>

							<?php if ($reserve_checkbox && $reserve_checkbox[0]->date1_draw_approved == 1) {?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_1(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date1_draw_released == 1) {echo 'Checked';}?>>
									<input type="hidden" id="released_1" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date1_draw_released : ''; ?>">
								</td>

							<?php } else {?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_1(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date1_draw_released == 1) {echo 'Checked';}?> disabled="disabled">
									<input type="hidden" id="released_1" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date1_draw_released : ''; ?>">
								</td>

							<?php }?>

							<?php if ($reserve_checkbox && $reserve_checkbox[0]->date2_draw_approved == 1) {?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_2(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date2_draw_released == 1) {echo 'Checked';}?>>
									<input type="hidden" id="released_2" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date2_draw_released : ''; ?>">
								</td>

							<?php } else {?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_2(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date2_draw_released == 1) {echo 'Checked';}?> disabled="disabled">
									<input type="hidden" id="released_2" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date2_draw_released : ''; ?>">
								</td>

							<?php }?>

							<?php if ($reserve_checkbox && $reserve_checkbox[0]->date3_draw_approved == 1) {?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_3(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date3_draw_released == 1) {echo 'Checked';}?>>
									<input type="hidden" id="released_3" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date3_draw_released : ''; ?>">
								</td>

							<?php } else {?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_3(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date3_draw_released == 1) {echo 'Checked';}?> disabled="disabled">
									<input type="hidden" id="released_3" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date3_draw_released : ''; ?>">
								</td>

							<?php }?>

							<?php if ($reserve_checkbox && $reserve_checkbox[0]->date4_draw_approved == 1) {?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_4(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date4_draw_released == 1) {echo 'Checked';}?>>
									<input type="hidden" id="released_4" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date4_draw_released : ''; ?>">
								</td>
							<?php } else {?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_4(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date4_draw_released == 1) {echo 'Checked';}?> disabled="disabled">
									<input type="hidden" id="released_4" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date4_draw_released : ''; ?>">
								</td>

							<?php }?>
							<?php if ($reserve_checkbox && $reserve_checkbox[0]->date5_draw_approved == 1) {?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_5(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date5_draw_released == 1) {echo 'Checked';}?>>
									<input type="hidden" id="released_5" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date5_draw_released : ''; ?>">
								</td>

							<?php } else {?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_5(this);" <?php if ($reserve_checkbox[0]->date5_draw_released == 1) {echo 'Checked';}?> disabled="disabled">
									<input type="hidden" id="released_5" name="draw_released[]" value="<?php echo $reserve_checkbox[0]->date5_draw_released ? $reserve_checkbox[0]->date5_draw_released : ''; ?>">
								</td>

							<?php }?>

							<?php if ($reserve_checkbox && $reserve_checkbox[0]->date6_draw_approved == 1) {?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_6(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date6_draw_released == 1) {echo 'Checked';}?>>
									<input type="hidden" id="released_6" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date6_draw_released : ''; ?>">
								</td>

							<?php } else {?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_6(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date6_draw_released == 1) {echo 'Checked';}?> disabled="disabled">
									<input type="hidden" id="released_6" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date6_draw_released : ''; ?>">
								</td>

							<?php }?>

							<?php if ($reserve_checkbox && $reserve_checkbox[0]->date7_draw_approved == 1) {?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_7(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date7_draw_released == 1) {echo 'Checked';}?>>
									<input type="hidden" id="released_7" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date7_draw_released : ''; ?>">
								</td>

							<?php } else {?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_7(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date7_draw_released == 1) {echo 'Checked';}?> disabled="disabled">
									<input type="hidden" id="released_7" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date7_draw_released : ''; ?>">
								</td>
							<?php }?>

							<?php if ($reserve_checkbox && $reserve_checkbox[0]->date8_draw_approved == 1) {?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_8(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date8_draw_released == 1) {echo 'Checked';}?>>
									<input type="hidden" id="released_8" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date8_draw_released : ''; ?>">
								</td>
							<?php } else {?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_8(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date8_draw_released == 1) {echo 'Checked';}?> disabled="disabled">
									<input type="hidden" id="released_8" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date8_draw_released : ''; ?>">
								</td>

							<?php }?>

							<?php if ($reserve_checkbox && $reserve_checkbox[0]->date9_draw_approved == 1) {?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_9(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date9_draw_released == 1) {echo 'Checked';}?> >
									<input type="hidden" id="released_9" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date9_draw_released : ''; ?>">
								</td>

							<?php } else {?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_9(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date9_draw_released == 1) {echo 'Checked';}?> disabled="disabled" >
									<input type="hidden" id="released_9" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date9_draw_released : ''; ?>">
								</td>

							<?php }?>

							<?php if ($reserve_checkbox && $reserve_checkbox[0]->date10_draw_approved == 1) {?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_10(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date10_draw_released == 1) {echo 'Checked';}?>>
									<input type="hidden" id="released_10" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date10_draw_released : ''; ?>">
								</td>

							<?php } else {?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_10(this);" <?php if ($reserve_checkbox[0]->date10_draw_released == 1) {echo 'Checked';}?> disabled="disabled">
									<input type="hidden" id="released_10" name="draw_released[]" value="<?php echo $reserve_checkbox[0]->date10_draw_released ? $reserve_checkbox[0]->date10_draw_released : ''; ?>">
								</td>

							<?php }?>

							<?php if ($reserve_checkbox && $reserve_checkbox[0]->date11_draw_approved == 1) {?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_11(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date11_draw_released == 1) {echo 'Checked';}?>>
									<input type="hidden" id="released_11" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date11_draw_released : ''; ?>">
								</td>
							<?php } else {?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_11(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date11_draw_released == 1) {echo 'Checked';}?> disabled="disabled">
									<input type="hidden" id="released_11" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date11_draw_released : ''; ?>">
								</td>
							<?php }?>

							<?php if ($reserve_checkbox && $reserve_checkbox[0]->date12_draw_approved == 1) {?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_12(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date12_draw_released == 1) {echo 'Checked';}?>>
									<input type="hidden" id="released_12" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date12_draw_released : ''; ?>">
								</td>

							<?php } else {?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_12(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date12_draw_released == 1) {echo 'Checked';}?> disabled="disabled">
									<input type="hidden" id="released_12" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date12_draw_released : ''; ?>">
								</td>

							<?php }?>

							<?php if ($reserve_checkbox && $reserve_checkbox[0]->date13_draw_approved == 1) {?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_13(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date13_draw_released == 1) {echo 'Checked';}?>>
									<input type="hidden" id="released_13" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date13_draw_released : ''; ?>">
								</td>

							<?php } else {?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_13(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date13_draw_released == 1) {echo 'Checked';}?> disabled="disabled">
									<input type="hidden" id="released_13" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date13_draw_released : ''; ?>">
								</td>
							<?php }?>

							<?php if ($reserve_checkbox && $reserve_checkbox[0]->date14_draw_approved == 1) {?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_14(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date14_draw_released == 1) {echo 'Checked';}?>>
									<input type="hidden" id="released_14" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date14_draw_released : ''; ?>">
								</td>

							<?php } else {?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_14(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date14_draw_released == 1) {echo 'Checked';}?> disabled="disabled">
									<input type="hidden" id="released_14" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date14_draw_released : ''; ?>">
								</td>

							<?php }?>

							<?php if ($reserve_checkbox && $reserve_checkbox[0]->date15_draw_approved == 1) {?>
								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_15(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date15_draw_released == 1) {echo 'Checked';}?>>
									<input type="hidden" id="released_15" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date15_draw_released : ''; ?>">
								</td>

							<?php } else {?>

								<td>
									<input type="checkbox" class="form-control" onchange="draw_released_15(this);" <?php if ($reserve_checkbox && $reserve_checkbox[0]->date15_draw_released == 1) {echo 'Checked';}?> disabled="disabled">
									<input type="hidden" id="released_15" name="draw_released[]" value="<?php echo $reserve_checkbox ? $reserve_checkbox[0]->date15_draw_released : ''; ?>">
								</td>

							<?php }?>
							</tr>

						</tfoot>
					</table>
					</div>
					<table id="hidden_table" style="display:none;">
						<tbody>
							<tr id="id">
								<input type="hidden" name="draws[]" value="">
								<td id="counting"></td><td class="draw_width"><textarea type="text" class="form-control" name="draw_description[]" value="" placeholder="Enter Description" rows="3"></textarea></td>

								<td><input type="text" class="form-control number_only amount_format" name="budget[]" value=""></td>

								<th></th>

								<th></th>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_1[]" value=""></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_2[]" value=""></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_3[]" value=""></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_4[]" value=""></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_5[]" value=""></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_6[]" value=""></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_7[]" value=""></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_8[]" value=""></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_9[]" value=""></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_10[]" value=""></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_11[]" value=""></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_12[]" value=""></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_13[]" value=""></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_14[]" value=""></td>

								<td><input type="text" class="form-control number_only amount_format" name="enter_amount_15[]" value=""></td>


							</tr>
						</tbody>
					</table>

					<div class="row">
						<a class="btn blue" id="add_test_row">Add Draw</a>
						<a class="btn blue" data-toggle="modal" href="#add_draw_images">Draw Images</a>
						<!--<a class="btn blue" id="add_disbursement">Add Disbursement</a>-->
					</div>

					<div class="row ecrw_select_div">
						<div class="col-md-3">
						<input type="hidden" name="hide" value="<?php echo $fci; ?>">
						<strong>Fund Control Service Provider:  <a href="<?php echo base_url() . 'vendor_data_view/id/' . $fetch_reserve_extra_field[0]->fund_servicer; ?>"><i class="fa fa-edit"></i></a></strong>
							<select class="form-control" id="fund_servicer" name="fund_servicer" onchange="hide_show(this)" id="draw_escrow_account">
							<?php
								foreach ($all_vendor_data as $key => $row) {

									if ($fetch_reserve_extra_field[0]->fund_servicer == '14') {

										$tha = $fci;
									} else {

										$tha = "";
									}
									?>
																<option value="<?php echo $row->vendor_id; ?>" <?php
									if (isset($fetch_reserve_extra_field[0]->fund_servicer)) {if ($fetch_reserve_extra_field[0]->fund_servicer == $row->vendor_id) {echo 'selected';}} else if ($row->vendor_name == 'FCI Lender Services') {echo 'selected';}?> > <?php echo $row->vendor_name; ?> </option>
																<?php
								}
								?>
							</select>
						</div>
					
						<div class="col-md-3" id="draw_escrow_account_depend">
							<strong>Fund Control Account #: </strong>


							<input class="form-control" id="result" name="fund_account"  value="<?php echo $tha; ?>" readonly>
						</div>

						<div class="col-md-3" id="draw_escrow_account_depend">
							<strong>Cost per Draw:</strong>

							<input class="form-control number_only amount_format" name="draw_cost_per"
							value="<?php echo isset($fetch_reserve_extra_field[0]->draw_cost_per) ? '$' . number_format($fetch_reserve_extra_field[0]->draw_cost_per, 2) : '$55.00'; ?>" readonly>
						</div>
					</div>

					<div class="row draws_row_padding">
						<div class="col-md-3">
						<strong>Borrower Name:</strong>
						<input class="form-control" name="borrower_name"  value="<?php echo isset($fetch_loan_borrower) ? $fetch_loan_borrower->b_name : ''; ?>" readonly = "readonly" ></input>
						</div>
						<div class="col-md-3">
							<strong>Contact Email:</strong>
							<input class="form-control" name="contact_email"  value="<?php echo isset($select_borrower_contact1_email) ? $select_borrower_contact1_email : ''; ?>" readonly = "readonly" ></input>
						</div>
						<div class="col-md-3">
							<strong>Contact Name:</strong>
							<input class="form-control" name="contact_name"  value="<?php echo isset($select_borrower_contact1) ? $select_borrower_contact1 : ''; ?>" readonly = "readonly" ></input>
						</div>
					</div>


				</div>
			</div>
			<div class="modal-footer">
				<a class="btn red" data-toggle="modal" href="#wire_information2">Deposit Account</a>
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn blue">Save</button>


				<input type="hidden" name="actions" value="saved">
			</div>
		</form>
		</div>
	</div>
</div>

<div class="modal fade bs-modal-lg" id="add_draw_images" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				
				<h4 class="modal-title">Images Folders : <?php echo $property_addresss; ?></h4>
			</div>
			<form method="post" action="<?php echo base_url() . 'form_loan_folder'; ?>">
			<div class="modal-body">
			<!----------- Hidden fields  ----------->
			<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
			<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">

			<!----------- End of hidden Fields ---->
				<div class="row" id="add-image-folderss">
				<?php

if (isset($loan_reserve_folders) && $loan_reserve_folders != '') {
	foreach ($loan_reserve_folders as $row) {
		$title = $row->folder_title;
		$id = $row->id;

		?>

						<div class="col-sm-3">
							<div class="folder-main-images">
								<a data-toggle="modal" href="<?php echo '#property-folders-' . $row->id; ?>"><img src="<?php echo base_url() . 'assets/images/folder.png'; ?>"></a>
							</div>
							<div class="folder-title">
								<input type="hidden" value="<?php echo $id; ?>" name="id[]">
								<input type="text" name="title[]" value="<?php echo $title ? $title : 'New Folder'; ?>">
							</div>
						</div>
						<?php

	}

} else {?>
						<div class="col-sm-3">
							<div class="folder-main-images">
								<a data-toggle="modal" href=""><img src="<?php echo base_url() . 'assets/images/folder.png'; ?>"></a>
							</div>
							<div class="folder-title">
								<input type="hidden" value="new" name="id[]">
								<input type="text" name="title[]" value="New Folder">
							</div>
						</div>
			<?php }?>


				</div>
			</div>
			<div class="modal-footer">
				<button class="btn blue" type="button" id="<?php echo $id ? $id : ''; ?>" onclick="add_draw_image_folder(this)">Add Folder</button>
				<button type="submit" class="btn blue">Save</button>
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
			</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<?php
if (isset($loan_reserve_folders) && $loan_reserve_folders != '') {
	foreach ($loan_reserve_folders as $seq => $row_folder) {

		$title1 = $row_folder->folder_title;
		$folder_id = $row_folder->id;

		?>
		<div class="modal fade bs-modal-lg" id="<?php echo 'property-folders-' . $row_folder->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Loan Reserve Images > <?php echo $title1; ?> </h4>
					</div>

					<form action="<?php echo base_url() . 'form_upload_reserve_images'; ?>" method="post" enctype="multipart/form-data">
						<!----------- Hidden fields  ----------->
						<input type="hidden" name = "talimar_no" value = "<?php echo $talimar_loan; ?>">
						<input type="hidden" name="loan_id" value="<?php echo $loan_id; ?>">

						<input type="hidden" name="folder_id" value="<?php echo $folder_id; ?>">
						<!----------- End of hidden Fields  ---->
						<div class="modal-body" >
							<div class="row property_imgs_<?php echo $folder_id; ?> " id="<?php echo 'property-imagesss-' . $folder_id; ?>">
								<?php
if (isset($fetch_loan_res_images[$folder_id])) {
			foreach ($fetch_loan_res_images[$folder_id] as $keyy => $row) {
				?>
										<h4 style="margin-left:40px !important;"></h4>
										<div class="col-sm-3  img-div_<?php echo $keyy; ?>" >
											<div class="property-image-inner-main" >

												<div class="property-image-inner-image">

													<a onclick="click_on_image_buttons(this)">

													<?php
				$property_image_paths = aws_s3_document_url('all_reserve_images/' . $row->talimar_loan . '/' . $folder_id . '/' . $row->image_name);
				$property_image_Erro = base_url() . 'assets/images/no_image.png';

				?>

													<img src="<?php echo $property_image_paths; ?>" class="property-imagesss" onerror="this.onerror=null;this.src='<?php echo $property_image_Erro; ?>';">
													</a>

													<input type="file" name="file[]" class="image_upload_inputs" onchange="image_readURL(this)" style="display:none">
													<input type="hidden" name="id[]" value="<?php echo $row->id; ?>" >
												</div>
												<div class="property-image-delete">
													<a data-id ="<?php echo $row->id; ?>" onclick="delete_images(this)"><i class="fa fa-trash" aria-hidden="true"></i></a>
												
													<a title="View image" target="_blank" href="<?php echo $property_image_paths; ?>" class="btn btn-xs"><i style="font-size: 13px !important;" class="fa fa-eye" aria-hidden="true"></i><a>
												</div>
											</div>
										</div>

								<?php }} else {
			?>
								<div class="col-sm-3  img-div_1">
											<div class="property-image-inner-main" >

												<div class="property-image-inner-image">

													<a onclick="click_on_image_buttons(this)">


													<img src="<?php echo base_url(); ?>assets/images/no_image.png" class="property-imagesss">
													</a>

													<input type="file" name="file[]" class="image_upload_inputs" onchange="image_readURL(this)" style="display:none;">
													<input type="hidden" name="id[]" value="new" >
												</div>
												<div class="property-image-delete"><a data-id ="1" onclick="delete_images(this)"><i class="fa fa-trash" aria-hidden="true"></i></a></div>

											</div>
										</div>

							<?php
}?>
							</div>
						</div>


						<div class="modal-footer">
							<button type="button" class="btn blue" onclick="add_image_sections(<?php echo $folder_id; ?>)">Add</button>
							<button type="submit" class="btn blue">Save</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>

<?php

	}
}

?>