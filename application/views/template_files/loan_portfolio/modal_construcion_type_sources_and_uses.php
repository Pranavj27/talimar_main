<div class="modal fade bs-modal-lg" id="construcion_type_sources_and_uses" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url().'form_loan_source_and_uses';?>">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<!--<h4 class="modal-title">Sources & Uses: #<?php echo $talimar_loan; ?></h4>-->
				<h4 class="modal-title">Loan Distribution: <?php echo $property_addresss; ?></h4>
				<input type="hidden" name="talimar_loan" value="<?= $talimar_loan; ?>">
				<input type="hidden" name="loan_id" value="/<?= $loan_id; ?>">
			</div>
			<div class="modal-body">
				<div class="row">
					<?php 
						if($fetch_loan_source_and_uses[0]->activate_table == 1){
							$checkedtab = 'checked';
							$actable = '1';
							$disthisTable = '';
						}else{
							$checkedtab = '';
							$actable = '0';
							$disthisTable = 'disabled';
						}
					?>
					<input type="checkbox" name="activate_source_and_users_table" onclick="ActivateTabledis(this)" value="<?php echo $actable;?>" <?php echo $checkedtab;?> > <b>Activate Loan Distribution</b>
				</div><br>
				
				<table class="table table-bordered table-striped table-condensed flip-content" id="table_loan_source_and_uses" >
					<thead>
						<tr>
							<th style="width:45%;">Line Items</th>
							<th style="width:15%;">Project Costs</th>
							<th style="width:15%;">Borrower Funds</th>
							<th style="width:15%;">Loan Funds</th>
							<th style="width:10%;">% of Costs</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$total_project_cost 	= 0;
						$total_borrower_funds 	= 0;
						$total_loan_funds 		= 0;
						$total_percostvalue 	= 0;
						
						if(isset($fetch_loan_source_and_uses))
						{
							foreach($fetch_loan_source_and_uses as $key => $row)
							{
								$total_project_cost 	= $total_project_cost + $row->project_cost;
								$total_borrower_funds 	= $total_borrower_funds + $row->borrower_funds;
								$total_loan_funds 		= $total_loan_funds + $row->loan_funds;
								
								$readonly_status = '';
								if($key <= 8)
								{
									$readonly_status = 'readonly';
								}

								$activeTable = $row->activate_table;
								if($activeTable == 1){

									$readonlyActable = '';
								}else{
									$readonlyActable = 'readonly';
								}

								?>
								<tr>
									<td>
										<input type="hidden" name="id[]" value="<?php echo $row->id; ?>">
										<input type="text" name="items[]" value="<?php echo $row->items; ?>" <?php echo $readonly_status; ?> />

									</td>

									<td>
										<input type="text" name="project_cost[]" value="$<?php echo $row->project_cost ? number_format($row->project_cost,2) : 0.00; ?>" class="number_only" onchange="loan_source_and_uses_change(this)" <?php echo $readonlyActable;?>/>
									</td>
									<td>
										<input type="text" name="borrower_funds[]" value="$<?php echo $row->borrower_funds ? number_format($row->borrower_funds,2) : 0.00; ?>"  class="number_only" onchange="loan_source_and_uses_change(this)" readonly />
									</td>
									<td>
										<input type="text" name="loan_funds[]" value="$<?php echo $row->loan_funds ? number_format($row->loan_funds,2) : 0.00; ?>" class="number_only" onchange="loan_source_and_uses_change(this)" <?php echo $readonlyActable;?>/>
									</td>

									<?php

										if($row->project_cost < 1){
											$percostvalue = 0.00;
										}else{
											$percostvalue = ($row->loan_funds / $row->project_cost) * 100;

											//echo $row->loan_funds / $row->project_cost;
											//echo '<br>';
										}

										$total_percostvalue += $percostvalue;

									?>
									<td>
										<span><?php echo number_format($percostvalue,2);?>%</span>
									</td>
								</tr>
								<?php
							}
						}
							
						
						?>	
						<?php if($row->items!='HOA dues'){ ?>
						
					
						<?php }?>
					</tbody>
					<tfoot>
						<tr>
							<th>Total</th>
							<th id="total_project_cost">$<?php echo number_format($total_project_cost,2); ?></th>
							<th id="total_borrower_funds">$<?php echo number_format($total_borrower_funds,2); ?></th>
							<th id="total_loan_funds">$<?php echo number_format($total_loan_funds,2); ?></th>
							<th><?php echo number_format(($total_loan_funds/$total_project_cost)*100,2);?>%</th>
						</tr>
					</tfoot>
				</table>
				
				<table id="table_loan_source_and_uses_hidden" style="display:none;">
					<tr>
						<td>
							<input type="hidden" name="id[]" value="new">
							<input type="text" name="items[]" value="">
						</td>
						<td><input type="text" name="project_cost[]" value="" onchange="loan_source_and_uses_change(this)" class="number_only" /></td>
						<td><input type="text" name="borrower_funds[]" value="" onchange="loan_source_and_uses_change(this)" readonly /></td>
						<td><input type="text" name="loan_funds[]" class="number_only" value="" onchange="loan_source_and_uses_change(this)" /></td>
						<td></td>
					</tr>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn blue" id="add_loan_source_and_uses" <?php echo $disthisTable;?>>Add</button>
				<button type="submit" class="btn blue">Save</button>
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
			</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>