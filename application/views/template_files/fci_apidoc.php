<?php
	$fci_doc_options = $this->config->item('fci_doc_options');
	$occupancy_value_option = $this->config->item('occupancy_value_option');
	$property_modal_property_type = $this->config->item('property_modal_property_type');
	$usa_city_county = $this->config->item('usa_city_county');
?>
<div class="modal fade bs-modal-lg" id="FCI_Documents" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url();?>FCI_API/save_datain_fci">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<!--<h4 class="modal-title">Loan Term :#<?php// echo $talimar_loan;?></h4>-->
					<h4 class="modal-title">Save Documents FCI: <?php echo $property_addresss;?></h4>
				</div>
				 
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<table class="table table-bordered table-striped table-condensed flip-content">
								<thead>
									<tr>
										<th>Document Name</th>
										<th>Date Uploaded</th>
										<th>FCI Access Code</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$count = 0;
									if(isset($fciboardingdata) && is_array($fciboardingdata)){ 
											foreach($fciboardingdata as $fci){ 
											$count++;
										?>

											<tr>
												<td>FCI PUSH API <?php echo $count;?></td>
												<td><?php echo date('m-d-Y', strtotime($fci->datetime));?></td>
												<td class="red"><?php echo $fci->access_code;?></td>
											</tr>

									<?php } }else{ ?>
											<tr>
												<td colspan="3">No data found!</td>
											</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<!----------Loan Info ------------->
						<input type="hidden" name="loan_id" value="<?php echo $loan_id;?>">
						<input type="hidden" name="talimar_loan" value="<?php echo $talimar_loan;?>">
						<input type="hidden" name="position" value="<?php echo $position;?>">
						<input type="hidden" name="lender_id" value="">
						<input type="hidden" name="loan_amount" value="<?php echo $loan_amount;?>">
						<input type="hidden" name="original_balance" value="<?php echo $current_balance;?>">
						<input type="hidden" name="loan_funding_date" value="<?php echo $loan_funding_date;?>">
						<input type="hidden" name="first_payment_date" value="<?php echo $first_payment_date;?>">
						<input type="hidden" name="paid_to_date" value="<?php echo date('m-d-Y');?>">
						<input type="hidden" name="next_due_date" value="<?php echo date('m-d-Y');?>">
						<input type="hidden" name="late_charges" value="<?php echo $late_charges_due;?>">
						<input type="hidden" name="payment" value="<?php echo $first_monyly_payment;?>">
						<input type="hidden" name="payment_impound" value="<?php echo $amount_monthly_payment[0]->amount;?>">
						<input type="hidden" name="payment_frequency" value="1">
						<input type="hidden" name="maturity_date" value="<?php echo $maturity_date;?>">
						<input type="hidden" name="exp_date" value="<?php echo date('m-d-Y');?>">
						<input type="hidden" name="term_month" value="<?php echo $term_month;?>">
						<input type="hidden" name="amortization_type" value="<?php echo $payment_type;?>">
						<input type="hidden" name="rate_type" value="1">
						<input type="hidden" name="note_rate" value="<?php echo str_replace('%', '', $intrest_rate);?>">
						<input type="hidden" name="sold_rate" value="1">
						<!----------Borrower Info ------------->
						<input type="hidden" name="b_full_name" value="<?php echo $fetch_loan_borrower->b_name;?>">
						<input type="hidden" name="b_first_name" value="">
						<input type="hidden" name="b_mid_name" value="">
						<input type="hidden" name="b_last_name" value="">
						<input type="hidden" name="b_street" value="<?php echo $fetch_loan_borrower->b_address;?>">
						<input type="hidden" name="b_city" value="<?php echo $fetch_loan_borrower->b_city;?>">
						<input type="hidden" name="b_state" value="<?php echo $fetch_loan_borrower->b_state;?>">
						<input type="hidden" name="b_zip" value="<?php echo $fetch_loan_borrower->b_zip;?>">
						<!----------Borrower Contact Info ------------->
						<input type="hidden" name="b_home_phone" value="">
						<input type="hidden" name="b_work_phone" value="">
						<input type="hidden" name="b_mobile" value="<?php echo $fetchborrowercontact[0]['contact_phone'];?>">
						<input type="hidden" name="b_fax" value="<?php echo $fetchborrowercontact[0]['contact_fax'];?>">
						<input type="hidden" name="b_tin" value="">
						<input type="hidden" name="b_email" value="<?php echo $fetchborrowercontact[0]['contact_email'];?>">
						<input type="hidden" name="b_contact_name" value="<?php echo $fetchborrowercontact[0]['contact_name'];?>">
						<input type="hidden" name="b_company" value="<?php echo $fetchborrowercontact[0]['company_name_1'];?>">
						<!----------Property Info ------------->
						<input type="hidden" name="pro_description" value="<?php echo $outlook_property[0]->legal_description;?>">
						<input type="hidden" name="property_address" value="<?php echo $outlook_property[0]->property_address;?>">
						<input type="hidden" name="property_city" value="<?php echo $outlook_property[0]->city;?>">
						<input type="hidden" name="property_state" value="<?php echo $outlook_property[0]->state;?>">
						<input type="hidden" name="property_zip" value="<?php echo $outlook_property[0]->zip;?>">
						<input type="hidden" name="pro_country" value="<?php echo $usa_city_county[$outlook_property[0]->country];?>">
						<input type="hidden" name="property_type" value="<?php echo $outlook_property[0]->property_type;?>">
						<input type="hidden" name="occupancy_status" value="<?php echo $outlook_property[0]->occupancy_type;?>">


						<!--<div class="col-md-4">
							<label>Upload Document:</label>
							<input type="file" name="fci_doc" class="form-control">
						</div>
						<div class="col-md-3">
							<label>Document Type:</label>
							<select class="form-control" name="fci_doc_type" >
								<?php foreach($fci_doc_options as $key => $row){ ?>
									<option value="<?php echo $key;?>"><?php echo $row;?></option>
								<?php } ?>
							</select>
						</div>-->
						<div class="col-md-12">
							<small><b>Note:</b> Click on the below PUSH FCI API button to send this loan data to the FCI API.</small>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
					<button type="submit" name="submitFCI" class="btn btn-success">PUSH FCI API</button>
				</div>
			</form>
		</div>
	</div>
</div>