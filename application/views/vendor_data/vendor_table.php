<?php 
//------------values from myConfig---------------
$vendor_types 		= $this->config->item('vendor_type');
asort($vendor_types);

?>
<style>

div.page-title h3 {
    padding: 7px 11px;
    font-weight: 600;
}
.col-md-2#dd {
    margin-left: 116px;
}
.col-md-2#marr {
    margin: -6px -112px 0px;
}

</style>

<div class="page-container contactlist-outmost">
			<!-- BEGIN PAGE HEAD -->
	<div class="container">
		<div class="tab-pane">
				<div class="page-head">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h3>Vendor Data</h3>
					</div>
						
				</div>


			<div class="col-md-2">	
				<label><b>Search Name:</b>&nbsp;</label>
			</div>

				<div class="col-md-2" id="marr">	
					<form method="POST" action="<?php echo base_url();?>vendor_data" id="form_search_vendor_by">
						
						<select onchange="vendor_select_by_id(this.value)" class="selectpicker" data-live-search="true" >
							<option value="">Select One</option>
							<?php
							foreach($all_vendor_data_result as $row)
							{
								if($selected_vender_id == $row->vendor_id){
									$selected = 'selected = "selected" ';
								}else{
									
									$selected = '';
								}
								?>
								<option <?php echo $selected;?> value="<?= $row->vendor_id;?>"  ><?= $row->vendor_name; ?></option>
								<?php
							}
							
							?>
						</select>
						</form>
				</div>


			<div class="col-md-2" id="dd">	
			<label><b>Vendor Type:</b></label>
			</div>

				<div class="col-md-2" id="marr">	
					<form id="vendor_type_options" method="post" action="<?php echo base_url();?>vendor-primary">
				
							<select class="form-control" name ="vendor_type" onchange="select_vendor_type(this.value);">
					     	<option value="">Select All</option>
							<?php 
							foreach($vendor_types as $key => $type){

								if($vendor_type == $key){
									$selected = 'selected = "selected" ';
								}else{
									
									$selected = '';
								}
								
								echo '<option value = '.$key.' '.$selected.'>'.$type.'</option>';
							}
							 
							?>
						</select>
						
					</form>
				</div>
				<div class="col-md-1 pull-right">
					<a href="<?php echo base_url();?>vendor_data/new" class="btn btn-primary">New</a>
				</div>
			<!-- END PAGE HEAD -->
			<div class="page-container">
                <div class="contactlist-res-div">	
				 
                <div class="col-md-12">	
					
					<table id ="table_contactlist" class="table table-responsive table-bordered table-striped table-condensed flip-content table-hover">
							<thead>
								<tr>
									<th style="width:20%;">Vendor Name</th>
									<th style="width:20%;">Vendor Type	</th>
									<th style="width:20%;">Vendor Contact</th>
									<th style="width:20%;">Vendor Phone</th>
									<th style="width:20%;">Vendor Email</th>
							
								</tr>
							</thead>
							<tbody>
								<?php 

							   if(isset($fetch_vendors_data) && is_array($fetch_vendors_data)){
								foreach($fetch_vendors_data as $key=>$row){

								?>
						
									<tr style = "cursor:pointer;" onclick = "redirect_vendor_individual_page(<?php echo $row['vendor_id'];?>)">
										<td><?php echo $row['vendor_name'];?></td>
										<td><?php echo isset($row['vendor_type']) ? $vendor_types[$row['vendor_type']]:'';?></td>
										<td><?php 



	$fetch_contact = $this->User_model->select_star('contact');
		$fetch_contact = $fetch_contact->result();

		
foreach ($fetch_contact as $key => $value) {

			if($value->contact_id==$row['vendor_contact']){


				echo $value->contact_firstname.' '.$value->contact_lastname;
	
 }    
}


										



										?></td>
										<td><?php echo $row['vendor_contact_phone'];?></td>
										<td><?php echo $row['vendor_contact_email'];?></td>
									
									</tr>
								<?php } } ?>
						
							</tbody>
							
					</table>
				</div>
			</div>
		</div>
								<!------------>
	
		</div>
	</div>
</div>


<script>
function vendor_select_by_id(id)
	{
		var search_by = $('#vendor_select_by_id').val();
		
			
		window.location.href = "<?php echo base_url();?>vendor_data_view/id/"+id;
			
		
		
	}
	function select_vendor_type(that)
	{
		//alert('ll');
		$('form#vendor_type_options').submit();
		
	}

	function redirect_vendor_individual_page(val){

		window.location.href = "<?php echo base_url();?>vendor_data_view/id/"+val;
	}

	$(document).ready(function(){
		
		$('#table_contactlist').DataTable({
			"pageLength":25,
			"order": [[ 0, 'asc' ]],
			//"bLengthChange":false,
			//"searching":false,
		});
		$("#table_contactlist_filter").hide();
	});

	
	
	
</script>
