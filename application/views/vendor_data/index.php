<?php
error_reporting(0);

//echo 'ip: '.$this->input->ip_address();


//------------values from myConfig---------------
$vendor_types = $this->config->item('vendor_type');
asort($vendor_types);
$phone_types = $this->config->item('phone_types');
$contact_number_option = $this->config->item('contact_number_option');
$email_types_option = $this->config->item('email_types_option');
$vendor_contact_phone_type = $this->config->item('vendor_contact_phone_type');


?>
<style>

.main-left-div h4 {
    padding: 6px;
}
.main-left2-div h4 {
    padding: 6px;
}


.same_check #uniform-same_mailing_address{
width:7% !important;
float:left;
}
.each_checkbox{
width:100% !important;
margin-bottom: 10px;

}
.each_checkbox .checker{
width: 20px !important;
margin-right:10px !important;
}
.outer_checkboxes{
border: 1px solid;
float: left;
width: 63% !important;
padding: 20px;
margin-left: 15px;
}

.main-left-div{
float:left;
width:100%;
}
.main-left2-div{
float:left;
width: 100%;
margin-top: 40px;
}

.borrower_data_row{
padding: 5px 80px !important;
}
.row.button_section_div {
padding: 20px 65px !important;
}
.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
width: 170px;
}

.chosen-drop {
width: 253px !important;
margin-left: 0px !important;
}

.chosen-container{
display: block !important;
}
.chosen-container.chosen-container-single {
width: 253px !important;
}
.chosen-container-single .chosen-single {
padding: 4px 0px 26px 6px !important;
border-radius: 4px !important;
border: 1px solid #e1e1e1 !important;
}
label {
font-weight: 600 !important;
}
</style>
<div class="page-container">
			<!-- BEGIN PAGE HEAD -->

			<div class="container">
			<!--------------------MESSEGE SHOW-------------------------->
				<?php if ($this->session->flashdata('error') != '') {?>
				 <div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error'); ?></div><?php }?>
				  <?php if ($this->session->flashdata('success') != '') {?>
				 <div id='success'><i class='fa fa-thumbs-o-up'></i><?php echo $this->session->flashdata('success'); ?></div><?php }?>

			<!-------------------END OF MESSEGE-------------------------->
			
			<form class="form-horizontal" method="POST" id="vendor_form" action= "<?php echo base_url(); ?>add_vendor_data">

					<div class="row button_section_div">

						<div class="col-md-6">
							<h4><b style="font-weight: 500;">Vendor Data : <?=isset($vendor->vendor_name) ? $vendor->vendor_name : '';?></b></h4>
						</div>
						<div class="col-md-6">
							
								<?php
									$uri = $_SERVER['REQUEST_URI'];
									$chunks = array_filter(explode('/', $uri));

									$new_class = "style = 'display:block;' ";
									if (isset($chunks[2]) && ($chunks[2] == 'new')) {
										$new_class = "style = 'display:none;' ";

										?>

								<button  style= "" type = "submit" name = "save" class="btn btn-primary borrower_save_button" value="save">Save</button>
								<a  class="btn btn-primary borrower_save_button" href = "<?php echo base_url() . 'vendor-primary'; ?>" >Close</a>

								<?php	} else {
									if (!(isset($vendor->vendor_id)) && ($user_role == 2)) {?>
								&nbsp;

								<a <?php echo $new_class; ?> class="btn btn-primary borrower_save_button" href="<?php echo base_url() . 'vendor_data/new'; ?>" >New</a>

								<?php } } ?>

								<?php if ((isset($vendor->vendor_id)) && ($user_role == 2)) {?>

								<a class="btn btn-danger pull-right borrower_save_button" onclick = "delete_vendor(<?php echo $vendor->vendor_id; ?>)" style="margin-top: 3px;border-radius: 0px;margin-left: 3px;">Delete</a>
								<?php }?>
								<?php if(isset($vendor->vendor_id)) {?>
								<button  style= "" type = "submit" name = "save" class="btn btn-primary borrower_save_button" value="save">Save</button>
								<a  class="btn btn-primary borrower_save_button" href = "<?php echo base_url() . 'vendor-primary'; ?>" >Close</a> <?php }?>

						</div>
						<!-- Button -->
					</div>
			
			<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					 Dashboard
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->

			<?php
// if(isset($all_vendors_data)){
// foreach($all_vendors_data as $vendor){

?>
			<!-- BEGIN PAGE CONTENT INNER -->
				<?php if (($search_by == '2') && ($vendor_option)) {
	?>

				<div class = "all_vendor_roles">
					<ul>
						<?php
if ($roles) {
		foreach ($roles as $v_role) {?>
									<li><a href = "<?php echo base_url() . 'vendor_data/id/' . $v_role->vendor_id; ?>"><?php echo $v_role->vendor_name; ?></a></li>

						<?php	}
	} else {

		echo '<h3>No Vendor Found! Try another Filter or Go to <a href = "' . base_url() . 'vendor_data">New Vendor</a> Form</h3><br>';
	}
	?>
					</ul>
				</div>


			<?php	}if ($search_by == '1' || ($vendor_option == '')) {
	?>
			<!--<form class="form-horizontal" method="POST" id = "vendor_form" action= "<?php echo base_url(); ?>add_vendor_data">
			-->

				<input type="hidden" name = "vendor_id" value="<?php echo isset($vendor->vendor_id) ? $vendor->vendor_id : ''; ?>">
				<div class = "main-left-div">
					<div class="row borrower_data_row">
						<h4>Vendor Data</h4>
						<div class="col-md-3">
							<label>Vendor Name:</label>
							<input type="hidden" id="vendor_id" class="form-control" name="vendor_id" value ="<?=isset($vendor->vendor_id) ? $vendor->vendor_id : '';?>"  >
							<input onchange = "check_duplicacy(this.value);" type="text" id="vendor_name" class="form-control" name="vendor_name" value ="<?=isset($vendor->vendor_name) ? $vendor->vendor_name : '';?>"  required="required"/>
						</div>
						<div class="col-md-3">
							<label>Vendor Type:</label>
							<select class="form-control" name = "vendor_type" required="required">
						     	<option value="">Select One</option>
								<?php
									foreach ($vendor_types as $key => $type) {
											$vendor_type_optn = isset($vendor->vendor_type) ? $vendor->vendor_type : '0';
											if ($key == $vendor->vendor_type) {
												$selected = 'selected = "selected"';
											} else {
												$selected = '';
											}

											echo '<option ' . $selected . ' value = ' . $key . '>' . $type . '</option>';
										}
										?>
							</select>
						</div>
					</div>
					<div class="row borrower_data_row">

						<div class="col-md-3">
								<label>Vendor Phone:</label>
								<input type="text" class="form-control phone-format" id="phone_number" name="v_phone" value ="<?=isset($vendor->v_phone) ? $vendor->v_phone : '';?>"  />

						</div>
						<div class="col-md-3">
								<label>Phone Type:</label>
								<select class="form-control" name="v_phone_type">

									<?php foreach ($vendor_contact_phone_type as $k => $p_type) {
										if($vendor->v_phone_type == $k){
											$selected = 'selected';
										}else{
											$selected = '';
										}
											echo '<option '.$selected.' value = ' . $k . '>' . $p_type . '</option>';
										
									} ?>
								</select>
						</div>
						<div class="col-md-1">
							<a type="button" class="btn btn-primary" onclick="displayalterinfo(this,'phn');" title="Add More"><i class="fa fa-plus"></i></a>
						</div>
					</div>
					

					<div class="row borrower_data_row" id="altervphn" style="display: none;">
						<div class="col-md-3">
								<label>Vendor Phone:</label>
								<input type="text" class="form-control phone-format" id="phone_number" name="v_phone1" value ="<?=isset($vendor->v_phone1) ? $vendor->v_phone1 : '';?>"  />

						</div>
						<div class="col-md-3">
								<label>Phone Type:</label>
								<select class="form-control" name="v_phone_type1">

									<?php foreach ($vendor_contact_phone_type as $k => $p_type) {
										if($vendor->v_phone_type1 == $k){
											$selected = 'selected';
										}else{
											$selected = '';
										}
											echo '<option '.$selected.' value = ' . $k . '>' . $p_type . '</option>';
										
									} ?>
								</select>
						</div>
					</div>

					<div class="row borrower_data_row">
						<div class="col-md-3">
								<label>Vendor E-Mail:</label>
								<input type="email" class="form-control" name="v_email" value ="<?=isset($vendor->v_email) ? $vendor->v_email : '';?>"  />
						</div>
						<div class="col-md-3">
								<label>E-Mail Type:</label>
								<select class="form-control" name="v_email_type">

									<?php foreach ($vendor_contact_phone_type as $k => $p_type) {
										if($vendor->v_email_type == $k){
											$selected = 'selected';
										}else{
											$selected = '';
										}
											echo '<option '.$selected.' value = ' . $k . '>' . $p_type . '</option>';
									} ?>
								</select>
						</div>
						<div class="col-md-1">
							<a type="button" onclick="displayalterinfo(this,'email')" class="btn btn-primary" title="Add More"><i class="fa fa-plus"></i></a>
						</div>
					</div>

					<div class="row borrower_data_row" id="altervemail" style="display: none;">
						<div class="col-md-3">
								<label>Vendor E-Mail:</label>
								<input type="email" class="form-control" name="v_email1" value ="<?=isset($vendor->v_email1) ? $vendor->v_email1 : '';?>"  />
						</div>
						<div class="col-md-3">
								<label>E-Mail Type:</label>
								<select class="form-control" name="v_email_type1">

									<?php foreach ($vendor_contact_phone_type as $k => $p_type) {
										if($vendor->v_email_type1 == $k){
											$selected = 'selected';
										}else{
											$selected = '';
										}
											echo '<option '.$selected.' value = ' . $k . '>' . $p_type . '</option>';
									} ?>
								</select>
						</div>
						
					</div>

					<script type="text/javascript">


						$(document).ready(function(){

							var alterphone = '<?php echo $vendor->v_phone1;?>';
							var alteremail = '<?php echo $vendor->v_email1;?>';

							if(alterphone !=''){
								$('#altervphn').css('display','block');
							}
							if(alteremail !=''){
								$('#altervemail').css('display','block');
							}

						});
						function displayalterinfo(that,key){

							if(key == 'phn'){
								$('#altervphn').css('display','block');
							}

							if(key == 'email'){
								$('#altervemail').css('display','block');
							}
						}
					</script>

					<div class="row borrower_data_row">

						<div class="col-md-3">
								<label>Website:</label>
								<input type="text" class="form-control" name = "website" value ="<?=isset($vendor->website) ? $vendor->website : '';?>"  />

						</div>

					</div>


				<div class="row borrower_data_row">
					<h4> Vendor Contact(s)</h4>
				</div>
				<div id="main_div">
					<?php

					// echo '<pre>';
					// print_r($vendor_contact);
					// echo '</pre>';
						if(isset($vendor_contact) && is_array($vendor_contact)){

							foreach($vendor_contact as $kh=>$row){
								if($row['id'] != ''){
								// print_r($row);

					 ?>
					<div id="inner_main_div">
						<div class="row borrower_data_row">

							<div class="col-md-3">
								<label> Contact Name:</label>

								<input type="hidden" class="form-control" id = "id" name = "id[]" value ="<?=isset($row['id']) ? $row['id'] : '';?>"  />
						
							    	<select  name ="contact_name[]"  onchange="option_select(this.value,this)" class="chosen" >
										<option value=''>SEARCH </option>
									<?php

										foreach ($fetch_all_contact as $roww) {


											if (isset($row['contact_id'])) {
												if ($row['contact_id'] == $roww->contact_id) {
													$selected = 'selected';
												} else {
													$selected = '';
												}
											}elseif(isset($vendor->contact_id)) {
												if ($vendor->contact_id == $roww->contact_id) {
													$selected = 'selected';
												} else {
													$selected = '';
												}
											}
											 else {
												$selected = '';
											}


											?>
											<option <?php echo $selected; ?> value="<?php echo $roww->contact_id; ?>" ><?php echo $roww->contact_firstname . ' ' . $roww->contact_middlename . ' ' . $roww->contact_lastname; ?></option>
														<?php
										}
											?>
									</select>
							</div>
							<div class="col-md-3">
								<label> Contact Title:</label>
								<input type="text" class="form-control" id = "contact_title" name = "contact_title[]" value ="<?=isset($row['contact_title']) ? $row['contact_title'] : '';?>"  />
							</div>
							<div class="col-md-3" style="display:none;">
								<label> Department:</label>
								<input type="text" class="form-control" id = "contact_department" name = "contact_department[]" value ="<?=isset($row['contact_department']) ? $row['contact_department'] : '';?>"  />
							</div>
							<div class="col-md-3">
								<label></label>
								<a title="Remove" onclick="DeleteContactinfo('<?php echo $row['id'];?>');" class="btn btn-danger" style="margin-top: 25px;"><i class="fa fa-trash"></i></a>
							</div>
						</div>

						<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>Phone:</label>
								<input type="text" class="form-control phone-format" id = "phone_number" name = "phone_number" value ="<?=isset($row['contact_phone']) ? $row['contact_phone'] : '';?>"  />
							</div>

							<div class="col-md-3">
								<label>Phone Type:</label>
								<select class="form-control" name = "phone_type">

									<?php
										foreach ($vendor_contact_phone_type as $k => $p_type) {
												//$phone_type_optn = isset($vendor->p_type) ? $vendor->p_type : '0';
												if ($k == $row['primary_option']) {
													$selected = 'selected = "selected"';
												} else {
													$selected = '';
												}

												echo '<option ' . $selected . ' value = ' . $k . '>' . $p_type . '</option>';
											}
											?>
								</select>
							</div>
						</div>

						<!--<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>Phone 2:</label>
								<input type="text" class="form-control phone-format" id = "phone_number2" name = "phone_number2" value ="<?=isset($row['alter_phone']) ? $row['alter_phone'] : '';?>"  />
							</div>

							<div class="col-md-3">
								<label>Phone Type 2:</label>
								<select class="form-control" name = "alter_option" >

									<?php
									/*foreach ($contact_number_option as $k => $p_type) {

											if ($k == $row['alter_option']) {
												$selected = 'selected = "selected"';
											} else {
												$selected = '';
											}

											echo '<option ' . $selected . ' value = ' . $k . '>' . $p_type . '</option>';
										}*/
										?>
								</select>
							</div>
						</div>-->

						<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>E-Mail:</label>
								<input type="text" class="form-control" id = "email" name = "email" value ="<?=isset($row['contact_email']) ? $row['contact_email'] : '';?>"  />

							</div>
							<div class="col-md-3">
								<label>E-Mail Type:</label>
								<select class="form-control" name="pemail_type">
									<?php foreach ($vendor_contact_phone_type as $key => $rowsss) {?>
										<option value="<?php echo $key; ?>" <?php if(isset($row['pemail_type'])){ if($row['pemail_type'] == $key){echo 'selected';}}?> ><?php echo $rowsss; ?></option>
									<?php }?>
								</select>
							</div>
						</div>


						<!--<div class="row borrower_data_row">
							<div class="col-md-3">
								<label>E-Mail: </label>
								<input type="text" class="form-control" id = "email2" name = "email2" value ="<?php echo  isset($row['contact_email2']) ? $row['contact_email2']:'';?>"  />

							</div>

							<div class="col-md-3">
								<label>E-Mail Type:</label>
								<select class="form-control" name="aemail_type">
									<?php /*foreach ($email_types_option as $key => $rowsss) {?>
										<option value="<?php echo $key; ?>" <?php if(isset($row['aemail_type'])) { if($row['aemail_type'] == $key){echo 'selected';}}?> ><?php echo $rowsss; ?></option>
									<?php }*/?>
								</select>
							</div>
						</div>-->		
					</div><p></p>		

				<?php } } } ?>
					
				</div>

					<hr style = "width: 90%;margin-left: 93px;float: left;">
		
						<div class="row borrower_data_row">
							<div class="col-md-3">
								<a style="width: 300px;" class="btn blue" onclick="add_this_contact(this);" data-toggle="modal" >Add Contact</a>

							</div>
						</div>

					<?php	//} ?>

						<!---  Add Contact modal Starts---->

					<div class="modal fade bs-modal-lg" id="add_contact" tabindex="-1" role="large" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Add Contact</h4>
								</div>
									<!-----------Hidden fields----------->
									<!-----------End of hidden Fields---->
								<div class="modal-body">
									<div class="row ">
										<div class="col-md-3">
											<label>Contact: </label>
										</div>
										<div class="col-md-3">

											<select  name ="search_contact"  onchange="option_select(this.value)" class="chosen" >
												<option value=''>SEARCH </option>
												<?php

													foreach ($fetch_all_contact as $row) {
														if (isset($vendor->contact_id)) {
															if ($vendor->contact_id == $row->contact_id) {
																$selected = 'selected';
															} else {
																$selected = '';
															}
														} else {
															$selected = '';
														}
														?>
														<option <?php echo $selected; ?> value="<?php echo $row->contact_id; ?>" ><?php echo $row->contact_firstname . ' ' . $row->contact_middlename . ' ' . $row->contact_lastname; ?></option>
																									<?php
														}
													?>
											</select>
										</div>
									</div>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn default" data-dismiss="modal">Close</button>
									<!--<button type="submit" name = "save" class="btn blue">Save</button>-->
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
					<!-- /.modal-dialog -->
					</div>

					<!--- Add Contact modal  End ---->



				</div>

				
				<div class = "main-left2-div">
					<div class="row borrower_data_row">
							<h4>Physical Address</h4>
					</div>
					<div class="row borrower_data_row">
						<!--<div class="col-md-3">
							<label>Address:</label>
							<input type="text" class="form-control" id = "address" name = "address" value ="<?=isset($vendor->address) ? $vendor->address : '';?>">
						</div>-->
						<div class="col-md-3">
							<label>Attn:</label>
							<input type="text" class="form-control" onchange = "fill_mailing_portion(this.id,this.value)" id = "attn" name = "attn" value ="<?=isset($vendor->attn) ? $vendor->attn : '';?>" />
						</div>

					</div>
					<div class="row borrower_data_row">
						<div class="col-md-3">
							<label>Street Address:</label>
							<input type="text" class="form-control" onchange = "fill_mailing_portion(this.id,this.value)" id = "street_address" name = "street_address" value ="<?=isset($vendor->street_address) ? $vendor->street_address : '';?>" />
						</div>
						<div class="col-md-3">
							<label>Unit #:</label>
							<input type="text" class="form-control" onchange = "fill_mailing_portion(this.id,this.value)" name = "unit" id = "unit" value ="<?=isset($vendor->unit) ? $vendor->unit : '';?>"  />
						</div>

					</div>
					<div class="row borrower_data_row">



						<div class="col-md-3">
							<label>City:</label>
							<input type="text" class="form-control" onchange = "fill_mailing_portion(this.id,this.value)" id = "city" name = "city" value ="<?=isset($vendor->city) ? $vendor->city : '';?>">
						</div>
						<div class="col-md-3">
							<label>State:</label>
							<input type="text" class="form-control" onchange = "fill_mailing_portion(this.id,this.value)" name = "state" id = "state" value ="<?=isset($vendor->state) ? $vendor->state : '';?>"  />
						</div>
						<div class="col-md-3">
							<label>Zip:</label>
							<input type="text" class="form-control" onchange = "fill_mailing_portion(this.id,this.value)" id = "zip" name = "zip" value ="<?=isset($vendor->zip) ? $vendor->zip : '';?>">
						</div>
					</div>

					<div class="row borrower_data_row">
							<h4>Mailing Address</h4>
						<div class="col-md-3 same_check" >

							<input type = "checkbox" <?php if (isset($vendor->vendor_id)) {if ($vendor->same_mailing_address == '1') {echo 'checked';}}?> id="same_mailing_chkbx" onclick="copy_address()" /> <span>Check, If same as Above</span>
							<input type = "hidden" id = "same_mailing_address" name = "same_mailing_address"  value =  "<?php echo isset($vendor->vendor_id) ? $vendor->same_mailing_address : '0'; ?>"/>
						</div>
					</div>
					<div class="row borrower_data_row">
						<!--<div class="col-md-3">
							<label>Address:</label>
							<input type="text" class="form-control" id = "address" name = "address" value ="<?=isset($vendor->address) ? $vendor->address : '';?>">
						</div>-->
						<div class="col-md-3">
							<label>Attn:</label>
							<input type="text" class="form-control" onchange = "fill_mailing_portion(this.id,this.value)" id = "mailing_attn" name = "mailing_attn" value ="<?=isset($vendor->mailing_attn) ? $vendor->mailing_attn : '';?>" />
						</div>

					</div>
					<div class="row borrower_data_row">
						<div class="col-md-3">
							<label>Street Address:</label>
							<input type="text" class="form-control" id = "mailing_street_address" name = "mailing_street_address" value ="<?=isset($vendor->mailing_street_address) ? $vendor->mailing_street_address : '';?>" />
						</div>
						<div class="col-md-3">
							<label>Unit #:</label>
							<input type="text" class="form-control" name = "mailing_unit"  id = "mailing_unit" value ="<?=isset($vendor->mailing_unit) ? $vendor->mailing_unit : '';?>"  />
						</div>

					</div>
					<div class="row borrower_data_row">



						<div class="col-md-3">
							<label>City:</label>
							<input type="text" class="form-control" id = "mailing_city" name = "mailing_city" value ="<?=isset($vendor->mailing_city) ? $vendor->mailing_city : '';?>">
						</div>
						<div class="col-md-3">
							<label>State:</label>
							<input type="text" class="form-control" name = "mailing_state" id = "mailing_state" value ="<?=isset($vendor->mailing_state) ? $vendor->mailing_state : '';?>"  />
						</div>
						<div class="col-md-3">
							<label>Zip:</label>
							<input type="text" class="form-control" id = "mailing_zip" name = "mailing_zip" value ="<?=isset($vendor->mailing_zip) ? $vendor->mailing_zip : '';?>">
						</div>
					</div>





					<div class="row borrower_data_row">
						<h4>Other</h4>
					</div>

					<div class="row borrower_data_row">
						<div class="col-md-3">
							<label>Tax ID #:</label>
							<input type="text" class="form-control" name = "tax_id" value ="<?=isset($vendor->tax_id) ? $vendor->tax_id : '';?>"  />
						</div>

					</div>

				</div>

			</form>
			<?php
}

?>


	</div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>


<script>
 $('.chosen').chosen();

	function vendor_select(id)
	{
		var search_by = $('#search_by').val();

		if(search_by  == '1'){

			window.location.href = "<?php echo base_url(); ?>vendor_data/id/"+id;

		}else{

			window.location.href = "<?php echo base_url(); ?>vendor_data/role/"+id;
		}

	}

	function DeleteContactinfo(rowid){

		if(confirm('Are you sure to remove this contact?')){

			$.ajax({
					type : 'POST',
					url  : '<?php echo base_url()."Vendor_data/delete_vendor_contact";?>',
					data : {'rowid':rowid},
					success : function(response){

						if(response == '1'){

							alert('Vendor contact removed successfully!');
							window.location.reload();
						}else{
							window.location.reload();
						}
					}
			})
		}
	}


	function delete_vendor(id)
	{

		if (confirm("Are you sure to delete this vendor?") == true) {
			window.location.href = "<?php echo base_url(); ?>delete_vendor_data/"+id;

		}

	}
	function search_vendor_by(value)
	{
		if(value == '1'){
			window.location.href = "<?php echo base_url(); ?>vendor_data/id/";
		}else{
			window.location.href = "<?php echo base_url(); ?>vendor_data/role/";
		}
		// $('#form_search_vendor_by').submit();
	}

	$(".phone-format").keypress(function (e) {
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		  return false;
		}
		var curchr = this.value.length;
		var curval = $(this).val();
		if (curchr == 3 && curval.indexOf("(") <= -1) {
		  $(this).val("(" + curval + ")" + "-");
		} else if (curchr == 4 && curval.indexOf("(") > -1) {
		  $(this).val(curval + ")-");
		} else if (curchr == 5 && curval.indexOf(")") > -1) {
		  $(this).val(curval + "-");
		} else if (curchr == 9) {
		  $(this).val(curval + "-");
		  $(this).attr('maxlength', '14');
		}
  });

  function copy_address(){

	if($('#same_mailing_chkbx').attr('checked')){
		
		var attn 			= $('#attn').val();
		var street_address 	= $('#street_address').val();
		var unit 			= $('#unit').val();
		var city 			= $('#city').val();
		var state 			= $('#state').val();
		var zip 			= $('#zip').val();

		$('#mailing_attn').val(attn);
		$('#mailing_street_address').val(street_address);
		$('#mailing_unit').val(unit);
		$('#mailing_city').val(city);
		$('#mailing_state').val(state);
		$('#mailing_zip').val(zip);

		$('#same_mailing_address').val('1');

	}else{

		$('#mailing_attn').val('');
		$('#mailing_street_address').val('');
		$('#mailing_unit').val('');
		$('#mailing_city').val('');
		$('#mailing_state').val('');
		$('#mailing_zip').val('');

		$('#same_mailing_address').val('0');
	}
}

function fill_mailing_portion(id,value){

	if($('#same_mailing_chkbx').attr('checked')){


		$('#mailing_'+id).val(value);
	}else{
		$('#mailing_'+id).val('');

	}
}

function option_select(value,that){


	// ajax_call...
	$.ajax({
				type: "post",
				url: "<?php echo base_url() ?>Vendor_data/fetch_contact_details/",
				data:{'id':value},
				success: function(response){
					if(response){

					var data = JSON.parse(response);
						
				
						//$(that).parent().parent().parent().find('input#contact_id').val(data.contact_id);
					//	$(that).parent().parent().parent().find('input#contact_name').val(data.contact_name);
						$(that).parent().parent().parent().find('input#contact_title').val(data.contact_title);
						$(that).parent().parent().parent().find('input#contact_department').val(data.contact_department);
						$(that).parent().parent().parent().find('input#email').val(data.contact_email);
						$(that).parent().parent().parent().find('input#email2').val(data.contact_email2);
						// $(that).parent().parent().parent().addClass('test_t');
						$(that).parent().parent().parent().find('input#phone_number').val(data.contact_phone);
						$(that).parent().parent().parent().find('input#phone_number2').val(data.contact_phone2);


						$(that).parent().parent().parent().find('select[name="phone_type"] option').attr('selected',false);

						$(that).parent().parent().parent().find('select[name="phone_type"] option[value="'+data.primary_option+'"]').attr('selected',true);


						
						$(that).parent().parent().parent().find('select[name="alter_option"] option').attr('selected',false);
						$(that).parent().parent().parent().find('select[name="alter_option"] option[value="'+data.alter_option+'"]').attr('selected',true);

							$(that).parent().parent().parent().find('select[name="pemail_type"] option').attr('selected',false);
						
						$(that).parent().parent().parent().find('select[name="pemail_type"] option[value="'+data.pemail_type+'"]').attr('selected',true);

							$(that).parent().parent().parent().find('select[name="aemail_type"] option').attr('selected',false);
						
						$(that).parent().parent().parent().find('select[name="aemail_type"] option[value="'+data.aemail_type+'"]').attr('selected',true);
						
						// $('#contact_id').val(data.contact_id);
						// $('#contact_name').val(data.contact_name);
						// $('#contact_title').val(data.contact_title);
						// $('#phone_number').val(data.contact_phone);
						// $('#phone_number2').val(data.contact_phone2 );
						// $('#email').val(data.contact_email);
						// $('#email2').val(data.contact_email2);
						// $('select[name="phone_type"] option[value="'+data.primary_option+'"]').attr('selected',true);
						// $('select[name="alter_option"] option[value="'+data.alter_option+'"]').attr('selected',true);
						// $('select[name="pemail_type"] option[value="'+data.pemail_type+'"]').attr('selected',true);
						// $('select[name="aemail_type"] option[value="'+data.aemail_type+'"]').attr('selected',true);
				
						$('#add_contact').modal('hide');

					}
				}
			});

}
function option_selects(valuee,thatt){


	// ajax_call...
	$.ajax({
				type: "post",
				url: "<?php echo base_url() ?>Vendor_data/fetch_contact_details/",
				data:{'id':valuee},
				success: function(response){
					if(response){

					var data = JSON.parse(response);

						// $(thatt).parent().parent().parent().find('select option').attr('selected',false);
						//$(that).parent().parent().parent().find('input#contact_id').val(data.contact_id);
						//$(thatt).parent().parent().parent().find('select#contact_namee ').val(data.contact_name);
						 $(thatt).parent().parent().parent().find('input#contact_titlee').val(data.contact_title);
						$(thatt).parent().parent().parent().find('input#emaill').val(data.contact_email);
						$(thatt).parent().parent().parent().find('input#email22').val(data.contact_email2);
						 // $(thatt).parent().parent().parent().addClass('test_t');
						$(thatt).parent().parent().parent().find('input#phone_numberr').val(data.contact_phone);
						$(thatt).parent().parent().parent().find('input#phone_number22').val(data.contact_phone2);
						
						$(thatt).parent().parent().parent().find('select#phone_typee option').attr('selected',false);
					
						$(thatt).parent().parent().parent().find('select#phone_typee option[value="'+data.primary_option+'"]').attr('selected',true);
						
						$(thatt).parent().parent().parent().find('select#alter_optionn option').attr('selected',false);
					   $(thatt).parent().parent().parent().find('select#alter_optionn option[value="'+data.alter_option+'"]').attr('selected',true);
						
						$(thatt).parent().parent().parent().find('select#pemail_typee option').attr('selected',false);
						$(thatt).parent().parent().parent().find('select#pemail_typee option[value="'+data.pemail_type+'"]').attr('selected',true);

						$(thatt).parent().parent().parent().find('select#aemail_typee option').attr('selected',false);

						$(thatt).parent().parent().parent().find('select#aemail_typee option[value="'+data.aemail_type+'"]').attr('selected',true);
						
						$('#add_contact').modal('hide');

					}
				}
			});

}

function check_duplicacy(value){

	// ajax call to check whether vendor name already exist or not....
	var edit_id = '<?php echo isset($vendor->vendor_id) ? $vendor->vendor_id : '' ?>';

	$.ajax({
		type : 'post',
		url  : '<?php echo base_url() . 'Vendor_data/check_duplicacy' ?>',
		data : {'value':value},
		success: function(response){
			if(response == '1'){
				if((edit_id) && (response == '1')){

				}else{
					$('#vendor_name').val('');
					alert('Vendor name: "' +value+ '" already Exists, Please Choose Another Vendor name!');

				}

			}

		}
	});
}

function display_search_fields(){
	$('.search_div').show();
	$('.edit_btn').hide();


}

function add_this_contact(that){
	 
	var rows= $('#main_div .borrower_data_row').length;

 	rows++;

 	$("#main_div #inner_main_div select.chosen_3222").chosen( $('#main_div').append('<div id="inner_main_div"><div class="row borrower_data_row"><div class="col-md-3"><label> Contact Name:</label><input type="hidden" class="form-control" id ="id" name = "id[]" value ="" ><select  name ="contact_name[]"  id="contact_namee "onchange="option_selects(this.value,this)" class="chosen_3222" ><option value="">SEARCH </option> <?php foreach ($fetch_all_contact as $roww) {  ?><option value="<?php echo $roww->contact_id; ?>" ><?php echo str_replace("'","",$roww->contact_firstname) . " " . $roww->contact_middlename . " " .str_replace("'","",$roww->contact_lastname); ?></option><?php } ?></select></div><div class="col-md-3"><label> Contact Title:</label><input type="text" class="form-control" id = "contact_titlee" name = "contact_title[]" value =""  /></div><div class="col-md-3"><label> Department:</label><input type="text" class="form-control" id = "contact_department" name = "contact_department[]" value =""  /></div></div><div class="row borrower_data_row"><div class="col-md-3"><label>Phone:</label><input type="text" class="form-control phone-format" id = "phone_numberr" name = "phone_number" value =""  /></div><div class="col-md-3"><label>Phone Type:</label><select class="form-control" id="phone_typee" name = "phone_type"><?php foreach ($vendor_contact_phone_type as $k => $p_type) { ?><option value="<?php echo $k; ?>" ><?php echo $p_type; ?></option><?php } ?></select></div></div><div class="row borrower_data_row"><div class="col-md-3"><label>E-Mail:</label><input type="text" class="form-control" id = "emaill" name = "email" value =""  /></div><div class="col-md-3"><label>E-Mail Type:</label><select class="form-control" id="pemail_typee" name="pemail_type"><?php foreach ($vendor_contact_phone_type as $key => $row) { ?><option value="<?php echo $key;?>"><?php echo $row; ?></option><?php } ?></select></div></div></div>'));

		
		setTimeout( function(){ 
		     $("#main_div #inner_main_div select.chosen_3222").chosen();
		  }  , 10 );

}



</script>
