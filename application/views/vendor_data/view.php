<?php 
//------------values from myConfig---------------
$vendor_types 						= $this->config->item('vendor_type');
asort($vendor_types);
$contact_number_option 						= $this->config->item('contact_number_option');
$email_types_option 						= $this->config->item('email_types_option');
$vendor_contact_phone_type 						= $this->config->item('vendor_contact_phone_type');


?>
<style>

.main-left-div h4 {
    padding: 6px;
}
.main-left2-div h4 {
    padding: 6px;
}
.row.borrower_data_row.valid-row div.col-md-6 {
    width: 50% !important;
}
.row.borrower_data_row.valid-row div.col-md-3 {
    width: 25% !important;
}
.row.borrower_data_row.valid-row div.col-md-8 {
    width: 66.66% !important;
}
.row.borrower_data_row.valid-row div.col-md-4 {
    width: 33.33% !important;
}
</style>
<div class="page-container" >
			<!-- BEGIN PAGE HEAD -->
		
			<div class="container" id="page-vender-data-view">
			<!--------------------MESSEGE SHOW-------------------------->
				<?php if($this->session->flashdata('error')!=''){  ?>
				 <div id='error'><i class='fa fa-thumbs-o-down'></i><?php echo $this->session->flashdata('error');?></div><?php } ?>
				  <?php if($this->session->flashdata('success')!=''){  ?>
				 <div id='success'><i class='fa fa-thumbs-o-up'></i><?php echo $this->session->flashdata('success');?></div><?php } ?>
				 

			<div class="row button_section_div">
				
				<div class="col-md-6">
					<h4><b style="font-weight: 500;">Vendor Data : <?= isset($vendor->vendor_name) ? $vendor->vendor_name : '';?></b></h4>
				</div>
				<div class="col-md-6">
					
						<?php
							$uri = $_SERVER['REQUEST_URI'];
							$chunks = array_filter(explode('/', $uri));
							
							$new_class = "style = 'display:block;' ";
							if(isset($chunks[2]) && ($chunks[2] == 'new')){
								$new_class = "style = 'display:none;' ";
							
						?>
						
						<button  style= "" type = "submit" name = "save" class="btn btn-primary borrower_save_button" value="save">Save</button>
						<a  class="btn btn-primary borrower_save_button" href = "<?php echo base_url().'vendor-primary';?>" >Close</a>
						
						<?php	}else{
						 if(!(isset($vendor->vendor_id)) && ($user_role == 2)) {?>		
						&nbsp;
						
						
						<a <?php echo $new_class;?> class="btn btn-primary borrower_save_button" href="<?php echo  base_url().'vendor_data/new';?>" >New</a>
						
						<?php } } ?>
						
						<?php if((isset($vendor->vendor_id)) ) {?>

						

						<a class="btn btn-primary edit_btn borrower_save_button" href="<?php echo base_url().'vendor_data/id/'.$vendor->vendor_id; ?>"  >Edit</a>
						
						<!--<a  class="btn btn-primary borrower_save_button" onclick = "delete_vendor(<?php echo $vendor->vendor_id;?>)">Delete</a> -->
						
						<a  class="btn btn-primary borrower_save_button" href = "<?php echo base_url().'vendor-primary';?>" >Close</a> <?php } ?>

						<?php if($user_role == 2){ ?>
						<a class="btn btn-danger edit_btn delete_action_vendor" href="<?php echo base_url().'delete_vendor/'.$vendor->vendor_id; ?>" onclick="return confirm('Are you sure, you want to delete it?')" >Delete</a>
						<?php } ?>
					
					
				</div> 
				<!-- Button -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					 Dashboard
				</li>
			</ul>
			
			<input type="hidden" name = "vendor_id" value="<?php echo isset($vendor->vendor_id) ? $vendor->vendor_id : '' ;?>">
			<div class = "main-left-div">
				
					
				<form method="POST" action="<?php echo base_url();?>vendor_data" id="form_search_vendor_by">
					<div class="row borrower_data_row valid-row">
					<div class="col-md-6">
						<label>Find Vendor:</label>
						
						<select onchange="vendor_select_by_id(this.value)" class="selectpicker" data-live-search="true" >
							<option value=''></option>	
							<?php
							foreach($all_vendor_data_result as $row)
							{
								if($selected_vender_id == $row->vendor_id){
									$selected = 'selected = "selected" ';
								}else{
									
									$selected = '';
								}
								?>
								<option <?php echo $selected;?> value="<?= $row->vendor_id;?>"  ><?= $row->vendor_name; ?></option>
								<?php
							}
							
							?>
						</select>
					</div>
					</div>
				</form>
				
					<div class="row borrower_data_row valid-row">
						<h4>Vendor Data</h4>
						<div class="col-md-3">
							<label>Vendor Name:</label>
							<input type="hidden" id="vendor_id" class="form-control" name="vendor_id" value ="<?= isset($vendor->vendor_id) ? $vendor->vendor_id : '';?>"  required/>
							<?php echo isset($vendor->vendor_name) ? $vendor->vendor_name : '';?>
						</div>
						<div class="col-md-3">
							<label>Vendor Type:</label>
							<?php echo isset($vendor->vendor_type) ? $vendor_types[$vendor->vendor_type] : '';?>
						</div>

					</div>
						

					<div class="row borrower_data_row">
						<div class="col-md-3">
								<label>Vendor Phone:</label>
								<?php echo isset($vendor->v_phone) ? $vendor->v_phone : '';?>
								
						</div>
						<div class="col-md-3">
								<label>Phone Type:</label>
								<?php echo isset($vendor->v_phone_type) ? $vendor_contact_phone_type[$vendor->v_phone_type] :'';?>
								
						</div>

					
						<div class="col-md-4">
								<label>Vendor E-Mail:</label>
								<a href="mailto:<?php echo isset($vendor->v_email) ? $vendor->v_email : '';?>"><?php echo isset($vendor->v_email) ? $vendor->v_email : '';?></a>
								
						</div>
						<div class="col-md-2">
								<label>E-Mail Type:</label>
								<?php echo isset($vendor->v_email_type) ? $vendor_contact_phone_type[$vendor->v_email_type] :'';?>
								
						</div>
					</div>

					<?php if($vendor->v_phone1 !=''){ ?>

						<div class="row borrower_data_row">
							<div class="col-md-3">
									<label>Vendor Phone:</label>
									<?php echo isset($vendor->v_phone1) ? $vendor->v_phone1 : '';?>
									
							</div>
							<div class="col-md-3">
									<label>Phone Type:</label>
									<?php echo isset($vendor->v_phone_type1) ? $vendor_contact_phone_type[$vendor->v_phone_type1] :'';?>
									
							</div>

						
							<div class="col-md-4">
									<label>Vendor E-Mail:</label>
									<a href="mailto:<?php echo isset($vendor->v_email1) ? $vendor->v_email1 : '';?>"><?php echo isset($vendor->v_email1) ? $vendor->v_email1 : '';?></a>
									
							</div>
							<div class="col-md-2">
									<label>E-Mail Type:</label>
									<?php echo isset($vendor->v_email_type1) ? $vendor_contact_phone_type[$vendor->v_email_type1] :'';?>
									
							</div>
						</div>

					<?php } ?>

					<div class="row borrower_data_row">
						<div class="col-md-12">
								<label>Website:</label>
								<?php echo isset($vendor->website) ? '<a href="#">'.$vendor->website.'</a>' : '';?>
								
						</div>
					</div>
				
					
					<div class="row borrower_data_row">		
						<h4>Vendor Contacts</h4>
						
					</div>
					<?php 
						if(isset($vendor_contact) && is_array($vendor_contact)){
							foreach($vendor_contact as $kh=>$row){
							if($row['id'] != ''){

					 ?>
					<div class="row borrower_data_row">		
						<div class="col-md-3">
							<label> Contact Name:</label>
							
							<?php 
							
								foreach ($fetch_all_contact as $roww) {

								if (isset($row['contact_id'])) {
									if ($row['contact_id']== $roww->contact_id) { ?>
									
									
									<a href="<?php echo base_url();?>viewcontact/<?php echo $row['contact_id'];?>"><?php echo $roww->contact_firstname.' '.$roww->contact_middlename.' '.$roww->contact_lastname; ?></a>

								
								<?php	} 

								  }
								}
							?>
						</div>

						<div class="col-md-3">
							<label> Contact Title:</label>
							<?php echo isset($row['contact_title']) ? $row['contact_title'] : '';?>
						</div>

						<div class="col-md-3" style="display: none;">
							<label>Department:</label>
							
							<?php echo isset($row['contact_department']) ? $row['contact_department'] : '';?>

							
						</div>
					</div>
				
					<div class="row borrower_data_row">		
						<div class="col-md-3">
							<label>Phone:</label>
							<?php echo isset($row['contact_phone']) ? $row['contact_phone']: '';?>
						</div>	
						
					
						<!-- <div class="col-md-3">
							<label>Phone:</label>
							<?php echo isset($row['alter_phone']) ? $row['alter_phone'] : ''; ?>
						</div> -->

						<div class="col-md-3">
							<label>Type:</label>
							<?php echo isset($row['primary_option']) ? $vendor_contact_phone_type[$row['primary_option']] : ''; ?>
						</div>	
						<!-- <div class="col-md-3">
							<label>Type:</label>
							<?php echo isset($row['alter_option']) ? $vendor_contact_phone_type[$row['alter_option']] : ''; ?>
						</div>	 -->
						
						
							
					</div>


					<div class="row borrower_data_row">		
						<div class="col-md-3">
							<label>E-Mail:</label>
							<a href="mailto:<?php echo isset($row['contact_email']) ? $row['contact_email'] : ''; ?>"><?php echo isset($row['contact_email']) ? $row['contact_email'] : ''; ?></a>
						</div>
			
						<!-- <div class="col-md-3">
							<label>E-Mail:</label>
							<?php //echo isset($row['contact_email2']) ? $row['contact_email2'] : ''; ?>
						</div> -->
				

						<div class="col-md-3">
							<label>Type:</label>
							<?php echo isset($row['pemail_type']) ? $vendor_contact_phone_type[$row['pemail_type']] : ''; ?>
						</div>
						<!-- <div class="col-md-3">
							<label>Type:</label>
							<?php echo isset($row['aemail_type']) ? $vendor_contact_phone_type[$row['aemail_type']] : ''; ?>
						</div> -->
					</div>

				<?php  } echo '<p></p>'; }  } ?>
					
				</div>
				
				
				<div class = "main-left2-div">
					<div class="row borrower_data_row">
						<h4>Physical Address</h4>
					
					</div>
					<div class="row borrower_data_row">					
						<!--<div class="col-md-3">
							<label>Address:</label>
							<input type="text" class="form-control" id = "address" name = "address" value ="<?= isset($vendor->address) ? $vendor->address : '';?>">
						</div>-->
						<div class="col-md-3">
							<label>Attn:</label>
							<?php echo isset($vendor->attn) ? $vendor->attn : ''; ?>
						</div>
					</div>
					<div class="row borrower_data_row">	
						<div class="col-md-3">
							<label>Street Address:</label>
							<?php echo isset($vendor->street_address) ? $vendor->street_address : ''; ?>
						</div>
						<div class="col-md-3">
							<label>Unit #:</label>
							<?php echo isset($vendor->unit) ? $vendor->unit : '';?>
						</div>
					
					</div>
					<div class="row borrower_data_row">					
						<div class="col-md-3">
							<label>City:</label>
							<?php echo isset($vendor->city) ? $vendor->city : ''; ?>
						</div>
						<div class="col-md-3">
							<label>State:</label>
							<?php echo isset($vendor->state) ? $vendor->state : '';?>
						</div>
						<div class="col-md-3">
							<label>Zip:</label>
							<?php echo isset($vendor->zip) ? $vendor->zip : ''; ?>
						</div>
					</div>
 
					<div class="row borrower_data_row">
						<h4>Mailing Address</h4>
						
						<!--<div class="col-md-3 same_check" >
							
							<input type = "checkbox" <?php if (isset($vendor->vendor_id)){ if($vendor->same_mailing_address == '1'){echo 'checked';} }?> id = "same_mailing_chkbx" onclick = "copy_address()" disabled /> Check, If same as Above 
							<input type = "hidden" id = "same_mailing_address" name = "same_mailing_address"  value =  "<?php echo isset($vendor->vendor_id) ? $vendor->same_mailing_address : '0';?>"/> 
						</div>-->
					</div>
					<div class="row borrower_data_row">					
		
						<div class="col-md-3">
							<label>Attn:</label>
							<?php echo isset($vendor->mailing_attn) ? $vendor->mailing_attn : ''; ?>
						</div>
					</div>
					<div class="row borrower_data_row">	
						<div class="col-md-3">
							<label>Street Address:</label>
							<?php echo isset($vendor->mailing_street_address) ? $vendor->mailing_street_address : ''; ?>
						</div>
						<div class="col-md-3">
							<label>Unit #:</label>
							<?php echo isset($vendor->mailing_unit) ? $vendor->mailing_unit : ''; ?>
						</div>
					
					</div>
					<div class="row borrower_data_row">					
						<div class="col-md-3">
							<label>City:</label>
							<?php echo isset($vendor->mailing_city) ? $vendor->mailing_city : '';?>
						</div>
						<div class="col-md-3">
							<label>State:</label>
							<?php echo isset($vendor->mailing_state) ? $vendor->mailing_state : '';?>
						</div>
						<div class="col-md-3">
							<label>Zip:</label>
							<?php echo isset($vendor->mailing_zip) ? $vendor->mailing_zip : '';?>
						</div>
					</div>
					
					
                    
					<div class="row borrower_data_row">					
							<h4>Other</h4>
					</div>
							
					<div class="row borrower_data_row">					
						<div class="col-md-3">
							<label>Tax ID #:</label>
							<?php echo isset($vendor->tax_id) ? $vendor->tax_id : '';?>
						</div>
						
					</div>
				
				</div>
			
			
	</div>
</div>

<script>
	function vendor_select(id)
	{
		var search_by = $('#search_by').val();
		window.location.href = "<?php echo base_url();?>vendor_data_view/role/"+id;
		
	}
	

	function vendor_select_by_id(id)
	{
		var search_by = $('#vendor_select_by_id').val();
		window.location.href = "<?php echo base_url();?>vendor_data_view/id/"+id;
		
	}
	
		
	function delete_vendor(id)
	{
		
		if (confirm("Are you sure to delete this vendor?") == true) {
			window.location.href = "<?php echo base_url();?>delete_vendor_data/"+id;
		}
	}
	
	function search_vendor_by(value)
	{
		if(value == '1'){
			window.location.href = "<?php echo base_url();?>vendor_data_view/id/";
		}else{
			window.location.href = "<?php echo base_url();?>vendor_data_view/role/";
		}
		// $('#form_search_vendor_by').submit();
	}
	
	$(".phone-format").keypress(function (e) {
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		  return false;
		}
		var curchr = this.value.length;
		var curval = $(this).val();
		if (curchr == 3 && curval.indexOf("(") <= -1) {
		  $(this).val("(" + curval + ")" + "-");
		} else if (curchr == 4 && curval.indexOf("(") > -1) {
		  $(this).val(curval + ")-");
		} else if (curchr == 5 && curval.indexOf(")") > -1) {
		  $(this).val(curval + "-");
		} else if (curchr == 9) {
		  $(this).val(curval + "-");
		  $(this).attr('maxlength', '14');
		}
  });
  
  function copy_address(){
	if($('#same_mailing_chkbx').attr('checked')){
		
		var attn 			= $('#attn').val();
		var street_address 	= $('#street_address').val();
		var unit 			= $('#unit').val();
		var city 			= $('#city').val();
		var state 			= $('#state').val();
		var zip 			= $('#zip').val();
		
		$('#mailing_attn').val(attn);
		$('#mailing_street_address').val(street_address);
		$('#mailing_unit').val(unit);
		$('#mailing_city').val(city);
		$('#mailing_state').val(state);
		$('#mailing_zip').val(zip);
		
		$('#same_mailing_address').val('1');
	}else{
		
		$('#mailing_attn').val('');
		$('#mailing_street_address').val('');
		$('#mailing_unit').val('');
		$('#mailing_city').val('');
		$('#mailing_state').val('');
		$('#mailing_zip').val('');
		
		$('#same_mailing_address').val('0');
	}
}

function fill_mailing_portion(id,value){
	
	if($('#same_mailing_chkbx').attr('checked')){
		
		
		$('#mailing_'+id).val(value);
	}else{
		$('#mailing_'+id).val('');
		
	}
}

function option_select(value){
	
	
	// ajax_call...
	$.ajax({
				type: "post",
				url: "<?php echo base_url()?>Vendor_data/fetch_contact_details/",
				data:{'id':value},
				success: function(response){
					if(response){
						
					var data = JSON.parse(response);
						
						$('#contact_id').val(data.contact_id);
						$('#contact_name').val(data.contact_name);
						$('#contact_title').val(data.contact_title);
						$('#phone_number').val(data.contact_phone);
						$('#phone_number2').val(data.contact_phone2);
						$('#email').val(data.contact_email);
							
						// auto-close popup...	
						$('#add_contact').modal('hide');		
						
					}
				}
			});
			
}

function check_duplicacy(value){
	
	// ajax call to check whether vendor name already exist or not....
	var edit_id = '<?php echo isset($vendor->vendor_id) ? $vendor->vendor_id : '' ?>';
	
	$.ajax({
		type : 'post',
		url  : '<?php echo base_url().'Vendor_data/check_duplicacy'?>',
		data : {'value':value},
		success: function(response){
			if(response == '1'){
				if((edit_id) && (response == '1')){
				
				}else{
					$('#vendor_name').val('');
					alert('Vendor name: "' +value+ '" already Exists, Please Choose Another Vendor name!');
					
				}
				
			}	
			
		}
	});
}

function display_search_fields(){
	$('.search_div').show();
	$('.edit_btn').hide();
	
	
}
</script>
<style>
	.same_check #uniform-same_mailing_address{
		width:7% !important;
		float:left;
	}
	.each_checkbox{
		width:100% !important;
		margin-bottom: 10px;
		
	}
	.each_checkbox .checker{
		width: 20px !important;
		margin-right:10px !important;
	}
	.outer_checkboxes{
		border: 1px solid;
		float: left;
		width: 63% !important;
		padding: 20px;
		margin-left: 15px;
	}
	
	.main-left-div{
		float:left;
		width:100%;
	}
	.main-left2-div{
		float:left;
		width: 100%;
		
	}
	.main-right-div{
		float:left;
		width:30%;
		
	}
	.borrower_data_row{
		padding: 5px 80px !important;
	}
	.row.button_section_div {
		padding: 20px 65px !important;
	}

		label{
		font-weight:600!important;
	}
	.delete_action_vendor{
		float: right;
	    border-radius: 0;
	    margin-top: 3px;
	}
	
</style>