<?php

require_once('vendor/autoload.php');
include 'vendor/docusign/esign-client/test/bootstrap.php';


// $username = "pankaj.wartiz@gmail.com";
// $password = "pankaj@1234";
// $integrator_key = "b739a535-d751-4995-beeb-19a276573136";     

// change to production before going live
//https://www.docusign.net/restapi
//https://www.docusign.com/p/RESTAPIGuide/Content/GettingStarted/REST%20API%20Version.htm
$host = "https://demo.docusign.net/restapi";

 // create a new DocuSign configuration and assign host and header(s)
$config = new \DocuSign\eSign\Configuration();
$config->setHost($host);
$config->setSSLVerification(false);
$config->addDefaultHeader("X-DocuSign-Authentication", "{\"Username\":\"" . $username . "\",\"Password\":\"" . $password . "\",\"IntegratorKey\":\"" . $integrator_key . "\"}");
// instantiate a new docusign api client
$apiClient = new \DocuSign\eSign\ApiClient($config);

//class DocuSignSample
//{
    //public function signatureRequestFromTemplate()
    //{
       /*$username = "pankaj.wartiz@gmail.com";
        $password = "pankaj@1234";
        $integrator_key = "b739a535-d751-4995-beeb-19a276573136";     

        // change to production (www.docusign.net) before going live
        $host = "https://demo.docusign.net/restapi";

        // create configuration object and configure custom auth header
        $config = new DocuSign\eSign\Configuration();
        $config->setHost($host);
        $config->addDefaultHeader("X-DocuSign-Authentication", "{\"Username\":\"" . $username . "\",\"Password\":\"" . $password . "\",\"IntegratorKey\":\"" . $integrator_key . "\"}");

        // instantiate a new docusign api client
        $apiClient = new DocuSign\eSign\ApiClient($config);
        $accountId = null;*/
        
        try 
        {
            //*** STEP 1 - Login API: get first Account ID and baseURL
            $authenticationApi = new DocuSign\eSign\Api\AuthenticationApi($apiClient);
            $options = new \DocuSign\eSign\Api\AuthenticationApi\LoginOptions();
            $loginInformation = $authenticationApi->login($options);
            if(isset($loginInformation))
            {

            	// echo '<pre>';
            	// print_r($loginInformation);
            	// echo '<pre>';

                $loginAccount = $loginInformation->getLoginAccounts()[0];
                $host = $loginAccount->getBaseUrl();
                $host = explode("/v2",$host);
                $host = $host[0];
	
                // UPDATE configuration object
                $config->setHost($host);
		
                // instantiate a NEW docusign api client (that has the correct baseUrl/host)
                $apiClient = new DocuSign\eSign\ApiClient($config);
	
                if(isset($loginInformation))
                {
                    $accountId = $loginAccount->getAccountId();
                    if(!empty($accountId))
                    {

                        // configure the document we want signed
                        $documentFileName = "/Talimar.doc";
                        $documentName = "Talimar.doc";

                        //*** STEP 2 - Signature Request from a Template
                        // create envelope call is available in the EnvelopesApi
                        $envelopeApi = new DocuSign\eSign\Api\EnvelopesApi($apiClient);

                        // Add a document to the envelope
                        $document = new DocuSign\eSign\Model\Document();
                        $document->setDocumentBase64(base64_encode(file_get_contents(__DIR__ . $documentFileName)));
                        $document->setName($documentName);
                        //$document->setFileExtension('doc');
                        $document->setDocumentId("1");

                        echo '<pre>';
                        print_r($document);
                        echo '</pre>';


                        // assign recipient to template role by setting name, email, and role name.  Note that the
                        // template role name must match the placeholder role name saved in your account template.
                        $templateRole = new  DocuSign\eSign\Model\TemplateRole();
                        $templateRole->setEmail("vishnu.wartiz@gmail.com");
                        $templateRole->setName("Vishnu joshi");
                        $templateRole->setRoleName("1");             

                        // instantiate a new envelope object and configure settings
                        $envelop_definition = new DocuSign\eSign\Model\EnvelopeDefinition();
                        $envelop_definition->setEmailSubject("[DocuSign PHP SDK] - Signature Request Sample");
                        //$envelop_definition->setTemplateId("d8cb1cff-441c-490b-ab52-2502c2c35350");
                        //$envelop_definition->setTemplateRoles(array($templateRole));
                        $envelop_definition->setDocuments(array($document));
                        
                        // set envelope status to "sent" to immediately send the signature request
                        //$envelop_definition->setStatus("sent");
                        $envelop_definition->setStatus("created");

                        // optional envelope parameters
                        $options = new \DocuSign\eSign\Api\EnvelopesApi\CreateEnvelopeOptions();
                        $options->setCdseMode(null);
                        $options->setMergeRolesOnDraft(null);

                        // create and send the envelope (aka signature request)

                        // $envelop_summary = $envelopeApi->createEnvelope($accountId, $envelop_definition, $options);
                        // if(!empty($envelop_summary))
                        // {
                        //     echo "$envelop_summary";
                        // }
                    }
                }
            }
        }
        catch (DocuSign\eSign\ApiException $ex)
        {
            echo "Exception: " . $ex->getMessage() . "\n";
        }
   // }
//}

?>