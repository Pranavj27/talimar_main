<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';

|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
 */

$route['default_controller'] = 'Home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['lender_td_details'] = 'Contact/lender_td_details'; //03-02-2021
$route['new_account_setup'] = 'Contact/new_account_setup'; //03-02-2021
$route['ajax_contactlist'] = 'Contact/ajax_contactlist'; //03-02-2021

$route['create_lender_user_mail'] = 'Contact/create_lender_user_mail';

$route['password_reset'] = 'profile/password_reset';

$route['logged_out'] = 'home/logged_out';

$route['form_profile_images'] = 'home/form_profile_images';

$route['form_profile_update'] = 'home/form_profile_update';

$route['forget_password'] = 'home/forget_password';

$route['forget_password_secure/(:any)'] = 'home/forget_password_secure/$1';

$route['form_create_newpassword'] = 'home/form_create_newpassword';

$route['loan_view'] = 'load_data/loan_view';
$route['borrower_view'] = 'borrower_data/view';
$route['borrower_view/(:any)'] = 'borrower_data/view/$1';
$route['delete_accured_id/(:any)'] = 'load_data/delete_accured_id';

$route['loan_property'] = 'load_data/loan_property';
$route['insert_new_add_property'] = 'load_data/insert_new_add_property';

$route['pay_off_processing_form'] = 'load_data/pay_off_processing_form';

$route['add_property'] = 'load_data/add_property';
$route['add_accured_charges'] = 'load_data/add_accured_charges';

$route['add_load_data_form'] = 'load_data/add_load_data_form';
$route['additional_loan'] = 'load_data/additional_loan';

// $route['add_vendor_data'] 						= 'load_data/add_vendor_data';
$route['form_wire_information'] = 'load_data/form_wire_information';
$route['update_active_boarding_fee_paid'] = 'load_data/update_active_boarding_fee_paid';

$route['add_borrower_data'] = 'borrower_data/add_borrower_data';

$route['borrower_contact'] = 'borrower_data/borrower_contact';

$route['add_borrower_contact'] = 'borrower_data/add_borrower_contact';

$route['view_borrower_data'] = 'borrower_data/view_borrower_data';

$route['form_encumbrances'] = 'load_data/form_encumbrances';

$route['form_encumbrances'] = 'load_data/form_encumbrances';

$route['form_closing_statement'] = 'load_data/form_closing_statement';

$route['form_underwriting_items'] = 'load_data/form_underwriting_items';

$route['load_data/(:any)'] = 'load_data/index/$1';
$route['insert_extension_request'] = 'load_data/insert_extension_request';

$route['loan_reserve_draws'] = 'load_data/loan_reserve_draws';

$route['form_monthly_payment'] = 'load_data/form_monthly_payment';

$route['form_escrow'] = 'load_data/form_escrow';

$route['delete_talimar_loan'] = 'load_data/delete_talimar_loan';

$route['update_multi_checkk'] = 'load_data/update_multi_checkk';

$route['form_loan_title'] = 'load_data/form_loan_title';
$route['print_lender_pdf/(:any)'] = 'load_data/print_lender_pdf/$1';
$route['print_lender_wire_schedule/(:any)'] = 'load_data/print_lender_wire_schedule/$1';

$route['form_recording_information'] = 'load_data/form_recording_information';

$route['delete_record_information'] = 'load_data/delete_record_information';

$route['loan_servicing_form'] = 'load_data/loan_servicing_form';

$route['loan_notes'] = 'load_data/loan_notes';

$route['loan_assigment'] = 'load_data/loan_assigment';

$route['draws_form'] = 'load_data/draws_form';

$route['estimate_lender_cost'] = 'load_data/estimate_lender_cost';

$route['form_ach_deposite'] = 'load_data/form_ach_deposite';

$route['lender_email'] = 'load_data/lender_email';

$route['form_loan_distibution'] = 'load_data/form_loan_distibution';

$route['form_comparable_sale'] = 'load_data/form_comparable_sale';

$route['form_property_images'] = 'load_data/form_property_images';

$route['form_property_user_images'] = 'load_data/form_property_user_images';

$route['form_loan_documents'] = 'load_data/form_loan_documents';

$route['form_create_album'] = 'load_data/form_create_album';

$route['form_album_images'] = 'load_data/form_album_images';

$route['Reserve_add_date'] = 'load_data/Reserve_add_date';
$route['add_reserve_data'] = 'load_data/add_reserve_data';
$route['add_checklist_value'] = 'load_data/add_checklist_value';


$route['delete_property_image/(:any)/(:any)'] = 'load_data/delete_property_image/$1/$2';

$route['form_fee_disbursement'] = 'load_data/form_fee_disbursement';

$route['additional_broker_data'] = 'load_data/additional_broker_data';

$route['form_wired_instruction'] = 'load_data/form_wired_instruction';

$route['form_impound_accounts'] = 'load_data/form_impound_accounts';

$route['form_closing_statement_items'] = 'load_data/form_closing_statement_items';

$route['update_all_assigment_data'] = 'load_data/update_all_assigment_data';

$route['updatedatabase_encumbrances'] = 'load_data/updatedatabase_encumbrances';

$route['delete_ecumbrance_id'] = 'load_data/delete_ecumbrance_id';

$route['load_ecumbrances_questions_ajax'] = 'load_data/load_ecumbrances_questions_ajax';

$route['form_sumbit_ecumbrance_data'] = 'load_data/form_sumbit_ecumbrance_data';

$route['load_ecumbrances_request_notice_ajax'] = 'load_data/load_ecumbrances_request_notice_ajax';

$route['form_sumbit_ecumbrance_request_notice'] = 'load_data/form_sumbit_ecumbrance_request_notice';

$route['form_loan_source_and_uses'] = 'load_data/form_loan_source_and_uses';

$route['delete_comparable_sale_id'] = 'load_data/delete_comparable_sale_id';

$route['borrower_data_loading'] = 'load_data/borrower_data_loading';
$route['add_contact_notes'] = 'load_data/add_contact_notes';

$route['edit_loan_contact_notes'] = 'load_data/edit_loan_contact_notes';
$route['add_affiliated_parties'] = 'load_data/add_affiliated_parties';
$route['loan_servicing_default_status'] = 'load_data/loan_servicing_default_status';
$route['additional_forecloser'] = 'load_data/additional_forecloser';

$route['form_wire_information2'] = 'load_data/form_wire_information2';

$route['loan_servicing_contacts'] = 'load_data/loan_servicing_contacts';
$route['add_payment_gurrantor'] = 'load_data/add_payment_gurrantor';

$route['form_wait_list'] = 'New_loan_data/form_wait_list';
$route['load_ajax_wait_list_data'] = 'New_loan_data/load_ajax_wait_list_data';
$route['remove_table_wait_lists'] = 'New_loan_data/remove_table_wait_lists';
$route['auto_debit_payment'] = 'New_loan_data/auto_debit_payment';

$route['form_property_home_page'] = 'Property_data/form_property_home_page';
$route['delete_property_home_id'] = 'Property_data/delete_property_home_id';
$route['form_monthly_income_expense'] = 'Property_data/form_monthly_income_expense';
$route['delete_monthly_income_row'] = 'Property_data/delete_monthly_income_row';
$route['delete_monthly_expense_row'] = 'Property_data/delete_monthly_expense_row';
$route['add_property_insurance'] = 'Property_data/add_property_insurance';
$route['form_property_taxes'] = 'Property_data/form_property_taxes';
$route['form_upload_property_images'] = 'Property_data/form_upload_property_images';
$route['delete_property_images'] = 'Property_data/delete_property_images';
$route['redirect_to_property_close/(:any)'] = 'Property_data/redirect_to_property_close/$1';
$route['form_images_folder'] = 'Property_data/form_images_folder';
$route['property_hidden_form'] = 'Property_data/property_hidden_form';
$route['print_insurence/(:any)'] = 'Property_data/print_insurence/$1';
$route['property_insurance_doc'] = 'Property_data/property_insurance_docs';
$route['form_loan_folder'] = 'Property_data/form_loan_folder';
$route['form_upload_reserve_images'] = 'Property_data/form_upload_reserve_images';
$route['delete_property_images'] = 'Property_data/delete_property_images';
$route['loan_overview/(:any)'] = 'loan_overview/index/$1';

//project routes...
$route['projects'] = 'ProjectData/projects';


// investor_data routes

$route['add_investor_data'] = 'investor_data/add_investor_data';

$route['investor_data/(:any)'] = 'investor_data/index/$1';
$route['investor_data/(:any)/(:any)'] = 'investor_data/index/$1/$2';

$route['investor_view'] = 'investor_data/view';
$route['investor_view/(:any)'] = 'investor_data/view/$1';
$route['vendor_data/(:any)'] = 'vendor_data/index/$1';
$route['vendor_data/(:any)/(:any)'] = 'vendor_data/index/$1/$2';

$route['delete_investor/(:any)'] = 'investor_data/delete_investor/$1';

$route['delete_contact_f/(:any)'] = 'investor_data/delete_contact_f/$1';

$route['delete_contact_s/(:any)'] = 'investor_data/delete_contact_s/$1';

$route['delete_lender_contact/(:any)'] = 'investor_data/delete_lender_contact/$1';
$route['lender_account_setup/(:any)'] = 'investor_data/lender_account_setup/$1';

$route['borrower_data/(:any)'] = 'borrower_data/index/$1';
$route['borrower_data/(:any)/(:any)'] = 'borrower_data/index/$1/$2';

$route['delete_borrower/(:any)'] = 'borrower_data/delete_borrower/$1';
$route['delete_borrower_contacts/(:any)/(:any)'] = 'borrower_data/delete_borrower_contacts/$1/$2';

$route['delete_borrower_s/(:any)'] = 'borrower_data/delete_borrower_s/$1';

$route['delete_borrower_f/(:any)'] = 'borrower_data/delete_borrower_f/$1';

$route['delete_borrower_contact/(:any)'] = 'borrower_data/delete_borrower_contact/$1';

$route['vendor-primary'] = 'vendor_data/vendor_table';
$route['add_vendor_data'] = 'vendor_data/add_vendor_data';
$route['delete_vendor_data/(:any)'] = 'vendor_data/delete_vendor_data/$1';

$route['vendor_data_view'] = 'vendor_data/vendor_data_view';
$route['vendor_data_view/(:any)'] = 'vendor_data/vendor_data_view/$1';
$route['vendor_data_view/(:any)/(:any)'] = 'vendor_data/vendor_data_view/$1/$2';

$route['existing_loan_schdule'] = 'reports/existing_loan_schdule';
$route['new_report'] = 'reports/new_report';

$route['property_insurance'] = 'reports/property_insurance';
$route['lender_negative_spread'] = 'reports/lender_negative_spread';
$route['social_media'] = 'reports/social_media';

$route['download_existing_loan'] = 'reports/download_existing_loan';
$route['download_lender_loan_pdf'] = 'reports/download_lender_loan_pdf';

$route['download_social_media'] = 'reports/download_social_media';
$route['download_property_insurance'] = 'reports/download_property_insurance';

$route['paid_of_loan_schedule'] = 'reports/paid_of_loan_schedule';

$route['download_paidoff_loan'] = 'reports/download_paidoff_loan';

$route['maturity_report_schedule'] = 'reports/maturity_report_schedule';

$route['assigment_in_progress'] = 'reports/assigment_in_progress';
$route['loan_servicer'] 		= 'reports/loanschedule_servicer';
$route['download_servicer'] 		= 'reports/download_loanschedule_servicer';

$route['download_assigment'] = 'reports/download_assigment';

$route['loan_boarding_schedule'] = 'reports/loan_boarding_schedule';

$route['monthly_servicing_income'] = 'reports/monthly_servicing_income';

$route['download_monthly_income'] = 'reports/download_monthly_income';

$route['download_maturity_reports'] = 'reports/download_maturity_reports';

$route['loan_schedule_borrower'] = 'reports/loan_schedule_borrower';
$route['new_report_pdf'] = 'reports/new_report_pdf';

$route['loan_portfolio_schedule'] = 'reports/loan_portfolio_schedule';
$route['download_loan_portfolio_schedule'] = 'reports/download_loan_portfolio_schedule';
$route['lender_schedule_contact'] = 'reports/lender_schedule_contact';

$route['download_boarding_schedule'] = 'reports/download_boarding_schedule';
$route['download_negative_spread'] = 'reports/download_negative_spread';

$route['download_boarding_schedule'] = 'reports/download_boarding_schedule';

$route['download_loan_schedule_borrower'] = 'reports/download_loan_schedule_borrower';

$route['fci_service_loan'] = 'reports/fci_service_loan';
$route['report_by_year'] = 'reports/report_by_year';

$route['master_borrower_schedule'] = 'reports/master_borrower_schedule';

$route['loan_schedule_investor'] = 'reports/loan_schedule_investor';
//$route['loan_schedule_investor/(:any)'] = 'reports/loan_schedule_investor/$1';

$route['master_investor_schedule'] = 'reports/master_investor_schedule';
$route['re881_disclosure_data'] = 'reports/re881_disclosure_data';

$route['trust_deed_schedule'] = 'reports/trust_deed_schedule';

$route['loan_default_schedule'] = 'reports/loan_default_schedule';

$route['download_trust_deed_excel'] = 'reports/download_trust_deed_excel';

$route['download_assigment_excel'] = 'reports/download_assigment_excel';

$route['loan_income_report'] = 'reports/loan_income_report';

$route['loan_income_report_pdf'] = 'reports/loan_income_report_pdf';

$route['download_master_lender_excel'] = 'reports/download_master_lender_excel';

$route['master_borrower_excel'] = 'reports/master_borrower_excel';

$route['download_fci_serviced_account'] = 'reports/download_fci_serviced_account';

$route['download_master_lender_pdf'] = 'reports/download_master_lender_pdf';

$route['master_borrower_pdf'] = 'reports/master_borrower_pdf';

$route['master_borrower_pdf/(:any)'] = 'reports/master_borrower_pdf/$1';

$route['download_fci_serviced_pdf'] = 'reports/download_fci_serviced_pdf';

$route['download_trust_deed_pdf'] = 'reports/download_trust_deed_pdf';

$route['download_loan_default'] = 'reports/download_loan_default';

$route['download_trust_deed_lender'] = 'reports/download_trust_deed_lender';

$route['download_trust_deed_lender/(:any)'] = 'reports/download_trust_deed_lender/$1';

$route['loan_funded_by_year'] = 'reports/loan_funded_by_year';

$route['accrued_charges'] = 'reports/accrued_charges';
$route['download_accrued_charges'] = 'reports/download_accrued_charges';

$route['closing_verification'] = 'reports/closing_verification';
$route['pdf_closing_verification'] = 'reports/pdf_closing_verification';

$route['diligence_schedule'] = 'reports/diligence_schedule';
$route['pdf_diligence_schedule'] = 'reports/pdf_diligence_schedule';

$route['master_loan_schedule'] = 'reports/master_loan_schedule';
$route['download_master_loan'] = 'reports/download_master_loan';

$route['loan_funded_pdf'] = 'reports/loan_funded_pdf';

$route['loan_funded_pdf/(:any)'] = 'reports/loan_funded_pdf/$1';

$route['lender_loan_check'] = 'reports/lender_loan_check';

$route['del_toro_serviced_account'] = 'reports/del_toro_serviced_account';

$route['download_del_toro_pdf'] = 'reports/download_del_toro_pdf';

$route['re870_audit_report'] = 'reports/re870_audit_report';

$route['download_re840_audit_pdf'] = 'reports/download_re840_audit_pdf';

$route['active_lender_data'] = 'reports/active_lender_data';

$route['download_lender_active_data'] = 'reports/download_lender_active_data';

$route['loan_funded_year'] = 'reports/loan_funded_year';
$route['existing_loan_schedule'] = 'reports/existing_loan_schedule';

$route['renovation_balance_pdf'] = 'reports/renovation_balance_pdf';

$route['loan_funded_year_pdf'] = 'reports/loan_funded_year_pdf';
$route['re881_disclosure_pdf'] = 'reports/re881_disclosure_pdf';

$route['renovation_balance_report'] = 'reports/renovation_balance_report';

$route['peer_street_schedule'] = 'reports/peer_street_schedule';

$route['construction_loan_reports'] = 'reports/construction_loan_reports';

$route['construction_loan_pdf'] = 'reports/construction_loan_pdf';

$route['peer_street_pdf'] = 'reports/peer_street_pdf';
$route['peer_street_csv'] = 'reports/peer_street_csv';
$route['delete_vendor/(:any)'] = 'vendor_data/delete_vendor/$1'; //05-21-2021

$route['trust_investor_schedule'] = 'reports/trust_investor_schedule';

$route['trust_investor_schedule_pdf'] = 'reports/trust_investor_schedule_pdf';

$route['master_lender_schedule'] = 'reports/master_lender_schedule';
$route['lender_loan_schedule'] = 'reports/lender_loan_schedule';
$route['self_directed_IRA_lenders'] = 'reports/self_directed_IRA_lenders';

$route['self_directed_IRA_lenders_download'] = 'reports/self_directed_IRA_lenders_download';

$route['cancelled_loan_schedule'] = 'reports/cancelled_loan_schedule';

$route['download_pdf_cancelled_loan'] = 'reports/download_pdf_cancelled_loan';
$route['upcoming_payoff_schedule'] = 'reports/upcoming_payoff_schedule';
$route['upcoming_payoff_schedule_download'] = 'reports/upcoming_payoff_schedule_download';
$route['employment_database_goal'] = 'reports/employment_database_goal';
$route['employment_database_goal_pdf'] = 'reports/employment_database_goal_pdf';
$route['lender_schedule_by_contact'] = 'reports/lender_schedule_by_contact';
$route['download_lender_schedule_by_contact'] = 'reports/download_lender_schedule_by_contact';

$route['Deliquent_property_taxes'] = 'reports/Deliquent_property_taxes';
$route['Deliquent_property_taxes_download'] = 'reports/Deliquent_property_taxes_download';
$route['Outstanding_lender_fees'] = 'reports/Outstanding_lender_fees';
$route['Outstanding_lender_fees_download'] = 'reports/Outstanding_lender_fees_download';
$route['Outstanding_draws_report'] = 'reports/Outstanding_draws_report';
$route['outstanding_draw_report_pdf'] = 'reports/outstanding_draw_report_pdf';

$route['contact_schedule_by_borrower'] = 'reports/contact_schedule_by_borrower';
$route['download_borrower_schedule'] = 'reports/download_borrower_schedule';

$route['property_loan_schedule'] = 'reports/property_loan_schedule';
$route['property_loan_schedule_pdf'] = 'reports/property_loan_schedule_pdf';

$route['auto_reminder'] = 'reports/auto_reminder';
$route['download_auto_reminder'] = 'reports/download_auto_reminder';

$route['investor_tape'] = 'reports/investor_tape';
$route['investor_tape_excel'] = 'reports/investor_tape_excel';
$route['outstanding_servicer_excel'] = 'reports/outstanding_servicer_excel';

$route['lender_filter'] = 'reports/lender_filter';
$route['daily_report'] = 'reports/daily_report';
$route['download_daily_report'] = 'reports/download_daily_report';

$route['loan_servicing_income_monthly_yearly'] = 'reports/loan_servicing_income_monthly_yearly';
$route['download_servicing_income_monthly_yearly'] = 'reports/download_servicing_income_monthly_yearly';

$route['prospective_investor_schedule'] = 'reports/prospective_investor_schedule';
$route['download_prospective_investors'] = 'reports/download_prospective_investors';

$route['lender_sales_list'] = 'reports/lender_sales_list';
$route['download_lender_sales_list'] = 'reports/download_lender_sales_list';
$route['lender_contact_list'] = 'reports/lender_contact_list';
$route['download_lender_contact_list'] = 'reports/download_lender_contact_list';

$route['sarah_prospectss'] = 'reports/sarah_prospectss';
$route['multi_lender_loan'] = 'reports/multi_lender_loan';
$route['multilender_loan_pdf'] = 'reports/multilender_loan_pdf';
$route['download_sarah_pros_con_list'] = 'reports/download_sarah_pros_con_list';

$route['prior_year_comparison'] = 'reports/prior_year_comparison';
$route['prior_year_comparison_pdf'] = 'reports/prior_year_comparison_pdf';

$route['report_loan_servicing_status'] = 'reports/report_loan_servicing_status';
$route['download_report_loan_servicing_status'] = 'reports/download_report_loan_servicing_status';

$route['recent_payoffs'] = 'reports/recent_payoffs';
$route['download_recent_payoffs'] = 'reports/download_recent_payoffs';

$route['new_lender_report'] = 'reports/new_lender_report';
$route['download_new_lender_report'] = 'reports/download_new_lender_report';
$route['report_loan_servicing_status'] = 'reports/report_loan_servicing_status';

$route['print_loan_document'] = 'print_document/print_loan_document';

$route['read_docx_file'] = 'print_document/read_docx_file';

$route['assigment_print_document'] = 'print_document/assigment_print_document';

$route['download_del_toro_excel_file'] = 'print_document/download_del_toro_excel_file';

$route['users'] = 'users/index';

$route['form_user'] = 'users/form_user';

$route['form_edit_users'] = 'users/form_edit_users';

$route['user_login_detail'] = 'users/user_login_detail';
$route['settings/(:any)'] = 'users/user_settings/$1';

$route['contact'] = 'Contact/index';
$route['viewcontact'] = 'Contact/contact';
$route['contactlist'] = 'Contact/contactlist';
$route['contactlist/(:any)/(:any)'] = 'Contact/contactlist/$1/$2';
$route['contact_notes/(:any)'] = 'Contact/contact_notes/$1';
$route['fetch_notes/'] = 'Contact/fetch_notes';
$route['updates_contacts_data'] = 'Contact/updates_contacts_data';
$route['delete_contact'] = 'Contact/delete_contact';
$route['delete_notes/(:any)/(:any)'] = 'Contact/delete_notes/$1/$2';
$route['contact/(:any)'] = 'Contact/index/$1';
$route['viewcontact/(:any)'] = 'Contact/contact/$1';
$route['add_contact_task'] = 'Contact/add_contact_task';
$route['edit_contact_task'] = 'Contact/edit_contact_task';
$route['add_note_by_contact'] = 'Contact/add_note_by_contact';
$route['task_view'] = 'Contact/task_view';
$route['contact_p_notes'] = 'Contact/contact_p_notes';
$route['contact_email_get'] = 'Contact/contact_email_get';
$route['contact_get'] = 'Contact/contact_get';
$route['lender_data_print/(:any)'] = 'Contact/lender_data_print/$1';
$route['print_all_task'] = 'Contact/print_all_task';

$route['print_contcat_stats/(:any)'] = 'Contact/print_contcat_stats/$1';
$route['contact_stats_borrow/(:any)'] = 'Contact/contact_stats_borrow/$1';
$route['contact_stats_lender/(:any)'] = 'Contact/contact_stats_lender/$1';

$route['uptade_title_lender'] = 'Investor_data/uptade_title_lender';

$route['graph_year'] = 'Graph/graph_year';

$route['edit_users/(:any)'] = 'users/edit_users/$1';
$route['credit_line'] = 'Fund_data/index';
$route['add_update_data'] = 'Fund_data/add_update_data';
$route['affiliated_trancation'] = 'reports/affiliated_trancation';
$route['download_affiliated_trancation'] = 'reports/download_affiliated_trancation';


$route['auto_mail/loan_payment_reminder'] = 'Auto_mail/loan_payment_reminder';
$route['auto_mail/borrower_auto_mails'] = 'Auto_mail/borrower_auto_mails';
$route['auto_mail/auto_trigger_mail_status'] = 'Auto_mail/auto_trigger_mail_status';
$route['auto_mail/borrower_reminder_track'] = 'Auto_mail/borrower_reminder_track';
$route['auto_mail/loan_reminder_track'] = 'Auto_mail/loan_reminder_track';
$route['auto_mail/delete_mail_contact'] = 'Auto_mail/delete_mail_contact';

$route['mortgageFund'] = 'MortgageFound/index';
$route['mortgageFund/editDetails/(:any)'] = 'MortgageFound/editDetails/$1';

//report modifications
$route['Reports/ajax_loan_schedule(:any)'] = 'Reports/ajax_loan_schedule/$1';

$route['task_view_list'] = 'Task/task_view';
$route['add_task/(:any)'] = 'Task/add_task/$1';
$route['add_task/(:any)/(:any)'] = 'Task/add_task/$1/$2';
$route['add_contact_note/(:any)/(:any)'] = 'Task/add_contact_note/$1/$2';

/*23-07-2021 By@Aj*/
$route['add_wholesale_deal/(:any)/(:any)'] = 'Wholesale/add_wholesale_deal/$1/$2';

/*
    Testing Report Create For Test
*/

$route['test_report'] = 'reports/test_report';
$route['studentlist'] = 'Contact/studentlist';
?>