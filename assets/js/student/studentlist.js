function init_datatable_for_student(){
	var dispatching_list_all = '#ajax_table_studentlist';
	$(dispatching_list_all).DataTable().clear().destroy();    

	if( $( dispatching_list_all ).length ){		
		$dis_list_all = $( dispatching_list_all ).DataTable({
			processing: true,
			serverSide: true,
			pageLength: 25,
		    ajax: {
				dataSrc:"data",
				data:function(data){
					// console.log(data);
				},
				url: '/Pranav/Talimar/Contact/ajax_studentlist',
				dataFilter: function(data){
					// filter_tag_add();
				 //    var json = JSON.parse( data );				    
				 //    json.recordsTotal = json.total;
				 //    json.recordsFiltered = json.total;
				 //    json.data = json.data;			    
				 //    return JSON.stringify( json );
				}
		    },
		    columns: [				        
		        { data: 'id' },
		        { data: 'name' },
		        { data: 'age' },
		        { data: 'mobileno' },				        
		        { data: 'action' }
		    ],
		    "aoColumnDefs": [
		        { "bSortable": false, "aTargets": [ 3, 5, 6, 7 ] }, 
		        { "bSearchable": false, "aTargets": [ 2,3,4,5,6,7 ] }
		    ],
		    order: [[ 4, "ASC" ]],
		    "language": { loadingRecords: '&nbsp;&nbsp;', processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
		    drawCallback: function( settings ) {
		        
		    }
		});
	}
}

$(document).ready(function() {
	init_datatable_for_student();
});