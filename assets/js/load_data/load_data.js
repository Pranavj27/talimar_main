function ServicerChargesProVal(date, paidfrom, paidto, description, invoice, amount, FeesOpt, charges_id){

	$('#ServicerCharge_date').val(date);
	$('#ServicerCharge_paidfrom').val(paidfrom);
	$('#ServicerCharge_paidto').val(paidto);
	$('#ServicerCharge_description').val(description);
	$('#ServicerCharge_invoice').val(invoice);
	$('#ServicerCharge_amount').val(amount);
	$('#BoardingFeesOpt').val(FeesOpt);
	$('#ServicerCharge_charges_id').val(charges_id);
}

function appraisal_document_del(mn){
	var name = doc_id = mn.id;
	if(confirm('Are You Sure To Delete?')){
		$.ajax({			
			type	:'POST',
			url		:'/Pranav/Talimar/Pranav/Talimar/Load_data/delete_appraisal_document',
			data	:{'id':doc_id},
			success : function(response){
				if(response == 1){
					$('table#appraisal_loan_doc_table tr[attrremove="'+name+'"]').remove();
				}
			}				
		});
	}
}

$(document).ready(function(){

	$('body').on('click', '#ct_appraisal_documentadd', function(){
   		$(".ct_appraisal_document").toggle('slow');
    });

	$('.delete_ServicerCharges').click(function(){
		var charges_id = $(this).attr('charges_id');
		swal({
			  title: "Are you sure?",
			  icon: "warning",
			  buttons: true,
			  dangerMode: true,
			})
			.then((willDelete) => {
			  if (willDelete) {
			  	$('#servicer_charge_delete_id').val(charges_id);
			  	setTimeout(function(){
			  		$('#servicer_charge_delete').submit();
			  	}, 1000);
			  }
			});
	});
	$('#ServicerCharge_date').datepicker({ dateFormat: 'mm-dd-yy' });
	$('.edit_ServicerCharges').click(function(){
		var date    	= $(this).attr('date');
		var paidfrom    = $(this).attr('paidfrom');
		var paidto    	= $(this).attr('paidto');
		var description = $(this).attr('description');
		var invoice     = $(this).attr('invoice');
		var amount    	= $(this).attr('fee');
		var FeesOpt    	= $(this).attr('feesOpt');
		var charges_id 	= $(this).attr('charges_id');

		$('.form_loan_boarding_data').toggle('slow');

		setTimeout(function(){
			if (!$(".form_loan_boarding_data").is(":visible")) {
				date    	= '';
				paidfrom    = '';
				paidto    	= '';
				description = '';
				invoice     = '';
				amount    	= ''; 
				FeesOpt    	= '';
				charges_id 	= 0;
			}
			ServicerChargesProVal(date, paidfrom, paidto, description, invoice, amount, FeesOpt, charges_id);
		}, 1000);
	});
	$('.close_ServicerCharges').click(function(){
		var date    	= '';
		var paidfrom    = '';
		var paidto    	= '';
		var description = '';
		var invoice     = '';
		var amount    	= ''; 
		var FeesOpt    	= '';
		var charges_id 	= 0;
		ServicerChargesProVal(date, paidfrom, paidto, description, invoice, amount, FeesOpt, charges_id);
		$('.form_loan_boarding_data').hide('slow');
	});
	$('#addServicerCharges').click(function(){
		var date    	= '';
		var paidfrom    = '';
		var paidto    	= '';
		var description = '';
		var invoice     = '';
		var amount    	= ''; 
		var FeesOpt    	= '';
		var charges_id 	= 0;
		ServicerChargesProVal(date, paidfrom, paidto, description, invoice, amount, FeesOpt, charges_id);
		$('.form_loan_boarding_data').show('slow');
	});

	$('body').on('click', '#ct_loan_saved_documentadd', function(){
		$(".ct_loan_saved_document").toggle('slow');
	});

	$('body').on('click', 'input.complete_checklist_id', function(){
		completeCheckList_id();
	});

	$('body').on('click', '#ct_loan_saved_documentaddn', function(){
		$(".ct_loan_saved_documenta").toggle('slow');
	});
	
	/*setTimeout(function(){ completeCheckList_id(); }, 2000);*/

	$('.delete_property_content').click(function(){
		swal({
		  title: "Are you sure?",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
		  if (willDelete) {
		  	var property_id = $(this).attr('property_id');
			$('#remove_property_home_page-'+property_id+'').submit();		    
		  }
		});
	});
}); 

function delete_DiligenceDocuments(that){
	var doc_id = that.id;
	if(confirm('Are You Sure To Delete?')){
		$.ajax({
			type	:'POST',
			url		:'/Pranav/Talimar/Load_data/delete_capital_diligence_documents',
			data	:{'id':doc_id},
			success : function(response){
				if(response == 1){
					$(that).parent().parent('li').remove();
					window.location.reload();
				}
			}

		});
	}
}

function completeCheckList_id(){
	if($('input.complete_checklist_id:checked').length == 6){
	  $('.checklist_single_Complete input').removeAttr('disabled');
	  $('.checklist_single_Complete .checker').removeClass('disabled');
	}else{
	  $('.checklist_single_Complete input[type="checkbox"]').prop("checked", false);
	  $('.checklist_single_Complete input').attr('disabled', true);
	  $('.checklist_single_Complete .checker').addClass('disabled');
	}
}
function AutoNotifications(NotificationType, NotificationValue, load_id){	
	$.ajax({
		type : 'POST',
		url : '/Pranav/Talimar/Load_data/auto_notifications_setting',
		data : {'NotificationType':NotificationType,'NotificationValue':NotificationValue,'load_id':load_id},
		success : function(response){
			if(response == 1){
				swal("Notifications setting updated successfully!", {icon: "success",});
			}else{
				swal("Some error occurred please try again!", {icon: "error",});
			}
		}
	});

}
/* Loan Services */
function drawLoanServices(that,num){
	if($(that).is(':checked')){
		 $('input#requestLoan_'+num).val('1');
		 var loan_id = $('input[name="loan_id"]').val();
		 var total_amount = $('input[id="requestLoan_'+num+'"]').attr('total_amount');
		 var loan_borrower_id = $('input[name="loan_borrower_id"]').val();
		 $.ajax({
			type : 'POST',
			url : '/Pranav/Talimar/Load_data/email_draw_loan_services',
			data : {'amount':total_amount,'loan_id':loan_id,'loan_borrower_id':loan_borrower_id},
			success : function(response)
			{
			}
		});
	}
	else
	{
		$('input#requestLoan_'+num).val('0');
	}
}

function delete_reserve_draw(that)
{
	var id 	= $(that).attr('id');
	if(confirm("Are you sure to delete")){
		$.ajax({
			type 	: 'POST',
			url 	: '/Pranav/Talimar/Load_data/delete_reseve',
			data 	: { "id" : id },
			success	: function(response){
				if(response=='1'){
					$(that).parents('tr').remove();
				}

			}
		});
	}
}

function hide_show(t)
{
	var id=$(t).val();
	var text=$(t).parent().find('input[name="hide"]').val();
	if(id =='14')
	{
		$('#result').val(text);
	}else{
		$('#result').val("");
	}
}

function redirect_to_page(page)
{
	if(page == 'borrower')
	{
		$('form#redirect-borrower-view input').attr('disabled',false);
		$('form#redirect-borrower-view').submit();
	}
}

function construction_reserve_readonly_css_setting1(that)
{
	var request_submitted = $(that).parent().parent('tr').find('input[name="request_submitted[]"]').val();
	var funds_disbursed = $(that).parent().parent('tr').find('input[name="funds_disbursed[]"]').val();
	if(request_submitted == '1' || funds_disbursed == '1')
	{
		$(that).parent().parent('tr').find('input').attr('readonly',true);
		$(that).parent().parent('tr').find('textarea:not([name="draws_notes[]"])').attr('readonly',true);
		$(that).parent().parent('tr').find('input').css('background','#dedede');
		$(that).parent().parent('tr').find('input').css('border','1px solid darkgrey');
		$(that).parent().parent('tr').find('textarea:not([name="draws_notes[]"])').css('background','#dedede');

	}
	else
	{
		$(that).parent().parent('tr').find('input').attr('readonly',false);
		$(that).parent().parent('tr').find('textarea').attr('readonly',false);
		$(that).parent().parent('tr').find('input').css('background','none');
		$(that).parent().parent('tr').find('input').css('border','1px solid darkgrey');
		$(that).parent().parent('tr').find('textarea').css('background','none');
	}
}

function construction_reserve_readonly_css_setting(that)
{
	var request_submitted = $(that).parent().parent().parent().parent('tr').find('input[name="request_submitted[]"]').val();
	var funds_disbursed = $(that).parent().parent().parent().parent('tr').find('input[name="funds_disbursed[]"]').val();
	if(request_submitted == '1' || funds_disbursed == '1')
	{
		$(that).parent().parent().parent().parent('tr').find('input').attr('readonly',true);
		$(that).parent().parent().parent().parent('tr').find('textarea:not([name="draws_notes[]"])').attr('readonly',true);
		$(that).parent().parent().parent().parent('tr').find('input').css('background','#dedede');
		$(that).parent().parent().parent().parent('tr').find('input').css('border','1px solid darkgrey');
		$(that).parent().parent().parent().parent('tr').find('textarea:not([name="draws_notes[]"])').css('background','#dedede');
	}
	else
	{
		$(that).parent().parent().parent().parent('tr').find('input').attr('readonly',false);
		$(that).parent().parent().parent().parent('tr').find('textarea').attr('readonly',false);
		$(that).parent().parent().parent().parent('tr').find('input').css('background','none');
		$(that).parent().parent().parent().parent('tr').find('input').css('border','1px solid darkgrey');
		$(that).parent().parent().parent().parent('tr').find('textarea').css('background','none');
	}
}

function construction_reserve_request_submitted(that)
{
	if($(that).is(':checked'))
	{
		$(that).parent().parent().parent().parent('tr').find('input[name="request_submitted[]"]').val('1');
		construction_reserve_readonly_css_setting(that);
	}
	else
	{
		$(that).parent().parent().parent().parent('tr').find('input[name="request_submitted[]"]').val('0');
		construction_reserve_readonly_css_setting(that);
	}
}

function construction_reserve_funds_disbursed(that)
{
	if($(that).is(':checked'))
	{
		$(that).parent().parent().parent().parent('tr').find('input[name="funds_disbursed[]"]').val('1');
		construction_reserve_readonly_css_setting(that);
	}
	else
	{
		$(that).parent().parent().parent().parent('tr').find('input[name="funds_disbursed[]"]').val('0');
		construction_reserve_readonly_css_setting(that);
	}
}

function loan_source_and_uses_change(that)
{
	var project_cost 	= $(that).parent().parent('tr').find('input[name="project_cost[]"]').val();
	var borrower_funds 	= $(that).parent().parent('tr').find('input[name="borrower_funds[]"]').val();
	var loan_funds 		= $(that).parent().parent('tr').find('input[name="loan_funds[]"]').val();

	project_cost = replace_dollar(project_cost);
	if(project_cost != '')
	{
		project_cost = parseFloat(replace_dollar(project_cost));
	}
	else
	{
		project_cost = 0;
	}

	loan_funds = replace_dollar(loan_funds);
	/*alert(loan_funds);*/
	if(loan_funds != '')
	{
		loan_funds = parseFloat(replace_dollar(loan_funds));
	}
	else
	{
		loan_funds = 0;
	}

	/*calculate borower funds*/
	borrower_funds = project_cost - loan_funds;

	$(that).parent().parent('tr').find('input[name="project_cost[]"]').val('$'+number_format(project_cost));

	$(that).parent().parent('tr').find('input[name="borrower_funds[]"]').val('$'+number_format(borrower_funds));

	$(that).parent().parent('tr').find('input[name="loan_funds[]"]').val('$'+number_format(loan_funds));

	/*calculate total project_cost*/
	var sum = 0;
    $('table#table_loan_source_and_uses tbody input[name="project_cost[]"]').each(function(){

		var check = replace_dollar($(this).val());
		if($(this).val() && check != '')
		{
			sum += + parseFloat(replace_dollar($(this).val()));
		}
    });
	$('table#table_loan_source_and_uses tfoot th#total_project_cost').text('$'+number_format(sum));

	/*calculate total project_cost*/
	var sum = 0;
    $('table#table_loan_source_and_uses tbody input[name="borrower_funds[]"]').each(function(){
		var check = replace_dollar($(this).val());
		if($(this).val() && check != '')
		{
			sum += + parseFloat(replace_dollar($(this).val()));
		}
    });
	$('table#table_loan_source_and_uses tfoot th#total_borrower_funds').text('$'+number_format(sum));
	/*calculate total project_cost*/
	var sum = 0;
    $('table#table_loan_source_and_uses tbody input[name="loan_funds[]"]').each(function(){
		var check = replace_dollar($(this).val());
		if($(this).val() && check != '')
		{
			sum += + parseFloat(replace_dollar($(this).val()));
		}
    });
	$('table#table_loan_source_and_uses tfoot th#total_loan_funds').text('$'+number_format(sum));
}

function search_loan_by()
{
	$('#form_search_loan_by').submit();
}
function draws_datepicker(that)
{
	$(that).datepicker({
		dateFormat : 'mm-dd-yy'
	});
	$(that).parent().parent().parent().find("input[name='draws_require[]']").focus();
	$(that).focus();
}

function draws_amount_format(that)
{
	var a = that.value;
	var a = replace_dollar(a);
	if(a == '')
	{
		a = 0;
	}
	a = parseFloat(a);
	$(that).val('$'+number_format(a))
}

function checkbox_draws_release(a)
{
	var result = a.value;
	if($(a).attr('checked'))
	{
		$(a).parent().parent().parent().find("input[name='draws_release[]']").val('1');
	}
	else
	{
		$(a).parent().parent().parent().find("input[name='draws_release[]']").val('0');
	}
}

function calculate_maturity_value()
{
	var doc_date 	= $('#loan_funding_date').val();
	var month 		= $('#term_month').val();
	$.ajax({
		type 	: 'POST',
		url 	: '/Pranav/Talimar/ajax_call/ajax_calculate_maturity_date',
		data 	: { "doc_date" : doc_date , "month" : month },
		success	: function(result){
			$('#maturity_date').val(result);
		}
	});
}



function replace_dollar(n)
{
	if(n){
		var a = n.replace('$', '');
		var b = a.replace(',', '');
		var b = b.replace(',', '');
		var b = b.replace(',', '');
		var b = b.replace(',', '');
		return b;
	}
}

function replace_percent(n)
{
	var a = n.replace('%', '');
	var b = a.replace(',', '');
	var b = b.replace(',', '');
	var b = b.replace(',', '');
	return b;
}

function auto_cal_payment_amount()
{
	var rate 			= parseFloat($('#intrest_rate').val());
	var loan_amount 	= $('#loan_amount').val();
	loan_amount 		= replace_dollar(loan_amount);
	var result  = 0 + (rate/100) * parseFloat(loan_amount) / 12;
	var result_a  = number_format(parseFloat(result));
	$('#payment_amount').val('$'+result_a);
}

function number_format(n) {
    return n.toFixed(2).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}
function number_format_rate(n) {
    return n.toFixed(3).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "" + c : c;
    });
}
function number_format_percent_loan(n) {
    return n.toFixed(8).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "" + c : c;
    });
}





function extention_select(val)
{
	/*alert(val);*/
	if(val == '1')
	{
		$(".extention_select_yes input#textinput").attr("disabled" , false);
		$(".extention_select_yes input#textinput").css("background-color" , '#ffffff');
	}
	else
	{
		$(".extention_select_yes input#textinput").attr("disabled" , true);
		$(".extention_select_yes input#textinput").css("background-color" , '#eeeeee');
		$(".extention_select_yes input#textinput").val('');
	}
}
/* for show div if select yes in minimum interst*/
function minimum_select(val)
{
	/*alert(val);*/
	if(val == '1')
	{
		$("#minimum_select_yes").css("display" , "block");
		$("#min_bal_type_option").css("display" , "block");
		$("#min_intrest_month").focus();
	}
	else
	{
		$("#minimum_select_yes").css("display" , "none");
		$("#min_bal_type_option").css("display" , "none");
	}
}

function add_new_images(){
	$('#image_section_div').css('display','block');
	$("div#image_section_div div.image_main_section").clone().insertAfter("div.images_tab_section div.image_main_section:last");
	$('#image_section_div').css('display','none');
}

function add_new_images2(){
	$('#image_section_div').css('display','block');
	$("div#image_section_div div.image_main_section").clone().insertAfter("div.images_tab_section2 div.image_main_section:last");
	$('#image_section_div').css('display','none');
}

function add_new_file(){
	$('#loan_document_div').css('display','block');
	$("div#loan_document_div div.image_main_section").clone().insertAfter("div.loan_document_tab div.image_main_section:last");
	$('#loan_document_div').css('display','none');
}

function change_this_image(that)
{
   if (that.files && that.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            /*$(that).parent().parent().find('.images_part img').css('background','red');*/
             $(that).parent().parent().find('.images_part img').attr('src', e.target.result).width('100%').height(175);
        };
        reader.readAsDataURL(that.files[0]);
    }
}

function draw_released(that){
	var id = that.id;
	/*alert(id);*/
	if($(that).is(':checked')){
		$('input#d_released_'+id).val('1');
	}else{
		$('input#d_released_'+id).val('0');
	}
}

function draw_request(that){
	var id = that.id;
	/*alert(id);*/
	if($(that).is(':checked')){
		$('input#d_requested_'+id).val('1');
	}else{
		$('input#d_requested_'+id).val('0');
	}
}

function draw_approved(that){
	var id = that.id;
	/*alert(id);*/
	if($(that).is(':checked')){
		$('input#d_approved_'+id).val('1');
	}else{
		$('input#d_approved_'+id).val('0');
	}
}

//draw submitted checkbox...

function draw_submit_1(that){

	if($(that).is(':checked')){

		$('input#submit_servicer_1').val('1');
	}else{
		$('input#submit_servicer_1').val('0');
	}
}

function draw_submit_2(that){

	if($(that).is(':Checked')){
		$('input#submit_servicer_2').val('1');
	}else{
		$('input#submit_servicer_2').val('0');
	}
}

function draw_submit_3(that){

	if($(that).is(':Checked')){
		$('input#submit_servicer_3').val('1');
	}else{
		$('input#submit_servicer_3').val('0');
	}
}

function draw_submit_4(that){

	if($(that).is(':Checked')){
		$('input#submit_servicer_4').val('1');
	}else{
		$('input#submit_servicer_4').val('0');
	}
}

function draw_submit_5(that){

	if($(that).is(':Checked')){
		$('input#submit_servicer_5').val('1');
	}else{
		$('input#submit_servicer_5').val('0');
	}
}
function draw_submit_6(that){

	if($(that).is(':Checked')){
		$('input#submit_servicer_6').val('1');
	}else{
		$('input#submit_servicer_6').val('0');
	}
}
function draw_submit_7(that){

	if($(that).is(':Checked')){
		$('input#submit_servicer_7').val('1');
	}else{
		$('input#submit_servicer_7').val('0');
	}
}
function draw_submit_8(that){

	if($(that).is(':Checked')){
		$('input#submit_servicer_8').val('1');
	}else{
		$('input#submit_servicer_8').val('0');
	}
}
function draw_submit_9(that){

	if($(that).is(':Checked')){
		$('input#submit_servicer_9').val('1');
	}else{
		$('input#submit_servicer_9').val('0');
	}
}
function draw_submit_10(that){

	if($(that).is(':Checked')){
		$('input#submit_servicer_10').val('1');
	}else{
		$('input#submit_servicer_10').val('0');
	}
}
function draw_submit_11(that){

	if($(that).is(':Checked')){
		$('input#submit_servicer_11').val('1');
	}else{
		$('input#submit_servicer_11').val('0');
	}
}
function draw_submit_12(that){

	if($(that).is(':Checked')){
		$('input#submit_servicer_12').val('1');
	}else{
		$('input#submit_servicer_12').val('0');
	}
}
function draw_submit_13(that){

	if($(that).is(':Checked')){
		$('input#submit_servicer_13').val('1');
	}else{
		$('input#submit_servicer_13').val('0');
	}
}
function draw_submit_14(that){

	if($(that).is(':Checked')){
		$('input#submit_servicer_14').val('1');
	}else{
		$('input#submit_servicer_14').val('0');
	}
}
function draw_submit_15(that){

	if($(that).is(':Checked')){
		$('input#submit_servicer_15').val('1');
	}else{
		$('input#submit_servicer_15').val('0');
	}
}

function draw_released_15(that){

	if($(that).is(':checked')){
		$('input#released_15').val('1');

	}else{

		$('input#released_15').val('0');
	}
}

/*draw_released checkbox...*/

function draw_released_1(that){

	if($(that).is(':checked')){
		$('input#released_1').val('1');

	}else{

		$('input#released_1').val('0');
	}
}

function draw_released_2(that){

	if($(that).is(':checked')){
		$('input#released_2').val('1');

	}else{

		$('input#released_2').val('0');
	}
}

function draw_released_3(that){

	if($(that).is(':checked')){
		$('input#released_3').val('1');

	}else{

		$('input#released_3').val('0');
	}
}

function draw_released_4(that){

	if($(that).is(':checked')){
		$('input#released_4').val('1');

	}else{

		$('input#released_4').val('0');
	}
}

function draw_released_5(that){

	if($(that).is(':checked')){
		$('input#released_5').val('1');

	}else{

		$('input#released_5').val('0');
	}
}

function draw_released_6(that){

	if($(that).is(':checked')){
		$('input#released_6').val('1');

	}else{

		$('input#released_6').val('0');
	}
}
function draw_released_7(that){

	if($(that).is(':checked')){
		$('input#released_7').val('1');

	}else{

		$('input#released_7').val('0');
	}
}
function draw_released_8(that){

	if($(that).is(':checked')){
		$('input#released_8').val('1');

	}else{

		$('input#released_8').val('0');
	}
}
function draw_released_9(that){

	if($(that).is(':checked')){
		$('input#released_9').val('1');

	}else{

		$('input#released_9').val('0');
	}
}
function draw_released_10(that){

	if($(that).is(':checked')){
		$('input#released_10').val('1');

	}else{

		$('input#released_10').val('0');
	}
}
function draw_released_11(that){

	if($(that).is(':checked')){
		$('input#released_11').val('1');

	}else{

		$('input#released_11').val('0');
	}
}
function draw_released_12(that){

	if($(that).is(':checked')){
		$('input#released_12').val('1');

	}else{

		$('input#released_12').val('0');
	}
}
function draw_released_13(that){

	if($(that).is(':checked')){
		$('input#released_13').val('1');

	}else{

		$('input#released_13').val('0');
	}
}
function draw_released_14(that){

	if($(that).is(':checked')){
		$('input#released_14').val('1');

	}else{

		$('input#released_14').val('0');
	}
}


function draw_request_15(last){
	if($(last).is(':checked')){
		$('input#request_15').val('1');
		var id=$(last).parents().find('input#request_15').val();
 		var tali=$(last).parents().find('input#tali').val();
  		var loa=$(last).parents().find('input#loa').val();
 		var amount = 0;
	    $('input[name="enter_amount_15[]"]').each(function(){
	       amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });
 		/*alert(amount);*/
		if(id=='1' && amount !='0')
		{
			$.ajax({
				type : 'POST',
				url : '/Pranav/Talimar/Load_data/email_script',
				data : {'amount':amount,'tali':tali,'loa':loa},
				success : function(response)
				{
				}
			});

		}
	}else{

		$('input#request_15').val('0');
	}
}

function draw_request_14(plig){
	if($(plig).is(':checked')){
		$('input#request_14').val('1');
		var id=$(plig).parents().find('input#request_14').val();
		var tali=$(plig).parents().find('input#tali').val();
		var loa=$(plig).parents().find('input#loa').val();
		var amount = 0;
	    $('input[name="enter_amount_14[]"]').each(function(){
	         amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });
	 	/*alert(amount);*/
		if(id=='1' && amount !='0')
		{
			$.ajax({
				type : 'POST',
				url : '/Pranav/Talimar/Load_data/email_script',
				data : {'amount':amount,'tali':tali,'loa':loa},
				success : function(response)
				{
				}

			});
		}
	}else{

		$('input#request_14').val('0');
	}
}

function draw_request_13(hug){

	if($(hug).is(':checked')){
		$('input#request_13').val('1');
		var id=$(hug).parents().find('input#request_13').val();
		var tali=$(hug).parents().find('input#tali').val();
		var loa=$(hug).parents().find('input#loa').val();
		var amount = 0;
	    $('input[name="enter_amount_13[]"]').each(function(){
	        amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });
 		/*alert(amount);*/
		if(id=='1' && amount !='0')
		{
			$.ajax({
				type : 'POST',
				url : '/Pranav/Talimar/Load_data/email_script',
				data : {'amount':amount,'tali':tali,'loa':loa},
				success : function(response)
				{
				}
			});

		}
	}else{

		$('input#request_13').val('0');
	}
}

function draw_request_12(ran){

	if($(ran).is(':checked')){
		$('input#request_12').val('1');
 		var id=$(ran).parents().find('input#request_12').val();
 		var tali=$(ran).parents().find('input#tali').val();
 		var loa=$(ran).parents().find('input#loa').val();
 		var amount = 0;
    	$('input[name="enter_amount_12[]"]').each(function(){
      		amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
    	});
 		/*alert(amount);*/
		if(id=='1' && amount !='0')
		{
			$.ajax({
				type : 'POST',
				url : '/Pranav/Talimar/Load_data/email_script',
				data : {'amount':amount,'tali':tali,'loa':loa},
				success : function(response)
				{
				}
			});
		}
	}else{
		$('input#request_12').val('0');
	}
}

function draw_request_11(run){

	if($(run).is(':checked')){
		$('input#request_11').val('1');
 		var id=$(run).parents().find('input#request_11').val();
 		var tali=$(run).parents().find('input#tali').val();
  		var loa=$(run).parents().find('input#loa').val();
 		var amount = 0;
    	$('input[name="enter_amount_11[]"]').each(function(){
        	amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
    	});
 		
 		if(id=='1' && amount !='0')
		{
			$.ajax({
				type : 'POST',
				url : '/Pranav/Talimar/Load_data/email_script',
				data : {'amount':amount,'tali':tali,'loa':loa},
				success : function(response)
				{
				}
			});
		}
	}else{
		$('input#request_11').val('0');
	}
}

function draw_request_10(hel){

	if($(hel).is(':checked')){
		$('input#request_10').val('1');
		var loa=$(hel).parents().find('input#loa').val();
		var id=$(hel).parents().find('input#request_10').val();
		var tali=$(hel).parents().find('input#tali').val();

		var amount = 0;
	    $('input[name="enter_amount_10[]"]').each(function(){
	        amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });

		if(id=='1' && amount !='0')
		{
			$.ajax({
				type : 'POST',
				url : '/Pranav/Talimar/Load_data/email_script',
				data : {'amount':amount,'tali':tali,'loa':loa},
				success : function(response)
				{
				}
			});
		}
	}else{
		$('input#request_10').val('0');
	}
}

function draw_request_9(hy){
	if($(hy).is(':checked')){
		$('input#request_9').val('1');
		var id=$(hy).parents().find('input#request_9').val();
		var tali=$(hy).parents().find('input#tali').val();
		var loa=$(hy).parents().find('input#loa').val();
		var amount = 0;
		$('input[name="enter_amount_9[]"]').each(function(){
		    amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
		});

		if(id=='1' && amount !='0')
		{
			$.ajax({
				type : 'POST',
				url : '/Pranav/Talimar/Load_data/email_script',
				data : {'amount':amount,'tali':tali,'loa':loa},
				success : function(response)
				{
				}
			});
		}
	}else{

		$('input#request_9').val('0');
	}
}

function draw_request_8(kam){
	if($(kam).is(':checked')){
		$('input#request_8').val('1');
 		var id=$(kam).parents().find('input#request_8').val();
 		var tali=$(kam).parents().find('input#tali').val();
  		var loa=$(kam).parents().find('input#loa').val();
 		var amount = 0;
    	$('input[name="enter_amount_8[]"]').each(function(){
        	amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
    	});

		if(id=='1' && amount !='0')
		{
			$.ajax({
				type : 'POST',
				url : '/Pranav/Talimar/Load_data/email_script',
				data : {'amount':amount,'tali':tali,'loa':loa},
				success : function(response)
				{
				}
			});
		}
	}else{

		$('input#request_8').val('0');
	}
}

function draw_request_7(km){

	if($(km).is(':checked')){
		$('input#request_7').val('1');
		var id=$(km).parents().find('input#request_7').val();
 		var tali=$(km).parents().find('input#tali').val();
   		var loa=$(km).parents().find('input#loa').val();
 		var amount = 0;
	    $('input[name="enter_amount_7[]"]').each(function(){
	         amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });

		if(id=='1' && amount !='0')
		{
			$.ajax({
				type : 'POST',
				url : '/Pranav/Talimar/Load_data/email_script',
				data : {'amount':amount,'tali':tali,'loa':loa},
				success : function(response)
				{
				}

			});
		}
	}
	else{
		$('input#request_7').val('0');
	}
}

function draw_request_6(tht){

	if($(tht).is(':checked')){
		$('input#request_6').val('1');
		var id=$(tht).parents().find('input#request_6').val();
		var tali=$(tht).parents().find('input#tali').val();
		var loa=$(tht).parents().find('input#loa').val();
		var amount = 0;
	    $('input[name="enter_amount_6[]"]').each(function(){
	        amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });

		if(id=='1' && amount !='0')
		{
			$.ajax({
				type : 'POST',
				url : '/Pranav/Talimar/Load_data/email_script',
				data : {'amount':amount,'tali':tali,'loa':loa},
				success : function(response)
				{
				}
			});
		}
	}else{

		$('input#request_6').val('0');
	}
}

function draw_request_5(tham){

	if($(tham).is(':checked')){
		$('input#request_5').val('1');
 		var id=$(tham).parents().find('input#request_5').val();
 		var tali=$(tham).parents().find('input#tali').val();
  		var loa=$(tham).parents().find('input#loa').val();
 		var amount = 0;
    	$('input[name="enter_amount_5[]"]').each(function(){
       		amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
    	});

		if(id=='1' && amount !='0')
		{
			$.ajax({
				type : 'POST',
				url : '/Pranav/Talimar/Load_data/email_script',
			    data : {'amount':amount,'tali':tali,'loa':loa},
				success : function(response)
				{
				}
			});
		}
	}
	else{
		$('input#request_5').val('0');
	}
}

function draw_request_4(thh){
	if($(thh).is(':checked')){
		$('input#request_4').val('1');
 		var id=$(thh).parents().find('input#request_4').val();
 		var tali=$(thh).parents().find('input#tali').val();
   		var loa=$(thh).parents().find('input#loa').val();
 		var amount = 0;
	    $('input[name="enter_amount_4[]"]').each(function(){
	        amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });

		if(id=='1' && amount !='0')
		{
			$.ajax({
				type : 'POST',
				url : '/Pranav/Talimar/Load_data/email_script',
			    data : {'amount':amount,'tali':tali,'loa':loa},
				success : function(response)
				{
				}

			});
		}
	}else{
		$('input#request_4').val('0');
	}
}

function draw_request_3(th){
	if($(th).is(':checked')){
		$('input#request_3').val('1');
		var id=$(th).parents().find('input#request_3').val();
		var tali=$(th).parents().find('input#tali').val();
		var loa=$(th).parents().find('input#loa').val();
		var amount = 0;
	    $('input[name="enter_amount_3[]"]').each(function(){
	        amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });

		if(id=='1' && amount !='0')
		{
			$.ajax({
				type : 'POST',
				url : '/Pranav/Talimar/Load_data/email_script',
				data : {'amount':amount,'tali':tali,'loa':loa},
				success : function(response)
				{
				}
			});
		}
	}else{
		$('input#request_3').val('0');
	}
}

function draw_request_2(t){

	if($(t).is(':checked')){
		$('input#request_2').val('1');
 		var id=$(t).parents().find('input#request_2').val();
 		var tali=$(t).parents().find('input#tali').val();
  		var loa=$(t).parents().find('input#loa').val();
 		var amount = 0;
    	$('input[name="enter_amount_2[]"]').each(function(){
        	amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
    	});

		if(id=='1' && amount !='0')
		{
			$.ajax({
				type : 'POST',
				url : '/Pranav/Talimar/Load_data/email_script',
				data : {'amount':amount,'tali':tali,'loa':loa},
				success : function(response)
				{
				}
			});
		}
	}else{
		$('input#request_2').val('0');
	}
}

/*draw_request checkbox...*/
function draw_request_1(that){
	if($(that).is(':checked')){
		$('input#request_1').val('1');
		var id=$(that).parents().find('input#request_1').val();
		var tali=$(that).parents().find('input#tali').val();
		var loa=$(that).parents().find('input#loa').val();
		var amount = 0;
	    $('input[name="enter_amount_1[]"]').each(function(){
	       amount += +$(this).val().replace("$", "").replace(",", "").replace(",", "").replace(",", "");
	    });

		if(id=='1' && amount !='0')
		{
			$.ajax({
				type : 'POST',
				url : '/Pranav/Talimar/Load_data/email_script',
				data : {'amount':amount,'tali':tali,'loa':loa},
				success : function(response)
				{
				}
			});
		}
	}
	else
	{
		$('input#request_1').val('0');
	}
}

function no_data_draw(that){
	alert('No Data Available in Loan Reserve!')
}
function no_data_drawd(tat){
	alert('Select The Loan First!')
}
function isNumber(evt){
	var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))

        return false;
		return true;
}

function delete_images(that)
{
	var rowcnt = $(that).data('id');
	var id = $(that).parent().parent().parent().find('input[name="id[]"]').val();
	if(id == 'new')
	{
		$(that).parents('div.col-sm-3.img-div_'+rowcnt).remove();
	}
	else
	{
		if(confirm('Are you sure to delete') === true)
		{
			$.ajax({
				type 	: 'POST',
				url 	: '/Pranav/Talimar/Property_data/delete_reserve_images',
				data 	: { 'id':id },
				success	: function(result)
				{
					$(that).parents('div.col-sm-3').remove();
				}
			})
		}
	}
}



function click_on_image_buttons(that)
{
	$(that).parent('div.property-image-inner-image').find('input.image_upload_inputs').click();
	$(that).parent('div.property-image-inner-image').find('input.image_upload_inputs').change(function() {
		image_readURL(this);
	});
}

function image_readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $(input).parent().find('img.property-imagesss').attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}


$(document).ready(function(){
	$('div#page-loan-data-info input').attr('disabled',true);

	var btn_name = $('#btn_name').val();
	if(btn_name == 'new'){
		$('#loan_terms').modal('show');
	}

	$('div#page-loan-data-info input').attr('disabled',true);

	var curchr = $('.phone-format').val().length;
    var curval = $('.phone-format').val();
    if (curchr == 3 && curval.indexOf("(") <= -1) {

      $('.phone-format').val("(" + curval + ")" + "-");
    } else if (curchr == 4 && curval.indexOf("(") > -1) {
      $('.phone-format').val(curval + ")-");
    } else if (curchr == 5 && curval.indexOf(")") > -1) {
      $('.phone-format').val(curval + "-");
    } else if (curchr == 9) {
      $('.phone-format').val(curval + "-");
      $('.phone-format').attr('maxlength', '14');
    }

	$('table#draws_table tbody input[name="request_submitted[]"]').each(function(){
		construction_reserve_readonly_css_setting1(this);
	});

	$('select#loan_payment_gurranty').change(function(){
		var payment_gurranty = $('select#loan_payment_gurranty').val();
		if(payment_gurranty == '1')
		{
			$('div#payment_gurranty_dependent').css('display','block');
		}
		else
		{
			$('div#payment_gurranty_dependent textarea').val('');
			$('div#payment_gurranty_dependent').css('display','none');
		}
	});

	var payment_gurranty = $('select#loan_payment_gurranty').val();
	if(payment_gurranty == '1')
	{
		$('div#payment_gurranty_dependent').css('display','block');
	}
	else
	{
		$('div#payment_gurranty_dependent textarea').val('');
		$('div#payment_gurranty_dependent').css('display','none');
	}



	/*Add row in Loan Reserve modal*/
	var id = 0;
    jQuery("#draws_addrow").click(function() {
        id++;
        var row = jQuery('.draws_table_blank tr').clone(true);
        row.find("input:text").val("");
        row.attr('id',id);
        row.appendTo('#draws_table');
        return false;
    });

	$('.draws_remove').on("click", function() {
	  $(this).parents("tr").remove();
	});

	/*Add row in Loan reserve 2 modal...*/
	var count=jQuery('#add_test_new_row tr').find('td#counting').text().length;
	jQuery("#add_test_row").click(function() {
		/*id++;
		var overall_count=count+id;*/
		var row = jQuery('#hidden_table tr').clone(true);
		row.find("input:text").val("");
		/*row.find("td#idd").text(overall_count);*/
		row.attr('id',id);
	    row.appendTo('#add_test_new_row');
		/*$('#add_test_new_row tbody').append($("#add_test_new_row tbody tr:last").clone());
		$('#add_test_new_row tbody tr:last :input').attr('value', '');
		$('#add_test_new_row tbody tr:last :textarea').attr('value', '');
		$('#add_test_new_row tbody tr:last td:first').html('');*/
		return false;
	});

	$('#loan_funding_date').change(function(){
		 calculate_maturity_value();
	 });

	var id = 0;
    
    $('body').on('click', "button#add_loan_source_and_uses", function() {
        id++;
        var row = jQuery('table#table_loan_source_and_uses_hidden tr').clone(true);
        row.find("input:text").val("");
        row.attr('id',id);
        row.appendTo('table#table_loan_source_and_uses');
        return false;
    });

	var val_data = $('select#minium_intrest').val();
	if(val_data == '1')
	{
		$("#minimum_select_yes").css("display" , "block");
		$("#min_bal_type_option").css("display" , "block");
		$("#min_intrest_month").focus();
	}
	else
	{
		$("#minimum_select_yes").css("display" , "none");
		$("#min_bal_type_option").css("display" , "none");
	}

	 /*onchange on loan  amount same to current balance*/
	$('#loan_amount').change(function(){
		var check =replace_dollar($('#loan_amount').val());
		if(check != '')
		{
		var loan_amount = replace_dollar($('#loan_amount').val());
		}
		else
		{
			var loan_amount = 0;
		}
		/* var loan_amount_a		= number_format(parseFloat(loan_amount));
		 $('#current_balance').val('$'+loan_amount_a);*/
		auto_cal_payment_amount();
		calulate_baloon_payment();
		ajax_change_loan_or_arv_amount();
		/* $('#lender_fee').focus();
		 document.getElementById("lender_fee").focus();*/
	});

	/*   onchange in Intrest rate*/
	$('#intrest_rate').change(function(){
		var interst_rate  = parseFloat($('#intrest_rate').val());
		interst_rate		= number_format_rate(interst_rate);
		$('#intrest_rate').val(interst_rate);
		auto_cal_payment_amount();
	});

	/*  onchange in lender fee*/
	$('#lender_fee').change(function(){
		var interst_rate  	= parseFloat($('#lender_fee').val());
		interst_rate		= number_format(interst_rate);
		$('#lender_fee').val(interst_rate+'%');

	});

	$('#late_charges_due').change(function() {
		var late_charges_due  = $('#late_charges_due').val();
		if(late_charges_due != '')
		{
			var rate = number_format(parseFloat(late_charges_due));
			$('#late_charges_due').val(rate+'%');
		}
		else
		{
			var rate = 0;
			$('#late_charges_due').val('0.00%');
		}
	});

	/*   ----  onchange in defalt rate*/
	$('#default_rate').change(function() {
		var default_rate  = $('#default_rate').val();
		if(default_rate != '')
		{
			var rate = number_format_rate(parseFloat(default_rate));
			$('#default_rate').val(rate+'%');
		}
		else
		{
			var rate = 0;
			$('#default_rate').val('0.00%');
		}
	});

	/*   ----  onchange in defalt rate*/
	$('#return_check_rate').change(function() {
		var check_rate  = replace_dollar($('#return_check_rate').val());
		if(check_rate != '')
		{
		var new_rate    = parseFloat(check_rate);
		var result = number_format(new_rate);
		$('#return_check_rate').val('$'+result);
		}
		else
		{
			$('#return_check_rate').val('0.00');
		}
	});

	$('#project_cost').change(function(){
		var income = replace_dollar($('#project_cost').val());
		if(income != '')
		{
			var result = parseFloat(income);
			var new_result = number_format(result);
			$('#project_cost').val('$'+new_result);
		}
		else
		{
			$('#project_cost').val('$0.00');
		}
	});

	$('#total_project_cost').change(function(){
		var income = replace_dollar($('#total_project_cost').val());
		if(income != '')
		{
			var result = parseFloat(income);
			var new_result = number_format(result);
			$('#total_project_cost').val('$'+new_result);
		}
		else
		{
			$('#total_project_cost').val('$0.00');
		}
	});

	$('#borrower_enquity').change(function(){
		var income = replace_dollar($('#borrower_enquity').val());
		if(income != '')
		{
			var result = parseFloat(income);
			var new_result = number_format(result);
			$('#borrower_enquity').val('$'+new_result);
		}
		else
		{
			$('#borrower_enquity').val('$0.00');
		}
	});

	$('#payment_amount').change(function(){
		var income = replace_dollar($('#payment_amount').val());
		if(income != '')
		{
			var result = parseFloat(income);
			var new_result = number_format(result);
			$('#payment_amount').val('$'+new_result);
		}
		else
		{
			$('#payment_amount').val('$0.00');
		}
	});

	/*---------On change in payment_type*/
	$('#loan_payment_type').change(function(){
		calulate_baloon_payment();
	});
	/*onchange event on term_month*/
	$('#term_month').change(function(){
		calculate_maturity_value();
	});

	$('.checkbox_draws_release').change(function(){
		var result = this.value;
		if($(this).attr('checked'))
		{
			$(this).parent().parent().parent().find("input[name='draws_release[]']").val('1');
		}
		else
		{
			$(this).parent().parent().parent().find("input[name='draws_release[]']").val('0');
		}
	});

	$(".amount_format").change(function(){
		var a = this.value;
		var a = replace_dollar(a);
		if(a == '')
		{
			a = 0;
		}
		if(isNaN(a) == true)
		{
			a = 0;
		}
		a = parseFloat(a);
		this.value = '$'+number_format(a);
	});

	

	var loan_type = $('#loan_type').val();
	if(loan_type == 1)
	{
		$('.loan_type_dependent').css('display','none');
		$('#construction_div').css('display','none');
	}
	else
	{
		$('.loan_type_dependent').css('display','block');
		$('#construction_div').css('display','block');
	}

	$('#loan_type').change(function(){
		var loan_type = $('#loan_type').val();
		if(loan_type == 1)
		{
			$('.loan_type_dependent').css('display','none');
			$('#construction_div').css('display','none');
		}
		else
		{
			$('.loan_type_dependent').css('display','block');
			$('#construction_div').css('display','block');
		}
	});

	var baloon_payment_type = $('#baloon_payment_type').val();
	if(baloon_payment_type == 1)
	{
		calulate_baloon_payment();
	}
	else
	{
		$('#loan_ballon_payment').val('$0');
	}

	$('#baloon_payment_type').change(function(){
		var baloon_payment_type = $('#baloon_payment_type').val();
		if(baloon_payment_type == 1)
		{
			calulate_baloon_payment();
		}
		else
		{
			$('#loan_ballon_payment').val('$0');
		}
	});

	$(document).on("change", ".autocal_draws_remaining", function() {
	    var sum = 0;
	    $(".autocal_draws_remaining").each(function(){
	        sum += + replace_dollar($(this).val());
	    });
	    $("#total_remaining").val('$'+number_format(sum));
	});

	$(document).on("change", ".autocal_draws_release_amount", function() {
	    var sum = 0;
	    var sum1 = 0;
	    $(".autocal_draws_release_amount").each(function(){
			var release = parseFloat(replace_dollar($(this).val()));
			var draw = $(this).parent().parent().find('input#draws_amount').val();
			if(draw != '')
			{
				draw = parseFloat(replace_dollar(draw));
				draws_remaining = draw - release;
				$(this).parent().parent().find('input#draws_remaining').val('$'+number_format(draws_remaining));
			}
	        sum += + replace_dollar($(this).val());
	    });
		// alert('sum');
	    $("#total_release_amount").val('$'+number_format(sum));

		$(".autocal_draws_remaining").each(function(){
	        sum1 += + replace_dollar($(this).parent().parent().find('input#draws_remaining').val());
	    });
	    $("#total_remaining").val('$'+number_format(sum1));
	});

	$(document).on("change", ".autocal_draws_amount", function() {
	    var sum = 0;
	    var sum1 = 0;
	    var sum2 = 0;
	    $(".autocal_draws_amount").each(function(){
			var draw = parseFloat(replace_dollar($(this).val()));
			var release = $(this).parent().parent().find('input#draws_release_amount').val();
			if(release != '')
			{
				release = parseFloat(replace_dollar(release));
				draws_remaining = draw - release;
				$(this).parent().parent().find('input#draws_remaining').val('$'+number_format(draws_remaining));
			}
			sum += + replace_dollar($(this).val());
	    });
	    $("#total_draws").val('$'+number_format(sum));

		$(".autocal_draws_remaining").each(function(){
	        sum1 += + replace_dollar($(this).parent().parent().find('input#draws_remaining').val());
	    });
	    $("#total_remaining").val('$'+number_format(sum1));
	});

	var result = $('#select_draws').val();
	if(result == '1')
	{
		$(".draws_button_list").css("display","block");
	}
	else
	{
		$(".draws_button_list").css("display","none");
	}

	$('#select_draws').change(function(){
		var result = $('#select_draws').val();
		if(result == '1')
		{
			$(".draws_button_list").css("display","block");
		}
		else
		{
			$(".draws_button_list").css("display","none");
		}
	});

	/*$('input[name="fci_loan_no"]').keydown(function(){
	var fci = this.value;
	var d = new Date();
	var year = d.getFullYear();
	year = year.toString().substr(-2);
	alert(fci.length);
	});*/


});


function calulate_baloon_payment()
{
	var payment_type = $('#loan_payment_type').val();
	if(payment_type == '1')
	{
		
		$('#loan_ballon_payment').attr('readonly', true);

		var payment_amount 	= replace_dollar($('#payment_amount').val());
		if(payment_amount == '')
		{
			payment_amount = 0;
		}
		payment_amount		= parseFloat(payment_amount);

		var loan_amount 	= replace_dollar($('#loan_amount').val());
		if(loan_amount == '')
		{
			loan_amount = 0;
		}
		loan_amount		= parseFloat(loan_amount);

		var ballon  		= loan_amount + payment_amount + 0;

		$('#loan_ballon_payment').val('$'+number_format(ballon));
	}
	else
	{
		$('#loan_ballon_payment').val('');
		$('#loan_ballon_payment').attr('readonly', true);
	}
}