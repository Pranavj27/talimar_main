$(document).ready(function(){
	jQuery("body").on('change', ".amount_format", function() {
		var a = this.value;
		var a = replace_dollar(a);
		if(a == '')
		{
			a = 0;
		}
		a = parseFloat(a);
		this.value = '$'+number_format(a);
	});

	jQuery("body").on('change', ".amount_format_without_decimal", function() {
		var a = this.value;
		var a = replace_dollar(a);
		if(a == '')
		{
			a = 0;
		}
		a = parseFloat(a);
		this.value = '$'+a.toLocaleString();
	});
	jQuery("body").on('change', ".rate_format", function() {
		var a = this.value;
		var a = replace_dollar(a);
		if(a == '')
		{
			a = 0;
		}
		a = parseFloat(a);
		this.value = number_format_rate(a)+'%';
	});
	jQuery("body").on('change', ".rate_format_without_decimal", function() {
		var a = this.value;
		var a = replace_dollar(a);
		if(a == '')
		{
			a = 0;
		}
		a = parseFloat(a);
		this.value = a.toLocaleString()+'%';
	});
});