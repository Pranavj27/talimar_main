$(document).ready(function() {
	var max_w = 0;
	if($('div#model_left_align a.btn').length > 0){
		$('div#model_left_align a.btn').each(function() {
		    max_w = Math.max(max_w, parseInt($(this).width()));
		}); 
		$('div#model_left_align a.btn').css('width',(parseInt(max_w) + parseInt(50))+'px');
	}

	var haveFirst = false;
	$('.username_input').on('keypress', function (event) {
		if( $(this).val().length === 0 ) {
		     haveFirst = false;
		}
		var regex = new RegExp("^[a-z0-9_]+$");
		var first = new RegExp("^[a-z]+$");
		var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		if(!first.test(key) && haveFirst == false){
		   event.preventDefault();
		   return false;
		}else if(regex.test(key)){
		   haveFirst = true;
		}
		if (!regex.test(key)) {
		   event.preventDefault();
		   return false;
		}
	});
});