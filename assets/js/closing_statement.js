//  Closing statement page js

function closing_statement_netfunding_checkbox(that)
{
	if($(that).is(':checked'))
	{
		$(that).parent().parent().parent().find('input#net_funding').val(1);
	}
	else
	{
		$(that).parent().parent().parent().find('input#net_funding').val(0);
	}
}

function closing_statement_prepaid_checkbox(that)
{
	if($(that).is(':checked'))
	{
		$(that).parent().parent().parent().find('input#prepaid_charge').val(1);
	}
	else
	{
		$(that).parent().parent().parent().find('input#prepaid_charge').val(0);
	}
}



function closing_statement_directtoservicer_checkbox(that)
{
	if($(that).is(':checked'))
	{
		$(that).parent().parent().parent().find('input#direct_to_servicer').val(1);
	}
	else
	{
		$(that).parent().parent().parent().find('input#direct_to_servicer').val(0);
	}
}

function lender_statement_checkbox(that)
{
	if($(that).is(':checked'))
	{
		$(that).parent().parent().parent().find('input#lender_statement').val(1);
	}
	else
	{
		$(that).parent().parent().parent().find('input#lender_statement').val(0);
	}
}

 //  Add row in closing statement new  modal
  
// jQuery(document).ready(function() {
    // jQuery("a#add_row_closing_items").click(function() {
                  
        // var row = jQuery('#hidden_closing_statemnet_new tr').clone(true);
        // row.find("input:text").val("");
        // row.attr('id',id);  
        // row.appendTo('#estimate_lender_table');        
        // return false;
    // });        
        
  // $('.estimate_lender_remove').on("click", function() {
  // $(this).parents("tr").remove();
// });
// });

function add_row_closing_items(that)
{
	
		var id	= that.id;
		// alert(id);
		var row = jQuery('#hidden_closing_statemnet_new tr').clone(true);
        row.find("input:text").val("");
        // row.appendTo($('tr#'+id+':after')); 
		$('tr#'+id).before(row);	
		$('.th.padding-zero.hud').css('padding','0px');
}


function number_only_format(that)
{
	if (/\D/g.test(that.value)) that.value = that.value.replace(/\D/g,'')
}

function closing_statement_deductions_add() 
{
	var row = jQuery('#closing-statement-deduction-hidden tr').clone(true);
        row.find("input:text").val("");
        row.appendTo($('#closing-statement-deduction>tbody:last')); 
		
}

