function changeUsername(){
	var edit_username 	= $('#edit_username').val();
	var edit_o_username = $('#edit_o_username').val();
	if(edit_username == edit_o_username){
		swal("Please put different username!", {icon: "error",});
	}else{
		$.ajax({
	        type : 'POST',
	        url  : "/Users/update_username",
	        data : {'edit_username':edit_username},
	        success : function(response){
	        	if(response == 'success'){
	        		$('#edit_o_username').val(edit_username)
	        		swal("Username information updated successfully!", {icon: "success",});	        		
	        	}else{
	        		swal("Some error occurred please try again!", {icon: "error",});
	        	}	        	
	        }
	    });
	}
}

function changePassword()
{
	var oldPassword = $('#oldPassword').val();
	var newPassword = $('#newPassword').val();
	var confirmPassword = $('#confirmPassword').val();
	var userId = $('#update_ida').val();
	
	$.ajax({
			type : 'POST',
			url  : '/Users/changePassword',
			data : {'id':userId,'oldPassword':oldPassword,'newPassword':newPassword,'confirmPassword':confirmPassword},
			success : function(response){
				var json =  JSON.parse(response);
				console.log(json);
				var message = json.message;
				$('form#changePassword').trigger("reset");
				$("#messagesChange").html(message);	
			}
	});
}

function SendLinkToForget(email)
{
	$.ajax({
	        type : 'POST',
	        url  : '/Users/forget_password',
	        data : {'email':email},
	        success : function(response){
				var json =  JSON.parse(response);
				//console.log(json);
				var message = json.message;
				
				$("#messagesSendLink").html(message);	
			}
	    });
}


function loan_origination_chkbox(that){

if($(that).is(':checked')){

  $('input#origination').val('1');
	}else{
		$('input#origination').val('0');
	}
}

function loan_servicing_chkbox(that){

if($(that).is(':checked')){

	$('input#servicing').val('1');
	}else{
		$('input#servicing').val('0');
	}
}

function investor_relations_chkbox(that){

	if($(that).is(':checked')){

	$('input#investor').val('1');
		}else{
			$('input#investor').val('0');
	}
}

function daily_report_chkbox(that){

	if($(that).is(':checked')){

		$('input#daily').val('1');
	}else{
			$('input#daily').val('0');
	}
 }

function marketing_chkbox(that){

	if($(that).is(':checked')){

		$('input#marketing').val('1');
	}else{
			$('input#marketing').val('0');
	}
 }

 function lenderPortaltracking(that){

	if($(that).is(':checked')){
		$('input#lenderportaltrack').val('1');
	}else{
		$('input#lenderportaltrack').val('0');
	}
 }


$(document).ready(function($) {        
	$("#changePassword").validate({
    rules: {
    	  oldPassword: "required",
	      newPassword: {
	        required: true,
	        minlength: 6
	      },
	      confirmPassword: {
	        required: true,
	        equalTo: "#newPassword"

	      }
     
    },
   messages: {
   	  oldPassword: "Please enter your old Password",
      newPassword: {
        required: "Please provide a password",
        minlength: "Your password must be at least 6 characters long"
      },
      confirmPassword: {
        required: "Please provide a confirm password",
        equalTo: "Confirm password not match"
      }

},
errorPlacement: function(error, element) 
{
    if ( element.is(":radio") ) 
    {
        error.appendTo( element.parents('.form-group') );
    }
    else 
    { // This is the default behavior 
        error.insertAfter( element );
    }
 },
submitHandler: function(form) {
        changePassword();
}
    
});
});