getLenderData();
function getLenderData()
{
	var dispatching_list_all = '#ajax_table_contactlist';
	$(dispatching_list_all).DataTable();
	$(dispatching_list_all).DataTable().clear().destroy();
	if( $( dispatching_list_all ).length ){	


		var nameLender = $('#nameLender').val();
		var subscriptionStatus = $('#subscriptionStatus').val();
		var accredited = $('#accredited').val();
	

		$dis_list_all = $( dispatching_list_all ).DataTable({
		processing: true,
		serverSide: true,
		pageLength: 4,
		ajax: {

				type : 'POST',
		    	dataSrc:"data",
				data:{'investor_id' : nameLender, 'subscriptionStatus' : subscriptionStatus, 'accredited' : accredited, 'orderby' : 'ASC'},
		    	url: '<?php echo base_url()."MortgageFound/ajaxGetLender1";?>',
			    dataFilter: function(data){
			    	
					var json = JSON.parse( data );
					json.recordsTotal = json.total;
					json.recordsFiltered = json.total;
					json.data = json.data;
					return JSON.stringify( json );
				},
			},
			columns: [				        
		        { data: 'nameLender' },
		        { data: 'subscriptionStatus' },
		        { data: 'accountType' },
		        { data: 'name' },
		        { data: 'phone' },				        
		        { data: 'email' },
		        { data: 'inProcess' },				        
		        { data: 'amountTotal' },
		        { data: 'edit' }
		    ],
			"aoColumnDefs": [
		        { "bSortable": false, "aTargets": [ 0,1,2,3,4,5,8 ] }, 
		        { "bSearchable": false, "aTargets": [1,2,3,4,5,6,7,8 ] }
		    ],
			"language": { loadingRecords: '&nbsp;&nbsp;', processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
			drawCallback: function( settings ) {
			        
			}
		});
	}
	

}

			
function selectLender()
{
 	$.ajax({
		type : 'POST',
		url  : '<?php echo base_url()."MortgageFound/selectOption";?>',
		delay : 3000,
		beforeSend: function() {
			$('#investor_id1').html('').select2({data: [{id: '', text: 'Select One'}]});
			$('#nameLender').html('').select2({data: [{id: '', text: 'Select All'}]});
		},
		success : function(result){
			var json = JSON.parse(result);
			$('#investor_id1').select2({
				data: json.lenderDataAll
			});

			$('#nameLender').select2({
				data: json.selectLender
			});			
		}	    
	});
}