function init_datatable_for_contact(){
	var dispatching_list_all = '#ajax_table_contactlist';
	$(dispatching_list_all).DataTable().clear().destroy();    

	if( $( dispatching_list_all ).length ){		
		$dis_list_all = $( dispatching_list_all ).DataTable({
			processing: true,
			serverSide: true,
			pageLength: 25,
		    ajax: {
				dataSrc:"data",
				data:function(data){
					// console.log(data);
					var internal_contact = $('#internal_contact_ajax').val();
					var csv_argument = '';
					if( internal_contact ){						
						data.internal_contact = internal_contact;
						csv_argument += 'internal_contact='+internal_contact+'&';
					}else{
						csv_argument += 'internal_contact=&';
					}

					var lenderwithacc = $('#lenderwithacc').val();
					if( lenderwithacc ){						
						data.lenderwithacc = lenderwithacc;
						csv_argument += 'lenderwithacc='+lenderwithacc+'&';
					}else{
						csv_argument += 'lenderwithacc=&';
					}

					var borrowerwithacc = $('#borrowerwithacc').val();
					if( borrowerwithacc ){						
						data.borrowerwithacc = borrowerwithacc;
						csv_argument += 'borrowerwithacc='+borrowerwithacc+'&';
					}else{
						csv_argument += 'borrowerwithacc=&';
					}

					var cp_contact_specialty = $('#cp_contact_specialty').val();
					if( cp_contact_specialty ){						
						data.contact_specialty = cp_contact_specialty;
						csv_argument += 'contact_specialty='+cp_contact_specialty+'&';
					}else{
						csv_argument += 'contact_specialty=&';
					}

					var cp_contact_marketing_data = $('#cp_contact_marketing_data').val();
					if( cp_contact_marketing_data ){						
						data.contact_Marketing = cp_contact_marketing_data;
						csv_argument += 'contact_Marketing='+cp_contact_marketing_data+'&';
					}else{
						csv_argument += 'contact_Marketing=&';
					}

					var cp_contact_email_blast = $('#cp_contact_email_blast').val();
					if( cp_contact_email_blast ){						
						data.contact_email_blast = cp_contact_email_blast;
						csv_argument += 'contact_email_blast='+cp_contact_email_blast+'&';
					}else{
						csv_argument += 'contact_email_blast=&';
					}

					var searchText = jQuery('input[type="search"]').val();

					if( searchText ){
						csv_argument += 'search_ext='+searchText+'&';
					}else{
						csv_argument += 'search_ext=&';
					}

					var orderby = $('#ajax_table_contactlist').DataTable().order()[0][0];
					var order   = $('#ajax_table_contactlist').DataTable().order()[0][1];
					csv_argument += 'orderby='+orderby+'&';
					csv_argument += 'order='+order+'&';					

					var company_name_ajax = $('#company_name_ajax').val();
					if( company_name_ajax ){						
						data.company_name = company_name_ajax;
						csv_argument += 'company_name='+company_name_ajax;
					}else{
						csv_argument += 'company_name=';
					}

					$('#download_contactlist_csv').attr('href', '/Pranav/Talimar/Contact/download_contactlist?'+csv_argument);
					$('#download_contactlist_pdf').attr('href', '/Pranav/Talimar/Contact/download_pdf?'+csv_argument);
				},
				url: '/Pranav/Talimar/Contact/ajax_contactlist',
				dataFilter: function(data){
					filter_tag_add();
					/*$('html, body').animate({
				        scrollTop: $('#ajax_table_contactlist').offset().top - 20
				    }, 'slow');*/
				    var json = JSON.parse( data );				    
				    json.recordsTotal = json.total;
				    json.recordsFiltered = json.total;
				    json.data = json.data;			    
				    return JSON.stringify( json );
				}
		    },
		    columns: [				        
		        { data: 'last_name' },
		        { data: 'first_name' },
		        { data: 'company_name' },
		        { data: 'phone' },
		        { data: 'email' },				        
		        { data: 'added' },
		        { data: 'modified' },				        
		        { data: 'action' }
		    ],
		    "aoColumnDefs": [
		        { "bSortable": false, "aTargets": [ 3, 5, 6, 7 ] }, 
		        { "bSearchable": false, "aTargets": [ 2,3,4,5,6,7 ] }
		    ],
		    order: [[ 4, "ASC" ]],
		    "language": { loadingRecords: '&nbsp;&nbsp;', processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
		    drawCallback: function( settings ) {
		        
		    }
		});
	}
}
$(document).ready(function() {
	/*$('.av_account_varification_account').click(function(){
		var av_account_data = $(this).attr('av_account_data');		
		$.ajax({
	        type : 'POST',
	        url  : "/Contact/create_lender_user_mail",
	        data : {'av_account_data':av_account_data},
	        success : function(response){	        	
	        	swal("Create vendor user email sent successfully!");
	        }
	    });
	});*/
	

	$('#lender_td_details').change(function(ev){
		ev.preventDefault();
		var contact_id = $(this).attr('contact_id');
		var lender_td_details = $(this).val();		
	    $.ajax({
	        type : 'POST',
	        url  : "/Pranav/Talimar/Contact/lender_td_details",
	        data : {'contact_id':contact_id,'lender_td_details':lender_td_details},
	        success : function(response){	        	
	        	swal("TD details has been successfully updated!");
	        }
	    });
	});

	$('.new_account_setup').click(function(ev){
		ev.preventDefault();
				
		var password 	= $(this).attr('password');
		var username 	= $(this).attr('username');
		var name 		= $(this).attr('name');
		var lender_id 	= $(this).attr('lender_id');
		var contact_email= $(this).attr('contact_email');

	    $.ajax({
	        type : 'POST',
	        url  : "/Pranav/Talimar/Contact/new_account_setup",
	        data : {'lender_id':lender_id,'name':name,'username':username,'password':password,'contact_email':contact_email},
	        success : function(response){	        	
	        	swal("New Account request has been sent successfully!");
	        }
	    });
	});

	init_datatable_for_contact();
	$('#internal_contact_ajax').change(function(){
		init_datatable_for_contact();
	});

	$('#company_name_ajax').change(function(){
		init_datatable_for_contact();
	});
	
	$('.ct_reset_filter_contacts').click(function(ct){
		ct.preventDefault();
		document.getElementById("form_filter_options").reset();

		$('#company_name_ajax').prop('selectedIndex',-1);
		$('#internal_contact_ajax').prop('selectedIndex',-1);

		$('select#company_name_ajax').trigger('chosen:updated');
		$('select#internal_contact_ajax').trigger('chosen:updated');

		$('select#lenderwithacc').trigger('chosen:updated');
		$('select#borrowerwithacc').trigger('chosen:updated');	
		$('#cp_contact_specialty').val('');
		$('#cp_contact_marketing_data').val('');
		$('#cp_contact_email_blast').val('');
		setTimeout(function(){
			init_datatable_for_contact();
		}, 300);
	});

	$('.ct_filter_contacts').click(function(ct){		
		ct.preventDefault();
		$(".content_filter_contacts").toggle(500);
		setTimeout(function(){ 
			$('.icons_active_filter').show();
			$('.icons_hide_filter').hide();
			if($('.content_filter_contacts').is(':visible')){
				$('.icons_active_filter').hide();
				$('.icons_hide_filter').show();
			}			
		}, 900);
	});

	$('body').on('click', '.ct_apply_filter', function(ct){
		ct.preventDefault();
		var Specialty = [];
		if($('.contactSpecialty').length > 0){
			$('.contactSpecialty').each(function(){
				if ($(this).is(':checked')) {	
					Specialty.push($(this).val());
				}
			});
		}
		$('#cp_contact_specialty').val(Specialty);

		var Marketing = [];
		if($('.contactMarketing').length > 0){
			$('.contactMarketing').each(function(){
				if ($(this).is(':checked')) {	
					Marketing.push($(this).val());
				}
			});
		}
		$('#cp_contact_marketing_data').val(Marketing);

		var SpecialtyEmail = [];
		if($('.contact_emailBlast').length > 0){
			$('.contact_emailBlast').each(function(){
				if ($(this).is(':checked')) {	
					SpecialtyEmail.push($(this).val());
				}
			});
		}
		$('#cp_contact_email_blast').val(SpecialtyEmail);
		$('.ct_filter_contacts').trigger( "click" );			
		init_datatable_for_contact();		
	});
});

function deleteContactUserInList(contact_id){	
	swal({
	  title: "Are you sure?",
	  text: "Delete all information for this user!",
	  icon: "warning",
	  buttons: true,
	  dangerMode: true,
	})
	.then((willDelete) => {
	  if (willDelete) {
		$.ajax({
	        type : 'POST',
	        url  : "/Pranav/Talimar/Contact/delete_contact",
	        data : {'contact_id':contact_id,'ajax_call':'yes'},
	        success : function(response){
	        	if(response == 'success'){
	        		swal("User information deleted successfully!", {icon: "success",});
	        		init_datatable_for_contact();
	        	}else{
	        		swal("Some error occurred please try again!", {icon: "error",});
	        	}	        	
	        }
	    });
	    
	  }
	});
}

function search_query(that){
	$.ajax({
		type:"POST",
		url:"/Pranav/Talimar/Contact/contactlist",
		data:"search="+ that,
		success: function(response) {		                        										
        }
	});
}
function displayRecords(numRecords,pageNum){
 	$.ajax({
		type: "POST",
		url: "/Pranav/Talimar/Contact/contactlist",
		data: "show=" + numRecords + "&pagenum=" + pageNum,
		cache: false,
		success: function(response) {
		}
	});							
}
/* used when user change row limit...*/
function changeDisplayRowCount(numRecords) {
	displayRecords(numRecords, 1);
}

function contact_row(value){
	
	var $row_count = value;
	$('#contact_row_limit').submit();
	
}

function contact_rows(value){
	
	var $row_count = value;
	$('#contact_internal').submit();
	
}
function submit_this(id){
	if($.isNumeric(id)){
		window.location.href = "/Pranav/Talimar/viewcontact/"+id;
    	return false;
 	}else{	
		$('#contact_search').submit();
		return false;
	}
}

function click_this(){
	$('#contact_search').submit();
}

function option_select(id){
	window.location.href = "/viewcontact/"+id;
}

function validate() {    
  if($('#borrower_chkbox_val').val() == '' && $('#lender_chkbox_val').prop('checked',false).val() == ''){
	  $('#chkbox_err').html('Please Choose Contact type Before Submit the form!').css('color','red');
	return false;	
  } else{
	   $('#chkbox_err').html('');
		return 1;
  }
}

function display_div(type){
	
	if($('#borrower_chkbox').prop('checked')  && $('#lender_chkbox').prop('checked')){
           /*$('.borrower_contact_type').css('display','block');
           $('.lender_contact_type').css('display','block');*/
		   $('#borrower_chkbox_val').val('1');
		   $('#lender_chkbox_val').val('1');
		   /*$('.borrower_enabled').prop('disabled',false);
		   $('.lender_enabled').prop('disabled',false);	*/	 
	}else if ($('#borrower_chkbox').prop('checked')) {
		   /*$('.borrower_contact_type').css('display','block');
           $('.lender_contact_type').css('display','none');*/
		   $('#borrower_chkbox_val').val('1');
		   $('#lender_chkbox_val').val('');		   
		   /*$('.borrower_enabled').prop('disabled',false);*/		   
    } else if($('#lender_chkbox').prop('checked')){
		   /*$('.borrower_contact_type').css('display','none');
           $('.lender_contact_type').css('display','block');*/
		   $('#borrower_chkbox_val').val('');
		   $('#lender_chkbox_val').val('1');
		   /*$('.lender_enabled').prop('disabled',false);*/           
    }else{
		   /*$('.borrower_contact_type').css('display','none');
           $('.lender_contact_type').css('display','none');*/
		   $('#borrower_chkbox_val').val('');
		   $('#lender_chkbox_val').val('');
		   /*$('.borrower_enabled').prop('disabled',true);
		   $('.lender_enabled').prop('disabled',true);*/
	}
	
}

function display_BK(value){
	if(value == '1'){
		$('.BK_optn').css('display','block');
	}else{
		$('.BK_optn').css('display','none');
		$('.BK_textarea_div').css('display','none');
		$("select#BK").val('0'); 
	}
}

function display_litigation(value){
	if(value == '1'){
		$('.litigation_optn').css('display','block');
	}else{
		$('.litigation_optn').css('display','none');		
	}
}

function display_felony(value){
	if(value == '1'){
		$('.felony_optn').css('display','block');
	}else{
		$('.felony_optn').css('display','none');
	}
}
function display_us_citizen(value){
	if(value == '0'){
		$('.us_citizen_optn').css('display','block');
	}else{
		$('.us_citizen_optn').css('display','none');
	}
}

function display_bk_txtarea(value){
	if(value == '1'){
		$('.BK_textarea_div').css('display','block');
	}else{
		
		$('.BK_textarea_div').css('display','none');
	}
}

function display_alternate_contact_fields(value){
	if(value == '1'){
		$('.alternate_contact_fields_display').css('display','block');
		$('#lender_alt_first_name') .prop('required',true);		
		$('#lender_alt_last_name')  .prop('required',true);
		
	}else{
		$('.alternate_contact_fields_display').css('display','none');
		$('#lender_alt_first_name') .prop('required',false);		
		$('#lender_alt_last_name')  .prop('required',false);
	}
}

function redirect_viewcontact(id){	
	window.location.href = "/viewcontact/"+id;
}

function borrower_select(id){	
	window.location.href = "/viewcontact/B"+id;	
}
function investor_select(id){	
	window.location.href = "/viewcontact/L"+id;	
}
function contact_select(id){	
	window.location.href = "/viewcontact/C"+id;	
}

function delete_contact(){
    if (confirm("Are you sure to delete?") == true) {
        $('#delete_contact_form').submit();
    } 
}

function copy_address(){
	if($('#same_mailing_address').attr('checked')){		
		var street_address 	= $('#address').val();
		var unit 			= $('#unit').val();
		var city 			= $('#city').val();
		var state 			= $('#state').val();
		var zip 			= $('#zip').val();
		
		$('#mailing_address').val(street_address);
		$('#mailing_unit').val(unit);
		$('#mailing_city').val(city);
		$('#mailing_state').val(state);
		$('#mailing_zip').val(zip);		
	}else{	
		$('#mailing_address').val('');
		$('#mailing_unit').val('');
		$('#mailing_city').val('');
		$('#mailing_state').val('');
		$('#mailing_zip').val('');
	}
}

function fill_mailing_portion(id,value){
	if($('#same_mailing_address').attr('checked')){
		$('#mailing_'+id).val(value);
	}else{
		$('#mailing_'+id).val('');		
	}
}
  
var i= 1;
function borrower_add_more_row(){
	  i++;		 
	 if(i < 6){		 
	  $( ".doc-div" ).append( '<br><div class="row"><div class="col-md-4"><input type="file" id = "borrower_upload_'+i+'" name ="borrower_upload_doc[]" class="select_images_file" onchange="change_this_image(this)" name="borrower_files[]"></div></div>' );
	 }	  
}
  
var j= 1;
function lender_add_more_row(){
	 j++;
	 if(j < 6){
		 
	  $( ".doc-div1" ).append( '<br><div class="row"><div class="col-md-4"><input type="file" id = "lender_upload_'+j+'" name ="lender_upload_doc[]" class="select_images_file" onchange="change_this_image(this)" name="lender_files[]"></div></div>' );
	 }  
  }
  
  
 var k= 1;

function filter_tag_add(){
	$('.filter_tag_contact_list').hide();
	$('.filter_tag_contact_list ul').html('');
	$('.filter_tag_main_title').hide();
	var lengthS = $('.contactSpecialty:checked').length;

	if(lengthS > 0){
		$('.filter_tag_main_title').show();
		$('.filter_tag_contact_specialty').show();
		var ix = 0;
		$('.contactSpecialty:checked').each(function(){
			if(ix < 2){
				var s_title = $(this).attr('title');
				$('.filter_tag_contact_specialty ul').append('<li>'+s_title+'</li>');
			}
			ix++;
		});
		if(lengthS > 2){
			$('.filter_tag_contact_specialty ul').append('<li>More Specialty...</li>');
		}
	}

	var lengthM = $('.contactMarketing:checked').length;

	if(lengthM > 0){
		$('.filter_tag_main_title').show();
		$('.filter_tag_marketing_data').show();
		var ix = 0;
		$('.contactMarketing:checked').each(function(){
			if(ix < 2){
				var s_title = $(this).attr('title');
				$('.filter_tag_marketing_data ul').append('<li>'+s_title+'</li>');
			}
			ix++;
		});
		if(lengthM > 2){
			$('.filter_tag_marketing_data ul').append('<li>More Marketing...</li>');
		}
	}


	var lengthE = $('.contact_emailBlast:checked').length;
	if(lengthE > 0){
		$('.filter_tag_main_title').show();
		$('.filter_tag_marketing_blast').show();
		var ix = 0;
		$('.contact_emailBlast:checked').each(function(){
			if(ix < 2){
				var E_title = $(this).attr('title');
				$('.filter_tag_marketing_blast ul').append('<li>'+E_title+'</li>');
			}
			ix++;
		});
		if(lengthE > 2){
			$('.filter_tag_marketing_blast ul').append('<li>More Marketing...</li>');
		}
	}


	var lenderwithacc = $('#lenderwithacc').val();
	if(lenderwithacc != ''){
		$('.filter_tag_main_title').show();
		$('.filter_tag_lender_data').show();
		var L_title = $('#lenderwithacc').find(':selected').text();
		$('.filter_tag_lender_data ul').append('<li>'+L_title+'</li>');
	}
	var borrowerwithacc = $('#borrowerwithacc').val();
	if(borrowerwithacc != ''){
		$('.filter_tag_main_title').show();
		$('.filter_tag_borrower_data').show();
		var B_title = $('#borrowerwithacc').find(':selected').text();
		$('.filter_tag_borrower_data ul').append('<li>'+B_title+'</li>');
	}
}

function tag_filter(value,tag_type,id){	
	console.log("value"+value);
	console.log("tag"+tag_type);
	console.log("id name"+id);
	var tag_label = '';
	if($('#'+tag_type+'_tag_'+value).prop('checked') == true){		
		$('#'+tag_type+'_'+value).val('1');
		$('#'+tag_type+'_val_'+value).val(value);
		$('#deselect_all').prop('checked',false);
	}else{
		$('#'+tag_type+'_'+value).val('0');		
	}
}

function check_all_filter(){	
	if($('#select_all').prop('checked') == true){
		$('.tag_fields').val('1');
		$('.all_tags').prop('checked',true);
		$('#deselect_all').prop('checked',false);
	}else{
		
		$('.tag_fields').val('0');
		$('.all_tags').prop('checked',false);
	}
}

function uncheck_all_filter(){
	if($('#deselect_all').prop('checked') == true){
		
		$('.all_tags').prop('checked',false);
		$('#select_all').prop('checked',false);
		$('.tag_fields').val('0');
	}else{
		$('.tag_fields').val('0');
		$('.all_tags').prop('checked',false);
	}	
}

function check_all_filter_marketingData(){	
	if($('#select_all_marketingData').prop('checked') == true){
		$('.tag_fields_marketingData').val('1');
		$('.all_tags_marketingData').prop('checked',true);
		$('#deselect_all_marketingData').prop('checked',false);
	}else{
		
		$('.tag_fields_marketingData').val('0');
		$('.all_tags_marketingData').prop('checked',false);
	}
}

function uncheck_all_filter_marketingData(){
	if($('#deselect_all_marketingData').prop('checked') == true){
		
		$('.all_tags_marketingData').prop('checked',false);
		$('#select_all_marketingData').prop('checked',false);
		$('.tag_fields_marketingData').val('0');
	}else{
		$('.tag_fields_marketingData').val('0');
		$('.all_tags_marketingData').prop('checked',false);
	}	
}

function pdf_download_fun(){

	$('form#pdf_download').submit();

}
$(document).ready(function(){
	$(".chosen").chosen({
		search_contains: true
	});
	displayRecords(25, 1);

	if($('#borrower_chkbox').prop('checked')  && $('#lender_chkbox').prop('checked')){
           /*$('.borrower_contact_type').css('display','block');
           $('.lender_contact_type')  .css('display','block');*/
		   $('#borrower_chkbox_val').val('1');
		   $('#lender_chkbox_val').val('1');
		  /* $('.borrower_enabled').prop('disabled',false);
		   $('.lender_enabled').prop('disabled',false);*/
		 
	}else if ($('#borrower_chkbox').prop('checked')) {
		  /* $('.borrower_contact_type').css('display','block');
           $('.lender_contact_type')  .css('display','none');*/
		   $('#borrower_chkbox_val').val('1');
		   $('#lender_chkbox_val').val('');
		   
		   /*$('.borrower_enabled').prop('disabled',false);*/
		   
    } else if($('#lender_chkbox').prop('checked')){
		   /*$('.borrower_contact_type').css('display','none');
           $('.lender_contact_type')  .css('display','block');*/
		   $('#borrower_chkbox_val').val('');
		   $('#lender_chkbox_val').val('1');
		   /*$('.lender_enabled').prop('disabled',false);*/
           
    }else{
		   /*$('.borrower_contact_type').css('display','none');
           $('.lender_contact_type')  .css('display','none');*/
		   $('#borrower_chkbox_val').val('');
		   $('#lender_chkbox_val').val('');
		   /*$('.borrower_enabled').prop('disabled',true);
		   $('.lender_enabled').prop('disabled',true);*/
	}
	if($('#us_citizen').val() == '0'){
	
		$('.us_citizen_optn').css('display','block');
	
	}else{
		
		$('.us_citizen_optn').css('display','none');
	}
	
	if($('#litigation').val() == '1'){
		
		$('.litigation_optn').css('display','block');
		
	}else{
		$('.litigation_optn').css('display','none');
		
	}
	
	if($('#felony').val() == '1'){
		
		$('.felony_optn').css('display','block');
		
	}else{
		$('.felony_optn').css('display','none');
		
	}

	
	var value= $('#alternate_contact_fields').val();
	if(value == '1'){
		$('.alternate_contact_fields_display').css('display','block');
		$('#lender_alt_first_name') .prop('required',true);
		// $('#lender_alt_middle_name').prop('required',true);
		$('#lender_alt_last_name')  .prop('required',true);
		
	}else{
		$('.alternate_contact_fields_display').css('display','none');
		$('#lender_alt_first_name') .prop('required',false);
		// $('#lender_alt_middle_name').prop('required',false);
		$('#lender_alt_last_name')  .prop('required',false);

	}

	/***phone number format***/
	var curchr = '';
	if($('.phone-format').val()){
		curchr = $('.phone-format').val().length;	
	} 	
    var curval = $('.phone-format').val();
    if (curchr == 3 && curval.indexOf("(") <= -1) {
		
      $('.phone-format').val("(" + curval + ")" + "-");
    } else if (curchr == 4 && curval.indexOf("(") > -1) {
      $('.phone-format').val(curval + ")-");
    } else if (curchr == 5 && curval.indexOf(")") > -1) {
      $('.phone-format').val(curval + "-");
    } else if (curchr == 9) {
      $('.phone-format').val(curval + "-");
      $('.phone-format').attr('maxlength', '14');
    }
});

$.noConflict();

function remove_this_selecttag(id){
	$('#spe_'+id).remove();
}

function remove_this_selecttag1(id){
	$('#ebla_'+id).remove();
}
function check_all_filter_marketing() {
    if ($('#select_all_marketing').prop('checked') == true) {
        $('.tag_fields_marketing').val('1');
        $('.all_tags_marketing').prop('checked', true);
        $('#deselect_all_marketing').prop('checked', false);
    } else {

        $('.tag_fields_marketing').val('0');
        $('.all_tags_marketing').prop('checked', false);
    }
}

function uncheck_all_filter_marketing() {
    if ($('#deselect_all_marketing').prop('checked') == true) {
        $('.all_tags_marketing').prop('checked', false);
        $('#select_all_marketing').prop('checked', false);
        $('.tag_fields_marketing').val('0');
    } else {
        $('.tag_fields_marketing').val('0');
        $('.all_tags_marketing').prop('checked', false);
    }
}
