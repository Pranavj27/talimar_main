$(document).ready(function() {
	$('body').on('click', '#opt_out_value_yes_4', function(){
		if ($(this).prop('checked')) {
			$('#hide_mail6').prop('checked', false);
			$('#l_hide_yes_7').prop('checked', false);
			$('#opt_out_value_yes_12').prop('checked', false);

			$('#hide_mail6').attr('disabled',true);
			$('#l_hide_yes_7').attr('disabled',true);
		}else{
			$('#hide_mail6').attr('disabled',false);
			$('#l_hide_yes_7').attr('disabled',false);
		}
	});

	$('body').on('click', '#opt_out_value_yes_5', function(){
		if ($(this).prop('checked')) {
			$('#l_hide_yes_2').prop('checked', false);
			$('#hide_yes_3').prop('checked', false);
			$('#hide_yes_1').prop('checked', false);
			$('#opt_out_value_yes_12').prop('checked', false);

			$('#hide_yes_1').attr('disabled',true);
			$('#hide_yes_3').attr('disabled',true);
			$('#l_hide_yes_2').attr('disabled',true);
		}else{
			$('#hide_yes_1').attr('disabled',false);
			$('#hide_yes_3').attr('disabled',false);
			$('#l_hide_yes_2').attr('disabled',false);
		}
	});

	$('body').on('click', '#opt_out_value_yes_12', function(){
		if ($(this).prop('checked')) {
			$('.Borrower_checkboxes').prop('checked', false);
			$('.Lender_checkboxes').prop('checked', false);
			$('#opt_out_value_yes_4').prop('checked', false);
			$('#opt_out_value_yes_5').prop('checked', false);

			$('.Borrower_checkboxes').attr('disabled',true);
			$('.Lender_checkboxes').attr('disabled',true);
			$('#opt_out_value_yes_4').attr('disabled',true);
			$('#opt_out_value_yes_5').attr('disabled',true);
		}else{
			$('.Borrower_checkboxes').attr('disabled',false);
			$('.Lender_checkboxes').attr('disabled',false);
			$('#opt_out_value_yes_4').attr('disabled',false);
			$('#opt_out_value_yes_5').attr('disabled',false);
		}
	});

});