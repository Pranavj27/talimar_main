function load_notes_data_new(){
		var page_no=$("#notes_page_no").val();
		var contact_list_id = $('#contact_list_id').val();
		page_no=parseInt(page_no)+1;
		$(".loader_image").html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:12px"></i>');
		$.ajax({
	        type : 'POST',
	        url  : "/Pranav/Talimar/Contact/contact_note_list",
	         data : {'contact_id':contact_list_id,'page_no':page_no},
	        success : function(response){
	        	$("#list_contact_note_data").append(response);
	        	var total_records = $('#total_record_notes_data').val();
	        	total_records=parseInt(total_records);
		        if(total_records>10 || response==""){
		        	$("#loader_link_btn").hide();
		        }else{
		        	$("#notes_page_no").val(page_no); 
		        	$(".loader_image").html('');
		        }	
	        	
	        }
	    });
	}
function edit_relationship_details(t){
	var id = t.id;
	$('div#edit-modal select#relationshipss option').attr('selected',false);
	$('div#edit-modal select#reltionn option').attr('selected',false);
	$.ajax({
        type: "post",
		url: "/Pranav/Talimar/Contact/fetch_data",
		data:{'id':id},
		success : function(response){
			if(response){

				var data = JSON.parse(response);


				$('div#edit-modal select#relationshipss option[value="'+data.relationship_contact_id+'"]').attr('selected',true);
				$('div#edit-modal select#reltionn option[value="'+data.relation_option+'"]').attr('selected',true);
				if(data.show_lender_portal=="1"){
					$('#edit_show_lender_portal').prop('checked', true);
				}

				$('div#edit-modal textarea#notes').text(data.relationship_note);
				$('div#edit-modal input#u_id').val(data.id);
				$('div#edit-modal').modal('show');



			}
		}
  });
}

function view_relationship_details(that){
	var id = that.id;
	$('div#view-modal select#relationships option').attr('selected',false);
	$('div#view-modal select#reltionn option').attr('selected',false);
	$.ajax({
        type: "post",
		url: "/Pranav/Talimar/Contact/fetch_data",
		data:{'id':id},
		success : function(response){
			if(response){

				var data = JSON.parse(response);

				$('div#view-modal select#relationships option[value="'+data.relationship_contact_id+'"]').attr('selected',true);
				$('div#view-modal select#reltionn option[value="'+data.relation_option+'"]').attr('selected',true);
				if(data.show_lender_portal=="1"){
					$('#view_show_lender_portal').prop('checked', true);
				}


				$('div#view-modal textarea#notes').text(data.relationship_note);
				$('div#view-modal').modal('show');
			}
		}
  });
}

function lenderTransectionform(that){
	
	$('form#lendertra_form').submit();
}
function AffiliatedTransectionform(that){
	
	$('form#affiliated_form').submit();
}
function borrowerTransectionform(that){
	
	$('form#borrowertra_form').submit();
}

function editContactNotes(noteId){
	$.ajax({
			type : 'POST',
			url  : '/Pranav/Talimar/Contact/GetcontactNotes',
			data : {'noteId':noteId},
			success : function(response){
				
				var data = JSON.parse(response);
				var notes_date = formatDate(data.notes_date);
				$('#updateNotesdata input[name="updateid"]').val(data.id);
				$('#updateNotesdata input[name="contact_id"]').val(data.contact_id);
				$('#updateNotesdata input[name="notedate"]').val(notes_date);
				$('#updateNotesdata select[name="notetype"] option[value="'+data.notes_type+'"]').attr('selected','selected');
				$('#updateNotesdata select[name="purpose_type"] option[value="'+data.purpose_type+'"]').attr('selected','selected');
				$('#updateNotesdata textarea[name="notetext"]').val(data.notes_text);
				
				$('#updateNotesdata').modal('show');
			}
		
	});
	
}

function formatDate(date) {
 var d = new Date(date),
	 month = '' + (d.getMonth() + 1),
	 day = '' + d.getDate(),
	 year = d.getFullYear();

 if (month.length < 2) month = '0' + month;
 if (day.length < 2) day = '0' + day;

 return [month, day, year].join('-');
}

function display_how_may_box(that){
	if(that == 1){
		$('#how_may_box').css('display','block');
	}else{
		$('#how_may_box').css('display','none');
	}
}

function relationship_edit_fun(e){
	var u_id=$(e).parent().parent().find('input#u_id').val();
	var relationship_note = $('textarea.v').val();
	var relationship = $(e).parent().parent().find('#relationshipss option:selected').val();
	var relation_option = $(e).parent().parent().find('#reltionn option:selected').val();
	if($('input[name="edit_show_lender_portal"]').is(':checked'))
	{
	  	var show_lender_portal=1;
	}else
	{
	 	var show_lender_portal=0;
	}
	$.ajax({
        type: "post",
		url: "/Pranav/Talimar/Contact/edit_update_data",
		data:{'id':u_id,'relationship_note':relationship_note,'relation_option':relation_option,'relationship':relationship,'show_lender_portal':show_lender_portal},
		success : function(response){
		//$("div.msg").empty();
		//$("div.msg").append('<i class="fa fa-thumbs-o-up"></i> Data Updated Successfully').addClass("alert alert-success");
		window.location.reload();
		}
	});
}

function display_package_date_nda_signed(tt){
	var val1 = tt;
	if(val1 == '1'){
		$('input[name="nda_date"]').attr('disabled',false);
	}else{

		$('input[name="nda_date"]').attr('disabled',true);
	}
}

function financial_value(that){
	var value = that.value;	
	if($(that).is(':checked')){
		$('input#financial_situation_value').val(value);
	}else{
		$('input#financial_situation_value').val('');
	}
}

function net_worth_financial_value(that){
	var value = that.value;
	if($(that).is(':checked')){
		$('input#net_worth_financial_situation_value').val(value);
	}else{
		$('input#net_worth_financial_situation_value').val('');
	}
}

function liquid_financial_value(that){
	var value = that.value;
	if($(that).is(':checked')){
		$('input#liquid_financial_situation_value').val(value);
	}else{
		$('input#liquid_financial_situation_value').val('');
	}
}

function liquidity_needs_value(that){
	var value = that.value;
	if($(that).is(':checked')){
		$('input#lender_liquidity_needs_chkbx').val(value);
	}else{
		$('input#lender_liquidity_needs_chkbx').val('');
	}
}

function loan_borrower_report(contact_id){	
	$('form#loan_borrower_reportdata input[name="contact_ids"]').val(contact_id);
	$('form#loan_borrower_reportdata').submit();
}

function investment_experience_value(that){
	var value = that.value;
	if($(that).is(':checked')){
		$('input#invest_ex_checkbox_value').val(value);
	}
}

function display_bankruptcy_7year(value){	
	if(value == '1'){
		$('.bankruptcy_7year_text').css('display','block');
	}else{
		$('.bankruptcy_7year_text').css('display','none');
	}
}

function display_us_citizenNew(value){
	if(value == '2'){
		$('.us_citizen_optn').css('display','block');
	}else{
		$('.us_citizen_optn').css('display','none');
		
	}
}

function delete_contact_task(id){
	var c_id = id;
	if(confirm('Are you Sure To Delete?')){

		$.ajax({
			type : 'POST',
			url	 : '/Pranav/Talimar/Contact/delete_tasks',
			data : { 'id':c_id },
			success : function(response){
				if(response == 1){
					$('div#contact_task_'+id).remove();
				}
			}

		});
	}
}

function delete_doc(that){
	var doc_id = that.id;
	if(confirm('Are You Sure To Delete?')){
		$.ajax({
			type	:'POST',
			url		:'/Pranav/Talimar/Contact/delete_document',
			data	:{'id':doc_id},
			success : function(response){
				if(response == 1){
					$(that).parent().parent('li').remove();
					window.location.reload();
				}
			}

		});
	}
}

function notes_delete_row(id,contact_id){
	if (confirm("Are you sure to delete this note?") == true) {
       window.location.href="/Pranav/Talimar/delete_notes/"+id+"/"+contact_id;
    }
}

function fetch_notes(value){
	$.ajax({
		type: "post",
		url: "/Pranav/Talimar/Contact/fetch_notes/",
		data:{'id':value},
		success: function(response){
			if(response){
				$('#historyfrm').css('display','block');
				var data  = JSON.parse(response);

				$('#fetch_notes_id').val(data.id);
				$('#fetch_contact_id').val(data.contact_id);


				$('#label_notes_date').html(data.notes_date);
				$('#fetch_notes_date').val(data.notes_date);


				$('#label_notes_type').html(data.notes_type_name);
				$('#fetch_notes_type').val(data.notes_type_name);
				$('#fetch_notes_type_id').val(data.notes_type);
				$('#fetch_select_notes_type').val(data.notes_type_name);
				$('.select_notes_type .bootstrap-select .filter-option').text(data.notes_type_name);
				$('select[name=fetch_notes_type]').val(data.notes_type);


				 $('#label_loan_id').html(data.talimar_loan);
				$('#fetch_talimar_loan').val(data.talimar_loan);
				$('#fetch_select_talimar_loan').val(data.loan_id);
				$('.select_loan_id .bootstrap-select .filter-option').text(data.talimar_loan);

				$('#label_loan_id').html(data.property_address);
				$('#fetch_talimar_loan').val(data.property_address);
				$('#fetch_select_talimar_loan').val(data.loan_id);
				$('.select_loan_id .bootstrap-select .filter-option').text(data.property_address);

				$('#label_personal_notes').html(data.notes_text);
				$('#fetch_personal_notes').html(data.notes_text);
			}
		}
	});

}
function contact_relationship_action(th)
{
	var id=$(th).attr('id');
	var contact_id=$(th).parent('div').find('#x_id').val();

	if(confirm('Are you sure want to Delete')){
		$.ajax({
			type: "post",
			url: "/Pranav/Talimar/Contact/contact_relationship_delete",
			data:{'id':id,'contact_id':contact_id},
			success: function(response){
		      	$(th).parent('div').parent().remove();
			}
		});
	}
}

function edit_contact_task(id){
	$('#contact_task_'+id).css('display','none');
	$('.select_task_'+id).css('display','block');
}

function relation_field_hide(){
	$('.relationship_div').css('display','none');
}

function check_permission_option(value,id){
	if(value == '3'){
		$('.particular_user_dropdown').css('display','block');

		if($('#checkbx_2').prop('checked') == true){
			$('#checkbx_2').prop('checked',false);


		}
		if($('#checkbx_3').prop('checked') == false){
			$('.particular_user_dropdown').css('display','none');
		}
	
	}else if(value == '2'){

		if($('#checkbx_2').prop('checked') == true){

			$('#checkbx_1').attr('disabled',true);
			$('#checkbx_4').attr('disabled',true);
			$('#checkbx_5').attr('disabled',true);
			$('#checkbx_3').attr('disabled',true);

			$('#checkbx_1').prop('checked',false);
			$('#checkbx_4').prop('checked',false);
			$('#checkbx_5').prop('checked',false);
			$('#checkbx_3').prop('checked',false);
			$('#particular_user_id').val('');

		}
		 else{
			// $('#checkbx_3').prop('checked',true);
			$('#checkbx_1').attr('disabled',false);
			$('#checkbx_4').attr('disabled',false);
			$('#checkbx_5').attr('disabled',false);
			$('#checkbx_3').attr('disabled',false);

		}
		$('.particular_user_dropdown').css('display','none');
	}else if(value == '1'){

		$('#checkbx_3').prop('checked',false);
		$('.particular_user_dropdown').css('display','none');
	}else{
		/*$('.particular_user_dropdown').css('display','none');*/
	}

}

function check_filename_duplicacy(id,contact_id){

	var new_filename = $('#new_filename'+id).val();
	var old_filename = $('#old_filename'+id).val();

	var new_extension = new_filename.substr( (new_filename.lastIndexOf('.') +1) );
	var old_extension = old_filename.substr( (old_filename.lastIndexOf('.') +1) );
	switch(new_extension) {
		case 'pdf':
			var filename =  new_filename;
			$('#new_filename'+id).val(filename);

		break;
		default:
			var filename = new_filename+'.'+old_extension;
			$('#new_filename'+id).val(filename);
	}

	/*ajax call to check duplicacy of file name....*/
	$.ajax({
		type 	: 'post',
		url  	: '/Pranav/Talimar/Contact/check_contact_filename',
		data	: { 'filename' : filename,'contact_id':contact_id},
		success : function(response){
			if(response == '1'){
				$('#new_filename'+id).val(' ').focus();
				alert('Filename: '  + filename + ' already exist for this Contact, Please try another filename!' );
			}
		}

	});

}

function SendUsername(contact_id,user_type,username)
{
	$.ajax({
        type : 'POST',
        url  : '/Pranav/Talimar/Contact/SendUsername',
        data : {'contact_id':contact_id,'user_type':user_type,'username':username},
        success : function(response){
	        var json =  JSON.parse(response);
			var message = json.message;
			if(user_type==1)
			{
				
				$("#messagesChange").html(message);
			}
			else
			{
				$("#messagesChange1").html(message);
			}
        }
	});
}


function SendLinkToForget(contact_id,user_type,username)
{
	$.ajax({
        type : 'POST',
        url  : '/Pranav/Talimar/Contact/sendLinkToForgetPassword',
        data : {'contact_id':contact_id,'user_type':user_type,'username':username},
        success : function(response){
	        var json =  JSON.parse(response);
			var message = json.message;
			if(user_type==1)
			{
				$("#messagesChange").html(message);
			}
			else
			{
				$("#messagesChange1").html(message);
			}
        }
	});
}

function load_modal(taa){
	$('input[name="popup_data"]').val('lender-data');
	/*$('#lender_data').modal('show');*/
	
	var WholeNoteInvestor = $("input[name='WholeNoteInvestor']:checked").val();
	var ownership_type = $("input[name='ownership_type']:checked").val();
	var lend_value = $("input[name='lend_value']:checked").val();
	var ltv = $("input[name='ltv[]'][type=checkbox]:checked");
	var position = $("input[name='position[]'][type=checkbox]:checked");
	var loan = $("input[name='loan[]'][type=checkbox]:checked");
	var property_type = $("input[name='loan[]'][type=checkbox]:checked");
	var yiled= $("input[name='yield[]'][type=checkbox]:checked");

	var property_type = $("input[name='property_type[]'][type=checkbox]:checked");
	var committed_funds= $('input#committed_funds').val();

	if((lend_value !='1' && lend_value !='2' || WholeNoteInvestor !='1' && WholeNoteInvestor !='2' || ownership_type !='1' && ownership_type !='2' || ltv.length=='' || position.length=='' || loan.length=='' || property_type.length=='' || yiled.length=='')){
		$("span#show_img").css({"margin-left": "155px", "margin-top": "-22px", "display":"block"});
  	}
  	if(committed_funds=='$'){
		$("span#show_imgg").css({"margin-left": "127px", "margin-top": "-24px", "display":"block"});
  	}
}

function fill_relationship_id(id,value){
	$('#relationship_contact_id'+id).val(value);
	$('#editrelationfrm'+id).submit();
}
function display_inputfield(id){
	var filename = $('#filename_'+id).data('name');

	$('.new_filename_div'+id).toggle();
	$('#contact_document_id'+id).val(id);
	$('#old_filename'+id).val(filename);

}
function fieldHideshow()
{
	var select = $("#employmentId").find(":selected").val();
	if(select == 'yes')
	{		
		$("input[name='current_position']").prop("disabled", false);
		$("input[name='cp_title']").prop("disabled", false);
		$("input[name='cp_length_of_position']").prop("disabled", false);
	}
	else
	{		
		$("input[name='current_position']").val('');
		$("input[name='cp_title']").val('');
		$("input[name='cp_length_of_position']").val('');

		$("input[name='current_position']").prop("disabled", true);
		$("input[name='cp_title']").prop("disabled", true);
		$("input[name='cp_length_of_position']").prop("disabled", true);
	}
}
function delete_contact(){
    if (confirm("Are you sure to delete?") == true) {
        $('#delete_contact_form').submit();
    } 
}

function display_litigation(value){
	if(value == '1'){
		$('.litigation_optn').css('display','block');
	}else{
		$('.litigation_optn').css('display','none');		
	}
}
function display_felony(value){
	if(value == '1'){
		$('.felony_optn').css('display','block');
	}else{
		$('.felony_optn').css('display','none');
	}
}
function display_bk_txtarea(value){
	if(value == '1'){
		$('.BK_textarea_div').css('display','block');
	}else{
		
		$('.BK_textarea_div').css('display','none');
	}
}
$(document).ready(function() {

	$('body').on('click', '.borrower_data_pop', function(){
		$('input[name="popup_data"]').val('borrower_data');
	});



	$('#export-menu li').bind("click", function() {
		var target 	= $(this).attr('class');
		var id 		= $(this).data('id');
		var optn 	= $(this).data('option');
		$('.hidden-type').val(target);

		 if(target == 'delete-note'){
			if (confirm("Are you sure to delete this note?") == true) {
				$('#selection_note_id_'+id).val(id);
				$('#frmq'+id).submit();
			}
		}else{


			$('.label_notes_date'+id).css('display','none');
			$('.input_notes_date'+id).css('display','block');

			$('.label_notes_type'+id).css('display','none');
			$('.select_notes_type'+id).css('display','block');

			$('.select_loan_id'+id).css('display','block');
			$('.label_loan_id'+id).css('display','none');

			$('.input_notes_text'+id).css('display','block');
			$('.label_notes_text'+id).css('display','none');
			$('#history_formsubmit_'+id).css('display','block');
			
		}
	});

	
	$('body').on('click', '#ct_loan_saved_documentadd', function(){
		$(".ct_loan_saved_document").toggle('slow');
	});


	var val = $('div#lender_data select[name="nda_signed"]').val();
	display_package_date_nda_signed(val);

	var val = $('div#lender_data select[name="re_complete"]').val();
	display_package_date_of_re(val);

	var val = $('div#lender_data select[name="setup_package"]').val();	



	$(".amount_format").keyup(function(){
		var a = this.value;
		var a = replace_dollar(a);
		if(a == '')
		{
			a = 0;
		}
		a = parseFloat(a);
		this.value = '$'+number_format(a);
	});

	


	var contact_list_id = $('#contact_list_id').val();
	var modelsNames=[ 'borrower_data','lender_data','brokerData','hardmoney','marketing','contact_document','contact_modal_tasks','add_relationship','edit_relationship','view_relationship','contact_tags','statistics','updateNotesdata','assigned' ]
	 
	$.each(modelsNames, function( index, value ) {
		$.ajax({
	        type : 'POST',
	        url  : "/Pranav/Talimar/Contact/contact_modal_popups",
	        data : {'modal':value,'contact_id':contact_list_id},
	        success : function(response){	
	        	if(value == 'borrower_data' || value == 'lender_data' || value == 'contact_modal_tasks' || value == 'contact_tags'){
	        		
	        		if(value == 'lender_data'){
	        			$('.contactlist_list_modal_form').append(response).selectpicker('refresh');
	        			$(".selectpicker").each(function() {
						  	$( this ).selectpicker('refresh');
						});
						$('.av_account_varification_account').click(function(){
							var av_account_data = $(this).attr('av_account_data');		
							$.ajax({
						        type : 'POST',
						        url  : "/Pranav/Talimar/Contact/create_lender_user_mail",
						        data : {'av_account_data':av_account_data},
						        success : function(response){	        	
						        	swal("Create vendor user email sent successfully!");
						        }
						    });
						});
			    		
	        		}
	        		else if(value == 'borrower_data'){
	        			$('.contactlist_list_modal_form').append(response);
	        			var ee = $('#browser_data_disclouser_id').val();
						if(ee == ''){
							$('#checkbx_2').prop('checked',true);
							$('#checkbx_1').attr('disabled',true);
							$('#checkbx_3').attr('disabled',true);
							$('#checkbx_4').attr('disabled',true);
							$('#checkbx_5').attr('disabled',true);
						}
						var us_citizen=$('#us_citizen').val();
						if($('#us_citizen').val() == '2'){
							$('.us_citizen_optn').css('display','block');
						}else{
							$('.us_citizen_optn').css('display','none');
						}
						if($('#litigation').val() == '1'){

							$('.litigation_optn').css('display','block');

						}else{
							$('.litigation_optn').css('display','none');
						}
						if($('#felony').val() == '1'){

							$('.felony_optn').css('display','block');

						}else{
							$('.felony_optn').css('display','none');
						}
						if($('#Bankruptcy').val() == '1'){

							$('.BK_optn').css('display','block');

						}else{
							$('.BK_optn').css('display','none');
						}
						if($('#BK').val() == '1'){
							$('.BK_textarea_div').css('display','block');
						}else{
							$('.BK_textarea_div').css('display','none');
						}
						if($('select#text_7year').val() == '1'){
							$('.bankruptcy_7year_text').css('display','block');
						}else{
							$('.bankruptcy_7year_text').css('display','none');
						}
	        		}else{
	        			$('.contactlist_list_modal_form').append(response);
	        		}
	        	}else{
	        		$('.contactlist_list_modal').append(response);	
	        	}

	        	if(value=="assigned"){
	        		$('#lender_data').each(function (i) {
	        			if(i==1){
	        				this.remove();
	        			}
					});
			    	$('.page-container').removeClass('load-screen');
			    	var popup_modal_hit = $("#popup_modal_hit").val();
					if(popup_modal_hit == 'lender-data'){
					    $('#lender_data').modal('show');
					}else if(popup_modal_hit == 'borrower_data'){
						$('#borrower_data').modal('show');
					}else{

					}
			    }
	        	$(".chosen").chosen({
					search_contains: true
				});	        	
	        	$('.datepicker').datepicker({ dateFormat: 'mm-dd-yy' });	
	        }
	    });
	    
	  
	});
	
	var page_no=0;
	
	load_notes_data_new();
	// $(document).on("click", ".pagination li a", function(e){
 //      e.preventDefault();
 //      var pageId = $(this).attr("id");
 //      load_notes_data();
 //    });
	

	

	var value = $('#borrower_experience_selectbox select[name="borrower_experience"]').val();
	display_how_may_box(value);
	$('#contactActivetable').DataTable();

	fieldHideshow();
	
	$('#lender_td_details').change(function(ev){
		ev.preventDefault();
		var contact_id = $(this).attr('contact_id');
		var lender_td_details = $(this).val();		
	    $.ajax({
	        type : 'POST',
	        url  : "/Pranav/Talimar/Contact/lender_td_details",
	        data : {'contact_id':contact_id,'lender_td_details':lender_td_details},
	        success : function(response){	        	
	        	swal("TD details has been successfully updated!");
	        }
	    });
	});

	

	if($('#borrower_chkbox').prop('checked')  && $('#lender_chkbox').prop('checked')){
           /*$('.borrower_contact_type').css('display','block');
           $('.lender_contact_type')  .css('display','block');*/
		   $('#borrower_chkbox_val').val('1');
		   $('#lender_chkbox_val').val('1');
		  /* $('.borrower_enabled').prop('disabled',false);
		   $('.lender_enabled').prop('disabled',false);*/
		 
	}else if ($('#borrower_chkbox').prop('checked')) {
		  /* $('.borrower_contact_type').css('display','block');
           $('.lender_contact_type')  .css('display','none');*/
		   $('#borrower_chkbox_val').val('1');
		   $('#lender_chkbox_val').val('');
		   
		   /*$('.borrower_enabled').prop('disabled',false);*/
		   
    } else if($('#lender_chkbox').prop('checked')){
		   /*$('.borrower_contact_type').css('display','none');
           $('.lender_contact_type')  .css('display','block');*/
		   $('#borrower_chkbox_val').val('');
		   $('#lender_chkbox_val').val('1');
		   /*$('.lender_enabled').prop('disabled',false);*/
           
    }else{
		   /*$('.borrower_contact_type').css('display','none');
           $('.lender_contact_type')  .css('display','none');*/
		   $('#borrower_chkbox_val').val('');
		   $('#lender_chkbox_val').val('');
		   /*$('.borrower_enabled').prop('disabled',true);
		   $('.lender_enabled').prop('disabled',true);*/
	}
	if($('#us_citizen').val() == '0'){
	
		$('.us_citizen_optn').css('display','block');
	
	}else{
		
		$('.us_citizen_optn').css('display','none');
	}
	
	if($('#litigation').val() == '1'){
		
		$('.litigation_optn').css('display','block');
		
	}else{
		$('.litigation_optn').css('display','none');
		
	}
	
	if($('#felony').val() == '1'){
		
		$('.felony_optn').css('display','block');
		
	}else{
		$('.felony_optn').css('display','none');
		
	}

	
	var value= $('#alternate_contact_fields').val();
	if(value == '1'){
		$('.alternate_contact_fields_display').css('display','block');
		$('#lender_alt_first_name') .prop('required',true);
		// $('#lender_alt_middle_name').prop('required',true);
		$('#lender_alt_last_name')  .prop('required',true);
		
	}else{
		$('.alternate_contact_fields_display').css('display','none');
		$('#lender_alt_first_name') .prop('required',false);
		// $('#lender_alt_middle_name').prop('required',false);
		$('#lender_alt_last_name')  .prop('required',false);

	}

	/***phone number format***/
	var curchr = '';
	if($('.phone-format').val()){
		curchr = $('.phone-format').val().length;	
	} 	
    var curval = $('.phone-format').val();
    if (curchr == 3 && curval.indexOf("(") <= -1) {
		
      $('.phone-format').val("(" + curval + ")" + "-");
    } else if (curchr == 4 && curval.indexOf("(") > -1) {
      $('.phone-format').val(curval + ")-");
    } else if (curchr == 5 && curval.indexOf(")") > -1) {
      $('.phone-format').val(curval + "-");
    } else if (curchr == 9) {
      $('.phone-format').val(curval + "-");
      $('.phone-format').attr('maxlength', '14');
    }

    $('.datepicker').datepicker({ dateFormat: 'mm-dd-yy' });
	$(".chosen").chosen({
		search_contains: true
	});
});

$.noConflict();