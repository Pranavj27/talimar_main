var search_value_check="";
var req ="";
function init_datatable_for_contact(){
	var totalRecord=0;	
	var dispatching_list_all = '#schedule_by_contact';
	$(dispatching_list_all).DataTable().clear().destroy();  
	if( $( dispatching_list_all ).length ){		
		$dis_list_all = $( dispatching_list_all ).DataTable({
			processing: true,
			serverSide: true,
			pageLength: 10,
			bFilter: true,
		    ajax: {
				dataSrc:"data",
				data:function(data){
					var contact_id 		= $('select[name="contact_id"]').val();
					var investor_id	 		= $('select[name="investor_id"]').val();
					var select_box 			= $('select[name="select_box"]').val();
					var search_text         	= $('#schedule_by_contact_filter input[type="search"]').val();
					data.contact_id 		= contact_id;
					data.investor_id 			= investor_id;
					data.select_box 			= select_box;
					data.search_text 			= search_text;
					data.calling_type 			= 'table';
					$('.pdf_dv_loan_schedule input[name="contact_id"]').val(contact_id);
					$('.pdf_dv_loan_schedule input[name="investor_id"]').val(investor_id);					
					$('.pdf_dv_loan_schedule input[name="select_box"]').val(select_box);
				},
				url: '/Pranav/Talimar/Reports/test_search_data',
				dataFilter: function(data){
					var json = JSON.parse( data );		
				    json.recordsTotal = json.totalNumRows;
				    json.recordsFiltered = json.totalNumRows;
				    totalRecord=json.totalNumRows;
				    json.data = json.data;				    
				    return JSON.stringify( json );
				}
		    },
		    columns: [				        
		        { data: 'lender_name' },
				{ data: 'property_address' },
				{ data: 'unit' },
				{ data: 'city' },
				{ data: 'state' },
				{ data: 'zip' },
				{ data: 'loan_condition' },
				{ data: 'loan_status' },
				{ data: 'servicer' },
				{ data: 'loan_amount' },
				{ data: 'interest_amount' },
				{ data: 'interest_percent' },
				{ data: 'lender_rate' },
				{ data: 'monthly_rate' },
				{ data: 'loan_term' },
				{ data: 'loan_servicer' }
		    ],
		    "aoColumnDefs": [
		        { "bSortable": false, "aTargets": [1,6,7] }, //, 6, 7
		        { "bSearchable": false, "aTargets": [0, 1, 2, 3, 4, 6, 7, 8, 9,10,11,12,13,14 ] }
		    ],
		    order: [[ 0, "ASC" ]],
		    "language": { loadingRecords: '&nbsp;&nbsp;', processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
		    drawCallback: function( settings ) {
		    	if ( req ) req.abort();
		    	total_rows_count=0;
				total_lender_count=0;
				total_loans_count=0;
				total_loan_amount=0;
				total_active_loans_count=0;
				total_active_loan_amount=0;
				total_commited_amount=0;
				total_avilable_amount=0;
		    	$('.lp_spin_load').html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:12px"></i>');
				   	total_datatable_for_contact(totalRecord);		    	
		    },
		    initComplete: function() {
		        $('#schedule_by_contact_filter input').unbind();
		        $('#schedule_by_contact_filter input').bind('blur change', function(e) {
		            $dis_list_all.search(this.value).draw();
		        });
		    }
		});
	}

}
var total_rows_count=0;
var total_lender_count=0;
var total_loans_count=0;
var total_loan_amount=0;
var total_active_loans_count=0;
var total_active_loan_amount=0;
var total_commited_amount=0;
var total_avilable_amount=0;
function total_datatable_for_contact(totalRecords){
	var contact_id 		= $('select[name="contact_id"]').val();
	var investor_id	 		= $('select[name="investor_id"]').val();
	var select_box 			= $('select[name="select_box"]').val();
	var search_text         	= $('#schedule_by_contact_filter input[type="search"]').val();
	$('.pdf_dv_loan_schedule input[name="contact_id"]').val(contact_id);
	$('.pdf_dv_loan_schedule input[name="investor_id"]').val(investor_id);					
	$('.pdf_dv_loan_schedule input[name="select_box"]').val(select_box);
	var data = {		
		'calling_type' 			: 'total_table',
		'contact_id' 			: contact_id,
		'investor_id' 			: investor_id,
		'select_box' 			: select_box,
		'search_text'           : search_text,
		'start' 				: '1',
		'length' 				: '10'
	};
	totalRecords=parseInt(totalRecords);
	var count=parseInt(totalRecords/1000);
	var remainRecord=parseInt(totalRecords%1000);
	var total_limit=1000;
	var total_offset=0;
	if(remainRecord>0){
		count=parseInt(count)+1;
	}
	var requestCount=1;
	data.total_limit=total_limit;
	data.total_offset=total_offset;
	req=$.ajax({
			type: "GET",
			dataType: 'json',
			url: "/Reports/test_search_data",
			data:data,
			success: function(respones){
				$('.total_records').html(respones.totalNumRows);
				$('.total_loan_amount').html(respones.total_loan_amount);
				$('.total_invest_amount').html(respones.total_invest_amount);
				$('.total_monthly_amount').html(respones.total_monthly_payment);
			}
	});
}
function ajaxCallback(data,count,remainRecord,total_offset){
	if(count>0){
			if(count==1){
				total_limit=remainRecord;
			}else{
				total_limit=1000;
			}
			data.total_limit=total_limit;
			data.total_offset=total_offset;
			req=$.ajax({
				type: "GET",
				dataType: 'json',
				url: "/ReportData/ajax_lender_filters",
				data:data,
				success: function(respones){
					$('.total_records').html(respones.total);
					$('.total_loan_amount').html(respones.total_loan_amount);
					$('.total_invest_amount').html(respones.total_invest_amount);
					$('.total_monthly_amount').html(respones.total_monthly_payment);
				}
			});
	}else{
		total_loan_amount=total_loan_amount.toLocaleString();
		total_active_loan_amount=total_active_loan_amount.toLocaleString();
		total_commited_amount=total_commited_amount.toLocaleString();
		total_avilable_amount=total_avilable_amount.toLocaleString();

		total_loan_amount=total_loan_amount+".00";
		total_active_loan_amount=total_active_loan_amount+".00";
		total_commited_amount=total_commited_amount+".00";
		total_avilable_amount=total_avilable_amount+".00";
		$('.total_contact_name').html(total_rows_count);
		$('.total_account_no').html(total_lender_count);
		$('.total_loan_count').html(total_loans_count);
		$('.total_loan_amount').html(total_loan_amount);
		$('.active_loan_count').html(total_active_loans_count);
		$('.active_loan_amount').html(total_active_loan_amount);
		$('.commited_capital_amount').html(total_commited_amount);
		$('.avilable_capital_amount').html(total_avilable_amount);
	}

}
function load_form()
{
	init_datatable_for_contact();
	
}
function load_full_table(type)
{
	var contact_id 			= $('select[name="contact_id"]').val();
	var investor_id	 		= $('select[name="investor_id"]').val();
	var select_box 			= $('select[name="select_box"]').val();
	$('.pdf_dv_loan_schedule input[name="contact_id"]').val(FundedTrustDeed);
	$('.pdf_dv_loan_schedule input[name="investor_id"]').val(ActiveTrustDeed);					
	$('.pdf_dv_loan_schedule input[name="select_box"]').val(WholeInvestor);
	$('.pdf_dv_loan_schedule input[name="reportDisplay"]').val(type);
	$('select_investor').submit();
}
// function disableActiveTrustDeed(value){
// 	if(value!=""){
// 		$(".ActiveTrustDeedDisabled").val("");
// 	}
// }
// function FundedTrustDeedDisabled(value){
// 	if(value!=""){
// 		$(".FundedTrustDeedDisabled").val("");
// 	}
// }
$(document).ready(function(){
	init_datatable_for_contact();
	$('.chosen').chosen();
});


// function select_investor_report(sel)
// {
	
// 	document.getElementById('select_investor').submit();
// }

// $(document).ready(function() {
	 
//     $('#table').DataTable({
// 		"aaSorting": [ 7, "asc" ]
// 	});
	
// });