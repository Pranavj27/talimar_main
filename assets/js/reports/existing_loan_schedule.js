function load_full_table()
{
	$('form#x').submit();
}


function total_datatable_for_contact(){

	var loan_type           = $('select[name="loan_type"]').val();
	var loan_status 		= $('select[name="loan_status"]').val();
	var loan_boarded	 	= $('select[name="loan_boarded"]').val();
	var board_complete 		= $('select[name="board_complete"]').val();
	var Listed_Properties 	= $('select[name="Listed_Properties"]').val();
	var county 				= $('input[name="county"]').val();
	var position 			= $('select[name="position"]').val();
	var late10 				= $('select[name="late10"]').val();
	var late 				= $('select[name="late"]').val();
	var construction_status = $('select[name="construction_status"]').val();
	var multipro 			= $('select[name="multipro"]').val();
	var multilender 		= $('select[name="multilender"]').val();
	var payoff_processed 	= $('select[name="payoff_processed"]').val();
	var welcome_call 		= $('select[name="welcome_call"]').val();
	var welcome_email 		= $('select[name="welcome_email"]').val();
	var sub_servicing_agent = $('select[name="sub_servicing_agent"]').val();
	var muturity_loan 		= $('select[name="muturity_loan"]').val();
	var payment_reserve 	= $('select[name="payment_reserve"]').val();
	var defaulted_loans 	= $('select[name="defaulted_loans"]').val();
	var term_sheet_sign 	= $('select[name="term_sheet_sign"]').val();
	var search_text         = $('#existing_loan_schedule_tab_filter input[type="search"]').val();
	var data = {
		'loan_type'         : loan_type,
		'loan_status' 		: loan_status,
		'loan_boarded' 		: loan_boarded,
		'board_complete' 	: board_complete,
		'Listed_Properties' : Listed_Properties,
		'county' 			: county,
		'position' 			: position,
		'late10' 			: late10,
		'late' 				: late,
		'construction_status' : construction_status,
		'multipro' 			: multipro,
		'multilender' 		: multilender,
		'payoff_processed' 	: payoff_processed,
		'welcome_call' 		: welcome_call,
		'welcome_email' 	: welcome_email,
		'sub_servicing_agent': sub_servicing_agent,
		'muturity_loan' 	: muturity_loan,
		'payment_reserve' 	: payment_reserve,
		'defaulted_loans' 	: defaulted_loans,
		'term_sheet_sign' 	: term_sheet_sign,
		'search_text' 		: search_text,
		'calling_type' 		: 'total_table',
		'start' 			: '1',
		'length' 			: '10'
	};

	$('.lp_spin_load').html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:12px"></i>');

	$.getJSON('/Pranav/Talimar/Reports/ajax_loan_schedule', data, function (json){		
		$('.total_loan').html(json.total_loan);
		$('.total_loan_amount').html(json.total_loan_amount);
		$('.total_loan_amount_payment').html(json.total_loan_amount_payment);
		$('.average_loan_amount').html(json.average_loan_amount);
		$('.average_loan_ltv').html(json.average_loan_ltv);
		$('.average_loan_rate').html(json.average_loan_rate);
		$('.average_loan_term').html(json.average_loan_term);
		$('.average_loan_payment').html(json.average_loan_payment);
	});
}

function init_datatable_for_contact(){
	var dispatching_list_all = '#existing_loan_schedule_tab';
	$(dispatching_list_all).DataTable().clear().destroy();    

	if( $( dispatching_list_all ).length ){		
		$dis_list_all = $( dispatching_list_all ).DataTable({
			processing: true,
			serverSide: true,
			pageLength: 10,
		    ajax: {
				dataSrc:"data",
				data:function(data){
					var loan_type           = $('select[name="loan_type"]').val();
					var loan_status 		= $('select[name="loan_status"]').val();
					var loan_boarded	 	= $('select[name="loan_boarded"]').val();
					var board_complete 		= $('select[name="board_complete"]').val();
					var Listed_Properties 	= $('select[name="Listed_Properties"]').val();
					var county 				= $('input[name="county"]').val();
					var position 			= $('select[name="position"]').val();
					var late10 				= $('select[name="late10"]').val();
					var late 				= $('select[name="late"]').val();
					var construction_status = $('select[name="construction_status"]').val();
					var multipro 			= $('select[name="multipro"]').val();
					var multilender 		= $('select[name="multilender"]').val();
					var payoff_processed 	= $('select[name="payoff_processed"]').val();
					var welcome_call 		= $('select[name="welcome_call"]').val();
					var welcome_email 		= $('select[name="welcome_email"]').val();
					var sub_servicing_agent = $('select[name="sub_servicing_agent"]').val();
					var muturity_loan 		= $('select[name="muturity_loan"]').val();
					var payment_reserve 	= $('select[name="payment_reserve"]').val();
					var defaulted_loans 	= $('select[name="defaulted_loans"]').val();
					var term_sheet_sign 	= $('select[name="term_sheet_sign"]').val();
					var search_text         = $('#existing_loan_schedule_tab_filter input[type="search"]').val();

					data.loan_type          = loan_type;
					data.loan_status 		= loan_status;
					data.loan_boarded 		= loan_boarded;
					data.board_complete 	= board_complete;
					data.Listed_Properties 	= Listed_Properties;
					data.county 			= county;
					data.position 			= position;
					data.late10 			= late10;
					data.late 				= late;
					data.construction_status = construction_status;
					data.multipro 			= multipro;
					data.multilender 		= multilender;
					data.payoff_processed 	= payoff_processed;
					data.welcome_call 		= welcome_call;
					data.welcome_email 		= welcome_email;
					data.sub_servicing_agent = sub_servicing_agent;
					data.muturity_loan 		= muturity_loan;
					data.payment_reserve 	= payment_reserve;
					data.defaulted_loans 	= defaulted_loans;
					data.term_sheet_sign 	= term_sheet_sign;
					data.search_text 		= search_text;
					data.calling_type 		= 'table';



					$('.pdf_dv_loan_schedule input[name="county"]').val(county);
					$('.pdf_dv_loan_schedule input[name="sub_servicing_agent"]').val(sub_servicing_agent);
					$('.pdf_dv_loan_schedule input[name="loan_type"]').val(loan_type);					
					$('.pdf_dv_loan_schedule input[name="borrower_id"]').val('');
					$('.pdf_dv_loan_schedule input[name="position"]').val(position);
					$('.pdf_dv_loan_schedule input[name="late"]').val(late);
					$('.pdf_dv_loan_schedule input[name="late10"]').val(late10);
					$('.pdf_dv_loan_schedule input[name="loan_status"]').val(loan_status);
					$('.pdf_dv_loan_schedule input[name="board_complete"]').val(board_complete);
					$('.pdf_dv_loan_schedule input[name="construction_status"]').val(construction_status);
					$('.pdf_dv_loan_schedule input[name="multipro"]').val(multipro);
					$('.pdf_dv_loan_schedule input[name="multilender"]').val(multilender);
					$('.pdf_dv_loan_schedule input[name="loan_boarded"]').val(loan_boarded);
					$('.pdf_dv_loan_schedule input[name="welcome_call"]').val(welcome_call);
					$('.pdf_dv_loan_schedule input[name="welcome_email"]').val(welcome_email);
					$('.pdf_dv_loan_schedule input[name="loan_preboarded"]').val('');
					$('.pdf_dv_loan_schedule input[name="payoff_processed"]').val(payoff_processed);
					$('.pdf_dv_loan_schedule input[name="closing_status"]').val('');
					$('.pdf_dv_loan_schedule input[name="muturity_loan"]').val(muturity_loan);
					$('.pdf_dv_loan_schedule input[name="payment_reserve"]').val(payment_reserve);
					$('.pdf_dv_loan_schedule input[name="defaulted_loans"]').val(defaulted_loans);
					$('.pdf_dv_loan_schedule input[name="term_sheet_sign"]').val(term_sheet_sign);
					$('.pdf_dv_loan_schedule input[name="Listed_Properties"]').val(Listed_Properties);
				},
				url: '/Pranav/Talimar/Reports/ajax_loan_schedule',
				dataFilter: function(data){
					/*console.log(data);*/
					var json = JSON.parse( data );	

				    json.recordsTotal = json.total;
				    json.recordsFiltered = json.total;
				    json.data = json.data;				    
				    return JSON.stringify( json );
				}
		    },
		    columns: [				        
		        { data: 'street_address' },
				{ data: 'unit' },
				{ data: 'city' },
				{ data: 'state' },
				{ data: 'zip' },
				{ data: 'borrower_name' },
				{ data: 'loan_amount' },
				{ data: 'position' },
				{ data: 'ltv' },
				{ data: 'rate' },
				{ data: 'term' },
				{ data: 'payment' },
				{ data: 'status' }
		    ],
		    "aoColumnDefs": [
		        { "bSortable": false, "aTargets": [8, 11] }, //, 6, 7
		        { "bSearchable": false, "aTargets": [0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12 ] }
		    ],
		    order: [[ 0, "ASC" ]],
		    "language": { loadingRecords: '&nbsp;&nbsp;', processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
		    drawCallback: function( settings ) {
		        total_datatable_for_contact();
		    }
		});
	}
}

function load_form()
{
	init_datatable_for_contact();
}
$(document).ready(function(){
	init_datatable_for_contact();
});

