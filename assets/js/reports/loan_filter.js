var search_value_check="";
var req ="";
function init_datatable_for_contact(){
	var totalRecord=0;	
	var dispatching_list_all = '#existing_loan_schedule_tab';
	$(dispatching_list_all).DataTable().clear().destroy();  
	if( $( dispatching_list_all ).length ){		
		$dis_list_all = $( dispatching_list_all ).DataTable({
			processing: true,
			serverSide: true,
			pageLength: 10,
			bFilter: true,
		    ajax: {
				dataSrc:"data",
				data:function(data){
					var FundedTrustDeed 		= $('select[name="FundedTrustDeed"]').val();
					var ActiveTrustDeed	 		= $('select[name="ActiveTrustDeed"]').val();
					var WholeInvestor 			= $('select[name="WholeInvestor"]').val();
					var lender_portal 			= $('select[name="lender_portal"]').val();
					var search_text         	= $('#existing_loan_schedule_tab_filter input[type="search"]').val();
					data.FundedTrustDeed 		= FundedTrustDeed;
					data.ActiveTrustDeed 		= ActiveTrustDeed;
					data.WholeInvestor 			= WholeInvestor;
					data.lender_portal 			= lender_portal;
					data.search_text 			= search_text;
					data.calling_type 			= 'table';
					$('.pdf_dv_loan_schedule input[name="FundedTrustDeed"]').val(FundedTrustDeed);
					$('.pdf_dv_loan_schedule input[name="ActiveTrustDeed"]').val(ActiveTrustDeed);					
					$('.pdf_dv_loan_schedule input[name="WholeInvestor"]').val(WholeInvestor);
					$('.pdf_dv_loan_schedule input[name="lender_portal"]').val(lender_portal);
				},
				url: '/Pranav/Talimar/ReportData/ajax_lender_filters',
				dataFilter: function(data){
					var json = JSON.parse( data );	
				    json.recordsTotal = json.total;
				    json.recordsFiltered = json.total;
				    totalRecord=json.recordsTotal;
				    json.data = json.data;				    
				    return JSON.stringify( json );
				}
		    },
		    columns: [				        
		        { data: 'name' },
				{ data: 'contact_accounts' },
				{ data: 'countTotal' },
				{ data: 'AmountTotal' },
				{ data: 'countActive' },
				{ data: 'AmountActive' },
				{ data: 'comment' },
				{ data: 'contact_funds' },
				{ data: 'contact_phone' },
				{ data: 'contact_email' }
		    ],
		    "aoColumnDefs": [
		        { "bSortable": false, "aTargets": [1,6,7] }, //, 6, 7
		        { "bSearchable": false, "aTargets": [0, 1, 2, 3, 4, 6, 7, 8, 9 ] }
		    ],
		    order: [[ 0, "ASC" ]],
		    "language": { loadingRecords: '&nbsp;&nbsp;', processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
		    drawCallback: function( settings ) {
		    	if ( req ) req.abort();
		    	total_rows_count=0;
				total_lender_count=0;
				total_loans_count=0;
				total_loan_amount=0;
				total_active_loans_count=0;
				total_active_loan_amount=0;
				total_commited_amount=0;
				total_avilable_amount=0;
		    	$('.lp_spin_load').html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:12px"></i>');
				   	total_datatable_for_contact(totalRecord);		    	
		    },
		    initComplete: function() {
		        $('#existing_loan_schedule_tab_filter input').unbind();
		        $('#existing_loan_schedule_tab_filter input').bind('blur change', function(e) {
		            $dis_list_all.search(this.value).draw();
		        });
		    }
		});
	}

}
var total_rows_count=0;
var total_lender_count=0;
var total_loans_count=0;
var total_loan_amount=0;
var total_active_loans_count=0;
var total_active_loan_amount=0;
var total_commited_amount=0;
var total_avilable_amount=0;
function total_datatable_for_contact(totalRecords){
	var FundedTrustDeed 		= $('select[name="FundedTrustDeed"]').val();
	var ActiveTrustDeed	 		= $('select[name="ActiveTrustDeed"]').val();
	var WholeInvestor 			= $('select[name="WholeInvestor"]').val();
	var lender_portal 			= $('select[name="lender_portal"]').val();
	var search_text         	= $('#existing_loan_schedule_tab_filter input[type="search"]').val();
	$('.pdf_dv_loan_schedule input[name="FundedTrustDeed"]').val(FundedTrustDeed);
	$('.pdf_dv_loan_schedule input[name="ActiveTrustDeed"]').val(ActiveTrustDeed);					
	$('.pdf_dv_loan_schedule input[name="WholeInvestor"]').val(WholeInvestor);
	$('.pdf_dv_loan_schedule input[name="lender_portal"]').val(lender_portal);
	var data = {		
		'calling_type' 			: 'total_table',
		'FundedTrustDeed' 		: FundedTrustDeed,
		'ActiveTrustDeed' 		: ActiveTrustDeed,
		'WholeInvestor' 		: WholeInvestor,
		'lender_portal' 		: lender_portal,
		'search_text'			: search_text,
		'start' 				: '1',
		'length' 				: '10'
	};
	totalRecords=parseInt(totalRecords);
	var count=parseInt(totalRecords/1000);
	var remainRecord=parseInt(totalRecords%1000);
	var total_limit=1000;
	var total_offset=0;
	if(remainRecord>0){
		count=parseInt(count)+1;
	}
	var requestCount=1;
	data.total_limit=total_limit;
	data.total_offset=total_offset;
	req=$.ajax({
			type: "GET",
			dataType: 'json',
			url: "/Pranav/Talimar/ReportData/ajax_lender_filters",
			data:data,
			success: function(respones){
				total_rows_count=parseInt(total_rows_count)+parseInt(respones.total_loan);
				total_lender_count=parseInt(total_lender_count)+parseInt(respones.total_account);
				total_loans_count=parseInt(total_loans_count)+parseInt(respones.hash_total_loan);
				total_loan_amount=parseFloat(total_loan_amount)+parseFloat(respones.amount_total_loan);
				total_active_loans_count=parseInt(total_active_loans_count)+parseInt(respones.hash_active_loan);
				total_active_loan_amount=parseFloat(total_active_loan_amount)+parseFloat(respones.amount_active_loan);
				total_commited_amount=parseFloat(total_commited_amount)+parseFloat(respones.commited_capital_amount);
				total_avilable_amount=parseFloat(total_avilable_amount)+parseFloat(respones.avilable_capital_amount);
				count=count-1;
				total_offset=parseInt(total_offset)+1000;
				ajaxCallback(data,count,remainRecord,total_offset);
			}
	});
}
function ajaxCallback(data,count,remainRecord,total_offset){
	if(count>0){
			if(count==1){
				total_limit=remainRecord;
			}else{
				total_limit=1000;
			}
			data.total_limit=total_limit;
			data.total_offset=total_offset;
			req=$.ajax({
				type: "GET",
				dataType: 'json',
				url: "/Pranav/Talimar/ReportData/ajax_lender_filters",
				data:data,
				success: function(respones){
					total_rows_count=parseInt(total_rows_count)+parseInt(respones.total_loan);
					total_lender_count=parseInt(total_lender_count)+parseInt(respones.total_account);
					total_loans_count=parseInt(total_loans_count)+parseInt(respones.hash_total_loan);
					total_loan_amount=parseFloat(total_loan_amount)+parseFloat(respones.amount_total_loan);
					total_active_loans_count=parseInt(total_active_loans_count)+parseInt(respones.hash_active_loan);
					total_active_loan_amount=parseFloat(total_active_loan_amount)+parseFloat(respones.amount_active_loan);
					total_commited_amount=parseFloat(total_commited_amount)+parseFloat(respones.commited_capital_amount);
					total_avilable_amount=parseFloat(total_avilable_amount)+parseFloat(respones.avilable_capital_amount);
					count=count-1;
					total_offset=parseInt(total_offset)+1000;
					ajaxCallback(data,count,remainRecord,total_offset);
				}
			});
	}else{
		total_loan_amount=total_loan_amount.toLocaleString();
		total_active_loan_amount=total_active_loan_amount.toLocaleString();
		total_commited_amount=total_commited_amount.toLocaleString();
		total_avilable_amount=total_avilable_amount.toLocaleString();

		total_loan_amount=total_loan_amount+".00";
		total_active_loan_amount=total_active_loan_amount+".00";
		total_commited_amount=total_commited_amount+".00";
		total_avilable_amount=total_avilable_amount+".00";
		$('.total_contact_name').html(total_rows_count);
		$('.total_account_no').html(total_lender_count);
		$('.total_loan_count').html(total_loans_count);
		$('.total_loan_amount').html(total_loan_amount);
		$('.active_loan_count').html(total_active_loans_count);
		$('.active_loan_amount').html(total_active_loan_amount);
		$('.commited_capital_amount').html(total_commited_amount);
		$('.avilable_capital_amount').html(total_avilable_amount);
	}

}
function load_form()
{
	init_datatable_for_contact();
	
}
function load_full_table(type)
{
	var FundedTrustDeed 		= $('select[name="FundedTrustDeed"]').val();
	var ActiveTrustDeed	 		= $('select[name="ActiveTrustDeed"]').val();
	var WholeInvestor 			= $('select[name="WholeInvestor"]').val();
	var lender_portal 			= $('select[name="lender_portal"]').val();
	$('.pdf_dv_loan_schedule input[name="FundedTrustDeed"]').val(FundedTrustDeed);
	$('.pdf_dv_loan_schedule input[name="ActiveTrustDeed"]').val(ActiveTrustDeed);					
	$('.pdf_dv_loan_schedule input[name="WholeInvestor"]').val(WholeInvestor);
	$('.pdf_dv_loan_schedule input[name="lender_portal"]').val(lender_portal);
	$('.pdf_dv_loan_schedule input[name="reportDisplay"]').val(type);
	$('form#x').submit();
}
function disableActiveTrustDeed(value){
	if(value!=""){
		$(".ActiveTrustDeedDisabled").val("");
	}
}
function FundedTrustDeedDisabled(value){
	if(value!=""){
		$(".FundedTrustDeedDisabled").val("");
	}
}
$(document).ready(function(){
	init_datatable_for_contact();
});