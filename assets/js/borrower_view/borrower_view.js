$(document).ready(function(){
	$('body').on('change', '#dp_reqt_status', function(){
		var property_id = $(this).attr('property_id');
		var property_status = $(this).val();
		var borrower_id = $(this).attr('borrower_id');
		$.ajax({
			type	:'POST',
			url		:'/Borrower_data/change_property_approval_status',
			data	:{'property_id':property_id,'property_status':property_status},
			success : function(response){
				swal("Property approval status updated successfully!", {icon: "success",});
				window.location.href = "/borrower_view/"+borrower_id+"#PropertyApproval";
			}

		});
	});
	
});