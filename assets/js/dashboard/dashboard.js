var loadicons = '<span class="spin_load_ajax"><i class="fa fa-circle-o-notch fa-spin"></i></span>';
function ajax_insurance()
{	
	$('div#ajinsurance').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_insurance',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#ajinsurance').html(response);
				/*window.location.reload();*/
			}
		}
	})
}

function ajax_SwagRequest()
{
	$('div#SwagRequest').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_SwagRequestData',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#SwagRequest').html(response);
				/*window.location.reload();*/
			}
		}
	})
}

function ajax_SignNotPosted()
{
	$('div#SignNotPosted').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_SignNotPostedData',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#SignNotPosted').html(response);
				/*window.location.reload();*/
			}
		}
	})
}

function ajax_GoogleReviewRequest()
{
	$('div#GoogleReviewRequest').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_GoogleReviewRequestData',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#GoogleReviewRequest').html(response);
				/*window.location.reload();*/
			}
		}
	})
}

function ajax_ClosingTombstone()
{
	$('div#ClosingTombstone').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_ClosingTombstoneData',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#ClosingTombstone').html(response);
				/*window.location.reload();*/
			}
		}
	})
}


function ajax_LoanClosingPosted()
{
	$('div#LoanClosingPosted').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_LoanClosingPostedData',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#LoanClosingPosted').html(response);
				/*window.location.reload();*/
			}
		}
	})
}

function ajax_SocialMediaBlast()
{
	$('div#SocialMediaBlast').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_SocialMediaBlastData',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#SocialMediaBlast').html(response);
				/*window.location.reload();*/
			}
		}
	})
}

function fci_servicing_schedule_ajax()
{
	$('div#fci_servicing_schedule').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/fci_servicing_schedule_data',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#fci_servicing_schedule').html(response);
				/*window.location.reload();*/
			}
		}
	})
}

function ajax_upcoming_paid()
{
	$('div#upcoming_paidd').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/upcoming_paid_off',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#upcoming_paidd').html(response);
				/*window.location.reload();*/
			}
		}
	})
}

function ajax_portfolio_divertification()
{
	$('div#portfolio_divertification').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_portfolio_divertification',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#portfolio_divertification').html(response);
				/*window.location.reload();*/
			}
		}
	})
}

function ajax_Extension_Processing()
{
	$('div#Extension_Processing').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_Extension_Processing',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#Extension_Processing').html(response);
				
			}
		}
	})
}

function ajax_IncomingFunds()
{
	$('div#IncomingFunds').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_IncomingFundsdata',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#IncomingFunds').html(response);
				
			}
		}
	})
}

function ajax_titleIncomingFunds()
{
	$('div#titleIncomingFunds').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_titleIncomingFundsdata',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#titleIncomingFunds').html(response);
				
			}
		}
	})
}

function ajax_MarketingNew()
{
	$('div#MarketingNew').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_MarketingNewdata',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#MarketingNew').html(response);
				
			}
		}
	})
}

function ajax_ClosingAdvertisement()
{
	$('div#ClosingAdvertisement').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_ClosingAdvertisementdata',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#ClosingAdvertisement').html(response);
				
			}
		}
	})
}

function ajax_beforeandafter()
{
	$('div#beforeandafter').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_beforeandafterdata',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#beforeandafter').html(response);
				
			}
		}
	})
}

function ajax_default_loans()
{
	$('div#default_loan').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_default_loans',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#default_loan').html(response);
				/*window.location.reload();*/
			}
		}
	})
}

function ajax_loan_closing_checklist()
{
	$('div#loan_closing_checklist').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_loanclosing_checklist',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#loan_closing_checklist').html(response);
				/*window.location.reload();*/
			}
		}
	})
}

function ajax_AccruedCharges()
{
	$('div#AccruedCharges').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/AccruedChargesQuery',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#AccruedCharges').html(response);
				/*window.location.reload();*/
			}
		}
	})
}

function ajax_servicing_companies(){
	$('div#servicing_companies').html(loadicons);
	$.ajax({
		type	: 'POST',
		url		: '/Home/ajax_servicing_companies',
		data	: {},
		success	: function(response){
			if(response)
			{
				$('div#servicing_companies').html(response);
				/*window.location.reload();*/
			}
		}
	});
}

function ajax_outstanding_draw(){
	$('div#outstanding_draw').html(loadicons);
	$.ajax({
		type 		: 'POST',
		url 		: '/Home/ajax_outstanding_draw',
		data 		: {},
		success 	: function(response){
			if(response)
			{
				$('div#outstanding_draw').html(response);
				/*window.location.reload();*/
			}
		}

	});
}

function ajax_construction_loan(){
	$('div#construction_loan').html(loadicons);
	$.ajax({
		type	:	'POST',
		url		:	'/Home/ajax_construction_loan',
		data	:	{},
		success	: 	function(response){
			if(response){
				$('div#construction_loan').html(response);
				/*window.location.reload();*/
			}
		}
	});
}

function ajax_OutstandingFunds(){
	$('div#OutstandingFunds').html(loadicons);
	$.ajax({
		type	:	'POST',
		url		:	'/Home/OutstandingFundsData',
		data	:	{},
		success	: 	function(response){
			if(response){
				$('div#OutstandingFunds').html(response);
				/*window.location.reload();*/
			}
		}
	});
}

function ajax_assignment_process(){
	return false;	
	$('div#assignment_process').html(loadicons);
	$.ajax({
		type	:	'POST',
		url		:	'/Home/ajax_assignment_process',
		data	:	{},
		success	: 	function(response){
			if(response){
				$('div#assignment_process').html(response);
				/*window.location.reload();*/
			}
		}
	});
}

/*ajax_top_lender();
function ajax_top_lender(){
	$.ajax({
		type	:	'POST',
		url		:	'Home/ajax_top_lender',
		data	:	{},
		success	: 	function(response){
			if(response){
				$('div#top_lender').append(response);
			}
		}
	});
}

ajax_top_borrower();
function ajax_top_borrower(){
	$.ajax({
		type	:	'POST',
		url		:	'Home/ajax_top_borrower',
		data	:	{},
		success	: 	function(response){
			if(response){
				$('div#top_borrower').append(response);
			}
		}
	});
}
*/

function ajax_maturity_schedule(){
	$('div#maturity_schedule').html(loadicons);
	$.ajax({
		type	: 'POST',
		url		: '/Home/ajax_maturity_schedule',
		data	: {},
		success : function(response){
			if(response){
				$('div#maturity_schedule').html(response);
				/*window.location.reload();*/
			}
		}
	});
}

function ajax_portfolio_statistics(){
	$('div#portfolio_statistics').html(loadicons);
	$.ajax({
		type	: 'POST',
		url		: '/Home/ajax_portfolio_statistics',
		data	: {},
		success : function(response){
			if(response){
				$('div#portfolio_statistics').html(response);
				/*window.location.reload();*/
			}
		}
	});
}

function ajax_year_to_date_statistics(){
	$('div#year_portfolio_statistics').html(loadicons);
	$.ajax({
		type	: 'POST',
		url		: '/Home/ajax_year_to_date_statistics',
		data	: {},
		success : function(response){
			if(response){
				$('div#year_portfolio_statistics').html(response);
				/*window.location.reload();*/
			}
		}
	});
}
/*ajax_trust_deed();
 function ajax_trust_deed(){
	$("#LoadingImage").show();
	$.ajax({
			type	: 'POST',
			url		: '/Home/ajax_trust_deed',
			data	: {},
			success : function(response){

				if(response){					
					$('div#trust_deed').append(response);					
				}
			}
	});
}
*/

function ajax_action_item(){
	/*$("#LoadingImage").show();*/
	$('div#action_item').html(loadicons);
	$.ajax({
			type	: 'POST',
			url		: '/Home/ajax_action_item',
			data	: {},
			success : function(response){

				if(response){
					/*$("#LoadingImage").hide();*/
					$('div#action_item').html(response);
					/*window.location.reload();*/
				}
			}
	});
}

function ajax_active_trust_deed(){
	/*$("#LoadingImage").show();*/
	$('div#active_trust_deed').html(loadicons);
	$.ajax({
			type	: 'POST',
			url		: '/Home/active_trust_deedData',
			data	: {},
			success : function(response){

				if(response){
					/*$("#LoadingImage").hide();*/
					$('div#active_trust_deed').html(response);
					/*window.location.reload();*/
				}
			}
	});
}

function capital_requirements(){
	/*$("#LoadingImage").show();*/
	$('div#capital_requirement').html(loadicons);
	$.ajax({
		type	: 'POST',
		url		: '/Home/capital_requirement',
		data	: {},
		success : function(response){

			if(response){
				/*$("#LoadingImage").hide();*/
				$('div#capital_requirement').html(response);
				/*window.location.reload();*/
			}
		}
	});
}

function balance_sheet_loan(){
	/*$("#LoadingImage").show();*/
	$('div#balance_sheet_loan').html(loadicons);
	$.ajax({
			type	: 'POST',
			url		: '/Home/balance_sheet_loan',
			data	: {},
			success : function(response){

				if(response){
					/*$("#LoadingImage").hide();*/
					$('div#balance_sheet_loan').html(response);
					/*window.location.reload();*/
				}
			}
	});
}

function OutstandingClosingFees(){
	/*$("#LoadingImage").show();*/
	$('div#OutstandingClosingFees').html(loadicons);
	$.ajax({
		type	: 'POST',
		url		: '/Home/OutstandingClosingFeesData',
		data	: {},
		success : function(response){

			if(response){
				/*$("#LoadingImage").hide();*/
				$('div#OutstandingClosingFees').html(response);
				/*window.location.reload();*/
			}
		}
	});
}
 
function avaliable_capital(){
	/*$("#LoadingImage").show();*/
	$('div#avaliable_capital').html(loadicons);
	$.ajax({
		type	: 'POST',
		url		: '/Home/available_capital',
		data	: {},
		success : function(response){

			if(response){
				/*$("#LoadingImage").hide();*/
				$('div#avaliable_capital').html(response);
				/*window.location.reload();*/
			}
		}
	});
}
 
function loan_servicing(){
	/*$("#LoadingImage").show();*/
	$('div#loan_servicing').html(loadicons);
	$.ajax({
		type	: 'POST',
		url		: '/Home/ajax_loan_servicing',
		data	: {},
		success : function(response){

			if(response){
				/*$("#LoadingImage").hide();*/
				$('div#loan_servicing').html(response);
				/*window.location.reload();*/
			}
		}
	});
}

 
function posted_trust_deed(){
	$('div#posted_trust').html(loadicons);
	/*$("#LoadingImage").show();*/
	$.ajax({
		type	: 'POST',
		url		: '/Home/posted_trust_deed_function',
		data	: {},
		success : function(response){

			if(response){
				/*$("#LoadingImage").hide();*/
				$('div#posted_trust').html(response);
				/*window.location.reload();*/
			}
		}
	});
}
 
function paid_off_process(){
	/*$("#LoadingImage").show();*/
	$('div#paid_offf_process').html(loadicons);
	$.ajax({
		type	: 'POST',
		url		: '/Home/paid_off_process',
		data	: {},
		success : function(response){

			if(response){
				/*$("#LoadingImage").hide();*/
				$('div#paid_offf_process').html(response);
				/*window.location.reload();*/
			}
		}
	});
}

function ajax_assigment_signture(){
	/*$("#LoadingImage").show();*/
	$('div#ready_signture').html(loadicons);
	$.ajax({
			type	: 'POST',
			url		: '/Home/ajax_assigment_signtur',
			data	: {},
			success : function(response){

				if(response){
					/*$("#LoadingImage").hide();*/
					$('div#ready_signture').html(response);
					/*window.location.reload();*/
				}
			}
	});
}

function ajax_AssignmentProcess(){
	/*$("#LoadingImage").show();*/
	$('div#AssignmentProcess').html(loadicons);
	$.ajax({
			type	: 'POST',
			url		: '/Home/AssignmentProcessData',
			data	: {},
			success : function(response){

				if(response){
					/*$("#LoadingImage").hide();*/
					$('div#AssignmentProcess').html(response);
					/*window.location.reload();*/
					$('div#AssignmentProcess table#table').DataTable({
				    	/*"aaSorting": false,*/
				    	"lengthChange": false,
				    	"searching": false,
				    	"paging":   false,
        				"info":     false
				    });

				}
			}
	});
}

/*ajax_term();
function ajax_term(){	
	$.ajax({
		type	: 'POST',
		url		: '/Home/ajax_term_sheet',
		data	: {},
		success : function(response){

			if(response){					
				$('div#ajax_term_sheet').append(response);					
			}
		}
	});
}
*/

function ajax_loan_service(){
	/*$("#LoadingImage").show();*/
	$('div#ajax_loan_service').html(loadicons);
	$.ajax({
		type	: 'POST',
		url		: '/Home/ajax_loan_service_fun',
		data	: {},
		success : function(response){

			if(response){
				/*$("#LoadingImage").hide();*/
				$('div#ajax_loan_service').html(response);
				/*window.location.reload();*/
			}
		}
	});
}

function ajax_loan_date(){
	/*$("#LoadingImage").show();*/
	$('div#ajax_loan_date_late').html(loadicons);
	$.ajax({
		type	: 'POST',
		url		: '/Home/ajax_loan_late_date',
		data	: {},
		success : function(response){

			if(response){
				/*$("#LoadingImage").hide();*/
				$('div#ajax_loan_date_late').html(response);
				/*window.location.reload();*/
			}
		}
	});
}

function ajax_pipeline()
{
	$('div#ajax_pipeline').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_pipeline_data',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#ajax_pipeline').html(response);
				/*window.location.reload();*/
			}
		}
	});
}

function ajax_active_loan_portfolio(){
	$('div#active_loan_portfolio').html(loadicons);
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_active_loan_portfolio',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#active_loan_portfolio').html(response);
				/*window.location.reload();*/
			}
		}
	});
}

function ajax_loan_holding_schedule(){
	$('div#loan_holding_schedule').html(loadicons);
	
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_loan_holding_schedule',
		data : {},
		success  : function(response)
		{
			if(response)
			{
				$('div#loan_holding_schedule').html(response);
				/*window.location.reload();*/
			}
		}
	});
}
ajax_pipeline();
ajax_loan_holding_schedule();
ajax_active_loan_portfolio();
ajax_insurance();
ajax_SwagRequest();
ajax_SignNotPosted();
ajax_GoogleReviewRequest();
ajax_ClosingTombstone();
ajax_LoanClosingPosted();
ajax_SocialMediaBlast();
fci_servicing_schedule_ajax();
ajax_upcoming_paid();
ajax_portfolio_divertification();
ajax_Extension_Processing();
ajax_IncomingFunds();
ajax_titleIncomingFunds();
ajax_MarketingNew();
ajax_ClosingAdvertisement();
ajax_beforeandafter();
ajax_default_loans();
ajax_loan_closing_checklist();
ajax_AccruedCharges();
ajax_servicing_companies();
ajax_outstanding_draw();
ajax_construction_loan();
ajax_OutstandingFunds();
ajax_assignment_process();
ajax_maturity_schedule();
ajax_portfolio_statistics();
ajax_year_to_date_statistics();
ajax_action_item();
ajax_active_trust_deed();
capital_requirements();
loan_servicing();
posted_trust_deed();
paid_off_process();
ajax_assigment_signture();
ajax_AssignmentProcess();
ajax_loan_service();
ajax_loan_date();
balance_sheet_loan();
OutstandingClosingFees();
avaliable_capital();
