ajax_portfolio_divertification();
function ajax_portfolio_divertification()
{
	dashboard_widget_loder_show('portfolio_divertification');
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_portfolio_divertification',
		data : {},
		success  : function(response)
		{
			dashboard_widget_loder_hide('portfolio_divertification');
			if(response)
			{
				$('div#portfolio_divertification').append(response);
				//window.location.reload();
			}
		}
	})
}

ajax_construction_loan();
function ajax_construction_loan(){
	dashboard_widget_loder_show('construction_loan');
	$.ajax({
				type	:	'POST',
				url		:	'/Home/ajax_construction_loan',
				data	:	{},
				success	: 	function(response){
					dashboard_widget_loder_hide('construction_loan');
					if(response){
						$('div#construction_loan').append(response);
						//window.location.reload();
					}
				}
	});
}

ajax_year_to_date_statistics();
function ajax_year_to_date_statistics(){
	dashboard_widget_loder_show('year_portfolio_statistics');
	$.ajax({
			type	: 'POST',
			url		: '/Home/ajax_year_to_date_statistics',
			data	: {},
			success : function(response){
				dashboard_widget_loder_hide('year_portfolio_statistics');
				if(response){
					$('div#year_portfolio_statistics').append(response);
					//window.location.reload();
				}
			}
	});
}