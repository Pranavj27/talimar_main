ajax_loan_servicing_overview();
function ajax_loan_servicing_overview()
{
	dashboard_widget_loder_show('loan_servicing_overview');
	$.ajax({
		type : 'POST',
		url : '/Home/loanServicingOverview',
		data : {},
		success  : function(response)
		{
			dashboard_widget_loder_hide('loan_servicing_overview');
			if(response)
			{
				$('div#loan_servicing_overview').append(response);
				//window.location.reload();
			}
		}
	})
}
ajax_AccruedCharges();
function ajax_AccruedCharges()
{
	dashboard_widget_loder_show('AccruedCharges');
	$.ajax({
		type : 'POST',
		url : '/Home/AccruedChargesQuery',
		data : {},
		success  : function(response)
		{
			dashboard_widget_loder_hide('AccruedCharges');
			if(response)
			{
				$('div#AccruedCharges').append(response);
				//window.location.reload();
			}
		}
	})
}

ajax_loan_closing_checklist();
function ajax_loan_closing_checklist()
{
	dashboard_widget_loder_show('loan_closing_checklist');
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_loanclosing_checklist',
		data : {},
		success  : function(response)
		{
			dashboard_widget_loder_hide('loan_closing_checklist');
			if(response)
			{
				$('div#loan_closing_checklist').append(response);
				//window.location.reload();
			}
		}
	})
}

paid_off_process();
function paid_off_process(){
	dashboard_widget_loder_show('paid_offf_process');
	$.ajax({
			type	: 'POST',
			url		: '/Home/paid_off_process',
			data	: {},
			success : function(response){
				dashboard_widget_loder_hide('paid_offf_process');
				if(response){
					//$("#LoadingImage").hide();
					$('div#paid_offf_process').append(response);
					//window.location.reload();
				}
			}
	});
}

ajax_upcoming_paid();
function ajax_upcoming_paid()
{
	dashboard_widget_loder_show('upcoming_paidd');
	$.ajax({
		type : 'POST',
		url : '/Home/upcoming_paid_off',
		data : {},
		success  : function(response)
		{
			dashboard_widget_loder_hide('upcoming_paidd');
			if(response)
			{
				$('div#upcoming_paidd').append(response);
				//window.location.reload();
			}
		}
	})
}

fci_servicing_schedule_ajax();
function fci_servicing_schedule_ajax()
{
	dashboard_widget_loder_show('fci_servicing_schedule');
	$.ajax({
		type : 'POST',
		url : '/Home/fci_servicing_schedule_data',
		data : {},
		success  : function(response)
		{
			dashboard_widget_loder_hide('fci_servicing_schedule');
			if(response)
			{
				$('div#fci_servicing_schedule').append(response);
				//window.location.reload();
			}
		}
	})
}

loan_servicing();
function loan_servicing(){
	dashboard_widget_loder_show('loan_servicing');
	$.ajax({
			type	: 'POST',
			url		: '/Home/ajax_loan_servicing',
			data	: {},
			success : function(response){
				dashboard_widget_loder_hide('loan_servicing');
				if(response){
					//$("#LoadingImage").hide();
					$('div#loan_servicing').append(response);
					//window.location.reload();
				}
			}
	});
}

ajax_outstanding_draw();
function ajax_outstanding_draw(){
	dashboard_widget_loder_show('outstanding_draw');
	$.ajax({
		type 		: 'POST',
		url 		: '/Home/ajax_outstanding_draw',
		data 		: {},
		success 	: function(response){
			dashboard_widget_loder_hide('outstanding_draw');
			if(response)
			{
				$('div#outstanding_draw').append(response);
				//window.location.reload();
			}
		}

	});
}

ajax_insurance();
function ajax_insurance()
{	
	dashboard_widget_loder_show('ajinsurance');
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_insurance',
		data : {},
		success  : function(response)
		{
			dashboard_widget_loder_hide('ajinsurance');
			if(response)
			{
				$('div#ajinsurance').append(response);
				//window.location.reload();
			}
		}
	})
}

ajax_loan_service();
function ajax_loan_service(){
	dashboard_widget_loder_show('ajax_loan_service');
	$.ajax({
			type	: 'POST',
			url		: '/Home/ajax_loan_service_fun',
			data	: {},
			success : function(response){
				dashboard_widget_loder_hide('ajax_loan_service');
				if(response){
					//$("#LoadingImage").hide();
					$('div#ajax_loan_service').append(response);
					//window.location.reload();
				}
			}
	});
}

ajax_maturity_schedule();
function ajax_maturity_schedule(){
	dashboard_widget_loder_show('maturity_schedule');
	$.ajax({
			type	: 'POST',
			url		: '/Home/ajax_maturity_schedule',
			data	: {},
			success : function(response){
				dashboard_widget_loder_hide('maturity_schedule');
				if(response){
					$('div#maturity_schedule').append(response);
					//window.location.reload();
				}
			}
	});

}

ajax_Extension_Processing();
function ajax_Extension_Processing()
{
	dashboard_widget_loder_show('Extension_Processing');
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_Extension_Processing',
		data : {},
		success  : function(response)
		{
			dashboard_widget_loder_hide('Extension_Processing');
			if(response)
			{
				$('div#Extension_Processing').append(response);
				
			}
		}
	})
}

ajax_default_loans();
function ajax_default_loans()
{
	dashboard_widget_loder_show('default_loan');
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_default_loans',
		data : {},
		success  : function(response)
		{
			dashboard_widget_loder_hide('default_loan');
			if(response)
			{
				$('div#default_loan').append(response);
				//window.location.reload();
			}
		}
	});
}

ajax_loan_date();
function ajax_loan_date(){
	dashboard_widget_loder_show('ajax_loan_date_late');
	$.ajax({
			type	: 'POST',
			url		: '/Home/ajax_loan_late_date',
			data	: {},
			success : function(response){
				dashboard_widget_loder_hide('ajax_loan_date_late');
				if(response){
					//$("#LoadingImage").hide();
					$('div#ajax_loan_date_late').append(response);
					//window.location.reload();
				}
			}
	});
}

ajax_late_days();
function ajax_late_days()
{
	dashboard_widget_loder_show('lateDayFCI');
	$.ajax({
		type : 'POST',
		url : '/Home/lateDaysInloans',
		data : {},
		success  : function(response)
		{
			dashboard_widget_loder_hide('lateDayFCI');
			if(response)
			{
				$('div#lateDayFCI').append(response);
				//window.location.reload();
			}
		}
	})
}
ajax_outstanding_servicer_deposits();
function ajax_outstanding_servicer_deposits()
{
	dashboard_widget_loder_show('OutstandingServicerDeposits');
	$.ajax({
		type : 'POST',
		url : '/Home/outstandingServicerDeposits',
		data : {},
		success  : function(response)
		{
			dashboard_widget_loder_hide('OutstandingServicerDeposits');
			if(response)
			{
				$('div#OutstandingServicerDeposits').addClass("scrolling-dashboard");
				$('div#OutstandingServicerDeposits').append(response);
				//window.location.reload();
			}
		}
	})
}

