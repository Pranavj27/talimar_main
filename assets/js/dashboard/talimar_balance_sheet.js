capital_requirements();
function capital_requirements(){
	dashboard_widget_loder_show('capital_requirement');
	$.ajax({
			type	: 'POST',
			url		: '/Home/capital_requirement',
			data	: {},
			success : function(response){
				dashboard_widget_loder_hide('capital_requirement');
				if(response){
					//$("#LoadingImage").hide();
					$('div#capital_requirement').append(response);
					//window.location.reload();
				}
			}
	});
}

balance_sheet_loan();
function balance_sheet_loan(){
	dashboard_widget_loder_show('balance_sheet_loan');
	$.ajax({
			type	: 'POST',
			url		: '/Home/balance_sheet_loan',
			data	: {},
			success : function(response){
				dashboard_widget_loder_hide('balance_sheet_loan');
				if(response){
					//$("#LoadingImage").hide();
					$('div#balance_sheet_loan').append(response);
					//window.location.reload();
				}
			}
	});
}

OutstandingClosingFees();
function OutstandingClosingFees(){
	dashboard_widget_loder_show('OutstandingClosingFees');
	$.ajax({
			type	: 'POST',
			url		: '/Home/OutstandingClosingFeesData',
			data	: {},
			success : function(response){
				dashboard_widget_loder_hide('OutstandingClosingFees');
				if(response){
					//$("#LoadingImage").hide();
					$('div#OutstandingClosingFees').append(response);
					//window.location.reload();
				}
			}
	});
}

ajax_OutstandingFunds();
function ajax_OutstandingFunds(){
	dashboard_widget_loder_show('OutstandingFunds');
	$.ajax({
				type	:	'POST',
				url		:	'/Home/OutstandingFundsData',
				data	:	{},
				success	: 	function(response){
					dashboard_widget_loder_hide('OutstandingFunds');
					if(response){
						$('div#OutstandingFunds').append(response);
						//window.location.reload();
					}
				}
	});
}

 avaliable_capital();
function avaliable_capital(){
	dashboard_widget_loder_show('avaliable_capital');
	$.ajax({
			type	: 'POST',
			url		: '/Home/available_capital',
			data	: {},
			success : function(response){
				dashboard_widget_loder_hide('avaliable_capital');
				if(response){
					//$("#LoadingImage").hide();
					$('div#avaliable_capital').append(response);
					//window.location.reload();
				}
			}
	});
}