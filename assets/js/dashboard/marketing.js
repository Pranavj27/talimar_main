ajax_MarketingNew();
function ajax_MarketingNew()
{
	dashboard_widget_loder_show('MarketingNew');
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_MarketingNewdata',
		data : {},
		success  : function(response)
		{
			dashboard_widget_loder_hide('MarketingNew');
			if(response)
			{
				$('div#MarketingNew').append(response);
				
			}
		}
	})
}
	
ajax_GoogleReviewRequest();
function ajax_GoogleReviewRequest()
{
	dashboard_widget_loder_show('GoogleReviewRequest');
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_GoogleReviewRequestData',
		data : {},
		success  : function(response)
		{
			dashboard_widget_loder_hide('GoogleReviewRequest');
			if(response)
			{
				$('div#GoogleReviewRequest').append(response);
				//window.location.reload();
			}
		}
	})
}

ajax_ClosingTombstone();
function ajax_ClosingTombstone()
{
	dashboard_widget_loder_show('ClosingTombstone');
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_ClosingTombstoneData',
		data : {},
		success  : function(response)
		{
			dashboard_widget_loder_hide('ClosingTombstone');
			if(response)
			{
				$('div#ClosingTombstone').append(response);
				//window.location.reload();
			}
		}
	})
}

ajax_ClosingAdvertisement();
function ajax_ClosingAdvertisement()
{
	dashboard_widget_loder_show('ClosingAdvertisement');
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_ClosingAdvertisementdata',
		data : {},
		success  : function(response)
		{
			dashboard_widget_loder_hide('ClosingAdvertisement');
			if(response)
			{
				$('div#ClosingAdvertisement').append(response);
				
			}
		}
	})
}


ajax_SignNotPosted();
function ajax_SignNotPosted()
{
	dashboard_widget_loder_show('SignNotPosted');
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_SignNotPostedData',
		data : {},
		success  : function(response)
		{
			dashboard_widget_loder_hide('SignNotPosted');
			if(response)
			{
				$('div#SignNotPosted').append(response);
				//window.location.reload();
			}
		}
	})
}

ajax_LoanClosingPosted();
function ajax_LoanClosingPosted()
{
	dashboard_widget_loder_show('LoanClosingPosted');
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_LoanClosingPostedData',
		data : {},
		success  : function(response)
		{
			dashboard_widget_loder_hide('LoanClosingPosted');
			if(response)
			{
				$('div#LoanClosingPosted').append(response);
				//window.location.reload();
			}
		}
	})
}


ajax_SocialMediaBlast();
function ajax_SocialMediaBlast()
{
	dashboard_widget_loder_show('SocialMediaBlast');
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_SocialMediaBlastData',
		data : {},
		success  : function(response)
		{
			dashboard_widget_loder_hide('SocialMediaBlast');
			if(response)
			{
				$('div#SocialMediaBlast').append(response);
				//window.location.reload();
			}
		}
	})
}

ajax_beforeandafter();
function ajax_beforeandafter()
{
	dashboard_widget_loder_show('beforeandafter');
	$.ajax({
		type : 'POST',
		url : '/Home/ajax_beforeandafterdata',
		data : {},
		success  : function(response)
		{
			dashboard_widget_loder_hide('beforeandafter');
			if(response)
			{
				$('div#beforeandafter').append(response);
				
			}
		}
	})
}