function b64_to_utf8( str ) {
  return decodeURIComponent(escape(window.atob( str )));
}

$(document).ready(function(){
$('body').on('load', function(){
	getLenderData();
});

$('body').on('click', '.open_email_content', function(){

	
		var to_email    = $(this).attr('to_email');
		var to_cc 		= $(this).attr('to_cc');
		var to_bcc 		= $(this).attr('to_bcc');

		var from_email 	= $(this).attr('from_email');
		var from_name 	= $(this).attr('from_name');

		var subject 	= $(this).attr('subject');
		var message 	= $(this).attr('message');

		var attach_file = $(this).attr('attach_file');
		var b_message = b64_to_utf8(message);

		var html_content = '';

		if(to_email){
			html_content += '<div class=""><strong>To Email : </strong> '+to_email+'</div>'
		}
		if(to_cc){
			html_content += '<div class=""><strong>Cc : </strong> '+to_cc+'</div>'
		}
		if(to_bcc){
			html_content += '<div class=""><strong>Bcc : </strong> '+to_bcc+'</div>'
		}
		if(from_email){
			html_content += '<div class=""><strong>From Email : </strong> '+from_email+'</div>'
		}
		if(from_name){
			html_content += '<div class=""><strong>From Name : </strong> '+from_name+'</div>'
		}
		if(subject){
			html_content += '<div class=""><strong>Subject : </strong> '+subject+'</div>'
		}
		if(attach_file){
			html_content += '<div class=""><strong>Attach File : </strong> '+attach_file+'</div>'
		}
		if(message){
			html_content += '<div class=""><strong>Message : </strong> '+b_message+'</div>'
		}

		$('.mail_popup_body').html(html_content);
		$('#mailviewModal').modal('show');
	});

	var talimat_loan_select = $('#talimat_loan_select').val();


	$('#ltrigger_auto_mails').DataTable( {
	    responsive: true,
	    "order": [[ 5, 'desc' ]]
	});
	$('#btrigger_auto_mails').DataTable( {
	    responsive: true,
	    "order": [[ 6, 'desc' ]]
	});

	$('#borrower_auto_mails').DataTable( {
	    responsive: true,
	    "order": [[ 0, 'asc' ]],
	    "oSearch": {"sSearch": talimat_loan_select}
	});
	$('#loan_payment_reminder').DataTable( {
	    responsive: true,
	    "oSearch": {"sSearch": talimat_loan_select}
	});



});

function delete_email_content(that){
		swal({
		  title: "Are you sure?",
		  text: "Delete all information for this mail!",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
		  	if (willDelete) {
			  	var mail_id = $(that).attr('reminder_id');
			  	console.log('mail_id'+mail_id);
				$.ajax({
			        type : 'POST',
			        url  : "/auto_mail/delete_mail_contact",
			        data : {'mail_id':mail_id,'ajax_call':'yes'},
			        success : function(response){
			        	if(response == 'success'){
			        		swal("Mail information deleted successfully!", {icon: "success",});
			        		location.reload();
			        	}else{
			        		swal("Some error occurred please try again!", {icon: "error",});
			        	}	        	
			        }
			    });
		    
		  	}
		});
}

function getLenderData()
{

	var dispatching_list_all = '#borrower_auto_mails';
	$(dispatching_list_all).DataTable();

	$('#borrower_auto_mails').DataTable().clear().destroy();

	if ( $.fn.DataTable.isDataTable('#borrower_auto_mails') ) {
  			$('#borrower_auto_mails').DataTable().destroy();
	}
		
	if( $( dispatching_list_all ).length ){	

		
		var conName = $('#conName').val();
		var createdAt = $('#createdAt').val();
		
		$dis_list_all = $( dispatching_list_all ).DataTable({
		processing: true,
		searching : false,
		serverSide: true,
		pageLength: 10,
		ajax: {

				type : 'POST',
		    	dataSrc:"data",
				data:{'conName' : conName, 'createdAt' : createdAt, 'orderby' : 'Desc'},
		    	url: '<?php echo base_url()."CronJobFCI/ajaxGetCron";?>',
			    dataFilter: function(data){
			    	
					var json = JSON.parse( data );
					total = json.total;
					json.recordsTotal = json.total;
					json.recordsFiltered = json.total;
					json.data = json.data;
					return JSON.stringify( json );
				},
			},
			columns: [				        
		        { data: 'name' },
		        { data: 'dateData' },
		        { data: 'timeData' },
		        { data: 'status'}
		    ],
			"aoColumnDefs": [
		        { "bSortable": false, "aTargets": [0,2,3 ] }, 
		        { "bSearchable": false, "aTargets": [0,1,2,3] }
		    ],
			"language": { loadingRecords: '&nbsp;&nbsp;', processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
			        
			drawCallback: function( settings ) {
			        
			}
		});
	}
	

}