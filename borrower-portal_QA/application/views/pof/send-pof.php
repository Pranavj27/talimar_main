<style type="text/css">
    .addspace{
        margin-bottom: 10px;
    }

    label{
        font-weight: 600;
    }
</style>
<div class="page-container">
                <!-- BEGIN SIDEBAR -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content" style="padding-left: 30px;padding-right: 30px;">
                <!-- BEGIN PAGE HEADER-->
                <div class="row">
                	<div class="col-md-10">
		                <div class="page-bar" style="border-bottom: 0px !important;padding-top: 10px;padding-bottom: 10px;">
		                    <h4><b>POF Request</b></h4>
		                </div>
		            </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Request Payoff</button>
                    </div>
		        </div>

                <div class="row">
                    <div class="col-md-12">
                        <?php if($this->session->flashdata('error') !=''){ ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <?php echo $this->session->flashdata('error');?> 
                            </div>
                        <?php } ?>
                        <?php if($this->session->flashdata('success') !=''){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <?php echo $this->session->flashdata('success');?> 
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <!-- <div class="row">
                    <div class="col-md-12">
                        <h4 class="deed_heading">Contact Information:</h4>
                    </div>
                </div> -->
                <?php // print_r($displayallaccount); ?>
            <form method="post" action="<?php echo base_url();?>pof-request">
                <div class="row addspace">
                    <div class="col-md-4">
                        <label>Borrower Accounts:</label>
                        <select class="form-control" name="b_account">
                            <option value="">Select One</option>
                            <?php foreach($displayallaccount as $row){ ?>
                                <option value="<?php echo $row->b_id;?>"><?php echo $row->b_name;?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="row addspace">
                    <div class="col-md-4">
                        <label>Street Address:</label>
                        <input type="text" name="address" class="form-control">
                    </div>

                    <div class="col-md-4">
                        <label>Unit:</label>
                        <input type="text" name="unit" class="form-control">
                    </div>
                </div>

                <div class="row addspace">
                    <div class="col-md-4">
                        <label>City:</label>
                        <input type="text" name="city" class="form-control">
                    </div>

                    <div class="col-md-4">
                        <label>State:</label>
                        <input type="text" name="state" class="form-control">
                    </div>

                    <div class="col-md-4">
                        <label>Zip:</label>
                        <input type="text" name="zip" class="form-control number_only">
                    </div>
                </div>

                <div class="row addspace">
                    <div class="col-md-4">
                        <label>Purchase Price:</label>
                        <input type="text" name="pur_price" class="form-control amount_format number_only">
                    </div>

                    <div class="col-md-4">
                        <label>Rehab Cost:</label>
                        <input type="text" name="reb_cost" class="form-control amount_format number_only">
                    </div>

                    <div class="col-md-4">
                        <label>Completion Value:</label>
                        <input type="text" name="com_value" class="form-control amount_format number_only">
                    </div>
                </div>

                <div class="row addspace">
                    <div class="col-md-12">
                        <button type="submit" name="send" class="btn btn-primary">Submit</button>
                        
                    </div>
                </div>

            </form>

            </div>

    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form method="post" action="<?php echo base_url();?>POF/send_req">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>Request Payoff</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="row addspace">
                <div class="col-md-12">
                    <label>Property:</label>
                    <select class="form-control" name="property">
                        <option value="">Select One</option>
                        <?php foreach($ActivePropertyname as $key => $row){ ?>
                            <option value="<?php echo $key;?>"><?php echo $row;?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="row addspace">
                <div class="col-md-12">
                    <label>Expected Payoff Date:</label>
                    <input type="text" name="exp_date" class="form-control datepicker">
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Send</button>
      </div>
    </div>
  </div>
</div>