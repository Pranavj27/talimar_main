<style type="text/css">
    .addspace{
        margin-top:10px;
    }
</style>
<div class="page-container">
                <!-- BEGIN SIDEBAR -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content" style="padding-left: 30px;padding-right: 30px;">
                <!-- BEGIN PAGE HEADER-->
                <div class="row">
                	<div class="col-md-12">
		                <div class="page-bar" style="border-bottom: 0px !important;padding-top: 10px;padding-bottom: 10px;">
		                    <h4><b>User Profile</b></h4>
		                </div>
		            </div>
		        </div>

                <div class="row">
                    <div class="col-md-12">
                        <?php if($this->session->flashdata('error') !=''){ ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <?php echo $this->session->flashdata('error');?> 
                            </div>
                        <?php } ?>
                        <?php if($this->session->flashdata('success') !=''){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <?php echo $this->session->flashdata('success');?> 
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h4 class="deed_heading">Contact Information:</h4>
                    </div>
                </div>

                <div class="row addspace">
                    <div class="col-md-12">
                        <label><b>Contact Name: </b></label> <?php echo $contactinfo[0]->contact_firstname.' '.$contactinfo[0]->contact_lastname;?>
                    </div>
                </div>

                <div class="row addspace">
                    <div class="col-md-12">
                        <label><b>Contact Phone: </b></label> <?php echo $contactinfo[0]->contact_phone;?>
                    </div>
                </div>

                <div class="row addspace">
                    <div class="col-md-12">
                        <label><b>Contact E-mail: </b></label> <?php echo $contactinfo[0]->contact_email;?>
                    </div>
                </div>
                <div class="row addspace">
                    <div class="col-md-12">
                        <label><b>Contact Mailing Address: </b></label> <?php echo $contactinfo[0]->contact_mailing_address.'; '.$contactinfo[0]->contact_mailing_city.', '.$contactinfo[0]->contact_mailing_state.' '.$contactinfo[0]->contact_mailing_zip;?>
                    </div>
                </div>

                <div class="row addspace">
                    <div class="col-md-12">
                        <h4 class="deed_heading">Upload Image:</h4>
                    </div>
                </div>

                 <div class="row addspace">
                    
                    <div class="col-md-8">
                        <button data-toggle="modal" data-target="#imageModal" class="btn btn-primary">Choose Image</button>
                    </div>
                </div>

            </div>
        </div>
</div>


<!-- Modal -->
<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form method="post" action="<?php echo base_url();?>Home/uploadImage" enctype="multipart/form-data">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>Profile Image</b></h5>
        
      </div>
      <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Choose File:</label>
                    <input type="file" name="file" class="form-control">
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="upload" class="btn btn-primary">Upload</button>
      </div>
    </form>
    </div>
  </div>
</div>