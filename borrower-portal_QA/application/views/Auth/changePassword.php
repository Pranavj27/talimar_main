<style type="text/css">
    .addspace{
        margin-bottom: 10px;
    }

    label{
        font-weight: 600;
    }
    .error{
        color: red;
    }
</style>
<div class="page-container">
                <!-- BEGIN SIDEBAR -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content" style="padding-left: 30px;padding-right: 30px;">
                <!-- BEGIN PAGE HEADER-->
                

                <div class="row">
                    <div class="col-md-4" id="messagesChange"></div>
                </div>

                <!-- <div class="row">
                    <div class="col-md-12">
                        <h4 class="deed_heading">Contact Information:</h4>
                    </div>
                </div> -->
                <?php // print_r($displayallaccount); ?>
                <form method="post" id="changePassword" novalidate="novalidate">
                    <div class="row addspace">
                        <div class="col-md-4">
                            <label>Old Password</label>
                            <input type="password" name="oldPassword" id="oldPassword" class="form-control" placeholder="Enter new password" required>
                        </div>
                    </div>

                    <div class="row addspace">
                        <div class="col-md-4">
                            <label>New Password</label>
                            <input type="password" name="newPassword" id="newPassword" class="form-control" placeholder="Enter new password" required>
                        </div>
                    </div>

                    <div class="row addspace">
                        <div class="col-md-4">
                            <label>Confirm Password</label>
                            <input type="password" name="confirmPassword" id="confirmPassword" class="form-control" placeholder="Enter confirm password" required>
                        </div>
                    </div>

                    <div class="row addspace">
                        <div class="col-md-12">
                            <button type="submit" name="update" class="btn btn-primary">Change Password</button>
                        </div>
                    </div>
                </form>

            </div>

    </div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function($) {
        
                $("#changePassword").validate({
                rules: {
                     oldPassword: "required",
                      newPassword: {
                        required: true,
                        minlength: 6
                      },
                      confirmPassword: {
                        required: true,
                        equalTo: "#newPassword"
                      }
                 
                },
               messages: {
                  oldPassword: "Please enter your old Password",
                  newPassword: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 6 characters long"
                  },
                  confirmPassword: {
                    required: "Please provide a confirm password",
                    equalTo: "Confirm password not match"
                  }

            },
            errorPlacement: function(error, element) 
            {
                if ( element.is(":radio") ) 
                {
                    error.appendTo( element.parents('.form-group') );
                }
                else 
                { // This is the default behavior 
                    error.insertAfter( element );
                }
             },
            submitHandler: function(form) {
                    changePassword();
            }        
        });
    });

function changePassword()
{
    let oldPassword = $('#oldPassword').val();
    let newPassword = $('#newPassword').val();
    let confirmPassword = $('#confirmPassword').val();
    
    $.ajax({
        type : 'POST',
        url  : '<?php echo base_url()."Home/changePasswordUpdate";?>',
        data : {'oldPassword':oldPassword,'newPassword':newPassword,'confirmPassword':confirmPassword},
        success : function(response){
            var json =  JSON.parse(response);
            // console.log(json);
            var message = json.message;
            $('form#changePassword').trigger("reset");
            $("#messagesChange").html(message); 
        }
    });
}
</script>
